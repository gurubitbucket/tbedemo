<?php
	class Examination_Model_Manualmarksentry extends Zend_Db_Table {
		
		
		protected $_name = 'tbl_studentapplication';
	

		
		
	 public function fncheckSuperUserPwd($password)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $lobjDbAdpt->select()
						->from(array("a" => "tbl_user"))
						->join(array('b' => 'tbl_definationms'),'a.IdRole = b.idDefinition')
						->where("a.passwd =?",$password)
						->where("b.DefinitionDesc = 'Superadmin'");
						//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
     
     
	 public function fnSearchstudentabsent($larrformData)
     {
     	//print_r($larrformData);die();
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
                                    ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
                                    ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename','b.IdProgrammaster as idprogram'))
			           ->join(array('c' => 'tbl_center'),'c.idcenter= a.Examvenue',array('c.centername as Venue'))	
			           ->join(array('m' => 'tbl_registereddetails'),'m.IDApplication= a.IDApplication',array('m.Regid as Regid'))	
			           ->join(array("i" =>"tbl_managesession"),'i.idmangesession=a.Examsession',array("i.managesessionname","i.starttime","i.endtime"))		
                                    ->where("a.DateTime=?",$larrformData['Dates'])
                                    ->where("a.Examvenue=?",$larrformData['Venues'])
                                      ->where("a.Examsession=?",$larrformData['examsession'])
                                       ->where("a.pass=4")
                                       ->order("a.FName");			
			//	if($larrformData['Venues']) $lstrSelect->where("a.idcenter = ?",$larrformData['Venues']);
				//if($larrformData['Dates']) $lstrSelect->where("b.ExamDate = ?",$larrformData['Dates']);
			//	echo $lstrSelect;die();
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     
     
     public function fngetintialgracetimeinfo()
     {
     	
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_config"),array('a.examgracetime'))
										  ->where("a.idConfig=1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     }
     
		  public function newfnGetcitydetailsgetsecid($venue)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_center"),array("a.city"))
						                  ->where("a.Active=1")
										  ->where("a.idcenter =?",$venue); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  	
	   public function fngetschedulerexception($days,$lintcity)
   {
   	  $db 	= 	Zend_Db_Table::getDefaultAdapter();	
	$lstrSelect = $db->select()
	                         ->from(array("a" =>"tbl_schedulerexception"))
	                         ->where('a.Date=?',$days)
	                         ->where('a.idcity=?',$lintcity);
	    $larrResult = $db->fetchRow($lstrSelect);
	    return $larrResult;
   	   
   }
   
     
    

     
     
		 public function fnGetVenueNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
				 				 ->join(array("c"=>"tbl_newschedulervenue"),'c.idvenue=a.idcenter',array(''))	
				 				  ->join(array("e"=>"tbl_newscheduler"),'e.idnewscheduler=c.idnewscheduler',array(''))
				 				 ->where("e.Active =1")			 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   

	  
		 public function fngetschedulerofcenterstart($idvenue,$idsession,$iddate)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_centerstartexam"),array("a.idSession"))
				 				 ->where("a.idcenter =?",$idvenue)	
				 				  ->where("a.idSession =?",$idsession)		
				 				   ->where("a.ExamDate =?",$iddate);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	   }
	   
	   
	   public function fngetalreadythere($idvenue,$idsession,$iddate)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_gracetime"),array("a.Idgracetime"))
				 				 ->where("a.Idvenue =?",$idvenue)	
				 				  ->where("a.Idsession =?",$idsession)		
				 				   ->where("a.ExamDate =?",$iddate);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	   	
	   }
	   
	public function newfncurrentgetyear($prog)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect="select `year`as `key` , `year` as `value` from tbl_newscheduler where active=1 group by `year`";
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
}



	
	public function newfnGetCitylistforyearvenues($idyear,$curmonth){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->where("a.Year  = ?",$idyear)
										  ->where("a.To>=?",$curmonth)
										  ->where("a.Active  = 1")
										  ->group("d.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
		
	public function fnnewmonthcaleshowlatest($idvenue,$year)
{
	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
	 $lstrSelect = "SELECT a.*
FROM tbl_newscheduler as a
where  a.Active=1
and a.idnewscheduler in (SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
and a.Year=$year";
	// echo $lstrSelect;die();
	 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}



		 public function  fngetschedulerofdate($ids,$days)
       {
       	
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler"))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler')
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }
       
       
		
 public function fngetdayofdate($iddate)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	  $sql = "select DAYOFWEEK('$iddate') as days"; 
				//echo $sql; die();
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
     public function   fngetschedulerofdatesessions($idscheduler)
     {
     	// ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.idcenter","value"=>"a.centername"))
     	
     	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
										  ->join(array("c"=>"tbl_newschedulersession"),'a.idmangesession=c.idmanagesession')
										  ->where("c.idnewscheduler in ($idscheduler)");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     	 
     }
     
     
     public function fninsertstudentmarksdetails($larrformData)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    
											
       for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){
		 $table = "tbl_studentmarks";
		 
		 
		 $result=self::getnofmarks($larrformData['Program'][$larrformData['IDApplication'][$linti]]);
		 $studentid=$larrformData['IDApplication'][$linti];
		 $pass=$result['Pass'];
		 //echo $pass;die();
		 $correct=$larrformData['Total'][$larrformData['IDApplication'][$linti]];
		 $noofquestions=$result['NosOfQues'];
         $percentage = ($correct/$noofquestions)*100;
		//   $larrgetgrade= self::fngetgradedetails($percentage);
		 // $Gradess=$larrgetgrade['gradename'];
$larrgetidgradeset=self::fngetgradesetbybatchid($result['IdBatch']);
						     $idgradeset=$larrgetidgradeset['Idgradeset'];
		   $larrgetgrade= self::fngetgradedetailsbyset($percentage,$idgradeset);
		  $Gradess=$larrgetgrade['gradename'];
		  
				        	if($Gradess!='F')  
				        	{

	$passresult=1;
				    $larrpass = self::fnupdatepass($studentid,$passresult,$result['IdBatch']);
				    $passpercent= (int)(($correct/$noofquestions)*100);

    $larrgetgrade= self::fngetgradedetailsbyset($passpercent,$idgradeset);
		  $Gradess=$larrgetgrade['gradename'];
		  
				   
					/*  if($passpercent>=85)
					  {
					  	$grade = 'A';
					  }
					  if($passpercent >= 70 && $passpercent <= 84)
					  {
					  	$grade = 'B';
					  }
					  if($passpercent >= 55 && $passpercent <=69)
					  {
					  	$grade = 'C';
					  }*/
				
		  }
		  else 
		  {
		  	$failresult=2;
				$larrpass = self::fnupdatepass($studentid,$failresult,$result['IdBatch']);
					$grade = $Gradess;
		  }
		 //print_r($larrformData['IDApplication']);die();
         $larrpostData = array( 'IDApplication'=>$larrformData['IDApplication'][$linti],	
		                    'NoofQtns'=>$result['NosOfQues'],	
		  					'Attended'=>$result['NosOfQues'],
		  					'Correct'=>$larrformData['Total'][$larrformData['IDApplication'][$linti]],
		                    'Grade' =>$grade);	
         			
	  $results=  $lobjDbAdpt->insert($table,$larrpostData);
		
		}

  }




 public function fngetgradesetbybatchid($idbatch)
    {
    	 /*  $db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->join(array('c'=>'tbl_tosmaster'),array("c.NosOfQues","c.TimeLimit","c.Pass"))
		             ->where("c.IdBatch = ?",$idbatch);
		    $result = $db->fetchRow($select);
		    return $result;*/
    	
        $db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_batchmaster'),array(""))
		            ->join(array('f'=>'tbl_programmaster'),'f.IdProgrammaster=a.IdProgrammaster',array("f.Idgradeset"))
		            ->where("a.IdBatch = ?",$idbatch);
		          //  echo $select;die();
		    $result = $db->fetchRow($select);
		    return $result;
		    
    	
    }
    
		  public function fngetgradedetailsbyset($percent,$idgradeset)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_gradesetdetails'),array("a.Gradename as gradename"))
		            ->where("a.percentagefrom <= ?",$percent)
		             ->where("a.percentageto >= ?",$percent)
		              ->where("a.Idgradeset=?",$idgradeset);
		             // echo $select;die();
		    $result = $db->fetchRow($select);
		    return $result;
    	
    }
    

  public function fngetgradedetails($percent)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_grademaster'),array("a.gradename"))
		            ->where("a.percentagefrom <= ?",$percent)
		             ->where("a.percentageto >= ?",$percent)
		              ->where("a.Active=1");
		             // echo $select;die();
		    $result = $db->fetchRow($select);
		    return $result;
    	
    }

  

  
	 public function fnupdatepass($idstudent,$result,$idbatch)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Pass'] = $result;	
		 $where = "IDApplication = '".$idstudent."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
		 
		 
		 $larrformData2['Cetreapproval'] = "1";	
		 
		    $larrformData2 = array( 'Cetreapproval'=>'1',	
		                    'IdBatch'=>$idbatch);	
		 $where = "IDApplication = '".$idstudent."'"; 	
		 $db->update('tbl_registereddetails',$larrformData2,$where);
    }
    
    
   
    
    public function fnupdatevenuetime($examdate,$examvenue,$exansession)
    {
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['VenueTime'] = '2';	
	 $where ="DateTime =".$examdate." and  	Examvenue =".$examvenue." and Examsession =".$exansession." and VenueTime =1 and Payment=1";
	 //echo $where;die();
		 $db->update('tbl_studentapplication',$larrformData1,$where);
    	
    }
    
    
    public function fngetexamdetails($idapp)
    {
    	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_studentapplication"),array("a.DateTime","a.Examvenue","a.Examsession"))
										  ->where("a.IDApplication=?",$idapp);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
    	
    }
  public function getnofmarks($idprogram)
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_tosmaster"),array("a.NosOfQues","a.Pass","a.TimeLimit","a.IdBatch"))
							 			    ->join(array("c"=>"tbl_batchmaster"),'a.IdBatch=c.IdBatch',array("c.IdProgrammaster"))
										  ->where("c.IdProgrammaster=?",$idprogram)
										  ->where("a.active=1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  	
  }
  
  
  
    public function fngetpartsid($program)
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_tosmaster"),array(""))
							 			    ->join(array("c"=>"tbl_batchmaster"),'a.IdBatch=c.IdBatch',array(""))
							 			       ->join(array("d"=>"tbl_batchdetail"),'d.IdBatch=c.IdBatch',array("group_concat(IdPart SEPARATOR ',') as parts"))
										  ->where("c.IdProgrammaster =?",$program)
										  ->where("a.active=1");
										 // echo $lstrSelect;die();
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  	
  }
	    public function fngetcountquestion($program,$type)
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_tosmaster"),array(""))
							 			    ->join(array("c"=>"tbl_batchmaster"),'a.IdBatch=c.IdBatch',array(""))
							 			       ->join(array("d"=>"tbl_tosdetail"),'a.IdTOS=d.IdTOS',array("sum(d.NosOfQuestion) as quescount"))
										  ->where("c.IdProgrammaster =?",$program)
										  ->where("a.active=1")
										  ->where("d.IdSection like  ? '%'",$type);
										  //$lstrSelect->where("a.Fname like '%' ? '%'",$larrformData['Studentname']);	
										  //echo $lstrSelect;die();
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  	
  }
  
  
  
	 public function getthesessiontimings($sessionid)
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("i" => "tbl_managesession"),array("i.managesessionname","i.starttime","i.endtime"))
										  ->where("i.idmangesession =?",$sessionid);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  	
  }
	    
  
  
  
  
  
public function fngetcenterstartexam($larrformData)
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("i" => "tbl_centerstartexam"),array("i.idcenterstartexam"))
										  ->where("i.idcenter =?",$larrformData['Venues'])
										  ->where("i.ExamDate=?",$larrformData['Dates'])
										  ->where("i.idSession=?",$larrformData['examsession']);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  	
  }
  
  public function fngetprogramwisestudent($idapplications)
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("i" =>"tbl_studentapplication"),array("distinct(i.Program) as program"))
										  ->where("i.IDApplication in ($idapplications)");
										 // echo $lstrSelect;die();
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
	public function fnstartcenterstartexam($larrformData,$program,$starttime,$endtime)
  {
  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    				
    
		 $table = "tbl_centerstartexam";
		 $arr=explode('-',$larrformData['Dates']);
		 $arrtime=explode(':',$starttime);
		 $startim=$arrtime[0]*60;
		 $startimeinminutes=$startim+$arrtime[1];
		 //$updtimings=$starttime*;
	 $result=self::getnofmarks($program);
		 
         $larrpostData = array( 'idcenter'=>$larrformData['Venues'],	
		                    'sessionid'=>'a076dc7bffd7139b1e75db45edafb67',	
		  					'Program'=>$result['IdProgrammaster'],
		  					'Totaltime'=>$result['TimeLimit'],
		                    'Startedtime' =>$startimeinminutes,
                            'Date'=>$arr[2],
        					'idSession'=>$larrformData['examsession'],
		                    'ExamDate' =>$larrformData['Dates'],
                            'ExamStartTime'=>$starttime,
         					'CloseTime'=>$endtime,
         );	
         			
	  $results=  $lobjDbAdpt->insert($table,$larrpostData);
		
		
  }
  
  
  public function fncheckstartprogramwise($program,$larrformData)
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("i" => "tbl_centerstartexam"),array("i.idcenterstartexam"))
										  ->where("i.idcenter =?",$larrformData['Venues'])
										  ->where("i.ExamDate=?",$larrformData['Dates'])
										    ->where("i.Program=?",$program)
										  ->where("i.idSession=?",$larrformData['examsession']);
										//  echo $lstrSelect;die();
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  	
  	
  }
  
     public function fninsertstudentparttwisedetails($larrformData)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    
					$ldtsystemDate = date( 'Y-m-d H:i:s' );						
       for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){
		 $table = "tbl_studentdetailspartwisemarks";
		 
		 
         $larrpostData = array( 'IDApplication'=>$larrformData['IDApplication'][$linti],	
		                    'Regid'=>$larrformData['regid'][$larrformData['IDApplication'][$linti]],	
		  					'A'=>$larrformData['PartA'][$larrformData['IDApplication'][$linti]],
		  					'B'=>$larrformData['PartB'][$larrformData['IDApplication'][$linti]],
		                    'C' =>$larrformData['PartC'][$larrformData['IDApplication'][$linti]],
                            'UpdDate'=>$ldtsystemDate);	
         			
	  $results=  $lobjDbAdpt->insert($table,$larrpostData);
		
		}

  }
  
  
  
  
  
	  
     public function fninsertintomanualentry($larrformData,$UpdUser)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    
					$ldtsystemDate = date( 'Y-m-d H:i:s' );						
       for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){
		 $table = "tbl_manualmarksentry";
		 
		 
         $larrpostData = array( 'IDApplication'=>$larrformData['IDApplication'][$linti],	
		                    'UpdUser'=>$UpdUser,
                            'UpdDate'=>$ldtsystemDate);	
         			
	  $results=  $lobjDbAdpt->insert($table,$larrpostData);
		
		}

  }
  
	 public function fninsertstudentstartexamdetailsdetails($larrformData,$starttime,$endtime)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    				
       for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){
		 $table = "tbl_studentstartexamdetails";
		 $updtimings=$larrformData['Dates'].' '.$starttime;
		$upddate="$updtimings";
		 
         $larrpostData = array( 'IDApplication'=>$larrformData['IDApplication'][$linti],	
		                    'Starttime'=>$starttime,	
		  					'Endtime'=>$endtime,
		  					'Submittedby'=>2,
		                    'UpdDate' =>$upddate,
                            'StudentIpAddress'=>0);	
         			
	  $results=  $lobjDbAdpt->insert($table,$larrpostData);
		
		}

  }
  
     
     public function newgetscheduleryear($year)
     {
     	
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler","a.To","a.From"))
                                          ->where("a.Active = 1")
										  ->where("a.Year=?",$year); 
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     	
     }       

     
     
		 public function  fngetschedulerofdatevenuestudent($iddate)
       {
       	
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
                                        ->from(array("a" => "tbl_venuedateschedule"),array(""))
                                        ->join(array("f"=>"tbl_center"),'a.idvenue=f.idcenter',array("key"=>"f.idcenter","value"=>"f.centername"))
                                        ->where("a.Active = 1")
                                          ->where("a.date=?",$iddate)
                                        //  ->where("a.centeractive=1")
                                             ->where("a.Reserveflag=1")
										    ->group("f.idcenter")
										    ->order("f.centername");
        
         // $lstrSelect = "SELECT b.idcenter as `key`, b.centername as `value` FROM tbl_venuedateschedule as a,tbl_center b  WHERE a.idvenue=b.idcenter and   a.date ='$iddate' and a.Active =  1 and a.Reserveflag=1  
 	      //   union  SELECT b.idcenter as `key`, b.centername as `value` FROM tbl_venuedateschedule as a,tbl_center b  WHERE a.idvenue=b.idcenter and   a.date ='$iddate'  and a.Allotedseats>0 ";
	//echo $lstrSelect;die();
          
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
				
       
       }
       
       
       
       
		 public function  fngetschedulerofdatesessionstudent($iddate,$venueid)
       {
       	
     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 /*	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array(""))
									      ->join(array("f"=>"tbl_managesession"),'a.idsession=f.idmangesession',array("key"=>"f.idmangesession","value"=>"f.managesessionname"))
                                              ->where("a.Active = 1")
									      ->where("a.date=?",$iddate)
                                          ->where("a.centeractive=1")
                                            ->where("a.idvenue=?",$venueid)
										  ->where("a.Reserveflag=1")
										   ->group("f.idmangesession");*/
     $lstrSelect = "SELECT b.idmangesession as `key`, b.managesessionname as `value` FROM tbl_venuedateschedule as a,tbl_managesession b  WHERE a.idsession=b.idmangesession and   a.date ='$iddate' and a.Active =  1 and a.Reserveflag=1 and  a.idvenue='$venueid'
 	         union  SELECT b.idmangesession as `key`, b.managesessionname as `value` FROM tbl_venuedateschedule as a,tbl_managesession b  WHERE a.idsession=b.idmangesession and   a.date ='$iddate'  and  a.idvenue='$venueid'
 	      and a.Allotedseats>0 ";
     
     
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       	
				
       
       }
       
       
       
       
		 public function  fngetschedulerofdatesessionstudentforcurrentdate($ids,$days,$iddate,$venueid,$time)
       {
       	
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array(""))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulervenue"),'a.idnewscheduler=d.idnewscheduler',array(""))
									      ->join(array("e"=>"tbl_newschedulersession"),'a.idnewscheduler=e.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_managesession"),'f.idmangesession=e.idmanagesession',array("key"=>"f.idmangesession","value"=>"f.managesessionname"))
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										  // ->where("e.idmanagesession not in ( select idSession from tbl_centerstartexam where ExamDate='$iddate' and idcenter='$venueid')")
										   ->where("c.Days=?",$days)
										     // ->where("f.endtime > '$time'")
										      ->where("d.idvenue=?",$venueid)
										   ->group("e.idmanagesession");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }
   
       
	
       
       
		
}
