<?php
	class Examination_Model_Autovenue extends Zend_Db_Table {


	public function  fngetvenuedateschedule($iddate)
       {
       			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			  ->where("a.date='$iddate'")
										  ->where("a.Active = 1")
										    ->where("a.Reserveflag= 1");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       }

      
    public function fngetallvenuesexamdate($iddate)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			  ->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("b.*"))
							 			  ->where("a.date='$iddate'")
							 			  ->where("b.ipaddress=1")
							 			    ->where("a.Allotedseats>0")
							 			  ->group("b.idcenter")
							 			  ->order("b.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
   
 /*   public function fninsertintochronevenue($larrformdata,$larrresultlastid)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$date = $larrformdata['Date'];
    	$cnt = count($larrformdata['idcenter']);
    
    	for($i=0;$i<$cnt;$i++)
    	{
    		
    		
    		$idcenter = $larrformdata['idcenter'][$i];
    		
    		$table = "tbl_cronevenue";
           $postData = array(		
							'idcenter' => $idcenter,		
            				'Examdate' =>$date,
           					'idautomail' => $larrresultlastid		
						);			
	        $lobjDbAdpt->insert($table,$postData);
    	}
    	 
	        
    }*/
    
	 public function fninsertintochronevenuequestions($larrformdata)
    {
    	
    	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$cnt = count($larrformdata['idcenter']);
    	 for($i=0;$i<$cnt;$i++)
    	 {
			   	$schedulerdate = $larrformdata['Schedulerdate'];
			   	$schedulertime = $larrformdata['ScheduleStartTime'];
			   	$schedulerdattime = $schedulerdate.' '.$schedulertime;
			    
			   	$interval = $interval+10;
			   	
			   	$queryoftime = "SELECT DATE_ADD('$schedulerdattime', INTERVAL '$interval:0' MINUTE_SECOND) as Timeintervalss";
			   	$larresulttime = $lobjDbAdpt->fetchRow($queryoftime);
			   	$larrscheduertime = $larresulttime['Timeintervalss'];
			   	$larraddedtime = explode(' ',$larrscheduertime);
			   	$addeddate = $larraddedtime[0];
			   	$addedtime = $larraddedtime[1];
			    
			   	
			    $postdata = array('schdeduleddate'=>$addeddate,
			                      'scheduledtime'=>$addedtime,
			                      'UpdDate' => $larrformdata['UpdDate'],
			                      'UpdUser' => $larrformdata['UpdUser']);
			    $lobjDbAdpt->insert('tbl_automail',$postdata);
			    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
			   	
			   	
    		   $idcenter = $larrformdata['idcenter'][$i];
    		
    		   $table = "tbl_chronequestions";
               $postData = array(		
							'upddate' => date('Y-m-d H:i:s'),		
            				'upduser' =>$larrformdata['UpdUser'],
                             'Status'=>0,
          					 'idcenter' => $idcenter,
           					'idautomail' => $lintlastinsid
						);			
	           $lobjDbAdpt->insert($table,$postData);
    	}
	        
    }
    
    
    
    
    public function fngetvenuesfordate($date)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_cronevenue"),array("a.*"))
							 			  ->where("a.Examdate='$date'");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
    
	public function fngetscheduledvenue($date)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_cronevenue"),array("a.*"))
							 			  ->where("a.Examdate='$date'");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
    
    
    public function fngetallcandidates($idcenter,$iddate)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_studentapplication"),array("a.*"))
							 			   ->join(array("b" => "tbl_registereddetails"),'a.IDApplication=b.IDApplication',array("b.*"))
							 			  ->where("a.Examvenue='$idcenter'")
							 			   ->where("a.VenueTime=0")
							 			     ->where("a.Payment=1")
  ->where("a.pass=3")
							 			   ->where("a.DateTime='$iddate'")
							 			   ->group("a.IDApplication");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
    
    
    public function fnupdatedchronedstatus($idcenter,$iddate)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_cronevenue"; 
			 $where1 = "idcenter = '".$idcenter."'  AND Examdate ='".$iddate."'"; 	  
						   $larrupdatedatas = array(		
							'Status' => 1
						);
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
    }
    
	public function fnupdatedchronedstatusfailed($idcenter,$iddate)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_cronevenue"; 
			 $where1 = "idcenter = '".$idcenter."'  AND Examdate ='".$iddate."'"; 	  
						   $larrupdatedatas = array(		
							'Status' => 2
						);
				
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
	         
	         
	         self::fngetvenuename($idcenter);
	     
    }   

	    public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}
	
	public function fngetreceivermail()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_config"));
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
	}
		
    public function fngetvenuename($idcenter)
    {
    		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_center"),array("a.*"))
							 			   ->where("a.idcenter='$idcenter'");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			$venuename = $larrResult['centername'];
			
			$larrEmailTemplateDesc = self::fnGetEmailTemplateDescription("Chrone Venue Fail");
			
					$larconfigdetails = self::fngetreceivermail();
			
			require_once('Zend/Mail.php');
			require_once('Zend/Mail/Transport/Smtp.php');
			
			 $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$lstrEmailTemplateBody = str_replace("[Venue]",$venuename,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
							$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'tbe@ibfim.com', 'password' => 'ibfim2oi2');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'tbe@ibfim.com';
										$sender = 'tbe';
										$receiver_email = $larconfigdetails['Schedulerpushemail'];
										$receiver = 'Admin';
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);							
									$result = $mail->send($transport);
											
    }
    
    public function fngetstatusofvenue($idvenue,$date)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_cronevenue"),array("a.*"))
							 			  ->where("a.Examdate='$date'")
							 			   ->where("a.idcenter='$idvenue'");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
    }
    
    public function fngetallvenues()
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_center"),array("a.*"))
							 			  ->where("a.ipaddress=1")
							 			  ->order("a.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }

      public function fngetvenuequestions($idcenter,$date)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			   ->from(array("a" => "tbl_chronequestions"),array("a.idchronequestions","a.idcenter","a.upduser","a.upddate","a.Status"))
							 			   	  ->where("a.idcenter=$idcenter")
							 			  ->where("date(a.upddate)='$date'");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
    }
   
    public function fninsertintochronescheduler($larrformdata)
    {
          $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$cnt = count($larrformdata['idcenter']);

    	for($i=0;$i<$cnt;$i++)
    	{
    		
    			$schedulerdate = $larrformdata['Schedulerdate'];
			   	$schedulertime = $larrformdata['ScheduleStartTime'];
			   	$schedulerdattime = $schedulerdate.' '.$schedulertime;
			    
			   	$interval = $interval+2;
			   	
			   	$queryoftime = "SELECT DATE_ADD('$schedulerdattime', INTERVAL '$interval:0' MINUTE_SECOND) as Timeintervalss";
			   	$larresulttime = $lobjDbAdpt->fetchRow($queryoftime);
			   	$larrscheduertime = $larresulttime['Timeintervalss'];
			   	$larraddedtime = explode(' ',$larrscheduertime);
			   	$addeddate = $larraddedtime[0];
			   	$addedtime = $larraddedtime[1];
			    
			   	
			    $postdata = array('schdeduleddate'=>$addeddate,
			                      'scheduledtime'=>$addedtime,
			                      'UpdDate' => $larrformdata['UpdDate'],
			                      'UpdUser' => $larrformdata['UpdUser']);
			    $lobjDbAdpt->insert('tbl_automail',$postdata);
			    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
			   	
			    
    		$idcenter = $larrformdata['idcenter'][$i];
    		
    		$table = "tbl_chronescheduler";
           $postData = array(		
							'upddate' => date('Y-m-d H:i:s'),		
            				'upduser' =>$larrformdata['UpdUser'],
           					'Schedulerfromdate' =>$larrformdata['FromDate'],		
            				'Schedulertodate' =>$larrformdata['ToDate'],
          					 'idcenter' => $idcenter,
           					'idautomail' => $lintlastinsid	
						);			
	        $lobjDbAdpt->insert($table,$postData);
    	}
    }
    
    public function fngetallscheduler($larrformdata)
    {
    	$fromdate = $larrformdata['FromDate'];
    	$todate = $larrformdata['ToDate'];
    	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			  ->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("b.*"))
							 			  ->where("a.date>='$fromdate'")
							 			  	  ->where("a.date<='$todate'")
							 			  ->where("b.ipaddress=1")
							 			  ->where("a.Active=1")
							 			  ->where("a.Reserveflag=1")
							 			  ->group("b.idcenter")
							 			  ->order("b.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    	    
    }
    
    public function fngetquestions()
    {
    	  	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_questions"),array("a.*"));
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
							 			  
    }
    
	    public function fngetanswers()
    {
    	  	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_answers"),array("a.*"));
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
							 			  
    }
    
    public function fngetvenusforquestions($date)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_chronequestions"),array("a.*"))
							 			  ->where("date(upddate)='$date'");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
    }
    
	    public function fngetvenusforscheduler($date)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_chronescheduler"),array("a.*"))
							 			  ->where("date(upddate)='$date'");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
    }   
    
    
	 public function fnupdatedchronedstatusforquestions($idchronequestions)
    {
    	
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_chronequestions"; 
			 $where1 = "idchronequestions = '".$idchronequestions."'"; 	  
						   $larrupdatedatas = array(		
							'Status' => 1
						);
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
    }
    
    	public function fnupdatedchronedstatusforquestionsfailed($idchronequestions)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_chronequestions"; 
			 $where1 = "idchronequestions = '".$idchronequestions."'"; 	  
						   $larrupdatedatas = array(		
							'Status' => 2
						);
				
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
    } 
    
    
    public function fngetvenueschedulerdates($idcenter,$fromdate,$todate)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			  ->where("a.idvenue='$idcenter'")
							 			  ->where("a.date>='$fromdate'")
							 			  ->where("a.date<='$todate'")
							 			  ->where("a.Active=1");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
    }
    
	public function fngetvenueidscheduler($idcenter,$fromdate,$todate)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.idnewscheduler"))
							 			  ->where("a.idvenue='$idcenter'")
							 			  ->where("a.date>='$fromdate'")
							 			  ->where("a.date<='$todate'")
							 			  ->where("a.Active=1")
							 			  ->group("a.idnewscheduler");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
    }
    
    
    public function fngetnewscheduler($idnewscheduler)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
							 			  ->where("a.idnewscheduler in($idnewscheduler)");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
    }
    
	public function fngetnewschedulercourse($idnewscheduler)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newschedulercourse"),array("a.*"))
							 			  ->where("a.idnewscheduler in($idnewscheduler)");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
    }
    
    
	 public function fnupdatedstudentapplication($idapplication)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_studentapplication"; 
			 $where1 = "IDApplication = '".$idapplication."'"; 	  
						   $larrupdatedatas = array(		
							'VenueTime' => 1
						);
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
    }
    
	 public function fninsertschedulerdetails($larrformdata)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   // $noofstu = count($larrformdata['idapp']);
		    $postdata = array('schdeduleddate'=>$larrformdata['Schedulerdate'],
		                      'scheduledtime'=>$larrformdata['ScheduleStartTime'],
		                      'UpdDate' => $larrformdata['UpdDate'],
		                      'UpdUser' => $larrformdata['UpdUser']);
		    $lobjDbAdpt->insert('tbl_automail',$postdata);
		       $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
		       return $lintlastinsid;
    }
    
    public function fninsertintochronevenue($larrformdata)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$examdate = $larrformdata['Date'];
		   // $noofstu = count($larrformdata['idapp']);
		   $interval=0;
		   for($i=0;$i<count($larrformdata['idcenter']);$i++)
		   {
			   	$larrformdata['ScheduleStartTime'];
			   	
			   	$schedulerdate = $larrformdata['Schedulerdate'];
			   	$schedulertime = $larrformdata['ScheduleStartTime'];
			   	$schedulerdattime = $schedulerdate.' '.$schedulertime;
			    
			   	$interval = $interval+1;
			   	
			   	$queryoftime = "SELECT DATE_ADD('$schedulerdattime', INTERVAL '$interval:0' MINUTE_SECOND) as Timeintervalss";
			   	$larresulttime = $lobjDbAdpt->fetchRow($queryoftime);
			   	$larrscheduertime = $larresulttime['Timeintervalss'];
			   	$larraddedtime = explode(' ',$larrscheduertime);
			   	$addeddate = $larraddedtime[0];
			   	$addedtime = $larraddedtime[1];
			    
			   	
			    $postdata = array('schdeduleddate'=>$addeddate,
			                      'scheduledtime'=>$addedtime,
			                      'UpdDate' => $larrformdata['UpdDate'],
			                      'UpdUser' => $larrformdata['UpdUser']);
			    $lobjDbAdpt->insert('tbl_automail',$postdata);
			    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
			       //fninsertintochronevenue($larrformData,$lintlastinsid);
			       
			       
			    $idcenter = $larrformdata['idcenter'][$i];
	    		
	    			$table = "tbl_cronevenue";
	          	   $postData = array(		
								'idcenter' => $idcenter,		
	            				'Examdate' =>$examdate,
	           					'idautomail' => $lintlastinsid		
							);			
		        $lobjDbAdpt->insert($table,$postData);
			       
		   }
    }
    
	public function fngetscheduledetails($shcdate,$shctime)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect =  "SELECT a.idautomail,a.scheduledtime,a.schdeduleddate
			   				FROM tbl_automail a
			   				WHERE a.scheduledtime = '$shctime' and a.schdeduleddate = '$shcdate'";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		
	/*
	 * function for fetching all the venues where questions has to be pulled for the particular automail
	 */
	public function fngetallvenuesforidautomail($idautomail)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_chronequestions"),array("a.*"))
							 			  ->where("idautomail in ($idautomail)")
							 			  ->group("a.idautomail");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
	}
	
	public function fngetvenuesforidautomail($idautomail)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_chronequestions"),array("a.*"))
							 			  ->where("idautomail ='$idautomail'");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
	}
	
	
	public function fngetallschedulervenuesforidautomail($idautomail)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_chronescheduler"),array("a.*"))
							 			   ->where("idautomail in ($idautomail)")
							 			    ->group("a.idautomail");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
	}
	
	public function fngetscheduleridautomail($idautomail)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_chronescheduler"),array("a.*"))
							 			  ->where("idautomail ='$idautomail'");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
	}
	
	public function fnupdatedchronedstatusforschedulerfailed($idchronescheduler)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_chronescheduler"; 
			 $where1 = "idchronescheduler = '".$idchronequestions."'"; 	  
						   $larrupdatedatas = array(		
							'Status' => 1
						);
				
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
	}
	
	public function fnupdatedchronedstatusforschedulersucess($idchronescheduler)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_chronescheduler"; 
			 $where1 = "idchronescheduler = '".$idchronequestions."'"; 	  
						   $larrupdatedatas = array(		
							'Status' => 1
						);
				
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
	}
	
	public function fngetallcandidatesvenuesforidautomail($idautomail)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_cronevenue"),array("a.*"))
							 			  ->where("idautomail in ($idautomail)")
							 			  ->group("a.idautomail");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
	}
	
	public function fngetschedulerstudentsvenuesidautomail($idautomail)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_cronevenue"),array("a.*"))
							 			  ->where("idautomail ='$idautomail'");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
	}
	public function fngetstatusofemailtemplate($idvenue)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_chroneemailtemplate"),array("a.*"))
										  ->join(array("b"=>"tbl_center"),'a.idcenter=b.idcenter',array("b.*"))
							 			 // ->where("a.Examdate='$date'")
							 			   ->where("a.idcenter='$idvenue'")
										   ->where("b.Nooffloors!=1" )
										     ->where("b.Active=1" )
										   ->group("b.idcenter")
							 			   ->order("b.centername");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
    }
	 public function fngetallvenuesforemailtemplate()
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("b" => "tbl_center"),array("b.*"))
							 			  //->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("b.*"))
							 			  //->where("a.date='$iddate'")
							 			  //->where("b.ipaddress=1")
							 			   // ->where("a.Allotedseats>0")
										   ->where("b.Nooffloors!=1" )
										   ->where("b.Active=1" )
							 			  ->group("b.idcenter")
							 			  ->order("b.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
	 public function fninsertintochroneemailtemplate($larrformdata)
    {
             // echo "<pre>";
				 // print_r($larrformdata);die();
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$cnt = count($larrformdata['idcenter']);
    	 for($i=0;$i<$cnt;$i++)
    	 {
			   	$schedulerdate = $larrformdata['Schedulerdate'];
			   	$schedulertime = $larrformdata['ScheduleStartTime'];
			   	$schedulerdattime = $schedulerdate.' '.$schedulertime;
			    
			   	$interval = $interval+1;
			   	
			   	$queryoftime = "SELECT DATE_ADD('$schedulerdattime', INTERVAL '$interval:0' MINUTE_SECOND) as Timeintervalss";
			   	$larresulttime = $lobjDbAdpt->fetchRow($queryoftime);
			   	$larrscheduertime = $larresulttime['Timeintervalss'];
			   	$larraddedtime = explode(' ',$larrscheduertime);
			   	$addeddate = $larraddedtime[0];
			   	$addedtime = $larraddedtime[1];
			    
			   	
			    $postdata = array('schdeduleddate'=>$addeddate,
			                      'scheduledtime'=>$addedtime,
			                      'UpdDate' => $larrformdata['UpdDate'],
			                      'UpdUser' => $larrformdata['UpdUser']);
			    $lobjDbAdpt->insert('tbl_automail',$postdata);
			    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
			   	
			   	
    		   $idcenter = $larrformdata['idcenter'][$i];
    		
    		    $table = "tbl_chroneemailtemplate";
                 $postData = array(		
							 //'upddate' => date('Y-m-d H:i:s'),		
            				 //'upduser' =>$larrformdata['UpdUser'],
                             'Status'=>0,
							 'idcenter' => $idcenter,
           					 'idautomail' => $lintlastinsid
						    );	
              						
	           $lobjDbAdpt->insert($table,$postData);
    	}  
    }
	}