<?php
class Examination_Model_Questionencryption extends Zend_Db_Table {
	


	
    public function fnGetQuestionDetails() {
    	  $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
					   ->from(array('a' => 'tbl_questions'),array('a.*'))
					   ->group("a.QuestionNumber");
		  $result = $db->fetchAll($select);
		return $result;    	  
    }
  
    public function fnupdatequestionids($questionids,$idquestions)
    {
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['QuestionId'] = $questionids;	
		 $where = "idquestions = '".$idquestions."'"; 	
		 $db->update('tbl_questions',$larrformData1,$where);
    }
    
    public function fngetquestionsgroup($questionnumber)
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
					   ->from(array('a' => 'tbl_questions'),array('a.*'))
					   ->where("a.QuestionNumber='$questionnumber'");
		  $result = $db->fetchAll($select);
		return $result; 
    }
    
    public function fnupdateanswerids($idquestions,$questionids)
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
					   ->from(array('a' => 'tbl_answers'),array('a.*'))
					   ->where("a.idquestion='$idquestions'");
		  $result = $db->fetchAll($select);
		  
		  for($i=0;$i<count($result);$i++)
		  {
					$answerids = $questionids.'-'.($i+1);
			    	$larrformData1['Answersid'] = $answerids;	
					$where = "idanswers = '".$result[$i]['idanswers']."'"; 	
					$db->update('tbl_answers',$larrformData1,$where);
		  }
		return $result;
    }
    
    public function fngetAllquestions()
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
					   ->from(array('a' => 'tbl_questions'),array('a.*'));
		  $result = $db->fetchAll($select);
		return $result; 
    }
    
    public function fngetAnswers($idquestion)
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
    	 $select = $db->select()	
					   ->from(array('a' => 'tbl_answers'),array('a.*'))
					   ->where("a.idquestion='$idquestion'");
		  $result = $db->fetchAll($select);
		  return $result; 
    }
    
    
    function encrypt($sData, $sKey){ 
    $sResult = ''; 
    for($i=0;$i<strlen($sData);$i++){ 
        $sChar    = substr($sData, $i, 1); 
        $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1); 
        $sChar    = chr(ord($sChar) + ord($sKeyChar)); 
        $sResult .= $sChar; 
    } 
    $encodedstring = self::encode_base64($sResult); 
    return $encodedstring;
   } 

function encode_base64($sData){ 
    $sBase64 = base64_encode($sData); 
    return strtr($sBase64, '+/', '-_'); 
} 

    public function fnupdateencrytptedquestions($engencryptedquestions,$malayencryptedquestions,$Tamilencryptedquestions,$arabicencryptedquestions,$idquestions)
    {
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Question'] = $engencryptedquestions;
    	$larrformData1['Malay'] = $malayencryptedquestions;	
    	$larrformData1['Tamil'] = $Tamilencryptedquestions;
    	$larrformData1['Arabic'] = $arabicencryptedquestions;			
		 $where = "idquestions = '".$idquestions."'"; 	
		 $db->update('tbl_questions',$larrformData1,$where);
    }
    
    public function fnupdateencrytptedanswers($engencryptedquestions,$malayencryptedquestions,$Tamilencryptedquestions,$arabicencryptedquestions,$idquestions)
    {
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['answers'] = $engencryptedquestions;
    	$larrformData1['Malay'] = $malayencryptedquestions;	
    	$larrformData1['Tamil'] = $Tamilencryptedquestions;
    	$larrformData1['Arabic'] = $arabicencryptedquestions;			
		 $where = "idanswers = '".$idquestions."'"; 	
		 $db->update('tbl_answers',$larrformData1,$where);
    }
       
   public function decrypt($sData, $sKey){ 
    $sResult = ''; 
    $sData   = self::decode_base64($sData); 
    for($i=0;$i<strlen($sData);$i++){ 
        $sChar    = substr($sData, $i, 1); 
        $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1); 
        $sChar    = chr(ord($sChar) - ord($sKeyChar)); 
        $sResult .= $sChar; 
    } 
    return $sResult; 
} 

function decode_base64($sData){ 
    $sBase64 = strtr($sData, '-_', '+/'); 
    return base64_decode($sBase64); 
}

public function fngetAllanswers()
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_answers"),array("a.*" ));					  	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
}

public function fnfindqtntype()
{
	 $db = Zend_Db_Table::getDefaultAdapter();
    	 $select = $db->select()	
					   ->from(array('a' => 'tbl_questions'),array('a.*'))
					   ->where("a.idquestions=1");
		  $result = $db->fetchRow($select);
		  return $result; 
}

//public fnconvertencrpttodecryptquestions($engdecryptedquestions,$malaydecryptedquestions,$Tamildecryptedquestions,$arabicdecryptedquestions,$idquestions)
}
