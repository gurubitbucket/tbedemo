<?php
     class Examination_Model_Examissuereports extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	

      	
    public function fnGetValues($strQry)
	{
		 $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$result = $db->fetchAll("$strQry");   
			return $result;
	}
	
     public function fngetsearchissuedetails($larrformData){
				if($larrformData['field5']){
					$venue = $larrformData['field5'];
				}
				if($larrformData['field10']){ 
					$edate = $larrformData['field10'];
				}

				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.FName as Studentname","a.IDApplication","a.Payment","a.pass","a.ICNO"))
					 	 	 ->join(array("d" => "tbl_studentissuedetails"),'a.IDApplication = d.IDApplication',array("d.Idissuedetails"))
						     ->join(array("m" => "tbl_issuelist"),'m.Idissue = d.Idissue',array("m.Issue"))
						     ->join(array("n" => "tbl_issuedetails"),'n.Idissuedetail=d.Idissuedetails',array("DATE_FORMAT(n.Examdate,'%d-%m-%Y') as ExamDate","n.Remark","DATE_FORMAT(date(n.UpdDate),'%d-%m-%Y') as issuedate"))
							 ->join(array("b" => "tbl_center"),'n.Examvenue = b.idcenter',array("b.idcenter","b.centername","b.ipaddress as Localserver"))
							 ->join(array("p" => "tbl_managesession"),'d.Idsession= p.idmangesession',array("p.managesessionname"))
							 ->join(array("q" => "tbl_programmaster"),'q.IdProgrammaster = a.Program',array("q.ProgramName"))
							 ->order("b.centername")
							 	 ->order("q.ProgramName")
							 	 	 ->order("p.managesessionname desc")
							 	 	 	 ->order("a.FName");
						
							 if(!empty($larrformData['Name']))
							 {
							 		$lstrSelect->where('a.FName like  "%" ? "%"',$larrformData['Name']);
			   							
							 }
							 if(!empty($larrformData['ICNO']))
							 {
							 	$lstrSelect->where('a.ICNO like  "%" ? "%"',$larrformData['ICNO']);
							 }
							 if(!empty($larrformData['Issues']))
							 {
							 	$lstrSelect->where("d.Idissue=?",$larrformData['Issues']);	
							 }
							 
							 
							 if(!empty($venue))
							 {

							 $lstrSelect->where("n.Examvenue=?",$venue);
							 }
	 if(!empty($edate))
							 {
							 $lstrSelect->where("n.Examdate=?",$edate);
							 }
							 
							 
							 //->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.idcenter","b.centername"))
							 //echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
	    public function fnGetAttedndedStudents($idcenter,$idprogram,$idsession,$exdate,$pass) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Payment = 1")
							  ->group("e.IDApplication")
							  ->order("a.FName");
							 
					if($pass == 3) {		 
						$lstrSelect ->where("a.pass != '$pass'");
					} elseif($pass == 1) {
						$lstrSelect ->where("a.pass = '$pass'");
					} elseif($pass == 2)	{
						$lstrSelect ->where("a.pass = '$pass'");
					} 
							
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }

      	public function fnGetRegistered($idcenter,$idprogram,$idsession,$exdate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Payment = 1")
							  ->group("e.IDApplication")
							  ->order("a.FName");
	
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }

	    
      	public function fnGetVenueList($examdate){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array(""))
							 	 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("key"=>"b.idcenter","value"=>"b.centername"))
				 				  ->where("a.DateTime = '$examdate'")
				 				 ->where("a.Payment = 1")
				 				 ->group("b.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		
		
			public function fngetallissuelist(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_issuelist"),array("key"=>"a.Idissue","value"=>"a.Issue"));
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		
		
      	public function fnGetCountOfStudents($idcenter,$idprogram,$idsession,$exdate,$pass) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Payment = 1")
							  ->order("a.FName");
/*					if($pass == 3) {		 
						$lstrSelect ->where("a.pass != '$pass'");
					} else*/
					if($pass == 1) {
						$lstrSelect ->where("a.pass = '$pass'");
					} elseif($pass == 2)	{
						$lstrSelect ->where("a.pass = '$pass'");
					} 
							
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
        public function fnGetCountOfStudentsAttended($idcenter,$idprogram,$idsession,$exdate,$pass) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication"))
							 ->join(array("b" => "tbl_studentstartexamdetails"),'a.IDApplication = b.IDApplication AND a.DateTime=DATE(b.UpdDate)',array(""))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 //->where("a.pass != '$pass'")
							 ->where("a.Payment = 1")
							 ->group("b.IDApplication")
							 ->order("a.FName");
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
      	    public function fnGetAttedndedStudentsDetails($idcenter,$idprogram,$idsession,$exdate,$pass){
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->join(array("f" => "tbl_studentstartexamdetails"),'a.IDApplication = f.IDApplication AND a.DateTime=DATE(f.UpdDate)',array(""))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Payment = 1")
							  ->group("e.IDApplication")
							  ->group("f.IDApplication")
							  ->order("a.FName");
							
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
              public function fnGetAbsent($idcenter,$idprogram,$idsession,$exdate) {
        	    $consistantresult = 'SELECT i.IDApplication  from tbl_studentapplication i,tbl_studentstartexamdetails k where i.Examvenue = "'.$idcenter.'" and i.Program = "'.$idprogram.'" and i.DateTime="'.$exdate.'" and i.Examsession = "'.$idsession.'"  and i.Payment=1 and i.DateTime=DATE(k.UpdDate) and i.IDApplication = k.IDApplication  group by k.IDApplication';

				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.IDApplication NOT IN ?",new Zend_Db_Expr('('.$consistantresult.')'))
 						     ->where("a.Payment = 1")
 						     ->group("e.IDApplication")
 						     ->order("a.FName");
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
      	   public function fnGetExtratime($idcenter,$iddession,$examdate) { 
	     	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()	
							->from(array('a' => 'tbl_gracetime'),array('a.Gracetime'))
                			->where('a.Idvenue = ?',$idcenter)
                			->where('a.Idsession = ?',$iddession)
                			->where('a.ExamDate = ?',$examdate);
			$result = $db->fetchRow($sql);
			return $result;
		}
		
		
        public function fnGetCountOfApprovedStudents($idcenter,$idprogram,$idsession,$exdate){
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("DISTINCT(a.IDApplication) as IDApplication"))
							 ->join(array("b" => "tbl_registereddetails"),'a.IDApplication = b.IDApplication',array(""))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("b.Cetreapproval = 1")
							 ->where("a.Payment = 1");
							// ->group("b.IDApplication");
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
      	    public function fnGetAttedndedStudentsApproved($idcenter,$idprogram,$idsession,$exdate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("DISTINCT(a.IDApplication) as IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Payment = 1")
							  ->where("e.Cetreapproval = 1")
							  //->group("e.IDApplication")
							  ->order("a.FName");
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
      public function fnGetAtt($idcenter,$idprogram,$idsession,$exdate){
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->join(array("f" => "tbl_studentstartexamdetails"),'a.IDApplication = f.IDApplication AND a.DateTime=DATE(f.UpdDate)',array(""))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Payment = 1")
							  ->group("e.IDApplication")
							  ->group("f.IDApplication")
							  ->order("a.FName");
							
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
				return $larrResult;
	    }
	    
	    public function fngetpullstatus($idven,$idprog,$idsess,$exdate)
	    {
	    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    	$lstrSelect = "Select if(VenueTime=1,'Pulled',if(VenueTime=2,'Pushed','Not Pulled')) as STATUS
	    	               From  tbl_studentapplication
	    	               Where DateTime = '$exdate' and Examvenue = $idven and Examsession = $idsess and 	Program = $idprog";
	    	//echo $lstrSelect;die();
	    	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }
		public function fninsertresultdetails($venue,$examdate,$user)
        {
echo $venue;
echo "<br/>";
echo $examdate;
echo "<br/>";

echo $user;
echo "<br/>";die();


                  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				      $result = self::fncheckifalreadyinserted($venue,$examdate);
if(empty($result)){

              // $internet=1;	      
		///try {
                        // $db = self::getAdaptor();
				//$internet=0;		
			//} 
//catch (Zend_Db_Adapter_Exception $e) {
		   // $internet=0;
		               //  }              
                   //if($internet == 1)
               //{		
                   $table = "tbl_resultsenddetails";
                   $postData = array(		
				'Date' =>$examdate,	
           			'Venue' =>$venue,
                                'Upduser'=>1,
            			'Upddate'=>date('Y-m-d:H:i:s'));          					
            	//echo "<pre>";
               // print_r($postData);die();
		//echo "<pre>";print_r($postData);
	       $lobjDbAdpt->insert($table,$postData);
		   }
//}
        }
	public	function fncheckifalreadyinserted($venue,$examdate)
{
	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_resultsenddetails"))
                                                                ->where("a.Date =?",$examdate)
									->where("a.Venue =?",$venue);
$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
       		return $larrResult;

}
private function getAdaptor(){			
				 $db = Zend_Db::factory('Pdo_Mysql', array(
				      'host'     => 'localhost',
													    'username' => 'pushpanath',
													    'password' => 'pushpanath',
													    'dbname'   => 'tbe01062013'));
								   				$db->getConnection();
		    
		
		
		return $db;
}
public function fngetreceivermail()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_config"));
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
	}
	public function fnchronepullstatus($examdate,$venue,$session)
	{
	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_venuedateschedule"))
									->join(array("c" => "tbl_chronepull"),'a.date = c.Examdate and c.Venue = a.idvenue and a.idsession = c.Session',array("c.*"))
									->join(array("b" => "tbl_autochronepull"),'b.idautochronepull = c.idautochronepull',array("b.*"))
									->where("a.date =?",$examdate)
									->where("a.idvenue =?",$venue)
									->where("a.idsession =?",$session)
									->where("c.Status in (0,2)");					        
       		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
       		return $larrResult;
	   
	}
	public function fnpullstatus($examdate,$venue)
	{
	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_venuedateschedule"))
									->join(array("c" => "tbl_chronepull"),'a.date = c.Examdate and c.Venue = a.idvenue',array("c.*"))
									->join(array("b" => "tbl_autochronepull"),'b.idautochronepull = c.idautochronepull',array("b.*"))
									->where("a.date =?",$examdate)
									->where("a.idvenue =?",$venue)									
									->where("c.Status in (0,2)");					        
       		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
       		return $larrResult;
	    
	}
	
	
	//updated for isuue screen 17/07/2013
	public function fnGetstudentexamdetails($student,$icno,$examdate,$venue)
	{
		$lintstatus="1,2,3,4";			
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()
								 ->from(array("tbl_studentapplication" => "tbl_studentapplication"),array("tbl_studentapplication.IDApplication","tbl_studentapplication.Examdate","tbl_studentapplication.ICNO","tbl_studentapplication.Exammonth","tbl_studentapplication.Year","tbl_studentapplication.FName","tbl_studentapplication.Program","tbl_studentapplication.DateTime","tbl_studentapplication.pass"))
								 ->join(array("tbl_registereddetails"=>"tbl_registereddetails"),'tbl_studentapplication.IDApplication = tbl_registereddetails.IDApplication', array("tbl_registereddetails.Regid","tbl_registereddetails.RegistrationPin"))								 
								 ->join(array("tbl_programmaster"=>"tbl_programmaster"),'tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster',array('tbl_programmaster.ProgramName','tbl_programmaster.IdProgrammaster'))
								 ->where("tbl_studentapplication.FName like '%".$student."%'")	
								 ->where("tbl_studentapplication.ICNO like '%".$icno."%'")								
								 ->where("tbl_studentapplication.pass in ($lintstatus)")
                                 ->where("tbl_studentapplication.Examvenue = '$venue'")							    
							     ->where("tbl_studentapplication.DateTime = '$examdate'") 
                                 ->group("tbl_studentapplication.IDApplication")								 
								;
			//echo $lstrSelect;exit;
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
	
	}
	public function fngetissuelist()
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_issuelist" ), array ("a.*") )
											 ->where( "a.Active=1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect );
		return $larrResult;
	}
	public function fnGetcandidatelookup($lintidapplication){		
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
							 	    ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("DATE_FORMAT(tbl_studentapplication.DateTime,'%d-%m-%Y') as examdate","tbl_studentapplication.EmailAddress as email"))								 				 
								    ->join(array("tbl_registereddetails"=>"tbl_registereddetails"),'tbl_studentapplication.IDApplication = tbl_registereddetails.IDApplication',array("tbl_studentapplication.FName","tbl_studentapplication.ICNO","tbl_studentapplication.IDApplication"))								    
							 	    ->where("tbl_studentapplication.IDApplication in (".$lintidapplication.")")					 											 				
							 	    ->order("tbl_studentapplication.FName");	
					 				
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
      	public function fnGetcandidatecurrentstatus($lintidapplication){		
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
							 	    ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("DATE_FORMAT(tbl_studentapplication.DateTime,'%d-%m-%Y') as examdate","tbl_studentapplication.EmailAddress as email","tbl_studentapplication.batchpayment"))								 				 
								    ->join(array("tbl_registereddetails"=>"tbl_registereddetails"),'tbl_studentapplication.IDApplication = tbl_registereddetails.IDApplication',array("tbl_studentapplication.FName","tbl_studentapplication.ICNO","tbl_studentapplication.IDApplication","tbl_registereddetails.Regid"))								    
								 ->join(array("b" => "tbl_center"),'tbl_studentapplication.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'tbl_studentapplication.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'tbl_studentapplication.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
						    ->where("tbl_studentapplication.IDApplication in (".$lintidapplication.")")					 											 				
							->group("tbl_studentapplication.IDApplication")
						    ->order("tbl_studentapplication.FName");	
					 				
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	public function fninsertissuedetails($formdata)
	{
	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  for($i=0;$i<count($formdata['mark']);$i++)
	  {
	       $idissue = $formdata['mark'][$i];
		   $table = "tbl_issuedetails";
                   $postData = array(		
				    'Examdate' =>$formdata['examdate'],	
           			'Examvenue' =>$formdata['examvenue'],
                    'Remark'=>$formdata['remark'][$idissue],
            		'UpdUser'=>1,
					'UpdDate'=>date('Y-m-d:H:i:s'),
					'Flag'=>1);            	
	       $lobjDbAdpt->insert($table,$postData);
		   $Idissuedetail = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_issuedetails','Idissuedetail');
		 for($j=0;$j<count($formdata['idapp'][$idissue]);$j++)
		 {
		         $session = self::fngetstudentsession($formdata['idapp'][$idissue][$j]);
				 if($formdata['idapp'][$idissue][$j]) 
                 {				 
		           $tablestudentissuedetails = "tbl_studentissuedetails";
                   $studentpostData = array(		
				    'Idissue' =>$idissue,	
           			'Idissuedetails' =>$Idissuedetail,
                    'IDApplication'=>$formdata['idapp'][$idissue][$j],
            		'Idsession'=>$session['Examsession']);					
		         $lobjDbAdpt->insert($tablestudentissuedetails,$studentpostData);
			}
		 }
	 
	}
	  } 
public function fngetstudentsession($idappln)
{
          $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
							 	    ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("tbl_studentapplication.Examsession"))
									->where("tbl_studentapplication.IDApplication=?",$idappln)								 				 
								    ;	
					 				
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
}
public function fncheckforissueupdates($examdate)
{
          $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
							 	    ->from(array("tbl_issuedetails"=>"tbl_issuedetails"),array("tbl_issuedetails.*"))
									->join(array("c" => "tbl_studentissuedetails"),'c.Idissuedetails = tbl_issuedetails.Idissuedetail',array("c.*"))
									->where("tbl_issuedetails.Examdate=?",$examdate) 
								    ;
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
}

public function fnGetcandidateremarkstatus($idissue)
{
	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
							 	    ->from(array("tbl_issuedetails"=>"tbl_issuedetails"),array("tbl_issuedetails.*"))
									->where("tbl_issuedetails.Idissuedetail=?",$idissue) ;
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
}

	  
 }    
		  	 
	
