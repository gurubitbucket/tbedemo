<?php
     class Examination_Model_Examsetmodel extends Zend_Db_Table_Abstract 
      {    

      	
      	
      	function fnGetExamDetails(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select ="SELECT b.IdBatch,t.IdTOS,b.BatchName,b.BatchFrom,b.BatchTo,t.NosOfQues,t.TimeLimit,t.AlertTime,t.Active,c.ProgramName
		  		FROM tbl_tosmaster AS t		  		
		  		INNER JOIN `tbl_batchmaster` AS `b` ON (t.IdBatch = b.IdBatch)
		  		INNER JOIN `tbl_programmaster` AS `c` ON (c.IdProgrammaster = b.IdProgrammaster) 	 		  		
		  		WHERE b.BatchStatus = 0  and t.Active=1";     		  	   
		  return	$result = $lobjDbAdpt->fetchAll($select);	
	  }
	  
      	function fnGetExamDetailsEdit($idTOSMaster){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select ="SELECT b.IdBatch,t.IdTOS,b.BatchName,b.BatchFrom,b.BatchTo,t.NosOfQues,t.TimeLimit,t.AlertTime,t.Pass,t.Active,t.Setcode
		  		FROM tbl_tosmaster AS t		  		
		  		INNER JOIN `tbl_batchmaster` AS `b` ON (t.IdBatch = b.IdBatch) 		  		
		  		WHERE b.BatchStatus = 0 AND t.IdTOS = ".$idTOSMaster; 		  	   
		  return	$result = $lobjDbAdpt->fetchAll($select);	
	}
	
	
      	function fnGetTOSFullDetails($idTOSMaster){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select ="SELECT bd.*,b.BatchFrom,b.BatchTo,q.QuestionNumber,q.idquestions, IF(td.NosOfQuestion,td.NosOfQuestion,0) tdNosOfQuestion ,
		 				 IF(tsd.NoofQuestions,tsd.NoofQuestions,0) NoofQuestions,tsd.IdDiffcultLevel
						FROM tbl_questions AS q 
						INNER JOIN `tbl_batchdetail` AS `bd` ON q.QuestionGroup=bd.IdPart 
						INNER JOIN `tbl_batchmaster` AS `b` ON (bd.IdBatch = b.IdBatch) 
						INNER JOIN tbl_tosmaster AS `t` ON (t.IdBatch = b.IdBatch AND t.IdTOS = ".$idTOSMaster.")		 
						LEFT JOIN tbl_tosdetail AS `td` ON (td.IdTOS = t.IdTOS AND td.IdSection = q.QuestionNumber) 
						LEFT JOIN tbl_tossubdetail AS `tsd` ON (tsd.IdTOSDetail = td.IdTOSDetail )	 
						WHERE b.BatchStatus = 0 AND bd.BatchDtlStatus = 0 
						GROUP BY q.QuestionNumber,q.QuestionGroup ,tsd.IdDiffcultLevel
						ORDER BY tsd.IdDiffcultLevel  ASC";
		// echo $select;die();
		 return	 $result = $lobjDbAdpt->fetchAll($select);	
	}
	
	public function fngettotalparts($idbatch)
	{
			
	 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_batchdetail'),array("c.IdPart"))
			->where("c.IdBatch=$idbatch");
			//->group("c.IdBatch");
			
		  $result = $db->fetchAll($select);		
		return $result;    

		
		
	}
	
	
	
	
      	public function fngetchaptereachpartpool($idpart)
	{
			 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionNumber","c.QuestionGroup as part"))
			->where("c.QuestionGroup = ?",$idpart)
			->group("c.QuestionGroup")
			->group("c.QuestionNumber")
			->order("c.QuestionGroup")
			->order("c.idquestions");
		//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
		
	}
	
	public function fngetquestionstosdetail($idpart,$idtos)
	{
			 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_tosdetail'),array("sum(c.NosOfQuestion) as totals"))
			->where("c.IdTOS = ?",$idtos)
		->where("c.IdSection like  ? '%'",$idpart);	
		//echo $select;die();
		  $result = $db->fetchRow($select);		
		return $result;
	}
	
      	public  function fnGetCourse($idTOSMaster)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_batchmaster"),array("a.IdProgrammaster","a.BatchName") )
       								->join(array("b"=>"tbl_programmaster"),'b.IdProgrammaster=a.IdProgrammaster' ,array("b.ProgramName") )
       								->join(array("c"=>"tbl_tosmaster"),'a.IdBatch=c.IdBatch')
					 				->where("c.IdTOS=?",$idTOSMaster);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
	
	
	
      	
	public function fngettotalpartqusetions($idparts){
		$db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionGroup"))
			->where("c.QuestionGroup in ($idparts)")
			->where("c.Active=1")
			->group("c.QuestionGroup")
			->order("c.QuestionGroup")
			->order("c.idquestions");
			//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	
	
	
	
	
	public function fngetchapterfrompool($idparts)
	{
			 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionNumber","c.QuestionGroup as part"))
			->where("c.QuestionGroup in ($idparts)")
			->where("c.Active=1")
			->group("c.QuestionGroup")
			->group("c.QuestionNumber")
			->order("c.QuestionGroup")
			->order("c.idquestions");
			//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
		
	}
	public function fngetchapterlevel($idparts)
	{
		
			 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cntlevel","c.QuestionLevel","c.QuestionNumber"))
			->where("c.QuestionGroup in ($idparts)")
			->where("c.Active=1")
			->group("c.QuestionGroup")
			->group("c.QuestionNumber")
			->group("c.QuestionLevel")
			->order("c.QuestionGroup")
			->order("c.idquestions");
			//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	
	
	
	public function fngetinactivechapters($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount","c.Active"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.Active=0");
			//->group("c.Active");
			//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
	}
      public function fngetreviewchapters($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount","c.Active"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.Active=2");
			//->group("c.Active");
			//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	
      public function fngetinactiveeasy($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=1")
			->where("c.Active=0");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	
      public function fngetrevieweeasy($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=1")
			->where("c.Active=2");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
       public function fngetinactivemed($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=2")
			->where("c.Active=0");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
      public function fngetreviewemed($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=2")
			->where("c.Active=2");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
      public function fngetinactivedif($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=3")
			->where("c.Active=0");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
       public function fngetreviewedif($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=3")
			->where("c.Active=2");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	
      public function fngettotalinactivequsetions($idparts){
     	
			$db = Zend_Db_Table::getDefaultAdapter();
			$km=array();
			for($i=0;$i<count($idparts);$i++){
				$select = $db->select()	
					->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionGroup","c.Active"))
					->where("c.QuestionGroup =?",$idparts[$i]['IdPart'])
					->where("c.Active=0");
		  		$result = $db->fetchAll($select);
		  		$result =	array_merge($km,$result);
		  		$km=$result;
		} 
		 //echo "<pre/>";print_r($result);
		return $result;
		
	}
	
	
      public function fngettotalreviewqusetions($idparts){
      	
		$db = Zend_Db_Table::getDefaultAdapter();
		$km=array();
		for($i=0;$i<count($idparts);$i++){
			$select = $db->select()	
				->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionGroup","c.Active"))
				->where("c.QuestionGroup =?",$idparts[$i]['IdPart'])
				->where("c.Active=2");
		  	$result = $db->fetchAll($select);
		  	$result =	array_merge($km,$result);
		  	$km=$result;
		  
		   
		}
		return $result; 
		
	}
	
	
 }    
		  	 
	
