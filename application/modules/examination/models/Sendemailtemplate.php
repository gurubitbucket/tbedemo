<?php

class Examination_Model_Sendemailtemplate extends Zend_Dojo_Form
{		
 protected $_name = 'tbl_emailtemplate';
    private $lobjDbAdpt;
    
	public function init()
	{
	}
    
     public function fngetemailtemplates() { //Function to get the user details
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select() 	
			   ->from(array('a' => 'tbl_emailtemplate'),array("key"=>"a.idTemplate","value"=>"a.TemplateName"))
			   ->order("a.TemplateName");
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
        return $result;
     }
   
}
        
        