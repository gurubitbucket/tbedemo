<?php
	class Examination_Model_Studentattempts extends Zend_Db_Table {


      public function fngetprog()
     {
     			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("b" => "tbl_programmaster"),array("key"=>"b.IdProgrammaster","value"=>"b.ProgramName"));
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
	 	/*public function fncountattemptforstudents($prog,$fromdate,$todate)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		  $select = "SELECT IDApplication,FName,ICNO,pass,DateTime,Program 	
					    FROM  `tbl_studentapplication`
						WHERE Program =$prog
						AND DateTime >='$fromdate'
						AND DateTime <='$todate'
					    AND payment=1 and pass in(1,2,3,4)
						";
 		    return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		*/
		
		public function fnupdatestudentapplication($Idappliaction,$appearcount)
		{
			
				
				
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$table = "tbl_studentapplication"; 
			 $where = "IDApplication = '".$Idappliaction."'"; 	  
						   $data = array(		
							'PermCity' =>$appearcount
						);
	        $lobjDbAdpt->update($table,$data,$where);
		
		
		}
		public function fncountattemptforstudents($fromdate,$todate)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		/*  $select = "SELECT IDApplication,FName,ICNO,pass,DateTime,Program 	
					    FROM  `tbl_studentapplication`						
						WHERE DateTime >='$fromdate'
						AND DateTime <='$todate'
					    AND payment=1 and pass in(1,2,3,4)
						";
						*/
						
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("b" => "tbl_studentapplication"),array("DATE_FORMAT(b.DateTime,'%d-%m-%Y') AS examdate","b.DateTime","b.pass","b.ICNO","b.FName","b.IDApplication"))
										   ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster=b.Program',array("c.ProgramName"))
										    ->join(array('d'=>'tbl_center'),'d.idcenter = b.Examvenue',array("d.centername"))
											 ->join(array('e'=>'tbl_managesession'),'e.idmangesession  = b.Examsession',array("e.managesessionname"))
										     ->where("b.DateTime >= ?",$fromdate)
										     ->where("b.DateTime <= ?",$todate)
										     	->where("b.Permcity=0")
										     	 ->where("b.DateTime >= '2012-04-12'")
											->where("b.payment =?",1)
											->where("b.idapplication >1148")
											->where("b.pass in(1,2,3,4)")
											->order("b.DateTime")
											->order("b.ICNO")	
											->order("c.ProgramName")
											->order("d.centername")
											->order("e.managesessionname")
										;
										    //->where("b.Program =?",$program);
					
 		    return	$result = $lobjDbAdpt->fetchAll($lstrSelect);

			
		}
		 public function fngetallattemptsforicno($icno,$examdate)
		 {
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("b" => "tbl_studentapplication"),array("b.*","DATE_FORMAT(b.DateTime,'%d-%m-%Y') AS DateTime"))
										   ->join(array("f" => "tbl_registereddetails"),'f.IDApplication=b.IDApplication',array("f.Regid"))
										   ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster=b.Program',array("c.ProgramName"))
										    ->join(array('d'=>'tbl_center'),'d.idcenter = b.Examvenue',array("d.centername"))
											 ->join(array('e'=>'tbl_managesession'),'e.idmangesession  = b.Examsession',array("e.managesessionname"))
										     ->where("b.ICNO=?",$icno)
										     ->where("b.DateTime < ?",$examdate)
											->where("b.payment =?",1)
											->order("b.DateTime")
											->order("c.ProgramName")
											->order("d.centername")
											->order("e.managesessionname")
											->group("b.IDApplication");
										    //->where("b.Program =?",$program);
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		 }

     public function fngetprogdetails($prog,$date)
     {
     	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	
		  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_programmaster"),array("a.*"))
							 			 ->join(array("b" => "tbl_batchmaster"),'a.IdProgrammaster=b.IdProgrammaster',array("b.*"))
							 			 ->join(array("t" => "tbl_tosmaster"),'b.IdBatch=t.IdBatch',array("t.*"))
							 			 ->join(array("c" => "tbl_batchdetail"),'b.IdBatch=c.IdBatch',array("c.*"))	
							 			 ->join(array("d" => "tbl_questions"),'c.IdPart=d.QuestionGroup',array("d.*"))							 			 							 			 
                                         ->where("t.Active=1")
                                        // ->where("d.Active=1")
                                         ->where("a.IdProgrammaster=?",$prog)
                                         //->where("d.Active=1")
                                         ->group("d.idquestions");
                                         
      if($date!=0)
     	 {
     	 	$lstrSelect->where("d.QuestionLevel = ?",$date);
     	 }
     	 				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
	 
     
    
		public function fncountattemptbyicno($icno,$date)
		{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		  $select = "SELECT count(IDApplication) as total
					    FROM  `tbl_studentapplication`
						WHERE  DateTime < '$date'
						and ICNO='$icno'
						and examvenue!='000'
							and examsession!=0 and IDApplication>1148
					    AND payment=1 and   DateTime >='2012-04-12' and pass in(1,2,3,4)
						";
 		  //echo $select;die();
 		    return	$result = $lobjDbAdpt->fetchRow($select);	
                                      
		}
     
	 public function fnGetQtnsapperedinexam($Questionid,$maxids){

 		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 
 		 $select = "
 		 SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '$Questionid,%'
Union 
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid' 
Union
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid,%'
 		 
 		 ";

		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		

		
		
	
			
		public function fngetregids($studentid)
		{
             $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_registereddetails"),array("a.Regid"))
                                         ->where("a.Idapplication in ($studentid)");
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);	
 		 	
		}
		
		public function fnGettotalattended($Questionid,$regid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo"))
                                         ->where("a.Regid in ($regid)")
                                          ->where("a.QuestionNo=?",$Questionid)
 ->group("a.Regid")
                                          ->group("a.QuestionNo");
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		
		public function fnGettotalCorrectattended($Questionid,$regid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo"))
							 			 ->join(array("b"=>"tbl_answers"),'b.idanswers=a.Answer',array())
                                         ->where("a.Regid in ($regid)")
                                          ->where("a.QuestionNo=?",$Questionid)
                                          ->where("b.CorrectAnswer=1");
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		public function fngetquestionanswerdetails($qtnid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_questions"),array("a.Question"))
							 			  ->join(array("b"=>"tbl_answers"),'b.idquestion=a.idquestions',array("b.answers","b.CorrectAnswer"))
                                          ->where("a.idquestions=?",$qtnid);
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}

   
	}
