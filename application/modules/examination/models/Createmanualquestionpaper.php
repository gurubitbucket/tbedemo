<?php
     class Examination_Model_Createmanualquestionpaper extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	
	        protected $_name = 'tbl_questiongroup';
	
        
        public function fngetquestionsetlist(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array("a.*"))
										  ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster=a.Program',array("c.*"))
										  ->order("a.UpdDate desc")
										  ->where("a.idmanual=?",1);
										  
			       
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		}
		
       public function fngetquestionsetlistsearch($larrformData){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array("a.PaperCode","a.UpdDate","a.UpdUser","a.idquestiongroup"))
										  ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster=a.Program',array("c.ProgramName"))
										  ->join(array("b" => "tbl_user"),'b.iduser=a.UpdUser',array("b.loginName"))
										  ->order("a.UpdDate desc")
										  ->where("a.idmanual=?",'1')
										  ->where('a.PaperCode like "%" ? "%"',$larrformData['field28']);
			       if($larrformData['field1']){	
						$lstrSelect->where("a.Program = ?",$larrformData['field1']);
					}
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		}
	  
            
            
      public function fnGetProgramName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										  ->join(array("c" => "tbl_program"),'c.IdProgram=a.idprog')
										  ->join(array("b"=>"tbl_batchmaster"),'b.IdProgrammaster=a.IdProgrammaster',array())
										  ->join(array("d"=>"tbl_tosmaster"),'b.IdBatch=d.IdBatch',array())
  										->join(array("e"=>"tbl_programrate"),'a.IdProgrammaster=e.idProgram',array())
										  ->where("e.Active=1")
										  ->where("d.Active=1")
										  ->where("c.Active =1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
            //Function for searching the author details	      
	public function fninserquestioncodes($larrformData,$idIdBatch,$iduser,$ldtsystemDate){
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   
		 	 $larrcreditnotedetails['Program'] = $larrformData['field1'];
		 	 $larrcreditnotedetails['Activeset'] = $idIdBatch;
		 	  $larrcreditnotedetails['PaperCode'] = $larrformData['field28'];
		 	 $larrcreditnotedetails['UpdDate'] = $ldtsystemDate;
		 	 $larrcreditnotedetails['UpdUser'] = $iduser;
		 	  $result = $lobjDbAdpt->insert('tbl_manualquestiongroup',$larrcreditnotedetails);
		 	  $lastid  = $lobjDbAdpt->lastInsertId("tbl_manualquestiongroup","idquestiongroup");
		 	  //echo 	$lastid;die();
   	 return $lastid;
   }
   
	public function fninserquestioncodesMain($larrformData,$idIdBatch,$iduser,$ldtsystemDate){
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   			$larrcreditnotedetails['Program'] = $larrformData['field1'];
		 	$larrcreditnotedetails['Activeset'] = $idIdBatch;
		 	$larrcreditnotedetails['PaperCode'] = $larrformData['field28'];
		 	$larrcreditnotedetails['UpdDate'] = $ldtsystemDate;
		 	$larrcreditnotedetails['UpdUser'] = $iduser;
		 	$larrcreditnotedetails['idmanual'] = 1;
		 	$result = $lobjDbAdpt->insert('tbl_questiongroup',$larrcreditnotedetails);
		 	$lastid  = $lobjDbAdpt->lastInsertId("tbl_questiongroup","idquestiongroup");
		 	  //echo 	$lastid;die();
   	 return $lastid;
   }
   
  public function fninserquestionsforabovecode($larrresult,$larrquestionid)
   {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  
   	  for($linti=0;$linti<count($larrquestionid);$linti++)
		 {
		 	 $larrcreditnotedetails['idquestiongroup'] = $larrresult;
		 	 $larrcreditnotedetails['idquestions'] = $larrquestionid[$linti];
		 	  $result = $lobjDbAdpt->insert('tbl_questiongroupdetails',$larrcreditnotedetails);
		 	 
		 }
   	 return $result;
   }
   
   
        public function fndisplayquestionsforabovecode($RegID){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				/*$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroupdetails"),array("")) 
										  ->join(array("c" => "tbl_questions"),'c.idquestions=a.idquestions',array('c.idquestions','c.Question'))
										  ->where("a.idquestiongroup=?",$RegID);
				$select2 ="SELECT c.idquestions,c.Question FROM tbl_questions as c ,tbl_questiongroupdetails as a
					WHERE c.idquestions=a.idquestions and a.idquestiongroup=$RegID
					ORDER BY SUBSTR(c.QuestionNumber FROM 1 FOR 1), CAST(SUBSTR(c.QuestionNumber FROM 2) AS UNSIGNED)DESC";	*/	

				$select2 ="SELECT c.idquestions,c.Question FROM tbl_questiongroupdetails as a ,tbl_questions as c
					WHERE c.idquestions=a.idquestions and a.idquestiongroup=$RegID
					ORDER BY SUBSTR(c.QuestionNumber FROM 1 FOR 1), CAST(SUBSTR(c.QuestionNumber FROM 2) AS UNSIGNED)DESC";
										  
				$larrResult = $lobjDbAdpt->fetchAll($select2);
				return $larrResult;
	}
	
      
	
      public function fndisplayanswersforquestions($ID){
      	      
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_answers"),array("a.answers")) 
										  ->join(array("c" => "tbl_questions"),'c.idquestions=a.idquestion',array())
										  ->where("a.idquestion=?",$ID)
										  ->order("a.idanswers");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
      public function fngetprgname($ID){
      	      
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array("a.PaperCode","DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as creationdate")) 
										  ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster = a.Program',array('c.ProgramName'))
										  ->where("a.idquestiongroup = ?",$ID);
										  //->order("a.idanswers");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	   }
	   
       public function fngetprgnamedraft($ID){
      	      
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_manualquestiongroup"),array("a.PaperCode","DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as creationdate")) 
										  ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster = a.Program',array('c.ProgramName'))
										  ->where("a.idquestiongroup = ?",$ID);
										  //->order("a.idanswers");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	   }
	   
      public function fngetprgnamedraftindex($ID){
      	      
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array("a.PaperCode","DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as creationdate")) 
										  ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster = a.Program',array('c.ProgramName'))
										  ->where("a.idquestiongroup = ?",$ID);
										  //->order("a.idanswers");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	   }
	   
	
      public function fngettosdesc($ID){
      	        
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array()) 
										  ->join(array("d" => "tbl_tosmaster"),'d.IdBatch = a.Activeset',array())
										  ->join(array("e" => "tbl_batchmaster"),'e.IdBatch = d.IdBatch',array('e.BatchName as Setcode'))
										  ->where("a.idquestiongroup = ?",$ID)
										  ->where("d.Active = 1");
										   
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
     public function fngettosdescdraft($ID){
      	        
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_manualquestiongroup"),array()) 
										  ->join(array("d" => "tbl_tosmaster"),'d.IdBatch = a.Activeset',array())
										  ->join(array("e" => "tbl_batchmaster"),'e.IdBatch = d.IdBatch',array('e.BatchName as Setcode'))
										  ->where("a.idquestiongroup = ?",$ID)
										  ->where("d.Active = 1");
										   
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
     public function fngettosdescdraftindex($ID){
      	        
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array()) 
										  ->join(array("d" => "tbl_tosmaster"),'d.IdBatch = a.Activeset',array('d.IdTOS'))
										  ->join(array("e" => "tbl_batchmaster"),'e.IdBatch = d.IdBatch',array('e.BatchName as Setcode'))
										  ->where("a.idquestiongroup = ?",$ID)
										  ->where("d.Active = 1");
										   
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	
	///////////////////////exam set model
	
	function fnGetExamDetails(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select ="SELECT b.IdBatch,t.IdTOS,b.BatchName,b.BatchFrom,b.BatchTo,t.NosOfQues,t.TimeLimit,t.AlertTime,t.Active,c.ProgramName
		  		FROM tbl_tosmaster AS t		  		
		  		INNER JOIN `tbl_batchmaster` AS `b` ON (t.IdBatch = b.IdBatch)
		  		INNER JOIN `tbl_programmaster` AS `c` ON (c.IdProgrammaster = b.IdProgrammaster) 	 		  		
		  		WHERE b.BatchStatus = 0  and t.Active=1";     		  	   
		  return	$result = $lobjDbAdpt->fetchAll($select);	
	  }
	  
      	function fnGetExamDetailsEdit($idTOSMaster){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select ="SELECT b.IdBatch,t.IdTOS,b.BatchName,b.BatchFrom,b.BatchTo,t.NosOfQues,t.TimeLimit,t.AlertTime,t.Pass,t.Active,t.Setcode
		  		FROM tbl_tosmaster AS t		  		
		  		INNER JOIN `tbl_batchmaster` AS `b` ON (t.IdBatch = b.IdBatch) 		  		
		  		WHERE b.BatchStatus = 0 AND t.IdTOS = ".$idTOSMaster; 		  	   
		  return	$result = $lobjDbAdpt->fetchAll($select);	
	}
	
	
      	function fnGetTOSFullDetails($idTOSMaster){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select ="SELECT bd.*,b.BatchFrom,b.BatchTo,q.QuestionNumber,q.idquestions, IF(td.NosOfQuestion,td.NosOfQuestion,0) tdNosOfQuestion ,
		 				 IF(tsd.NoofQuestions,tsd.NoofQuestions,0) NoofQuestions,tsd.IdDiffcultLevel
						FROM tbl_questions AS q 
						INNER JOIN `tbl_batchdetail` AS `bd` ON q.QuestionGroup=bd.IdPart 
						INNER JOIN `tbl_batchmaster` AS `b` ON (bd.IdBatch = b.IdBatch) 
						INNER JOIN tbl_tosmaster AS `t` ON (t.IdBatch = b.IdBatch AND t.IdTOS = ".$idTOSMaster.")		 
						LEFT JOIN tbl_tosdetail AS `td` ON (td.IdTOS = t.IdTOS AND td.IdSection = q.QuestionNumber) 
						LEFT JOIN tbl_tossubdetail AS `tsd` ON (tsd.IdTOSDetail = td.IdTOSDetail )	 
						WHERE b.BatchStatus = 0 AND bd.BatchDtlStatus = 0 
						GROUP BY q.QuestionNumber,q.QuestionGroup ,tsd.IdDiffcultLevel
						ORDER BY tsd.IdDiffcultLevel  ASC";
		// echo $select;die();
		 return	 $result = $lobjDbAdpt->fetchAll($select);	
	}
	
	public function fngettotalparts($idbatch)
	{
			
	 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_batchdetail'),array("c.IdPart"))
			->where("c.IdBatch=$idbatch")
			->order("c.IdPart");
			//->group("c.IdBatch");
			
		  $result = $db->fetchAll($select);		
		return $result;    

		
		
	}
	
	
	
	
      	public function fngetchaptereachpartpool($idpart)
	{
			 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionNumber","c.QuestionGroup as part"))
			->where("c.QuestionGroup = ?",$idpart)
			->group("c.QuestionGroup")
			->group("c.QuestionNumber")
			->order("c.QuestionGroup")
			->order("c.idquestions");
		//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
		
	}
	
	public function fngetquestionstosdetail($idpart,$idtos)
	{
			 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_tosdetail'),array("sum(c.NosOfQuestion) as totals"))
			->where("c.IdTOS = ?",$idtos)
		->where("c.IdSection like  ? '%'",$idpart);	
		//echo $select;die();
		  $result = $db->fetchRow($select);		
		return $result;
	}
	
      	public  function fnGetCourse($idTOSMaster)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_batchmaster"),array("a.IdProgrammaster","a.BatchName") )
       								->join(array("b"=>"tbl_programmaster"),'b.IdProgrammaster=a.IdProgrammaster' ,array("b.ProgramName") )
       								->join(array("c"=>"tbl_tosmaster"),'a.IdBatch=c.IdBatch')
					 				->where("c.IdTOS=?",$idTOSMaster);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
	
	
	
      	
	public function fngettotalpartqusetions($idparts){
		$db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionGroup"))
			->where("c.QuestionGroup in ($idparts)")
			->where("c.Active=1")
			->group("c.QuestionGroup")
			->order("c.QuestionGroup")
			->order("c.idquestions");
			//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	
	
	
	
	
	public function fngetchapterfrompool($idparts)
	{
			 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionNumber","c.QuestionGroup as part"))
			->where("c.QuestionGroup in ($idparts)")
			->where("c.Active=1")
			->group("c.QuestionGroup")
			->group("c.QuestionNumber")
			->order("c.idquestions")
			->order("c.QuestionNumber");
			
			$select1 ="SELECT count(c.idquestions) as cnt,c.QuestionNumber,c.QuestionGroup as part FROM tbl_questions AS c
						WHERE c.QuestionGroup in ($idparts) and c.Active=1 
						GROUP BY c.QuestionNumber
						ORDER BY LENGTH(c.QuestionNumber), c.QuestionNumber";
			
			
			$select2 ="SELECT count(idquestions) as cnt,QuestionNumber ,QuestionGroup as part FROM tbl_questions 
			WHERE QuestionGroup in ($idparts) and Active=1 
			group by QuestionNumber
			ORDER BY SUBSTR(QuestionNumber FROM 1 FOR 1), CAST(SUBSTR(QuestionNumber FROM 2) AS UNSIGNED)DESC";
			
			$result = $db->fetchAll($select2);		
		return $result; 
		
		
	}
	public function fngetchapterlevel($idparts)
	{
		
			 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cntlevel","c.QuestionLevel","c.QuestionNumber"))
			->where("c.QuestionGroup in ($idparts)")
			->where("c.Active=1")
			->group("c.QuestionGroup")
			->group("c.QuestionNumber")
			->group("c.QuestionLevel")
			->order("c.QuestionGroup")
			->order("c.idquestions");
			//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	
	
	
	public function fngetinactivechapters($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount","c.Active"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.Active=0");
			//->group("c.Active");
			//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
	}
      public function fngetreviewchapters($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount","c.Active"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.Active=2");
			//->group("c.Active");
			//echo $select;die();
		  $result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	
      public function fngetinactiveeasy($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=1")
			->where("c.Active=0");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	
      public function fngetrevieweeasy($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=1")
			->where("c.Active=2");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
       public function fngetinactivemed($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=2")
			->where("c.Active=0");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
      public function fngetreviewemed($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=2")
			->where("c.Active=2");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
      public function fngetinactivedif($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=3")
			->where("c.Active=0");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
       public function fngetreviewedif($idch){
			$db = Zend_Db_Table::getDefaultAdapter();
		  	$select = $db->select()	
			->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as chcount"))
			->where("c.QuestionNumber =?",$idch)
			->where("c.QuestionLevel=3")
			->where("c.Active=2");
			$result = $db->fetchAll($select);		
		return $result; 
		
	}
	
	public function fngettotalinactivequsetions($idparts){
     	
			$db = Zend_Db_Table::getDefaultAdapter();
			$km=array();
			for($i=0;$i<count($idparts);$i++){
				$select = $db->select()	
					->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionGroup","c.Active"))
					->where("c.QuestionGroup =?",$idparts[$i]['IdPart'])
					->where("c.Active=0");
		  		$result = $db->fetchAll($select);
		  		$result =	array_merge($km,$result);
		  		$km=$result;
		} 
		 //echo "<pre/>";print_r($result);
		return $result;
		
	}
	
	
      public function fngettotalreviewqusetions($idparts){
      	
		$db = Zend_Db_Table::getDefaultAdapter();
		$km=array();
		for($i=0;$i<count($idparts);$i++){
			$select = $db->select()	
				->from(array('c' => 'tbl_questions'),array("count(c.idquestions) as cnt","c.QuestionGroup","c.Active"))
				->where("c.QuestionGroup =?",$idparts[$i]['IdPart'])
				->where("c.Active=2");
		  	$result = $db->fetchAll($select);
		  	$result =	array_merge($km,$result);
		  	$km=$result;
		  
		   
		}
		return $result; 
	}    
	
	
	/////////////////////////////manual create questions
	
	 public function fngetactivetosquestions($chapter,$level){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					->from(array("a" => "tbl_questions"),array("a.idquestions","a.Question","a.Active","a.QuestionNumber","a.QuestionLevel"))
					//->join(array("b" => "tbl_answers"),'b.idquestion=a.idquestions',array("b.answers","b.CorrectAnswer"))
					->where("a.QuestionNumber =?",$chapter)
					->where("a.QuestionLevel =?",$level)
					//->group("a.idquestions")
					->where("a.Active=1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	 
	 public function fngetselectedquestions($chapter,$level,$idTOSMaster){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
	 	$lstrSelect = $lobjDbAdpt->select()
					->from(array("a" => "tbl_manualquestiongroupdetails"),array("a.idtos","a.chapter","a.level","a.idquestions"))
					->where("a.level =?",$level)
					->where("a.chapter =?",$chapter)
					->where("a.idtos =?",$idTOSMaster)
					->where('idquestiongroup =?',$_SESSION['lastinsertid']);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	 
	public function fnupdatemanualquestions($saveque,$idtos,$chapter,$level){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       	$where = array(
   			'idtos = ?' => $idtos,
   			'chapter = ?' => $chapter,
       		'level = ?' => $level,
       		'idquestiongroup =?' => $_SESSION['lastinsertid']
		);
       	$postData = array(		
			'idquestions' => $saveque													
		);
		$table = "tbl_manualquestiongroupdetails";
		$result =$lobjDbAdpt->update($table,$postData,$where);
       	
       	/*$lobjDbAdpt->delete('tbl_manualquestiongroupdetails', array(
    		'idtos = ?' => $idtos,
   			'chapter = ?' => $chapter,
       		'level = ?' => $level
		));
       	$manual['idtos'] = $idtos;
	    $manual['chapter'] = $chapter;
		$manual['level'] = $level;
	 	$manual['idquestions'] = $saveque;
	 	$result = $lobjDbAdpt->insert('tbl_manualquestiongroupdetails',$manual);*/
	  	
		return $result;
       	
	 }
	 
	 public function fnGetidtosonidprogram($idprogram){
	 			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array(""))
										  ->join(array("c" => "tbl_program"),'c.IdProgram=a.idprog',array(""))
										  ->join(array("b"=>"tbl_batchmaster"),'b.IdProgrammaster=a.IdProgrammaster',array(""))
										  ->join(array("d"=>"tbl_tosmaster"),'b.IdBatch=d.IdBatch',array("d.IdTOS"))
  										 ->join(array("e"=>"tbl_programrate"),'a.IdProgrammaster=e.idProgram',array(""))
										  ->where("e.Active=1")
										  ->where("d.Active=1")
										  ->where("c.Active =1")
										  ->where("a.IdProgrammaster =?",$idprogram);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	 	
	 }
	 
	 public function fngetmanualquestioncodes($larrsection,$larrformData1,$lvl){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_manualquestiongroupdetails"),array("a.idquestions","a.level","a.chapter","a.idtos"))
										  ->where("a.chapter =?",$larrsection)
										  ->where("a.level =?",$lvl)
										  ->where("a.idtos =?",$larrformData1['idtos'])
										  ->where("a.idquestiongroup=?",$_SESSION['lastinsertid']);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	 	
	 }
	
       public function fngetprogramoftos($tos){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("a.IdProgrammaster"))
										  ->join(array("b"=>"tbl_batchmaster"),'b.IdProgrammaster=a.IdProgrammaster',array(""))
										  ->join(array("d"=>"tbl_tosmaster"),'b.IdBatch=d.IdBatch',array(""))
  										 ->where("d.Active=1")
										  ->where("d.IdTOS =?",$tos);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	 	
	 }
	 
	 public function fninserttosrandomly($chapter,$level,$tosquestions,$idtos){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					->from(array("a" => "tbl_questions"),array("a.idquestions"))
					->where("a.QuestionNumber =?",$chapter)
					->where("a.QuestionLevel =?",$level)
					->where("a.Active=1")
					->order("rand()");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		
		$larrResult=array_slice($larrResult,0,$tosquestions);
		
		$values="";
		foreach($larrResult as $larrResult1){
			$value = $larrResult1['idquestions'];
			$values.=','.$value;
		}
		$randtosinit=trim($values,',');
		/////////////////delete for same tos///////
		 $lobjDbAdpt->delete('tbl_manualquestiongroupdetails', array(
    		'idtos = ?' => $idtos,
   			'chapter = ?' => $chapter,
       		'level = ?' => $level,
			'idquestiongroup = ?' => $_SESSION['lastinsertid']
		));
		
		/////////////////insert initially/////////////
		if($randtosinit){
			$manual['idtos'] = $idtos;
	       	$manual['chapter'] = $chapter;
	       	$manual['level'] = $level;
	       	$manual['idquestions'] = $randtosinit;
	       	$manual['idquestiongroup'] =$_SESSION['lastinsertid'];
		    $result = $lobjDbAdpt->insert('tbl_manualquestiongroupdetails',$manual);
		}
		
	    return 1;
		
	 	
	 }
	 
	 public function fngetrandomtosquest($chapter,$level,$tosquestions,$idtos){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_manualquestiongroupdetails"),array("a.idquestions"))
								  ->where("a.chapter =?",$chapter)
								  ->where("a.level =?",$level)
								  ->where("a.idtos =?",$idtos)
								  ->where("a.idquestiongroup=?",$_SESSION['lastinsertid']);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	 }
	 
	 public function fngetallidquestions($RegID){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_manualquestiongroupdetails"),array("a.idquestions")) 
										->where("a.idquestiongroup=?",$RegID);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
      public function fngetallidquestionsindex($RegID){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				/*$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroupdetails"),array("a.idquestions")) 
										  ->join(array("b"=>"tbl_questions"),'b.idquestions=a.idquestions',array(""))
										->where("a.idquestiongroup=?",$RegID)
										->order("b.QuestionNumber");*/
				$select2 ="SELECT a.idquestions FROM tbl_questiongroupdetails as a ,tbl_questions as b
					WHERE b.idquestions=a.idquestions and a.idquestiongroup=$RegID
					ORDER BY SUBSTR(b.QuestionNumber FROM 1 FOR 1), CAST(SUBSTR(b.QuestionNumber FROM 2) AS UNSIGNED)DESC";					
				$larrResult = $lobjDbAdpt->fetchAll($select2);
				return $larrResult;
	}
	
	
       public function fndisplayquestions($merquesarr){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$queslist=array();
				foreach($merquesarr as $merquesarr1){
					$lstrSelect = $lobjDbAdpt->select()
						->from(array("c" => "tbl_questions"),array("c.idquestions",'c.Question',"c.QuestionNumber","c.QuestionLevel"))
						->where("c.idquestions =?",$merquesarr1);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				$queslist=array_merge($queslist,$larrResult);
				}
				return $queslist;
	}
	
	public function fngettosdetails($idtos){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_tosmaster"),array("a.IdTOS"))
										  ->join(array("b"=>"tbl_tosdetail"),'b.IdTOS=a.IdTOS',array("b.IdSection","b.NosOfQuestion as chaptotal"))
										  ->join(array("c"=>"tbl_tossubdetail"),'c.IdTOSDetail =b.IdTOSDetail',array("c.IdDiffcultLevel","c.NoofQuestions as leveltotal"))
										  //->join(array("d"=>"tbl_batchmaster"),'d.IdBatch=a.IdBatch',array(""))
										   //->join(array("e"=>"tbl_batchdetail"),'e.IdBatch=d.IdBatch',array("e.IdPart","e.NoOfQuestions"))
										  ->where("a.Active=1")
										  ->where("a.IdTOS =?",$idtos);
										  //->order("b.IdSection")
										//->order("c.IdDiffcultLevel");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	 	
	 }
      public function fngettosdetailsorder($idtos){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	$select2 ="SELECT a.IdTOS,b.IdSection ,b.NosOfQuestion as chaptotal,c.IdDiffcultLevel,c.NoofQuestions as leveltotal FROM tbl_tosmaster as a ,tbl_tosdetail as b,tbl_tossubdetail as c
				WHERE b.IdTOS=a.IdTOS and a.Active=1 and c.IdTOSDetail =b.IdTOSDetail and a.IdTOS =$idtos
				ORDER BY SUBSTR(b.IdSection FROM 1 FOR 1), CAST(SUBSTR(b.IdSection FROM 2) AS UNSIGNED)DESC";
	 	$larrResult = $lobjDbAdpt->fetchAll($select2);
		return $larrResult;
	 }
	 
      public function fngetparts($idtos){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_tosmaster"),array("a.IdTOS"))
										 ->join(array("d"=>"tbl_batchmaster"),'d.IdBatch=a.IdBatch',array(""))
										   ->join(array("e"=>"tbl_batchdetail"),'e.IdBatch=d.IdBatch',array("e.IdPart","e.NoOfQuestions"))
										  ->where("a.Active=1")
										  ->where("a.IdTOS =?",$idtos);
										  //->order("b.IdSection")
										//->order("c.IdDiffcultLevel");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	 	
	 }
	 
	
	 
	 
		  	 
}
