<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_QuestionsattendedController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjQuestionsattendedmodel = new Examination_Model_Questionsattended(); //intialize newscreen db object
		$this->lobjQuestionsattendedForm = new Examination_Form_Questionattended(); 
	}
	
	public function indexAction() 
	{
	
	$this->view->lobjQuestionsattendedForm = $this->lobjQuestionsattendedForm;
	$larrresultprog =$this->lobjQuestionsattendedmodel->fngetprog(); 
		$this->lobjQuestionsattendedForm->Program->addMultiOptions(array(
									'' =>'All'));
	$this->lobjQuestionsattendedForm->Program->addMultiOptions($larrresultprog);
	
	$this->lobjQuestionsattendedForm->Program->addMultiOptions(array(
									'' =>'All'));
	
	$larrresultgroups =$this->lobjQuestionsattendedmodel->fngetquestiongrp(); 
	$this->lobjQuestionsattendedForm->grouptype->addMultiOptions($larrresultgroups);
	 

	if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformdata = $this->_request->getPost ();
			
			$this->lobjQuestionsattendedForm->FromDate->setValue($larrformdata['FromDate']);
				$this->lobjQuestionsattendedForm->ToDate->setValue($larrformdata['ToDate']);
			$this->view->fromdate = $larrformdata['FromDate'];
			$this->view->todate = $larrformdata['ToDate'];
			$this->view->prog = $larrformdata['Program'];
			$this->view->difftype = $larrformdata['Difficultytype'];
			$this->view->questionids = $larrformdata['questionids'];
			$this->view->grouptype = $larrformdata['grouptype'];
			$this->lobjQuestionsattendedForm->Program->setValue($larrformdata['Program']);
			$this->lobjQuestionsattendedForm->Difficultytype->setValue($larrformdata['Difficultytype']);
			$this->lobjQuestionsattendedForm->questionids->setValue($larrformdata['questionids']);
			$this->lobjQuestionsattendedForm->grouptype->setValue($larrformdata['grouptype']);
		//	$larrresultquestions = $this->lobjQuestionsattendedmodel->fngetprogdetails($larrformdata['Program'],$larrformdata['Difficultytype']);
		
			$larrresultquestions = $this->lobjQuestionsattendedmodel->fngetpartdetails($larrformdata,$larrformdata['Difficultytype']);
			$larrbatchids=$this->lobjQuestionsattendedmodel->fngetbatchidbyparts($larrformdata);
			//echo "<pre/>";
			//print_r($larrbatchids);
		
		for($idbatch=0;$idbatch<count($larrbatchids);$idbatch++)
		{
		if($idbatch==0)
		{
			$idbatchs=$larrbatchids[$idbatch]['IdBatch'];
		}
		else 
		{
				$idbatchss=$larrbatchids[$idbatch]['IdBatch'];
		$idbatchs.=','.$idbatchss;	
		}

		
		}
			
		//echo $idbatchs;	die();
			if(empty($idbatchs))
			{
				$idbatchs=0;
			}
			//$larresutstudentid = $this->lobjQuestionsattendedmodel->fnmaxids($larrformdata['Program'],$larrformdata['FromDate'],$larrformdata['ToDate']);
			
			
			$larrresultgetidapplications=$this->lobjQuestionsattendedmodel->fngetidapplications($larrformdata['FromDate'],$larrformdata['ToDate'],$idbatchs);
			//print_r($larrresultgetidapplications);die();
			
			
			for($idapp=0;$idapp<count($larrresultgetidapplications);$idapp++)
		{
		if($idapp==0)
		{
			$idapplications=$larrresultgetidapplications[$idapp]['idapplication'];
		}
		else 
		{
				$idapplicationss=$larrresultgetidapplications[$idapp]['idapplication'];
		$idapplications.=','.$idapplicationss;	
		}

		
		}
			
		//echo $idapplications;	die();
			if(empty($idapplications))
			{
				$idapplications=0;
			}
			
			$larresutstudentid = $this->lobjQuestionsattendedmodel->fnmaxidsbybatch($idbatchs,$idapplications,$larrformdata['FromDate'],$larrformdata['ToDate']);
		$this->view->cntofstudent = count($larresutstudentid);
		//echo count($larresutstudentid);die();
		
		$larrgetprgramdetails= $this->lobjQuestionsattendedmodel->fngetprgdetails($idapplications);
			$values=0;$studentid=0;
		for($idsech=0;$idsech<count($larresutstudentid);$idsech++)
		{
		
		$value=$larresutstudentid[$idsech]['Appeared'];
		$idapplication = $larresutstudentid[$idsech]['Idapplication'];
		$studentid = $studentid.','.$idapplication;
		$values=$values.','.$value;
		
		}	
		$larresutregids = $this->lobjQuestionsattendedmodel->fngetregids($studentid);
		
		$regids = 0;
		//echo $values;die();
		for($linti=0;$linti<count($larresutregids);$linti++)
		{
		
		$value=$larresutregids[$linti]['Regid'];
		$regids = $regids.','."'$value'";		
		}	
		$this->view->regids = $regids;
		$this->view->maxids = $values;
			$this->view->examdate = $larrformdata['FromDate'];
			$this->view->questions = $larrresultquestions;
			$this->view->prgdetails=$larrgetprgramdetails;
			
			
			
		}
		
		
		
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/schedulerutility/index');
			//$this->_redirect($this->view->url(array('module'=>'registrations' ,'controller'=>'batchapproval', 'action'=>'index'),'default',true));
		}
		
	}
	
	
	public function fngetquestiondetailsAction()
	{
		
		 $this->_helper->layout->disableLayout();
		 $this->_helper->viewRenderer->setNoRender();
		 $qtnid = $this->_getParam('qtnid');		
		 $result = $this->lobjQuestionsattendedmodel->fngetquestionanswerdetails($qtnid);
		
		  $tabledata.= '<br><fieldset><legend align = "left"> Question and Answer:<b>Question Id'.$qtnid.'</b></legend>
					                    <table class="table" border=1 align = "center" width=100%>
					                    	<tr>
					                    	   <th><b>Question</b></th>
					                    		<th></th>
					                    		
					                    	</tr><tr>';
		  
		                
						  $tabledata.= '<td align = "left">'.$result[0]['Question'].'</td></tr><tr>
					                    		<th><b>Answers</b></th>
					                    		
					                    	</tr><tr>';
						    $tabledata.= '<td align = "left">'.$result[0]['answers'].'</td></tr><tr>';
						      $tabledata.= '<td align = "left">'.$result[1]['answers'].'</td></tr><tr>';
						        $tabledata.= '<td align = "left">'.$result[2]['answers'].'</td></tr><tr>';
						          $tabledata.= '<td align = "left">'.$result[3]['answers'].'</td></tr>';
						          for($i=0;$i<4;$i++)
						          {
						          	  if($result[$i]['CorrectAnswer']==1)
						          	  {
						          	    $tabledata.= '<tr><th><b>Correct Answer</b></th></tr><tr>';
						          	      $tabledata.= '<td align = "left">'.$result[$i]['answers'].'</td></tr>';
						          	  }
						          }
						           $tabledata.="<tr><td align='right'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
			 echo  $tabledata;	   
	}
	
	public function fnpdfexportAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$frmdate = $this->_getParam('frmdate');
		$todate = $this->_getParam('todate');
		$prog = $this->_getParam('prog');
		$difftype = $this->_getParam('difftype');
	$questionids = $this->_getParam('questionids');
	
		$grouptype = $this->_getParam('grouptype');
		if(!empty($grouptype))
		{
		$post['grouptype']=$grouptype;
		}
		
	    $larrresultquestions = $this->lobjQuestionsattendedmodel->fngetpartdetails($post,$difftype);
	  
	if(!empty($prog))
		{
		$post['Program']=$prog;
		}
	    
	    		$larrbatchids=$this->lobjQuestionsattendedmodel->fngetbatchidbyparts($post);
	    		
	    		
	 //   $progname = $larrresultquestions[0]['ProgramName'];
	  
	    
	    	for($idbatch=0;$idbatch<count($larrbatchids);$idbatch++)
		{
		if($idbatch==0)
		{
			$idbatchs=$larrbatchids[$idbatch]['IdBatch'];
		}
		else 
		{
				$idbatchss=$larrbatchids[$idbatch]['IdBatch'];
		$idbatchs.=','.$idbatchss;	
		}

		
		}
			
		//echo $idbatchs;	die();
			if(empty($idbatchs))
			{
				$idbatchs=0;
			}
			//$larresutstudentid = $this->lobjQuestionsattendedmodel->fnmaxids($larrformdata['Program'],$larrformdata['FromDate'],$larrformdata['ToDate']);
			
			
			$larrresultgetidapplications=$this->lobjQuestionsattendedmodel->fngetidapplications($frmdate,$todate,$idbatchs);
			//print_r($larrresultgetidapplications);die();
			
	for($idapp=0;$idapp<count($larrresultgetidapplications);$idapp++)
		{
		if($idapp==0)
		{
			$idapplications=$larrresultgetidapplications[$idapp]['idapplication'];
		}
		else 
		{
				$idapplicationss=$larrresultgetidapplications[$idapp]['idapplication'];
		$idapplications.=','.$idapplicationss;	
		}

		
		}
			
		//echo $idapplications;	die();
			if(empty($idapplications))
			{
				$idapplications=0;
			}
			
			$larresutstudentid = $this->lobjQuestionsattendedmodel->fnmaxidsbybatch($idbatchs,$idapplications,$frmdate,$todate);
			//print_r($larresutstudentid);die();
		$totalstudent = count($larresutstudentid);
		//echo $totalstudent;die();
		//echo count($larresutstudentid);die();
		
		$larrgetprgramdetails= $this->lobjQuestionsattendedmodel->fngetprgdetails($idapplications);
			$values=0;$studentid=0;
		for($idsech=0;$idsech<count($larresutstudentid);$idsech++)
		{
		
		$value=$larresutstudentid[$idsech]['Appeared'];
		$idapplication = $larresutstudentid[$idsech]['Idapplication'];
		$studentid = $studentid.','.$idapplication;
		$values=$values.','.$value;
		
		}	
		$larresutregids = $this->lobjQuestionsattendedmodel->fngetregids($studentid);
		
		$regids = 0;
		//echo $values;die();
		for($linti=0;$linti<count($larresutregids);$linti++)
		{
		
		$value=$larresutregids[$linti]['Regid'];
		$regids = $regids.','."'$value'";		
		}
		$this->view->regids = $regids;
		$this->view->maxids = $values;
		$this->view->examdate = $frmdate;
		$this->view->questions = $larrresultquestions;
			
		$host = $_SERVER['SERVER_NAME'];
	$imgp = "http://".$host."/tbe/images/reportheader.jpg";  
	$html ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';	
		$html.="<br><br><table border=1 align=center width=100%><tr><td align ='left' colspan = 1><b>Exam From Date</b></td><td align ='left' colspan = 1><b>$frmdate</b></td><td align ='left' colspan = 1><b>Exam ToDate</b></td><td align ='left' colspan = 1><b>$todate</b></td><td align ='left' colspan = 2><b>Question Group</b></td><td align ='left' colspan = 3><b>$grouptype</b></td></tr></table><br><br>";
		$html.= '<table class="table" width="100%" cellpadding="5" cellspacing="1" border="1">
		
	    		<tr>
	    	
	    	       <th>Question </th>
	    	       <th>Question Group</th>
	    	       <th>Chapter</th>
	    	            <th>Question Level</th>
	    	     <th>Total Candidate</th>
	    	    	    	       <th>Appeared</th>
     <th>% Usage</th>
	    	         <th>Attended</th>
	    	    
	    	         
	    	         <th>% Correct</th>
	    	         <th>Wrong</th>
	    	         <th>% Wrong</th>
	        	
	    	</tr>'	;
		 $cnts = count($larrresultquestions);
			     		$larrquestiondetails = $larrresultquestions; 
			     		$examdate = $frmdate;$sum=0;$attended=0;$correct=0;
   			     $regids;
	       for($i=0;$i<$cnts;$i++){
	        	$questionlevel = $larrquestiondetails[$i]['QuestionLevel'];
	        	if($questionlevel=='1')
	        	  $questionanswelevel = "Easy";
	        	else if($questionlevel=='2')
	        	  $questionanswelevel = "Medium";
	        	else 
	        	    $questionanswelevel = "Difficult";

 if($questionids==1)
	        	    {
	        	    	$larrquestios = $larrquestiondetails[$i]['idquestions'];
	        	    }
	        	    else
	        	    {
	        	    	$larrquestios = $larrquestiondetails[$i]['Question'];
	        	    } 
	     
	        	    
	        if(empty($totalstudent)||$totalstudent==0)
	      {
	      	$totalstudent=1;
	      }
	             if(empty($totalattendedquestion)||$totalattendedquestion==0)
	      {
	      	$totalattendedquestion=1;
	      }
	      
	        $html.='<tr>
	  			<td>'.$larrquestios.'</td>
	  		      <td>'.$larrquestiondetails[$i]['QuestionGroup'].'</td>
	  		     <td>'.$larrquestiondetails[$i]['QuestionNumber'].'</td>
	  		     <td>'.$questionanswelevel.'</td>';
	  		 $lobjexamquestionsattendedmodel = new Examination_Model_Questionsattended();
	  		       $maxidss = $values;
	  		       $larrresultattended = $lobjexamquestionsattendedmodel->fnGetQtnsapperedinexam($larrquestiondetails[$i]['idquestions'],$maxidss);
	  	           $cnt = count($larrresultattended);
	  	           $sum = $sum+$cnt;
	  	           $larrresultstudentattended = $lobjexamquestionsattendedmodel->fnGettotalattended($larrquestiondetails[$i]['idquestions'],$regids);
	  	           $totalattendedquestion = count($larrresultstudentattended);

	  	            $attended = $attended+$totalattendedquestion;
	  	           $larrresultstudentcorrectattended = $lobjexamquestionsattendedmodel->fnGettotalCorrectattended($larrquestiondetails[$i]['idquestions'],$regids);
	  	           $totalcorrectattended = count($larrresultstudentcorrectattended);
	  	             $correct=$correct+$totalcorrectattended;
	  	           $totalwrong = $totalattendedquestion-$totalcorrectattended;
 $totalusage=0.00;$correctusage=0.00;$wrongusage=0.00;
	  	           if($totalattendedquestion>0)
	  	           {
	  	           	
	  	           $totalusage = number_format(($totalattendedquestion/$totalstudent*100),2);
	  	           } 
	  	           
	  	            if($totalcorrectattended>0)
	  	           {
	  	           	
	  	           $correctusage = number_format(($totalcorrectattended/$totalattendedquestion*100),2);
	  	           } 
	  	             if($totalwrong>0)
	  	           {
	  	           	
	  	           $wrongusage = number_format(($totalwrong/$totalattendedquestion*100),2);
	  	           } 

	  	         $html.='
	          <td>'.$totalstudent.'</td>
	  	      <td>'.$cnt.'</td>
              <td>'.$totalusage.'</td>  
	  		   <td>'.$totalattendedquestion.'</td>  
	  		  
	  		       <td>'.$totalcorrectattended.'</td>  
	  		        <td>'.$correctusage.'</td>  
	  		        <td>'.$totalwrong.'</td> 
	  		         <td>'.$wrongusage.'</td> 
	  		</tr>';
	        
	        }
	        	          
 $html.='<tr><td colspan="5" align="left">
</td>';
 $html.='<td align="right">'.$sum.'</td>';
 $html.='<td></td>';
 $html.='<td align="right">'.$attended.'</td>';

$html.='<td align="right">'.$correct.'</td>';
$html.='<td></td>';
$wrongs=($attended-$correct);
$html.='<td align="right">'.$wrongs.'</td>';
	       $html.='</table>';
	      
	        /*echo $html;
	        die();*/
	       $filename='questions'; 
	     $ourFileName = realpath('.')."/data"; 
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); //open a file to write a text
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($html));//write the content of htmlcode into text file
		fclose($ourFileHandle); //closeing a file 
		header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
		header("Content-Disposition: attachment; filename=$filename.xls");
		readfile($ourFileName);
		unlink($ourFileName);   

			
		}

	
}