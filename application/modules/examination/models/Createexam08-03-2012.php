<?php
class Examination_Model_Createexam extends Zend_Db_Table {
	
	/*
	 * functin to fetch all easy questions
	 */
	public function fnGetBatchArray($batch)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt-> select()
		    					 -> from(array("a" => "tbl_batchmaster"),array("key"=>"a.IdBatch","value"=>"a.BatchName"))
								 -> where("a.BatchStatus = 0");
		if($batch != 0)	$lstrSelect -> where("a.IdBatch = ".$batch)	;	
		else			$lstrSelect -> where("a.IdBatch NOT IN (SELECT IdBatch FROM tbl_tosmaster)")	;		
			 				$lstrSelect -> order("a.IdBatch desc");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnGetBatchArraySearch()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt-> select()
		    					 -> from(array("a" => "tbl_batchmaster"),array("key"=>"a.IdBatch","value"=>"a.BatchName"))
								 -> where("a.BatchStatus = 0");									
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	/*
	 * function to fetch all the sections
	 */
	public function fnGetSections($idbatch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select ="SELECT distinct(`b`.`IdSection`) FROM `tbl_tosdetail` AS `b` INNER JOIN `tbl_tosmaster` AS `c` ON b.IdTOS = c.IdTOS INNER JOIN `tbl_batchmaster` AS `d` ON c.IdBatch = d.IdBatch WHERE (d.IdBatch =$idbatch)" ;                         
		 return	$result = $lobjDbAdpt->fetchAll($select);	
				
	}
	public function fnGetBatchDetailsAjax($idbatch){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select ="SELECT bd.*,b.BatchFrom,b.BatchTo,q.QuestionNumber,q.idquestions,count(q.idquestions) cnt,q.QuestionLevel
		  		FROM tbl_questions AS q
		  		INNER JOIN `tbl_batchdetail` AS `bd` ON q.QuestionGroup=bd.IdPart
		  		INNER JOIN `tbl_batchmaster` AS `b` ON ( bd.IdBatch = b.IdBatch AND b.IdBatch =$idbatch ) 		  		
		  		WHERE b.BatchStatus = 0 AND bd.BatchDtlStatus = 0 
		  		GROUP BY q.QuestionNumber,q.QuestionGroup 
		  		ORDER BY q.idquestions ASC" ;                         
		return	$result = $lobjDbAdpt->fetchAll($select);		
	}
	public function fnGetQuestionDetailsAjax($idbatch){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select ="SELECT q.idquestions,q.QuestionNumber,q.QuestionLevel,count(q.QuestionLevel) qcnt,q.QuestionGroup 
		  		FROM tbl_questions AS q
		  		INNER JOIN `tbl_batchdetail` AS `bd` ON q.QuestionGroup=bd.IdPart
		  		INNER JOIN `tbl_batchmaster` AS `b` ON ( bd.IdBatch = b.IdBatch AND b.IdBatch =$idbatch ) 		  		
		  		WHERE b.BatchStatus = 0 AND bd.BatchDtlStatus = 0 
		  		GROUP BY q.QuestionNumber,q.QuestionLevel" ;                         
		return	$result = $lobjDbAdpt->fetchAll($select);	
	}
	function fnCreateexam($post){		
		$db = Zend_Db_Table::getDefaultAdapter();
	    $table = "tbl_tosmaster";
		$msg = $db->insert($table,$post);	
		return $lintIdBatchMaster = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_tosmaster','IdTOS');
	}
	function fnCreateexamDetails($post){
		$db = Zend_Db_Table::getDefaultAdapter();
	    $table = "tbl_tosdetail";
		$msg = $db->insert($table,$post);	
		return $lintIdBatchMaster = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_tosdetail','IdTOSDetail');
	}
	function fnCreateexamSubDetails($post){
		$db = Zend_Db_Table::getDefaultAdapter();
	    $table = "tbl_tossubdetail";
		$msg = $db->insert($table,$post);	
		return $lintIdBatchMaster = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_tossubdetail','IdTOSSubDetail');
	}
	function fnGetExamDetails($batchId){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select ="SELECT b.IdBatch,t.IdTOS,b.BatchName,b.BatchFrom,b.BatchTo,t.NosOfQues,t.TimeLimit,t.AlertTime,t.Active
		  		FROM tbl_tosmaster AS t		  		
		  		INNER JOIN `tbl_batchmaster` AS `b` ON (t.IdBatch = b.IdBatch) 		  		
		  		WHERE b.BatchStatus = 0 and t.Active=1";     
		  if($batchId != 0)   $select .= " AND b.IdBatch=".$batchId; 		  	   
		  return	$result = $lobjDbAdpt->fetchAll($select);	
	}
	function fnGetExamDetailsEdit($idTOSMaster){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select ="SELECT b.IdBatch,t.IdTOS,b.BatchName,b.BatchFrom,b.BatchTo,t.NosOfQues,t.TimeLimit,t.AlertTime,t.Pass,t.Active
		  		FROM tbl_tosmaster AS t		  		
		  		INNER JOIN `tbl_batchmaster` AS `b` ON (t.IdBatch = b.IdBatch) 		  		
		  		WHERE b.BatchStatus = 0 AND t.IdTOS = ".$idTOSMaster; 		  	   
		  return	$result = $lobjDbAdpt->fetchAll($select);	
	}
	function fnUpdateBatchDetails($larrBatchDetailsData,$larrBatchDetailsCond){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$data = array('NoOfQuestions' => $larrBatchDetailsData['NoOfQuestions']);		
		$where['IdBatch = ? ']= $larrBatchDetailsCond['IdBatch'];
		$where['IdPart = ? ']= $larrBatchDetailsCond['IdPart'];		
		return $db->update('tbl_batchdetail', $data, $where);		
	}
	
	public  function fnGetCourse($idTOSMaster)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_batchmaster"),array("a.IdProgrammaster") )
       								->join(array("b"=>"tbl_programmaster"),'b.IdProgrammaster=a.IdProgrammaster' ,array("b.ProgramName") )
       								->join(array("c"=>"tbl_tosmaster"),'a.IdBatch=c.IdBatch')
					 				->where("c.IdTOS=?",$idTOSMaster);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
	
public  function fnGetCoursesws($idprog,$idbatch)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_batchmaster"),array("a.IdProgrammaster","a.IdBatch") )
       								->join(array("b"=>"tbl_programmaster"),'b.IdProgrammaster=a.IdProgrammaster' ,array("b.ProgramName") )
       								->where("a.IdProgrammaster=?",$idprog)	
					 				->where("a.IdBatch in (select IdBatch  from tbl_tosmaster where a.IdBatch!=$idbatch)");					  	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		
	}
	
	public function fndeactivateset($values)
	{
		
		$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$select ="update tbl_tosmaster set Active=0 where IdBatch in($values)";
		$db->query($select);

		
	}
	function fnactivateset($idbatch){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$data = array(	'Active' =>'1');		
		$where['IdBatch = ? ']=$idbatch;		
		return $db->update('tbl_tosmaster', $data, $where);	
	}
	
	
public  function fnGetcrs($idbatch)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_batchmaster"),array("a.IdProgrammaster","a.IdBatch") )
       								->join(array("b"=>"tbl_programmaster"),'b.IdProgrammaster=a.IdProgrammaster' ,array("b.ProgramName") )
					 				->where("a.IdBatch=?",$idbatch);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
	
	
	function fnGetTOSFullDetails($idTOSMaster){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select ="SELECT bd.*,b.BatchFrom,b.BatchTo,q.QuestionNumber,q.idquestions, IF(td.NosOfQuestion,td.NosOfQuestion,0) tdNosOfQuestion ,
		 				 IF(tsd.NoofQuestions,tsd.NoofQuestions,0) NoofQuestions,tsd.IdDiffcultLevel
						FROM tbl_questions AS q 
						INNER JOIN `tbl_batchdetail` AS `bd` ON q.QuestionGroup=bd.IdPart 
						INNER JOIN `tbl_batchmaster` AS `b` ON (bd.IdBatch = b.IdBatch) 
						INNER JOIN tbl_tosmaster AS `t` ON (t.IdBatch = b.IdBatch AND t.IdTOS = ".$idTOSMaster.")		 
						LEFT JOIN tbl_tosdetail AS `td` ON (td.IdTOS = t.IdTOS AND td.IdSection = q.QuestionNumber) 
						LEFT JOIN tbl_tossubdetail AS `tsd` ON (tsd.IdTOSDetail = td.IdTOSDetail )	 
						WHERE b.BatchStatus = 0 AND bd.BatchDtlStatus = 0 
						GROUP BY q.QuestionNumber,q.QuestionGroup ,tsd.IdDiffcultLevel
						ORDER BY q.idquestions ASC";
		 return	$result = $lobjDbAdpt->fetchAll($select);		
	}
	function fnUpdateexam($larrExamData,$larrExamCond){	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$data = array(	'NosOfQues' => $larrExamData['NosOfQues'],
						'TimeLimit' => $larrExamData['TimeLimit'],
						'AlertTime' => $larrExamData['AlertTime'],
						'UpdDate' 	=> $larrExamData['UpdDate'],
						'UpdUser'	=> $larrExamData['UpdUser']);		
		$where['IdBatch = ? ']= $larrExamCond['IdBatch'];
		$where['IdTOS = ? ']= $larrExamCond['idTOSMaster'];			
		$db->update('tbl_tosmaster', $data, $where);	
		return $larrExamCond['idTOSMaster'];
	}
	function fnUpdateexamDetails($larrExamDetailsData,$larrExamDetailsCond){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$data = array(	'NosOfQuestion' => $larrExamDetailsData['NosOfQuestion'],
						'UpdDate' => $larrExamDetailsData['UpdDate'],
						'UpdUser' => $larrExamDetailsData['UpdUser']);		
		$where['IdTOS = ? ']= $larrExamDetailsCond['IdTOS'];
		$where['IdSection = ? ']= $larrExamDetailsCond['IdSection'];	
		$db->update('tbl_tosdetail', $data, $where);
		$select = "SELECT IdTOSDetail FROM tbl_tosdetail WHERE IdTOS = ".$larrExamDetailsCond['IdTOS']." AND IdSection = '".$larrExamDetailsCond['IdSection']."'";
		$result = $db->fetchRow($select);
		return $result['IdTOSDetail'];
	}
	function fnUpdateexamSubDetails($larrExamSubDetailsData,$larrExamSubDetailsCond){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$data = array(	'UpdDate' => $larrExamSubDetailsData['UpdDate'],
						'UpdUser' => $larrExamSubDetailsData['UpdUser'],
						'NoofQuestions' => $larrExamSubDetailsData['NoofQuestions']);		
		$where['IdTOSDetail = ? ']= $larrExamSubDetailsCond['IdTOSDetail'];
		$where['IdDiffcultLevel = ? ']= $larrExamSubDetailsCond['IdDiffcultLevel'];		
		return $db->update('tbl_tossubdetail', $data, $where);	
	}
	
	
	public function fnGetBatch($IdTos)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_tosmaster"),array("a.IdBatch"))	
       								->where("IdTOS=?",$IdTos);	  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	}
	
public function fnGetBatchquestions($idbatch)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_tosmaster"),array("a.IdBatch"))	
       								->where("IdTOS=?",$IdTos);	  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	}
	public function  fnGetquestions($idq)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_questions"),array("a.*"))
       								->where("a.idquestions not in ($idq)")
       								->order("a.QuestionGroup");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		
	}
	public function fnaddExamquestions($larrid,$idbatches,$porgid)
	{
       	$db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_questionset";
          for($i=0;$i<count($larrid);$i++)
          {
            $postData = array(		
							'idquestion' => $larrid[$i],	
           					'idbatch' =>$idbatches,	
                            'Idprogram'=>$porgid	
						);	
					
	     $db->insert($table,$postData);
          }
		
	}
	
	public function fnupdateExamquestions($idbatches)
	{
       	$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$data = array(	'editbatch' =>'0');		
		$where['idbatch = ? ']=$idbatches;		
		return $db->update('tbl_questionset', $data, $where);
		
	}
	
public function  fnGetViewquestions($idq)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_questionset"),array(""))
       								->join(array("b"=>"tbl_questions"),'a.idquestion=b.idquestions',array("b.*")) 
       								->where("a.editbatch=1")    
       								->where("a.idbatch=?",$idq);
       								  								
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		
	}
	
	
}
	/*SELECT bd.*,b.BatchFrom,b.BatchTo,q.QuestionNumber,q.idquestions, IF(td.NosOfQuestion,td.NosOfQuestion,0) 
		  		FROM tbl_questions AS q
		  		INNER JOIN `tbl_batchdetail` AS `bd` ON q.QuestionGroup=bd.IdPart
		  		INNER JOIN `tbl_batchmaster` AS `b` ON (bd.IdBatch = b.IdBatch) 	
		  		INNER JOIN tbl_tosmaster AS `t` ON (t.IdBatch = b.IdBatch AND  t.IdTOS = ".$idTOSMaster.")	
		  		INNER JOIN tbl_tosdetail AS `td` ON (td.IdTOS  = t.IdTOS)	 
		  		INNER JOIN tbl_tossubdetail AS `tsd` ON (tsd.IdTOSDetail   = td.IdTOSDetail )	 		
		  		WHERE b.BatchStatus = 0 AND bd.BatchDtlStatus = 0 
		  		GROUP BY q.QuestionNumber,q.QuestionGroup 
		  		ORDER BY q.idquestions ASC";
		
		
		
		/*SELECT tsd.*,td.*,t.*,b.*,bd.IdPart
		  		FROM tbl_tossubdetail AS tsd		  		
		  		INNER JOIN tbl_tosdetail AS td ON td.IdTOSDetail = tsd.IdTOSDetail 
		  		INNER JOIN tbl_tosmaster AS t	ON 	 ( t.IdTOS=td.IdTOS		AND  t.IdTOS = ".$idTOSMaster.")
		  		INNER JOIN tbl_batchmaster AS `b` ON (t.IdBatch = b.IdBatch)
		  		INNER JOIN tbl_batchdetail AS `bd` ON (t.IdBatch = bd.IdBatch)";*/ 		 