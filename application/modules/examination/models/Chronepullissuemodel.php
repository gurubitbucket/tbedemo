<?php
	class Examination_Model_Chronepullissuemodel extends Zend_Db_Table {


	public function fngetvenuedateschedule($date)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  $lstrSelect =  $lobjDbAdpt->select()
		           ->from(array('a'=>'tbl_venuedateschedule'),array())
				   ->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("key"=>"b.idcenter","value"=>"b.centername"))
				   ->join(array("c"=>"tbl_managesession"),'a.idsession=c.idmangesession',array("c.*"))
				   ->where("a.date='$date'")
				   ->where("a.Active = 1")
					->where("a.Reserveflag= 1")
					->where("b.ipaddress=1")
					->where("a.Allotedseats>0")
				    ->group("b.idcenter")
					->order("b.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;			
	}
	
	 public function fngetallvenues($larrformdata)
    {
    	$iddate = $larrformdata['Date'];
    	$Venue = $larrformdata['Venue'];
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			  ->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("b.*"))
							 			  ->join(array("c"=>"tbl_managesession"),'a.idsession=c.idmangesession',array("c.*"))
							 			  ->where("a.date='$iddate'")
							 			  ->where("b.idcenter='$Venue'")
							 			  ->where("b.ipaddress=1")
							 			  ->where("a.Allotedseats>0")
							 			  ->group("b.idcenter")
							 			  ->order("b.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
	 public function fngetstatusofvenue($larrformData)
	 {
	if($larrformData['Venue']) $venue = $larrformData['Venue'];
		if($larrformData['Date']) $examdate = $larrformData['Date'];
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_autoissue"),array("a.*"))
							 			  ->join(array("b"=>"tbl_chroneissue"),'a.Idautoissue=b.Idautoissue',array("b.*"));
										 
                                       
		if($larrformData['Venue'])	$lstrSelect->where("b.Examcenter=?",$venue);	
        if($larrformData['Date'])	$lstrSelect->where("b.Examdate=?",$examdate);	
//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	 }
	  public function fninsertissuescheduledetails($larrformdata)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$cnt = count($larrformdata['idcenter']);
        $interval=0;

    	for($i=0;$i<$cnt;$i++)
    	{			   	
			   	$schedulerdate = $larrformdata['Schedulerdate'];
			   	$schedulertime = $larrformdata['ScheduleStartTime'];
			   	$schedulerdattime = $schedulerdate.' '.$schedulertime;
			   
			   	$interval = $interval+1;
			   	
			   	$queryoftime = "SELECT DATE_ADD('$schedulerdattime', INTERVAL '$interval:0' MINUTE_SECOND) as Timeintervalss";
				
			   	$larresulttime = $lobjDbAdpt->fetchRow($queryoftime);
				//echo "<pre>";
				//print_r($larresulttime);die();
			   	$larrscheduertime = $larresulttime['Timeintervalss'];
			   	$larraddedtime = explode(' ',$larrscheduertime);
			   	$addeddate = $larraddedtime[0];
			   	$addedtime = $larraddedtime[1];
			   	
			   	
		    $postdata = array('Scheduleddate'=>$addeddate,
		                      'Scheduledtime'=>$addedtime,
		                      'UpdDate' => $larrformdata['UpdDate'],		    				  
		                      'UpdUser' => $larrformdata['UpdUser']);	
		    $lobjDbAdpt->insert('tbl_autoissue',$postdata);
		    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
		    
			$idcenter = $larrformdata['idcenter'][$i];
    		
    		$table = "tbl_chroneissue";
            $postData = array(		
							'Examcenter' => $idcenter,		
            				'Examdate' =>$larrformdata['Date'],
           					'Status'=>0,
           					'Idautoissue' => $lintlastinsid		
						);			
	        $lobjDbAdpt->insert($table,$postData);
    	}   
    }
	public function fnupdatedchronedstatusfailed($idchronepull)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_chroneissue"; 
			 $where1 = "Idautoissue = '".$idchronepull."'"; 	  
						   $larrupdatedatas = array(		
							'Status' =>2,
							'Remark'=>"NA"
						);
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
	}
	public function fnupdatedserverdownchronedstatus($idchronepull)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       $tablestartexamdetails = "tbl_chroneissue"; 
	   $where1 = "Idautoissue = '".$idchronepull."'"; 	  
						   $larrupdatedatas = array(		
							'Status' =>2
							
						);
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
	}
	
	public function fngetstatusforthevenue($Venue,$idchronepull)
	{
		
		 $connected=1;
			try 
			{
			  $db = self::getAdaptor($Venue);
			 
			}
			catch (Exception $e)
			{
				 $connected=0;
				 $larresult = self::fnupdatedchronedstatusfailed($idchronepull);
			}
			
			return $connected;
	}
	public function getAdaptor($idcenter){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_center"),array("a.hostname"))
							 			  ->where("a.idcenter=$idcenter")
							 			  ->order("a.centername");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				 $hostname = $larrResult['hostname'];
		
		switch($idcenter)
									{
										Case 23 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     =>  $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
		    
								
								   				 
								   		Case 24 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     =>  $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
								   				 
								   	  Case 1 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													   'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
		    
							
								   				 
								   		Case 2 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
								   				 
								   				 
								   	Case 11 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
		    
										
								   				 
								   		Case 12 :  $db = Zend_Db::factory('Pdo_Mysql', array(
																		    'host'     =>$hostname,
																		    'username' => 'testing',
																		    'password' => 'testing',
																		    'dbname'   => 'clientdb'
																		));
								   				 Break;
								   				 
								   				 
								   	Case 13 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													   'username' => 'testing',
														'password' => 'testing',
														'dbname'   => 'clientdb'
													));
								   				 Break;
		    
									
								   				 
								   		Case 14 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													  	'username' => 'testing',
														'password' => 'testing',
														'dbname'   => 'clientdb'
													));
								   				 Break;
								   Case 15 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													   'host'     => $hostname,
													    'username' => 'testing',
														'password' => 'testing',
														'dbname'   => 'clientdb'
													));
								   				 Break;
		    
										
								   				 
								   		Case 16 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
								   				 
								   	Case 17 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'root',
													    'password' => 'admin',
													    'dbname'   => 'client_ipoh'
													));
								   				 Break;
		    
										
								   				 
								   		Case 18 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
								   				 
								   				 
								   	Case 19 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
		    
											
								   				 
								   		Case 20 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
								   Case 21 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
		    
											
								   				 
								   		Case 22 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
								   				 
								   	Case 25 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													   'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
		    
										
								   				 
								   		Case 26 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
								   				 
								   				 
								   	Case 27 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
		    
										
								   				 
								   		Case 28 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
								   Case 29 :
											
											$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
		    
									
								   				 
								   		Case 32 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;
								   				 
										   		Case 33 :$db = Zend_Db::factory('Pdo_Mysql', array(
													    'host'     => $hostname,
													    'username' => 'testing',
													    'password' => 'testing',
													    'dbname'   => 'clientdb'
													));
								   				 Break;						   				 
								  
									}		 
			
				
		    $db->getConnection();
		    
		
		
		return $db;
	}
	public function fngetstudentsexamissuedetails($Venue,$Examdate)
   {
	
	   $db = self::getAdaptor($Venue);
	   $lstrSelect = $db->select()
			   			->from(array("b"=>"tbl_studentissuedetails"),array("b.*"))
			   			->join(array("c"=>"tbl_issuedetails"),'c.Idissuedetail=b.Idissuedetails',array("c.*"))
						->join(array("d"=>"tbl_issuelist"),'d.Idissue=b.Idissue',array("d.*"))			   			
								 ->where("c.Examdate=?",$Examdate)
								 ->where("c.Examvenue=?",$Venue)
								 ->where("d.Active=1")
								 ->where("c.Flag=0");
								 //->group("a.idapplication");			 						
		$larrResult = $db->fetchAll($lstrSelect); 		
		return $larrResult;
    }
	public function fnupdatedchronedsucess($idissue)
	{
	           $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    		$tablestartexamdetails = "tbl_chroneissue"; 
				 $where1 = "Idautoissue = '".$idissue."'"; 	  
							   $larrupdatedatas = array(		
								'Status' => 1
							);
					
		         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
	}
	public function fngetupdatepushstatus($Venue,$Examdate)
	{
			$db = self::getAdaptor($Venue);  
					  $where = "Examdate = '".$Examdate."'
       	                 AND  Examvenue ='".$Venue."'";
						   $larrupdatedatas = array(		
							'Flag' =>1															
						);
				$tablestartexamdetails = "tbl_issuedetails"; 
	            $db->update($tablestartexamdetails,$larrupdatedatas,$where);
	}
	public function fnfindthemaxtimeofprogram($Venue,$Examdate)
      {
      	  	$db = self::getAdaptor($Venue);
		    $lstrSelect = $db->select()
				 				 ->from(array("a"=>"tbl_centerstartexam"),array("max(CAST((a.Totaltime) AS UNSIGNED INT)) as maximumtime","a.Startedtime")) 	
				 				 ->where("a.Examdate=?",$Examdate)
				 				// ->where("a.idSession=?",$Session)
				 				 ->where("a.idcenter=?",$Venue)
				 				 ->group("a.Examdate")
 ->order("a.Startedtime desc");
			$larrResult = $db->fetchRow($lstrSelect);
			return $larrResult;	
      }
	  public function fnfindthemaxstarttimeofvenue($Venue,$Examdate)
      {
      	  	$db = self::getAdaptor($Venue);
		    $lstrSelect = $db->select()
				 				 ->from(array("a"=>"tbl_centerstartexam"),array("max(CAST((a.Startedtime) AS UNSIGNED INT)) as maximumstartedtime")) 	
				 				
				 				 ->where("a.Examdate=?",$Examdate)				 				
				 				 ->where("a.idcenter=?",$Venue)
				 				 ->group("a.Examdate")
								 ->order("a.Startedtime desc");
			$larrResult = $db->fetchRow($lstrSelect);
			return $larrResult;	
      }
      
	      /*
       * function for finding the max startedtime
       */
	  
	  public function fnupdatestudent($larresult)
    { 
       	$db = Zend_Db_Table::getDefaultAdapter();
      	$cnt = count($larresult);
		
 		for($i=0;$i<$cnt;$i++)
       	{
       		
				
						  
						  
						    $tableissuedetails = "tbl_issuedetails";
		                    $studentdetails = array(	
								           					'Examdate' =>$larresult[$i]['Examdate'], 
															'Examvenue' =>$larresult[$i]['Examvenue'],	
								           					'Remark' =>$larresult[$i]['Remark'],
										                 	'UpdUser' =>$larresult[$i]['UpdUser'],
										                   	'UpdDate' =>$larresult[$i]['UpdDate'],	
								           					'Flag' =>$larresult[$i]['Flag']	  
														);
						  $db->insert($tableissuedetails,$studentdetails);

$Idissuedetailoflocal = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_issuedetails','Idissuedetail');


		    $tablestudentissuedetails = "tbl_studentissuedetails";
		                    $issuedetails = array(	
							           					'Idissue' =>$larresult[$i]['Idissue'], 
														'Idissuedetails' =>$Idissuedetailoflocal,	
							           					'IDApplication' =>$larresult[$i]['IDApplication'],
									                 	'Idsession' =>$larresult[$i]['Idsession'],
                                                        'Result' =>$larresult[$i]['Result']													
													);
						    $db->insert($tablestudentissuedetails,$issuedetails);
	                      
	               
	    }
		//return $IDApplication;
    }	
    public function fngetallvenuesforidautoissuemail($idautomail)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_autoissue"),array("a.*"))
							 			  ->join(array("b"=>"tbl_chroneissue"),'a.Idautoissue=b.Idautoissue',array("b.*"))
							 			  ->where("a.Idautoissue in ($idautomail)");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
	}
	public function fngetscheduledetails($shcdate,$shctime)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect =  "SELECT a.Idautoissue,a.Scheduledtime,a.Scheduleddate
			   				FROM tbl_autoissue a
			   				WHERE a.Scheduledtime = '$shctime' and a.Scheduleddate = '$shcdate'";
//echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
       	public function fngetissuelisttoupdate($larrformData)
	{
	   
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($larrformData['Venue']) $venue = $larrformData['Venue'];
		if($larrformData['Date']) $examdate = $larrformData['Date'];
	    $lstrSelect =  $lobjDbAdpt->select()
		           ->from(array('a'=>'tbl_venuedateschedule'),array("a.*"))
				   ->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("b.*"))
				   ->join(array("c"=>"tbl_managesession"),'a.idsession=c.idmangesession',array("c.*"))				 
				    ->where("a.date='$examdate'")
				    ->where("a.Active = 1")
					->where("a.Reserveflag= 1")
					->where("b.ipaddress=1")
					->where("a.Allotedseats>0")
					->where("a.idvenue not in (select m.Examvenue from tbl_issuedetails as m,tbl_studentissuedetails as n where m.Examdate='$examdate' and  n.Idissuedetails=m.Idissuedetail )")
		    		->group("b.idcenter")
					->order("b.centername");
		if($larrformData['Venue'])	$lstrSelect->where("a.idvenue=?",$venue);	
        if($larrformData['Date'])	$lstrSelect->where("a.date=?",$examdate);				
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);	
		return $larrResult;			
	}

	
	}