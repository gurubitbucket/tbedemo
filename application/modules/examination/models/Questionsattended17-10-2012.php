<?php
	class Examination_Model_Questionsattended extends Zend_Db_Table {


     

     public function fngetprogdetails($prog,$date)
     {
     	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	
		  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("tbl_programmaster" => "tbl_programmaster"),array("tbl_programmaster.ProgramName"))
							 			 ->join(array("tbl_batchmaster" => "tbl_batchmaster"),'tbl_programmaster.IdProgrammaster=tbl_batchmaster.IdProgrammaster',array())
							 			 ->join(array("tbl_tosmaster" => "tbl_tosmaster"),'tbl_batchmaster.IdBatch=tbl_tosmaster.IdBatch',array())
							 			 ->join(array("tbl_batchdetail" => "tbl_batchdetail"),'tbl_batchmaster.IdBatch=tbl_batchdetail.IdBatch',array())	
							 			 ->join(array("tbl_questions" => "tbl_questions"),'tbl_batchdetail.IdPart=tbl_questions.QuestionGroup',array("tbl_questions.*"))							 			 							 			 
                                         ->where("tbl_tosmaster.Active=1")
                                         ->where("tbl_questions.Active=1")
                                         ->where("tbl_programmaster.IdProgrammaster=?",$prog)
                                         ->where("tbl_questions.Active=1")
                                         ->group("tbl_questions.idquestions");
                                         
      if($date!=0)
     	 {
     	 	$lstrSelect->where("tbl_questions.QuestionLevel = ?",$date);
     	 }                
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     public function fngetprog()
     {
     			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_programmaster" => "tbl_programmaster"),array("key"=>"tbl_programmaster.IdProgrammaster","value"=>"tbl_programmaster.ProgramName"));
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
	 public function fnGetQtnsapperedinexam($Questionid,$maxids){

 		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 
 		 $select = "
 		 SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '$Questionid,%'
Union 
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid' 
Union
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid,%'
 		 
 		 ";

		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		

		
		
		public function fnmaxids($prog,$examdate)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		    $select = "SELECT MAX(idquestionsetforstudents) as Appeared,Idapplication
					 FROM  `tbl_questionsetforstudents` where idapplication in (		  
 		  			 SELECT b.idapplication 
						FROM tbl_studentapplication AS b
						WHERE b.Program =$prog
						AND b.DateTime ='$examdate'
					AND b.payment=1 and b.pass in(1,2)) group by idapplication";
 		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		
			
		public function fngetregids($studentid)
		{
             $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("tbl_registereddetails" => "tbl_registereddetails"),array("tbl_registereddetails.Regid"))
                                         ->where("tbl_registereddetails.Idapplication in ($studentid)");
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);	
 		 	
		}
		
		public function fnGettotalattended($Questionid,$regid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("tbl_answerdetails" => "tbl_answerdetails"),array("tbl_answerdetails.QuestionNo"))
                                         ->where("tbl_answerdetails.Regid in ($regid)")
                                          ->where("tbl_answerdetails.QuestionNo=?",$Questionid);
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		
		public function fnGettotalCorrectattended($Questionid,$regid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("tbl_answerdetails" => "tbl_answerdetails"),array("tbl_answerdetails.QuestionNo"))
							 			 ->join(array("tbl_answers"=>"tbl_answers"),'tbl_answers.idanswers=tbl_answerdetails.Answer',array())
                                         ->where("tbl_answerdetails.Regid in ($regid)")
                                          ->where("tbl_answerdetails.QuestionNo=?",$Questionid)
                                          ->where("tbl_answers.CorrectAnswer=1");
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		
		public function fngetquestionanswerdetails($qtnid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("tbl_questions" => "tbl_questions"),array("tbl_questions.Question"))
							 			  ->join(array("tbl_answers"=>"tbl_answers"),'tbl_answers.idquestion=tbl_questions.idquestions',array("tbl_answers.answers","tbl_answers.CorrectAnswer"))
                                          ->where("tbl_questions.idquestions=?",$qtnid);
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}

   
	}