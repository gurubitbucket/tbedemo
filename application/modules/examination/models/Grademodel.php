<?php
     class Examination_Model_Grademodel extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	
	        protected $_name = 'tbl_groupnames';
	
            //function to add the template
            public function fnaddgrade($post)
	        {	 
	       	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_grademaster";
		$postData = array(		
							'gradename' =>$post['gradename'],	
           					'Gradedescription' => $post['gradedescription'], 
							'percentagefrom' =>$post['Marksfrom'],	
							'percentageto'=>$post['Marksto'],	
		  					'effectivdate'=>$post['Effectidate'],
		  					'Active'=>'1',
							'Upduser'=>$post['Upduser'],
		  					'Upddate'=>$post['Upddate']
		                      
						);
						$result=  $lobjDbAdpt->insert($table,$postData);
	
						   return $result;
	        	
	        	
	        	
	        }
	        
	        public function fninactiveprasentgarde($gradename)
	        {
	    /*    	$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(
							'Active'=>'0',
						);
		//$where['idgrademaster =?']= $gradename;	
		$where = "idgrademaster = '".$gradename."'"; 
	//	print_r($postData);
		// echo ($where);die();
		 $db->update('tbl_grademaster', $postData, $where);
		 */
		 
		 $db = Zend_Db_Table::getDefaultAdapter();
    	  
					$postData1 = array(
							'Active'=>'0',
						);
	 $where1 = "gradename='".$gradename."'"; 
	     $db->update('tbl_grademaster',$postData1,$where1);
		 
	        
	        }
	        
	        public function fncheckgrade($gradename)
	        {
	        	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_grademaster"),array("a.gradename"))
			                ->where("a.gradename='$gradename'");
			            
			           //  echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchRow($lstrSelect);
		     return $result;
	        	
	        	
	        }
	        
         public function fncheckstartgarde($gradename)
	        {
	        	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_grademaster"),array("a.percentagefrom","a.percentageto","a.gradename"))
			                ->where("a.gradename='$gradename'")
			                ->where("a.percentagefrom=0")
			                ->where("a.Active=1");
			            
			           // echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchRow($lstrSelect);
		     return $result;
	        	
	        	
	        }
	        
	        
	        
         public function fncheckendgarde($gradename)
	        {
	        	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_grademaster"),array("a.percentagefrom","a.percentageto","a.gradename"))
			                ->where("a.gradename='$gradename'")
			                ->where("a.percentageto=100")
			                ->where("a.Active=1");
			            
			           // echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchRow($lstrSelect);
		     return $result;
	        	
	        	
	        }
	        
	    	 public function fngetallgarde()
	        {
	        	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_grademaster"),array("a.*"))
			                ->where("a.Active=1")
			                ->order("a.gradename desc");
			            
			           //  echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchAll($lstrSelect);
		     return $result;
	        	
	        	
	        }
	        
	    
         public function fncheckgarderange($gradename)
	        {
	        	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_grademaster"),array("a.percentagefrom","a.percentageto"))
			                ->where("a.gradename!='$gradename'")
			                ->where("a.percentagefrom > (select percentageto from tbl_grademaster where a.gradename='$gradename' and active=1 )");
			            
			           // echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchRow($lstrSelect);
		     return $result;
	        	
	        	
	        }
	        
	        //Function to get the webpagetemplate details 	
            public function fngetgradelist()
            {             
   	           	    	  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_grademaster"),array("a.*","DATE_FORMAT(effectivdate,'%d-%m-%Y') as effectivdate"))
			                ->where("Active=1")
			                ->order("a.gradename desc");
			            
			           //  echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchAll($lstrSelect);
		     return $result;
            }
 	
            //Function for searching the author details	
           public function fnSearchgrade($post = array())
	       { 
	       		 $field7 = "Active = ".$post["field7"];	 
	       	    	  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_grademaster"),array("a.*","DATE_FORMAT(effectivdate,'%d-%m-%Y') as effectivdate"))
			                ->where('a.gradename  like "%" ? "%"',$post['field2'])
			                   ->where($field7)
			                     ->order("a.gradename desc"); 
			           //  echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchAll($lstrSelect);
		     return $result;
	       }
	       
	        public function fneditgrade($id)     
			{		      		
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect =$lobjDbAdpt->select()
									->from(array('a'=>'tbl_grademaster'),array('a.*'))
									->where('idgrademaster='.$id );
								//	echo $lstrSelect;die();
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
			} 
	       
			public function fnupdategrade($id,$udata) 
            {             
	        	$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(
							'Active'=>$udata['Active'],
							'Gradedescription'=>$udata['gradedescription'],
		'Upduser'=>$post['Upduser'],
		  					'Upddate'=>$post['Upddate']
						);
		$where['idgrademaster = ? ']= $id;		
		 $db->update('tbl_grademaster', $postData, $where);
            }

       public function getactivesets(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_tosmaster'),array('a.IdTOS'))
								->where("a.Active=1");
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}
		
		public function fnGetvalidategarde($gradname,$gradval){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_grademaster'),array('a.gradename'))
								->where("a.percentagefrom>=?",$gradval)
										->where("a.percentageto<=?",$gradval)
								->where("a.Active=1");
				$result = $lobjDbAdpt->fetchRow($select);	
				return $result;
			
		}
		
         public function gettossection($values,$groupname){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_tosdetail'),array('a.*'))
								->where("a.IdTOS in ($values)")
								->where('a.IdSection  like ? "-%"',$groupname);
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}
		
        public function getgroupsection($idgroupname){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_questions'),array('a.QuestionGroup'))
								->where("a.QuestionType=?",$idgroupname)
								->where("a.Active=1");
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}
		
	         //Function To Get Pagination Count from Initial Config
	       public function fnGetPaginationCountFromInitialConfig()
	       {
		       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		       $lintPageCount = "";
		       $lstrSelect = $lobjDbAdpt->select()
								        ->from(array("a"=>"tbl_config"),array("noofrowsingrid") );
		       $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		       if($larrResult['noofrowsingrid'] == "" || $larrResult['noofrowsingrid'] == "0")
		       {
			    $lintPageCount = "5";
		       }
		       else
		       {
			    $lintPageCount = $larrResult['noofrowsingrid'];
		       }
		       return $lintPageCount;
	       }
	
	       //Function to Get Initial Config Data
	       public function fnGetInitialConfigData()
	       {
		    $lobjDbAdpt=Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect=$lobjDbAdpt->select()
								   ->from(array("a"=>"tbl_definationms"),array("LCASE(SUBSTRING(a.DefinitionCode,1,2)) AS Language") )
					 			   ->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType',array())
				 				   ->join(array("c"=>"tbl_config"),'c.Language = a.idDefinition',array("c.HtmlDir","c.DefaultCountry"));		  
		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		    return $larrResult;
	       }	
        }    
		  	 
	
