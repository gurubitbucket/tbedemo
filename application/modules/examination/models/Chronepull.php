<?php
	class Examination_Model_Chronepull extends Zend_Db_Table {


	public function  fngetvenuedateschedule($iddate)
       {
       			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array())
							 			  ->join(array("b"=>"tbl_managesession"),'a.idsession=b.idmangesession',array("key"=>"b.idmangesession","value"=>"b.managesessionname"))
							 			  ->where("a.date='$iddate'")
										  ->where("a.Active = 1")
										    ->where("a.Reserveflag= 1")
										     ->group("b.idmangesession")
										     ->order("b.managesessionname");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       }

      
    public function fngetallvenues($larrformdata)
    {
    	$iddate = $larrformdata['Date'];
    	$session = $larrformdata['Session'];
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			  ->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("b.*"))
							 			  ->join(array("c"=>"tbl_managesession"),'a.idsession=c.idmangesession',array("c.*"))
							 			  ->where("a.date='$iddate'")
							 			  ->where("a.idsession='$session'")
							 			  ->where("b.ipaddress=1")
							 			    ->where("a.Allotedseats>0")
							 			  ->group("b.idcenter")
							 			  ->order("b.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
    
    public function fngetallsessions()
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("b"=>"tbl_managesession"),array("key"=>"b.idmangesession","value"=>"b.managesessionname"))
										;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
    
    public function fninsertchronepull($larrformData,$larrresultlastid)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$date = $larrformData['Date'];
    	$Session = $larrformData['Session'];
    	$cnt = count($larrformData['idcenter']);
    
    	for($i=0;$i<$cnt;$i++)
    	{
    		
    		
    		$idcenter = $larrformData['idcenter'][$i];
    		
    		$table = "tbl_chronepull";
           $postData = array(		
							'Venue' => $idcenter,		
            				'Examdate' =>$date,
           					'Session'=>$Session,
           					'idautochronepull' => $larrresultlastid		
						);			
	        $lobjDbAdpt->insert($table,$postData);
    	}
    }
    
    
	 public function fninsertschedulerdetails($larrformdata)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$cnt = count($larrformdata['idcenter']);
    $interval=0;

    	for($i=0;$i<$cnt;$i++)
    	{			   	
			   	$schedulerdate = $larrformdata['Schedulerdate'];
			   	$schedulertime = $larrformdata['ScheduleStartTime'];
			   	$schedulerdattime = $schedulerdate.' '.$schedulertime;
			   
			   	$interval = $interval+1;
			   	
			   	$queryoftime = "SELECT DATE_ADD('$schedulerdattime', INTERVAL '$interval:0' MINUTE_SECOND) as Timeintervalss";
			   	$larresulttime = $lobjDbAdpt->fetchRow($queryoftime);
			   	$larrscheduertime = $larresulttime['Timeintervalss'];
			   	$larraddedtime = explode(' ',$larrscheduertime);
			   	$addeddate = $larraddedtime[0];
			   	$addedtime = $larraddedtime[1];
			   	
			   	
		    $postdata = array('schdeduleddate'=>$addeddate,
		                      'scheduledtime'=>$addedtime,
		                      'UpdDate' => $larrformdata['UpdDate'],
		    				  'Session'=>$larrformdata['Session'],
				'Automated'=>1,
		                      'UpdUser' => $larrformdata['UpdUser']);
		    $lobjDbAdpt->insert('tbl_autochronepull',$postdata);
		    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
		    
			$idcenter = $larrformdata['idcenter'][$i];
    		
    		$table = "tbl_chronepull";
            $postData = array(		
							'Venue' => $idcenter,		
            				'Examdate' =>$larrformdata['Date'],
           					'Session'=>$larrformdata['Session'],
           					'idautochronepull' => $lintlastinsid		
						);			
	        $lobjDbAdpt->insert($table,$postData);
    	}   
    }
    
		public function fngetscheduledetails($shcdate,$shctime)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect =  "SELECT a.idautochronepull,a.scheduledtime,a.schdeduleddate
			   				FROM tbl_autochronepull a
			   				WHERE a.scheduledtime = '$shctime' and a.schdeduleddate = '$shcdate'";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		/*
	 * function for fetching all the venues where questions has to be pulled for the particular automail
	 */
	public function fngetallvenuesforidautomail($idautomail)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_chronepull"),array("a.*"))
							 			  ->join(array("b"=>"tbl_autochronepull"),'a.idautochronepull=b.idautochronepull',array("b.*"))
							 			  ->where("a.idautochronepull in ($idautomail)");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
	}
	
	public function fngettempxlsdetails($idapplication)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a"=>"tbl_studentapplication"),array("a.*"))
										->where("a.IDApplication=?",$idapplication);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	public function fnupdatestudent($larresult)
    { 
       	$db = Zend_Db_Table::getDefaultAdapter();
       	$cnt = count($larresult);
       	$IDApplication=0;
       	
 		for($i=0;$i<$cnt;$i++)
       	{
       		$passs = $larresult[$i]['pass'];
       		
       	     $where = "IDApplication = '".$larresult[$i]['IDApplication']."' AND Program ='".$larresult[$i]['Program']."'
       	                 AND DateTime ='".$larresult[$i]['DateTime']."'
       	                 AND ICNO ='".$larresult[$i]['ICNO']."'
       	                 AND VenueTime ='1'
       	                 AND Examsession ='".$larresult[$i]['Examsession']."'"; 
              $larrupdatedata = array(		
							'pass' => $passs,
                            'VenueTime' =>2																			
						);
				$tablevenuedate = "tbl_studentapplication"; 
	           $resultss =  $db->update($tablevenuedate,$larrupdatedata,$where);
	          /* print_R($resultss);
	           die();*/
	             if($resultss==1)
	             {
	             	 $where1 = "idapplication = '".$larresult[$i]['IDApplication']."'";
	                 $larrupdatedatas = array(		
												'Cetreapproval' => $larresult[$i]['Cetreapproval'],
						                        'IdBatch' => $larresult[$i]['IdBatch']															
											);
				    $tableregdetails = "tbl_registereddetails"; 
	                $db->update($tableregdetails,$larrupdatedatas,$where1);
	            
	            
	            
						   $tablestudentmarks = "tbl_studentmarks";
		                   $studentmarksdetails = array(	
							           					'IDApplication' =>$larresult[$i]['IDApplication'], 
														'NoofQtns' =>$larresult[$i]['NoofQtns'],	
							           					'Attended' =>$larresult[$i]['Attended'],
									                 	'Grade' =>$larresult[$i]['Grade'],	
							           					'Correct' =>$larresult[$i]['Correct']	  
													);
						  $db->insert($tablestudentmarks,$studentmarksdetails);
						  
						  
						    $tablestudentpartwisemarks = "tbl_studentdetailspartwisemarks";
		                    $studentpartwisemarksdetails = array(	
								           					'IDApplication' =>$larresult[$i]['IDApplication'], 
															'Regid' =>$larresult[$i]['Regid'],	
								           					'A' =>$larresult[$i]['A'],
										                 	'B' =>$larresult[$i]['B'],
										                   	'C' =>$larresult[$i]['C'],	
								           					'UpdDate' =>$larresult[$i]['UpdDate']	  
														);
						  $db->insert($tablestudentpartwisemarks,$studentpartwisemarksdetails);
						  
						  
						  
	               
						$larstudentstartexamdetails = self::fngetstartexamdetails($larresult[$i]['IDApplication']);
						
						  if(count($larstudentstartexamdetails)>0)
						  {
								 $where1 = 'idapplication = '.$larresult[$i]['IDApplication'];	  
									   $larrupdatedatas = array(		
																'Endtime' => $larresult[$i]['Endtime'],
															    'Submittedby' => $larresult[$i]['Submittedby'],
															    'StudentIpAddress' =>$larresult[$i]['StudentIpAddress'],		
									           					'UpdDate' =>$larresult[$i]['UpdDate']															
																);
							     $tablestartexamdetails = "tbl_studentstartexamdetails"; 
				                 $db->update($tablestartexamdetails,$larrupdatedatas,$where1);
	            
						  }
						  else 
						  {
						  $tablestudentstartexam = "tbl_studentstartexamdetails";
		                  $studentstartexam = array(	
						           					'IDApplication' =>$larresult[$i]['IDApplication'], 
													'Starttime' =>$larresult[$i]['Starttime'],	
						           					'Endtime' =>$larresult[$i]['Endtime'],
								                 	'Submittedby' =>$larresult[$i]['Submittedby'],
								                  	'StudentIpAddress' =>$larresult[$i]['StudentIpAddress'],		
						           					'UpdDate' =>$larresult[$i]['UpdDate']	  
												);
						  $db->insert($tablestudentstartexam,$studentstartexam);  
						  }
						  
						  
						   $tablequestionsetforstudents = "tbl_questionsetforstudents";
						  $questionsetforstudentsdetails = array(	
										           					'IDApplication' =>$larresult[$i]['IDApplication'], 
																	'Program' =>$larresult[$i]['Program'],	
										           					'IdBatch' =>$larresult[$i]['IdBatch'],
												                 	'idquestions' =>$larresult[$i]['idquestions']
												                );
						 $db->insert($tablequestionsetforstudents,$questionsetforstudentsdetails);
						  
	            
	             }
	             else 
	             {

			     		$larresulttempdata = self::fngettempxlsdetails($larresult[$i]['IDApplication']);
			     		if(count($larresulttempdata)<5)
			     		{
			     	//$db = Zend_Db_Table::getDefaultAdapter();
				  $table = "tbl_logxlstudentapplication";
					$postData = array(	
					                     'IDApplication' =>$larresult[$i]['IDApplication'],	
										'StudentId' =>$larresult[$i]['StudentId'],	
			           					'FName' =>$larresult[$i]['FName'],	
			           					'MName' =>$larresult[$i]['MName'], 
										'LName' =>$larresult[$i]['LName'],	
			           					'DateOfBirth' =>$larresult[$i]['DateOfBirth'],	
			           					'PermCity' =>$larresult[$i]['PermCity'], 
			           					'EmailAddress' =>$larresult[$i]['EmailAddress'],
								        'username' =>$larresult[$i]['username'],	
			           					'UpdDate' => $larresult[$i]['UpdDate'], 
										'UpdUser' =>$larresult[$i]['UpdUser'],	
			           					'IdBatch' =>$larresult[$i]['IdBatch'],	
			           					'Venue' => $larresult[$i]['Venue'], 
										'VenueTime' =>$larresult[$i]['VenueTime'],	
			           					'Program' =>$larresult[$i]['Program'],	
			           					'idschedulermaster' =>$larresult[$i]['idschedulermaster'], 
										'Amount' =>$larresult[$i]['Amount'],	
			           					'ICNO' =>$larresult[$i]['ICNO'],	
								        'password' =>$larresult[$i]['password'],	
			           					'Payment' =>$larresult[$i]['Payment'], 
										'DateTime'=>$larresult[$i]['DateTime'],	
			           					'PermAddressDetails' =>$larresult[$i]['PermAddressDetails'],	
			           					'Takafuloperator' =>$larresult[$i]['Takafuloperator'], 
										'VenueChange' =>$larresult[$i]['VenueChange'],	
			           					'ArmyNo' =>$larresult[$i]['ArmyNo'],	
			           					'batchpayment' =>$larresult[$i]['batchpayment'], 
										'Gender' =>$larresult[$i]['Gender'],	
			           					'Race' =>$larresult[$i]['Race'],	
			           					'Qualification' =>$larresult[$i]['Qualification'], 
										'State' =>$larresult[$i]['State'],	
			           					'CorrAddress' =>$larresult[$i]['CorrAddress'],	
			           					'PostalCode' =>$larresult[$i]['PostalCode'], 
					 					'ContactNo' =>$larresult[$i]['ContactNo'],	
			           					'MobileNo' =>$larresult[$i]['MobileNo'], 
					  					'ExamState'=>$larresult[$i]['ExamState'],
					  					'ExamCity'=>$larresult[$i]['ExamCity'],
					  					'Year'=>$larresult[$i]['Year'],
					  					'Examdate'=>$larresult[$i]['Examdate'],
					  					'Exammonth'=>$larresult[$i]['Exammonth'],
					  					'Examvenue'=>$larresult[$i]['Examvenue'],	
					  					'Examsession'=>$larresult[$i]['Examsession'],
					  					'Pass'=>$larresult[$i]['pass'],
										 'Religion'=>$larresult[$i]['Religion'],
										'Pushedfrom'=>2		  
									);
								$db->insert($table,$postData);	
							
	            
	            
	            
						   $tablestudentmarks = "tbl_logxlstudentmarks";
		                  $studentmarksdetails = array(	
           					'IDApplication' =>$larresult[$i]['IDApplication'], 
							'NoofQtns' =>$larresult[$i]['NoofQtns'],	
           					'Attended' =>$larresult[$i]['Attended'],
		                 	'Grade' =>$larresult[$i]['Grade'],	
           					'Correct' =>$larresult[$i]['Correct']	  
						);
						  $db->insert($tablestudentmarks,$studentmarksdetails);
						  
						  
						    $tablestudentpartwisemarks = "tbl_logxlstudentdetailspartwisemarks";
		                  $studentpartwisemarksdetails = array(	
           					'IDApplication' =>$larresult[$i]['IDApplication'], 
							'Regid' =>$larresult[$i]['Regid'],	
           					'A' =>$larresult[$i]['A'],
		                 	'B' =>$larresult[$i]['B'],
		                   	'C' =>$larresult[$i]['C'],	
           					'UpdDate' =>$larresult[$i]['UpdDate']	  
						);
						  $db->insert($tablestudentpartwisemarks,$studentpartwisemarksdetails);
						  
						  $tablestudentstartexam = "tbl_logxlstudentstartexamdetails";
				                  $studentstartexam = array(	
								           					'IDApplication' =>$larresult[$i]['IDApplication'], 
															'Starttime' =>$larresult[$i]['Starttime'],	
								           					'Endtime' =>$larresult[$i]['Endtime'],
										                 	'Submittedby' =>$larresult[$i]['Submittedby'],
										                  	'StudentIpAddress' =>$larresult[$i]['StudentIpAddress'],		
								           					'UpdDate' =>$larresult[$i]['UpdDate']	  
															);
								  $db->insert($tablestudentstartexam,$studentstartexam);  
						
	            
						  
						  $tablequestionsetforstudents = "tbl_logxlquestionsetforstudents";
						  $questionsetforstudentsdetails = array(	
				           					'IDApplication' =>$larresult[$i]['IDApplication'], 
											'Program' =>$larresult[$i]['Program'],	
				           					'IdBatch' =>$larresult[$i]['IdBatch'],
						                 	'idquestions' =>$larresult[$i]['idquestions']
						                );
						$db->insert($tablequestionsetforstudents,$questionsetforstudentsdetails);
	
	               }
	             }
       	}	
       	
       	return $IDApplication;
       	
	}
	
	public function fngetcenterstartfromserver($Venue,$Session,$Examdate)
    {
    		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db ->select()
			   			->from(array("a" =>"tbl_centerstartexam"),array("a.*"))
								 ->where("a.idcenter=?",$Venue)
								 ->where("a.ExamDate=?",$Examdate)
								 ->where("a.idSession=?",$Session);
		$larrResult = $db->fetchAll($selectData);
		return $larrResult;
    }
	
	public function fninsertanswerdetailsinserver($larranswerdetails)
    { 
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$select = "insert into tbl_answerdetails (Regid, QuestionNo,Answer) values";
    	for($lvarm=0;$lvarm<count($larranswerdetails);$lvarm++){
    		if($lvarm==0)
    		{
    			$select .= "('".$larranswerdetails[$lvarm]['Regid']."',".$larranswerdetails[$lvarm]['QuestionNo'].",".$larranswerdetails[$lvarm]['Answer'].")";
    		}
    		else 
    		{
    		$select .= ",('".$larranswerdetails[$lvarm]['Regid']."',".$larranswerdetails[$lvarm]['QuestionNo'].",".$larranswerdetails[$lvarm]['Answer'].")";
    		}
    	}
        $larrResult = $db->query($select);
    	
    }
    
    
	public function fngetstartexamdetails($idappliation)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectData = $db ->select()
			   			->from(array("a" =>"tbl_studentstartexamdetails"),array("a.*"))
			   			->where("a.idapplication=?",$idappliation);
		$larrResult = $db->fetchAll($selectData);
		return $larrResult;
	}	
	
	
	public function fnupdatestudentabsent($larresult)
{
	
	 	$db = Zend_Db_Table::getDefaultAdapter();
       	$cnt = count($larresult);
       
       	$IDApplication=0;
       	for($i=0;$i<$cnt;$i++)
       	{
       		
       	    $where = "IDApplication = '".$larresult[$i]['IDApplication']."'";
              $larrupdatedata = array(		
							'pass' => $larresult[$i]['pass'],
                            'VenueTime' =>2														
						);
				$tablevenuedate = "tbl_studentapplication"; 
	            $db->update($tablevenuedate,$larrupdatedata,$where);
	            
	            
	            
                  $where1 = "IDApplication = '".$larresult[$i]['IDApplication']."'";
	              $larrupdatedatas = array(		
							'Cetreapproval' => $larresult[$i]['Cetreapproval'],
	                        'IdBatch' => $larresult[$i]['IdBatch']															
						);
				  $tableregdetails = "tbl_registereddetails"; 
	              $db->update($tableregdetails,$larrupdatedatas,$where1);
	             
       	}
}


		public function fnupdateclosetimeserver($larresult)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
       	$cnt = count($larresult);
       	for($i=0;$i<1;$i++)
       	{
       		$tablestartexamdetails = "tbl_centerstartexam"; 
			 $where1 = "idcenter = '".$larresult[$i]['idcenter']."'  AND ExamDate ='".$larresult[$i]['ExamDate']."' AND idSession ='".$larresult[$i]['idSession']."'"; 	  
			 //echo $where1;die();
						   $larrupdatedatas = array(		
							'CloseTime' => $larresult[$i]['CloseTime']
						);
				
	         $db->update($tablestartexamdetails,$larrupdatedatas,$where1);
       	}
	}
	
	
		public function fninsertexamcentermainserver($larresult)
  {
       	$db = Zend_Db_Table::getDefaultAdapter();
       	$cnt = count($larresult);
       	for($i=0;$i<$cnt;$i++)
       	{
       		   $table = "tbl_centerstartexam";
               $postData = array(		
							'idcenter' => $larresult[$i]['idcenter'],	
           					'sessionid' =>$larresult[$i]['sessionid'],
                            'Program'=>$larresult[$i]['Program'],
            				'Totaltime'=>$larresult[$i]['Totaltime'],
                            'Startedtime'=>$larresult[$i]['Startedtime'],
          					'idSession'=>$larresult[$i]['idSession'],
          					'ExamDate'=>$larresult[$i]['ExamDate'],
          					'ExamStartTime'=>$larresult[$i]['ExamStartTime'],
          					'Date'=>$larresult[$i]['Date'],
              				 'CloseTime'=>$larresult[$i]['CloseTime']
            																
						);
	           $db->insert($table,$postData);
						 
       	}
  }
  
      	public function fnupdatedchronedstatussucess($idchronequestions)
	    {
	    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    		$tablestartexamdetails = "tbl_chronepull"; 
				 $where1 = "idchronepull = '".$idchronequestions."'"; 	  
							   $larrupdatedatas = array(		
								'Status' => 1
							);
					
		         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
	    } 
    
    
	 public function fngetstatusofvenue($date,$idvenue,$idsession)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_chronepull"),array("a.*"))
							 			  ->where("a.Examdate='$date'")
							 			   ->where("a.Session='$idsession'")
							 			   ->where("a.Venue='$idvenue'");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
    }
    
	public function fngetstatusforthevenue($idchronepull)
	{
		
	}
   
	}