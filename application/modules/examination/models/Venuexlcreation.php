<?php
class Examination_Model_Venuexlcreation extends Zend_Db_Table {		
	protected $_name = 'tbl_studentapplication';			
	public function fnGetSchedulerSessionDetails(){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt ->select()
								  ->from(array("a" => "tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
							      ->where("a.active 	 = 1");	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}			
	public function fnSearchStudent($larrformData) {
		//echo "<pre>";print_r($larrformData);die();
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
								 ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.ProgramName'))
								 ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.centername'))
								 ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
								 ->join(array("e"=>"tbl_managesession"),'a.Examsession=e.idmangesession',array('e.managesessionname'))
								 ->where("a.Payment=1")
								 ->where("a.VenueTime = 0")
								 ->where("a.pass = 3 ")
								 ->where("a.IDApplication>1148")
								 ->where("a.Examvenue!=000")
								 ->group("d.IDApplication")
								 ->order("a.FName");	
										  
		if($larrformData['Studentname']) $lstrSelect->where("a.Fname like '%' ? '%'",$larrformData['Studentname']);	
		if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$larrformData['ICNO']);					  
		if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$larrformData['Coursename']);
		if($larrformData['Venues']) $lstrSelect->where("c.idcenter = ?",$larrformData['Venues']);
		if($larrformData['examsession']) $lstrSelect->where("a.Examsession = ?",$larrformData['examsession']);
		if(isset($larrformData['Date']) && !empty($larrformData['Date'])){
			  $lstrFromDate = $larrformData['Date'];				
			  $lstrSelect = $lstrSelect->where("a.DateTime = '$lstrFromDate'");
		}	     		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);				
		return $larrResult;
    }
	public function fngetStudentsdata($larrformData) {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
								 ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
								 ->where("a.VenueTime = ?",0)
								 ->where("a.IDApplication IN (? ",$larrformData['IDApplication']);								
		$lstrSelect =  $lstrSelect.")";		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);				
		return $larrResult;
     }
	public function fngetotherdata($idprgm,$idcenter,$idsession) {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("b" =>"tbl_programmaster"),array('b.ProgramName'))
								 ->joinLeft(array("c"=>"tbl_center"),'c.idcenter = '.$idcenter,array('c.centername'))
								 ->joinLeft(array("e"=>"tbl_managesession"),'e.idmangesession = '.$idsession,array('e.managesessionname'))
								 ->where("b.IdProgrammaster = ".$idprgm);	
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);				
		return $larrResult;
     }
	public function fnReportSearchDetails($larrformData,$lvarVenues) {		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.FName as Studentname","a.IDApplication","DATE_FORMAT(a.DateTime,'%d-%m-%Y') as ExamDate","a.DateTime as exdate","a.Payment","a.pass","count(a.IDApplication) as Registered"))
							 ->join(array("e" => "tbl_centerstartexam"),'a.Examvenue = e.idcenter AND a.Examsession = e.idSession AND a.Program = e.Program AND a.DateTime = e.ExamDate',array("e.ExamStartTime","e.CloseTime","e.AutoSubmitCloseTime"))
							 ->joinLeft(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.idcenter","b.centername"))
							 ->joinLeft(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName","c.IdProgrammaster"))
							 ->joinLeft(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.idmangesession","d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Payment = 1")
							 ->where("a.VenueTime = 2");			

		if($larrformData['Coursename'])  $lstrSelect->where("c.IdProgrammaster = ?",$larrformData['Coursename']);
		if($lvarVenues) 				 $lstrSelect->where("b.idcenter = ?",$lvarVenues);
		if($larrformData['examsession']) $lstrSelect->where("a.Examsession = ?",$larrformData['examsession']);
		if(isset($larrformData['Date']) && !empty($larrformData['Date'])){
			  $lstrFromDate = $larrformData['Date'];				
			  $lstrSelect 	= $lstrSelect->where("a.DateTime = '$lstrFromDate'");
		}	       
				$lstrSelect  ->group("b.idcenter")
							 ->group("c.IdProgrammaster")
							 ->group("d.idmangesession")
							 ->group("a.DateTime")
							 ->order("b.centername")
							 ->order("c.IdProgrammaster")
							 ->order("d.starttime");
					
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			    
				return $larrResult;
     }
	public function fnReportDownloadedSearchDetails($larrformData,$lvarVenues) {		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.FName as Studentname","a.IDApplication","DATE_FORMAT(a.DateTime,'%d-%m-%Y') as ExamDate","a.DateTime as exdate","a.Payment","a.pass","count(a.IDApplication) as Registered"))
							 ->join(array("e" => "tbl_tempxlcenterstartexam"),'a.Examvenue = e.idcenter AND a.Examsession = e.idSession AND a.Program = e.Program AND a.DateTime = e.ExamDate',array("e.ExamStartTime","e.CloseTime","e.AutoSubmitCloseTime"))
							 ->joinLeft(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.idcenter","b.centername"))
							 ->joinLeft(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName","c.IdProgrammaster"))
							 ->joinLeft(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.idmangesession","d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Payment = 1");
							 //->where("a.VenueTime = 1");			

		if($larrformData['Coursename'])  $lstrSelect->where("c.IdProgrammaster = ?",$larrformData['Coursename']);
		if($lvarVenues) 				 $lstrSelect->where("b.idcenter = ?",$lvarVenues);
		if($larrformData['examsession']) $lstrSelect->where("a.Examsession = ?",$larrformData['examsession']);
		if(isset($larrformData['Date']) && !empty($larrformData['Date'])){
			  $lstrFromDate = $larrformData['Date'];				
			  $lstrSelect 	= $lstrSelect->where("a.DateTime = '$lstrFromDate'");
		}	       
				$lstrSelect  -> group("b.idcenter")
							 ->group("c.IdProgrammaster")
							 ->group("d.idmangesession")
							 ->group("a.DateTime")
							 ->order("b.centername")
							 ->order("c.IdProgrammaster")
							 ->order("d.starttime");
						 
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     public function fnReportDataDetails($larrformDate){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication"))
							 ->join(array("b" => "tbl_studentmarks"),"b.IDApplication = a.IDApplication",array("b.*")) 
							 ->joinLeft(array("c" => "tbl_studentdetailspartwisemarks"),"c.IDApplication = a.IDApplication",array("c.*")) 
							 ->joinLeft(array("d" => "tbl_studentstartexamdetails"),"d.IDApplication = a.IDApplication",array("d.*")) 
							 ->where("a.DateTime = '$larrformDate'");		
     	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
  
     public  function fninsertdata($totalarray,$sessionID){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table0 = "tbl_tempxlcenterstartexam";
    	$table1 = "tbl_tempxlstudentmarks";
    	$table2 = "tbl_tempxlstudentdetailspartwisemarks";
    	$table3 = "tbl_tempxlstudentstartexamdetails";
    	$table4 = "tbl_tempxlanswerdetails"; 
    	$table5 = "tbl_tempxlstudentapplication"; 
    	$table6 = "tbl_tempxlquestionsetforstudents"; 

    
    	
		    	
    	for($lvarcnti=6;$lvarcnti<($totalarray->CenterCount+6);$lvarcnti++){
    		$dats =  'Row_'.($lvarcnti-5);
    		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_tempxlcenterstartexam"),array('a.idcenter'))
								 ->where('idcenter = ?',$totalarray->center->$dats->idcenter)
								 ->where('Program = ?',$totalarray->center->$dats->Program)
								 ->where('ExamDate = ?',$totalarray->center->$dats->ExamDate)
								 ->where('idSession = ?',$totalarray->center->$dats->idSession); 								 
			$larrRowResult = $lobjDbAdpt->fetchAll($lstrSelect);
			if(count($larrRowResult) == 0){	
	    		$postData0 = array(								
								'idcenter'=>$totalarray->center->$dats->idcenter,					
			 					'sessionid' =>$totalarray->center->$dats->sessionid,
	           					'Program' =>$totalarray->center->$dats->Program,
	           					'Totaltime' =>$totalarray->center->$dats->Totaltime,	
	           					'Startedtime' => $totalarray->center->$dats->Startedtime,
								'Date' =>$totalarray->center->$dats->Date,
	    						'idSession' =>$totalarray->center->$dats->idSession,
	           					'ExamDate' =>$totalarray->center->$dats->ExamDate,	
	           					'ExamStartTime' => $totalarray->center->$dats->ExamStartTime,
								'CloseTime' =>$totalarray->center->$dats->CloseTime,
	    						'AutoSubmitCloseTime' =>$totalarray->center->$dats->AutoSubmitCloseTime,
	    						'UserSession' =>$sessionID
							);						
				$lobjDbAdpt->insert($table0,$postData0);
			}
    	}
			
    	for($lvarcnt=7;$lvarcnt<($totalarray->ResultCount+7);$lvarcnt++){  
    		$dats =  'Data_'.($lvarcnt-6);	
    		//print_r($totalarray->$dats);    exit;		
    		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_tempxlstudentmarks"),array('a.IDApplication'))
								 ->where('idstudentmarks = ?',$totalarray->result->$dats->idstudentmarks); 
			$larrRowResult = $lobjDbAdpt->fetchAll($lstrSelect);
			if(count($larrRowResult) == 0){	 	
				$postData1 = array(
							'idstudentmarks' =>$totalarray->result->$dats->idstudentmarks,
							'IDApplication'=>$totalarray->result->$dats->IDApplication,					
		 					'NoofQtns' =>$totalarray->result->$dats->NoofQtns,
           					'Attended' =>$totalarray->result->$dats->Attended,
           					'Correct' =>$totalarray->result->$dats->Correct,	
           					'Grade' => $totalarray->result->$dats->Grade,
				            'pass' => $totalarray->result->$dats->pass,
							'UserSession' =>$sessionID
						);						
				$lobjDbAdpt->insert($table1,$postData1);
			}
			$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_tempxlstudentdetailspartwisemarks"),array('a.IDApplication'))
								 ->where('idstudentdetailspartwisemarks = ?',$totalarray->result->$dats->idstudentdetailspartwisemarks); 
			$larrRowResult = $lobjDbAdpt->fetchAll($lstrSelect);
			if(count($larrRowResult) == 0){	
				$postData2 = array('idstudentdetailspartwisemarks'=>$totalarray->result->$dats->idstudentdetailspartwisemarks,
							'IDApplication' =>$totalarray->result->$dats->IDApplication,	
		 					'Regid' =>$totalarray->result->$dats->Regid,	
           					'A' =>$totalarray->result->$dats->A,	
           					'B' =>$totalarray->result->$dats->B,		
           					'C' => $totalarray->result->$dats->C,	
							'UpdDate' => $totalarray->result->$dats->UpdDate1,	// str_replace("'","",$totalarray[$lvarcnt][12]),
							'UserSession' =>$sessionID
					);
			    $lobjDbAdpt->insert($table2,$postData2);
			}
		 $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_tempxlstudentstartexamdetails"),array('a.IDApplication'))
								 ->where('idstudentstartexamdetails = ?',$totalarray->result->$dats->idstudentstartexamdetails); 
		 $larrRowResult = $lobjDbAdpt->fetchAll($lstrSelect);
				
    	 if(count($larrRowResult) == 0){	
				$postData3 = array('idstudentstartexamdetails'=>$totalarray->result->$dats->idstudentstartexamdetails,	
							'IDApplication' =>$totalarray->result->$dats->IDApplication,	
		 					'Starttime' =>$totalarray->result->$dats->Starttime,	
           					'Endtime' =>$totalarray->result->$dats->Endtime,	
           					'Submittedby' =>$totalarray->result->$dats->Submittedby,		
           					'UpdDate' => $totalarray->result->$dats->UpdDate2,	 //str_replace("'","",$totalarray[$lvarcnt][12]),
							'StudentIpAddress' =>$totalarray->result->$dats->StudentIpAddress,	
							'UserSession' =>$sessionID
					);
			    $lobjDbAdpt->insert($table3,$postData3);
			}					
    	}
      for($lvarcnti=8;$lvarcnti<($totalarray->AnswerCount+8);$lvarcnti++){
    		    $dats =  'Answer_'.($lvarcnti-7);    		
	    		$postData4 = array(												
			 					'Regid' =>$totalarray->answer->$dats->Regid,
	           					'QuestionNo' =>$totalarray->answer->$dats->QuestionNo,
	           					'Answer' =>$totalarray->answer->$dats->Answer,	           			
	    						'UserSession' =>$sessionID
							);						
				$lobjDbAdpt->insert($table4,$postData4);
		  	}	
       for($lvarcnti=9;$lvarcnti<($totalarray->StudentCount+9);$lvarcnti++){
    		    $dats =  'Student_'.($lvarcnti-8);    		
	    		$postData5 = array(												
			 					'IDApplication' =>$totalarray->student->$dats->IDApplication,
	           					'pass' =>$totalarray->student->$dats->pass,	 
					    		'Regid' =>$totalarray->student->$dats->Regid,	 
					    		'Cetreapproval' =>$totalarray->student->$dats->Cetreapproval,
	    						'StudentId' => $totalarray->student->$dats->StudentId,	  
					    		'username' => $totalarray->student->$dats->username,	   
					    		'password' => $totalarray->student->$dats->password,	   
					    		'FName' => $totalarray->student->$dats->FName,	   
					    		'MName' => $totalarray->student->$dats->MName,	   
					    		'LName' => $totalarray->student->$dats->LName,	   
					    		'DateOfBirth' => $totalarray->student->$dats->DateOfBirth,	   
					    		'PermCity' => $totalarray->student->$dats->PermCity,	   
					    		'EmailAddress' => $totalarray->student->$dats->EmailAddress,	   
					    		'UpdDate' => $totalarray->student->$dats->UpdDate,	   
					    		'UpdUser' => $totalarray->student->$dats->UpdUser,	   
					    		'IdBatch' => $totalarray->student->$dats->IdBatch,	   
					    		'Venue' => $totalarray->student->$dats->Venue,	   
					    		'VenueTime' => $totalarray->student->$dats->VenueTime,	   
					    		'Program' => $totalarray->student->$dats->Program,	   
					    		'idschedulermaster' => $totalarray->student->$dats->idschedulermaster,	   
					    		'Amount' => $totalarray->student->$dats->Amount,	   
					    		'ICNO' => $totalarray->student->$dats->ICNO,	   
					    		'Payment' => $totalarray->student->$dats->Payment,	   
					    		'DateTime' => $totalarray->student->$dats->DateTime,	   
					    		'PermAddressDetails' => $totalarray->student->$dats->PermAddressDetails,	  
					    		'Takafuloperator' => $totalarray->student->$dats->Takafuloperator,	   
					    		'VenueChange' => $totalarray->student->$dats->VenueChange,	   
					    		'ArmyNo' => $totalarray->student->$dats->ArmyNo,	   
					    		'batchpayment' => $totalarray->student->$dats->batchpayment,	   
					    		'Gender' => $totalarray->student->$dats->Gender,	   
					    		'Race' => $totalarray->student->$dats->Race,	   
					    		'Qualification' => $totalarray->student->$dats->Qualification,	 
					    		'State' => $totalarray->student->$dats->State,	   
					    		'CorrAddress' => $totalarray->student->$dats->CorrAddress,	   
					    		'PostalCode' => $totalarray->student->$dats->PostalCode,	   
					    		'ContactNo' => $totalarray->student->$dats->ContactNo,	   
					    		'MobileNo' => $totalarray->student->$dats->MobileNo,	   
					    		'ExamState' => $totalarray->student->$dats->ExamState,	   
					    		'ExamCity' => $totalarray->student->$dats->ExamCity,	   
					    		'Year' => $totalarray->student->$dats->Year,	   
					    		'Examdate' => $totalarray->student->$dats->Examdate,	   
					    		'Exammonth' => $totalarray->student->$dats->Exammonth,	   
					    		'Examvenue' => $totalarray->student->$dats->Examvenue,	   
					    		'Examsession' => $totalarray->student->$dats->Examsession,	   
					    		'Religion' => $totalarray->student->$dats->Religion,	              						           			
	    						'UserSession' =>$sessionID	 
	    		);						
				$lobjDbAdpt->insert($table5,$postData5);
		  	}	
      for($lvarcnti=11;$lvarcnti<($totalarray->QuestionCount+11);$lvarcnti++){
    		    $dats =  'Question_'.($lvarcnti-10);    		
	    		$postData6 = array(												
			 					'idquestionsetforstudents' => $totalarray->question->$dats->idquestionsetforstudents,
	           					'IDApplication' => $totalarray->question->$dats->IDApplication,	 
					    		'Program' => $totalarray->question->$dats->Program,	 
					    		'IdBatch' => $totalarray->question->$dats->IdBatch,	
	    						'idquestions' => $totalarray->question->$dats->idquestions,           						           			
	    						'UserSession' => $sessionID
							);						
				$lobjDbAdpt->insert($table6,$postData6);
		  	}		
	}
	public function fnDeletetempdetails($sessionID){
    	$db 	= Zend_Db_Table::getDefaultAdapter();    	
    	$where  = $db->quoteInto('UserSession = ?',$sessionID);
    	$db->delete('tbl_tempxlcenterstartexam',$where);
 		$db->delete('tbl_tempxlstudentmarks',$where);
 		$db->delete('tbl_tempxlstudentdetailspartwisemarks',$where);
 		$db->delete('tbl_tempxlstudentstartexamdetails',$where);
 		$db->delete('tbl_tempxlanswerdetails',$where);
 		$db->delete('tbl_tempxlstudentapplication',$where);
 		$db->delete('tbl_tempxlquestionsetforstudents',$where);
    }	
	public function fninsertMaindetails($sessionID){
    	$db 	= Zend_Db_Table::getDefaultAdapter(); 
    	$table0 = 'tbl_tempxlcenterstartexam';
    	$table1 = 'tbl_tempxlstudentmarks';
    	$table2 = 'tbl_tempxlstudentdetailspartwisemarks';    	
    	$table3 = 'tbl_tempxlstudentstartexamdetails'; 
    	$table4 = 'tbl_tempxlanswerdetails';
    	$table5 = 'tbl_tempxlstudentapplication';
    	$table6 = 'tbl_tempxlquestionsetforstudents';
    	
    	
    	
    	
    	$larrRowResult = array();
    	$larrlogArray  = array();
		$lstrSelect = $db->select()
						 ->from(array("a" =>$table5),array('a.*'))							 
						 ->where('UserSession = ?',$sessionID); 
								 
		$larrRowResult = $db->fetchAll($lstrSelect);
		for($lavri=0;$lavri<count($larrRowResult);$lavri++){
				$lstrSel = $db->select()
								 ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
								 ->where('a.IDApplication = ?',$larrRowResult[$lavri]['IDApplication']); 
			$larrRowRes = $db->fetchAll($lstrSel);
			
			if($larrRowRes[0]['VenueTime'] == 1 && $larrRowRes[0]['pass'] == 3 &&  $larrRowResult[$lavri]['Program'] == $larrRowRes[0]['Program'] && $larrRowResult[$lavri]['ICNO'] == $larrRowRes[0]['ICNO'] && $larrRowResult[$lavri]['DateTime'] == $larrRowRes[0]['DateTime']  && $larrRowResult[$lavri]['Examdate'] == $larrRowRes[0]['Examdate'] && $larrRowResult[$lavri]['Examsession'] == $larrRowRes[0]['Examsession']){			
				$str5 .= $larrRowResult[$lavri]['IDApplication'];
				//if($lavri != count($larrRowResult)-1)$str5 .=  ",";	
				$str5 .=  ",";			
			    $where  = $db->quoteInto('IDApplication = ?',$larrRowResult[$lavri]['IDApplication']);	
				$postData = array('VenueTime' => 2,
								  'pass' => $larrRowResult[$lavri]['pass']);
				$table = "tbl_studentapplication";
			    $db->update($table,$postData,$where);
			    
			    $wheres    = $db->quoteInto('IDApplication = '.$larrRowResult[$lavri]['IDApplication'].' AND Regid = ?',$larrRowResult[$lavri]['Regid']);
			    $postDatas = array('Cetreapproval' => $larrRowResult[$lavri]['Cetreapproval']);
				$tables = "tbl_registereddetails";
			    $db->update($tables,$postDatas,$wheres);
			}
			else{	
				$larrlogArray[] = $larrRowResult[$lavri]['IDApplication'];			
				$larrResult8['IDApplication'] = $larrRowResult[$lavri]['IDApplication'];
				$larrResult8['StudentId'] = $larrRowResult[$lavri]['StudentId'];
				$larrResult8['username'] = $larrRowResult[$lavri]['username'];
				$larrResult8['password'] = $larrRowResult[$lavri]['password'];
				$larrResult8['FName'] = $larrRowResult[$lavri]['FName'];
				$larrResult8['MName'] = $larrRowResult[$lavri]['MName'];
				$larrResult8['LName'] = $larrRowResult[$lavri]['LName'];
				$larrResult8['DateOfBirth'] = $larrRowResult[$lavri]['DateOfBirth'];
				$larrResult8['PermCity'] = $larrRowResult[$lavri]['PermCity'];
				$larrResult8['EmailAddress'] = $larrRowResult[$lavri]['EmailAddress'];
				$larrResult8['UpdDate'] = $larrRowResult[$lavri]['UpdDate'];
				$larrResult8['UpdUser'] = $larrRowResult[$lavri]['UpdUser'];
				$larrResult8['IdBatch'] = $larrRowResult[$lavri]['IdBatch'];
				$larrResult8['Venue'] = $larrRowResult[$lavri]['Venue'];
				$larrResult8['VenueTime'] = $larrRowResult[$lavri]['VenueTime'];
				$larrResult8['Program'] = $larrRowResult[$lavri]['Program'];
				$larrResult8['idschedulermaster'] = $larrRowResult[$lavri]['idschedulermaster'];
				$larrResult8['Amount'] = $larrRowResult[$lavri]['Amount'];
				$larrResult8['ICNO'] = $larrRowResult[$lavri]['ICNO'];
				$larrResult8['Payment'] = $larrRowResult[$lavri]['Payment'];
				
				$larrResult8['DateTime'] = $larrRowResult[$lavri]['DateTime'];
				$larrResult8['PermAddressDetails'] = $larrRowResult[$lavri]['PermAddressDetails'];
				$larrResult8['Takafuloperator'] = $larrRowResult[$lavri]['Takafuloperator'];
				$larrResult8['VenueChange'] = $larrRowResult[$lavri]['VenueChange'];
				$larrResult8['ArmyNo'] = $larrRowResult[$lavri]['ArmyNo'];
				$larrResult8['batchpayment'] = $larrRowResult[$lavri]['batchpayment'];
				$larrResult8['Gender'] = $larrRowResult[$lavri]['Gender'];
				$larrResult8['Race'] = $larrRowResult[$lavri]['Race'];
				$larrResult8['Qualification'] = $larrRowResult[$lavri]['Qualification'];
				$larrResult8['State'] = $larrRowResult[$lavri]['State'];
				$larrResult8['CorrAddress'] = $larrRowResult[$lavri]['CorrAddress'];
				$larrResult8['PostalCode'] = $larrRowResult[$lavri]['PostalCode'];
				$larrResult8['ContactNo'] = $larrRowResult[$lavri]['ContactNo'];
				$larrResult8['MobileNo'] = $larrRowResult[$lavri]['MobileNo'];
				$larrResult8['ExamState'] = $larrRowResult[$lavri]['ExamState'];
				$larrResult8['ExamCity'] = $larrRowResult[$lavri]['ExamCity'];
				$larrResult8['Year'] = $larrRowResult[$lavri]['Year'];
				$larrResult8['Examdate'] = $larrRowResult[$lavri]['Examdate'];
				$larrResult8['Exammonth'] = $larrRowResult[$lavri]['Exammonth'];
				$larrResult8['Examvenue'] = $larrRowResult[$lavri]['Examvenue'];
				
				$larrResult8['Examsession'] = $larrRowResult[$lavri]['Examsession'];
				$larrResult8['pass'] = $larrRowResult[$lavri]['pass'];
				$larrResult8['Religion'] = $larrRowResult[$lavri]['Religion'];
				
				
				$db->insert("tbl_logxlstudentapplication",$larrResult8);	
			}
		} 
    	
    	
    	
    	
    	
    	
    	$larrRowResult = array();  
	 	$lstrSelect = $db->select()
						 ->from(array("a" =>$table0),array('idcenterstartexam','idcenter','sessionid','Program','Totaltime','Startedtime','Date','idSession','ExamDate','ExamStartTime','CloseTime','AutoSubmitCloseTime'))							 
						 ->where('UserSession = ?',$sessionID); 
						 
		$larrRowResult = $db->fetchAll($lstrSelect);
		
		for($lavri=0;$lavri<count($larrRowResult);$lavri++){
			$str0 .= $larrRowResult[$lavri]['idcenterstartexam'];
			if($lavri != count($larrRowResult)-1)$str0 .=  ",";		
			$lstrSel = $db->select()
								 ->from(array("a" =>"tbl_centerstartexam"),array('a.idcenterstartexam'))
								 ->where('idcenter = ?',$larrRowResult[$lavri]['idcenter'])
								 ->where('ExamDate = ?',$larrRowResult[$lavri]['ExamDate'])
								 ->where('Program = ?',$larrRowResult[$lavri]['Program'])
								 ->where('idSession = ?',$larrRowResult[$lavri]['idSession']); 
			$larrRowRes = $db->fetchAll($lstrSel);
			$larrResult0['idcenter'] = $larrRowResult[$lavri]['idcenter'];
			$larrResult0['sessionid'] = $larrRowResult[$lavri]['sessionid'];
			$larrResult0['Program'] = $larrRowResult[$lavri]['Program'];
			$larrResult0['Totaltime'] = $larrRowResult[$lavri]['Totaltime'];
			$larrResult0['Startedtime'] = $larrRowResult[$lavri]['Startedtime'];
			$larrResult0['Date'] = $larrRowResult[$lavri]['Date'];
			$larrResult0['idSession'] = $larrRowResult[$lavri]['idSession'];
			$larrResult0['ExamDate'] = $larrRowResult[$lavri]['ExamDate'];
			$larrResult0['ExamStartTime'] = $larrRowResult[$lavri]['ExamStartTime'];
			$larrResult0['CloseTime'] = $larrRowResult[$lavri]['CloseTime'];
			$larrResult0['AutoSubmitCloseTime'] = $larrRowResult[$lavri]['AutoSubmitCloseTime'];
			if(count($larrRowRes) == 0){			
				$db->insert("tbl_centerstartexam",$larrResult0);
			}else{
				$wher    = $db->quoteInto('idcenterstartexam = ?',$larrRowRes[0]['idcenterstartexam']);
			    $db->update("tbl_centerstartexam",$larrResult0,$wher);
			}
		}
		
		
    	$larrRowResult = array();   	
    	
		$lstrSelect = $db->select()
						 ->from(array("a" =>$table1),array('idstudentmarks','IDApplication','NoofQtns','Attended','Correct','Grade'))							 
						 ->where('UserSession = ?',$sessionID); 
						 
		$larrRowResult = $db->fetchAll($lstrSelect);
	
		for($lavri=0;$lavri<count($larrRowResult);$lavri++){
			$str1 .= $larrRowResult[$lavri]['idstudentmarks'];
			if($lavri != count($larrRowResult)-1)$str1 .=  ",";	
							
			/*$lstrSel = $db->select()
								 ->from(array("a" =>"tbl_studentmarks"),array('a.IDApplication'))
								 ->where('idstudentmarks = ?',$larrRowResult[$lavri]['idstudentmarks']); 
			$larrRowRes = $db->fetchAll($lstrSel);*/
			$lstrSel = $db->select()
								 ->from(array("a" =>"tbl_studentapplication"),array('a.IDApplication','a.VenueTime','a.pass'))
								 ->where('a.IDApplication = ?',$larrRowResult[$lavri]['IDApplication']); 
			$larrRowRes = $db->fetchAll($lstrSel);
			//if(count($larrRowRes) == 0){	
			if(!in_array($larrRowResult[$lavri]['IDApplication'],$larrlogArray)){	
				$stdstr1 .= $larrRowResult[$lavri]['IDApplication'];
				//if($lavri != count($larrRowResult)-1)$stdstr1 .=  ",";
				$stdstr1 .=  ",";
				//$larrResult1['idstudentmarks'] = $larrRowResult[$lavri]['idstudentmarks'];
				$larrResult1['IDApplication'] = $larrRowResult[$lavri]['IDApplication'];
				$larrResult1['NoofQtns'] = $larrRowResult[$lavri]['NoofQtns'];
				$larrResult1['Attended'] = $larrRowResult[$lavri]['Attended'];
				$larrResult1['Correct'] = $larrRowResult[$lavri]['Correct'];
				$larrResult1['Grade'] = $larrRowResult[$lavri]['Grade'];
				$db->insert("tbl_studentmarks",$larrResult1);	
			}	
			else{	
				//$stdstr1 .= $larrRowResult[$lavri]['IDApplication'];
				//if($lavri != count($larrRowResult)-1)$stdstr1 .=  ",";
				//$stdstr1 .=  ",";
				//$larrResult1['idstudentmarks'] = $larrRowResult[$lavri]['idstudentmarks'];
				$larrResult1['IDApplication'] = $larrRowResult[$lavri]['IDApplication'];
				$larrResult1['NoofQtns'] = $larrRowResult[$lavri]['NoofQtns'];
				$larrResult1['Attended'] = $larrRowResult[$lavri]['Attended'];
				$larrResult1['Correct'] = $larrRowResult[$lavri]['Correct'];
				$larrResult1['Grade'] = $larrRowResult[$lavri]['Grade'];
				$db->insert("tbl_logxlstudentmarks",$larrResult1);	
			}
			//}
		}
			
		$larrRowResult = array();
		$lstrSelect = $db->select()
						 ->from(array("a" =>$table2),array('idstudentdetailspartwisemarks','IDApplication','Regid','A','B','C','UpdDate'))							 
						 ->where('UserSession = ?',$sessionID); 
								 
		$larrRowResult = $db->fetchAll($lstrSelect);
		for($lavrj=0;$lavrj<count($larrRowResult);$lavrj++){
			$str2 .= $larrRowResult[$lavrj]['idstudentdetailspartwisemarks'];
			//if($lavrj != count($larrRowResult)-1)$str2 .=  ",";	
			$str2 .=  ",";				
			/*$lstrSel = $db->select()
								 ->from(array("a" =>"tbl_studentdetailspartwisemarks"),array('a.IDApplication'))
								 ->where('idstudentdetailspartwisemarks = ?',$larrRowResult[$lavrj]['idstudentdetailspartwisemarks']); 
			$larrRowRes = $db->fetchAll($lstrSel);*/
			//if(count($larrRowRes) == 0){	
				//$larrResult2['idstudentdetailspartwisemarks'] = $larrRowResult[$lavri]['idstudentdetailspartwisemarks'];
				
			$lstrSel = $db->select()
								 ->from(array("a" =>"tbl_studentapplication"),array('a.IDApplication','a.VenueTime','a.pass'))
								 ->where('a.IDApplication = ?',$larrRowResult[$lavrj]['IDApplication']); 
			$larrRowRes = $db->fetchAll($lstrSel);
			
			if(!in_array($larrRowResult[$lavrj]['IDApplication'],$larrlogArray)){
				$larrResult2['IDApplication'] = $larrRowResult[$lavrj]['IDApplication'];
				$larrResult2['Regid'] = $larrRowResult[$lavrj]['Regid'];
				$larrResult2['A'] = $larrRowResult[$lavrj]['A'];
				$larrResult2['B'] = $larrRowResult[$lavrj]['B'];
				$larrResult2['C'] = $larrRowResult[$lavrj]['C'];
				$larrResult2['UpdDate'] = $larrRowResult[$lavrj]['UpdDate'];
				$db->insert("tbl_studentdetailspartwisemarks",$larrResult2);
			}
			else{
				$larrResult2['IDApplication'] = $larrRowResult[$lavrj]['IDApplication'];
				$larrResult2['Regid'] = $larrRowResult[$lavrj]['Regid'];
				$larrResult2['A'] = $larrRowResult[$lavrj]['A'];
				$larrResult2['B'] = $larrRowResult[$lavrj]['B'];
				$larrResult2['C'] = $larrRowResult[$lavrj]['C'];
				$larrResult2['UpdDate'] = $larrRowResult[$lavrj]['UpdDate'];
				$db->insert("tbl_logxlstudentdetailspartwisemarks",$larrResult2);
			}
			//}
		}
		
		
		
		$larrRowResult = array();
		$lstrSelect = $db->select()
						 ->from(array("a" =>$table3),array('idstudentstartexamdetails','IDApplication','Starttime','Endtime','Submittedby','UpdDate','StudentIpAddress'))							 
						 ->where('UserSession = ?',$sessionID); 
								 
		$larrRowResult = $db->fetchAll($lstrSelect);
		for($lavri=0;$lavri<count($larrRowResult);$lavri++){
			$str3 .= $larrRowResult[$lavri]['idstudentstartexamdetails'];
			//if($lavri != count($larrRowResult)-1)$str3 .=  ",";	
			$str3 .=  ",";				
			/*$lstrSel = $db->select()
								 ->from(array("a" =>"tbl_studentstartexamdetails"),array('a.idstudentstartexamdetails'))
								 ->where('idstudentstartexamdetails = ?',$larrRowResult[$lavri]['idstudentstartexamdetails']); 
			$larrRowRes = $db->fetchAll($lstrSel);*/
			//if(count($larrRowRes) == 0){	
				//$larrResult3['idstudentstartexamdetails'] = $larrRowResult[$lavri]['idstudentstartexamdetails'];
			$lstrSel = $db->select()
								 ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
								 ->where('a.IDApplication = ?',$larrRowResult[$lavri]['IDApplication']); 
			$larrRowRes = $db->fetchAll($lstrSel);
			if(!in_array($larrRowResult[$lavri]['IDApplication'],$larrlogArray)){
				$larrResult3['IDApplication'] = $larrRowResult[$lavri]['IDApplication'];
				$larrResult3['Starttime'] = $larrRowResult[$lavri]['Starttime'];
				$larrResult3['Endtime'] = $larrRowResult[$lavri]['Endtime'];
				$larrResult3['Submittedby'] = $larrRowResult[$lavri]['Submittedby'];
				$larrResult3['UpdDate'] = $larrRowResult[$lavri]['UpdDate'];
				$larrResult3['StudentIpAddress'] = $larrRowResult[$lavri]['StudentIpAddress'];
				$db->insert("tbl_studentstartexamdetails",$larrResult3);
			}
			else{	
				$larrResult3['IDApplication'] = $larrRowResult[$lavri]['IDApplication'];
				$larrResult3['Starttime'] = $larrRowResult[$lavri]['Starttime'];
				$larrResult3['Endtime'] = $larrRowResult[$lavri]['Endtime'];
				$larrResult3['Submittedby'] = $larrRowResult[$lavri]['Submittedby'];
				$larrResult3['UpdDate'] = $larrRowResult[$lavri]['UpdDate'];
				$larrResult3['StudentIpAddress'] = $larrRowResult[$lavri]['StudentIpAddress'];
				$db->insert("tbl_logxlstudentstartexamdetails",$larrResult3);
			}
			//}
		}   	
		
		
		$larrRowResult = array();
		$lstrSelect = $db->select()
						 ->from(array("a" =>$table4),array('idanswerdetail','Regid','QuestionNo','Answer'))							 
						 ->where('UserSession = ?',$sessionID); 
								 
		$larrRowResult = $db->fetchAll($lstrSelect);
		for($lavri=0;$lavri<count($larrRowResult);$lavri++){
			$str4 .= $larrRowResult[$lavri]['idanswerdetail'];
			//if($lavri != count($larrRowResult)-1)$str4 .=  ",";
			$str4 .=  ",";			
			//$larrResult3['idstudentstartexamdetails'] = $larrRowResult[$lavri]['idstudentstartexamdetails'];
			$larrResult4['Regid'] = $larrRowResult[$lavri]['Regid'];
			$larrResult4['QuestionNo'] = $larrRowResult[$lavri]['QuestionNo'];
			$larrResult4['Answer'] = $larrRowResult[$lavri]['Answer'];
			$db->insert("tbl_answerdetails",$larrResult4);			
		}   
		
		
	    
		
	    $larrRowResult = array();
		$lstrSelect = $db->select()
						 ->from(array("a" =>$table6),array('idquestionsetforstudents','IDApplication','Program','IdBatch','idquestions'))							 
						 ->where('UserSession = ?',$sessionID); 
								 
		$larrRowResult = $db->fetchAll($lstrSelect);
		for($lavri=0;$lavri<count($larrRowResult);$lavri++){			
			if(!in_array($larrRowResult[$lavri]['IDApplication'],$larrlogArray)){
				$str6 .= $larrRowResult[$lavri]['idquestionsetforstudents'];
				//if($lavri != count($larrRowResult)-1)$str6 .=  ",";	
				$str6 .=  ",";			
				$larrResult6['IDApplication'] = $larrRowResult[$lavri]['IDApplication'];
				$larrResult6['Program'] = $larrRowResult[$lavri]['Program'];
				$larrResult6['IdBatch'] = $larrRowResult[$lavri]['IdBatch'];
				$larrResult6['idquestions'] = $larrRowResult[$lavri]['idquestions'];
				$db->insert("tbl_questionsetforstudents",$larrResult6);
			}	
			else{
				$larrResult6['IDApplication'] = $larrRowResult[$lavri]['IDApplication'];
				$larrResult6['Program'] = $larrRowResult[$lavri]['Program'];
				$larrResult6['IdBatch'] = $larrRowResult[$lavri]['IdBatch'];
				$larrResult6['idquestions'] = $larrRowResult[$lavri]['idquestions'];
				$db->insert("tbl_logxlquestionsetforstudents",$larrResult6);
			}		
		}
		
		
		if($str0 != ""){
			$where0  = $db->quoteInto('idcenterstartexam IN ('.rtrim($str0,",").') AND UserSession = ?',$sessionID);	
			$db->delete($table0,$where0);
		}
		if($str1 != ""){
			$where1  = $db->quoteInto('idstudentmarks IN ('.rtrim($str1,",").') AND UserSession = ?',$sessionID);
			$db->delete($table1,$where1);
		}	
		if($str2 != ""){
			$where2  = $db->quoteInto('idstudentdetailspartwisemarks IN ('.rtrim($str2,",").') AND UserSession = ?',$sessionID);
			$db->delete($table2,$where2);
		}
		if($str3 != ""){
			$where3  = $db->quoteInto('idstudentstartexamdetails IN ('.rtrim($str3,",").') AND UserSession = ?',$sessionID);
			$db->delete($table3,$where3);
		}
		if($str4 != ""){
			$where4  = $db->quoteInto('idanswerdetail IN ('.rtrim($str4,",").') AND UserSession = ?',$sessionID);	
			$db->delete($table4,$where4);
		}
		if($str5 != ""){
			$where5  = $db->quoteInto('IDApplication IN ('.rtrim($str5,",").') AND UserSession = ?',$sessionID);
			$db->delete($table5,$where5);
		}
		if($str6 != ""){
			$where6  = $db->quoteInto('idquestionsetforstudents IN ('.rtrim($str6,",").') AND UserSession = ?',$sessionID);
			$db->delete($table6,$where6);
		} 	
    }
	public function fnGetCountOfTempApprovedStudents($idcenter,$idprogram,$idsession,$exdate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("DISTINCT(a.IDApplication) as IDApplication"))
							 ->join(array("b" => "tbl_registereddetails"),'a.IDApplication = b.IDApplication',array(""))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("b.Cetreapproval = 1")
							 ->where("a.Payment = 1");
							// ->group("b.IDApplication");
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
    public function fnGetCountOfTempStudentsAttended($idcenter,$idprogram,$idsession,$exdate,$pass) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_tempxlstudentapplication"),array("a.IDApplication"))
							 ->join(array("b" => "tbl_tempxlstudentstartexamdetails"),'a.IDApplication = b.IDApplication AND a.DateTime=DATE(b.UpdDate)',array(""))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 //->where("a.pass != '$pass'")
							 ->where("a.Payment = 1")
							 ->group("b.IDApplication")
							 ->order("a.FName");
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	public function fnGetCountOfTempStudents($idcenter,$idprogram,$idsession,$exdate,$pass) {
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
						 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication"))
						 ->where("a.Examvenue = '$idcenter'")
						 ->where("a.Program = '$idprogram'")
						 ->where("a.DateTime = '$exdate'")
						 ->where("a.Examsession = '$idsession'")
						 ->where("a.Payment = 1")
						 ->order("a.FName");
			  /*if($pass == 3) {		 
					$lstrSelect ->where("a.pass != '$pass'");
				} else*/
				if($pass == 1) {
					$lstrSelect ->where("a.pass = '$pass'");
				} elseif($pass == 2)	{
					$lstrSelect ->where("a.pass = '$pass'");
				} 
						
		    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	    }	
	   public function fnselectdata($sessionID,$larrformData,$venuefield)    {
   			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
						 ->from(array("a" => "tbl_tempxlstudentapplication"),array("a.IDApplication"))
						 ->join(array("b" => "tbl_tempxlstudentstartexamdetails"),'a.IDApplication = b.IDApplication',array(""))
						 ->join(array("c" => "tbl_tempxlstudentmarks"),'a.IDApplication = c.IDApplication',array("c.pass"))
						 ->where("a.Payment = 1")
						 ->where("b.UserSession = '".$sessionID."'")
						 ->where("a.DateTime = ?",$larrformData['Date'])
						 ->where("a.Examvenue = ?",$venuefield)
						 ->group("b.IDApplication")
						 ->order("a.FName");							
		    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			    
			return $larrResult;
	   } 
	  public function fnUpdateStudentApplication($stdstr1,$val)    {
		  	$db = Zend_Db_Table::getDefaultAdapter();
		   	$where  = $db->quoteInto('IDApplication IN ('.$stdstr1.')','');	
			$postData = array('VenueTime' => $val);
			$table = "tbl_studentapplication";
		    $db->update($table,$postData,$where);
	  }
}
