<?php
class Examination_Model_Uploadfiles extends Zend_Db_Table {
	
	protected $_name = 'tbl_fileuploads'; // table name

	public function fnAddUploadfiles($larrformData) {					
		$this->insert($larrformData);
	}
	public function fnGetBankDetails() {
		return array();    	  
    }
    public function fnSearchcourse($post = array()) {
		return array();    	  
    }
    /*
	 * fetch all  Active Bank details
	 */
    public function fnGetQuestionDetails() {
    	  $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_answers'),array("GROUP_CONCAT(answers SEPARATOR ' #^ ') as answers"))
			->join(array('a' => 'tbl_questions'),'c.idquestion = a.idquestions',array('a.*'))
			->join(array('b' => 'tbl_definationms'),'a.QuestionType = b.idDefinition',array('DefinitionDesc'))
			->where("a.Active=1")
			->group("c.idquestion");
			//->order("c.idanswers");
		  $result = $db->fetchAll($select);
		
		return $result;    	  
    }
    
    
public function fnCheckQtnsDetails($Questionid)
	{
		 $db = Zend_Db_Table::getDefaultAdapter();
		   $select = $db->select()	
			->from(array('a' => 'tbl_answerdetails'),array('a.*'))
			->where("a.QuestionNo=?",$Questionid);
		  $result = $db->fetchrow($select);
		return $result;    	  
	}
	
 public function fnGetcorrectQuestionDetails() {
    	  $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_answers'),array("c.answers as corect"))
			->join(array('a' => 'tbl_questions'),'c.idquestion = a.idquestions',array(''))
			->join(array('b' => 'tbl_definationms'),'a.QuestionType = b.idDefinition',array(''))
			->where("c.CorrectAnswer=1")
			->order("c.idquestion");
		  $result = $db->fetchAll($select);
		
		return $result;    	  
    }
     public function fnGetAllQuestion(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_questions'),array('a.Question'));
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}	
    /*
     * search method
     */
	public function fnSearchQuestion($post = array()) {
		//echo $post['field1'];
		
		 $lintdefinitionid = $post['field1'];
		 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
						->from(array('c' => 'tbl_answers'),array("GROUP_CONCAT(answers SEPARATOR ' #^ ') as answers"))
						->join(array('a' => 'tbl_questions'),'c.idquestion = a.idquestions',array('a.*'))
						->join(array('b' => 'tbl_definationms'),'a.QuestionType = b.idDefinition',array('DefinitionDesc'))
						->where('a.Question like "%" ? "%"',$post['field3'])
						->group("c.idquestion"); 

		if($post['field1']){	$select->where("b.idDefinition = ?",$post['field1']);}	
		if($post['field8']){	$select->where("a.QuestionGroup = ?",$post['field8']);}	
			if($post['field15']){	$select->where("a.QuestionNumber = ?",$post['field15']);}	
			
		$result = $db->fetchAll($select);
		
		return $result;
	}
	
public function fncorrectQuestion($post = array()) {
		//echo $post['field1'];
		
		 $lintdefinitionid = $post['field1'];
		 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
						->from(array('c' => 'tbl_answers'),array("c.answers as corect"))
						->join(array('a' => 'tbl_questions'),'c.idquestion = a.idquestions',array('a.*'))
						->join(array('b' => 'tbl_definationms'),'a.QuestionType = b.idDefinition',array('DefinitionDesc'))
							->where("c.CorrectAnswer=1")
						->where('a.Question like "%" ? "%"',$post['field3'])
						->order("c.idquestion");

		if($post['field1']){	$select->where("b.idDefinition = ?",$post['field1']);}	
		if($post['field8']){	$select->where("a.QuestionGroup = ?",$post['field8']);}	
			if($post['field15']){	$select->where("a.QuestionNumber = ?",$post['field15']);}	
			
		$result = $db->fetchAll($select);
		
		return $result;
	}
	
	public function fnUpdateQutnstochange($idquestion,$crranswer)
	{
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $where = 'idquestion = '.$idquestion; 
		  $postData = array(		
							'CorrectAnswer' => 0,
								);
				$table = "tbl_answers";
	            $lobjDbAdpt->update($table,$postData,$where);
	            
	            ///////////////////////////////////////////////////////
	              $where1 = 'idanswers = '.$crranswer;
			     $postData1 = array(		
							'CorrectAnswer' => 1,	
			     		
								);
				$table1 = "tbl_answers";
	            $lobjDbAdpt->update($table1,$postData1,$where1);
	            ////////////////////////////////////////////////
	}
	public function fnGetQuestionGroup()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questions"),array("key"=>"a.QuestionGroup","value"=>"a.QuestionGroup"))
										  ->group("a.QuestionGroup");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
public function fnGetChapterGroup()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questions"),array("key"=>"a.QuestionNumber","value"=>"a.QuestionNumber"))
										  ->group("a.QuestionNumber");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
 public function fninsertquestion($linefile,$section,$QuestionLevel,$questionnumber,$questiongroup,$field1,$fieldtwo,$fieldthree,$fieldfour,$fieldfive,$malay)
   {
   	    $sessionID = Zend_Session::getId();
   	    
   	       $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_tempquestions";
            $postData = array(		
            				'Sessionid'=>$sessionID,
							'Question' => $linefile,		
            				'QuestionAlias' => $linefile,		
		            		'QuestionType' =>$section,	
		            		'QuestionLevel'	=>$QuestionLevel,
		            		'QuestionNumber'=>$questionnumber,
		            		'QuestionGroup'=>$questiongroup,
				            'Field1'=>$field1,
				            'Field2'=>$fieldtwo,
				            'Field3'=>$fieldthree,
				            'Field4'=>$fieldfour,
				            'Field5'=>$fieldfive,
				            'AnswerStyle' => 0,
				            'Malay'=>$malay															
						);			
	        $db->insert($table,$postData);
	       
   }
   
   /*
    * function to insert inot the answers table
    */
 public function fninsertanswers($linefile,$lintidquestion,$malay)
   {
   	    
   	       $db = Zend_Db_Table::getDefaultAdapter();
           $table = "tbl_tempanswers";
           $postData = array(		
							'idquestion' => $lintidquestion,
            				'answers' => $linefile,	
           					'Malay'=>$malay													
						);			
	      $db->insert($table,$postData);
	      return $db->lastInsertId('tbl_tempanswers','idanswers');
	        
   }
 public function fnupdateanswerkey($larrresultanswer)
  {
  		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			    $where = 'idanswers = '.$larrresultanswer;
			     $postData = array(		
							'CorrectAnswer' => 1														
						);
				$table = "tbl_tempanswers";
	            $lobjDbAdpt->update($table,$postData,$where);
  }
  public function fnupdatefunction($lintidquestion,$updatedquestions)
  {
  		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			    $where = 'idquestions = '.$lintidquestion;
			     $postData = array(		
							'Question' => $updatedquestions														
						);
				$table = "tbl_tempquestions";
	            $lobjDbAdpt->update($table,$postData,$where);
  }
	public function fnGetQtnsDetails($Questionid)
	{
		 $db = Zend_Db_Table::getDefaultAdapter();
		   $select = $db->select()	
			->from(array('a' => 'tbl_questions'),array('a.*'))
			->join(array('b' => 'tbl_definationms'),'a.QuestionType = b.idDefinition',array('DefinitionDesc'))
			->where("a.idquestions=?",$Questionid);
		  $result = $db->fetchrow($select);
		return $result;    	  
	}
	
	public function fnGetAnswerforQtn($Questionid)
	{
		 $db = Zend_Db_Table::getDefaultAdapter();
		   $select = $db->select()	
			->from(array('a' => 'tbl_answers'),array('a.*'))
			->join(array('b' => 'tbl_questions'),'b.idquestions = a.idquestion',array())
			->where("b.idquestions=?",$Questionid);
		  $result = $db->fetchAll($select);
		return $result;
	}
	
	
	public function fnUpdateQutns($larrformdata,$idquestions)
	{
		     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			    $where = 'idquestions = '.$idquestions;
			     $postData = array(		
							'Question' => $larrformdata['Questions'],	
			     			'QuestionAlias' => $larrformdata['Questions'],
			     			'QuestionType' => $larrformdata['Section'],
			     			'AnswerStyle' => $larrformdata['AnswerStyle'],
			     			'Field2' => $larrformdata['Field2'],
			     			'Field3' => $larrformdata['Field3'],
			     			'Field4' => $larrformdata['Field4'],
			     			'Field5' => $larrformdata['Field5'],
			     			'Field1' => $larrformdata['Field1'],
			     			'QuestionLevel' => $larrformdata['QuestionLevel'],
			     			'QuestionGroup' => $larrformdata['QuestionGroup'],
			     			'QuestionNumber' => $larrformdata['QuestionNumber'],
			     				'Active' => $larrformdata['Active'],
			                'Malay' => $larrformdata['MalayQuestion']
			     																
						);
				$table = "tbl_questions";
	            $lobjDbAdpt->update($table,$postData,$where);
	            
	      
	      //////////////////////////////////////////////////////// 	
			     $where = 'idanswers = '.$larrformdata['Hiddenidanswer1'];
			     $postData = array(		
							'answers' => $larrformdata['Answers1'],	
			     			'Malay' => $larrformdata['MalayAnswers1'],	
								);
				$table = "tbl_answers";
	            $lobjDbAdpt->update($table,$postData,$where);
	     ////////////////////////////////////////////////////////
	     	      //////////////////////////////////////////////////////// 	
			     $where = 'idanswers = '.$larrformdata['Hiddenidanswer2'];
			     $postData = array(		
							'answers' => $larrformdata['Answers2'],	
			     			'Malay' => $larrformdata['MalayAnswers2'],	
								);
				$table = "tbl_answers";
	            $lobjDbAdpt->update($table,$postData,$where);
	     ////////////////////////////////////////////////////////
	     	      //////////////////////////////////////////////////////// 	
			     $where = 'idanswers = '.$larrformdata['Hiddenidanswer3'];
			     $postData = array(		
							'answers' => $larrformdata['Answers3'],	
			     			'Malay' => $larrformdata['MalayAnswers3'],
								);
				$table = "tbl_answers";
	            $lobjDbAdpt->update($table,$postData,$where);
	     ////////////////////////////////////////////////////////
	     	      //////////////////////////////////////////////////////// 	
			     $where = 'idanswers = '.$larrformdata['Hiddenidanswer4'];
			     $postData = array(		
							'answers' => $larrformdata['Answers4'],	
			     			'Malay' => $larrformdata['MalayAnswers4'],
								);
				$table = "tbl_answers";
	            $lobjDbAdpt->update($table,$postData,$where);
	     ////////////////////////////////////////////////////////
           
	}

	 public function fnGetQuestion($lintidquestion){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_tempquestions'),array('a.*'))
								->where("a.idquestions=?",$lintidquestion);;
				$result = $lobjDbAdpt->fetchrow($select);	
				return $result;
		}	
		
	public function fninsertmain($larrresulttempquestions,$larrresulttempanswers)
	{
		/*echo "<pre/>";
		print_r($larrresulttempquestions);
		print_r($larrresulttempanswers);
		die();*/
		$db 	= Zend_Db_Table::getDefaultAdapter();
 	    for($linti = 0;$linti<count($larrresulttempquestions);$linti++)
		  	 {
		            $table = "tbl_questions";
		            $postData = array('Question' => $larrresulttempquestions[$linti]['Question'],		
		            				'QuestionAlias' => $larrresulttempquestions[$linti]['QuestionAlias'],		
				            		'QuestionType' =>$larrresulttempquestions[$linti]['QuestionType'],	
				            		'QuestionLevel'	=>$larrresulttempquestions[$linti]['QuestionLevel'],
				            		'QuestionNumber'=>$larrresulttempquestions[$linti]['QuestionNumber'],
				            		'QuestionGroup'=>$larrresulttempquestions[$linti]['QuestionGroup'],
						            'Field1'=>$larrresulttempquestions[$linti]['Field1'],
						            'Field2'=>$larrresulttempquestions[$linti]['Field2'],
						            'Field3'=>$larrresulttempquestions[$linti]['Field3'],
						            'Field4'=>$larrresulttempquestions[$linti]['Field4'],
						            'Field5'=>$larrresulttempquestions[$linti]['Field5'],
		            				'AnswerStyle' => $larrresulttempquestions[$linti]['Active'],
		                            'Tamil'=>$larrresulttempquestions[$linti]['Tamil'],
		            				'Arabic' => $larrresulttempquestions[$linti]['Arabic'],
		            				'Malay' => $larrresulttempquestions[$linti]['Malay'],
                                                         'Active'=>'1'	
		            																
								   );			
			        $db->insert($table,$postData);
			        $lintidquestion = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_questions','idquestions');
			        for($lintj = 0;$lintj<count($larrresulttempanswers);$lintj++)
		  	          {   
					        if($larrresulttempquestions[$linti]['idquestions']==$larrresulttempanswers[$lintj]['idquestion'])
					        {
					        	   $table = "tbl_answers";
						           $postData = array(		
													'idquestion' => $lintidquestion,
						            				'answers' => $larrresulttempanswers[$lintj]['answers'],
						           					'CorrectAnswer' => $larrresulttempanswers[$lintj]['CorrectAnswer'],	
										            'Tamil'=>$larrresulttempanswers[$lintj]['Tamil'],
						            				'Arabic' => $larrresulttempanswers[$lintj]['Arabic'],
						           					'Malay' => $larrresulttempanswers[$lintj]['Malay']														
												);			
							      $db->insert($table,$postData);
					        }
		  	          }  
			        
		  	 }
	}
	
	public function fnGetAllQuestionFromTemp()
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_tempquestions'),array('a.*'));
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
	}

	public function fnGetAllAnswersFromTemp()
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_tempanswers'),array('a.*'));
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
	}
	
public function fnDeleteTempDetails()
 	{
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$db->delete('tbl_tempanswers');
 		$db->delete('tbl_tempquestions');
 		
 	}
 	
 public function fnAddSingleQuestion($larrformdata)
 {
 	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 	     $table = "tbl_questions";
 	     
		 $postData = array('Question' => $larrformdata['Questions'],	
			     		    'QuestionAlias' => $larrformdata['Questions'],
			     			'QuestionType' => $larrformdata['Section'],
			     			'AnswerStyle' => $larrformdata['AnswerStyle'],
			     			'Field2' => '',
			     			'Field3' => '',
			     			'Field4' => '',
			     			'Field5' => '',
			     			'Field1' => '',
			     			'QuestionLevel' => $larrformdata['QuestionLevel'],
			     			'QuestionGroup' => $larrformdata['QuestionGroup'],
			     			'QuestionNumber' => $larrformdata['QuestionNumber'],
			     			'Active' => $larrformdata['Active'],
			                'Malay' => $larrformdata['MalayQuestion']
			     			);
	  $lobjDbAdpt->insert($table,$postData);
	  $lintidquestion = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_questions','idquestions');
	    for($lintj = 1;$lintj<=4;$lintj++)
		{ 
			 $answertable = 'tbl_answers';
			 $answerData = array('idquestion' => $lintidquestion,	
			     		    'answers' => $larrformdata['Answers'.$lintj],
			     			'CorrectAnswer' => 0,
			     			'Tamil' => '',
							'Malay' => $larrformdata['MalayAnswers'.$lintj]);
		     $lobjDbAdpt->insert($answertable,$answerData);
		     $lintididanswers = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_answers','idanswers');
		     
		     if($lintj == $larrformdata['CorrectAnswer'])
		     {
		     	 $where = 'idanswers = '.$lintididanswers;
			     $postData = array(		
									'CorrectAnswer' => 1
								  );
				$table = "tbl_answers";
	            $lobjDbAdpt->update($table,$postData,$where);
		     }
		}
		
		
	  
 }
}
