<?php
	class Examination_Model_Automailmodel extends Zend_Db_Table{
		
		public function fngetstudentdetails($larrformdata)
		{
			if($larrformdata['Studentname']) $name = $larrformdata['Studentname'];
			if($larrformdata['ICNO']) $icno = $larrformdata['ICNO'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
		              	 ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","a.ICNO as ICNO","DATE_FORMAT(a.DateTime,'%d-%m-%Y') as Examdate")) 	   			   
						 ->join(array('b' => 'tbl_center'),'b.idcenter= a.Examvenue',array('b.centername as Venue'))
						 ->join(array('c' => 'tbl_programmaster'),'c.IdProgrammaster = a.Program',array('c.ProgramName as Coursename'))
		              	 ->where("a.Payment=1")
					   	 ->where("a.FName like  ? '%'",$name)
					   	 ->where("a.ICNO like '%' ? '%'",$icno)
					   	 ->order("a.FName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		public function fninsertdetails($larrformdata)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $noofstu = count($larrformdata['idapp']);
		    $postdata = array('schdeduleddate'=>$larrformdata['Dates'],
		                      'scheduledtime'=>$larrformdata['ScheduleStartTime'],
		                      'UpdDate' => $larrformdata['UpdDate'],
		                      'UpdUser' => $larrformdata['UpdUser']);
		    $lobjDbAdpt->insert('tbl_automail',$postdata); 	
		    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
		    for($linti=0;$linti<$noofstu;$linti++)
		    {
		    	$newpostdata = array('idautomail'=>$lintlastinsid,
		    	                     'idapplication'=>$larrformdata['idapp'][$linti]);
		    	$lobjDbAdpt->insert('tbl_idmaildetails',$newpostdata); 
		    }
		}
		public function fngetscheduledetails($shcdate,$shctime)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect =  "SELECT a.idautomail,a.scheduledtime,a.schdeduleddate
			   				FROM tbl_automail a
			   				WHERE a.scheduledtime = '$shctime' and a.schdeduleddate = '$shcdate'";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
	   public function fngetiddetails($idauto)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect =  "SELECT b.idapplication
			   				FROM tbl_idmaildetails b 
			   				WHERE b.idautomail  = '$idauto'";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		public function fngetemailtempdetails()
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect =  "SELECT TemplateFrom,TemplateSubject,TemplateBody
							FROM tbl_emailtemplate 
							WHERE idTemplate = 5";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		public function fnupddetails($idapp)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect =  "Update tbl_studentapplication set pass = 10 where IDApplication = $idapp";
			$lobjDbAdpt->query($lstrSelect);
			
		}
		public function fngetsendingdetails($idapp)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = "SELECT FName,IDApplication,EmailAddress,Program,ICNO,username,password
						   FROM tbl_studentapplication 
						   WHERE IDApplication= $idapp"; 
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
}
