<?php
class Examination_Model_Testexcels extends Zend_Db_Table{
		 protected $_name = 'tbl_studentchangeddetails';
		public function fngetalldetails()
		{
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_tempapplication"),array("a.*"));
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
			return $larrResult;
		}
		public function fninsertintotemp($informaticnum,$studentnamefield,$Gender,$email,
					$race,$education,$dateofbirth,$Address,$CorrAddress,$Postal,$Country,$State,$Contactno,$Mobile,$mailingaddrs,$remarks,$idapp)
		{
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		     $newpostdata = array('ICNO'=>$informaticnum,
		    	               'Studentname'=>$studentnamefield,
							   'Email'=>$email,
							   'Race'=>$race,
							   'Education'=>$education,
							   'DateofBirth'=>$dateofbirth,
		    	               'Gender'=>$Gender,
							   'Mailing Address'=>$Address,
							   'Correspondance Address'=>$CorrAddress,
							   'Postal Code'=>$Postal,
		    	               'Country'=>$Country,
							   'State'=>$State,
							   'Contact No'=>$Contactno,
							   'Mobile No'=>$Mobile,
							   'Mailing Address'=>$mailingaddrs,
							   'UpdDate'=>date('Y-m-d H:i:s'),
							   'Remarks'=>$remarks,
							   'IDApplication'=>$idapp
							   );
		   $larrResult = $lobjDbAdpt->insert('tbl_tempapplication',$newpostdata); 
		    return $larrResult;
		}
		public function fnResetArrayFromValuesToNames($OrginialArray){
			$larrNewArr = array();
			$OrgnArray = @array_values($OrginialArray);
			for($lintI=0;$lintI<count($OrgnArray);$lintI++){
				$larrNewArr[$lintI]["name"] = $OrgnArray[$lintI]["value"];
				$larrNewArr[$lintI]["key"] = $OrgnArray[$lintI]["key"];
			}
			return $larrNewArr;
		}
		public function fnGetcompanyortakafulList($id)
        {
		    if($id == 1)
		    {
       	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		  $lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"a.CompanyName"))
					 				 //->where("a.idCountry = ?",$idCountry)
					 				 ->order("a.CompanyName");
			  $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			  return $larrResult;
			}
			if($id == 2)
			{
			    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		    $lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"))
					 				// ->where("a.idCountry = ?",$idCountry)
					 				 ->order("a.TakafulName");
			   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			   return $larrResult;
			}
		} 
        public function  fninsertintostudentchangedetails($CT,$CTid,$idappn,$icno,$res,$upduser)
        {
		
		
		       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		  $lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_studentchangeddetails"),array("a.*"))
					 				 ->where("a.ICNO = ?",$icno);					 				
			   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			   
			   for($i=0;$i<count($larrResult);$i++)
			   {			     
				 $icnos =$larrResult[$i]['ICNO'];
		
			     $where = "ICNO=".$icnos;	
			     $postData = array('Active' => 0);	
				
				  $select ="update tbl_studentchangeddetails set Active=0 where ".$where;
				
			     $lobjDbAdpt->query($select);
				 
			   }
			   $newpostdata = array('IDApplication'=>$idappn,
								   'companytype'=>$CT,
								   'Idcompany'=>$CTid,
								   'ICNO'=>$icno,
								   'pass'=>$res,
								   'UpdDate'=>date('Y-m-d H:i:s'),
								   'Active'=>1,
								   'UpdUser'=>$upduser
								   );
						//echo "<pre>";
						//print_r($newpostdata);die();		   
				$larrResult = $lobjDbAdpt->insert('tbl_studentchangeddetails',$newpostdata); 
				return $larrResult;
        }	
       public function fngeticnumber($idapp)
      {
	       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.ICNO","a.pass"))
		               ->where("a.IDApplication= ?",$idapp);
					   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		   return $larrResult;
      }	   
		public function fngetregisterdetails($idappn)
		{
		 
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.batchpayment","a.pass","a.IDApplication"))
					   ->join(array("b" => "tbl_registereddetails"),'a.IDApplication=b.IDApplication',array("b.Regid",'b.RegistrationPin','b.IDApplication'))
		               ->where("a.IDApplication= ?",$idappn);
					   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		   return $larrResult;
		}
		public function getcompanydetails($idbatchpayment,$regpin)
		{
		
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
         if($idbatchpayment==1)
		 {     			 
		    $lstrSelect = $lobjDbAdpt->select()
							 	  ->from(array("a"=>"tbl_batchregistration"),array(""))
								  ->join(array("c" => "tbl_registereddetails"),'c.RegistrationPin = a.registrationPin',array(""))
                                  ->join(array("b" => "tbl_companies"),'a.idCompany = b.IdCompany',array("b.CompanyName as Company"))								  
							 	  ->where("c.registrationPin = ?",$regpin);	
            					$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		 return $larrResult;			  
		 }
		 if($idbatchpayment==2)
		 {     			 
		    $lstrSelect = $lobjDbAdpt->select()
							 	  ->from(array("a"=>"tbl_batchregistration"),array(""))
								  ->join(array("c" => "tbl_registereddetails"),'c.RegistrationPin = a.registrationPin',array(""))
                                  ->join(array("b" => "tbl_takafuloperator"),'a.idCompany = b.idtakafuloperator',array("b.TakafulName as Takaful"))								  
							 	  ->where("c.registrationPin = ?",$regpin); 
								  $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		 return $larrResult;
		 }
		 
		  
		}
		public function fncheckicnum($icnum)
		{
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.*"))
					   ->where("a.ICNO=?",$icnum);
		   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		   return $larrResult;
		}
		public function getdatedifference($dateofbirth)
		{
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
		   $select = "SELECT DATEDIFF(curdate(),'$dateofbirth') as days"; 
		   $result = $lobjDbAdpt->fetchRow($select);
		   return $result;
		}
		public function fngetyear()
		{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_config"),array("a.MinAge as age"))
					   ->where("a.idUniversity=1");
		   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		   return $larrResult;
		}
		
		public function fngetvalidate($age)
		{
		
		
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = "SELECT DATE_SUB( curdate( ) , INTERVAL $age YEAR ) AS validdate";
		   //echo $lstrSelect;die();
		   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		   return $larrResult;
		}
		public function fncheckicnumfromtemp($icnum)
		{
		//echo "ddddddd";die();
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_tempapplication"),array("a.*"))
					   ->where("a.ICNO=?",$icnum);
		   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		   return $larrResult;
		}
		public function fninsertduplicateicnum($icnum,$student)
		{
		  $error1 ="Duplicate ICNO";
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  
		  $newpostdata = array('Icno'=>$icnum,
		    	               'Studentname'=>$student,
							   'UpdDate'=>date('Y-m-d H:i:s'),
							   'Error'=>$error1
							   
							   );
		 $larrResult = $lobjDbAdpt->insert('tbl_tempapplication',$newpostdata); 
		  return $larrResult;
		   
		}
		public function fnDeleteTempDetails()
 	   {
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$db->delete('tbl_tempapplication');
		}
 		
		public function fninserticnumnotformat($icnum,$student)
		{
		 //echo $icnum;
		 // echo $student;
		   $error2 ="Informat ICNO";
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   
		  $newpostdata = array('Icno'=>$icnum,
		    	               'Studentname'=>$student,
							   'UpdDate'=>date('Y-m-d H:i:s'),
							   'Error'=>$error2
							   );
		 // print_r($newpostdata);die();
		  $larrResult= $lobjDbAdpt->insert('tbl_tempapplication',$newpostdata); 
		  return $larrResult;
		}
		public function fninsertincorrectgender($icnum,$student)
		{
		   $error3 ="Incorrect Gender";
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   
		   $newpostdata = array('Icno'=>$icnum,
		    	               'Studentname'=>$student,
							   'UpdDate'=>date('Y-m-d H:i:s'),
							   'Error'=>$error3
							   );
		  $larrResult= $lobjDbAdpt->insert('tbl_tempapplication',$newpostdata); 
		  return $larrResult;
		}
		
		
}
