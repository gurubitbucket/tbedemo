<?php
	class Examination_Model_Autochronepush extends Zend_Db_Table_Abstract {


    
	public function fngetconfigvalues()
    {
    	
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_config"),array("a.*"));
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
    }
    
   	public function fngetvenuesforthenextexamdates($fromdate,$todate)
    {
    	        $date=date('Y-m-d');
    	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			  ->where("a.Allotedseats>0")
							 			  ->where("a.date >='$fromdate'")
							 			   ->where("a.date <='$todate'")
							 			  ->group("a.date");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
    
    
    public function fngetvenuelist($examdate)
    {
    	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array())
							 			  ->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("b.centername","b.idcenter"))
							 			  ->where("a.Allotedseats>0")
							 			  ->where("a.date ='$examdate'")
							 			  ->group("a.idvenue");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
    
    public function fngetallvenuesfortheexamdate($examdate)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			   ->where("a.Allotedseats>0")
							 			  ->where("a.date ='$examdate'")
							 			  ->group("a.idvenue");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        return $larresult;
    }
    
    public function fninsertchronepushallvenues($larrformdata,$datetotime)
    {
    		// print_R($datetotime);
    	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     $totalselectedcount = count($larrformdata['allvenues']);
    	 
    	 for($i=0;$i<$totalselectedcount;$i++)
    	 {
    	 	
    	 	$examdateall = explode('All',$larrformdata['allvenues'][$i]);
    	 	$examdate = $examdateall[1];
    	 	$schedulerdate = $examdate;
    	 	$schedulertime = $datetotime['DatetoTime'][$examdate];
    	 	$larresultsallvenues = self::fngetallvenuesfortheexamdate($examdate);
    	 	//print_R($larresultsallvenues);
    	 	 for($j=0;$j<count($larresultsallvenues);$j++)
    	 	 {
    	 	 	 $postdata = array('schdeduleddate'=>$schedulerdate,
			                      'scheduledtime'=>$schedulertime,
			                      'UpdDate' => date('Y-m-d H:i:s'),
			                      'UpdUser' => 1);
			    $lobjDbAdpt->insert('tbl_automail',$postdata);
			    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
			       //fninsertintochronevenue($larrformData,$lintlastinsid);

	    		
	    			$table = "tbl_cronevenue";
	          	   $postData = array(		
								'idcenter' => $larresultsallvenues[$j]['idvenue'],		
	            				'Examdate' =>$examdate,
	           					'idautomail' => $lintlastinsid		
							);			
		        $lobjDbAdpt->insert($table,$postData);
    	 	 	
    	 	 }
    	 }
    }
    
    public function fninsertchronepush($larrformdata,$datetotime)
    {
    	 echo "<pre/>";
    	// print_R($datetotime);
    	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     $totalselectedcount = count($larrformdata['centerids']);
    	 
    	 for($i=0;$i<$totalselectedcount;$i++)
    	 {
    	 
    	 	$idcenterwithdate = $larrformdata['centerids'][$i];
    	 	$larrcenterdate = explode('**',$idcenterwithdate);
    	 	$examdate = $larrcenterdate[1];
    	 	$examcenter= $larrcenterdate[0];
    	 	$schedulerdate = $examdate;
    		$schedulertime = $datetotime['DatetoTime'][$examdate];
    		
    		  $postdata = array('schdeduleddate'=>$schedulerdate,
			                      'scheduledtime'=>$schedulertime,
			                      'UpdDate' => date('Y-m-d H:i:s'),
			                      'UpdUser' => 1);
			    $lobjDbAdpt->insert('tbl_automail',$postdata);
			    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
			       //fninsertintochronevenue($larrformData,$lintlastinsid);

	    		
	    			$table = "tbl_cronevenue";
	          	   $postData = array(		
								'idcenter' => $examcenter,		
	            				'Examdate' =>$examdate,
	           					'idautomail' => $lintlastinsid		
							);			
		        $lobjDbAdpt->insert($table,$postData);
    		
    	 }
    }
    
    public function dateformats($date)
    {
    	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$select = "SELECT DATE_FORMAT('$date', '%d %M %Y') as examdate";
    	$larrResult = $lobjDbAdpt->fetchrow($select);
				return $larrResult;
    }
    
    public function fngetvenuestatus($idcenter,$examdates)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_cronevenue"),array("a.*"))
							 			  ->where("a.Examdate = '$examdates'")
							 			  ->where("a.idcenter = '$idcenter'");
        $larresult = $lobjDbAdpt->fetchRow($lstrSelect);
        return $larresult;
    }
    
	public function fngetduplicateregid(){
		$date=date("Y-m-d");
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_registereddetails"),array("a.Regid",'count(a.Regid) as totalrepetion'))
							 			  ->join(array("b"=>"tbl_studentapplication"),'b.IDApplication=a.IDApplication',array(""))
							 			  -> where("DATE_FORMAT(b.DateTime,'%Y-%m-%d') >'".$date."'")
							 			  ->group("a.Regid")
							 			  ->having("count(a.Regid)>1");
        $larresult = $lobjDbAdpt->fetchAll($lstrSelect);
        //echo $lstrSelect;
        return $larresult;
    }
    
    
	}
