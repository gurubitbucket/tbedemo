<?php
	class Examination_Model_Newscreen extends Zend_Db_Table {
		
		
		protected $_name = 'tbl_studentapplication';
	

 public function fngetstudentinformation($maxtimes)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("f" =>"tbl_studentpaymentoption"),'f.IDApplication=a.IDApplication',array('f.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->joinleft(array("e" =>"tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array('e.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->where("a.pass=3")
										  ->where("a.Venue<=?",$maxtimes)
										  ->group("d.IDApplication");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
		
     
     
     
     
	 public function fnSearchStudent($larrformData,$maxtimes)
     {
     	
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										    // ->join(array("f" =>"tbl_studentpaymentoption"),'f.IDApplication=a.IDApplication',array('f.*'))
										    ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										    ->joinleft(array("e" =>"tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array('e.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										   ->where("a.Venue<=?",$maxtimes)
										  ->where("a.pass in (3,4)")
										    ->where("a.IDApplication>1148")
										   ->where("a.Examvenue!=000")
										  ->group("d.IDApplication")
										   ->order("a.FName");	
										  
										  if($larrformData['Studentname']) $lstrSelect->where("a.Fname like  ? '%'",$larrformData['Studentname']);	
				if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like  ? '%'",$larrformData['ICNO']);					  
				if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$larrformData['Coursename']);
				if($larrformData['Venues']) $lstrSelect->where("c.idcenter = ?",$larrformData['Venues']);
				if($larrformData['Takafulname']) $lstrSelect->where("e.idtakafuloperator = ?",$larrformData['Takafulname']);
					if($larrformData['Applicationtype']==1) {}
				if($larrformData['Applicationtype']==2) $lstrSelect->where("a.batchpayment = 0");
				if($larrformData['Applicationtype']==3) $lstrSelect->where("a.batchpayment = 1");
				if($larrformData['Applicationtype']==4) $lstrSelect->where("a.batchpayment = 2");
				
                if(isset($larrformData['Date']) && !empty($larrformData['Date']) && isset($larrformData['Date2']) && !empty($larrformData['Date2']))
                {
				  $lstrFromDate = $larrformData['Date'];
				  $lstrToDate = $larrformData['Date2'];
				  $lstrSelect = $lstrSelect->where("a.DateTime >= '$lstrFromDate' and a.DateTime <= '$lstrToDate'");
		        }					  
		        //echo $lstrSelect;die();
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     
     
     
     
     
     
     
     
     
     
     public function  fngetstudenteachinformation($lintidstudent)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as AppliedDate"))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.IdProgrammaster','b.ProgramName'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("w" =>"tbl_studentpaymentoption"),'w.IDApplication=a.IDApplication',array('w.*'))
										  ->join(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->join(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }
     
     	public function  fngetstudenteachinformationforbatch($lintidstudent)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as AppliedDate","year(a.datetime) as years"))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.IdProgrammaster','b.ProgramName'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->join(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }
     
     
     
     
	    public function fngetstudenttype($lintidstudent)
     {
     	
     	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.batchpayment"))
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     }
     
     

	     public function  newfnGetdatedifference($days,$applieddate)
     {
     	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	  $sql = "select DATEDIFF('$days','$applieddate') as nodays"; 
    	  //SELECT DATEDIFF( '2006-07-01', '2006-06-01' ) 
				//echo $sql; die();
    		 $result = $db->fetchRow($sql);    
			 return $result;
     	
     	
     }
     
     
	 public function  fngetstudentinformationfromconfig()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_config"),array("a.studedit","a.ClosingBatch"))
										  ->where("a.idConfig =1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }
     
     
		 public function fnGetVenueNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
				 				// ->join(array("c"=>"tbl_newschedulervenue"),'c.idvenue=a.idcenter',array(''))	
				 				// ->join(array("e"=>"tbl_newscheduler"),'e.idnewscheduler=c.idnewscheduler',array(''))
				 				// ->where("e.Active =1")			 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   

	   
		 public function fnGetStudentNames($maxtimes)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.FName","value"=>"a.FName"))
				 				  ->where("a.Payment=1")
										   ->where("a.VenueChange<=?",$maxtimes)
										  ->where("a.pass in (3,4)")
										  ->group("a.FName")
										    ->order("a.FName")	;	 				 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	 public function fnGetTakafulNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName")) 				 
				 				 ->order("a.TakafulName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
     public function fnGetCourseNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
		
	   
	    public function fnGetSchedulerDetails()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Description")) 				 
				 				 ->order("a.Year");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
		public function fnGetStateName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
										   ->where("a.idCountry  = ?","121")
										   ->order("a.StateName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			public function fnGetCityName($idcity){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_city"),array("key"=>"a.idCity","value"=>"a.CityName"))
										   ->where("a.idCity  = ?",$idcity);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			
			
		public function fnGetVenueName($idcity){	
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
										   ->where("a.city = ?",$idcity);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
		public function fnGetSchedulerSessionDetails($idschdulersesion){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
										   ->where("a.idmangesession = ?",$idschdulersesion);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			public function fnGetSchedulerYearDetails($idsch)
			{
				
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.year"))
										   ->where("a.idnewscheduler = ?",$idsch);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
			}
			
	public function fnGetYearlistforcourse($idprog){		
			

$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Description"))
										  //->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										    ->join(array("d"=>"tbl_newmonths"),"a.From=d.idmonth",array())
										  ->join(array("e"=>"tbl_newmonths"),"a.To=e.idmonth",array())
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())

                                          ->where("a.Active   = 1")
										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;





	}
	
	
	

	

	
	public function fnGetVenuelistforcourse($idcity,$idprog,$idseched){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array())
										  ->where("d.city = ?",$idcity)
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler  = ?",$idseched)
										  ->group("d.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	



	
	
	

   
	 public function fnGetVenuedetailsgetsecid($idsech)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
						                  ->where("a.Active=1")
										  ->where("a.idnewscheduler =?",$idsech); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
	
  
  
	  public function fnGetsesssiondetails($year,$city,$venue,$idsession,$day,$month)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler')
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession',array("key"=>"g.idmangesession","value"=>"g.managesessionname"))
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter')
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array())
										 // ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										   ->where("a.idnewscheduler =?",$year)
										     ->where("b.idvenue =?",$venue)
										     ->where("f.idmanagesession not in (select Examsession from  tbl_studentapplication where  Examsession in ($idsession) and ExamCity=$city and Examdate=$day and Examvenue=$venue and Year=$year)")
										   ->group("g.idmangesession");
									
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
	 public function fngetyearforthescheduler($id)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
										  ->where("a.idnewscheduler=?",$id);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
		 public function fngetstudentoldinfo($idapplication)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										  ->where("a.IDApplication =?",$idapplication);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
 public function fnUpdateStudentnewscreen($idapplication,$larrformData,$hiddenscheduler,$iduser,$idstate,$idcity,$venuechange,$idbatch)
   {
   	
   $db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(
		                    'UpdUser' =>$iduser,
							'IdBatch'=>$idbatch,
		                    'Program' =>$larrformData['Coursename1'],	
		                    'Venue' =>$venuechange, 
		                    'VenueTime'=>0,
							'DateTime' =>$larrformData['Newprgdate'],	
		  					'ExamState'=>$idstate,
		  					'ExamCity'=>$idcity,
		  					'Year'=>$hiddenscheduler,
		  					'Examdate'=>$larrformData['newexamdates'],
		  					'Exammonth'=>$larrformData['newexammonth'],
		                   	'Examvenue'=>$larrformData['newprgexamvenue'],	
		  					'Examsession'=>$larrformData['newprgexamsession'],	  
		                    'Pass'=>'3',	  
						);
		$where['IDApplication = ? ']= $idapplication;		
		 $db->update('tbl_studentapplication', $postData, $where);	
		 
		 
		 
		$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(
							'IdBatch'=>$idbatch,
							'Cetreapproval'=>'0',	  
						);
		$where['IDApplication = ? ']= $idapplication;		
		 $db->update('tbl_registereddetails', $postData, $where);
		 
   }
			

	public function newfngetyear($prog)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect="select `year`as `key` , `year` as `value` from tbl_newscheduler where idnewscheduler in (Select idnewscheduler from tbl_newschedulercourse where idprogrammaster=$prog) and active=1 group by `year`";
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
}


	public function newfnGetCitylistforcourse($idprog,$idyear,$curmonth){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array())
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.Year  = ?",$idyear)
										  ->where("a.To>=?",$curmonth)
										  ->where("a.Active  = 1")
										  ->group("d.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	


public function fnnewmonthsrange($frommonth,$tomonth)
{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
  $lstrSelect = "Select a.idmonth as `key`,a.MonthName as `value`
from tbl_newmonths  as a
where a.idmonth>=$frommonth and a.idmonth<=$tomonth";	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}
public function fnGetvenuecity($idvenue)
{
	
	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
 		$lstrSelect = "SELECT  * FROM tbl_center where idcenter=$idvenue";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
  	return $larrResult;
}


	public function fngetstudentinsertinfo($studenteditresult,$iduser,$larrformData,$oldprgamount,$newprogamount,$programflag,$amountflag) { //Function to get the user details
   	
		
	  
  
  
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_venuechange";
		$postData = array(		
							'IDapplication' =>$studenteditresult['IDApplication'],	
           					'UpdDate' => $larrformData['UpdDate'], 
							'UpdUser' =>$iduser,	
							'DateTime'=>$studenteditresult['DateTime'],	
		  					'ExamState'=>$studenteditresult['ExamState'],
		  					'ExamCity'=>$studenteditresult['ExamCity'],
		  					'year'=>$studenteditresult['Year'],
		  					'ExamDate'=>$studenteditresult['Examdate'],
		  					'ExamMonth'=>$studenteditresult['Exammonth'],
		  					'Examvenue'=>$studenteditresult['Examvenue'],	
		  					'ExamSession'=>$studenteditresult['Examsession'],	
		                    'oldupddate'=>$studenteditresult['UpdDate'],
		                    'NewExamDate'=>$larrformData['Newprgdate'],
		                    'NewExamVenue'=>$larrformData['newprgexamvenue'],
		                    'NewExamSession'=>$larrformData['newprgexamsession'],
		                    'oldprogramid'=>$studenteditresult['Program'],
							'newprogramid'=>$larrformData['Coursename1'],
		                    'oldamount'=>$oldprgamount,
		                    'newamount'=>$newprogamount,
		                    'prgcahngeflag'=>$programflag,
		                    'amtchangeflag'=>$amountflag,
		                    
						);
						$result=  $lobjDbAdpt->insert($table,$postData);
	
						   return $result;
     }
   
     
	public function fnnewmonthcaleshow($prog,$idvenue,$year)
{
	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
	 $lstrSelect = "SELECT a.*,b.*
FROM tbl_newscheduler as a,tbl_newschedulercourse  as b,tbl_newschedulercourse as c
where a.idnewscheduler=b.idnewscheduler
and a.idnewscheduler=c.idnewscheduler
and a.Active=1
and b.IdProgramMaster=$prog
and a.idnewscheduler in (SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
and a.Year=$year group by a.idnewscheduler";
	  	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}


	
 public function fngetdayofdate($iddate)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	  $sql = "select DAYOFWEEK('$iddate') as days"; 
				//echo $sql; die();
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
	 public function  fngetschedulerofdate($ids,$days)
       {
       	
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler"))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler')
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }

       
	  public function newfnGetcitydetailsgetsecid($venue)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_center"),array("a.city"))
						                  ->where("a.Active=1")
										  ->where("a.idcenter =?",$venue); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  

    
         public function newgetscheduleryear($year)
     {
     	
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler","a.To","a.From"))
                                          ->where("a.Active = 1")
										  ->where("a.Year=?",$year); 
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     	
     }   
     
     
		 public function  fngetschedulerofvenuestudent($ids,$days)
       {
       	
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array(""))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulervenue"),'a.idnewscheduler=d.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_center"),'d.idvenue=f.idcenter',array("key"=>"f.idcenter","value"=>"f.centername"))
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days)
										   ->group("f.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }
       
       
       public function  fngetscheduleridofsessionstudent($ids,$days,$idsession,$idvenue)
       {
       	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			  	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler"))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulersession"),'a.idnewscheduler=d.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_newschedulervenue"),'a.idnewscheduler=f.idnewscheduler',array(""))
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										  ->where("d.idmanagesession=?",$idsession)
										  ->where("f.idvenue=?",$idvenue)
										   ->where("c.Days=?",$days);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
       	
       }
       
       
	   
  
       
	       public function  fngetidschedulerofstudent($ids,$days,$idvenue,$idsession)
       {
       	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler"))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulersession"),'a.idnewscheduler=d.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_newschedulervenue"),'a.idnewscheduler=f.idnewscheduler',array(""))
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days)
										   ->where("f.idvenue=?",$idvenue)
										    ->where("d.idmanagesession=?",$idsession);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
       	
       }
       
       	public function fnGetProgAmount($idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT sum(abc.amount) from 

(select Rate as amount from tbl_programrate where `EffectiveDate` in 
(SELECT max(a.EffectiveDate)  from tbl_programrate a,tbl_accountmaster b
where a.idProgram= $idprog and 
 b.idAccount= a.IdAccountmaster and  
 b.Active=1 and a.Active=1 and b.idAccount not in (select t.idAccount from tbl_accountmaster t where t.duringRegistration=1 ) group by a.IdAccountmaster)and idProgram=$idprog and Active=1   and IdAccountmaster not in (select r.idAccount from tbl_accountmaster r where r.duringRegistration=1 ))as abc ";
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetProgvalidate($idprog,$ids,$days)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array(""))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulercourse"),'a.idnewscheduler=d.idnewscheduler',array(""))
                                          ->where("a.Active = 1")
                                          ->where("d.IdProgramMaster=?",$idprog)
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		
	}
	
	
		 public function  fngetschedulerofdatevenuestudent($ids,$days,$iddate,$idprg)
       {
       	
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array(""))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulervenue"),'a.idnewscheduler=d.idnewscheduler',array(""))
										   ->join(array("m"=>"tbl_newschedulercourse"),'a.idnewscheduler=m.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_center"),'d.idvenue=f.idcenter',array("key"=>"f.idcenter","value"=>"f.centername"))
                                          ->where("a.Active = 1")
                                            ->where("m.IdProgramMaster=?",$idprg)
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days)
										   ->group("f.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }
       
		 public function  fngetschedulerofdatesessionstudent($ids,$days,$iddate,$idprg)
       {
       	
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array(""))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										   ->join(array("m"=>"tbl_newschedulercourse"),'a.idnewscheduler=m.idnewscheduler',array(""))
									      ->join(array("e"=>"tbl_newschedulersession"),'a.idnewscheduler=e.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_managesession"),'f.idmangesession=e.idmanagesession',array("key"=>"f.idmangesession","value"=>"f.managesessionname"))
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
								          ->where("c.Days=?",$days)
										  ->where("m.IdProgramMaster=?",$idprg)
										  ->group("f.idmanagesession");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }
       
       
       
       public function  fnGetschdelerdates($iddates,$idprogrammaster)
       { 	
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array(""))
							 			  ->join(array("f"=>"tbl_center"),'a.idvenue=f.idcenter',array("key"=>"f.idcenter","value"=>"f.centername"))
							 			    ->join(array("m"=>"tbl_newschedulercourse"),'a.idnewscheduler=m.idnewscheduler',array(""))
                                          ->where("a.Active = 1")
                                          ->where("a.date=?",$iddates)
                                          ->where("a.centeractive=1")
                                            ->where("m.IdProgramMaster=?",$idprogrammaster)
										  ->where("a.Reserveflag=1")
										    ->group("f.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
       	
       }
       
	       public function  fnGetschdelerdates2($iddates,$idprogrammaster)
       { 	
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array(""))
							 			  ->join(array("f"=>"tbl_center"),'a.idvenue=f.idcenter',array("key"=>"f.idcenter","value"=>"f.centername"))
							 			    ->join(array("m"=>"tbl_newschedulercourse"),'a.idnewscheduler=m.idnewscheduler',array(""))
                                          ->where("a.Active = 1")
                                          ->where("a.date=?",$iddates)
                                         // ->where("a.centeractive=1")
                                            ->where("m.IdProgramMaster=?",$idprogrammaster)
										  ->where("a.Reserveflag=1")
										    ->group("f.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
       	
       }
       
	  public function  fnGetschdelerdactivecenter($iddates,$idvenue)
       { 	
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.idvenue"))
                                          ->where("a.Active = 1")
                                          ->where("a.date=?",$iddates)
                                           ->where("a.idvenue=?",$idvenue)
										  ->where("a.Reserveflag=1")
										  ->where("a.centeractive=1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;	
       	
       }
       
	
	public function  fngetschedulerofsessionstudent($iddates,$idprogrammaster)
       {
       	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array(""))
										  ->join(array("m"=>"tbl_newschedulercourse"),'a.idnewscheduler=m.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_managesession"),'a.idsession=f.idmangesession',array("key"=>"f.idmangesession","value"=>"f.managesessionname"))
                                              ->where("a.Active = 1")
									      ->where("a.date=?",$iddates)
                                          ->where("a.centeractive=1")
                                            ->where("m.IdProgramMaster=?",$idprogrammaster)
										  ->where("a.Reserveflag=1")
										   ->group("f.idmangesession");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       	
       }
       
       
		
	public function  fnGetvalidateRemainingseats($iddates,$idExamvenue,$idsession)
       {
       	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("(a.Totalcapacity-Allotedseats) as rem"))
									      ->where("a.Active = 1")
									      ->where("a.date=?",$iddates)
                                          ->where("a.centeractive=1")
										  ->where("a.Reserveflag=1")
										  ->where("a.idvenue=?",$idExamvenue)
										  ->where("a.idsession=?",$idsession);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
       	
       }
       
       
       
   public function fncheckSuperUserPwd($password)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $lobjDbAdpt->select()
						->from(array("a" => "tbl_user"))
						->join(array('b' => 'tbl_definationms'),'a.IdRole = b.idDefinition')
						->where("a.passwd =?",$password)
						->where("b.DefinitionDesc = 'Superadmin'");
						//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
     

	
	
	public function fnGetidschdeler($iddates,$idExamvenue,$idsession)
	{
		
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.idnewscheduler"))
									      ->where("a.Active = 1")
									      ->where("a.date=?",$iddates)
                                          ->where("a.centeractive=1")
										  ->where("a.Reserveflag=1")
										  ->where("a.idvenue=?",$idExamvenue)
										  ->where("a.idsession=?",$idsession);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
		
		
	}
	 public function fncheckicnivalidate($idprog,$icno)
	{
		
		/*$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $lobjDbAdpt->select()
						  ->from(array("a" =>"tbl_studentapplication"),array('a.ICNO'))
						->where("a.ICNO =?",$icno)
						->where("a.Payment=1")
						->where("a.pass=1")
						->where("a.Program = ?",$idprog);
						//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;*/
		
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $select =
"SELECT a.pass
FROM tbl_studentapplication a
WHERE a.IDApplication =(select max(IDApplication) from tbl_studentapplication where pass=1 
AND ICNO =$icno
AND Program=$idprog)
UNION
SELECT a.pass
FROM tbl_studentapplication a
WHERE a.IDApplication =(select max(IDApplication) from tbl_studentapplication where Payment=1 
AND ICNO =$icno
AND Program=$idprog
and pass in (3,4))
UNION
SELECT a.pass
FROM `tbl_studentapplication` a
WHERE a.IDApplication = (select max(IDApplication) from tbl_studentapplication
where ICNO =$icno
AND Program=$idprog
AND Examvenue!=000
and pass=3)";
//echo $select;die();
	$result = $lobjDbAdpt->fetchRow($select);
	return $result;
		
	}
	
		
	     public function fngetstudentvalidatealloted($examdate,$examvenue,$examsession)
     {
     	
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.Allotedseats"))
                                          ->where("a.Active = 1")
										  ->where("a.date=?",$examdate)
										  ->where("a.idvenue=?",$examvenue)
										  ->where("a.idsession=?",$examsession); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     } 
     
     
	   public function fngetstudentdecrease($studenteditresult)
   {
   	$examdate=$studenteditresult['DateTime'];
   	$examvenue=$studenteditresult['Examvenue'];
   	$examsession=$studenteditresult['Examsession'];
   	
          $db = Zend_Db_Table::getDefaultAdapter();
    $larrformData1 = array("Allotedseats"=>new Zend_Db_Expr("Allotedseats-1"));	
    $where = 	$db->quoteInto('idvenue = ?',$examvenue).
      				$db->quoteInto(' AND date = ?',$examdate). 	
      				$db->quoteInto(' AND idsession = ?',$examsession); 	 
    $db->update('tbl_venuedateschedule',$larrformData1,$where);
   
   
   }
   
   
   public function fngetstudentincrease($studenteditresult)
   {
   	$examdate=$studenteditresult['DateTime'];
   	$examvenue=$studenteditresult['Examvenue'];
   	$examsession=$studenteditresult['Examsession'];
   	
   	$db = Zend_Db_Table::getDefaultAdapter();
    $larrformData1 = array("Allotedseats"=>new Zend_Db_Expr("Allotedseats+1"));	
    $where = 	$db->quoteInto('idvenue = ?',$examvenue).
      				$db->quoteInto(' AND date = ?',$examdate). 	
      				$db->quoteInto(' AND idsession = ?',$examsession); 	 
    $db->update('tbl_venuedateschedule',$larrformData1,$where);
		
   	
   }
   
	   public function fngetactivebatchprg($idprog)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        	$select = $db->select()
		            		->from(array('b'=>'tbl_batchmaster'),array("b.IdBatch"))
		            		->join(array('c'=>'tbl_tosmaster'),'b.IdBatch=c.Idbatch',array("c.*"))
		            		->where("c.Active=1")
		           		    ->where("b.Idprogrammaster=?",$idprog);
		          
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
      	     public function fngetstudeicnosinformation($IDApplication,$ICNO,$Program)
     {
     	
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_studentapplication"),array("a.*"))
                                          ->where("a.ICNO = ?",$ICNO)
										  ->where("a.Program=?",$Program)
										  ->where("a.Examvenue!=000")
										  ->where("a.Examsession!=000")
										   ->where("a.IDApplication > ?",$IDApplication); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     } 
     
	
 public function fngetdayofdatemonth($iddate)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	  $sql = "select month('$iddate') as month"; 
				//echo $sql; die();
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
	
          	     public function fngetstudeicnoscurrentdatevalid($IDApplication)
     {
     	
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_studentapplication"),array("a.*"))
										  ->where("a.DateTime=curdate()")
										  ->where("a.Examvenue!=000")
										  ->where("a.Examsession!=000")
										   ->where("a.IDApplication = ?",$IDApplication); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     } 
     
     
     
             	     public function fngetstudeicnoscurrentstartvalid($IDApplication)
     {
     	
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_studentmarks"),array("a.IDApplication"))
										  //->where("a.DateTime=curdate()")
										//  ->where("a.Examvenue!=000")
										 // ->where("a.Examsession!=000")
										   ->where("a.IDApplication = ?",$IDApplication); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     } 
     
     
     
                    	     public function fngetstudeicnoscurrentdatetime($IDApplication)
     {
     	
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication"))
										  //->where("a.DateTime=curdate()")
										//  ->where("a.Examvenue!=000")
										 // ->where("a.Examsession!=000")
										   ->where("a.VenueTime =1"); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     } 
	
}
