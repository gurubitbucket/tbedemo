<?php
	class Examination_Model_Exambulkdatechange extends Zend_Db_Table {
		
		
		protected $_name = 'tbl_studentapplication';
	

 public function fngetstudentinformation($maxtimes)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("f" =>"tbl_studentpaymentoption"),'f.IDApplication=a.IDApplication',array('f.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->joinleft(array("e" =>"tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array('e.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->where("a.pass=3")
										  ->where("a.Venue<=?",$maxtimes)
										  ->group("d.IDApplication");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
		
     
     
     
     
	 public function fnSearchStudent($larrformData,$maxtimes)
     {
     	
     
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										    // ->join(array("f" =>"tbl_studentpaymentoption"),'f.IDApplication=a.IDApplication',array('f.*'))
										    ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										    ->joinleft(array("e" =>"tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array('e.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										    ->join(array("n"=>"tbl_managesession"),'a.Examsession=n.idmangesession',array('n.managesessionname'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										   ->where("a.Venue<=?",$maxtimes)
										  ->where("a.pass in (3,4)")
										    ->where("a.IDApplication>1148")
										   ->where("a.Examvenue!=000")
										  ->group("d.IDApplication")
										   ->order("a.FName");	
										  
													  
				if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$larrformData['Coursename']);
				if($larrformData['Venues']) $lstrSelect->where("c.idcenter = ?",$larrformData['Venues']);
				if($larrformData['newvenues']) $lstrSelect->where("n.idmangesession = ?",$larrformData['newvenues']);
				
			
                if(isset($larrformData['Date']) && !empty($larrformData['Date']))
                {
				  $lstrFromDate = $larrformData['Date'];
				  if($lstrFromDate==date('Y-m-d'))
				  {
				  	  $lstrSelect = $lstrSelect->where("a.DateTime = '2010-02-30'");
				  	  // $lstrSelect = $lstrSelec  ->where("a.pass in (3,4)");
				  }
				  else 
				  {
				  	 if($lstrFromDate<date('Y-m-d'))
				  {
				  $lstrSelect = $lstrSelect->where("a.DateTime = '$lstrFromDate'");
				  $lstrSelect = $lstrSelect->where("a.pass in (4)");
				  }
				   	 if($lstrFromDate>date('Y-m-d'))
				  {
				  $lstrSelect = $lstrSelect->where("a.DateTime = '$lstrFromDate'");
				  $lstrSelect = $lstrSelect->where("a.pass in (3)");
				  }
				  
				  }
				  }					  
		    // echo $lstrSelect;die();
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     
     
     
     
     
     
     
  
     
	 public function  fngetstudentinformationfromconfig()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_config"),array("a.studedit","a.ClosingBatch"))
										  ->where("a.idConfig =1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }
     
     
		 public function fnGetVenueNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
				 				// ->join(array("c"=>"tbl_newschedulervenue"),'c.idvenue=a.idcenter',array(''))	
				 				// ->join(array("e"=>"tbl_newscheduler"),'e.idnewscheduler=c.idnewscheduler',array(''))
				 				// ->where("e.Active =1")			 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
	   
	
		 public function fnGetSessionforthemonth($Program,$Year,$Newvenue)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
			                       ->from(array("e"=>"tbl_newscheduler"),array('e.*'))
			                      ->join(array("r"=>"tbl_newschedulercourse"),'r.idnewscheduler=e.idnewscheduler',array(''))
			                      ->join(array("c"=>"tbl_newschedulervenue"),'c.idnewscheduler = e.idnewscheduler',array(''))
			                      ->where("e.Year=?",$Year)	
				 				  ->where("e.Active =1")			 
				 				  ->where("r.IdProgramMaster=?",$Program)
				 				  ->where("c.idvenue=?",$Newvenue);
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
	
		 public function fnGetExectSession($values,$days)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
			                       ->from(array("e"=>"tbl_newschedulersession"),array(''))
			                      ->join(array("r"=>"tbl_managesession"),'r.idmangesession=e.idmanagesession',array('r.*'))
			                         ->join(array("m"=>"tbl_newschedulerdays"),'m.idnewscheduler=e.idnewscheduler',array(''))
			                      ->where("e.idnewscheduler in ($values)")
			                        ->where("m.Days = ?",$days)
			                      ->group("r.idmangesession");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
	 public function fnGetExectSessiondays($values)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
			                       ->from(array("e"=>"tbl_newschedulerdays"),array(''))
			                      ->join(array("r"=>"tbl_days"),'r.iddays = e.Days',array('r.iddays'))
			                      ->where("e.idnewscheduler in ($values)")
			                      ->group("r.Days");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
		 public function fnGetStudentNames($maxtimes)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.FName","value"=>"a.FName"))
				 				  ->where("a.Payment=1")
										   ->where("a.VenueChange<=?",$maxtimes)
										  ->where("a.pass=3")
										  ->group("a.FName")
										    ->order("a.FName")	;	 				 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	 public function fnGetTakafulNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName")) 				 
				 				 ->order("a.TakafulName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
     public function fnGetCourseNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
		
	   
	    public function fnGetSchedulerDetails()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Description")) 				 
				 				 ->order("a.Year");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
		public function fnGetStateName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
										   ->where("a.idCountry  = ?","121")
										   ->order("a.StateName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			public function fnGetCityName($idcity){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_city"),array("key"=>"a.idCity","value"=>"a.CityName"))
										   ->where("a.idCity  = ?",$idcity);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			
			
		public function fnGetVenueName($idcity){	
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
										   ->where("a.city = ?",$idcity);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
		public function fnGetSchedulerSessionDetails($idschdulersesion){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
										   ->where("a.idmangesession = ?",$idschdulersesion);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			public function fnGetSchedulerYearDetails($idsch)
			{
				
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.year"))
										   ->where("a.idnewscheduler = ?",$idsch);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
			}
			
	public function fnGetYearlistforcourse($idprog){		
			

$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Description"))
										  //->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										    ->join(array("d"=>"tbl_newmonths"),"a.From=d.idmonth",array())
										  ->join(array("e"=>"tbl_newmonths"),"a.To=e.idmonth",array())
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())

                                          ->where("a.Active   = 1")
										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;





	}
	
	
	
	public function fnGetStatelistforcourse($idprog,$idyear){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->group("e.idState")
										  ->where("a.idnewscheduler=?",$idyear);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetCitylistforcourse($idstate,$idprog,$idseched){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array("key"=>"e.idCity","value"=>"e.CityName"))
										  ->where("e.idState  = ?",$idstate)
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler  = ?",$idseched)
										  ->group("e.CityName");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetVenuelistforcourse($idcity,$idprog,$idseched){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array())
										  ->where("d.city = ?",$idcity)
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler  = ?",$idseched)
										  ->group("d.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnbetweenmonths($id)
{
$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
										  ->where("a.idnewscheduler=?",$id);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	
}


	public function fnGetmonthsbetween2($tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >=(SELECT month(curdate( ))) and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
		public function fnGetmonthsbetweenvalid($expiredmonth,$tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >= $expiredmonth and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
	public function fnGetmonthsbetween($frommonth,$tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >='$frommonth' and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
	
	
	   public function fngetschedulerexception($days,$lintcity)
   {
   	  $db 	= 	Zend_Db_Table::getDefaultAdapter();	
	$lstrSelect = $db->select()
	                         ->from(array("a" =>"tbl_schedulerexception"))
	                         ->where('a.Date=?',$days)
	                         ->where('a.idcity=?',$lintcity);
	    $larrResult = $db->fetchRow($lstrSelect);
	    return $larrResult;
   	   
   }
   
	 public function fnGetVenuedetailsgetsecid($idsech)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
						                  ->where("a.Active=1")
										  ->where("a.idnewscheduler =?",$idsech); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
  
	  public function fnGetsesssiondetails($year,$city,$venue,$idsession,$day,$month)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler')
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession',array("key"=>"g.idmangesession","value"=>"g.managesessionname"))
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter')
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array())
										 // ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										   ->where("a.idnewscheduler =?",$year)
										     ->where("b.idvenue =?",$venue)
										     ->where("f.idmanagesession not in (select Examsession from  tbl_studentapplication where  Examsession in ($idsession) and ExamCity=$city and Examdate=$day and Examvenue=$venue and Year=$year)")
										   ->group("g.idmangesession");
									
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
	 public function fngetyearforthescheduler($id)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
										  ->where("a.idnewscheduler=?",$id);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
		 public function fngetstudentoldinfo($idapplication)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										  ->where("a.IDApplication =?",$idapplication);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  	

public function fnGetvenuecity($idvenue)
{
	
	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
 		$lstrSelect = "SELECT  * FROM tbl_center where idcenter=$idvenue";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
  	return $larrResult;
}


	
   

       
	  public function newfnGetcitydetailsgetsecid($venue)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_center"),array("a.city"))
						                  ->where("a.Active=1")
										  ->where("a.idcenter =?",$venue); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
 



 public function fngetcountofsessions($NewCity,$year)
    {
       $db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_newschedulervenue"),array("a.idvenue"))
			         ->join(array("b"=>"tbl_newscheduler"),'a.idnewscheduler=b.idnewscheduler',array(""))
			         ->join(array("c"=>"tbl_newschedulersession"),'b.idnewscheduler=c.idnewscheduler',array("COUNT(c.idmanagesession) as countidmanagesession","c.idnewscheduler"))
			          ->join(array("d"=>"tbl_newschedulerdays"),'a.idnewscheduler=d.idnewscheduler',array("d.*"))
                     ->where("a.idvenue =?",$NewCity)
                     ->where("b.Active =1")
                     ->where("b.Year =?",$year)
                     ->group("d.days"); 
	$result = $db->fetchAll($select);
	return $result;
    } 
  
    
    
	public function newfngetyear($prog)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect="select `year`as `key` , `year` as `value` from tbl_newscheduler where idnewscheduler in (Select c.idnewscheduler from tbl_newschedulercourse as c,tbl_venuedateschedule as d where c.idnewscheduler=d.idnewscheduler and  c.idprogrammaster=$prog and d.date >=curdate()) and active=1 group by `year`";
	//echo $lstrSelect;die();
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
}
  

	
    public function  newfnstudentinfos($values)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*","year(a.DateTime) as yearss"))
										  ->where("a.IDApplication in ($values)");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }
     
     
	public function newfnGetCitylistforcourse($idprog,$idyear){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler',array())
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler',array())
										  ->join(array("k"=>"tbl_venuedateschedule"),'a.idnewscheduler=k.idnewscheduler',array())
										  ->join(array("d"=>"tbl_center"),'k.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("CAST(a.Year AS SIGNED)  = ?",$idyear)
										  ->where("k.date >=curdate()")
										  ->where("a.Active  = 1")
										  ->where("k.Active=1")
										  ->group("d.idcenter")
										  ->order("d.centername");
										  //echo $lstrSelect;die();
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
		
	public function fnGetSessionforthesechedulers($Program,$Year,$Newvenue)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				   $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("count(a.idsession) as total"))
										  ->join(array("b"=>"tbl_newschedulercourse"),'a.idnewscheduler=b.idnewscheduler',array(""))
										  ->where("a.Active = 1")
										  ->where("a.Active = 1")
                                          ->where("b.IdProgramMaster=?",$Program)
                                          ->where("a.idvenue=?",$Newvenue)
                                          ->where("year(a.date) =  ?",$Year)
                                       	  ->where("a.date >curdate()")
                                          ->group("a.date");          
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		
		
	}
	
	
	public	function fnvalidateseats($venue,$selecteddate)
	{
	  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  	$select = "SELECT Allotedseats,Totalcapacity from  tbl_venuedateschedule where idvenue =$venue and Active=1 AND Reserveflag=1  AND centeractive=1 AND date ='$selecteddate'";
		return	$result = $lobjDbAdpt->fetchAll($select);
	}
	
	public function fngetdayStudent($dates){	
	//echo $dates;die();
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
				 $select = "select DAYNAME('$dates') as days"; 
			
				$result = $lobjDbAdpt->fetchAll($select);
				return $result;
			}
			
	public function fnGetProgAmount($idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT sum(abc.amount) from 

(select Rate as amount from tbl_programrate where `EffectiveDate` in 
(SELECT max(a.EffectiveDate)  from tbl_programrate a,tbl_accountmaster b
where a.idProgram= $idprog and 
 b.idAccount= a.IdAccountmaster and  
 b.Active=1 and a.Active=1 and b.idAccount not in (select t.idAccount from tbl_accountmaster t where t.duringRegistration=1 ) group by a.IdAccountmaster)and idProgram=$idprog and Active=1   and IdAccountmaster not in (select r.idAccount from tbl_accountmaster r where r.duringRegistration=1 ))as abc ";
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	
	   public function fngetstudentbulkdecrease($examdate,$examvenue,$examsession,$totalseat)
   {

   	
          $db = Zend_Db_Table::getDefaultAdapter();
    $larrformData1 = array("Allotedseats"=>new Zend_Db_Expr("Allotedseats-$totalseat"));	
    $where = 	$db->quoteInto('idvenue = ?',$examvenue).
      				$db->quoteInto(' AND date = ?',$examdate). 	
      				$db->quoteInto(' AND idsession = ?',$examsession); 	 
    $db->update('tbl_venuedateschedule',$larrformData1,$where);
   
   
   }
   
   
   public function fngetstudentbulkincrease($examdate,$examvenue,$examsession,$totalseat)
   {
   	
   	$db = Zend_Db_Table::getDefaultAdapter();
    $larrformData1 = array("Allotedseats"=>new Zend_Db_Expr("Allotedseats+$totalseat"));	
    $where = 	$db->quoteInto('idvenue = ?',$examvenue).
      				$db->quoteInto(' AND date = ?',$examdate). 	
      				$db->quoteInto(' AND idsession = ?',$examsession); 	 
    $db->update('tbl_venuedateschedule',$larrformData1,$where);
		
   	
   }
   
   public function newfnstudentbulkinfos($values)
   {
   
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										  ->where("a.IDApplication in ($values)");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult; 	
   
   }
   
   public function fninserttempvenuechange($larresult,$iduser,$oldprogamt,$larrformData,$selecteddate,$idcity,$idstate,$idsche)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   for($i=0;$i<count($larresult);$i++)
		   {
		   	  $venue = $larresult[$i]['Venue'];
		   	  $venue++;
		    	$table = "tbl_venuechange";
		    	if($larresult[$i]['IDApplication']<1)
		    	{
		    		$larresult[$i]['IDApplication']=0;
		    	}
				$postData = array(		
									'IDapplication' =>$larresult[$i]['IDApplication'],	
		           					'UpdDate' =>date('Y-m-d H:i:s'), 
									'UpdUser' =>$iduser,	
									'DateTime'=>$larresult[$i]['DateTime'],	
				  					'ExamState'=>$larresult[$i]['ExamState'],
				  					'ExamCity'=>$larresult[$i]['ExamCity'],
				  					'year'=>$larresult[$i]['Year'],
				  					'ExamDate'=>$larresult[$i]['Examdate'],
				  					'ExamMonth'=>$larresult[$i]['Exammonth'],
				  					'Examvenue'=>$larresult[$i]['Examvenue'],	
				  					'ExamSession'=>$larresult[$i]['Examsession'],	
				                    'oldupddate'=>$larresult[$i]['UpdDate'],
				                    'NewExamDate'=>$selecteddate,
				                    'NewExamVenue'=>$larrformData['newexamcity'],
				                    'NewExamSession'=>$larrformData['idsession'],
				                    'oldprogramid'=>$larresult[$i]['Program'],
									'newprogramid'=>$larresult[$i]['Program'],
				                    'oldamount'=>$oldprogamt,
				                    'newamount'=>$oldprogamt,
				                    'prgcahngeflag'=>0,
				                    'amtchangeflag'=>0,
				                    
								);
								
								$result=  $lobjDbAdpt->insert($table,$postData);
								
								
								
						 $postDataa = array(
		                    'UpdUser' =>$iduser,
							'DateTime' =>$selecteddate,	
		  					'ExamState'=>$idstate,
		  					'ExamCity'=>$idcity,
						    'Venue'=>$venue,
						    'VenueTime'=>0,
		  					'Year'=>$idsche,
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		                   	'Examvenue'=>$larrformData['newexamcity'],	
		  					'Examsession'=>$larrformData['idsession'],
						    'pass'=>'3'	  	  
						);		
							$where['IDApplication = ? ']= $larresult[$i]['IDApplication'];		
					 $lobjDbAdpt->update('tbl_studentapplication',$postDataa,$where);	

					 
					 $db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData1 = array(
							'Cetreapproval'=>'0',	  
						);
		$where1['IDApplication = ? ']= $larresult[$i]['IDApplication'];			
		 $db->update('tbl_registereddetails', $postData1, $where1);
								
								
		   }
   }
   
		public function fnGetidschdeler($iddates,$idExamvenue,$idsession)
	{
		
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.idnewscheduler","(a.Totalcapacity-a.Allotedseats) as rem"))
									      ->where("a.Active = 1")
									      ->where("a.date=?",$iddates)
                                          ->where("a.centeractive=1")
										  ->where("a.Reserveflag=1")
										  ->where("a.idvenue=?",$idExamvenue)
										  ->where("a.idsession=?",$idsession);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
		
		
	}
	
	   public function  fnGetschdelerdates2($iddates,$idprogrammaster)
       { 	
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array(""))
							 			  ->join(array("f"=>"tbl_center"),'a.idvenue=f.idcenter',array("key"=>"f.idcenter","value"=>"f.centername"))
							 			    ->join(array("m"=>"tbl_newschedulercourse"),'a.idnewscheduler=m.idnewscheduler',array(""))
                                          //->where("a.Active = 1")
                                          ->where("a.date=?",$iddates)
                                         // ->where("a.centeractive=1")
                                            ->where("m.IdProgramMaster=?",$idprogrammaster)
										 // ->where("a.Reserveflag=1")
										    ->group("f.idcenter")
										    ->order("centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
       	
       }
       
	public function  fngetschedulerofsessionstudent($iddates,$idprogrammaster)
       {
       	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array(""))
										  ->join(array("m"=>"tbl_newschedulercourse"),'a.idnewscheduler=m.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_managesession"),'a.idsession=f.idmangesession',array("key"=>"f.idmangesession","value"=>"f.managesessionname"))
                                              ->where("a.Active = 1")
									      ->where("a.date=?",$iddates)
                                         //->where("a.centeractive=1")
                                            ->where("m.IdProgramMaster=?",$idprogrammaster)
										//  ->where("a.Reserveflag=1")
										   ->group("f.idmangesession");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       	
       }
       
       public function newfnGeticno($idapp)
       {
       		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.ICNO"))
										  ->where("a.IDApplication = ?",$idapp);
										  
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult; 
       	
       }
       
       public function newfnGeticnovalidationagain($idapp,$Program,$ICNO)
       {
       	
       		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.ICNO"))
										  ->where("a.IDApplication > ?",$idapp)
										  ->where("a.Program = ?",$Program)
										  ->where("a.ICNO=?",$ICNO)
										  ->Where("a.pass=3");				  
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult; 
       	
       }

	
   
		
}
