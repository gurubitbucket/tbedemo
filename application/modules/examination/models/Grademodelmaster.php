<?php
     class Examination_Model_Grademodelmaster extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	
	        protected $_name = 'tbl_groupnames';
	
            //function to add the template


			public function fnupdatemismatchgardeset($id,$flag) 
            {             
	        	$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(
							'flag'=>$flag
							
						);
		$where['Idgradeset = ? ']= $id;		
		 $db->update('tbl_gradesetmaster', $postData, $where);
		 
		 
            }
			

            public function fnaddgradeset($post)
	        {	 
	       	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_gradesetmaster";
		$postData = array(		
							
           					'Gradesetautoid' => $post['gradedescription'], 
							'Effectivedate' =>$post['Effectidate'],	
							'Active'=>'1',	
							'Upduser'=>$post['Upduser'],
		  					'Upddate'=>$post['Upddate'],
							'Gradesetname' =>$post['gradesetname'],	
	'flag'=>'1',	
		                      
						);
						
						$result=  $lobjDbAdpt->insert($table,$postData);
	  $lastid  = $lobjDbAdpt->lastInsertId("tbl_gradesetmaster","Idgradeset");	
	  
	  
	  for($gra=0;$gra<count($post['gradename']);$gra++)
	  
	  {
	  	
	  	$table1 = "tbl_gradesetdetails";
		$postData1 = array(		
							'Idgradeset' =>$lastid,	
           					'Gradename' => $post['gradename'][$gra], 
							'Percentagefrom' =>$post['Marksfrom'][$gra],	
							'Percentageto'=>$post['Marksto'][$gra]	
		                      
						);
						
					  $lobjDbAdpt->insert($table1,$postData1);
	  }
						   return $lastid;
	        	
	        	
	        	
	        }
	        
	        
          public function  fngetgrdaeslist()
	        {
	        	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_grades"),array("key"=>"a.Gradename","value"=>"a.Gradename"));
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	        }
	        
	        public function fninactiveprasentgarde($gradename)
	        {
	    /*    	$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(
							'Active'=>'0',
						);
		//$where['idgrademaster =?']= $gradename;	
		$where = "idgrademaster = '".$gradename."'"; 
	//	print_r($postData);
		// echo ($where);die();
		 $db->update('tbl_grademaster', $postData, $where);
		 */
		 
		 $db = Zend_Db_Table::getDefaultAdapter();
    	  
					$postData1 = array(
							'Active'=>'0',
						);
	 $where1 = "gradename='".$gradename."'"; 
	     $db->update('tbl_grademaster',$postData1,$where1);
		 
	        
	        }
	        
	        public function fncheckgrade($gradename)
	        {
	        	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_grademaster"),array("a.gradename"))
			                ->where("a.gradename='$gradename'");
			            
			           //  echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchRow($lstrSelect);
		     return $result;
	        	
	        	
	        }
	        
         public function fncheckstartgardeset($gradename,$lastid)
	        {
	        	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_gradesetdetails"),array("a.*"))
			                ->where("a.Gradename='$gradename'")
			             ->where("a.Idgradeset=?",$lastid)
			                ->where("a.Percentagefrom=0");
			               
			            
			           // echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchRow($lstrSelect);
		     return $result;
	        	
	        	
	        }
	        
	        
	        
         public function fncheckendgardeset($lastid)
	        {
	        	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_gradesetdetails"),array("a.*"))
			               // ->where("a.Gradename='$gradename'")
			                  ->where("a.Idgradeset=?",$lastid)
			                ->where("a.percentageto=100");
			               // ->where("a.Active=1");
			            
			           // echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchRow($lstrSelect);
		     return $result;
	        	
	        	
	        }
	        
	    	 public function fngetallgardeset($lastid)
	        {
	        	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_gradesetdetails"),array("a.*"))
			                ->where("a.Idgradeset=?",$lastid)
			                ->order("a.Percentagefrom");
			            
			           //  echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchAll($lstrSelect);
		     return $result;
	        	
	        	
	        }
	        
	    
      
	        //Function to get the webpagetemplate details 	
            public function fngetgradesetlist()
            {             
   	           	    	  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_gradesetmaster"),array("a.*","DATE_FORMAT(Effectivedate,'%d-%m-%Y') as effectivdate"));
			               // ->where("Active=1")
			               // ->order("a.gradename desc");
			            
			           //  echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchAll($lstrSelect);
		     return $result;
            }
 	
            //Function for searching the author details	
           public function fnSearchgradeset($post = array())
	       { 
	       		 //$field7 = "Active = ".$post["field7"];	 
	       	    	  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         $lstrSelect = $lobjDbAdpt->select()
			                ->from(array("a" =>"tbl_gradesetmaster"),array("a.*","DATE_FORMAT(Effectivedate,'%d-%m-%Y') as effectivdate"))
			                ->where('a.Gradesetname  like "%" ? "%"',$post['field2']);
			                  // ->where($field7)
			                    // ->order("a.gradename desc"); 
			           //  echo $lstrSelect;die();
		     $result = $lobjDbAdpt->fetchAll($lstrSelect);
		     return $result;
	       }
	       
	        public function fneditgradeset($id)     
			{		      		
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect =$lobjDbAdpt->select()
									->from(array('a'=>'tbl_gradesetmaster'),array('a.*'))
									->where('Idgradeset='.$id );
								//	echo $lstrSelect;die();
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
			} 
	       
			
			public function fneditgradesetdetails($id)
			{
				 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect =$lobjDbAdpt->select()
									->from(array('a'=>'tbl_gradesetdetails'),array('a.*'))
									->where('a.Idgradeset=?',$id )
									->order('a.Gradename desc');
								//	echo $lstrSelect;die();
			 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			 return $larrResult;
			
			}
			public function fnupdategradeset($id,$udata) 
            {             
	        	$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(
							'Gradesetname'=>$udata['gradesetname'],
							'Effectivedate'=>$udata['Effectidate'],
							'Active'=>$udata['Active'],
		              		'Upduser'=>$udata['Upduser'],
		  					 'Upddate'=>$udata['Upddate'],
						);
		$where['Idgradeset = ? ']= $id;		
		 $db->update('tbl_gradesetmaster', $postData, $where);
		 
		 
		 
		/*$postData2 = array(
							'Gradesetname'=>$udata['gradesetname'],
							'Effectivedate'=>$udata['Effectidate'],
							'Active'=>$udata['Active'],
						);
		$where2['Idgradeset = ? ']= $id;		
		 $db->update('tbl_gradesetmaster', $postData2, $where2);*/
		 
		
 		$select = "Delete FROM  tbl_gradesetdetails  where Idgradeset='$id'";
 		$db->query($select);
 		
 		
 		  
	  for($gra=0;$gra<count($udata['gradename']);$gra++)
	  
	  {
	  	
	  	$table1 = "tbl_gradesetdetails";
		$postData1 = array(	'Idgradeset' =>$id,	
           					'Gradename' => $udata['gradename'][$gra], 
							'Percentagefrom' =>$udata['Marksfrom'][$gra],	
							'Percentageto'=>$udata['Marksto'][$gra]	
		                      
						);
						
					  $db->insert($table1,$postData1);
	  }
					
		 
            }

       public function getactivesets(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_tosmaster'),array('a.IdTOS'))
								->where("a.Active=1");
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}
		
		public function fnGetvalidategarde($gradname,$gradval){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_grademaster'),array('a.gradename'))
								->where("a.percentagefrom>=?",$gradval)
										->where("a.percentageto<=?",$gradval)
								->where("a.Active=1");
				$result = $lobjDbAdpt->fetchRow($select);	
				return $result;
			
		}
		
         public function gettossection($values,$groupname){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_tosdetail'),array('a.*'))
								->where("a.IdTOS in ($values)")
								->where('a.IdSection  like ? "-%"',$groupname);
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}
		
        public function getgroupsection($idgroupname){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_questions'),array('a.QuestionGroup'))
								->where("a.QuestionType=?",$idgroupname)
								->where("a.Active=1");
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}
		
	         //Function To Get Pagination Count from Initial Config
	       public function fnGetPaginationCountFromInitialConfig()
	       {
		       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		       $lintPageCount = "";
		       $lstrSelect = $lobjDbAdpt->select()
								        ->from(array("a"=>"tbl_config"),array("noofrowsingrid") );
		       $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		       if($larrResult['noofrowsingrid'] == "" || $larrResult['noofrowsingrid'] == "0")
		       {
			    $lintPageCount = "5";
		       }
		       else
		       {
			    $lintPageCount = $larrResult['noofrowsingrid'];
		       }
		       return $lintPageCount;
	       }
	
	       //Function to Get Initial Config Data
	       public function fnGetInitialConfigData()
	       {
		    $lobjDbAdpt=Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect=$lobjDbAdpt->select()
								   ->from(array("a"=>"tbl_definationms"),array("LCASE(SUBSTRING(a.DefinitionCode,1,2)) AS Language") )
					 			   ->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType',array())
				 				   ->join(array("c"=>"tbl_config"),'c.Language = a.idDefinition',array("c.HtmlDir","c.DefaultCountry"));		  
		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		    return $larrResult;
	       }	
        }    
		  	 
	
