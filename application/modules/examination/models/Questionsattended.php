<?php
	class Examination_Model_Questionsattended extends Zend_Db_Table {


     

     public function fngetprogdetails($prog,$date)
     {
     	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	
		  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_programmaster"),array("a.*"))
							 			 ->join(array("b" => "tbl_batchmaster"),'a.IdProgrammaster=b.IdProgrammaster',array("b.*"))
							 			 ->join(array("t" => "tbl_tosmaster"),'b.IdBatch=t.IdBatch',array("t.*"))
							 			 ->join(array("c" => "tbl_batchdetail"),'b.IdBatch=c.IdBatch',array("c.*"))	
							 			 ->join(array("d" => "tbl_questions"),'c.IdPart=d.QuestionGroup',array("d.*"))							 			 							 			 
                                         ->where("t.Active=1")
                                        // ->where("d.Active=1")
                                         ->where("a.IdProgrammaster=?",$prog)
                                         ->where("d.Active=1")
                                         ->group("d.idquestions");
                                         
      if($date!=0)
     	 {
     	 	$lstrSelect->where("d.QuestionLevel = ?",$date);
     	 }
     	 				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     public function  fngetpartdetails($post,$date)
     {
     	
     	$grouptype=$post['grouptype'];
     	  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	
		  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_batchmaster"),array("a.*"))
							 			 ->join(array("b" => "tbl_batchdetail"),'a.IdBatch=b.IdBatch',array("b.*"))
							 			 ->join(array("t" => "tbl_tosmaster"),'a.IdBatch=t.IdBatch',array("t.*"))
							 			 ->join(array("d" => "tbl_questions"),'b.IdPart=d.QuestionGroup',array("d.*"))							 			 							 			 
                                         ->where("t.Active=1")
                                         ->where("d.Active=1")
                                         ->where("b.IdPart='$grouptype'")
                                        // ->where("d.Active=1")
                                         ->group("d.idquestions");
                                         
      if($date!=0)
     	 {
     	 	$lstrSelect->where("d.QuestionLevel = ?",$date);
     	 }
     	 
       if(!empty($post['Program']))
     	 {
     	 	$lstrSelect->where("a.IdProgrammaster = ?",$post['Program']);
     	 }
     	// echo $lstrSelect;die();
     	 				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     	
     }
     
     
     
     public function fngetprog()
     {
     			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("b" => "tbl_programmaster"),array("key"=>"b.IdProgrammaster","value"=>"b.ProgramName"));
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
	  public function fngetquestiongrp()
     {
     			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("b" => "tbl_batchdetail"),array("key"=>"b.IdPart","value"=>"b.IdPart"))
										  ->group("IdPart");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
	 public function fnGetQtnsapperedinexam($Questionid,$maxids){

 		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 
 		 $select = "
 		 SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '$Questionid,%'
Union 
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid' 
Union
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid,%'";

 		// echo $select;die();
		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		

		
		
		public function fnmaxids($prog,$fromdate,$todate)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		    $select = "SELECT MAX(idquestionsetforstudents) as Appeared,Idapplication
					 FROM  `tbl_questionsetforstudents` where idapplication in (		  
 		  			 SELECT b.idapplication 
						FROM tbl_studentapplication AS b
						WHERE b.Program =$prog
						AND b.DateTime >='$fromdate'
						AND b.DateTime <='$todate'
					AND b.payment=1 and b.pass in(1,2)) group by idapplication";
 		   // echo $select;die();
 		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		
		public function fnmaxidsbybatch($idbatches,$idapplicationss,$fromdate,$todate)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		    $select = "SELECT MAX(idquestionsetforstudents) as Appeared,Idapplication
					 FROM  `tbl_questionsetforstudents` where idapplication in ($idapplicationss)		  
 		  		and IdBatch in ($idbatches)
						 group by idapplication";
 		  //  echo $select;die();
 		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		
  public function  fngetprgdetails($idapps)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		    $select = "SELECT count(a.IDApplication) as cnt,b.ProgramName,b.IdProgrammaster
					 FROM  tbl_studentapplication as a,tbl_programmaster as b where a.IDApplication in ($idapps)	 and a.Program=b.IdProgrammaster 	  
						 group by b.IdProgrammaster";
 		   // echo $select;die();
 		 return	$result = $lobjDbAdpt->fetchAll($select);	
  }
		public function fngetbatchidbyparts($post)
		{ 
			 $grouptype=$post['grouptype'];
            	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	
		  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_batchmaster"),array("a.IdBatch"))
							 			 ->join(array("b" => "tbl_batchdetail"),'a.IdBatch=b.IdBatch',array(""))
							 			 ->join(array("t" => "tbl_tosmaster"),'a.IdBatch=t.IdBatch',array(""))			 
                                         ->where("t.Active=1")
                                         ->where("b.IdPart='$grouptype'");
		  if(!empty($post['Program']))
     	 {
     	 	$lstrSelect->where("a.IdProgrammaster = ?",$post['Program']);
     	 }
                                      //  echo $lstrSelect;die();
                                        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 		 	
		}
		
		public function fngetregids($studentid)
		{
             $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_registereddetails"),array("a.Regid"))
                                         ->where("a.Idapplication in ($studentid)")
                                         ->where("a.Approved = 1");
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);	
 		 	
		}
		
	 public function fngetidapplicationsstudent($IDApplication,$fromdate,$todate,$idbatches)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		    $select = "	SELECT b.idapplication 
						FROM tbl_studentapplication AS b,tbl_questionsetforstudents as c
						WHERE c.IdBatch in ($idbatches) and c.idapplication=b.IDApplication  and b.ICNO = '$IDApplication' and  b.DateTime >='$fromdate'
						AND b.DateTime <='$todate'
					AND b.payment=1 and b.pass in(1,2)  group by idapplication";
 		   // echo $select;die();
 		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		
		public function fngetidapplications($fromdate,$todate,$idbatches)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		    $select = "	SELECT b.idapplication 
						FROM tbl_studentapplication AS b,tbl_questionsetforstudents as c
						WHERE c.IdBatch in ($idbatches) and c.idapplication=b.IDApplication  and b.DateTime >='$fromdate'
						AND b.DateTime <='$todate'
					AND b.payment=1 and b.pass in(1,2)  group by idapplication";
 		   // echo $select;die();
 		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		public function fnGettotalattended($Questionid,$regid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo"))
                                         ->where("a.Regid in ($regid)")
                                          ->where("a.QuestionNo=?",$Questionid)
										  ->group("a.Regid")
                                          ->group("a.QuestionNo");
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		
		public function fnGettotalCorrectattended($Questionid,$regid)
		{
	
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Regid"))
							 			 ->join(array("b"=>"tbl_answers"),'b.idanswers=a.Answer',array())
                                         ->where("a.Regid in ($regid)")
                                          ->where("a.QuestionNo=?",$Questionid)
                                          ->where("b.CorrectAnswer=1")
                                          ->group("a.Regid");
                                         

 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		
	public function fnGettotalCorrectattendedstudent($Questionid,$regid)
		{
	
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Regid"))
							 			 ->join(array("b"=>"tbl_answers"),'b.idanswers=a.Answer',array())
							 			 ->join(array("c"=>"tbl_registereddetails"),'a.Regid=c.Regid',array())
							 			  ->join(array("d"=>"tbl_studentapplication"),'c.IDApplication 	=d.IDApplication',array())
                                         ->where("a.Regid in ($regid)")
                                          ->where("a.QuestionNo=?",$Questionid)
                                          ->where("b.CorrectAnswer=1")
                                          ->where("d.IDApplication = 25")
                                          ->group("a.Regid");
                                 
                                         

 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		public function fngetquestionanswerdetails($qtnid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_questions"),array("a.Question"))
							 			  ->join(array("b"=>"tbl_answers"),'b.idquestion=a.idquestions',array("b.answers","b.CorrectAnswer"))
                                          ->where("a.idquestions=?",$qtnid);
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}


	public function fngetallanswers($idquestion)
		{
			
			  $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_answers'),array("GROUP_CONCAT(answers SEPARATOR ' #^ ') as answers","c.idquestion","GROUP_CONCAT(idanswers SEPARATOR ' #^ ') as answersids"))
			->where("c.idquestion in ($idquestion)")
			->group("c.idquestion");
			//->order("c.idanswers");
		  $result = $db->fetchAll($select);		
		return $result; 
		
			
			
		}
		
		
		
		
		
			
		public function fngetcorrectans($idquestion)
		{
			
			  $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
			->from(array('c' => 'tbl_answers'),array("c.*"))
			->where("c.idquestion in ($idquestion)")
			->where("c.CorrectAnswer=1")
			->group("c.idquestion");
			//->order("c.idanswers");
		  $result = $db->fetchAll($select);		
		return $result; 
		
			
			
		}



   
	}
