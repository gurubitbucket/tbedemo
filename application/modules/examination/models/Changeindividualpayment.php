<?php
	class Examination_Model_Changeindividualpayment extends Zend_Db_Table {
	 
     
     
    
     
	 
		 public function fnfpxpaymentdetails($larrformdata,$iduser,$email)
     {
     	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
     	$table = "tbl_registrationfpx";
		$postData = array(		
							'IDApplication' =>$larrformdata['IDApplication'],	
           					'entryFrom' =>1, 
							'payerMailId' =>$email,	
							'grossAmount'=>$larrformdata['amount'],	
		  					'orderNumber'=>$larrformdata['TxnOrderNo'],
							'TxnDate' =>$larrformdata['Dates'], 
							'fpxTxnId' =>$larrformdata['fpxtransactionid'],	
							'bankCode'=>$larrformdata['BuyerBank'],
							'bankBranch'=>$larrformdata['BuyerBank'],	
		  					'debitAuthCode'=>'00',
							'debitAuthNo' =>$larrformdata['BuyerBankAcc'], 
							'creditAuthCode' =>'Manual',	
							'creditAuthNo'=>'Manual',	
		  					'paymentStatus'=>'1',
							'UpdUser' =>$iduser, 
							'UpdDate' =>date('y-m-d H:i:s'),	
							'ApprovedDate'=>date('y-m-d H:i:s'));
		$result=  $lobjDbAdpt->insert($table,$postData);
	    return $result;
     }
     public function fninsertregdetails($studentId,$Takafuloperator,$idbatch)
    {
	    $db = Zend_Db_Table::getDefaultAdapter();
		$larrformData1['Payment'] = 1;	
		 $where = "IDApplication = '".$studentId."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);

		 
		 $ModelBatchlogin = new App_Model_Batchlogin();
		 $Regid = $ModelBatchlogin->fnGenerateCode($Takafuloperator,$studentId);	
		 	 
		 $table = "tbl_registereddetails";
         $postData = array('Regid' =>   $Regid,	
           					'IdBatch' =>$idbatch,	
         					'Approved' =>1,	
         					'RegistrationPin'=>'0000000',
         					'Cetreapproval'=>'0',
           					'IDApplication' => $studentId);					
	         $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_registereddetails","idregistereddetails");
		// return $Regid; 
              //updating number of attempts
		 $lstrSelect = $db->select()
		                         ->from(array("a" => "tbl_studentapplication"),array("a.ICNO"))
		                        
		                         ->where("a.IDApplication = ?",$studentId);
		    $larrResult2 = $db->fetchRow($lstrSelect);
		    $icno=$larrResult2['ICNO'];
		 	$lstrSelect3 = $db->select()
		                         ->from(array("a" => "tbl_studentapplication"),array("max(a.PermCity) as attempts"))
		                         ->where("a.ICNO = ?",$icno);
		  $larrResult3 = $db->fetchRow($lstrSelect3);
		  $attempts=$larrResult3['attempts']+1;
		  if(empty($larrResult3['attempts']))
		  {
		 	$attempts=1;
		  }
		 $larrformData1['PermCity'] = $attempts;	
		 $where = "IDApplication = '".$studentId."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
		  //end of attempts
}
 public function fngetRegid($lintidstudent)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_registereddetails"),array('a.*'))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     public function fnviewstudentdetailssss($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										   ->join(array("d" =>"tbl_newscheduler"),'a.Year=d.idnewscheduler',array('d.Year as years'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										   ->join(array("e"=>"tbl_managesession"),'a.Examsession=e.idmangesession',array('e.*'))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
	 
	  public function fnGetEmailTemplateDescription($TemplateName)
	  {
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}	
     
	 public function fnGetVenueNames()
	{
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))		 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	 }
	   
	   
	public function fnGetCourseNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   } 
     
     public function  fngetstudenteachinformation($lintidstudent)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as AppliedDate","DATE_FORMAT(a.Datetime,'%d-%m-%Y') as ExamDate"))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.IdProgrammaster','b.ProgramName'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("w" =>"tbl_studentpaymentoption"),'w.IDApplication=a.IDApplication',array('w.*'))
										  // ->joinleft(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'a.IDApplication=tbl_fpxpaymentlog.IdApplication',array("tbl_fpxpaymentlog.TxnOrderNo","tbl_fpxpaymentlog.Amount","tbl_fpxpaymentlog.Dates"))
										  ->join(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->join(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }
	 /* public function fninsertedstudentdetails($larrformdata,$iduser)
     {
     	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
     	$table = "tbl_studentpaymentdetails";
		$postData = array(		
							'IDApplication' =>$larrformdata['IDApplication'],	
           					'Amount' =>$larrformdata['amount'], 
							'ChequeNo' =>$larrformdata['ReferenceNo'],	
							'companyflag'=>0,	
		  					'ChequeDt'=>$larrformdata['ReferenceDate'],
							'BankName' =>'NA', 
							'UpdUser' =>$iduser,	
							'UpdDate'=>date('Y-m-d H:i:s')
						);
		$result=  $lobjDbAdpt->insert($table,$postData);
	    return $result;
     }*/
 public function fncreditcardpaymentdetails($larrformdata,$iduser)
     {
     	$lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
     	$table = "tbl_migspayment";
		$postData = array(		
							'vpc_MerchTxnRef' =>$larrformdata['IDApplication'],	
           					'vpc_Card' =>$larrformdata['Cardtype'], 
							'vpc_Merchant' =>'Manual',	
							'vpc_ReceiptNo'=>$larrformdata['ReceiptNumber'],	
		  					'vpc_TransactionNo'=>$larrformdata['MerchantTransactionNum'],
							'vpc_TxnResponseCode' =>'0', 
							'vpc_SecureHash' =>'Manual',	
							'vpc_VerStatus'=>'Manual',
							'vpc_BatchNo'=>$larrformdata['Batchnumber'],                            							
		  					//'vpc_AuthorizeId'=>$iduser,
							'vpc_AuthorizeId'=>$larrformdata['AuthorisationCode'],
							'UpdDate'=>date('Y-m-d H:i:s')
						);
		$result=  $lobjDbAdpt->insert($table,$postData);
	    return $result;
     }
 public function fnindividualpayment($larrformData)
     {
	     
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*',"date(a.UpdDate) as applieddate"))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->joinleft(array("e" =>"tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array('e.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_studentpaymentoption"),'a.IDApplication=d.IDApplication',array('d.ModeofPayment'))
										  ->where("a.Payment=0")
										  ->where("d.companyflag=0")
										  ->where("a.batchpayment=0")
										  ->where("a.DateTime >= curdate()")
										  ->where("a.pass in (3)")
										  ->where("a.IDApplication>1148")
										  ->where("a.Examvenue!=000")
										  ->where("d.ModeofPayment in (1,10,7)")
										  ->group("d.IDApplication")
										  // ->order("a.DateTime Desc");
										  ->order("a.FName");	
				if($larrformData['Studentname']) $lstrSelect->where("a.Fname like  ? '%'",$larrformData['Studentname']);	
				if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$larrformData['ICNO']);					  
				if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$larrformData['Coursename']);
				if($larrformData['Venues']) $lstrSelect->where("c.idcenter = ?",$larrformData['Venues']);
				if($larrformData['Takafulname']) $lstrSelect->where("e.idtakafuloperator = ?",$larrformData['Takafulname']);
				if($larrformData['paymentmode']==1) $lstrSelect->where("d.ModeofPayment = 1");
				if($larrformData['paymentmode']==10) $lstrSelect->where("d.ModeofPayment = 10");
                                if($larrformData['paymentmode']==7) $lstrSelect->where("d.ModeofPayment = 7");
				
                if(isset($larrformData['Date']) && !empty($larrformData['Date']))
                {
				  $lstrFromDate = $larrformData['Date'];
				 
				  $lstrSelect = $lstrSelect->where("a.DateTime = '$lstrFromDate'");
		        }					  
		        
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
public function fnstudentpaymentoption($idstudent)
{
          $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();
          if(empty($idstudent))	
	  {
           $idstudent=0;  
          }
     	  $table = "tbl_studentpaymentoption";
	  $postData = array(
		                    'ModeofPayment' =>7 
						);
		
 	  $where = "IDApplication =$idstudent"; 	
	  $result = $lobjDbAdpt->update('tbl_studentpaymentoption', $postData, $where);
	  return $result;
}
 public function fninserttemp($larrformdata,$iduser)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_individualpaymentchanged";
		$postData = array(		
							'IDApplication' =>$larrformdata['IDApplication'],	
           					'Upddate' => date('Y-m-d H:i:s'), 
							'Upduser' =>$iduser,	
							'Oldpayment'=>$larrformdata['oldpayment'],	
		  					'Newpayment'=>$larrformdata['PaymentmodeTo']
						);
						$result=  $lobjDbAdpt->insert($table,$postData);
	
						   return $result;
     }
 public function fnupdatestudentpayment($larrformdata)
     {
     	 $db 	= 	Zend_Db_Table::getDefaultAdapter();	
     	 $idapplication =$larrformdata['IDApplication'];
     	
		$postData = array(
		                    'ModeofPayment' =>$larrformdata['PaymentmodeTo'] 
						);
		
 		$where = "IDApplication = '".$idapplication."'  AND companyflag ='0'"; 	
		 $db->update('tbl_studentpaymentoption', $postData, $where);	
     }
	 
     
     
}
