<?php
	class Examination_Model_Chronepull extends Zend_Db_Table {


	public function  fngetvenuedateschedule($iddate)
       {
       			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array())
							 			  ->join(array("b"=>"tbl_managesession"),'a.idsession=b.idmangesession',array("key"=>"b.idmangesession","value"=>"b.managesessionname"))
							 			  ->where("a.date='$iddate'")
										  ->where("a.Active = 1")
										    ->where("a.Reserveflag= 1")
										     ->group("b.idmangesession")
										     ->order("b.managesessionname");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       }

      
    public function fngetallvenues($larrformdata)
    {
    	$iddate = $larrformdata['Date'];
    	$session = $larrformdata['Session'];
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			  ->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("b.*"))
							 			  ->join(array("c"=>"tbl_managesession"),'a.idsession=c.idmangesession',array("c.*"))
							 			  ->where("a.date='$iddate'")
							 			  ->where("a.idsession='$session'")
							 			  ->where("b.ipaddress=1")
							 			    ->where("a.Allotedseats>0")
							 			  ->group("b.idcenter")
							 			  ->order("b.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
    
    public function fngetallsessions()
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("b"=>"tbl_managesession"),array("key"=>"b.idmangesession","value"=>"b.managesessionname"))
										;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
    
    public function fninsertchronepull($larrformData,$larrresultlastid)
    {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$date = $larrformData['Date'];
    	$Session = $larrformData['Session'];
    	$cnt = count($larrformData['idcenter']);
    
    	for($i=0;$i<$cnt;$i++)
    	{
    		
    		
    		$idcenter = $larrformData['idcenter'][$i];
    		
    		$table = "tbl_chronepull";
           $postData = array(		
							'Venue' => $idcenter,		
            				'Examdate' =>$date,
           					'Session'=>$Session,
           					'idautochronepull' => $larrresultlastid		
						);			
	        $lobjDbAdpt->insert($table,$postData);
    	}
    }
    
    
	 public function fninsertschedulerdetails($larrformdata)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$cnt = count($larrformdata['idcenter']);
    $interval=0;

    	for($i=0;$i<$cnt;$i++)
    	{			   	
			   	$schedulerdate = $larrformdata['Schedulerdate'];
			   	$schedulertime = $larrformdata['ScheduleStartTime'];
			   	$schedulerdattime = $schedulerdate.' '.$schedulertime;
			   
			   	$interval = $interval+1;
			   	
			   	$queryoftime = "SELECT DATE_ADD('$schedulerdattime', INTERVAL '$interval:0' MINUTE_SECOND) as Timeintervalss";
			   	$larresulttime = $lobjDbAdpt->fetchRow($queryoftime);
			   	$larrscheduertime = $larresulttime['Timeintervalss'];
			   	$larraddedtime = explode(' ',$larrscheduertime);
			   	$addeddate = $larraddedtime[0];
			   	$addedtime = $larraddedtime[1];
			   	
			   	
		    $postdata = array('schdeduleddate'=>$addeddate,
		                      'scheduledtime'=>$addedtime,
		                      'UpdDate' => $larrformdata['UpdDate'],
		    				  'Session'=>$larrformdata['Session'],
				'Automated'=>1,
		                      'UpdUser' => $larrformdata['UpdUser']);
		    $lobjDbAdpt->insert('tbl_autochronepull',$postdata);
		    $lintlastinsid = $lobjDbAdpt->lastInsertId(); 
		    
			$idcenter = $larrformdata['idcenter'][$i];
    		
    		$table = "tbl_chronepull";
            $postData = array(		
							
