<?php
class Examination_Model_Examquestions extends Zend_Db_Table { 
	public function fndisplayquestionsforabovecode($questiongroup,$questionstatus,$fromqtn,$toqtn,$QuestionLevel,$QuestionChapter,$Questionname){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			 		->from(array("c" => "tbl_questions"),array('c.idquestions','c.Question','c.QuestionGroup','c.QuestionNumber','c.QuestionLevel','c.Active'))
				    ->join(array("d" => "tbl_groupnames"),'d.groupname=c.QuestionGroup',array(""))
				    //->join(array("e" => "tbl_groupnames"),'d.groupname=c.QuestionGroup',array(""))
				    ->order("c.idquestions");
	   				if(isset($questiongroup) && !empty($questiongroup) ){
						$lstrSelect = $lstrSelect->where("d.idgroupname= ?",$questiongroup);
					}
	   			 	if($questionstatus=='0' || $questionstatus=='1' || $questionstatus=='2'){
	   			 		$lstrSelect = $lstrSelect->where("c.Active = ?",$questionstatus);
					}
	   				 if(isset($fromqtn) && !empty($fromqtn) ){
						$lstrSelect = $lstrSelect->where("c.idquestions>= ?",$fromqtn);
					}
	   				 if(isset($toqtn) && !empty($toqtn) ){
						$lstrSelect = $lstrSelect->where("c.idquestions<= ?",$toqtn);
					}
					if(isset($QuestionChapter) && !empty($QuestionChapter) ){
						$lstrSelect = $lstrSelect->where("c.QuestionNumber= ?",$QuestionChapter);
					}
					if(isset($QuestionLevel) && !empty($QuestionLevel) ){
						$lstrSelect = $lstrSelect->where("c.QuestionLevel= ?",$QuestionLevel);
					}
					if(isset($Questionname) && !empty($Questionname) ){
						$lstrSelect = $lstrSelect->where('c.Question like "%" ? "%"',$Questionname);
						
						
					}
					//echo $lstrSelect;
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					
					return $larrResult;
	}
	public function fndisplayanswersforquestions($ID){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
					  ->from(array("a" => "tbl_answers"),array("a.idanswers","a.answers","a.CorrectAnswer")) 
					  ->join(array("c" => "tbl_questions"),'c.idquestions=a.idquestion',array(""))
					  ->where("a.idquestion=?",$ID)
					  ->order("a.idanswers");	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	function fngetquestiongroup(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				   ->from(array("a" =>"tbl_groupnames"),array("key"=>"idgroupname","value"=>"groupname"))
				   ->order("a.groupname");	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
	}
	
	function fngetgroupname($groupid){
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
						  ->from(array("a" =>"tbl_groupnames"),array("a.groupname"))
					      ->where("a.idgroupname=?",$groupid);	
				$larrResult = $lobjDbAdpt->fetchrow($lstrSelect);
				return $larrResult;
		
	}
	
	function fngettotalquestions(){
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							->from(array("a" =>"tbl_questions"),array("max(a.idquestions) as count"));
				$larrResult = $lobjDbAdpt->fetchrow($lstrSelect);
				return $larrResult;
				
				
	}
	
	public function fnGetChapterGroup($group)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questions"),array("key"=>"a.QuestionNumber","value"=>"a.QuestionNumber"))
										  ->group("a.QuestionNumber")
										  ->where("a.QuestionType=?",$group);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				//echo $larrResult;
				return $larrResult;
	}
}