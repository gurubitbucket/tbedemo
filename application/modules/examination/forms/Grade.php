<?php
class Examination_Form_Grade extends Zend_Dojo_Form 
{    
	
    public function init() 
    {    
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    					$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
       
        
        $GroupName = new Zend_Dojo_Form_Element_FilteringSelect ('gradename');
		$GroupName->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$GroupName	->	setAttrib('style','width:100px;')	;		
		$GroupName->removeDecorator ("DtDdWrapper");
		$GroupName->removeDecorator("Label");
		$GroupName->removeDecorator('HtmlTag');
		$GroupName->setAttrib('required',"true");
        
        $Marksfrom = new Zend_Dojo_Form_Element_FilteringSelect ('Marksfrom');
		$Marksfrom->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" )
		  ->setAttrib('OnChange','fnGetValidategrade(this.value)');
		$Marksfrom	->	setAttrib('style','width:100px;')	;		
		$Marksfrom->removeDecorator ("DtDdWrapper");
		$Marksfrom->removeDecorator("Label");
		$Marksfrom->removeDecorator('HtmlTag');
		$Marksfrom->setAttrib('required',"true");
		
		$Marksto = new Zend_Dojo_Form_Element_FilteringSelect ('Marksto');
		$Marksto->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Marksto  ->setAttrib('OnChange','fnGetValidategrade2(this.value)');
			$Marksto	->	setAttrib('style','width:100px;')	;		
		$Marksto->removeDecorator ("DtDdWrapper");
		$Marksto->removeDecorator("Label");
		$Marksto->removeDecorator('HtmlTag');
		$Marksto->setAttrib ('required',"true");
		
		
        		
       	$Description = new Zend_Form_Element_Text('gradedescription');
		$Description->setAttrib('dojoType',"dijit.form.ValidationTextBox");    			         				      
        $Description->removeDecorator("DtDdWrapper");
        $Description->removeDecorator("Label");
        $Description->removeDecorator('HtmlTag');
        
   
	            
	            
	            $Effectidate = new Zend_Dojo_Form_Element_DateTextBox('Effectidate');
	        	$Effectidate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
	        						
	        				->setAttrib('required',"true")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							//->setAttrib('constraints', "$dateofbirth")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
									
							
							
	            
        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
         $Save = new Zend_Form_Element_Submit('Save');
         $Save->dojotype="dijit.form.Button";
         $Save->label = $gstrtranslate->_("Save");
         $Save->setAttrib('class', 'NormalBtn');
         $Save->setAttrib('id', 'submitbutton');
         $Save->removeDecorator("DtDdWrapper");
         $Save->removeDecorator("Label");
         $Save->removeDecorator('HtmlTag');
           		         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn');
		$Back->removeDecorator("Label");
		$Back->removeDecorator("DtDdWrapper");
		$Back->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($GroupName,$Marksfrom,$Marksto,$Effectidate,
        						 $Description,
        						 $Active,       						
                                 $Save,
                                 $Back
                                )
                          );

    }
}