<?php
class Examination_Form_Createmanualquestionpaper extends Zend_Dojo_Form 
{    
	
    public function init() 
    {    
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$field1 = new Zend_Dojo_Form_Element_FilteringSelect('field1');
        $field1->setAttrib('dojoType',"dijit.form.FilteringSelect"); 
        $field1->removeDecorator("DtDdWrapper");        
        $field1->removeDecorator("Label");
        $field1->removeDecorator('HtmlTag');
        $field1->setAttrib('required',"false");
        //$field1->setAttrib('OnChange', 'fnGetDetails');
        //$field1->setRegisterInArrayValidator(false);  
        
        
    		
        $field28 = new Zend_Form_Element_Text('field28',array('invalidMessage'=>""));
		$field28 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$field28 ->setAttrib('class', 'txt_put') 
			//->setAttrib('validator', 'validateUsername')
			//->setAttrib('required',"true")     
    		->removeDecorator("DtDdWrapper")
        	->removeDecorator("Label")
        	->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($field1,$field28
                                )
                          );

    }
}