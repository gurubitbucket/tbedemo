<?php
	class Examination_Form_Changeindividualpayment extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
		
			$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day+1),$year));
		
			$yesterdaydate1= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
	    $vliddate="{max:'$yesterdaydate1',datePattern:'dd-MM-yyyy'}"; 
	
	    
				$Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
			    $Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Coursename->addMultiOption('','Select'); 	           	         		       		     
	            $Coursename->removeDecorator("DtDdWrapper");
	            $Coursename->removeDecorator("Label");
	            $Coursename->removeDecorator('HtmlTag');   
	     
	            $ICNO = new Zend_Form_Element_Text('ICNO');
			    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $ICNO->setAttrib('class','txt_put');  
	            $ICNO->removeDecorator("DtDdWrapper");
	            $ICNO->removeDecorator("Label");
	            $ICNO->removeDecorator('HtmlTag');
	         
	            $Studentname = new Zend_Form_Element_Text('Studentname');
			    $Studentname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $Studentname->setAttrib('class','txt_put');  
	            $Studentname->removeDecorator("DtDdWrapper");
	            $Studentname->removeDecorator("Label");
	            $Studentname->removeDecorator('HtmlTag');
	            
	            $Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	            $Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
			    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Venues->addMultiOption('','Select');  
	            $Venues->removeDecorator("DtDdWrapper");
	            $Venues->removeDecorator("Label");
	            $Venues->removeDecorator('HtmlTag');

	            $paymentmode = new Zend_Dojo_Form_Element_FilteringSelect('paymentmode');
			    $paymentmode->setAttrib('dojoType',"dijit.form.FilteringSelect");
			    //$paymentmode->setAttrib('OnChange','fnshowpaymentdetails(this.value)');
	            $paymentmode->addMultiOption('','Select'); 
	            $paymentmode->addMultiOptions(array(
									'1' => 'FPX',
									'10' => 'Credit Card',
                                                                        '7'=>'Credit To IBFIM'	            					
	            					)); 	           	         		       		     
	            $paymentmode->removeDecorator("DtDdWrapper");
	            $paymentmode->removeDecorator("Label");
	            $paymentmode->removeDecorator('HtmlTag');
	           
	            $Clear = new Zend_Form_Element_Submit('Clear');
        		$Clear->dojotype="dijit.form.Button";
        		$Clear->label = $gstrtranslate->_("Clear");
				$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
				$search = new Zend_Form_Element_Submit('Search');
        		$search->dojotype="dijit.form.Button";
        		$search->label = $gstrtranslate->_("Search");
        		$search->removeDecorator("DtDdWrapper");
       			$search->removeDecorator("Label");
        		$search->removeDecorator('HtmlTag')
         		       ->class = "NormalBtn";
         		       
         		$Save = new Zend_Form_Element_Submit('Save');
        		$Save->dojotype="dijit.form.Button";
        		$Save->removeDecorator("DtDdWrapper");
        		$Save->removeDecorator("Label");
        		$Save->removeDecorator('HtmlTag');
        		$Save->class = "NormalBtn";
        		$Save->label = $gstrtranslate->_("Save"); 
				
				$Cardtype = new Zend_Dojo_Form_Element_FilteringSelect('Cardtype');
			    $Cardtype->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Cardtype->addMultiOptions(array(
									'MC' => 'Master Card',
									'VC' => 'Visa Card')); 	           	         		       		     
	            $Cardtype->removeDecorator("DtDdWrapper");
	            $Cardtype->removeDecorator("Label");
	            $Cardtype->removeDecorator('HtmlTag');	
	            				
	            $TxnOrderNo = new Zend_Form_Element_Text('TxnOrderNo');
			    $TxnOrderNo->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $TxnOrderNo->setAttrib('class','txt_put');  
				  $TxnOrderNo-> setAttrib('required',"true"); 
	            $TxnOrderNo->removeDecorator("DtDdWrapper");
	            $TxnOrderNo->removeDecorator("Label");
	            $TxnOrderNo->removeDecorator('HtmlTag'); 
	            
	            $fpxtransactionid = new Zend_Form_Element_Text('fpxtransactionid');
			    $fpxtransactionid->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $fpxtransactionid->setAttrib('class','txt_put');  
				 $fpxtransactionid-> setAttrib('required',"true");
	            $fpxtransactionid->removeDecorator("DtDdWrapper");
	            $fpxtransactionid->removeDecorator("Label");
	            $fpxtransactionid->removeDecorator('HtmlTag');

                $Amount = new Zend_Form_Element_Text('Amount');
				 $Amount-> setAttrib('required',"true");
			    $Amount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $Amount->setAttrib('class','txt_put');  
	            $Amount->removeDecorator("DtDdWrapper");
	            $Amount->removeDecorator("Label");
	            $Amount->removeDecorator('HtmlTag');  	            
         		
         		
	            $TxnDate = new Zend_Dojo_Form_Element_DateTextBox('Dates');
	        	$TxnDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('required',"true")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');	
						
			    $BuyerBank = new Zend_Form_Element_Text('BuyerBank');
			    $BuyerBank          ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	                      ->setAttrib('class','txt_put') 
						  ->setAttrib('required',"true")
	                      ->removeDecorator("DtDdWrapper")
	                      ->removeDecorator("Label")
	                      ->removeDecorator('HtmlTag');
					
							
			    $BuyerBankAcc = new Zend_Form_Element_Text('BuyerBankAcc');
			    $BuyerBankAcc          ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	                      ->setAttrib('class','txt_put') 
						  ->setAttrib('required',"true")
	                      ->removeDecorator("DtDdWrapper")
	                      ->removeDecorator("Label")
	                      ->removeDecorator('HtmlTag');
                $DebitAuthCode = new Zend_Form_Element_Text('DebitAuthCode');
			    $DebitAuthCode          ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	                      ->setAttrib('class','txt_put')
	                      ->removeDecorator("DtDdWrapper")
	                      ->removeDecorator("Label")
	                      ->removeDecorator('HtmlTag');
                $DebitAuthNumber = new Zend_Form_Element_Text('DebitAuthNumber');
			     $DebitAuthNumber         ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	                      ->setAttrib('class','txt_put') 
						  ->setAttrib('required',"true")
	                      ->removeDecorator("DtDdWrapper")
	                      ->removeDecorator("Label")
	                      ->removeDecorator('HtmlTag');
                $CreditAuthCode = new Zend_Form_Element_Text('CreditAuthCode');
			    $CreditAuthCode          ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	                      ->setAttrib('class','txt_put')
	                      ->removeDecorator("DtDdWrapper")
	                      ->removeDecorator("Label")
	                      ->removeDecorator('HtmlTag');
                $CreditAuthNumber = new Zend_Form_Element_Text('CreditAuthNumber');
			    $CreditAuthNumber         ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	                      ->setAttrib('class','txt_put')
	                      ->removeDecorator("DtDdWrapper")
	                      ->removeDecorator("Label")
	                      ->removeDecorator('HtmlTag');				
						
						
						
						
						
						
				$VpcMerchant = new Zend_Form_Element_Text('VpcMerchant');
	        	$VpcMerchant->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');	
						
                $AuthorisationCode = new Zend_Form_Element_Text('AuthorisationCode');
	        	$AuthorisationCode->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('required',"true")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');	
                
                $Batchnumber = new Zend_Form_Element_Text('Batchnumber');
	        	$Batchnumber->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('required',"true")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');	
						
						
				$ReceiptNumber = new Zend_Form_Element_Text('ReceiptNumber');
	        	$ReceiptNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('required',"true")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
                 
                $MerchantTransactionNum = new Zend_Dojo_Form_Element_DateTextBox('MerchantTransactionNum');
	        	$MerchantTransactionNum->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('required',"true")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
			
			    $ReferenceDate = new Zend_Dojo_Form_Element_DateTextBox('ReferenceDate');
	        	$ReferenceDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('required',"true")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');	  
			    			
                        $PaymentmodeTo = new Zend_Dojo_Form_Element_FilteringSelect('PaymentmodeTo');
			    $PaymentmodeTo->setAttrib('dojoType',"dijit.form.FilteringSelect");
			    $PaymentmodeTo->setAttrib('OnChange','fnshowpaymentdetails(this.value)');
	            $PaymentmodeTo->removeDecorator("DtDdWrapper");
	            $PaymentmodeTo->removeDecorator("Label");
	            $PaymentmodeTo->removeDecorator('HtmlTag');	
                   
                    		
		    $ReferenceNumber = new Zend_Form_Element_Text('ReferenceNumber');
	            $ReferenceNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('required',"true")
						//->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');

                   $Reference = new Zend_Dojo_Form_Element_DateTextBox('Reference');
	           $Reference->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('required',"true")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');		  
				
                 		
			$this->addElements(
        					array($Coursename,
        						  $ICNO,
        					      $Studentname,
        					      $Venues,
        					      $Date,
        						  $paymentmode,
        						  $Clear,
        						  $search,
        						  $Save,
        						  $ReferenceDate,$Cardtype,$TxnOrderNo,$fpxtransactionid,$TxnDate,$AuthorisationCode,$Batchnumber,$ReceiptNumber,$MerchantTransactionNum,$VpcMerchant
								  ,$BuyerBank,$BuyerBankAcc,$DebitAuthCode,$DebitAuthNumber,$CreditAuthCode,$CreditAuthNumber,$Amount,$Reference,$PaymentmodeTo,$ReferenceNumber
        						
        						)
        			);
		}
}
