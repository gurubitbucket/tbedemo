<?php
class Examination_Form_Questionmaster extends Zend_Dojo_Form {
    public function init() {

    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$FileName = new Zend_Form_Element_File('FileName');
		$FileName->setAttrib('required',"true")      
                 ->setValueDisabled(true)
        		 ->setAttrib('maxlength','50')  
        		// ->setAttrib('style','width:700%;')        		
        		//->setAttrib('dojoType',"dijit.form.File")
                // ->setDestination('upload/document')
        		 ->removeDecorator("DtDdWrapper")
        	     ->removeDecorator("Label")
        		 ->removeDecorator('HtmlTag');
    	
    	$Name = new Zend_Form_Element_Text('Name',array('regExp'=>"[A-Za-z1-9 -.]+",'invalidMessage'=>"Special Characters Not Allowed"));
        $Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       // $CourseName->setAttrib('class', 'txt_put') ;
        $Name		->setAttrib('required',"true")       			 
	        		->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');  
    	
      
        $Section = new Zend_Form_Element_Select('Section');	
        $Section->setAttrib('dojoType',"dijit.form.FilteringSelect");      
        $Section->removeDecorator("DtDdWrapper");
        $Section->removeDecorator("Label");
        $Section->removeDecorator('HtmlTag'); 
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
       
        $Active1 = new Zend_Dojo_Form_Element_CheckBox('Active1');     
        $Active1->removeDecorator("DtDdWrapper")
        ->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active1->removeDecorator("Label");
        $Active1->removeDecorator('HtmlTag');
        $Active1->setValue('1');
                          
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        //new addition		
        
        $Schedule = new Zend_Form_Element_Submit('Schedule');
        $Schedule->dojotype="dijit.form.Button";
        $Schedule->label = $gstrtranslate->_("Schedule");
        $Schedule->removeDecorator("DtDdWrapper");
        $Schedule->removeDecorator("Label");
        $Schedule->removeDecorator('HtmlTag')
         		->class = "NormalBtn"; 		
         		
       	$Questions = new Zend_Form_Element_Text('Questions');
        $Questions->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Questions->setAttrib('style','width:700px');
        $Questions->setAttrib('required',"true")       			        
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag'); 

       $Name = new Zend_Form_Element_Text('Name',array('regExp'=>"[A-Za-z1-9 -.]+",'invalidMessage'=>"Special Characters Not Allowed"));
       $Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Name		->setAttrib('required',"true")       			 
	        		->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

$Field1 = new Zend_Form_Element_Text('Field1',array());
       $Field1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Field1->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

$Field2 = new Zend_Form_Element_Text('Field2',array());
       $Field2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Field2->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

$Field3 = new Zend_Form_Element_Text('Field3',array());
       $Field3->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Field3->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

$Field4 = new Zend_Form_Element_Text('Field4',array());
       $Field4->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Field4->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

$Field5 = new Zend_Form_Element_Text('Field5',array());
       $Field5->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Field5->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

/*   $QuestionLevel = new Zend_Form_Element_Text('QuestionLevel',array());
       $QuestionLevel->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $QuestionLevel		->setAttrib('required',"true")       			 
	        		->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');*/

$Questiongroup = new Zend_Dojo_Form_Element_FilteringSelect('QuestionGroup');
     $Questiongroup  ->setAttrib('dojoType',"dijit.form.FilteringSelect")
                    ->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			//->addMultiOption('','Select')
        			->setAttrib('style','width:165px;')       	
        			//->setAttrib('class', 'txt_put MakeEditable')         			        			       		        					
        			->	setAttrib('required',"true"); 	    

$Answers1 = new Zend_Form_Element_Text('Answers1',array());
       $Answers1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Answers1		->setAttrib('required',"true")       			 
	        		//->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

$Answers2 = new Zend_Form_Element_Text('Answers2',array());
       $Answers2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Answers2		->setAttrib('required',"true")       			 
	        		//->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

$Answers3 = new Zend_Form_Element_Text('Answers3',array());
       $Answers3->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Answers3		->setAttrib('required',"true")       			 
	        		//->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

      $Answers4 = new Zend_Form_Element_Text('Answers4',array());
       $Answers4->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $Answers4		->setAttrib('required',"true")       			 
	        		//->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');
	        		
 $Hiddenidanswer1 = new Zend_Form_Element_Hidden('Hiddenidanswer1');
        $Hiddenidanswer1->removeDecorator("DtDdWrapper");
        $Hiddenidanswer1->removeDecorator("Label");
        $Hiddenidanswer1->removeDecorator('HtmlTag');
        	        		
 $Hiddenidanswer2 = new Zend_Form_Element_Hidden('Hiddenidanswer2');
        $Hiddenidanswer2->removeDecorator("DtDdWrapper");
        $Hiddenidanswer2->removeDecorator("Label");
        $Hiddenidanswer2->removeDecorator('HtmlTag');

 $Hiddenidanswer3 = new Zend_Form_Element_Hidden('Hiddenidanswer3');
        $Hiddenidanswer3->removeDecorator("DtDdWrapper");
        $Hiddenidanswer3->removeDecorator("Label");
        $Hiddenidanswer3->removeDecorator('HtmlTag');

 $Hiddenidanswer4 = new Zend_Form_Element_Hidden('Hiddenidanswer4');
        $Hiddenidanswer4->removeDecorator("DtDdWrapper");
        $Hiddenidanswer4->removeDecorator("Label");
        $Hiddenidanswer4->removeDecorator('HtmlTag');        
	        			        		
  $Questionid = new Zend_Form_Element_Hidden('Questionid');
        $Questionid->removeDecorator("DtDdWrapper");
        $Questionid->removeDecorator("Label");
        $Questionid->removeDecorator('HtmlTag');   

        
 $AnswerStyle = new  Zend_Dojo_Form_Element_FilteringSelect('AnswerStyle');
       $AnswerStyle->addMultiOptions(array('0'=>'Multiple Choice'))       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        	    ->setAttrib('dojoType',"dijit.form.FilteringSelect")
	        		->removeDecorator('HtmlTag');

 $QuestionType = new Zend_Form_Element_Text('QuestionType');
       $QuestionType->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $QuestionType		->setAttrib('required',"true")       			 
	        		->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');  
  
  $QuestionNumber = new Zend_Form_Element_Text('QuestionNumber');
       $QuestionNumber->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $QuestionNumber		->setAttrib('required',"true")
                    ->setAttrib('propercase','true')         			 
	        		->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');                 
        
     	$Close = new Zend_Form_Element_Submit('Close');
		$Close->setAttrib('class', 'buttonStyle')
			->setAttrib('onclick', 'fnCloseLyteBox()')
			->removeDecorator("Label")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag');
			
			
	$MalayQuestion = new Zend_Form_Element_Text('MalayQuestion');
        $MalayQuestion->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $MalayQuestion->setAttrib('style','width:700px');
        $MalayQuestion->setAttrib('required',"true")       			        
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag'); 			
			
$MalayAnswers1 = new Zend_Form_Element_Text('MalayAnswers1',array());
       $MalayAnswers1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $MalayAnswers1		->setAttrib('required',"true")       			 
	        		//->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

$MalayAnswers2 = new Zend_Form_Element_Text('MalayAnswers2',array());
       $MalayAnswers2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $MalayAnswers2		->setAttrib('required',"true")       			 
	        		//->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

$MalayAnswers3 = new Zend_Form_Element_Text('MalayAnswers3',array());
       $MalayAnswers3->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $MalayAnswers3		->setAttrib('required',"true")       			 
	        		//->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

      $MalayAnswers4 = new Zend_Form_Element_Text('MalayAnswers4',array());
       $MalayAnswers4->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       $MalayAnswers4		->setAttrib('required',"true")       			 
	        		//->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');	

 $CorrectAnswer = new  Zend_Dojo_Form_Element_FilteringSelect('CorrectAnswer');
       $CorrectAnswer->addMultiOptions(array('1'=>'Answer1','2'=>'Answer2','3'=>'Answer3','4'=>'Answer4'))       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        	    ->setAttrib('dojoType',"dijit.form.FilteringSelect")
	        		->removeDecorator('HtmlTag');	
	        		
 $QuestionLevel = new  Zend_Dojo_Form_Element_FilteringSelect('QuestionLevel');
       $QuestionLevel->addMultiOptions(array('1'=>'Easy','2'=>'Medium','3'=>'Difficult'))       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        	    ->setAttrib('dojoType',"dijit.form.FilteringSelect")
	        		->removeDecorator('HtmlTag');

	    $Active = new Zend_Form_Element_Radio('Active');
		$Active->setAttrib('dojoType',"dijit.form.RadioButton");
        $Active->addMultiOptions(array('1' => 'Active','0' => 'Inactive','2'=>'Review'))
        			//->setvalue('1')
        			->setSeparator('&nbsp;')
        			->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');
        			
        $reviewchk = new Zend_Dojo_Form_Element_CheckBox('reviewchk');     
        $reviewchk->removeDecorator("DtDdWrapper")
        ->setAttrib('dojoType',"dijit.form.CheckBox")
        ->setAttrib('readonly',"true")
        ->setAttrib('checked',"checked");
        $reviewchk->removeDecorator("Label");
        $reviewchk->removeDecorator('HtmlTag');
        //$reviewchk->setValue('2');
         
        $this->addElements(array($Name,$UpdDate,$UpdUser,$FileName,$Section,                               
                                 $Active,$Save, $Close,$Questions,
                                 $Field1,$Field2,$Field3,$Field4,$Field5,$Questiongroup,$QuestionLevel,
                                 $Answers1,$Answers2,$Answers3,$Answers4,
                                 $Hiddenidanswer1,$Hiddenidanswer2,$Hiddenidanswer3,$Hiddenidanswer4,$Questionid,
                                 $AnswerStyle,$QuestionType,$QuestionNumber,$Schedule,
                                 $MalayAnswers1,$MalayAnswers2,$MalayAnswers3,$MalayAnswers4,$MalayQuestion,$CorrectAnswer,
                                 $Active1,$reviewchk));
    }
}
