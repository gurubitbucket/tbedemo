<?php
class Examination_Form_Batchmaster extends Zend_Dojo_Form {
    public function init() {    	

    	$strSystemDate = date('Y-m-d H:i:s');
    	
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
			
        $IdBatch = new Zend_Form_Element_Hidden('IdBatch');
        $IdBatch->removeDecorator("DtDdWrapper");
        $IdBatch->removeDecorator("Label");
        $IdBatch->removeDecorator('HtmlTag');
        
            $Update = new Zend_Form_Element_Hidden('UpdDate');
        	$Update->removeDecorator("DtDdWrapper")
        			->setvalue($strSystemDate)
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');
        		 	 
			$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
			$UpdUser->setAttrib('id','UpdUser')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
				 	->removeDecorator('HtmlTag');
        

        $BatchName = new Zend_Form_Element_Text('BatchName');
        $BatchName->setAttrib('dojoType',"dijit.form.ValidationTextBox")
        			->setAttrib('required',"true") 
        		  	->setAttrib('maxlength','25')
        		  	->addFilter('StripTags')
				 	->addFilter('StringTrim')
        			->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');
        
      $IdProgrammaster = new Zend_Form_Element_Select('IdProgrammaster');
      $IdProgrammaster->  setAttrib('dojoType',"dijit.form.FilteringSelect")
                    ->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			//->addMultiOption('','Select')
        			->setAttrib('style','width:165px;')       	
        			//->setAttrib('class', 'txt_put MakeEditable')         			        			       		        					
        			->	setAttrib('required',"true");  
        			
        			
        $Part = new Zend_Form_Element_Select('IdPart');
        $Part->  setAttrib('dojoType',"dijit.form.FilteringSelect")
                    ->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			//->addMultiOption('','Select')
        			->setAttrib('style','width:165px;')       	
        			//->setAttrib('class', 'txt_put MakeEditable')         			        			       		        					
        			->	setAttrib('required',"true"); 
        $BatchFrom = new Zend_Dojo_Form_Element_DateTextBox('BatchFrom');
         $BatchFrom ->setAttrib('dojoType',"dijit.form.DateTextBox")
						->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('BatchTo').constraints.min = arguments[0];")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
        
        $BatchTo = new Zend_Dojo_Form_Element_DateTextBox('BatchTo');
        $BatchTo->setAttrib('dojoType',"dijit.form.DateTextBox")
							->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->setAttrib('onChange', "dijit.byId('BatchFrom').constraints.max = arguments[0];")
							->setAttrib('required',"true")		
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
        
        $BatchStatus = new Zend_Form_Element_Checkbox('BatchStatus');
		$BatchStatus->setRequired(true)
					->setChecked(1)
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label") 					
					->removeDecorator('HtmlTag');
        
      $Add = new Zend_Form_Element_Submit('Add');
        $Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Save");
        $Add->removeDecorator("DtDdWrapper");
        $Add->removeDecorator("Label");
        $Add->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
      
         $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
        
	
        $Close = new Zend_Form_Element_Button('Close');
        $Close->dojotype="dijit.form.Button";
        $Close->label = $gstrtranslate->_("Close");
        $Close->removeDecorator("DtDdWrapper");
        $Close->removeDecorator("Label");
        $Close->removeDecorator('HtmlTag')
         		->class = "NormalBtn";	

        $this->addElements(array($IdBatch,$IdProgrammaster,$BatchName,$Part,$BatchFrom,$BatchTo,$BatchStatus,$Add,$Save,$Close,$Update,$UpdUser));

    }
}