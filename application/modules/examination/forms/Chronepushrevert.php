<?php
	class Examination_Form_Chronepushrevert extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
		
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
	  $yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 

		

         	    
	            $Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "newsessionlists();")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
											
			  
				$Session = new Zend_Dojo_Form_Element_FilteringSelect('Session');
			    $Session->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Session->removeDecorator("DtDdWrapper");
	            $Session->removeDecorator("Label");
	            $Session->removeDecorator('HtmlTag');
	            

	            
	              $Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search for Venue");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag');

        	              $Adds = new Zend_Form_Element_Submit('Adds');
        $Adds->dojotype="dijit.form.Button";
        $Adds->label = $gstrtranslate->_("Adds");
        $Adds->removeDecorator("DtDdWrapper");
        $Adds->removeDecorator("Label");
        $Adds->removeDecorator('HtmlTag');
        
        $Clear = new Zend_Form_Element_Button('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');    

	   

$Schedulerdate = new Zend_Dojo_Form_Element_DateTextBox('Schedulerdate');
		$Schedulerdate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Schedulerdate->setAttrib('title',"dd-mm-yyyy");
		$Schedulerdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Schedulerdate->setAttrib('constraints', "$dateofbirth");
		$Schedulerdate->removeDecorator("Label");
		$Schedulerdate->removeDecorator("DtDdWrapper");
		$Schedulerdate->removeDecorator('HtmlTag');							

		
		$ScheduleStartTime = new Zend_Form_Element_Text('ScheduleStartTime');
        $ScheduleStartTime->removeDecorator("DtDdWrapper"); 
        $ScheduleStartTime->removeDecorator("Label");
        $ScheduleStartTime->removeDecorator('HtmlTag');
		$ScheduleStartTime->setAttrib('dojoType',"dijit.form.TimeTextBox")
					->setAttrib('constraints',"{timePattern: 'HH:mm'}");
					
					
        		 $go = new Zend_Form_Element_Submit('go');
        		$go->dojotype="dijit.form.Button";
        		$go->removeDecorator("DtDdWrapper");
        		$go->removeDecorator("Label");
        		$go->removeDecorator('HtmlTag');
        		$go->class = "NormalBtn";
        		$go->label = $gstrtranslate->_("Go"); 

	            $Changetype = new Zend_Dojo_Form_Element_FilteringSelect('Changetype');
			    $Changetype->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Changetype->addMultiOptions(array(
	                                '1'=>'After Pushed to Local',
	                                '2' => 'Before pushed to Local'));      	         		       		     
	            $Changetype->removeDecorator("DtDdWrapper");
	            $Changetype->removeDecorator("Label");
	            $Changetype->removeDecorator('HtmlTag');
	            
			$this->addElements(
        					array($Date,
        					      $Venue,$Adds,
        					      $Course,$Search,$Clear,$Type,$ToDate,$FromDate,$Schedulerdate,$ScheduleStartTime,$Session,$go,$Changetype
        			
        						)
        			);
		}
}
