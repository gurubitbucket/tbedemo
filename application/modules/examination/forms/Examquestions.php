<?php
class Examination_Form_Examquestions extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ( 'Zend_Translate' );
		
		$Marksfrom = new Zend_Dojo_Form_Element_FilteringSelect ('Marksfrom');
		$Marksfrom->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Marksfrom->removeDecorator ("DtDdWrapper");
		$Marksfrom->setAttrib ('onChange',"fncheckvalue();");
		$Marksfrom->addMultiOptions(array('0'=>'Starting'));
		$Marksfrom->removeDecorator("Label");
		$Marksfrom->removeDecorator('HtmlTag');
		$Marksfrom->setAttrib('required',"true");
		
		$Marksto = new Zend_Dojo_Form_Element_FilteringSelect ('Marksto');
		$Marksto->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Marksto->setAttrib ('onChange',"fncheckvalue();");
		//$Marksto->addMultiOptions(array('0'=>'Ending'));
		$Marksto->removeDecorator ("DtDdWrapper");
		$Marksto->removeDecorator("Label");
		$Marksto->removeDecorator('HtmlTag');
		$Marksto->setAttrib ('required',"true");
		
		$Questiongroup = new Zend_Dojo_Form_Element_FilteringSelect ('Questiongroup');
		$Questiongroup->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Questiongroup->addMultiOptions(array('0'=>'Select'));
		$Questiongroup->setAttrib ('onChange',"fngetchapterlist(this.value);");
		$Questiongroup->removeDecorator ("DtDdWrapper");
		$Questiongroup->removeDecorator("Label");
		$Questiongroup->removeDecorator('HtmlTag');
		//$Questiongroup->setAttrib ('required',"true");
		
		$Questionstatus = new Zend_Dojo_Form_Element_FilteringSelect ('Questionstatus');
		$Questionstatus->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Questionstatus->addMultiOptions(array('3'=>'Select','1'=>'Active','0'=>'InActive','2'=>'Review'));
		$Questionstatus->removeDecorator ("DtDdWrapper");
		$Questionstatus->removeDecorator("Label");
		$Questionstatus->removeDecorator('HtmlTag');
		$Questionstatus->setAttrib ('required',"true");
		
		
		$Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
		
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Print");
        $submit->removeDecorator("DtDdWrapper");
        //$submit->setAttrib ('onClick',"fnloadLoader();");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
         	   
        $submitnew= new Zend_Form_Element_Submit('submitnew');
        $submitnew->dojotype="dijit.form.Button";
        $submitnew->label = $gstrtranslate->_("Printnew");
        $submitnew->removeDecorator("DtDdWrapper");
        //$submitnew->setAttrib ('onClick',"fnloadLoader();");
        $submitnew->removeDecorator("Label");
        $submitnew->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
         	   
         	   
		$QuestionLevel = new  Zend_Dojo_Form_Element_FilteringSelect('QuestionLevel');
       	$QuestionLevel->addMultiOptions(array('0'=>'Select','1'=>'Easy','2'=>'Medium','3'=>'Difficult'))       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        	    ->setAttrib('dojoType',"dijit.form.FilteringSelect")
	        		->removeDecorator('HtmlTag');
	        		
	    $QuestionChapter =new Zend_Form_Element_Select('QuestionChapter');        
        $QuestionChapter->removeDecorator("DtDdWrapper");
        $QuestionChapter->addMultiOption('0','Select');
        $QuestionChapter->removeDecorator("Label");
        $QuestionChapter->removeDecorator('HtmlTag');
        $QuestionChapter->setAttrib ('onClick',"fnselectgroup();");
        //$QuestionChapter->setAttrib('OnChange', 'fnGetDetails');
        $QuestionChapter->	setAttrib('required',"false"); 
        $QuestionChapter->setRegisterInArrayValidator(false);
		$QuestionChapter->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Questionname = new Zend_Form_Element_Text('Questionname');
        $Questionname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Questionname->setAttrib('placeholder',"Search by question");
        //$Questionname->setAttrib('style','width:100%');
        $Questionname->setAttrib('maxlength',"100")  ;     			        
	    $Questionname->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');
	        		
	    /*$Questionname = new Zend_Form_Element_Textarea('Questionname');	
       	$Questionname	->setAttrib('cols', '23');
        $Questionname	->setAttrib('rows','3')
        				->setAttrib('style','width = 10%;')
        				->setAttrib('dojoType',"dijit.form.SimpleTextarea")
        				->setAttrib('style','margin-top:10px;border:1px light-solid #666666;color:#666666;font-size:11px')
        				->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag');*/
		
		$this->addElements ( array ($Marksfrom,
		                            $Marksto,
		                            $Questiongroup,
		                            $Questionstatus,
		                            $Search,
		                            $submit,
		                            $QuestionLevel,$QuestionChapter,$Questionname,$submitnew)
		                   );
	}
}
