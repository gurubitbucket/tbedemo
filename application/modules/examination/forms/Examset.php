<?php
class Examination_Form_Examset extends Zend_Dojo_Form 
{    
	
    public function init() 
    {    
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    		
        $GroupName = new Zend_Form_Element_Text('groupname');	
		$GroupName->setAttrib('dojoType',"dijit.form.ValidationTextBox");		
        $GroupName->setAttrib('required',"true");       			        		  
        $GroupName->removeDecorator("DtDdWrapper");
        $GroupName->removeDecorator("Label");
        $GroupName->removeDecorator('HtmlTag');
        		
       	$Description = new Zend_Form_Element_Text('groupdescription');
		$Description->setAttrib('dojoType',"dijit.form.ValidationTextBox");    			         				      
        $Description->removeDecorator("DtDdWrapper");
        $Description->removeDecorator("Label");
        $Description->removeDecorator('HtmlTag');
        
        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
         $Save = new Zend_Form_Element_Submit('Save');
         $Save->dojotype="dijit.form.Button";
         $Save->label = $gstrtranslate->_("Save");
         $Save->setAttrib('class', 'NormalBtn');
         $Save->setAttrib('id', 'submitbutton');
         $Save->removeDecorator("DtDdWrapper");
         $Save->removeDecorator("Label");
         $Save->removeDecorator('HtmlTag');
           		         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn');
		$Back->removeDecorator("Label");
		$Back->removeDecorator("DtDdWrapper");
		$Back->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($GroupName,
        						 $Description,
        						 $Active,       						
                                 $Save,
                                 $Back
                                )
                          );

    }
}