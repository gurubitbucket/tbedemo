<?php
class Examination_Form_Automail extends Zend_Dojo_Form {
	public function init(){
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}";
	  
		$Studentname = new Zend_Form_Element_Text('Studentname');
		$Studentname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Studentname->setAttrib('class','txt_put');
		$Studentname->setAttrib('required','true');
		$Studentname->removeDecorator("DtDdWrapper");
		$Studentname->removeDecorator("Label");
		$Studentname->removeDecorator('HtmlTag');

		$ICNO = new Zend_Form_Element_Text('ICNO');
	    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $ICNO->setAttrib('class','txt_put');  
	    $ICNO->setAttrib('required','true');  
	    $ICNO->removeDecorator("DtDdWrapper");
	    $ICNO->removeDecorator("Label");
	    $ICNO->removeDecorator('HtmlTag');
	            
		$Dates = new Zend_Dojo_Form_Element_DateTextBox('Dates');
		$Dates->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Dates->setAttrib('title',"dd-mm-yyyy")
		      ->setAttrib('required',"true");
		$Dates->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Dates->setAttrib('constraints', "$dateofbirth");
		$Dates->removeDecorator("Label");
		$Dates->removeDecorator("DtDdWrapper");
		$Dates->removeDecorator('HtmlTag');
			  
		$ScheduleStartTime = new Zend_Form_Element_Text('ScheduleStartTime');
        $ScheduleStartTime->setAttrib('required',"true");
        $ScheduleStartTime->removeDecorator("DtDdWrapper"); 
        $ScheduleStartTime->removeDecorator("Label");
        $ScheduleStartTime->removeDecorator('HtmlTag');
		$ScheduleStartTime->setAttrib('dojoType',"dijit.form.TimeTextBox")
					->setAttrib('constraints',"{timePattern: 'HH:mm'}");
		 
		$search = new Zend_Form_Element_Submit('Search');
		$search->dojotype="dijit.form.Button";
		$search->label = $gstrtranslate->_("Search");
		$search->removeDecorator("DtDdWrapper");
		$search->removeDecorator("Label");
		$search->removeDecorator('HtmlTag');
		$search->class = "NormalBtn";
		 
		$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype="dijit.form.Button";
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator("Label");
		$Save->removeDecorator('HtmlTag');
		$Save->class = "NormalBtn";
		$Save->label = $gstrtranslate->_("Save");
        
		$Add = new Zend_Form_Element_Submit('Add');
		$Add->dojotype="dijit.form.Button";
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator("Label");
		$Add->removeDecorator('HtmlTag');
		$Add->class = "NormalBtn";
		$Add->label = $gstrtranslate->_("Add");
		
		$this->addElements(
							array(
									$Studentname,
									$ICNO,
									$Dates,
									$Save,
									$search,
									$Add,
									$ScheduleStartTime
							     )
						  );
	}
}
