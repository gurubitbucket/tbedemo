<?php
class Examination_Form_Studentissuesform extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	//$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		
        
        $Issue = new Zend_Form_Element_Text('Issue');	
		$Issue->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Issue->setAttrib('required',"true")       			 
        		//->setAttrib('maxlength','100')
       	->	setAttrib('style','width:400px;')				
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        $Back = new Zend_Form_Element_Button('Back');
        $Back->label ="Back";
		//$Back->setAttrib('onclick','fngoback();');
        $Back->dojotype="dijit.form.Button";
        $Back->removeDecorator("DtDdWrapper");
        $Back->removeDecorator('HtmlTag')
         		->class = "NormalBtn";	
				
		$Add = new Zend_Form_Element_Button('Add');
        $Add->label ="Add";
		$Add->setAttrib('onclick','fngoback();');
        $Add->dojotype="dijit.form.Button";
        $Add->removeDecorator("DtDdWrapper");
        $Add->removeDecorator('HtmlTag')
         		->class = "NormalBtn";			
				
		$Clear = new Zend_Form_Element_Button('Clear');
        $Clear->label ="Clear";
		//$Clear->setAttrib('onclick','fngoback();');
        $Clear->dojotype="dijit.form.Button";
        $Clear->removeDecorator("DtDdWrapper");
        $Clear->removeDecorator('HtmlTag')
         		->class = "NormalBtn";			
							
        		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label ="Save";
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";

        $Search = new Zend_Form_Element_Submit('Search');
		$Search->dojotype="dijit.form.Button";
		$Search->label ="Search";
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator("Label");
		$Search->removeDecorator('HtmlTag');
		$Search->class = "NormalBtn";
						
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
       			
        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');				
    
        $this->addElements(array($Issue,$Back,$Add,$Clear,$Save,$Search,$UpdDate,$UpdUser,$Active ));

    }
}