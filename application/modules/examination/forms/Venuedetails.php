<?php
class Examination_Form_Venuedetails extends Zend_Dojo_Form {
    public function init() {

    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$FileName = new Zend_Form_Element_File('FileName');
		$FileName->setAttrib('required',"true")      
                 ->setValueDisabled(true)
        		 ->setAttrib('maxlength','50')  
        		 ->setAttrib('style','width:700%;')        		
        		// ->setAttrib('dojoType',"dijit.form.File")
                // ->setDestination('upload/document')
        		 ->removeDecorator("DtDdWrapper")
        	     ->removeDecorator("Label")
        		 ->removeDecorator('HtmlTag');
    	
    	$Name = new Zend_Form_Element_Text('Name',array('regExp'=>"[A-Za-z1-9 -.]+",'invalidMessage'=>"Special Characters Not Allowed"));
        $Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       // $CourseName->setAttrib('class', 'txt_put') ;
        $Name		->setAttrib('required',"true")       			 
	        		->setAttrib('maxlength','100')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');  
    	
      
        $Section = new Zend_Form_Element_Select('Section');	
        $Section->setAttrib('dojoType',"dijit.form.FilteringSelect");      
        $Section->removeDecorator("DtDdWrapper");
        $Section->removeDecorator("Label");
        $Section->removeDecorator('HtmlTag'); 
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
       
        $Active = new Zend_Dojo_Form_Element_CheckBox('Active');     
        $Active->removeDecorator("DtDdWrapper")
        ->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        $Active->setValue('1');
                          
        $Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
         		
$FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
	    $FromDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	    				->setAttrib('title',"dd-mm-yyyy")
	        		   ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
	        		   ->setAttrib('onChange', "examtoDateSetting();")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');

						
								$ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
	    $ToDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
								   ->setAttrib('onChange', "fillvenue(this.value);")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');

        $Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');        
        $Venue->removeDecorator("DtDdWrapper");
        $Venue->removeDecorator("Label");
        $Venue->removeDecorator('HtmlTag');
       	$Venue->setAttrib('dojoType',"dijit.form.FilteringSelect");								
         
        $this->addElements(array($Name,$UpdDate,$UpdUser,$FileName,$Section,                               
                                 $Active,$Search,$FromDate,$ToDate,$Venue));
    }
}
