<?php
	class Examination_Form_Individualpaymentchange extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
		
			$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day+1),$year));
		
			$yesterdaydate1= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
	    $vliddate="{max:'$yesterdaydate1',datePattern:'dd-MM-yyyy'}"; 
	
	    
				$Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
			    $Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Coursename->addMultiOption('','Select'); 	           	         		       		     
	            $Coursename->removeDecorator("DtDdWrapper");
	            $Coursename->removeDecorator("Label");
	            $Coursename->removeDecorator('HtmlTag');   
	     
	            $ICNO = new Zend_Form_Element_Text('ICNO');
			    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $ICNO->setAttrib('class','txt_put');  
	            $ICNO->removeDecorator("DtDdWrapper");
	            $ICNO->removeDecorator("Label");
	            $ICNO->removeDecorator('HtmlTag');
	         
	            $Studentname = new Zend_Form_Element_Text('Studentname');
			    $Studentname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $Studentname->setAttrib('class','txt_put');  
	            $Studentname->removeDecorator("DtDdWrapper");
	            $Studentname->removeDecorator("Label");
	            $Studentname->removeDecorator('HtmlTag');
	            
	            $Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	            $Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
			    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Venues->addMultiOption('','Select');  
	            $Venues->removeDecorator("DtDdWrapper");
	            $Venues->removeDecorator("Label");
	            $Venues->removeDecorator('HtmlTag');

	            $paymentmode = new Zend_Dojo_Form_Element_FilteringSelect('paymentmode');
			    $paymentmode->setAttrib('dojoType',"dijit.form.FilteringSelect");
			          $paymentmode->setAttrib('OnChange','fnshowpaymentdetails(this.value)');
	            $paymentmode->addMultiOption('','Select'); 
	            $paymentmode->addMultiOptions(array(
									'1' => 'FPX',
									'2' => 'Credit Card',
	            					'5' => 'Money Order',
	            					'6' => 'Postal Order',
	            					'7' => 'Credit/Bank to IBFIM account')); 	           	         		       		     
	            $paymentmode->removeDecorator("DtDdWrapper");
	            $paymentmode->removeDecorator("Label");
	            $paymentmode->removeDecorator('HtmlTag');
	           
	            $Clear = new Zend_Form_Element_Submit('Clear');
        		$Clear->dojotype="dijit.form.Button";
        		$Clear->label = $gstrtranslate->_("Clear");
				$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
				$search = new Zend_Form_Element_Submit('Search');
        		$search->dojotype="dijit.form.Button";
        		$search->label = $gstrtranslate->_("Search");
        		$search->removeDecorator("DtDdWrapper");
       			$search->removeDecorator("Label");
        		$search->removeDecorator('HtmlTag')
         		       ->class = "NormalBtn";
         		       
         		$Save = new Zend_Form_Element_Submit('Save');
        		$Save->dojotype="dijit.form.Button";
        		$Save->removeDecorator("DtDdWrapper");
        		$Save->removeDecorator("Label");
        		$Save->removeDecorator('HtmlTag');
        		$Save->class = "NormalBtn";
        		$Save->label = $gstrtranslate->_("Save"); 
        		
	            $ReferenceNo = new Zend_Form_Element_Text('ReferenceNo');
			    $ReferenceNo->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $ReferenceNo->setAttrib('class','txt_put');  
	            $ReferenceNo->removeDecorator("DtDdWrapper");
	            $ReferenceNo->removeDecorator("Label");
	            $ReferenceNo->removeDecorator('HtmlTag');  

	            $ReferenceDate = new Zend_Dojo_Form_Element_DateTextBox('ReferenceDate');
	        	$ReferenceDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');	  

						
				$Cardtype = new Zend_Dojo_Form_Element_FilteringSelect('Cardtype');
			    $Cardtype->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Cardtype->addMultiOptions(array(
									'MC' => 'Master Card',
									'VC' => 'Visa Card')); 	           	         		       		     
	            $Cardtype->removeDecorator("DtDdWrapper");
	            $Cardtype->removeDecorator("Label");
	            $Cardtype->removeDecorator('HtmlTag');	
	            				
	            $ReceiptNo = new Zend_Form_Element_Text('ReceiptNo');
			    $ReceiptNo->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $ReceiptNo->setAttrib('class','txt_put');  
	            $ReceiptNo->removeDecorator("DtDdWrapper");
	            $ReceiptNo->removeDecorator("Label");
	            $ReceiptNo->removeDecorator('HtmlTag'); 
	            
	            $fpxtransactionid = new Zend_Form_Element_Text('fpxtransactionid');
			    $fpxtransactionid->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $fpxtransactionid->setAttrib('class','txt_put');  
	            $fpxtransactionid->removeDecorator("DtDdWrapper");
	            $fpxtransactionid->removeDecorator("Label");
	            $fpxtransactionid->removeDecorator('HtmlTag'); 	            
         		
         		
	            $TxnDate = new Zend_Dojo_Form_Element_DateTextBox('TxnDate');
	        	$TxnDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');	
						         		
			$this->addElements(
        					array($Coursename,
        						  $ICNO,
        					      $Studentname,
        					      $Venues,
        					      $Date,
        						  $paymentmode,
        						  $Clear,
        						  $search,
        						  $Save,
        						  $ReferenceNo,
        						  $ReferenceDate,$Cardtype,$ReceiptNo,$fpxtransactionid,$TxnDate
        						
        						)
        			);
		}
}
