<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_AutochronepushController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjAutochronepushForm = new Examination_Form_Autochronepush();
		$this->lobjAutochronepushmodel = new Examination_Model_Autochronepush(); //intialize newscreen db object
		$this->lobjCommonmodel = new GeneralSetup_Model_DbTable_Common();
	}
	
	public function indexAction() 
	{
	//	   $this->_helper->layout->disableLayout();
	
	       $this->view->lobjAutochronepushForm =$this->lobjAutochronepushForm;
		   $this->lobjAutochronepushmodel = new Examination_Model_Autochronepush();
		   $larrconfigvalues = $this->lobjAutochronepushmodel->fngetconfigvalues();
		   $larrschedulingtime = $larrconfigvalues['Schedulerpushtime'];
		   $larrresultdetails = explode(':',$larrschedulingtime);
		   //print_R($larrresultdetails);
		   $larrhours = explode('T',$larrresultdetails[0]);
		   //print_R($larrhours);
		  $hours = $larrhours[1];
		  $min = $larrresultdetails[1];
	 $currenttime = date('H:i');
		//die();

		  $schedulertime = $hours.':'.$min;
		
		   $date = date('Y-m-d');

		   $this->view->examdatelist = array();
		
        if ($this->_request->isPost() && $this->_request->getPost ( 'Add' )) {
			$larrformdata = $this->_request->getPost ();
			//echo "<pre/>";
			//print_R($larrformdata);
			for($j=0;$j<count($larrformdata['ScheduleStartTime']);$j++)
			{
				$larrfulltimehms = explode('T',$larrformdata['ScheduleStartTime'][$j]);
				
				$larrfulltime = explode(':',$larrfulltimehms[1]);
				$hourtime = $larrfulltime[0];
			    $mintime = $larrfulltime[1];
				$time = $hourtime.':'.$mintime.':00';
				$date = $larrformdata['datetime'][$j];
				$datetime['DatetoTime'][$date] = $time;
				
			}
			//for selected venue program
			$larrresultselecteddatas = $this->lobjAutochronepushmodel->fninsertchronepush($larrformdata,$datetime);

			$larrresultallvenuedatas - $this->lobjAutochronepushmodel->fninsertchronepushallvenues($larrformdata,$datetime);
			
			
			
}
	if ($this->_request->isPost() && $this->_request->getPost ( 'Search' )) {
			$larrformdata = $this->_request->getPost ();
			$fromdate = $larrformdata['FromDate'];
			$todate = $larrformdata['ToDate'];
			$this->view->StartTime= "Sat Nov 12 2011".' '.'10:00';
			$larrvenuedateschedule = $this->lobjAutochronepushmodel->fngetvenuesforthenextexamdates($fromdate,$todate);
		    $this->view->totalloop = count($larrvenuedateschedule);
		    $this->view->examdatelist = $larrvenuedateschedule;
		   
		   for($j=0;$j<count($larrvenuedateschedule);$j++)
		   {
		   	  $dates[] = $larrvenuedateschedule[$j]['date'];
		   }
		  // print_R($dates);
		  // die();
         $this->view->schedulerdates = $dates;
}	   
		   
	}
	
	
	public function venuelistAction()
	{/*
		 $this->_helper->layout->disableLayout();
		 $examdate = $this->_getParam('examdate');
		 $larresults = $this->lobjAutochronepushmodel->fngetvenuelist($examdate);
		 
		/// echo 
		$html = '<form action="" method="POST">
		<table width="100%" class="table">
		          <tr>
		              <th width="5%"></th>
		              <th width="80%">Venue Name</th>
		            </tr>';
		for($i=0;$i<count($larresults);$i++)
		{
			$centername = $larresults[$i]['centername'];
			$idcenter = $larresults[$i]['idcenter'];
			$html.='<tr>
					  <td><input type="checkbox" name=centerids[] value='.$idcenter.'></td>
			          <td>'.$centername.'</td>
			          </tr>';
		}
		$html.='<tr>
		            <td colspan="2"><input type="submit" name="save" id="save" value="save" onclick="">
<input type="button" name="close" id="close" value="close" onclick="fnhidethediv();"></td>
</tr>
</table>
</form>';
		
		//echo $html;
		 	
	}*/
	}
	
		
}
