<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');

class Examination_ExamscheduleController extends Base_Base 
{
	//private $lobjanswer; //db variable
	//private $lobjform;//Form variable
	private $_gobjlogger;
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object 
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjexamschedule = new Examination_Model_Examschedulemodel(); //intialize user db object
		$this->lobjExamscheduleform = new Examination_Form_Examschedule(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjExamscheduleform = $this->lobjExamscheduleform; //send the lobjForm object to the view
		
		$larrprogramnames = $this->lobjexamschedule->fnGetProgramName();
		$this->lobjExamscheduleform->field1->addMultiOption('','Select');
		$this->lobjExamscheduleform->field1->addMultiOptions($larrprogramnames);
		
		$larrcenternames = $this->lobjexamschedule->fngetcenternames();
		$this->lobjExamscheduleform->field5->addMultiOption('','Select');
		$this->lobjExamscheduleform->field5->addMultiOptions($larrcenternames);
		
		/*// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		{
			    $larrformData = $this->_request->getPost ();			    									   
				unset ( $larrformData ['Search'] );
				//print_r($larrformData);die();
				
				$larrresult = $this->lobjanswer->fnGetsearchdetails($larrformData); //searching the values for the businesstype
				$this->view->Active = $larrformData['Active'];	
				
				 $this->view->checkEmpty = 2;
				$this->view->larrresult = $larrresult;
			    $this->lobjform->populate($larrformData);
		}
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/examination/createanswerpaper/index');			
		}*/
	}

public function newfnnewcaleshowAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
        
		$fromdate = $this->_getParam('fromdate');
		$todate = $this->_getParam('todate');
		$larr = explode('-',$fromdate);
		$larr1 = explode('-',$todate);
		
 		$curmonth = date('m'); //month in number 2 digits
	    $monat = date('n'); // month in number 1 digits
	    $jahr = date('Y'); // year in number 4 digits
        $heute = date('d'); //current date in number
        
        $fmonth = date('j',mktime(0,0,0,1,$larr[1],$jahr));
        $tmonth = date('j',mktime(0,0,0,1,$larr1[1],$jahr));
       
        //array of months
        $monate = array('1'=>'January','2'=>'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December');
        $sub_monate = array();
		/*for($k=$fmonth;$k<=$tmonth;$k++)
       	 {
       	  	$sub_monate[$k] = $monate[$k];
       	 }
       $cnt = count($sub_monate);*/
       echo '<table border=0  width=100% align=left>';
       echo '<th colspan=6 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
       //$cnt=0;
       echo '<tr>';
       for($k=$fmonth;$k<=$tmonth;$k++)  
        {
        	$sub_monate[$k] = $monate[$k];
        //for ($spalte=1;$spalte<=4;$spalte++) //4 columns
         // {
	        //$cnt++;
	       // for($j=1;$j<)
		    //declaring variables
	        $mondays="#FFFFF";
		    $tuesdays="#FFFFF";
		    $wednesdays="#FFFFF";
		    $thursdays="#FFFFF";
		    $fridays="#FFFFF";
		    $saturdays="#FFFFF";
		    $sundays="#FFFFF";
            $presentyear = date('Y');
            $curmonth = date('m');
		    //for enabling the dates of particular day
		    $monday=1;
		    $tuesday=1;
		    $wednesday=1;
		    $thursday=1;
		    $friday=1;
		    $saturday=1;
		    $sunday=1;
		    
                $this_month = $k;//($reihe-1)*4+$spalte;// 1
               
				$erster = date('w',mktime(0,0,0,$this_month,1,$jahr));// 0
				 
				$insgesamt = date('t',mktime(0,0,0,$this_month,1,$jahr)); //31
				
				if($erster==0)
				{
					$erster=7;
				}
				// table for header of calender
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$sub_monate[$k].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				
				$i = 1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i = 1;
				while($i<=$insgesamt)
				{
				 $rest = ($i+$erster-1)%7;
				 if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				 else{
					echo '<td  align=center ';}
				$curdate = 0;
				if($i<$curdate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
				else 
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
 	       //}

}
echo '</tr>';
echo '</table>';

$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Displayed the calender"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	
}
	 }