<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
//phpinfo();die();

class Examination_CreateanswerpaperController extends Base_Base 
{
	//private $lobjanswer; //db variable
	//private $lobjform;//Form variable
	private $_gobjlogger;
	
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjanswer = new Examination_Model_Answer(); //intialize user db object
		$this->lobjform = new App_Form_Search(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction() 
	{   
		$this->view->checkEmpty = 1;
		$this->view->Active = 1;	 
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Submit' )) {
					$larrformData = $this->_request->getPost ();
					if ($this->lobjform->isValid($larrformData)){
						$lstrpassword = $larrformData ['passwd'];
						$larrformData ['passwd'] = md5 ( $lstrpassword );
						 
						$resultpassword = $this->lobjanswer->fncheckSuperUserPwd($larrformData['passwd']);

						if(!empty($resultpassword['passwd']) && $larrformData['passwd'] == $resultpassword['passwd'])
					{
						 $this->view->resultpassword =  $resultpassword['passwd'];
						 
						 $this->view->checkEmpty = 2;
						// $this->_redirect( $this->baseUrl . '/examination/createanswerpaper/search');
					 }
					 else{
					 	 echo "<script> alert('Please Enter Valid Password of Super Admin')</script>";
					 }
			}		
			}
		
		
		 if(!$this->_getParam('search') ) 
			unset($this->gobjsessionsis->createexampaginatorresult);	
		
		$larrprogramnames = $this->lobjanswer->fnGetProgramName();
		$this->lobjform->field1->addMultiOption('','Select');
		$this->lobjform->field1->addMultiOptions($larrprogramnames);
		
		$larrcenternames = $this->lobjanswer->fngetcenternames();
		$this->lobjform->field5->addMultiOption('','Select');
		$this->lobjform->field5->addMultiOptions($larrcenternames);
		
		
		
		//$larrresult = $this->lobjanswer->fnGetdetails();
		
		$lintpagecount = $this->gintPageCount; 
		$lintpage = $this->_getParam('page',1);
		if(isset($this->gobjsessionsis->createexampaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->createexampaginatorresult,$lintpage,$lintpagecount);
			 $this->view->checkEmpty = 2;
		} 
			
		// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		{
			    $larrformData = $this->_request->getPost ();			    									   
				unset ( $larrformData ['Search'] );
					
				//print_r($larrformData);die();
				
				$larrresult = $this->lobjanswer->fnGetsearchdetails($larrformData); //searching the values for the businesstype
				
				if(!empty($larrresult) ){
					$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
					$this->gobjsessionsis->createexampaginatorresult = $larrresult;
				$this->view->Active = $larrformData['Active'];	
				
				 $this->view->checkEmpty = 2;
				//$this->view->larrresult = $larrresult;
			    $this->lobjform->populate($larrformData);
				}else{
					$this->view->larrNoResult = "No Data Found";
					$this->view->checkEmpty = 2;
				}
		}
		// check if the user has clicked the clear
		/*if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/examination/createanswerpaper/index');			
		}*/
	}
	

		///////////////////
	public function pdfexportAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IDApp = $this->_getParam('id');
		//$progname = $this->_getParam('prg');
		//$name = $this->_getParam('name');
		$Active = $this->_getParam('Active');
		
		$arrstuddtls = $this->lobjanswer->fnGetStudentDetails($IDApp);
		
		$larrcentrestartdtls = $this->lobjanswer->fnGetcentrestartDetails($IDApp);
		
		$displayquestions= $this->lobjanswer->fndisplayquestions($IDApp);
		$questionids = explode(',',$displayquestions['idquestions']);
		
		$count = count($questionids);
		
		//Student Details
		$Studentname = $arrstuddtls['FName'];
		$ICNO = $arrstuddtls['ICNO'];
		$StudExamdate = $arrstuddtls['StudExamdate'];
		$managesessionname = $arrstuddtls['managesessionname'];
		$centername = $arrstuddtls['centername'];
		$ProgramName= $arrstuddtls['ProgramName'];
		$NoOfQuestions = $arrstuddtls['NoofQtns'];
		$NoOfAnswered = $arrstuddtls['Attended'];
		$UnAnswered = $NoOfQuestions - $NoOfAnswered;
		$NoOfCorrect = $arrstuddtls['Correct'];
		$Marks = $arrstuddtls['Correct'];
		$NoOfWrong = $NoOfAnswered - $NoOfCorrect;
		$Grade = $arrstuddtls['Grade'];
		$scheduledtime = $arrstuddtls['ExamSessionTime'];
		$centrestarttime = $larrcentrestartdtls['ExamStartTime'];
		$studstarttime = $arrstuddtls['Studentstarttime'];
		$qarray = array();
		$aarray = array();
		for($j=0;$j<$count;$j++)
		{
			
			$qarray[$j] = $this->lobjanswer->fngetquestions($questionids[$j]);
		}
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		//$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		//$stylesheet = file_get_contents('../public/css/default.css');	
		//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Answer" ).' '.$this->view->translate( "Paper" );
		
		//$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		//$mpdf->WriteHTML();
		
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Student Name&nbsp;</b></td><td width="50%"><b>: </b>'.$Studentname.'</td><td width="13%"><b>IC No&nbsp;</b></td><td width="23%"><b>: </b>'.$ICNO.'</td><td width="14%"><b>Course&nbsp;</b></td><td width="28%"><b>: </b>'.$ProgramName.'</td></tr></table>';
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Venue&nbsp;</b></td><td width="50%"><b>: </b>'.$centername.'</td><td width="13%"><b>Exam Date&nbsp;</b></td><td width="23%"><b>: </b>'.$StudExamdate.'</td><td width="14%"><b>Exam Session&nbsp;</b></td><td width="28%"><b>: </b>'.$managesessionname.'</td></tr></table>';
		
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Scheduled Time&nbsp;</b></td><td  width="50%"><b>: </b>'.$scheduledtime.'</td><td width="13%"><b>Centre Start Time&nbsp;</b></td><td width="23%"><b>: </b>'.$centrestarttime.'</td><td width="14%"><b>Student Start Time&nbsp;</b></td><td width="28%"><b>: </b>'.$studstarttime.'</td></tr></table>';
		
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Total Questions&nbsp;</b></td><td width="50%"><b>: </b>'.$NoOfQuestions.'</td><td width="13%"><b>Answered:&nbsp;</b></td><td width="23%"><b>: </b>'.$NoOfAnswered.'</td><td width="14%"><b>Unanswered&nbsp;</b></td><td width="28%"><b>: </b>'.$UnAnswered.'</td></tr></table>';
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Correct Answers&nbsp;</b></td><td width="50%"><b>: </b>'.$NoOfCorrect.'</td><td width="13%"><b>Wrong Answers&nbsp;</b></td><td width="23%"><b>: </b>'.$NoOfWrong.'</td><td width="14%"><b>Marks Obtained&nbsp;</b></td><td width="28%"><b>: </b>'.$Marks.'</td></tr></table>';
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Grade&nbsp;</b></td><td  width="50%"><b>: </b>'.$Grade.'</td><td width="13%"></td><td width="23%"></td><td width="14%"></td><td width="28%"></td></tr></table>';
		$tabledata.='<hr></hr><br>';
		?>
		<?php  for($i=1,$s=0;$i<=count($qarray),$s<count($qarray);$s++,$i++){ ?>
		<?php if($Active==0){

	
			
		$tabledata.='<table ><tr>
     				<td valign="top" >'.'Q'.$i.'.'.' </td><td align="left">('.trim($qarray[$s]['QuestionGroup']).').'.' '.trim($qarray[$s]['Question']).'<br></td>
	  				</tr></table><br>';
		
		                $aarray = $this->lobjanswer->fngetStudentAnsweerActualAns($questionids[$s],$IDApp);
		                
						//$aarray = $this->lobjanswer->fngetStudentAnsweerActualAns('254','1067');
		                
		                //$GetAns = $this->lobjanswer->fngetanswersCheck($questionids[$s],$IDApp);
		                
		            	/*for($d=1,$a=0;$d<=count($aarray),$a<count($aarray);$d++,$a++){
		            		if($GetAns['Answer']==$aarray[$a]['Studidanswers']){
		            		$checked="true";
		            		}else{
		            			$checked="";
		            		}
		            		checked='.$checked.'
		            		bgcolor="#FF0000"
		            		
		            		&& !empty($aarray['StudAns'])
		            		
		            		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" checked='.$checked.'  name="sports" value="soccer"  />'.'A'.$i.'.'.$questionids[$s].')'.' '.$aarray[$a]['answers'].'<br></td>
		            		*/
								if(!empty($aarray['StudAns']))
								{
									if($aarray['Correctidanswers']==$aarray['Studidanswers']  )
									{
							            	$tabledata.='<table><tr>
							            	<td valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"   name="sports" value="soccer" checked="true"  />'.'A'.$i.')'.'&nbsp;</td><td>'.$aarray['CorrectAns'].'<br></td>
						  					</tr></table><br>';	
											$tabledata.='<table><tr>
											<td valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="background-color:green;"><input type="checkbox"  name="sports" value="soccer" checked="true" /></b>'.'A'.$i.')'.'&nbsp;</td><td>'.$aarray['StudAns'].'<br></td>
						  					</tr></table><br>';
									}else{
											
											$tabledata.='<table><tr>
							            	<td valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"   name="sports" value="soccer" checked="true"  />'.'A'.$i.')'.'&nbsp;</td><td>'.$aarray['CorrectAns'].'<br></td>
						  					</tr></table><br>';	
											$tabledata.='<table><tr>
											<td valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="background-color:red;"><input type="checkbox"  name="sports" value="soccer" checked="true"  /></b>'.'A'.$i.')'.'&nbsp;</td><td>'.$aarray['StudAns'].'<br></td>
						  					</tr></table><br>';
									}
								}//end of if
								else{
											$tabledata.='<table><tr>
							            	<td valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"   name="sports" value="soccer" checked="true"  />'.'A'.$i.'.'.$questionids[$s].')'.'&nbsp;</td><td>'.$aarray['CorrectAns'].'<br></td>
						  					</tr></table><br>';	
											$tabledata.='<table><tr>
											<td valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"  name="sports" value="soccer" checked="true" />'.'A'.$i.'.'.$questionids[$s].')'.'&nbsp;</td><td>'.'Unanswered'.'<br></td>
						  					</tr></table><br>';
									
								}//end of else	
					}//end of if 
					else{
						
							
		$tabledata.='<table ><tr>
     				<td valign="top" >'.'Q'.$i.'.'.'&nbsp;'.$questionids[$s].'.'.' </td><td align="left">('.trim($qarray[$s]['QuestionGroup']).').'.' '.trim($qarray[$s]['Question']).'<br></td>
	  				</tr></table><br>';
		
		                $aarray = $this->lobjanswer->fngetStudentAnsweerActualAns($questionids[$s],$IDApp);
		                
						//$aarray = $this->lobjanswer->fngetStudentAnsweerActualAns('254','1067');
		                
		                //$GetAns = $this->lobjanswer->fngetanswersCheck($questionids[$s],$IDApp);
		                
		            	/*for($d=1,$a=0;$d<=count($aarray),$a<count($aarray);$d++,$a++){
		            		if($GetAns['Answer']==$aarray[$a]['Studidanswers']){
		            		$checked="true";
		            		}else{
		            			$checked="";
		            		}
		            		checked='.$checked.'
		            		bgcolor="#FF0000"
		            		
		            		&& !empty($aarray['StudAns'])
		            		
		            		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" checked='.$checked.'  name="sports" value="soccer"  />'.'A'.$i.'.'.$questionids[$s].')'.' '.$aarray[$a]['answers'].'<br></td>
		            		*/
								if(!empty($aarray['StudAns']))
								{
									if($aarray['Correctidanswers']==$aarray['Studidanswers']  )
									{
							            	$tabledata.='<table><tr>
							            	<td valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"   name="sports" value="soccer" checked="true"  />'.'A'.$i.'.'.$questionids[$s].')'.'&nbsp;</td><td>'.$aarray['Correctidanswers'].'<br></td>
						  					</tr></table><br>';	
											$tabledata.='<table><tr>
											<td align="left" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="background-color:green;"><input type="checkbox"  name="sports" value="soccer" checked="true" /></b>'.'A'.$i.'.'.$questionids[$s].')'.'&nbsp;</td><td>'.$aarray['Studidanswers'].'<br></td>
						  					</tr></table><br>';
									}else{
											
											$tabledata.='<table><tr>
							            	<td  valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"   name="sports" value="soccer" checked="true"  />'.'A'.$i.'.'.$questionids[$s].')'.'&nbsp;</td><td>'.$aarray['Correctidanswers'].'<br></td>
						  					</tr></table><br>';	
											$tabledata.='<table><tr>
											<td valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="background-color:red;"><input type="checkbox"  name="sports" value="soccer" checked="true"  /></b>'.'A'.$i.'.'.$questionids[$s].')'.'&nbsp;</td><td>'.$aarray['Studidanswers'].'<br></td>
						  					</tr></table><br>';
									}
								}//end of if
								else{
									
											$tabledata.='<table><tr>
							            	<td  valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"   name="sports" value="soccer" checked="true"  />'.'A'.$i.'.'.$questionids[$s].')'.'&nbsp;</td><td>'.$aarray['Correctidanswers'].'<br></td>
						  					</tr></table><br>';	
											$tabledata.='<table><tr>
											<td valign="top"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"  name="sports" value="soccer" checked="true" />'.'A'.$i.'.'.$questionids[$s].')'.'&nbsp;</td><td>'.'Unanswered'.'<br></td>
						  					</tr></table><br>';
									
								}//end of else	   
					}//end of else
		          }// end of for

		?><?php 
		//set auto page breaks
		//$mpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		//$mpdf->WriteHTML('<tocentry content="A4 landscape" /><p>'.$tabledata.''.'</p>');
		//$mpdf->autoPageBreak = false;
		//$mpdf->AddPage();
		
		
		$mpdf->WriteHTML($tabledata);  
		$mpdf->Output($Studentname.'_'.'Answer_Paper_Report','D');
		
		$auth = Zend_Auth::getInstance();
    	// Write Logs
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Printed the Answer Paper Report for IdApplication =".$IDApp."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);
	
	}
	
	public function pdfexport1Action()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$IDApp = $this->_getParam('id');
		$progname = $this->_getParam('prg');
		$name = $this->_getParam('name');
		//$tosdesc = $this->lobjanswer->fngettosdesc($RegID);
		$displayquestions= $this->lobjanswer->fndisplayquestions($IDApp);
		$questionids = explode(',',$displayquestions['idquestions']);
		$count = count($questionids);
		$qarray = array();
		$aarray = array();
		for($j=0;$j<$count;$j++)
		{
			$qarray[$j] = $this->lobjanswer->fngetquestions($questionids[$j]);
		}
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Answer" ).' '.$this->view->translate( "Paper" );
		$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		
		$tabledata = 'Student Name :&nbsp;'.$name.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Course :&nbsp;'.$progname;
		$tabledata.='<hr></hr><br>';
		?>
		<?php  for($i=1,$s=0;$i<=count($qarray),$s<count($qarray);$s++,$i++){ ?>
		<?php 
		$tabledata.='<tr>
     				<td width="20">'.'Q'.$i.'</td><td>. '.$qarray[$s]['Question'].'<br></td>
	  				</tr><br>';
		                $aarray = $this->lobjanswer->fngetanswers($questionids[$s]);
		                
		                $GetAns = $this->lobjanswer->fngetanswersCheck($questionids[$s],$IDApp);
		                
		            	for($d=1,$a=0;$d<=count($aarray),$a<count($aarray);$d++,$a++){
		            		if($GetAns['Answer']==$aarray[$a]['idanswers']){
		            		$checked="true";
		            		}else{
		            			$checked="";
		            		}
		            		
						$tabledata.='<tr>
						<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" checked='.$checked.'  name="sports" value="soccer"  />'.'A'.$i.'.'.$d.')'.' '.$aarray[$a]['answers'].'<br></td>
	  					</tr><br>';
		           }
		            }
		?><?php 
		$mpdf->WriteHTML($tabledata);   
		$mpdf->Output('Answer_Paper_Report','D');
		
		$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Printed the Answer Paper Report for IdApplication =  ".$IDApp."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	
	}
	
	
		

 }
