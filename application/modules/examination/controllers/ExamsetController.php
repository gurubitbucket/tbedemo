<?php
class Examination_ExamsetController extends Base_Base 
{
	private $lobjGroup; //db variable
	private $lobjGroupform;//Form variable
	private $_gobjlogger;
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object 
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjGroup = new Examination_Model_Examsetmodel(); //intialize user db object
		$this->lobjGroupform = new Examination_Form_Examset(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction() 
	{    	 
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view
		$larrresult = $this->lobjGroup->fnGetExamDetails(); //get businesstype details
			
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->Businesstypepaginatorresult); // clear the search session
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->Businesstypepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Businesstypepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
	
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/examination/group/index');			
		}
		
	}
	
	
    
    //function to update the businesstype
	public function viewAction()
	{   	
		$this->view->lobjGroupform = $this->lobjGroupform; //send the Form the view
		$idTOSMaster=$this->_Getparam("id"); // get the id of the group to be updated
		
		//echo $id;die();
		
		//$larrtosdetails=$this->lobjGroup->
		
		
		$larrrtosfulldetails=$this->lobjGroup->fnGetTOSFullDetails($idTOSMaster);
		
		$prgname=$this->lobjGroup->fnGetCourse($idTOSMaster);
		$this->view->prgramname=$prgname['ProgramName'];
		$this->view->setname=$prgname['BatchName'];
		$this->view->tosfull=$larrrtosfulldetails;
		
		if(!empty($larrrtosfulldetails[0]['IdBatch']))
		{
		$larrgetotalparts=$this->lobjGroup->fngettotalparts($larrrtosfulldetails[0]['IdBatch']);
		
		for($idp=0;$idp<count($larrgetotalparts);$idp++)
		{
			if($idp==0)
			{
				$idparts=$larrgetotalparts[$idp]['IdPart'];
				
				$idparts="'$idparts'";
			}
			else 
			{
				$idpartss=$larrgetotalparts[$idp]['IdPart'];
				$idpartss="'$idpartss'";
				$idparts=$idparts.','.$idpartss;
			}
			
			$idpart2=$larrgetotalparts[$idp]['IdPart'];
			
			$resulttos=$this->lobjGroup->fngetquestionstosdetail($idpart2,$idTOSMaster);
			$ecahpartquestions[$idpart2]=$resulttos['totals'];
			//$larrtotalchapters2[]=$this->lobjGroup->fngetchaptereachpartpool($larrgetotalparts[$idp]['IdPart']);
			
		}
		//echo $idparts;die();
		$larrchpterlevel=$this->lobjGroup->fngetchapterlevel($idparts);//////
		$larrtotalchapters=$this->lobjGroup->fngetchapterfrompool($idparts);//////
		
		$larrpartquestions=$this->lobjGroup->fngettotalpartqusetions($idparts);/////
		
		$larrinactivequestions=$this->lobjGroup->fngettotalinactivequsetions($larrgetotalparts);/////
		$this->view->totalinactivequestions=$larrinactivequestions;
		
		$larrreviewquestions=$this->lobjGroup->fngettotalreviewqusetions($larrgetotalparts);/////
		$this->view->totalreviewquestions=$larrreviewquestions;
		
		//echo "<pre>";print_r($larrreviewquestions);
		
		}
		
		for($lev=0;$lev<count($larrchpterlevel);$lev++)
		{
			$chapter=$larrchpterlevel[$lev]['QuestionNumber'];
			$level=$larrchpterlevel[$lev]['QuestionLevel'];
			$larrlevels[$chapter][$level]=$larrchpterlevel[$lev]['cntlevel'];
		}
		$this->view->chapters=$larrtotalchapters;
		//echo "<pre/>";print_r($larrlevels);
		$this->view->levelchapters=$larrlevels;
		$this->view->groups=$larrpartquestions;
		$this->view->eachgrp=$ecahpartquestions;
		}
	  
	  
}