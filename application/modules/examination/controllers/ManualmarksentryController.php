<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_ManualmarksentryController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjexamtimemodel = new Examination_Model_Manualmarksentry(); //intialize newscreen db object
		$this->lobjexamtimeForm = new Examination_Form_Manualmarksentry(); 
		$this->lobjform = new App_Form_Search(); 
	}
	
	public function indexAction() 
	{
		
		$this->view->checkEmpty = 1;
		$this->view->Active = 1;	 
		$this->view->lobjform = $this->lobjexamtimeForm; //send the lobjForm object to the view
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Submit' )) 
		{
		$larrformData = $this->_request->getPost ();
		if ($this->lobjform->isValid($larrformData)) 
		{
		$lstrpassword = $larrformData ['passwd'];
		$larrformData ['passwd'] = md5 ( $lstrpassword );
		$resultpassword = $this->lobjexamtimemodel->fncheckSuperUserPwd($larrformData['passwd']);
		if(!empty($resultpassword['passwd']) && $larrformData['passwd'] == $resultpassword['passwd'])
		{
		$this->view->resultpassword =  $resultpassword['passwd'];
		$this->view->checkEmpty = 2;
		}
		else
		{
		echo "<script> alert('Please Enter Valid Password of Super Admin')</script>";
		}
		}		
		}
		$larrresult=0;
		
		if($this->_getParam('checked'))
		{
				$this->view->checkEmpty = 2;	
		}
		
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->examtimepaginatorresult);
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		//$larrVenuesresult = $this->lobjexamtimemodel->fnGetVenueNames();	
	//	$this->lobjexamtimeForm->Venues->addMultiOptions($larrVenuesresult);
	//echo count($larrresult);die();
	$this->view->countresult=count($larrresult);
		if(isset($this->gobjsessionsis->examtimepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->examtimepaginatorresult,$lintpage,$lintpagecount);
			$this->view->checkEmpty = 2;
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		$larrformData = $this->_request->getPost ();
		//echo "<pre/>";print_r($larrformData);die();
		
		
		
		
		
		if ($this->lobjform->isValid ( $larrformData )) 
		{
		$this->view->paramsearch =  $this->_getParam('search');
		
		$larrVenuesresult = $this->lobjexamtimemodel->fngetschedulerofdatevenuestudent($larrformData['Dates']);	
		//echo "<pre/>";print_r($larrVenuesresult);die();
                $this->lobjexamtimeForm->Venues->addMultiOptions($larrVenuesresult);
			$this->view->lobjform->Dates->setValue($larrformData['Dates']);
			$this->view->lobjform->Venues->setValue($larrformData['Venues']);
			$larrsessionresult = $this->lobjexamtimemodel->fngetschedulerofdatesessionstudent($larrformData['Dates'],$larrformData['Venues']);	
			//print_r($larrsessionresult);die();
		$this->lobjexamtimeForm->examsession->addMultiOptions($larrsessionresult);
	
			$this->view->lobjform->examsession->setValue($larrformData['examsession']);
	    $larrresult = $this->lobjexamtimemodel->fnSearchstudentabsent($larrformData); //searching the values for the user
	    //echo "<pre>";print_r($larrresult);die();
		$this->view->countresult=count($larrresult);
	    if(!empty($larrresult) )
	    {
		$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		$this->gobjsessionsis->examtimepaginatorresult = $larrresult;
		$this->view->checkEmpty = 2;
		$this->lobjform->populate($larrformData);
		}
		else
		{
		$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		$this->gobjsessionsis->examtimepaginatorresult = $larrresult;
		$this->view->checkEmpty = 2;
		}	
		}
		}
	/*if($this->_getParam('search'))
	{
	$larrresult = $this->lobjexamtimemodel->fnSearchstudentabsent($larrformData); //searching the values for the user
	$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
	$this->gobjsessionsis->examtimepaginatorresult = $larrresult;
	$this->view->checkEmpty = 2;
	}	*/

	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
		$larrformData = $this->_request->getPost ();
		$auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
				
					   	/*	  $idapplication=$larrformData['IDApplication'][0];
				   		  $larrstudetails=$this->lobjexamtimemodel->fngetexamdetails($idapplication);
				   		//  print_r($larrstudetails);die();
		 if((!empty($larrstudetails['DateTime'])) && (!empty($larrstudetails['Examvenue'])) && (!empty($larrstudetails['Examsession'])) )
			{
				
				$examdate=$larrstudetails['DateTime'];
					$examdate="'$examdate'";
				$examvenue=$larrstudetails['Examvenue'];
				$exansession=$larrstudetails['Examsession'];
				
				
				$larrvenue=$this->lobjexamtimemodel->fnupdatevenuetime($examdate,$examvenue,$exansession);
			}  
			die();*/
				if(empty($larrformData['IDApplication']))
				{
					echo '<script language="javascript">alert("Atleast check One candidate")</script>';
		
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/manualmarksentry/index';</script>";
		die();
				}
			//echo "<pre />";
			//print_r($larrformData);
			//echo $larrformData['PartA'][$larrformData['IDApplication'][0]],
			//die();
		
		$this->lobjexamtimemodel->fninsertstudentmarksdetails($larrformData); //Call a function to adding the paypal details to the paypaldetails table
			$this->lobjexamtimemodel->fninsertstudentparttwisedetails($larrformData);//Call a function to adding the paypal details to the manualpaypaldetails table
			$larrgetexamtimes=$this->lobjexamtimemodel->getthesessiontimings($larrformData['examsession']);
			$this->lobjexamtimemodel->fninsertstudentstartexamdetailsdetails($larrformData,$larrgetexamtimes['starttime'],$larrgetexamtimes['endtime']);//Call a function to adding the registration details to the registereddetails table
				
			
			$this->lobjexamtimemodel->fninsertintomanualentry($larrformData,$iduser);
			$checkcentersatrt=$this->lobjexamtimemodel->fngetcenterstartexam($larrformData);
			//echo abc;die();
			if($checkcentersatrt)
			{
				$this->_redirect( $this->baseUrl . '/examination/manualmarksentry/index');	
			}
			else {
				
				  		$values=0;
				  for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){			
			$value=$larrformData['IDApplication'][$linti];
				$values=$values.','.$value;
				
				   	}
				   	$larrtakeprogramids=$this->lobjexamtimemodel->fngetprogramwisestudent($values);
				
				   		  for($linti=0;$linti<count($larrtakeprogramids);$linti++){		
		//print_r($larrtakeprogramids[$linti]['program']);die();
				  	$checkcenterstartprogramwise=$this->lobjexamtimemodel->fncheckstartprogramwise($larrtakeprogramids[$linti]['program'],$larrformData);
				  	if($checkcenterstartprogramwise)
				  	{
				  		
				  	}
				  	else 
				  	{
				  		$startcenterexam=$this->lobjexamtimemodel->fnstartcenterstartexam($larrformData,$larrtakeprogramids[$linti]['program'],$larrgetexamtimes['starttime'],$larrgetexamtimes['endtime']);
				  	}
				   		  }
				   		  
				   		  $idapplication=$larrformData['IDApplication'][0];
				   		  $larrstudetails=$this->lobjexamtimemodel->fngetexamdetails($idapplication);
		 if((!empty($larrstudetails['DateTime'])) && (!empty($larrstudetails['Examvenue'])) && (!empty($larrstudetails['Examsession'])) )
			{
				
				$examdate=$larrstudetails['DateTime'];
				$examdate="'$examdate'";
				$examvenue=$larrstudetails['Examvenue'];
				$exansession=$larrstudetails['Examsession'];
				
				
				$larrvenue=$this->lobjexamtimemodel->fnupdatevenuetime($examdate,$examvenue,$exansession);
			}  
				   		  
			
				$this->_redirect( $this->baseUrl . '/examination/manualmarksentry/index');	
			}
		}
				
		//$this->_redirect( $this->baseUrl . '/examination/manualmarksentry/index');
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$larrresult=0;
					$this->gobjsessionsis->examtimepaginatorresult = $larrresult;
		$this->view->checkEmpty = 2;
			 $this->_redirect( $this->baseUrl . 'examination/manualmarksentry/index/checked/1');
		
		}
	}
	
	public function newexamtimeAction() 
	{	
	
	$this->view->lobjnewscreenForm = $this->lobjexamtimeForm;
	$studentexamgracetime=$this->lobjexamtimemodel->fngetintialgracetimeinfo();		
	$this->view->gracetime=$studentexamgracetime['examgracetime'];
	$this->view->lobjnewscreenForm->ExamDatetime->setValue(date('Y-m-d'));
		
	if ($this->_request->isPost() && $this->_request->getPost('Save')) 
	{
	$larrformData = $this->_request->getPost();
	
	$larrcheckdata=$this->lobjexamtimemodel->fngetalreadythere($larrformData['newexamcity'],$larrformData['examsession'],$larrformData['ExamDatetime']);
	if($larrcheckdata)
	{
		echo '<script language="javascript">alert("This Data is already there")</script>';
		die();
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/examtime/index';</script>";
 		//die();
		
	}
	else 
	{
		//print_r($larrformData);die();
	$auth = Zend_Auth::getInstance();
	$iduser=$auth->getIdentity()->iduser;
	$larrformData['UpdDate']=date('Y-m-d H:i:s');
	$studenteditiinserlarr=$this->lobjexamtimemodel->fngetgracetimeinsertinfo($larrformData,$iduser);
	$this->_redirect( $this->baseUrl . '/examination/examtime/index/search/1');		
	}
	}			
	}
	
	
	

public function schedulerexceptionAction()
{
    $this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$lintday = $this->_getParam('day');//city
	$lintcity = $this->_getParam('city');
	$lintmonth = $this->_getParam('month');
	$year = $this->_getParam('year');
	$days = $year.'-'.$lintmonth.'-'.$lintday;
	$larrresultcity=$this->lobjexamtimemodel->newfnGetcitydetailsgetsecid($lintcity);
	$resultsss = $this->lobjexamtimemodel->fngetschedulerexception($days,$larrresultcity['city']);
	$counts = count($resultsss);
	if($counts>1)
	{
		echo "No exams are offerred on the selected date. It can be a public holiday, please select a different date.";
		die();
		
	}
	else 
	{
		
	}	
	
}	
	
	public function newfngetyearAction()
 {
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
        $year=date('Y');
        $larrvenuetimeresults = $this->lobjexamtimemodel->newfncurrentgetyear($year);
        $larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
 }
		
public function newvenuelistsnamesAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		
	   	$larrschedulerdays=$this->lobjexamtimemodel->fngetschedulerofdatevenuestudent($iddate);
	   	
	   	if($larrschedulerdays)
		{
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdays);
		}
		else 
		{
		$larrschedulerdays="";
		}
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}


public function newvenuelistsemptynamesAction()
{
	 	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$larrCountryStatesDetailss[]=array('key'=>'0','name'=>'Select');
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}




public function newsessionemptynamesAction()
{
	 	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
        $iddate = $this->_getParam('iddate');
		$idvenue = $this->_getParam('idvenues');
		$idsession= $this->_getParam('idsessions');
	
		$larrschedulerdays=$this->lobjexamtimemodel->fngetschedulerofcenterstart($idvenue,$idsession,$iddate);
		$message="";
		if($larrschedulerdays)
		{
		$message="This exam venue is already started";
		}
		else 
		{
		$today=date('Y-m-d');
		if($today==$iddate)
		{
		$time=date('H:i:s');	
		$larrdates=explode('-',$iddate);
		$year= $larrdates[0];
		$month=$larrdates[1];
		$date=$larrdates[2];
		$larrschedulerresults = $this->lobjexamtimemodel->newgetscheduleryear($year);
		
		$values=0;
		for($idsech=0;$idsech<count($larrschedulerresults);$idsech++)
		{
		if(( $month >= $larrschedulerresults[$idsech]['From']) && ($month <= $larrschedulerresults[$idsech]['To'] ))
		{
		$value=$larrschedulerresults[$idsech]['idnewscheduler'];
		$values=$values.','.$value;
		}
		}	
	   	$larrweekday = $this->lobjexamtimemodel->fngetdayofdate($iddate);
	    if($larrweekday['days']==1)
		{
		$larrweekday['days']= 7;
		}
		else 
		{
		$larrweekday['days']=$larrweekday['days']-1;	
		}
			
	   	$larrschedulerdayssession=$this->lobjexamtimemodel->fngetschedulerofdatesessionstudentforcurrentdate($values,$larrweekday['days'],$iddate,$idvenue,$time);
		if($larrschedulerdayssession=='')
		{
		$message="This exam venue session  time is elapsed";
		}
			
		}		
		}

		$larrschedulerresultthere=$this->lobjexamtimemodel->fngetalreadythere($idvenue,$idsession,$iddate);
		if($larrschedulerresultthere)
		{
		$message="This exam venue session data is already present";
		
		}
		echo  $message;

		 
}

public function newsessionlistsnamesAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$iddate = $this->_getParam('iddate');
		$venueid = $this->_getParam('venueid');
		
	
		$larrschedulerdayssession=$this->lobjexamtimemodel->fngetschedulerofdatesessionstudent($iddate,$venueid);
		
		if($larrschedulerdayssession)
		{
		$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdayssession);
		}
		else 
		{
		$larrCountrysessionDetailss='';	
		}
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
}

public function validatestudentprogramAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$Program = $this->_getParam('Program');
		//echo $Program;die();		
	
		$larrschedulerdayssession=$this->lobjexamtimemodel->fngetpartsid($Program);
		//print_r($larrschedulerdayssession['parts']);die();
		
	
		echo $larrschedulerdayssession['parts'];
}



public function validatemaxpartquestionsAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$Program = $this->_getParam('Program');
		$type = $this->_getParam('type');
		//echo $Program;die();		
	
		$larrcountquestions=$this->lobjexamtimemodel->fngetcountquestion($Program,$type);
		//print_r($larrcountquestions['quescount']);die();
		
	
		echo $larrcountquestions['quescount'];
}

///////////////////////////////////////////////////dojo calender date disble operation/////////////////////////

    public function dateAction() {
    	
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		define("DATABASE_SERVER","localhost");
		
		define("DATABASE_NAME","clt14082012");
		define("DATABASE_USER","root");
		define("DATABASE_PWD","root");
		$datess = $this->_getParam('vd');	
		//echo $datess;die();
if (isset($datess)) {
    // refdate contains a partial date like this yyyymm00
    $refdate = "'$datess'";


    $session = mysql_connect(DATABASE_SERVER,DATABASE_USER,DATABASE_PWD);
    if ($session) {
        mysql_select_db(DATABASE_NAME,$session);
        // Query to get all disabled dates for a month
        $query = "select distinct(date) from tbl_venuedateschedule where  (date < " .$refdate . ") and active=1 and Reserveflag=1 order by date asc";
       // echo $query;die();
        $tabs = mysql_query($query,$session);
        mysql_close($session);
        if ($tabs && (mysql_num_rows($tabs) > 0)) {
            $ref = 0;
            while ($row = mysql_fetch_row($tabs)) {
                if ($ref != $row[0]) {
                    if ($ref > 0) print(",");
                    $ref = $row[0];
                    print($ref);
                }
            }
        }
    }
}

    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
}