<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_ChangeindividualpaymentController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	public function fnsetObj()
	{	
		$this->lobjindividualpaymentchangemodel = new  Examination_Model_Changeindividualpayment(); //intialize newscreen db object
		$this->lobjindividualpaymentform = new Examination_Form_Changeindividualpayment(); 

	}
	
	public function indexAction() 
	{
		$this->view->lobjindividualpaymentform = $this->lobjindividualpaymentform;
		
	 	$larrresult=0;
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->studentelapsedexamdatepaginatorresult);		
		$larrCourseresult = $this->lobjindividualpaymentchangemodel->fnGetCourseNames();	
		$this->lobjindividualpaymentform->Coursename->addMultiOptions($larrCourseresult);
		$larrVenuesresult = $this->lobjindividualpaymentchangemodel->fnGetVenueNames();	
		$this->lobjindividualpaymentform->Venues->addMultiOptions($larrVenuesresult);
		//$larrTakafulresult = $this->lobjindividualpaymentchangemodel->fnGetTakafulNames();	
		//$this->lobjnewscreenForm->Takafulname->addMultiOptions($larrTakafulresult);	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData ))
			{
				$this->view->paramsearch =  $this->_getParam('search');			
					if($larrformData['Studentname'])
					{		
					     $this->lobjindividualpaymentform->Studentname->setValue($larrformData['Studentname']);
					}
					if($larrformData['ICNO'])
					{	
					     $this->lobjindividualpaymentform->ICNO->setValue($larrformData['ICNO']);
					}
					if($larrformData['Date'])
					{
					    $this->lobjindividualpaymentform->Date->setValue($larrformData['Date']);
					}
			      
					if($larrformData['Coursename'])
					{
					    $this->lobjindividualpaymentform->Coursename->setValue($larrformData['Coursename']);
					}
					if($larrformData['Venues'])
					{
					   $this->lobjindividualpaymentform->Venues->setValue($larrformData['Venues']);
					}
			 		if($larrformData['paymentmode'])
					{
					    $this->lobjindividualpaymentform->paymentmode->setValue($larrformData['paymentmode']);
					}
					
					
				 $larrresult = $this->lobjindividualpaymentchangemodel->fnindividualpayment($larrformData); //searching the values for the user				
				$this->view->larrresult =$larrresult;
				 $this->view->paginator = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl.'/examination/changeindividualpayment/index');
		}
	}
	
	public function editstudentinfoAction() 
	{	
		
		$this->view->lobjindividualpaymentform = $this->lobjindividualpaymentform;

		$this->view->idstudent = $lintidstudent = $this->_getParam('id');


		$this->view->id = $lintidstudent;
		$larrstudentinforesult= $this->lobjindividualpaymentchangemodel->fngetstudenteachinformation($lintidstudent); 		
		$this->view->Fname = $larrstudentinforesult['FName'];
		$this->view->ICNO = $larrstudentinforesult['ICNO'];
		$this->view->EmailAddress = $larrstudentinforesult['EmailAddress'];
		$this->view->Program = $larrstudentinforesult['ProgramName'];
	    $this->view->Venue = $larrstudentinforesult['centername'];
	    $this->view->oldpayment = $larrstudentinforesult['ModeofPayment'];
	    $this->view->Idapplication = $lintidstudent;
	    $this->view->amount = $larrstudentinforesult['Amount'];
	    $email = $larrstudentinforesult['EmailAddress'];
		$this->view->examdate = $larrstudentinforesult['ExamDate'];
		$this->view->session  = $larrstudentinforesult['managesessionname'];
		$this->view->ModeOfPayment   =$larrstudentinforesult['ModeofPayment'];


		if($larrstudentinforesult['ModeofPayment'] ==10)
		{
                   $larrpaymenttypes = array(array(key=>1,value=>"FPX"),array(key=>7,value=>"Credit To IBFIM"));                   
                   $this->lobjindividualpaymentform->PaymentmodeTo->addmultioptions($larrpaymenttypes);
		   $this->lobjindividualpaymentform->PaymentmodeTo->setValue(1);
		   
		}
		else if($larrstudentinforesult['ModeofPayment'] ==1)
		{
                   $larrpaymenttypes = array(array(key=>7,value=>"Credit To IBFIM"),array(key=>10,value=>"Credit Card"));
                   $this->lobjindividualpaymentform->PaymentmodeTo->addmultioptions($larrpaymenttypes);
		   $this->lobjindividualpaymentform->PaymentmodeTo->setValue(10);
		   
		}
                else if($larrstudentinforesult['ModeofPayment'] ==7)
		{
                   $larrpaymenttypes = array(array(key=>1,value=>"FPX"),array(key=>10,value=>"Credit Card"));
                   $this->lobjindividualpaymentform->PaymentmodeTo->addmultioptions($larrpaymenttypes);
		   $this->lobjindividualpaymentform->PaymentmodeTo->setValue(10);
		   
		}





	    switch ($larrstudentinforesult['ModeofPayment'])
	    {
	    	case 1: $paymenttype = 'FPX';
	    	        break;
	   	    case 2: $paymenttype = 'Credit Card';
	    	        break;
	    	case 5: $paymenttype = 'Money Order';
	    	        break;
	   	    case 6: $paymenttype = 'Postal Order';
	    	        break;
	    	case 7: $paymenttype = 'Credit/Bank to IBFIM account';
	    	        break;
	   	    case 10: $paymenttype = 'Credit-Card';
	    	        break;
	    }
	    $this->view->paymenttype = $paymenttype;
	    
	    $idbatch=$larrstudentinforesult['IdBatch'];
	    $takafuloperator=0;
	   if ($this->_request->isPost () && $this->_request->getPost ( 'Save' ))
	   {
			 $larrformdata = $this->_request->getPost();
//echo "<pre>";
//print_r($larrformdata);die();			
			 $auth = Zend_Auth::getInstance();
			 $iduser=$auth->getIdentity()->iduser;
			 $insertedtemp = $this->lobjindividualpaymentchangemodel->fninserttemp($larrformdata,$iduser);			 
			 $updatequeryresult = $this->lobjindividualpaymentchangemodel->fnupdatestudentpayment($larrformdata);			
			 //if($larrformdata['paymentmode']=='5' ||$larrformdata['paymentmode']=='6' || $larrformdata['paymentmode']=='7')
			// {
 				
			 	//$resultinserted = $this->lobjindividualpaymentchangemodel->fninsertedstudentdetails($larrformdata,$iduser);
			 
			// }



			 if($larrformdata['PaymentmodeTo']=='2' ||$larrformdata['PaymentmodeTo']=='10')
			 {
 					$resultinserted = $this->lobjindividualpaymentchangemodel->fncreditcardpaymentdetails($larrformdata,$iduser);		 	
			 }
			 else if($larrformdata['PaymentmodeTo']=='1')
			 {
			 	
			 	$resultinserted = $this->lobjindividualpaymentchangemodel->fnfpxpaymentdetails($larrformdata,$iduser,$email);	
			 }




			 $larrestultregids = $this->lobjindividualpaymentchangemodel->fninsertregdetails($lintidstudent,$takafuloperator,$idbatch);
			 $larrregid = $this->lobjindividualpaymentchangemodel->fngetRegid($lintidstudent);
			 
			 //
			 
		$studentapplicationarray = $this->lobjindividualpaymentchangemodel->fnviewstudentdetailssss($lintidstudent);			 
	    $larrEmailTemplateDesc =  $this->lobjindividualpaymentchangemodel->fnGetEmailTemplateDescription("Student Application");
					
									
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$studentapplicationarray['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$studentapplicationarray['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$studentapplicationarray['ProgramName'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[venue]",$studentapplicationarray['centername'],$lstrEmailTemplateBody);
                                   		$lstrEmailTemplateBody = str_replace("[venue]",$studentapplicationarray['centername'].' '.$studentapplicationarray['addr1'].' '.$studentapplicationarray['addr2'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$studentapplicationarray['Examdate'].'-'.$studentapplicationarray['Exammonth'].'-'.$studentapplicationarray['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$studentapplicationarray['PermAddressDetails'].'-'.$studentapplicationarray['CorrAddress'],$lstrEmailTemplateBody);
										
										//$lstrEmailTemplateBody = str_replace("[Session]",$studentapplicationarray['managesessionname'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Session]",$studentapplicationarray['managesessionname'].'('.$studentapplicationarray['starttime'].'--'.$studentapplicationarray['endtime'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$studentapplicationarray['managesessionname'].'('.$studentapplicationarray['ampmstart'].'--'.$studentapplicationarray['ampmend'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$studentapplicationarray['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$studentapplicationarray["username"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$studentapplicationarray['password'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										

										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'iTWINE';
										$receiver_email = $studentapplicationarray["EmailAddress"];
										$receiver = $studentapplicationarray['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										//$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {

                                                                             if(strtotime($larrformdata['examdate']) >= strtotime(date('d-m-Y')))
									     {
                                                                                 //echo "fine";die();

									         $result = $mail->send($transport);
									     }
										 else
										 {
                                                                                    //echo "elsepart";die();
										    $this->_redirect( $this->baseUrl . "/examination/changeindividualpayment/index");	 
										 }
									//$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Because of server problem mails cannot be send this time")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/individualpaymentchange/index';</script>";
				                	// die();
								}
			 
		                         $this->_redirect( $this->baseUrl . "/examination/changeindividualpayment/index");	 
		}
		
	
	}
 public function fnupdatepaymentoptionAction()
 {  
	        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$idstudent = $this->_getParam('idstudent');

		$larrvenuetimeresults = $this->lobjindividualpaymentchangemodel->fnstudentpaymentoption($idstudent);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
 }		
		
}
