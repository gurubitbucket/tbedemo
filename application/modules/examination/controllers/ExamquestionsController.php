<?php
ini_set('display_errors','On');
ini_set('memory_limit','-1');
class Examination_ExamquestionsController extends Base_Base 
{	
    public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjExammarksModel = new Examination_Model_Examquestions();
		$this->lobjExammarksform = new  Examination_Form_Examquestions();
		$this->lobjform = new App_Form_Search(); //intialize user lobjbusinesstypeForm
		$this->lobjanswer = new Examination_Model_Answer(); //intialize user db object
	}
	public function indexAction() 
	{    
		$this->view->checkEmpty = 1;
		$this->view->Active = 1;	 
		$this->view->lobjformsearch = $this->lobjform; //send the lobjForm object to the view
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Submitpw' )) {
					$larrformData = $this->_request->getPost ();
					if ($this->lobjform->isValid($larrformData)){
						$lstrpassword = $larrformData ['passwd'];
						$larrformData ['passwd'] = md5 ( $lstrpassword );
						 
						$resultpassword = $this->lobjanswer->fncheckSuperUserPwd($larrformData['passwd']);

						if(!empty($resultpassword['passwd']) && $larrformData['passwd'] == $resultpassword['passwd'])
					{
						 $this->view->resultpassword =  $resultpassword['passwd'];
						 
						 $this->view->checkEmpty = 2;
						// $this->_redirect( $this->baseUrl . '/examination/createanswerpaper/search');
					 }
					 else{
					 	 echo "<script> alert('Please Enter Valid Password of Super Admin')</script>";
					 }
			}		
			}
		
		$this->view->lobjform = $this->lobjExammarksform;
		$total=$this->lobjExammarksModel->fngettotalquestions();
		//echo $total['count'];die();
		
		for($linti=1;$linti<=$total['count'];$linti++){
			$larrrand[$linti] = $linti;
		}
		$questiongroup=$this->lobjExammarksModel->fngetquestiongroup();
		$this->lobjExammarksform->Questiongroup->addMultiOptions($questiongroup);
		$this->lobjExammarksform->Marksfrom->addMultiOptions($larrrand);
		$this->lobjExammarksform->Marksto->addMultiOption($total['count']+1,"Ending");
		$this->lobjExammarksform->Marksto->addMultiOptions($larrrand);
		$this->lobjExammarksform->QuestionChapter->removeMultiOption('0');
		
		$this->view->last=$total['count']+1;
		
		
		
		//$larrquestionchapter = $this->lobjExammarksModel->fnGetChapterGroup();
		//$this->lobjExammarksform->QuestionChapter->addMultiOptions($larrquestionchapter);
		
		if($this->_request->isPost() && $this->_request->getPost('Search')){
			$formdata=$this->_request->getPost();
			Unset($formdata['Search']);
			$this->view->checkEmpty = 2;
			$this->lobjExammarksform->Questiongroup->setvalue($formdata['Questiongroup']);
			$this->lobjExammarksform->Questionstatus->setvalue($formdata['Questionstatus']);
			$this->lobjExammarksform->Marksfrom->setvalue($formdata['Marksfrom']);
			$this->lobjExammarksform->Marksto->setvalue($formdata['Marksto']);
			$this->lobjExammarksform->QuestionLevel->setvalue($formdata['QuestionLevel']);
			
			$this->lobjExammarksform->QuestionChapter->addMultiOption('0',"Select");
			$larresult = $this->lobjExammarksModel->fnGetChapterGroup($formdata['Questiongroup']);
			$this->lobjExammarksform->QuestionChapter->addMultiOptions($larresult);
			//if(!empty($formdata['QuestionChapter'])){
			$this->lobjExammarksform->QuestionChapter->setvalue($formdata['QuestionChapter']);
			//}
			$this->lobjExammarksform->Questionname->setvalue($formdata['Questionname']);
			$namespace = new Zend_Session_Namespace('exampdf');
        	$namespace->data = $formdata['Questionname'];
			$this->view->displayquestions=$displayquestions = $this->lobjExammarksModel->fndisplayquestionsforabovecode($formdata['Questiongroup'],$formdata['Questionstatus'],$formdata['Marksfrom'],$formdata['Marksto'],$formdata['QuestionLevel'],$formdata['QuestionChapter'],$formdata['Questionname']);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost('submit')){ 

				$larrformData = $this->_request->getPost();
			    Unset($larrformData['submit']);
			   	$questiongroup=$larrformData['Questiongroup'];
			    $questionstatus=$larrformData['Questionstatus'];
			    $fromqtn = $larrformData['Marksfrom'];
			    $toqtn = $larrformData['Marksto'];
			    $QuestionLevel = $larrformData['QuestionLevel'];
			    $QuestionChapter = $larrformData['QuestionChapter'];
			    $QuestionChapter = $larrformData['QuestionChapter'];
			    //$Questionname=$larrformData['Questionname'];
			    $this->view->checkEmpty = 2;
			    $kk=$this->_redirect(  $this->baseUrl  ."/examination/examquestions/fnexport/fromqtn/$fromqtn/toqtn/$toqtn/questiongroup/$questiongroup/questionstatus/$questionstatus/QuestionLevel/$QuestionLevel/QuestionChapter/$QuestionChapter");
			}
			
	/*if ($this->_request->isPost () && $this->_request->getPost('submitnew')){ 

				$larrformData = $this->_request->getPost();
			    Unset($larrformData['submit']);
			   	$questiongroup=$larrformData['Questiongroup'];
			    $questionstatus=$larrformData['Questionstatus'];
			    $fromqtn = $larrformData['Marksfrom'];
			    $toqtn = $larrformData['Marksto'];
			    $QuestionLevel = $larrformData['QuestionLevel'];
			    $QuestionChapter = $larrformData['QuestionChapter'];
			    $QuestionChapter = $larrformData['QuestionChapter'];
			    //$Questionname=$larrformData['Questionname'];
			    $this->view->checkEmpty = 2;
			    $kk=$this->_redirect(  $this->baseUrl  ."/examination/examquestions/fnexportnew/fromqtn/$fromqtn/toqtn/$toqtn/questiongroup/$questiongroup/questionstatus/$questionstatus/QuestionLevel/$QuestionLevel/QuestionChapter/$QuestionChapter");
			}*/
    }
    
	public function fngetchaptersAction()
     {
     	$this->_helper->layout->disableLayout();
     	$this->_helper->viewRenderer->setNoRender();
		$group = $this->_getParam('group');
		//echo $group;die();
		$larresult = $this->lobjExammarksModel->fnGetChapterGroup($group);
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
     }
    
    
	
	public function fnexportAction()
    {   
     
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$fromqtn = $this->_getParam('fromqtn');
		$toqtn = $this->_getParam('toqtn');
		$questiongroup = $this->_getParam('questiongroup');
		$questionstatus = $this->_getParam('questionstatus');
		$questionslevel = $this->_getParam('QuestionLevel');
		$questionschapter = $this->_getParam('QuestionChapter');
		$namespace = new Zend_Session_Namespace('exampdf');
		$Questionname=$namespace->data; 
		//echo $Questionname;die();
		$displayquestions = $this->lobjExammarksModel->fndisplayquestionsforabovecode($questiongroup,$questionstatus,$fromqtn,$toqtn,$questionslevel,$questionschapter,$Questionname);
		if(!empty($questiongroup) && isset($questiongroup)){
			$qsngroupnamearray=$this->lobjExammarksModel->fngetgroupname($questiongroup);
			$qsngroupname=$qsngroupnamearray['groupname'];
		}else{
			$qsngroupname="All";	
		}
		if(isset($questionstatus) && $questionstatus!='3'){
			if($questionstatus==1){
					$qsnstatus="Active";
			}else if($questionstatus==0){
					$qsnstatus="InActive";
			}else if($questionstatus==2){
				$qsnstatus="Review";
			}
		}else{
			$qsnstatus="All";
		}
		if(isset($fromqtn) && !empty($fromqtn)){
			$fromqsn=$fromqtn;	
		}else{
			$fromqsn="1";
		}
    	if(isset($toqtn) && !empty($toqtn)){
			$toqsn=$toqtn;	
		}
		
		if(isset($questionslevel) && !empty($questionslevel)){
			if($questionslevel=='1'){
				$questionslevelrep="Easy";
			}else if($questionslevel=='2'){
				$questionslevelrep="Medium";
			}else if($questionslevel=='3'){
				$questionslevelrep="Difficult";
			}
		}else{
			$questionslevelrep="All";
		}
		
		if(isset($questionschapter) && !empty($questionschapter)){
			$questionschapterrep=$questionschapter;
		}else{
			$questionschapterrep="All";
		}
		
    	if(isset($Questionname) && !empty($Questionname)){
			$questname=$Questionname;
		}else{
			$questname="";
		}
    	
		
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$stylesheet = file_get_contents(APPLICATION_PATH.'/../library/MPDF53/mpdfstyletables.css');
		$mpdf->WriteHTML($stylesheet,1);
		
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Exam" ).' '.$this->view->translate( "Questions" );
		ini_set('max_execution_time',3600);
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		//echo $imgp;die();
		$tabledata = '<img width=100% src= "'.$imgp.'" />';
		
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table width=100% border=0>
						<tr><td><b>Questions Group &nbsp;:</b></td>
							<td><b>'.$qsngroupname.'</b></td>
							<td><b>Questions Chapter &nbsp;:</b></td>
							<td><b>'.$questionschapterrep.'</b></td>
							
						</tr>
						<tr><td><b>Questions Level &nbsp;:</b></td>
							<td><b>'.$questionslevelrep.'</b></td>
							<td><b>Questions Status &nbsp;:</b></td>
							<td><b>'.$qsnstatus.'</b></td>
						</tr>
						<tr><td><b>Questions From &nbsp;:</b></td>
							<td><b>'.$fromqsn.'</b></td>
							<td><b>Questions To &nbsp;:</b></td>
							<td><b>'.$toqsn.'</b></td>
						</tr>';
		if(!empty($questname)){
			$tabledata.= '<tr><td><b>Question Name&nbsp;:</b></td>
							<td><b>'.$questname.'</b></td>
						</tr>';
			
		}
		$tabledata.= '</table>';
		
		$tabledata.='<hr style="width:100%;"></hr><br>';
		if(count($displayquestions)!=0){
		$tabledata.='<h6 style="text-align:right;">Total Questions='.count($displayquestions).'</h6>';
		
		$tabledata.='<table width=100% border=1><tr>
		<th>Question</th>
		<th>Answers</th>
		<th>correct Answer</th>
		<th>Group</th>
		<th>Chapter</th>
		<th>Level</th>
		<th>Status</th>
		</tr>';
		
		  for($i=1,$s=0;$i<=count($displayquestions),$s<count($displayquestions);$s++,$i++)
		  { 		
	  				$tabledata.='<tr>';
     				$tabledata.='<td align="left">
     					'.'Q'.$i.'.'.'&nbsp;'.$displayquestions[$s]['idquestions'].')'.''.trim($displayquestions[$s]['Question']).'
     				</td><td align="left">';
	  				
     					$larranswers = $this->lobjExammarksModel->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
     					
		            	for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++)
		            	{
		            		 $tabledata.="A$i.".$larranswers[$a]['idanswers'].")";
		            		 $tabledata.="".$larranswers[$a]['answers']."<br>";
		            		  if($larranswers[$a]['CorrectAnswer']==1){
		            		  	$correct=$larranswers[$a]['answers'];	
		            		  }
		            	}
		    $tabledata.='</td>';
		    $tabledata.="<td align='left'>".$correct."</td>";
			$tabledata.="<td align='left'>".$displayquestions[$s]['QuestionGroup']."</td>";
			$tabledata.="<td align='left'>".$displayquestions[$s]['QuestionNumber']."</td>";
			if($displayquestions[$s]['QuestionLevel']==1){
				$tabledata.="<td align='left'>Easy</td>";
			}else if($displayquestions[$s]['QuestionLevel']==2){
				$tabledata.="<td align='left'>Medium</td>";
			}else if($displayquestions[$s]['QuestionLevel']==3){
				$tabledata.="<td align='left'>Difficult</td>";
			}
		  	if($displayquestions[$s]['Active']==1){
				$tabledata.="<td align='left'>Active</td>";
			}else if($displayquestions[$s]['Active']==0){
				$tabledata.="<td align='left'>InActive</td>";
			}else if($displayquestions[$s]['Active']==2){
				$tabledata.="<td align='left'>Review</td>";
			}
			
			
			         	
			$tabledata.='</tr>';
		}
		$tabledata.='</table><br>';
		}else{
			$tabledata.='<table>
				<tr class="oddrow">
     				<td align="right">Results not Found for the Your Search Criteria.</td>
	  			</tr>
	  		</table><br>';
		}
	
		$mpdf->WriteHTML($tabledata);   
		$mpdf->Output('Questions_Report.pdf','D');
		return 1;
    }
    
/*public function fnexportnewAction()
    {   
     
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$fromqtn = $this->_getParam('fromqtn');
		$toqtn = $this->_getParam('toqtn');
		$questiongroup = $this->_getParam('questiongroup');
		$questionstatus = $this->_getParam('questionstatus');
		$questionslevel = $this->_getParam('QuestionLevel');
		$questionschapter = $this->_getParam('QuestionChapter');
		$namespace = new Zend_Session_Namespace('exampdf');
		$Questionname=$namespace->data; 
		//echo $Questionname;die();
		$displayquestions = $this->lobjExammarksModel->fndisplayquestionsforabovecode($questiongroup,$questionstatus,$fromqtn,$toqtn,$questionslevel,$questionschapter,$Questionname);
		if(!empty($questiongroup) && isset($questiongroup)){
			$qsngroupnamearray=$this->lobjExammarksModel->fngetgroupname($questiongroup);
			$qsngroupname=$qsngroupnamearray['groupname'];
		}else{
			$qsngroupname="All";	
		}
		if(isset($questionstatus) && $questionstatus!='3'){
			if($questionstatus==1){
					$qsnstatus="Active";
			}else if($questionstatus==0){
					$qsnstatus="InActive";
			}else if($questionstatus==2){
				$qsnstatus="Review";
			}
		}else{
			$qsnstatus="All";
		}
		if(isset($fromqtn) && !empty($fromqtn)){
			$fromqsn=$fromqtn;	
		}else{
			$fromqsn="1";
		}
    	if(isset($toqtn) && !empty($toqtn)){
			$toqsn=$toqtn;	
		}
		
		if(isset($questionslevel) && !empty($questionslevel)){
			if($questionslevel=='1'){
				$questionslevelrep="Easy";
			}else if($questionslevel=='2'){
				$questionslevelrep="Medium";
			}else if($questionslevel=='3'){
				$questionslevelrep="Difficult";
			}
		}else{
			$questionslevelrep="All";
		}
		
		if(isset($questionschapter) && !empty($questionschapter)){
			$questionschapterrep=$questionschapter;
		}else{
			$questionschapterrep="All";
		}
		
    	if(isset($Questionname) && !empty($Questionname)){
			$questname=$Questionname;
		}else{
			$questname="";
		}
    	
		
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$stylesheet = file_get_contents(APPLICATION_PATH.'/../library/MPDF53/mpdfstyletables.css');
		$mpdf->WriteHTML($stylesheet,1);
		
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Exam" ).' '.$this->view->translate( "Questions" );
		ini_set('max_execution_time',3600);
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		//echo $imgp;die();
		$tabledata = '<img width=100% src= "'.$imgp.'" />';
		
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table width=100% border=0>
						<tr><td><b>Questions Group &nbsp;:</b></td>
							<td><b>'.$qsngroupname.'</b></td>
							<td><b>Questions Status &nbsp;:</b></td>
							<td><b>'.$qsnstatus.'</b></td>
						</tr>
						<tr><td><b>Questions Level &nbsp;:</b></td>
							<td><b>'.$questionslevelrep.'</b></td>
							<td><b>Questions Chapter &nbsp;:</b></td>
							<td><b>'.$questionschapterrep.'</b></td>
						</tr>
						<tr><td><b>Questions From &nbsp;:</b></td>
							<td><b>'.$fromqsn.'</b></td>
							<td><b>Questions To &nbsp;:</b></td>
							<td><b>'.$toqsn.'</b></td>
						</tr>';
		if(!empty($questname)){
			$tabledata.= '<tr><td><b>Question Name&nbsp;:</b></td>
							<td><b>'.$questname.'</b></td>
						</tr>';
			
		}
		$tabledata.= '</table>';
		
		$tabledata.='<hr style="width:100%;"></hr><br>';
		if(count($displayquestions)!=0){
		$tabledata.='<b style="font-size:10;">
        	<span style=" display:inline-block;">
            	<b style="width:100%;background-color:green;">Correct Answer</b>
        	</span >
       	 	<span style="width:100%; display:inline-block;text-align:right;">
            	<h6 style="text-align:right;">Total Questions='.count($displayquestions).'</h6>
        	</span>
    	</b>';
		
		$tabledata.='<table class="bpmTopicC" width=100%><tr class="headerrow">
		<th>Question</th>
		<th>Answers</th>
		<th>Group</th>
		<th>Chapter</th>
		<th>Level</th>
		<th>Status</th>
		</tr>';
		
		  for($i=1,$s=0;$i<=count($displayquestions),$s<count($displayquestions);$s++,$i++)
		  { 		if($i%2==0){
		  				$tabledata.='<tr class="evenrow">';
		  			}else{
		  				$tabledata.='<tr class="oddrow">';
		  			}
	  				//$tabledata.='<tr class="oddrow">
     				$tabledata.='<td align="left">
     					'.'Q'.$i.'.'.'&nbsp;'.$displayquestions[$s]['idquestions'].')'.''.trim($displayquestions[$s]['Question']).'
     				</td><td align="left">';
	  				
     					$larranswers = $this->lobjExammarksModel->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
     					
		            	for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++)
		            	{
		            		 $tabledata.="A$i.".$larranswers[$a]['idanswers'].")";
		            		  if($larranswers[$a]['CorrectAnswer']==1){
		            		  	$tabledata.="<b style='background-color:green;'>".$larranswers[$a]['answers']."</b><br>";	
		            		  }else{
		            		  	$tabledata.="".$larranswers[$a]['answers']."<br>";
		            		  }
		            		 
		            		 
		            		
		            	}
		    $tabledata.='</td>';
			$tabledata.="<td align='left'>".$displayquestions[$s]['QuestionGroup']."</td>";
			$tabledata.="<td align='left'>".$displayquestions[$s]['QuestionNumber']."</td>";
			if($displayquestions[$s]['QuestionLevel']==1){
				$tabledata.="<td align='left'>Easy</td>";
			}else if($displayquestions[$s]['QuestionLevel']==2){
				$tabledata.="<td align='left'>Medium</td>";
			}else if($displayquestions[$s]['QuestionLevel']==3){
				$tabledata.="<td align='left'>Difficult</td>";
			}
		  	if($displayquestions[$s]['Active']==1){
				$tabledata.="<td align='left'>Active</td>";
			}else if($displayquestions[$s]['Active']==0){
				$tabledata.="<td align='left'>InActive</td>";
			}else if($displayquestions[$s]['Active']==2){
				$tabledata.="<td align='left'>Review</td>";
			}
			
			
			         	
			$tabledata.='</tr>';
		}
		$tabledata.='</table><br>';
		}else{
			$tabledata.='<table>
				<tr class="oddrow">
     				<td align="right">Results not Found for the Your Search Criteria.</td>
	  			</tr>
	  		</table><br>';
		}
	
		$mpdf->WriteHTML($tabledata);   
		$mpdf->Output('Questions_Report.pdf','D');
		return 1;
    }*/
}
