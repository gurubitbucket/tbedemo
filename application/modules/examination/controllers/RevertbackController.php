<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_RevertbackController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj();
	}
    
	private function fnsetObj()
	{	
		$this->lobjRevertbackmodel = new Examination_Model_Revertback(); //intialize newscreen db object
		$this->lobjRevertbackForm = new Examination_Form_Revertback(); 
       // $lobjcommonmodel = GeneralSetup_Model_DbTable_Common();
	}
	
	public function indexAction() 
	{
	   $this->view->lobjRefertbackForm = $this->lobjRevertbackForm;
	   
	    if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
		
			$this->view->lobjRefertbackForm->Date->setValue($larrformData['Date']);
			$larrvenuedetails = $this->lobjRevertbackmodel->fngetallvenues($larrformData['Date']);
			$this->lobjRevertbackForm->Venue->addMultiOptions($larrvenuedetails);
			$this->view->lobjRefertbackForm->Venue->setValue($larrformData['Venue']);
			
			$larrvenuedetails = $this->lobjRevertbackmodel->fngetallsessions($larrformData['Date'],$larrformData['Venue']);
			$this->lobjRevertbackForm->session->addMultiOptions($larrvenuedetails);
			$this->view->lobjRefertbackForm->session->setValue($larrformData['session']);
			$larrresults = $this->lobjRevertbackmodel->fngetstudents($larrformData);
			$this->view->studentnames = $larrresults;
	    }
	    
	    if ($this->_request->isPost () && $this->_request->getPost ( 'Approve' )) {
			$larrformData = $this->_request->getPost ();

			$larrupdatedsink = $this->lobjRevertbackmodel->fnupdatetherevertstatus($larrformData);
		
			
	    }
	   
	} 

	
	//ajax functin for getting the venues details for that date
	public function getsessionsAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$iddate = $this->_getParam('iddate');
		
		$larrvenuedetails = $this->lobjRevertbackmodel->fngetallvenues($iddate);
	    	$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuedetails);
		
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
		
	}
	
	//ajax functin for getting the venues details for that date
	public function selectsessionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$selecteddate = $this->_getParam('selecteddate');
		$selectedvenue = $this->_getParam('selectedvenue');
				
		$larrvenuedetails = $this->lobjRevertbackmodel->fngetallsessions($selecteddate,$selectedvenue);
	    	$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuedetails);
		
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
		
	}
}