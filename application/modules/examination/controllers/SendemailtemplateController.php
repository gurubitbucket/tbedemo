<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_SendemailtemplateController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	public function fnsetObj()
	{	
		$this->lobjSendemailtemplatenmodel = new Examination_Model_Sendemailtemplate(); //intialize newscreen db object
		$this->lobjSendemailtemplatenForm = new GeneralSetup_Form_Sendemailtemplate();
	}
	
	public function indexAction() 
	{
		
		$this->view->lobjform = $this->lobjSendemailtemplatenForm;
	    $larretemailtemplatesresult= $this->lobjSendemailtemplatenmodel->fngetemailtemplates(); 
		$this->lobjSendemailtemplatenForm->Emailtemplate->addMultiOptions($larretemailtemplatesresult);
		if ($this->_request->isPost () && $this->_request->getPost( 'Send' )) 
		{
			$larrformData = $this->_request->getPost(); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Send'] );
			$idTemplate=$larrformData['Emailtemplate'];
		    require_once('Zend/Mail.php');
		    require_once('Zend/Mail/Transport/Smtp.php');
		    $lobjDbAdpt = 	Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where('a.idTemplate= ?',$idTemplate);
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		$lstrEmailTemplateFrom 	  =  $larrResult['TemplateFrom'];
       		$lstrEmailTemplateFromDesc=  $larrResult['TemplateFromDesc'];
       		$lstrEmailTemplateSubject =  $larrResult['TemplateSubject'];
       		$lstrEmailTemplateBody    =  $larrResult['TemplateBody'];
       		$lstrEmailTemplateFooter  =  $larrResult['TemplateFooter'];
			$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
       		$auth = 'ssl';
       		$port = '465';
       		$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
       		try
       		{
       			$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
       		}
       		catch(Exception $e)
       		{
       			echo '<script language="javascript">alert("Unabsadfasfsa")</script>';
       		}
       		$mail = new Zend_Mail();
       		try
       		{
       			$mail->setBodyHtml($lstrEmailTemplateBody);
       		}
       		catch(Exception $e)
       		{
       			echo '<script language="javascript">alert("body is not seted")</script>';
       		}
       		$sender_email = 'ibfiminfo@gmail.com';
       		$sender = 'ibfim';
       		$receiver_email =$larrformData['email'];
       		$mail->setFrom($sender_email, $sender)
       		->addTo($receiver_email)
       		->setSubject($lstrEmailTemplateSubject);
       		try
       		{
       			$result = $mail->send($transport);
       		}
       		catch (Exception $e)
       		{
       			echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
       		}
		}
	}
}