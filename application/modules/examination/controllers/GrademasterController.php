<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_GrademasterController extends Base_Base 
{
	private $lobjGroup; //db variable
	private $lobjGroupform;//Form variable
	//private $_gobjlogger;
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
   	 //   $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object 
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjGroup = new Examination_Model_Grademodelmaster(); //intialize user db object
		$this->lobjGroupform = new Examination_Form_Grademaster(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction() 
	{    	 
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view
		$larrresult = $this->lobjGroup->fngetgradesetlist(); //get businesstype details
			
		//print_r($larrresult);die();
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->Grademasterpaginatorresult); // clear the search session
		
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->Grademasterpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Grademasterpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		/*
		
	$larrgetallgrade=$this->lobjGroup->fngetallgarde();
			//$flag=1;
			$redarry=array();
			for($gra=0;$gra<count($larrgetallgrade);$gra++)
			{
				$idgrade=$larrgetallgrade[$gra]['idgrademaster'];
				$gradename=$larrgetallgrade[$gra]['gradename'];
				if($gradename=='F')
				{
					$larrstartresult=$this->lobjGroup->fncheckstartgarde($gradename);
					if(empty($larrstartresult['gradename']))
					{
						$flag=2;
						$redarry[$idgrade]=2;
					}
				}
				
				if($gradename=='A')
				{
					$larrstartresult=$this->lobjGroup->fncheckendgarde($gradename);
					if(empty($larrstartresult['gradename']))
					{
						$flag=2;
						$redarry[$idgrade]=2;
					}
				}
				
				
				if($gradename!='F')
				{
					$diff=$gra-1;
					$gradeto=$larrgetallgrade[$diff]['percentageto'];
					$gradefrm=$larrgetallgrade[$gra]['percentagefrom'];
					//echo $gradefrm;die();
					$totdiff=$gradefrm-$gradeto;
					if($totdiff!=1)
					{
						$flag=2;
					$redarry[$idgrade]=2;
					}
					
				}
				//$larrcheckgrade=$this->lobjGroup->fncheckgarderange($larrformData['gradename']);
				
				
				
			}
			//print_r($redarry);
			$this->view->redgardes=$redarry;
			
		*/
		// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		{
			    $larrformData = $this->_request->getPost ();			    									   
				unset ( $larrformData ['Search'] );
				$larrresult = $this->lobjGroup->fnSearchgradeset($larrformData); //searching the values for the businesstype
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Grademasterpaginatorresult = $larrresult;
			
		}
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/examination/grademaster/index');			
		}
		
	}
	
	//function to add a businesstype
	public function addAction() 
	{   
		$this->view->lobjGroupform = $this->lobjGroupform; //send the form to the view		
		
		

		//$this->lobjGroup->fncheckgarderange('A');
		for($linti=0;$linti<=100;$linti++){
			$larrrand[$linti] = $linti;
		}
		$larrgradeslist=$this->lobjGroup->fngetgrdaeslist();
		
		$this->view->lobjGroupform->gradename->addMultiOptions($larrgradeslist);
		$this->view->lobjGroupform->Marksfrom->addMultiOptions($larrrand);
		$this->view->lobjGroupform->Marksto->addMultiOptions($larrrand);
		// check if the user has clicked the save
		if ($this->_request->isPost() && $this->_request->getPost('Save'))
		{
			$larrformData = $this->_request->getPost();	
			$auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
				
			$larrformData['Upduser']=$iduser;
			
			$larrformData['Upddate']=date('Y-m-d H:i:s');
	//	echo "<pre/>";
		//	print_r($larrformData);
			//die();
			unset ( $larrformData ['Save'] );
			
			$larrcheckgrade=$this->lobjGroup->fncheckgrade($larrformData['gradename']);
			if(!empty($larrcheckgrade['gradename']))
			{
				$larrinactive=$this->lobjGroup->fninactiveprasentgarde($larrformData['gradename']);
			}
			
			$lastid=$this->lobjGroup->fnaddgradeset($larrformData);
			
			$larrgetallgrade=$this->lobjGroup->fngetallgardeset($lastid);
			$flag=1;
			for($gra=0;$gra<count($larrgetallgrade);$gra++)
			{
				$gradename=$larrgetallgrade[$gra]['Gradename'];
				if($gradename=='F')
				{
					$larrstartresult=$this->lobjGroup->fncheckstartgardeset($gradename,$lastid);
					if(empty($larrstartresult['Gradename']))
					{
						$flag=2;
						break;
					}
				}
				
				
				$larrstartresult=$this->lobjGroup->fncheckendgardeset($lastid);
					if(empty($larrstartresult['Gradename']))
					{
						$flag=2;
						break;
					}
					
			/*	if($gradename=='A')
				{
					$larrstartresult=$this->lobjGroup->fncheckendgardeset($gradename,$lastid);
					if(empty($larrstartresult['Gradename']))
					{
						$flag=2;
						break;
					}
				}*/
				
				
				if($gradename!='F')
				{
					$diff=$gra-1;
					$gradeto=$larrgetallgrade[$diff]['Percentageto'];
					$gradefrm=$larrgetallgrade[$gra]['Percentagefrom'];
					$totdiff=$gradefrm-$gradeto;
					echo $totdiff."<br/>";
					if($totdiff!=1)
					{
						$flag=2;
						break;
					}
					
				}
				//$larrcheckgrade=$this->lobjGroup->fncheckgarderange($larrformData['gradename']);
				
				
				
			}
		//	die();
			if($flag==2)
			{
				//$flag=2;
					$larrupdateresult=$this->lobjGroup->fnupdatemismatchgardeset($lastid,$flag);
			echo '<script language="javascript">alert("Mismatch is there,please correct it")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/grademaster/index';</script>";
 		die();	
			}
			else {
				//$flag=1;
					$larrupdateresult=$this->lobjGroup->fnupdatemismatchgardeset($lastid,$flag);
			$this->_redirect( $this->baseUrl . '/examination/grademaster/index'); //after adding Redirect to index page			
			}

				//$this->_redirect( $this->baseUrl . '/examination/grademaster/index'); //after adding Redirect to index page			
        }     
    }
    
    //function to update the businesstype
	public function editAction()
	{   	
		$this->view->lobjGroupform = $this->lobjGroupform; //send the Form the view
		$id=$this->_Getparam("id"); // get the id of the group to be updated
		$larrresult = $this->lobjGroup->fneditgradeset($id);	// get the details from the DB into array variable	
		
		$larrgrdaesetdetails=$this->lobjGroup->fneditgradesetdetails($id);
			
		for($linti=0;$linti<=100;$linti++){
			$larrrand[$linti] = $linti;
		}
		
	$larrgradeslist=$this->lobjGroup->fngetgrdaeslist();
		
		$this->view->lobjGroupform->gradename->addMultiOptions($larrgradeslist);
		$this->view->lobjGroupform->Marksfrom->addMultiOptions($larrrand);
		$this->view->lobjGroupform->Marksto->addMultiOptions($larrrand);
		$this->view->gradesetdetails=$larrgrdaesetdetails;
		//$this->view->lobjGroupform->populate($larrresult); // populate the result in the edit form
			  	$this->view->lobjGroupform->gradesetname->setValue($larrresult['Gradesetname']);
			  	$this->view->lobjGroupform->Effectidate->setValue($larrresult['Effectivedate']);
			  	$this->view->lobjGroupform->Active->setValue($larrresult['Active']);
		
			  /*	$this->view->lobjGroupform->Marksfrom->setValue($larrresult['percentagefrom']);
		$this->view->lobjGroupform->Marksto->setValue($larrresult['percentageto']);
		
			$this->view->lobjGroupform->gradedescription->setValue($larrresult['Gradedescription']);
				$this->view->lobjGroupform->Active->setValue($larrresult['Active']);
		
				
				 	$this->view->lobjGroupform->gradename	->setAttrib('readonly','true');
			  	$this->view->lobjGroupform->Marksfrom	->setAttrib('readonly','true');
		$this->view->lobjGroupform->Marksto	->setAttrib('readonly','true');
		$this->view->lobjGroupform->Effectidate	->setAttrib('readonly','true');
			 */
		
	    // check if the user has clicked the save
    	if ($this->_request->isPost() && $this->_request->getPost('Save'))
    	 {       	 			 	
    		$larrformData = $this->_request->getPost();	 // get the form data  		    		    	
	   		$lintId = $id;	//store the id to be updated    		
	   		unset ($larrformData ['Save']);	 //unset the save data in form data  
    	//echo $lintId;
    	$auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
				
			$larrformData['Upduser']=$iduser;
			
			$larrformData['Upddate']=date('Y-m-d H:i:s');
			
			//print_r($larrformData);die();
			
				$this->lobjGroup->fnupdategradeset($lintId,$larrformData);
		//$lintId=$lintId;
    	 $larrgetallgrade=$this->lobjGroup->fngetallgardeset($lintId);
			$flag=1;
			for($gra=0;$gra<count($larrgetallgrade);$gra++)
			{
				$gradename=$larrgetallgrade[$gra]['Gradename'];
				if($gradename=='F')
				{
					$larrstartresult=$this->lobjGroup->fncheckstartgardeset($gradename,$lintId);
					if(empty($larrstartresult['Gradename']))
					{
						$flag=2;
						break;
					}
				}
				
				
				$larrstartresult=$this->lobjGroup->fncheckendgardeset($lintId);
					if(empty($larrstartresult['Gradename']))
					{
						$flag=2;
						break;
					}
					
			/*	if($gradename=='A')
				{
					$larrstartresult=$this->lobjGroup->fncheckendgardeset($gradename,$lastid);
					if(empty($larrstartresult['Gradename']))
					{
						$flag=2;
						break;
					}
				}*/
				
				
				if($gradename!='F')
				{
					$diff=$gra-1;
					$gradeto=$larrgetallgrade[$diff]['Percentageto'];
					$gradefrm=$larrgetallgrade[$gra]['Percentagefrom'];
					$totdiff=$gradefrm-$gradeto;
					echo $totdiff."<br/>";
					if($totdiff!=1)
					{
						$flag=2;
						break;
					}
					
				}
				//$larrcheckgrade=$this->lobjGroup->fncheckgarderange($larrformData['gradename']);
				
				
				
			}
		//	die();
			if($flag==2)
			{
				//$flag=2;
					$larrupdateresult=$this->lobjGroup->fnupdatemismatchgardeset($lintId,$flag);
			echo '<script language="javascript">alert("Mismatch is there,please correct it")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/grademaster/index';</script>";
 		die();	
			}
			else {
				//$flag=1;
					$larrupdateresult=$this->lobjGroup->fnupdatemismatchgardeset($lintId,$flag);
			$this->_redirect( $this->baseUrl . '/examination/grademaster/index'); //after adding Redirect to index page			
			}
			//$this->_redirect( $this->baseUrl . '/examination/grademaster/index'); //redirect to index page after updating		
	   		
							
    	}
	  }
	  
public function newvalidategradeAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$gradname = $this->_getParam('gradname');
		$gradval = $this->_getParam('gradval');
		//$gradvals=
	//echo $gradval;die();
	
		
		
		$larrscheduleractivecenter=$this->lobjGroup->fnGetvalidategarde($gradname,$gradval);
	   	
	   	if($larrscheduleractivecenter)
		{
		echo 1;die();
		}
		else 
		{
		echo 2;die();
		}
		
	}
	
}