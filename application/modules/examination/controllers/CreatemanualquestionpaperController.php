<?php
class Examination_CreatemanualquestionpaperController extends Base_Base
{
	private $lobjGroup; //db variable
	private $lobjGroupform;//Form variable
	public $idTOSMaster;
	public function init()
	{
		
		//echo date("ymd");
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
		$this->fnsetObj(); //call fnsetObj
	}

	//Function to set the objects
	public function fnsetObj()
	{
		$this->lobjGroup = new Examination_Model_Createmanualquestionpaper(); //intialize user db object
		$this->lobjGroupform = new Examination_Form_Createmanualquestionpaper(); //intialize user lobjbusinesstypeForm
		//$this->lobjGroup = new Examination_Model_Examsetmodel();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}

	//function to set and display the result
	public function indexAction()
	{
		//echo "session_id(): ".session_id()."<br>
		//COOKIE: ".$_COOKIE["PHPSESSID"];
		$this->view->lobjform = $this->lobjGroupform; //send the lobjForm object to the view
		$this->lobjGroupform->field28->setAttrib('onblur','fnsetname();');
		$larrbatchresult = $this->lobjGroup->fnGetProgramName();
		$this->lobjGroupform->field1->addMultiOptions($larrbatchresult);
		//$larrresult = $this->lobjGroup->fngetquestionsetlist(); //get businesstype details
		$larrresult=array();
		if(!$this->_getParam('search') && !$this->_getParam('page'))
		unset($this->gobjsessionsis->Businesstypepaginatorresult); // clear the search session
		//$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpagecount =15;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->Businesstypepaginatorresult)){
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Businesstypepaginatorresult,$lintpage,$lintpagecount);
		}else{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}

		// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			$larrresult = $this->lobjGroup->fngetquestionsetlistsearch($larrformData);
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->Businesstypepaginatorresult = $larrresult;
		}
		/*
		if ($this->_request->isPost () && $this->_request->getPost ( 'submit' )) {
			
			$larrformData = $this->_request->getPost ();
			$idprogram=$larrformData['field1'];
			$setname=$larrformData['field28'];
			$_SESSION['views']=$setname;
			//$this->view->idprogram=$idprogram;
			$idTOSMasterfind=$this->lobjGroup->fnGetidtosonidprogram($idprogram);
			$idTOSMaster=$idTOSMasterfind['IdTOS'];
			$_SESSION['idtosmaster']=$idTOSMaster;
			$_SESSION['idprogram']=$idprogram;
			
			$larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($idprogram);
			$idIdBatch = $larractiveidbatch['IdBatch'];
			$iduser =$auth->getIdentity()->iduser;
			$ldtsystemDate = date('Y-m-d H:i:s');
			$larrresult = $this->lobjGroup->fninserquestioncodes($larrformData,$idIdBatch,$iduser,$ldtsystemDate); //searching the values for the businesstype
			//print_r($larrformData);die();
			//$this->_redirect( $this->baseUrl . '/examination/createmanualquestionpaper/view');
			
		}*/
		
		if ($this->_request->isPost () && $this->_request->getPost('genmanual')){
			$larrformData1 = $this->_request->getPost ();
			$program=$_SESSION['idprogram'];
			$larrformData['field28']=$larrformData1['setname'];
			$larrformData['field1']=$larrformData1['idprogram'];
			
			$lobjExamdetailsmodel = new App_Model_Examdetails();
			$larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
			
			$idIdBatch = $larractiveidbatch['IdBatch'];
			$this->view->idbatch = $idIdBatch;
			$larrresultaa = $lobjExamdetailsmodel->fnGetBatchName($idIdBatch);
			
			$this->view->noofqtns = $larrresultaa[0]['NosOfQues'];
			//echo $idIdBatch;die();
			$larrmultisections = $lobjExamdetailsmodel->fnGetSectionsss($idIdBatch);
			//echo "<pre/>";print_r($larrmultisections);
			$larridqtnfrombatch = $lobjExamdetailsmodel->fnGetQtnfromidbatch($idIdBatch);
			
			for ($linta=0;$linta<count($larridqtnfrombatch);$linta++)
			{
				$larrsection[$linta]=$larrmultisections[$linta]['IdSection'];
				$larrqtnfrombatch[$larrsection[$linta]]=$larridqtnfrombatch[$linta]['NosOfQuestion'];
			}
			
			$this->view->sectionqtns = $larrqtnfrombatch;
			for ($sec=0;$sec<count($larrmultisections);$sec++)
			{
				$larrsection[$sec]=$larrmultisections[$sec]['IdSection'];
			}
			for($s=0;$s<count($larrsection);$s++)
			{
				$value = $larrsection[$s];
				if($s==0)
				{
					$values=$value;
				}
				else
				$values.=','.$value;
					
			}
			$g=0;$mergearry=array();
			for($i=0;$i<count($larrsection);$i++){
				$larrresulteasy = $lobjExamdetailsmodel->fnGetNoofQuestion($idIdBatch,$larrsection[$i]);
				if($larrresulteasy[0]['NoofQuestions']!=0)
				{
					$larrdef[$g]=$larrresulteasy[0]['IdDiffcultLevel'];
					$g++;
				}
				if($larrresulteasy[1]['NoofQuestions']!=0)
				{
					$larrdef[$g]=$larrresulteasy[1]['IdDiffcultLevel'];
					$g++;
				}
				if($larrresulteasy[2]['NoofQuestions']!=0)
				{
					$larrdef[$g]=$larrresulteasy[2]['IdDiffcultLevel'];
					$g++;
				}
				for($k=0;$k<count($larrresulteasy);$k++)
				{
					$questionArr  = $larrresulteasy[$k]['IdDiffcultLevel'];
					$manuldbquestion=$this->lobjGroup->fngetmanualquestioncodes($larrsection[$i],$larrformData1,$questionArr);
					//echo "<pre/>";print_r($manuldbquestion);
					
					if(!empty($manuldbquestion)){
						$manualquestionarray=explode(',',$manuldbquestion['idquestions']);
						$mergearry=array_merge($mergearry,$manualquestionarray);
					}
				}
			}
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			//////////////Delete from temptables/////////////
			$lobjDbAdpt->delete('tbl_manualquestiongroupdetails',array(
    			'idquestiongroup = ?' => $_SESSION['lastinsertid']
			));
			$lobjDbAdpt->delete('tbl_manualquestiongroup',array(
    			'idquestiongroup = ?' => $_SESSION['lastinsertid']
			));
			
		$auth = Zend_Auth::getInstance();
		$iduser =$auth->getIdentity()->iduser;
		$ldtsystemDate = date('Y-m-d H:i:s');
		 
		 $larrresult = $this->lobjGroup->fninserquestioncodesMain($larrformData,$idIdBatch,$iduser,$ldtsystemDate); //searching the values for the businesstype
		 $insermanualquestion= $this->lobjGroup->fninserquestionsforabovecode($larrresult,$mergearry); 
		 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/createmanualquestionpaper/index';</script>";	
		}
		
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' ))
		{
			$this->_redirect( $this->baseUrl . '/examination/creatmanualequestionpaper/index');
		}
	}
	
	public function newmanualsetAction(){
		$this->view->lobjform = $this->lobjGroupform; //send the lobjForm object to the view
		$this->lobjGroupform->field28->setAttrib('onblur','fnsetname();');
		$larrbatchresult = $this->lobjGroup->fnGetProgramName();
		$this->lobjGroupform->field1->addMultiOptions($larrbatchresult);
		if ($this->_request->isPost () && $this->_request->getPost ( 'submit' )) {
			$larrformData = $this->_request->getPost ();
			$idprogram=$larrformData['field1'];
			$setname=$larrformData['field28'];
			$_SESSION['views']=$setname;
			$lobjExamdetailsmodel = new App_Model_Examdetails();
			$larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($idprogram);
			$idIdBatch = $larractiveidbatch['IdBatch'];
			$auth = Zend_Auth::getInstance();
			$iduser =$auth->getIdentity()->iduser;
			$ldtsystemDate = date('Y-m-d H:i:s');
			$larrresult = $this->lobjGroup->fninserquestioncodes($larrformData,$idIdBatch,$iduser,$ldtsystemDate); 
			unset($_SESSION['lastinsertid']);
			echo $_SESSION['lastinsertid']=$larrresult;
			//$this->view->idprogram=$idprogram;
			$idTOSMasterfind=$this->lobjGroup->fnGetidtosonidprogram($idprogram);
			$idTOSMaster=$idTOSMasterfind['IdTOS'];
			$_SESSION['idtosmaster']=$idTOSMaster;
			$_SESSION['idprogram']=$idprogram;
			$this->_redirect( $this->baseUrl . '/examination/createmanualquestionpaper/view');
			//echo "submit";die();
		}
		
	}
	
	public function newmanualset2Action(){
		$this->view->lobjform = $this->lobjGroupform; //send the lobjForm object to the view
		$this->lobjGroupform->field28->setAttrib('onblur','fnsetname();');
		$larrbatchresult = $this->lobjGroup->fnGetProgramName();
		$this->lobjGroupform->field1->addMultiOptions($larrbatchresult);
		unset($_SESSION['samepage']);
		if ($this->_request->isPost () && $this->_request->getPost ( 'submit' )) {
			$larrformData = $this->_request->getPost ();
			$_SESSION['samepage']=1;
			$idprogram=$larrformData['field1'];
			$setname=$larrformData['field28'];
			$_SESSION['views']=$setname;
			//$this->view->idprogram=$idprogram;
			$idTOSMasterfind=$this->lobjGroup->fnGetidtosonidprogram($idprogram);
			$idTOSMaster=$idTOSMasterfind['IdTOS'];
			$_SESSION['idtosmaster']=$idTOSMaster;
			$_SESSION['idprogram']=$idprogram;
			
			
			
			$this->view->lobjGroupform = $this->lobjGroupform; //send the Form the view
		//$idTOSMaster=$this->_Getparam("id"); // get the id of the group to be updated
		//$idTOSMaster=22;
		$idTOSMaster=$_SESSION['idtosmaster'];
		$idprogram=$_SESSION['idprogram'];
		$larrrtosfulldetails=$this->lobjGroup->fnGetTOSFullDetails($idTOSMaster);
		$prgname=$this->lobjGroup->fnGetCourse($idTOSMaster);
		$this->view->prgramname=$prgname['ProgramName'];
		$this->view->idprogram=$idprogram;
		$this->view->setname=$prgname['BatchName'];
		$this->view->setnamepaper=$_SESSION['views'];
		$this->view->tosfull=$larrrtosfulldetails;
		$this->view->idtos=$idTOSMaster;
		
		if(!empty($larrrtosfulldetails[0]['IdBatch']))
		{
		$larrgetotalparts=$this->lobjGroup->fngettotalparts($larrrtosfulldetails[0]['IdBatch']);
		for($idp=0;$idp<count($larrgetotalparts);$idp++){
			if($idp==0){
				$idparts=$larrgetotalparts[$idp]['IdPart'];
				$idparts="'$idparts'";
			}else{
				$idpartss=$larrgetotalparts[$idp]['IdPart'];
				$idpartss="'$idpartss'";
				$idparts=$idparts.','.$idpartss;
			}
			$idpart2=$larrgetotalparts[$idp]['IdPart'];
			$resulttos=$this->lobjGroup->fngetquestionstosdetail($idpart2,$idTOSMaster);
			$ecahpartquestions[$idpart2]=$resulttos['totals'];
			//$larrtotalchapters2[]=$this->lobjGroup->fngetchaptereachpartpool($larrgetotalparts[$idp]['IdPart']);
		}
		//echo $idparts;die();
		$larrchpterlevel=$this->lobjGroup->fngetchapterlevel($idparts);//////
		$larrtotalchapters=$this->lobjGroup->fngetchapterfrompool($idparts);//////
		$larrpartquestions=$this->lobjGroup->fngettotalpartqusetions($idparts);/////
		$larrinactivequestions=$this->lobjGroup->fngettotalinactivequsetions($larrgetotalparts);/////
		$this->view->totalinactivequestions=$larrinactivequestions;
		$larrreviewquestions=$this->lobjGroup->fngettotalreviewqusetions($larrgetotalparts);/////
		$this->view->totalreviewquestions=$larrreviewquestions;
		}
		
		for($lev=0;$lev<count($larrchpterlevel);$lev++)
		{
			$chapter=$larrchpterlevel[$lev]['QuestionNumber'];
			$level=$larrchpterlevel[$lev]['QuestionLevel'];
			$larrlevels[$chapter][$level]=$larrchpterlevel[$lev]['cntlevel'];
		}
		$this->view->chapters=$larrtotalchapters;
		
		$this->view->levelchapters=$larrlevels;
		$this->view->groups=$larrpartquestions;
		$this->view->eachgrp=$ecahpartquestions;
			
			
			
			
			
			
			
			
			//$this->_redirect( $this->baseUrl . '/examination/createmanualquestionpaper/newmanualset2');
			//echo "submit";die();
		}
	}
	
public function pdfexportAction()
	{
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$RegID = $this->_getParam('id');
		$progname = $this->lobjGroup->fngetprgname($RegID);
		$tosdesc = $this->lobjGroup->fngettosdesc($RegID);
		$displayquestions= $this->lobjGroup->fndisplayquestionsforabovecode($RegID);
		$date=date("ymd");
		$filename = 'ManualQuestionPaper_'.$date.'_'.$progname['ProgramName'];
		
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		//$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		//$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('SetcreationDate :'.$progname['creationdate'].'       '.'Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Question" ).' '.$this->view->translate( "Paper" );

		//$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',4000);
		$tabledata = '<p style="text-align:center;"><img width="30%" src="../public/images/image.jpg" /></p>';
		//$tabledata.= "<table border=1  align=center style='width:auto'><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= "<table border=1  align=center><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		//$tabledata.= 'Course :&nbsp;'.$progname['ProgramName'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'TOS:&nbsp;'.$tosdesc['Setcode'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Question Set:&nbsp;'.$progname['PaperCode'].'<br>';
		$tabledata.= '<table border=0 width=100%>
						<tr>
							<td align="left"><b>Course:</b>&nbsp;'.$progname['ProgramName'].'</td>
							<td align="right"><b>TOS Name:</b>&nbsp;'.$tosdesc['Setcode'].'</td>
						</tr>
						<tr>
							<td colspan="2" align="left"><b>Question Paper Code:</b>&nbsp;'.$progname['PaperCode'].'</td>
							
						</tr>
					</table><br>';
		
		$tabledata.='<hr></hr><br></table>';
		?><?php 
		$tabledata.='<table border="0" width="100%">'; 
		$i=1;
		for($s=0;$s<count($displayquestions);$s++){ 
			$tabledata.='<tr>
						 <td valign="top" align="right">'.'Q'.$i.'.</td>
						 <td colspan="2">'.trim($displayquestions[$s]['Question']).'</td>
     					 
     					 </tr>';
			$larranswers = $this->lobjGroup->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
			$d=1;
			for($a=0;$a<count($larranswers);$a++){
				$tabledata.='<tr>
								<td></td>
								<td style="width:12%;" align="right" valign="top">'."&#x25a2; A ".$i.'.'.$d.')'.'</td>
								<td style="width:100%;">'.trim($larranswers[$a]['answers']).'&nbsp;
     							</td></tr>';
				$d++;
			}
			$i++;
			$tabledata.='<tr><td colspan="3"></td></tr>';
		}
		$tabledata.='</table>';
		?><?php
		//echo "<pre/>";print_r($tabledata);die();
		//$stylesheet = file_get_contents(APPLICATION_PATH.'/../library/MPDF53/mpdfstyletables.css');
		//$mpdf->WriteHTML($stylesheet,1); 
		
		
		
		$mpdf->WriteHTML(utf8_encode($tabledata));   
		$mpdf->Output("$filename.pdf",'D');
	}
	
public function draftviewAction()
	{
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$RegID = $this->_getParam('id');
		$indexdraft = $this->_getParam('indexdraft');
		if($indexdraft=='indexdraft'){
			$progname = $this->lobjGroup->fngetprgnamedraftindex($RegID);
			$larridquestions= $this->lobjGroup->fngetallidquestionsindex($RegID);
			$tosdesc = $this->lobjGroup->fngettosdescdraftindex($RegID);
			$idTOSMaster = $tosdesc['IdTOS'];
			$merquesarr=array();
			foreach($larridquestions as $larridquestions1){
				array_push($merquesarr,$larridquestions1['idquestions']);
				
			}
		}else{
			$idTOSMaster = $this->_getParam('idtos');
			//echo "<pre/>";print_r($uniquechapters);die();
			$progname = $this->lobjGroup->fngetprgnamedraft($RegID);
			$tosdesc = $this->lobjGroup->fngettosdescdraft($RegID);
			$larridquestions= $this->lobjGroup->fngetallidquestions($RegID);
			$merquesarr=array();
			foreach($larridquestions as $larridquestions1){
				$manualquestionsexp=explode(',',$larridquestions1['idquestions']);
				$merquesarr=array_merge($merquesarr,$manualquestionsexp);
			}
		   //$arraymstr=implode(',',$merquesarr);
	   
	   
		}
		
		
	   
	   
	   $tosdetails=$this->lobjGroup->fngettosdetailsorder($idTOSMaster);//////
		$tosparts=$this->lobjGroup->fngetparts($idTOSMaster);
		$tmp = array();
		foreach ($tosdetails as $item) {
		    if (!in_array($item['IdSection'], $tmp)) {
		        $uniquechapters[] = $item;
		        $tmp[] = $item['IdSection'];
		    }
		}
		$tmp1 = array();
		$part=$uniquechapters['0']['IdSection'];
	   
	   	$displayquestions= $this->lobjGroup->fndisplayquestions($merquesarr);
		//echo "<pre/>";print_r($displayquestions);die();
		$date=date("ymd");
		$filename = 'Draftview_ManualQuestionPaper_'.$date.'_'.$progname['ProgramName'];
		
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		//$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		//$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('SetcreationDate :'.$progname['creationdate'].'       '.'Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Question" ).' '.$this->view->translate( "Paper Draft View" );

		//$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		//ini_set('max_execution_time',3600);
		$tabledata = '<p style="text-align:center;"><img width="30%" src="../public/images/image.jpg" /></p>';
		$tabledata.= "<table border=1  align=center><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		//$tabledata.= 'Course :&nbsp;'.$progname['ProgramName'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'TOS:&nbsp;'.$tosdesc['Setcode'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Question Set:&nbsp;'.$progname['PaperCode'].'<br>';
		$tabledata.= '<table border=0 width=100%>
						<tr>
							<td align="left"><b>Course:</b>&nbsp;'.$progname['ProgramName'].'</td>
							<td align="right"><b>TOS Name:</b>&nbsp;'.$tosdesc['Setcode'].'</td>
						</tr>
						<tr>
							<td colspan="2" align="left"><b>Question Paper Code:</b>&nbsp;'.$progname['PaperCode'].'</td>
							
						</tr>
					</table>';
		
		$tabledata.='</table>';
		$tabledata.='<table border=0  width=100%>
		<tr><td><hr style="width:100%;"></hr></td></tr></table>';
		
		$tabledata.='<h4 style="text-align:center;"><b>TOS DETAILS</b></h4>';
		foreach($tosparts as $tosparts1){
			$tabledata.='<p><b>Part '.$tosparts1['IdPart'].'    ('.$tosparts1['NoOfQuestions'].')'.'</b></p>';
		}
		
		$tabledata.='<table border=1  width=100%>
		<tr>
			<th width="80px;" style="background-color:#909090 ;color:white;">Chapter</th>
			<th width="80px;" style="background-color:#909090 ;color:white;">Total</th>
			<th width="50px;" style="background-color:#909090 ;color:white;">Easy</th>
			<th width="50px;" style="background-color:#909090 ;color:white;">Medium</th>
			<th width="50px;" style="background-color:#909090 ;color:white;">Difficult</th>
		</tr>';
		
		$chaptotal=0;
		$easytotal=0;
		$medtotal=0;
		$difftotal=0;
		$p=0;
		$firstpartchaptotal=0;
		$firstparteasytotal=0;
		$firstpartmedtotal=0;
		$firstpartdifftotal=0;
		$partsubprev=substr(trim($uniquechapters[$p]['IdSection']),0,1);
		foreach($uniquechapters as $uniquechapters1){
			$partsubnow=substr(trim($uniquechapters1['IdSection']),0,1);
			if($partsubprev!=$partsubnow){
				$tabledata.='<tr>
					<td></td>
					<td style="background-color:#B0B0B0  ;color:white;">Total Part '.$partsubprev.' Questions=<b>'.$chaptotal.'</b></td>
					<td style="background-color:#B0B0B0  ;color:white;">Total Easy=<b>'.$easytotal.'</b></td>
					<td style="background-color:#B0B0B0  ;color:white;">Total Medium=<b>'.$medtotal.'</b></td>
					<td style="background-color:#B0B0B0  ;color:white;">Total Difficult=<b>'.$difftotal.'</b></td>
				</tr>';
				$firstpartchaptotal=$chaptotal;
				$firstparteasytotal=$easytotal;
				$firstpartmedtotal=$medtotal;
				$firstpartdifftotal=$difftotal;
				$partsubprev=substr(trim($uniquechapters1['IdSection']),0,1);
			}
			$tabledata.='<tr></td>';
			$tabledata.='<td>'.$uniquechapters1['IdSection'].'</td>';
			$tabledata.='<td>'.$uniquechapters1['chaptotal'].'</td>';
			$chaptotal=$chaptotal+$uniquechapters1['chaptotal'];
			foreach($tosdetails as $tosdetails1){
				if($uniquechapters1['IdSection']==$tosdetails1['IdSection']){
					if($tosdetails1['IdDiffcultLevel']==1){
						$tabledata.='<td>'.$tosdetails1['leveltotal'].'</td>';
						$easytotal=$easytotal+$tosdetails1['leveltotal'];
					}else if($tosdetails1['IdDiffcultLevel']==2){
						$tabledata.='<td>'.$tosdetails1['leveltotal'].'</td>';
						$medtotal=$medtotal+$tosdetails1['leveltotal'];
					}else if($tosdetails1['IdDiffcultLevel']==3){
						$tabledata.='<td>'.$tosdetails1['leveltotal'].'</td>';
						$difftotal=$difftotal+$tosdetails1['leveltotal'];
					}
					
				}
					
			}
			$tabledata.='</tr>';
			
			
			
			
		}
		$tabledata.='<tr>
					<td></td>
					<td style="background-color:#B0B0B0   ;color:white;">Total Part '.$partsubnow.' Questions=<b>'.($chaptotal-$firstpartchaptotal).'</b></td>
					<td style="background-color:#B0B0B0  ;color:white;">Total Easy=<b>'.($easytotal-$firstparteasytotal).'</b></td>
					<td style="background-color:#B0B0B0  ;color:white;">Total Medium=<b>'.($medtotal-$firstpartmedtotal).'</b></td>
					<td style="background-color:#B0B0B0  ;color:white;">Total Difficult=<b>'.($difftotal-$firstpartdifftotal).'</b></td>
				</tr>';
		$tabledata.='<tr>
					<td style="background-color:#909090 ;color:white;">Grand Total</td>
					<td style="background-color:#909090 ;color:white;">Total TOS Questions=<b>'.$chaptotal.'</b></td>
					<td style="background-color:#909090 ;color:white;">Total Easy=<b>'.$easytotal.'</b></td>
					<td style="background-color:#909090 ;color:white;">Total Medium=<b>'.$medtotal.'</b></td>
					<td style="background-color:#909090 ;color:white;">Total Difficult=<b>'.$difftotal.'</b></td>
		</tr>';
		$tabledata.='</table>';
		
		$tabledata.='<pagebreak />';
		$tabledata.='<h4 style="text-align:center;"><b>QUESTION PAPER DETAILS</b></h4>';
		$tabledata.='<table border=1  width=100%>
		<tr>
			<th width="70px;" style="background-color:#909090 ;color:white;">Chapter</th>
			<th width="80px;" style="background-color:#909090 ;color:white;">Level</th>
			<th width="50px;" style="background-color:#909090 ;color:white;">ID</th>
			<th align="center" style="background-color:#909090 ;color:white;">Question And Answers</th>
		</tr>';
		
		?>
		<?php  
	for($i=1,$s=0;$i<=count($displayquestions),$s<count($displayquestions);$s++,$i++){ 
			$tabledata.="<tr>
			<td width='70px;'>".$displayquestions[$s]['QuestionNumber']."</td>";
			if($displayquestions[$s]['QuestionLevel']==1){
				$tabledata.="<td width='80px;'>Easy</td>";
			}else if($displayquestions[$s]['QuestionLevel']==2){
				$tabledata.="<td width='80px;'>Medium</td>";
			}else if($displayquestions[$s]['QuestionLevel']==3){
				$tabledata.="<td width='80px;'>Difficult</td>";
			}
			$tabledata.="<td width='50px;'>".$displayquestions[$s]['idquestions']."</td>";
			
			$tabledata.='<td>';
				$tabledata.='<table border=0 width=100%>
							<tr>
								<td valign="top" >'.'Q'.$i.'.'.'</td>
     							<td align="left">'.trim($displayquestions[$s]['Question']).'</td>
	  						</tr>
	  					</table>';
			$larranswers = $this->lobjGroup->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
			for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++){
				$tabledata.='<table border=0 width=100%>
								<tr>	
									<td valign="top" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'&#x25a2; A'.$i.'.'.$d.')'.'</td>
									<td align="left">'.trim($larranswers[$a]['answers']).'&nbsp;</td>
	  							</tr>
	  						</table>';
			}
			
			$tabledata.='</td></tr>';
		}
		$tabledata.='</table>';
		?><?php
		//echo "<pre/>";print_r($tabledata);die();
		$stylesheet = file_get_contents(APPLICATION_PATH.'/../library/MPDF53/mpdfstyletables.css');
		$mpdf->WriteHTML($stylesheet,1); 
		
		
		
		$mpdf->WriteHTML(utf8_encode($tabledata));   
		$mpdf->Output("$filename.pdf",'D');
	
}

	/*public function pdfexportAction()
	{
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$RegID = $this->_getParam('id');
		$progname = $this->lobjGroup->fngetprgname($RegID);
		$tosdesc = $this->lobjGroup->fngettosdesc($RegID);
		$displayquestions= $this->lobjGroup->fndisplayquestionsforabovecode($RegID);
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('SetcreationDate :'.$progname['creationdate'].'       '.'Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Question" ).' '.$this->view->translate( "Paper" );

		$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',5000);
		
		$tabledata = 'Course :&nbsp;'.$progname['ProgramName'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'TOS:&nbsp;'.$tosdesc['Setcode'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Question Set:&nbsp;'.$progname['PaperCode'].'<br>';
		
		$tabledata.='<hr></hr><br>';
		?>
		<?php  
		for($i=1,$s=0;$i<=count($displayquestions),$s<count($displayquestions);$s++,$i++){ 
		
			
			$tabledata.='<tr>
     						<td width="20">'.'Q'.$i.'</td>
     						<td>.'.mysql_real_escape_string(str_replace("\r\n",'', $displayquestions[$s]['Question'])).'<br></td>
	  					</tr>
	  					<br>';
			$larranswers = $this->lobjGroup->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
			for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++){
				$tabledata.='<tr>
     							<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$i.'.'.$d.')'.' '.mysql_real_escape_string(str_replace("\r\n",'', $larranswers[$a]['answers'])).'
     							<input type="checkbox" />
     							<br></td>
	  						</tr>
	  						<br>';
			}

		}
		?><?php
		//echo "<pre/>";print_r($tabledata);die();
		//$stylesheet = file_get_contents(APPLICATION_PATH.'/../library/MPDF53/mpdfstyletables.css');
		$mpdf->WriteHTML($stylesheet,1); 
		
		
		
		$mpdf->WriteHTML(utf8_encode($tabledata));   
		$mpdf->Output('Questions_Report.pdf','D');
	}*/
	
	//function to add a businesstype
public function addAction() 
	{   
		$this->view->lobjGroupform = $this->lobjGroupform; //send the form to the view			
		// check if the user has clicked the save
		if ($this->_request->isPost() && $this->_request->getPost('Save'))
		{
			$larrformData = $this->_request->getPost();	
			unset ( $larrformData ['Save'] );
			$this->lobjGroup->fnaddgroup($larrformData);
			$this->_redirect( $this->baseUrl . '/examination/group/index'); //after adding Redirect to index page				
        }     
    }
    
    //function to update the businesstype
	public function editAction()
	{   	
		$this->view->lobjGroupform = $this->lobjGroupform; //send the Form the view
		$id=$this->_Getparam("id"); // get the id of the group to be updated
		$larrresult = $this->lobjGroup->fneditgroup($id);	// get the details from the DB into array variable	
		$this->view->lobjGroupform->populate($larrresult); // populate the result in the edit form
			
	    // check if the user has clicked the save
    	if ($this->_request->isPost() && $this->_request->getPost('Save'))
    	 {       	 			 	
    		$larrformData = $this->_request->getPost();	 // get the form data  		    		    	
	   		$lintId = $id;	//store the id to be updated    		
	   		unset ($larrformData ['Save']);	 //unset the save data in form data  
    	 $result25=$this->lobjGroup->getactivesets();
	     	
	       
			$values=0;
			for($i=0;$i<count($result25);$i++)
			{
				$value=$result25[$i]['IdTOS'];
				$values=$values.','.$value;
			}
			$resultgroup=$this->lobjGroup->getgroupsection($larrresult['idgroupname']);
			//echo "<pre />";
	   		//print_r($resultgroup[0]['QuestionGroup']);
	   		//die();
	   		$groupname=$resultgroup[0]['QuestionGroup'];
	   		$result99=$this->lobjGroup->gettossection($values,$groupname);
	   		
	   		if($result99 && $larrformData['Active']==0)
	   		{
	   		echo '<script language="javascript">alert("The system cannot allow to remove this question group as its  reffered by active set");</script>';   	
			echo "<script>parent.location = '".$this->view->baseUrl()."/examination/group/index';</script>";
                	die();
	   			
	   		}
	   		else 
	   		{
	   				$this->lobjGroup->fnupdategroup($lintId,$larrformData); //update businesstype
			$this->_redirect( $this->baseUrl . '/examination/group/index'); //redirect to index page after updating		
	   		}
							
    	}
	  }
public function excelexportAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$RegID = $this->_getParam('id');
		$progname = $this->lobjGroup->fngetprgname($RegID);
		$tosdesc = $this->lobjGroup->fngettosdesc($RegID);
		$displayquestions= $this->lobjGroup->fndisplayquestionsforabovecode($RegID);
	 	$host = $_SERVER['SERVER_NAME'];
    	$imgp = "http://".$host."/tbedemo/images/image.jpg"; 
    	//$tabledata = '<p style="text-align:center;"><img width="30%" src="../public/images/image.jpg" /></p>';
		$tabledata='<br><br>';
    	$tabledata.='<html><body><table border=0><tr><td></td><td></td><td colspan ="3" height="210"><img src= "'.$imgp.'" /></td></tr></table>';
		$ReportName = $this->view->translate( "Question" ).' '.$this->view->translate( "Paper" );
		$tabledata.= "<table border=0 ><tr><td style='border:1px solid black;' colspan ='5' align=center><b> {$ReportName}</b></td></tr></table><br>";

		//$tabledata.= 'Course :&nbsp;'.$progname['ProgramName'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'TOS:&nbsp;'.$tosdesc['Setcode'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Question Set:&nbsp;'.$progname['PaperCode'].'<br>';
		
		$tabledata.= '<table border=0 width=100%>
						<tr>
							<td align="left" colspan="2"><b>Course:</b>&nbsp;'.$progname['ProgramName'].'</td>
							<td align="right" colspan="3"><b>TOS Name:</b>&nbsp;'.$tosdesc['Setcode'].'</td>
						</tr>
						<tr>
							<td colspan="5" align="left"><b>Question Paper Code:</b>&nbsp;'.$progname['PaperCode'].'</td>
							
						</tr>
					</table>';
		
		$tabledata.='<hr></hr>';
		for($i=1,$s=0;$i<=count($displayquestions),$s<count($displayquestions);$s++,$i++){ 
			$tabledata.='<br><table border=0 width=100%><tr>
     						<td valign="top">'.'Q'.$i.'.'.'</td>
     						<td valign="top" align="left" colspan ="4">'.$displayquestions[$s]['Question'].'</td>
	  					</tr>';
		            	$larranswers = $this->lobjGroup->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
		            	for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++){
							$tabledata.='<tr>
											<td></td>
     										<td align="left" colspan ="4" valign="top">'.'A'.$i.'.'.$d.')'.' '.$larranswers[$a]['answers'].'<br>
     										</td>
	  									</tr><br>';
		            	}
		            	$tabledata.='<tr>
     										<td colspan="5" ></td>
	  									</tr><br>';
		            
		 }
		$tabledata.='</table>';
		//echo "<pre/>";print_r($tabledata);die();
		$date=date("ymd");
		$progname['ProgramName'];
		$prg = str_replace(' ', '', $progname['ProgramName']);
		$filename = 'ManualQuestionPaper_'.$date.'_'.$prg;
		$ourFileName = realpath('.')."/data"; 
	    $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); //open a file to write a text
		//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(500);
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));//write the content of htmlcode into text file
		fclose($ourFileHandle); //closeing a file 
		header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
		header("Content-Disposition: attachment;filename=$filename.xls"); 
		//header("filename=mn.xls");
		//header("Content-Type: application/vnd.ms-word,charset=UTF-8");
		//header("Content-Disposition: attachment; filename=$filename.doc");
		
		readfile($ourFileName);
		unlink($ourFileName); 
	}
		
		
	public function viewAction(){   	
		$this->view->lobjGroupform = $this->lobjGroupform; //send the Form the view
		//$idTOSMaster=$this->_Getparam("id"); // get the id of the group to be updated
		//$idTOSMaster=22;
		$idTOSMaster=$_SESSION['idtosmaster'];
		$idprogram=$_SESSION['idprogram'];
		$larrrtosfulldetails=$this->lobjGroup->fnGetTOSFullDetails($idTOSMaster);
		
		$prgname=$this->lobjGroup->fnGetCourse($idTOSMaster);
		$this->view->prgramname=$prgname['ProgramName'];
		$this->view->idprogram=$idprogram;
		$this->view->setname=$prgname['BatchName'];
		$this->view->setnamepaper=$_SESSION['views'];
		$this->view->tosfull=$larrrtosfulldetails;
		$this->view->idtos=$idTOSMaster;
		
		if(!empty($larrrtosfulldetails[0]['IdBatch']))
		{
		$larrgetotalparts=$this->lobjGroup->fngettotalparts($larrrtosfulldetails[0]['IdBatch']);
		for($idp=0;$idp<count($larrgetotalparts);$idp++){
			if($idp==0){
				$idparts=$larrgetotalparts[$idp]['IdPart'];
				$idparts="'$idparts'";
			}else{
				$idpartss=$larrgetotalparts[$idp]['IdPart'];
				$idpartss="'$idpartss'";
				$idparts=$idparts.','.$idpartss;
			}
			$idpart2=$larrgetotalparts[$idp]['IdPart'];
			$resulttos=$this->lobjGroup->fngetquestionstosdetail($idpart2,$idTOSMaster);
			$ecahpartquestions[$idpart2]=$resulttos['totals'];
			//$larrtotalchapters2[]=$this->lobjGroup->fngetchaptereachpartpool($larrgetotalparts[$idp]['IdPart']);
		}
		//echo $idparts;die();
		$larrchpterlevel=$this->lobjGroup->fngetchapterlevel($idparts);//////
		$larrtotalchapters=$this->lobjGroup->fngetchapterfrompool($idparts);//////
		//echo "<pre/>";print_r($larrtotalchapters);
		//sort($larrtotalchapters,SORT_NATURAL);
		
		$larrpartquestions=$this->lobjGroup->fngettotalpartqusetions($idparts);/////
		$larrinactivequestions=$this->lobjGroup->fngettotalinactivequsetions($larrgetotalparts);/////
		$this->view->totalinactivequestions=$larrinactivequestions;
		$larrreviewquestions=$this->lobjGroup->fngettotalreviewqusetions($larrgetotalparts);/////
		$this->view->totalreviewquestions=$larrreviewquestions;
		}
		
		for($lev=0;$lev<count($larrchpterlevel);$lev++)
		{
			$chapter=$larrchpterlevel[$lev]['QuestionNumber'];
			$level=$larrchpterlevel[$lev]['QuestionLevel'];
			$larrlevels[$chapter][$level]=$larrchpterlevel[$lev]['cntlevel'];
		}
		$this->view->chapters=$larrtotalchapters;
		
		$this->view->levelchapters=$larrlevels;
		$this->view->groups=$larrpartquestions;
		$this->view->eachgrp=$ecahpartquestions;
		}
	
public function lookupbatchidAction(){		
		// Added on 05-09-2012
		
		$this->_helper->layout->disableLayout();
		 $chapter=$this->_getParam('chapter');
		 $level=$this->_getParam('level');
		 $activecount=$this->_getParam('activecount');
		 $toscount=$this->_getParam('toscount');
		 $idtos=$this->_getParam('idtos');
		 $idprogramar=$this->lobjGroup->fngetprogramoftos($idtos);
		 $idprogram=$idprogramar['IdProgrammaster'];
		$questions=$this->lobjGroup->fngetactivetosquestions($chapter,$level);
		
		$questionsselected=$this->lobjGroup->fngetselectedquestions($chapter,$level,$idtos);
		//echo "<pre/>";print_r($questionsselected);
		$this->view->questions=$questions;
		if(!empty($questionsselected['0']['idquestions'])){
		$selquestionarray=explode(',',$questionsselected['0']['idquestions']);
		}else{
			$selquestionarray=array('0');
		}
		$this->view->idprogram=$idprogram;
		$this->view->idtos=$idtos;
		$this->view->toscount=$toscount;
		$this->view->selquestions=$selquestionarray;
		
	}
	
public function lookupinsertionAction(){		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$saveque=$this->_getParam('saveque');
		$idtos=$this->_getParam('idtos');
		$chapter=$this->_getParam('chapter');
		$level=$this->_getParam('level');
		
		$larrgetotalparts=$this->lobjGroup->fnupdatemanualquestions($saveque,$idtos,$chapter,$level);
			echo 1;
	}
		

		
	
	  
}
