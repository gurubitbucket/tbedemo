<?php
//error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_ExamissuesreportController extends Zend_Controller_Action { //Controller for the User Module
public $gsessionidCenter;//Global Session Name
	public function init() { //initialization function		
		//echo "init function";
		$this->gsessionidCenter = Zend_Registry::get('sis'); 
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
		$this->lobjCommon = new App_Model_Common();
	}
	
	public function fnsetObj() {
		$this->lobjanswer = new Examination_Model_Answer(); //intialize user db object
		$this->lobjform = new App_Form_Search(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjexamreport = new Examination_Model_Examissuereports(); //intialize user db object
		$this->lobjstudentissueform = new App_Form_Studentissueform();
	}
	
	public function indexAction() 
	{
/*		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$todaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$examdate = "{max:'$todaydate',datePattern:'dd-MM-yyyy'}"; */
		Zend_Session:: namespaceUnset('examissue');
		$this->view->lobjform = $this->lobjstudentissueform; //send the lobjForm object to the view

		$larrcenternames = $this->lobjanswer->fngetcenternames();
			$this->view->lobjform->field5->addMultiOptions(array(''=>'All'));
		$this->view->lobjform->field5->addMultiOptions($larrcenternames);
				$this->lobjform->field5->setRegisterInArrayValidator(false);
		$this->lobjform->field5->setAttrib('required',"false");
		$this->lobjform->field10->setAttrib('required',"true");
		//$this->lobjform->field10->setAttrib('constraints', "$examdate");
		//$this->lobjform->field5->setAttrib('readOnly',true);
		
		$larrrisuuelist=$this->lobjexamreport->fngetallissuelist();
		$this->view->lobjform->Issues->addMultiOptions(array(''=>'All'));
		$this->view->lobjform->Issues->addMultiOptions($larrrisuuelist);
		
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->examissuesreportpaginatorresult);
		
		
		$lintpagecount = $this->gintPageCount = 100;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->examissuesreportpaginatorresult)) {
			$this->view->venufield= $this->gobjsessionsis->examvenue;
			$this->view->datefield = $this->gobjsessionsis->examrvenuedate;
			
			$this->lobjform->field5->setValue($this->view->venufield);
			$this->lobjform->field10->setValue($this->view->datefield);
			
			
			
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->examissuesreportpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			Zend_Session:: namespaceUnset('examissue');
			//echo "<pre/>";
			//print_r($larrformData);
			//die();
			if ($this->lobjform->isValid($larrformData)) 
			{
				$venuefield = $this->view->venufield=$larrformData['field5'];
				$examrvenuedate= $this->view->datefield=$larrformData['field10'];
				unset ( $larrformData ['Search'] );
				
				if(!empty($larrformData['field10']))
				{
					
						$this->view->lobjform->field10->setValue($larrformData['field10']);
				}
			if(!empty($larrformData['field5']))
				{
			$larrcenternames = $this->lobjanswer->fngetcenternames();
		$this->view->lobjform->field5->addMultiOptions($larrcenternames);
		$this->view->lobjform->field5->setValue($larrformData['field5']);
					
				}
				
			if(!empty($larrformData['ICNO']))
				{
						$this->view->lobjform->ICNO->setValue($larrformData['ICNO']);
				}
				if(!empty($larrformData['Name']))
				{
					$this->view->lobjform->Name->setValue($larrformData['Name']);	
				}
				
			if(!empty($larrformData['Issues']))
				{
					$larrrisuuelist=$this->lobjexamreport->fngetallissuelist();
					$this->view->lobjform->Issues->addMultiOptions($larrrisuuelist);
		$this->view->lobjform->Issues->setValue($larrformData['Issues']);
					
				}
				$larrresult = $this->lobjexamreport->fngetsearchissuedetails($larrformData); //searching the values for the businesstype
				
				
				$larrColorArray['red'] =  Array();
					$larrColorArray['redsession'] =  Array();
					$larrColorArray['redprogram'] =  Array();
					foreach ($larrresult as $lobjCountry)
					{
					
							$larrColorArray['red'][] = $lobjCountry['idcenter'];
							$larrColorArray['redprogram'][] = $lobjCountry['IdProgrammaster']; 
							$larrColorArray['redsession'][] = $lobjCountry['idmangesession']; 
						 
					 }
					 $this->view->larrcolor = $larrColorArray;
					 
				//echo "<pre>";
				//print_r($larrresult);
				//die();
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->examissuesreportpaginatorresult = $larrresult;
				$this->gobjsessionsis->examvenue=$venuefield;
				$this->gobjsessionsis->examrvenuedate = $examrvenuedate;
				    $namespace = new Zend_Session_Namespace('examissue');
    				$namespace->data = $larrresult;
    					
			}
		}
	}
	
	public function pdfexportAction()
	{
		
		
	    	  $namespace = new Zend_Session_Namespace('examissue');
    $data = $namespace->data; 
    
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$selectedvenu = $this->_getParam('venu');
		$takenexamdate = $this->_getParam('examdate');
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		//$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		  //$stylesheet = file_get_contents('../public/css/default.css');	
		  //$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Exam Issue" ).' '.$this->view->translate( "Report" );
		$Examvenue = $this->view->translate( "Examvenue" );
		$Program = $this->view->translate( "Program" );
		$ExamSession = $this->view->translate( "Exam Session" );
		$Name = $this->view->translate( "StudentName" );
		$Issue = $this->view->translate( "Issue" );
		
		//$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		//$mpdf->WriteHTML();
		
		$datelabel = "Date :";
		$timelabel = "Time :";
		$currentdates = date('d-m-Y');
		$currenttimes = date('H:i:s');
		
		
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		$tabledata.= "<br><table border=1 align=center width=100%>
						<tr>
							<th align=center><b> {$datelabel}</b></th>
							<th align=center><b> {$currentdates}</b></th>
							<th align=center><b> {$timelabel}</b></th>
							<th align=center><b> {$currenttimes}</b></th>
						</tr></table>";
		$tabledata.= "<br><table border=1 align=center width=100%>
							<tr>
								<th align=center><b> {$ReportName}</b></th>
							</tr></table>";
		$centerarray = array();
		$tabledata.= "<br><table border=1 width=100%>
							<tr>
								<th align=center><b> {$Examvenue}</b></th>
								<th align=center><b> {$Program}</b></th>
								<th align=center><b> {$ExamSession}</b></th>
								<th align=center><b> {$Name}</b></th>
								<th align=center><b> {$Issue}</b></th>
								
						
							</tr>";
		
			$larrformData['field10'] = $takenexamdate;
			$larrformData['field5'] = $selectedvenu;
			//$larrresult = $this->lobjexamreport->fnGetsearchdetails($larrformData); //searching the values for the businesstype
			
			$larrresult=$data;
			
			//print_r($larrresult);die();
			foreach($larrresult as $larrresultss)	{ 
				$tabledata.="<tr>";
				
						$tabledata.="<td>";
					    $tabledata.=$larrresultss['centername'];
						$tabledata.="</td>";
					
										
	
						$tabledata.="<td>";
					    $tabledata.=$larrresultss['ProgramName'];
						$tabledata.="</td>";
						
						
	
						$tabledata.="<td>";
					    $tabledata.=$larrresultss['managesessionname'];
						$tabledata.="</td>";			
	
						
						
						$tabledata.="<td>";
						$tabledata.=$larrresultss['Studentname'];
						$tabledata.="</td>";
				
						
						$tabledata.="<td>";
					    $tabledata.=$larrresultss['Issue'];
						$tabledata.="</td>";
				
					
				$tabledata.="</tr>";	
			}

		 /*
		  
		  $tabledata.="<tr>
		  				<td colspan='8' align='center'><b>{$total}</b></td>
		  				<td><b>{$regsum}</b></td>
		  				<td><b>{$Apprsum}</b></td>
		  				<td><b>{$attsum}</b></td>
		  				<td><b>{$absum}</b></td>
		  				<td><b>{$passsum}</b></td>
		  				<td><b>{$failsum}</b></td>
		  			</tr>";	
			*/
		
			$tabledata.="</table><br>";
		
		$currentselectedvenu = $larrresultss['centername'];
		$examdateorig =  date ( "M_d_Y", strtotime ($takenexamdate) );
		
		$mpdf->WriteHTML($tabledata);  
		$mpdf->Output('Exam_Issuereport_for'.'_'.'For'.'_'.$examdateorig,'D');
	}
	public function issuesAction() 
	{
	
	    $this->view->lobjform = $this->lobjstudentissueform;
	    $this->view->venue = $selectedvenu = $this->_getParam('venu');
		$this->view->examdate =$takenexamdate = $this->_getParam('examdate');		
		$result = $this->lobjexamreport->fngetissuelist();
		if ($this->_request->isPost () )
		{
			$larrformData = $this->_request->getPost ();
			
				$this->lobjexamreport->fninsertissuedetails($larrformData);
			}	
		$this->view->larresult = $result;
	}
	
	public function lookupstudentAction()
	{	
		$this->_helper->layout->disableLayout();
		$this->view->venue= $Examvenue = $this->_getParam('Venue');
		$this->view->examdate =$Examdate = $this->_getParam('Exam');
		$this->view->idissue =$Examdate = $this->_getParam('idissue');		
	}	
	public function refinestudentlistAction(){
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		 $strcandidatename = $this->_getParam('candidatename');
		 $strcandidateicno = $this->_getParam('candidateicno');
		$this->view->examdate =$examdate = $this->_getParam('examdate');
		$this->view->venue =$examvenue = $this->_getParam('examvenue');
		$larrgetregpin = $this->lobjexamreport->fnGetstudentexamdetails($strcandidatename,$strcandidateicno,$examdate,$examvenue);
		echo Zend_Json_Encoder::encode($larrgetregpin);
		
	}
	public function getcandidatelookupAction(){		
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$lintidapplication = $this->_getParam('idapplication');
		$idissue = $this->_getParam('idissue');
		
		$larrgetcandidatelookup = $this->lobjexamreport->fnGetcandidatelookup($lintidapplication);        		
		 $count=count($larrgetcandidatelookup);
        $tabledata.='<table class="table" width="100%"><tr><th><b>Candidates Name</b></th><th><b>ICNO</b></th>';	
		for($lokiter=0;$lokiter<$count;$lokiter++){			
			 $iadppss=$larrgetcandidatelookup[$lokiter]['IDApplication'];
				$tabledata.='<tr><td ><b>'.$larrgetcandidatelookup[$lokiter]['FName'].' </b></td><td ><b>'.$larrgetcandidatelookup[$lokiter]['ICNO']."</b></td><td><input type='hidden' id='idapp' name='idapp[$idissue][]' value='$iadppss'></td></tr>";
				
			
		}	
		echo $tabledata.='<table/>';
	}
	
	
	
	
	public function studentdetailsAction(){		
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$lintidapplication = $this->_getParam('idapplication');
		
		$larrgetcandidatelookup = $this->lobjexamreport->fnGetcandidatecurrentstatus($lintidapplication);        		
		 $count=count($larrgetcandidatelookup);
        $tabledata.='<table class="table" width="100%"><tr><th><b>Candidates Name</b></th><th><b>ICNO</b></th><th><b>Email</b></th><th><b>Exam date</b></th><th><b>Exam date</b><th><b>Candidate Id</b></th><th><b>Type</b></th></tr>';	
		for($lokiter=0;$lokiter<$count;$lokiter++){			
			 $iadppss=$larrgetcandidatelookup[$lokiter]['IDApplication'];
				$tabledata.='<tr><td ><b>'.$larrgetcandidatelookup[$lokiter]['FName'].' </b></td><td ><b>'.$larrgetcandidatelookup[$lokiter]['ICNO'].'</b></td><td ><b>'.$larrgetcandidatelookup[$lokiter]['email'].' </b></td><td ><b>'.$larrgetcandidatelookup[$lokiter]['examdate'].'</b></td><td ><b>'.$larrgetcandidatelookup[$lokiter]['examdate'].'</b></td><td ><b>'.$larrgetcandidatelookup[$lokiter]['Regid'].'</b></td>';
				if($larrgetcandidatelookup[$lokiter]['batchpayment']==0)
				$tabledata.='<td ><b>Individual</b></td></tr>';
				if($larrgetcandidatelookup[$lokiter]['batchpayment']==1)
				$tabledata.='<td ><b>Company</b></td></tr>';
				if($larrgetcandidatelookup[$lokiter]['batchpayment']==2)
				$tabledata.='<td ><b>Takaful</b></td></tr>';
			
		}	
		$tabledata.="<tr><td colspan= '4' align='right'><input type='button' id='close' name='close'  value='Close' onClick='closebox();'></td></tr>";
	
		echo $tabledata.='<table/>';
	}
	
	public function studentissuedetailsAction()
	{
		
			$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$idissue = $this->_getParam('idissue');
		
		$larrgetremark = $this->lobjexamreport->fnGetcandidateremarkstatus($idissue);        
		if(empty($larrgetremark['Remark']))
		{
			$larrgetremark['Remark']="No Remarks";
		}		
		// $count=count($larrgetcandidatelookup);
        $tabledata.='<table class="table" width="100%"><tr><th colspan= "4"><b>Remark</b></th>';	
	
				$tabledata.='<tr><td colspan= "4" align="center" ><b>'.$larrgetremark['Remark'].' </b></td>';
				
			
	
		$tabledata.="<tr><td colspan= '4' align='right'><input type='button' id='close' name='close'  value='Close' onClick='closebox();'></td></tr>";
	
		echo $tabledata.='<table/>';
		
	}
	
	
}