<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_StudentattemptsController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjStudentattemptsmodel = new Examination_Model_Studentattempts(); //intialize newscreen db object
		$this->lobjStudentattemptsForm = new Examination_Form_Studentattempts(); 
	}
	
	public function indexAction() 
	{
	
	$this->view->lobjStudentattemptsForm = $this->lobjStudentattemptsForm;
	$larrresultprog =$this->lobjStudentattemptsmodel->fngetprog(); 
	$this->lobjStudentattemptsForm->Program->addMultiOptions($larrresultprog);
	 

	if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
	{
			$larrformdata = $this->_request->getPost ();
			$this->lobjStudentattemptsForm->FromDate->setValue($larrformdata['FromDate']);
				$this->lobjStudentattemptsForm->ToDate->setValue($larrformdata['ToDate']);
			$this->view->fromdate = $larrformdata['FromDate'];
			$this->view->todate = $larrformdata['ToDate'];
			$this->view->prog = $larrformdata['Program'];
			
			$this->lobjStudentattemptsForm->Program->setValue($larrformdata['Program']);
			
			$larresutstudentid = $this->lobjStudentattemptsmodel->fncountattemptforstudents($larrformdata['FromDate'],$larrformdata['ToDate']);
			
		//	echo "<pre>";
	  // print_r($larresutstudentid);//die();
			for($pra=0;$pra<count($larresutstudentid);$pra++)
			{
			$icno=$larresutstudentid[$pra]['ICNO'];
			$idapplication=$larresutstudentid[$pra]['IDApplication'];
			
			$examdate=$larresutstudentid[$pra]['DateTime'];
			
			$larrgettotalcount= $this->lobjStudentattemptsmodel->fncountattemptbyicno($icno,$examdate);
			$larrcnt[$idapplication]=$larrgettotalcount['total'];
			}			$this->view->paginator = $larresutstudentid;
			$this->view->cntattempt=$larrcnt;
			
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Update' )) 
		{
			$larrformData = $this->_request->getPost ();			
			if(isset($larrformData['stdid']))
			{			
			for($i=0;$i<count($larrformData['stdid']);$i++)
			{
			
			
			   $Idappln[] = explode(',',$larrformData['stdid'][$i]);
			   $Idappliaction = $Idappln[$i]['0'];
			 
			   $appearcount = $Idappln[$i]['3']+1;			 
			   $larresultchronevenue = $this->lobjStudentattemptsmodel->fnupdatestudentapplication($Idappliaction,$appearcount);
			
			  }
}			  
			  
			}
		
		
	    if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' ))
	    {
			$this->_redirect( $this->baseUrl . '/examination/studentattempts/index');
			//$this->_redirect($this->view->url(array('module'=>'registrations' ,'controller'=>'batchapproval', 'action'=>'index'),'default',true));
		}
		
	}
	public function getallattemptsAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$icno = $this->_getParam('icno');
		$examdate = $this->_getParam('examdate');
		$program = $this->_getParam('program');
		$result = $this->lobjStudentattemptsmodel->fngetallattemptsforicno($icno,$examdate);
		$tabledata.= '<br><fieldset><legend align = "left">Previous Attempts:</legend>
					                    <table class="table" border=1 align = "center" width=100%>
					                    	<tr>
					                    	   <th><b>Examdate</b></th>
											   <th><b>RegisterID</b></th>
					                    		<th><b>Venue</b></th>
												<th><b>Program</b></th>
												<th><b>Session</b></th>
												<th><b>ICNO</b></th>
												<th><b>Name</b></th>
												<th><b>Result</b></th>
					                    		
					                    	</tr>';
		  
		               
		for($i=0;$i<count($result);$i++)
		{
		 
						  $tabledata.= '<tr><td align = "left">'.$result[$i]['DateTime'].'</td>';
					            $tabledata.= '<td align = "left">'.$result[$i]['Regid'].'</td>';          		
						    $tabledata.= '<td align = "left">'.$result[$i]['centername'].'</td>';
						      $tabledata.= '<td align = "left">'.$result[$i]['ProgramName'].'</td>';
						        $tabledata.= '<td align = "left">'.$result[$i]['managesessionname'].'</td>';
								 $tabledata.= '<td align = "left">'.$result[$i]['ICNO'].'</td>';
								 $tabledata.= '<td align = "left">'.$result[$i]['FName'].'</td>';
								if($result[$i]['pass']==1)
								{
						          $tabledata.= '<td align = "left">Pass</td></tr>';
								  }
								  elseif($result[$i]['pass']==2)
								  {
								     $tabledata.= '<td align = "left">Fail</td></tr>';
								  }
								  elseif($result[$i]['pass']==3)
								  {
								      $tabledata.= '<td align = "left">Applied</td></tr>';
								  }
								  elseif($result[$i]['pass']==4)
								  {
								      $tabledata.= '<td align = "left">Absent</td></tr>';
								  }
								 
		                      
						          
						         
			 }
			  $tabledata.="<tr><td colspan= '8' align='right'><input type='button' id='close' name='close'  value='Close' onClick='Closevenue();'></td></tr>";
		                     $tabledata.="</table><br>";
			 echo  $tabledata;	   
		//echo "<pre>";
		//print_r($larrresult);die();
		 
	
	}
	
	
	
	public function fnpdfexportAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$frmdate = $this->_getParam('frmdate');
		$todate = $this->_getParam('todate');
		$prog = $this->_getParam('prog');
		//$difftype = $this->_getParam('difftype');
	   // $questionids = $this->_getParam('questionids');
	    //$larrresultquestions = $this->lobjQuestionsattendedmodel->fngetprogdetails($prog,$difftype);
	  //  $progname = $larrresultquestions[0]['ProgramName'];
	  
		//$larresutstudentid = $this->lobjQuestionsattendedmodel->fnmaxids($prog,$frmdate,$todate);
	    //$totalstudent = count($larresutstudentid);
		//$values=0;$studentid=0;
		$larresutstudentid = $this->lobjStudentattemptsmodel->fncountattemptforstudents($frmdate,$todate);
		$host = $_SERVER['SERVER_NAME'];
	    $imgp = "http://".$host."/tbenew/images/reportheader.jpg";  
	    $html ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';	
		$html.="<br><br><table border=1 align=center width=100%><tr><td align ='left' colspan = 1><b>Exam From Date</b></td><td align ='left' colspan = 1><b>$frmdate</b></td><td align ='left' colspan = 1><b>Exam ToDate</b></td><td align ='left' colspan = 1><b>$todate</b></td></tr></table><br><br>";
		$html.= '<table class="table" width="100%" cellpadding="5" cellspacing="1" border="1">';
		$html.='<tr>	
	    	       <th>IDAppliction</th>
	    	       <th>Candidate Name</th>
	    	       <th>ICNO</th>
	    	       <th>Exam Date</th>
	    	       <th>Result</th>
	    	       <th>Already Appeared</th>  
	        	
	    	</tr>'	;
		for($pra=0;$pra<count($larresutstudentid);$pra++)
			{
			$icno=$larresutstudentid[$pra]['ICNO'];
			$idapplication=$larresutstudentid[$pra]['IDApplication'];
			$examdate=$larresutstudentid[$pra]['DateTime'];
			if($larresutstudentid[$pra]['pass'] ==1)
			{
			 $res= "Pass";
			}
			elseif($larresutstudentid[$pra]['pass'] ==2)
			{
			 $res= "Fail";
			}
			elseif($larresutstudentid[$pra]['pass'] ==3)
			{
			 $res= "Applied";
			}
			elseif($larresutstudentid[$pra]['pass'] ==4)
			{
			 $res= "Absent";
			}
			
			$larrgettotalcount= $this->lobjStudentattemptsmodel->fncountattemptbyicno($icno,$examdate);
			$larrcnt[$idapplication]=$larrgettotalcount['total'];
			
			 $html.="<tr><td >".$larresutstudentid[$pra]['IDApplication']."</td>";
			$html.="<td >".$larresutstudentid[$pra]['FName']."</td>";
			$html.="<td >".$larresutstudentid[$pra]['ICNO']."</td>";
			$html.="<td >".$larresutstudentid[$pra]['DateTime']."</td>";
			$html.="<td >".$res."</td>";
			$html.="<td >".$larrcnt[$idapplication]."</td></tr>";
			}				
	
	       $html.='</table>';
	      
	        /*echo $html;
	        die();*/
	       $filename='StudentAttempts'; 
	     $ourFileName = realpath('.')."/data"; 
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); //open a file to write a text
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($html));//write the content of htmlcode into text file
		fclose($ourFileHandle); //closeing a file 
		header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
		header("Content-Disposition: attachment; filename=$filename.xls");
		readfile($ourFileName);
		unlink($ourFileName);   

			
		}

	
}