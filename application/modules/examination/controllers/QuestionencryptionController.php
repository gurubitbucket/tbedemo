<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_QuestionencryptionController extends Base_Base {


	public function init() {	
		
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);  
	}
	public function fnsetObj(){
		$this->lobjquestionnoutilityModel = new Examination_Model_Questionencryption();

	}
	/*
	 *  search form & grid display  
	 */
	public function indexAction() {
 		$lobjsearchform = new App_Form_Search();  //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjuserForm object to the view
	
		 $larresults = $this->lobjquestionnoutilityModel->fnfindqtntype();
		 //print_R($larresults);
		 //die();
		 $normal =  strlen($larresults['Question']);
		
		 IF($normal==167)
		 {
		 	$this->view->encrypt = 2;
		 }
		 else 
		 {
		 	$this->view->encrypt = 1;
		 }
	if ($this->_request->isPost () && $this->_request->getPost ( 'Encrypt' )) {
				
				$larresult = $this->lobjquestionnoutilityModel->fngetAllquestions();
				for($i=0;$i<count($larresult);$i++)
				{
					$engquestions = $larresult[$i]['Question'];
					$Malayquestions = $larresult[$i]['Malay'];
					$Tamilquestions = $larresult[$i]['Tamil'];
					$Arabicquestions = $larresult[$i]['Arabic'];
					$idquestions = $larresult[$i]['idquestions'];
					$key='mystring';
					$engencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($engquestions,$key);
					
				
					$malayencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Malayquestions,$key);
					
					$Tamilencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Tamilquestions,$key);
				
					$arabicencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Arabicquestions,$key);
					
		            $larrupdateresult = $this->lobjquestionnoutilityModel->fnupdateencrytptedquestions($engencryptedquestions,$malayencryptedquestions,$Tamilencryptedquestions,$arabicencryptedquestions,$idquestions);
					
				}
		
			$larresult = $this->lobjquestionnoutilityModel->fngetAllanswers();
				for($i=0;$i<count($larresult);$i++)
				{
					$engquestions = $larresult[$i]['answers'];
					$Malayquestions = $larresult[$i]['Malay'];
					$Tamilquestions = $larresult[$i]['Tamil'];
					$Arabicquestions = $larresult[$i]['Arabic'];
					$idanswers = $larresult[$i]['idanswers'];
					$key='mystring';
					$engencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($engquestions,$key);
				
					$malayencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Malayquestions,$key);
					
					$Tamilencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Tamilquestions,$key);
				
					$arabicencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Arabicquestions,$key);
					
		            $larrupdateresult = $this->lobjquestionnoutilityModel->fnupdateencrytptedanswers($engencryptedquestions,$malayencryptedquestions,$Tamilencryptedquestions,$arabicencryptedquestions,$idanswers);
					
				}
		    $this->view->encrypt = 1;
		}
		
	

		if ($this->_request->isPost () && $this->_request->getPost ( 'Decrypt' )) {
			
			$larresult = $this->lobjquestionnoutilityModel->fngetAllquestions();
			for($i=0;$i<count($larresult);$i++)
			{
				$engquestions = $larresult[$i]['Question'];
				$Malayquestions = $larresult[$i]['Malay'];
				$Tamilquestions = $larresult[$i]['Tamil'];
				$Arabicquestions = $larresult[$i]['Arabic'];
				$idquestions = $larresult[$i]['idquestions'];
				$key='mystring';
				$engdecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($engquestions,$key);
				
			
				$malaydecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Malayquestions,$key);
				
				$Tamildecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Tamilquestions,$key);
			
				$arabicdecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Arabicquestions,$key);
				
	            $larrupdateresult = $this->lobjquestionnoutilityModel->fnupdateencrytptedquestions($engdecryptedquestions,$malaydecryptedquestions,$Tamildecryptedquestions,$arabicdecryptedquestions,$idquestions);
				
			}
			
			
		$larresultanswers = $this->lobjquestionnoutilityModel->fngetAllanswers();
		for($i=0;$i<count($larresultanswers);$i++)
		{
			$engquestions = $larresultanswers[$i]['answers'];
			$Malayquestions = $larresultanswers[$i]['Malay'];
			$Tamilquestions = $larresultanswers[$i]['Tamil'];
			$Arabicquestions = $larresultanswers[$i]['Arabic'];
			$idanswers = $larresultanswers[$i]['idanswers'];
			$key='mystring';
			$engdecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($engquestions,$key);
		
			$malaydecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Malayquestions,$key);
			
			$Tamildecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Tamilquestions,$key);
		
			$arabicdecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Arabicquestions,$key);
			
            $larrupdateresult = $this->lobjquestionnoutilityModel->fnupdateencrytptedanswers($engdecryptedquestions,$malaydecryptedquestions,$Tamildecryptedquestions,$arabicdecryptedquestions,$idanswers);
			
		}
		$this->view->encrypt = 2;
		}
		
		
		
	}
	
public function fnpdfexportAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$type = $this->_getParam('Type');
		if($type=='Questions')
		{
			$filename='questions'; 
			$host = $_SERVER['SERVER_NAME'];
	        $imgp = "http://".$host."/tbenew/images/reportheader.jpg";  
	        $html ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';	
		    
		    $html.= '<table class="table" width="100%" cellpadding="5" cellspacing="1" border="1">
		
	    		<tr>
	    	       <th>Sl No</th>
	    	       <th>Question Id</th>
	    	       <th>Question Group</th>
	    	       <th>Status</th>
	        	     <th>Question Chapter</th>
	    	        <th>Question (English)</th>
	    	       <th>Answer Id</th>
	    	        <th>Answer(English)</th>
	    	       
	    	       
	    	     </tr>'	;
		    $larquestions = $this->lobjquestionnoutilityModel->fngetAllquestions();
		    for($i=0;$i<count($larquestions);$i++)
		    {
		    	$idquestions = $larquestions[$i]['idquestions'];
		    	$questions = $larquestions[$i]['Question'];
		    	$QuestionId = $larquestions[$i]['QuestionId'];
		    	$QuestionGroup = $larquestions[$i]['QuestionGroup'];
		    	$QuestionNumber = $larquestions[$i]['QuestionNumber'];
		    	if($larquestions[$i]['Active']==1)
		    	{
		    		$Active='Active';
		    	}
		    	else 
		    	{
		    		$Active='In-Active';
		    	}
		    	$larresultsanswers = $this->lobjquestionnoutilityModel->fngetAnswers($idquestions);
		    	for($j=0;$j<count($larresultsanswers);$j++)
		    	{
		    		if($i==0 && $j==0)
		    		{
		    		$slno = 1;
		    		}
		    		else 
		    		{
		    			
		    		}
		    		$answerid = $larresultsanswers[$j]['Answersid'];
		    		$answers = $larresultsanswers[$j]['answers'];
		    		if($j==0)
		    		{
		    			$html.='<tr><td>'.$slno.'</td>
		    			            <td>'.$QuestionId.'</td> 
		    			            <td>'.$QuestionGroup.'</td>
		    			              <td>'.$Active.'</td>
		    			            <td>'.$QuestionNumber.'</td>
		    			            <td>'.$questions.'</td> 
		    			            <td>'.$answerid.'</td>
		    			            <td>'.$answers.'</td> 
		    			            
		    			            </tr>';
		    		}
		    		else 
		    		{
		    			$html.='<tr><td>'.$slno.'</td>
		    			            <td></td> 
		    			            <td></td>
		    			            <td></td>
		    			            <td></td>
		    			            <td></td> 
		    			            <td>'.$answerid.'</td>
		    			            <td>'.$answers.'</td> 
		    			            
		    			            </tr>';
		    		}
		    		$slno++;
		    	}
		    	
		    }
		      $html.='</table>';
	      
	        /*echo $html;
	        die();*/
	    $filename='Questions_And_Answers'; 
	    $ourFileName = realpath('.')."/data"; 
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); //open a file to write a text
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($html));//write the content of htmlcode into text file
		fclose($ourFileHandle); //closeing a file 
		header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
		header("Content-Disposition: attachment; filename=$filename.xls");
		readfile($ourFileName);
		unlink($ourFileName);   
		}
		
	}
}