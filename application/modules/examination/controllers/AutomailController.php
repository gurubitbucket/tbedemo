<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_AutomailController extends Base_Base 
{
	public function init() 
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->registry = Zend_Registry::getInstance();
   	    $this->locale = $this->registry->get('Zend_Locale');
   	    $this->lobjautomailmodel = new Examination_Model_Automailmodel(); //intialize newscreen db object
		$this->lobjautomailForm = new Examination_Form_Automail(); 
	}
    
	public function indexAction() 
	{
		$this->view->lobjform = $this->lobjautomailForm;
		$larrresult = array();
		//$auth = Zend_Auth::getInstance();
		$this->view->UpdUser = 1;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->UpdDate = $ldtsystemDate;
		if(!$this->_getParam('search')) 
	    unset($this->gobjsessionsis->studentpaginatorresult);
		if ($this->_request->isPost() && $this->_request->getPost('Search'))
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();               
				unset ( $larrformData ['Search']);
				$larrresult = $this->lobjautomailmodel->fngetstudentdetails($larrformData);
				$this->view->datacount = count($larrresult);
				$this->view->paginator = $larrresult;
			}
		}
		if ($this->_request->isPost() && $this->_request->getPost('Save'))
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				unset ( $larrformData ['Save']);
				//echo "<pre>";print_r($larrformData);die();
				$larrreqhrs = explode('T',$larrformData['ScheduleStartTime']);
			    $larrformData['ScheduleStartTime'] = $larrreqhrs[1];
			    $larrformData['status'] = 0;
				$larrresult = $this->lobjautomailmodel->fninsertdetails($larrformData);
			}
		}
	}
}