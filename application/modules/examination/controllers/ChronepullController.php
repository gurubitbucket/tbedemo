<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_ChronepullController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjChronepullmodel = new Examination_Model_Chronepull(); //intialize newscreen db object
		$this->lobjChronepullForm = new Examination_Form_Chronepull(); 
		$this->lobjCommonmodel = new GeneralSetup_Model_DbTable_Common();
	}
	
	public function indexAction() 
	{
		$larrvenueexamday = array();
		$this->view->StartTime= "Sat Nov 12 2011".' '.'10:00';
		 $this->view->centerdetails = $larrvenueexamday;
			$this->view->lobjChronepullForm = $this->lobjChronepullForm;
			  if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			  $larrformdata = $this->_request->getPost();

			  $larresultsessions = $this->lobjChronepullmodel->fngetallsessions();
			  $larresult = $this->lobjChronepullmodel->fngetallvenues($larrformdata);
			  $this->lobjChronepullForm->Session->addMultiOptions($larresultsessions);

			  $this->view->centreresults = $larresult;
			    $this->view->lobjChronepullForm->Date->setValue($larrformdata['Date']);
			    $this->view->lobjChronepullForm->Session->setValue($larrformdata['Session']);
			    if(date('Y-m-d') >= $larrformdata['Date'])
			    {
			    	$this->view->lobjChronepullForm->Schedulerdate->setValue(date('Y-m-d'));
			    }
			    else 
			    {
			    	$this->view->lobjChronepullForm->Schedulerdate->setValue($larrformdata['Date']);
			    }
			    
			  	$this->view->StartTime= "Sat Nov 12 2011".' '.$larresult[0]['endtime'];
			    
			  }
			  
			   if ($this->_request->isPost () && $this->_request->getPost ( 'Approve' )) {
			     $larrformData = $this->_request->getPost ();
			
			  $larrreqhrs = explode('T',$larrformData['ScheduleStartTime']);
			    $larrformData['ScheduleStartTime'] = $larrreqhrs[1];
			    $larrformData['status'] = 0;
			    $larrformData['UpdDate']=date('Y-m-d H:i:s');
			    $auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
			    $larrformData['UpdUser']=$iduser;
			   
			    $larrresultlastid = $this->lobjChronepullmodel->fninsertschedulerdetails($larrformData);
			    ///$larresultchronepull = $this->lobjChronepullmodel->fninsertchronepull($larrformData,$larrresultlastid);
			   }
			   
			    if ($this->_request->isPost () && $this->_request->getPost ( 'updatenow' )) {
			     $larrformData = $this->_request->getPost ();
					$Venue = $larrformData['updatenow'][0];
					$Session = $larrformData['Session'];
					$Examdate = $larrformData['Date'];
					$idchronepull = $larrformData['idchronepull'];
					$lobjchronepullclientmodel = new Examination_Model_Chronepullclient();
			    	$larresultidautovenueslist = $lobjchronepullclientmodel->fngetstatusforthevenue($Venue,$idchronepull);
					   	if($larresultidautovenueslist==1)
					   	{
					   		   //function to find the maximum time for the session
					   		    $larrclosetimefinding = $lobjchronepullclientmodel->fnfindthemaxtimeofprogram($Venue,$Session,$Examdate);
					   		    $maxprogramtime = $larrclosetimefinding['maximumtime'];
					   		    
					   		    
					   		    //function to find the start time of the exam
					   		    $larresultsmaxtime = $lobjchronepullclientmodel->fnfindthemaxstarttimeofvenue($Venue,$Session,$Examdate);
								$maxstartedtime = $larresultsmaxtime['maximumstartedtime'];
								
								$totaltime=$maxprogramtime+$maxstartedtime+2; 
		
								$currenttime = date('H');
								$min = date('i');
								
								//echo $selecteddate;
								$currenttimeinmin = $currenttime*60+$min;
								$currentdate =  date('Y-m-d');
								$activatetheclosetime=0;
								
								/*echo $currenttimeinmin;
								echo $totaltime;
								die();*/
					   			if($currentdate>$Examdate)
								{
									$activatetheclosetime=1;
								}
								
								if($activatetheclosetime==0)
								{
						   			if($currenttimeinmin>$totaltime)
									{
										$activatetheclosetime=1;
									}
									else
									{
							 			$activatetheclosetime=0;
									}
								}
								/*print_r($activatetheclosetime);
								die();*/
								if($activatetheclosetime==1)
								{
							   		    $larrcandidatepresent = $lobjchronepullclientmodel->closetimeimplementation($Venue,$Session,$Examdate);
							   		
							   			$larrexamstudents = $lobjchronepullclientmodel->fngetstudentsexamdetails($Venue,$Session,$Examdate);
							   			//function to update the student pass,fail, and inserting the student marks,details marks etc on the server
			    						$larupdate = $this->lobjChronepullmodel->fnupdatestudent($larrexamstudents);
			    						$IDApplication = 0;$Regid=0;
							    		for($linti=0;$linti<count($larrexamstudents);$linti++)
							    		{
							    			$value=$larrexamstudents[$linti]['IDApplication'];
							    			$valueregids = $larrexamstudents[$linti]['Regid'];
							    			$Regid = $Regid.','."'$valueregids'";
							    			$IDApplication = $IDApplication.','."'$value'";
							    		}
			    					  $larranswerdetails = $lobjchronepullclientmodel->fngetanswerdetails($Venue,$Regid);
							   		  if(count($larranswerdetails)>0)
							            {
							    				$larranswerdetails = $this->lobjChronepullmodel->fninsertanswerdetailsinserver($larranswerdetails);
							            }
							            ////absentese student/////////////////////////////////////////////
							    		$larrexamstudentabsent = $lobjchronepullclientmodel->fngetstudentsabsent($Venue,$Session,$Examdate);
							    		$larupdatess = $this->lobjChronepullmodel->fnupdatestudentabsent($larrexamstudentabsent);
							    		
							    		
							    		$larresultstarttimeincenter = $this->lobjChronepullmodel->fngetcenterstartfromserver($Venue,$Session,$Examdate);
										$larrclosetimeupdate = $lobjchronepullclientmodel->fngetclosetimeserver($Venue,$Session,$Examdate);
										if(count($larresultstarttimeincenter)>0)
										{
											$larrresultupdateonserver = $this->lobjChronepullmodel->fnupdateclosetimeserver($larrclosetimeupdate);
										}
										else 
										{
											$larrresultupdateonserver = $this->lobjChronepullmodel->fninsertexamcentermainserver($larrclosetimeupdate);
										}
										
										
										$larrresultupdateonserver = $this->lobjChronepullmodel->fnupdatedchronedstatussucess($idchronepull);
										$larrexamstudentabsent = $lobjchronepullclientmodel->fngetupdatepushstatus($Venue,$Session,$Examdate);
											
											
								}
					   	}
			    }
		
	}
	
	
	public function getsessionsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$larresultvenuedateschedule = $this->lobjChronepullmodel->fngetvenuedateschedule($iddate);
		$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larresultvenuedateschedule);
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
	}
	

	public function chronejobsdetailsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	 	$Currentdate = date('Y-m-d H:i'); 
		$lobjchronepullclientmodel = new Examination_Model_Chronepullclient();
		$larr = explode(' ',$Currentdate);
		$shcdate = $larr[0];
		
		$shcdtime ='19:01:00';//$larr[1] = $larr[1].':00';
		$larrresultidautomails = $this->lobjChronepullmodel->fngetscheduledetails($shcdate,$shcdtime);
		$totalcountofidautomails = count($larrresultidautomails);
	
		if($totalcountofidautomails>0)
		{
			$idautomail = 0;
			for($i=0;$i<count($larrresultidautomails);$i++)
			{
				$idautomail = $idautomail.','.$larrresultidautomails[$i]['idautochronepull'];
			}
		
		
			$larresultvenuesid = $this->lobjChronepullmodel->fngetallvenuesforidautomail($idautomail);
			
			$venuecount = count($larresultvenuesid);

			if($venuecount>0)
			{
				 for($j=0;$j<$venuecount;$j++)
				   {
					   	 
					   	$Venue = $larresultvenuesid[$j]['Venue'];
					   	$Session = $larresultvenuesid[$j]['Session'];
					   	$Examdate = $larresultvenuesid[$j]['Examdate'];
					   	$idchronepull = $larresultvenuesid[$j]['idchronepull'];
					   	$larresultidautovenueslist = $lobjchronepullclientmodel->fngetstatusforthevenue($Venue,$idchronepull);
	
					   	if($larresultidautovenueslist==1)
					   	{
					   	
					   		    $larrclosetimefinding = $lobjchronepullclientmodel->fnfindthemaxtimeofprogram($Venue,$Session,$Examdate);
					   		    $maxprogramtime = $larrclosetimefinding['maximumtime'];
					   		    
					   		    $larresultsmaxtime = $lobjchronepullclientmodel->fnfindthemaxstarttimeofvenue($Venue,$Session,$Examdate);
								$maxstartedtime = $larresultsmaxtime['maximumstartedtime'];
								
								$totaltime=$maxprogramtime+$maxstartedtime+10; 
		
								$currenttime = date('H');
								$min = date('i');
								
								//echo $selecteddate;
								$currenttimeinmin = $currenttime*60+$min;
								$currentdate =  date('Y-m-d');
								$activatetheclosetime=0;
								
								/*echo $currenttimeinmin;
								echo $totaltime;
								die();*/
					   			if($currentdate>$Examdate)
								{
									$activatetheclosetime=1;
								}
								
								if($activatetheclosetime==0)
								{
						   			if($currenttimeinmin>$totaltime)
									{
										$activatetheclosetime=1;
									}
									else
									{
							 			$activatetheclosetime=0;
									}
								}
							/*	print_r($activatetheclosetime);
								die();*/
								if($activatetheclosetime==1)
								{
									
									$larrextratime = $lobjchronepullclientmodel->fngetextratime($Venue,$Session,$Examdate);
									
										$larrresultupdateonserver = $this->lobjChronepullmodel->fninsertchronepullinmain($larrextratime);
										
							   		    $larrcandidatepresent = $lobjchronepullclientmodel->closetimeimplementation($Venue,$Session,$Examdate);
							   		
							   			$larrexamstudents = $lobjchronepullclientmodel->fngetstudentsexamdetails($Venue,$Session,$Examdate);
							   			//function to update the student pass,fail, and inserting the student marks,details marks etc on the server
			    						$larupdate = $this->lobjChronepullmodel->fnupdatestudent($larrexamstudents);
			    						$IDApplication = 0;$Regid=0;
							    		for($linti=0;$linti<count($larrexamstudents);$linti++)
							    		{
							    			$value=$larrexamstudents[$linti]['IDApplication'];
							    			$valueregids = $larrexamstudents[$linti]['Regid'];
							    			$Regid = $Regid.','."'$valueregids'";
							    			$IDApplication = $IDApplication.','."'$value'";
							    		}
			    					  $larranswerdetails = $lobjchronepullclientmodel->fngetanswerdetails($Venue,$Regid);
							   		  if(count($larranswerdetails)>0)
							            {
							    				$larranswerdetails = $this->lobjChronepullmodel->fninsertanswerdetailsinserver($larranswerdetails);
							            }
							            ////absentese student/////////////////////////////////////////////
							    		$larrexamstudentabsent = $lobjchronepullclientmodel->fngetstudentsabsent($Venue,$Session,$Examdate);
							    		$larupdatess = $this->lobjChronepullmodel->fnupdatestudentabsent($larrexamstudentabsent);
							    		
							    		
							    		$larresultstarttimeincenter = $this->lobjChronepullmodel->fngetcenterstartfromserver($Venue,$Session,$Examdate);
										$larrclosetimeupdate = $lobjchronepullclientmodel->fngetclosetimeserver($Venue,$Session,$Examdate);
										if(count($larresultstarttimeincenter)>0)
										{
											$larrresultupdateonserver = $this->lobjChronepullmodel->fnupdateclosetimeserver($larrclosetimeupdate);
										}
										else 
										{
											$larrresultupdateonserver = $this->lobjChronepullmodel->fninsertexamcentermainserver($larrclosetimeupdate);
										}
										$larrextratime = $lobjchronepullclientmodel->fngetextratime($Venue,$Session,$Examdate);
										$larrresultupdateonserver = $this->lobjChronepullmodel->fninsertchronepullinmain($larrextratime);
										
								}
								
								if($activatetheclosetime==0)
								{
									 	$larresultidautovenueslist = $lobjchronepullclientmodel->fnupdatedchronedstatusfailed($idchronepull);
								}
					   	}
					   								
				   }
			}
		}
		
	}
	
}