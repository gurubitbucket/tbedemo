<?php
error_reporting (E_ALL & ~E_NOTICE & ~E_DEPRECATED);
class Examination_AnsweruploadsController extends Base_Base 
{
	public function init() 
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->registry = Zend_Registry::getInstance();
   	    $this->locale = $this->registry->get('Zend_Locale');
   	    $this->lobjquestionsmodel = new Examination_Model_Questionuploads(); //intialize newscreen db object
		$this->lobjquestionsForm = new Examination_Form_Questionuploads(); 
	}
	public function indexAction() 
	{
	
		$this->view->lobjform = $this->lobjquestionsForm;
		if ($this->_request->isPost() && $this->_request->getPost('Upload'))
		{
			$larrformData = $this->_request->getPost();
			$lintfilecount['Count'] =0;
			$lstruploaddir = "/uploads/testingdata/";
			$larrformData['FileLocation'] = $lstruploaddir;			    
			 require_once 'Excel/excel_reader2.php';			
			 if ($this->lobjform->isValid($larrformData))
			 {
			    if($_FILES['FileName']['error'] != UPLOAD_ERR_NO_FILE)
				{
				   
				  $lintfilecount['Count']++; 				
				  $lstrfilename = pathinfo(basename($_FILES['FileName']['name']),PATHINFO_FILENAME);					
				  $lstrext = pathinfo(basename($_FILES['FileName']['name']),PATHINFO_EXTENSION);				 
				  $filename = $lintfilecount['Count'].".".date('YmdHis').".".$lstrext;
				  $filename = str_replace(' ','_',$lstrfilename)."_".$filename;
				  $file = realpath('.').$lstruploaddir . $filename;	
				  if (move_uploaded_file($_FILES['FileName']['tmp_name'],$file)) 
				 {
					
				 }
				  $data = new Spreadsheet_Excel_Reader(APPLICATION_PATH . '/../public/uploads/testingdata/'.$filename);			 			    
     		      $arr = $data->sheets;				  
				  $min = (array_keys($arr[0]['cells']));				  
				  $firstrowvalue = $min[1];				  
				  $totalcols = max(array_keys($arr[0]['cells'][1]));
				  $cnt = max(array_keys($arr[0]['cells']));
				  $maxfirstvalue = max(array_keys($arr[0]['cells'][1]));				  
                 
				if($cnt > 1)
				{
					for($i=$firstrowvalue;$i<=$cnt;$i++)
					{ 
						if($arr [0] ['cells'] [1] [1] != "Answerid")
						{
							 echo '<script language="javascript">alert("Incorrect excel format Please change First field  to Answerid")</script>';
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/answeruploads/index';</script>";
						}
						if($arr [0] ['cells'] [1] [2] != "Tamil Answer") 
						{
							 echo '<script language="javascript">alert("Incorrect excel format Please change Second field to Tamil Answer")</script>';
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/answeruploads/index';</script>";
						}
						if($arr [0] ['cells'] [1] [3] != "Chinese Answer")
						{
							 echo '<script language="javascript">alert("Incorrect excel format Please change Third field to Chinese Answer")</script>';
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/answeruploads/index';</script>";
						}
						$answerid = $arr[0]['cells'][$i][1];								    
						$tamilanswer = $arr[0]['cells'][$i][2];
						$chineseanswer = $arr[0]['cells'][$i][3];
                        if($answerid =='')
                        {
						     echo '<script language="javascript">alert("Answerid field is empty at row number '.$i.' correct it and upload again")</script>';						    
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/answeruploads/index';</script>";
                        }
                        if($tamilanswer =='')
                        {
						     echo '<script language="javascript">alert("Tamil Answer field is empty at row number '.$i.' correct it and upload again")</script>';						    
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/answeruploads/index';</script>";
                        }
                        if($chineseanswer =='')
                        {
						     echo '<script language="javascript">alert("Chinese Answer field is empty at row number '.$i.' correct it and upload again")</script>';						    
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/answeruploads/index';</script>";
                        }						
						if($answerid !='' && $tamilanswer !='' && $chineseanswer !='')
						{	
						    $tamilquestn = $tamilanswer;
							$chinesequestn = $chineseanswer;
							/*$key = 'guruprasad';
							$secure_tamil = (AES_ENCRYPT($tamilquestn,$key));
							print_R($secure_tamil);
							die();*/
							
                          /*   $secure_tamilquestn = echo trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $tamilquestn, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
							die();
                            
 
                            $unsecure_tamilquestn = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($secure_tamilquestn), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
                           
							
							$secure_chinesequestn = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $chinesequestn, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
                            						
 
                            $unsecure_chinesequestn = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($secure_chinesequestn), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
                           
							//die();
							*/					
						    
							$larrinfo = $this->lobjquestionsmodel->fnupdateanswers($answerid,$tamilquestn,$chinesequestn);
						}
					}
			 }
			}
					
		}
	}
}
}