<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_ChronepushrevertController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjChronepushrevertmodel = new Examination_Model_Chronepushrevert(); //intialize newscreen db object
		$this->lobjChronepushrevertForm = new Examination_Form_Chronepushrevert(); 
		$this->lobjCommonmodel = new GeneralSetup_Model_DbTable_Common();
	}
	
	public function indexAction() 
	{
		$larrvenueexamday = array();
		 $this->view->centerdetails = $larrvenueexamday;
			$this->view->lobjChronepushrevertForm = $this->lobjChronepushrevertForm;
			  if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			  $larrformdata = $this->_request->getPost();
			   $larresult = $this->lobjChronepushrevertmodel->fngetallvenues($larrformdata);
			   $this->view->centreresults = $larresult;
			   $this->view->lobjChronepushrevertForm->Date->setValue($larrformdata['Date']);
 
			  }
			  
			  
	 if ($this->_request->isPost () && $this->_request->getPost ( 'Approve' )) {
			     $larrformData = $this->_request->getPost ();
			    $larresults = $this->lobjChronepushrevertmodel->fnrevertbackvenue($larrformData);

			   }
			   

		
	}
	
	
	public function getsessionsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$larresultvenuedateschedule = $this->lobjChronepullmodel->fngetvenuedateschedule($iddate);
		$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larresultvenuedateschedule);
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
	}
	

	public function selectoptionAction()
	{
		$this->view->lobjChronepushrevertForm = $this->lobjChronepushrevertForm;
		if ($this->_request->isPost () && $this->_request->getPost ( 'go' )) 
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) 
			{
				if($larrformData['Changetype']==1)
					{	
			 			$this->_redirect( $this->baseUrl . '/examination/revertback/index');
					}
			    else 
					{
						 $this->_redirect( $this->baseUrl . '/examination/chronepushrevert/index');
					}
			}
		}
	}

	
}