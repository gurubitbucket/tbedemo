<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_IndividualpaymentchangeController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	public function fnsetObj()
	{	
		$this->lobjindividualpaymentchangemodel = new  Examination_Model_Individualpaymentchange(); //intialize newscreen db object
		$this->lobjindividualpaymentform = new Examination_Form_Individualpaymentchange(); 

	}
	
	public function indexAction() 
	{
		$this->view->lobjindividualpaymentform = $this->lobjindividualpaymentform;
	 	$larrresult=0;
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->studentelapsedexamdatepaginatorresult);
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$larrCourseresult = $this->lobjindividualpaymentchangemodel->fnGetCourseNames();	
		$this->lobjindividualpaymentform->Coursename->addMultiOptions($larrCourseresult);
		$larrVenuesresult = $this->lobjindividualpaymentchangemodel->fnGetVenueNames();	
		$this->lobjindividualpaymentform->Venues->addMultiOptions($larrVenuesresult);
		/*$larrTakafulresult = $this->lobjindividualpaymentchangemodel->fnGetTakafulNames();	
		$this->lobjnewscreenForm->Takafulname->addMultiOptions($larrTakafulresult);*/
		
		if(isset($this->gobjsessionsis->studentelapsedexamdatepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->studentelapsedexamdatepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$this->view->paramsearch =  $this->_getParam('search');
				/*echo "<pre>";
				print_r($larrformData);
				die();*/
				
					
					if($larrformData['Studentname'])
					{		
					$this->lobjindividualpaymentform->Studentname->setValue($larrformData['Studentname']);
					}
		
					if($larrformData['ICNO'])
					{	
					$this->lobjindividualpaymentform->ICNO->setValue($larrformData['ICNO']);
					}
					
					
					if($larrformData['Date'])
					{
					$this->lobjindividualpaymentform->Date->setValue($larrformData['Date']);
					}
			      
					if($larrformData['Coursename'])
					{
					$this->lobjindividualpaymentform->Coursename->setValue($larrformData['Coursename']);
					}
					if($larrformData['Venues'])
					{
					
					$this->lobjindividualpaymentform->Venues->setValue($larrformData['Venues']);
					}
			 		if($larrformData['modeofpayment'])
					{
					$this->lobjindividualpaymentform->modeofpayment->setValue($larrformData['modeofpayment']);
					}
					
					
				 $larrresult = $this->lobjindividualpaymentchangemodel->fnindividualpaymentSearchStudent($larrformData); //searching the values for the user
				/* echo "<pre/>";
				 print_R($larrresult);
				 die();*/
				$this->view->larrresult =$larrresult;
				 $this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->studentelapsedexamdatepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl.'/examination/individualpaymentchange/index');
		}
		
		
		
		
		
		
		
	}
	
	public function editstudentinfoAction() 
	{	
		
		$this->view->lobjindividualpaymentform = $this->lobjindividualpaymentform;
		$lintidstudent = $this->_getParam('id');
		$this->view->id = $lintidstudent;
		
		$larrstudentinforesult= $this->lobjindividualpaymentchangemodel->fngetstudenteachinformation($lintidstudent); 
		/*echo "<pre/>";
		print_R($larrstudentinforesult);
		die();*/
		
		$this->view->Fname = $larrstudentinforesult['FName'];
		$this->view->ICNO = $larrstudentinforesult['ICNO'];
		$this->view->EmailAddress = $larrstudentinforesult['EmailAddress'];
		$this->view->Program = $larrstudentinforesult['ProgramName'];
	    $this->view->Venue = $larrstudentinforesult['centername'];
	    $this->view->oldpayment = $larrstudentinforesult['ModeofPayment'];
	    $this->view->Idapplication = $lintidstudent;
	    $this->view->amount = $larrstudentinforesult['Amount'];
	    $email = $larrstudentinforesult['EmailAddress'];
	    switch ($larrstudentinforesult['ModeofPayment'])
	    {
	    	case 1: $paymenttype = 'FPX';
	    	        break;
	   	    case 2: $paymenttype = 'Credit Card';
	    	        break;
	    	case 5: $paymenttype = 'Money Order';
	    	        break;
	   	    case 6: $paymenttype = 'Postal Order';
	    	        break;
	    	case 7: $paymenttype = 'Credit/Bank to IBFIM account';
	    	        break;
	   	    case 10: $paymenttype = 'Credit-Card';
	    	        break;
	    }
	    $this->view->paymenttype = $paymenttype;
	    
	    $idbatch=$larrstudentinforesult['IdBatch'];
	    $takafuloperator=0;
	   if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			 $larrformdata = $this->_request->getPost();
		/*	print_R($larrformdata);
			die();*/
			 $auth = Zend_Auth::getInstance();
			 $iduser=$auth->getIdentity()->iduser;
			 $insertedtemp = $this->lobjindividualpaymentchangemodel->fninserttemp($larrformdata,$iduser);
			 
			 $updatequeryresult = $this->lobjindividualpaymentchangemodel->fnupdatestudentpayment($larrformdata);
			
			 if($larrformdata['paymentmode']=='5' ||$larrformdata['paymentmode']=='6' || $larrformdata['paymentmode']=='7')
			 {
 				
			 	$resultinserted = $this->lobjindividualpaymentchangemodel->fninsertedstudentdetails($larrformdata,$iduser);
			 
			 }
			 else if($larrformdata['paymentmode']=='2' ||$larrformdata['paymentmode']=='10')
			 {
 					$resultinserted = $this->lobjindividualpaymentchangemodel->fncreditcardpaymentdetails($larrformdata,$iduser);		 	
			 }
			 else if($larrformdata['paymentmode']=='1')
			 {
			 	
			 	$resultinserted = $this->lobjindividualpaymentchangemodel->fnfpxpaymentdetails($larrformdata,$iduser,$email);	
			 }
			 $larrestultregids = $this->lobjindividualpaymentchangemodel->fninsertregdetails($lintidstudent,$takafuloperator,$idbatch);
			 $larrregid = $this->lobjindividualpaymentchangemodel->fngetRegid($lintidstudent);
			 //
			 //
			 //
			 //
			 //
			 //
			 //
			 
		$studentapplicationarray = $this->lobjindividualpaymentchangemodel->fnviewstudentdetailssss($lintidstudent);	 
			 
	   $larrEmailTemplateDesc =  $this->lobjindividualpaymentchangemodel->fnGetEmailTemplateDescription("Student Application");
					
									
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$studentapplicationarray['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$studentapplicationarray['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$studentapplicationarray['ProgramName'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[venue]",$studentapplicationarray['centername'],$lstrEmailTemplateBody);
                                   		$lstrEmailTemplateBody = str_replace("[venue]",$studentapplicationarray['centername'].' '.$studentapplicationarray['addr1'].' '.$studentapplicationarray['addr2'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$studentapplicationarray['Examdate'].'-'.$studentapplicationarray['Exammonth'].'-'.$studentapplicationarray['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$studentapplicationarray['PermAddressDetails'].'-'.$studentapplicationarray['CorrAddress'],$lstrEmailTemplateBody);
										
										//$lstrEmailTemplateBody = str_replace("[Session]",$studentapplicationarray['managesessionname'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Session]",$studentapplicationarray['managesessionname'].'('.$studentapplicationarray['starttime'].'--'.$studentapplicationarray['endtime'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$studentapplicationarray['managesessionname'].'('.$studentapplicationarray['ampmstart'].'--'.$studentapplicationarray['ampmend'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$studentapplicationarray['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$studentapplicationarray["username"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$studentapplicationarray['password'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										

										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'tbe@ibfim.com';
										$sender = 'tbe';
										$receiver_email = $studentapplicationarray["EmailAddress"];
										$receiver = $studentapplicationarray['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										//$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
									$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Because of server problem mails cannot be send this time")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
			 
		$this->_redirect( $this->baseUrl . "/examination/individualpaymentchange/index");	 
		}
		
	
	}		
		
}
