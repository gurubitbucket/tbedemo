<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_SchedulerutilityController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjSchedulerutilitymodel = new Examination_Model_Schedulerutility(); //intialize newscreen db object
		$this->lobjClosetimeForm = new Examination_Form_Schedulerutility(); 
       // $lobjcommonmodel = GeneralSetup_Model_DbTable_Common();
	}
	
	public function indexAction() 
	{
	
		$lobjcommonmodel = new GeneralSetup_Model_DbTable_Common();
		$this->view->lobjClosetimeForm = $this->lobjClosetimeForm;
		$date = date('Y-m-d');
		$this->view->lobjClosetimeForm->Date->setValue($date);
		$this->view->flag=1;
		//$this->view->lobjClosetimeForm->Date->setValue(date('Y-m-d'));
	    if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			$larrresult = $this->lobjSchedulerutilitymodel->fnsearchCandidates($larrformData); //searching the values for the Candidates
		/*	echo "<pre>";
			print_r($larrresult);
			die();*/
			$this->view->studentresult = $larrresult;	
			$this->view->lobjClosetimeForm->Date->setValue($larrformData['Date']);

	    }

	if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/schedulerutility/index');
			//$this->_redirect($this->view->url(array('module'=>'registrations' ,'controller'=>'batchapproval', 'action'=>'index'),'default',true));
		}
		
	 if ($this->_request->isPost() && $this->_request->getPost ( 'Approve' )) {
			$larrformdata = $this->_request->getPost ();
			$larrresult = $this->lobjSchedulerutilitymodel->fnUpdateCandidates($larrformdata); 
		/*	echo "<pre>";
			print_r($larrformdata);
			die();*/
		}
		
	}
	
	


	
}