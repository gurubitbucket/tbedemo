<?php
error_reporting (E_ALL & ~E_NOTICE & ~E_DEPRECATED);
class Examination_QuestionuploadsController extends Base_Base 
{
	public function init() 
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->registry = Zend_Registry::getInstance();
   	    $this->locale = $this->registry->get('Zend_Locale');
   	    $this->lobjquestionsmodel = new Examination_Model_Questionuploads(); //intialize newscreen db object
		$this->lobjquestionsForm = new Examination_Form_Questionuploads(); 
	}
	public function indexAction() 
	{
	
		$this->view->lobjform = $this->lobjquestionsForm;
		if ($this->_request->isPost() && $this->_request->getPost('Upload'))
		{
			$larrformData = $this->_request->getPost();
			$lintfilecount['Count'] =0;
			$lstruploaddir = "/uploads/testingdata/";
			$larrformData['FileLocation'] = $lstruploaddir;			    
			 require_once 'Excel/excel_reader2.php';			
			 if ($this->lobjform->isValid($larrformData))
			 {
			    if($_FILES['FileName']['error'] != UPLOAD_ERR_NO_FILE)
				{
				   
				  $lintfilecount['Count']++; 				
				  $lstrfilename = pathinfo(basename($_FILES['FileName']['name']),PATHINFO_FILENAME);					
				  $lstrext = pathinfo(basename($_FILES['FileName']['name']),PATHINFO_EXTENSION);				 
				  $filename = $lintfilecount['Count'].".".date('YmdHis').".".$lstrext;
				  $filename = str_replace(' ','_',$lstrfilename)."_".$filename;
				  $file = realpath('.').$lstruploaddir . $filename;	
				  if (move_uploaded_file($_FILES['FileName']['tmp_name'],$file)) 
				 {
					
				 }
				  $data = new Spreadsheet_Excel_Reader(APPLICATION_PATH . '/../public/uploads/testingdata/'.$filename);			 			    
     		      $arr = $data->sheets;				  
				  $min = (array_keys($arr[0]['cells']));				  
				  $firstrowvalue = $min[1];				  
				  $totalcols = max(array_keys($arr[0]['cells'][1]));
				  $cnt = max(array_keys($arr[0]['cells']));
				  $maxfirstvalue = max(array_keys($arr[0]['cells'][1]));				  
                 
				if($cnt > 1)
				{
					for($i=$firstrowvalue;$i<=$cnt;$i++)
					{ 
						if($arr [0] ['cells'] [1] [1] != "Questionid")
						{
							 echo '<script language="javascript">alert("Incorrect excel format Please change First field  to Questionid")</script>';
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/questionuploads/index';</script>";
						}
						if($arr [0] ['cells'] [1] [2] != "Tamil") 
						{
							 echo '<script language="javascript">alert("Incorrect excel format Please change Second field to Tamil")</script>';
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/questionuploads/index';</script>";
						}
						if($arr [0] ['cells'] [1] [3] != "Chinese")
						{
							 echo '<script language="javascript">alert("Incorrect excel format Please change Third field to Chinese")</script>';
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/questionuploads/index';</script>";
						}
						$questid = $arr[0]['cells'][$i][1];								    
						$tamil = $arr[0]['cells'][$i][2];
						$chinese = $arr[0]['cells'][$i][3];
                        if($questid =='')
                        {
						     echo '<script language="javascript">alert("Questionid field is empty at row number '.$i.' correct it and upload again")</script>';						    
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/questionuploads/index';</script>";
                        }
                        if($tamil =='')
                        {
						     echo '<script language="javascript">alert("Tamil field is empty at row number '.$i.' correct it and upload again")</script>';						    
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/questionuploads/index';</script>";
                        }
                        if($chinese =='')
                        {
						     echo '<script language="javascript">alert("Chinese field is empty at row number '.$i.' correct it and upload again")</script>';						    
							 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/questionuploads/index';</script>";
                        }						
						if($questid !='' && $tamil !='' && $chinese !='')
						{
						    $tamilquestn = $tamil;
							$chinesequestn = $chinese;
                            $secure_tamilquestn = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $tamilquestn, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
                            
                            $unsecure_tamilquestn = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($secure_tamilquestn), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
                            							
							$secure_chinesequestn = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $chinesequestn, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
                           	
                            $unsecure_chinesequestn = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($secure_chinesequestn), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
                           
							//die();
							
							$larrinfo = $this->lobjquestionsmodel->fnupdatequestions($questid,$secure_tamilquestn,$secure_chinesequestn);
						}
					}
			 }
			}
					
		}
	}
}
}