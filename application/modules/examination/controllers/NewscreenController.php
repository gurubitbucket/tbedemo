<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_NewscreenController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	public function fnsetObj()
	{	
		$this->lobjnewscreenmodel = new Examination_Model_Newscreen(); //intialize newscreen db object
		$this->lobjnewscreenForm = new Examination_Form_Newscreen(); 
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object

	
	
	}
	
	public function indexAction() 
	{
		
		
		$this->view->lobjform = $this->lobjnewscreenForm;
		
	$larrresultmaximum = $this->lobjnewscreenmodel->fngetstudentinformationfromconfig(); 
	//	echo "<pre>";
		//print_r($larrresultmaximum['studedit']);
	//	die();
		
				$larrresult = $this->lobjnewscreenmodel->fngetstudentinformation($larrresultmaximum['studedit']); 
				$larrresult=0;
		//print_r($larrresult);die();
		
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->studentchangevenuepaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		
		//$larrStudentnameresult = $this->lobjnewscreenmodel->fnGetStudentNames($larrresultmaximum['studedit']);	
	//	$this->lobjnewscreenForm->Studentname->addMultiOptions($larrStudentnameresult);
		
		
		$larrCourseresult = $this->lobjnewscreenmodel->fnGetCourseNames();	
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrCourseresult);
		$larrVenuesresult = $this->lobjnewscreenmodel->fnGetVenueNames();	
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrVenuesresult);
		$larrTakafulresult = $this->lobjnewscreenmodel->fnGetTakafulNames();	
		$this->lobjnewscreenForm->Takafulname->addMultiOptions($larrTakafulresult);
		
		if(isset($this->gobjsessionsis->studentchangevenuepaginatorresult)) 
		{

			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->studentchangevenuepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$this->view->paramsearch =  $this->_getParam('search');
				/*echo "<pre>";
				print_r($larrformData);
				die();*/
				
					
					if($larrformData['Studentname'])
					{		
		//$larrStudentnameresult = $this->lobjnewscreenmodel->fnGetStudentNames($larrresultmaximum['studedit']);	
		//$this->lobjnewscreenForm->Studentname->addMultiOptions($larrStudentnameresult);
		$this->lobjnewscreenForm->Studentname->setValue($larrformData['Studentname']);
					}
		
			if($larrformData['ICNO'])
					{	
			$this->lobjnewscreenForm->ICNO->setValue($larrformData['ICNO']);
					}
					
					
				if($larrformData['Date'])
					{
					$this->lobjnewscreenForm->Date->setValue($larrformData['Date']);
					}
			        if($larrformData['Date2'])
					{
					$this->lobjnewscreenForm->Date2->setValue($larrformData['Date2']);
					}
					if($larrformData['Coursename'])
					{
					//$larrCourseresult = $this->lobjnewscreenmodel->fnGetCourseNames();	
		          //  $this->lobjnewscreenForm->Coursename->addMultiOptions($larrCourseresult);
					$this->lobjnewscreenForm->Coursename->setValue($larrformData['Coursename']);
					}
					if($larrformData['Venues'])
					{
					//$larrVenuesresult = $this->lobjnewscreenmodel->fnGetVenueNames();	
		          //  $this->lobjnewscreenForm->Venues->addMultiOptions($larrVenuesresult);
					$this->lobjnewscreenForm->Venues->setValue($larrformData['Venues']);
					}
			 if($larrformData['Applicationtype'])
					{
					$this->lobjnewscreenForm->Applicationtype->setValue($larrformData['Applicationtype']);
					}
					
					
				 $larrresult = $this->lobjnewscreenmodel->fnSearchStudent($larrformData,$larrresultmaximum['studedit']); //searching the values for the user
				 $this->view->larrresult =$larrresult;
				 $this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->studentchangevenuepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/examination/newscreen/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
		
		
		
		
		
		
	}
	
	public function editstudentinfoAction() 
	{	
		
		$this->view->lobjnewscreenForm = $this->lobjnewscreenForm;
		
		$lintidstudent = $this->_getParam('id');
		$this->view->id = $lintidstudent;
		//echo $lintidstudent;die();
	
		
	$larrresultstudenttype=$this->lobjnewscreenmodel->fngetstudenttype($lintidstudent);
		//print_r($larrresultstudenttype);die();
		
		
		$this->view->batch=$larrresultstudenttype['batchpayment'];
		if($larrresultstudenttype['batchpayment'] !=0)
		{
			//echo "abc";
			$larrstudentinforesult= $this->lobjnewscreenmodel->fngetstudenteachinformationforbatch($lintidstudent); 
		}
		else 
		{
		$larrstudentinforesult= $this->lobjnewscreenmodel->fngetstudenteachinformation($lintidstudent); 
		}
		
		//echo "<pre>";
		//print_r($larrstudentinforesult);
		$this->view->AppliedDate=$larrstudentinforesult['AppliedDate'];
		
		
	$larrvalidateicnos=$this->lobjnewscreenmodel->fngetstudeicnosinformation($larrstudentinforesult['IDApplication'],$larrstudentinforesult['ICNO'],$larrstudentinforesult['Program']); 

		if($larrvalidateicnos)
		{
			   echo '<script language="javascript">alert("This candidate again registered for this course")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/newscreen/index';</script>";
 		die();
		}
		
		
		
	
		
		
		if($larrstudentinforesult['pass']==1 || $larrstudentinforesult['pass']==2 || $larrstudentinforesult['Payment']==0)
		{
		 $this->_redirect( $this->baseUrl . '/examination/newscreen/index');	
		}
		
		
		$larrvalidateicnoscurrentdate=$this->lobjnewscreenmodel->fngetstudeicnoscurrentdatevalid($larrstudentinforesult['IDApplication']); 

		if($larrvalidateicnoscurrentdate)
		{
			   echo '<script language="javascript">alert("This candidate has exam  today so cannot be changed today")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/newscreen/index';</script>";
 		die();
		}
		
		
	$larrvalidateicnovenuetime=$this->lobjnewscreenmodel->fngetstudeicnoscurrentdatetime($larrstudentinforesult['IDApplication']); 

		if($larrvalidateicnovenuetime)
		{
		//	   echo '<script language="javascript">alert("This candidate taking the exam so cannot be changed today")</script>';
		//echo "<script>parent.location = '".$this->view->baseUrl()."/examination/newscreen/index';</script>";
 		//die();
		}
		
		
		
		$larrvalidateicnosstart=$this->lobjnewscreenmodel->fngetstudeicnoscurrentstartvalid($larrstudentinforesult['IDApplication']); 

		if($larrvalidateicnosstart)
		{
			   echo '<script language="javascript">alert("This candidate has aleardy taken the exam")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/newscreen/index';</script>";
 		die();
		}
		
		
	$todaydate=date('Y-m-d');
		//$todaydate='2013-01-01';
		//echo $todaydate;
	$expireddate=0;
	
	
	
		if($todaydate>$larrstudentinforesult['DateTime'])
		{
			
			$expireddate=1;
			
		if($larrstudentinforesult['pass']!=4)
          {
        echo '<script language="javascript">alert("You can change date only if the candidate status is absent. since examdate is over")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/newscreen/index';</script>";
 		die();
          }
		
		}
		
		
		
	
		
		
		
		$larrweekmonthss = $this->lobjnewscreenmodel->fngetdayofdatemonth($larrstudentinforesult['DateTime']);
		//print_r($larrweekday);die();
	    
	
		$this->view->monthid=$larrweekmonthss['month'];
		$this->view->exp=$expireddate;

		//echo "<pre>";
		//print_r($larrstudentinforesult);
		//die();
		$this->view->Fname=$larrstudentinforesult['FName'];
		$this->view->EmailAddress=$larrstudentinforesult['EmailAddress'];
		$this->view->ICNO=$larrstudentinforesult['ICNO'];
		$this->view->AppliedDate=$larrstudentinforesult['AppliedDate'];
		
		$larrcourse = $this->lobjnewscreenmodel->fnGetCourseNames();
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrcourse);
		$this->view->lobjnewscreenForm->Coursename->setValue($larrstudentinforesult['IdProgrammaster']);
		 
		$this->view->lobjnewscreenForm->Coursename1->setValue($larrstudentinforesult['IdProgrammaster']);
	
		$this->view->examdateDate=$larrstudentinforesult['DateTime'];
					
		$larrscheduler = $this->lobjnewscreenmodel->fnGetSchedulerDetails();
		$this->lobjnewscreenForm->schedulerename->addMultiOptions($larrscheduler);
		$this->view->lobjnewscreenForm->schedulerename->setValue($larrstudentinforesult['Year']);
		
			
		$larrstate = $this->lobjnewscreenmodel->fnGetStateName();
		$this->lobjnewscreenForm->examstate->addMultiOptions($larrstate);
		
		$this->view->lobjnewscreenForm->examstate->setValue($larrstudentinforesult['ExamState']);
		
		$larrcity = $this->lobjnewscreenmodel->fnGetCityName($larrstudentinforesult['ExamCity']);
		$this->lobjnewscreenForm->examcity->addMultiOptions($larrcity);
		$this->view->lobjnewscreenForm->examcity->setValue($larrstudentinforesult['ExamCity']);
			
			
		$larrvenue = $this->lobjnewscreenmodel->fnGetVenueName($larrstudentinforesult['ExamCity']);
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrvenue);
		$this->view->lobjnewscreenForm->Venues->setValue($larrstudentinforesult['Examvenue']);
			
		$larrschedulersession = $this->lobjnewscreenmodel->fnGetSchedulerSessionDetails($larrstudentinforesult['Examsession']);
		$this->lobjnewscreenForm->examsession->addMultiOptions($larrschedulersession);
		$this->view->lobjnewscreenForm->examsession->setValue($larrstudentinforesult['Examsession']);
			
		$larrscheduleryear = $this->lobjnewscreenmodel->fnGetSchedulerYearDetails($larrstudentinforesult['Year']);
		$examdate=$larrstudentinforesult['Examdate'].'-'.$larrstudentinforesult['Exammonth'].'-'.$larrscheduleryear['year'];
		$this->view->lobjnewscreenForm->Examdate->setValue($examdate);
			
		$this->view->lobjnewscreenForm->paymentmode->setValue($larrstudentinforesult['ModeofPayment']);
		$this->view->lobjnewscreenForm->Payment->setValue($larrstudentinforesult['Payment']);
		$this->view->lobjnewscreenForm->Coursename->setAttrib('readonly','true'); 
		//$this->view->lobjnewscreenForm->Coursename1->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->schedulerename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examstate->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examcity->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->Venues->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examsession->setAttrib('readonly','true');
												$this->view->lobjnewscreenForm->Examdate->setAttrib('readonly','true');
				                              $this->view->lobjnewscreenForm->paymentmode->setAttrib('readonly','true');
												$this->view->lobjnewscreenForm->Payment->setAttrib('readonly','true');
				
	
		//$auth = Zend_Auth::getInstance();
		//$this->view->lobjCompanypaymentForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		
	///////////////////////////////////////////program change////////////////////////////////////////////////////////////////////////////////
	
		
		
		
		$larrcourse = $this->lobjnewscreenmodel->fnGetCourseNames();
		$this->lobjnewscreenForm->Coursename1->addMultiOptions($larrcourse);
		//$this->view->lobjnewscreenForm->Coursename1->setAttrib('readonly','true');
			
		
	
			
			
				$iddate = $larrstudentinforesult['DateTime'];
		
		

			
			//echo $values;die();
			
	   	$larrweekday = $this->lobjnewscreenmodel->fngetdayofdate($iddate);
	        if($larrweekday['days']==1)
			{
				$larrweekday['days']= 7;
			}
			else 
			{
			$larrweekday['days']=$larrweekday['days']-1;	
			}
			
			
			$larrresultsprog=$this->lobjnewscreenmodel->fnGetProgAmount($larrstudentinforesult['IdProgrammaster']);
			$this->view->amount=$larrresultsprog['sum(abc.amount)'];
			
			
	
			$this->view->idprg=$larrstudentinforesult['IdProgrammaster'];
		$larrschedulerdays=$this->lobjnewscreenmodel->fnGetschdelerdates($iddate,$larrstudentinforesult['IdProgrammaster']);
	   	if($larrschedulerdays)
	   	{
	   		
	   		$this->view->oldday=$larrweekday['days'];
	   		
	   	
	   	$this->view->lobjnewscreenForm->Newprgdate->setValue($larrstudentinforesult['DateTime']);
		$this->lobjnewscreenForm->newprgexamvenue->addMultiOptions($larrschedulerdays);
	   	
		$this->view->lobjnewscreenForm->newprgexamvenue->setValue($larrstudentinforesult['Examvenue']);
		
			
			
			
			
			$larrschedulersessions=$this->lobjnewscreenmodel->fngetschedulerofsessionstudent($iddate,$larrstudentinforesult['IdProgrammaster']);
			

			$this->lobjnewscreenForm->newprgexamsession->addMultiOptions($larrschedulersessions);
	   	
			$this->view->lobjnewscreenForm->newprgexamsession->setValue($larrstudentinforesult['Examsession']);
			
		$venueselect = $this->lobjnewscreenmodel->fnGetvalidateRemainingseats($iddate,$larrstudentinforesult['Examvenue'],$larrstudentinforesult['Examsession']);
			$this->view->lobjnewscreenForm->newremainingseats->setValue($venueselect['rem']);
		
	  
		
	   	}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost();
			
			
				$auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
				
			
	
			$oldprgamount=$larrresultsprog['sum(abc.amount)'];
			
			$newprogamount=$larrresultsprog['sum(abc.amount)'];
			$programflag=0;
			
			$amountflag=0;
			
			$idactivebatch=$this->lobjnewscreenmodel->fngetactivebatchprg($larrstudentinforesult['IdProgrammaster']);
			$idbatch=$idactivebatch['IdBatch'];
			//$oldidprg=$larrstudentinforesult['IdProgrammaster'];
			if($larrstudentinforesult['IdProgrammaster']!=$larrformData['Coursename1'])
			{
				
				$larrresultsidprog=$this->lobjnewscreenmodel->fnGetProgAmount($larrformData['Coursename1']);
				$newprogamount=$larrresultsidprog['sum(abc.amount)'];
				
				$idactivebatch=$this->lobjnewscreenmodel->fngetactivebatchprg($larrformData['Coursename1']);
				$idbatch=$idactivebatch['IdBatch'];
			
				$programflag=1;
				
				if($oldprgamount==$newprogamount)
				{
					$amountflag=0;
				}
			if($oldprgamount>$newprogamount)
				{
					$amountflag=1;
				}
				if($oldprgamount<$newprogamount)
				{
					$amountflag=2;
				}
			}
			//print_r($larrformData);die();
			
				if(($larrformData['Newprgdate']==$larrstudentinforesult['DateTime'])&&($larrformData['newprgexamvenue']==$larrstudentinforesult['Examvenue']) &&($larrformData['newprgexamsession']==$larrstudentinforesult['Examsession']) &&($larrformData['Coursename1']==$larrstudentinforesult['IdProgrammaster']) )
				{
					//echo 'abc';die();
					$this->_redirect( $this->baseUrl . '/examination/newscreen/index');		
					die();
				}	
			
			$idsecheduler=$this->lobjnewscreenmodel->fnGetidschdeler($larrformData['Newprgdate'],$larrformData['newprgexamvenue'],$larrformData['newprgexamsession']);	
			
				
			if($idsecheduler)
			{
			$idsche=$idsecheduler['idnewscheduler']	;
		
			
			$larrformData['UpdDate']=date('Y-m-d H:i:s');
			$idapplication = $larrformData['IDApplication'];
			
			
			
			$studenteditresult = $this->lobjnewscreenmodel->fngetstudentoldinfo($idapplication);
			
	$validateallotedseats=$this->lobjnewscreenmodel->fngetstudentvalidatealloted($studenteditresult['DateTime'],$studenteditresult['Examvenue'],$studenteditresult['Examsession']);
			
		//	print_r($validateallotedseats);die();
			
		if($validateallotedseats['Allotedseats']>0)
		{   
			
			$studentseatdecrement=$this->lobjnewscreenmodel->fngetstudentdecrease($studenteditresult);
		}
			
			
			
			
			
			
			
			$change=$studenteditresult['Venue'];
			$venuechange=$change+1;
		
			$studenteditiinserlarr=$this->lobjnewscreenmodel->fngetstudentinsertinfo($studenteditresult,$iduser,$larrformData,$oldprgamount,$newprogamount,$programflag,$amountflag);
			//print_r($studentgetcityandstate);die();
			$studentgetcityandstate=$this->lobjnewscreenmodel->fnGetvenuecity($larrformData['newprgexamvenue']);
			$idstate=$studentgetcityandstate['state'];
			$idcity=$studentgetcityandstate['city'];
			//print_r($larrformData);die();
			$iddateexam=$larrformData['Newprgdate'];
			$larrdates=explode('-',$iddateexam);
			$year= $larrdates[0];
			//$month=$larrdates[1];
			//$date=$larrdates[2];
			
			
			$month=$larrdates[1];
			if($month=='01')
			$month=1;
			if($month=='02')
			$month=2;
			if($month=='03')
			$month=3;
			if($month=='04')
			$month=4;
			if($month=='05')
			$month=5;
			if($month=='06')
			$month=6;
			if($month=='07')
			$month=7;
			if($month=='08')
			$month=8;
			if($month=='09')
			$month=9;
			
			$date=$larrdates[2];
			if($date=='01')
			$date=1;
			if($date=='02')
			$date=2;
			if($date=='03')
			$date=3;
			if($date=='04')
			$date=4;
			if($date=='05')
			$date=5;
			if($date=='06')
			$date=6;
			if($date=='07')
			$date=7;
			if($date=='08')
			$date=8;
			if($date=='09')
			$date=9;
			
			
			
			$larrformData['newexamdates']=$date;
			$larrformData['newexammonth']=$month;
  			 	$this->lobjnewscreenmodel->fnUpdateStudentnewscreen($idapplication,$larrformData,$idsche,$iduser,$idstate,$idcity,$venuechange,$idbatch);
  		
  			 	   $studenteditresultss = $this->lobjnewscreenmodel->fngetstudentoldinfo($idapplication);
                $studentseatincrement=$this->lobjnewscreenmodel->fngetstudentincrease($studenteditresultss);
  			 	
  			 	
$this->_redirect( $this->baseUrl . '/examination/newscreen/index');		
die();




	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    						
					$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($idapplication);	
					$larrregid = $this->lobjstudentmodel->fngetRegid($idapplication);
					
					$Programdetails = $this->lobjstudentmodel->fngetprogramrate($larrregid['IdProgrammaster']);
$Fee = $Programdetails['Rate'];
$GST = ($Programdetails['Rate']*$Programdetails['ServiceTax'])/100;
$GST = number_format($GST, 2);			
				//	print_r($larrregid);die();
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Venue Change");
						
						//echo "<pre />";
						//print_r($larrresult);
						//die();
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
									//print_R($larrresult);die();
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										 $lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'].' '.$larrresult['addr2'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
										//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'].'('.$larrresult['ampmstart'].'--'.$larrresult['ampmend'].')',$lstrEmailTemplateBody);
	                                  if($larrresult['batchpayment']!=0)
										{
											$lstrEmailTemplateBody = str_replace("[Amount]",'NA',$lstrEmailTemplateBody);
											$lstrEmailTemplateBody = str_replace("[Fee]",'NA',$lstrEmailTemplateBody);
                                            $lstrEmailTemplateBody = str_replace("[GST]",'NA',$lstrEmailTemplateBody);

										}
										else 
										{
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Fee]",$Fee,$lstrEmailTemplateBody);
                                        $lstrEmailTemplateBody = str_replace("[GST]",$GST,$lstrEmailTemplateBody);

										}
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										//print_r($lstrEmailTemplateBody);
										//die();
									/*	$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
									//$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									//echo '<script language="javascript">alert("Because of server problem mails cannot be send this time")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
								
								 //$this->_redirect( $this->baseUrl . "/registration/index");
								if(mess){
									
								}
					
    			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    		




  			 		$this->_redirect( $this->baseUrl . '/examination/newscreen/index');	

			} 		
  			else 
			{
			echo '<script language="javascript">alert("There is no scheduler defined for this venue for this date");</script>';   
		//	echo "<script>parent.location = '".$this->view->baseUrl()."/examination/newscreen/index';</script>";
 		//die();
			
			}
			
  		//echo "<pre>";
  		//print_r($larrformData);
  		//die();
  		}
			
		
	}
	
 public function fngetyearAction()
 {
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjnewscreenmodel->fnGetYearlistforcourse($Program);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
 }
 
 
 



public function fngetvenuenamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('idcity');
		$Program = $this->_getParam('Program');
		$idseched = $this->_getParam('idsecheduler');

		$larrvenuetimeresults = $this->lobjnewscreenmodel->fnGetVenuelistforcourse($lintdate,$Program,$idseched);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}










public function applieddatevalidAction()
{
$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintday = $this->_getParam('day');//city
		$applieddate = $this->_getParam('applieddate');
		$lintmonth = $this->_getParam('month');
		$year = $this->_getParam('year');
		$days = $year.'-'.$lintmonth.'-'.$lintday;
		//echo $days;
		//echo "a".$applieddate;die();
		$larrresultdates=$this->lobjnewscreenmodel->newfnGetdatedifference($days,$applieddate);
		//print_r($larrresultdates);
		$larrresultvalidatedays=$this->lobjnewscreenmodel->fngetstudentinformationfromconfig();
		//print_r($larrresultvalidatedays);die();
		
	
	if($larrresultdates['nodays']<$larrresultvalidatedays['ClosingBatch'])
	{
		$daysvalidateflag=$larrresultvalidatedays['ClosingBatch'];
		
	}
	else 
	{
		$daysvalidateflag=0;
	}	
	echo $daysvalidateflag;
	
}



public function newfngetnewprogramamountAction()
{
		$this->_helper->layout->disableLayout();
		//$this->view->lobjstudentForm = $this->lobjnewscreenForm; //send the lobjuserForm object to the view
		$this->_helper->viewRenderer->setNoRender();

		$idprog = $this->_getParam('idprog');//city
	
		$larrresultprogamt=$this->lobjnewscreenmodel->fnGetProgAmount($idprog);
		
	
	
		$newamountflag=$larrresultprogamt['sum(abc.amount)'];
	
	echo $newamountflag;
	
}



public function newvalidatepresentdataAction()
{
$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$idprog = $this->_getParam('idprog');//city
		
		$iddate= $this->_getParam('presentdate');//city
	
			   	
	   	$larrresultsvalidateprog=$this->lobjnewscreenmodel->fnGetschdelerdates($iddate,$idprog);
	   	
	   	if($larrresultsvalidateprog)
	   	{
	   
	   		$flag=1;
	   		echo $flag;
	   	
	   	}
	   	else 
	   	{
	   		$flag=2;
	   		echo $flag;
	   		
	   	}
	
}



public function newvenuelistsnamesAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$idprg = $this->_getParam('idprog');
	
		
		
		
		
		
			
		$larrschedulerdays=$this->lobjnewscreenmodel->fnGetschdelerdates2($iddate,$idprg);
	   	
	   	if($larrschedulerdays)
		{
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdays);
		}
		else 
		{
		$larrschedulerdays="";
		}
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
		//echo $larrCountryStatesDetailss;
}


public function newsessionlistsnamesAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$iddate = $this->_getParam('iddate');
		$idprog = $this->_getParam('idprog');
		
		
			$larrschedulerdayssession=$this->lobjnewscreenmodel->fngetschedulerofsessionstudent($iddate,$idprog);
		
		if($larrschedulerdayssession)
		{
		$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdayssession);
		}
		else 
		{
		$larrCountrysessionDetailss='';	
		}
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
}




public function newgetremseatsdataAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$iddate = $this->_getParam('presentdate');
		$idvenue = $this->_getParam('presntvenue');
		$idsession = $this->_getParam('presntsession');
		
		
	$venueselect = $this->lobjnewscreenmodel->fnGetvalidateRemainingseats($iddate,$idvenue,$idsession);
		if($venueselect)
	echo $venueselect['rem'];
	else 
	echo "";
	
}





public function getnewdatedaydataAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$iddate = $this->_getParam('iddate');		
		$larrweekday = $this->lobjnewscreenmodel->fngetdayofdate($iddate);
	    if($larrweekday['days']==1)
		{
		$larrweekday['days']= 7;
		}
		else 
		{
		$larrweekday['days']=$larrweekday['days']-1;	
		}
	
	
		$dayid=$larrweekday['days'];
	
		echo $dayid;
	
}

public function newvalidateconfidentialAction()
{
	   	$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
$this->view->lobjstudentForm = $this->lobjnewscreenForm; //send the lobjuserForm object to the view
		
		$idusers = $this->_getParam('idusers');		
		//$idusers=md5($idusers);
		//echo $idusers;die();
		//$larrweekday = $this->lobjnewscreenmodel->fngetidusers($idusers);
		$this->view->usr=$idusers;
	    
}

public function newvalidateuserAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$idusers = $this->_getParam('idusers');		
		$idusers=md5($idusers);
		//echo $idusers;die();
		$larrweekday = $this->lobjnewscreenmodel->fncheckSuperUserPwd($idusers);
		//$this->view->usr=$idusers;
		if($larrweekday)
		{
			echo 1;die();
		}
		else 
		{
			echo 2;die();
		}
	    
}


public function newvalidatepresenticnoAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$idprog = $this->_getParam('idprog');	
		$icno = $this->_getParam('icno');		
		
		
		$larrvalidateicno = $this->lobjnewscreenmodel->fncheckicnivalidate($idprog,$icno);
   //print_r($larrvalidateicno[pass]);die();
		if($larrvalidateicno['pass']==1)
		{
			echo 1;die();
		}
		else if($larrvalidateicno['pass']==3 || $larrvalidateicno['pass']==4)
		{
			echo 3;die();
		}
		else 
		{
			echo 2;die();
		}
	    
}

	public function newgetvalidateactivecenterAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('presentdate');
		$idvenue = $this->_getParam('idvenue');
	
		$larrscheduleractivecenter=$this->lobjnewscreenmodel->fnGetschdelerdactivecenter($iddate,$idvenue);
	   	
	   	if($larrscheduleractivecenter)
		{
		echo 1;die();
		}
		else 
		{
		echo 2;die();
		}
		
	}
public function getnewdatedaydatemonthAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$iddate = $this->_getParam('presentdate');		
		$larrweekday = $this->lobjnewscreenmodel->fngetdayofdatemonth($iddate);
		//print_r($larrweekday);die();
	    
	
		$monthid=$larrweekday['month'];
	
		echo $monthid;die();
	
}


public function examchangerequestAction()
{
	   	$this->view->lobjform = $this->lobjnewscreenForm;
	   	
	   	
	   	
if ($this->_request->isPost () && $this->_request->getPost ( 'go' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
			
					
                                        if($larrformData['Changetype']==2)
					{	
			            $this->_redirect( $this->baseUrl . '/examination/exambulkdatechange/index');
					}
					else if($larrformData['Changetype']==1)
					{
						$this->_redirect( $this->baseUrl . '/examination/newscreen/index');
					}
					else if($larrformData['Changetype']==3)
					{
					    $this->_redirect( $this->baseUrl . '/examination/changeindividualpayment');
					}


					
				
			}
		}
	
}


	
}
