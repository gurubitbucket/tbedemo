<?php
ini_set('memory_limit', '-1');

error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_AutovenueController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjAutovenuemodel = new Examination_Model_Autovenue(); //intialize newscreen db object
		$this->lobjAutovenueForm = new Examination_Form_Autovenue(); 
		$this->lobjCommonmodel = new GeneralSetup_Model_DbTable_Common();
	}
	
	public function indexAction() 
	{
$auth = Zend_Auth::getInstance();
		$larrvenueexamday = array();
		$this->view->typeid = 0;
		 $this->view->centerdetails = $larrvenueexamday;
		 	$lobjchroneclientmodel = new Examination_Model_Chroneclient();
			$this->view->lobjAutovenueForm = $this->lobjAutovenueForm;
			  if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			     $larrformData = $this->_request->getPost ();
			  
			   $this->view->lobjAutovenueForm->Type->setValue($larrformData['Type']);
			   $this->view->lobjAutovenueForm->FromDate->setValue($larrformData['FromDate']);
			   $this->view->lobjAutovenueForm->ToDate->setValue($larrformData['ToDate']);
			     if($larrformData['Type']==1)
			     {
			     $this->view->typeid = $larrformData['Type'];
			     $iddate = $larrformData['Date'];
			     $this->view->lobjAutovenueForm->Date->setValue($iddate);
			     $larrvenueexamday = $this->lobjAutovenuemodel->fngetallvenuesexamdate($iddate);
			    
			     $this->view->centerdetails = $larrvenueexamday;
			     }
			     else if($larrformData['Type']==2)
			     {
			     	
			     	 $this->view->typeid = $larrformData['Type'];
			     	 $larrvenues = $this->lobjAutovenuemodel->fngetallvenues();
			     	
			         $this->view->centers = $larrvenues;
			     }
			  	else if($larrformData['Type']==3)
			     {
			     	 $this->view->typeid = $larrformData['Type'];
			     	 $larrvenuescheduler = $this->lobjAutovenuemodel->fngetallscheduler($larrformData);
			         $this->view->centersscheduler = $larrvenuescheduler;
			         $this->view->fromdate = $larrformData['FromDate'];
			         $this->view->todate = $larrformData['ToDate'];
			     }
				 else if($larrformData['Type']==6)
			     {
			     	
			     	 $this->view->typeid = $larrformData['Type'];
			     	 $larrvenues = $this->lobjAutovenuemodel->fngetallvenues();
			     	 $this->view->centers = $larrvenues;
			     }
				 else if($larrformData['Type']==7)
			     {
					  $this->view->typeid = $larrformData['Type'];
					  //$iddate = $larrformData['Date'];
					  //$this->view->lobjAutovenueForm->Date->setValue($iddate);
					  $larrvenue = $this->lobjAutovenuemodel->fngetallvenuesforemailtemplate();
					  //echo "<pre>";
					 // print_r($larrvenue);die();
					  $this->view->venuedetails = $larrvenue;
					  //echo "fine";die();
			     }
			  }
			  
			  
			   if ($this->_request->isPost () && $this->_request->getPost ( 'Approve' )) {
			     $larrformData = $this->_request->getPost ();
			     if($larrformData['Type']==1)
			     {
			     	
			     		  $larrreqhrs = explode('T',$larrformData['ScheduleStartTime']);
						    $larrformData['ScheduleStartTime'] = $larrreqhrs[1];
						    $larrformData['status'] = 0;
						    $larrformData['UpdDate']=date('Y-m-d H:i:s');
							$iduser=$auth->getIdentity()->iduser;
			    $larrformData['UpdUser']=$iduser;
			     			$larresultchronevenue = $this->lobjAutovenuemodel->fninsertintochronevenue($larrformData);
			     }
			     else if($larrformData['Type']==2)
			     {
			     		  $larrreqhrs = explode('T',$larrformData['ScheduleStartTime']);
							    $larrformData['ScheduleStartTime'] = $larrreqhrs[1];
							    $larrformData['status'] = 0;
							    $larrformData['UpdDate']=date('Y-m-d H:i:s');
							   	$iduser=$auth->getIdentity()->iduser;
			    $larrformData['UpdUser']=$iduser;
								$larrresultlastid = $this->lobjAutovenuemodel->fninsertintochronevenuequestions($larrformData);
			     			   //  $larresultchronevenuequesstions = $this->lobjAutovenuemodel->fninsertintochronevenuequestions($larrformData,$larrresultlastid);
			     }
			     else if($larrformData['Type']==3)
			     {
			     		  $larrreqhrs = explode('T',$larrformData['ScheduleStartTime']);
			    $larrformData['ScheduleStartTime'] = $larrreqhrs[1];
			    $larrformData['status'] = 0;
			    $larrformData['UpdDate']=date('Y-m-d H:i:s');
			    	$iduser=$auth->getIdentity()->iduser;
			    $larrformData['UpdUser']=$iduser;
				$larrresultlastid = $this->lobjAutovenuemodel->fninsertintochronescheduler($larrformData);
			     	// $larresultchronevenuescheduler = $this->lobjAutovenuemodel->fninsertintochronescheduler($larrformData,$larrresultlastid);
			     }
				 else if($larrformData['Type']==6)
			     {
					$larrreqhrs = explode('T',$larrformData['ScheduleStartTime']);
				    $larrformData['ScheduleStartTime'] = $larrreqhrs[1];
				    $larrformData['status'] = 0;
				    $larrformData['UpdDate']=date('Y-m-d H:i:s');
				   	$iduser=$auth->getIdentity()->iduser;
                    $larrformData['UpdUser']=$iduser;
					$larrresultlastid = $this->lobjAutovenuemodel->fninsertintochronevenueTOS($larrformData);
			     }
				else if($larrformData['Type']==7)
			    {
				  
			    $larrreqhrs = explode('T',$larrformData['ScheduleStartTime']);
			    $larrformData['ScheduleStartTime'] = $larrreqhrs[1];
			    $larrformData['status'] = 0;
			    $larrformData['UpdDate']=date('Y-m-d H:i:s');
			    //$larrformData['UpdUser']=1;
			    $iduser = 1;//$auth->getIdentity()->iduser;
			    $larrformData['UpdUser']=$iduser;
				//echo "<pre>";
				// print_r($larrformData);die();
				$larrresultlastid = $this->lobjAutovenuemodel->fninsertintochroneemailtemplate($larrformData);
			     	// $larresultchronevenuescheduler = $this->lobjAutovenuemodel->fninsertintochronescheduler($larrformData,$larrresultlastid);
			     }
	
			   }
				 
	
			   
			     if ($this->_request->isPost () && $this->_request->getPost ( 'updatenow' )) {
			     $larrformData = $this->_request->getPost ();
			     /*print_r($larrformData);
			     die();*/
			     $larresult = $lobjchroneclientmodel->fnupdatevenuestudentsonceagain($larrformData);
			     }
		
	}
	
	
	public function getsessionsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$larresultvenuedateschedule = $this->lobjAutovenuemodel->fngetvenuedateschedule($iddate);
		
		if(count($larresultvenuedateschedule)<1)
		{
			$flag=0;
		}
		else 
		{
			
		$larrersultsscheduledatess = $this->lobjAutovenuemodel->fngetscheduledvenue($iddate);
			if(count($larrersultsscheduledatess)<1)
			{
				$flag=1;
			}
			else 
			{
				$flag=2;
			}
		}
		echo $flag;
	}
	
	
	public function chronejobsAction()
	{
		$date=date('Y-m-d');
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjchroneclientmodel = new Examination_Model_Chroneclient();
		
		//function to fetch all students from server model
		$larrchronevenues = $this->lobjAutovenuemodel->fngetvenuesfordate($date);
		$larrchronevenuedetails = $lobjchroneclientmodel->fninsertclientmodel($larrchronevenues);
		
		//function for the questions
		//$larrmainquestions = $this->lobjAutovenuemodel->fngetquestions();
		//$larrmainanswers = $this->lobjAutovenuemodel->fngetanswers();
		
		//$larrresult =$this->lobjAutovenuemodel->fngetvenusforquestions($date);
		//$larrchronevenuedetails = $lobjchroneclientmodel->fninsertclientquestions($larrresult,$larrmainquestions,$larrmainanswers,$date);
		
		//$larrresultscheduler =$this->lobjAutovenuemodel->fngetvenusforscheduler($date);
		
		
	//$larrchronevenuescheduler = $lobjchroneclientmodel->fninsertclientscheduler($larrresultscheduler);
		
		
	}
	
	public function chronejobsdetailsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	 	$Currentdate = date('Y-m-d H:i'); 
		$lobjchroneclientmodel = new Examination_Model_Chroneclient();
		$larr = explode(' ',$Currentdate);
		$shcdate = $larr[0];
		$shcdtime ='14:30:00';//$larr[1].':00';
		$larrresultidautomails = $this->lobjAutovenuemodel->fngetscheduledetails($shcdate,$shcdtime);
		$totalcountofidautomails = count($larrresultidautomails);
	
		if($totalcountofidautomails>0)
		{
			$idautomail = 0;
			for($i=0;$i<count($larrresultidautomails);$i++)
			{
				$idautomail = $idautomail.','.$larrresultidautomails[$i]['idautomail'];
			}
			print_R($idautomail);
			
			////for questions updating functions/////////////////////////
			$larresultquestionsid = $this->lobjAutovenuemodel->fngetallvenuesforidautomail($idautomail);
			
			$venuequestioncount = count($larresultquestionsid);
			//print_R($venuequestioncount);			
			if($venuequestioncount>0)
			{
				
				$larrmainquestions = $this->lobjAutovenuemodel->fngetquestions();
				$larrmainanswers = $this->lobjAutovenuemodel->fngetanswers();
				   for($j=0;$j<$venuequestioncount;$j++)
				   {
				   	
				   	$larresultidautovenueslist = $this->lobjAutovenuemodel->fngetvenuesforidautomail($larresultquestionsid[$j]['idautomail']);
				  	$larrchronevenuedetails = $lobjchroneclientmodel->fninsertclientquestions($larresultidautovenueslist,$larrmainquestions,$larrmainanswers);
				   }
			}
			
			/////////////////end of questions functioning/////////////////////
			
			
			////////////////start of the scheduler functioning/////////////////
			$larrresultidschedulers = $this->lobjAutovenuemodel->fngetallschedulervenuesforidautomail($idautomail);
			$venueschedulercount = count($larrresultidschedulers);
			if($venueschedulercount>0)
			{
				 for($s=0;$s<$venueschedulercount;$s++)
				   {
				   
					$larresultschedulervenuelist = $this->lobjAutovenuemodel->fngetscheduleridautomail($larrresultidschedulers[$s]['idautomail']);
				/*	print_R($larresultschedulervenuelist);
			die();*/
					$larrchronevenuescheduler = $lobjchroneclientmodel->fninsertclientscheduler($larresultschedulervenuelist);
					
				   }
			}
			/////////////end of the scheduler scheduling//////////////////////////
			
			$larrresultidcandidatesscheduler = $this->lobjAutovenuemodel->fngetallcandidatesvenuesforidautomail($idautomail);
			//print_r($larrresultidcandidatesscheduler);die();
			$venueschedulercandidatescount = count($larrresultidcandidatesscheduler);
			if($venueschedulercandidatescount>0)
			{
				for($c=0;$c<$venueschedulercandidatescount;$c++)
				   {
				     	$larresultschedulerstudentslist = $this->lobjAutovenuemodel->fngetschedulerstudentsvenuesidautomail($larrresultidcandidatesscheduler[$c]['idautomail']);
				     	$larrchronevenuedetails = $lobjchroneclientmodel->fninsertclientmodel($larresultschedulerstudentslist);
				     	//print_R($larrch)
				   }
			}
			
			
		}
		
	}
public function chronechangerequestAction()
{
	   	$this->view->lobjform = $this->lobjAutovenueForm;	    	
        if ($this->_request->isPost () && $this->_request->getPost ( 'go' )) 
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) 
			{
			
			        if($larrformData['Changetype']==1)
					{	
			            $this->_redirect( $this->baseUrl . '/examination/autovenue/index');
					}
					else if($larrformData['Changetype']==2)
					{
						$this->_redirect( $this->baseUrl . '/examination/chronepull/index');
					}
					else if($larrformData['Changetype']==3)
					{
					    $this->_redirect( $this->baseUrl . '/examination/chronepullissues/index');
					}
					
					
				
			}
		}
	
}
	
}