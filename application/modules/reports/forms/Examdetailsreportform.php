<?php
class Reports_Form_Examdetailsreportform extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ( 'Zend_Translate' );
		
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$session = new Zend_Dojo_Form_Element_FilteringSelect ( 'session' );
		$session->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$session->removeDecorator ( "DtDdWrapper" );
		$session->setAttrib ( 'onChange', "programlists(this.value);" );
		$session->removeDecorator ( "Label" );
		$session->removeDecorator ( 'HtmlTag' );
		$session->setAttrib ( 'required', "true" );
		
		$Date9 = new Zend_Dojo_Form_Element_DateTextBox ( 'Date9' );
		$Date9->setAttrib ( 'dojoType', "dijit.form.DateTextBox" )
		      ->setAttrib ( 'title', "dd-mm-yyyy" )
		      ->setAttrib ( 'constraints', "{datePattern:'dd-MM-yyyy'}" )
		      ->setAttrib ( 'constraints', "$dateofbirth" )
		      ->setAttrib ( 'onChange', "newvenuelists();")
		      ->setAttrib ( 'required', "true" )
		      ->removeDecorator ( "Label" )
		      ->removeDecorator ( "DtDdWrapper" )
		      ->removeDecorator ( 'HtmlTag' );
		
		$Venue = new Zend_Dojo_Form_Element_FilteringSelect ( 'Venue' );
		$Venue->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Venue->setAttrib ( 'required', "true" );
		$Venue->removeDecorator ( "DtDdWrapper" );
		$Venue->setAttrib ( 'onChange', "newsessionlists();" );
		$Venue->removeDecorator ( "Label" );
		$Venue->removeDecorator ( 'HtmlTag' );
		
		$Course = new Zend_Dojo_Form_Element_FilteringSelect ( 'Course' );
		$Course->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Course->setAttrib ( 'required', "true" );
		$Course->removeDecorator ( "DtDdWrapper" );
		$Course->removeDecorator ( "Label" );
		$Course->removeDecorator ( 'HtmlTag' );
		
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
		
		$this->addElements ( array ($session, 
		                            $Date9, 
		                            $Course,
		                            $Venue,
		                            $submit ) );
	}
}
