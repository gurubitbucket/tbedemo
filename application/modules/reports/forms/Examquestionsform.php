<?php
class Reports_Form_Examquestionsform extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ( 'Zend_Translate' );
		
		$Marksfrom = new Zend_Dojo_Form_Element_FilteringSelect ('Marksfrom');
		$Marksfrom->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Marksfrom->removeDecorator ("DtDdWrapper");
		$Marksfrom->removeDecorator("Label");
		$Marksfrom->removeDecorator('HtmlTag');
		$Marksfrom->setAttrib('required',"true");
		
		$Marksto = new Zend_Dojo_Form_Element_FilteringSelect ('Marksto');
		$Marksto->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Marksto->setAttrib ('onChange',"fncheckvalue();");
		$Marksto->removeDecorator ("DtDdWrapper");
		$Marksto->removeDecorator("Label");
		$Marksto->removeDecorator('HtmlTag');
		$Marksto->setAttrib ('required',"true");
		
		
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Print");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
		
		$this->addElements ( array ($Marksfrom,
		                            $Marksto,
		                            $submit)
		                   );
	}
}
