<?php
class Reports_Form_Allreportsform extends Zend_Dojo_Form { //Formclass for the report module
    public function init() 
     {    	       
     	$gstrtranslate = Zend_Registry::get ( 'Zend_Translate' );
		
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		//Attendancelist
		$Date3 = new Zend_Dojo_Form_Element_DateTextBox('Date3');
	    $Date3->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Date3->setAttrib('title',"dd-mm-yyyy");
		$Date3->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Date3->setAttrib('onChange', "examtoDateSetting();");
		$Date3->setAttrib('constraints', "$dateofbirth");
		$Date3->setAttrib('required',"true");		
		$Date3->removeDecorator("Label");
		$Date3->removeDecorator("DtDdWrapper");
		$Date3->removeDecorator('HtmlTag');
		
		$Date4 = new Zend_Dojo_Form_Element_DateTextBox('Date4');
	    $Date4->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Date4->setAttrib('title',"dd-mm-yyyy");
		$Date4->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Date4->setAttrib('onChange', "fngetexamcentre();");
		$Date4->setAttrib('required',"true");	
		$Date4->removeDecorator("Label");
		$Date4->removeDecorator("DtDdWrapper");
		$Date4->removeDecorator('HtmlTag');				
	    
		$Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
	    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Venues->setRegisterInArrayValidator(false);
	    $Venues->setAttrib ( 'required', "false" );
	    $Venues->addMultiOption('','Select');  
	    $Venues->removeDecorator("DtDdWrapper");
	    $Venues->removeDecorator("Label");
	    $Venues->removeDecorator('HtmlTag');
		
		$Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
		$Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Coursename->addMultiOption('','Select'); 	           	         		       		     
	    $Coursename->removeDecorator("DtDdWrapper");
	    $Coursename->removeDecorator("Label");
	    $Coursename->removeDecorator('HtmlTag');  
		// Examdetailsreport New
	    $session = new Zend_Dojo_Form_Element_FilteringSelect('session');
		$session->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$session->removeDecorator ("DtDdWrapper");
		$session->setAttrib ('onChange',"programlists(this.value);");
		$session->removeDecorator ("Label");
		$session->removeDecorator ('HtmlTag');
		$session->setAttrib ('required', "true");
		
		$Date9 = new Zend_Dojo_Form_Element_DateTextBox('Date9');
		$Date9->setAttrib('dojoType', "dijit.form.DateTextBox");
		$Date9->setAttrib('title', "dd-mm-yyyy");
		$Date9->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}");
		$Date9->setAttrib('constraints',"$dateofbirth");
		$Date9->setAttrib('onChange',"newvenuelists();");
		$Date9->setAttrib('required',"true");
		$Date9->removeDecorator("Label");
		$Date9->removeDecorator("DtDdWrapper");
		$Date9->removeDecorator('HtmlTag');
		
		$Venue = new Zend_Dojo_Form_Element_FilteringSelect ( 'Venue' );
		$Venue->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Venue->setAttrib ( 'required', "true" );
		$Venue->removeDecorator ( "DtDdWrapper" );
		$Venue->setAttrib ( 'onChange', "newsessionlists();" );
		$Venue->removeDecorator ( "Label" );
		$Venue->removeDecorator ( 'HtmlTag' );
		
		$Course = new Zend_Dojo_Form_Element_FilteringSelect ( 'Course' );
		$Course->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Course->setAttrib ( 'required', "true" );
		$Course->removeDecorator ( "DtDdWrapper" );
		$Course->removeDecorator ( "Label" );
		$Course->removeDecorator ( 'HtmlTag' );
	    //Batchregreport
	    $FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
		$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$FromDate->setAttrib('title',"dd-mm-yyyy");
	  	$FromDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$FromDate->setAttrib('required',"true");
		$FromDate->removeDecorator("Label");
		$FromDate->removeDecorator("DtDdWrapper");
		$FromDate->removeDecorator('HtmlTag');
		
		$ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
		$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$ToDate->setAttrib('title',"dd-mm-yyyy");
	  	$ToDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$ToDate->setAttrib('required',"true");
		$ToDate->removeDecorator("Label");
		$ToDate->removeDecorator("DtDdWrapper");
		$ToDate->removeDecorator('HtmlTag');
		
		$Company = new Zend_Dojo_Form_Element_FilteringSelect('Company');
        $Company->removeDecorator("DtDdWrapper");
        $Company->removeDecorator("Label");
        $Company->removeDecorator('HtmlTag');
		$Company->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    //Datereportform
	    $Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	    $Date2->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Date2->setAttrib('title',"dd-mm-yyyy");
		$Date2->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Date2->setAttrib('onChange', "fngetexamcentre();");
		$Date2->removeDecorator("Label");
		$Date2->removeDecorator("DtDdWrapper");
		$Date2->removeDecorator('HtmlTag');
		
		$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	    $Date->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Date->setAttrib('title',"dd-mm-yyyy");
		$Date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Date->removeDecorator("Label");
		$Date->removeDecorator("DtDdWrapper");
		$Date->removeDecorator('HtmlTag');

		$Studentname = new Zend_Form_Element_Text('Studentname');
	    $Studentname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Studentname->setAttrib('class','txt_put');  
	    $Studentname->removeDecorator("DtDdWrapper");
	    $Studentname->removeDecorator("Label");
	    $Studentname->removeDecorator('HtmlTag');
	    
	    $ICNO = new Zend_Form_Element_Text('ICNO');
	    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $ICNO->setAttrib('class','txt_put');  
	    $ICNO->removeDecorator("DtDdWrapper");
	    $ICNO->removeDecorator("Label");
	    $ICNO->removeDecorator('HtmlTag');
	    //Examinationpartwise
	    $Date7 = new Zend_Dojo_Form_Element_DateTextBox('Date7');
	    $Date7->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Date7->setAttrib('title',"dd-mm-yyyy");
		$Date7->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Date7->setAttrib('onChange', "examtoDateSetting();");
		$Date7->removeDecorator("Label");
		$Date7->removeDecorator("DtDdWrapper");
		$Date7->removeDecorator('HtmlTag');
		
		$Date8 = new Zend_Dojo_Form_Element_DateTextBox('Date8');
	    $Date8->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Date8->setAttrib('title',"dd-mm-yyyy");
		$Date8->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Date8->setAttrib('onChange', "fngetexamcentre();");
		$Date8->removeDecorator("Label");
		$Date8->removeDecorator("DtDdWrapper");
		$Date8->removeDecorator('HtmlTag');

		$Canditatename = new Zend_Dojo_Form_Element_FilteringSelect('Canditatename');
	    $Canditatename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Canditatename->removeDecorator("DtDdWrapper");
	    $Canditatename->removeDecorator("Label");
	    $Canditatename->removeDecorator('HtmlTag');
	    $Canditatename->addMultiOption('','Select');
	    //Pullpushdatareport
	    $ExamDate = new Zend_Dojo_Form_Element_DateTextBox('ExamDate');
		$ExamDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$ExamDate->setAttrib('title',"dd-mm-yyyy");
	  	$ExamDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	  	$ExamDate->setAttrib('constraints', "$dateofbirth");
		$ExamDate->setAttrib('required',"true");
		$ExamDate->removeDecorator("Label");
		$ExamDate->removeDecorator("DtDdWrapper");
		$ExamDate->removeDecorator('HtmlTag');		
		$ExamDate->setAttrib('onChange', "getcentres();");
		
		$Centre = new Zend_Dojo_Form_Element_FilteringSelect('Centre');
        $Centre->removeDecorator("DtDdWrapper");
        $Centre->removeDecorator("Label");
        $Centre->removeDecorator('HtmlTag');
        $Centre->setAttrib('required',"false");
		$Centre->setAttrib('dojoType',"dijit.form.FilteringSelect");
		//Statisticsnew
		$Type = new Zend_Dojo_Form_Element_FilteringSelect('Type');
	    $Type->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Type->addMultiOptions(array('8'=>'All','1'=>'Age Wise','2'=>'Exam Category Wise','3'=>'Exam Centre Wise','4'=>'Exam Result Wise','5'=>'Gender Wise','6'=>'Race Wise','7'=>'Qualification Wise')); 	           	         		       		     
	    $Type->removeDecorator("DtDdWrapper");
	    $Type->removeDecorator("Label");
	    $Type->removeDecorator('HtmlTag');
	    
	    $Month = new Zend_Dojo_Form_Element_FilteringSelect('Month');
	    $Month->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    //$Month->addMultiOptions(array('1'=>'January','2'=>'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December')); 	           	         		       		     
	    $Month->removeDecorator("DtDdWrapper");
	    $Month->removeDecorator("Label");
	    $Month->removeDecorator('HtmlTag');
	    
	    $Year = new Zend_Dojo_Form_Element_FilteringSelect('Year');
	    $Year->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Year->removeDecorator("DtDdWrapper");
	    $Year->removeDecorator("Label");
	    $Year->removeDecorator('HtmlTag');
	    
	    $Statistics = new Zend_Dojo_Form_Element_FilteringSelect('Statistics');
		$Statistics->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$Statistics->addMultiOptions(array('1'=>'Age Wise','2'=>'Exam Category Wise','3'=>'Exam Centre Wise','4'=>'Exam Result Wise','5'=>'Gender Wise','6'=>'Race Wise','7'=>'Qualification Wise'));
		$Statistics->setAttrib('required',"true");
		$Statistics->removeDecorator("DtDdWrapper");
		$Statistics->removeDecorator("Label");
		$Statistics->removeDecorator('HtmlTag');
		// Studentapplication new
		$Dates = new Zend_Dojo_Form_Element_DateTextBox('Dates');
	    $Dates->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Dates->setAttrib('title',"dd-mm-yyyy");
	    $Dates->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	    $Dates->setAttrib('onChange', "examtoDateSetting();");
	    //$Dates->setAttrib('required',"true");
	    $Dates->removeDecorator("Label");
		$Dates->removeDecorator("DtDdWrapper");
		$Dates->removeDecorator('HtmlTag');
	       	 	
	    $Dates2 = new Zend_Dojo_Form_Element_DateTextBox('Dates2');
	    $Dates2->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Dates2->setAttrib('title',"dd-mm-yyyy");
		$Dates2->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		//$Dates2->setAttrib('required',"true");
		$Dates2->removeDecorator("Label");
		$Dates2->removeDecorator("DtDdWrapper");
		$Dates2->removeDecorator('HtmlTag');
	    
	    $UpDate1 = new Zend_Dojo_Form_Element_DateTextBox('UpDate1');
	    $UpDate1->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $UpDate1->setAttrib('title',"dd-mm-yyyy");
		$UpDate1->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$UpDate1->setAttrib('onChange', "examtoDateSetting1();");	
		$UpDate1->removeDecorator("Label");
		$UpDate1->removeDecorator("DtDdWrapper");
		$UpDate1->removeDecorator('HtmlTag');
	       	 	
	    $UpDate2 = new Zend_Dojo_Form_Element_DateTextBox('UpDate2');
	    $UpDate2->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $UpDate2->setAttrib('title',"dd-mm-yyyy");
		$UpDate2->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");	
		$UpDate2->removeDecorator("Label");
		$UpDate2->removeDecorator("DtDdWrapper");
		$UpDate2->removeDecorator('HtmlTag');
		
		$Takafulname = new Zend_Dojo_Form_Element_FilteringSelect('Takafulname');
		$Takafulname->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Takafulname->addMultiOption('','Select'); 	           	         		       		     
	    $Takafulname->removeDecorator("DtDdWrapper");
	    $Takafulname->removeDecorator("Label");
	    $Takafulname->removeDecorator('HtmlTag');   
	            
	    $Payment = new Zend_Dojo_Form_Element_FilteringSelect('Payment');
		$Payment->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Payment->addMultiOption('','Select');
	    $Payment->addMultiOptions(array(
									'1' => 'Paid',
									'2' => 'Pending'));
	    $Payment->removeDecorator("DtDdWrapper");
	    $Payment->removeDecorator("Label");
	    $Payment->removeDecorator('HtmlTag');

	    $paymentmode = new Zend_Dojo_Form_Element_FilteringSelect('paymentmode');
	    $paymentmode->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $paymentmode->addMultiOption('','Select'); 
	    $paymentmode->addMultiOptions(array(
									'1' => 'FPX',
									'2' => 'Credit Card',
	            					'5' => 'Money Order',
	            					'6' => 'Postal Order',
	            					'7' => 'Credit/Bank to IBFIM account',
									'10' => 'Credit_Card(MIGS)')); 	           	         		       		     
	    $paymentmode->removeDecorator("DtDdWrapper");
	    $paymentmode->removeDecorator("Label");
	    $paymentmode->removeDecorator('HtmlTag');        
	            
	    $approval = new Zend_Dojo_Form_Element_FilteringSelect('approval');
		$approval->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $approval->addMultiOption('','Select'); 
	    $approval->addMultiOptions(array(
									'1' => 'Approved',
									'2' => 'Not Approved')); 	           	         		       		     
	    $approval->removeDecorator("DtDdWrapper");
	    $approval->removeDecorator("Label");
	    $approval->removeDecorator('HtmlTag');
	          
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag');
        $submit->class = "NormalBtn";
        
        $Clear = new Zend_Form_Element_Submit('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
        $Clear->removeDecorator("DtDdWrapper");
        $Clear->removeDecorator("Label");
        $Clear->removeDecorator('HtmlTag');
        $Clear->class = "NormalBtn";
		
        $excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag');
		
		$PDF = new Zend_Form_Element_Submit('PrintPDF');
		$PDF->dojotype="dijit.form.Button";
		$PDF->label = $gstrtranslate->_("Print PDF");
		$PDF->removeDecorator("DtDdWrapper");
		$PDF->removeDecorator("Label");
		$PDF->removeDecorator('HtmlTag');

$Army = new Zend_Form_Element_Text('Army',array());
		$Army	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','12')
						->setAttrib('class','txt_put')						 
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
		
		$this->addElements ( array ($Date4,$Date3,$Date9,$FromDate,$ToDate,$Date2,$Date,$Date7,$Date8,$ExamDate,$Dates,$Dates2,$UpDate1,$UpDate2, 
		                            $Coursename,$Course,$Company,$Studentname,$ICNO,$Canditatename,$Takafulname,$Payment,
		                            $Venues,$Venue,$Centre,$Type,$Month,$Year,$Statistics,$paymentmode,$approval,$session,
		                            $submit,$excel,$PDF,$Clear,$Army ));
	
     }
}