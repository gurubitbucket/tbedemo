<?php
class Reports_Form_Examquestions extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ( 'Zend_Translate' );
		
		$Marksfrom = new Zend_Dojo_Form_Element_FilteringSelect ('Marksfrom');
		$Marksfrom->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Marksfrom->removeDecorator ("DtDdWrapper");
		$Marksfrom->setAttrib ('onChange',"fncheckvalue();");
		$Marksfrom->addMultiOptions(array(''=>'Starting'));
		$Marksfrom->removeDecorator("Label");
		$Marksfrom->removeDecorator('HtmlTag');
		$Marksfrom->setAttrib('required',"true");
		
		$Marksto = new Zend_Dojo_Form_Element_FilteringSelect ('Marksto');
		$Marksto->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Marksto->setAttrib ('onChange',"fncheckvalue();");
		$Marksto->addMultiOptions(array(''=>'Ending'));
		$Marksto->removeDecorator ("DtDdWrapper");
		$Marksto->removeDecorator("Label");
		$Marksto->removeDecorator('HtmlTag');
		$Marksto->setAttrib ('required',"true");
		
		$Questiongroup = new Zend_Dojo_Form_Element_FilteringSelect ('Questiongroup');
		$Questiongroup->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Questiongroup->addMultiOptions(array(''=>'Select'));
		$Questiongroup->removeDecorator ("DtDdWrapper");
		$Questiongroup->removeDecorator("Label");
		$Questiongroup->removeDecorator('HtmlTag');
		$Questiongroup->setAttrib ('required',"true");
		
		$Questionstatus = new Zend_Dojo_Form_Element_FilteringSelect ('Questionstatus');
		$Questionstatus->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Questionstatus->addMultiOptions(array(''=>'Select','1'=>'Active','0'=>'InActive'));
		$Questionstatus->removeDecorator ("DtDdWrapper");
		$Questionstatus->removeDecorator("Label");
		$Questionstatus->removeDecorator('HtmlTag');
		$Questionstatus->setAttrib ('required',"true");
		
		
		$Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
		
		$submit = new Zend_Form_Element_Submit('submit');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Print");
        $submit->removeDecorator("DtDdWrapper");
        $submit->setAttrib ('onClick',"fnloadLoader();");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
		
		$this->addElements ( array ($Marksfrom,
		                            $Marksto,
		                            $Questiongroup,
		                            $Questionstatus,
		                            $Search,
		                            $submit)
		                   );
	}
}
