<?php
	class Reports_Form_Report extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
	    $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
			$UserNameList = new Zend_Dojo_Form_Element_FilteringSelect('UserNameList');
			$UserNameList->setAttrib('ComboBoxOnChange','')	
							->addMultiOptions(array());
			$UserNameList->removeDecorator("DtDdWrapper");
			$UserNameList->removeDecorator("Label");
			$UserNameList->removeDecorator('HtmlTag');
			$UserNameList->setRegisterInArrayValidator(false);
			$UserNameList->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    
	  /*$month3= 03; // Month value
		$day3=  21; //today's date
		$year3=2012; // Year value*/
		
		
		$yesterdaydate1= date('Y-m-d', mktime(0,0,0,$month,($day+14),$year));
		$dateofbirth1 = "{min:'$yesterdaydate1',datePattern:'dd-MM-yyyy'}"; 
		
				$Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
			    $Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Coursename->addMultiOption('','Select'); 	           	         		       		     
	            $Coursename->removeDecorator("DtDdWrapper");
	            $Coursename->removeDecorator("Label");
	            $Coursename->removeDecorator('HtmlTag');   
	            
	            $Companyname = new Zend_Dojo_Form_Element_FilteringSelect('Companyname');
			    $Companyname->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Companyname->addMultiOption('','Select'); 	           	         		       		     
	            $Companyname->removeDecorator("DtDdWrapper");
	            $Companyname->removeDecorator("Label");
	            $Companyname->removeDecorator('HtmlTag');  

	            $city = new Zend_Dojo_Form_Element_FilteringSelect('cities');
			    $city->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            //$city->addMultiOption('','Select'); 	           	         		       		     
	            $city->removeDecorator("DtDdWrapper");
	            $city->removeDecorator("Label");
	            $city->removeDecorator('HtmlTag');

	            $Takafulname = new Zend_Dojo_Form_Element_FilteringSelect('Takafulname');
			    $Takafulname->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Takafulname->addMultiOption('','Select'); 	           	         		       		     
	            $Takafulname->removeDecorator("DtDdWrapper");
	            $Takafulname->removeDecorator("Label");
	            $Takafulname->removeDecorator('HtmlTag');   
	            
	           
	            
	            $ICNO = new Zend_Form_Element_Text('ICNO');
			    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $ICNO->setAttrib('class','txt_put');  
	            $ICNO->removeDecorator("DtDdWrapper");
	            $ICNO->removeDecorator("Label");
	            $ICNO->removeDecorator('HtmlTag');
	            
	            $Studentname = new Zend_Form_Element_Text('Studentname');
			    $Studentname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $Studentname->setAttrib('class','txt_put');  
	            $Studentname->removeDecorator("DtDdWrapper");
	            $Studentname->removeDecorator("Label");
	            $Studentname->removeDecorator('HtmlTag');
	            
	            $Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						//->setAttrib('onChange', "dijit.byId('Date2').constraints.min = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth")
						//->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	        	$Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							//->setAttrib('onChange', "dijit.byId('Date').constraints.max = arguments[0];")
							//->setAttrib('constraints', "$dateofbirth")
							//->setAttrib('required',"true")		
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
				$Dates = new Zend_Dojo_Form_Element_DateTextBox('Dates');
	        	$Dates->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						//->setAttrib('onChange', "dijit.byId('Dates2').constraints.min = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth")
						//->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$Dates2 = new Zend_Dojo_Form_Element_DateTextBox('Dates2');
	        	$Dates2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							//->setAttrib('onChange', "dijit.byId('Dates').constraints.max = arguments[0];")
							//->setAttrib('constraints', "$dateofbirth")
							//->setAttrib('required',"true")		
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
	       	 	
				$UpDate1 = new Zend_Dojo_Form_Element_DateTextBox('UpDate1');
	        	$UpDate1->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						//->setAttrib('onChange', "dijit.byId('UpDate2').constraints.min = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth")
						//->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$UpDate2 = new Zend_Dojo_Form_Element_DateTextBox('UpDate2');
	        	$UpDate2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							//->setAttrib('onChange', "dijit.byId('UpDate1').constraints.max = arguments[0];")
							//->setAttrib('constraints', "$dateofbirth")
							//->setAttrib('required',"true")		
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
				$Date3 = new Zend_Dojo_Form_Element_DateTextBox('Date3');
	        	$Date3->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('constraints', "$dateofbirth")
						->setAttrib('onChange', "examtoDateSetting();")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
				$Date4 = new Zend_Dojo_Form_Element_DateTextBox('Date4');
	        	$Date4->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						//->setAttrib('onChange', "dijit.byId('Date3').constraints.max = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
				//$Date2->removeDecorator("DtDdWrapper");
				//$Date2->setAttrib('title',"dd-mm-yyyy")
				// ->setAttrib('constraints', "$dateofbirth");
	       	    //$Date2->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	        	//$Date2->removeDecorator("Label");
	        	//$Date2->removeDecorator('HtmlTag');
	        	
	        	//$Date->setAttrib('required',"true");
	            //$Date->removeDecorator("DtDdWrapper");
				//$Date->setAttrib('title',"dd-mm-yyyy")
				//     ->setAttrib('constraints', "$dateofbirth");
	       	    //$Date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	        	//$Date->removeDecorator("Label");
	        	//$Date->removeDecorator('HtmlTag'); 
	        	//$Date->setAttrib('required',"true");
	        	
	            $Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
			    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Venues->addMultiOption('','Select');  
	            $Venues->removeDecorator("DtDdWrapper");
	            $Venues->removeDecorator("Label");
	            $Venues->removeDecorator('HtmlTag');
	           // $Venues->setAttrib('required',"true");
	            
	            $State = new Zend_Form_Element_Text('State');
			    $State->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $State->setAttrib('class','txt_put');  
	            $State->removeDecorator("DtDdWrapper");
	            $State->removeDecorator("Label");
	            $State->removeDecorator('HtmlTag');
			
				$FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
	        	$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	       	 	$FromDate->removeDecorator("DtDdWrapper")
						 ->setAttrib('title',"dd-mm-yyyy")
	       				 ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	        	$FromDate->removeDecorator("Label");
	        	$FromDate->removeDecorator('HtmlTag'); 
        				
			  
				$ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
	        	$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	       	 	$ToDate->removeDecorator("DtDdWrapper")
						->setAttrib('title',"dd-mm-yyyy")
	       				 ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");;
	        	$ToDate->removeDecorator("Label");
	        	$ToDate->removeDecorator('HtmlTag'); 
	        	
	        	
	        	$Approvaltype = new Zend_Dojo_Form_Element_FilteringSelect('Approvaltype');
			    $Approvaltype->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Approvaltype->addMultiOptions(array(
									'1' => 'All',
									'2' => 'Approved',
	                                '3' => 'Not Approved' ));           	         		       		     
	            $Approvaltype->removeDecorator("DtDdWrapper");
	            $Approvaltype->removeDecorator("Label");
	            $Approvaltype->removeDecorator('HtmlTag');
	            
	            $Applicationtype = new Zend_Dojo_Form_Element_FilteringSelect('Applicationtype');
			    $Applicationtype->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Applicationtype->addMultiOptions(array(
									'1' => 'All',
									'2' => 'Individual',
	                                '3' => 'Company',
	                                '4' => 'Takaful'));      	         		       		     
	            $Applicationtype->removeDecorator("DtDdWrapper");
	            $Applicationtype->removeDecorator("Label");
	            $Applicationtype->removeDecorator('HtmlTag');
	            
	            $Payment = new Zend_Dojo_Form_Element_FilteringSelect('Payment');
			    $Payment->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Payment->addMultiOption('','Select');
	            $Payment->addMultiOptions(array(
									'1' => 'Paid',
									'2' => 'Pending'));
	            $Payment->removeDecorator("DtDdWrapper");
	            $Payment->removeDecorator("Label");
	            $Payment->removeDecorator('HtmlTag');
	            
	            $paymentmode = new Zend_Dojo_Form_Element_FilteringSelect('paymentmode');
			    $paymentmode->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $paymentmode->addMultiOption('','Select'); 
	            $paymentmode->addMultiOptions(array(
									'1' => 'FPX',
									'2' => 'Credit Card',
	            					'5' => 'Money Order',
	            					'6' => 'Postal Order',
	            					'7' => 'Credit/Bank to IBFIM account')); 	           	         		       		     
	            $paymentmode->removeDecorator("DtDdWrapper");
	            $paymentmode->removeDecorator("Label");
	            $paymentmode->removeDecorator('HtmlTag');
	            
	            $approval = new Zend_Dojo_Form_Element_FilteringSelect('approval');
			    $approval->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $approval->addMultiOption('','Select'); 
	            $approval->addMultiOptions(array(
									'1' => 'Approved',
									'2' => 'Not Approved')); 	           	         		       		     
	            $approval->removeDecorator("DtDdWrapper");
	            $approval->removeDecorator("Label");
	            $approval->removeDecorator('HtmlTag');
	            
	            $Canditatename = new Zend_Dojo_Form_Element_FilteringSelect('Canditatename');
			    $Canditatename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Canditatename->removeDecorator("DtDdWrapper");
	            $Canditatename->removeDecorator("Label");
	            $Canditatename->removeDecorator('HtmlTag')
	                          ->addMultiOption('','Select');  
	                          
	            $Date7 = new Zend_Dojo_Form_Element_DateTextBox('Date7');
	        	$Date7->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						 ->setAttrib('onChange', "examtoDateSetting();")
						//->setAttrib('constraints', "$dateofbirth1")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
				$Date8 = new Zend_Dojo_Form_Element_DateTextBox('Date8');
	        	$Date8->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						//->setAttrib('onChange', "dijit.byId('Date7').constraints.max = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth1")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	            
			    $session = new Zend_Dojo_Form_Element_FilteringSelect('session');
			    $session->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $session->removeDecorator("DtDdWrapper");
	            $session->setAttrib('onChange', "programlists(this.value);");
	            $session->removeDecorator("Label");
	            $session->removeDecorator('HtmlTag');
	            $session->setAttrib('required',"true");	
         	    
	            $Date9 = new Zend_Dojo_Form_Element_DateTextBox('Date9');
	        	$Date9->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('constraints', "$dateofbirth")
						->setAttrib('onChange', "newvenuelists();")
						->setAttrib('required',"true")	
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
			  
				$Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');
			    $Venue->setAttrib('dojoType',"dijit.form.FilteringSelect");
			    $Venue->setAttrib('required',"true");
	            $Venue->removeDecorator("DtDdWrapper");
	            $Venue->setAttrib('onChange', "newsessionlists();");
	            $Venue->removeDecorator("Label");
	            $Venue->removeDecorator('HtmlTag');
	            
	            $Course = new Zend_Dojo_Form_Element_FilteringSelect('Course');
			    $Course->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Course->setAttrib('required',"true"); 	
	            $Course->removeDecorator("DtDdWrapper");
	            $Course->removeDecorator("Label");
	            $Course->removeDecorator('HtmlTag');
			/*-----------COMMON FIELDS FOR REPORT & NOT TO BE CHANGED-----------*/	
			//Common Form Elements For Report Generation
			
			
    	$Generate = new Zend_Form_Element_Submit('Generate');
		$Generate->dojotype="dijit.form.Button";
        $Generate->setAttrib('class', 'NormalBtn')
        		  ->removeDecorator("DtDdWrapper")
	        	  ->removeDecorator("Label")
    	    	   ->removeDecorator('HtmlTag');
		$Generate->label = $gstrtranslate->_("Generate");
    	    			
		$Print = new Zend_Form_Element_Button('Print');
		$Print->dojotype="dijit.form.Button";
        $Print->label = "Print";
        $Print->setAttrib('id', 'Print')
        		->setAttrib('class', 'NormalBtn')
        		->setAttrib('onclick', 'GenerateReport()')
        		->removeDecorator("DtDdWrapper")
	        	->removeDecorator("Label")
    	    	->removeDecorator('HtmlTag');
        
        $ColumnFilter = new Zend_Form_Element_MultiCheckbox('ColumnFilter');
	    $ColumnFilter->setAttrib('onclick','ToggleTableHeaders(this.value)')
    	    		  ->setAttrib('id','ColumnFilter')
    	    		  ->removeDecorator("DtDdWrapper")
        			  ->removeDecorator("Label")
        			  ->removeDecorator('HtmlTag');
        					
        $ColumnOdrerBy = new Zend_Form_Element_Select('ColumnOdrerBy');
	    $ColumnOdrerBy->setAttrib('onChange','ToggleTableHeaders(this.value)')
    	    			->setAttrib('id','ColumnOdrerBy')
						->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag')
        				->addMultiOptions(array(''=>''));
        	
        $ReportType = new Zend_Dojo_Form_Element_FilteringSelect('ReportType');	
        $ReportType->addMultiOptions(array('pdf' => 'PDF',
									   'excel' => 'Excel'))
        		   ->setAttrib('id','ColumnOdrerBy')
	        	   ->setAttrib('class', 'MakeEditable');
        $ReportType->removeDecorator("DtDdWrapper");
        $ReportType->removeDecorator("Label");
        $ReportType->removeDecorator('HtmlTag');
		$ReportType->setAttrib('dojoType',"dijit.form.FilteringSelect");
 			
        				
        $Export = new Zend_Form_Element_Submit('Export');
        $Export->dojotype="dijit.form.Button";
        $Export->label = $gstrtranslate->_("Export");
        $Export	->setAttrib('id', 'Export')
        		->setAttrib('class', 'NormalBtn')
        		->setAttrib('onclick', 'ExportReport()')
        		->removeDecorator("DtDdWrapper")
	        	->removeDecorator("Label")
    	    	->removeDecorator('HtmlTag');
		    			
    	    	
    	   $Date5 = new Zend_Dojo_Form_Element_DateTextBox('Date5');
	       $Date5->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->setAttrib('OnChange', 'fnGetschedulerList(this.value)')
							//->setAttrib('onChange', "dijit.byId('Date').constraints.max = arguments[0];")
							//->setAttrib('constraints', "$dateofbirth")
							->setAttrib('required',"true")		
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
    	   	
				$schedulerename = new Zend_Dojo_Form_Element_FilteringSelect('schedulerename');
			    $schedulerename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	           // $schedulerename ->setAttrib('OnChange', 'fnGetschedulerList');           	         		       		     
	            $schedulerename->removeDecorator("DtDdWrapper");
	            $schedulerename->removeDecorator("Label");
	            $schedulerename->removeDecorator('HtmlTag');   
			
	    $Clear = new Zend_Form_Element_Submit('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		
        
        $submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $prin = new Zend_Form_Element_Submit('print');
        $prin->dojotype="dijit.form.Button";
        $prin->label = $gstrtranslate->_("print");
        $prin->removeDecorator("DtDdWrapper");
        $prin->removeDecorator("Label");
        $prin->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		$Monthyear = new Zend_Dojo_Form_Element_DateTextBox('Monthyear');
        	$Monthyear->setAttrib('dojoType',"dijit.form.DateTextBox");
       	 	$Monthyear->removeDecorator("DtDdWrapper")
					//->setAttrib('title',"dd-mm-yyyy")
					->setAttrib('required',"true")
       				 ->setAttrib('constraints', "{datePattern:'MM_yyyy'}");
        	$Monthyear->removeDecorator("Label");
        	$Monthyear->removeDecorator('HtmlTag'); 
        				  
        		  
        $Description = new Zend_Form_Element_Text('Description');
		$Description->setAttrib('dojoType',"dijit.form.ValidationTextBox");              			        
        $Description->removeDecorator("DtDdWrapper")
        	        ->removeDecorator("Label")
        		    ->removeDecorator('HtmlTag');	

        $Controller = new Zend_Form_Element_Text('Controller1');
		$Controller->setAttrib('dojoType',"dijit.form.ValidationTextBox");              			        
        $Controller->removeDecorator("DtDdWrapper")
        	       ->removeDecorator("Label")
        		   ->removeDecorator('HtmlTag');

 $Fromdate = new Zend_Dojo_Form_Element_DateTextBox('Fromdate');
	    $Fromdate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Todate').constraints.min = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth")
->setAttrib('onChange', "examtoDateSetting();")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	    $Todate = new Zend_Dojo_Form_Element_DateTextBox('Todate');
	    $Todate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->setAttrib('onChange', "dijit.byId('Fromdate').constraints.max = arguments[0];")
							//->setAttrib('constraints', "$dateofbirth")
							->setAttrib('required',"true")		
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	

        $Student = new Zend_Form_Element_Text('Student',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
		$Student->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Student->setAttrib('class','txt_put');
       // $Student->setAttrib('onChange', "Checkstudent();");		
	    $Student->removeDecorator("DtDdWrapper");
	    $Student->removeDecorator("Label");
	    $Student->removeDecorator('HtmlTag');

 $Result = new Zend_Dojo_Form_Element_FilteringSelect('Result');
			    $Result->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Result->addMultiOptions(array(''=>'Select','3'=>'Applied','1'=>'Pass','2'=>'Fail','4'=>'Absent')); 	           	         		       		     
	            $Result->removeDecorator("DtDdWrapper");
	            $Result->removeDecorator("Label");
	            $Result->removeDecorator('HtmlTag');  	
	            
			$this->addElements(
        					array($Coursename,
        					      $schedulerename,
        					      $Companyname,
        					      $Monthyear,
        					      $Description,
        					      $Controller,
        					      $city,
        						  $ICNO,
        					      $Studentname,
        					      $Venue,
        					      $Venues,
        					      $Date,
        					      $Date2,
        					      $Date5,
        					      $State,
        					      $Takafulname,
        					      $Result,
        					      $FromDate,
        					      $ToDate,
        					      $Generate,
        					      $Print,
        					      $ColumnFilter,	
        						  $ColumnOdrerBy,
        						  $UserNameList,
        						  $ReportType,
        						  $Date3,$Date4,
        						  $Export,
        						  $UpDate1,
        						  $UpDate2,
        						  $Dates,
        						  $Dates2,
        						  $Approvaltype,
        						  $Applicationtype,
        						  $Payment,
        						  $paymentmode,
        						  $approval,
        						  $Clear,
        						  $submit,
        						  $prin,
        						  $Canditatename,
        						  $Date7,
        						  $Date8,
        						  $Date9,
        						  $session,
        						  $Course,$Fromdate,$Todate,$Student
        						)
        			);
		}
}
