<?php
class Reports_Form_Attendancelistform extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ( 'Zend_Translate' );
		
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$Date3 = new Zend_Dojo_Form_Element_DateTextBox('Date3');
	    $Date3->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "examtoDateSetting();")
						->setAttrib('constraints', "$dateofbirth")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
		
		$Date4 = new Zend_Dojo_Form_Element_DateTextBox('Date4');
	    $Date4->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "fngetexamcentre();")
						//->setAttrib('onChange', "dijit.byId('Date7').constraints.max = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth1")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');				
		
		$Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
	    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect")
	    	   ->setAttrib('required',"false");
	    //$Venues->addMultiOption('','Select');  
	    $Venues->removeDecorator("DtDdWrapper");
	    $Venues->removeDecorator("Label");
	    $Venues->removeDecorator('HtmlTag');
		
	    $Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
	    $Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Coursename->addMultiOption('','Select'); 	           	         		       		     
	    $Coursename->removeDecorator("DtDdWrapper");
	    $Coursename->removeDecorator("Label");
	    $Coursename->removeDecorator('HtmlTag');  
		
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
		
		$this->addElements ( array ($Date4, 
		                            $Date3, 
		                            $Coursename,
		                            $Venues,
		                            $submit ) );
	}
}
