<?php
	class Reports_Form_Centerwisestatisticsform extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

                
                $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
                
                
		$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
		$Date->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Date->setAttrib('title',"dd-mm-yyyy");
	  	$Date->setAttrib('constraints', "{datePattern:'MM-yyyy'}");
		$Date->setAttrib('required',"true");
		$Date->removeDecorator("Label");
		$Date->removeDecorator("DtDdWrapper");
		$Date->removeDecorator('HtmlTag');
		 
		$Type = new Zend_Dojo_Form_Element_FilteringSelect('Type');
	    $Type->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Type->addMultiOptions(array(''=>'Select','1'=>'Age Wise','2'=>'Exam Category Wise','3'=>'Exam Centre Wise','4'=>'Exam Result Wise','5'=>'Gender Wise','6'=>'Race Wise','7'=>'Qualification Wise')); 	           	         		       		     
	    $Type->removeDecorator("DtDdWrapper");
	    $Type->removeDecorator("Label");
	    $Type->removeDecorator('HtmlTag');
	           
		$submit = new Zend_Form_Element_Submit('Search');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag');
		$submit->class = "NormalBtn";
		
		$excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
                
                
                 $Month = new Zend_Dojo_Form_Element_FilteringSelect('Month');
	    $Month->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Month->addMultiOptions(array('1'=>'January','2'=>'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December')); 	           	         		       		     
	    $Month->removeDecorator("DtDdWrapper");
	    $Month->removeDecorator("Label");
	    $Month->removeDecorator('HtmlTag');
	    
	    $Year = new Zend_Dojo_Form_Element_FilteringSelect('Year');
	    $Year->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Year->removeDecorator("DtDdWrapper");
	    $Year->removeDecorator("Label");
	    $Year->removeDecorator('HtmlTag');
            
            $ToMonth = new Zend_Dojo_Form_Element_FilteringSelect('ToMonth');
	    $ToMonth->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $ToMonth->addMultiOptions(array('1'=>'January','2'=>'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December')); 	           	         		       		     
	    $ToMonth->removeDecorator("DtDdWrapper");
	    $ToMonth->removeDecorator("Label");
	    $ToMonth->removeDecorator('HtmlTag');
	    
	    $ToYear = new Zend_Dojo_Form_Element_FilteringSelect('ToYear');
	    $ToYear->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $ToYear->removeDecorator("DtDdWrapper");
	    $ToYear->removeDecorator("Label");
	    $ToYear->removeDecorator('HtmlTag');
		
            
            
             $Date1 = new Zend_Dojo_Form_Element_DateTextBox('Date1');
	        	$Date1->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						//->setAttrib('onChange', "dijit.byId('Date2').constraints.min = arguments[0];")
						->setAttrib('constraints', "$dateofbirth")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	        	$Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							//->setAttrib('onChange', "dijit.byId('Date1').constraints.max = arguments[0];")
							->setAttrib('constraints', "$dateofbirth")
							->setAttrib('required',"true")		
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
	    $this->addElements(
        				   array(
        					   $Date,
        					   $submit,
        					   $excel,
        					   $Type,$Year,$Month,$ToYear,$ToMonth,$Date1,$Date2
        				     )
        			      );
		}
}
