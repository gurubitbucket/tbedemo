<?php
class Reports_Form_Excelstudentapplicationform extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ( 'Zend_Translate' );
		
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$ExamfromDate = new Zend_Dojo_Form_Element_DateTextBox('ExamfromDate');
	    $ExamfromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $ExamfromDate->setAttrib('title',"dd-mm-yyyy");
	    $ExamfromDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	    //$ExamfromDate->setAttrib('required',"true");	
	    $ExamfromDate->removeDecorator("Label");
		$ExamfromDate->removeDecorator("DtDdWrapper");
		$ExamfromDate->removeDecorator('HtmlTag');
	       	 	
	    $ExamtoDate = new Zend_Dojo_Form_Element_DateTextBox('ExamtoDate');
	    $ExamtoDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $ExamtoDate->setAttrib('title',"dd-mm-yyyy");
		$ExamtoDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		//$ExamtoDate->setAttrib('onChange', "fngetexamcentre();");
		//$ExamtoDate->setAttrib('required',"true");	
		$ExamtoDate->removeDecorator("Label");
		$ExamtoDate->removeDecorator("DtDdWrapper");
		$ExamtoDate->removeDecorator('HtmlTag');
	    
	    $Appliedfrom = new Zend_Dojo_Form_Element_DateTextBox('Appliedfrom');
	    $Appliedfrom->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Appliedfrom->setAttrib('title',"dd-mm-yyyy");
		$Appliedfrom->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");	
		$Appliedfrom->removeDecorator("Label");
		$Appliedfrom->removeDecorator("DtDdWrapper");
		$Appliedfrom->removeDecorator('HtmlTag');
	       	 	
	    $Appliedto = new Zend_Dojo_Form_Element_DateTextBox('Appliedto');
	    $Appliedto->setAttrib('dojoType',"dijit.form.DateTextBox");
	    $Appliedto->setAttrib('title',"dd-mm-yyyy");
		$Appliedto->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");	
		$Appliedto->removeDecorator("Label");
		$Appliedto->removeDecorator("DtDdWrapper");
		$Appliedto->removeDecorator('HtmlTag');
							
		$Studentname = new Zend_Form_Element_Text('Studentname');
	    $Studentname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Studentname->setAttrib('class','txt_put');  
	    $Studentname->removeDecorator("DtDdWrapper");
	    $Studentname->removeDecorator("Label");
	    $Studentname->removeDecorator('HtmlTag');
	            
	    $ICNO = new Zend_Form_Element_Text('ICNO');
	    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $ICNO->setAttrib('class','txt_put');  
	    $ICNO->removeDecorator("DtDdWrapper");
	    $ICNO->removeDecorator("Label");
	    $ICNO->removeDecorator('HtmlTag');						
	
		$Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
	    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Venues->setAttrib('required',"false");
	    $Venues->setRegisterInArrayValidator(false);
	    $Venues->removeDecorator("DtDdWrapper");
	    $Venues->removeDecorator("Label");
	    $Venues->removeDecorator('HtmlTag');
	    
	    $Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
		$Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Coursename->addMultiOption('','Select'); 	           	         		       		     
	    $Coursename->removeDecorator("DtDdWrapper");
	    $Coursename->removeDecorator("Label");
	    $Coursename->removeDecorator('HtmlTag'); 
	    
	    $submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag');
        $submit->class = "NormalBtn";
		
        $excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag');
		
		$PDF = new Zend_Form_Element_Submit('PrintPDF');
		$PDF->dojotype="dijit.form.Button";
		$PDF->label = $gstrtranslate->_("Print PDF");
		$PDF->removeDecorator("DtDdWrapper");
		$PDF->removeDecorator("Label");
		$PDF->removeDecorator('HtmlTag');
		
		$this->addElements ( array (
		                            $Coursename,
		                            $Venues,
		                            $ICNO,
		                            $Studentname,
		                            $Appliedfrom,
		                            $Appliedto,
		                            $ExamfromDate,
		                            $ExamtoDate,
		                            $submit,
		                            $excel,
		                            $PDF
		                             ) );
	}
}
