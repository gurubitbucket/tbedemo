<?php
	class Reports_Form_Datereportform extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

		$Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	    $Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->setAttrib('onChange', "fngetexamcentre();")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
		
		$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	    $Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');

		$Studentname = new Zend_Form_Element_Text('Studentname');
	    $Studentname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Studentname->setAttrib('class','txt_put');  
	    $Studentname->removeDecorator("DtDdWrapper");
	    $Studentname->removeDecorator("Label");
	    $Studentname->removeDecorator('HtmlTag');
	    
	    $ICNO = new Zend_Form_Element_Text('ICNO');
	    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $ICNO->setAttrib('class','txt_put');  
	    $ICNO->removeDecorator("DtDdWrapper");
	    $ICNO->removeDecorator("Label");
	    $ICNO->removeDecorator('HtmlTag');
	    
	    $Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
	    $Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Coursename->addMultiOption('','Select'); 	           	         		       		     
	    $Coursename->removeDecorator("DtDdWrapper");
	    $Coursename->removeDecorator("Label");
	    $Coursename->removeDecorator('HtmlTag');
	    
	    $Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
	    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect")
	    	   ->setAttrib('required',"false")
	    	   ->setRegisterInArrayValidator(false);
	    $Venues->removeDecorator("DtDdWrapper");
	    $Venues->removeDecorator("Label");
	    $Venues->removeDecorator('HtmlTag');
	    
	    $this->addElements(
        				   array(
        					   $Date2,
        					   $Date,
        					   $Studentname,
        					   $ICNO,
        					   $Coursename,
        					   $Venues
        				     )
        			      );
		}
}
