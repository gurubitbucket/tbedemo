<?php
	class Reports_Form_Statisticsnew extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

		$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
		$Date->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Date->setAttrib('title',"dd-mm-yyyy");
	  	$Date->setAttrib('constraints', "{datePattern:'MM-yyyy'}");
		$Date->setAttrib('required',"true");
		$Date->removeDecorator("Label");
		$Date->removeDecorator("DtDdWrapper");
		$Date->removeDecorator('HtmlTag');
		
		$Type = new Zend_Dojo_Form_Element_FilteringSelect('Type');
	    $Type->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Type->addMultiOptions(array('8'=>'All','1'=>'Age Wise','2'=>'Exam Category Wise','3'=>'Exam Centre Wise','4'=>'Exam Result Wise','5'=>'Gender Wise','6'=>'Race Wise','9'=>'Session Wise','7'=>'Qualification Wise')); 	           	         		       		     
	    $Type->removeDecorator("DtDdWrapper");
	    $Type->removeDecorator("Label");
	    $Type->removeDecorator('HtmlTag');
	    
	    $Month = new Zend_Dojo_Form_Element_FilteringSelect('Month');
	    $Month->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Month->addMultiOptions(array('1'=>'January','2'=>'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December')); 	           	         		       		     
	    $Month->removeDecorator("DtDdWrapper");
	    $Month->removeDecorator("Label");
	    $Month->removeDecorator('HtmlTag');
	    
	    $Year = new Zend_Dojo_Form_Element_FilteringSelect('Year');
	    $Year->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Year->removeDecorator("DtDdWrapper");
	    $Year->removeDecorator("Label");
	    $Year->removeDecorator('HtmlTag');
		
		$submit = new Zend_Form_Element_Submit('Search');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag');
		$submit->class = "NormalBtn";
		
		$excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
		
		$Statistics = new Zend_Dojo_Form_Element_FilteringSelect('Statistics');
		$Statistics->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$Statistics->addMultiOptions(array('1'=>'Age Wise','2'=>'Exam Category Wise','3'=>'Exam Centre Wise','4'=>'Exam Result Wise','5'=>'Gender Wise','6'=>'Race Wise','7'=>'Qualification Wise'));
		$Statistics->setAttrib('required',"true");
		$Statistics->removeDecorator("DtDdWrapper");
		$Statistics->removeDecorator("Label");
		$Statistics->removeDecorator('HtmlTag');
		     
	    $this->addElements(
        				   array(
        					   $Date,
        					   $Month,
        					   $Year,
        					   $submit,
        					   $excel,
        					   $Type,
        					   $Statistics
        				     )
        			      );
		}
}
