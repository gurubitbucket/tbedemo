<?php
	class Reports_Form_Pullpushdata extends Zend_Dojo_Form {
		public function init() {
		
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
        $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$ExamDate = new Zend_Dojo_Form_Element_DateTextBox('ExamDate');
		$ExamDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$ExamDate->setAttrib('title',"dd-mm-yyyy");
	  	$ExamDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	  	//$ExamDate->setAttrib('constraints', "$dateofbirth");
		$ExamDate->setAttrib('required',"true");
		$ExamDate->removeDecorator("Label");
		$ExamDate->removeDecorator("DtDdWrapper");
		$ExamDate->removeDecorator('HtmlTag');		
		$ExamDate->setAttrib('onChange', "getcentres();");
		
		$Centre = new Zend_Dojo_Form_Element_FilteringSelect('Centre');
        $Centre->removeDecorator("DtDdWrapper");
        $Centre->removeDecorator("Label");
        $Centre->removeDecorator('HtmlTag');
        $Centre->setAttrib('required',"false");
		$Centre->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Session = new Zend_Dojo_Form_Element_FilteringSelect('Session');
        $Session->removeDecorator("DtDdWrapper");
        $Session->removeDecorator("Label");
        $Session->removeDecorator('HtmlTag');
        $Session->setAttrib('required',"false");
		$Session->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Search = new Zend_Form_Element_Submit('Search');
		$Search->dojotype="dijit.form.Button";
		$Search->label = $gstrtranslate->_("Search");
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator("Label");
		$Search->removeDecorator('HtmlTag');
		$Search->class = "NormalBtn";
		
		$PDF = new Zend_Form_Element_Submit('PrintPDF');
		$PDF->dojotype="dijit.form.Button";
		$PDF->label = $gstrtranslate->_("Print PDF");
		$PDF->removeDecorator("DtDdWrapper");
		$PDF->removeDecorator("Label");
		$PDF->removeDecorator('HtmlTag');
		
	    $this->addElements(
        				   array(
        					   $ExamDate,
        					   $Centre,
        					   $Session,
        					   $Search,
        					   $PDF
        				     )
        			      );
		}
}
