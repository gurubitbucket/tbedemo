<?php
	class Reports_Form_Pullpushdata extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

		$ExamDate = new Zend_Dojo_Form_Element_DateTextBox('ExamDate');
		$ExamDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$ExamDate->setAttrib('title',"dd-mm-yyyy");
	  	$ExamDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$ExamDate->setAttrib('required',"true");
		$ExamDate->removeDecorator("Label");
		$ExamDate->removeDecorator("DtDdWrapper");
		$ExamDate->removeDecorator('HtmlTag');		
		
		
		$Centre = new Zend_Dojo_Form_Element_FilteringSelect('Centre');
        $Centre->removeDecorator("DtDdWrapper");
        $Centre->removeDecorator("Label");
        $Centre->removeDecorator('HtmlTag');
		$Centre->setAttrib('dojoType',"dijit.form.FilteringSelect");		
		
		
		$Search = new Zend_Form_Element_Submit('Search');
		$Search->dojotype="dijit.form.Button";
		$Search->label = $gstrtranslate->_("Search");
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator("Label");
		$Search->removeDecorator('HtmlTag');
		$Search->class = "NormalBtn";
		
		$excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
		
	    $this->addElements(
        				   array(
        					   $ExamDate,
        					   $Centre,
        					   $Search,
        					   $excel,
        				     )
        			      );
		}
}
