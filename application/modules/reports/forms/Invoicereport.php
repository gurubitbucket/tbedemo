<?php
	class Reports_Form_Invoicereport extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
	    $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$yesterdaydate1= date('Y-m-d', mktime(0,0,0,$month,($day+14),$year));
		$dateofbirth1 = "{min:'$yesterdaydate1',datePattern:'dd-MM-yyyy'}"; 
		
		$field19 = new Zend_Dojo_Form_Element_FilteringSelect('field19');        
        $field19 ->removeDecorator("DtDdWrapper");
        $field19 ->addMultiOption('','Select');
        $field19 ->removeDecorator("Label");        
        $field19 ->removeDecorator('HtmlTag')
        		 ->setAttrib('class', 'txt_put') ;
        $field19 ->setRegisterInArrayValidator(false);
		$field19 ->setAttrib('dojoType',"dijit.form.FilteringSelect");
		 
		$field3 = new Zend_Form_Element_Text('field3');
        $field3 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
      	$field3->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
		
		$Date3 = new Zend_Dojo_Form_Element_DateTextBox('Date3');
		$Date3->setAttrib('dojoType',"dijit.form.DateTextBox")
			->setAttrib('title',"dd-mm-yyyy")
			->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
			->setAttrib('constraints', "$dateofbirth")
			->setAttrib('onChange', "examtoDateSetting();")
			->setAttrib('required',"true")
			->removeDecorator("Label")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag');
						
		$Date4 = new Zend_Dojo_Form_Element_DateTextBox('Date4');
		$Date4->setAttrib('dojoType',"dijit.form.DateTextBox")
			->setAttrib('title',"dd-mm-yyyy")
			->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
			->setAttrib('required',"true")
			->removeDecorator("Label")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag');

		$submit = new Zend_Form_Element_Submit('Search');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag')
			->class = "NormalBtn";
				
	  $this->addElements(array($submit,$Date4,$Date3,$field3,$field19
        				)
        			);
		}
}
