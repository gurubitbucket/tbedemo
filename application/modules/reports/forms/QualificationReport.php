<?php
class Reports_Form_QualificationReport extends Zend_Dojo_Form { //Formclass for the user module
    public function init() 
     {    	
        $gstrtranslate =Zend_Registry::get('Zend_Translate'); //get translator instance 
     	
        $Monthyear = new Zend_Dojo_Form_Element_DateTextBox('Monthyear');
        $Monthyear->setAttrib('dojoType',"dijit.form.DateTextBox");
       	$Monthyear->removeDecorator("DtDdWrapper")
		          ->setAttrib('required',"true")
       	          ->setAttrib('constraints', "{datePattern:'MM_yyyy'}");
        $Monthyear->removeDecorator("Label");
        $Monthyear->removeDecorator('HtmlTag');
         
	    $submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         		->class = "NormalBtn";  	

        $excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;		
      
        $this->addElements(array($Monthyear,$submit,$excel));
     }
}