<?php
	class Reports_Form_Migspayment extends Zend_Dojo_Form {
		public function init() {
		
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$Fromdate = new Zend_Dojo_Form_Element_DateTextBox('Fromdate');
	    $Fromdate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						//->setAttrib('onChange', "examtoDateSetting();")
						->setAttrib('constraints', "$dateofbirth")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
		
		$Todate = new Zend_Dojo_Form_Element_DateTextBox('Todate');
	    $Todate ->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "fngetexamcentre();")
						//->setAttrib('onChange', "dijit.byId('Date7').constraints.max = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth1")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');

		$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
		$Date->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Date->setAttrib('title',"dd-mm-yyyy");
	  	$Date->setAttrib('constraints', "{datePattern:'MM-yyyy'}");
		$Date->setAttrib('required',"true");
		$Date->removeDecorator("Label");
		$Date->removeDecorator("DtDdWrapper");
		$Date->removeDecorator('HtmlTag');
		 
		
	           
		$submit = new Zend_Form_Element_Submit('Search');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag');
		$submit->class = "NormalBtn";
		
		$excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
		     
	    $this->addElements(
        				   array(
        					   $Date,$Todate,$Fromdate,
        					   $submit,
        					   $excel
        				     )
        			      );
		}
}
