<?php
	class Reports_Form_Statistics extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

		$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
		$Date->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Date->setAttrib('title',"dd-mm-yyyy");
	  	$Date->setAttrib('constraints', "{datePattern:'MM-yyyy'}");
		$Date->setAttrib('required',"true");
		$Date->removeDecorator("Label");
		$Date->removeDecorator("DtDdWrapper");
		$Date->removeDecorator('HtmlTag');
		 
		$Type = new Zend_Dojo_Form_Element_FilteringSelect('Type');
	    $Type->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Type->addMultiOptions(array(''=>'Select','1'=>'Age Wise','2'=>'Exam Category Wise','3'=>'Exam Centre Wise','4'=>'Exam Result Wise','5'=>'Gender Wise','6'=>'Race Wise','7'=>'Qualification Wise')); 	           	         		       		     
	    $Type->removeDecorator("DtDdWrapper");
	    $Type->removeDecorator("Label");
	    $Type->removeDecorator('HtmlTag');
	           
		$submit = new Zend_Form_Element_Submit('Search');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag');
		$submit->class = "NormalBtn";
		
		$excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
		     
	    $this->addElements(
        				   array(
        					   $Date,
        					   $submit,
        					   $excel,
        					   $Type
        				     )
        			      );
		}
}
