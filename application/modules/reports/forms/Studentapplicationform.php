<?php
class Reports_Form_Studentapplicationform extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ( 'Zend_Translate' );
		
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$Dates = new Zend_Dojo_Form_Element_DateTextBox('Dates');
	    $Dates->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	    $Dates2 = new Zend_Dojo_Form_Element_DateTextBox('Dates2');
	    $Dates2->setAttrib('dojoType',"dijit.form.DateTextBox")
	          ->setAttrib('title',"dd-mm-yyyy")
			  ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
			  //->setAttrib('onChange', "fngetexamcentre();")
			  //->setAttrib('required',"true")	
			  ->removeDecorator("Label")
			  ->removeDecorator("DtDdWrapper")
			  ->removeDecorator('HtmlTag');
	    
	    $UpDate1 = new Zend_Dojo_Form_Element_DateTextBox('UpDate1');
	    $UpDate1->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")	
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	    $UpDate2 = new Zend_Dojo_Form_Element_DateTextBox('UpDate2');
	    $UpDate2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")	
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
		$Studentname = new Zend_Form_Element_Text('Studentname');
	    $Studentname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Studentname->setAttrib('class','txt_put');  
	    $Studentname->removeDecorator("DtDdWrapper");
	    $Studentname->removeDecorator("Label");
	    $Studentname->removeDecorator('HtmlTag');
	            
	    $ICNO = new Zend_Form_Element_Text('ICNO');
	    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $ICNO->setAttrib('class','txt_put');  
	    $ICNO->removeDecorator("DtDdWrapper");
	    $ICNO->removeDecorator("Label");
	    $ICNO->removeDecorator('HtmlTag');						
	
		$Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
	    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect")
	    	   ->setAttrib('required',"false")
	    	   ->setRegisterInArrayValidator(false);
	    $Venues->removeDecorator("DtDdWrapper");
	    $Venues->removeDecorator("Label");
	    $Venues->removeDecorator('HtmlTag');
	    
	    $Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
			    $Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Coursename->addMultiOption('','Select'); 	           	         		       		     
	            $Coursename->removeDecorator("DtDdWrapper");
	            $Coursename->removeDecorator("Label");
	            $Coursename->removeDecorator('HtmlTag'); 
		
	     $Takafulname = new Zend_Dojo_Form_Element_FilteringSelect('Takafulname');
			    $Takafulname->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Takafulname->addMultiOption('','Select'); 	           	         		       		     
	            $Takafulname->removeDecorator("DtDdWrapper");
	            $Takafulname->removeDecorator("Label");
	            $Takafulname->removeDecorator('HtmlTag');   
	            
	    $Payment = new Zend_Dojo_Form_Element_FilteringSelect('Payment');
			    $Payment->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Payment->addMultiOption('','Select');
	            $Payment->addMultiOptions(array(
									'1' => 'Paid',
									'2' => 'Pending'));
	            $Payment->removeDecorator("DtDdWrapper");
	            $Payment->removeDecorator("Label");
	            $Payment->removeDecorator('HtmlTag');

	    $paymentmode = new Zend_Dojo_Form_Element_FilteringSelect('paymentmode');
			    $paymentmode->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $paymentmode->addMultiOption('','Select'); 
	            $paymentmode->addMultiOptions(array(
									'1' => 'FPX',
									'2' => 'Credit Card',
	            					'5' => 'Money Order',
	            					'6' => 'Postal Order',
	            					'7' => 'Credit/Bank to IBFIM account')); 	           	         		       		     
	            $paymentmode->removeDecorator("DtDdWrapper");
	            $paymentmode->removeDecorator("Label");
	            $paymentmode->removeDecorator('HtmlTag');        
	            
	    $approval = new Zend_Dojo_Form_Element_FilteringSelect('approval');
			    $approval->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $approval->addMultiOption('','Select'); 
	            $approval->addMultiOptions(array(
									'1' => 'Approved',
									'2' => 'Not Approved')); 	           	         		       		     
	            $approval->removeDecorator("DtDdWrapper");
	            $approval->removeDecorator("Label");
	            $approval->removeDecorator('HtmlTag');
	            
		
		
		$this->addElements ( array ($approval, 
		                            $paymentmode, 
		                            $Payment,
		                            $Takafulname,
		                            $Coursename,
		                            $Venues,
		                            $ICNO,
		                            $Studentname,
		                            $UpDate2,
		                            $UpDate1,
		                            $Dates2,
		                            $Dates
		                             ) );
	}
}
