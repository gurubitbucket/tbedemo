<?php
	class Reports_Form_Schedulereport extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

		$FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
		$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$FromDate->setAttrib('title',"dd-mm-yyyy");
	  	$FromDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$FromDate->setAttrib('required',"true");
		$FromDate->removeDecorator("Label");
		$FromDate->removeDecorator("DtDdWrapper");
		$FromDate->removeDecorator('HtmlTag');
		
		$ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
		$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$ToDate->setAttrib('title',"dd-mm-yyyy");
	  	$ToDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$ToDate->setAttrib('required',"true");
		$ToDate->removeDecorator("Label");
		$ToDate->removeDecorator("DtDdWrapper");
		$ToDate->removeDecorator('HtmlTag');
		
		$submit = new Zend_Form_Element_Submit('Search');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag');
		$submit->class = "NormalBtn";
		
		$excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
		
	    $this->addElements(
        				   array(
        					   $FromDate,
        					   $ToDate,
        					   $submit,
        					   $excel,
        				     )
        			      );
		}
}
