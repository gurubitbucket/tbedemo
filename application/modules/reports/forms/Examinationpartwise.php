<?php
	class Reports_Form_Examinationpartwise extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

		$Date7 = new Zend_Dojo_Form_Element_DateTextBox('Date7');
	    $Date7->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->setAttrib('onChange', "examtoDateSetting();")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
		
		$Date8 = new Zend_Dojo_Form_Element_DateTextBox('Date8');
	    $Date8->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "fngetexamcentre();")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');

		$Canditatename = new Zend_Dojo_Form_Element_FilteringSelect('Canditatename');
			    $Canditatename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Canditatename->removeDecorator("DtDdWrapper");
	            $Canditatename->removeDecorator("Label");
	            $Canditatename->removeDecorator('HtmlTag')
	                          ->addMultiOption('','Select');
	    
	    $ICNO = new Zend_Form_Element_Text('ICNO');
	    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $ICNO->setAttrib('class','txt_put');  
	    $ICNO->removeDecorator("DtDdWrapper");
	    $ICNO->removeDecorator("Label");
	    $ICNO->removeDecorator('HtmlTag');
	    
	    $Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
	    $Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Coursename->addMultiOption('','Select'); 	           	         		       		     
	    $Coursename->removeDecorator("DtDdWrapper");
	    $Coursename->removeDecorator("Label");
	    $Coursename->removeDecorator('HtmlTag');
	    
	    $Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
	    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect")
	    	   ->setAttrib('required',"false")
	    	   ->setRegisterInArrayValidator(false);
	    $Venues->removeDecorator("DtDdWrapper");
	    $Venues->removeDecorator("Label");
	    $Venues->removeDecorator('HtmlTag');
	    
	    $this->addElements(
        				   array(
        					   $Date7,
        					   $Date8,
        					   $Canditatename,
        					   $ICNO,
        					   $Coursename,
        					   $Venues
        				     )
        			      );
		}
}
