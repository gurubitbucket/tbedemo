<?php
	class Reports_Form_Slowqueryreport extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
	    $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
	    $Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
        $Date->setAttrib('dojoType',"dijit.form.DateTextBox")
        		    ->setAttrib('title',"dd-mm-yyyy")
					->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
					->setAttrib('onChange', "dijit.byId('Date2').constraints.min = arguments[0];")
					//->setAttrib('constraints', "$dateofbirth")
					//->setAttrib('required',"true")		
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
       	 	
        $Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
        $Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
        				->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Date').constraints.max = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth")
						//->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
							
		$Generate = new Zend_Form_Element_Submit('Generate');
		$Generate->dojotype="dijit.form.Button";
        $Generate->setAttrib('class', 'NormalBtn')
        		  ->removeDecorator("DtDdWrapper")
	        	  ->removeDecorator("Label")
    	    	   ->removeDecorator('HtmlTag');
		$Generate->label = $gstrtranslate->_("Generate");
    	    			
		$Print = new Zend_Form_Element_Button('Print');
		$Print->dojotype="dijit.form.Button";
        $Print->label = "Print";
        $Print->setAttrib('id', 'Print')
        		->setAttrib('class', 'NormalBtn')
        		->setAttrib('onclick', 'GenerateReport()')
        		->removeDecorator("DtDdWrapper")
	        	->removeDecorator("Label")
    	    	->removeDecorator('HtmlTag');
        
       
        				
       

    
			
			$this->addElements(
        					array($Date,
        					      $Date2,
        					      $Generate,
        					      $Print
        						)
        			);
		}
}
