<?php
	class Reports_Form_Studentquestions extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
		 
		$field3 = new Zend_Form_Element_Text('field3');
        $field3 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field3 ->setAttrib('required',"true");
      	$field3->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
		
		$submit = new Zend_Form_Element_Submit('Search');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag')
			->class = "NormalBtn";
				
	  $this->addElements(array($submit,$field3));
		}
}
