<?php
class Reports_CompletedetailsController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;	
	
	public function init() { //initialization function
		$this->gobjsessionstudent = Zend_Registry::get('sis');
		/*$this->gobjsessionsis = Zend_Registry::get('sis');
		if(empty($this->gobjsessionsis->iduser)){ 
			$this->_redirect( $this->baseUrl . '/index/logout');					
		}	*/	
			
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() 
	{
		$this->lobjcompletedetailsmodel = new Reports_Model_DbTable_Completedetails(); //user model object
		$this->lobjcompletedetailsform = new Reports_Form_Completedetails(); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

	public function indexAction() 
	{ // action for search and view
		$this->view->lobjcompletedetailsform = $this->lobjcompletedetailsform;
		/*if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->studentdetailsresult);
		$lintpagecount = 30;
		$lintpage = $this->_getParam('page',1); //Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->studentdetailsresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($result,$lintpage,$lintpagecount);
		}
       */
		if ($this->_request->isPost() && $this->_request->getPost('Search'))
		{
			$larrformData = $this->_request->getPost();
			$larrresult = $this->lobjcompletedetailsmodel->fngetdetailforstudent($larrformData['Fromdate'],$larrformData['Todate']);
			//echo "<pre>";
			//print_r($larrresult);die();
			$this->view->paginator = $larrresult;
			$this->view->lobjcompletedetailsform->populate($larrformData);
					
		}	
		//$larresult = $this->lobjmodel->fnGetbatch();
		//$this->lobjAttendencereportForm->Batch->addMultiOptions($larresult);
	}
		public function getallattemptsAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idapp = $this->_getParam('idapp');
		$cnt = $this->_getParam('cnt');
		$icno = $this->_getParam('icno');
		$result = $this->lobjcompletedetailsmodel->fngetallattemptsforicno($icno,$cnt);
		$tabledata= '<br><fieldset><legend align = "left">Previous Attempts:</legend>
					                    <table class="table" border=1 align = "center" width=100%>
					                    	<tr>
					                    	   <th><b>Examdate</b></th>
											   <th><b>RegisterID</b></th>
					                    		<th><b>Venue</b></th>
												<th><b>Program</b></th>
												<th><b>Session</b></th>
												<th><b>ICNO</b></th>
												<th><b>Name</b></th>
													<th><b>Attempts</b></th>
												<th><b>Result</b></th>
											
					                    		
					                    	</tr>';
		  
		               
		for($i=0;$i<count($result);$i++)
		{
		 
						  $tabledata.= '<tr><td align = "left">'.$result[$i]['DateTime'].'</td>';
					            $tabledata.= '<td align = "left">'.$result[$i]['Regid'].'</td>';          		
						    $tabledata.= '<td align = "left">'.$result[$i]['centername'].'</td>';
						      $tabledata.= '<td align = "left">'.$result[$i]['ProgramName'].'</td>';
						        $tabledata.= '<td align = "left">'.$result[$i]['managesessionname'].'</td>';
								 $tabledata.= '<td align = "left">'.$result[$i]['ICNO'].'</td>';
								 $tabledata.= '<td align = "left">'.$result[$i]['FName'].'</td>';
								    $tabledata.= '<td align = "left">'.$result[$i]['PermCity'].'</td>';
								if($result[$i]['pass']==1)
								{
						          $tabledata.= '<td align = "left">Pass</td></tr>';
								  }
								  elseif($result[$i]['pass']==2)
								  {
								     $tabledata.= '<td align = "left">Fail</td></tr>';
								  }
								  elseif($result[$i]['pass']==3)
								  {
								      $tabledata.= '<td align = "left">Applied</td></tr>';
								  }
								  elseif($result[$i]['pass']==4)
								  {
								      $tabledata.= '<td align = "left">Absent</td></tr>';
								  }
								 
		                    
						          
						         
			 }
			  $tabledata.="<tr><td colspan= '8' align='right'><input type='button' id='close' name='close'  value='Close' onClick='Closevenue();'></td></tr>";
		                     $tabledata.="</table><br>";
			 echo  $tabledata;	   
		//echo "<pre>";
		//print_r($larrresult);die();
		 
	
	}
	
	
}
