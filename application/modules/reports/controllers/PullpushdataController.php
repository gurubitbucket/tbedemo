<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_PullpushdataController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
		$this->lobjmodel = new Reports_Model_DbTable_Pullpushdata(); //model object
		$this->lobjnewform = new  Reports_Form_Pullpushdata(); //form object
		$this->registry = Zend_Registry::getInstance(); //get registry instance
		$this->locale = $this->registry->get('Zend_Locale'); //get locale
	}
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjnewform;
		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->attendancereportpaginatorresult);
		$lintpagecount = 10000;
		$lintpage = $this->_getParam('page',1);//Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->attendancereportpaginatorresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				unset ( $larrformData ['Search']);
				$this->view->examdate = $ldtexamdate = $larrformData['ExamDate'];
				$this->view->centre = $lintidcenter = $larrformData['Centre'];
				$larrregdetails = $this->lobjmodel->fngetdetails($larrformData);//($ldtexamdate,$lintidcenter);
				for($linti=0;$linti<count($larrregdetails);$linti++)
				{
					$m=1;
					$k=1;
					$lintpulled = 0;
					$lintpushed = 0;
					$resarr = explode(',',$larrregdetails[$linti]['Result']);
					for($lintj=0;$lintj<sizeof($resarr);$lintj++){
					 	 if($resarr[$lintj] == 1 || $resarr[$lintj] == 2){
					 	 	$lintpulled++;
					 	 }
					 	 if($resarr[$lintj]== 2){
					 	 	$lintpushed++;
					 	 }
					 }
					if($m==$k)
					{
						$resultedarray1[$linti] = $lintpulled;
						$resultedarray2[$linti] = $lintpushed;
					} 
					$m++;
					$k++;
				}
				for($linti=0;$linti<count($larrregdetails);$linti++)
				{
					$larrregdetails[$linti]['Pulled'] = $resultedarray1[$linti];
					$larrregdetails[$linti]['Pushed'] = $resultedarray2[$linti];
					$larrregdetails[$linti]['NotPulled'] = $larrregdetails[$linti]['Registered']-$larrregdetails[$linti]['Pulled'];
					$larrregdetails[$linti]['NotPushed'] = $larrregdetails[$linti]['Pulled']-$larrregdetails[$linti]['Pushed'];
				}
				if($resultedarray1){
					$this->view->totalpulled = $sum_pulled = array_sum($resultedarray1);
				}else{
					$this->view->totalpulled = $sum_pulled = 0;
				}
			    if($resultedarray2){
					$this->view->totalpushed = $sum_pushed = array_sum($resultedarray2);
				}else{
					$this->view->totalpushed = $sum_pushed = 0;
				}
				
				$larrColorArray['blue'] =  Array();
				$larrColorArray['red'] =  Array();
				$larrColorArray['black'] =  Array();
				foreach ($larrregdetails as $lobjCountry )
				{
		        	if($lobjCountry['Pulled']>0 && $lobjCountry['Pushed']>0)
		        	{
		        		$larrColorArray['blue'][] = $lobjCountry['idcenter'];
		        	}elseif($lobjCountry['Pulled']>0 && $lobjCountry['Pushed'] <= 0)
		        	{
		        		$larrColorArray['red'][] = $lobjCountry['idcenter'];
		        	}else
		        	{
		        		$larrColorArray['black'][] = $lobjCountry['idcenter'];
		        	}
	            }
				$this->view->paginator = $this->lobjCommon->fnPagination($larrregdetails,$lintpage,$lintpagecount);
				$this->view->larrColorArray = $larrColorArray;
				$larrcenters = $this->lobjmodel->fngetcenternames($ldtexamdate);
				$this->lobjnewform->Centre->addmultioption('','Select');
				$this->lobjnewform->Centre->addmultioptions($larrcenters);
				$this->view->lobjform->Centre->setValue($lintidcenter);
				$this->view->lobjform->populate($larrformData);
			}
		}
	}
	public function fngetcentresAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$ldtedate = $this->_getParam('iddate');
		$larrcenters = $this->lobjmodel->fngetcenternames($ldtedate);
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}
	public function getdetailsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidcenter = $this->_getParam('idcenter');
		$ldtedate = $this->_getParam('edate');
		$lstrtype = $this->_getParam('type');
		$lintidsess = $this->_getParam('idsess');
		$result = array();
		switch($lstrtype)
		{
			case 'Pulled': $result = $this->lobjmodel->fngetpulleddetails($lintidcenter,$ldtedate,$lintidsess);
			               break;
			case 'NotPulled': $result = $this->lobjmodel->fngetpullfaileddetails($lintidcenter,$ldtedate,$lintidsess);
			               break;
			case 'Pushed': $result = $this->lobjmodel->fngetpusheddetails($lintidcenter,$ldtedate,$lintidsess);
			               break;
			case 'NotPushed': $result = $this->lobjmodel->fngetpushfaileddetails($lintidcenter,$ldtedate,$lintidsess);
			                   break;
			default: break;
		}             
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Push/Pull Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>ICNO</b></th><th><b>Registered ID</b></th><th><b>Program</b></th><th><b>Exam Venue</b></th></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th><th><b>Result</b></th><th><b>Status</b></th></tr>";
		foreach($result as $lobjCountry)
		{
			$tabledata.="<tr><td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".$lobjCountry['RegisteredID']."</td><td align = 'left'>".$lobjCountry['ProgramName']."</td><td align = 'left'>".$lobjCountry['ExamVenue']."</td><td align = 'left'>".$lobjCountry['ExamDate']."</td><td align = 'left'>".$lobjCountry['managesessionname']."</td></td><td align = 'left'>".$lobjCountry['Result']."</td><td align = 'left'>".$lstrtype."</td></tr>";
		}
		$tabledata.="</table></form><br>";
		$tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
		$tabledata.="<tr><td colspan= '8'><input type='button' id='printpdf' name='printpdf'  value='PrintPdf' onClick=Printfn(".$lintidcenter.",'".$ldtedate."','".$lstrtype."',".$lintidsess.");></td></tr>";
		echo  $tabledata;
	}
	
	public function fngetsessionsAction()
    {
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$venueid = $this->_getParam('idcentre');
		$larrschedulersessions = $this->lobjmodel->fnajaxgetsessbydtvenue($iddate,$venueid);
	    $larrsessbydtvenue = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulersessions);
		echo Zend_Json_Encoder::encode($larrsessbydtvenue);
    }
    
    public function printdetailsAction()
    {   
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$lintidcenter =  $larrformData['Centre'];
		$ldtedate = $larrformData['ExamDate'];
		$lstrtype = $this->_getParam('type');
		$frmdate =date('d-m-Y',strtotime($ldtedate));
    	if($larrformData['Centre'])
		{
			$larrcentre = $this->lobjmodel->fngetcentername($larrformData['Centre']);
			$larrformData['CentreName'] = $larrcentre['centername'];
		}else{
			$larrformData['CentreName'] = 'All';
		}
    	$larrregdetails = $this->lobjmodel->fngetdetails($larrformData);//($ldtexamdate,$lintidcenter);
		for($linti=0;$linti<count($larrregdetails);$linti++)
		{
			$m=1;
			$k=1;
			$lintpulled = 0;
			$lintpushed = 0;
			$resarr = explode(',',$larrregdetails[$linti]['Result']);
			for($lintj=0;$lintj<sizeof($resarr);$lintj++)
			     {
					 if($resarr[$lintj] == 1 || $resarr[$lintj] == 2)
					 {
					 	$lintpulled++;
					 }
					 if($resarr[$lintj]== 2)
					 {
					 	 $lintpushed++;
					 }
				 }
					 if($m==$k)
					 {
						$resultedarray1[$linti] = $lintpulled;
						$resultedarray2[$linti] = $lintpushed;
					 } 
					 $m++;
					 $k++;
	     }
		 for($linti=0;$linti<count($larrregdetails);$linti++)
		  {
			$larrregdetails[$linti]['Pulled'] = $resultedarray1[$linti];
			$larrregdetails[$linti]['Pushed'] = $resultedarray2[$linti];
			$larrregdetails[$linti]['NotPulled'] = $larrregdetails[$linti]['Registered']-$larrregdetails[$linti]['Pulled'];
			$larrregdetails[$linti]['NotPushed'] = $larrregdetails[$linti]['Pulled']-$larrregdetails[$linti]['Pushed'];
		  }
    	$larrColorArray['blue'] =  Array();
				$larrColorArray['red'] =  Array();
				$larrColorArray['black'] =  Array();
				foreach ($larrregdetails as $lobjCountry )
				{
		        	if($lobjCountry['Pulled']>0 && $lobjCountry['Pushed']>0)
		        	{
		        		$larrColorArray['blue'][] = $lobjCountry['idcenter'];
		        	}elseif($lobjCountry['Pulled']>0 && $lobjCountry['Pushed'] <= 0 )
		        	{
		        		$larrColorArray['red'][] = $lobjCountry['idcenter'];
		        	}else
		        	{
		        		$larrColorArray['black'][] = $lobjCountry['idcenter'];
		        	}
	            }
		
	    $sum_pulled = array_sum($resultedarray1);
		$sum_pushed = array_sum($resultedarray2);
		$sum_reg = $larrformData['reg'];
		$sum_notpulled = $larrformData['notpull'];
		$sum_notpushed = $larrformData['notpush'];        
	    $day = date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Pullpushdata_Report_'.$ldtedate;
		$ReportName = $this->view->translate( "Pullpushdata" ).' '.$this->view->translate( "Report" );
		$tabledata = '';
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" /><br><br>';		
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 1><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 1><b> Time</b></td><td align=left colspan = 2><b>$time</b></td></tr>";
        $tabledata.= "<tr><td align=left colspan = 2><b>Exam Date </b></td><td align=left colspan = 2><b>".$frmdate."</b></td><td  align=left colspan = 2><b> Exam Centre </b></td><td align=left colspan = 2><b>".$larrformData['CentreName']."</b></td></tr></table>";
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 6><b> {$ReportName}</b></td></tr></table><br>";
	    $tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Exam Centre</b></th><th><b>Local Server</b></th><th><b>Exam Session</b></th><th><b>Registered</b></th><th><b> To Local Server</b></th><th><b>Not Pulled</b></th><th><b> Pushed </b></th><th><b>Not Pushed</b></th></tr>";
	    //echo "<pre>";print_r($larrregdetails);die();	
	    $centerarray = array();
	    foreach($larrregdetails as $lobjCountry)
		{
			$tabledata.="<tr>";
			 if(in_array($lobjCountry['idcenter'],$larrColorArray['blue'])){
				 if(!in_array($lobjCountry['centername'],$centerarray))
				{
					$centerarray[] = $lobjCountry['centername'];
					$centerarrays[$lobjCountry['centername']] = array();
					$tabledata.= "<td style='font-family:verdana;color: #0000FF'>".$lobjCountry['centername']."</td>";
				}
				else{
					$tabledata.= "<td></td>";
				}
			 }elseif(in_array($lobjCountry['idcenter'],$larrColorArray['red'])){
			 if(!in_array($lobjCountry['centername'],$centerarray))
				{
					$centerarray[] = $lobjCountry['centername'];
					$centerarrays[$lobjCountry['centername']] = array();
					$tabledata.= "<td style='font-family:verdana;color: #FF0000'>".$lobjCountry['centername']."</td>";
				}
			 else{
					$tabledata.= "<td></td>";
				}
			 }else{ 
			 	if(!in_array($lobjCountry['centername'],$centerarray)){
			 	    $centerarray[] = $lobjCountry['centername'];
					$centerarrays[$lobjCountry['centername']] = array();
					$tabledata.= "<td>".$lobjCountry['centername']."</td>";
			 }else{
			 	$tabledata.= "<td></td>";
			 }
			 }
			 if(!in_array($lobjCountry['centername'],$iparray))
				{
					$iparray[] = $lobjCountry['centername'];
					$iparrays[$lobjCountry['centername']] = array();
					$tabledata.= "<td >".$lobjCountry['Localserver']."</td>";
				}
			else{
					$tabledata.= "<td></td>";
				}
			  if($lobjCountry['Pulled']>0 && $lobjCountry['Pushed']>0){
			  	$tabledata.="<td align = 'left' style='font-family:verdana;color: #0000FF;'>".$lobjCountry['managesessionname']."</td>";
			  }elseif(($lobjCountry['Pulled']>0 && $lobjCountry['Pushed']<=0)){
			  	$tabledata.="<td align = 'left' style='font-family:verdana;color: #FF0000;'>".$lobjCountry['managesessionname']."</td>";
			  }else{
			  	$tabledata.="<td align = 'left'>".$lobjCountry['managesessionname']."</td>";
			  }
			$tabledata.="<td align = 'left'>".$lobjCountry['Registered']."</td>";
			if($lobjCountry['Pulled']>0){
				if($lobjCountry['Pulled']==$lobjCountry['Pushed']){
					$tabledata.="<td align = 'left' style='color: #0000FF'>".$lobjCountry['Pulled']."</td>";
				}else{
					$tabledata.="<td align = 'left' style='color: #FF0000'>".$lobjCountry['Pulled']."</td>";
				}
			}else{
			    $tabledata.="<td align = 'left'>".$lobjCountry['Pulled']."</td>";	
			}
			$tabledata.="<td align = 'left' >".$lobjCountry['NotPulled']."</td>";
			if($lobjCountry['Pushed']>0){
				$tabledata.="<td align = 'left' style='color: #0000FF'>".$lobjCountry['Pushed']."</td>";
			}else{
			    $tabledata.="<td align = 'left'>".$lobjCountry['Pushed']."</td>";	
			}
			if($lobjCountry['NotPushed']>0){
				$tabledata.="<td align = 'left' style='color: #FF0000'>".$lobjCountry['NotPushed']."</td>";
			}else{
			    $tabledata.="<td align = 'left'>".$lobjCountry['NotPushed']."</td>";	
			}
		}
		$tabledata.="</tr><tr><td></td><td></td><td><b>Total</b></td>
		                 <td><b>$sum_reg</b></td>
		                 <td><b>$sum_pulled</b></td>
		                 <td><b>$sum_notpulled</b></td>
		                 <td><b>$sum_pushed</b></td>
		                 <td><b>$sum_notpushed</b></td>
		            </tr>";
		$tabledata.="</table>";
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		ini_set('max_execution_time',3600);
		$mpdf->WriteHTML($tabledata);
		$mpdf->Output($filename.pdf,'D');
    }
	public function printdetails2Action()
    {   
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$lintidcenter =  $this->_getParam('idcenter');
		$ldtedate = $this->_getParam('edate');
		$lstrtype = $this->_getParam('type'); 
		$lintidsess = $this->_getParam('idses');
		$result = array();
		switch($lstrtype)
		{
			case 'Pulled': $result = $this->lobjmodel->fngetpulleddetails($lintidcenter,$ldtedate,$lintidsess);
			               break;
			case 'NotPulled': $result = $this->lobjmodel->fngetpullfaileddetails($lintidcenter,$ldtedate,$lintidsess);
			               break;
			case 'Pushed': $result = $this->lobjmodel->fngetpusheddetails($lintidcenter,$ldtedate,$lintidsess);
			               break;
			case 'NotPushed': $result = $this->lobjmodel->fngetpushfaileddetails($lintidcenter,$ldtedate,$lintidsess);
			                   break;
			default: break;
		}
		$day = date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Pullpushdata_Report_'.$ldtedate;
		$ReportName = $this->view->translate( "Pullpushdata" ).' '.$this->view->translate( "Report" );
		$tabledata = '';
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" /><br>';		
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 1><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 1><b> Time</b></td><td align=left colspan = 2><b>$time</b></td></tr></table>";
        $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 6><b> {$ReportName}</b></td></tr></table><br>";
    	$tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>ICNO</b></th><th><b>Registered ID</b></th><th><b>Program</b></th><th><b>Exam Venue</b></th></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th><th><b>Result</b></th><th><b>Status</b></th></tr>";
		foreach($result as $lobjCountry)
		{
			$tabledata.="<tr><td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".$lobjCountry['RegisteredID']."</td><td align = 'left'>".$lobjCountry['ProgramName']."</td><td align = 'left'>".$lobjCountry['ExamVenue']."</td><td align = 'left'>".$lobjCountry['ExamDate']."</td><td align = 'left'>".$lobjCountry['managesessionname']."</td></td><td align = 'left'>".$lobjCountry['Result']."</td><td align = 'left'>".$lstrtype."</td></tr>";
		}
		$tabledata.="</table></form><br>";
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		ini_set('max_execution_time',3600);
		$mpdf->WriteHTML($tabledata);
		$mpdf->Output($filename.pdf,'D');
    }
}