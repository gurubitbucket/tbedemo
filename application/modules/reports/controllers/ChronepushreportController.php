<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
class Reports_chronepushreportController extends Base_Base 
{	
    public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->ChronepushreportModel = new Reports_Model_DbTable_Chronepushreport();
		$this->Chronepushreport = new  Reports_Form_Chronepushreport(); 
		$this->lobjCommon = new App_Model_Common();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$index=0;
		$this->view->id = $index;
		$this->view->Chronepushreport = $this->Chronepushreport;
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$larrresult = array();
$this->view->results = $larrresult;
		
		 if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {
		    	$index=1;
		$this->view->id = $index;
		    	$larrformData = $this->_request->getPost ();
		    	$fromdate = $larrformData['Date'];
		    	$todate = $larrformData['Date2'];
		    	$this->view->Chronepushreport->Date->setValue($fromdate);
		    	$this->view->Chronepushreport->Date2->setValue($todate);
		    		$this->view->Chronepushreport->Changetype->setValue($larrformData['Changetype']);
		    	$this->view->viewtable = $larrformData['Changetype'];
		    	if($larrformData['Changetype']==1)
		    	{
		    	$larrresult = $this->ChronepushreportModel->fngetchronepushdatas($fromdate,$todate);
		    	}
		    if($larrformData['Changetype']==2)
		    	{
		    	$larrresult = $this->ChronepushreportModel->fngetchronescheduler($fromdate,$todate);
		    	}
		    if($larrformData['Changetype']==3)
		    	{
		    	$larrresult = $this->ChronepushreportModel->fngetchronequestions($fromdate,$todate);
		    	}
		    	//print_r($larrresult);
		    	$this->view->results = $larrresult;
		    }
	 }
	 
	 public function fnexportexcelAction()
	 {
	 	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbe/images/reportheader.jpg";
   	    $dymfromdate = $this->_getParam('fromdate');
   	       	    $dmytodate = $this->_getParam('todate');
   	    $chronetype = $this->_getParam('chronetype');

   	    $fromdate = date('Y-m-d',strtotime($dymfromdate));
   	     $todate = date('Y-m-d',strtotime($dmytodate));
   	     if($chronetype==1)
   	     {
   	     	 $filename = 'Chrone_Push_Report_From'.$fromdate.'--'.$todate;        
		   	    $larrresults = $this->ChronepushreportModel->fngetchronepushdatas($fromdate,$todate);
		   	    $totalcount = count($larrresults);
		   	    $tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
		   	    $tabledata.= "<table border=1 width=100%>";
		   	    $tabledata.="<tr><th>Exam Date</th>
		   	    				 <th>Center Name</th>
		   	    				 <th>Scheduled Date</th>
		   	    				  <th>Scheduled Status</th>
		   	    				 <th>Scheduled Time</th>
		   	    				 <th>UpdDate</th>
		   	    				 <th>User</th></tr>";
		   	    for($i=0;$i<$totalcount;$i++)
		   	    {
		   	    	  $examdate = date('d-m-Y',strtotime($larrresults[$i]['Examdate']));
		   	    	  $centername = $larrresults[$i]['centername'];
		   	    	  $scheduleddate = date('d-m-Y',strtotime($larrresults[$i]['schdeduleddate']));
		   	    	  $scheduledtime = $larrresults[$i]['scheduledtime'];
		   	    	  $upddate =date('d-m-Y',strtotime($larrresults[$i]['UpdDate']));
		   	    	  $fName = $larrresults[$i]['fName'];
		   	    	  $schedulerstatus = $larrresults[$i]['Status'];
		   	    	  if($schedulerstatus==1)
		   	    	  {
		   	    	  	$statuss = "Success";
		   	    	  }
		   	    	  else if($schedulerstatus==2)
		   	    	  {
		   	    	  	$statuss="Fail";
		   	    	  }
		   	    	  else if($schedulerstatus==0)
		   	    	  {
		   	    	  	$statuss="Not chroned";
		   	    	  }
		   	    	 $tabledata.="<tr><td>$examdate</td>
		   	    				 <td>$centername</td>
		   	    				 <td>$scheduleddate</td>
		   	    				  <td>$statuss</td>
		   	    				  <td>$scheduledtime</td>
		   	    				 <td>$upddate</td>
		   	    				 <td>$fName</td>";
		   	    }
		   	    $tabledata.="</tr></table>";
   	     }else if($chronetype==2){
   	     	$filename = 'Scheduler_Push_Report_From'.$fromdate.'--'.$todate;
   	     		$larrresults = $this->ChronepushreportModel->fngetchronescheduler($fromdate,$todate);
		   	    $totalcount = count($larrresults);
		   	    $tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
		   	    $tabledata.= "<table border=1 width=100%>";
		   	    $tabledata.="<tr>
		   	    				 <th>Center Name</th>
		   	    				 <th>Scheduled Date</th>
		   	    				  <th>Scheduled Status</th>
		   	    				  <th>Scheduled From Date</th>
		   	    				  <th>Scheduled To Date</th>
		   	    				 <th>Scheduled Time</th>
		   	    				 <th>UpdDate</th>
		   	    				 <th>User</th></tr>";
		   	    for($i=0;$i<$totalcount;$i++)
		   	    {
		   	    	 
		   	    	  $centername = $larrresults[$i]['centername'];
		   	    	  $scheduleddate = date('d-m-Y',strtotime($larrresults[$i]['schdeduleddate']));
		   	    	  $scheduledtime = $larrresults[$i]['scheduledtime'];
		   	    	  $upddate =date('d-m-Y',strtotime($larrresults[$i]['UpdDate']));
		   	    	  $fName = $larrresults[$i]['fName'];
		   	    	  $schedulerstatus = $larrresults[$i]['Status'];
		   	    	  $scheduledfromdate = date('d-m-Y',strtotime($larrresults[$i]['Schedulerfromdate']));
		   	    	   $scheduledtodate = date('d-m-Y',strtotime($larrresults[$i]['Schedulertodate']));
		   	    	  if($schedulerstatus==1)
		   	    	  {
		   	    	  	$statuss = "Success";
		   	    	  }
		   	    	  else if($schedulerstatus==2)
		   	    	  {
		   	    	  	$statuss="Fail";
		   	    	  }
		   	    	  else if($schedulerstatus==0)
		   	    	  {
		   	    	  	$statuss="Not chroned";
		   	    	  }
		   	    	 $tabledata.="<tr>
		   	    				 <td>$centername</td>
		   	    				 <td>$scheduleddate</td>
		   	    				  <td>$statuss</td>
		   	    				   <td>$scheduledfromdate</td>
		   	    				  <td>$scheduledtodate</td>
		   	    				  <td>$scheduledtime</td>
		   	    				 <td>$upddate</td>
		   	    				 <td>$fName</td>";
		   	    }
		   	    $tabledata.="</tr></table>";
   	     }else if($chronetype==3){
   	     	$filename = 'Questions_Push_Report_From'.$fromdate.'--'.$todate;
   	     			$larrresults = $this->ChronepushreportModel->fngetchronequestions($fromdate,$todate);
		   	    $totalcount = count($larrresults);
		   	    $tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
		   	    $tabledata.= "<table border=1 width=100%>";
		   	    $tabledata.="<tr>
		   	    				 <th>Center Name</th>
		   	    				 <th>Scheduled Date</th>
		   	    				  <th>Scheduled Status</th>
		   	    				 <th>Scheduled Time</th>
		   	    				 <th>UpdDate</th>
		   	    				 <th>User</th></tr>";
		   	    for($i=0;$i<$totalcount;$i++)
		   	    {
		   	    	 
		   	    	  $centername = $larrresults[$i]['centername'];
		   	    	  $scheduleddate = date('d-m-Y',strtotime($larrresults[$i]['schdeduleddate']));
		   	    	  $scheduledtime = $larrresults[$i]['scheduledtime'];
		   	    	  $upddate =date('d-m-Y',strtotime($larrresults[$i]['UpdDate']));
		   	    	  $fName = $larrresults[$i]['fName'];
		   	    	  $schedulerstatus = $larrresults[$i]['Status'];
		   	    	  if($schedulerstatus==1)
		   	    	  {
		   	    	  	$statuss = "Success";
		   	    	  }
		   	    	  else if($schedulerstatus==2)
		   	    	  {
		   	    	  	$statuss="Fail";
		   	    	  }
		   	    	  else if($schedulerstatus==0)
		   	    	  {
		   	    	  	$statuss="Not chroned";
		   	    	  }
		   	    	 $tabledata.="<tr>
		   	    				 <td>$centername</td>
		   	    				 <td>$scheduleddate</td>
		   	    				  <td>$statuss</td>
		   	    				  <td>$scheduledtime</td>
		   	    				 <td>$upddate</td>
		   	    				 <td>$fName</td>";
		   	    }
		   	    $tabledata.="</tr></table>";
   	     }
   	     
   	     
   	          
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
   	    
	 }
	 
	 public function selectoptionAction()
	 {
	 	
	 }
 }