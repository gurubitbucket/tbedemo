<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
class Reports_chronepullreportController extends Base_Base 
{	
    public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->ChronepullreportModel = new Reports_Model_DbTable_Chronepullreport();
		$this->Chronepullreport = new  Reports_Form_Chronepullreport(); 
		$this->lobjCommon = new App_Model_Common();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->Chronepullreport = $this->Chronepullreport;
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$larrresult = array();
$this->view->results = $larrresult;
		
		 if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {
		    	$larrformData = $this->_request->getPost ();
		    	$fromdate = $larrformData['Date'];
		    	$todate = $larrformData['Date2'];
		    	$this->view->Chronepullreport->Date->setValue($fromdate);
		    	$this->view->Chronepullreport->Date2->setValue($todate);
		    	$larrresult = $this->ChronepullreportModel->fngetchronepulldatas($fromdate,$todate);
		    	$this->view->results = $larrresult;
		    	$larrresults = $this->ChronepullreportModel->fngetchronepulldatasautos($fromdate,$todate);
		    	$this->view->resultss = $larrresults;
		    }
	 }
	 
	 public function fnexportexcelAction()
	 {
	 	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbe/images/reportheader.jpg";
   	    $dymfromdate = $this->_getParam('fromdate');
   	       	    $dmytodate = $this->_getParam('todate');

   	    $fromdate = date('Y-m-d',strtotime($dymfromdate));
   	     $todate = date('Y-m-d',strtotime($dmytodate));

   	     	 $filename = 'Chrone_Push_Report_From'.$fromdate.'--'.$todate;        
		   	    $larrresults = $this->ChronepullreportModel->fngetchronepulldatas($fromdate,$todate);
		   	    $larrresultsss = $this->ChronepullreportModel->fngetchronepulldatasautos($fromdate,$todate);
		   	    $totalcounts = count($larrresultsss);
		   	    $totalcount = count($larrresults);
		   	    $tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
		   	    if($totalcount>0)
		   	    {
		   	    $tabledata.= "<table border=1 width=100%>";
		   	    $tabledata.="<tr><th style='width:12.5%'>Exam Date</th>
		   	    				 <th style='width:12.5%'>Center Name</th>
		   	    				  <th style='width:12.5%'>Exam Session</th>
		   	    				 <th style='width:12.5%'>Scheduled Date</th>
		   	    				  <th style='width:12.5%'>Scheduled Status</th>
		   	    				 <th style='width:12.5%'>Scheduled Time</th>
		   	    				 <th style='width:12.5%'>UpdDate</th>
		   	    				 <th style='width:12.5%'>User</th></tr>";
		   	    for($i=0;$i<$totalcount;$i++)
		   	    {
		   	    	  $examdate = date('d-m-Y',strtotime($larrresults[$i]['Examdate']));
		   	    	  $centername = $larrresults[$i]['centername'];
		   	    	   $managesessionname = $larrresults[$i]['managesessionname'];
		   	    	  $scheduleddate = date('d-m-Y',strtotime($larrresults[$i]['schdeduleddate']));
		   	    	  $scheduledtime = $larrresults[$i]['scheduledtime'];
		   	    	  $upddate =date('d-m-Y',strtotime($larrresults[$i]['UpdDate']));
		   	    	  $fName = $larrresults[$i]['fName'];
		   	    	  $schedulerstatus = $larrresults[$i]['Status'];
		   	    	  if($schedulerstatus==1)
		   	    	  {
		   	    	  	$statuss = "Success";
		   	    	  }
		   	    	  else if($schedulerstatus==2)
		   	    	  {
		   	    	  	$statuss="Fail";
		   	    	  }
		   	    	  else if($schedulerstatus==0)
		   	    	  {
		   	    	  	$statuss="Not chroned";
		   	    	  }
		   	    	 $tabledata.="<tr><td>$examdate</td>
		   	    				 <td>$centername</td>
		   	    				  <td>$managesessionname</td>
		   	    				 <td>$scheduleddate</td>
		   	    				  <td>$statuss</td>
		   	    				  <td>$scheduledtime</td>
		   	    				 <td>$upddate</td>
		   	    				 <td>$fName</td>";
		   	    }
		   	    $tabledata.="</tr></table>";
		   	    }
		   	    
		   	    if($totalcounts>0)
		   	    {
		   	    	 $tabledata.= "<table border=1 width=100%>";
		   	    $tabledata.="<tr><th style='width:12.5%'>Exam Date</th>
		   	    				 <th style='width:12.5%'>Center Name</th>
		   	    				  <th style='width:12.5%'>Exam Session</th>
		   	    				 <th style='width:12.5%'>Scheduled Date</th>
		   	    				  <th style='width:12.5%'>Scheduled Status</th>
		   	    				 <th style='width:12.5%'>Scheduled Time</th>
		   	    				 <th style='width:12.5%'>UpdDate</th>
		   	    				 <th style='width:12.5%'>Auto</th></tr>";
		   	    for($i=0;$i<$totalcounts;$i++)
		   	    {
		   	    	  $examdate = date('d-m-Y',strtotime($larrresultsss[$i]['Examdate']));
		   	    	  $centername = $larrresultsss[$i]['centername'];
		   	    	   $managesessionname = $larrresultsss[$i]['managesessionname'];
		   	    	  $scheduleddate = date('d-m-Y',strtotime($larrresultsss[$i]['schdeduleddate']));
		   	    	  $scheduledtime = $larrresultsss[$i]['scheduledtime'];
		   	    	  $upddate =date('d-m-Y',strtotime($larrresultsss[$i]['UpdDate']));
		   	    	  $fName = $larrresultsss[$i]['fName'];
		   	    	  $schedulerstatus = $larrresultsss[$i]['Status'];
		   	    	  if($schedulerstatus==1)
		   	    	  {
		   	    	  	$statuss = "Success";
		   	    	  }
		   	    	  else if($schedulerstatus==2)
		   	    	  {
		   	    	  	$statuss="Fail";
		   	    	  }
		   	    	  else if($schedulerstatus==0)
		   	    	  {
		   	    	  	$statuss="Not chroned";
		   	    	  }
		   	    	 $tabledata.="<tr><td>$examdate</td>
		   	    				 <td>$centername</td>
		   	    				  <td>$managesessionname</td>
		   	    				 <td>$scheduleddate</td>
		   	    				  <td>$statuss</td>
		   	    				  <td>$scheduledtime</td>
		   	    				 <td>$upddate</td>
		   	    				 <td>Automated</td>";
		   	    }
		   	    $tabledata.="</tr></table>";
		   	    }
		   	    
   	  
   	     
   	          
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
   	    
	 }
	 
	 public function selectoptionAction()
	 {
	 	
	 }
 }