<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_AllstatisticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjallstatistics = new Reports_Model_DbTable_Allstatisticsmodel;
   	    $this->lobjStasticsForm = new  Reports_Form_Statisticsnew();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjStasticsForm;
		$year = 2012;
		$max = $year+100;
		for($linti=$year;$linti<$max;$linti++){
			$larryears[$linti] = $linti;
		} 
		$this->lobjStasticsForm->Year->addmultioptions($larryears);
		$ldtdate = getdate();
		$lintmonthnumber = $ldtdate['mon']-1;
		$this->lobjStasticsForm->Month->setValue($lintmonthnumber); 
		if ($this->_request->isPost () && $this->_request->getPost('Search')) 
		    {   
		    	$larrformData = $this->_request->getPost();
		    	//print_r($larrformData);die();
				if ($this->lobjform->isValid($larrformData)) 
				{
				   $larrformData = $this->_request->getPost();
				   $lintmon = $larrformData['Month'];
				   $lintyear = $larrformData['Year'];
				   $linttype = $larrformData['Type'];
				   $lstrmonthname = date( 'F', mktime(0,0,0,$lintmon));
				   $this->view->month = $lintmon;
				   $this->view->monthname = $lstrmonthname;
				   $this->view->year = $lintyear;
				   $lintagetype = 0;$lintcategorytype = 0;$lintcentretype = 0;$lintresulttype = 0;$lintgendertype = 0;$lintracetype = 0; $lintqualificationtype = 0;
				   switch($linttype)
				   {
				   	case 1: $lintagetype = 1;
				   	        break;
				    case 2: $lintcategorytype = 1;
				   	        break;
				   	case 3: $lintcentretype = 1;
				   	        break;
				   	case 4: $lintresulttype = 1;
				   	        break;
				   	case 5: $lintgendertype = 1;
				   	        break;
				   	case 6: $lintracetype = 1;
				   	        break;
				   	case 7: $lintqualificationtype = 1;
				   	        break;
				   	default:$lintagetype = 1;$lintcategorytype = 1;$lintcentretype = 1;$lintresulttype = 1;$lintgendertype = 1;$lintracetype = 1; $lintqualificationtype = 1;
				   	        break;
				   }
				   $this->view->linttype = $linttype; 
				   $this->view->lintagetype = $lintagetype; 
				   $this->view->lintcategorytype = $lintcategorytype;
				   $this->view->lintcentretype = $lintcentretype;
				   $this->view->lintresulttype = $lintresulttype; 
				   $this->view->lintgendertype = $lintgendertype;
				   $this->view->lintracetype = $lintracetype;
				   $this->view->lintqualificationtype = $lintqualificationtype;
//////////////////// Age/////////////////
		if($lintagetype==1)
		{      
				$larrages = $this->lobjallstatistics->fngetages($lintmon,$lintyear);
				$lintagecount = count($larrages);
				$larragescount['18-21']=0;$larragescount['22-25']=0;$larragescount['26-29']=0;$larragescount['30-34']=0;$larragescount['35-39']=0;$larragescount['40-44']=0;$larragescount['45-49']=0;$larragescount['50-54']=0;
				$larragescount['55-59']=0;$larragescount['60Above']=0;
				for($linti=0;$linti<$lintagecount;$linti++){
					if($larrages[$linti]['Age']>=18 && $larrages[$linti]['Age']<=21){
						$larragescount['18-21']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['18-21sat']++;
							$larragescount['18-21pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['18-21sat']++;
							$larragescount['18-21fail']++;
						}else{
							$larragescount['18-21absent']++;
						}
					}elseif($larrages[$linti]['Age']>=22 && $larrages[$linti]['Age']<=25){
						$larragescount['22-25']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['22-25sat']++;
							$larragescount['22-25pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['22-25sat']++;
							$larragescount['22-25fail']++;
						}else{
							$larragescount['22-25absent']++;
						}
					}
					elseif($larrages[$linti]['Age']>=26 && $larrages[$linti]['Age']<=29){
						$larragescount['26-29']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['26-29sat']++;
							$larragescount['26-29pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['26-29sat']++;
							$larragescount['26-29fail']++;
						}else{
							$larragescount['26-29absent']++;
						}
					}elseif($larrages[$linti]['Age']>=30 && $larrages[$linti]['Age']<=34){
						$larragescount['30-34']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['30-34sat']++;
							$larragescount['30-34pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['30-34sat']++;
							$larragescount['30-34fail']++;
						}else{
							$larragescount['30-34absent']++;
						}
					}elseif($larrages[$linti]['Age']>=35 && $larrages[$linti]['Age']<=39){
						$larragescount['35-39']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['35-39sat']++;
							$larragescount['35-39pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['35-39sat']++;
							$larragescount['35-39fail']++;
						}else{
							$larragescount['35-39absent']++;
						}
					}elseif($larrages[$linti]['Age']>=40 && $larrages[$linti]['Age']<=44){
						$larragescount['40-44']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['40-44sat']++;
							$larragescount['40-44pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['40-44sat']++;
							$larragescount['40-44fail']++;
						}else{
							$larragescount['40-44absent']++;
						}
					}elseif($larrages[$linti]['Age']>=45 && $larrages[$linti]['Age']<=49){
						$larragescount['45-49']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['45-49sat']++;
							$larragescount['45-49pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['45-49sat']++;
							$larragescount['45-49fail']++;
						}else{
							$larragescount['45-49absent']++;
						}
					}elseif($larrages[$linti]['Age']>=50 && $larrages[$linti]['Age']<=54){
						$larragescount['50-54']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['50-54sat']++;
							$larragescount['50-54pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['50-54sat']++;
							$larragescount['50-54fail']++;
						}else{
							$larragescount['50-54absent']++;
						}
					}elseif($larrages[$linti]['Age']>=55 && $larrages[$linti]['Age']<=59){
						$larragescount['55-59']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['55-59sat']++;
							$larragescount['55-59pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['55-59sat']++;
							$larragescount['55-59fail']++;
						}else{
							$larragescount['55-59absent']++;
						}
					}else{
						$larragescount['60Above']++;
						if($larrages[$linti]['pass']==1){
							$larragescount['60Abovesat']++;
							$larragescount['60Abovepass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['60Abovesat']++;
							$larragescount['60Abovefail']++;
				}else{
					$larragescount['60Aboveabsent']++;
				}
			}
		}
		$larragescount['regtotal']=  $larragescount['18-21']+$larragescount['22-25']+$larragescount['26-29']+$larragescount['30-34']+$larragescount['35-39']+$larragescount['40-44']+$larragescount['45-49']+$larragescount['50-54']+$larragescount['55-59']+$larragescount['60Above'];
		$larragescount['sattotal']=  $larragescount['18-21sat']+$larragescount['22-25sat']+$larragescount['26-29sat']+$larragescount['30-34sat']+$larragescount['35-39sat']+$larragescount['40-44sat']+$larragescount['45-49sat']+$larragescount['50-54sat']+$larragescount['55-59sat']+$larragescount['60Abovesat'];
		$larragescount['passtotal']=  $larragescount['18-21pass']+$larragescount['22-25pass']+$larragescount['26-29pass']+$larragescount['30-34pass']+$larragescount['35-39pass']+$larragescount['40-44pass']+$larragescount['45-49pass']+$larragescount['50-54pass']+$larragescount['55-59pass']+$larragescount['60Abovepass'];
		$larragescount['failtotal']=  $larragescount['18-21fail']+$larragescount['22-25fail']+$larragescount['26-29fail']+$larragescount['30-34fail']+$larragescount['35-39fail']+$larragescount['40-44fail']+$larragescount['45-49fail']+$larragescount['50-54fail']+$larragescount['55-59fail']+$larragescount['60Abovefail'];
		$larragescount['absenttotal']=  $larragescount['18-21absent']+$larragescount['22-25absent']+$larragescount['26-29absent']+$larragescount['30-34absent']+$larragescount['35-39absent']+$larragescount['40-44absent']+$larragescount['45-49absent']+$larragescount['50-54absent']+$larragescount['55-59absent']+$larragescount['60Aboveabsent'];
		$this->view->larragescount = $larragescount;
		}
		////////// Result///////////
		if($lintresulttype==1)
		{
		$larrprograms = $this->lobjallstatistics->fngetresultprogramnames();
		for($linti=0;$linti<count($larrprograms);$linti++)
		{
			$larrresult[$linti] = $this->lobjallstatistics->fngetresultgrade($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
			$larrregdetails[$linti] = $this->lobjallstatistics->fngetabsentdetails($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
			$lintgradeA[$linti] = 0;$lintgradeB[$linti] = 0;$lintgradeC[$linti] = 0;$lintgradeF[$linti] = 0;
			for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++){
				if($larrresult[$linti][$lintj]['Grade']=='A'){
					$lintgradeA[$linti]++;
				}elseif($larrresult[$linti][$lintj]['Grade']=='B'){
					$lintgradeB[$linti]++;
				}elseif($larrresult[$linti][$lintj]['Grade']=='C'){
					$lintgradeC[$linti]++;
				}else{
					$lintgradeF[$linti]++;
				}
			}
			$lintabsent[$linti] = $larrregdetails[$linti]['NoOfCandidates'] -($lintgradeA[$linti]+$lintgradeB[$linti]+$lintgradeC[$linti]+$lintgradeF[$linti]);
		}
		$this->view->regdetails = $larrregdetails;
		$this->view->programs = $larrprograms;
		$this->view->gradeA = $lintgradeA;
		$this->view->gradeB = $lintgradeB;
		$this->view->gradeC = $lintgradeC;
		$this->view->resultfail = $lintgradeF;
		$this->view->resultabsent = $lintabsent;
		}
/////////// Gender/////////////////
		if($lintgendertype==1){
		$lintmaleid = 1;$lintfemaleid = 0;
		$larrracescount = $this->lobjallstatistics->fngetgenderscount($lintmon,$lintyear);
		$lintregisteredtotal = $larrracescount[0]['NoOfCandidates']+$larrracescount[1]['NoOfCandidates'];
		$larrracescount[2]['regtotal1'] = $lintregisteredtotal;;
		$larrmaleresult = $this->lobjallstatistics->fngetgenderresult($lintmon,$lintyear,$lintmaleid);
		$lintmalecount = count($larrmaleresult);
		$lintpass = 0;
		$lintfail = 0;
		$lintabsent = 0;
		for($linti=0;$linti<$lintmalecount;$linti++){
			if($larrmaleresult[$linti]['pass']==1)
			{
				$lintpass++;
			}elseif ($larrmaleresult[$linti]['pass']==2){
				$lintfail++;
			}else{
				$lintabsent++;
			}
		}
		$larrracescount[]['msat'] = $lintpass+$lintfail;
		$larrracescount[]['mpass'] = $lintpass;
		$larrracescount[]['mfail']= $lintfail;
		$larrracescount[]['mabsent']= $larrracescount[1]['NoOfCandidates']-($lintpass+$lintfail);//$lintabsent;
		$larrfemaleresult = $this->lobjallstatistics->fngetgenderresult($lintmon,$lintyear,$lintfemaleid);
		$lintfemalecount = count($larrfemaleresult);
		$lintpass = 0;
		$lintfail = 0;
		$lintabsent = 0;
		for($linti=0;$linti<$lintfemalecount;$linti++){
			if($larrfemaleresult[$linti]['pass']==1)
			{
				$lintpass++;
			}elseif ($larrfemaleresult[$linti]['pass']==2){
				$lintfail++;
			}else{
				$lintabsent++;
			}
		}
		$larrracescount[]['fsat'] = $lintpass+$lintfail;
		$larrracescount[]['fpass'] = $lintpass;
		$larrracescount[]['ffail']= $lintfail;
		$larrracescount[]['fabsent']= $larrracescount[0]['NoOfCandidates']-($lintpass+$lintfail);//$lintabsent;
		$larrracescount[]['sattotal']=  $larrracescount[3]['msat']+$larrracescount[7]['fsat'];
		$larrracescount[]['passtotal']=  $larrracescount[4]['mpass']+$larrracescount[8]['fpass'];
		$larrracescount[]['failtotal']=  $larrracescount[5]['mfail']+$larrracescount[9]['ffail'];
		$larrracescount[]['absenttotal']=  $larrracescount[6]['mabsent']+$larrracescount[10]['fabsent'];
		$this->view->larrgendercount   =  $larrracescount;
		$this->view->paginator = $larrracescount;
		}
////// Qualification //////////
		if($lintqualificationtype==1)
		{
		$larrqualificationcount = $this->lobjallstatistics->fngetqualification($lintmon,$lintyear); //Call a function to get number of candidates registered for different qualification
		$larrqualification = $this->lobjallstatistics->fngetqualificationnames($lintmon,$lintyear); //Call a function to get different qualification names
		$this->view->larrqualificationcount=$larrqualificationcount;
		$lintregisteredtotal=0;
		for($linti=0;$linti<count($larrqualification);$linti++){
			$lintqualificationid= $larrqualification[$linti]['idDefinition'];
			$larrqualificationquali[$linti]  =	$larrqualification[$linti]['QualificationName'];
			$larrQualificationresult = $this->lobjallstatistics->fngetqualificationresult($lintmon,$lintyear,$lintqualificationid); //Call a function to get number of Diploma candidates details
			$lintregisteredtotal += $larrqualificationcount[$linti]['NoOfCandidates'];
			$lintcountQualification = count($larrQualificationresult);

			$lintqualificationreg[$linti]         = $larrqualificationcount[$linti]['NoOfCandidates'];
			$lintqualificationsat[$linti] = $lintcountQualification;
			$lintqualificationpass[$linti] = 0;
			$lintqualificationfail[$linti] = 0;
			$lintqualificationabsent[$linti]=0;

			for($lintj=0;$lintj<$lintcountQualification;$lintj++){
				if($larrQualificationresult[$lintj]['pass']==1)
				{
					$lintqualificationpass[$linti]++;
				}elseif ($larrQualificationresult[$lintj]['pass']==2){
					$lintqualificationfail[$linti]++;
				}else{
					$lintqualificationabsent[$linti]++;
				}
			}
			$lintqualificationabsent[$linti] = $larrqualificationcount[$linti]['NoOfCandidates']-$lintqualificationsat[$linti];
		}

		$this->view->larrquali   =  $larrqualificationquali; //sending all count of candidates to the view
		$this->view->lintreg     =  $lintqualificationreg; //sending all count of candidates to the view
		$this->view->lintsat   =  $lintqualificationsat; //sending all count of candidates to the view
		$this->view->lintpass   =  $lintqualificationpass; //sending all count of candidates to the view
		$this->view->lintfail   =  $lintqualificationfail; //sending all count of candidates to the view
		$this->view->lintabsent   =  $lintqualificationabsent; //sending all count of candidates to the view
		}
//////// Race  ////////
        if($lintracetype ==1){
        $larrracescount = $this->lobjallstatistics->fngetraces($lintmon,$lintyear);
        $larrraces = $this->lobjallstatistics->fngetracesnames($lintmon,$lintyear);

        for($linti=0;$linti<count($larrraces);$linti++){
        	$lintraceid = $larrraces[$linti]['idDefinition'];
        	$larrracename = $larrraces[$linti]['Race'];
        	$larrraceresult[$linti] = $this->lobjallstatistics->fngetracesresult($lintmon,$lintyear,$lintraceid);
        	$lintracecount[$linti] = count($larrraceresult[$linti]);
        	$lintracepass[$linti] = 0;
        	$lintracefail[$linti] = 0;
        	$lintraceabsent[$linti] = 0;
        	$lintracesat[$linti] = 0;
        	for($lintj=0;$lintj<$lintracecount[$linti];$lintj++){
        		if($larrraceresult[$linti][$lintj]['pass']==1)
        		{
        			$lintracepass[$linti]++;
        		}elseif ($larrraceresult[$linti][$lintj]['pass']==2){
        			$lintracefail[$linti]++;
        		}
        	}
        	$lintracesat[$linti] = $lintracepass[$linti]+$lintracefail[$linti];
        	$lintraceabsent[$linti] = $larrracescount[$linti]['NoOfCandidates'] - $lintracesat[$linti];
        }
        $this->view->racename = $larrracescount;
        $this->view->racenames = $larrraces;
        $this->view->racepass = $lintracepass;
        $this->view->racefail = $lintracefail;
        $this->view->raceabsent = $lintraceabsent;
        $this->view->racesat = $lintracesat;
        }
/////////// Category ////////////
        if($lintcategorytype == 1)
        {
        $larrprograms = $this->lobjallstatistics->fngetprogramnames();
        $larrprogramcount = $this->lobjallstatistics->fngetprograms($lintmon,$lintyear);
        for($linti=0;$linti<count($larrprograms);$linti++){
        	$lintpartabid = $larrprograms[$linti]['IdProgrammaster'];
        	$larrprogramname = $larrprograms[$linti]['ProgramName'];
        	$larrpartabresult[$linti] = $this->lobjallstatistics->fngetprogramresult($lintmon,$lintyear,$lintpartabid);
        	$lintprogrampartabcount[$linti] = count($larrpartabresult[$linti]);
        	$lintprogrampass[$linti] = 0;
        	$lintprogramfail[$linti] = 0;
        	$lintprogramabsent[$linti] = 0;
        	$lintprogramsat[$linti] = 0;
        	for($lintj=0;$lintj<$lintprogrampartabcount[$linti];$lintj++){
        		if($larrpartabresult[$linti][$lintj]['pass']==1)
        		{
        			$lintprogrampass[$linti]++;
        		}elseif ($larrpartabresult[$linti][$lintj]['pass']==2){
        			$lintprogramfail[$linti]++;
        		}
        	}
        	$lintprogramsat[$linti] = $lintprogrampass[$linti]+$lintprogramfail[$linti];
        	$lintprogramabsent[$linti] = $larrprogramcount[$linti]['NoOfCandidates'] - $lintprogramsat[$linti];
        }
        $this->view->programname = $larrprogramcount;
        $this->view->programs = $larrprograms;
        $this->view->pass = $lintprogrampass;
        $this->view->fail = $lintprogramfail;
        $this->view->absent = $lintprogramabsent;
        $this->view->sat = $lintprogramsat;
		}
		///////// Centre ///////////
		if($lintcentretype == 1){
		$larrcentrenames = $this->lobjallstatistics->fngetcentrenames($lintmon);
		for($linti=0;$linti<count($larrcentrenames);$linti++)
		{
			$larrresult[$linti] = $this->lobjallstatistics->fngetcentres($lintmon,$lintyear,$larrcentrenames[$linti]['idcenter']);
			$lintexamcentrereg[$linti] = count($larrresult[$linti]);
			$lintexamcentrepass[$linti] = 0;$lintexamcentrefail[$linti] = 0;$lintexamcentreabsent[$linti] = 0;$lintexamcentresat[$linti] = 0;
			for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++){
				if($larrresult[$linti][$lintj]['pass']==1){
					$lintexamcentrepass[$linti]++;
				}elseif($larrresult[$linti][$lintj]['pass']==2){
					$lintexamcentrefail[$linti]++;
				}else{
					$lintexamcentreabsent[$linti]++;
				}
			}
			$lintexamcentresat[$linti] = $lintexamcentrepass[$linti]+$lintexamcentrefail[$linti];
			$lintexamcentreabsent[$linti] = $lintexamcentrereg[$linti] -($lintexamcentrepass[$linti]+$lintexamcentrefail[$linti]);
		}
		$this->view->centredata = $lintexamcentrereg;
		$this->view->centres = $larrcentrenames;
		$this->view->centrepass = $lintexamcentrepass;
		$this->view->centrefail = $lintexamcentrefail;
		$this->view->centreabsent = $lintexamcentreabsent;
		$this->view->centresat = $lintexamcentresat;
		/////////	
	 }
		$this->lobjStasticsForm->Month->setValue($lintmon);
		$this->lobjStasticsForm->Year->setValue($lintyear);
		$year = 2012;
		$max = $year+100;
		for ($linti=$year;$linti<$max;$linti++){
			$larryears[$linti] = $linti;
		} 
		$this->lobjStasticsForm->Year->addmultioptions($larryears);
		$this->lobjStasticsForm->Type->setValue($linttype); 
   }
}
}

public function fnexportexcelAction()
{
	$this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$larrformData = $this->_request->getPost();
	$lintmon = $larrformData['lmonth'];
	$lintyear = $larrformData['lyear'];
	$linttype = $larrformData['linttype'];
	unset ( $larrformData['ExportToExcel']);
	$lday= date("d-m-Y");
	$ltime = date('h:i:s',time());
	$lstrmonthname = date( 'F', mktime(0, 0, 0, $lintmon));
	$lstrmonthyear = $lstrmonthname.'_'.$lintyear;
	
	$lintagetype = 0;$lintcategorytype = 0;$lintcentretype = 0;$lintresulttype = 0;$lintgendertype = 0;$lintracetype = 0; $lintqualificationtype = 0;
	switch($linttype)
		{
		  case 1: $lintagetype = 1;$filename = 'Age_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 2: $lintcategorytype = 1;$filename = 'Exam_Category_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 3: $lintcentretype = 1;$filename = 'Exam_Centre_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 4: $lintresulttype = 1;$filename = 'Result_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 5: $lintgendertype = 1;$filename = 'Gender_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 6: $lintracetype = 1;$filename = 'Race_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 7: $lintqualificationtype = 1;$filename = 'Qualification_Wise_Statistics_Report_'.$lstrmonthyear;	
				  break;
		  default:$lintagetype = 1;$lintcategorytype = 1;$lintcentretype = 1;$lintresulttype = 1;$lintgendertype = 1;$lintracetype = 1; $lintqualificationtype = 1;
				  $filename = 'All_Statistics_Report_'.$lstrmonthyear;
		          break;
	   }
	$host = $_SERVER['SERVER_NAME'];
	$imgp = "http://".$host."/tbenew/images/reportheader.jpg";  
	$tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
	$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2><b>Date </b></td><td align ='left' colspan = 4><b>$lday</b></td><td align ='left' colspan = 3><b> Time</b></td><td align ='left' colspan = 5><b>$ltime</b></td></tr></table>"; 
	
	if($lintagetype==1)
	{   
		$ReportName = $this->view->translate( "Age Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		$tabledata.="<table border=1 align=center width=100%><tr><td colspan = 14 align=center><b>{$ReportName}</b></td></tr></table><br>";
		$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 12><b>".$lstrmonthyear."</b></td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Age</b></td><td align ='center' colspan = 1><b>18-21</b></td><td align ='center' colspan = 1><b>22-25</b></td><td align ='center' colspan = 1><b>26-29</b></td><td align ='center' colspan = 1><b>30-34</b></td><td align ='center' colspan = 1><b>35-39</b></td><td align ='center' colspan = 1><b>40-44</b></td><td align ='center' colspan = 1><b>45-49</b></td><td align ='center' colspan = 1><b>50-54</b></td><td align ='center' colspan = 1><b>55-59</b></td><td align ='center' colspan = 1><b>60 Above</b></td><td align ='center' colspan = 2><b>Total</b></td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td><td align ='center' colspan = 1>".$larrformData['18-21']."</td><td align ='center' colspan = 1>".$larrformData['22-25']."</td><td align ='center' colspan = 1>".$larrformData['26-29']."</td><td align ='center' colspan = 1>".$larrformData['30-34']."</td><td align ='center' colspan = 1>".$larrformData['35-39']."</td><td align ='center' colspan = 1>".$larrformData['40-44']."</td><td align ='center' colspan = 1>".$larrformData['45-49']."</td><td align ='center' colspan = 1>".$larrformData['50-54']."</td><td align ='center' colspan = 1>".$larrformData['55-59']."</td><td align ='center' colspan = 1>".$larrformData['60Above']."</td><td align ='center' colspan = 2>".$larrformData['regtotal']."</td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td><td align ='center' colspan = 1>".$larrformData['18-21sat']."</td><td align ='center' colspan = 1>".$larrformData['22-25sat']."</td><td align ='center' colspan = 1>".$larrformData['26-29sat']."</td><td align ='center' colspan = 1>".$larrformData['30-34sat']."</td><td align ='center' colspan = 1>".$larrformData['35-39sat']."</td><td align ='center' colspan = 1>".$larrformData['40-44sat']."</td><td align ='center' colspan = 1>".$larrformData['45-49sat']."</td><td align ='center' colspan = 1>".$larrformData['50-54sat']."</td><td align ='center' colspan = 1>".$larrformData['55-59sat']."</td><td align ='center' colspan = 1>".$larrformData['60Abovesat']."</td><td align ='center' colspan = 2>".$larrformData['sattotal']."</td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td><td align ='center' colspan = 1>".$larrformData['18-21pass']."</td><td align ='center' colspan = 1>".$larrformData['22-25pass']."</td><td align ='center' colspan = 1>".$larrformData['26-29pass']."</td><td align ='center' colspan = 1>".$larrformData['30-34pass']."</td><td align ='center' colspan = 1>".$larrformData['35-39pass']."</td><td align ='center' colspan = 1>".$larrformData['40-44pass']."</td><td align ='center' colspan = 1>".$larrformData['45-49pass']."</td><td align ='center' colspan = 1>".$larrformData['50-54pass']."</td><td align ='center' colspan = 1>".$larrformData['55-59pass']."</td><td align ='center' colspan = 1>".$larrformData['60Abovepass']."</td><td align ='center' colspan = 2>".$larrformData['passtotal']."</td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td><td align ='center' colspan = 1>".$larrformData['18-21fail']."</td><td align ='center' colspan = 1>".$larrformData['22-25fail']."</td><td align ='center' colspan = 1>".$larrformData['26-29fail']."</td><td align ='center' colspan = 1>".$larrformData['30-34fail']."</td><td align ='center' colspan = 1>".$larrformData['35-39fail']."</td><td align ='center' colspan = 1>".$larrformData['40-44fail']."</td><td align ='center' colspan = 1>".$larrformData['45-49fail']."</td><td align ='center' colspan = 1>".$larrformData['50-54fail']."</td><td align ='center' colspan = 1>".$larrformData['55-59fail']."</td><td align ='center' colspan = 1>".$larrformData['60Abovefail']."</td><td align ='center' colspan = 2>".$larrformData['failtotal']."</td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td><td align ='center' colspan = 1>".$larrformData['18-21absent']."</td><td align ='center' colspan = 1>".$larrformData['22-25absent']."</td><td align ='center' colspan = 1>".$larrformData['26-29absent']."</td><td align ='center' colspan = 1>".$larrformData['30-34absent']."</td><td align ='center' colspan = 1>".$larrformData['35-39absent']."</td><td align ='center' colspan = 1>".$larrformData['40-44absent']."</td><td align ='center' colspan = 1>".$larrformData['45-49absent']."</td><td align ='center' colspan = 1>".$larrformData['50-54absent']."</td><td align ='center' colspan = 1>".$larrformData['55-59absent']."</td><td align ='center' colspan = 1>".$larrformData['60Aboveabsent']."</td><td align ='center' colspan = 2>".$larrformData['absenttotal']."</td></tr></table><br>";
	}
	
	if($lintcategorytype == 1)
        {
	        $ReportName = $this->view->translate( "Exam Category Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
        	$larrprograms = $this->lobjallstatistics->fngetprogramnames();
	        $larrprogramcount = $this->lobjallstatistics->fngetprograms($lintmon,$lintyear);
	        for($linti=0;$linti<count($larrprograms);$linti++){
	        	$lintpartabid = $larrprograms[$linti]['IdProgrammaster'];
	        	$larrprogramname = $larrprograms[$linti]['ProgramName'];
	        	$larrpartabresult[$linti] = $this->lobjallstatistics->fngetprogramresult($lintmon,$lintyear,$lintpartabid);
	        	$lintprogrampartabcount[$linti] = count($larrpartabresult[$linti]);
	        	$lintprogrampass[$linti] = 0;
	        	$lintprogramfail[$linti] = 0;
	        	$lintprogramabsent[$linti] = 0;
	        	$lintprogramsat[$linti] = 0;
	        	for($lintj=0;$lintj<$lintprogrampartabcount[$linti];$lintj++){
	        		if($larrpartabresult[$linti][$lintj]['pass']==1)
	        		{
	        			$lintprogrampass[$linti]++;
	        		}elseif ($larrpartabresult[$linti][$lintj]['pass']==2){
	        			$lintprogramfail[$linti]++;
	        		}
	        	}
	        	$lintprogramsat[$linti] = $lintprogrampass[$linti]+$lintprogramfail[$linti];
	        	$lintprogramabsent[$linti] = $larrprogramcount[$linti]['NoOfCandidates'] - $lintprogramsat[$linti];
	        }
			$tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 11 align=center><b>{$ReportName}</b></td></tr></table><br>";
			$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' > </td><td align ='center' colspan = 10><b>".$lstrmonthyear."</b></td></tr>";
			 
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>ExamCategory/Type</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$larrprograms[$lvars]['ProgramName'].'</b></td>';
			}
			$tabledata.='<td align ="center" colspan = 2><b>Total</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Registered</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
				$TotalNoOfCandidates = $TotalNoOfCandidates+ $larrprogramcount[$lvars]['NoOfCandidates'];
				if($larrprogramcount[$lvars]['NoOfCandidates'])
				$tabledata.='<td align ="center" colspan = 2><b>'.$larrprogramcount[$lvars]['NoOfCandidates'].'</b></td>';
				else
				$tabledata.='<td align ="center" colspan = 2><b>0</b></td>';
			}
			$tabledata.='<td align ="center" colspan = 2><b>'.$TotalNoOfCandidates.'</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Sat</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$lintprogramsat[$lvars].'</b></td>';
			}
			$Totalsat = array_sum($lintprogramsat);
			$tabledata.='<td align ="center" colspan = 2><b>'.$Totalsat.'</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Pass</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$lintprogrampass[$lvars].'</b></td>';
			}
			$Totalpass = array_sum($lintprogrampass);
			$tabledata.='<td align ="center" colspan = 2><b>'.$Totalpass.'</b></td></tr>';
		
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Fail</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$lintprogramfail[$lvars].'</b></td>';
			}
			$Totalfail = array_sum($lintprogramfail);
			$tabledata.='<td align ="center" colspan = 2><b>'.$Totalfail.'</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Absent</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$lintprogramabsent[$lvars].'</b></td>';
			}
			$Totalabsent = array_sum($lintprogramabsent);
			$tabledata.='<td align ="center" colspan = 2><b>'.$Totalabsent.'</b></td></tr></table><br>';
     }
	
	if($lintcentretype == 1)
	{
		$ReportName = $this->view->translate( "Exam Centre Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		$larrcentrenames = $this->lobjallstatistics->fngetcentrenames($lintmon);
		for($linti=0;$linti<count($larrcentrenames);$linti++)
		{
			$larrresult[$linti] = $this->lobjallstatistics->fngetcentres($lintmon,$lintyear,$larrcentrenames[$linti]['idcenter']);
			$lintexamcentrereg[$linti] = count($larrresult[$linti]);
			$lintexamcentrepass[$linti] = 0;$lintexamcentrefail[$linti] = 0;$lintexamcentreabsent[$linti] = 0;$lintexamcentresat[$linti] = 0;
			for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++){
				if($larrresult[$linti][$lintj]['pass']==1){
					$lintexamcentrepass[$linti]++;
				}elseif($larrresult[$linti][$lintj]['pass']==2){
					$lintexamcentrefail[$linti]++;
				}else{
					$lintexamcentreabsent[$linti]++;
				}
			}
			$lintexamcentresat[$linti] = $lintexamcentrepass[$linti]+$lintexamcentrefail[$linti];
			$lintexamcentreabsent[$linti] = $lintexamcentrereg[$linti] -($lintexamcentrepass[$linti]+$lintexamcentrefail[$linti]);
		}
		$tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 23 align=center><b>{$ReportName}</b></td></tr></table><br>";
		$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 1> </td><td align ='center' colspan = 22><b>".$lstrmonthyear."</b></td></tr>";
	
		$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Exam Centre</b></td>';
		for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
			$tabledata.='<td align ="center" colspan = 1><b>'.$larrcentrenames[$lvars]['centername'].'</b></td>';
		}
		$tabledata.='<td align ="center" colspan = 1><b>Total</b></td></tr>';
		$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Registered</b></td>';
		for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
			if($lintexamcentrereg[$lvars])
			$tabledata.='<td align ="center" colspan = 1><b>'.$lintexamcentrereg[$lvars].'</b></td>';
			else
			$tabledata.='<td align ="center" colspan = 1><b>0</b></td>';
		}
		$tabledata.='<td align ="center" colspan = 1><b>'.array_sum($lintexamcentrereg).'</b></td></tr>';
		$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Sat</b></td>';
		for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
			$tabledata.='<td align ="center" colspan = 1><b>'.$lintexamcentresat[$lvars].'</b></td>';
		}
		$Totalsat = array_sum($lintexamcentresat);
		$tabledata.='<td align ="center" colspan = 1><b>'.$Totalsat.'</b></td></tr>';
		$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Pass</b></td>';
		for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
			$tabledata.='<td align ="center" colspan = 1><b>'.$lintexamcentrepass[$lvars].'</b></td>';
		}
		$Totalpass = array_sum($lintexamcentrepass);
		$tabledata.='<td align ="center" colspan = 1><b>'.$Totalpass.'</b></td></tr>';
	
		$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Fail</b></td>';
		for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
			$tabledata.='<td align ="center" colspan = 1><b>'.$lintexamcentrefail[$lvars].'</b></td>';
		}
		$Totalfail = array_sum($lintexamcentrefail);
		$tabledata.='<td align ="center" colspan = 1><b>'.$Totalfail.'</b></td></tr>';
		$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Absent</b></td>';
		for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
			$tabledata.='<td align ="center" colspan =1><b>'.$lintexamcentreabsent[$lvars].'</b></td>';
		}
		$Totalabsent = array_sum($lintexamcentreabsent);
		$tabledata.='<td align ="center" colspan =1><b>'.$Totalabsent.'</b></td></tr></table><br>';
		}
		if($lintresulttype==1)
			{
				$ReportName = $this->view->translate( "Result Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
				$larrprograms = $this->lobjallstatistics->fngetprogramnames();
				for($linti=0;$linti<count($larrprograms);$linti++)
				{
					$larrresult[$linti] = $this->lobjallstatistics->fngetresultgrade($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
					$larrregdetails[$linti] = $this->lobjallstatistics->fngetabsentdetails($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
					$lintgradeA[$linti] = 0;$lintgradeB[$linti] = 0;$lintgradeC[$linti] = 0;$lintgradeF[$linti] = 0;
					for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++){
						if($larrresult[$linti][$lintj]['Grade']=='A'){
							$lintgradeA[$linti]++;
						}elseif($larrresult[$linti][$lintj]['Grade']=='B'){
							$lintgradeB[$linti]++;
						}elseif($larrresult[$linti][$lintj]['Grade']=='C'){
							$lintgradeC[$linti]++;
						}else{
							$lintgradeF[$linti]++;
						}
					}
					$lintabsent[$linti] = $larrregdetails[$linti]['NoOfCandidates'] -($lintgradeA[$linti]+$lintgradeB[$linti]+$lintgradeC[$linti]+$lintgradeF[$linti]);
				}
				$tabledata.="<br><br><table border=1 align=center width=100%><tr><td colspan = 14 align=center><b>{$ReportName}</b></td></tr></table><br>";
				$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 12><b>".$lstrmonthyear."</b></td></tr>";
				$tabledata.="<tr> <td align ='left' colspan = 2><b>Examcategory/Result</b></td><td align ='center' colspan = 6><b>Pass/Grade</b></td><td align ='center' colspan = 2><b>Fail</b></td><td align ='center' colspan = 2><b>Absent</b></td><td align ='center' colspan = 2><b>Total</b></td></tr>";
				$tabledata.="<tr> <td align ='left' colspan = 2><b></b></td><td align ='center' colspan = 2><b>A</b></td><td align ='center' colspan = 2><b>B</b></td><td align ='center' colspan = 2><b>C</b></td><td align ='center' colspan = 2><b></b></td><td align ='center' colspan = 2><b></b></td><td align ='center' colspan = 2></td></tr>";
				for($linti=0;$linti<count($larrprograms);$linti++){
					$tabledata.='<tr><td align ="center" colspan = 2><b>'.$larrprograms[$linti]['ProgramName'].'</b></td><td align ="center" colspan = 2>'.$lintgradeA[$linti].'</td><td align ="center" colspan = 2>'.$lintgradeB[$linti].'</td><td align ="center" colspan = 2>'.$lintgradeC[$linti].'</td><td align ="center" colspan = 2>'.$lintgradeF[$linti].'</td><td align ="center" colspan = 2>'.$lintabsent[$linti].'</td>';
					if($larrregdetails[$linti]['NoOfCandidates']){
					  $tabledata.='<td align ="center" colspan = 2>'.$larrregdetails[$linti]['NoOfCandidates'].'</td></tr>';	
					}else{
						$tabledata.='<td align ="center" colspan = 2>0</td></tr>';
					}
				}
				$tabledata.="</table><br>";
			}
			
		if($lintgendertype==1)
		{
			$ReportName = $this->view->translate( "Gender Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
			$tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 11 align=center><b>{$ReportName}</b></td></tr></table><br>";
			$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 9><b>".$lstrmonthyear."</b></td></tr>";
			$tabledata.="<tr> <td align ='left' colspan = 2><b>Gender</b></td><td align ='center' colspan = 3><b>Male</b></td><td align ='center' colspan = 3><b>Female</b></td><td align ='center' colspan = 3><b>Total</b></td></tr>";
			$tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td><td align ='center' colspan = 3>".$larrformData['malecand']."</td><td align ='center' colspan = 3>".$larrformData['femalecand']."</td><td align ='center' colspan = 3>".$larrformData['regtotal1']."</td></tr>";
			$tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td><td align ='center' colspan = 3>".$larrformData['msat']."</td><td align ='center' colspan = 3>".$larrformData['fsat']."</td><td align ='center' colspan = 3>".$larrformData['sattotal']."</td></tr>";
			$tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td><td align ='center' colspan = 3>".$larrformData['mpass']."</td><td align ='center' colspan = 3>".$larrformData['fpass']."</td><td align ='center' colspan = 3>".$larrformData['passtotal']."</td></tr>";
			$tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td><td align ='center' colspan = 3>".$larrformData['mfail']."</td><td align ='center' colspan = 3>".$larrformData['ffail']."</td><td align ='center' colspan = 3>".$larrformData['failtotal']."</td></tr>";
			$tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td><td align ='center' colspan = 3>".$larrformData['mabsent']."</td><td align ='center' colspan = 3>".$larrformData['fabsent']."</td><td align ='center' colspan = 3>".$larrformData['absenttotal']."</td></tr></table><br>";
		}	
			
		if($lintracetype ==1)
		{
	        $ReportName = $this->view->translate( "Race Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
			$larrracescount = $this->lobjallstatistics->fngetraces($lintmon,$lintyear);
	        $larrraces = $this->lobjallstatistics->fngetracesnames($lintmon,$lintyear);
	
	        for($linti=0;$linti<count($larrraces);$linti++)
	        {
	        	$lintraceid = $larrraces[$linti]['idDefinition'];
	        	$larrracename = $larrraces[$linti]['Race'];
	        	$larrraceresult[$linti] = $this->lobjallstatistics->fngetracesresult($lintmon,$lintyear,$lintraceid);
	        	$lintracecount[$linti] = count($larrraceresult[$linti]);
	        	$lintracepass[$linti] = 0;
	        	$lintracefail[$linti] = 0;
	        	$lintraceabsent[$linti] = 0;
	        	$lintracesat[$linti] = 0;
	        	for($lintj=0;$lintj<$lintracecount[$linti];$lintj++){
	        		if($larrraceresult[$linti][$lintj]['pass']==1)
	        		{
	        			$lintracepass[$linti]++;
	        		}elseif ($larrraceresult[$linti][$lintj]['pass']==2){
	        			$lintracefail[$linti]++;
	        		}
	        	}
	            $lintracesat[$linti] = $lintracepass[$linti]+$lintracefail[$linti];
	            $lintraceabsent[$linti] = $larrracescount[$linti]['NoOfCandidates'] - $lintracesat[$linti];
            }
	        
	        $tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 11 align=center><b>{$ReportName}</b></td></tr></table><br>";
			$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' > </td><td align ='center' colspan = 10><b>".$lstrmonthyear."</b></td></tr>";
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Race</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$larrraces[$lvars]['RaceName'].'</b></td>';
			}
			$tabledata.='<td align ="center" colspan = 2><b>Total</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Registered</b></td>';
			$TotalNoOfCandidates = 0;
			for($lvars=0;$lvars<count($larrraces);$lvars++){
				$TotalNoOfCandidates = $TotalNoOfCandidates+ $larrracescount[$lvars]['NoOfCandidates'];
				if($larrracescount[$lvars]['NoOfCandidates'])
				$tabledata.='<td align ="center" colspan = 2><b>'.$larrracescount[$lvars]['NoOfCandidates'].'</b></td>';
				else 
				$tabledata.='<td align ="center" colspan = 2><b>0</b></td>';
			}
			$tabledata.='<td align ="center" colspan = 2><b>'.$TotalNoOfCandidates.'</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Sat</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$lintracesat[$lvars].'</b></td>';
			}
			$Totalsat = array_sum($lintracesat);
			$tabledata.='<td align ="center" colspan = 2><b>'.$Totalsat.'</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Pass</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$lintracepass[$lvars].'</b></td>';
			}
			$Totalpass = array_sum($lintracepass);
			$tabledata.='<td align ="center" colspan = 2><b>'.$Totalpass.'</b></td></tr>';
				
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Fail</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$lintracefail[$lvars].'</b></td>';
			}
			$Totalfail = array_sum($lintracefail);
			$tabledata.='<td align ="center" colspan = 2><b>'.$Totalfail.'</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr><td><b>Absent</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
				$tabledata.='<td align ="center" colspan = 2><b>'.$lintraceabsent[$lvars].'</b></td>';
			}
			$Totalabsent = array_sum($lintraceabsent);
			$tabledata.='<td align ="center" colspan = 2><b>'.$Totalabsent.'</b></td></tr></table><br>';
	    }
	   
	   if($lintqualificationtype==1)
		{
		$ReportName = $this->view->translate( "Qualification Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		
		$larrqualificationcount = $this->lobjallstatistics->fngetqualification($lintmon,$lintyear); //Call a function to get number of candidates registered for different qualification
		$larrqualification = $this->lobjallstatistics->fngetqualificationnames($lintmon,$lintyear); //Call a function to get different qualification names
		$this->view->larrqualificationcount=$larrqualificationcount;
		$lintregisteredtotal=0;
		for($linti=0;$linti<count($larrqualification);$linti++){
			$lintqualificationid= $larrqualification[$linti]['idDefinition'];
			$larrqualificationquali[$linti]  =	$larrqualification[$linti]['QualificationName'];
			$larrQualificationresult = $this->lobjallstatistics->fngetqualificationresult($lintmon,$lintyear,$lintqualificationid); //Call a function to get number of Diploma candidates details
			$lintregisteredtotal += $larrqualificationcount[$linti]['NoOfCandidates'];
			$lintcountQualification = count($larrQualificationresult);

			$lintqualificationreg[$linti]         = $larrqualificationcount[$linti]['NoOfCandidates'];
			$lintqualificationsat[$linti] = $lintcountQualification;
			$lintqualificationpass[$linti] = 0;
			$lintqualificationfail[$linti] = 0;
			$lintqualificationabsent[$linti]=0;

			for($lintj=0;$lintj<$lintcountQualification;$lintj++){
				if($larrQualificationresult[$lintj]['pass']==1)
				{
					$lintqualificationpass[$linti]++;
				}elseif ($larrQualificationresult[$lintj]['pass']==2){
					$lintqualificationfail[$linti]++;
				}else{
					$lintqualificationabsent[$linti]++;
				}
			}
			$lintqualificationabsent[$linti] = $larrqualificationcount[$linti]['NoOfCandidates']-$lintqualificationsat[$linti];
		}
		$QualificationReportName = $this->view->translate( "Qualification" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		$tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 14 align=center><b>{$ReportName}</b></td></tr></table><br>";
		$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 12><b>".$lstrmonthyear."</b></td></tr>";
	
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Qualification</b></td>";
		for($linti=0;$linti<count($larrqualificationquali);$linti++) {
			$tabledata.="<td align ='center' colspan = 2><b>$larrqualificationquali[$linti]</b></td>";
		}
		$tabledata.="<td align ='center' colspan = 2><b>TOTAL</b></td></tr>";
	
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td>";
		$linttotalreg =0;
		for($linti=0;$linti<count($lintqualificationreg);$linti++) {
			$linttotalreg+=$lintqualificationreg[$linti];
			if($lintqualificationreg[$linti])
			$tabledata.="<td align ='center' colspan = 2><b>$lintqualificationreg[$linti]</b></td>";
			else
			$tabledata.="<td align ='center' colspan = 2><b>0</b></td>";
		}
		if($linttotalreg)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalreg</b></td></tr>";
		else
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr>";
	
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td>";
		$linttotalsat =0;
		for($linti=0;$linti<count($lintqualificationsat);$linti++) {
			$linttotalsat+=$lintqualificationsat[$linti];
			$tabledata.="<td align ='center' colspan = 2><b>$lintqualificationsat[$linti]</b></td>";
		}
		if($linttotalsat)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalsat</b></td></tr>";
		else
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr>";
	
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td>";
		$linttotalpass =0;
		for($linti=0;$linti<count($lintqualificationpass);$linti++) {
			$linttotalpass+=$lintqualificationpass[$linti];
			$tabledata.="<td align ='center' colspan = 2><b>$lintqualificationpass[$linti]</b></td>";
		}
		if($linttotalpass)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalpass</b></td></tr>";
		else
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr>";
	
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td>";
		$linttotalfail =0;
		for($linti=0;$linti<count($lintqualificationfail);$linti++) {
			$linttotalfail+=$lintqualificationfail[$linti];
			$tabledata.="<td align ='center' colspan = 2><b>$lintqualificationfail[$linti]</b></td>";
		}
		if($linttotalreg)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalfail</b></td></tr>";
		else
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr>";
	
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td>";
		$linttotalabsent =0;
		for($linti=0;$linti<count($lintqualificationabsent);$linti++) {
			$linttotalabsent+=$lintqualificationabsent[$linti];
			$tabledata.="<td align ='center' colspan = 2><b>$lintqualificationabsent[$linti]</b></td>";
		}
		if($linttotalreg)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalabsent</b></td></tr></table><br>";
		else
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr></table><br>";
	}
	$ourFileName = realpath('.')."/data";
	$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
	ini_set('max_execution_time', 3600);
	fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
	fclose($ourFileHandle);
	header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
	header("Content-Disposition: attachment; filename=$filename.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	readfile($ourFileName);
	unlink($ourFileName);
}
}
