<?php
class Reports_LogController extends Base_Base {
private $_gobjlog;
private $_gobjlogger;
	public function init() {
		$this->_gobjlog = Zend_Registry::get ( 'log' ); //instantiate log object
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate logger object	
	}
	
	   
public function indexAction() {               //Action to get Log contents
		$this->view->checkEmpty = 0;
		$lobjReportsForm = new Reports_Form_Report();
		$this->view->form = $lobjReportsForm;
		$lobjlogreportModel = new Reports_Model_DbTable_Log();
		$loginnames=$lobjlogreportModel->fnGetloginnamesList();  //Get user details
		$lobjReportsForm->UserNameList->addMultiOption('','Select');
		$lobjReportsForm->UserNameList->addMultiOptions($loginnames); 
		$jsondata = '{
    				"label":"UpdateDate",
					"identifier":"id",
					"items":""
				  }';
		$this->view->jsondata = $jsondata;
		if ($this->_request->isPost() && $this->_request->getPost('Generate')) {
		    $larrformData = $this->_request->getPost();
			if ($lobjReportsForm->isValid ( $larrformData )) 
			{
			  if($larrformData['Monthyear'])
			   {
			     $YearDate=explode("-",$larrformData['Monthyear']);
			     $Year=$YearDate[0];
			     $month=$YearDate[1];
			   }
			   else
			   {
				 $Year=date('Y');
				 $month=date('m');
		       }
			$file = APPLICATION_PATH.'/logs/application_'.$month.'_'.$Year.'log.txt';
			$contents = file_get_contents($file);
			$UserNameList=$larrformData['UserNameList'];
			//$FromDate=$larrformData['FromDate'];
			//$ToDate=$larrformData['ToDate'];
			$Description=$larrformData['Description'];
			$controller=$larrformData['Controller1'];
			$searcharray = array();
			if($larrformData['Monthyear'])array_push($searcharray,$larrformData['Monthyear']);
			if($UserNameList)array_push($searcharray,$UserNameList);
			//if($FromDate)array_push($searcharray,$FromDate);
			//if($ToDate)array_push($searcharray,$ToDate);
			if($Description)array_push($searcharray,$Description);
			if($controller)array_push($searcharray,$controller);	
			if($searcharray)
			{
				$larr=array();
				$larr1=array();
				$cnts = 0;
				$ccnt = 0;
				$cntarr = count($searcharray);
			    foreach($searcharray as $searcharray1) 
			    {
			         $cnts++; 
			         if($cnts == 1 ) $searcharray1 =  "";
			         $pattern = "/^.*$searcharray1.*\$/m";     
		             preg_match_all($pattern, $contents, $matches);          
				     $strresult =  implode("\n", $matches[0]);	
				     $contents = $strresult;
	   		         $larr1 = explode("\t\t\t\t\r",$strresult);
	   		         if($cnts == $cntarr){
		   		         foreach($larr1 as $larr12) 
					  			{			  				
		   		         			$larr123 = explode("\t\t\t\t",$larr12);
		   		         			if(count($larr123)>2){    		         				
		   		         				$larr1234[$ccnt]['time'] = $larr123[0];
		   		         				$larr1234[$ccnt]['Controller'] = $larr123[1];
		   		         				$larr1234[$ccnt]['Description'] = $larr123[2];
		   		         				if(count($larr123)>3)$larr1234[$ccnt]['FromIP'] = $larr123[3];
		   		         				else $larr1234[$ccnt]['FromIP'] = "";
		   		         				if(count($larr123)>4)$larr1234[$ccnt]['Actionstatus'] = $larr123[4];
		   		         				else $larr1234[$ccnt]['Actionstatus'] = "";
		   		         				if(count($larr123)>5) 	$larr1234[$ccnt]['user'] = $larr123[5];
		   		         				else $larr1234[$ccnt]['user'] = "";
		   		         				$ccnt++;
		   		         			}    		         			
					  			}
					    }			  
				  }
		     }
			 else
			 {
  		      echo "No matches found";
		     }   
            if(@$larr1234) $this->view->checkEmpty = 1;	
            $page = $this->_getParam('page',1);
			$this->view->counter = (count(@$larr1234));
			$this->view->lobjPaginator = @$larr1234;

   		    $jsonresult = Zend_Json_Encoder::encode(@$larr1234);
	   	    $jsondata = '{
    				      "label":"Controller",
					      "identifier":"time",
					      "items":'.$jsonresult.
				        '}';
			$this->view->jsondata = $jsondata;
			}
		}
	}


	//Action to generate Log Details
	public function generatereportAction(){
    $lobjReportsForm = new Reports_Form_Report();
		
		$this->view->form = $lobjReportsForm;		
		//Check Whether the form is submitted
	if($this->_request->getPost()){
		$larrformData = $this->_request->getPost();
		$this->view->datacount = $larrformData['datacount'];
		$this->view->datacounttable = $larrformData['datacounttable'];
	}else{ 
	$this->_redirect( $this->baseUrl . 'reports/log/index');
	}	
	}
	
	public function pdfexportAction(){	
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
		$htmldata = $larrformData['datacount'];
		$htmltabledata = $larrformData['datacounttable'];
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);	
		//log file	
		$auth = Zend_Auth::getInstance();
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Invoice Details Report Export"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);			
		if($larrformData['ReportType'] == "pdf"){
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');		
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img align=center width=100% src="../public/images/header.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->WriteHTML('<br/><br/>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ( date ( "d-m-Y H:i:s" ) . "                                          ".'{PAGENO}{nbpg}' );
		// LOAD a stylesheet
		$stylesheet = file_get_contents('../public/css/default.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Log" ).' '.$this->view->translate( "Report" );
		$mpdf->WriteFixedPosHTML ( "<table border=1  align=center width=100%><tr><td align=center> {$ReportName}</td></tr></table>", 80, 35, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		$html = htmlspecialchars_decode ( $htmltabledata );
		$mpdf->WriteHTML($html);
		$mpdf->Output('Log_Report.pdf','D');			
		}else{			
		$ourFileName = realpath('.')."/data";
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); 
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($htmldata));
		fclose($ourFileHandle);		   
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=Log_Report.xls");
			header("Pragma: no-cache");
			header("Expires: 0");			
			readfile($ourFileName);
			unlink($ourFileName);
		}
	}
}