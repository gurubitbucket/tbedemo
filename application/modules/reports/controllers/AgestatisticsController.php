<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_AgestatisticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjStasticsForm = new  Reports_Form_Statistics();
		$this->lobjstastics = new Reports_Model_DbTable_Agestatistics();		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjStasticsForm;
		    /*if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {   
		    	$larrformData = $this->_request->getPost ();
				if ($this->lobjform->isValid($larrformData)) 
				{
				 $larrformData = $this->_request->getPost ();*/
				 $ldtdate = $this->_getParam('ldtdate');//$larrformData['Date'];
				 $this->view->searchdate = $ldtdate;
				 $larrdates = explode('-',$ldtdate);
				 $lintmon = $larrdates['1'];
				 $lintyear = $larrdates['0'];
				 $lstrmonthname = date( 'F', mktime(0,0,0,$lintmon));
				 $this->view->month = $lstrmonthname;
				 $this->view->year = $lintyear;
				 unset ( $larrformData ['Search'] );
				 $larrages = $this->lobjstastics->fngetages($lintmon,$lintyear);
				 $lintagecount = count($larrages);
				 $larragescount['18-21']=0;$larragescount['22-25']=0;$larragescount['26-29']=0;$larragescount['30-34']=0;$larragescount['35-39']=0;$larragescount['40-44']=0;$larragescount['45-49']=0;$larragescount['50-54']=0;
				 $larragescount['55-59']=0;$larragescount['60Above']=0;
				 for($linti=0;$linti<$lintagecount;$linti++){
				 	if($larrages[$linti]['Age']>=18 && $larrages[$linti]['Age']<=21){
				 		$larragescount['18-21']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['18-21sat']++;	
				 		   $larragescount['18-21pass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['18-21sat']++;	
				 		   $larragescount['18-21fail']++;
				 		}else{
				 		    $larragescount['18-21absent']++;
				 		}
				 	}elseif($larrages[$linti]['Age']>=22 && $larrages[$linti]['Age']<=25){
				 		$larragescount['22-25']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['22-25sat']++;	
				 		   $larragescount['22-25pass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['22-25sat']++;	
				 		   $larragescount['22-25fail']++;
				 		}else{
				 		   $larragescount['22-25absent']++;
				 		}
				 	}
				 	elseif($larrages[$linti]['Age']>=26 && $larrages[$linti]['Age']<=29){
				 		$larragescount['26-29']++;
				 	    if($larrages[$linti]['pass']==1){
				 		   $larragescount['26-29sat']++;	
				 		   $larragescount['26-29pass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['26-29sat']++;	
				 		   $larragescount['26-29fail']++;
				 		}else{
				 		   $larragescount['26-29absent']++;
				 		}
				 	}elseif($larrages[$linti]['Age']>=30 && $larrages[$linti]['Age']<=34){
				 		$larragescount['30-34']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['30-34sat']++;	
				 		   $larragescount['30-34pass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['30-34sat']++;	
				 		   $larragescount['30-34fail']++;
				 		}else{
				 		   $larragescount['30-34absent']++;
				 		}
				 	}elseif($larrages[$linti]['Age']>=35 && $larrages[$linti]['Age']<=39){
				 		$larragescount['35-39']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['35-39sat']++;	
				 		   $larragescount['35-39pass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['35-39sat']++;	
				 		   $larragescount['35-39fail']++;
				 		}else{
				 		   $larragescount['35-39absent']++;
				 		}
				 	}elseif($larrages[$linti]['Age']>=40 && $larrages[$linti]['Age']<=44){
				 		$larragescount['40-44']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['40-44sat']++;	
				 		   $larragescount['40-44pass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['40-44sat']++;	
				 		   $larragescount['40-44fail']++;
				 		}else{
				 		   $larragescount['40-44absent']++;
				 		}
				 	}elseif($larrages[$linti]['Age']>=45 && $larrages[$linti]['Age']<=49){
				 		$larragescount['45-49']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['45-49sat']++;	
				 		   $larragescount['45-49pass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['45-49sat']++;	
				 		   $larragescount['45-49fail']++;
				 		}else{
				 		   $larragescount['45-49absent']++;
				 		}
				 	}elseif($larrages[$linti]['Age']>=50 && $larrages[$linti]['Age']<=54){
				 		$larragescount['50-54']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['50-54sat']++;	
				 		   $larragescount['50-54pass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['50-54sat']++;	
				 		   $larragescount['50-54fail']++;
				 		}else{
				 		   $larragescount['50-54absent']++;
				 		}
				 	}elseif($larrages[$linti]['Age']>=55 && $larrages[$linti]['Age']<=59){
				 		$larragescount['55-59']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['55-59sat']++;	
				 		   $larragescount['55-59pass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['55-59sat']++;	
				 		   $larragescount['55-59fail']++;
				 		}else{
				 		   $larragescount['55-59absent']++;
				 		}
				 	}else{
				 		$larragescount['60Above']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['60Abovesat']++;	
				 		   $larragescount['60Abovepass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['60Abovesat']++;	
				 		   $larragescount['60Abovefail']++;
				 		}else{
				 		   $larragescount['60Aboveabsent']++;
				 		}
				 	}
				 }
				 $larragescount['regtotal']=  $larragescount['18-21']+$larragescount['22-25']+$larragescount['26-29']+$larragescount['30-34']+$larragescount['35-39']+$larragescount['40-44']+$larragescount['45-49']+$larragescount['50-54']+$larragescount['55-59']+$larragescount['60Above'];
				 $larragescount['sattotal']=  $larragescount['18-21sat']+$larragescount['22-25sat']+$larragescount['26-29sat']+$larragescount['30-34sat']+$larragescount['35-39sat']+$larragescount['40-44sat']+$larragescount['45-49sat']+$larragescount['50-54sat']+$larragescount['55-59sat']+$larragescount['60Abovesat'];
				 $larragescount['passtotal']=  $larragescount['18-21pass']+$larragescount['22-25pass']+$larragescount['26-29pass']+$larragescount['30-34pass']+$larragescount['35-39pass']+$larragescount['40-44pass']+$larragescount['45-49pass']+$larragescount['50-54pass']+$larragescount['55-59pass']+$larragescount['60Abovepass'];
				 $larragescount['failtotal']=  $larragescount['18-21fail']+$larragescount['22-25fail']+$larragescount['26-29fail']+$larragescount['30-34fail']+$larragescount['35-39fail']+$larragescount['40-44fail']+$larragescount['45-49fail']+$larragescount['50-54fail']+$larragescount['55-59fail']+$larragescount['60Abovefail'];
				 $larragescount['absenttotal']=  $larragescount['18-21absent']+$larragescount['22-25absent']+$larragescount['26-29absent']+$larragescount['30-34absent']+$larragescount['35-39absent']+$larragescount['40-44absent']+$larragescount['45-49absent']+$larragescount['50-54absent']+$larragescount['55-59absent']+$larragescount['60Aboveabsent'];
				 $this->view->larrracescount = $larragescount;
			     $this->view->paginator = $larrracescount;
				 //$this->view->lobjform->populate($larrformData);
			//}
 	      //}
	}
			public function fnexportexcelAction()
				{    
					 $this->_helper->layout->disableLayout();
					 $this->_helper->viewRenderer->setNoRender();
					 $larrformData = $this->_request->getPost();
					 unset ( $larrformData['ExportToExcel']);
					 $lday= date("d-m-Y");
					 $ltime = date('h:i:s',time());
					 $host = $_SERVER['SERVER_NAME'];
					 $imgp = "http://".$host."/tbenew/images/reportheader.jpg";
					 $filename = 'Age_Statistics_Report_'.$larrformData['selmonth'];
					 $ReportName = $this->view->translate( "Age" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
					 $tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2><b>Date </b></td><td align ='left' colspan = 4><b>$lday</b></td><td align ='left' colspan = 3><b> Time</b></td><td align ='left' colspan = 5><b>$ltime</b></td></tr></table>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 14 align=center><b>{$ReportName}</b></td></tr></table><br>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 12><b>".$larrformData['selmonth']."</b></td></tr>";
					 $tabledata.="<tr> <td align ='left' colspan = 2><b>Age</b></td><td align ='center' colspan = 1><b>18-21</b></td><td align ='center' colspan = 1><b>22-25</b></td><td align ='center' colspan = 1><b>26-29</b></td><td align ='center' colspan = 1><b>30-34</b></td><td align ='center' colspan = 1><b>35-39</b></td><td align ='center' colspan = 1><b>40-44</b></td><td align ='center' colspan = 1><b>45-49</b></td><td align ='center' colspan = 1><b>50-54</b></td><td align ='center' colspan = 1><b>55-59</b></td><td align ='center' colspan = 1><b>60 Above</b></td><td align ='center' colspan = 2><b>Total</b></td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td><td align ='center' colspan = 1>".$larrformData['18-21']."</td><td align ='center' colspan = 1>".$larrformData['22-25']."</td><td align ='center' colspan = 1>".$larrformData['26-29']."</td><td align ='center' colspan = 1>".$larrformData['30-34']."</td><td align ='center' colspan = 1>".$larrformData['35-39']."</td><td align ='center' colspan = 1>".$larrformData['40-44']."</td><td align ='center' colspan = 1>".$larrformData['45-49']."</td><td align ='center' colspan = 1>".$larrformData['50-54']."</td><td align ='center' colspan = 1>".$larrformData['55-59']."</td><td align ='center' colspan = 1>".$larrformData['60Above']."</td><td align ='center' colspan = 2>".$larrformData['regtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td><td align ='center' colspan = 1>".$larrformData['18-21sat']."</td><td align ='center' colspan = 1>".$larrformData['22-25sat']."</td><td align ='center' colspan = 1>".$larrformData['26-29sat']."</td><td align ='center' colspan = 1>".$larrformData['30-34sat']."</td><td align ='center' colspan = 1>".$larrformData['35-39sat']."</td><td align ='center' colspan = 1>".$larrformData['40-44sat']."</td><td align ='center' colspan = 1>".$larrformData['45-49sat']."</td><td align ='center' colspan = 1>".$larrformData['50-54sat']."</td><td align ='center' colspan = 1>".$larrformData['55-59sat']."</td><td align ='center' colspan = 1>".$larrformData['60Abovesat']."</td><td align ='center' colspan = 2>".$larrformData['sattotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td><td align ='center' colspan = 1>".$larrformData['18-21pass']."</td><td align ='center' colspan = 1>".$larrformData['22-25pass']."</td><td align ='center' colspan = 1>".$larrformData['26-29pass']."</td><td align ='center' colspan = 1>".$larrformData['30-34pass']."</td><td align ='center' colspan = 1>".$larrformData['35-39pass']."</td><td align ='center' colspan = 1>".$larrformData['40-44pass']."</td><td align ='center' colspan = 1>".$larrformData['45-49pass']."</td><td align ='center' colspan = 1>".$larrformData['50-54pass']."</td><td align ='center' colspan = 1>".$larrformData['55-59pass']."</td><td align ='center' colspan = 1>".$larrformData['60Abovepass']."</td><td align ='center' colspan = 2>".$larrformData['passtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td><td align ='center' colspan = 1>".$larrformData['18-21fail']."</td><td align ='center' colspan = 1>".$larrformData['22-25fail']."</td><td align ='center' colspan = 1>".$larrformData['26-29fail']."</td><td align ='center' colspan = 1>".$larrformData['30-34fail']."</td><td align ='center' colspan = 1>".$larrformData['35-39fail']."</td><td align ='center' colspan = 1>".$larrformData['40-44fail']."</td><td align ='center' colspan = 1>".$larrformData['45-49fail']."</td><td align ='center' colspan = 1>".$larrformData['50-54fail']."</td><td align ='center' colspan = 1>".$larrformData['55-59fail']."</td><td align ='center' colspan = 1>".$larrformData['60Abovefail']."</td><td align ='center' colspan = 2>".$larrformData['failtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td><td align ='center' colspan = 1>".$larrformData['18-21absent']."</td><td align ='center' colspan = 1>".$larrformData['22-25absent']."</td><td align ='center' colspan = 1>".$larrformData['26-29absent']."</td><td align ='center' colspan = 1>".$larrformData['30-34absent']."</td><td align ='center' colspan = 1>".$larrformData['35-39absent']."</td><td align ='center' colspan = 1>".$larrformData['40-44absent']."</td><td align ='center' colspan = 1>".$larrformData['45-49absent']."</td><td align ='center' colspan = 1>".$larrformData['50-54absent']."</td><td align ='center' colspan = 1>".$larrformData['55-59absent']."</td><td align ='center' colspan = 1>".$larrformData['60Aboveabsent']."</td><td align ='center' colspan = 2>".$larrformData['absenttotal']."</td></tr>";
					 $ourFileName = realpath('.')."/data";
					 $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
					 ini_set('max_execution_time', 3600);
					 fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
					 fclose($ourFileHandle);
					 header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
					 header("Content-Disposition: attachment; filename=$filename.xls");
					 header("Pragma: no-cache");
					 header("Expires: 0");
					 readfile($ourFileName);
					 unlink($ourFileName);
				}	
}	
 