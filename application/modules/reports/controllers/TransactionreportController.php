<?php
	error_reporting (E_ALL ^ E_WARNING);
    error_reporting (E_ALL ^ E_NOTICE);
class Reports_TransactionreportController extends Base_Base {
	
private $_gobjlogger; 
	public function init() 
	{
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
    
	public function indexAction() 
	{
		$this->view->checkEmpty = 0;
		$lobjReportsForm = new  Reports_Form_Report();
		$lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$this->view->lobjform = $lobjReportsForm;
				
		$larrcourses=$lobjExamreportModel->fngetprogramnames();		
		$lobjReportsForm->Coursename->addMultiOption('','Select'); 	
		$lobjReportsForm->Coursename->addmultioptions($larrcourses);
		
		$larrcenters=$lobjExamreportModel->fngetcenternames();
		$lobjReportsForm->Venues->addMultiOption('','Select'); 	
		$lobjReportsForm->Venues->addmultioptions($larrcenters);
		
	    $jsondata = '{
    				"label":"Name",
					"identifier":"Serialno",
					"items":""
				  }';
		$this->view->jsondata = $jsondata;
 		
 		if($this->_request->isPost() && $this->_request->getPost('Generate')) 
 		{
 			$larrformData = $this->_request->getPost();
 			unset($larrformData['Generate']);
 			if ($lobjReportsForm->isValid ( $larrformData )) 
 			{
 				$result = $lobjExamreportModel->fngettransactiondetails($larrformData);
 	       		$count=count($result);
 				for($i=0;$i<count($result);$i++)
 				{
 					$result[$i]['Serialno'] = $i+1;
 				}

 			if($result) $this->view->checkEmpty = 1;	
			$page = $this->_getParam('page',1);
			$this->view->counter = (count($result));
			$this->view->lobjPaginator = $result;
			$jsonresult = Zend_Json_Encoder::encode($result);
    		$jsondata = '{
    				"label":"StudentName",
					"identifier":"Serialno",
					"items":'.$jsonresult.
				  '}';
			$this->view->jsondata = $jsondata;
		}	
	  }
	}
	
	public function generatereportAction()
	{
		$lobjReportsForm = new  Reports_Form_Report();
		$this->view->lobjform = $lobjReportsForm;
		//Check Whether the form is submitted
		if($this->_request->getPost())
		{
			$larrformData = $this->_request->getPost();
			$this->view->datacount = $larrformData['datacount'];
			$this->view->datacounttable = $larrformData['datacounttable'];
			// Write to Logs
			$auth = Zend_Auth::getInstance();
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Generated the Report"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);
		}
		else
		{
			$this->_redirect( $this->baseUrl . 'reports/transactionreport/index');
	    }
		
	 }
	
	 
public function pdfexportAction()
	{
		//require_once 'FPDF/fpdf.php';
		//require_once 'html2pdf_v4.03/pdf.php';
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
		$htmldata = $larrformData['datacount'];
		$htmltabledata = $larrformData['datacounttable'];
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
		if($larrformData['ReportType'] == "pdf"){
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img align=center width=100% src="../public/images/header.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                            '.'{PAGENO}{nbpg}' );
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Transaction" ).' '.$this->view->translate( "Report" );
		$mpdf->WriteFixedPosHTML ( "<table border=1  align=center width=100%><tr><td align=center> {$ReportName}</td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		$html = htmlspecialchars_decode ( $htmltabledata );
		$mpdf->WriteHTML($html);
		$mpdf->Output('Paypal_Receipt_Information_Report','D');
		// Write to Logs
		$auth = Zend_Auth::getInstance();
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Paypal Receipt Information Report(PDF)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);	
		}else{
			
			$ourFileName = realpath('.')."/data";
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); 
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($htmldata));
		fclose($ourFileHandle);
		   // $data = str_replace("\r","",$xlExportData);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=Paypal_Receipt_Information_Report");
			header("Pragma: no-cache");
			header("Expires: 0");
			//print $csv_output."\n".$data;
			readfile($ourFileName);
			unlink($ourFileName);
			// Write to Logs
		$auth = Zend_Auth::getInstance();
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Paypal Receipt Information Report(Excel)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);
		}
	}
	 
	 
	/*public function pdfexportAction()
	{
		//require_once 'FPDF/fpdf.php';
		//require_once 'html2pdf_v4.03/pdf.php';
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
		$htmldata = $larrformData['datacount'];
		$htmltabledata = $larrformData['datacounttable'];
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
		
		if($larrformData['ReportType'] == "pdf")
		{
		 include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		
		 $mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		 $mpdf->debug = true;
		 $mpdf->SetDirectionality ( $this->gstrHTMLDir );
		 $mpdf->text_input_as_HTML = true;
		 $mpdf->useLang = true;
		 $mpdf->SetAutoFont();
		 $mpdf->WriteHTML('<img align=center width=100% src="../public/images/header.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		 $mpdf->SetDisplayMode('fullpage');
		 $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		 $mpdf->pagenumSuffix = ' / ';
		 $mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                            '.'{PAGENO}{nbpg}' );
		 //$mpdf->setFooter ( date ( "d-m-Y H:i:s" ) . "                                          ".'{PAGENO}{nbpg}' );
		 // LOAD a stylesheet
		 //$stylesheet = file_get_contents('../public/css/default.css');
		 //$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		 $mpdf->allow_charset_conversion = true; // Set by default to TRUE
		 $mpdf->charset_in = 'utf-8';
		 $ReportName = $this->view->translate( "Examination" ).' '.$this->view->translate( "Report" );
		 $mpdf->WriteFixedPosHTML ( "<table border=1  align=center width=100%><tr><td align=center> {$ReportName}</td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		 ini_set('max_execution_time',3600);
		
		 $html = htmlspecialchars_decode ( $htmltabledata );
		 $mpdf->WriteHTML($html);
		 $mpdf->Output('Examination_Report.pdf','D');
		 $mpdf->exit;	
		}
	}*/
	
		
}
