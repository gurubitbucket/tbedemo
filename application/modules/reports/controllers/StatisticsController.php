<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_StatisticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjStasticsForm = new  Reports_Form_Statistics();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjStasticsForm;
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {   
		    	$larrformData = $this->_request->getPost ();
				if ($this->lobjform->isValid($larrformData)) 
				{
				 $larrformData = $this->_request->getPost ();
				 $ldtdate = $larrformData['Date'];
					 if($larrformData['Type']==1){
					 	$this->_redirect( $this->baseUrl . '/reports/agestatistics/index/ldtdate/'.$ldtdate);
					 }elseif($larrformData['Type']==2){
					 	$this->_redirect( $this->baseUrl . '/reports/examcategorystatistics/index/ldtdate/'.$ldtdate);
					 }elseif($larrformData['Type']==3){
					 	$this->_redirect( $this->baseUrl . '/reports/examcentrestatistics/index/ldtdate/'.$ldtdate);
					 }elseif($larrformData['Type']==4){
					 	$this->_redirect( $this->baseUrl . '/reports/examresultstastics/index/ldtdate/'.$ldtdate);
					 }elseif($larrformData['Type']==5){
					 	$this->_redirect( $this->baseUrl . '/reports/genderstatistics/index/ldtdate/'.$ldtdate);
					 }elseif($larrformData['Type']==6){
					 	$this->_redirect( $this->baseUrl . '/reports/racestatistics/index/ldtdate/'.$ldtdate);
					 }elseif($larrformData['Type']==7){
					 	$this->_redirect( $this->baseUrl . '/reports/qualificationreport/index/ldtdate/'.$ldtdate);
					 }else{
					 	$this->_redirect( $this->baseUrl . '/reports/statistics/index');
					 }
			}
 	      }
	}
			
}	
 