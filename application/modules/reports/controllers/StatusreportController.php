<?php
class Reports_StatusreportController extends Base_Base {
	
private $_gobjlogger; 
	public function init() 
	{
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
    
	public function indexAction() 
	{
		$this->view->checkEmpty = 0;
		$lobjReportsForm = new Reports_Form_Report();
		$lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$this->view->lobjform = $lobjReportsForm;		
		$larrcourses=$lobjExamreportModel->fngetprogramnames();	
                $lobjReportsForm->Student->setAttrib('onkeyup', 'fnGetStudentsNames');		
		$lobjReportsForm->Coursename->addMultiOption('','Select'); 	
		$lobjReportsForm->Coursename->addmultioptions($larrcourses);	
 		if($this->_request->isPost() && $this->_request->getPost('Search')) 
 		{
		    Zend_Session:: namespaceUnset('pdfaction');
 			$larrformData = $this->_request->getPost();
			//echo "<pre>";
		    //print_r($larrformData);die();
 			$this->view->find =1;
                                $larrtomonth=$lobjExamreportModel->fngettodate($larrformData['Fromdate']);
					$yesterdaydate=$larrtomonth['todate'];
					//print_r($larrtomonth);die();
					$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}";
					
					$this->view->lobjform->Todate->setAttrib('constraints',"$dateofbirth");


 			if ($lobjReportsForm->isValid ( $larrformData ))
			{
			        $this->view->ExamFromdate =$larrformData['Fromdate'];
			        $this->view->ExamTodate = $larrformData['Todate'];
					$this->view->Name =$larrformData['Student'];
			        $this->view->ICNO = $larrformData['ICNO'];
					if($larrformData['Coursename'] ==1)	
                    {					
					       $this->view->Courseee = "Part A and B";
					}
					if($larrformData['Coursename'] ==2)	
					{
					       $this->view->Courseee = "Part A and C";
				    }
					if($larrformData['Coursename'] ==3)
                   {					
					      $this->view->Courseee = "Part B";
				   }
					if($larrformData['Coursename'] ==4)
					{
					        $this->view->Courseee = "Part C";
				    }
                   				
					
					if($larrformData['Result'] ==1)	
{				
					$this->view->Results ="Pass";
}
					if($larrformData['Result'] ==2)	
{
					$this->view->Results ="Fail";
}
					if($larrformData['Result'] ==3)	
{
					$this->view->Results ="Applied";
}
					if($larrformData['Result'] ==4)
{
					$this->view->Results ="Absent";
}
					
					//if($larrformData['Result']==)
			        $this->view->Result = $larrformData['Result'];
					$result = $lobjExamreportModel->fnReportSearchstudentStatus($larrformData);
					$count=count($result);			
					for($i=0;$i<count($result);$i++)
					{
						if($result[$i]['Result']==3)
						  $result[$i]['Result']= "Applied";
						if($result[$i]['Result']==1)
						 $result[$i]['Result']= "Pass";
						if($result[$i]['Result']==2)
						 $result[$i]['Result']= "Fail"; 
						if($result[$i]['Result']==4)
						  $result[$i]['Result']= "Absent"; 
					}
					if($result) $this->view->checkEmpty = 1;	
					$page = $this->_getParam('page',1);
					$this->view->counter = (count($result));
					 $namespace = new Zend_Session_Namespace('pdfaction');
    				$namespace->data = $result;
					$this->view->paginator = $result;
					
		    }	
	  }
	}
	
	public function getallattemptsAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idapp = $this->_getParam('idapp');
		$cnt = $this->_getParam('cnt');
		$icno = $this->_getParam('icno');
		$lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$result = $lobjExamreportModel->fngetallattemptsforicno($icno,$cnt);		
		$tabledata= '<br><fieldset><legend align = "left">Attempts:</legend>
					                    <table class="table" border=1 align = "center" width=100%>
					                    	<tr>
					                    	   <th><b>Examdate</b></th>
											   <th><b>RegisterID</b></th>
					                    		<th><b>Venue</b></th>
												<th><b>Program</b></th>
												<th><b>Session</b></th>
												<th><b>ICNO</b></th>
												<th><b>Name</b></th>
												<th><b>Attempts</b></th>
												<th><b>Result</b></th>	
					                    	</tr>';
		for($i=0;$i<count($result);$i++)
		{
		 
						  $tabledata.= '<tr><td align = "left">'.$result[$i]['DateTime'].'</td>';
					            $tabledata.= '<td align = "left">'.$result[$i]['Regid'].'</td>';          		
						    $tabledata.= '<td align = "left">'.$result[$i]['centername'].'</td>';
						      $tabledata.= '<td align = "left">'.$result[$i]['ProgramName'].'</td>';
						        $tabledata.= '<td align = "left">'.$result[$i]['managesessionname'].'</td>';
								 $tabledata.= '<td align = "left">'.$result[$i]['ICNO'].'</td>';
								 $tabledata.= '<td align = "left">'.$result[$i]['FName'].'</td>';
								    $tabledata.= '<td align = "left">'.$result[$i]['PermCity'].'</td>';
								if($result[$i]['pass']==1)
								{
						          $tabledata.= '<td align = "left">Pass</td></tr>';
								  }
								  elseif($result[$i]['pass']==2)
								  {
								     $tabledata.= '<td align = "left">Fail</td></tr>';
								  }
								  elseif($result[$i]['pass']==3)
								  {
								      $tabledata.= '<td align = "left">Applied</td></tr>';
								  }
								  elseif($result[$i]['pass']==4)
								  {
								      $tabledata.= '<td align = "left">Absent</td></tr>';
								  }								 
	    }
		$tabledata.="<tr><td colspan= '8' align='right'><input type='button' id='close' name='close'  value='Close' onClick='Closevenue();'></td></tr>";
		$tabledata.="</table><br>";
		echo  $tabledata;  
	}
	public function pdfexportAction()
	{   
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//$larrformdata1 = $this->_request->getPost();
	    $larrformData = $this->_request->getPost();
		//echo "<pre>";
		//print_r($larrformData);die();
	    $namespace = new Zend_Session_Namespace('pdfaction');
        $data      =     $namespace->data; 
		//echo "<pre>";
		//print_r($larrformData);die();
		$fdate = $larrformData['fromdate'];
		$tdate = $larrformData['todate'];
		$frmdate =date('d-m-Y',strtotime($fdate));
		$todate =date('d-m-Y',strtotime($tdate));
		//$result = $this->lobjcompletedetailsmodel->fngetdetailforstudent($larrformdata1['Fromdate'],$larrformdata1['Todate']);
		$count=count($result);
		
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'CompleteStudentDetails_Report_'.$frmdate.'_'.$todate;
		$ReportName = $this->view->translate( "Student" ).' '.$this->view->translate( "Status" ).' '.$this->view->translate( "Report" );		
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
		
		 $tabledata.= "<table border=1  align=center width=100%>";	
		
		                         if($larrformData['student'])
                                  {
								        $tabledata.= "<tr>"; 
                                        $tabledata.="<td align=left colspan = 2><b>Student Name</b></td>
						                <td align=left colspan = 2><b>".$larrformData['student']."</b></td>";
										 $tabledata.= "</tr>";
                                  }
		                         
                                 
                                 if($larrformData['icno'])
		                        {
                                     $tabledata.= "<tr>";
                                     $tabledata.="<td align=left colspan = 2><b>ICNO</b></td>
						             <td align=left colspan = 2><b>".$larrformData['icno']."</b></td></tr>";
                                 }
								    //$tabledata.= "</tr>";
								   $tabledata.= "<tr>";
								  if($larrformData['coursename'])
		                        {
                                   
                                     $tabledata.="<td align=left colspan = 2><b>Course</b></td>
						             <td align=left colspan = 2><b>".$larrformData['coursename']."</b></td>";
                                 }
                                 else
                                 {
								    
                                    $tabledata.="<td align=left colspan = 2><b>Course</b></td>
						            <td align=left colspan = 2><b>All</b></td>";
                                 }
                                  if($larrformData['result'])
		                        {
                                    
                                     $tabledata.="<td align=left colspan = 2><b>Result</b></td>
						             <td align=left colspan = 2><b>".$larrformData['result']."</b></td>";
                                 }
                                 else
                                 {
                                    $tabledata.="<td align=left colspan = 2><b>Result</b></td>
						            <td align=left colspan = 2><b>All</b></td>";
                                 }
                                 $tabledata.= "</tr>";
		
		if($larrformData['fromdate'])
		{
		       $tabledata.= "<tr>
		                  <td align=left colspan = 2><b>Exam From Date </b></td>
						  <td align=left colspan = 2><b>&nbsp;".$frmdate."</b></td>";
						  
				  if($larrformData['todate'])
		          {
		               $tabledata.= "<td  align=left colspan = 2><b> Exam To Date </b></td>
						    <td align=left colspan = 2><b>&nbsp;".$todate."</b></td>
					    </tr>";
		           }
				   else
				   {
				     $tabledata.="</tr>";
				   }
		}
		
        
		$tabledata.="</table>";
		//$tabledata.= "<tr><td align=left colspan = 2><b>Exam From Date </b></td><td align=left colspan = 2><b>".$frmdate."</b></td><td  align=left colspan = 2><b> Exam To Date </b></td><td align=left colspan = 2><b>".$todate."</b></td></tr>";
		
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 8><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table border=1 align=center width=100%><tr><th><b>Student Name</b></th><th><b>ICNO</b></th><th><b>Course</b></th><th><b>Exam Date</b></th></th><th><b>Venue</b></th><th><b>Result</b></th><th><b>Attempts</b></th></tr>';
		$cnts  = 0;
		$centerarray =array();
		$centerarrays =array();
		$centerarrayss =array();
		$centerarraysss =array();
		foreach($data as $lobjCountry)
		{
			    $tabledata.="<tr>";
				
				$tabledata.="</td>";
			    $tabledata.= '<td>'.$lobjCountry['StudentName'].'</td><td>'.$lobjCountry['ICNO'].'</td>';
				
				$tabledata.="<td>";				
				$tabledata.=$lobjCountry['Coursename'];
				$tabledata.="</td>"; 
				 
				 //$tabledata.= "&nbsp;".date('d-m-Y',strtotime($lobjCountry['DateTime']));
			    $tabledata.="<td>";
				$tabledata.="&nbsp;".$lobjCountry['Date'];
                $tabledata.="</td>";				
			
			    $tabledata.="<td>";				
				$tabledata.=$lobjCountry['Venue'];
			    $tabledata.="</td>";
				
			    // $tabledata.="<td>";				
				//$tabledata.=$lobjCountry['managesessionname'];
				//$tabledata.="</td>";
				 $tabledata.="<td>";				
				$tabledata.=$lobjCountry['Result'];
			    $tabledata.="</td>";
				
				
			    //$tabledata.= '<td>Pass</td>';
				//$lobjCountry['Venue']
				$tabledata.="<td>";
				$tabledata.=$lobjCountry['Attempts'];
				$tabledata.="</td>";
			    $tabledata.="</tr>";
				
		
		}
		$tabledata.="<tr>";
		$tabledata.="<td colspan='7' align='right'>";
		$tabledata.="TOTAL:".count($data);
		$tabledata.="</td>";
		$tabledata.="</tr>";
		    $tabledata.="</table>";	


if(isset($larrformData['pdf']))
		{
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img align=center width=100% src="../public/images/header.jpg" /><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                            '.'{PAGENO}{nbpg}' );	
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';		
		$mpdf->WriteFixedPosHTML ( "<table border=1  align=center width=100%><tr><td align=center> {$ReportName}</td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		$html = htmlspecialchars_decode ($tabledata);
		$mpdf->WriteHTML($html);
		$mpdf->Output($filename.pdf,'D');
		// Write to Logs
		$auth = Zend_Auth::getInstance();
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Takaful Operator Report(PDF)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);	
		}
else
{		
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
			}
		
	}
public function getstudentsnamesAction(){
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$newresult="";
		//$opType = $this->_getParam('opType');	
		$FromDate = $this->_getParam('FromDate');
        $ToDate = $this->_getParam('ToDate');
		//echo $FromDate;
		//echo $ToDate;
		$Namestring = $this->_getParam('namestring');
			
		$larrresultopnames = $lobjExamreportModel->fnGetstudentsNames($FromDate,$ToDate,$Namestring);
		//echo Zend_Json_Encoder::encode ( $larrresultopnames );
		//exit;
		
		foreach ($larrresultopnames as $larrresnewarray){
						$opname=$larrresnewarray['name'];
			$newresult=$newresult."<tr><td><span id='idspan'   onclick='fnsetvalue(\"".$opname."\");'>".$larrresnewarray['name']."</span></td></tr></BR>";			
		}
		
		echo $newresult;
		exit;
		
		
	}
	
		
}