<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');

class Reports_AttendancelistController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
		$this->fnsetObj(); //call fnsetObj
		$this->lobjCommon = new App_Model_Common();
	}
	//Function to set the objects
	public function fnsetObj()
	{
		$this->lobjReportsForm = new  Reports_Form_Attendancelistform();
		$this->lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjReportsForm;
		$larrcourses=$this->lobjExamreportModel->fngetprogramnames();
		$this->lobjReportsForm->Coursename->addMultiOption('','Select');
		$this->lobjReportsForm->Coursename->addmultioptions($larrcourses);
		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->attendancereportpaginatorresult);
		$lintpagecount = 10000;
		$lintpage = $this->_getParam('page',1); //Paginator instance
		$result = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->attendancereportpaginatorresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($result,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			$fromdate = $this->view->fromdate=$larrformData['Date3'];
			$todate = $this->view->todate=$larrformData['Date4'];
			$cname = $this->view->cname=$larrformData['Coursename'];
			$venue = $this->view->venue=$larrformData['Venues'];
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost ();
				$fromdate = $this->view->fromdate=$larrformData['Date3'];
				$todate = $this->view->todate=$larrformData['Date4'];
				$cname = $this->view->cname=$larrformData['Coursename'];
				$venue = $this->view->venue=$larrformData['Venues'];
				unset ( $larrformData ['Search'] );
				$result = $this->lobjExamreportModel->fngetexamanddatedetails($larrformData); //searching the values for the attendance
				$count=count($result);
				for($i=0;$i<$count;$i++){
					$m=1;
					$k=1;
					$resarr= explode(',',$result[$i]['Result']);
					$count1=0;
					$count2=0;
					$count3=0;
					for($j=0;$j<sizeof($resarr);$j++)
					{
						if($resarr[$j]==1)
						{
							$count1++;
						}
						if($resarr[$j]==2)
						{
							$count2++;
						}
						if($resarr[$j]==3 || $resarr[$j]==2 || $resarr[$j]==1)
						{
							$count3++;
						}
					}
					if($m==$k)
					{
						$resultedarray1[$i]=$count1;
						$resultedarray2[$i]=$count2;
						$resultedarray3[$i]=$count3;
					}
					$m++;
					$k++;
				}
			}
			for($s=0;$s<$count;$s++)
			{
				$result[$s]['pass']=$resultedarray1[$s];
				$result[$s]['fail']=$resultedarray2[$s];
				$result[$s]['sitted']=$resultedarray1[$s]+$resultedarray2[$s];
				$day= date("d-m-Y");
				if($result[$s]['Date']<=$day)
				{
					$result[$s]['absent'] = $result[$s]['applied']-$result[$s]['sitted'];//$result[$s]['absent']=0;
				}
				else
				{
					$result[$s]['absent'] = $result[$s]['applied']-$result[$s]['sitted'];
				}
			}
			if($resultedarray2)
			{
				$failsum = $this->view->failsum = $failsum = array_sum($resultedarray2);
				$total = $this->view->total = 'GRAND TOTAL';
			}
			if($resultedarray2)
			{
				$passsum = $this->view->passsum = array_sum($resultedarray1);
			}
			if($resultedarray2)
			{
				$appliedsum = $this->view->appliedsum = array_sum($resultedarray3);
			}
			$sittedsum = $this->view->sittedsum = ($passsum+$failsum);
			$absentsum = $this->view->absentsum = ($appliedsum-$sittedsum);
			$this->view->paginator = $this->lobjCommon->fnPagination($result,$lintpage,$lintpagecount);
			$this->gobjsessionsis->attendancereportpaginatorresult = $result;
			$larrcenters = $this->lobjExamreportModel->fnajaxgetcenternames($fromdate,$todate);
		    $this->lobjReportsForm->Venues->addMultiOption('','Select');
			$this->lobjReportsForm->Venues->addmultioptions($larrcenters);
			$this->lobjReportsForm->Venues->setValue($venue);
			$this->view->lobjform->populate($larrformData);
		}
	}
	public function fngetcentresAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$ldfromdate = $this->_getParam('regFromDate');
		$ldtodate = $this->_getParam('toDate');
		$larrcenters = $this->lobjExamreportModel->fnajaxgetcenternames($ldfromdate,$ldtodate);
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		//$larrcentreDetailss[] = array('key'=>'0',name=>'Select');
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}
	public function pdfexportAction()
	{   
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformdata1 = $this->_request->getPost();
		$larrformdata = array();
	    if($larrformdata1['venue'])
		{
			$larrcentre = $this->lobjExamreportModel->fngetcentername($larrformdata1['venue']);
			$larrformdata['CentreName'] = $larrcentre['centername'];
		}else{
			$larrformdata['CentreName'] = 'All';
		}
		$fdate = $larrformdata1['fromdate'];
		$tdate = $larrformdata1['todate'];
		$frmdate =date('d-m-Y',strtotime($fdate));
		$todate =date('d-m-Y',strtotime($tdate));
		$larrformdata['Date3'] = $fdate;//$larrformdata1['cname'];
		$larrformdata['Date4'] = $tdate;//$larrformdata1['cname'];
		$larrformdata['Coursename'] = $larrformdata1['cname'];
		$larrformdata['Venues'] = $larrformdata1['venue'];
		$lstrreportytpe = $larrformdata1['Print1'];//$this->_getParam('reporttype');
		$result = $this->lobjExamreportModel->fngetexamanddatedetails($larrformdata);
		$count=count($result);
		for($i=0;$i<$count;$i++)
		{
			$m = 1;
			$k = 1;
			$resarr = explode(',',$result[$i]['Result']);
			$count1 = 0;
			$count2 = 0;
			$count3 = 0;
			for($j=0;$j<sizeof($resarr);$j++)
			{
				if($resarr[$j]==1)
				{
					$count1++;
				}
				if($resarr[$j]==2)
				{
					$count2++;
				}
				if($resarr[$j]==3 || $resarr[$j]==2 || $resarr[$j]==1)
				{
					$count3++;
				}
			}
			if($m==$k)
			{
				$resultedarray1[$i] = $count1;
				$resultedarray2[$i] = $count2;
				$resultedarray3[$i] = $count3;
			}
			$m++;
			$k++;
		}
		for($s=0;$s<$count;$s++)
		{
			$result[$s]['pass']=$resultedarray1[$s];
			$result[$s]['fail']=$resultedarray2[$s];
			$result[$s]['sitted']=$resultedarray1[$s]+$resultedarray2[$s];
			$day= date("d-m-Y");
			if($result[$s]['Date']>=$day)
			{
				$result[$s]['absent'] = $result[$s]['applied']-$result[$s]['sitted'];//$result[$s]['absent']=0;
			}
			else
			{
				$result[$s]['absent'] = $result[$s]['applied']-$result[$s]['sitted'];
			}
		}
		$failsum = $larrformdata1['totalfail'];
		$passsum = $larrformdata1['totalpass'];
		$appliedsum = $larrformdata1['totalapplied'];
		$sittedsum = $larrformdata1['totalsitted'];
		$absentsum = $larrformdata1['totalabsent'];
		$total = ' GRAND TOTAL';
		$centerarray = array();
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbe/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Attendance_List_Report_'.$frmdate.'_'.$todate;
		$ReportName = $this->view->translate( "Attendance" ).' '.$this->view->translate( "List" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Excel')
		{
			$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
		}
		else
		{
		    $tabledata = '<img width=100% src="../public/images/reportheader.jpg" /><br><br><br><br><br>';
		}
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 2><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 2><b> Time</b></td><td align=left colspan = 2><b>$time</b></td></tr>";
		$tabledata.= "<tr><td align=left colspan = 2><b>Exam From Date </b></td><td align=left colspan = 2><b>".$frmdate."</b></td><td  align=left colspan = 2><b> Exam To Date </b></td><td align=left colspan = 2><b>".$todate."</b></td></tr>";
		$tabledata.= "<tr><td align=left colspan = 2><b>Venue </b></td><td align=left colspan = 2><b>".$larrformdata['CentreName']."</b></td><td  align=left colspan = 2><b>  </b></td><td align=left colspan = 2><b> </b></td></tr></table>";
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 8><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table border=1 align=center width=100%><tr><th><b>Exam Date</b></th><th><b>Venue</b></th><th><b>Course</b></th><th><b>Applied</b></th><th><b>Sitted</b></th><th><b>Absent</b></th><th><b>Pass</b></th><th><b>Fail</b></th></tr>';
		$cnts  = 0;
		foreach($result as $lobjCountry)
		{
			if(!in_array($lobjCountry['Date'],$centerarray) && $cnts != 0)
			{
				$tabledata.="<tr >
								<td colspan='3' align='center'><b>TOTAL DATEWISE</b></td>
								<td><b>".$sumarray['applied'][$datesss]."</b></td>
								<td><b>".$sumarray['sitted'][$datesss]."</b></td>
								<td><b>".$sumarray['absent'][$datesss]."</b></td>
								<td><b>".$sumarray['pass'][$datesss]."</b></td>
								<td><b>".$sumarray['fail'][$datesss]."</b></td>
							</tr>";
			}
			$cnts = 1;
			$tabledata.="<tr>";
			$tabledata.="<td>";
			if(!in_array($lobjCountry['Date'],$centerarray))
			{
				$centerarray[] = $lobjCountry['Date'];
				$centerarrays[$lobjCountry['Date']] = array();
				$tabledata.= $lobjCountry['Date'];
				$datesss = $lobjCountry['Date'];
				$sumarray['applied'][$lobjCountry['Date']] = 0;
				$sumarray['sitted'][$lobjCountry['Date']]  = 0;
				$sumarray['absent'][$lobjCountry['Date']]  = 0;
				$sumarray['pass'][$lobjCountry['Date']]    = 0;
				$sumarray['fail'][$lobjCountry['Date']]    = 0;
			}
			$sumarray['applied'][$lobjCountry['Date']] = $sumarray['applied'][$lobjCountry['Date']] + $lobjCountry['applied'];
			$sumarray['sitted'][$lobjCountry['Date']] = $sumarray['sitted'][$lobjCountry['Date']] + $lobjCountry['sitted'];
			$sumarray['absent'][$lobjCountry['Date']] = $sumarray['absent'][$lobjCountry['Date']] + $lobjCountry['absent'];
			$sumarray['pass'][$lobjCountry['Date']] = $sumarray['pass'][$lobjCountry['Date']] + $lobjCountry['pass'];
			$sumarray['fail'][$lobjCountry['Date']] = $sumarray['fail'][$lobjCountry['Date']] + $lobjCountry['fail'];
			$tabledata.="</td>";
			$tabledata.="<td>";
			if(!in_array($lobjCountry['Venue'],$centerarrays[$lobjCountry['Date']]))
			{
				$centerarrays[$lobjCountry['Date']][] = $lobjCountry['Venue'];
				$tabledata.=$lobjCountry['Venue'];
			}
			$tabledata.="</td>";
			$tabledata.= '<td>'.$lobjCountry['Course'].'</td><td>'.$lobjCountry['applied'].'</td><td>'.$lobjCountry['sitted'].'</td><td>'.$lobjCountry['absent'].'</td><td>'.$lobjCountry['pass'].'</td><td>'.$lobjCountry['fail'].'</td>';
			$tabledata.="</tr>";
		}
		$tabledata.="<tr>
						<td colspan='3' align='center'><b>TOTAL DATEWISE</b></td><td><b>".$sumarray['applied'][$datesss]."</b></td><td><b>".$sumarray['sitted'][$datesss]."</b></td><td><b>".$sumarray['absent'][$datesss]."</b></td><td><b>".$sumarray['pass'][$datesss]."</b></td><td><b>".$sumarray['fail'][$datesss]."</b></td>
					 </tr>";
		$tabledata.="<tr><td colspan='8'> &nbsp;</td></tr>";
		$tabledata.="<tr>
						<td colspan='3' align='center'><b>{$total}</b></td><td><b>{$appliedsum}</b></td><td><b>{$sittedsum}</b></td><td><b>{$absentsum}</b></td><td><b>{$passsum}</b></td><td><b>{$failsum}</b></td>
					</tr>";	
		$tabledata.="</table>";
		if($lstrreportytpe=='Excel'){
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}else {
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}
	}

}
