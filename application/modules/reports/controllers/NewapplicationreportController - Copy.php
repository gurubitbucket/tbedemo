<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_NewapplicationreportController extends Base_Base{
	
	public function init()
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjstudentappform = new Reports_Form_Allreportsform();
		$this->Studentapplicationmodel = new Reports_Model_DbTable_Studentapplicationmodel();
	}
	
	public function indexAction() 
	{
		$this->view->lobjstudentappform = $this->lobjstudentappform;
		$larrprograms = $this->Studentapplicationmodel->fngetprogramnames();
		$this->lobjstudentappform->Coursename->addMultioptions($larrprograms);
		$larrcenters = $this->Studentapplicationmodel->fngetcenternames();
		$this->lobjstudentappform->Venues->addMultioptions($larrcenters);
		if(!$this->_getParam('Search'))
		unset($this->gobjsessionsis->attendancereportpaginatorresult);
		$lintpagecount = 10000;
		$lintpage = $this->_getParam('page',1);
		$larrresult = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
		{
			
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->attendancereportpaginatorresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			unset ( $larrformData ['Search']);
			if($larrformData['Search'] == '' && $larrformData['Dates'] == '' && $larrformData['Dates2'] == '' && $larrformData['UpDate1'] == '' && $larrformData['UpDate2'] == '' && $larrformData['Studentname'] == '' && $larrformData['ICNO'] == '' && $larrformData['Coursename'] == '' && $larrformData['Venues'] == '' && $larrformData['Payment'] == '' &&$larrformData['paymentmode'] == '')
				{
				  echo '<script language="javascript">alert("Please select atleast one field for the result")</script>';
				  echo "<script>parent.location = '".$this->view->baseUrl()."/reports/newapplicationreport/index';</script>";
 				  die();
				}
			if ($this->lobjstudentappform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				unset ( $larrformData ['Search']);
				$this->view->set = 1;
				$larrresult = $this->Studentapplicationmodel->fngetalldetails($larrformData);
				$this->view->total = count($larrresult);
				$this->view->formdata = $larrformData;
				$this->view->paginator = $larrresult;
				$this->view->lobjstudentappform->populate($larrformData);
			}
		}
	}
	
	public function getstudentdetailsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidapp = $this->_getParam('idapp');
		$lobjCountry = $this->Studentapplicationmodel->fngetpersonaldetails($lintidapp);
		$larrregsetails = $this->Studentapplicationmodel->fngetregdetails($lintidapp,$lobjCountry['type']);
		if($lobjCountry['type']==0){
			$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Registration Details </legend>
					                    <table class="table" border=1 align = "center" width=100%><tr><th><b>Registered Through</b></th><th><b>Company/Takaful Name</b></th><th><b>Batch ID</b></th></tr><tr>';
						  $tabledata.= '<td align = "left">'.$lobjCountry['Typename'].'</td><td align = "left"> NA </td><td align = "left">NA</td></tr>';
					      $tabledata.="</table></fieldset><br><br>";
		$tabledata.= '<fieldset><legend align = "left"> Student Personal Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>Qualification</b></th><th><b>Race</b></th><th><b>Age</b></th><th><b>Gender</b></th><th><b>Email Id</b></th></tr><tr>";
		$tabledata.="<td align = 'left'>".$lobjCountry['fname']."</td><td align = 'left'>".$lobjCountry['Qualification']."</td><td align = 'left'>".$lobjCountry['Race']."</td><td align = 'left'>".$lobjCountry['age']."</td><td align = 'left'>".$lobjCountry['Gender']."</td><td align = 'left'>".$lobjCountry['EmailAddress']."</td></tr>";
		$tabledata.="</table><br>";
		$tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
		}else{
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Registration Details </legend>
					                    <table class="table" border=1 align = "center" width=100%><tr><th><b>Registered Through</b></th><th><b>Company/Takaful Name</b></th><th><b>Batch ID</b></th></tr><tr>';
						  $tabledata.= '<td align = "left">'.$lobjCountry['Typename'].'</td><td align = "left">'.$larrregsetails['name'].'</td><td align = "left">'.$larrregsetails['RegistrationPin'].'</td></tr>';
					      $tabledata.="</table></fieldset><br><br>";
		$tabledata.= '<fieldset><legend align = "left"> Student Personal Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>Qualification</b></th><th><b>Race</b></th><th><b>Age</b></th><th><b>Gender</b></th><th><b>Email Id</b></th></tr><tr>";
		$tabledata.="<td align = 'left'>".$lobjCountry['fname']."</td><td align = 'left'>".$lobjCountry['Qualification']."</td><td align = 'left'>".$lobjCountry['Race']."</td><td align = 'left'>".$lobjCountry['age']."</td><td align = 'left'>".$lobjCountry['Gender']."</td><td align = 'left'>".$lobjCountry['EmailAddress']."</td></tr>";
		$tabledata.="</table><br>";
		$tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
		}
		echo  $tabledata;
	}
	public function fnpdfexportAction()
		{
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$larrformData = $this->_request->getPost();
			if($larrformData['programname']){
				$larrpname = $this->Studentapplicationmodel->fngetprogramname($larrformData['programname']);
				$lstrpname = $larrpname['ProgramName'];
			}else{
				$lstrpname = 'ALL';
			}
			if($larrformData['centername']){
				$larrcname = $this->Studentapplicationmodel->fngetcentername($larrformData['centername']);
				$lstrcname = $larrcname['centername'];
			}else{
				$lstrcname = 'ALL';
			}
			$ReportType = $larrformData['Print2'];
			$larrresult = $this->Studentapplicationmodel->fngetalldetails($larrformData);
			$total = count($larrresult);
			$ldtfromdate = $larrformData['Dates'];
			$ldttodate = $larrformData['Dates2'];
			$ldtappfromdate = $larrformData['UpDate1'];
			$ldtapptodate = $larrformData['UpDate2'];
			if($ldtfromdate){
			$fdate = date('d-m-Y',strtotime($ldtfromdate));
			}else{
			  $fdate = date('d-m-Y',strtotime($ldtappfromdate));;
			}
			if($ldttodate){
			$tdate = date('d-m-Y',strtotime($ldttodate)); 
			}else{
			  $tdate = date('d-m-Y',strtotime($ldtapptodate));
			}
			 
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
			$time = date('h:i:s',time());
			$filename = 'StudentApplication_Report_'.$fdate.'_'.$tdate;
			$ReportName = $this->view->translate( "StudentApplication" ).' '.$this->view->translate( "Report" );
			if($ReportType=='PDF')
			{
				$tabledata = '<img width=100% src="../public/images/reportheader.jpg" /><br>';	
			}
			else
			{
			    $tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
			}
	   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 2><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 2><b> Time</b></td><td align=left colspan = 3><b>$time</b></td></tr>";
	        $tabledata.= "<tr><td align=left colspan = 2><b>From Date </b></td><td align=left colspan = 2><b>".$fdate."</b></td><td  align=left colspan = 2><b>To Date</b></td><td align=left colspan = 3><b>".$tdate."</b></td></tr>";
	   	    $tabledata.= "<tr><td align=left colspan = 2><b>Program Name </b></td><td align=left colspan = 2><b>".$lstrpname."</b></td><td  align=left colspan = 2><b>Venue</b></td><td align=left colspan = 3><b>".$lstrcname."</b></td></tr></table>";
	   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 9><b> {$ReportName}</b></td></tr></table><br>";
	        $tabledata.= '<table border=1 align=center width=100%><tr><th><b>Student Name</b></th><th><b>ICNO</b></th><th><b>Course Name</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Applied Date</b></th><th><b>Type</b></th><th><b>Status</b></th><th><b>Payment Status</b></th><th><b>Payment Mode</b></th></tr>';
	        foreach($larrresult as $lobjCountry)
			{
				$tabledata.= '<tr><td>'.$lobjCountry['fname'].'</td><td>'.$lobjCountry['icno'].'</td><td>'.$lobjCountry['programname'].'</td><td>'.$lobjCountry['centername'].'</td><td>'.$lobjCountry['ExamDate1'].'</td><td>'.$lobjCountry['Applieddate'].'</td><td>'.$lobjCountry['Type'].'</td><td>'.$lobjCountry['Result'].'</td><td>'.$lobjCountry['PaymentStatus'].'</td><td>'.$lobjCountry['Modeofpayment'].'</td></tr>';
			}
			$tabledata.="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><b>TOTAL</b></td><td align = 'left'><b>".$total."</b></td></tr>";
			$tabledata.="</table>";
			if($ReportType == "PDF"){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
			}else
				{
				 $ourFileName = realpath('.')."/data";
				 $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
				 ini_set('max_execution_time', 3600);
				 fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
				 fclose($ourFileHandle);
				 header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
				 header("Content-Disposition: attachment; filename=$filename.xls");
				 header("Pragma: no-cache");
				 header("Expires: 0");
				 readfile($ourFileName);
				 unlink($ourFileName);
			   }
		}
	 
}