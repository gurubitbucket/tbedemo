<?php
    error_reporting (E_ALL ^ E_WARNING);
    error_reporting (E_ALL ^ E_NOTICE);
    ini_set('memory_limit', '-1');
class Reports_NewapplicationreportController extends Base_Base {
	
	public function indexAction() 
	{
		$this->view->checkEmpty = 0;
		$lobjReportsForm = new  Reports_Form_Report();
		$this->lobjstudentappform = new Reports_Form_Studentapplicationform();
		$lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$this->view->lobjform = $lobjReportsForm;
		$this->view->lobjstudentappform = $this->lobjstudentappform;
		$larrcentres = $lobjExamreportModel->fngetcenternames();
		$this->lobjstudentappform->Venues->addMultiOption('','Select'); 	
		$this->lobjstudentappform->Venues->addmultioptions($larrcentres);
		$larrcourses = $lobjExamreportModel->fngetprogramnames();		
		$this->lobjstudentappform->Coursename->addMultiOption('','Select'); 	
		$this->lobjstudentappform->Coursename->addmultioptions($larrcourses);
		$larrtakafulnames=$lobjExamreportModel->fngettakafulnames();	
		$this->lobjstudentappform->Takafulname->addMultiOption('','Select'); 	
		$this->lobjstudentappform->Takafulname->addmultioptions($larrtakafulnames);
	    $jsondata = '{
    				"label":"StudentName",
					"identifier":"Serialno",
					"items":""
				  }';
		$this->view->jsondata = $jsondata;
 		if($this->_request->isPost() && $this->_request->getPost('Generate')) 
 		{
 			$larrformData = $this->_request->getPost();
 			unset($larrformData['Generate']);
 			if ($this->lobjstudentappform->isValid ( $larrformData )) 
 			{
	 			$result = $lobjExamreportModel->fngetnewsummarydetailsapplication($larrformData);	
	 			$count=count($result);
				$lobjExamdetailsmodel = new App_Model_Examdetails();
	 			 for($i=0;$i<count($result);$i++)
	 			   {    
	 			   	    $result[$i]['Serialno'] = $i+1;
	 			   }
	 			if($result) $this->view->checkEmpty = 1;	
				$page = $this->_getParam('page',1);
				$this->view->counter = (count($result));
				$this->view->lobjPaginator = $result;
				$jsonresult = Zend_Json_Encoder::encode($result);
	    		$jsondata = '{
	    				"label":"StudentName",
						"identifier":"Serialno",
						"items":'.$jsonresult.
					  '}';
				$this->view->jsondata = $jsondata;
				$larrcenters = $lobjExamreportModel->fnajaxgetcenternames($larrformData['Dates'],$larrformData['Dates2']);
				$this->lobjstudentappform->Venues->addMultiOption('','Select'); 	
				$this->lobjstudentappform->Venues->addmultioptions($larrcenters);
				$this->lobjstudentappform->Venues->setValue($larrformData['Venues']);
				$this->view->lobjstudentappform->populate($larrformData);
		}	
	  }
	}
	public function generatereportAction()
	{
		$lobjReportsForm = new  Reports_Form_Report();
		$this->view->lobjform = $lobjReportsForm;
		if($this->_request->getPost())
		{
			$larrformData = $this->_request->getPost();
			$this->view->datacount = $larrformData['datacount'];
			$this->view->datacounttable = $larrformData['datacounttable'];
		}
		else
		{
			$this->_redirect( $this->baseUrl . 'reports/newapplicationreportController/index');
	    }
	 }
	public function fngetcentresAction()
		{
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$lobjExamreportModel = new Reports_Model_DbTable_Examreport();
			$ldfromdate = $this->_getParam('regFromDate');
			$ldtodate = $this->_getParam('toDate');
			$larrcenters = $lobjExamreportModel->fnajaxgetcenternames($ldfromdate,$ldtodate);
			$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
			echo Zend_Json_Encoder::encode($larrcentreDetailss);
		}
	public function pdfexportAction()
		{
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$larrformData = $this->_request->getPost();
			$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
			$htmldata = $larrformData['datacount'];
			$htmltabledata = $larrformData['datacounttable'];
			$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
			if($larrformData['ReportType'] == "pdf"){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->WriteHTML('<img align=center width=100% src="../public/images/header.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                            '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			$ReportName = $this->view->translate( "Summary" ).' '.$this->view->translate( "Report" );
			$mpdf->WriteFixedPosHTML ( "<table border=1  align=center width=100%><tr><td align=center> {$ReportName}</td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
			ini_set('max_execution_time',3600);
			$html = htmlspecialchars_decode ( $htmltabledata );
			$mpdf->WriteHTML($html);
			$mpdf->Output('Student_Application.pdf','D');
			}else
				{
				 $ourFileName = realpath('.')."/data";
				 $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); 
				 ini_set('max_execution_time', 3600);
				 fwrite($ourFileHandle,htmlspecialchars_decode($htmldata));
				 fclose($ourFileHandle);
				 header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
				 header("Content-Disposition: attachment; filename = Summary_Report.xls");
				 header("Pragma: no-cache");
				 header("Expires: 0");
				 readfile($ourFileName);
				 unlink($ourFileName);
			   }
		}
	 
}