<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_CenterwisestatisticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	        Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	        $this->lobjStasticsForm = new  Reports_Form_Centerwisestatisticsform();
		$this->lobcenterwisestatistic = new Reports_Model_DbTable_Centerwisestatisticsmodel();		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjStasticsForm;
                $this->view->search = 1;
               // $this->view->lobjform->Year->setAttrib('onchange','fngetmonths()');
               //  $this->view->lobjform->ToYear->setAttrib('onchange','fngetmonthsforToyear()');
		//$year = 2012;
		//$lintcuryear = date("Y"); 
		//for($linti=$year;$linti<=$lintcuryear;$linti++){
			//$larryears[$linti] = $linti;
		//} 
		//$this->lobjStasticsForm->Year->addmultioptions($larryears);
                //$this->lobjStasticsForm->ToYear->addmultioptions($larryears);
                if ($this->_request->isPost () && $this->_request->getPost('Search')) 
		    {   
                    
                       $larrformData = $this->_request->getPost();
                       //echo "<pre>";
                       //print_r($larrformData);die();
                       $larresults = $this->lobcenterwisestatistic->fngetresultfordates($larrformData);
                       echo "<pre>";
                       print_r($larresults);die();
                    }
                }
			//}
 	      //}
	
			public function fnexportexcelAction()
				{    
					 $this->_helper->layout->disableLayout();
					 $this->_helper->viewRenderer->setNoRender();
					 $larrformData = $this->_request->getPost();
					 unset ( $larrformData['ExportToExcel']);
					 $lday= date("d-m-Y");
					 $ltime = date('h:i:s',time());
					 $host = $_SERVER['SERVER_NAME'];
					 $imgp = "http://".$host."/tbenew/images/reportheader.jpg";
					 $filename = 'Age_Statistics_Report_'.$larrformData['selmonth'];
					 $ReportName = $this->view->translate( "Age" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
					 $tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2><b>Date </b></td><td align ='left' colspan = 4><b>$lday</b></td><td align ='left' colspan = 3><b> Time</b></td><td align ='left' colspan = 5><b>$ltime</b></td></tr></table>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 14 align=center><b>{$ReportName}</b></td></tr></table><br>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 12><b>".$larrformData['selmonth']."</b></td></tr>";
					 $tabledata.="<tr> <td align ='left' colspan = 2><b>Age</b></td><td align ='center' colspan = 1><b>18-21</b></td><td align ='center' colspan = 1><b>22-25</b></td><td align ='center' colspan = 1><b>26-29</b></td><td align ='center' colspan = 1><b>30-34</b></td><td align ='center' colspan = 1><b>35-39</b></td><td align ='center' colspan = 1><b>40-44</b></td><td align ='center' colspan = 1><b>45-49</b></td><td align ='center' colspan = 1><b>50-54</b></td><td align ='center' colspan = 1><b>55-59</b></td><td align ='center' colspan = 1><b>60 Above</b></td><td align ='center' colspan = 2><b>Total</b></td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td><td align ='center' colspan = 1>".$larrformData['18-21']."</td><td align ='center' colspan = 1>".$larrformData['22-25']."</td><td align ='center' colspan = 1>".$larrformData['26-29']."</td><td align ='center' colspan = 1>".$larrformData['30-34']."</td><td align ='center' colspan = 1>".$larrformData['35-39']."</td><td align ='center' colspan = 1>".$larrformData['40-44']."</td><td align ='center' colspan = 1>".$larrformData['45-49']."</td><td align ='center' colspan = 1>".$larrformData['50-54']."</td><td align ='center' colspan = 1>".$larrformData['55-59']."</td><td align ='center' colspan = 1>".$larrformData['60Above']."</td><td align ='center' colspan = 2>".$larrformData['regtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td><td align ='center' colspan = 1>".$larrformData['18-21sat']."</td><td align ='center' colspan = 1>".$larrformData['22-25sat']."</td><td align ='center' colspan = 1>".$larrformData['26-29sat']."</td><td align ='center' colspan = 1>".$larrformData['30-34sat']."</td><td align ='center' colspan = 1>".$larrformData['35-39sat']."</td><td align ='center' colspan = 1>".$larrformData['40-44sat']."</td><td align ='center' colspan = 1>".$larrformData['45-49sat']."</td><td align ='center' colspan = 1>".$larrformData['50-54sat']."</td><td align ='center' colspan = 1>".$larrformData['55-59sat']."</td><td align ='center' colspan = 1>".$larrformData['60Abovesat']."</td><td align ='center' colspan = 2>".$larrformData['sattotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td><td align ='center' colspan = 1>".$larrformData['18-21pass']."</td><td align ='center' colspan = 1>".$larrformData['22-25pass']."</td><td align ='center' colspan = 1>".$larrformData['26-29pass']."</td><td align ='center' colspan = 1>".$larrformData['30-34pass']."</td><td align ='center' colspan = 1>".$larrformData['35-39pass']."</td><td align ='center' colspan = 1>".$larrformData['40-44pass']."</td><td align ='center' colspan = 1>".$larrformData['45-49pass']."</td><td align ='center' colspan = 1>".$larrformData['50-54pass']."</td><td align ='center' colspan = 1>".$larrformData['55-59pass']."</td><td align ='center' colspan = 1>".$larrformData['60Abovepass']."</td><td align ='center' colspan = 2>".$larrformData['passtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td><td align ='center' colspan = 1>".$larrformData['18-21fail']."</td><td align ='center' colspan = 1>".$larrformData['22-25fail']."</td><td align ='center' colspan = 1>".$larrformData['26-29fail']."</td><td align ='center' colspan = 1>".$larrformData['30-34fail']."</td><td align ='center' colspan = 1>".$larrformData['35-39fail']."</td><td align ='center' colspan = 1>".$larrformData['40-44fail']."</td><td align ='center' colspan = 1>".$larrformData['45-49fail']."</td><td align ='center' colspan = 1>".$larrformData['50-54fail']."</td><td align ='center' colspan = 1>".$larrformData['55-59fail']."</td><td align ='center' colspan = 1>".$larrformData['60Abovefail']."</td><td align ='center' colspan = 2>".$larrformData['failtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td><td align ='center' colspan = 1>".$larrformData['18-21absent']."</td><td align ='center' colspan = 1>".$larrformData['22-25absent']."</td><td align ='center' colspan = 1>".$larrformData['26-29absent']."</td><td align ='center' colspan = 1>".$larrformData['30-34absent']."</td><td align ='center' colspan = 1>".$larrformData['35-39absent']."</td><td align ='center' colspan = 1>".$larrformData['40-44absent']."</td><td align ='center' colspan = 1>".$larrformData['45-49absent']."</td><td align ='center' colspan = 1>".$larrformData['50-54absent']."</td><td align ='center' colspan = 1>".$larrformData['55-59absent']."</td><td align ='center' colspan = 1>".$larrformData['60Aboveabsent']."</td><td align ='center' colspan = 2>".$larrformData['absenttotal']."</td></tr>";
					 $ourFileName = realpath('.')."/data";
					 $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
					 ini_set('max_execution_time', 3600);
					 fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
					 fclose($ourFileHandle);
					 header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
					 header("Content-Disposition: attachment; filename=$filename.xls");
					 header("Pragma: no-cache");
					 header("Expires: 0");
					 readfile($ourFileName);
					 unlink($ourFileName);
				}	
}	
 