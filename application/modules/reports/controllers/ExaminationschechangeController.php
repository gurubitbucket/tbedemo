<?php
class Reports_ExaminationschechangeController extends Base_Base {
	
private $_gobjlogger; 
	public function init() 
	{
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
    
	public function indexAction() 
	{
		$this->view->checkEmpty = 0;
		$lobjReportsForm = new  Reports_Form_Examvenuechange();
		$lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$lobjExamschchangereportModel = new Reports_Model_DbTable_Examschchangereport();
		$this->view->lobjform = $lobjReportsForm;
				
		$larrcourses=$lobjExamschchangereportModel->fngetstudentnameschange();			
		$lobjReportsForm->Coursename->addmultioptions($larrcourses);
		
		
		
	    $jsondata = '{
    				"label":"StudentName",
					"identifier":"Serialno",
					"items":""
				  }';
		$this->view->jsondata = $jsondata;
		
		
		$today=date('Y-m-d');
		//echo $today;die();
		
		$this->view->lobjform->Date->setValue($today);
			$this->view->lobjform->Date2->setValue($today);
 		
 		if($this->_request->isPost() && $this->_request->getPost('Generate')) 
 		{
 			$larrformData = $this->_request->getPost();
 			unset($larrformData['Generate']);
 			if ($lobjReportsForm->isValid ( $larrformData )) 
 			{
 			//print_r($larrformData);die();
 			
 			
 			if($larrformData['Coursename'])
					{		
		$this->view->lobjform->Coursename->setValue($larrformData['Coursename']);
					}
		
			if($larrformData['ICNO'])
					{	
			$this->view->lobjform->ICNO->setValue($larrformData['ICNO']);
					}
					
					
 				$result = $lobjExamschchangereportModel->fngetexamschedulechange($larrformData);
 				$count=count($result);
 				$this->view->count = $count;
 				//print_r($result);die();
 	       		
 				$this->view->results=$result;
 				//$count=count($result);
 				
 			
 			
		}	
	  }
	  
	  
			if($this->_request->isPost() && $this->_request->getPost('Print')) 
 		{
 			$larrformData = $this->_request->getPost();
 			//echo "<pre>";print_r($larrformData);die();
 			unset($larrformData['Generate']);
 			if ($lobjReportsForm->isValid ( $larrformData )) 
 			{
 				$result = $lobjExamschchangereportModel->fngetexamschedulechange($larrformData);
 				$this->view->results=$result;
 				$fdate = $larrformData['Date'];
 				$tdate = $larrformData['Date2'];
 			    $frmdate =date('d-m-Y',strtotime($fdate));
				$todate =date('d-m-Y',strtotime($tdate));
 		      //  print_r();die();
 				$count=count($result);
 				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();
 				include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
				$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
				$mpdf->SetDirectionality ( $this->gstrHTMLDir );
				$mpdf->text_input_as_HTML = true;
				$mpdf->useLang = true;
				$mpdf->SetAutoFont();
				$mpdf->SetDisplayMode('fullpage');
				$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
				$mpdf->pagenumSuffix = ' / ';
				$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                            '.'{PAGENO}{nbpg}' );
				$mpdf->allow_charset_conversion = true; // Set by default to TRUE
				$mpdf->charset_in = 'utf-8';
				$ReportName = $this->view->translate( "Examination" ).' '.$this->view->translate( "Schedule" ).' '.$this->view->translate( "Change" ).' '.$this->view->translate( "Report" );
				$day= date("d-m-Y");
				$time = date('h:i:s a', time());
				ini_set('max_execution_time',3600);
				$html = htmlspecialchars_decode ( $htmltabledata );
				if($fdate){
					$filename = $ReportName.'_'.$frmdate.'_'.$todate;
				}else
				{
					$filename = $ReportName.'_'.$day.'_'.$time;
				}
				$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
				$tabledata.= "<br><table border=1  align=center width=100%><tr><td><b>Date </b></td><td><b>$day</b></td><td><b> Time</b></td><td><b>$time</b></td></tr></table>";
				$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
				$tabledata.= '<table border=1 width=100%><tr> <th colspan="2"></th><th colspan="2" style="text-align: center">Course Name</th><th colspan="2" style="text-align: center">Exam Venue</th>  <th colspan="2" style="text-align: center">Exam Session</th>< <th colspan="2" style="text-align: center">Exam Date</th><th></th></tr>';
				$centerarray = array();
				$centerarray1 = array();
				$tabledata.= '<tr><th><b>Candidate Name</b></th><th><b>ICNO</b></th><th><b>Old Course</b></th><th><b> New Course</b></th><th><b>Old Venue</b></th><th><b>New Venue</b></th><th><b>Old Session</b></th><th><b>New Session</b></th><th><b>Old Date</b></th><th><b>New Date</b></th><th><b>Changed by</b></th></tr>';
				?>
				<?php  for($i=0;$i<$count;$i++){ 
						$result[$i]['NewExamDate'] =  date('d-m-Y',strtotime($result[$i]['NewExamDate']));
 		        $result[$i]['oldexamdate']=   date('d-m-Y',strtotime($result[$i]['oldexamdate']));
 		        
 		        
				$tabledata.= '<tr>';
				
					$tabledata.="<td>";
				        if(!in_array($result[$i]['IDApplication'],$centerarray)){
							$centerarray[] = $result[$i]['IDApplication'];
				        	$tabledata.=$result[$i]['FName'];
	         			}
					$tabledata.="</td>";
					
					$tabledata.="<td>";
				        if(!in_array($result[$i]['IDApplication'],$centerarray1)){
							$centerarray1[] = $result[$i]['IDApplication'];
				        	$tabledata.=$result[$i]['ICNO'];
	         			}
					$tabledata.="</td>";
				
				if($result[$i]['progdate']<'2012-06-15'){ 
				$tabledata.= '<td>'.' '.'</td><td>'.' '.'</td><td>'.$result[$i]['centername'].'</td><td>'.$result[$i]['newexamvenue'].'</td><td>'.$result[$i]['managesessionname'].'</td><td>'.$result[$i]['newsession'].'</td><td>'.$result[$i]['oldexamdate'].'</td><td>'.$result[$i]['NewExamDate'].'</td><td>'.$result[$i]['username'].'</td>';
				}else { 
				$tabledata.= '<td>'. $result[$i]['oldprg'].'</td><td>'.$result[$i]['newprg'].'</td><td>'.$result[$i]['centername'].'</td><td>'.$result[$i]['newexamvenue'].'</td><td>'.$result[$i]['managesessionname'].'</td><td>'.$result[$i]['newsession'].'</td><td>'.$result[$i]['oldexamdate'].'</td><td>'.$result[$i]['NewExamDate'].'</td><td>'.$result[$i]['username'].'</td>';
				
				}
				$tabledata.= '</tr>';
								}
				$tabledata.="<tr>
					  				<td colspan='8' align='right'><b>GRAND TOTAL </b></td>
					  				<td colspan='3' align='center'><b>{$count}</b></td>
					  		</tr>";				
				$tabledata.= '</table>';
				$mpdf->WriteHTML($tabledata);
				$mpdf->Output($filename,'D');
				// Write to Logs
		$auth = Zend_Auth::getInstance();
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Examination Schedule Change Report(PDF)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);	
		}	
	  }
	}
}
