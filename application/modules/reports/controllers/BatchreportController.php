<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_BatchreportController extends Base_Base 
{	
    public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjbatchreportmodel = new Reports_Model_DbTable_Batchreportmodel();
		$this->lobjExammarksform = new  Reports_Form_Batchreport();
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjExammarksform;
		$this->lobjExammarksform->Takcomp->addMultiOptions(array(''=>'Select','1'=>'Company','2'=>'Takaful'));
	    if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->exammarksreportpaginatorresult);
		$lintpagecount = 10000;
		$lintpage = $this->_getParam('page',1); 
		$larrresult = array();
		if(isset($this->gobjsessionsis->exammarksreportpaginatorresult))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->exammarksreportpaginatorresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				unset ($larrformData ['$larrformData']);
				$this->view->Takcomp = $lintidname = $larrformData['Takcomp'];
				$this->view->Takcompnames = $lintidcomp = $larrformData['Takcompnames'];
				$this->view->Pin = $lintpin = $larrformData['Pin'];
				$larrresult = $this->lobjbatchreportmodel->fngetstuddetails($larrformData['Pin']);
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->attendancereportpaginatorresult = $larrresult;
				$larrcenters = $this->lobjbatchreportmodel->fnajaxgetcenternames($lintidname);
				$this->lobjExammarksform->Takcompnames->addmultioptions($larrcenters);
				$this->lobjExammarksform->Takcompnames->setValue($larrformData['Takcompnames']);
				$larrpins = $this->lobjbatchreportmodel->fnajaxgetpins($lintidname,$lintidcomp);
				$this->lobjExammarksform->Pin->addmultioptions($larrpins);
				$this->lobjExammarksform->Pin->setValue($larrformData['Pin']);
				$this->view->lobjform->populate($larrformData);
		   }
	   }	
    }
	public function fnajaxgettakcompnamesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidname = $this->_getParam('idname');
		$larrcenters = $this->lobjbatchreportmodel->fnajaxgetcenternames($lintidname);
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}
	public function fnajaxgetpinsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidname = $this->_getParam('idname');
		$lintidcomp = $this->_getParam('idcomp');
		$larrcenters = $this->lobjbatchreportmodel->fnajaxgetpins($lintidname,$lintidcomp);
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}
	public function fnexportAction()
    {   
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$larrresult = $this->lobjbatchreportmodel->fngetstuddetails($larrformData['Pin']);
		$larrname = $this->lobjbatchreportmodel->fngettakcompname($larrformData['Takcomp'],$larrformData['Takcompnames']);
		
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Batchregistration_Report_'.$larrname[0]['CompanyName'].'_'.$larrformData['Pin'];
		$ReportName = $this->view->translate( "Batchregistration" ).' '.$this->view->translate( "Report" );
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 3><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 1><b> Time</b></td><td align=left colspan = 2><b>$time</b></td></tr>";
        $tabledata.= "<tr><td align=left colspan = 3><b>Name</b></td><td align=left colspan = 2><b>".$larrname[0]['CompanyName']."</b></td><td  align=left colspan = 1><b> RegistrationPin</b></td><td align=left colspan = 2><b>".$larrformData['Pin']."</b></td></tr></table>";
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 8><b> {$ReportName}</b></td></tr></table><br>";
	    $tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>ICNO</b></th><th><b>Program Name</b></th><th><b>Exam Date</b></th><th><b>Exam Venue</b></th><th><b>Exam Session</b></th><th><b>Status</b></th><th><b>Grade</b></th></tr>";
	    foreach($larrresult as $lobjCountry)
		{
			$tabledata.="<tr><td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left' >".$lobjCountry['ProgramName']."</td><td align = 'left'>".$lobjCountry['ExamDate']."</td><td align = 'left'>".$lobjCountry['ExamVenue']."</td><td align = 'left'>".$lobjCountry['session']."</td><td align = 'left'>".$lobjCountry['Result']."</td><td align = 'left'>".$lobjCountry['Grade']."</td></tr>";
		}
		$tabledata.="</table><br>";
    	
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		
    }
    
    
}