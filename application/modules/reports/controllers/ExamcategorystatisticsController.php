<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_ExamcategorystatisticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjStasticsForm = new  Reports_Form_Statistics();
		$this->lobjstastics = new Reports_Model_DbTable_Examcategorystatistics();		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjStasticsForm;
		
		    /*if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {   
		    	$larrformData = $this->_request->getPost ();
				if ($this->lobjform->isValid($larrformData)) 
				{
				 $larrformData = $this->_request->getPost ();
				 $ldtdate = $larrformData['Date'];*/
				 $ldtdate = $this->_getParam('ldtdate');
				 $this->view->searchdate = $ldtdate;
				 $larrdates = explode('-',$ldtdate);
				 $lintmon = $larrdates['1'];
				 $lintyear = $larrdates['0'];
				 $lstrmonthname = date( 'F', mktime(0,0,0,$lintmon));
				 $lintpartabid = 1;$lintpartacid = 2;$lintpartbid = 3;$lintpartcid = 4;
				 $this->view->month = $lstrmonthname;
				 $this->view->year = $lintyear;
				 unset ( $larrformData ['Search'] );
				 $larrprogramcount = $this->lobjstastics->fngetprograms($lintmon,$lintyear);
				 $lintregisteredtotal = $larrprogramcount[0]['NoOfCandidates']+$larrprogramcount[1]['NoOfCandidates']+$larrprogramcount[2]['NoOfCandidates']+$larrprogramcount[3]['NoOfCandidates'];
				 $larrprogramcount[4]['regtotal'] = $lintregisteredtotal;
				 $this->view->regtotal = $lintregisteredtotal;
				 $larrpartabresult = $this->lobjstastics->fngetprogramresult($lintmon,$lintyear,$lintpartabid);
				 $lintpartabcount = count($larrpartabresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent = 0;
				 for($linti=0;$linti<$lintpartabcount;$linti++){
				 	if($larrpartabresult[$linti]['pass']==1)
				 	{
				 		$lintpass++;
				 	}elseif ($larrpartabresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrprogramcount[]['absat'] = $lintpass+$lintfail;
				 $larrprogramcount[]['abpass'] = $lintpass;
				 $larrprogramcount[]['abfail'] = $lintfail;
				 $larrprogramcount[]['ababsent'] = $larrprogramcount[0]['NoOfCandidates']- ($lintpass+$lintfail);//;$lintabsent;
				 $larrpartacresult = $this->lobjstastics->fngetprogramresult($lintmon,$lintyear,$lintpartacid);
				 $lintpartaccount = count($larrpartacresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent = 0;
				 for($linti=0;$linti<$lintpartaccount;$linti++){
				 	if($larrpartacresult[$linti]['pass']==1)
				    {
				 		$lintpass++;
				 	}elseif ($larrpartacresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrprogramcount[]['acsat'] = $lintpass+$lintfail;
				 $larrprogramcount[]['acpass'] = $lintpass;
				 $larrprogramcount[]['acfail']= $lintfail;
				 $larrprogramcount[]['acabsent']= $larrprogramcount[1]['NoOfCandidates']- ($lintpass+$lintfail);//$lintabsent;
				 $larrpartbresult = $this->lobjstastics->fngetprogramresult($lintmon,$lintyear,$lintpartbid);
				 $lintpartbcount = count($larrpartbresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent = 0;
				 for($linti=0;$linti<$lintpartbcount;$linti++){
				 	if($larrpartbresult[$linti]['pass']==1)
				    {
				 		$lintpass++;
				 	}elseif ($larrpartbresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrprogramcount[]['bsat'] = $lintpass+$lintfail;
				 $larrprogramcount[]['bpass'] = $lintpass;
				 $larrprogramcount[]['bfail']= $lintfail;
				 $larrprogramcount[]['babsent']= $larrprogramcount[2]['NoOfCandidates']- ($lintpass+$lintfail);//$lintabsent;
				 $larrpartcresult = $this->lobjstastics->fngetprogramresult($lintmon,$lintyear,$lintpartcid);
				 $lintpartccount = count($larrpartcresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent = 0;
				 for($linti=0;$linti<$lintpartccount;$linti++){
				 	if($larrpartcresult[$linti]['pass']==1)
				    {
				 		$lintpass++;
				 	}elseif ($larrpartcresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrprogramcount[]['csat'] = $lintpass+$lintfail;
				 $larrprogramcount[]['cpass'] = $lintpass;
				 $larrprogramcount[]['cfail']= $lintfail;
				 $larrprogramcount[]['cabsent']= $larrprogramcount[3]['NoOfCandidates']- ($lintpass+$lintfail);//$lintabsent;
				 $larrprogramcount[]['sattotal']=  $larrprogramcount[5]['absat']+$larrprogramcount[9]['acsat']+$larrprogramcount[13]['bsat']+$larrprogramcount[17]['csat'];
				 $larrprogramcount[]['passtotal']=  $larrprogramcount[6]['abpass']+$larrprogramcount[10]['acpass']+$larrprogramcount[14]['bpass']+$larrprogramcount[18]['cpass'];
				 $larrprogramcount[]['failtotal']=  $larrprogramcount[7]['abfail']+$larrprogramcount[11]['acfail']+$larrprogramcount[15]['bfail']+$larrprogramcount[19]['cfail'];
				 $larrprogramcount[]['absenttotal']=  $larrprogramcount[8]['ababsent']+$larrprogramcount[12]['acabsent']+$larrprogramcount[16]['babsent']+$larrprogramcount[20]['cabsent'];
				 $this->view->larrracescount   =  $larrprogramcount;
			     $this->view->paginator = $larrprogramcount;
			     //echo "<pre>";print_r($larrprogramcount);die();
				 //$this->view->lobjform->populate($larrformData);
			//}
 	      //}
	}
			public function fnexportexcelAction()
				{    
					 $this->_helper->layout->disableLayout();
					 $this->_helper->viewRenderer->setNoRender();
					 $larrformData = $this->_request->getPost();
					 unset ( $larrformData ['ExportToExcel'] );
					 //echo "<pre>";print_r($larrformData);die();
					 $lday= date("d-m-Y");
					 $ltime = date('h:i:s',time());
					 $host = $_SERVER['SERVER_NAME'];
					 $imgp = "http://".$host."/tbenew/images/reportheader.jpg";
					 $filename = 'Examcategory_Statistics_Report_'.$larrformData['selmonth'];
					 $ReportName = $this->view->translate( "Examcategory").' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
					 $tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 3><b>Date </b></td><td align ='left' colspan = 3><b>$lday</b></td><td align ='left' colspan = 3><b> Time</b></td><td align ='left' colspan = 3><b>$ltime</b></td></tr></table>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 12 align=center><b>{$ReportName}</b></td></tr></table><br>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 10><b>".$larrformData['selmonth']."</b></td></tr>";
					 $tabledata.="<tr> <td align ='left' colspan = 2><b>Examcategory/Type</b></td><td align ='center' colspan = 2><b>Part A and B </b></td><td align ='center' colspan = 2><b>Part A and C</b></td><td align ='center' colspan = 2><b>Part B</b></td><td align ='center' colspan = 2><b>Part C</b></td><td align ='center' colspan = 2><b>Total</b></td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td><td align ='center' colspan = 2>".$larrformData['abcand']."</td><td align ='center' colspan = 2>".$larrformData['accand']."</td><td align ='center' colspan = 2>".$larrformData['bcand']."</td><td align ='center' colspan = 2>".$larrformData['ccand']."</td><td align ='center' colspan = 2>".$larrformData['regtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td><td align ='center' colspan = 2>".$larrformData['absat']."</td><td align ='center' colspan = 2>".$larrformData['acsat']."</td><td align ='center' colspan = 2>".$larrformData['bsat']."</td><td align ='center' colspan = 2>".$larrformData['csat']."</td><td align ='center' colspan = 2>".$larrformData['sattotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td><td align ='center' colspan = 2>".$larrformData['abpass']."</td><td align ='center' colspan = 2>".$larrformData['acpass']."</td><td align ='center' colspan = 2>".$larrformData['bpass']."</td><td align ='center' colspan = 2>".$larrformData['cpass']."</td><td align ='center' colspan = 2>".$larrformData['passtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td><td align ='center' colspan = 2>".$larrformData['abfail']."</td><td align ='center' colspan = 2>".$larrformData['acfail']."</td><td align ='center' colspan = 2>".$larrformData['bfail']."</td><td align ='center' colspan = 2>".$larrformData['cfail']."</td><td align ='center' colspan = 2>".$larrformData['failtotal']."</td></tr>";				
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td><td align ='center' colspan = 2>".$larrformData['ababsent']."</td><td align ='center' colspan = 2>".$larrformData['acabsent']."</td><td align ='center' colspan = 2>".$larrformData['babsent']."</td><td align ='center' colspan = 2>".$larrformData['cabsent']."</td><td align ='center' colspan = 2>".$larrformData['absenttotal']."</td></tr>";				
					 $ourFileName = realpath('.')."/data";
					 $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
					 ini_set('max_execution_time', 3600);
					 fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
					 fclose($ourFileHandle);
					 header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
					 header("Content-Disposition: attachment; filename=$filename.xls");
					 header("Pragma: no-cache");
					 header("Expires: 0");
					 readfile($ourFileName);
					 unlink($ourFileName);
				}	
}	
 