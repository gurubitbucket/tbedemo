<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_StudentquestionsController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator		
		$this->lobjinvoiceForm = new  Reports_Form_Studentquestions();//invoice form
		$this->lobjInvoicereportModel = new Reports_Model_DbTable_Studentquestions();//invoice model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjinvoiceForm;	
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))	{
			$larrformData = $this->_request->getPost ();		
			if ($this->lobjform->isValid($larrformData)){
				unset ( $larrformData ['Search'] );
				
				$larrresult = $this->lobjInvoicereportModel->fnSearchstudent($larrformData['field3']); //searching the values for the Companies
				$this->view->lobjform->field3->setvalue($larrformData['field3']);
				if(count($larrresult)>0){
					$this->view->studentquestions=$larrresult;
					$larrrtos = $this->lobjInvoicereportModel->fnfetchtosofstudent($larrresult['0']['idquestions']);
					$this->view->tosstructure=$larrrtos;
				}else{
					echo '<script language="javascript">alert("Candidate not taken the Exam or invalid Register id Entered")</script>';
				}
				
				
				
			}
		}		
	}
				
}
