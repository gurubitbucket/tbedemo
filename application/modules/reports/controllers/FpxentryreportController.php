<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');

class Reports_FpxentryreportController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator		
		//$this->lobjCommon = new App_Model_Common();
		$this->lobjReportsForm = new  Reports_Form_Report();
		$this->lobjPaypalentryreportModel = new Reports_Model_DbTable_Paypalentryreport();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjReportsForm;		
		$this->lobjReportsForm->Venues->addMultiOption('','Select');
		$larrcenters = array(array(key=>1,value=>"Indivdual Registration"),array(key=>2,value=>"Comapny Registration"),array(key=>3,value=>"Takaful Registration"));
		$this->lobjReportsForm->Venues->addmultioptions($larrcenters);
		
	    $this->lobjReportsForm->Date3->setAttrib('onChange', "dijit.byId('Date4').constraints.min = arguments[0];");
	    //$this->lobjReportsForm->Date4->setAttrib('onChange', "dijit.byId('Date3').constraints.min = arguments[0];");
		$larrresult = array();
		

		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))	{
			$larrformData = $this->_request->getPost ();		
			if ($this->lobjform->isValid($larrformData)){				
				$lvarfromdate = $this->view->fromdate=$larrformData['Date3'];
				$lvartodate = $this->view->todate=$larrformData['Date4'];				
				$lvartype = $this->view->venue=$larrformData['Venues'];
				unset ( $larrformData ['Search'] );
				$result1 = array();
				$result2 = array();
				$result3 = array();
				if($lvartype == "" || $lvartype == 1){
					$result1 = $this->lobjPaypalentryreportModel->fngetstudentfpxentrydetails($larrformData); //searching the values for the student
				}
				if($lvartype == "" || $lvartype == 2){
					$result2 = $this->lobjPaypalentryreportModel->fngetcompanypfxentrydetails($larrformData); //searching the values for the company	
				}		
				if($lvartype == "" || $lvartype == 3){
					$result3 = $this->lobjPaypalentryreportModel->fngettakafulfpxentrydetails($larrformData); //searching the values for the takaful
				}
				$this->view->result1 = $result1;
				$this->view->result2 = $result2;
				$this->view->result3 = $result3;
				$this->view->lobjform->populate($larrformData);
			}
		}
			
			
	}
 public function getdetailsAction()	{    
	  $this->_helper->layout->disableLayout();
	  $this->_helper->viewRenderer->setNoRender();
	  $lintIdapp = $this->_getParam('idapp');
	  $lintfrom = $this->_getParam('from');
	  $tabledata = '';	
	  if($lintfrom ==0){	
		  $result = $this->lobjPaypalentryreportModel->fngetfpxstuddetails($lintIdapp);
		  
		  foreach($result as $lobjCountry){ 
		  		   $tabledata.="<br><fieldset><legend align = 'left'> Candidate Details </legend>
			         		   <table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>IdApplication</b></th><th><b>ICNO</b></th><th><b>DateOfBirth</b></th><th><b>Email</b></th></tr><tr>";
			      $tabledata.="<td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['IDApplication']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".date('d-m-Y H:i:s',strtotime($lobjCountry['DateOfBirth']))."</td><td align = 'left'>".$lobjCountry['EmailAddress']."</td></tr>";
			      $tabledata.="</table></fieldset><br>";
			     $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
				         		   <table class='table' border=1 align='center' width=100%>
				         		   <tr><th><b>Order No</b></th><th><b>Tarnsaction Id</b></th><th><b>Bank</b></th><th><b>Branch</b></th><th><b>Amount</b></th><th><b>Payer Mail Id</b></th><th><b>Payment Date</b></th>
				         		   <th><b>Approved Date</b></th></tr><tr>";
				      $tabledata.="<td align = 'left'>".$lobjCountry['orderNumber']."</td>
				      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
				      		<td align = 'left'>".$lobjCountry['bankCode']."</td>
				      		<td align = 'left'>".$lobjCountry['bankBranch']."</td>
				      		<td align = 'left'>".$lobjCountry['grossAmount']."</td>
				      		
				      		<td align = 'left'>".$lobjCountry['payerId']."</td>
				      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
				      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['ApprovedDate']))."</td></tr>";
				      $tabledata.="</table></fieldset><br>";
				      $this->lobjExamreportModel = new Reports_Model_DbTable_Examreport();
				       $results = $this->lobjExamreportModel->fngetstuddetails($lobjCountry['IDApplication']);
				       $tabledata.= '<br><fieldset><legend align = "left"> Exam Details </legend>
					                    <table class="table" border=1 align = "center" width=100%><tr><th><b>Program</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th></tr><tr>';
						  $tabledata.= '<td align = "left">'.$results[0]['ProgramName'].'</td><td align = "left">'.$results[0]['ExamVenue'].'</td><td align = "left">'.$results[0]['ExamDate'].'</td><td align = "left">'.$results[0]['session'].'</td></tr>';
					      $tabledata.="</table></fieldset>";
			   
			      		
			}
	  }
 	else if($lintfrom ==1){	
		  $result =$this->lobjPaypalentryreportModel->fngetfpxcompanydetails($lintIdapp);	  
 		  foreach($result as $lobjCountry){ 
			  			 $tabledata.= '<br><fieldset><legend align = "left"> Company Details </legend>			
				                    <table class="table" border=1 align = "center" width=100%>
				                    <tr>
				                    	<th><b>Company Name</b></th><th><b>Short Name</b></th><th><b>Email</b></th><th>Registration Pin</th><th>No Of Candidates</th>
				                    </tr><tr>';
					  $tabledata.= '<td align = "left">'.$lobjCountry['CompanyName'].'</td><td align = "left">'.$lobjCountry['ShortName'].'</td>
					  <td align = "left">'.$lobjCountry['Email'].'</td><td>'.$lobjCountry['registrationPin'].'</td><td>'.$lobjCountry['totalNoofCandidates'].'</td></tr>';
				      $tabledata.="</table></fieldset><br><br>";
				      $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
				         		   <table class='table' border=1 align='center' width=100%>
				         		   <tr><th><b>Order No</b></th><th><b>Tarnsaction Id</b></th><th><b>Bank</b></th><th><b>Branch</b></th><th><b>Amount</b></th><th><b>Payer Mail Id</b></th><th><b>Payment Date</b></th>
				         		   <th><b>Approved Date</b></th></tr><tr>";
				      $tabledata.="<td align = 'left'>".$lobjCountry['orderNumber']."</td>
				      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
				      		<td align = 'left'>".$lobjCountry['bankCode']."</td>
				      		<td align = 'left'>".$lobjCountry['bankBranch']."</td>
				      		<td align = 'left'>".$lobjCountry['grossAmount']."</td>
				      		
				      		<td align = 'left'>".$lobjCountry['payerId']."</td>
				      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
				      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['ApprovedDate']))."</td></tr>";
				      $tabledata.="</table></fieldset>";
				      		
				}
	  }
	 else if($lintfrom ==2){	
			  $result = $this->lobjPaypalentryreportModel->fngetfpxtakafuldetails($lintIdapp);	  
			  //echo "<pre>";
			  //print_r($result);exit;
			  foreach($result as $lobjCountry){ 
			  			 $tabledata.= '<br><fieldset><legend align = "left"> Takaful Details </legend>			
				                    <table class="table" border=1 align = "center" width=100%>
				                    <tr>
				                    	<th><b>Takaful Name</b></th><th><b>Short Name</b></th><th><b>Email</b></th><th>Registration Pin</th><th>No Of Candidates</th>
				                    </tr><tr>';
					  $tabledata.= '<td align = "left">'.$lobjCountry['TakafulName'].'</td><td align = "left">'.$lobjCountry['TakafulShortName'].'</td>
					  <td align = "left">'.$lobjCountry['email'].'</td><td>'.$lobjCountry['registrationPin'].'</td><td>'.$lobjCountry['totalNoofCandidates'].'</td></tr>';
				      $tabledata.="</table></fieldset><br><br>";
				      $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
				         		   <table class='table' border=1 align='center' width=100%>
				         		   <tr><th><b>Order No</b></th><th><b>Tarnsaction Id</b></th><th><b>Bank</b></th><th><b>Branch</b></th><th><b>Amount</b></th><th><b>Payer Mail Id</b></th><th><b>Payment Date</b></th>
				         		   <th><b>Approved Date</b></th></tr><tr>";
				      $tabledata.="<td align = 'left'>".$lobjCountry['orderNumber']."</td>
				      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
				      		<td align = 'left'>".$lobjCountry['bankCode']."</td>
				      		<td align = 'left'>".$lobjCountry['bankBranch']."</td>
				      		<td align = 'left'>".$lobjCountry['grossAmount']."</td>
				      		
				      		<td align = 'left'>".$lobjCountry['payerId']."</td>
				      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
				      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['ApprovedDate']))."</td></tr>";
				      $tabledata.="</table></fieldset>";
				      		
				}
		  }
		$tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";	
		   echo  $tabledata; 
	}

	public function pdfexportAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformdata = array();
		$fdate = $larrformData['Date3'] = $this->_getParam('fromdate');
		$tdate = $larrformData['Date4'] = $this->_getParam('todate');
		$frmdate =date('d-m-Y',strtotime($fdate));
		$todate =date('d-m-Y',strtotime($tdate));
		//$larrformdata['Coursename'] = $this->_getParam('cname');
		$lvartype = $larrformData['Venues'] = $this->_getParam('venue');
		$lstrreportytpe = $this->_getParam('reporttype');	
					
				$result1 = array();
				$result2 = array();
				$result3 = array();
				if($lvartype == "" || $lvartype == 1){
					$result1 = $this->lobjPaypalentryreportModel->fngetstudentfpxentrydetails($larrformData); //searching the values for the student
				}
				if($lvartype == "" || $lvartype == 2){
					$result2 = $this->lobjPaypalentryreportModel->fngetcompanypfxentrydetails($larrformData); //searching the values for the company	
				}		
				if($lvartype == "" || $lvartype == 3){
					$result3 = $this->lobjPaypalentryreportModel->fngettakafulfpxentrydetails($larrformData); //searching the values for the takaful
				}				
		
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'FPX_Reciept_Report_'.$frmdate.'_'.$todate;
		$ReportName = $this->view->translate( "FPX" ).' '.$this->view->translate( "Reciept" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
		}
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 5><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 8><b> {$ReportName}</b></td>	
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b>Payment From</b></th>
							<th><b>Name</b></th>
							<th><b>Order No</b></th>
							<th><b>Registration Pin</b></th>
							<th><b>Amount</b></th>
							<th><b>Transaction Id</b></th>
							<th><b>Date Of Payment</b></th>
							<th><b>Approved Date</b></th>
						</tr>';     
	 	$sum[1] = 0; 
		if (count($result1)){
			 $cnt = 0; 
      		  foreach ($result1 as $lobjStudent ){
      		  	  //$tabledata.= ' <tr>';
				  //if($cnt == 0)  $tabledata.= '<td rowspan="'.count($result1).'"><b>Indivdual Registration</b></td> ';
				   $tabledata.= ' <tr>
				   		   <td><b>'; 
				   if($cnt == 0) $tabledata.= 'Indivdual Registration';
      		      $tabledata.= '</b></td>
					       <td>'.$lobjStudent['FName'].'</td> 
						   <td>'.$lobjStudent['orderNumber'].'</td> 
						   <td>'.$lobjStudent['Regid'].'</td> 
						   <td>'.$lobjStudent['grossAmount'].'</td> 
						   <td>'.$lobjStudent['transactionId'].'</td> 		  
						   <td>'.date('d-m-Y',strtotime($lobjStudent['PaymentDate'])).'</td> 
						   <td>'.date('d-m-Y',strtotime($lobjStudent['ApprovedDate'])).'</td> 		   
				        </tr> ';
				   			$sum[1]= $sum[1]+$lobjStudent['grossAmount'];   
	    	$cnt++; 
      		  }
	      $tabledata.= '<tr>	      	
	     	<td colspan="2" align="center"><b>Total Count</b></td>
	     	<td><b>'.$cnt.'</b></td>
	     	<td></td>
	     	<td><b>'.$sum[1].'</b></td>
	     	<td></td>
	     	<td></td>
	     	<td></td>
	     </tr>';
		 }		
		$sum[2] = 0; 
		if (count($result2)){
			 $cnt = 0; 
      		  foreach ($result2 as $lobjStudent ){
      		    // if($cnt == 0)  $tabledata.= '<td rowspan="'.count($result2).'"><b>Comapny Registration</b></td> ';
      		  	// $tabledata.= ' <tr>';
				   $tabledata.= '<tr>
				   		   <td><b>'; 
				   if($cnt == 0) $tabledata.= 'Comapny Registration'; 
				   $tabledata.= '</b></td>			  
				   		   <td>'.$lobjStudent['CompanyName'].'</td> 
						   <td>'.$lobjStudent['orderNumber'].'</td> 
						   <td>'.$lobjStudent['registrationPin'].'</td> 
						   <td>'.$lobjStudent['grossAmount'].'</td> 
						   <td>'.$lobjStudent['transactionId'].'</td> 		  
						   <td>'.date('d-m-Y',strtotime($lobjStudent['PaymentDate'])).'</td> 
						   <td>'.date('d-m-Y',strtotime($lobjStudent['ApprovedDate'])).'</td> 		   
				        </tr> ';
				   			$sum[2]= $sum[2]+$lobjStudent['grossAmount'];   
	    			$cnt++; 
      		  }
	      $tabledata.= '<tr>	      	
	     	<td colspan="2" align="center"><b>Total Count</b></td>
	     	<td ><b>'.$cnt.'</b></td>
	     	<td></td>
	     	<td ><b>'.$sum[2].'</b></td>
	     	<td></td>
	     	<td></td>
	     	<td></td>
	     </tr>';
		 }
		
		$sum[3] = 0; 
		if (count($result3)){
			 $cnt = 0; 
      		  foreach ($result3 as $lobjStudent ){
      		  	//if($cnt == 0)  $tabledata.= '<tr><td rowspan="'.count($result3).'"><b>Takaful Registration</b></td> ';
				   $tabledata.= ' <tr>
				   		   <td><b>'; 
				   if($cnt == 0) $tabledata.= 'Takaful Registration'; 
				   $tabledata.= '</b></td> 
				
						   <td>'.$lobjStudent['TakafulName'].'</td> 
						   <td>'.$lobjStudent['orderNumber'].'</td> 
						   <td>'.$lobjStudent['registrationPin'].'</td> 
						   <td>'.$lobjStudent['grossAmount'].'</td> 
						   <td>'.$lobjStudent['transactionId'].'</td> 		  
						   <td>'.date('d-m-Y',strtotime($lobjStudent['PaymentDate'])).'</td> 
						   <td>'.date('d-m-Y',strtotime($lobjStudent['ApprovedDate'])).'</td> 		   
				        </tr> ';
				   			$sum[3]= $sum[3]+$lobjStudent['grossAmount'];   
	    	$cnt++; 
      		  }
	      $tabledata.= '<tr>
	      	
	     	<td colspan="2" align="center"><b>Total Count</b></td>
	     	<td><b>'.$cnt.'</b></td>
	     	<td></td>
	     	<td><b>'.$sum[3].'</b></td>
	     	<td></td>
	     	<td></td>
	     	<td></td>
	     </tr>
	      ';
		 }
		$totalsum = $sum[1]+$sum[2]+$sum[3];
		 $tabledata.= '<tr>
		      	
		     	<td colspan="4" align="center"><b>Grand Total</b></td>
		     	
		     	<td ><b>'.$totalsum.'</b></td>
		     	<td></td>
		     	<td></td>
		     	<td></td>
		     </tr>
		     </table>';	
		
		//echo $tabledata;exit;
		if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}
				
	}
}
