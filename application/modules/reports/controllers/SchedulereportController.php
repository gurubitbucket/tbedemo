<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
class Reports_SchedulereportController extends Base_Base
{
	public function init()
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
		$this->registry = Zend_Registry::getInstance(); //get registry instance
		$this->locale = $this->registry->get('Zend_Locale'); //get locale
		$this->lobjSchedulereport = new Reports_Model_DbTable_Schedulereportmodel(); //model object
		$this->lobjSchedulereportform = new  Reports_Form_Schedulereport(); //form object
	}
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjSchedulereportform;
		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->attendancereportpaginatorresult);
		$lintpagecount = 1000;
		$lintpage = $this->_getParam('page',1);//Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->attendancereportpaginatorresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				unset ( $larrformData ['Search']);
				$this->view->FromDate = $FromDate = $larrformData['FromDate'];
				$this->view->ToDate = $ToDate = $larrformData['ToDate'];
				$larrresultcomp = $this->lobjSchedulereport->fngetschdeuledates($FromDate,$ToDate);
				$larrresultvenues = $this->lobjSchedulereport->fngetvenues();
				$this->view->dates = $larrresultcomp;
				$this->view->venues = $larrresultvenues;
				$this->view->lobjform->populate($larrformData);
			}
		}
	}
    public function fnexportexcelAction()
    {	
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$ldtfromdate = $larrformData['FromDate'];
		$ldttodate =$larrformData['ToDate'];
		$fdate = date('d-m-Y',strtotime($ldtfromdate));
		$tdate = date('d-m-Y',strtotime($ldttodate));
        $larrresultcomp = $this->lobjSchedulereport->fngetschdeuledates($ldtfromdate,$ldttodate);
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Schedulereport'.$ldtfromdate.'_'.$ldttodate;
		$ReportName = $this->view->translate( "Schedule" ).' '.$this->view->translate( "Report" );
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 4><b>Date </b></td><td align=left colspan = 4><b>$day</b></td><td  align=left colspan = 2><b> Time</b></td><td align=left colspan = 4><b>$time</b></td></tr>";
        $tabledata.= "<tr><td align=left colspan = 4><b>From Date </b></td><td align=left colspan = 4><b>".$fdate."</b></td><td  align=left colspan = 2><b>To Date</b></td><td align=left colspan = 2><b>".$tdate."</b></td></tr></table>";
	    $tabledata.= "<table><tr><td><b>VENUES/DATE</b></td>";
	    $cols = count($larrresultcomp)+1; 
	    for($lvars=0;$lvars<count($larrresultcomp);$lvars++){
	    	$tabledata.="<td colspan = '1'><b>".$larrresultcomp[$lvars]['Date']."</b><br>";
	    	$tabledata.="<b>".$larrresultcomp[$lvars]['dayname']."</b><td>";
	    }
	    $tabledata.="</tr>";
	    $larrresultvenues = $this->lobjSchedulereport->fngetvenues();
	    for($lvar=0;$lvar<count($larrresultvenues);$lvar++)
	      {
	    	$tabledata.= "<tr><td>".$larrresultvenues[$lvar]['centername']."</td>";
	    	for($j=0;$j<count($larrresultcomp);$j++)
	    	   {
	    		   $this->lobjSchedulereport = new Reports_Model_DbTable_Schedulereportmodel();
        	       $idcenter = $larrresultvenues[$lvar]['idcenter'];
        	       $date = $larrresultcomp[$j]['date'];
        	       $larresult =$this->lobjSchedulereport->fngetseats($idcenter,$date); 
        	       $cnt = count($larresult);
        	       if($cnt==2)
        	       { 
        	       	$tabledata.= "<td colspan = '2'>".$larresult[0]['ampmstart']."[".$larresult[0]['Totalcapacity']."]"."<br>"
        	       	             .$larresult[1]['ampmstart']."[".$larresult[1]['Totalcapacity']."]</td>";
        	       }
        	      else if($cnt==1)
        	       {  
        	       	$tabledata.= "<td colspan = '2'>".$larresult[0]['ampmstart']."[".$larresult[0]['Totalcapacity']."]</td>"; 
        	       }
        	       else 
        	       {
        	       	$tabledata.= "<td colspan = '2'>No Exam</td>";
        	       }
        	   }     
                $tabledata.= "</tr><tr><td colspan='$cols'><hr></td></tr>";
	      }
	        $tabledata.="</table>";
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
    }
}