<?php
ini_set('display_errors','On');
ini_set('memory_limit','-1');
class Reports_ExamquestionsController extends Base_Base 
{	
    public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjExammarksModel = new Reports_Model_DbTable_Examquestions();
		$this->lobjExammarksform = new  Reports_Form_Examquestions();
	}
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjExammarksform;
		$total=$this->lobjExammarksModel->fngettotalquestions();
		
		for($linti=1;$linti<=$total['count'];$linti++){
			$larrrand[$linti] = $linti;
		}
		$questiongroup=$this->lobjExammarksModel->fngetquestiongroup();
		$this->lobjExammarksform->Questiongroup->addMultiOptions($questiongroup);
		$this->lobjExammarksform->Marksfrom->addMultiOptions($larrrand);
		$this->lobjExammarksform->Marksto->addMultiOptions($larrrand);
		
		if($this->_request->isPost() && $this->_request->getPost('Search')){
			$formdata=$this->_request->getPost();
			Unset($formdata['Search']);
			$this->lobjExammarksform->Questiongroup->setvalue($formdata['Questiongroup']);
			$this->lobjExammarksform->Questionstatus->setvalue($formdata['Questionstatus']);
			$this->lobjExammarksform->Marksfrom->setvalue($formdata['Marksfrom']);
			$this->lobjExammarksform->Marksto->setvalue($formdata['Marksto']);
			$this->view->displayquestions=$displayquestions = $this->lobjExammarksModel->fndisplayquestionsforabovecode($formdata['Questiongroup'],$formdata['Questionstatus'],$formdata['Marksfrom'],$formdata['Marksto']);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost('submit')){  
				$larrformData = $this->_request->getPost();
			    Unset($larrformData['submit']);
			   	$questiongroup=$larrformData['Questiongroup'];
			    $questionstatus=$larrformData['Questionstatus'];
			    $fromqtn = $larrformData['Marksfrom'];
			    $toqtn = $larrformData['Marksto'];
			    $kk=$this->_redirect(  $this->baseUrl  ."/reports/examquestions/fnexport/fromqtn/$fromqtn/toqtn/$toqtn/questiongroup/$questiongroup/questionstatus/$questionstatus");
			}
    }
	
	public function fnexportAction()
    {   
     
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$fromqtn = $this->_getParam('fromqtn');
		$toqtn = $this->_getParam('toqtn');
		$questiongroup = $this->_getParam('questiongroup');
		$questionstatus = $this->_getParam('questionstatus');
		$displayquestions = $this->lobjExammarksModel->fndisplayquestionsforabovecode($questiongroup,$questionstatus,$fromqtn,$toqtn);
		if(!empty($questiongroup)){
			$qsngroupnamearray=$this->lobjExammarksModel->fngetgroupname($questiongroup);
			$qsngroupname=$qsngroupnamearray['groupname'];
		}else{
			$qsngroupname="All";	
		}
		if(isset($questionstatus)){
			if($questionstatus==1){
					$qsnstatus="Active";
			}else if($questionstatus==0){
					$qsnstatus="InActive";
			}
		}else{
			$qsnstatus="Active && InActive";
		}
		if(isset($fromqtn) && !empty($fromqtn)){
			$fromqsn=$fromqtn;	
		}else{
			$fromqsn="1(Starting)";
		}
    	if(isset($toqtn) && !empty($toqtn)){
			$toqsn=$toqtn;	
		}else{
			$total=$this->lobjExammarksModel->fngettotalquestions();
			$toqsn=$total['count']."(Ending)";
		}
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Question" ).' '.$this->view->translate( "Paper" );
		ini_set('max_execution_time',3600);
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		//echo $imgp;die();
		$tabledata = '<img width=100% src= "'.$imgp.'" />';
		
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table width=100% border=0>
						<tr><td><b>Questions Group &nbsp;:</b></td>
							<td><b>'.$qsngroupname.'</b></td>
							<td><b>Questions Status &nbsp;:</b></td>
							<td><b>'.$qsnstatus.'</b></td>
						</tr>
						<tr><td><b>Questions From &nbsp;:</b></td>
							<td><b>'.$fromqsn.'</b></td>
							<td><b>Questions To &nbsp;:</b></td>
							<td><b>'.$toqsn.'</b></td>
						</tr>
					</table>';
		
		$tabledata.='<hr></hr><br>';
		$tabledata.='<h3 style="text-align:right;">Total Questions='.count($displayquestions).'</h3>';
		if(count($displayquestions)!=0){
		  for($i=1,$s=0;$i<=count($displayquestions),$s<count($displayquestions);$s++,$i++)
		  { 
	  				$tabledata.='<table ><tr>
     				<td valign="top" >'.'Q'.$i.'.'.'&nbsp;'.$displayquestions[$s]['idquestions'].')'.' </td><td align="left">'.trim($displayquestions[$s]['Question']).'<br></td>
	  				</tr></table><br>';
		            	$larranswers = $this->lobjExammarksModel->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
		            	for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++)
		            	{
		            		
							$tabledata.='<table>
											<tr>
												<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$i.'.'.$larranswers[$a]['idanswers'].')'.'</td>
												<td>'.$larranswers[$a]['answers'].'<br></td>';
							if($larranswers[$a]['CorrectAnswer']==1)
							{
								$tabledata.='<td style="background-color:green;">Correct Answer</td>';
							}
							$tabledata.='</tr>  
										 </table><br>';
		       			}
		       
		}
		}else{
			$tabledata.='<table >
				<tr>
     				<td><p align="center" style="font-weight:bold;">Results not Found for the Your Search Criteria.</p></td>
	  			</tr>
	  		</table><br>';
		}
	
		$mpdf->WriteHTML($tabledata);   
		$mpdf->Output('Questions_Report.pdf','D');
		return 1;
    }
}