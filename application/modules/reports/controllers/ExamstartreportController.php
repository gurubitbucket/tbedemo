<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_ExamstartreportController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjallstatistics = new Reports_Model_DbTable_Examstartreportmodel;
   	    $this->lobjStasticsForm = new  Reports_Form_Statisticsnew();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
//function to set and display the result
	public function indexAction() 
	{    
		
			Zend_Session:: namespaceUnset('startex');
		$this->view->lobjform = $this->lobjStasticsForm;
		///new
		$this->view->lobjform->Year->setAttrib('onchange','fngetmonths()');
		$year = 2012;
		$lintcuryear = date("Y"); 
		for($linti=$year;$linti<=$lintcuryear;$linti++){
			$larryears[$linti] = $linti;
		} 
		$this->lobjStasticsForm->Year->addmultioptions($larryears);
		$ldtdate = getdate();
		$this->view->search = 1;
		$this->view->searchmon = $lintmonthnumber = $ldtdate['mon']-1;
		/*if($lintmonthnumber!=1){
		$lintmonthnumber = $ldtdate['mon']-1;
		}else{
		$lintmonthnumber = $ldtdate['mon'];
		}*/
		$lintyearnumber = $ldtdate['year'];
		$this->lobjStasticsForm->Year->setValue($lintyearnumber);
		//$this->lobjStasticsForm->Month->setValue($lintmonthnumber); 
		///
		
		if ($this->_request->isPost () && $this->_request->getPost('Search')) 
		    {   $this->view->flag=1;
		    	
		    	Zend_Session:: namespaceUnset('startex');
		    	
		    	$larrformData = $this->_request->getPost();
		    	
		    	
		    	$this->view->month=$larrformData['Month'];
		    	$this->view->year=$larrformData['Year'];
		    	
		    	//$lintcuryear="Y";
		    	echo "<pre/>";
		    	//print_r($larrformData);
		    	//$lintyr=$larrformData['Year'];
$lintyr=$larrformData['Year'];
		    if($lintyr == $lintcuryear){
			    $larrresult = $this->lobjallstatistics->fngetmonthsajaxss(1);
			}elseif($lintyr == '2012'){
				$larrresult = $this->lobjallstatistics->fngetmonthsajaxss(3);
			}
	        else{
				$larrresult = $this->lobjallstatistics->fngetmonthsajaxss(2);
			}
			$year = 2012;
		$lintcuryear = date("Y"); 
		for($linti=$year;$linti<=$lintcuryear;$linti++){
			$larryears[$linti] = $linti;
		} 
		
			//print_r($larryears);
				//print_r($larrresult);die();
			
		$this->lobjStasticsForm->Year->addmultioptions($larryears);
			$this->lobjStasticsForm->Year->setValue($larrformData['Year']);
			
				$this->lobjStasticsForm->Month->addMultiOptions($larrresult);
				$this->lobjStasticsForm->Month->setValue($larrformData['Month']);
		    	
		    	$larrresultbyvenue=$this->lobjallstatistics->fngetvenuestartdetails($larrformData);
		    	
		    	
		    	//$larrrestudentreport=$this->lobjallstatistics->fnlocaldatesession($larrformData);
		    	//echo "<pre/>";
		    	//print_r($larrrestudentreport);
		    	//print_r($larrresultbyvenue);die();
				$this->view->paginator=$larrresultbyvenue;
				
				
				 $namespace = new Zend_Session_Namespace('startex');
    				$namespace->data = $larrresultbyvenue;
}
}

public function fnexportexcelAction()
{
	$this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$larrformData = $this->_request->getPost();
	$lintmon = $larrformData['lmonth'];
	$lintyear = $larrformData['lyear'];
	$linttype = $larrformData['linttype'];
	unset ( $larrformData['ExportToExcel']);
	$lday= date("d-m-Y");
	$ltime = date('h:i:s',time());
	
	$monthname=$this->lobjallstatistics->fngetmonthname($lintmon);
	$mname=$monthname['MonthName'];
	//echo $mname;die();
	 $namespace = new Zend_Session_Namespace('startex');
    $data = $namespace->data; 
	
    $larrresult=$data;
    
    $day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Examstart_Report_'.$lday.'_'.$ltime;
		$ReportName = $this->view->translate( "Examstart" ).' '.$this->view->translate( "Report" );
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left ><b> Year :</b></td><td align=left ><b>$lintyear</b></td><td  align=left ><b> Month :</b></td><td align=left colspan = 3><b>$mname</b></td></tr></table>";
        $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 6><b> {$ReportName}</b></td></tr></table><br>";
	    $tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Exam Venue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th><th><b>Schedule Time</b></th><th><b>Start Time</b></th><th><b>In-Time</b></th></tr>";
	  
$i=0;
  foreach($larrresult as $lobjCountry)
		{
			$tabledata.="<tr><td align = 'left'>".$lobjCountry['centername']."</td><td align = 'left'>".''.$lobjCountry['ExamDate1']."</td><td align = 'left' >".$lobjCountry['sename']."</td><td align = 'left'>".$lobjCountry['starttime']."</td><td align = 'left'>".$lobjCountry['ExamStartTime']."</td></td>";
			
			
			if($lobjCountry['Startedtime']<=$lobjCountry['totalstarttime'])
			{
			$tabledata.="<td align = 'left'>YES</td></tr>";
$i++;
			}
			else 
			{
					$tabledata.="<td align = 'left'>NO</td></tr>";
			}
		}
//$tabledata.="<tr><td></td><td></td><td></td><td></td><td ><b>Total :</b>".echo count($larrresult);."<b>In-Time :</b>".echo $i;."<b>Late :</b>".echo count($larrresult)-$i;."</td></tr>";
			
		
$tabledata.="<tr><td></td><td></td><td></td><td ><b>Total :</b>";
		$tabledata.=count($larrresult);
		$tabledata.="</td><td>";
		$tabledata.="<b>  In-Time  :</b>";
		$tabledata.=$i;
			$tabledata.="</td><td>";
		$tabledata.="<b>  Late :</b>";
		$tabledata.=count($larrresult)-$i;
		$tabledata.="</td></tr>";
$tabledata.="</table><br>";
    	
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		
    
    
    
    
    
    
    
}

	 public function fngetmonthnamesAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintyr = $this->_getParam('val');
		$lintcuryear = date("Y"); 
			if($lintyr == $lintcuryear){
			    $larrresult = $this->lobjallstatistics->fngetmonthsajax(1);
			}elseif($lintyr == '2012'){
				$larrresult = $this->lobjallstatistics->fngetmonthsajax(3);
			}
	        else{
				$larrresult = $this->lobjallstatistics->fngetmonthsajax(2);
			}
		echo Zend_Json_Encoder::encode($larrresult);
	}

}
