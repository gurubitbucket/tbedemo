<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);

class Reports_TosquestionsController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator		
		$this->lobjinvoiceForm = new  Reports_Form_Tosquestions();//invoice form
		$this->lobjInvoicereportModel = new Reports_Model_DbTable_Tosquestions();//invoice model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjinvoiceForm;	
		$larrresulteasy =  $this->lobjInvoicereportModel->fnGetBatchArraySearch();
  		$this->view->lobjform->TosSet->addMultiOption("","Select");
  		$this->view->lobjform->TosSet->addMultiOptions($larrresulteasy);
  		$larrresult = $this->lobjInvoicereportModel->fnGetExamDetails(0);
  		
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->tospaginatorresult);
			
		$lintpagecount = $this->gintPageCount; 
		$lintpage = $this->_getParam('page',1);
		if(isset($this->gobjsessionsis->tospaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->tospaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjInvoicereportModel->fnGetExamDetails($larrformData['TosSet']); // get bank details
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->tospaginatorresult = $larrresult;
			}
		}
  		
  				
	}
	
	public function tosquestionsformatAction() { 
		$Idtos = $this->_getParam('idtos');	
		$this->view->idtos=$Idtos;
		$larrtosquestions =  $this->lobjInvoicereportModel->fnGetTOSquestionpool($Idtos);
		//echo "<pre/>";print_r($larrtosquestions);
		$this->view->tosquestions=$larrtosquestions;
	}
	
public function tosquestionsformatreportAction() { 
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$Idtos=$this->_getParam('idtos');
		//echo $Idtos;die();
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'TOS_Sample_Questions_Validation_Report'.$day;
		$ReportName = $this->view->translate( "TOS_Sample_Questions_Validation_Report" );
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		$tabledata.= "<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 8><b> {$ReportName}</b></td>	
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date</b></td>
								<td align= 'left' colspan= 2><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 4><b>$time</b></td>
							</tr>";
			
		
		$larrtosquestions1 =  $this->lobjInvoicereportModel->fnGetTOSquestionpool($Idtos);
		$tabledata.= "<tr>	
							<td><b>TOS Set Name </b></td>
							<td align= 'left' colspan= 2><b>"."&nbsp;".$larrtosquestions1[0]['tosname']."</b></td>
							<td><b></b></td>
							<td align = 'left' colspan= 4><b></b></td>
						</tr>";
		$tabledata.="</table><br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b>Si.No</b></th>
							<th><b>Question ID</b></th>
							<th><b>Group</b></th>
							<th><b>Chapter</b></th>
							<th><b>Level</b></th>
							<th><b>Question</b></th>
						</tr>'; 
		
		$i=0;$prevgroupname="";$prevchapter="";
		 foreach($larrtosquestions1 as $tosquestions){
		 	$randomquestions=$this->lobjInvoicereportModel->fnGetRandomQuestions($tosquestions);
		 		foreach($randomquestions as $random){
		 			$tabledata.= '<tr>
			      	<td><b>'.++$i.'</b></td>
			      	<td>'."&nbsp;".$random['idquestions'].'</td>';
		 			if($prevgroupname!=$random['QuestionGroup']){
		 				$tabledata.= '<td >'.$random['QuestionGroup'].'</td>';
		 				$prevgroupname=$random['QuestionGroup'];
		 			}else{
		 				$tabledata.= '<td>'."".'</td>';
		 			}
		 			
		 			if($prevchapter!=$tosquestions['IdSection']){
		 				$tabledata.= '<td><b>'.$tosquestions['IdSection'].'</b></td>';
		 				$prevchapter=$tosquestions['IdSection'];
		 			}else{
		 				$tabledata.= '<td>'."".'</td>';
		 			}
		 			if($tosquestions['IdDiffcultLevel']==1){
		 				$tabledata.= '<td >'."Easy".'</td>';
		 			}else if($tosquestions['IdDiffcultLevel']==2){
		 				$tabledata.= '<td >'."Medium".'</td>';
		 			}else if($tosquestions['IdDiffcultLevel']==3){
		 				$tabledata.= '<td >'."Difficult".'</td>';
		 			} 
			      	$tabledata.= '<td >'.$random['Question'].'</td>';
			     	
			      	$tabledata.= '</tr>';
		 		}
		 		
		 }
		 $tabledata.= '</tr></table>';
		 	include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2013, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output("$filename.pdf",'D');
		
		
	}
				
}
