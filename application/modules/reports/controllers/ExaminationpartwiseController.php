<?php
    error_reporting (E_ALL ^ E_WARNING);
    error_reporting (E_ALL ^ E_NOTICE);
class Reports_ExaminationpartwiseController extends Base_Base {
	
private $_gobjlogger; 
	public function init() 
	{
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
    
	public function indexAction() 
	{
		$this->view->checkEmpty = 0;
		$lobjReportsForm = new  Reports_Form_Report();
		$lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$this->view->lobjform = $lobjReportsForm;
				
		$larrcourses=$lobjExamreportModel->fngetprogramnames();		
		$lobjReportsForm->Coursename->addMultiOption('','Select'); 	
		$lobjReportsForm->Coursename->addmultioptions($larrcourses);
		
		$larrcenters=$lobjExamreportModel->fngetcenternames();
		$lobjReportsForm->Venues->addMultiOption('','Select'); 	
		$lobjReportsForm->Venues->addmultioptions($larrcenters);
		
	    $jsondata = '{
    				"label":"StudentName",
					"identifier":"Serialno",
					"items":""
				  }';
		$this->view->jsondata = $jsondata;
 		
 		if($this->_request->isPost() && $this->_request->getPost('Generate')) 
 		{
 			$larrformData = $this->_request->getPost();
 		    $larr= explode('-',$larrformData['Date']);
 			$larrformData['Year']=$larr[0];
 			$larrformData['Month']=$larr[1];
 			$larrformData['Day']=$larr[2];
 			
 			if($larrformData['Date2'])
 				 {
 					$larr2= explode('-',$larrformData['Date2']);
 					$larrformData['Year1']=$larr2[0];
 					$larrformData['Month1']=$larr2[1];
 					$larrformData['Day1']=$larr2[2];
 					$larrformData['flag1']=0;
 				 }
 			unset($larrformData['Generate']);
 			
 			if ($lobjReportsForm->isValid ( $larrformData )) 
 			{
 			$result = $lobjExamreportModel->fngetnoprogramdetails($larrformData);	
			$lobjExamdetailsmodel = new App_Model_Examdetails();
			/*echo "<pre/>";
 			print_R($result);
 			die();*/
 			for($i=0;$i<count($result);$i++)
 			{
 				$result[$i]['Serialno'] = $i+1;
 				
 				if($result[$i]['Result']==1)
 				{
 					$RegID = $result[$i]['ExamNo'];
	 				$larrpercentageset = $lobjExamdetailsmodel->fnExampercentage($result[$i]['batch']); 
					$noquestions = $larrpercentageset['NosOfQues'];//get the count of questions in the program
					$larrtotalattended = count($lobjExamdetailsmodel->totalattendedquestions($result[$i]['ExamNo']));
					$correct = count($lobjExamdetailsmodel->fnAttended($RegID)); // get the count of correctly answered questions 
					////////////////////////////
					
					//echo $correct;die();
					$idbatchresult=$result[$i]['batch'];
					$larresultidparts= $lobjExamreportModel->fngettheidparts($idbatchresult);
					$idpartsresultsarray=$larresultidparts['partsids'];
			//$larresultidparts['partsids'];
				  	$resarr= explode(',',$idpartsresultsarray);
				  	//echo $resarr[0];
				  		//echo $resarr[1]."hello";die();
				/*  	for($t=0;$t<count($resarr);$t++)
				  	{
				  	 $part= $resarr[$t];
				  		$RegID = $result[$i]['ExamNo'];
				  		$subdetails = $lobjExamreportModel->fnAttended($RegID,$part);
				  		$result[$i]['subdetails'] = $part;
				  		$result[$i]['Correct'] = $subdetails['count(idquestions)'];	
				  	}*/
				/*  if((($resarr['0']=='A')|| ($resarr['1']=='A')) &&(($resarr['0']=='B')|| ($resarr['1']=='B')))
				  {
				  	$resarr['1']='B';
				  	$resarr['0']='A';
				  }
 				if((($resarr['0']=='A')|| ($resarr['1']=='A')) &&(($resarr['0']=='C')||( $resarr['1']=='C')))
				  {
				  	$resarr['1']='C';
				  	$resarr['0']='A';
				  }*/
				  	//count();
				  	
			  	//$larrpartstoknow[]=$result[$i]['IdPart'];
				  	
				  //	$partarray== explode(',',$larrpartstoknow[i]);
				  //	echo "<pre />";
				  //	echo $larrpartstoknow[2];
				  	//print_r($partarray);die();
				  	   if($resarr['0'])
				  	   {
				  	    $part= $resarr['0'];
				  		$RegID = $result[$i]['ExamNo'];
				  		$subdetails = $lobjExamreportModel->fnAttended($RegID,$part);
				  		$result[$i]['subdetails'] = $part;
				  		$result[$i]['Correct'] = $subdetails['count(idquestions)'];
				  	   } 
				  	   if($resarr['1'])
				  	   {
				  	    $part1= $resarr['1'];
				  		$RegID = $result[$i]['ExamNo'];
				  		$subdetails = $lobjExamreportModel->fnAttended($RegID,$part1);
				  		$result[$i]['subdetails1'] = $part1;
				  		$result[$i]['Correct1'] = $subdetails['count(idquestions)'];
				  	   }
					///////////////////////////////////
					
				    $passpercent= (int)(($correct/$noquestions)*100);//calculate the percentage
				    $result[$i]['Marks'] = $correct;
		 			 if($passpercent>=85)
					  {
					  	$result[$i]['Grade'] = 'A';
					  }
					 else if($passpercent >= 70 && $passpercent <= 84)
					  {
					  	$result[$i]['Grade'] = 'B';
					  }
					 else if($passpercent >= 55 && $passpercent <=69)
					  {
					  	$result[$i]['Grade'] = 'C';
					  }
 				}
 				
 				else if($result[$i]['Result']==2)
 				{
 					$RegID = $result[$i]['ExamNo'];
 					//$part= $result[$i]['IdPart'];
	 				$larrpercentageset = $lobjExamdetailsmodel->fnExampercentage($result[$i]['batch']); 
					$noquestions = $larrpercentageset['NosOfQues'];//get the count of questions in the program
					
					$larrtotalattended = count($lobjExamdetailsmodel->totalattendedquestions($result[$i]['ExamNo']));
					$correct = count($lobjExamdetailsmodel->fnAttended($RegID)); // get the count of correctly answered questions 
					//echo $correct;
					//die();
					
					///////////////////////////////////////////////////////
					
					
					
 					$idbatchresult=$result[$i]['batch'];
					$larresultidparts= $lobjExamreportModel->fngettheidparts($idbatchresult);
					$idpartsresultsarray=$larresultidparts['partsids'];
			
				  	$resarr= explode(',',$idpartsresultsarray);
				  	
				 /* 	for($t=0;$t<count($resarr);$t++)
				  	{
				  	 $part= $resarr[$t];
				  		$RegID = $result[$i]['ExamNo'];
				  		$subdetails = $lobjExamreportModel->fnAttended($RegID,$part);
				  		$result[$i]['subdetails'] = $part;
				  		$result[$i]['Correct'] = $subdetails['count(idquestions)'];	
				  	}*/
					
					
					
					
					
					
					//////////////////////////////////////////////////////////////////
					//$larrpartstoknow[]=$result[$i]['IdPart'];
					
					 	//$partarray== explode(',',$larrpartstoknow[i]);
				 // 	echo "<pre />";
				  //	echo $larrpartstoknow[2] ;
				  //	print_r($partarray);die();
				//$resarr= explode(',',$result[$i]['IdPart']);
 					//$resarr= explode(',',$result[$i]['IdPart']);
				  	
				/*  if((($resarr['0']=='A')|| ($resarr['1']=='A')) &&(($resarr['0']=='B')|| ($resarr['1']=='B')))
				  {
				  	$resarr['1']='B';
				  	$resarr['0']='A';
				  }
 				 
				  if((($resarr['0']=='A')|| ($resarr['1']=='A')) &&(($resarr['0']=='C')||( $resarr['1']=='C')))
				  {
				  	$resarr['1']='C';
				  	$resarr['0']='A';
				  }*/
				  	 if($resarr['0'])
				  	  { 
				  	  	$part= $resarr['0'];
				  		$RegID = $result[$i]['ExamNo'];
				  		$subdetails = $lobjExamreportModel->fnAttended($RegID,$part);
				  		$result[$i]['subdetails'] = $part;
				  		$result[$i]['Correct'] = $subdetails['count(idquestions)'];
				  	  }
				  		if($resarr['1'])
				  		{
				  		 $part1= $resarr['1'];
				  		 $RegID = $result[$i]['ExamNo'];
				  		 $subdetails = $lobjExamreportModel->fnAttended($RegID,$part1);
				  		 $result[$i]['subdetails1'] = $part1;
				  		 $result[$i]['Correct1'] = $subdetails['count(idquestions)'];
				  		}
					/*$subdetails = $lobjExamreportModel->fnAttended($RegID,$part);
					 $result[$i]['subdetails'] = $part;
					 $result[$i]['Correct'] = $subdetails['count(idquestions)'];*/
				   
 				    $result[$i]['Grade'] = 'F';
 				    $result[$i]['Marks'] = $result[$i]['Correct']+$result[$i]['Correct1'];
 				}
 				
 				else
 				 {
 				 		//$larrpartstoknow[]=$result[$i]['IdPart'];
 				 		//	echo $larrpartstoknow[2] ;
 					 $result[$i]['Grade'] = 'NA';
 					 $result[$i]['Marks'] = 'NA';
 				}
			  
 			}
 		    	  //  print_r($larrpartstoknow);die();
 			for($i=0;$i<count($result);$i++)
 			{
 				if($result[$i]['Gender']==1)
 				 $result[$i]['Gender']= "MALE";
 				else
 				 $result[$i]['Gender']= "FEMALE";
 			}
 			for($i=0;$i<count($result);$i++)
 			{
 				if($result[$i]['Result']==3)
 				 $result[$i]['Result']= "Applied";
 				if($result[$i]['Result']==1)
 				 $result[$i]['Result']= "Pass";
 				if($result[$i]['Result']==2)
 				 $result[$i]['Result']= "Fail"; 
 			}
 			
 			if($result) $this->view->checkEmpty = 1;	
			$page = $this->_getParam('page',1);
			$this->view->counter = (count($result));
			$this->view->lobjPaginator = $result;
			$jsonresult = Zend_Json_Encoder::encode($result);
    		$jsondata = '{
    				"label":"StudentName",
					"identifier":"Serialno",
					"items":'.$jsonresult.
				  '}';
			$this->view->jsondata = $jsondata;
		}	
	  }
		
	}
	
	public function generatereportAction()
	{
		$lobjReportsForm = new  Reports_Form_Report();
		$this->view->lobjform = $lobjReportsForm;
		//Check Whether the form is submitted
		if($this->_request->getPost())
		{
			$larrformData = $this->_request->getPost();
			$this->view->datacount = $larrformData['datacount'];
			$this->view->datacounttable = $larrformData['datacounttable'];
			$auth = Zend_Auth::getInstance();
    	// Write Logs
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Generated the Report"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);
		}
		else
		{
			$this->_redirect( $this->baseUrl . 'reports/examreport/index');
	    }
		
	 }
	
	 
public function pdfexportAction()
	{
		//require_once 'FPDF/fpdf.php';
		//require_once 'html2pdf_v4.03/pdf.php';
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
		$htmldata = $larrformData['datacount'];
		$htmltabledata = $larrformData['datacounttable'];
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
		if($larrformData['ReportType'] == "pdf"){
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img align=center width=100% src="../public/images/header.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                            '.'{PAGENO}{nbpg}' );
		//$mpdf->setFooter ( date ( "d-m-Y H:i:s" ) . "                                          ".'{PAGENO}{nbpg}' );
		// LOAD a stylesheet
		//$stylesheet = file_get_contents('../public/css/default.css');
		//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Examination" ).' '.$this->view->translate( "Report" );
		$mpdf->WriteFixedPosHTML ( "<table border=1  align=center width=100%><tr><td align=center> {$ReportName}</td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		$html = htmlspecialchars_decode ( $htmltabledata );
		$mpdf->WriteHTML($html);
		$mpdf->Output('Examination_Part_Wise_Report.pdf','D');
		$auth = Zend_Auth::getInstance();
    	// Write Logs
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Examination Parts Wise Report(PDF)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);	
		}else{
			
			$ourFileName = realpath('.')."/data";
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); 
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($htmldata));
		fclose($ourFileHandle);
		   // $data = str_replace("\r","",$xlExportData);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename = Examination_Part_Wise_Report.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			//print $csv_output."\n".$data;
			readfile($ourFileName);
			unlink($ourFileName);
			$auth = Zend_Auth::getInstance();
    	// Write Logs
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Examination Parts Wise Report(Excel)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);
		}
	}
	 
}