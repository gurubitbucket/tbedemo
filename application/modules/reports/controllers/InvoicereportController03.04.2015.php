<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_InvoicereportController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator		
		$this->lobjinvoiceForm = new  Reports_Form_Invoicereport();//invoice form
		$this->lobjInvoicereportModel = new Reports_Model_DbTable_Invoicereport();//invoice model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjinvoiceForm;	
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Company";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Takaful";
		$this->view->lobjform->field19->addMultiOptions($larrtypeofcompany);
		$this->view->lobjform->field19->setAttrib('onchange', 'emptythenamelist');
		$this->lobjinvoiceForm->field3->setAttrib('onkeyup', 'fnGetOperatorNames');
	    $this->lobjinvoiceForm->Date3->setAttrib('onChange', "dijit.byId('Date4').constraints.min = arguments[0];");
	    //$this->lobjReportsForm->Date4->setAttrib('onChange', "dijit.byId('Date3').constraints.min = arguments[0];");
	    $larrresult=array();
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))	{
			$larrformData = $this->_request->getPost ();		
			if ($this->lobjform->isValid($larrformData)){
				if($larrformData['field3'] && !$larrformData['field19']){
					echo '<script language="javascript">alert("Please Select Type Of Operator")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/reports/invoicereport/index/';</script>";					
					exit;
				}
				$lvarfromdate = $this->view->fromdate=$larrformData['Date3'];
				$lvartodate = $this->view->todate=$larrformData['Date4'];				
				$operatortype = $this->view->operatortype=$larrformData['field19'];
				$operatorname = $this->view->operatorname=$larrformData['field3'];
				unset ( $larrformData ['Search'] );
				$larrresult = $this->lobjInvoicereportModel->fnSearchCompanies($larrformData); //searching the values for the Companies
				//echo "<pre/>";print_r($larrresult);die();
				$this->view->invoiceresult=$larrresult;
				$this->view->lobjform->populate($larrformData);
			}
		}		
	}
	
	public function getoperatornamesAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$newresult="";
		$opType = $this->_getParam('opType');	
		$namestring = $this->_getParam('namestring');	
		$larrresultopnames = $this->lobjInvoicereportModel->fnGetOperatorNames($opType,$namestring); 
		foreach ($larrresultopnames as $larrresnewarray){
			$opname=$larrresnewarray['name'];
			$newresult=$newresult."<tr><td><span id='idspan'   onclick='fnsetvalue(\"".$opname."\");'>".$larrresnewarray['name']."</span></td></tr></BR>";			
		}
		echo $newresult;
		exit;
	}
	
 	public function getdetailsAction()	{    
		$this->_helper->layout->disableLayout();
	  	$this->_helper->viewRenderer->setNoRender();
	  	$idinvoice = $this->_getParam('idinvoice');
	  	$opr = $this->_getParam('opr');
	  	$tabledata = '';	
	  	$result = $this->lobjInvoicereportModel->fngetinvoicedetails($idinvoice,$opr);	
	  	foreach($result as $lobjCountry){
	  	if ($lobjCountry['Flag'] == 1){ 
					$head="Company"; 
					$oprname="Company";
					$name=$lobjCountry['CompanyName'];
			}else if($lobjCountry['Flag'] == 2){ 
					$head= "Takaful";
					$oprname="Takafull";
					$name=$lobjCountry['TakafulName'];
			}
			break;
	  	}
	  	$tabledata.= '<fieldset><legend align = "left"> Invoice Details - ';
	  	$tabledata.=$head; 
	  	$tabledata.='</legend>
				<br><fieldset><legend align = "left"> '.$oprname.' Details </legend>			
				<table class="table" border=1 align = "center" width=100%>
					<tr>
						<th><b>operator Name</b></th>
						<th><b>Invoice no</b></th>
						<th><b>Registration Pin</b></th>
						<th><b>No. Of Candidates</b></th>
						<th><b>Amount</b></th>
					</tr>';
	  	$row=0;
	  	foreach($result as $lobjCountry){
	  		$tabledata.= '<tr>';
	  		if($row==0){
		  		$tabledata.= '<td align = "left">'.$name.'</td>';
		  		$tabledata.= '<td align = "left">'.$lobjCountry['invoiceno'].'</td>';
		  	}else{
		  		$tabledata.= '<td align = "left"></td>';
		  		$tabledata.= '<td align = "left"></td>';
		  	}
	  	$row++;	
	  	$tabledata.= '<td align = "left">'.$lobjCountry['Regpin'].'</td>
						<td>'.$lobjCountry['Totalcandidates'].'</td>
						<td>'.$lobjCountry['Amount'].'</td>
					</tr>';
		}
	  	$tabledata.="</table></fieldset><br>";
	  	$tabledata.="<tr><td colspan= '6'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'>&nbsp;&nbsp;</td></tr></fieldset>";	
		echo  $tabledata; 
	}

	
	public function pdfexportAction()
	{   
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	    $fromdate = date('d-m-Y',strtotime($this->_getParam('fromdate')));
	    $todate = date('d-m-Y',strtotime($this->_getParam('todate')));
	    $operatortype=$this->_getParam('operatortype');
	    $operatorname=$this->_getParam('operatorname');
	    if($operatortype==1){
	    	$operatortype="Company";
	    }else if($operatortype==2){
	    	$operatortype="Takafull";
	    }
	    if($operatorname==""){
	    	$operatorname="-----";
	    }
	    $larrformData = array();
	    $larrformData['Date3']=$this->_getParam('fromdate');
	    $larrformData['Date4']=$this->_getParam('todate');
	    $larrformData['field19']=$this->_getParam('operatortype');
	    $larrformData['field3']=$this->_getParam('operatorname');
		
	    $result1 = $this->lobjInvoicereportModel->fnSearchCompanies($larrformData); //searching the values for the Companies
	    $namespace = new Zend_Session_Namespace('pdfaction');
        $data=$namespace->data; 
		$count=count($result);
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Invoice_Report_'.$fromdate.'_'.$todate;
		$ReportName = $this->view->translate( "Invoice" ).' '.$this->view->translate( "Details" ).' '.$this->view->translate( "Report" );
		$lstrreportytpe=$this->_getParam('reporttype');
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br/><br/<br/><br/><br/><br/>';
		}
		$tabledata.= "<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 8><b> {$ReportName}</b></td>	
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date</b></td>
								<td align= 'left' colspan= 2><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 4><b>$time</b></td>
							</tr>";
			$tabledata.= "<tr>	
							<td><b>Invoice From Date </b></td>
							<td align= 'left' colspan= 2><b>"."&nbsp;".$fromdate."</b></td>
							<td><b>Invoice To Date</b></td>
							<td align = 'left' colspan= 4><b>"."&nbsp;".$todate."</b></td>
						</tr>";
			if($operatortype!=''){
			$tabledata.="<tr>	
							<td><b>Type of Operator</b></td>
							<td align= 'left' colspan= 2><b>$operatortype</b></td>
							<td><b>Operator Name</b></td>
							<td align='left' colspan= 4><b>$operatorname</b></td>
						</tr>";
					
			}
			$tabledata.="</table><br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th align="left"><b>Company/Takaful</b></th>
							<th><b>Generated Date</b></th>
							<th><b>Operator Name</b></th>
							<th><b>Invoice Number</b></th>
							<th><b>Batch ID</b></th>
							<th width="20"><b>Total Candidates</b></th>  
							<th><b>Amount</b></th>
							<th><b>Generated By</b></th>
							
						</tr>';  
	if (count($result1)): 
		$cnt = 0;$cnt1 = 0;$camt = 0;$tamt = 0; $ctc = 0;$ttc = 0;
        ///////////company
        //echo "<pre/>";print_r($result1);
		foreach ($result1 as $invoiceresult ): 
		//echo trim($invoiceresult['Regpin'],",");echo "<br/>";
       		if($invoiceresult['Flag']==1){
	       		if($cnt==0){$name = 'Company';}else {$name = '';}
	       		$ctc=$ctc+$invoiceresult['Totalcandidates'];
	       		$camt=$camt+$invoiceresult['Amount'];
	       		$reglist=str_replace(",",", ",trim($invoiceresult['Regpin'],","));
			       	$tabledata.= '<tr>
			      	<td><b>'.$name.'</b></td>
			      	<td>'."&nbsp;".$invoiceresult['UpdDate'].'</td>
			      	<td >'.$invoiceresult['CompanyName'].'</td>
			     	<td >'.$invoiceresult['invoiceno'].'</td>
			     	<td align="left">'.$reglist.'</td>
			     	<td align="right">'.$invoiceresult['Totalcandidates'].'</td>
			     	<td align="right">'.$invoiceresult['Amount'].'</td>
			     	<td>'.$invoiceresult['fName'].'</td>
			     	
			      	</tr>';
			      	$cnt++;	
				}
        endforeach;
		if($cnt>0){
				$tabledata.='<tr><td colspan="8"><hr></td></tr>';
				$tabledata.= '<tr >
			      	<td></td>
			     	<td ><b>'."Total Count".'</b></td>
			     	<td></td>
			     	<td ><b>'.$cnt.'</b></td>
			     	<td></td>
			     	<td align="right"><b>'.number_format($ctc).'</b></td>
			     	<td align="right"><b>'.number_format($camt,2).'</b></td>
			     	<td></td>
			     	
		     		</tr>';
	     		$tabledata.='<tr><td colspan="8"><hr></td></tr>';
			}
		
		////////takaful
		foreach ($result1 as $invoiceresult ): 
       		if($invoiceresult['Flag']==2){
       			$generateddate1=$invoiceresult['UpdDate'];
	       		if($cnt1==0){$name = 'Takaful';}else {$name = '';}
	       		$ttc=$ttc+$invoiceresult['Totalcandidates'];
	       		$tamt=$tamt+$invoiceresult['Amount'];
	       		$reglist=str_replace(",",", ",trim($invoiceresult['Regpin'],","));
			       	$tabledata.= '<tr>
			      	<td><b>'.$name.'</b></td>
			      	<td>'."&nbsp;".$invoiceresult['UpdDate'].'</td>
			      	<td >'.$invoiceresult['TakafulName'].'</td>
			     	<td >'.$invoiceresult['invoiceno'].'</td>
			     	<td align="left">'.$reglist.'</td>
			     	<td align="right">'.$invoiceresult['Totalcandidates'].'</td>
			     	<td align="right">'.$invoiceresult['Amount'].'</td>
			     	<td>'.$invoiceresult['fName'].'</td>
			     	
			      	</tr>';
			      	$cnt1++;	
				}
        endforeach;
		if($cnt1>0){
				$tabledata.='<tr><td colspan="8"><hr></td></tr>';
				$tabledata.= '<tr >
			      	<td></td>
			     	<td ><b>'."Total Count".'</b></td>
			     	<td></td>
			     	<td ><b>'.$cnt1.'</b></td>
			     	<td></td>
			     	<td align="right"><b>'.number_format($ttc).'</b></td>
			     	<td align="right"><b>'.number_format($tamt,2).'</b></td>
			     	<td></td>
			     	</tr>';
	     		$tabledata.='<tr><td colspan="8"><hr></td></tr>';
			}
	endif;
			
	$tabledata.= '<tr >
	<td></td>
	<td ><b>'."Grand Total".'</b></td>
	<td></td>
	<td ><b>'.number_format($cnt+$cnt1).'</b></td>
	<td></td>
	<td align="right"><b>'.number_format($ctc+$ttc).'</b></td>
	<td align="right"><b>'.number_format($camt+$tamt,2).'</b></td>
	<td></td>
	
	</tr></table>';
		
	if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2013, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output("$filename.pdf",'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}
				
	}
				
}
