<?php
class Reports_StudentexamreportController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;	
	
	public function init() { //initialization function
		$this->gobjsessionstudent = Zend_Registry::get('sis');
		/*$this->gobjsessionsis = Zend_Registry::get('sis');
		if(empty($this->gobjsessionsis->iduser)){ 
			$this->_redirect( $this->baseUrl . '/index/logout');					
		}	*/	
			
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() 
	{
		$this->lobjStudentexamreportmodel = new Reports_Model_DbTable_Studentexamreport(); //user model object
		$this->lobjStudentexamreportform = new Reports_Form_Studentexamreport(); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

	public function indexAction() 
	{ 
		$this->view->lobjStudentexamreportform = $this->lobjStudentexamreportform;
		$larrexamcenters = $this->lobjStudentexamreportmodel->fngetcenternames();	
		$this->lobjStudentexamreportform->examcenter->addmultioptions($larrexamcenters);
		
		if ($this->_request->isPost() && $this->_request->getPost('Submit'))
		{ 
		   $larrformData = $this->_request->getPost();
             //echo "<pre/>";print_r($larrformData);die();       
		   $fromdate = $this->view->fromdate=$larrformData['Fromdate'];
		   $todate = $this->view->todate=$larrformData['Todate'];
		   $this->view->icno=$larrformData['ICNO'];
		   $this->view->name=$larrformData['Studentname'];
		    $this->view->examcenter=$larrformData['examcenter'];
		    
		   $larrresult = $this->lobjStudentexamreportmodel->fngetdetailforstudent($larrformData['Fromdate'],$larrformData['Todate'],$larrformData);
		  
		   $this->view->paginator = $larrresult;
		   $this->view->lobjStudentexamreportform->populate($larrformData);
		  
		}
		if ($this->_request->isPost() && $this->_request->getPost('No'))
		{
		    $this->_redirect(  $this->baseUrl  . "/reports/studentexamreport/index");
			    die();
		}
		if ($this->_request->isPost() && $this->_request->getPost('Search'))
		{
			 Zend_Session:: namespaceUnset('someaction');
			$larrformData = $this->_request->getPost();
			
			//echo "<pre>";print_R($larrformData);die();
		   $result = $this->lobjStudentexamreportmodel->fngetchronestatus($larrformData['Fromdate'],$larrformData['Todate'],$larrformData['examcenter']);
		   if(!empty($result))
			{
			    $this->_redirect(  $this->baseUrl  . "/reports/studentexamreport/pushpulldetails/fromdate/".$larrformData['Fromdate']."/todate/".$larrformData['Todate']."/name/".$larrformData['Studentname']."/icno/".$larrformData['ICNO']."/center/".$larrformData['examcenter']);
			    die();
			} 
			else
			{
                   $fromdate = $this->view->fromdate=$larrformData['Fromdate'];
			       $todate = $this->view->todate=$larrformData['Todate'];
			       $this->view->icno=$larrformData['ICNO'];
			       $this->view->name=$larrformData['Studentname'];
			       $this->view->examcenter=$larrformData['examcenter'];
			       $larrresult = $this->lobjStudentexamreportmodel->fngetdetailforstudent($larrformData['Fromdate'],$larrformData['Todate'],$larrformData);
			       //echo "<>";
			       $this->view->paginator = $larrresult;
			       $this->view->lobjStudentexamreportform->populate($larrformData);

			}			
		}		
		
	}
public function pushpulldetailsAction()
{	
		$this->view->lobjStudentexamreportform = $this->lobjStudentexamreportform;
		$this->view->Examfromdate = $Fromdate = $this->_getParam('fromdate');
		$this->view->Examtodate  = $Todate = $this->_getParam('todate');
		$this->view->Studentname = $this->_getParam('name');
		$this->view->Icno  = $this->_getParam('icno');
		$this->view->center  =$center= $this->_getParam('center');
		 $result = $this->lobjStudentexamreportmodel->fngetchronestatus($Fromdate,$Todate,$center);
		

	   $this->view->pushpulldata =$result;
}	public function pdfexportAction()
	{   
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformdata1 = $this->_request->getPost();
		
		$fdate = $larrformdata1['Fromdate'];
		$tdate = $larrformdata1['Todate'];
		$examcenterid=$larrformdata1['examcenter'];
		if(isset($examcenterid) && !empty($examcenterid)){
			$examcenterdata= $this->lobjStudentexamreportmodel->fngetcenternameonid($examcenterid);
			$examcenter=$examcenterdata['centername'];
		}else{
			$examcenter="All";
		}
		
		$frmdate =date('d-m-Y',strtotime($fdate));
		$todate =date('d-m-Y',strtotime($tdate));
		$lstrreportytpe = $larrformdata1['Print1'];//$this->_getParam('reporttype');
		
		
		$result = $this->lobjStudentexamreportmodel->fngetdetailforstudent($larrformdata1['Fromdate'],$larrformdata1['Todate'],$larrformdata1);
		$count=count($result);
		
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbedemo/images/image.jpg"; 
		$time = date('h:i:s',time());
		$filename = 'Studentsummaryreport_'.$frmdate.'_'.$todate;
		$ReportName = $this->view->translate( "Student" ).' '.$this->view->translate( "Summary" ).' '.$this->view->translate( "Report" );
		
		//$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
		if($lstrreportytpe=='Excel')
		{
			//$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
			$tabledata='<table border=0><tr><td height="210"><img src= "'.$imgp.'" /></td></tr></table>';	
		}
		else
		{
		   //$tabledata = '<img width=100% src="../public/images/reportheader.jpg" /><br><br><br><br><br>';
		    $tabledata = '<p style="text-align:left;"><img width="30%" src="../public/images/image.jpg" /></p>';
		}
		
		$tabledata.= "<tr>
			<td align=left colspan = 2><b>Exam From Date</b></td>
			<td align=left colspan = 2><b> : ".$frmdate."&nbsp;&nbsp;</b></td>
			<td  align=left colspan = 2><b> Exam To Date </b></td>
			<td align=left colspan = 2><b>: ".$todate."</b>&nbsp;&nbsp;</td>
			<td  align=left colspan = 2><b>Exam Center</b></td>
			<td align=left colspan = 2><b>: ".$examcenter."</b></td>
		</tr>";
		
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 8><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table border=1 align=center width=100%><tr><th><b>Exam Date</b></th><th><b>Exam Center</b></th><th><b>Student</b></th><th><b>TakafulName</b></th><th><b>Qualification</b></th></th><th><b>Race</b></th>
		<th><b>Gender</b></th><th><b>Age</b></th><th><b>Email</b></th><th><b>Program</b></th><th><b>ICNO</b></th><th><b>Result</b></th><th><b>Grade</b></th></tr>';
		$cnts  = 0;
		$centerarray =array();
		$centerarrays =array();
		$centerarrayss =array();
		$centerarraysss =array();
		foreach($result as $lobjCountry)
		{
			
			
			    $tabledata.="<tr>";
			    
			     $tabledata.="<td>";
				$tabledata.="&nbsp;".date('d-m-Y',strtotime($lobjCountry['Date']));
                $tabledata.="</td>";				
		
				
			  $tabledata.="<td>";				
				$tabledata.=$lobjCountry['centername'];
			  $tabledata.="</td>";
			  	$tabledata.= '<td>'.$lobjCountry['FName'].'</td>';
			  $tabledata.="<td>";				
				$tabledata.=$lobjCountry['TakafulName'];
			  $tabledata.="</td>";
			  
			  
			   $tabledata.="<td>";				
				$tabledata.=$lobjCountry['Qualification'];
			  $tabledata.="</td>";
			   $tabledata.="<td>";
				
				
				
				$tabledata.=$lobjCountry['Race'];
			  $tabledata.="</td>";
			   if($lobjCountry['Gender'] ==1)
			   {
			       $tabledata.= '<td>Male</td>';
			   }
			   else
			   {
			    $tabledata.= '<td>Female</td>';
			   }
			    $tabledata.="<td>";				
				$tabledata.=$lobjCountry['Age'];
			  $tabledata.="</td>";
				$tabledata.="<td>";
				$tabledata.=$lobjCountry['EmailAddress'];
			  $tabledata.="</td>";
			  
			  
			   $tabledata.="<td>";
				$tabledata.=$lobjCountry['ProgramName'];
				$tabledata.="</td>";
			
			
			 $tabledata.="<td>";
				$tabledata.=$lobjCountry['ICNO'];
				$tabledata.="</td>";
			
				if($lobjCountry['pass'] == 1)
				{  
			    $tabledata.= '<td>Pass</td>';
				}
				elseif($lobjCountry['pass'] == 2)
				{
				    $tabledata.= '<td>Fail</td>';
				}
				elseif($lobjCountry['pass'] == 4)
				{
				
				 $tabledata.= '<td>Absent</td>';
				 }
				  $tabledata.="<td>";
				
				
				$tabledata.=$lobjCountry['Grade'];
				$tabledata.="</td>";
			
				 
			$tabledata.="</tr>";
		}
		
		    $tabledata.="</table>";
			if($lstrreportytpe=='Excel'){
		
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
			}
			else {
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}
			
			
			
		
	}

	

}
