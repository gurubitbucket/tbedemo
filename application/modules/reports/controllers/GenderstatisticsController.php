<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_GenderstatisticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjstasticsform = new  Reports_Form_Statistics();
		$this->lobjstastics = new Reports_Model_DbTable_Genderstatistics();		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjstasticsform;
		   /*if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {   
		    	$larrformData = $this->_request->getPost ();
				if ($this->lobjform->isValid($larrformData)) 
				{
				 $larrformData = $this->_request->getPost ();
				 $ldtdate = $larrformData['Date'];*/
		         $ldtdate = $this->_getParam('ldtdate');
				 $this->view->searchdate = $ldtdate;
				 $larrdates = explode('-',$ldtdate);
				 $lintmon = $larrdates['1'];
				 $lintyear = $larrdates['0'];
				 $lstrmonthname = date( 'F', mktime(0,0,0,$lintmon));
				 $this->view->month = $lstrmonthname;
				 $this->view->year = $lintyear;
				 unset ( $larrformData ['Search'] );
				 $lintmaleid = 1;$lintfemaleid = 0;
				 $larrracescount = $this->lobjstastics->fngetgenderscount($lintmon,$lintyear);
				 $lintregisteredtotal = $larrracescount[0]['NoOfCandidates']+$larrracescount[1]['NoOfCandidates'];
				 $larrracescount[2]['regtotal'] = $lintregisteredtotal;;
				 $larrmaleresult = $this->lobjstastics->fngetgenderresult($lintmon,$lintyear,$lintmaleid);
				 $lintmalecount = count($larrmaleresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent = 0;
				 for($linti=0;$linti<$lintmalecount;$linti++){
				 	if($larrmaleresult[$linti]['pass']==1)
				 	{
				 		$lintpass++;
				 	}elseif ($larrmaleresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrracescount[]['msat'] = $lintpass+$lintfail;
				 $larrracescount[]['mpass'] = $lintpass;
				 $larrracescount[]['mfail']= $lintfail;
				 $larrracescount[]['mabsent']= $larrracescount[1]['NoOfCandidates']-($lintpass+$lintfail);//$lintabsent;
				 $larrfemaleresult = $this->lobjstastics->fngetgenderresult($lintmon,$lintyear,$lintfemaleid);
				 $lintfemalecount = count($larrfemaleresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent = 0;
				 for($linti=0;$linti<$lintfemalecount;$linti++){
				 	if($larrfemaleresult[$linti]['pass']==1)
				    {
				 		$lintpass++;
				 	}elseif ($larrfemaleresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrracescount[]['fsat'] = $lintpass+$lintfail;
				 $larrracescount[]['fpass'] = $lintpass;
				 $larrracescount[]['ffail']= $lintfail;
				 $larrracescount[]['fabsent']= $larrracescount[0]['NoOfCandidates']-($lintpass+$lintfail);//$lintabsent;
				 $larrracescount[]['sattotal']=  $larrracescount[3]['msat']+$larrracescount[7]['fsat'];
				 $larrracescount[]['passtotal']=  $larrracescount[4]['mpass']+$larrracescount[8]['fpass'];
				 $larrracescount[]['failtotal']=  $larrracescount[5]['mfail']+$larrracescount[9]['ffail'];
				 $larrracescount[]['absenttotal']=  $larrracescount[6]['mabsent']+$larrracescount[10]['fabsent'];
				 $this->view->larrracescount   =  $larrracescount;
			     $this->view->paginator = $larrracescount;
				 //$this->view->lobjform->populate($larrformData);
			//}
 	      //}
	}
			public function fnexportexcelAction()
				{    
					 $this->_helper->layout->disableLayout();
					 $this->_helper->viewRenderer->setNoRender();
					 $larrformData = $this->_request->getPost();
					 unset ( $larrformData ['ExportToExcel'] );
					 $lday= date("d-m-Y");
					 $ltime = date('h:i:s',time());
					 $host = $_SERVER['SERVER_NAME'];
					 $imgp = "http://".$host."/tbenew/images/reportheader.jpg";
					 $filename = 'Gender_Statistics_Report_'.$larrformData['selmonth'];
					 $ReportName = $this->view->translate( "Gender" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
					 $tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2><b>Date </b></td><td align ='left' colspan = 3><b>$lday</b></td><td align ='left' colspan = 3><b> Time</b></td><td align ='left' colspan = 3><b>$ltime</b></td></tr></table>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 11 align=center><b>{$ReportName}</b></td></tr></table><br>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 9><b>".$larrformData['selmonth']."</b></td></tr>";
					 $tabledata.="<tr> <td align ='left' colspan = 2><b>Gender</b></td><td align ='center' colspan = 3><b>Male</b></td><td align ='center' colspan = 3><b>Female</b></td><td align ='center' colspan = 3><b>Total</b></td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td><td align ='center' colspan = 3>".$larrformData['malecand']."</td><td align ='center' colspan = 3>".$larrformData['femalecand']."</td><td align ='center' colspan = 3>".$larrformData['regtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td><td align ='center' colspan = 3>".$larrformData['msat']."</td><td align ='center' colspan = 3>".$larrformData['fsat']."</td><td align ='center' colspan = 3>".$larrformData['sattotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td><td align ='center' colspan = 3>".$larrformData['mpass']."</td><td align ='center' colspan = 3>".$larrformData['fpass']."</td><td align ='center' colspan = 3>".$larrformData['passtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td><td align ='center' colspan = 3>".$larrformData['mfail']."</td><td align ='center' colspan = 3>".$larrformData['ffail']."</td><td align ='center' colspan = 3>".$larrformData['failtotal']."</td></tr>";				
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td><td align ='center' colspan = 3>".$larrformData['mabsent']."</td><td align ='center' colspan = 3>".$larrformData['fabsent']."</td><td align ='center' colspan = 3>".$larrformData['absenttotal']."</td></tr>";				
					 $ourFileName = realpath('.')."/data";
					 $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
					 ini_set('max_execution_time', 3600);
					 fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
					 fclose($ourFileHandle);
					 header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
					 header("Content-Disposition: attachment; filename=$filename.xls");
					 header("Pragma: no-cache");
					 header("Expires: 0");
					 readfile($ourFileName);
					 unlink($ourFileName);
				}	
}	
 