<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_RecieptreportController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator		
		//$this->lobjCommon = new App_Model_Common();
		$this->lobjReportsForm = new  Reports_Form_Report();
		$this->lobjPaypalentryreportModel = new Reports_Model_DbTable_Paypalentryreport();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjReportsForm;		
		$this->lobjReportsForm->Venues->addMultiOption('','Select');
		//$this->lobjReportsForm->State->addMultiOption('','Select');
		
		$larrcenters = array(array(key=>1,value=>"Indivdual Registration"),array(key=>2,value=>"Company Registration"),array(key=>3,value=>"Takaful Registration"));
		$this->lobjReportsForm->Venues->addmultioptions($larrcenters);
		
		$larrpaymenttypes = array(array(key=>1,value=>"FPX"),array(key=>2,value=>"Credit Card"),array(key=>4,value=>"Cheque"),array(key=>5,value=>"Money Order"),array(key=>6,value=>"Postal Order"),array(key=>7,value=>"Credit/Bank to IBFIM account"),array(key=>181,value=>"Pay Later"));
		$this->lobjReportsForm->Coursename->addmultioptions($larrpaymenttypes);
		
	    $this->lobjReportsForm->Date3->setAttrib('onChange', "dijit.byId('Date4').constraints.min = arguments[0];");
	    //$this->lobjReportsForm->Date4->setAttrib('onChange', "dijit.byId('Date3').constraints.min = arguments[0];");
		$larrresult = array();
		$gst = $this->lobjPaypalentryreportModel->fngettaxrate();
         $this->view->GST = $gst['GST'];
         $this->view->eachcharge=$gst['eachcharge'];
		 

		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))	{
			$larrformData = $this->_request->getPost ();		
			if ($this->lobjform->isValid($larrformData)){				
				$lvarfromdate = $this->view->fromdate=$larrformData['Date3'];
				$lvartodate = $this->view->todate=$larrformData['Date4'];				
				$lvartype = $this->view->venue=$larrformData['Venues'];
				$lvarpaymenttype = $this->view->Coursename=$larrformData['Coursename'];
				unset ( $larrformData ['Search'] );
				$result1 = array();
				$result2 = array();
				$result3 = array();
				if($lvartype == "" || $lvartype == 1){//Individual 
					$result11 = array();
					$result12 = array();
					$result13 = array();
					$result66= array();
					if($lvarpaymenttype == "" || $lvarpaymenttype == 1){	// FPX				
						$result11 = $this->lobjPaypalentryreportModel->fngetstudentfpxentrydetails($larrformData); //searching the values for the student
					}
					if($lvarpaymenttype == "" || $lvarpaymenttype == 2){	//CreditCard				
						$result12 = $this->lobjPaypalentryreportModel->fngetstudentpaypalentrydetails($larrformData); //searching the values for the student
					}
			/*	if($lvarpaymenttype == "" || $lvarpaymenttype == 10){	//CreditCard				
						$result66 = $this->lobjPaypalentryreportModel->fngetstudentmigsentrydetails($larrformData); //searching the values for the student
					}*/
					if($lvarpaymenttype == "" || ($lvarpaymenttype != 1 && $lvarpaymenttype != 2)){	// Other Payments				
						$result13 = $this->lobjPaypalentryreportModel->fngetstudentpaymententrydetails($larrformData); //searching the values for the student
					}
					$result1 = array_merge($result11,$result12,$result13);	
                   	//echo "<pre>";print_r($result1);die();			
				}
				if($lvartype == "" || $lvartype == 2){//Company 
					$result21 = array();
					$result22 = array();
					$result23 = array();
					$result55=  array();
					
					if($lvarpaymenttype == "" || $lvarpaymenttype == 1){	// FPX
						$result21 = $this->lobjPaypalentryreportModel->fngetcompanypfxentrydetails($larrformData); //searching the values for the company
						
					}
					if($lvarpaymenttype == "" || $lvarpaymenttype == 2){	//CreditCard
						$result22 = $this->lobjPaypalentryreportModel->fngetcompanypaypalentrydetails($larrformData); //searching the values for the company
						
					}
					
					if($lvarpaymenttype == "" || $lvarpaymenttype == 181) {		//paylater		
						$result55 = $this->lobjPaypalentryreportModel->fngetcompanypaymentpaylaterdetails($larrformData); //searching the values for the takaful
						
					}
					if($lvarpaymenttype == "" ||  $lvarpaymenttype == 4 || $lvarpaymenttype == 7 || $lvarpaymenttype == 5 || $lvarpaymenttype == 6 ){	// Other Payments	
						$result23 = $this->lobjPaypalentryreportModel->fngetcompanypaymententrydetails1($larrformData); //searching the values for the company	
						
					}
					if(empty($result23))
					{
						$result2 = array_merge($result21,$result22,$result55);
					}
					else
					{
					
					    $result2 = array_merge($result21,$result22,$result23,$result55);
				   }
                    					
                    //echo "<pre>";print_r($result2);die();			
				}		
				if($lvartype == "" || $lvartype == 3){//Takaful 
					$result31 = array();
					$result32 = array();
					$result33 = array();
					$result44=  array();
					if($lvarpaymenttype == "" || $lvarpaymenttype == 1){	// FPX				
						$result31 = $this->lobjPaypalentryreportModel->fngettakafulfpxentrydetails($larrformData); //searching the values for the takaful
					}
					if($lvarpaymenttype == "" || $lvarpaymenttype == 2){		//CreditCard			
						$result32 = $this->lobjPaypalentryreportModel->fngettakafulpaypalentrydetails($larrformData); //searching the values for the takaful
					}
				if($lvarpaymenttype == "" || $lvarpaymenttype == 181) {		//paylater			
						$result44 = $this->lobjPaypalentryreportModel->fngettakafulpaylaterdetails($larrformData); //searching the values for the takaful
					}
					
					if($lvarpaymenttype == "" || $lvarpaymenttype == 4 || $lvarpaymenttype == 7 || $lvarpaymenttype == 5 || $lvarpaymenttype == 6 ){	// Other Payments	
						$result33 = $this->lobjPaypalentryreportModel->fngettakafulpaymententrydetails($larrformData); //searching the values for the takaful	
					}
					$result3 = array_merge($result31,$result32,$result33,$result44);
					//echo "<pre>";print_r($result3);die();
				}
				//	echo "<pre/>";
				//print_r($result3);
			//	die();
				  $batchids="";
				  
				for($rq=0;$rq<count($result3);$rq++)
				{
					if($rq==0)
					{
						$batchids=$result3[$rq]['registrationPin'];
						$batchids="'$batchids'";
					}
					else 
					{
						$batchidsss=$result3[$rq]['registrationPin'];
 				   		$batchidsss="'$batchidsss'";
 				   		$batchids.=','.$batchidsss;
					}
				}
				$resulttq= $this->lobjPaypalentryreportModel->fnbatchregcnt($batchids);
				
				$result333=array();
				for($r=0;$r<count($resulttq);$r++)
				{
					$idpin=$resulttq[$r]['RegistrationPin'];
					$result333[$idpin]=$resulttq[$r]['regtotal'];
					
				}
				
				
				 $batchidscmp="";
					for($raq=0;$raq<count($result2);$raq++)
				{
					if($raq==0)
					{
						$batchidscmp=$result2[$raq]['registrationPin'];
						$batchidscmp="'$batchidscmp'";
					}
					else 
					{
						$batchidsss=$result2[$raq]['registrationPin'];
 				   		$batchidsss="'$batchidsss'";
 				   		$batchidscmp.=','.$batchidsss;
					}
				}
				
				$result222 = array();
				$resultcmp= $this->lobjPaypalentryreportModel->fnbatchregcnt($batchidscmp);
				//echo "<pre />";
				//print_r($resultcmp);die();
				for($r=0;$r<count($resultcmp);$r++)
				{
					$idpin=$resultcmp[$r]['RegistrationPin'];
					$result222[$idpin]=$resultcmp[$r]['regtotal'];
					
				}
				
				
				$this->view->result1 = $result1;
				
				$this->view->result2 = $result2;
				$this->view->result222=$result222;
				$this->view->result3 = $result3;
				$this->view->result333=$result333;
				$this->view->lobjform->populate($larrformData);
			}
		}		
	}
 public function getdetailsAction()	{    
	  $this->_helper->layout->disableLayout();
	  $this->_helper->viewRenderer->setNoRender();
	  $lintIdapp = $this->_getParam('idapp');
	  $lintfrom = $this->_getParam('from');
	  $lintmdp = $this->_getParam('mop');
	  $tabledata = '';	
	  if($lintfrom ==0){
	  	  if($lintmdp == 1){
			  $result = $this->lobjPaypalentryreportModel->fngetfpxstuddetails($lintIdapp);		  
			  foreach($result as $lobjCountry){ 
			  		   $tabledata.="<fieldset><legend align = 'left'> Indivdual Registration - FPX</legend>
			  		   			<br><fieldset><legend align = 'left'> Candidate Details </legend>
				         		   <table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>IdApplication</b></th><th><b>IC No.</b></th><th><b>Date Of Birth</b></th><th><b>Email</b></th></tr><tr>";
				      $tabledata.="<td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['IDApplication']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['DateOfBirth']))."</td><td align = 'left'>".$lobjCountry['EmailAddress']."</td></tr>";
				      $tabledata.="</table></fieldset><br>";
				     $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
					         		   <table class='table' border=1 align='center' width=100%>
					         		   <tr><th><b>Order No.</b></th><th><b>Transaction Id</b></th><th><b>Bank</b></th><th><b>Branch</b></th><th><b>Amount</b></th><th><b>Payer Mail Id</b></th><th><b>Payment Date</b></th>
					         		   <th><b>Approved Date</b></th></tr><tr>";
					      $tabledata.="<td align = 'left'>".$lobjCountry['orderNumber']."</td>
					      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
					      		<td align = 'left'>";						      
						      if($lobjCountry['BankName'])$tabledata.= $lobjCountry['bankCode']; else $tabledata.="NA";						      
						     $tabledata.="</td>
					      		<td align = 'left'>";						      
						      if($lobjCountry['BankName'])$tabledata.= $lobjCountry['BankName']; else $tabledata.="NA";						      
						     $tabledata.="</td>
					      		<td align = 'right'>".$lobjCountry['grossAmount']."</td>
					      		
					      		<td align = 'left'>".$lobjCountry['payerId']."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['ApprovedDate']))."</td></tr>";
					      $tabledata.="</table></fieldset><br>";
					      $this->lobjExamreportModel = new Reports_Model_DbTable_Examreport();
					       $results = $this->lobjExamreportModel->fngetstuddetails($lobjCountry['IDApplication']);
					       $tabledata.= '<br><fieldset><legend align = "left"> Exam Details </legend>
						                    <table class="table" border=1 align = "center" width=100%><tr><th><b>Program</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th></tr><tr>';
							  $tabledata.= '<td align = "left">'.$results[0]['ProgramName'].'</td><td align = "left">'.$results[0]['ExamVenue'].'</td><td align = "left">'.$results[0]['ExamDate'].'</td><td align = "left">'.$results[0]['session'].'</td></tr>';
						      $tabledata.="</table></fieldset>";				      		
				}
	  	  }
	  	  else if($lintmdp == 2){
		  	   $result = $this->lobjPaypalentryreportModel->fngetstuddetails($lintIdapp);
			  
			  	foreach($result as $lobjCountry){ 
			  		   $tabledata.="<fieldset><legend align = 'left'> Indivdual Registration - Credit Card</legend>
			  		   <br><fieldset><legend align = 'left'> Candidate Details </legend>
				         		   <table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>IdApplication</b></th><th><b>IC No.</b></th><th><b>Date Of Birth</b></th><th><b>Email</b></th></tr><tr>";
				      $tabledata.="<td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['IDApplication']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['DateOfBirth']))."</td><td align = 'left'>".$lobjCountry['EmailAddress']."</td></tr>";
				      $tabledata.="</table></fieldset><br>";
				     $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
					         		   <table class='table' border=1 align='center' width=100%>
					         		   <tr><th><b>Reference No.</b></th><th><b>Transaction Id</b></th><th><b>Amount</b></th><th><b>Verify Sign</b></th><th><b>Payer Mail Id</b></th><th><b>Payment Date</b></th>
					         		   <th><b>Approved Date</b></th></tr><tr>";
					      $tabledata.="<td align = 'left'>".$lobjCountry['IDApplication']."</td>
					      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
					      		<td align = 'right'>".$lobjCountry['grossAmount']."</td>
					      		<td align = 'left'>".$lobjCountry['verifySign']."</td>
					      		<td align = 'left'>".$lobjCountry['payerId']."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['ApprovedDate']))."</td></tr>";
					      $tabledata.="</table></fieldset><br>";
					      $this->lobjExamreportModel = new Reports_Model_DbTable_Examreport();
					       $results = $this->lobjExamreportModel->fngetstuddetails($lobjCountry['IDApplication']);
					       $tabledata.= '<br><fieldset><legend align = "left"> Exam Details </legend>
						                    <table class="table" border=1 align = "center" width=100%><tr><th><b>Program</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th></tr><tr>';
							  $tabledata.= '<td align = "left">'.$results[0]['ProgramName'].'</td><td align = "left">'.$results[0]['ExamVenue'].'</td><td align = "left">'.$results[0]['ExamDate'].'</td><td align = "left">'.$results[0]['session'].'</td></tr>';
						      $tabledata.="</table></fieldset>";
				   
				      		
				}
	  	  }
	  	  else {
	  	  	$result = $this->lobjPaypalentryreportModel->fngetstudotherpaymentdetails($lintIdapp);	  	  	
		  	  foreach($result as $lobjCountry){ 
				  		   $tabledata.="
				  		   <fieldset><legend align = 'left'> Indivdual Registration - ";
				  	  if ($lobjCountry['ModeofPayment'] == 1) $tabledata.="FPX"; else if($lobjCountry['ModeofPayment'] == 2) $tabledata.= "Credit Card";else if($lobjCountry['ModeofPayment'] == 4) $tabledata.= "Cheque";else if($lobjCountry['ModeofPayment'] == 5) $tabledata.= "Money Order";else if($lobjCountry['ModeofPayment'] == 6) $tabledata.= "Postal Order";else if($lobjCountry['ModeofPayment'] == 7) $tabledata.= "Credit/Bank to IBFIM account";else if($lobjCountry['ModeofPayment'] == 181) $tabledata.= "Pay Later";
						     	   
				  		  $tabledata.="</legend>
				  		   <br><fieldset><legend align = 'left'> Candidate Details </legend>
					         		   <table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>IdApplication</b></th><th><b>IC No</b></th><th><b>Date Of Birth</b></th><th><b>Email</b></th></tr><tr>";
					      $tabledata.="<td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['IDApplication']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['DateOfBirth']))."</td><td align = 'left'>".$lobjCountry['EmailAddress']."</td></tr>";
					      $tabledata.="</table></fieldset><br>";
					     $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
						         		   <table class='table' border=1 align='center' width=100%>
						         		   <tr><th><b>Reference No.</b></th><th><b>Transaction Id/Cheque No.</b></th><th><b>Amount</b></th><th><b>Bank Name</b></th><th><b>Mode Of Payment</b></th><th><b>Payment Date</b></th>
						         		    </tr><tr>";
						      $tabledata.="<td align = 'left'>".$lobjCountry['IDApplication']."</td>
						      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
						      		<td align = 'right'>".$lobjCountry['grossAmount']."</td>
						      		<td align = 'left'>";						      
						      if($lobjCountry['BankName'])$tabledata.= $lobjCountry['BankName']; else $tabledata.="NA";						      
						     $tabledata.="</td>
						      		<td align = 'left'>";
						       if ($lobjCountry['ModeofPayment'] == 1) $tabledata.="FPX"; else if($lobjCountry['ModeofPayment'] == 2) $tabledata.= "Credit Card";else if($lobjCountry['ModeofPayment'] == 4) $tabledata.= "Cheque";else if($lobjCountry['ModeofPayment'] == 5) $tabledata.= "Money Order";else if($lobjCountry['ModeofPayment'] == 6) $tabledata.= "Postal Order";else if($lobjCountry['ModeofPayment'] == 7) $tabledata.= "Credit/Bank to IBFIM account";else if($lobjCountry['ModeofPayment'] == 181) $tabledata.= "Pay Later";
						      $tabledata.="</td>
						      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
						      		</tr>";
						      $tabledata.="</table></fieldset><br>";
						      $this->lobjExamreportModel = new Reports_Model_DbTable_Examreport();
						       $results = $this->lobjExamreportModel->fngetstuddetails($lobjCountry['IDApplication']);
						       $tabledata.= '<br><fieldset><legend align = "left"> Exam Details </legend>
							                    <table class="table" border=1 align = "center" width=100%><tr><th><b>Program</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th></tr><tr>';
								  $tabledata.= '<td align = "left">'.$results[0]['ProgramName'].'</td><td align = "left">'.$results[0]['ExamVenue'].'</td><td align = "left">'.$results[0]['ExamDate'].'</td><td align = "left">'.$results[0]['session'].'</td></tr>';
							      $tabledata.="</table></fieldset>";
					   
					      		
					}
	  	  }
	  }
 	else if($lintfrom ==1){	
 		  $CompanyStudentsresult =$this->lobjPaypalentryreportModel->fngetcompanysttudentsdetails($lintIdapp);
 		  if($lintmdp == 1){
			  $result =$this->lobjPaypalentryreportModel->fngetfpxcompanydetails($lintIdapp);	  
	 		  foreach($result as $lobjCountry){ 
				  			 $tabledata.= '<fieldset><legend align = "left"> Company Registration - FPX</legend>
				  			 <br><fieldset><legend align = "left"> Company Details </legend>			
					                    <table class="table" border=1 align = "center" width=100%>
					                    <tr>
					                    	<th><b>Company Name</b></th><th><b>Short Name</b></th><th><b>Email</b></th><th><b>Registration Pin</b></th><th><b>No. Of Candidates</b></th>
					                    </tr><tr>';
						  $tabledata.= '<td align = "left">'.$lobjCountry['CompanyName'].'</td><td align = "left">'.$lobjCountry['ShortName'].'</td>
						  <td align = "left">'.$lobjCountry['Email'].'</td><td>'.$lobjCountry['registrationPin'].'</td><td>'.$lobjCountry['totalNoofCandidates'].'</td></tr>';
					      $tabledata.="</table></fieldset><br><br>";
					      $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
					         		   <table class='table' border=1 align='center' width=100%>
					         		   <tr><th><b>Order No.</b></th><th><b>Transaction Id</b></th><th><b>Bank</b></th><th><b>Branch</b></th><th><b>Amount</b></th><th><b>Payer Mail Id</b></th><th><b>Payment Date</b></th>
					         		   <th><b>Approved Date</b></th></tr><tr>";
					      $tabledata.="<td align = 'left'>".$lobjCountry['orderNumber']."</td>
					      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
					      		<td align = 'left'>".$lobjCountry['bankCode']."</td>
					      		<td align = 'left'>";						      
						      if($lobjCountry['BankName'])$tabledata.= $lobjCountry['bankCode']; else $tabledata.="NA";						      
						     $tabledata.="</td>
					      		<td align = 'left'>";						      
						      if($lobjCountry['BankName'])$tabledata.= $lobjCountry['BankName']; else $tabledata.="NA";						      
						     $tabledata.="</td>
					      		
					      		<td align = 'left'>".$lobjCountry['payerId']."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['ApprovedDate']))."</td></tr>";
					      $tabledata.="</table></fieldset>";					      		
					}
 		  }
 		   else if($lintmdp == 2){
	 		    $result =$this->lobjPaypalentryreportModel->fngetcompanydetails($lintIdapp);	 
		 		 foreach($result as $lobjCountry){ 
				  			 $tabledata.= '<fieldset><legend align = "left"> Company Registration - Credit Card</legend>
				  			 <br><fieldset><legend align = "left"> Company Details </legend>			
					                    <table class="table" border=1 align = "center" width=100%>
					                    <tr>
					                    	<th><b>Company Name</b></th><th><b>Short Name</b></th><th><b>Email</b></th><th><b>Registration Pin</b></th><th><b>No. Of Candidates</b></th>
					                    </tr><tr>';
						  $tabledata.= '<td align = "left">'.$lobjCountry['CompanyName'].'</td><td align = "left">'.$lobjCountry['ShortName'].'</td>
						  <td align = "left">'.$lobjCountry['Email'].'</td><td>'.$lobjCountry['registrationPin'].'</td><td>'.$lobjCountry['totalNoofCandidates'].'</td></tr>';
					      $tabledata.="</table></fieldset><br><br>";
					      $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
					         		   <table class='table' border=1 align='center' width=100%>
					         		   <tr><th><b>Reference No.</b></th><th><b>Transaction Id</b></th><th><b>Amount</b></th><th><b>Verify Sign</b></th><th><b>Payer Mail Id</b></th><th><b>Payment Date</b></th>
					         		   <th><b>Approved Date</b></th></tr><tr>";
					      $tabledata.="<td align = 'left'>".$lintIdapp."</td>
					      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
					      		<td align = 'right'>".$lobjCountry['grossAmount']."</td>
					      		<td align = 'left'>".$lobjCountry['verifySign']."</td>
					      		<td align = 'left'>".$lobjCountry['payerId']."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['ApprovedDate']))."</td></tr>";
					      $tabledata.="</table></fieldset>";
					      		
					}
 		   }
 		else {
 			
	  	  	$result = $this->lobjPaypalentryreportModel->fngetcompanyotherpaymentdetails($lintIdapp);
	  	  	  foreach($result as $lobjCountry){ 
				  			 $tabledata.= '<fieldset><legend align = "left"> Company Registration - ';
				  	  if ($lobjCountry['ModeofPayment'] == 1) $tabledata.="FPX"; else if($lobjCountry['ModeofPayment'] == 2) $tabledata.= "Credit Card";else if($lobjCountry['ModeofPayment'] == 4) $tabledata.= "Cheque";else if($lobjCountry['ModeofPayment'] == 5) $tabledata.= "Money Order";else if($lobjCountry['ModeofPayment'] == 6) $tabledata.= "Postal Order";else if($lobjCountry['ModeofPayment'] == 7) $tabledata.= "Credit/Bank to IBFIM account";else if($lobjCountry['ModeofPayment'] == 181) $tabledata.= "Pay Later";
						     	   
				  		  $tabledata.='</legend>
				  			 <br><fieldset><legend align = "left"> Company Details </legend>			
					                    <table class="table" border=1 align = "center" width=100%>
					                    <tr>
					                    	<th><b>Company Name</b></th><th><b>Short Name</b></th><th><b>Email</b></th><th><b>Registration Pin</b></th><th><b>No. Of Candidates</b></th>
					                    </tr><tr>';
						  $tabledata.= '<td align = "left">'.$lobjCountry['CompanyName'].'</td><td align = "left">'.$lobjCountry['ShortName'].'</td>
						  <td align = "left">'.$lobjCountry['Email'].'</td><td>'.$lobjCountry['registrationPin'].'</td><td>'.$lobjCountry['totalNoofCandidates'].'</td></tr>';
					      $tabledata.="</table></fieldset><br><br>";
					      				      		      	if($lintmdp != 181)
	 			{
					 	 $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
						         		   <table class='table' border=1 align='center' width=100%>
						         		   <tr><th><b>Reference No.</b></th><th><b>Transaction Id/Cheque No.</b></th><th><b>Amount</b></th><th><b>Bank Name</b></th><th><b>Mode Of Payment</b></th><th><b>Payment Date</b></th>
						         		    </tr><tr>";
						      $tabledata.="<td align = 'left'>".$lobjCountry['idBatchRegistration']."</td>
						      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
						      		<td align = 'right'>".$lobjCountry['grossAmount']."</td>
						      		<td align = 'left'>";						      
						      if($lobjCountry['BankName'])$tabledata.= $lobjCountry['BankName']; else $tabledata.="NA";						      
						     $tabledata.="</td>
						      		<td align = 'left'>";
						       if ($lobjCountry['ModeofPayment'] == 1) $tabledata.="FPX"; else if($lobjCountry['ModeofPayment'] == 2) $tabledata.= "Credit Card";else if($lobjCountry['ModeofPayment'] == 4) $tabledata.= "Cheque";else if($lobjCountry['ModeofPayment'] == 5) $tabledata.= "Money Order";else if($lobjCountry['ModeofPayment'] == 6) $tabledata.= "Postal Order";else if($lobjCountry['ModeofPayment'] == 7) $tabledata.= "Credit/Bank to IBFIM account";else if($lobjCountry['ModeofPayment'] == 181) $tabledata.= "Pay Later";
						    $tabledata.="</td>
						      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
						      		</tr>";
						    $tabledata.="</table></fieldset>";
	 			}
	  	  	  }    
	  	  }
 			if(count($CompanyStudentsresult)>0){
	  	  	 $tabledata.= '<br><fieldset><legend align = "left">Candidates Details </legend>
							                    <table class="table" border=1 align = "center" width=100%><tr><th><b>Student Name</b></th><th><b>IC No.</b></th><th><b>Program</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th></tr>';
	  	  	 for($lvars=0;$lvars<count($CompanyStudentsresult);$lvars++)
								  $tabledata.= '<tr><td align = "left">'.$CompanyStudentsresult[$lvars]['FName'].'</td><td align = "left">'.$CompanyStudentsresult[$lvars]['ICNO'].'</td><td align = "left">'.$CompanyStudentsresult[$lvars]['ProgramName'].'</td><td align = "left">'.$CompanyStudentsresult[$lvars]['centername'].'</td><td align = "left">'.date('d-m-Y',strtotime($CompanyStudentsresult[$lvars]['DateTime'])).'</td><td align = "left">'.$CompanyStudentsresult[$lvars]['managesessionname'].'('.$CompanyStudentsresult[$lvars]['starttime'].'-'.$CompanyStudentsresult[$lvars]['endtime'].')</td></tr>';
							      $tabledata.="</table></fieldset>";
	  	  	}
	  }
	 else if($lintfrom ==2){
	 	 $CompanyStudentsresult =$this->lobjPaypalentryreportModel->fngetcompanysttudentsdetails($lintIdapp);
	 	  if($lintmdp == 1){	
				  $result = $this->lobjPaypalentryreportModel->fngetfpxtakafuldetails($lintIdapp);	  
				  //echo "<pre>";
				  //print_r($result);exit;
				  foreach($result as $lobjCountry){ 
				  			 $tabledata.= '<fieldset><legend align = "left"> Takaful Registration - FPX</legend>
				  			 		<br><fieldset><legend align = "left"> Takaful Details </legend>			
					                    <table class="table" border=1 align = "center" width=100%>
					                    <tr>
					                    	<th><b>Takaful Name</b></th><th><b>Short Name</b></th><th><b>Email</b></th><th><b>Registration Pin</b></th><th><b>No. Of Candidates</b></th>
					                    </tr><tr>';
						  $tabledata.= '<td align = "left">'.$lobjCountry['TakafulName'].'</td><td align = "left">'.$lobjCountry['TakafulShortName'].'</td>
						  <td align = "left">'.$lobjCountry['email'].'</td><td>'.$lobjCountry['registrationPin'].'</td><td>'.$lobjCountry['totalNoofCandidates'].'</td></tr>';
					      $tabledata.="</table></fieldset><br><br>";
					      $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
					         		   <table class='table' border=1 align='center' width=100%>
					         		   <tr><th><b>Order No.</b></th><th><b>Transaction Id</b></th><th><b>Bank</b></th><th><b>Branch</b></th><th><b>Amount</b></th><th><b>Payer Mail Id</b></th><th><b>Payment Date</b></th>
					         		   <th><b>Approved Date</b></th></tr><tr>";
					      $tabledata.="<td align = 'left'>".$lobjCountry['orderNumber']."</td>
					      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
					      		<td align = 'left'>";						      
						      if($lobjCountry['BankName'])$tabledata.= $lobjCountry['bankCode']; else $tabledata.="NA";						      
						     $tabledata.="</td>
					      		<td align = 'left'>";						      
						      if($lobjCountry['BankName'])$tabledata.= $lobjCountry['BankName']; else $tabledata.="NA";						      
						     $tabledata.="</td>
					      		<td align = 'right'>".$lobjCountry['grossAmount']."</td>
					      		
					      		<td align = 'left'>".$lobjCountry['payerId']."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
					      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['ApprovedDate']))."</td></tr>";
					      $tabledata.="</table></fieldset>";
					      		
					}
	 	  	}
	 	  	 else if($lintmdp == 2){
			 	  	 $result = $this->lobjPaypalentryreportModel->fngettakafuldetails($lintIdapp);	  
					  //echo "<pre>";
					  //print_r($result);exit;
					  foreach($result as $lobjCountry){ 
					  			 $tabledata.= '<fieldset><legend align = "left"> Takaful Registration - Credit Card</legend>
					  			 <br><fieldset><legend align = "left"> Takaful Details </legend>			
						                    <table class="table" border=1 align = "center" width=100%>
						                    <tr>
						                    	<th><b>Takaful Name</b></th><th><b>Short Name</b></th><th><b>Email</b></th><th><b>Registration Pin</b></th><th><b>No. Of Candidates</b></th>
						                    </tr><tr>';
							  $tabledata.= '<td align = "left">'.$lobjCountry['TakafulName'].'</td><td align = "left">'.$lobjCountry['TakafulShortName'].'</td>
							  <td align = "left">'.$lobjCountry['email'].'</td><td>'.$lobjCountry['registrationPin'].'</td><td>'.$lobjCountry['totalNoofCandidates'].'</td></tr>';
						      $tabledata.="</table></fieldset><br><br>";
						      $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
						         		   <table class='table' border=1 align='center' width=100%>
						         		   <tr><th><b>Reference No.</b></th><th><b>Transaction Id</b></th><th><b>Amount</b></th><th><b>Verify Sign</b></th><th><b>Payer Mail Id</b></th><th><b>Payment Date</b></th>
						         		   <th><b>Approved Date</b></th></tr><tr>";
						      $tabledata.="<td align = 'left'>".$lintIdapp."</td>
						      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
						      		<td align = 'right'>".$lobjCountry['grossAmount']."</td>
						      		<td align = 'left'>".$lobjCountry['verifySign']."</td>
						      		<td align = 'left'>".$lobjCountry['payerId']."</td>
						      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
						      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['ApprovedDate']))."</td></tr>";
						      $tabledata.="</table></fieldset>";
						      		
						}
	 	  	 }
	 		else {
	 			if($lintmdp == 181)
	 			{
	 				$result = $this->lobjPaypalentryreportModel->fngettakafulpayleterpaymentdetails($lintIdapp);
	 			}
	 		else 
	 	   {
	  	  		$result = $this->lobjPaypalentryreportModel->fngettakafulotherpaymentdetails($lintIdapp);
	 		}
	 			 foreach($result as $lobjCountry){
					  			 $tabledata.= '<fieldset><legend align = "left"> Takaful Registration - ';
				  	  if ($lobjCountry['ModeofPayment'] == 1) $tabledata.="FPX"; else if($lobjCountry['ModeofPayment'] == 2) $tabledata.= "Credit Card";else if($lobjCountry['ModeofPayment'] == 4) $tabledata.= "Cheque";else if($lobjCountry['ModeofPayment'] == 5) $tabledata.= "Money Order";else if($lobjCountry['ModeofPayment'] == 6) $tabledata.= "Postal Order";else if($lobjCountry['ModeofPayment'] == 7) $tabledata.= "Credit/Bank to IBFIM account";else if($lobjCountry['ModeofPayment'] == 181) $tabledata.= "Pay Later";
						     	   
				  		  $tabledata.='</legend>
					  			 <br><fieldset><legend align = "left"> Takaful Details </legend>			
						                    <table class="table" border=1 align = "center" width=100%>
						                    <tr>
						                    	<th><b>Takaful Name</b></th><th><b>Short Name</b></th><th><b>Email</b></th><th><b>Registration Pin</b></th><th><b>No. Of Candidates</b></th>
						                    </tr><tr>';
							  $tabledata.= '<td align = "left">'.$lobjCountry['TakafulName'].'</td><td align = "left">'.$lobjCountry['TakafulShortName'].'</td>
							  <td align = "left">'.$lobjCountry['email'].'</td><td>'.$lobjCountry['registrationPin'].'</td><td>'.$lobjCountry['totalNoofCandidates'].'</td></tr>';
						      $tabledata.="</table></fieldset><br><br>";
						      		      	if($lintmdp != 181)
	 			{
						      $tabledata.="<fieldset><legend align = 'left'> Payment Details </legend>
						         		   <table class='table' border=1 align='center' width=100%>
						         		   <tr><th><b>Reference No.</b></th><th><b>Transaction Id/Cheque No.</b></th><th><b>Amount</b></th><th><b>Bank Name</b></th><th><b>Mode Of Payment</b></th><th><b>Payment Date</b></th>
						         		    </tr><tr>";
						      $tabledata.="<td align = 'left'>".$lobjCountry['idBatchRegistration']."</td>
						      		<td align = 'left'>".$lobjCountry['transactionId']."</td>
						      		<td align = 'right'>".$lobjCountry['grossAmount']."</td>
						      		<td align = 'left'>";						      
						      if($lobjCountry['BankName'])$tabledata.= $lobjCountry['BankName']; else $tabledata.="NA";						      
						     $tabledata.="</td>
						      		<td align = 'left'>";
						       if ($lobjCountry['ModeofPayment'] == 1) $tabledata.="FPX"; else if($lobjCountry['ModeofPayment'] == 2) $tabledata.= "Credit Card";else if($lobjCountry['ModeofPayment'] == 4) $tabledata.= "Cheque";else if($lobjCountry['ModeofPayment'] == 5) $tabledata.= "Money Order";else if($lobjCountry['ModeofPayment'] == 6) $tabledata.= "Postal Order";else if($lobjCountry['ModeofPayment'] == 7) $tabledata.= "Credit/Bank to IBFIM account";else if($lobjCountry['ModeofPayment'] == 181) $tabledata.= "Pay Later";
						    $tabledata.="</td>
						      		<td align = 'left'>".date('d-m-Y',strtotime($lobjCountry['PaymentDate']))."</td>
						      		</tr>";
						    $tabledata.="</table></fieldset>";
	 			}
						      		
						}
	  	  	}
	  	  	if(count($CompanyStudentsresult)>0){
	  	  	 $tabledata.= '<br><fieldset><legend align = "left">Candidates Details </legend>
							                    <table class="table" border=1 align = "center" width=100%><tr><th><b>Student Name</b></th><th><b>IC No.</b></th><th><b>Program</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th></tr>';
	  	  	 for($lvars=0;$lvars<count($CompanyStudentsresult);$lvars++)
								  $tabledata.= '<tr><td align = "left">'.$CompanyStudentsresult[$lvars]['FName'].'</td><td align = "left">'.$CompanyStudentsresult[$lvars]['ICNO'].'</td><td align = "left">'.$CompanyStudentsresult[$lvars]['ProgramName'].'</td><td align = "left">'.$CompanyStudentsresult[$lvars]['centername'].'</td><td align = "left">'.date('d-m-Y',strtotime($CompanyStudentsresult[$lvars]['DateTime'])).'</td><td align = "left">'.$CompanyStudentsresult[$lvars]['managesessionname'].'('.$CompanyStudentsresult[$lvars]['starttime'].'-'.$CompanyStudentsresult[$lvars]['endtime'].')</td></tr>';
							      $tabledata.="</table></fieldset>";
	  	  	}
		 }
		$tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'>&nbsp;&nbsp;</td></tr></fieldset>";	
		echo  $tabledata; 
	}

	public function pdfexportAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//$larrformdata = array();
		$fdate = $larrformData['Date3'] = $this->_getParam('fromdate');
		$tdate = $larrformData['Date4'] = $this->_getParam('todate');
		$frmdate =date('d-m-Y',strtotime($fdate));
		$todate =date('d-m-Y',strtotime($tdate));
		$lvarpaymenttype = $larrformData['Coursename'] = $this->_getParam('Coursename');
		$lvartype = $larrformData['Venues'] = $this->_getParam('venue');		
		$lstrreportytpe = $this->_getParam('reporttype');
		
		$result1 = array();
				$result2 = array();
				$result3 = array();
				if($lvartype == "" || $lvartype == 1){//Indidual 
					$result11 = array();
					$result12 = array();
					$result13 = array();
					if($lvarpaymenttype == "" || $lvarpaymenttype == 1){	// FPX				
						$result11 = $this->lobjPaypalentryreportModel->fngetstudentfpxentrydetails($larrformData); //searching the values for the student
					}
					if($lvarpaymenttype == "" || $lvarpaymenttype == 2){	//CreditCard				
						$result12 = $this->lobjPaypalentryreportModel->fngetstudentpaypalentrydetails($larrformData); //searching the values for the student
					}
					if($lvarpaymenttype == "" || ($lvarpaymenttype != 1 && $lvarpaymenttype != 2)){	// Other Payments				
						$result13 = $this->lobjPaypalentryreportModel->fngetstudentpaymententrydetails($larrformData); //searching the values for the student
					}
				
					$result1 = array_merge($result11,$result12,$result13);					
				}
				if($lvartype == "" || $lvartype == 2){//Company 
					$result21 = array();
					$result22 = array();
					$result23 = array();
					$result55= array();
					if($lvarpaymenttype == "" || $lvarpaymenttype == 1){	// FPX
						$result21 = $this->lobjPaypalentryreportModel->fngetcompanypfxentrydetails($larrformData); //searching the values for the company
					}
					if($lvarpaymenttype == "" || $lvarpaymenttype == 2){	//CreditCard
						$result22 = $this->lobjPaypalentryreportModel->fngetcompanypaypalentrydetails($larrformData); //searching the values for the company
					}
					/*if($lvarpaymenttype == "" || ($lvarpaymenttype != 1 && $lvarpaymenttype != 2)){	// Other Payments	
						$result23 = $this->lobjPaypalentryreportModel->fngetcompanypaymententrydetails($larrformData); //searching the values for the company	
					}*/
					if($lvarpaymenttype == "" || $lvarpaymenttype == 181) {		//paylater		
						$result55 = $this->lobjPaypalentryreportModel->fngetcompanypaymentpaylaterdetails($larrformData); //searching the values for the takaful
					}
					if($lvarpaymenttype == "" ||  $lvarpaymenttype == 4 || $lvarpaymenttype == 7 || $lvarpaymenttype == 5 || $lvarpaymenttype == 6 ){	// Other Payments	
						$result23 = $this->lobjPaypalentryreportModel->fngettakafulpaymententrydetails($larrformData); //searching the values for the company	
					}
					
					$result2 = array_merge($result21,$result22,$result23,$result55);				
				}		
				if($lvartype == "" || $lvartype == 3){//Takaful 
					$result31 = array();
					$result32 = array();
					$result33 = array();
					$result44 = array();
					
					if($lvarpaymenttype == "" || $lvarpaymenttype == 1){	// FPX				
						$result31 = $this->lobjPaypalentryreportModel->fngettakafulfpxentrydetails($larrformData); //searching the values for the takaful
					}
					if($lvarpaymenttype == "" || $lvarpaymenttype == 2){		//CreditCard			
						$result32 = $this->lobjPaypalentryreportModel->fngettakafulpaypalentrydetails($larrformData); //searching the values for the takaful
					}
				if($lvarpaymenttype == "" || $lvarpaymenttype == 181) {		//paylater			
						$result44 = $this->lobjPaypalentryreportModel->fngettakafulpaylaterdetails($larrformData); //searching the values for the takaful
					}
					
					if($lvarpaymenttype == "" || $lvarpaymenttype == 4 || $lvarpaymenttype == 7 || $lvarpaymenttype == 5 || $lvarpaymenttype == 6 ){	// Other Payments	
						$result33 = $this->lobjPaypalentryreportModel->fngettakafulpaymententrydetails($larrformData); //searching the values for the takaful	
					}
					$result3 = array_merge($result31,$result32,$result33,$result44);
					//echo "<pre>";print_r($result3);die();
				}
								
			  $batchids="";
				  
				for($rq=0;$rq<count($result3);$rq++)
				{
					if($rq==0)
					{
						$batchids=$result3[$rq]['registrationPin'];
						$batchids="'$batchids'";
					}
					else 
					{
						$batchidsss=$result3[$rq]['registrationPin'];
 				   		$batchidsss="'$batchidsss'";
 				   		$batchids.=','.$batchidsss;
					}
				}
				$resulttq= $this->lobjPaypalentryreportModel->fnbatchregcnt($batchids);
				
				$result333=array();
				for($r=0;$r<count($resulttq);$r++)
				{
					$idpin=$resulttq[$r]['RegistrationPin'];
					$result333[$idpin]=$resulttq[$r]['regtotal'];
					
				}
				
				
				 $batchidscmp="";
					for($raq=0;$raq<count($result2);$raq++)
				{
					if($raq==0)
					{
						$batchidscmp=$result2[$raq]['registrationPin'];
						$batchidscmp="'$batchidscmp'";
					}
					else 
					{
						$batchidsss=$result2[$raq]['registrationPin'];
 				   		$batchidsss="'$batchidsss'";
 				   		$batchidscmp.=','.$batchidsss;
					}
				}
				
				$result222 = array();
				$resultcmp= $this->lobjPaypalentryreportModel->fnbatchregcnt($batchidscmp);
				//echo "<pre />";
				//print_r($resultcmp);die();
				for($r=0;$r<count($resultcmp);$r++)
				{
					$idpin=$resultcmp[$r]['RegistrationPin'];
					$result222[$idpin]=$resultcmp[$r]['regtotal'];
					
				}
				
				
				   
				   
	    $day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Payment_Receipt_Report_'.$frmdate.'_'.$todate;
		$ReportName = $this->view->translate( "Payment" ).' '.$this->view->translate( "Receipt" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br/><br/<br/><br/><br/><br/>';
		}
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 6><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 9><b> {$ReportName}</b></td>	
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b>Payment From</b></th>
							<th><b>Payment Mode</b></th>
							<th><b>Name</b></th>
							<th>Receipt Number</th>  
							<th><b>Registration Pin</b></th>
							<th><b>Total Candidates</b></th>
							<th><b>Amount</b></th>
							<th><b>GST</b></th>
							<th><b>Total Amount</b></th>
							<th><b>Transaction Id/Cheque No.</b></th>
							<th><b>Order Number</b></th>
							<th><b>Date Of Payment</b></th>
							
						</tr>';  
			
		
		 
				
				
				
				
			  if (count($result1)): 
        $cnt1 = 0;
        $sum1 = 0;
        $totalreg=0; $totalapplied=0; 
        $taxsum1 = 0;
		$totalsum1 = 0;
       foreach ($result1 as $lobjStudent ): 
       
        if($cnt == 0){$modeofpayment = $lobjStudent['ModeofPayment']; }
        if($modeofpayment != $lobjStudent['ModeofPayment']){
        	
        	
        	$tabledata.= '<tr>
      	<td></td>
     	<td ><b>Total</b></td>
     	<td ><b>'.$cnt.'</b></td>
     	<td></td>
     	<td ></td>
     	<td></td>
     	<td align="right"><b>'.number_format($sum[1],2).'</b></td>
     	<td align="right"><b>'.number_format($taxsum[1],2).'</b></td>
     	<td align="right"><b>'.number_format($totalsum[1],2).'</b></td>
     	<td></td>
     	<td></td>
     	<td></td>
     </tr>';
        	 $cnt = 0;   $sum1 = $sum1+$sum[1]; $sum[1] = 0; $taxsum1=$taxsum1+$taxsum[1];$taxsum[1]=0;$totalsum1 = $totalsum1+$totalsum[1];$totalsum[1]=0;
        }
        
        
    $tabledata.= '  
    <tr>
   		   <td><b>';
      if($cnt1 == 0)  $tabledata.= 'Indivdual Registration';
      $tabledata.= '</b></td> 
		   <td><b>';
       if($cnt == 0){ if ($lobjStudent['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
       else if($lobjStudent['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
       else if($lobjStudent['ModeofPayment'] == 4) $tabledata.= 'Cheque';
       else if($lobjStudent['ModeofPayment'] == 5) $tabledata.= 'Money Order';
       else if($lobjStudent['ModeofPayment'] == 6) $tabledata.= 'Postal Order';
       else if($lobjStudent['ModeofPayment'] == 7) $tabledata.= 'Credit/Bank to IBFIM account';
       else if($lobjStudent['ModeofPayment'] == 181) $tabledata.= 'Pay Later';
       
       }
       $tabledata.= '</b></td> 
		   <td>'.$lobjStudent['FName'].'</a></td> 
		   <td>NA</td> 
		   <td>'.$lobjStudent['Regid'].'</td> 
		   <td>NA</td> ';
		   if($lobjStudent['finalamount']== '80.00' || $lobjStudent['finalamount']== '75.00')
			   {
                $Tax=0;  
				
		       }
			   else
			   {
				$Tax=$this->escape($lobjStudent['finalamount'])-$this->escape($lobjStudent['grossAmount']);   
			   }
			   $FeesWithoutTax=$lobjStudent['grossAmount'];
			   $FeesWithTax=$FeesWithoutTax+$Tax;
		   $tabledata.='<td align="right">'.$FeesWithoutTax.'</td>';$sum[1]= $sum[1]+$FeesWithoutTax;
		   $tabledata.='<td align="right">'.$Tax.'</td>';$taxsum[1]=$taxsum[1]+$Tax;
		   $tabledata.='<td align="right">'.$FeesWithTax.'</td>';$totalsum[1]=$totalsum[1]+$FeesWithTax;
		   $tabledata.='<td>'.$lobjStudent['transactionId'].'</td> 	
		   <td>NA</td>	  
		   <td align="left">'.date('d-m-Y',strtotime($lobjStudent['PaymentDate'])).'</td> 
		  
        </tr> '; 
         //$sum[1]= $sum[1]+$lobjStudent['grossAmount'];    
         $modeofpayment = $lobjStudent['ModeofPayment'];
    				$cnt++; $cnt1++;
    	endforeach; 
      $tabledata.= '<tr ><td></td>';
      if ($modeofpayment == 1) 
      $tabledata.='<td><b>Total - FPX</b></td>'; 
      else if($modeofpayment == 2) 
      $tabledata.='<td><b>Total - Credit Card</b></td>';
      else if($modeofpayment == 4) 
      $tabledata.='<td><b>Total - Cheque</b></td>';
      else if($modeofpayment == 5)
      $tabledata.='<td><b>Total - Money Order</b></td>';
      else if($modeofpayment == 6) 
      $tabledata.='<td><b>Total - Postal Order</b></td>';
      else if($modeofpayment == 7) 
      $tabledata.='<td><b>Total - Credit/Bank to IBFIM account</b></td>';
      else if($modeofpayment == 181)
      $tabledata.='<td><b>Total - Pay Later</b></td>';
      $tabledata.='<td ><b>'.$cnt.'</b></td>
				   <td></td>
			       <td></td>
     	           <td></td>';
     	
     $tabledata.='<td align="left"><b>'.number_format($sum[1],2).'</b></td>';
     $sum1 = $sum1+$sum[1];
	 $tabledata.='<td align="left"><b>'.number_format($taxsum[1],2).'</b></td>';
	 $taxsum1 = $taxsum1+$taxsum[1];
	 $tabledata.='<td align="left"><b>'.number_format($totalsum[1],2).'</b></td>';
	 $totalsum1 = $totalsum1+$totalsum[1];
	 $tabledata.='<td></td>
				  <td></td>
				  <td></td>
     </tr>';
     //$sum1 = $sum1+$sum[1];
    $tabledata.= '  <tr>
      	<td colspan="12"><hr></td>
     	
     </tr>
      <tr >
      	<td></td>
     	<td ><b>Total Count</b></td>
     	<td ><b>'.$cnt1.'</b></td>
     	<td ></td>
     	<td></td>
     	<td></td>	
     	<td align="left"><b>'.number_format($sum1,2).'</b></td>
     	<td align="left"><b>'.number_format($taxsum1,2).'</b></td>
     	<td align="left"><b>'.number_format($totalsum1,2).'</b></td>
     	<td></td>
     	<td></td>
     	<td></td>
     </tr>
      <tr>
      	<td colspan="12"><hr></td>
     	
     </tr>';
      endif;
      $sum[2] = 0;$taxsum[2] = 0;$totalsum[2] = 0; if (count($result2)): $cnt = 0; $cnt2 = 0; 
        foreach ($result2 as $lobjCompany ): 
        
        if($cnt == 0){$modeofpayment = $lobjCompany['ModeofPayment']; }
        if($modeofpayment != $lobjCompany['ModeofPayment']){
        	
        	
        	$tabledata.= '<tr>
      	<td></td>
     	<td ><b>Total</b></td>
     	<td ><b>'.$cnt.'</b></td>
     	<td></td>
     	<td ></td>
     	<td></td>
     	<td align="left"><b>'.number_format($sum[2],2).'</b></td>
     	<td align="left"><b>'.number_format($taxsum[2],2).'</b></td>
     	<td align="left"><b>'.number_format($totalsum[2],2).'</b></td>
     	<td></td>
     	<td></td>
     	<td></td>
     </tr>';
        	 $cnt = 0;   $sum2 = $sum2+$sum[2]; $sum[2] = 0; $taxsum2 = $taxsum2+$taxsum[2]; $taxsum[2] = 0; $totalsum2 = $totalsum2+$totalsum[2]; $totalsum[2] = 0;
        }
        
        
   $tabledata.= ' <tr>
   		   <td><b>';
         if($cnt2 == 0)  $tabledata.= 'Company Registration';
         $tabledata.= '</b></td>
		   <td><b>';
         if($cnt == 0){ if ($lobjCompany['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		       else if($lobjCompany['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		       else if($lobjCompany['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		       else if($lobjCompany['ModeofPayment'] == 5) $tabledata.= 'Money Order';
		       else if($lobjCompany['ModeofPayment'] == 6) $tabledata.= 'Postal Order';
		       else if($lobjCompany['ModeofPayment'] == 7) $tabledata.= 'Credit/Bank to IBFIM account';
		       else if($lobjCompany['ModeofPayment'] == 181) $tabledata.= 'Pay Later';
		         }
         
                $tabledata.= ' </b></td>';
	            $tabledata.='<td>'.$lobjCompany['CompanyName'].'</td>';
	   if($lobjCompany['ReceiptNum'])
					{
                         $tabledata.='<td><b>'.$lobjCompany['ReceiptNum'].'</b></td>';
					}
					else
					{
						$tabledata.='<td><b>'.NA.'</b></td>';
					}
	   
		        $tabledata.='<td>'.$lobjCompany['registrationPin'].'</td>';
				$idpin=$lobjCompany['registrationPin']; if(empty($result222[$idpin])){$result222[$idpin]=0; } $totalreg=$totalreg+$result222[$idpin]; $totalapplied=$totalapplied+$lobjCompany['totalNoofCandidates'];
				$tabledata.='<td>'.$lobjCompany['totalNoofCandidates'].'</td>';
				$companyactualfees = $lobjCompany['grossAmount']-$lobjCompany['ServiceTax'];
				$tabledata.='<td>'.$companyactualfees.'</td>';$sum[2]= $sum[2]+$companyactualfees;
				$tabledata.='<td>'.$lobjCompany['ServiceTax'].'</td>'; $taxsum[2]= $taxsum[2]+$lobjCompany['ServiceTax'];
				$totalfees = $companyactualfees + $lobjCompany['ServiceTax'];
				$tabledata.='<td>'.$totalfees.'</td>';$totalsum[2]=$totalsum[2]+$totalfees; 
				$tabledata.='<td>'.NA.'</td>';
				$tabledata.='<td>'.NA.'</td><td align="left">'.date('d-m-Y',strtotime($lobjCompany['PaymentDate'])).'</td></tr>  ';  
				//$sum[2]= $sum[2]+$lobjCompany['grossAmount'];  
				$cnt++; $cnt2++; 
				$modeofpayment = $lobjCompany['ModeofPayment']; 
    endforeach; 
      $tabledata.= '<tr>
      	<td></td>
     	<td ><b>Total</b></td>
     	<td ><b>'.$cnt.'</b></td>
     	<td></td>
     	<td ></td>
     	<td></td>
     	<td align="left"><b>'.number_format($sum[2],2).'</b></td>
     	<td align="left"><b>'.number_format($taxsum[2],2).'</b></td>
     	<td align="left"><b>'.number_format($totalsum[2],2).'</b></td>
     	<td></td>
     	<td></td>
     	<td></td>
     </tr>';
      $sum2 = $sum2+$sum[2];$taxsum2 = $taxsum2+$taxsum[2];$totalsum2 = $totalsum2+$totalsum[2];
      $tabledata.= '<tr>
      	<td colspan="12"><hr></td>
     	
     </tr>
      <tr>
      	<td></td>
     	<td ><b>Total Count</b></td>
     	<td ><b>'.$cnt2.'</b></td>
     	<td ></td>
     	<td></td>
     	<td></td>
     	<td align="left"><b>'.number_format($sum2,2).'</b></td>
     	<td align="left"><b>'.number_format($taxsum2,2).'</b></td>
     	<td align="left"><b>'.number_format($totalsum2,2).'</b></td>
     	<td></td>
     	<td></td>
     	<td></td>
     </tr>
      <tr>
      	<td colspan="12"><hr></td>
     	
     </tr>';
     endif;
     
    
       $sum[3] = 0;$taxsum[3] = 0;$totalsum[3]=0; if (count($result3)): $cnt = 0; $cnt3=0;
       foreach ($result3 as $lobjTakaful ): 
        
         if($cnt == 0){$modeofpayment = $lobjTakaful['ModeofPayment']; }
        if($modeofpayment != $lobjTakaful['ModeofPayment']){
        	
        	
        	$tabledata.= '<tr>
      	<td></td>
     	<td ><b>Total</b></td>
     	<td ><b>'. $cnt.'</b></td>
     	<td></td>
     	<td ></td>
     	<td></td>
     	<td align="left"><b>'.number_format($sum[3],2).'</b></td>
     	<td align="left"><b>'.number_format($taxsum[3],2).'</b></td>
     	<td align="left"><b>'.number_format($totalsum[3],2).'</b></td>
     	<td></td>
     	<td></td>
     	<td></td>
     </tr>';
        $cnt = 0;   $sum3 = $sum3+$sum[3]; $sum[3] = 0; $taxsum3 = $taxsum3+$taxsum[3];$taxsum[3] = 0;$totalsum3 = $totalsum3+$totalsum[3];$totalsum[3] = 0;
        }
      
   $tabledata.= ' <tr>
   		   <td><b>';
   			 if($cnt3 == 0) $tabledata.= 'Takaful Registration';
   			 
   			 $tabledata.= '</b></td>
		   <td><b>';
	 if($cnt == 0){ if ($lobjTakaful['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		       else if($lobjTakaful['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		       else if($lobjTakaful['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		       else if($lobjTakaful['ModeofPayment'] == 5) $tabledata.= 'Money Order';
		       else if($lobjTakaful['ModeofPayment'] == 6) $tabledata.= 'Postal Order';
		       else if($lobjTakaful['ModeofPayment'] == 7) $tabledata.= 'Credit/Bank to IBFIM account';
		       else if($lobjTakaful['ModeofPayment'] == 181) $tabledata.= 'Pay Later';
		         }
		  $tabledata.= ' </b></td> 
		   <td>'.$lobjTakaful['TakafulName'].'</td>
		   <td>'.NA.'</td>
		   <td>'.$lobjTakaful['registrationPin'].'</td>';
		  $idpin=$lobjTakaful['registrationPin']; if(empty($result333[$idpin])){$result333[$idpin]=0; } $totalreg=$totalreg+$result333[$idpin]; $totalapplied=$totalapplied+$lobjTakaful['totalNoofCandidates']; 
  $tabledata.= '<td>'.$lobjTakaful['totalNoofCandidates'].'</td>';
		  $actualfees = $lobjTakaful['grossAmount']-$lobjTakaful['ServiceTax'];
		  $tabledata.='<td><b>'.$actualfees.'</b></td>'; $sum[3]= $sum[3]+$actualfees;
		  $tabledata.='<td><b>'.$lobjTakaful['ServiceTax'].'</b></td>'; $taxsum[3]=$taxsum[3]+$lobjTakaful['ServiceTax'];
		  $takafultotalfees = $actualfees + $lobjTakaful['ServiceTax'];
		  $tabledata.='<td><b>'.$takafultotalfees.'</b></td>'; $totalsum[3]=$totalsum[3]+$takafultotalfees;
		  $tabledata.='<td>'.NA.'</td>
		               <td>'.NA.'</td>
					   <td align="left">'.date('d-m-Y',strtotime($lobjTakaful['PaymentDate'])).'</td> 
		  
		   
        </tr>  ';  
     $cnt++;  $cnt3++; $modeofpayment = $lobjTakaful['ModeofPayment']; endforeach; 
    $tabledata.= '  <tr>
     	<td></td>
     	<td ><b>Total</b></td>
     	<td ><b>'.$cnt.'</b></td>
     	<td></td>
     	<td ></td>
     	<td></td>
     	<td align="left"><b>'.number_format($sum[3],2).'</b></td>
     	<td align="left"><b>'.number_format($taxsum[3],2).'</b></td>	
     	<td align="left"><b>'.number_format($totalsum[3],2).'</b></td>
     	<td></td>
     	<td></td>
     	<td></td>
     </tr>
             <tr>
      	<td colspan="12"><hr></td>
     	
     </tr>';
    $sum3 = $sum3+$sum[3];$taxsum3 = $taxsum3+$taxsum[3];$totalsum3 = $totalsum3+$totalsum[3];
   $tabledata.= '   <tr>
      	<td></td>
     	<td ><b>Total Count</b></td>
     	<td ><b>'.$cnt3.'</b></td>
     	<td ></td>
     	<td></td>
     	<td></td>
     	<td align="left"><b>'.number_format($sum3,2).'</b></td>
     	<td align="left"><b>'.number_format($taxsum3,2).'</b></td>	
     	<td align="left"><b>'.number_format($totalsum3,2).'</b></td>
     	<td></td>
     	<td></td>
     	<td></td>
     </tr>
      <tr>
      	<td colspan="12"><hr></td>
     	
     </tr>';
     endif;
    $tolc = $cnt1+$cnt2+$cnt3;
    $tols =  $sum1+$sum2+$sum3;
	$toltax = $taxsum1+$taxsum2+$taxsum3;
	$toltacharge = $totalsum1+$totalsum2+$totalsum3;
      $tabledata.= '  <tr>
      	<td></td>
     	<td ><b>Grand Total</b></td>
     	<td ><b>'.$tolc.'</b></td>
     	<td >'.$totalapplied.'</td>
     	<td></td>
     	<td><b>'.$totalapplied.'</b></td>
     	<td align="left"><b>'.number_format($tols,2).'</b></td>
     	<td align="left"><b>'.number_format($toltax,2).'</b></td>	
     	<td align="left"><b>'.number_format($toltacharge,2).'</b></td>
     	<td></td>
     	<td></td>
     	<td></td>
     </tr></table>';
		
		
		
		
		
		
		
		
	if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/mpdf60/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->autoLangToFont  = true;
			$mpdf->autoScriptToLang = true;
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
			/*	$result1 = array();
				$result2 = array();
				$result3 = array();
				if($lvartype == "" || $lvartype == 1){
					$result1 = $this->lobjPaypalentryreportModel->fngetstudentfpxentrydetails($larrformData); //searching the values for the student
				}
				if($lvartype == "" || $lvartype == 2){
					$result2 = $this->lobjPaypalentryreportModel->fngetcompanypfxentrydetails($larrformData); //searching the values for the company	
				}		
				if($lvartype == "" || $lvartype == 3){
					$result3 = $this->lobjPaypalentryreportModel->fngettakafulfpxentrydetails($larrformData); //searching the values for the takaful
				}				
		
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'FPX_Reciept_Report_'.$frmdate.'_'.$todate;
		$ReportName = $this->view->translate( "FPX" ).' '.$this->view->translate( "Reciept" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
		}
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 5><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 8><b> {$ReportName}</b></td>	
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b>Payment From</b></th>
							<th><b>Name</b></th>
							<th><b>Order No</b></th>
							<th><b>Registration Pin</b></th>
							<th><b>Amount</b></th>
							<th><b>Transaction Id</b></th>
							<th><b>Date Of Payment</b></th>
							<th><b>Approved Date</b></th>
						</tr>';     
	 	$sum[1] = 0; 
		if (count($result1)){
			 $cnt = 0; 
      		  foreach ($result1 as $lobjStudent ){
      		  	  //$tabledata.= ' <tr>';
				  //if($cnt == 0)  $tabledata.= '<td rowspan="'.count($result1).'"><b>Indivdual Registration</b></td> ';
				   $tabledata.= ' <tr>
				   		   <td><b>'; 
				   if($cnt == 0) $tabledata.= 'Indivdual Registration';
      		      $tabledata.= '</b></td>
					       <td>'.$lobjStudent['FName'].'</td> 
						   <td>'.$lobjStudent['orderNumber'].'</td> 
						   <td>'.$lobjStudent['Regid'].'</td> 
						   <td>'.$lobjStudent['grossAmount'].'</td> 
						   <td>'.$lobjStudent['transactionId'].'</td> 		  
						   <td>'.date('d-m-Y',strtotime($lobjStudent['PaymentDate'])).'</td> 
						   <td>'.date('d-m-Y',strtotime($lobjStudent['ApprovedDate'])).'</td> 		   
				        </tr> ';
				   			$sum[1]= $sum[1]+$lobjStudent['grossAmount'];   
	    	$cnt++; 
      		  }
	      $tabledata.= '<tr>	      	
	     	<td colspan="2" align="center"><b>Total Count</b></td>
	     	<td><b>'.$cnt.'</b></td>
	     	<td></td>
	     	<td><b>'.$sum[1].'</b></td>
	     	<td></td>
	     	<td></td>
	     	<td></td>
	     </tr>';
		 }		
		$sum[2] = 0; 
		if (count($result2)){
			 $cnt = 0; 
      		  foreach ($result2 as $lobjStudent ){
      		    // if($cnt == 0)  $tabledata.= '<td rowspan="'.count($result2).'"><b>Comapny Registration</b></td> ';
      		  	// $tabledata.= ' <tr>';
				   $tabledata.= '<tr>
				   		   <td><b>'; 
				   if($cnt == 0) $tabledata.= 'Comapny Registration'; 
				   $tabledata.= '</b></td>			  
				   		   <td>'.$lobjStudent['CompanyName'].'</td> 
						   <td>'.$lobjStudent['orderNumber'].'</td> 
						   <td>'.$lobjStudent['registrationPin'].'</td> 
						   <td>'.$lobjStudent['grossAmount'].'</td> 
						   <td>'.$lobjStudent['transactionId'].'</td> 		  
						   <td>'.date('d-m-Y',strtotime($lobjStudent['PaymentDate'])).'</td> 
						   <td>'.date('d-m-Y',strtotime($lobjStudent['ApprovedDate'])).'</td> 		   
				        </tr> ';
				   			$sum[2]= $sum[2]+$lobjStudent['grossAmount'];   
	    			$cnt++; 
      		  }
	      $tabledata.= '<tr>	      	
	     	<td colspan="2" align="center"><b>Total Count</b></td>
	     	<td ><b>'.$cnt.'</b></td>
	     	<td></td>
	     	<td ><b>'.$sum[2].'</b></td>
	     	<td></td>
	     	<td></td>
	     	<td></td>
	     </tr>';
		 }
		
		$sum[3] = 0; 
		if (count($result3)){
			 $cnt = 0; 
      		  foreach ($result3 as $lobjStudent ){
      		  	//if($cnt == 0)  $tabledata.= '<tr><td rowspan="'.count($result3).'"><b>Takaful Registration</b></td> ';
				   $tabledata.= ' <tr>
				   		   <td><b>'; 
				   if($cnt == 0) $tabledata.= 'Takaful Registration'; 
				   $tabledata.= '</b></td> 
				
						   <td>'.$lobjStudent['TakafulName'].'</td> 
						   <td>'.$lobjStudent['orderNumber'].'</td> 
						   <td>'.$lobjStudent['registrationPin'].'</td> 
						   <td>'.$lobjStudent['grossAmount'].'</td> 
						   <td>'.$lobjStudent['transactionId'].'</td> 		  
						   <td>'.date('d-m-Y',strtotime($lobjStudent['PaymentDate'])).'</td> 
						   <td>'.date('d-m-Y',strtotime($lobjStudent['ApprovedDate'])).'</td> 		   
				        </tr> ';
				   			$sum[3]= $sum[3]+$lobjStudent['grossAmount'];   
	    	$cnt++; 
      		  }
	      $tabledata.= '<tr>
	      	
	     	<td colspan="2" align="center"><b>Total Count</b></td>
	     	<td><b>'.$cnt.'</b></td>
	     	<td></td>
	     	<td><b>'.$sum[3].'</b></td>
	     	<td></td>
	     	<td></td>
	     	<td></td>
	     </tr>
	      ';
		 }
		$totalsum = $sum[1]+$sum[2]+$sum[3];
		 $tabledata.= '<tr>
		      	
		     	<td colspan="4" align="center"><b>Grand Total</b></td>
		     	
		     	<td ><b>'.$totalsum.'</b></td>
		     	<td></td>
		     	<td></td>
		     	<td></td>
		     </tr>
		     </table>';	
		*/
		//echo $tabledata;exit;
		/*if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}*/
				
	}
}
