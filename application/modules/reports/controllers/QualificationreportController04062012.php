<?php
class Reports_QualificationreportController extends Base_Base {
	
	public function init() 
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjStasticsForm = new  Reports_Form_Statistics();  //creating object of Statistics form
		$this->lobjQualificationReportModel = new Reports_Model_DbTable_QualificationReport();		//creating object of QualificationReport model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
    
	public function indexAction() 
	{
		$this->view->lobjform = $this->lobjStasticsForm;
		$ldtdate=$this->_getParam ( 'ldtdate' );
		$this->view->Monthyear =$ldtdate;
		$this->lobjStasticsForm->Date->setValue($ldtdate);
		$this->lobjStasticsForm->Type->setValue(7);
		$larrdates =explode("-",$ldtdate);
		$lintYear=$larrdates[0];
		$lintmonth=$larrdates[1];
		$this->view->month1=$lintmonth;
		$lstrmonthname = date( 'F', mktime(0, 0, 0, $lintmonth));
		$this->view->month = $lstrmonthname;
		$this->view->Year = $lintYear;
		
		$larrqualificationcount = $this->lobjQualificationReportModel->fngetqualification($lintmonth,$lintYear); //Call a function to get number of candidates registered for different qualification
		$this->view->larrqualificationcount=$larrqualificationcount;
		$lintregisteredtotal=0;
        for($linti=0;$linti<count($larrqualificationcount);$linti++){
        	$lintqualificationid= $larrqualificationcount[$linti]['idDefinition'];
        	$larrquali[$linti]  =	$larrqualificationcount[$linti]['DefinitionDesc'];
        	$larrQualificationresult = $this->lobjQualificationReportModel->fngetqualificationresult($lintmonth,$lintYear,$lintqualificationid); //Call a function to get number of Diploma candidates details
        	$lintregisteredtotal += $larrqualificationcount[$linti]['NoOfCandidates'];
        	$lintcountQualification = count($larrQualificationresult);
        	
        	$lintreg[$linti]         = $larrqualificationcount[$linti]['NoOfCandidates'];        	
        	$lintsat[$linti] = $lintcountQualification;
        	$lintpass[$linti] = 0;
        	$lintfail[$linti] = 0;
        	$lintabsent[$linti]=0;
        	
        	for($lintj=0;$lintj<$lintcountQualification;$lintj++){
        		if($larrQualificationresult[$lintj]['pass']==1)
        		{
        			$lintpass[$linti]++;
        		}elseif ($larrQualificationresult[$lintj]['pass']==2){
        			$lintfail[$linti]++;
        		}else{
        			$lintabsent[$linti]++;
        		}
        	}
        	$lintabsent[$linti] = $larrqualificationcount[$linti]['NoOfCandidates']-$lintsat[$linti];
        } 
        
        $this->view->larrquali   =  $larrquali; //sending all count of candidates to the view
        $this->view->lintreg     =  $lintreg; //sending all count of candidates to the view
        $this->view->lintsat   =  $lintsat; //sending all count of candidates to the view
        $this->view->lintpass   =  $lintpass; //sending all count of candidates to the view
        $this->view->lintfail   =  $lintfail; //sending all count of candidates to the view
        $this->view->lintabsent   =  $lintabsent; //sending all count of candidates to the view
	}
	
	public function fnexportexcelAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		unset ( $larrformData ['ExportToExcel'] );
		$lday= date("d-m-Y");
		$ltime = date('h:i:s',time());
		$larrdates =explode("-",$larrformData['selmonth']);
		$lintmonth=$larrdates[0];
		$lintYear=$larrdates[1];
		$lstrmonthname = date( 'F', mktime(0, 0, 0, $lintmonth));
		$larrqualificationcount = $this->lobjQualificationReportModel->fngetqualification($lintmonth,$lintYear); //Call a function to get number of candidates registered for different qualification
		$this->view->larrqualificationcount=$larrqualificationcount;
		$lintregisteredtotal=0;
        for($linti=0;$linti<count($larrqualificationcount);$linti++){
        	$lintqualificationid= $larrqualificationcount[$linti]['idDefinition'];
        	$larrquali[$linti]  =	$larrqualificationcount[$linti]['DefinitionDesc'];
        	$larrQualificationresult = $this->lobjQualificationReportModel->fngetqualificationresult($lintmonth,$lintYear,$lintqualificationid); //Call a function to get number of Diploma candidates details
        	$lintregisteredtotal += $larrqualificationcount[$linti]['NoOfCandidates'];
        	$lintcountQualification = count($larrQualificationresult);
        	
        	$lintreg[$linti]         = $larrqualificationcount[$linti]['NoOfCandidates'];        	
        	$lintsat[$linti] = $lintcountQualification;
        	$lintpass[$linti] = 0;
        	$lintfail[$linti] = 0;
        	$lintabsent[$linti]=0;
        	
        	for($lintj=0;$lintj<$lintcountQualification;$lintj++){
        		if($larrQualificationresult[$lintj]['pass']==1)
        		{
        			$lintpass[$linti]++;
        		}elseif ($larrQualificationresult[$lintj]['pass']==2){
        			$lintfail[$linti]++;
        		}else{
        			$lintabsent[$linti]++;
        		}
        	}
        	$lintabsent[$linti] = $larrqualificationcount[$linti]['NoOfCandidates']-$lintsat[$linti];
        } 
		$host = $_SERVER['SERVER_NAME']; //Host address
		$imgp = "http://".$host."/tbefinal/images/reportheader.jpg"; //Image
		$filename = 'Qualification_Statistics_Report_'.$larrformData['selmonth'];//FileName
		$ReportName = $this->view->translate( "Qualification" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		$tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src="'.$imgp.'" /></td></tr></table><br><br><br><br>';
		$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 3><b>Date </b></td><td align ='left' colspan = 4><b>$lday</b></td><td align ='left' colspan = 3><b> Time</b></td><td align ='left' colspan = 4><b>$ltime</b></td></table>";
		$tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 14 align=center><b>{$ReportName}</b></td></tr></table><br>";
		$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 12><b>".$lstrmonthname.""._."".$lintYear."</b></td></tr>";
		
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Qualification</b></td>";
		for($linti=0;$linti<count($larrquali);$linti++) {
		$tabledata.="<td align ='center' colspan = 2><b>$larrquali[$linti]</b></td>";
		}
		$tabledata.="<td align ='center' colspan = 2><b>TOTAL</b></td></tr>";
		
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td>";
		$linttotalreg =0;
		for($linti=0;$linti<count($lintreg);$linti++) {
		$linttotalreg+=$lintreg[$linti];
		$tabledata.="<td align ='center' colspan = 2><b>$lintreg[$linti]</b></td>";
		}
		if($linttotalreg)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalreg</b></td></tr>";
		else 
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr>";
		
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td>";
		$linttotalsat =0;
		for($linti=0;$linti<count($lintsat);$linti++) {
		$linttotalsat+=$lintsat[$linti];
		$tabledata.="<td align ='center' colspan = 2><b>$lintsat[$linti]</b></td>";
		}
		if($linttotalsat)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalsat</b></td></tr>";
		else 
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr>";
		
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td>";
		$linttotalpass =0;
		for($linti=0;$linti<count($lintpass);$linti++) {
		$linttotalpass+=$lintpass[$linti];
		$tabledata.="<td align ='center' colspan = 2><b>$lintpass[$linti]</b></td>";
		}
		if($linttotalpass)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalpass</b></td></tr>";
		else 
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr>";
		
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td>";
		$linttotalfail =0;
		for($linti=0;$linti<count($lintfail);$linti++) {
		$linttotalfail+=$lintfail[$linti];
		$tabledata.="<td align ='center' colspan = 2><b>$lintfail[$linti]</b></td>";
		}
		if($linttotalreg)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalfail</b></td></tr>";
		else 
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr>";
		
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td>";
		$linttotalabsent =0;
		for($linti=0;$linti<count($lintabsent);$linti++) {
		$linttotalabsent+=$lintabsent[$linti];
		$tabledata.="<td align ='center' colspan = 2><b>$lintabsent[$linti]</b></td>";
		}
		if($linttotalreg)
		$tabledata.="<td align ='center' colspan = 2><b>$linttotalabsent</b></td></tr>";
		else 
		$tabledata.="<td align ='center' colspan = 2><b>0</b></td></tr>";
		
		$ourFileName = realpath('.')."/data"; 
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); //open a file to write a text
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));//write the content of htmlcode into text file
		fclose($ourFileHandle); //closeing a file 
		header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
		header("Content-Disposition: attachment; filename=$filename.xls");
		readfile($ourFileName);
		unlink($ourFileName);
	}
}