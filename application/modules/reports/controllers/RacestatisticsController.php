<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_RacestatisticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjStasticsForm = new  Reports_Form_Statistics();
		$this->lobjstastics = new Reports_Model_DbTable_Racestatistics();		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjStasticsForm;
		
		    /*if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {   
		    	$larrformData = $this->_request->getPost ();
				if ($this->lobjform->isValid($larrformData)) 
				{
				 $larrformData = $this->_request->getPost ();
				 $ldtdate = $larrformData['Date'];*/
		         $ldtdate = $this->_getParam('ldtdate');
				 $this->view->searchdate = $ldtdate;
				 $larrdates = explode('-',$ldtdate);
				 $lintmon = $larrdates['1'];
				 $lintyear = $larrdates['0'];
				 $lstrmonthname = date( 'F', mktime(0,0,0,$lintmon));
				 $lintmalayid = 166;$lintindanid = 165;$lintchineseid = 164;$lintothersid = 172;
				 $this->view->month = $lstrmonthname;
				 $this->view->year = $lintyear;
				 unset ( $larrformData ['Search'] );
				 $larrracescount = $this->lobjstastics->fngetraces($lintmon,$lintyear);
				 $lintregisteredtotal = $larrracescount[0]['NoOfCandidates']+$larrracescount[1]['NoOfCandidates']+$larrracescount[2]['NoOfCandidates']+$larrracescount[3]['NoOfCandidates'];
				 $this->view->regtotal = $lintregisteredtotal;
				 $larrmalayresult = $this->lobjstastics->fngetracesresult($lintmon,$lintyear,$lintmalayid);
				 $lintmalaycount = count($larrmalayresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent = 0;
				 for($linti=0;$linti<$lintmalaycount;$linti++){
				 	if($larrmalayresult[$linti]['pass']==1)
				 	{
				 		$lintpass++;
				 	}else{
				 		$lintfail++;
				 	}
				 }
				 $larrracescount[]['msat'] = $lintpass+$lintfail;
				 $larrracescount[]['mpass'] = $lintpass;
				 $larrracescount[]['mfail']= $lintfail;
				 $larrracescount[]['mabsent']= $larrracescount[2]['NoOfCandidates'] - ($lintpass+$lintfail) ;
				 $larrindianresult = $this->lobjstastics->fngetracesresult($lintmon,$lintyear,$lintindanid);
				 $lintcount = count($larrindianresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 for($linti=0;$linti<$lintcount;$linti++){
				 	if($larrindianresult[$linti]['pass']==1)
				    {
				 		$lintpass++;
				 	}else{
				 		$lintfail++;
				 	}
				 }
				 $larrracescount[]['isat'] = $lintpass+$lintfail;
				 $larrracescount[]['ipass'] = $lintpass;
				 $larrracescount[]['ifail']= $lintfail;
				 $larrracescount[]['iabsent']= $larrracescount[1]['NoOfCandidates'] - ($lintpass+$lintfail);
				 $larrchineseresult = $this->lobjstastics->fngetracesresult($lintmon,$lintyear,$lintchineseid);
				 $lintcount = count($larrchineseresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 for($linti=0;$linti<$lintcount;$linti++){
				 	if($larrchineseresult[$linti]['pass']==1)
				    {
				 		$lintpass++;
				 	}else{
				 		$lintfail++;
				 	}
				 }
				 $larrracescount[]['csat'] = $lintpass+$lintfail;
				 $larrracescount[]['cpass'] = $lintpass;
				 $larrracescount[]['cfail']= $lintfail;
				 $larrracescount[]['cabsent']= $larrracescount[0]['NoOfCandidates'] - ($lintpass+$lintfail);
				 $larrothersresult = $this->lobjstastics->fngetracesresult($lintmon,$lintyear,$lintothersid);
				 $lintcount = count($larrothersresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 for($linti=0;$linti<$lintcount;$linti++){
				 	if($larrothersresult[$linti]['pass']==1)
				    {
				 		$lintpass++;
				 	}else{
				 		$lintfail++;
				 	}
				 }
				 $larrracescount[]['osat'] = $lintpass+$lintfail;
				 $larrracescount[]['opass'] = $lintpass;
				 $larrracescount[]['ofail']= $lintfail;
				 $larrracescount[]['oabsent']= $larrracescount[3]['NoOfCandidates'] - ($lintpass+$lintfail);
				 $larrracescount[]['sattotal']=  $larrracescount[4]['msat']+$larrracescount[8]['isat']+$larrracescount[12]['csat']+$larrracescount[16]['osat'];
				 $larrracescount[]['passtotal']=  $larrracescount[5]['mpass']+$larrracescount[9]['ipass']+$larrracescount[13]['cpass']+$larrracescount[17]['opass'];
				 $larrracescount[]['failtotal']=  $larrracescount[6]['mfail']+$larrracescount[10]['ifail']+$larrracescount[14]['cfail']+$larrracescount[18]['ofail'];
				 $larrracescount[]['absenttotal']=  $lintregisteredtotal - ($larrracescount[4]['msat']+$larrracescount[8]['isat']+$larrracescount[12]['csat']+$larrracescount[16]['osat']);//$larrracescount[7]['mabsent']+$larrracescount[11]['iabsent']+$larrracescount[15]['cabsent']+$larrracescount[19]['oabsent'];
				 $this->view->larrracescount   =  $larrracescount;
			     $this->view->paginator = $larrracescount;
				 //$this->view->lobjform->populate($larrformData);
			//}
 	      //}
	}
			public function fnexportexcelAction()
				{    
					 $this->_helper->layout->disableLayout();
					 $this->_helper->viewRenderer->setNoRender();
					 $larrformData = $this->_request->getPost();
					 unset ( $larrformData ['ExportToExcel'] );
					 $lday= date("d-m-Y");
					 $ltime = date('h:i:s',time());
					 $host = $_SERVER['SERVER_NAME'];
					 $imgp = "http://".$host."/tbenew/images/reportheader.jpg";
					 $filename = 'Race_Statistics_Report_'.$larrformData['selmonth'];
					 $ReportName = $this->view->translate( "Race" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
					 $tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 3><b>Date </b></td><td align ='left' colspan = 3><b>$lday</b></td><td align ='left' colspan = 3><b> Time</b></td><td align ='left' colspan = 3><b>$ltime</b></td></tr></table>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 12 align=center><b>{$ReportName}</b></td></tr></table><br>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 10><b>".$larrformData['selmonth']."</b></td></tr>";
					 $tabledata.="<tr> <td align ='left' colspan = 2><b>Race</b></td><td align ='center' colspan = 2><b>Malay</b></td><td align ='center' colspan = 2><b>Indian</b></td><td align ='center' colspan = 2><b>Chinese</b></td><td align ='center' colspan = 2><b>Others</b></td><td align ='center' colspan = 2><b>Total</b></td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td><td align ='center' colspan = 2>".$larrformData['malaycand']."</td><td align ='center' colspan = 2>".$larrformData['Indiancand']."</td><td align ='center' colspan = 2>".$larrformData['chinesecand']."</td><td align ='center' colspan = 2>".$larrformData['otherscand']."</td><td align ='center' colspan = 2>".$larrformData['regtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td><td align ='center' colspan = 2>".$larrformData['msat']."</td><td align ='center' colspan = 2>".$larrformData['isat']."</td><td align ='center' colspan = 2>".$larrformData['csat']."</td><td align ='center' colspan = 2>".$larrformData['osat']."</td><td align ='center' colspan = 2>".$larrformData['sattotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td><td align ='center' colspan = 2>".$larrformData['mpass']."</td><td align ='center' colspan = 2>".$larrformData['ipass']."</td><td align ='center' colspan = 2>".$larrformData['cpass']."</td><td align ='center' colspan = 2>".$larrformData['opass']."</td><td align ='center' colspan = 2>".$larrformData['passtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td><td align ='center' colspan = 2>".$larrformData['mfail']."</td><td align ='center' colspan = 2>".$larrformData['ifail']."</td><td align ='center' colspan = 2>".$larrformData['cfail']."</td><td align ='center' colspan = 2>".$larrformData['ofail']."</td><td align ='center' colspan = 2>".$larrformData['failtotal']."</td></tr>";				
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td><td align ='center' colspan = 2>".$larrformData['mabsent']."</td><td align ='center' colspan = 2>".$larrformData['iabsent']."</td><td align ='center' colspan = 2>".$larrformData['cabsent']."</td><td align ='center' colspan = 2>".$larrformData['oabsent']."</td><td align ='center' colspan = 2>".$larrformData['absenttotal']."</td></tr>";				
					 $ourFileName = realpath('.')."/data";
					 $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
					 ini_set('max_execution_time', 3600);
					 fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
					 fclose($ourFileHandle);
					 header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
					 header("Content-Disposition: attachment; filename=$filename.xls");
					 header("Pragma: no-cache");
					 header("Expires: 0");
					 readfile($ourFileName);
					 unlink($ourFileName);
				}	
}	
 