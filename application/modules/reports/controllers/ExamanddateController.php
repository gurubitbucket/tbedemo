<?php
	error_reporting (E_ALL ^ E_WARNING);
    error_reporting (E_ALL ^ E_NOTICE);
class Reports_ExamanddateController extends Base_Base {
	
	private $_gobjlogger; 
	public function init() 
	{
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
    
	public function indexAction() 
	{
		$this->view->checkEmpty = 0;
		$lobjReportsForm = new  Reports_Form_Report();
		$lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$this->view->lobjform = $lobjReportsForm;		
		$larrcenters=$lobjExamreportModel->fngetcenternames();		
		$lobjReportsForm->Venues->addMultiOption('','Select'); 	
		$lobjReportsForm->Venues->addmultioptions($larrcenters);
		$larrcourses=$lobjExamreportModel->fngetprogramnames();		
		$lobjReportsForm->Coursename->addMultiOption('','Select'); 	
		$lobjReportsForm->Coursename->addmultioptions($larrcourses);
		
		$jsondata = '{
    				"label":"slno",
					"identifier":"uniq",
					"items":""
				  }';
		$this->view->jsondata = $jsondata;
 		
 		if($this->_request->isPost() && $this->_request->getPost('Generate')) 
 		{
 			$larrformData = $this->_request->getPost();
 			 /*$larrformData1 = $larrformData;
 			$larr= explode('-',$larrformData['Date']);
 			$larrformData['Year']=$larr[0];
 			$larrformData['Month']=$larr[1];
 			$larrformData['Day']=$larr[2];
 			
 			if($larrformData['Date2'])
 				 {
 					$larr2= explode('-',$larrformData['Date2']);
 					$larrformData['Year1']=$larr2[0];
 					$larrformData['Month1']=$larr2[1];
 					$larrformData['Day1']=$larr2[2];
 					$larrformData['flag1']=0;
 				 }*/
 			/*else {
 					$month= date("m"); // Month value
					$day= date("d"); //today's date
					$year= date("Y"); //current year
 					$larrformData['Year1']=$year;
 					$larrformData['Month1']=$month;
 					$larrformData['Day1']=$day;
 					//$larrformData['Day']=1;
 					
 					//$larrformData['flag1']=5;
 				 }*/
 			
 			//unset($larrformData['Date']);
 			//unset($larrformData['Date2']);
 			unset($larrformData['Generate']);
 			/*echo "<pre>";
 			print_r($larrformData);
 			die();*/
 				
 			if ($lobjReportsForm->isValid ( $larrformData )) 
 			{
 				$result = $lobjExamreportModel->fngetexamanddatedetails($larrformData);
 				$count=count($result);
 				/*echo "<pre>";
 			print_r($result);
 			die();*/
 				for($i=0;$i<$count;$i++){
 					$m=1;
 					$k=1;
 					$resarr= explode(',',$result[$i]['Result']);
 				    $count1=0;
 				    $count2=0;
 				  //$count3=0;
 				        for($j=0;$j<sizeof($resarr);$j++)
 				        {
 				        	if($resarr[$j]==1)
 				        	{
 				        		$count1++;
 				        	}
 				        	if($resarr[$j]==2)
 				        	{
 				        		$count2++;
 				        	}
 				            //if($resarr[$j]==3)
 				        	//{
 				        		//$count3++;
 				        	//}
 				        }
 				        
 					if($m==$k)
 				        	{
 				        		$resultedarray1[$i]=$count1;
 				        		$resultedarray2[$i]=$count2;
 				              //$resultedarray3[$i]=$count3;	
 				        	}
 				        	$m++;
 				        	$k++;
 				}
 			}
 			else 
 				{
 				 $this->_redirect( $this->baseUrl . 'reports/examanddate/index');	
 				}
 		       
 			for($s=0;$s<$count;$s++)
 				{
 					$result[$s]['slno']=$s+1;
 					$result[$s]['uniq']=$s;
 					$result[$s]['pass']=$resultedarray1[$s];
 					$result[$s]['fail']=$resultedarray2[$s];
 					$result[$s]['sitted']=$resultedarray1[$s]+$resultedarray2[$s];
 					$day= date("d-m-Y");
 					/*echo $day;
 					die();*/
 					if($result[$s]['Date']>=$day)
 					{
 						$result[$s]['absent'] = $result[$s]['applied']-$result[$s]['sitted'];//$result[$s]['absent']=0;
 					}
 					else 
 					{
 						$result[$s]['absent']=0;
 					}
 					}
 					/*echo "<pre>";
 			print_r($result);
 			die();*/
 			if($result) $this->view->checkEmpty = 1;	
			$page = $this->_getParam('page',1);
			$this->view->counter = (count($result));
			$this->view->lobjPaginator = $result;
			$jsonresult = Zend_Json_Encoder::encode($result);
    		$jsondata = '{
    						"label":"slno",
							"identifier":"uniq",
							"items":'.$jsonresult.
				  		'}';
			$this->view->jsondata = $jsondata;
	
				//$lobjReportsForm->populate($larrformData1);	
		}	

	  }
	
	public function generatereportAction()
	{
		$lobjReportsForm = new  Reports_Form_Report();
		$this->view->lobjform = $lobjReportsForm;
		//Check Whether the form is submitted
		if($this->_request->getPost())
		{
			$larrformData = $this->_request->getPost();
			$this->view->datacount = $larrformData['datacount'];
			$this->view->datacounttable = $larrformData['datacounttable'];
			 $auth = Zend_Auth::getInstance();
    	// Write Logs
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Generated the Report"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);
		}
		else
		{
			$this->_redirect( $this->baseUrl . 'reports/examanddate/index');
	    }
	   
	 }
	
	public function pdfexportAction()
	{
		//require_once 'FPDF/fpdf.php';
		//require_once 'html2pdf_v4.03/pdf.php';
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
		$htmldata = $larrformData['datacount'];
		$htmltabledata = $larrformData['datacounttable'];
		$CheckedValuesList = explode(",",$larrformData['checkedvalues']);
		
		if($larrformData['ReportType'] == "pdf"){
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img align=center width=100% src="../public/images/header.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                              '.'{PAGENO}{nbpg}' );
		//$mpdf->setFooter ( date ( "d-m-Y H:i:s" ) . "                                          ".'{PAGENO}{nbpg}' );
		// LOAD a stylesheet
		//$stylesheet = file_get_contents('../public/css/default.css');
		//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Attendance" ).' '.$this->view->translate( "Details" );
		$mpdf->WriteFixedPosHTML ( "<table border=1  align=center width=100%><tr><td align=center> {$ReportName}</td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		$html = htmlspecialchars_decode ( $htmltabledata );
		$mpdf->WriteHTML($html);
		$mpdf->Output('Attendance_Report.pdf','D');
		$auth = Zend_Auth::getInstance();
    	// Write Logs
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Attendence List Report(PDF)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);	
		}else{
			
		$ourFileName = realpath('.')."/data";
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); 
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($htmldata));
		fclose($ourFileHandle);
		   // $data = str_replace("\r","",$xlExportData);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=Attendance_Report.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			//print $csv_output."\n".$data;
			readfile($ourFileName);
			unlink($ourFileName);
			$auth = Zend_Auth::getInstance();
    	// Write Logs
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Attendence List Report(Excel)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);
		}
		
	}
	
		
}