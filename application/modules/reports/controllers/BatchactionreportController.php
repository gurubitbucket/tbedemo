<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_BatchactionreportController extends Base_Base 
{	
    public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjbatchreportmodel = new Reports_Model_DbTable_Batchactionreportmodel();
		$this->lobjbatchreportform = new  Reports_Form_Batchactionreport();
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjbatchreportform;
		$this->lobjbatchreportform->Takcomp->addMultiOptions(array(''=>'Select','1'=>'Company','2'=>'Takaful'));
		$this->lobjbatchreportform->Action->addMultiOptions(array('1'=>'Cancel Payment','2'=>'Blocked Registration','3'=>'Moved Candidates'));
		//$this->lobjbatchreportform->Action->addMultiOptions(array('1'=>'Cancel Payment','2'=>'Blocked Registration','3'=>'Moved Candidates'));
	    if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->exammarksreportpaginatorresult);
		$lintpagecount = 10000;
		$lintpage = $this->_getParam('page',1); 
		$larrresult = array();
		if(isset($this->gobjsessionsis->exammarksreportpaginatorresult))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->exammarksreportpaginatorresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();		
			$this->view->lobjform->populate($larrformData);		
			$larrcenters = $this->lobjbatchreportmodel->fnajaxgetcenternames($larrformData['Takcomp']);
			$this->lobjbatchreportform->Takcompnames->addMultioptions($larrcenters);
			$this->lobjbatchreportform->Takcompnames->setValue($larrformData['Takcompnames']);	
            $this->view->Takcomp = $larrformData['Takcomp'];	
            $this->view->Takcompnames = $larrformData['Takcompnames'];			
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();			
				if($larrformData['Action'] == 3)
				{
				   $this->view->action =  $larrformData['Action'];
				   $this->view->paginator = $this->lobjbatchreportmodel->fngetdetailsformovedcandidates($larrformData);				  
				}
				if($larrformData['Action'] == 2)
				{
				   $this->view->action =  $larrformData['Action'];
				   $this->view->paginator = $this->lobjbatchreportmodel->fngetdetailsforblockedregistration($larrformData);				   
				}
				if($larrformData['Action'] == 1)
				{
				   $this->view->action =  $larrformData['Action'];
				   $this->view->paginator = $this->lobjbatchreportmodel->fngetdetailsforcancelpayment($larrformData);
				}
				
		   }
	   }	
    }
	public function fnajaxgettakcompnamesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidname = $this->_getParam('idname');
		$larrcenters = $this->lobjbatchreportmodel->fnajaxgetcenternames($lintidname);
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}
	
	public function exporttoexcelAction()
    {   
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		//echo "<pre>";
		//print_r($larrformData);die();
		
				
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Batchaction_Report_'.$larrname[0]['CompanyName'].'_'.$larrformData['Pin'];
		$ReportName = $this->view->translate( "Batch" ).' '.$this->view->translate( "Action" ).' '.$this->view->translate( "Report" );
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
		 $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left ><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left ><b> Time</b></td><td align=left colspan = 2><b>$time</b></td></tr>";
		 $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 5><b> {$ReportName}</b></td></tr></table><br>";
		 
		       if($larrformData['Act'] == 3)
				{
				   $this->view->action =  $larrformData['Act'];
				   $this->view->paginator = $larrresult =  $this->lobjbatchreportmodel->fngetdetailsformovedcandidates($larrformData);
				   $tabledata.= '<table border=1 align=center width=100%><tr><th><b>Company/Takaful Name</b></th><th><b>Total Candidates</b></th><th><b>Old Regpin</b></th><th><b>New Regpin</b></th><th><b>Date</b></th></th><th><b>User</b></th>
		             </tr>';
				    foreach ($larrresult as $lobjCountry ):
					
					 $tabledata.= "<tr><td align=left ><b>".$lobjCountry['Company']."</b></td><td align=left ><b>".$lobjCountry['Totalcandidates']."</b></td>";
   	                 $tabledata.= "<td align=left ><b>".$lobjCountry['Oldregpin']."</b><td align=left ><b>".$lobjCountry['Newregpin']."</b></td>";
				    $tabledata.= "<td align=left ><b>".$lobjCountry['Date']."</b></td><td align=left ><b>".$lobjCountry['User']."</b></td></tr>";
				 
				     endforeach;		   
				}
				if($larrformData['Act'] == 2)
				{
					$this->view->action =  $larrformData['Act'];
					$this->view->paginator = $larrresult = $this->lobjbatchreportmodel->fngetdetailsforblockedregistration($larrformData); 
					$tabledata.= '<table border=1 align=center width=100%><tr><th><b>Company/Takaful Name</b></th><th><b>Totalregistered</b></th><th><b>Current Total</b></th><th><b>Date</b></th><th><b>User</b></th></th>
		             </tr>';
					 foreach ($larrresult as $lobjCountry ):
					
					 $tabledata.= "<tr><td align=left ><b>".$lobjCountry['Company']."</b></td><td align=left ><b>".$lobjCountry['Totalregistered']."</b></td>";
   	                 $tabledata.= "<td align=left ><b>".$lobjCountry['Totalcandidates']."</b><td align=left ><b>".$lobjCountry['Date']."</b></td>";
				    $tabledata.= "<td align=left ><b>".$lobjCountry['User']."</b></td></tr>";
				 
				     endforeach;	
							
				}
				if($larrformData['Act'] == 1)
				{
				    $this->view->action =  $larrformData['Act'];
				    $this->view->paginator = $larrresult = $this->lobjbatchreportmodel->fngetdetailsforcancelpayment($larrformData);
					$tabledata.= '<table border=1 align=center width=100%><tr><th><b>Company/Takaful Name</b></th><th><b>Canceled Registration Pin</b></th><th><b>Remarks</b></th><th><b>Cancelation Details</b></th><th><b>Date</b></th></th><th><b>User</b></th>
		             </tr>';
					 foreach ($larrresult as $lobjCountry ):
					
					 $tabledata.= "<tr><td align=left ><b>".$lobjCountry['Company']."</b></td><td align=left ><b>".$lobjCountry['Regpin']."</b></td>";
   	                 $tabledata.= "<td align=left ><b>".$lobjCountry['Remarks']."</b><td align=left ><b>".$lobjCountry['Details']."</b></td>";
				    $tabledata.= "<td align=left ><b>".$lobjCountry['Date']."</b></td><td align=left ><b>".$lobjCountry['User']."</b></td></tr>";
				 
				     endforeach;
                  					 
				}
				$tabledata.= "</table>";
				
				
				
		//$larrresult = $this->lobjbatchreportmodel->fngetstuddetails($larrformData['Pin']);
		//$larrname = $this->lobjbatchreportmodel->fngettakcompname($larrformData['Takcomp'],$larrformData['Takcompnames']);
		
		
		
	   
    	
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		
    }
    
    
}