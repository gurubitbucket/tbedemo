<?php
class Reports_MigspaymentreportController extends Base_Base { //Controller for the User Module
	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;	
	
	public function init() { //initialization function
		$this->gobjsessionstudent = Zend_Registry::get('sis');
		/*$this->gobjsessionsis = Zend_Registry::get('sis');
		if(empty($this->gobjsessionsis->iduser)){ 
			$this->_redirect( $this->baseUrl . '/index/logout');					
		}	*/	
			
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	public function fnsetObj() 
	{
		$this->lobjcompletedetailsmodel = new Reports_Model_DbTable_Migspayment(); //user model object
		$this->lobjmigsform = new Reports_Form_Migspayment(); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	public function indexAction() 
	{ 
	    $this->view->lobjmigsform = $this->lobjmigsform;
		if ($this->_request->isPost() && $this->_request->getPost('Search'))
		{
			$larrformData = $this->_request->getPost();
			$fromdate = $this->view->fromdate=$larrformData['Fromdate'];
			$todate = $this->view->todate=$larrformData['Todate'];
			$larrresult = $this->lobjcompletedetailsmodel->fngetdetailforstudentpayment($larrformData['Fromdate'],$larrformData['Todate']);
			
			$this->view->paginator = $larrresult;
			$this->view->lobjmigsform->populate($larrformData);
		}	
	}
	public function pdfexportAction()
	{   
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformdata1 = $this->_request->getPost();
		//echo "<pre>";print_R($larrformdata1);die();
		$fdate = $larrformdata1['Fromdate'];
		$tdate = $larrformdata1['Todate'];
		$frmdate =date('d-m-Y',strtotime($fdate));
		$todate =date('d-m-Y',strtotime($tdate));
		$result = $this->lobjcompletedetailsmodel->fngetdetailforstudentpayment($larrformdata1['Fromdate'],$larrformdata1['Todate']);
		$count=count($result);
		
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "";//"http://".$host."/tbe/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Migspayment_'.$frmdate.'_'.$todate;
		$ReportName = $this->view->translate( "Student" ).' '.$this->view->translate( "Payment" ).' '.$this->view->translate( "Report" );
		
		//$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
		
		
		$tabledata= "<tr><td align=left colspan = 2><b>Payment From Date </b></td><td align=left colspan = 2><b>".$frmdate."</b></td><td  align=left colspan = 2><b> Payment To Date </b></td><td align=left colspan = 2><b>".$todate."</b></td></tr>";
		
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 8><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table border=1 align=center width=100%><tr><th><b>IDApplication</b></th><th><b>Student Name</b></th><th><b>Program</b></th><th><b>Venue</b></th></th><th><b>Examdate</b></th><th><b>Card Type</b></th><th><b>Transaction Number</b></th><th><b>Payment Date</b></th><th><b>Payment Status</b></th></tr>';
		$cnts  = 0;
		$centerarray =array();
		$centerarrays =array();
		$centerarrayss =array();
		$centerarraysss =array();
		foreach($result as $lobjCountry)
		{
			
			
			    $tabledata.="<tr>";
			    
			     $tabledata.="<td>";
				$tabledata.=$lobjCountry['vpc_MerchTxnRef'];
                $tabledata.="</td>";				
			
				
			  $tabledata.="<td>";
				
				$tabledata.=$lobjCountry['FName'];
			  $tabledata.="</td>";
			    $tabledata.="<td>";
				
				$tabledata.=$lobjCountry['ProgramName'];
				$tabledata.="</td>";
			
			    $tabledata.="<td>";
				
				
				$tabledata.=$lobjCountry['centername'];
				$tabledata.="</td>";
			
			
			$tabledata.= '<td>'.$lobjCountry['Examdate'].'</td><td>'.$lobjCountry['vpc_Card'].'</td>';
			$tabledata.= '<td>'.$lobjCountry['vpc_TransactionNo'].'</td><td>'.$lobjCountry['Date'].'</td>';
				if($lobjCountry['Status'] == 0)
				{  
			    $tabledata.= '<td>Success</td>';
				}
				else
				{
				    $tabledata.= '<td>Fail</td>';
				}
			$tabledata.="</tr>";
		}
		
		    $tabledata.="</table>";
		
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		
	}

	

}