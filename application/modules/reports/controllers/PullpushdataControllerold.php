<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
class Reports_PullpushdataController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
		$this->lobjmodel = new Reports_Model_DbTable_Pullpushdata(); //model object
		$this->lobjnewform = new  Reports_Form_Pullpushdata(); //form object
		$this->registry = Zend_Registry::getInstance(); //get registry instance
		$this->locale = $this->registry->get('Zend_Locale'); //get locale
	}
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjnewform;
		$larrcenters = $this->lobjmodel->fngetcenternames();
		$this->lobjnewform->Centre->addmultioptions($larrcenters);
		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->attendancereportpaginatorresult);
		$lintpagecount = 1000;
		$lintpage = $this->_getParam('page',1);//Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->attendancereportpaginatorresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				unset ( $larrformData ['Search']);
				echo "<pre>";print_r($larrformData);die();
				$larrresult = $this->lobjmodel->fngetdetails($larrformData);
				//$this->view->fromdate = $FromDate = $larrformData['FromDate'];
				//$this->view->todate = $ToDate = $larrformData['ToDate'];
				/*$larrresultcomp = $this->lobjbatchregreport->fngetcompanydetails($FromDate,$ToDate);
				$larrresulttakaful = $this->lobjbatchregreport->fngettakafuldetails($FromDate,$ToDate);
				$larrcompanycredit = $this->lobjbatchregreport->fngetcompanycredit($FromDate,$ToDate);
				$larrtakafukcredit = $this->lobjbatchregreport->fngettakafulcredit($FromDate,$ToDate);
				$larrcompanyfpx = $this->lobjbatchregreport->fngetcompanyfpx($FromDate,$ToDate);
				$larrtakafukfpx = $this->lobjbatchregreport->fngettakafulfpx($FromDate,$ToDate);
				$larrresult = array_merge($larrresultcomp,$larrcompanycredit,$larrcompanyfpx,$larrresulttakaful,$larrtakafukcredit,$larrtakafukfpx);*/
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->attendancereportpaginatorresult = $larrresult;
				$this->view->lobjform->populate($larrformData);
			}
		}
	}
	public function getstudentdetailsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintregpin = $this->_getParam('regpin');
		$result = $this->lobjbatchregreport->fngetstuddetails($lintregpin);
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Batch Registered Student Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>ICNO</b></th><th><b>Program</b></th><th><b>ExamVenue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th></tr><tr>";
		foreach($result as $lobjCountry)
		{
			$tabledata.="<td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".$lobjCountry['ProgramName']."</td><td align = 'left'>".$lobjCountry['ExamVenue']."</td><td align = 'left'>".$lobjCountry['ExamDate']."</td><td align = 'left'>".$lobjCountry['session']."</td></tr>";
		}
		$tabledata.="</table><br>";
		$tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
		echo  $tabledata;
	}
    public function fnexportAction()
    {   
    	
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$ldtfromdate = $this->_getParam('fromdate');
		$ldttodate = $this->_getParam('todate');
		$lstrreportytpe = $this->_getParam('type'); 
		$larrresultcomp = $this->lobjbatchregreport->fngetcompanydetails($ldtfromdate,$ldttodate);
		$larrresulttakaful = $this->lobjbatchregreport->fngettakafuldetails($ldtfromdate,$ldttodate);
		$larrcompanycredit = $this->lobjbatchregreport->fngetcompanycredit($ldtfromdate,$ldttodate);
		$larrtakafukcredit = $this->lobjbatchregreport->fngettakafulcredit($ldtfromdate,$ldttodate);
		$larrcompanyfpx = $this->lobjbatchregreport->fngetcompanyfpx($ldtfromdate,$ldttodate);
		$larrtakafukfpx = $this->lobjbatchregreport->fngettakafulfpx($ldtfromdate,$ldttodate);
	    $result = array_merge($larrresultcomp,$larrcompanycredit,$larrcompanyfpx,$larrresulttakaful,$larrtakafukcredit,$larrtakafukfpx);
    	$centerarray = array();
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Batchregistration_Report_'.$ldtfromdate.'_'.$ldttodate;
		$ReportName = $this->view->translate( "Batchregistration" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Excel')
		{
			$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
		}
		else
		{
		    $tabledata = '<img width=100% src="../public/images/reportheader.jpg" /><br><br><br><br><br>';
		}
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 1><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 1><b> Time</b></td><td align=left colspan = 2><b>$time</b></td></tr></table>";
        $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 6><b> {$ReportName}</b></td></tr></table><br>";
        $tabledata.= '<table border=1 align=center width=100%><tr><th><b>Company/Takaful Name</b></th><th><b>Program</b></th><th><b>Registration Pin</b></th><th><b>Payment Status</b></th><th><b>Mode Of Payment</b></th><th><b>Payment Date</b></th></tr>';
	    foreach($result as $lobjCountry)
			{
				$tabledata.="<tr><td>";
				if(!in_array($lobjCountry['CompanyName'],$centerarray))
				{
					$centerarray[] = $lobjCountry['CompanyName'];
					$centerarrays[$lobjCountry['CompanyName']] = array();
					$tabledata.= $lobjCountry['CompanyName'];
				}
				$tabledata.="</td>";
				$tabledata.="<td>";
				if(!in_array($lobjCountry['ProgramName'],$centerarrays[$lobjCountry['CompanyName']]))
				{
					$centerarrays[$lobjCountry['CompanyName']][] = $lobjCountry['ProgramName'];
					$centerarrayss[$lobjCountry['CompanyName']][$lobjCountry['ProgramName']] = array();
					$tabledata.=$lobjCountry['ProgramName'];
				}
				$tabledata.="</td>";
				$tabledata.= '<td>'.$lobjCountry['Regpin'].'</td><td>'.$lobjCountry['paymentstatus'].'</td><td>'.$lobjCountry['Modeofpayment'].'</td>';
				if($lobjCountry['Paydate']){
					$tabledata.= '<td>'.$lobjCountry['Paydate'].'</td>';
				}else {
					$tabledata.= '<td>00-00-0000</td>';
				}
				$tabledata.="</tr>";
			}
	    $tabledata.="</table>";
    	if($lstrreportytpe=='Excel'){
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}else {
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}
    }

}