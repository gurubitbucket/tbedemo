<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
class Reports_ExamdetailsreportController extends Base_Base 
{	
    public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjExamreportModel = new GeneralSetup_Model_DbTable_Common();
		$this->lobjReportsForm = new  Reports_Form_Examdetailsreportform(); 
		$this->lobjCommon = new App_Model_Common();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjReportsForm;
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->attendancereportpaginatorresult);
		$lintpagecount = 10000;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
	     {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->attendancereportpaginatorresult,$lintpage,$lintpagecount);
		 } 
		 else 
		 {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		 }
		 if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {
		    	$larrformData = $this->_request->getPost ();
		    	if ($this->lobjform->isValid($larrformData)) 
				{
					$larrformData = $this->_request->getPost ();
					
					unset ( $larrformData ['Search']);
					$ldtiddate = $larrformData['Date9'];
					$lintvenueid = $larrformData['Venue'];
					$lintidprogram = $larrformData['Course'];
					$lintidsession = $larrformData['session'];
					$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrselect = " SELECT  a.IDApplication,a.FName,if(a.pass=1,'Pass','Fail') as Result,b.NoofQtns as TotalQuestions,b.Attended as Attended,b.Correct,
							                (b.NoofQtns - b.Attended) as Unattended,(b.Attended -b.Correct) as Wrong,b.Grade,
							                c.Starttime,c.Endtime,if(c.Submittedby=1,'Candidate Submit',if(c.Submittedby=2,'Center Submit','Auto Submitted')) as Submittedby
						           FROM    tbl_studentapplication a 
			             				   left outer join tbl_studentmarks b on a.IDApplication = b.IDApplication
			             				   left outer join tbl_studentstartexamdetails c on a.IDApplication = c.IDApplication
			             				   left outer join tbl_managesession d on a.examsession = d.idmangesession
			             				   left outer join tbl_programmaster e on a.Program = e.IdProgrammaster
										   left outer join tbl_center f on a.examvenue = f.idcenter
								   WHERE   a.IDApplication>1148 and examvenue <> 000 and a.Payment = 1 and a.pass not in(3,4) and a.DateTime = '$ldtiddate' and f.idcenter = $lintvenueid and e.IdProgrammaster = $lintidprogram and d.idmangesession = $lintidsession
								   Group by a.IDApplication
								   Order by a.FName";
					$larrresult = $this->lobjExamreportModel->fnGetValues($lstrselect);
					$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
					$this->gobjsessionsis->attendancereportpaginatorresult = $larrresult;
					$lstrSelect = $lobjDbAdpt->select()
		                        ->from(array("a" => "tbl_studentapplication"),array(""))
								->join(array("b"=>"tbl_center"),'b.idcenter = a.examvenue',array("key"=>"b.idcenter","value"=>"b.centername"))
								->where("a.payment=1")
								->where("a.pass in (1,2)")
								->where("a.DateTime='$ldtiddate'")
								->group("b.idcenter")
								->order("b.centername");
					$larrschedulerdays = $this->lobjExamreportModel->fnGetValues($lstrSelect);
					$this->view->lobjform->Venue->addMultiOptions($larrschedulerdays);
					$this->view->lobjform->Venue->setValue($lintvenueid);
					$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
			                        ->from(array("a" => "tbl_studentapplication"),array(""))
									->join(array("b"=>"tbl_managesession"),'b.idmangesession = a.examsession',array("key"=>"b.idmangesession","value"=>"b.managesessionname"))
									->where("a.payment=1")
									->where("a.pass in (1,2)")
									->where("a.DateTime='$ldtiddate'")
									->where("a.examvenue ='$lintvenueid'")
									->group("b.idmangesession")
									->order("b.idmangesession");
					$larrschedulerdayssession = $this->lobjExamreportModel->fnGetValues($lstrSelect);
					$this->view->lobjform->session->addMultiOptions($larrschedulerdayssession);
					$this->view->lobjform->session->setValue($lintidsession);
				    $lstrSelect =  $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName"))
					 			     ->join(array("d" => "tbl_studentapplication"),'a.IdProgrammaster = d.program',array(""))
					 				 ->where("d.DateTime = '$ldtiddate'") 
					 				 ->where("d.Examsession = $lintidsession")	
					 				 ->where("d.Examvenue = $lintvenueid")					 				 
					 				 ->where("d.payment=1")
					 				 ->where("d.pass in(1,2)")
					 				 ->group("a.IdProgrammaster")			 
					 				 ->order("a.ProgramName");
 					$larrschedulerdayssession1 = $lobjDbAdpt->fetchAll($lstrSelect);
 					$this->view->lobjform->Course->addMultiOptions($larrschedulerdayssession1);
					$this->view->lobjform->Course->setValue($lintidprogram);
					$this->view->lobjform->populate($larrformData);
				    /*echo $lstrselect = " SELECT  a.IDApplication,a.FName,if(a.pass=1,'Pass','Fail') as Result,b.NoofQtns as TotalQuestions,b.Attended as Attended,b.Correct,
							                (b.NoofQtns - b.Attended) as Unattended,(b.Attended -b.Correct) as Wrong,b.Grade,
							                c.Starttime,c.Endtime,if(c.Submittedby=1,'Candidate Submit',if(c.Submittedby=2,'Center Submit','Auto Submitted')) as Submittedby
						           FROM    tbl_studentapplication a 
			             				   left outer join tbl_studentmarks b on a.IDApplication = b.IDApplication
			             				   left outer join tbl_studentstartexamdetails c on a.IDApplication = c.IDApplication
			             				   left outer join tbl_managesession d on a.examsession = d.idmangesession
			             				   left outer join tbl_programmaster e on a.Program = e.IdProgrammaster
										   left outer join tbl_center f on a.examvenue = f.idcenter
								   WHERE   a.IDApplication>1148 and examvenue <> 000 and a.Payment = 1 and a.pass not in(3,4) and a.DateTime = '$ldtiddate' and f.idcenter = $lintvenueid and e.IdProgrammaster = $lintidprogram and d.idmangesession = $lintidsession
								   Group by a.IDApplication
								   Order by a.FName"; die();
					$larrresult = $this->lobjExamreportModel->fnGetValues($lstrselect);
					$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
					$this->gobjsessionsis->attendancereportpaginatorresult = $larrresult;
					$this->view->lobjform->populate($larrformData);*/
				}
		    }
	 }
	public function fnpdfexportAction()
	{   
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintIDApp = $this->_getParam('id');
		$lintactive = $this->_getParam('active');
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();   
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("b" =>"tbl_studentapplication"),array("b.*","CONCAT(b.Examdate,'-',IFNULL(b.Exammonth,' '),'-',IFNULL(r.Year,' ')) as StudExamdate"))
								 ->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
								 ->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
								 ->join(array("r" =>"tbl_newscheduler"),'r.idnewscheduler=b.Year',array("r.Year as Years"))
								 ->join(array("i" =>"tbl_managesession"),'i.idmangesession=b.Examsession',array("i.managesessionname","i.starttime as ExamSessionTime"))
								 ->join(array("j" =>"tbl_studentmarks"),'j.IDApplication = b.IDApplication')
								 ->join(array("k" =>"tbl_studentstartexamdetails"),'k.IDApplication = b.IDApplication',array("k.Starttime as Studentstarttime"))
								 ->where("b.IDApplication = $lintIDApp");
		$larrstuddtls = $this->lobjExamreportModel->fnGetValuesRow($lstrSelect);
		$lstrSelect = "SELECT a.IDApplication,b.ExamStartTime  
		               FROM tbl_studentapplication a, tbl_centerstartexam b 
		               WHERE  a.Examvenue = b.idcenter and a.Program = b.Program and a.Examsession = b.idSession and a.DateTime = b.ExamDate and a.IDApplication = '$lintIDApp'";
		$larrcentrestartdtls = $this->lobjExamreportModel->fnGetValuesRow($lstrSelect);
		$lintUnAnswered = $larrstuddtls['NoofQtns'] - $larrstuddtls['Attended'];
		$lintNoOfWrong = $larrstuddtls['Attended'] - $larrstuddtls['Correct'];
		$ldtday= date("d-m-Y");
		$time = date('h:i:s a', time());
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Exam" ).' '.$this->view->translate( "Details" ).' '.$this->view->translate( "Report" );
		$html = htmlspecialchars_decode ( $htmltabledata );
		ini_set('max_execution_time',3600);
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td><b>Date </b></td><td><b>$ldtday</b></td><td><b> Time</b></td><td><b>$time</b></td></tr></table>";
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Student Name&nbsp;</b></td><td width="50%"><b>: </b>'.$larrstuddtls['FName'].'</td><td width="13%"><b>IC No&nbsp;</b></td><td width="23%"><b>: </b>'.$larrstuddtls['ICNO'].'</td><td width="14%"><b>Course&nbsp;</b></td><td width="28%"><b>: </b>'.$larrstuddtls['ProgramName'].'</td></tr></table>';
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Venue&nbsp;</b></td><td width="50%"><b>: </b>'.$larrstuddtls['centername'].'</td><td width="13%"><b>Exam Date&nbsp;</b></td><td width="23%"><b>: </b>'.$larrstuddtls['StudExamdate'].'</td><td width="14%"><b>Exam Session&nbsp;</b></td><td width="28%"><b>: </b>'.$larrstuddtls['managesessionname'].'</td></tr></table>';
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Scheduled Time&nbsp;</b></td><td  width="50%"><b>: </b>'.$larrstuddtls['ExamSessionTime'].'</td><td width="13%"><b>Centre Start &nbsp;</b></td><td width="23%"><b>: </b>'.$larrcentrestartdtls['ExamStartTime'].'</td><td width="14%"><b>Student Start &nbsp;</b></td><td width="28%"><b>: </b>'.$larrstuddtls['Studentstarttime'].'</td></tr></table>';
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Total Questions&nbsp;</b></td><td width="50%"><b>: </b>'.$larrstuddtls['NoofQtns'].'</td><td width="13%"><b>Answered&nbsp;</b></td><td width="23%"><b>: </b>'.$larrstuddtls['Attended'].'</td><td width="14%"><b>Unanswered&nbsp;</b></td><td width="28%"><b>: </b>'.$lintUnAnswered.'</td></tr></table>';
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Correct Answers&nbsp;</b></td><td width="50%"><b>: </b>'.$larrstuddtls['Correct'].'</td><td width="13%"><b>Wrong Answers&nbsp;</b></td><td width="23%"><b>: </b>'.$lintNoOfWrong.'</td><td width="14%"><b>Marks Obtained&nbsp;</b></td><td width="28%"><b>: </b>'.$larrstuddtls['Correct'].'</td></tr></table>';
		$tabledata.= '<table width=100%><tr><td width="15%"><b>Grade&nbsp;</b></td><td  width="50%"><b>: </b>'.$larrstuddtls['Grade'].'</td><td width="13%"></td><td width="23%"></td><td width="14%"></td><td width="28%"></td></tr></table>';
		$tabledata.='<hr></hr><br>';
		Switch ($lintactive){
			case 1: 
				    $filename = $ReportName.'_'.$lintIDApp.'_'.$lstrStudentname.'_'.$ldtStudExamdate.'_Attended';
				    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       				$lstrSelect = $lstrSelect = $lobjDbAdpt->select()
                             							   ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Answer"))
                           								   ->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid',array())
                          								   ->join(array("c" => "tbl_answers"),'a.Answer = c.idanswers',array("c.answers AS StudAns","c.idanswers as Studidanswers"))
                          								   ->join(array("d" => "tbl_questions"),'a.QuestionNo = d.idquestions',array("d.Question"))
                         								   ->where("b.IDApplication = $lintIDApp");
				    $larrresult = $this->lobjExamreportModel->fnGetValues($lstrSelect);
		            $lintcount = count($larrresult);
		            for($linti=1,$lints=0;$linti<=$lintcount,$lints<$lintcount;$lints++,$linti++){
		            	$tabledata.='<table><tr>
     				                           <td valign="top" >'.'Q'.$linti.'.'.$larrresult[$lints]['QuestionNo'].')'.' </td><td align="left">'.trim($larrresult[$lints]['Question']).'<br></td>
	  				                         </tr>
	  				                 </table><br>';
		            	$tabledata.='<table><tr>
     				                           <td valign="top" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$linti.'.'.$larrresult[$lints]['Studidanswers'].')'.' </td><td align="left">'.trim($larrresult[$lints]['StudAns']).'<br></td>
	  				                         </tr>
	  				                 </table><br>';
		            }
			        break;
			case 2:  
				    $filename = $ReportName.'_'.$lintIDApp.'_'.$lstrStudentname.'_'.$ldtStudExamdate.'_UnAttended';
				    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       				$lstrSelect = $lobjDbAdpt->select()
                                			 ->from(array("a" => "tbl_studentapplication"),array(""))
                                			 ->join(array("b" => "tbl_questionsetforstudents"),'a.IDApplication = b.IDApplication',array('b.idquestions'))
                                			 ->where("b.IDApplication = $lintIDApp")
                               				 ->order("b.idquestionsetforstudents desc");
				    $larrallquestions = $this->lobjExamreportModel->fnGetValuesRow($lstrSelect);
				    $questionids = explode(',',$larrallquestions['idquestions']);
       				$lstrSelect1 = $lstrSelect = $lobjDbAdpt->select()
                             								->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Answer"))
                             								->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid',array())
                             								->join(array("c" => "tbl_answers"),'a.Answer = c.idanswers',array("c.answers AS StudAns","c.idanswers as Studidanswers"))
                            								->join(array("d" => "tbl_questions"),'a.QuestionNo = d.idquestions',array("d.Question"))
                             								->where("b.IDApplication = $lintIDApp");
				    $larrresult = $this->lobjExamreportModel->fnGetValues($lstrSelect1);
				    $lintcount = count($larrresult);
				    for($linti=0;$linti<$lintcount;$linti++){
				    	$larrattended[] = $larrresult[$linti]['QuestionNo'];
				    }
			        $larrunattended = array_values(array_diff($questionids,$larrattended));
			        $lintcount = count($larrunattended);
					for ($lintk=0,$lintl=1;$lintk<$lintcount,$lintl<=$lintcount;$lintk++,$lintl++)
					{
					    $lstrSelect = $lobjDbAdpt->select()
                             					 ->from(array("a" => "tbl_questions"),array("TRIM(a.Question) AS Question"))
                             					 ->where("a.idquestions = $larrunattended[$lintk]");
						$larrunattendedquestions = $this->lobjExamreportModel->fnGetValuesRow($lstrSelect);
						$lstrSelect1 = $lobjDbAdpt->select()
	                             				  ->from(array("a" => "tbl_answers"),array("a.answers","a.idanswers"))
	                             				  ->where("a.idquestion = $larrunattended[$lintk]");
						$larranswers = $this->lobjExamreportModel->fnGetValues($lstrSelect1);
						$tabledata.='<table><tr>
     				                           <td valign="top" >'.'Q'.$lintl.'.'.$larrunattended[$lintk].')'. ' </td><td align="left">'.trim($larrunattendedquestions['Question']).'<br></td>
	  				                         </tr>
	  				                 </table><br>';
						for($lintm=0,$lintn=1;$lintm<4,$lintn<=4;$lintm++,$lintn++){
					    $tabledata.='<table><tr>
     				                           <td valign="top" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$lintl.' '.$larranswers[$lintm]['idanswers'].')'.'</td><td align="left">'.trim($larranswers[$lintm]['answers']).'<br></td>
	  				                        </tr>
	  				                 </table><br>';
						       }
					}
			        break;
			case 3: 
				    $filename = $ReportName.'_'.$lintIDApp.'_'.$lstrStudentname.'_'.$ldtStudExamdate.'_Correct';
				    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       				$lstrSelect = $lstrSelect = $lobjDbAdpt->select()
                             							   ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Answer"))
                             							   ->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid',array())
                             							   ->join(array("c" => "tbl_answers"),'a.Answer = c.idanswers',array("c.answers AS StudAns","c.idanswers as Studidanswers"))
                             							   ->join(array("d" => "tbl_questions"),'a.QuestionNo = d.idquestions',array("d.Question"))
                             							   ->where("c.CorrectAnswer = 1")
                             							   ->where("b.IDApplication = $lintIDApp");
				    $larrcorrect = $this->lobjExamreportModel->fnGetValues($lstrSelect);
					$lintcount = count($larrcorrect);
		            for($linti=1,$lints=0;$linti<=$lintcount,$lints<$lintcount;$lints++,$linti++){
		            	$tabledata.='<table><tr>
     				                           <td valign="top" >'.'Q'.$linti.'.'.$larrcorrect[$lints]['QuestionNo'].')'.' </td><td align="left">'.trim($larrcorrect[$lints]['Question']).'<br></td>
	  				                         </tr>
	  				                 </table><br>';
		            	$tabledata.='<table><tr>
     				                           <td valign="top" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$linti.'.'.$larrcorrect[$lints]['Studidanswers'].')'.' </td><td align="left">'.trim($larrcorrect[$lints]['StudAns']).'<br></td>
	  				                         </tr>
	  				                 </table><br>';
		            }
			        break;
			case 4: 
				    $filename = $ReportName.'_'.$lintIDApp.'_'.$lstrStudentname.'_'.$ldtStudExamdate.'_Wrong';
				    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       				 $lstrSelect = $lstrSelect = $lobjDbAdpt->select()
	                             							->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Answer"))
	                             							->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid',array())
	                             							->join(array("c" => "tbl_answers"),'a.Answer = c.idanswers',array("c.answers AS StudAns","c.idanswers as Studidanswers"))
	                             							->join(array("d" => "tbl_questions"),'a.QuestionNo = d.idquestions',array("d.Question"))
	                             							->where("c.CorrectAnswer = 0")
	                             							->where("b.IDApplication = $lintIDApp");
				    $larrwrong = $this->lobjExamreportModel->fnGetValues($lstrSelect);
					$lintcount = count($larrwrong);
		            for($linti=1,$lints=0;$linti<=$lintcount,$lints<$lintcount;$lints++,$linti++){
		            	$tabledata.='<table><tr>
     				                           <td valign="top" >'.'Q'.$linti.'.'.$larrwrong[$lints]['QuestionNo'].')'.' </td><td align="left">'.trim($larrwrong[$lints]['Question']).'<br></td>
	  				                         </tr>
	  				                 </table><br>';
		            	$tabledata.='<table><tr>
     				                           <td valign="top" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$linti.'.'.$larrwrong[$lints]['Studidanswers'].')'.' </td><td align="left">'.trim($larrwrong[$lints]['StudAns']).'<br></td>
	  				                         </tr>
	  				                 </table><br>';
		            }
			        break;
			default: break;
		}
		$mpdf->WriteHTML($tabledata);
		$mpdf->Output($filename,'D');
	}
	public function getvenuesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$ldtiddate = $this->_getParam('iddate');
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                        ->from(array("a" => "tbl_studentapplication"),array(""))
								->join(array("b"=>"tbl_center"),'b.idcenter = a.examvenue',array("key"=>"b.idcenter","value"=>"b.centername"))
								->where("a.payment=1")
								->where("a.pass in (1,2)")
								->where("a.DateTime='$ldtiddate'")
								->group("b.idcenter")
								->order("b.centername");
		/*$lstrSelect = $lobjDbAdpt->select()
		                        ->from(array("a" => "tbl_venuedateschedule"),array(""))
								->join(array("b"=>"tbl_center"),'b.idcenter = a.idvenue',array("key"=>"b.idcenter","value"=>"b.centername"))
								->where("a.Allotedseats>0")
								->where("a.date='$ldtiddate'")
								->group("b.idcenter")
								->order("b.centername");*/
		$larrschedulerdays = $this->lobjExamreportModel->fnGetValues($lstrSelect);
		if($larrschedulerdays)
		 {
			$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdays);
		 }
		 else 
		 {
		 	$larrCountryStatesDetailss = "";
		 }
		 echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
	}
	public function getsessionsAction()
    {
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$ldtiddate = $this->_getParam('iddate');
		$lintvenueid = $this->_getParam('idvenue');
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			                        ->from(array("a" => "tbl_studentapplication"),array(""))
									->join(array("b"=>"tbl_managesession"),'b.idmangesession = a.examsession',array("key"=>"b.idmangesession","value"=>"b.managesessionname"))
									->where("a.payment=1")
									->where("a.pass in (1,2)")
									->where("a.DateTime='$ldtiddate'")
									->where("a.examvenue ='$lintvenueid'")
									->group("b.idmangesession")
									->order("b.idmangesession");
		/*$lstrSelect = $lobjDbAdpt->select()
			                        ->from(array("a" => "tbl_venuedateschedule"),array(""))
									->join(array("b"=>"tbl_managesession"),'b.idmangesession = a.idsession',array("key"=>"b.idmangesession","value"=>"b.managesessionname"))
									->where("a.Allotedseats>0")
									->where("a.date='$ldtiddate'")
									->where("a.idvenue ='$lintvenueid'")
									->group("b.idmangesession")
									->order("b.idmangesession");*/
		$larrschedulerdayssession = $this->lobjExamreportModel->fnGetValues($lstrSelect);
		if($larrschedulerdayssession){
	    	$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdayssession);
		}else{
		 	$larrCountrysessionDetailss='';	
		}
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
    }   
	public function getprogramsAction()
    {
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$ldtiddate = $this->_getParam('iddate');
		$lintvenueid = $this->_getParam('idvenue');
		$lintsessionid = $this->_getParam('idsession');
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect =  $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName"))
					 			     ->join(array("d" => "tbl_studentapplication"),'a.IdProgrammaster = d.program',array(""))
					 				 ->where("d.DateTime = '$ldtiddate'") 
					 				 ->where("d.Examsession = $lintsessionid")	
					 				 ->where("d.Examvenue = $lintvenueid")					 				 
					 				 ->where("d.payment=1")
					 				 ->where("d.pass in(1,2)")
					 				 ->group("a.IdProgrammaster")			 
					 				 ->order("a.ProgramName");
		/*$lstrSelect = $lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName"))
					 				 ->join(array("b"=>"tbl_newschedulercourse"),'a.IdProgrammaster = b.IdProgramMaster',array(""))
					 				 ->join(array("c" => "tbl_venuedateschedule"),'c.idnewscheduler = b.idnewscheduler',array(""))
					 			    ->join(array("d" => "tbl_studentapplication"),'b.IdProgramMaster = d.program',array(""))
					 				 ->where("c.Allotedseats>0")
					 				 ->where("c.date = '$ldtiddate'") 
					 				 ->where("c.idsession = $lintsessionid")	
					 				 ->where("c.idvenue = $lintvenueid")
					 				 ->where("d.pass not in (3,4)")
					 				 ->where("d.payment=1)")
					 				 ->group("a.IdProgrammaster")			 
					 				 ->order("a.ProgramName");*/
					 				//echo $lstrSelect;die(); 
 		$larrschedulerdayssession = $lobjDbAdpt->fetchAll($lstrSelect);
		if($larrschedulerdayssession){
	    	$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdayssession);
		}else{
		 	$larrCountrysessionDetailss='';	
		}
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
    }
 }