<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');

class Reports_ExamdetailsController extends Base_Base 
{	private $_gobjlogger; 
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    $this->fnsetObj(); //call fnsetObj
   	    $this->lobjCommon = new App_Model_Common();
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjanswer = new Examination_Model_Answer(); //intialize user db object
		$this->lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$this->lobjform = new App_Form_Search(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjexamreport = new Reports_Model_DbTable_Examdetails(); //intialize user db object
	}
	
	//function to set and display the result
	public function indexAction() 
	{    
		
		$this->view->venufield=0;
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view
		
		$larrcenternames = $this->lobjanswer->fngetcenternames();
		$this->lobjform->field5->addMultiOption('','Select'); 
		$this->lobjform->field5->addMultiOptions($larrcenternames);
		$this->lobjform->field5->setRegisterInArrayValidator(false);
		//$this->lobjform->field10->setAttrib('required',"true");
		$this->lobjform->field5->setAttrib('required',"false");
		//$this->lobjform->field10->setAttrib('constraints', "$examdate");
		//$this->lobjform->field10->setAttrib('OnChange','fnGetVenuedetails(this)');
		
		$this->lobjform->RegFromDate->setAttrib('OnChange','fnFillToDate(this)');
		$this->lobjform->RegToDate->setAttrib('readOnly',"true") ;
		
		$larrcourses= $this->lobjExamreportModel->fngetprogramnames();		
		$this->lobjform->field8->addMultiOption('','Select'); 	
		$this->lobjform->field8->addmultioptions($larrcourses);
		
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->examdetailspaginatorresult);
		
		
		$lintpagecount = $this->gintPageCount;	
		//$lintpagecount=100;	
		$lintpage = $this->_getParam('page',1); //Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->examdetailspaginatorresult)) {
			$this->view->venufield= $this->gobjsessionsis->examvenue;
			$this->lobjform->field5->setValue($this->view->venufield);
			
			$this->view->FromDate= $this->gobjsessionsis->FromDate;
			$this->lobjform->FromDate->setValue($this->view->FromDate);
			
			$this->view->ToDate= $this->gobjsessionsis->ToDate;
			$this->lobjform->ToDate->setValue($this->view->ToDate);
			
			$this->view->RegFromDate= $this->gobjsessionsis->RegFromDate;
			$this->lobjform->RegFromDate->setValue($this->view->RegFromDate);
			
			$this->view->RegToDate= $this->gobjsessionsis->RegToDate;
			$this->lobjform->RegToDate->setValue($this->view->RegToDate);
			
			$this->view->Programfield= $this->gobjsessionsis->Programfield;
			$this->lobjform->field8->setValue($this->view->Programfield);
			
			
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->examdetailspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		
		
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData)) 
			{		
					$FromDate= $this->view->FromDate=$larrformData['FromDate'];
				    $ToDate= $this->view->ToDate=$larrformData['ToDate'];
				    
				    $RegFromDate = $this->view->RegFromDate=$larrformData['RegFromDate'];
				    $RegToDate = $this->view->RegToDate=$larrformData['RegToDate'];
				    
				    $venuefield = $this->view->venufield=$larrformData['field5'];
				    $Programfield = $this->view->Programfield=$larrformData['field8'];
				    
				   unset ( $larrformData ['Search'] );
					$larrresult = $this->lobjexamreport->fnGetsearchdetails($larrformData); //searching the values for the businesstype
					$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
					$this->gobjsessionsis->examdetailspaginatorresult = $larrresult;
					$this->gobjsessionsis->examvenue=$venuefield;
					$this->gobjsessionsis->FromDate = $FromDate;
					$this->gobjsessionsis->ToDate = $ToDate;
					$this->gobjsessionsis->RegFromDate = $RegFromDate;
					$this->gobjsessionsis->RegToDate = $RegToDate;
					$this->gobjsessionsis->Programfield = $Programfield;
			}
		}
	}
	
	public function reportdetailsAction() 
	{    
		$this->_helper->layout->disableLayout();
		$idcenter = $this->_getParam('idcenter');
		$idprogram = $this->_getParam('idprogram');
		$idsession = $this->_getParam('idsession');
		$examdate = $this->_getParam('examdate');
		$type = $this->_getParam('type');
		
		
		
		switch($type) {
			case "registered":
				$this->view->larrresult= $larrresult = $this->lobjexamreport->fnGetAllRegisteredDtls($idcenter,$idprogram,$idsession,$examdate);
				
				break;
				
		}
	}
	
public function pendingdetailsAction() 
	{    
		$this->_helper->layout->disableLayout();
		$idcenter = $this->_getParam('idcenter');
		$idprogram = $this->_getParam('idprogram');
		$idsession = $this->_getParam('idsession');
		$examdate = $this->_getParam('examdate');
		$type = $this->_getParam('type');
		
		switch($type) {
			case "registered":
				$this->view->larrresult= $larrresult = $this->lobjexamreport->fnGetPendingDetails($idcenter,$idprogram,$idsession,$examdate);
				break;
				
			case "attended":	
				$this->view->larrresult=$larrresult = $this->lobjexamreport->fnGetAttedndedStudents($idcenter,$idprogram,$idsession,$examdate,'3');	
				break;
				
			case "absent":	
				$this->view->larrresult = $larrresult = $this->lobjexamreport->fnGetAbsent($idcenter,$idprogram,$idsession,$examdate);
				break;
				
			case "passed":	
				$this->view->larrresult =$larrresult = $this->lobjexamreport->fnGetAttedndedStudents($idcenter,$idprogram,$idsession,$examdate,'1');
				break;
				
			case "failed":	
				$this->view->larrresult = $larrresult = $this->lobjexamreport->fnGetAttedndedStudents($idcenter,$idprogram,$idsession,$examdate,'2');
				break;
				
		}
	}
public function paymentmodedetailsAction() 
	{    
		$this->_helper->layout->disableLayout();
		$idcenter = $this->_getParam('idcenter');
		$idprogram = $this->_getParam('idprogram');
		$idsession = $this->_getParam('idsession');
		$examdate = $this->_getParam('examdate');
		$type = $this->_getParam('type');
		
		switch($type) {
			case "fpx":
				$this->view->TrasactionIdLabel= "Trasaction Id";
				$this->view->larrresult= $larrresult = $this->lobjexamreport->fnGetFpxpaymentmodedetails($idcenter,$idprogram,$idsession,$examdate);
				
				break;
				
			case "creditcard":	
				$this->view->TrasactionIdLabel= "Trasaction Id";
				$this->view->larrresult= $larrresult = $this->lobjexamreport->fnGetCCpaymentmodedetails($idcenter,$idprogram,$idsession,$examdate);	
				break;
				
			case "moneyorder":	
				$this->view->TrasactionIdLabel= "Cheque No";
				$this->view->larrresult= $larrresult = $this->lobjexamreport->fnGetMOpaymentmodedetails($idcenter,$idprogram,$idsession,$examdate);	
				break;
				
			case "postalorder":	
				$this->view->TrasactionIdLabel= "Cheque No";
				$this->view->larrresult =$larrresult = $this->lobjexamreport->fnGetPOpaymentmodedetails($idcenter,$idprogram,$idsession,$examdate,'1');
				break;
				
			case "creditbank":	
				$this->view->TrasactionIdLabel= "Cheque No";
				$this->view->larrresult = $larrresult = $this->lobjexamreport->fnGetCreditBankpaymentmodedetails($idcenter,$idprogram,$idsession,$examdate,'2');
				break;
				
		}
	}
	public function getvebudetailsAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$examdate = $this->_getParam('examdate');
		if($examdate =="") {
			$larrStateVenueDetails = Array();
		} else {
			$larrStateVenueDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjexamreport->fnGetVenueList($examdate));
		}
		echo Zend_Json_Encoder::encode($larrStateVenueDetails);
	}

	public function pdfexportAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$venu = $this->_getParam('venu');
		$FromDate = $this->_getParam('FromDate');
		$ToDate = $this->_getParam('ToDate');
		$RegFromDate = $this->_getParam('RegFromDate');
		$RegToDate = $this->_getParam('RegToDate');
		$program = $this->_getParam('program');
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		//$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		  $stylesheet = file_get_contents('../public/css/default.css');	
		  $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Exam" ).' '.$this->view->translate( "Details" ).' '.$this->view->translate( "Report" );
		
		$Venue = $this->view->translate( "Venue" );
		$examdate = $this->view->translate( "Exam Date" );
		$Pogram = $this->view->translate( "Pogram" );
		$Session = $this->view->translate( "Session" );
		$registered = $this->view->translate( "Registered" );
		$Paid = $this->view->translate( "Paid" );
		$Pending = $this->view->translate( "Pending" );
		$FPX = $this->view->translate( "FPX" );
		$CreditCard = $this->view->translate( "CC" );
		$MoneyOrder = $this->view->translate( "MO" );
		$PostalOrder = $this->view->translate( "PO" );
		$CreditBank = $this->view->translate( "Credit/Bank to IBFIM account" );
		$Others = $this->view->translate( "Others" );
		
		
		//$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		//$mpdf->WriteHTML();
		
		$datelabel = "Date :";
		$timelabel = "Time :";
		$currentdates = date('d-m-Y');
		$currenttimes = date('H:i:s');
		
		
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		/*$tabledata.= "<br><table border=1 align=center width=100%>
						<tr>
							<th align=center><b> {$datelabel}</b></th>
							<th align=center><b> {$currentdates}</b></th>
							<th align=center><b> {$timelabel}</b></th>
							<th align=center><b> {$currenttimes}</b></th>
						</tr></table>";*/
		$tabledata.= "<br><table border=1 align=center width=100%>
							<tr>
								<th align=center><b> {$ReportName}</b></th>
							</tr></table>";
		$centerarray = array();
		$tabledata.= "<br><table class='table' border=1 width='100%' cellpadding='5' cellspacing='1'>
							<tr>
								<th align=center><b> {$Venue}</b></th>
								<th align=center><b> {$examdate}</b></th>
								<th align=center><b> {$Pogram}</b></th>
								<th align=center><b> {$Session}</b></th>
								<th align=center><b> {$registered}</b></th>
								<th colspan='6' align=center><b> {$Paid}</b></th>
								<th align=center><b> {$Pending}</b></th>
							</tr>";
		
		$tabledata.= "<tr>
								<th></th>
						        <th></th>
						        <th></th>
						        <th></th>
						        <th></th>
						        <th align=center><b> {$FPX}</b></th>
						        <th align=center><b> {$CreditCard}</b></th>
						        <th align=center><b> {$MoneyOrder}</b></th>
						        <th align=center><b> {$PostalOrder}</b></th>
						        <th align=center colspan='2'><b> {$Others}</b></th>
						        <th align=center></th>
						</tr>";
			$larrformData['FromDate'] = $FromDate;
			$larrformData['ToDate'] = $ToDate;
			$larrformData['RegFromDate'] = $RegFromDate;
			$larrformData['RegToDate'] = $RegToDate;
			$larrformData['field5'] = $venu;
			$larrformData['field8'] = $program;
			$larrresult = $this->lobjexamreport->fnGetsearchdetails($larrformData); //searching the values for the businesstype
			//print_r($larrresult);die();
			//$CountVar=count($larrresult);
			// $tabledata.="<td>{$CountVar}</td>";	
			
		$countReg=0;
     	$countPaid=0;
     	$countPending=0;
     	$countFPX=0;
     	$countCreditcard=0;
     	$countMoneyOrder=0;
     	$countPostalOrder=0;
     	$countCreditBankIBFIM =0;
     
			foreach($larrresult as $larrresultss)	{ 
				$tabledata.="<tr>";
				$centerarrays[$larrresultss['idcenter']] = array();
				
				$tabledata.="<td>";
				        if(!in_array($larrresultss['idcenter'],$centerarray)){
							$centerarray[] = $larrresultss['idcenter'];		
							$tabledata.=$larrresultss['centername'];
	         			}
					$tabledata.="</td>";
				
				$tabledata.="<td>";
				
				        if(!in_array($larrresultss['ExamDate'],$centerarrays[$larrresultss['idcenter']])){
							$centerarrays[$larrresultss['idcenter']][] = $larrresultss['ExamDate'];
							$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']] = array();;	
				        	$tabledata.=$larrresultss['ExamDate'];
	         			}
					$tabledata.="</td>";
				
					
				
					// $tabledata.="<td>{$larrresultss['ExamDate']}</td>";	

					 

					$tabledata.="<td>";
				        if(!in_array($larrresultss['ProgramName'],$centerarrays[$larrresultss['idcenter']])){
							$centerarrays[$larrresultss['idcenter']][] = $larrresultss['ProgramName'];
							$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']] = array();;	
				        	$tabledata.=$larrresultss['ProgramName'];
	         			}
					$tabledata.="</td>";
					
					

					$tabledata.="<td>";
				        if(!in_array($larrresultss['managesessionname'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
							$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['managesessionname'];	
				        	$tabledata.=$larrresultss['managesessionname'];
	         			}
					$tabledata.="</td>";			

					
					
					  $lobjExamdetailsmodel = new Reports_Model_DbTable_Examdetails(); 
				      $idcenter = $larrresultss['idcenter'];
				      $IdProgrammaster = $larrresultss['IdProgrammaster'];
				      $idmangesession = $larrresultss['idmangesession'];
				      $exdate = $larrresultss['exdate'];

				      $paidresult = $lobjExamdetailsmodel->fnGetCountPaidRegistered($idcenter,$IdProgrammaster,$idmangesession,$exdate);
				      $pendingresult = $lobjExamdetailsmodel->fnGetCountPendingdRegistered($idcenter,$IdProgrammaster,$idmangesession,$exdate);
				      $FPXresult = $lobjExamdetailsmodel->fnGetCountModeOfPaymenFPXtRegistered($idcenter,$IdProgrammaster,$idmangesession,$exdate);
				      $CreditCardresult = $lobjExamdetailsmodel->fnGetCountModeOfPaymenCreditCardtRegistered($idcenter,$IdProgrammaster,$idmangesession,$exdate);
				      $MoneyOrderresult = $lobjExamdetailsmodel->fnGetCountModeOfPaymenMoneyOrderRegistered($idcenter,$IdProgrammaster,$idmangesession,$exdate);
				      $PostalOrderresult = $lobjExamdetailsmodel->fnGetCountModeOfPaymenPostalOrderRegistered($idcenter,$IdProgrammaster,$idmangesession,$exdate);
				      $CreditBankIBFIMresult = $lobjExamdetailsmodel->fnGetCountModeOfPaymenCreditBankIBFIMRegistered($idcenter,$IdProgrammaster,$idmangesession,$exdate);
				      $Regresult = $lobjExamdetailsmodel->fnGetCountRegistered($idcenter,$IdProgrammaster,$idmangesession,$exdate);
				      
					$tabledata.="<td align='center'>{$Regresult['CountReg']}</td>";	      
				    $tabledata.="<td align='center'>{$FPXresult['FPX']}</td>";
				    $tabledata.="<td align='center'>{$CreditCardresult['CreditCard']}</td>";
				    $tabledata.="<td align='center'>{$MoneyOrderresult['MoneyOrder']}</td>";
				    $tabledata.="<td align='center'>{$PostalOrderresult['PostalOrder']}</td>";
				    $tabledata.="<td colspan='2' align='center'>{$CreditBankIBFIMresult['CreditBankIBFIM']}</td>";
				  	$tabledata.="<td align='center'>{$pendingresult['Pending']}</td>";
				$tabledata.="</tr>";	
				
				$countReg = $countReg+$Regresult['CountReg'];
		     	$countPaid = $countPaid+$paidresult['Paid'];
		     	$countPending = $countPending+$pendingresult['Pending'];
		     	$countFPX = $countFPX+$FPXresult['FPX'];
		     	$countCreditcard = $countCreditcard+$CreditCardresult['CreditCard'];
		     	$countMoneyOrder = $countMoneyOrder+$MoneyOrderresult['MoneyOrder'];
		     	$countPostalOrder = $countPostalOrder+$PostalOrderresult['PostalOrder'];
		     	$countCreditBankIBFIM = $countCreditBankIBFIM+$CreditBankIBFIMresult['CreditBankIBFIM'];
		     	$countOthers = $countOthers+$Othersresult['Others'];
		     	
		   }	
			
			
			$tabledata.="<tr>";
		     	$tabledata.="
						     <td colspan='5' align='right'>Total Registered: {$countReg}</td>
						     <td align='center'>Total FPX: {$countFPX}</td>
						     <td align='center'>Total CC: {$countCreditcard}</td>
						     <td align='center'>Total MO: {$countMoneyOrder}</td>
						     <td align='center'>Total PO: {$countPostalOrder}</td>
						     <td colspan='2' align='right'>Total Others: {$countCreditBankIBFIM}</td>
						     <td align='center'>Total Pending: {$countPending}</td>
						    
						     ";
		     	$tabledata.="</tr>";
			$tabledata.="</table><br>";

		$examdateorig =  date ( "M_d_Y", strtotime ($takenexamdate) );
		$mpdf->WriteHTML($tabledata);  
		$mpdf->Output('Exam_Report_For_Exam_Date'.'_'.$examdateorig,'D');
		// Write to Logs
		$auth = Zend_Auth::getInstance();
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Exam details Report(PDF)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);
	}
 }