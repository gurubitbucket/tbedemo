<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_ExaminationpartwisenewController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
   	    $this->lobjCommon = new App_Model_Common();
	}
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjReportsForm = new  Reports_Form_Report();
		$this->lobjExaminationpartwise = new  Reports_Form_Examinationpartwise();
		$this->lobjExamreportModel = new Reports_Model_DbTable_Examreport();		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjReportsForm;
		$this->view->lobjExaminationpartwise = $this->lobjExaminationpartwise;
		$larrstudents = $this->lobjExamreportModel->fngetallnames();
		$this->lobjExaminationpartwise->Canditatename->addmultioptions($larrstudents);
		/*$larrcenters = $this->lobjExamreportModel->fngetcenternames();		
		$this->lobjExaminationpartwise->Venues->addMultiOption('','Select'); 	
		$this->lobjExaminationpartwise->Venues->addmultioptions($larrcenters);*/
		$larrcourses = $this->lobjExamreportModel->fngetprogramnames();		
		$this->lobjExaminationpartwise->Coursename->addMultiOption('','Select'); 	
		$this->lobjExaminationpartwise->Coursename->addMultioptions($larrcourses);
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->attendancereportpaginatorresult);
		$lintpagecount = $this->gintPageCount;		
		$lintpage = $this->_getParam('page',1);//Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
	    {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->attendancereportpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		    if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {
			        $larrformData = $this->_request->getPost ();
			        $fromdate = $this->view->fromdate= $larrformData['Date7'];
					$todate = $this->view->todate= $larrformData['Date8'];
					$cname = $this->view->cname= $larrformData['Coursename'];	
					$venue = $this->view->venue= $larrformData['Venues'];
					$icno = $this->view->icno= $larrformData['ICNO'];
					$candidatename = $this->view->name= $larrformData['Canditatename'];
				if ($this->lobjform->isValid($larrformData)) 
				{
						$larrformData = $this->_request->getPost ();
						unset ( $larrformData ['Search'] );
						$result = $this->lobjExamreportModel->fngetnewpartwisedetails($larrformData); //searching the values for the attendance
						$count=count($result);
	 				    for($i=0;$i<$count;$i++){
	 				    	if($result[$i]['IdProgrammaster']==1){
	 				    		$result[$i]['Part1']= 'A';
	 				    		$result[$i]['Part2']= 'B';
	 				    	}
	 				    	elseif($result[$i]['IdProgrammaster']==2){
	 				    		$result[$i]['Part1']= 'A';
	 				    		$result[$i]['Part2']= 'C';
	 				    		$result[$i]['partB']= $result[$i]['partC'];
	 				    	}
	 				        elseif($result[$i]['IdProgrammaster']==3){
	 				    		$result[$i]['Part1']= 'B';
	 				    		$result[$i]['partA']= $result[$i]['partB'];
	 				    		$result[$i]['partB']= 'NA';
	 				    		$result[$i]['Part2']= 'NA';
	 				    	}
	 				    elseif($result[$i]['IdProgrammaster']==4){
	 				    		$result[$i]['Part1']= 'C';
	 				    		$result[$i]['partA']= $result[$i]['partC'];
	 				    		$result[$i]['partB']= 'NA';
	 				    		$result[$i]['Part2']= 'NA';
	 				    	}
	 				    	$this->view->count = $count;
	 				    	$this->view->total = 'GRAND TOTAL';
	 				    }
					$this->view->paginator = $this->lobjCommon->fnPagination($result,$lintpage,$lintpagecount);
					$this->gobjsessionsis->attendancereportpaginatorresult = $result;
					$larrcenters = $this->lobjExamreportModel->fnajaxgetcenternames($larrformData['Date7'],$larrformData['Date8']);
					$this->lobjExaminationpartwise->Venues->addMultiOption('','Select'); 	
					$this->lobjExaminationpartwise->Venues->addmultioptions($larrcenters);
					$this->lobjExaminationpartwise->Venues->setValue($larrformData['Venues']);
					$this->view->lobjExaminationpartwise->populate($larrformData);
			}
 	      }
	}
			public function pdfexportAction()
				{    
					 $this->_helper->layout->disableLayout();
					 $this->_helper->viewRenderer->setNoRender();
					 $larrformdata = array(); 
					 $fdate = $larrformdata['Date7'] = $this->_getParam('fromdate');
					 $tdate = $larrformdata['Date8'] = $this->_getParam('todate');
					 $larrformdata['Coursename'] = $this->_getParam('cname');
					 $larrformdata['Venues'] = $this->_getParam('venue');
					 $larrformdata['ICNO'] = $this->_getParam('icno');
					 $larrformdata['Canditatename'] = $this->_getParam('name');
					 $frmdate =date('d-m-Y',strtotime($fdate));
					 $todate =date('d-m-Y',strtotime($tdate));
					 $result = $this->lobjExamreportModel->fngetnewpartwisedetails($larrformdata);
					 $count=count($result);
					 for($i=0;$i<$count;$i++){
	 				    	if($result[$i]['IdProgrammaster']==1){
	 				    		$result[$i]['Part1']= 'A';
	 				    		$result[$i]['Part2']= 'B';
	 				    	}
	 				    	elseif($result[$i]['IdProgrammaster']==2){
	 				    		$result[$i]['Part1']= 'A';
	 				    		$result[$i]['Part2']= 'C';
	 				    		$result[$i]['partB']= $result[$i]['partC'];
	 				    	}
	 				        elseif($result[$i]['IdProgrammaster']==3){
	 				    		$result[$i]['Part1']= 'B';
	 				    		$result[$i]['partA']= $result[$i]['partB'];
	 				    		$result[$i]['partB']= 'NA';
	 				    		$result[$i]['Part2']= 'NA';
	 				    	}
	 				        elseif($result[$i]['IdProgrammaster']==4){
	 				    		$result[$i]['Part1']= 'C';
	 				    		$result[$i]['partA']= $result[$i]['partC'];
	 				    		$result[$i]['partB']= 'NA';
	 				    		$result[$i]['Part2']= 'NA';
	 				    	}
	 				    }
					$day= date("d-m-Y");
					$time = date('h:i:s a', time());
					include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
					$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
					$mpdf->SetDirectionality ( $this->gstrHTMLDir );
					$mpdf->text_input_as_HTML = true;
					$mpdf->useLang = true;
					$mpdf->SetAutoFont();
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumSuffix = ' / ';
					$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
					$mpdf->allow_charset_conversion = true; // Set by default to TRUE
					$mpdf->charset_in = 'utf-8';
					$ReportName = $this->view->translate( "Examination" ).' '.$this->view->translate( "Parts" ).' '.$this->view->translate( "Wise" ).' '.$this->view->translate( "Report" );
					$html = htmlspecialchars_decode ( $htmltabledata );
					ini_set('max_execution_time',3600);
					$centerarray = array();
					$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
					$tabledata.= "<br><table border=1  align=center width=100%><tr><td><b>Date </b></td><td><b>$day</b></td><td><b> Time</b></td><td><b>$time</b></td></tr></table>";
					$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
					$tabledata.= '<table border=1 align=center width=100%><tr><th><b>Program Name</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th><th><b>Candidate Name</b></th><th><b>Part</b></th><th><b>Marks</b></th><th><b>Part</b></th><th><b>Marks</b></th><th><b>Total Marks</b></th><th><b>Result</b></th><th><b>Grade</b></th></tr>';
					$filename = $ReportName.'_'.$frmdate.'_'.$todate;
					$total = "GRAND TOTAL";
					foreach($result as $lobjCountry)
						 { 
						 	if(!in_array($lobjCountry['ProgramName'],$centerarray) && $cnts != 0)
						         	 {
						         	 	 $tabledata.="<tr >									        
									        <td colspan='4' align='center'><b>TOTAL PARTWISE</b></td>
									        <td colspan='8'><b>".$sumarray['Program'][$pname]."</b></td>
									     </tr>";
						         	 }
						 	$cnts++;
						 	     $tabledata.="<tr>";
								    $tabledata.="<td>";
							         	 if(!in_array($lobjCountry['ProgramName'],$centerarray))
							         	 {
											$centerarray[] = $lobjCountry['ProgramName'];		
											$centerarrays[$lobjCountry['ProgramName']] = array();
											$tabledata.= $lobjCountry['ProgramName'];
											$pname = $lobjCountry['ProgramName'];
					  						$sumarray['Program'][$lobjCountry['ProgramName']] = 0;
										 }
										 $sumarray['Program'][$lobjCountry['ProgramName']]++;
			            			 $tabledata.="</td>";
			        
						             $tabledata.="<td>";
										 if(!in_array($lobjCountry['ExamVenue'],$centerarrays[$lobjCountry['ProgramName']]))
										 {
											$centerarrays[$lobjCountry['ProgramName']][] = $lobjCountry['ExamVenue'];
											$centerarrayss[$lobjCountry['ProgramName']][$lobjCountry['ExamVenue']] = array();
											$tabledata.=$lobjCountry['ExamVenue'];
										 }
									$tabledata.="</td>";
									$tabledata.="<td>";
										 if(!in_array($lobjCountry['ExamDate'],$centerarrayss[$lobjCountry['ProgramName']][$lobjCountry['ExamVenue']]))
										 {
											$centerarrayss[$lobjCountry['ProgramName']][$lobjCountry['ExamVenue']][] = $lobjCountry['ExamDate'];
											$tabledata.=$lobjCountry['ExamDate'];
										 }
									$tabledata.="</td>";
									$tabledata.= '<td>'.$lobjCountry['session'].'</td><td>'.$lobjCountry['FName'].'</td><td>'.$lobjCountry['Part1'].'</td><td>'.$lobjCountry['partA'].'</td><td>'.$lobjCountry['Part2'].'</td><td>'.$lobjCountry['partB'].'</td><td>'.$lobjCountry['TotalMarks'].'</td><td>'.$lobjCountry['Result'].'</td><td>'.$lobjCountry['Grade'].'</td>';
									$tabledata.="</tr>";
						  }
						  $tabledata.="<tr>
									        <td colspan='4' align='center'><b>TOTAL PARTWISE</b></td>
									        <td colspan='8'><b>".$sumarray['Program'][$pname]."</b></td>
									  </tr>";
						  
						$tabledata.="<tr><td colspan='12'> &nbsp;</td></tr>"; 
						$tabledata.="<tr>
					  				<td colspan='4' align='center'><b>{$total}</b></td>
					  				<td colspan='8'><b>{$count}</b></td>
					  			</tr>";	
					$tabledata.="</table>";
					$mpdf->WriteHTML($tabledata);
					$mpdf->Output($filename,'D');
			  }
		    public function getstudentdetailsAction()
				{    
				  $this->_helper->layout->disableLayout();
				  $this->_helper->viewRenderer->setNoRender();
				  $lintIdapp = $this->_getParam('idapp');		
				  $result = $this->lobjExamreportModel->fngetstuddetails($lintIdapp);
				  $tabledata = '';
				  foreach($result as $lobjCountry)
					{ 
					      $tabledata.= '<br><fieldset><legend align = "left"> Exam Details </legend>
					                    <table class="table" border=1 align = "center" width=100%><tr><th><b>Program</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th></tr><tr>';
						  $tabledata.= '<td align = "left">'.$lobjCountry['ProgramName'].'</td><td align = "left">'.$lobjCountry['ExamVenue'].'</td><td align = "left">'.$lobjCountry['ExamDate'].'</td><td align = "left">'.$lobjCountry['session'].'</td></tr>';
					      $tabledata.="</table></fieldset><br><br>";
					      $tabledata.="<fieldset><legend align = 'left'> Candidate Details </legend>
					         		   <table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>IdApplication</b></th><th><b>ICNO</b></th><th><b>DateOfBirth</b></th><th><b>Email</b></th></tr><tr>";
					      $tabledata.="<td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['IDApplication']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".$lobjCountry['DateOfBirth']."</td><td align = 'left'>".$lobjCountry['email']."</td></tr>";
					      $tabledata.="</table></fieldset><br>";
					      $tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";			
					}
					   echo  $tabledata; 
				}
public function fngetcentresAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$this->lobjExamreportModel = new Reports_Model_DbTable_Examreport();
		$ldfromdate = $this->_getParam('regFromDate');
		$ldtodate = $this->_getParam('toDate');
		$larrcenters = $this->lobjExamreportModel->fnajaxgetcenternames($ldfromdate,$ldtodate);
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}
}	
 