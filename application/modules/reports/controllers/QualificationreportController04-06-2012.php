<?php
class Reports_QualificationreportController extends Base_Base {
	
	//private $_gobjlogger; 
	public function init() 
	{
		//$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjQualificationReportForm = new  Reports_Form_QualificationReport();  //creating object of QualificationReport form
		$this->lobjQualificationReportModel = new Reports_Model_DbTable_QualificationReport();		//creating object of QualificationReport model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
    
	public function indexAction() 
	{
		$this->view->lobjform = $this->lobjQualificationReportForm;	//send the lobjQualificationReportForm object to the view
		/*if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    { 
		        $larrformData = $this->_request->getPost ();
				if ($this->lobjform->isValid($larrformData)) 
				{
				 $larrformData = $this->_request->getPost ();
				 $ldtdate = $larrformData['Monthyear'];//get monthyear of report to be Generate*/
		         $ldtdate = $this->_getParam('ldtdate');
				 $this->view->Monthyear = $ldtdate;
				 $larrdates =explode("-",$ldtdate);
			     $lintYear=$larrdates[0];
			     $lintmonth=$larrdates[1];
				 $lstrmonthname = date( 'F', mktime(0, 0, 0, $lintmonth));
				 $this->view->month = $lstrmonthname;
				 $this->view->Year = $lintYear;
				 // unset ( $larrformData ['Search']);
				 $larrqualificationcount = $this->lobjQualificationReportModel->fngetqualification($lintmonth,$lintYear); //Call a function to get number of candidates registered for different qualification
				 $lintregisteredtotal = $larrqualificationcount[0]['NoOfCandidates']+$larrqualificationcount[1]['NoOfCandidates']+$larrqualificationcount[2]['NoOfCandidates']+$larrqualificationcount[3]['NoOfCandidates']+$larrqualificationcount[4]['NoOfCandidates'];
				 $this->view->regtotal = $lintregisteredtotal;
				 $lintDiplomaid=167;
				 $larrDiplomaresult = $this->lobjQualificationReportModel->fngetqualificationresult($lintmonth,$lintYear,$lintDiplomaid); //Call a function to get number of Diploma candidates details
				 $lintcountDiploma = count($larrDiplomaresult);
				 $lintpass = 0;$lintfail = 0;$lintabsent=0;
				 for($linti=0;$linti<$lintcountDiploma;$linti++){
				 	if($larrDiplomaresult[$linti]['pass']==1)
				 	{
				 		$lintpass++;
				 	}elseif ($larrDiplomaresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrqualificationcount[]['Diplomasat'] = $lintpass+$lintfail; //number of Diploma candidates attended 
				 $larrqualificationcount[]['Diplomapass'] = $lintpass; //number of Diploma candidates pass
				 $larrqualificationcount[]['Diplomafail']= $lintfail; //number of Diploma candidates fail
				 $larrqualificationcount[]['Diplomaabsent']= $larrqualificationcount[0]['NoOfCandidates']-$lintcountDiploma;//$lintabsent; //number of Diploma candidates absent
				 $lintDegreeid=168;
				 $larrDegreeresult = $this->lobjQualificationReportModel->fngetqualificationresult($lintmonth,$lintYear,$lintDegreeid); //Call a function to get number of Degree candidates details
				 $lintDegreecount = count($larrDegreeresult);
				 $lintpass = 0;$lintfail = 0;$lintabsent=0;
				 for($linti=0;$linti<$lintDegreecount;$linti++){
				 	if($larrDegreeresult[$linti]['pass']==1)
				 	{
				 		$lintpass++;
				 	}elseif ($larrDegreeresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrqualificationcount[]['Degreesat'] = $lintpass+$lintfail; //number of Degree candidates attended
				 $larrqualificationcount[]['Degreepass'] = $lintpass; //number of Degree candidates pass
				 $larrqualificationcount[]['Degreefail']= $lintfail; //number of Degree candidates fail
				 $larrqualificationcount[]['Degreeabsent']= $larrqualificationcount[1]['NoOfCandidates']-$lintDegreecount; //$lintabsent; //number of Degree candidates absent
				 $lintSPMid=170;
				 $larrSPMresult = $this->lobjQualificationReportModel->fngetqualificationresult($lintmonth,$lintYear,$lintSPMid); //Call a function to get number of SPM candidates details
				 $lintSPMcount = count($larrSPMresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent=0;
				 for($linti=0;$linti<$lintSPMcount;$linti++){
				 	if($larrSPMresult[$linti]['pass']==1)
				 	{
				 		$lintpass++;
				 	}elseif ($larrSPMresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrqualificationcount[]['SPMsat'] = $lintpass+$lintfail; //number of SPM candidates attended
				 $larrqualificationcount[]['SPMpass'] = $lintpass; //number of SPM candidates pass
				 $larrqualificationcount[]['SPMfail']= $lintfail; //number of SPM candidates fail
				 $larrqualificationcount[]['SPMabsent']= $larrqualificationcount[2]['NoOfCandidates']-$lintSPMcount;//$lintabsent; //number of SPM candidates absent
				 $lintSTPMid=171;
				 $larrSTPMresult = $this->lobjQualificationReportModel->fngetqualificationresult($lintmonth,$lintYear,$lintSTPMid); //Call a function to get number of STPM candidates details
				 $lintSTPMcount = count($larrSTPMresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent=0;
				 for($linti=0;$linti<$lintSTPMcount;$linti++){
				 	if($larrSTPMresult[$linti]['pass']==1)
				 	{
				 		$lintpass++;
				 	}elseif ($larrSTPMresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrqualificationcount[]['STPMsat'] = $lintpass+$lintfail; //number of STPM candidates attended
				 $larrqualificationcount[]['STPMpass'] = $lintpass; //number of STPM candidates pass
				 $larrqualificationcount[]['STPMfail']= $lintfail; //number of STPM candidates pass
				 $larrqualificationcount[]['STPMabsent']= $larrqualificationcount[3]['NoOfCandidates']-$lintSTPMcount;//$lintabsent;  //number of STPM candidates absent
				 $lintotherid=183;
				 $larrotherresult = $this->lobjQualificationReportModel->fngetqualificationresult($lintmonth,$lintYear,$lintotherid); //Call a function to get number of other candidates details
				 $lintothercount = count($larrotherresult);
				 $lintpass = 0;
				 $lintfail = 0;
				 $lintabsent=0;
				 for($linti=0;$linti<$lintothercount;$linti++){
				 	if($larrotherresult[$linti]['pass']==1)
				 	{
				 		$lintpass++;
				 	}elseif ($larrotherresult[$linti]['pass']==2){
				 		$lintfail++;
				 	}else{
				 		$lintabsent++;
				 	}
				 }
				 $larrqualificationcount[]['otherssat'] = $lintpass+$lintfail; //number of others candidates attended
				 $larrqualificationcount[]['otherspass'] = $lintpass; //number of others candidates pass
				 $larrqualificationcount[]['othersfail']= $lintfail;  //number of others candidates pass
				 $larrqualificationcount[]['othersabsent']= $larrqualificationcount[4]['NoOfCandidates']-$lintothercount;//$lintabsent;  //number of others candidates absent
				 $larrqualificationcount[]['sattotal']=  $larrqualificationcount[13]['SPMsat']+$larrqualificationcount[17]['STPMsat']+$larrqualificationcount[5]['Diplomasat']+$larrqualificationcount[9]['Degreesat']+$larrqualificationcount[21]['otherssat']; //Total number of candidates attended
				 $larrqualificationcount[]['passtotal']=  $larrqualificationcount[14]['SPMpass']+$larrqualificationcount[18]['STPMpass']+$larrqualificationcount[6]['Diplomapass']+$larrqualificationcount[10]['Degreepass']+$larrqualificationcount[22]['otherspass']; //Total number of candidates pass
				 $larrqualificationcount[]['failtotal']=  $larrqualificationcount[15]['SPMfail']+$larrqualificationcount[19]['STPMfail']+$larrqualificationcount[7]['Diplomafail']+$larrqualificationcount[11]['Degreefail']+$larrqualificationcount[23]['othersfail']; //Total number of candidates fail
				 $larrqualificationcount[]['absenttotal']=  $larrqualificationcount[16]['SPMabsent']+$larrqualificationcount[20]['STPMabsent']+$larrqualificationcount[8]['Diplomaabsent']+$larrqualificationcount[12]['Degreeabsent']+$larrqualificationcount[24]['othersabsent']; //Total number of candidates absent
				 $this->view->larrqualificationcount   =  $larrqualificationcount; //sending all count of candidates to the view
			     $this->view->paginator = $larrqualificationcount;
				 //$this->view->lobjform->populate($larrformData);
			//}
		    //}	
	}
	
	public function fnexportexcelAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		unset ( $larrformData ['ExportToExcel'] );
		$lday= date("d-m-Y");
		$ltime = date('h:i:s',time());
		$host = $_SERVER['SERVER_NAME']; //Host address
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg"; //Image
		$filename = 'Qualification_Statistics_Report_'.$larrformData['selmonth'];//FileName
		$ReportName = $this->view->translate( "Qualification" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		$tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
		$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 3><b>Date </b></td><td align ='left' colspan = 4><b>$lday</b></td><td align ='left' colspan = 3><b> Time</b></td><td align ='left' colspan = 4><b>$ltime</b></td></table>";
		$tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 14 align=center><b>{$ReportName}</b></td></tr></table><br>";
		$tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 12><b>".$larrformData['selmonth']."</b></td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Qualification</b></td><td align ='center' colspan = 2><b>SPM</b></td><td align ='center' colspan = 2><b>STPM</b></td><td align ='center' colspan = 2><b>DIPLOMA</b></td><td align ='center' colspan = 2><b>DEGREE</b></td><td align ='center' colspan = 2><b>OTHERS</b></td><td align ='center' colspan = 2><b>TOTAL</b></td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Registered</b></td><td align ='center' colspan = 2>".$larrformData['spmcand']."</td><td align ='center' colspan = 2>".$larrformData['stpmcand']."</td><td align ='center' colspan = 2>".$larrformData['diplomacand']."</td><td align ='center' colspan = 2>".$larrformData['degreecand']."</td><td align ='center' colspan = 2>".$larrformData['othercand']."</td><td align ='center' colspan = 2>".$larrformData['regtotal']."</td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Sat</b></td><td align ='center' colspan = 2>".$larrformData['SPMsat']."</td><td align ='center' colspan = 2>".$larrformData['STPMsat']."</td><td align ='center' colspan = 2>".$larrformData['Diplomasat']."</td><td align ='center' colspan = 2>".$larrformData['Degreesat']."</td><td align ='center' colspan = 2>".$larrformData['otherssat']."</td><td align ='center' colspan = 2>".$larrformData['sattotal']."</td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Pass</b></td><td align ='center' colspan = 2>".$larrformData['SPMpass']."</td><td align ='center' colspan = 2>".$larrformData['STPMpass']."</td><td align ='center' colspan = 2>".$larrformData['Diplomapass']."</td><td align ='center' colspan = 2>".$larrformData['Degreepass']."</td><td align ='center' colspan = 2>".$larrformData['otherspass']."</td><td align ='center' colspan = 2>".$larrformData['passtotal']."</td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Fail</b></td><td align ='center' colspan = 2>".$larrformData['SPMfail']."</td><td align ='center' colspan = 2>".$larrformData['STPMfail']."</td><td align ='center' colspan = 2>".$larrformData['Diplomafail']."</td><td align ='center' colspan = 2>".$larrformData['Degreefail']."</td><td align ='center' colspan = 2>".$larrformData['othersfail']."</td><td align ='center' colspan = 2>".$larrformData['failtotal']."</td></tr>";
		$tabledata.="<tr> <td align ='left' colspan = 2><b>Absent</b></td><td align ='center' colspan = 2>".$larrformData['SPMabsent']."</td><td align ='center' colspan = 2>".$larrformData['STPMabsent']."</td><td align ='center' colspan = 2>".$larrformData['Diplomaabsent']."</td><td align ='center' colspan = 2>".$larrformData['Degreeabsent']."</td><td align ='center' colspan = 2>".$larrformData['othersabsent']."</td><td align ='center' colspan = 2>".$larrformData['absenttotal']."</td></tr>";
		$ourFileName = realpath('.')."/data"; 
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); //open a file to write a text
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));//write the content of htmlcode into text file
		fclose($ourFileHandle); //closeing a file 
		header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
		header("Content-Disposition: attachment; filename=$filename.xls");
		readfile($ourFileName);
		unlink($ourFileName);
	}
	
		
}