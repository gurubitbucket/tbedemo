<?php
ini_set('display_errors','On');
ini_set('memory_limit','-1');
class Reports_ExamquestionsreportController extends Base_Base 
{	
    public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjExammarksModel = new Reports_Model_DbTable_Examquestionsmodel();
		$this->lobjExammarksform = new  Reports_Form_Examquestionsform();
	}
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjExammarksform;
		for($linti=1;$linti<=1000;$linti++){
			$larrrand[$linti] = $linti;
		}
		$this->lobjExammarksform->Marksfrom->addMultiOptions($larrrand);
		$this->lobjExammarksform->Marksto->addMultiOptions($larrrand);
		 if ($this->_request->isPost () && $this->_request->getPost('Search')) 
		{   
			    $larrformData = $this->_request->getPost();
			    $fromqtn = $larrformData['Marksfrom'];
			    $toqtn = $larrformData['Marksto'];
			    $this->_redirect(  $this->baseUrl  . "/reports/examquestionsreport/fnexport/fromqtn/$fromqtn/toqtn/$toqtn");
		}
    }
	
	public function fnexportAction()
    {   
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$fromqtn = $this->_getParam('fromqtn');
		$toqtn = $this->_getParam('toqtn');
		$displayquestions = $this->lobjExammarksModel->fndisplayquestionsforabovecode($fromqtn,$toqtn);
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Question" ).' '.$this->view->translate( "Paper" );
		ini_set('max_execution_time',3600);
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table width=100%><tr><td><b>Questions From &nbsp;:</b></td><td align = "left">'.$fromqtn.'</td><td><b>Questions To &nbsp;:</b></td><td align = "left">'.$toqtn.'</td><td></td><td></td></tr></table>';
		$tabledata.='<hr></hr><br>';
		  for($i=1,$s=0;$i<=count($displayquestions),$s<count($displayquestions);$s++,$i++)
		  { 
	  				$tabledata.='<table ><tr>
     				<td valign="top" >'.'Q'.$i.'.'.'&nbsp;'.$displayquestions[$s]['idquestions'].')'.' </td><td align="left">'.trim($displayquestions[$s]['Question']).'<br></td>
	  				</tr></table><br>';
		            	$larranswers = $this->lobjExammarksModel->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
		            	for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++)
		            	{
		            		
							$tabledata.='<table>
											<tr>
												<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$i.'.'.$larranswers[$a]['idanswers'].')'.'</td>
												<td>'.$larranswers[$a]['answers'].'<br></td>';
							if($larranswers[$a]['CorrectAnswer']==1)
							{
								$tabledata.='<td style="background-color:green;">Correct Answer</td>';
							}
							$tabledata.='</tr>  
										 </table><br>';
		       			}
		       
		}
		$mpdf->WriteHTML($tabledata);   
		$mpdf->Output('Questions_Report.pdf','D');
    }
}