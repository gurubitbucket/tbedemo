<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_ExamresultstasticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjStasticsForm = new  Reports_Form_Statistics();
		$this->lobjstastics = new Reports_Model_DbTable_Examresultstastics();		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjStasticsForm;
		   /* if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		    {   
		    	$larrformData = $this->_request->getPost ();
				if ($this->lobjform->isValid($larrformData)) 
				{
				 $larrformData = $this->_request->getPost ();
				 $ldtdate = $larrformData['Date'];*/
		         $ldtdate = $this->_getParam('ldtdate');
				 $this->view->searchdate = $ldtdate;
				 $larrdates = explode('-',$ldtdate);
				 $lintmon = $larrdates['1'];
				 $lintyear = $larrdates['0'];
				 $lstrmonthname = date( 'F', mktime(0,0,0,$lintmon));
				 $lintpartabid = 1;$lintpartacid = 2;$lintpartbid = 3;$lintpartcid = 4;
				 $this->view->month = $lstrmonthname;
				 $this->view->year = $lintyear;
				 unset($larrformData ['Search']);
				 $larrprogramcountresult = $this->lobjstastics->fngetabsentdetails($lintmon,$lintyear);
				 $larrpartabresult = $this->lobjstastics->fngetresultgrade($lintmon,$lintyear,$lintpartabid);
				 $lintpartabcount = count($larrpartabresult);
				 $lintgradeA = 0;$lintgradeB = 0; $lintgradeC = 0;$lintgradeF = 0;
				 for($linti=0;$linti<$lintpartabcount;$linti++){
				 	if($larrpartabresult[$linti]['Grade']=='A')
				 	{
				 		$lintgradeA++;
				 	}elseif($larrpartabresult[$linti]['Grade']=='B'){
				 		$lintgradeB++;
				 	}elseif($larrpartabresult[$linti]['Grade']=='C'){
				 		$lintgradeC++;
				 	}else{
				 		$lintgradeF++;
				 	}
				 }
				 $larrprogramcount['ABGradeA'] = $lintgradeA;
				 $larrprogramcount['ABGradeB'] = $lintgradeB;
				 $larrprogramcount['ABGradeC'] = $lintgradeC;
				 $larrprogramcount['ABGradeF'] = $lintgradeF;//;$lintabsent;
				 $larrprogramcount['ABGradeAbsent'] = $larrprogramcountresult[0]['NoOfCandidates']-($lintgradeA+$lintgradeB+$lintgradeC+$lintgradeF);
				 $larrpartacresult = $this->lobjstastics->fngetresultgrade($lintmon,$lintyear,$lintpartacid);
				 $lintpartaccount = count($larrpartacresult);
				 $lintgradeA = 0;$lintgradeB = 0; $lintgradeC = 0;$lintgradeF = 0;
				 for($linti=0;$linti<$lintpartaccount;$linti++){
				 	if($larrpartacresult[$linti]['Grade']=='A')
				 	{
				 		$lintgradeA++;
				 	}elseif($larrpartacresult[$linti]['Grade']=='B'){
				 		$lintgradeB++;
				 	}elseif($larrpartacresult[$linti]['Grade']=='C'){
				 		$lintgradeC++;
				 	}else{
				 		$lintgradeF++;
				 	}
				 }
				 $larrprogramcount['ACGradeA'] = $lintgradeA;
				 $larrprogramcount['ACGradeB'] = $lintgradeB;
				 $larrprogramcount['ACGradeC']= $lintgradeC;
				 $larrprogramcount['ACGradeF']= $lintgradeF;//$lintabsent;
				 $larrprogramcount['ACGradeAbsent'] = $larrprogramcountresult[1]['NoOfCandidates']-($lintgradeA+$lintgradeB+$lintgradeC+$lintgradeF);
				 $larrpartbresult = $this->lobjstastics->fngetresultgrade($lintmon,$lintyear,$lintpartbid);
				 $lintpartbcount = count($larrpartbresult);
				 $lintgradeA = 0;$lintgradeB = 0; $lintgradeC = 0;$lintgradeF = 0;
				 for($linti=0;$linti<$lintpartbcount;$linti++){
				 	if($larrpartbresult[$linti]['Grade']=='A')
				 	{
				 		$lintgradeA++;
				 	}elseif($larrpartbresult[$linti]['Grade']=='B'){
				 		$lintgradeB++;
				 	}elseif($larrpartbresult[$linti]['Grade']=='C'){
				 		$lintgradeC++;
				 	}else{
				 		$lintgradeF++;
				 	}
				 }
				 $larrprogramcount['BGradeA'] = $lintgradeA;
				 $larrprogramcount['BGradeB'] = $lintgradeB;
				 $larrprogramcount['BGradeC']= $lintgradeC;
				 $larrprogramcount['BGradeF']= $lintgradeF;//$lintabsent;
				 $larrprogramcount['BGradeAbsent'] = $larrprogramcountresult[2]['NoOfCandidates']-($lintgradeA+$lintgradeB+$lintgradeC+$lintgradeF);
				 $larrpartcresult = $this->lobjstastics->fngetresultgrade($lintmon,$lintyear,$lintpartcid);
				 $lintpartccount = count($larrpartcresult);
				 $lintgradeA = 0; $lintgradeB = 0; $lintgradeC = 0;$lintgradeF = 0;
				 for($linti=0;$linti<$lintpartccount;$linti++){
				 	if($larrpartcresult[$linti]['Grade']=='A')
				 	{
				 		$lintgradeA++;
				 	}elseif($larrpartcresult[$linti]['Grade']=='B'){
				 		$lintgradeB++;
				 	}elseif($larrpartcresult[$linti]['Grade']=='C'){
				 		$lintgradeC++;
				 	}else{
				 		$lintgradeF++;
				 	}
				 }
				 $larrprogramcount['CGradeA'] = $lintgradeA;
				 $larrprogramcount['CGradeB'] = $lintgradeB;
				 $larrprogramcount['CGradeC']= $lintgradeC;
				 $larrprogramcount['CGradeF']= $lintgradeF;//$lintabsent;
				 $larrprogramcount['CGradeAbsent'] = $larrprogramcountresult[3]['NoOfCandidates']-($lintgradeA+$lintgradeB+$lintgradeC+$lintgradeF); 
				 $larrprogramcount['PartABtotal']=  $larrprogramcount['ABGradeA']+$larrprogramcount['ABGradeB']+$larrprogramcount['ABGradeC']+$larrprogramcount['ABGradeF']+$larrprogramcount['ABGradeAbsent'];
				 $larrprogramcount['PartACtotal']=  $larrprogramcount['ACGradeA']+$larrprogramcount['ACGradeB']+$larrprogramcount['ACGradeC']+$larrprogramcount['ACGradeF']+$larrprogramcount['ACGradeAbsent'];
				 $larrprogramcount['PartBtotal']=  $larrprogramcount['BGradeA']+$larrprogramcount['BGradeB']+$larrprogramcount['BGradeC']+$larrprogramcount['BGradeF']+$larrprogramcount['BGradeAbsent'];
				 $larrprogramcount['PartCtotal']=  $larrprogramcount['CGradeA']+$larrprogramcount['CGradeB']+$larrprogramcount['CGradeC']+$larrprogramcount['CGradeF']+$larrprogramcount['CGradeAbsent'];
				 $this->view->larrracescount = $larrprogramcount;
			     $this->view->paginator = $larrprogramcount;
				 //$this->view->lobjform->populate($larrformData);
			//}
 	      //}
	}
			public function fnexportexcelAction()
				{    
					 $this->_helper->layout->disableLayout();
					 $this->_helper->viewRenderer->setNoRender();
					 $larrformData = $this->_request->getPost();
					 unset ( $larrformData ['ExportToExcel'] );
					 $lday= date("d-m-Y");
					 $ltime = date('h:i:s',time());
					 $host = $_SERVER['SERVER_NAME'];
					 $imgp = "http://".$host."/tbenew/images/reportheader.jpg";
					 $filename = 'Examresult_Statistics_Report_'.$larrformData['selmonth'];
					 $ReportName = $this->view->translate( "Exam Result").' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
					 $tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 3><b>Date </b></td><td align ='left' colspan = 3><b>$lday</b></td><td align ='left' colspan = 3><b> Time</b></td><td align ='left' colspan = 5><b>$ltime</b></td></tr></table>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 14 align=center><b>{$ReportName}</b></td></tr></table><br>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 12><b>".$larrformData['selmonth']."</b></td></tr>";
					 $tabledata.="<tr> <td align ='left' colspan = 2><b>Examcategory/Result</b></td><td align ='center' colspan = 6><b>Pass</b></td><td align ='center' colspan = 2><b>Fail</b></td><td align ='center' colspan = 2><b>Absent</b></td><td align ='center' colspan = 2><b>Total</b></td></tr>";
					 $tabledata.="<tr> <td align ='left' colspan = 2><b></b></td><td align ='center' colspan = 2><b>A</b></td><td align ='center' colspan = 2><b>B</b></td><td align ='center' colspan = 2><b>C</b></td><td align ='center' colspan = 2><b>F</b></td><td align ='center' colspan = 2><b>X</b></td><td align ='center' colspan = 2></td></tr>";
					 $tabledata.="<tr> <td align ='left' colspan = 2><b>A and B</b></td><td align ='center' colspan = 2>".$larrformData['PartABGradeA']."</td><td align ='center' colspan = 2>".$larrformData['PartABGradeB']."</td><td align ='center' colspan = 2>".$larrformData['PartABGradeC']."</td><td align ='center' colspan = 2>".$larrformData['PartABGradeF']."</td><td align ='center' colspan = 2>".$larrformData['PartABAbsent']."</td><td align ='center' colspan = 2>".$larrformData['PartABtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>A and C</b></td><td align ='center' colspan = 2>".$larrformData['PartACGradeA']."</td><td align ='center' colspan = 2>".$larrformData['PartACGradeB']."</td><td align ='center' colspan = 2>".$larrformData['PartACGradeC']."</td><td align ='center' colspan = 2>".$larrformData['PartACGradeF']."</td><td align ='center' colspan = 2>".$larrformData['PartACAbsent']."</td><td align ='center' colspan = 2>".$larrformData['PartACtotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>B only</b></td><td align ='center' colspan = 2>".$larrformData['PartBGradeA']."</td><td align ='center' colspan = 2>".$larrformData['PartBGradeB']."</td><td align ='center' colspan = 2>".$larrformData['PartBGradeC']."</td><td align ='center' colspan = 2>".$larrformData['PartBGradeF']."</td><td align ='center' colspan = 2>".$larrformData['PartBAbsent']."</td><td align ='center' colspan = 2>".$larrformData['PartBTotal']."</td></tr>";
        			 $tabledata.="<tr> <td align ='left' colspan = 2><b>C only</b></td><td align ='center' colspan = 2>".$larrformData['PartCGradeA']."</td><td align ='center' colspan = 2>".$larrformData['PartCGradeB']."</td><td align ='center' colspan = 2>".$larrformData['PartCGradeC']."</td><td align ='center' colspan = 2>".$larrformData['PartCGradeF']."</td><td align ='center' colspan = 2>".$larrformData['PartCAbsent']."</td><td align ='center' colspan = 2>".$larrformData['PartCtotal']."</td></tr>";				
        			 $ourFileName = realpath('.')."/data";
					 $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
					 ini_set('max_execution_time', 3600);
					 fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
					 fclose($ourFileHandle);
					 header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
					 header("Content-Disposition: attachment; filename=$filename.xls");
					 header("Pragma: no-cache");
					 header("Expires: 0");
					 readfile($ourFileName);
					 unlink($ourFileName);
				}	
}	
 