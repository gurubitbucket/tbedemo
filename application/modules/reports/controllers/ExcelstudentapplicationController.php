<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_ExcelstudentapplicationController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjexlstuForm = new  Reports_Form_Excelstudentapplicationform();
		$this->lobjexlstumodel = new Reports_Model_DbTable_Excelstudentapplicationmodel();
	}
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjexlstuForm;
		$larrcourses=$this->lobjexlstumodel->fngetprogramnames();
		$this->lobjexlstuForm->Coursename->addMultiOption('','Select');
		$this->lobjexlstuForm->Coursename->addmultioptions($larrcourses);
		$larrcentres = $this->lobjexlstumodel->fngetcenternames();
		$this->lobjexlstuForm->Venues->addMultiOption('','Select'); 	
		$this->lobjexlstuForm->Venues->addmultioptions($larrcentres);
		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->attendancereportpaginatorresult);
		$lintpagecount = 10000;
		$lintpage = $this->_getParam('page',1); //Paginator instance
		$result = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->attendancereportpaginatorresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($result,$lintpage,$lintpagecount);
		}

		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			$fromdate = $this->view->fromdate=$larrformData['ExamfromDate'];
			$todate = $this->view->todate=$larrformData['ExamtoDate'];
			$Appfromdate = $this->view->appfromdate=$larrformData['Appliedfrom'];
			$Apptodate = $this->view->apptodate=$larrformData['Appliedto'];
			$cname = $this->view->cname=$larrformData['Coursename'];
			$venue = $this->view->venue=$larrformData['Venues'];
			$name = $this->view->name = $larrformData['Studentname'];
			$icno = $this->view->ICNO = $larrformData['ICNO'];
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost ();
				$result = $this->lobjexlstumodel->fngetexlstuappdetails($larrformData);
			}
			$this->view->lintcount = $lintcount = count($result);
			$this->view->paginator = $this->lobjCommon->fnPagination($result,$lintpage,$lintpagecount);
			$this->gobjsessionsis->attendancereportpaginatorresult = $result;
			$this->view->lobjform->populate($larrformData);
		}
	}
	public function fngetcentresAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$ldfromdate = $this->_getParam('regFromDate');
		$ldtodate = $this->_getParam('toDate');
		$larrcenters = $this->lobjexlstumodel->fnajaxgetcenternames($ldfromdate,$ldtodate);
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}
	public function fnpdfexportAction()
	{   
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformdata = $this->_request->getPost();
		$day= date("d-m-Y");
	    if($larrformdata['Venues'])
		{
			$larrcentre = $this->lobjexlstumodel->fngetcentername($larrformdata['Venues']);
			$larrformdata['CentreName'] = $larrcentre['centername'];
		}else{
			$larrformdata['CentreName'] = 'ALL';
		}
		if($larrformdata['Coursename'])
		{
			$larrprogram = $this->lobjexlstumodel->fngetprgrname($larrformdata['Coursename']);
			$larrformdata['ProgramName'] = $larrprogram['ProgramName'];
		}else{
			$larrformdata['ProgramName'] = 'ALL';
		}
		$examfdate = $larrformdata['ExamfromDate'];
		$examtdate = $larrformdata['ExamtoDate'];
		if($larrformdata['ExamfromDate']){
		$frmdate = date('d-m-Y',strtotime($examfdate));
		}else{
			$frmdate = 'ALL';
		}
		if($larrformdata['ExamtoDate']){
		$todate = date('d-m-Y',strtotime($examtdate));
		}else{
			$todate = 'ALL';
		}
		
		$lstrreportytpe = $larrformdata['Print1'];
		$result = $this->lobjexlstumodel->fngetexlstuappdetails($larrformdata);
		$lintcount = count($result);
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbe/images/reportheader.jpg";
		$time = date('h:i:s',time());
		if($larrformdata['ExamfromDate']){
		$filename = 'Pull/Push_Error_Report_'.$frmdate.'_'.$todate;
		}else{
			$filename = 'Pull/Push_Error_Report_'.$day;
		}
		$ReportName = $this->view->translate( "Pull/Push Error" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Excel')
		{
			$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
		}
		else
		{
		    $tabledata = '<img width=100% src="../public/images/reportheader.jpg" /><br><br>';
		}
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 2><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 2><b> Time</b></td><td align=left colspan = 1><b>$time</b></td></tr>";
		$tabledata.= "<tr><td align=left colspan = 2><b>Exam From Date </b></td><td align=left colspan = 2><b>".$frmdate."</b></td><td  align=left colspan = 2><b> Exam To Date </b></td><td align=left colspan = 1><b>".$todate."</b></td></tr>";
		$tabledata.= "<tr><td align=left colspan = 2><b>Venue </b></td><td align=left colspan = 2><b>".$larrformdata['CentreName']."</b></td><td  align=left colspan = 2><b> Program </b></td><td align=left colspan = 1><b>".$larrformdata['ProgramName']."</b></td></tr></table>";
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 7><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.='<table border=1 align=center width=100%><tr><th><b>Student Name</b></th><th><b>ICNO</b></th><th><b>Course</b></th><th><b>Venue</b></th><th><b>Exam Date</b></th><th><b>Pushed From</b></th><th ><b>Result</b></th></tr>';
		foreach($result as $lobjCountry)
		{
			$tabledata.="<tr><td align = 'left'>".$lobjCountry['Stuname']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left' >".$lobjCountry['ProgramName']."</td><td align = 'left'>".$lobjCountry['centername']."</td><td align = 'left'>".$lobjCountry['examdate']."</td><td align = 'left'>".$lobjCountry['Pushedfrom']."</td><td  align = 'left'>".$lobjCountry['Result']."</td></tr>";
		}
		$tabledata.="<tr><td> </td><td> </td><td> </td><td> </td><td> </td><td  align ='right'><b> TOTAL </b></td><td><b>".$lintcount."</b></td></tr>";
		$tabledata.="</table><br>";
		
			if($lstrreportytpe=='Excel'){
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}else {
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );  
			$mpdf->allow_charset_conversion = true;  // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}
	}

}
