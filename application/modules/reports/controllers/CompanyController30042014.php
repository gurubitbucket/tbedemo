<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
class Reports_CompanyController extends Base_Base 
{
	
	public function init() 
	{


	}
    
	public function indexAction() 
	{  
		$this->view->checkEmpty = 0;

		Zend_Session:: namespaceUnset('someaction');
		
		$lobjReportsForm = new  Reports_Form_BatchExamreport();
		$lobjExamreportModel = new  Reports_Model_DbTable_BatchExamreport();
		$this->view->lobjform = $lobjReportsForm;
				
		$lobjReportsForm->Takcomp->addMultiOptions(array(''=>'Select','1'=>'Company','2'=>'Takaful'));
		$larrcourses=$lobjExamreportModel->fngetprogramnames();		
		$lobjReportsForm->Coursename->addMultiOption('','All'); 	
		$lobjReportsForm->Coursename->addmultioptions($larrcourses);
		
		$larrcenters=$lobjExamreportModel->fngetcenternames();
		$lobjReportsForm->Venues->addMultiOption('','All'); 	
		$lobjReportsForm->Venues->addmultioptions($larrcenters);
		
		//$larrcompanynames=$lobjExamreportModel->fngetcompanynames();	
		//$lobjReportsForm->Companyname->addMultiOption('','Select'); 	
		//$lobjReportsForm->Companyname->addmultioptions($larrcompanynames);
		if($this->_request->isPost() && $this->_request->getPost('Search')) 
 		{

Zend_Session:: namespaceUnset('someaction');

 			$larrformData = $this->_request->getPost();
$this->view->find =1;
			//echo "<pre>";
			//print_r($larrformData);
			$this->view->Company = $idcompany= $larrformData['Takcompnames'];	
			$this->view->compflag = $companyflag= $larrformData['Takcomp'];	
			
			$larrcenters = $lobjExamreportModel->fnajaxgetcenternames($companyflag);
				 $this->view->lobjform->Takcompnames->addmultioptions($larrcenters);
				 $this->view->lobjform->Takcompnames->setValue($larrformData['Takcompnames']);
			
			
            $larrbatchids = $lobjExamreportModel->fngetpinforbatch($companyflag,$idcompany);
			  $this->view->lobjform->Pin->addmultioption('','All');
			    $this->view->lobjform->Pin->addmultioptions($larrbatchids);
			if($larrformData['Pin'])
			{
			  
			 // echo "<pre>";
			//  print_r($larrbatchids);
			
			  //echo $larrformData['Pin'];
			  $this->view->lobjform->Pin->setvalue($larrformData['Pin']);
			}
			//echo "<pre>";
			//print_r($larrformData);die();

if(empty($larrbatchids))
			{
			   //echo "coming";
			   //$this->view->paginator ="";
			   
			}
			//echo "<pre>";
			//print_r($larrformData);die();
			else
			{
			$this->view->Takcomp = $lintidname = $larrformData['Takcomp'];
			$this->view->Student =$larrformData['Studentname'];
			$this->view->Regpin =$larrformData['Pin'];
			$this->view->Icno =$larrformData['ICNO'];
			$this->view->Course =$larrformData['Coursename'];
			$this->view->Venue =$larrformData['Venues'];
			$this->view->ExamFromdate =$larrformData['Date'];
			$this->view->ExamTodate =$larrformData['Date2'];
			$this->view->AppliedFromdate =$larrformData['AFromDate'];
			$this->view->AppliedTodate =$larrformData['AToDate'];
			
 			$result = $lobjExamreportModel->fngetcompanytakafulgroupwise($larrformData);

                 $regpindetails = $lobjExamreportModel->fngetpinforbatchess($larrformData['Takcomp'],$larrformData['Takcompnames']);	
 				
 				  $batchids="";
 				   for($reg=0;$reg<count($regpindetails);$reg++)
 				   {
 				   	if($reg==0)
 				   	{
 				   		$batchids=$regpindetails[$reg]['value'];
 				   		$batchids="'$batchids'";
 				   	}
 				   	else {
 				   		$batchidsss=$regpindetails[$reg]['value'];
 				   		$batchidsss="'$batchidsss'";
 				   		$batchids.=','.$batchidsss;
 				   		
 				   	}
 				   	
 				   }
			//echo $batchids;die();
			
			$batchidsconts= $lobjExamreportModel->fnbatchregcnt($batchids);
			$partial=array();
 			   for($regs=0;$regs<count($batchidsconts);$regs++)
 			   {
 			   	$idpin=$batchidsconts[$regs]['RegistrationPin'];
 				$partial[$idpin] =$batchidsconts[$regs]['regtotal'];
 				  
 				   	
 				   }

    $namespace = new Zend_Session_Namespace('someaction');
    				$namespace->data = $result;
			
 				 $this->view->partialreg=$partial;

 
		//$registry = new Zend_Registry(array('index' => $result));	
            $this->view->paginator = $result;
}
            $this->view->lobjform->populate($larrformData);
	    }
	}	
	public function pdfexportAction()
	{
	      $namespace = new Zend_Session_Namespace('someaction');
    $data = $namespace->data; 
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjExamreportModel = new  Reports_Model_DbTable_BatchExamreport();
		$larrformData = $this->_request->getPost();


		$fdate = $larrformData['fromdate'];
		$tdate = $larrformData['todate'];
		$Afdate = $larrformData['Afromdate'];
		$Atdate = $larrformData['Atodate'];
		$currentdate = date('d-m-Y:H:i:s');
		$frmdate =date('d-m-Y',strtotime($fdate));
		$todate =date('d-m-Y',strtotime($tdate));
		$Afrmdate =date('d-m-Y',strtotime($Afdate));
		$Atodate =date('d-m-Y',strtotime($Atdate));
           
	
		//$result = $lobjExamreportModel->fngetbatchdetails($larrformData);	
$result=$data ;

                $regpindetails = $lobjExamreportModel->fngetpinforbatchess($larrformData['Takcomp'],$larrformData['Companyname']);	
 				
 				  $batchids="";
 				   for($reg=0;$reg<count($regpindetails);$reg++)
 				   {
 				   	if($reg==0)
 				   	{
 				   		$batchids=$regpindetails[$reg]['value'];
 				   		$batchids="'$batchids'";
 				   	}
 				   	else {
 				   		$batchidsss=$regpindetails[$reg]['value'];
 				   		$batchidsss="'$batchidsss'";
 				   		$batchids.=','.$batchidsss;
 				   		
 				   	}
 				   	
 				   }
			//echo $batchids;die();
			
			$batchidsconts= $lobjExamreportModel->fnbatchregcnt($batchids);
			$partial=array();
 			   for($regs=0;$regs<count($batchidsconts);$regs++)
 			   {
 			   	$idpin=$batchidsconts[$regs]['RegistrationPin'];
 				$partial[$idpin] =$batchidsconts[$regs]['regtotal'];
 				  
 				   	
 				   }

		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";		
		$time = date('h:i:s',time());
		if($larrformData['Takcomp']==1)
		{
		$filename = 'Company_Report'.$currentdate;
		$ReportName = $this->view->translate( "Company" ).' '.$this->view->translate( "Report" );
	    }
	    else
	    {
			$filename = 'Takaful_Operator_Report'.$currentdate;
			$ReportName = $this->view->translate( "Takaful" ).' '.$this->view->translate( "Operator" ).' '.$this->view->translate( "Report" );
		}
		
		//$ReportName = $this->view->translate( "Company" ).' '.$this->view->translate( "Report" );		
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	

		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 8><b> {$ReportName}</b></td></tr></table><br>";
        $tabledata.= "<table border=1  align=center width=100%>";

                if($larrformData['Takcomp']==1)
		{
		       $tabledata.= "<tr>
		                  <td align=left colspan = 2><b>Type</b></td>
						  <td align=left colspan = 2><b>Company</b></td>";
				
		               $tabledata.= "</tr>";
		          
		}
               if($larrformData['Takcomp']==2)
		{
		       $tabledata.= "<tr>
		                  <td align=left colspan = 2><b>Type</b></td>
						  <td align=left colspan = 2><b>Takaful</b></td>";
				
		               $tabledata.= "</tr>";
		          
		}
                 $Companyname = $lobjExamreportModel->fngetcompanyname($larrformData['Takcomp'],$larrformData['Companyname']);
                 $Coursename  = $lobjExamreportModel->fngetprogramname($larrformData['course']);
                 $Venuename  = $lobjExamreportModel->fngetvenuename($larrformData['venue']);

                if($larrformData['Companyname'])
		{
		                 $tabledata.= "<tr>
		                  <td align=left colspan = 2><b>Name</b></td>
						  <td align=left colspan = 2><b>".$Companyname['Company']."</b></td>";
				
                                 if($larrformData['student'])
                                  {
                                        $tabledata.="<td align=left colspan = 2><b>Student</b></td>
						  <td align=left colspan = 2><b>".$larrformData['student']."</b></td>";
                                  }
		                 $tabledata.= "</tr>";
                                 $tabledata.= "<tr>";
                                 if($larrformData['companypin'])
		                 {
                                     
                                     $tabledata.="<td align=left colspan = 2><b>BatchId</b></td>
						  <td align=left colspan = 2><b>"."&nbsp;".$larrformData['companypin']."</b></td>";
                                 }
                                 else
                                 {
                                    $tabledata.="<td align=left colspan = 2><b>BatchId</b></td>
						  <td align=left colspan = 2><b>All</b></td>";
                                 }
                                 if($larrformData['icno'])
                                 {
                                     $tabledata.="<td align=left colspan = 2><b>ICNO</b></td>
						  <td align=left colspan = 2><b>".$larrformData['icno']."</b></td>";
                                 }
                                 $tabledata.= "</tr>";
                                 $tabledata.= "<tr>";
                                 if($larrformData['course'])
		                 {
                                     
                                     $tabledata.="<td align=left colspan = 2><b>Course</b></td>
						  <td align=left colspan = 2><b>".$Coursename['ProgramName']."</b></td>";
                                 }
                                 else
                                 {
                                      $tabledata.="<td align=left colspan = 2><b>Course</b></td>
						  <td align=left colspan = 2><b>All</b></td>";
                                 }
                                 if($larrformData['venue'])
		                 {
                                     
                                     $tabledata.="<td align=left colspan = 2><b>ExamVenue</b></td>
						  <td align=left colspan = 2><b>".$Venuename['centername']."</b></td>";
                                 }
                                 else
                                 {
                                    $tabledata.="<td align=left colspan = 2><b>ExamVenue</b></td>
						  <td align=left colspan = 2><b>All</b></td>";
                                 }

                                 $tabledata.= "</tr>";                                
		          
		}
                
               
		
		if($larrformData['fromdate'])
		{
		       $tabledata.= "<tr>
		                  <td align=left colspan = 2><b>Exam From Date </b></td>
						  <td align=left colspan = 2><b>".$frmdate."</b></td>";
						  
				  if($larrformData['todate'])
		          {
		               $tabledata.= "<td  align=left colspan = 2><b> Exam To Date </b></td>
						    <td align=left colspan = 2><b>".$todate."</b></td>
					    </tr>";
		           }
				   else
				   {
				     $tabledata.="</tr>";
				   }
		}
		
                if($larrformData['Afromdate'])
                {		
		      $tabledata.= "<tr>
		                  <td align=left colspan = 2><b>Applied From Date </b></td>
						  <td align=left colspan = 2><b>".$Afrmdate."</b></td>";
					if($larrformData['Atodate'])
		            {
		              $tabledata.= "<td  align=left colspan = 2><b>Applied To Date </b></td>
						    <td align=left colspan = 2><b>".$Atodate."</b></td>
					        </tr>";
		            }
					 else
				   {
				     $tabledata.="</tr>";
				   }
		}
		$tabledata.="</table><br>";
		$tabledata.= '<table border=1 align=center width=100%>
		               <tr>
					        <th><b>BatchId</b></th><th><b>BatchIDUsed</b></th><th><b>Examdate</b></th>
							<th><b>Venue</b></th></th><th><b>Course</b></th><th><b>Session</b></th>
							<th><b>Student</b></th><th><b>ICNO</b></th><th><b>Applied Date</b></th>
							<th><b>Result</b></th><th><b>Payment Status</b></th>
					  </tr>';
		$cnts  = 0;
		$centerarray =array();
		$centerarrays =array();
		$centerarrayss =array();
		$centerarraysss =array();
		foreach($result as $lobjCountry)
		{
			    $tabledata.="<tr>";
			    
				
			    $tabledata.="<td>";				
				$tabledata.="&nbsp;".$lobjCountry['registrationPin'];
			    $tabledata.="</td>";


 $tabledata.="<td>";	
if($lobjCountry['paymentStatus'] == 2){
	$tot=$lobjCountry['totalNoofCandidates'];
$tabledata.=$tot."|".$tot;
} 
if($lobjCountry['paymentStatus'] == 1){
	if(array_key_exists($lobjCountry['registrationPin'],$partial)){
		$pin=$lobjCountry['registrationPin'];
	$tot=$lobjCountry['totalNoofCandidates'];
	$tots=$partial[$pin];
 $tabledata.="".$tots."|".$tot;
	// print_r($partialreg[$pin])."/".print_r($this->escape($lobjCountry['totalNoofCandidates']));
	}
}
				
				
			    $tabledata.="</td>";

				
			    $tabledata.="<td>";
				$tabledata.= date('d-m-Y',strtotime($lobjCountry['DateTime']));
				$tabledata.="</td>";
			
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['ExamVenue'];
				$tabledata.="</td>";
			   
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['ProgramName'];
                $tabledata.="</td>";
				
			    $tabledata.="<td>";				
				$tabledata.=$lobjCountry['managesessionname'];
			    $tabledata.="</td>";
				
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['FName'];
				$tabledata.="</td>";
			
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['ICNO'];
				$tabledata.="</td>";
				
				$tabledata.="<td>";
				$tabledata.=$lobjCountry['upddate'];
				$tabledata.="</td>";
				
				$tabledata.="<td>";				
				$tabledata.=$lobjCountry['Result'];
			    $tabledata.="</td>";
				
				if($lobjCountry['paymentStatus'] == 2)
				{
				    $tabledata.= '<td>Registration Completed</td>';
				}
				if($lobjCountry['paymentStatus'] == 1)
				{
				    $tabledata.= '<td>Partially Filled</td>';
				}
			$tabledata.="</tr>";
		}
		$tabledata.="<tr>";
		$tabledata.="<td colspan='8' align='right'>";
		$tabledata.="TOTAL:".count($result);
		$tabledata.="</td>";
		$tabledata.="</tr>";
		 $tabledata.="</table>";		
		if(isset($larrformData['pdf']))
		{
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		//$mpdf->WriteHTML('<img align=center width=100% src="../public/images/header.jpg" /><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                            '.'{PAGENO}{nbpg}' );	
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';		
		//$mpdf->WriteFixedPosHTML ( "<table border=1  align=center width=100%><tr><td align=center> {$ReportName}</td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		$html = htmlspecialchars_decode ($tabledata);
		$mpdf->WriteHTML($html);
		$mpdf->Output($filename.pdf,'D');
		// Write to Logs
			
		}
		else
		{
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
			// Write to Logs
		    /* $auth = Zend_Auth::getInstance();
		     $priority=Zend_Log::INFO;
		     $controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		     $message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Takaful Summary Report(Excel)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		      $this->_gobjlogger->log($message,5);
			  */
		}
		
	}
	public function getallregpinsforbatchAction()
	{
	   $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender ();
	   $compflag = $this->_getParam('compflag');
	   $idcompany = $this->_getParam('idcomp');
	   $lobjExamreportModel = new  Reports_Model_DbTable_BatchExamreport();
          

  
	   $regpindetails = $lobjExamreportModel->fngetpinforbatch($compflag,$idcompany);
  $regpindetails[]=array('key'=>'','value'=>'All');
	   //$regpindetails1 = array_merge($regpindetailss,$regpindetails)


	   $larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ($regpindetails);	
	   echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );
	}
	public function fnajaxgettakcompnamesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidname = $this->_getParam('idname');
		$lobjExamreportModel = new  Reports_Model_DbTable_BatchExamreport();	 
		$larrcenters = $lobjExamreportModel->fnajaxgetcenternames($lintidname);
		
		
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		
		// print_r($larrcentreDetailss);die();
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}


	 

}
