<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_ExamcentrestatisticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjStasticsForm = new  Reports_Form_Statistics();
		$this->lobjstastics = new Reports_Model_DbTable_Examcentrestastics();		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjStasticsForm;
		    /*if ($this->_request->isPost () && $this->_request->getPost('Search')) 
		    {   
		    	$larrformData = $this->_request->getPost();
				if ($this->lobjform->isValid($larrformData)) 
				{
				 $larrformData = $this->_request->getPost();
				 $ldtdate = $larrformData['Date'];*/
				 $ldtdate = $this->_getParam('ldtdate');
				 $this->view->searchdate = $ldtdate;
				 $larrdates = explode('-',$ldtdate);
				 $lintmon = $larrdates['1'];
				 $lintyear = $larrdates['0'];
				 $lstrmonthname = date( 'F', mktime(0,0,0,$lintmon));
				 $this->view->month = $lstrmonthname;
				 $this->view->year = $lintyear;
				 unset ($larrformData ['Search']);
				 $larrages = $this->lobjstastics->fngetcentres($lintmon,$lintyear);
				 
				 $this->view->larrcentredetails = $larrages;
				 //echo "<pre>";print_r($larrages);die();
				 $lintcentrecount = count($larrages);
				 for($linti=0;$linti<$lintcentrecount;$linti++){
				 	if($larrages[$linti]['idcenter']=='24'){
				 		$larragescount['WP']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['WPsat']++;	
				 		   $larragescount['WPpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['WPsat']++;	
				 		   $larragescount['WPfail']++;
				 		}else{
				 		    $larragescount['WPabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='1'){
				 		$larragescount['SA']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['SAsat']++;	
				 		   $larragescount['SApass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['SAsat']++;	
				 		   $larragescount['SAfail']++;
				 		}else{
				 		   $larragescount['SAabsent']++;
				 		}
				 	}
				 	elseif($larrages[$linti]['idcenter']=='25'){
				 		$larragescount['PJ']++;
				 	    if($larrages[$linti]['pass']==1){
				 		   $larragescount['PJsat']++;	
				 		   $larragescount['PJpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['PJsat']++;	
				 		   $larragescount['PJfail']++;
				 		}else{
				 		   $larragescount['PJabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='15'){
				 		$larragescount['SB']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['SBsat']++;	
				 		   $larragescount['SBpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['SBsat']++;	
				 		   $larragescount['SBfail']++;
				 		}else{
				 		   $larragescount['SBabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='18'){
				 		$larragescount['GH']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['GHsat']++;	
				 		   $larragescount['GHpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['GHsat']++;	
				 		   $larragescount['GHfail']++;
				 		}else{
				 		   $larragescount['GHabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='14'){
				 		$larragescount['ML']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['MLsat']++;	
				 		   $larragescount['MLpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['MLsat']++;	
				 		   $larragescount['MLfail']++;
				 		}else{
				 		   $larragescount['MLabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='21'){
				 		$larragescount['KK']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['KKsat']++;	
				 		   $larragescount['KKpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['KKsat']++;	
				 		   $larragescount['KKfail']++;
				 		}else{
				 		   $larragescount['KKabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='17'){
				 		$larragescount['IP']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['IPsat']++;	
				 		   $larragescount['IPpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['IPsat']++;	
				 		   $larragescount['IPfail']++;
				 		}else{
				 		   $larragescount['IPabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='20'){
				 		$larragescount['SJ']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['SJsat']++;	
				 		   $larragescount['SJpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['SJsat']++;	
				 		   $larragescount['SJfail']++;
				 		}else{
				 		   $larragescount['SJabsent']++;
				 		}
				 	}if($larrages[$linti]['idcenter']=='23'){
				 		$larragescount['BG']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['BGsat']++;	
				 		   $larragescount['BGpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['BGsat']++;	
				 		   $larragescount['BGfail']++;
				 		}else{
				 		    $larragescount['BGabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='11'){
				 		$larragescount['JB']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['JBsat']++;	
				 		   $larragescount['JBpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['JBsat']++;	
				 		   $larragescount['JBfail']++;
				 		}else{
				 		   $larragescount['JBabsent']++;
				 		}
				 	}
				 	elseif($larrages[$linti]['idcenter']=='16'){
				 		$larragescount['KN']++;
				 	    if($larrages[$linti]['pass']==1){
				 		   $larragescount['KNsat']++;	
				 		   $larragescount['KNpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['KNsat']++;	
				 		   $larragescount['KNfail']++;
				 		}else{
				 		   $larragescount['KNabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='27'){
				 		$larragescount['KT']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['KTsat']++;	
				 		   $larragescount['KTpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['KTsat']++;	
				 		   $larragescount['KTfail']++;
				 		}else{
				 		   $larragescount['KTabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='12'){
				 		$larragescount['BP']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['BPsat']++;	
				 		   $larragescount['BPpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['BPsat']++;	
				 		   $larragescount['BPfail']++;
				 		}else{
				 		   $larragescount['BPabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='19'){
				 		$larragescount['PP']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['PPsat']++;	
				 		   $larragescount['PPpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['PPsat']++;	
				 		   $larragescount['PPfail']++;
				 		}else{
				 		   $larragescount['PPabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='13'){
				 		$larragescount['KB']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['KBsat']++;	
				 		   $larragescount['KBpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['KBsat']++;	
				 		   $larragescount['KBfail']++;
				 		}else{
				 		   $larragescount['KBabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='22'){
				 		$larragescount['KG']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['KGsat']++;	
				 		   $larragescount['KGpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['KGsat']++;	
				 		   $larragescount['KGfail']++;
				 		}else{
				 		   $larragescount['KGabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='26'){
				 		$larragescount['MR']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['MRsat']++;	
				 		   $larragescount['MRpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['MRsat']++;	
				 		   $larragescount['MRfail']++;
				 		}else{
				 		   $larragescount['MRabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='28'){
				 		$larragescount['SN']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['SNsat']++;	
				 		   $larragescount['SNpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['SNsat']++;	
				 		   $larragescount['SNfail']++;
				 		}else{
				 		   $larragescount['SNabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='29'){
				 		$larragescount['TW']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['TWsat']++;	
				 		   $larragescount['TWpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['TWsat']++;	
				 		   $larragescount['TWfail']++;
				 		}else{
				 		   $larragescount['TWabsent']++;
				 		}
				 	}elseif($larrages[$linti]['idcenter']=='2'){
				 		$larragescount['SP']++;
				 		if($larrages[$linti]['pass']==1){
				 		   $larragescount['SPsat']++;	
				 		   $larragescount['SPpass']++;
				 		}elseif($larrages[$linti]['pass']==2){
				 		   $larragescount['SPsat']++;	
				 		   $larragescount['SPfail']++;
				 		}else{
				 		   $larragescount['SPabsent']++;
				 		}
				 	}else{
				 		
				 	}
				 }
				 $larragescount['regtotal'] = $larragescount['SA']+$larragescount['SP']+$larragescount['JB']+$larragescount['BP']+$larragescount['KB']+$larragescount['ML']+$larragescount['SB']+$larragescount['KN']+$larragescount['IP']+$larragescount['GH']+
				                              $larragescount['PP']+$larragescount['SJ']+$larragescount['KK']+$larragescount['KG']+$larragescount['BG']+$larragescount['WP']+$larragescount['PJ']+$larragescount['MR']+$larragescount['KT']+$larragescount['SN']+$larragescount['TW'];
				 $larragescount['sattotal'] = $larragescount['SAsat']+$larragescount['SPsat']+$larragescount['JBsat']+$larragescount['BPsat']+$larragescount['KBsat']+$larragescount['MLsat']+$larragescount['SBsat']+$larragescount['KNsat']+$larragescount['IPsat']+$larragescount['GHsat']+
				                              $larragescount['PPsat']+$larragescount['SJsat']+$larragescount['KKsat']+$larragescount['KGsat']+$larragescount['BGsat']+$larragescount['WPsat']+$larragescount['PJsat']+$larragescount['MRsat']+$larragescount['KTsat']+$larragescount['SNsat']+$larragescount['TWsat'];
				 $larragescount['passtotal'] = $larragescount['SApass']+$larragescount['SPpass']+$larragescount['JBpass']+$larragescount['BPpass']+$larragescount['KBpass']+$larragescount['MLpass']+$larragescount['SBpass']+$larragescount['KNpass']+$larragescount['IPpass']+$larragescount['GHpass']+
				 							   $larragescount['PPpass']+$larragescount['SJpass']+$larragescount['KKpass']+$larragescount['KGpass']+$larragescount['BGpass']+$larragescount['WPpass']+$larragescount['PJpass']+$larragescount['MRpass']+$larragescount['KTpass']+$larragescount['SNpass']+$larragescount['TWpass'];
				 $larragescount['failtotal'] = $larragescount['SAfail']+$larragescount['SPfail']+$larragescount['JBfail']+$larragescount['BPfail']+$larragescount['KBfail']+$larragescount['MLfail']+$larragescount['SBfail']+$larragescount['KNfail']+$larragescount['IPfail']+$larragescount['GHfail']+
				 							   $larragescount['PPfail']+$larragescount['SJfail']+$larragescount['KKfail']+$larragescount['KGfail']+$larragescount['BGfail']+$larragescount['WPfail']+$larragescount['PJfail']+$larragescount['MRfail']+$larragescount['KTfail']+$larragescount['SNfail']+$larragescount['TWfail'];
				 $larragescount['absenttotal'] = $larragescount['regtotal'] - $larragescount['sattotal'];
				                                //$larragescount['SAabsent']+$larragescount['SPabsent']+$larragescount['JBabsent']+$larragescount['BPabsent']+$larragescount['KBabsent']+$larragescount['MLabsent']+$larragescount['SBabsent']+$larragescount['KNabsent']+$larragescount['IPbsent']+$larragescount['GHabsent']+
				                                //$larragescount['PPabsent']+$larragescount['SJabsent']+$larragescount['KKabsent']+$larragescount['KGabsent']+$larragescount['BGabsent']+$larragescount['WPabsent']+$larragescount['PJabsent']+$larragescount['MRabsent']+$larragescount['KTabsent']+$larragescount['SNabsent']+$larragescount['TWabsent'];
				 $this->view->larrracescount = $larragescount;
			     $this->view->paginator = $larrracescount;
				 //$this->view->lobjform->populate($larrformData);
			//}
 	      //}
	}
			public function fnexportexcelAction()
				{    
					 $this->_helper->layout->disableLayout();
					 $this->_helper->viewRenderer->setNoRender();
					 $larrformData = $this->_request->getPost();
					 unset ( $larrformData['ExportToExcel']);
					 $lday= date("d-m-Y");
					 $ltime = date('h:i:s',time());
					 $host = $_SERVER['SERVER_NAME'];
					 $imgp = "http://".$host."/tbenew/images/reportheader.jpg";
					 $filename = 'Examcentre_Statistics_Report_'.$larrformData['selmonth'];
					 $ReportName = $this->view->translate( "Examcentre" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
					 $tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 5><b>Date </b></td><td align ='left' colspan = 6><b>$lday</b></td><td align ='left' colspan = 5><b> Time</b></td><td align ='left' colspan = 9><b>$ltime</b></td></tr></table>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td colspan = 25 align=center><b>{$ReportName}</b></td></tr></table><br>";
					 $tabledata.="<br><table border=1 align=center width=100%><tr><td align ='left' colspan = 2> </td><td align ='center' colspan = 23><b>".$larrformData['selmonth']."</b></td></tr>";
					 
					 $tabledata.="<tr><td align ='left' colspan = 2><b>Exam centre</b></td><td align ='center' colspan = 1><b>SA</b></td><td align ='center' colspan = 1><b>SP</b></td><td align ='center' colspan = 1><b>JB</b></td><td align ='center' colspan = 1><b>BP</b></td><td align ='center' colspan = 1><b>KB</b></td><td align ='center' colspan = 1><b>ML</b></td><td align ='center' colspan = 1><b>SB</b></td><td align ='center' colspan = 1><b>KN</b></td><td align ='center' colspan = 1><b>IP</b></td><td align ='center' colspan = 1><b>GH</b></td>
					 				  <td align ='left' colspan = 1><b>PP</b></td><td align ='center' colspan = 1><b>SJ</b></td><td align ='center' colspan = 1><b>KK</b></td><td align ='center' colspan = 1><b>KG</b></td><td align ='center' colspan = 1><b>BG</b></td><td align ='center' colspan = 1><b>WP</b></td><td align ='center' colspan = 1><b>PJ</b></td><td align ='center' colspan = 1><b>MR</b></td><td align ='center' colspan = 1><b>KT</b></td><td align ='center' colspan = 1><b>SN</b></td><td align ='center' colspan = 1><b>TW</b></td><td align ='center' colspan = 2><b>Total</b></td></tr>";
					 
        			 $tabledata.="<tr><td align ='left' colspan = 2><b>Registered</b></td><td align ='center' colspan = 1>".$larrformData['SA']."</td><td align ='center' colspan = 1>".$larrformData['SP']."</td><td align ='center' colspan = 1>".$larrformData['JB']."</td><td align ='center' colspan = 1>".$larrformData['BP']."</td><td align ='center' colspan = 1>".$larrformData['KB']."</td><td align ='center' colspan = 1>".$larrformData['ML']."</td><td align ='center' colspan = 1>".$larrformData['SB']."</td><td align ='center' colspan = 1>".$larrformData['KN']."</td><td align ='center' colspan = 1>".$larrformData['IP']."</td><td align ='center' colspan = 1>".$larrformData['GH']."</td>
        			 			      <td align ='center' colspan = 1>".$larrformData['PP']."</td><td align ='center' colspan = 1>".$larrformData['SJ']."</td><td align ='center' colspan = 1>".$larrformData['KK']."</td><td align ='center' colspan = 1>".$larrformData['KG']."</td><td align ='center' colspan = 1>".$larrformData['BG']."</td><td align ='center' colspan = 1>".$larrformData['WP']."</td><td align ='center' colspan = 1>".$larrformData['PJ']."</td><td align ='center' colspan = 1>".$larrformData['MR']."</td><td align ='center' colspan = 1>".$larrformData['KT']."</td><td align ='center' colspan = 1>".$larrformData['SN']."</td><td align ='center' colspan = 1>".$larrformData['TW']."</td><td align ='center' colspan = 2>".$larrformData['regtotal']."</td></tr>";
        			 
        			 $tabledata.="<tr><td align ='left' colspan = 2><b>Sat</b></td><td align ='center' colspan = 1>".$larrformData['SAsat']."</td><td align ='center' colspan = 1>".$larrformData['SPsat']."</td><td align ='center' colspan = 1>".$larrformData['JBsat']."</td><td align ='center' colspan = 1>".$larrformData['BPsat']."</td><td align ='center' colspan = 1>".$larrformData['KBsat']."</td><td align ='center' colspan = 1>".$larrformData['MLsat']."</td><td align ='center' colspan = 1>".$larrformData['SBsat']."</td><td align ='center' colspan = 1>".$larrformData['KNsat']."</td><td align ='center' colspan = 1>".$larrformData['IPsat']."</td><td align ='center' colspan = 1>".$larrformData['GHsat']."
        			                   </td><td align ='center' colspan = 1>".$larrformData['PPsat']."</td><td align ='center' colspan = 1>".$larrformData['SJsat']."</td><td align ='center' colspan = 1>".$larrformData['KKsat']."</td><td align ='center' colspan = 1>".$larrformData['KGsat']."</td><td align ='center' colspan = 1>".$larrformData['BGsat']."</td><td align ='center' colspan = 1>".$larrformData['WPsat']."</td><td align ='center' colspan = 1>".$larrformData['PJsat']."</td><td align ='center' colspan = 1>".$larrformData['MRsat']."</td><td align ='center' colspan = 1>".$larrformData['KTsat']."</td><td align ='center' colspan = 1>".$larrformData['SNsat']."</td><td align ='center' colspan = 1>".$larrformData['TWsat']."</td><td align ='center' colspan = 2>".$larrformData['sattotal']."</td></tr>";
        			 
        			 $tabledata.="<tr><td align ='left' colspan = 2><b>Pass</b></td><td align ='center' colspan = 1>".$larrformData['SApass']."</td><td align ='center' colspan = 1>".$larrformData['SPpass']."</td><td align ='center' colspan = 1>".$larrformData['JBpass']."</td><td align ='center' colspan = 1>".$larrformData['BPpass']."</td><td align ='center' colspan = 1>".$larrformData['KBpass']."</td><td align ='center' colspan = 1>".$larrformData['MLpass']."</td><td align ='center' colspan = 1>".$larrformData['SBpass']."</td><td align ='center' colspan = 1>".$larrformData['KNpass']."</td><td align ='center' colspan = 1>".$larrformData['IPpass']."</td><td align ='center' colspan = 1>".$larrformData['GHpass']."
        			 				   </td><td align ='center' colspan = 1>".$larrformData['PPpass']."</td><td align ='center' colspan = 1>".$larrformData['SJpass']."</td><td align ='center' colspan = 1>".$larrformData['KKpass']."</td><td align ='center' colspan = 1>".$larrformData['KGpass']."</td><td align ='center' colspan = 1>".$larrformData['BGpass']."</td><td align ='center' colspan = 1>".$larrformData['WPpass']."</td><td align ='center' colspan = 1>".$larrformData['PJpass']."</td><td align ='center' colspan = 1>".$larrformData['MRpass']."</td><td align ='center' colspan = 1>".$larrformData['KTpass']."</td><td align ='center' colspan = 1>".$larrformData['SNpass']."</td><td align ='center' colspan = 1>".$larrformData['TWpass']."</td><td align ='center' colspan = 2>".$larrformData['passtotal']."</td></tr>";
        			 
        			 $tabledata.="<tr><td align ='left' colspan = 2><b>Fail</b></td><td align ='center' colspan = 1>".$larrformData['SAfail']."</td><td align ='center' colspan = 1>".$larrformData['SPfail']."</td><td align ='center' colspan = 1>".$larrformData['JBfail']."</td><td align ='center' colspan = 1>".$larrformData['BPfail']."</td><td align ='center' colspan = 1>".$larrformData['KBfail']."</td><td align ='center' colspan = 1>".$larrformData['MLfail']."</td><td align ='center' colspan = 1>".$larrformData['SBfail']."</td><td align ='center' colspan = 1>".$larrformData['KNfail']."</td><td align ='center' colspan = 1>".$larrformData['IPfail']."</td><td align ='center' colspan = 1>".$larrformData['GHfail']."
        			 				  </td><td align ='center' colspan = 1>".$larrformData['PPfail']."</td><td align ='center' colspan = 1>".$larrformData['SJfail']."</td><td align ='center' colspan = 1>".$larrformData['KKfail']."</td><td align ='center' colspan = 1>".$larrformData['KGfail']."</td><td align ='center' colspan = 1>".$larrformData['BGfail']."</td><td align ='center' colspan = 1>".$larrformData['WPfail']."</td><td align ='center' colspan = 1>".$larrformData['PJfail']."</td><td align ='center' colspan = 1>".$larrformData['MRfail']."</td><td align ='center' colspan = 1>".$larrformData['KTfail']."</td><td align ='center' colspan = 1>".$larrformData['SNfail']."</td><td align ='center' colspan = 1>".$larrformData['TWfail']."</td><td align ='center' colspan = 2>".$larrformData['failtotal']."</td></tr>";
        			 
        			 $tabledata.="<tr><td align ='left' colspan = 2><b>Absent</b></td><td align ='center' colspan = 1>".$larrformData['SAabsent']."</td><td align ='center' colspan = 1>".$larrformData['SPabsent']."</td><td align ='center' colspan = 1>".$larrformData['JBabsent']."</td><td align ='center' colspan = 1>".$larrformData['BPabsent']."</td><td align ='center' colspan = 1>".$larrformData['KBabsent']."</td><td align ='center' colspan = 1>".$larrformData['MLabsent']."</td><td align ='center' colspan = 1>".$larrformData['SBabsent']."</td><td align ='center' colspan = 1>".$larrformData['KNabsent']."</td><td align ='center' colspan = 1>".$larrformData['IPabsent']."</td><td align ='center' colspan = 1>".$larrformData['GHabsent']."
        			                  </td><td align ='center' colspan = 1>".$larrformData['PPabsent']."</td><td align ='center' colspan = 1>".$larrformData['SJabsent']."</td><td align ='center' colspan = 1>".$larrformData['KKabsent']."</td><td align ='center' colspan = 1>".$larrformData['KGabsent']."</td><td align ='center' colspan = 1>".$larrformData['BGabsent']."</td><td align ='center' colspan = 1>".$larrformData['WPabsent']."</td><td align ='center' colspan = 1>".$larrformData['PJabsent']."</td><td align ='center' colspan = 1>".$larrformData['MRabsent']."</td><td align ='center' colspan = 1>".$larrformData['KTabsent']."</td><td align ='center' colspan = 1>".$larrformData['SNabsent']."</td><td align ='center' colspan = 1>".$larrformData['TWabsent']."</td><td align ='center' colspan = 2>".$larrformData['absenttotal']."</td></tr>";
        			 
					 $ourFileName = realpath('.')."/data";
					 $ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
					 ini_set('max_execution_time', 3600);
					 fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
					 fclose($ourFileHandle);
					 header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
					 header("Content-Disposition: attachment; filename=$filename.xls");
					 header("Pragma: no-cache");
					 header("Expires: 0");
					 readfile($ourFileName);
					 unlink($ourFileName);
				}	
}	
 