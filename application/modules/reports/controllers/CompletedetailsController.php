<?php
class Reports_CompletedetailsController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;	
	
	public function init() { //initialization function
		$this->gobjsessionstudent = Zend_Registry::get('sis');
		/*$this->gobjsessionsis = Zend_Registry::get('sis');
		if(empty($this->gobjsessionsis->iduser)){ 
			$this->_redirect( $this->baseUrl . '/index/logout');					
		}	*/	
			
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() 
	{
		$this->lobjcompletedetailsmodel = new Reports_Model_DbTable_Completedetails(); //user model object
		$this->lobjcompletedetailsform = new Reports_Form_Completedetails(); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

	public function indexAction() 
	{ // action for search and view
		$this->view->lobjcompletedetailsform = $this->lobjcompletedetailsform;
		
			  $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		$this->view->lobjcompletedetailsform->Fromdate->setAttrib('constraints', "$dateofbirth");
		$this->view->lobjcompletedetailsform->Todate->setAttrib('constraints', "$dateofbirth");
		
		
		if ($this->_request->isPost() && $this->_request->getPost('Search'))
		{
			$larrformData = $this->_request->getPost();
			$fromdate = $this->view->fromdate=$larrformData['Fromdate'];
			$todate = $this->view->todate=$larrformData['Todate'];
			$larrresult = $this->lobjcompletedetailsmodel->fngetdetailforstudent($larrformData['Fromdate'],$larrformData['Todate']);
			$larrattempts=$this->lobjcompletedetailsmodel->fngetattempts($larrformData['Fromdate'],$larrformData['Todate']);
			
			for($att=0;$att<count($larrattempts);$att++)
			{
				$attempts[]=$larrattempts[$att]['attempts'];
			}
			//echo "<pre>";print_R($attempts);die();
			$this->view->attempts=$attempts;
			$this->view->paginator = $larrresult;
			$this->view->lobjcompletedetailsform->populate($larrformData);
					
		}	
		
	}
	public function pdfexportAction()
	{   
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformdata1 = $this->_request->getPost();
		//echo "<pre>";print_R($larrformdata1);die();
		$fdate = $larrformdata1['Fromdate'];
		$tdate = $larrformdata1['Todate'];
		$frmdate =date('d-m-Y',strtotime($fdate));
		$todate =date('d-m-Y',strtotime($tdate));
		
			$larrattempts=$this->lobjcompletedetailsmodel->fngetattempts($fdate,$tdate);
			$attempts=array();
			for($att=0;$att<count($larrattempts);$att++)
			{
				$attempts[]=$larrattempts[$att]['attempts'];
			}
			//echo "<pre>";print_R($attempts);die();
			$this->view->attempts=$attempts;
		
		$result = $this->lobjcompletedetailsmodel->fngetdetailforstudent($larrformdata1['Fromdate'],$larrformdata1['Todate']);
		$count=count($result);
		
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'CompleteStudentDetails_Report_'.$frmdate.'_'.$todate;
		$ReportName = $this->view->translate( "Billing" ).' '.$this->view->translate( "Report" );
		
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
		
		
		$tabledata.= "<tr><td align=left colspan = 2><b>Exam From Date </b></td><td align=left colspan = 2><b>".$frmdate."</b></td><td  align=left colspan = 2><b> Exam To Date </b></td><td align=left colspan = 2><b>".$todate."</b></td></tr>";
		
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 8><b> {$ReportName}</b></td></tr></table><br>";
		$tabledata.= '<table border=1 align=center width=100%><tr><th><b>Exam Date</b></th><th><b>Venue</b></th><th><b>Session</b></th><th><b>Course</b></th></th><th><b>Candidate Name</b></th><th><b>ICNO</b></th><th><b>Result</b></th><th><b>Attempts</b></th></tr>';
		$cnts  = 0;
		$centerarray =array();
		$centerarrays =array();
		$centerarrayss =array();
		$centerarraysss =array();
		$sum=0;
		foreach($result as $lobjCountry)
		{
			
			 $tabledata.="<table>";
			    
			    $tabledata.="<tr>";
			    
			     $tabledata.="<td>";
				$tabledata.=$lobjCountry['Date'];
                $tabledata.="</td>";				
			
				
			  $tabledata.="<td>";
				
				$tabledata.=$lobjCountry['centername'];
			  $tabledata.="</td>";
			    $tabledata.="<td>";
				
				$tabledata.=$lobjCountry['managesessionname'];
				$tabledata.="</td>";
			
			    $tabledata.="<td>";
				
				
				$tabledata.=$lobjCountry['ProgramName'];
				$tabledata.="</td>";
			
			$cnts++;
			$tabledata.= '<td>'.$lobjCountry['FName'].'</td><td>'.$lobjCountry['ICNO'].'</td>';
				if($lobjCountry['pass'] == 1)
				{  
			    $tabledata.= '<td>Pass</td>';
				}
				else
				{
				    $tabledata.= '<td>Fail</td>';
				}
				
				  $tabledata.="<td>";
				
				
				$tabledata.=$lobjCountry['PermCity'];
				
				  if(in_array($lobjCountry['PermCity'],$attempts))
         	  {
         	  	$idattempts=$lobjCountry['PermCity'];
         	  	if(empty($attmarray[$idattempts]))
         	  	{
         	  		$attmarray[$idattempts]=0;
         	  	}
         	  	$idapps=$attmarray[$idattempts];
         	$attmarray[$idattempts]=$idapps+1;
				$attmarray[$idattempts] = $sum+$attmarray[$idattempts];
				
				
				}  
				$tabledata.="</td>";
			$tabledata.="</tr>";
		}
		 $tabledata.="</table>";
		 $tabledata.="<table>";
		  $tabledata.="<tr>";
		  
		 for($att=0;$att<count($attempts);$att++) {
      $attemps=$attempts[$att]; if($attempts[$att]==1){
     $tabledata.="<th>";
     $tabledata.=$attempts[$att]."st attempt";
      $tabledata.="</th>";
     } else if ($attempts[$att]==2) {
		  $tabledata.="<th>";
     $tabledata.=$attempts[$att]."nd attempt";
      $tabledata.="</th>";
      } else if ($attempts[$att]==3) {
         $tabledata.="<th>";
        $tabledata.=$attempts[$att]."rd attempt";
         $tabledata.="</th>";
                } else  { 
                     $tabledata.="<th>";
                   $tabledata.=$attempts[$att]."th attempt";
                     $tabledata.="</th>"; 
                       
    }}
    $tabledata.="</tr>";
       $tabledata.="<tr>";
        
     for($att=0;$att<count($attempts);$att++) {
		 
      $attemps=$attempts[$att]; if($attempts[$att]==1){
		  $tabledata.="<td>";
   $tabledata.= $attmarray[$attemps];
   $tabledata.="</td>";
      } else if ($attempts[$att]==2) {
     $tabledata.="<td>";
      $tabledata.=$attmarray[$attemps];
      $tabledata.="</td>"; 
      } else if ($attempts[$att]==3) {
        $tabledata.="<td>";
         $tabledata.=$attmarray[$attemps];
         $tabledata.="</td>"; 
                } else  { 
                     $tabledata.="<td>";
                      $tabledata.=$attmarray[$attemps];
                    $tabledata.="</td>"; 
                      
                       
     }}
    $tabledata.="</tr>";
    
      $tabledata.='<tr><td colspan="6" align="right"><b>TOTAL :';
      $tabledata.= $cnts;
		    $tabledata.='</b></td> </tr>';
		    $tabledata.="</table>";
		
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		
	}
	
	public function getallattemptsAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idapp = $this->_getParam('idapp');
		$cnt = $this->_getParam('cnt');
		$icno = $this->_getParam('icno');
		$result = $this->lobjcompletedetailsmodel->fngetallattemptsforicno($icno,$cnt);
		$tabledata= '<br><fieldset><legend align = "left">Previous Attempts:</legend>
					                    <table class="table" border=1 align = "center" width=100%>
					                    	<tr>
					                    	   <th><b>Examdate</b></th>
											   <th><b>RegisterID</b></th>
					                    		<th><b>Venue</b></th>
												<th><b>Program</b></th>
												<th><b>Session</b></th>
												<th><b>ICNO</b></th>
												<th><b>Name</b></th>
													<th><b>Attempts</b></th>
												<th><b>Result</b></th>
											
					                    		
					                    	</tr>';
		  
		               
		for($i=0;$i<count($result);$i++)
		{
		 
						  $tabledata.= '<tr><td align = "left">'.$result[$i]['DateTime'].'</td>';
					            $tabledata.= '<td align = "left">'.$result[$i]['Regid'].'</td>';          		
						    $tabledata.= '<td align = "left">'.$result[$i]['centername'].'</td>';
						      $tabledata.= '<td align = "left">'.$result[$i]['ProgramName'].'</td>';
						        $tabledata.= '<td align = "left">'.$result[$i]['managesessionname'].'</td>';
								 $tabledata.= '<td align = "left">'.$result[$i]['ICNO'].'</td>';
								 $tabledata.= '<td align = "left">'.$result[$i]['FName'].'</td>';
								    $tabledata.= '<td align = "left">'.$result[$i]['PermCity'].'</td>';
								if($result[$i]['pass']==1)
								{
						          $tabledata.= '<td align = "left">Pass</td></tr>';
								  }
								  elseif($result[$i]['pass']==2)
								  {
								     $tabledata.= '<td align = "left">Fail</td></tr>';
								  }
								  elseif($result[$i]['pass']==3)
								  {
								      $tabledata.= '<td align = "left">Applied</td></tr>';
								  }
								  elseif($result[$i]['pass']==4)
								  {
								      $tabledata.= '<td align = "left">Absent</td></tr>';
								  }
								 
		                    
						          
						         
			 }
			  $tabledata.="<tr><td colspan= '8' align='right'><input type='button' id='close' name='close'  value='Close' onClick='Closevenue();'></td></tr>";
		                     $tabledata.="</table><br>";
			 echo  $tabledata;	   
		//echo "<pre>";
		//print_r($larrresult);die();
		 
	
	}
}
