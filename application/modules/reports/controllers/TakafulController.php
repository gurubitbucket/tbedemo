<?php
class Reports_TakafulController extends Base_Base 
{
	
private $_gobjlogger; 
	public function init() 
	{
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
    
	public function indexAction() 
	{  
		$this->view->checkEmpty = 0;
		
		$lobjReportsForm = new  Reports_Form_BatchExamreport();
		$lobjExamreportModel = new  Reports_Model_DbTable_BatchExamreport();
		$this->view->lobjform = $lobjReportsForm;
				
		$larrcourses=$lobjExamreportModel->fngetprogramnames();		
		$lobjReportsForm->Coursename->addMultiOption('','Select'); 	
		$lobjReportsForm->Coursename->addmultioptions($larrcourses);
		
		$larrcenters=$lobjExamreportModel->fngetcenternames();
		$lobjReportsForm->Venues->addMultiOption('','Select'); 	
		$lobjReportsForm->Venues->addmultioptions($larrcenters);
		
		$larrtakafulnames=$lobjExamreportModel->fngettakafulnames();	
		$lobjReportsForm->Takafulname->addMultiOption('','Select'); 	
		$lobjReportsForm->Takafulname->addmultioptions($larrtakafulnames);
	
 		if($this->_request->isPost() && $this->_request->getPost('Search')) 
 		{
 			$larrformData = $this->_request->getPost();
			$this->view->Takaful =$idtakaful=  $larrformData['Takafulname'];
             //echo "<pre>";
			 // print_r($larrformData);
			  $larrbatchids = $lobjExamreportModel->fngetpinfortakaful($idtakaful);
			  $this->view->lobjform->Pin->addmultioption('','Select');
			    $this->view->lobjform->Pin->addmultioptions($larrbatchids);
			if($larrformData['Pin'])
			{
			  
			 // echo "<pre>";
			//  print_r($larrbatchids);
			
			  //echo $larrformData['Pin'];
			  $this->view->lobjform->Pin->setvalue($larrformData['Pin']);
			}
			
			$this->view->Student =$larrformData['Studentname'];
			$this->view->Regpin =$larrformData['Pin'];
			$this->view->Icno =$larrformData['ICNO'];
			$this->view->Course =$larrformData['Coursename'];
			$this->view->Venue =$larrformData['Venues'];
			$this->view->ExamFromdate =$larrformData['Date'];
			$this->view->ExamTodate =$larrformData['Date2'];
			$this->view->AppliedFromdate =$larrformData['AFromDate'];
			$this->view->AppliedTodate =$larrformData['AToDate'];
			
 			$result = $lobjExamreportModel->fngettakafulgroupwise($larrformData);
            $this->view->paginator = $result;
            $this->view->lobjform->populate($larrformData);	
	    }
	}
	
	public function pdfexportAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lobjExamreportModel = new  Reports_Model_DbTable_BatchExamreport();
		$larrformData = $this->_request->getPost();
		$fdate = $larrformData['fromdate'];
		$tdate = $larrformData['todate'];
		$Afdate = $larrformData['Afromdate'];
		$Atdate = $larrformData['Atodate'];
		
		$frmdate =date('d-m-Y',strtotime($fdate));
		$todate =date('d-m-Y',strtotime($tdate));
		$Afrmdate =date('d-m-Y',strtotime($Afdate));
		$Atodate =date('d-m-Y',strtotime($Atdate));
		$currentdate = date('d-m-Y:H:i:s');
		
		
		$result = $lobjExamreportModel->fngettakafuldetails($larrformData);		
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";	
		$time = date('h:i:s',time());
		$filename = 'Takaful_Operator_Report'.$currentdate;
		$ReportName = $this->view->translate( "Takaful" ).' '.$this->view->translate( "Operator" ).' '.$this->view->translate( "Report" );
		//if(isset($larrformData['excel'])){		
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
	    //}
		 $tabledata.= "<table border=1  align=center width=100%>";	
		if($larrformData['fromdate'])
		{
		       $tabledata.= "<tr>
		                  <td align=left colspan = 2><b>Exam From Date </b></td>
						  <td align=left colspan = 2><b>".$frmdate."</b></td>";
						  
				  if($larrformData['todate'])
		          {
		               $tabledata.= "<td  align=left colspan = 2><b> Exam To Date </b></td>
						    <td align=left colspan = 2><b>".$todate."</b></td>
					    </tr><br>";
		           }
				   else
				   {
				     $tabledata.="</tr>";
				   } 
		}
		
        if($larrformData['Afromdate'])
        {		
		      $tabledata.= "<tr>
		                  <td align=left colspan = 2><b>Applied From Date </b></td>
						  <td align=left colspan = 2><b>".$Afrmdate."</b></td>";
					if($larrformData['Atodate'])
		            {
		              $tabledata.= "<td  align=left colspan = 2><b>Applied To Date </b></td>
						    <td align=left colspan = 2><b>".$Atodate."</b></td>
					        </tr>";
		            }
					 else
				   {
				     $tabledata.="</tr>";
				   }	
		}
		 $tabledata.="</table>";
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 8><b>".$ReportName."</b></td></tr></table><br>";
		$tabledata.= "<table border=1 align=center width=100%>
		               <tr>
					        <th><b>TakafulName</b></th><th><b>BatchId</b></th><th><b>Examdate</b></th>
							<th><b>Venue</b></th></th><th><b>Course</b></th><th><b>Session</b></th>
							<th><b>Student</b></th><th><b>ICNO</b></th><th><b>Applied Date</b></th>
							<th><b>Result</b></th><th><b>Payment Status</b></th>
					  </tr>";
		$cnts  = 0;
		$centerarray =array();
		$centerarrays =array();
		$centerarrayss =array();
		$centerarraysss =array();
		foreach($result as $lobjCountry)
		{
			    $tabledata.="<tr>";
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['TakafulName'];
                $tabledata.="</td>";
				
			    $tabledata.="<td>";				
				$tabledata.=$lobjCountry['registrationPin'];
			    $tabledata.="</td>";
				
			    $tabledata.="<td>";
				$tabledata.= date('d-m-Y',strtotime($lobjCountry['DateTime']));
				$tabledata.="</td>";
			
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['ExamVenue'];
				$tabledata.="</td>";
			   
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['ProgramName'];
                $tabledata.="</td>";
				
			    $tabledata.="<td>";				
				$tabledata.=$lobjCountry['managesessionname'];
			    $tabledata.="</td>";
				
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['FName'];
				$tabledata.="</td>";
			
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['ICNO'];
				$tabledata.="</td>";
				
				$tabledata.="<td>";
				$tabledata.=$lobjCountry['upddate'];
				$tabledata.="</td>";
				
				$tabledata.="<td>";				
				$tabledata.=$lobjCountry['Result'];
			    $tabledata.="</td>";
				
				if($lobjCountry['paymentStatus'] == 2)
				{
				    $tabledata.="<td>Registration Completed</td>";
				}
				if($lobjCountry['paymentStatus'] == 1)
				{
				    $tabledata.="<td>Partially Filled</td>";
				}
			$tabledata.="</tr>";
		}
		$tabledata.="<tr>";
		$tabledata.="<td colspan='10' align='right'>";
		$tabledata.="TOTAL :".count($result);
		$tabledata.="</td>";
		$tabledata.="</tr>";
		 $tabledata.="</table>";	
		
		if(isset($larrformData['pdf']))
		{
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			//echo $tabledata;die();
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}
		else
		{
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
			// Write to Logs
		    /* $auth = Zend_Auth::getInstance();
		     $priority=Zend_Log::INFO;
		     $controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		     $message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Exported the Takaful Summary Report(Excel)"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		      $this->_gobjlogger->log($message,5);
			  */
		}
	}
	public function getallregpinsforbatchAction()
	{
	     $this->_helper->layout->disableLayout();
	     $this->_helper->viewRenderer->setNoRender ();
	     $idtakaful = $this->_getParam('idtakaful');
		 $lobjExamreportModel = new  Reports_Model_DbTable_BatchExamreport();
	     $regpindetails = $lobjExamreportModel->fngetpinfortakaful($idtakaful);	 
	     $larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ($regpindetails);	 
	     echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );
	}
}
