<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_AllstatisticsController extends Base_Base 
{
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->lobjallstatistics = new Reports_Model_DbTable_Allstatisticsmodel;
   	    $this->lobjStasticsForm = new  Reports_Form_Statisticsnew();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjStasticsForm;
		///new
		$this->view->lobjform->Year->setAttrib('onchange','fngetmonths()');
		$year = 2012;
		$lintcuryear = date("Y"); 
		for($linti=$year;$linti<=$lintcuryear;$linti++){
			$larryears[$linti] = $linti;
		} 
		$this->lobjStasticsForm->Year->addmultioptions($larryears);
		$ldtdate = getdate();
		$this->view->search = 1;
		$this->view->searchmon = $lintmonthnumber = $ldtdate['mon']-1;
		
		$lintyearnumber = $ldtdate['year'];
		$this->lobjStasticsForm->Year->setValue($lintyearnumber);
		//$this->lobjStasticsForm->Month->setValue($lintmonthnumber); 
		
		if ($this->_request->isPost () && $this->_request->getPost('Search')) 
		    {   
		    	
		    	$larrformData = $this->_request->getPost();
				if ($this->lobjform->isValid($larrformData)) 
				{
				   $larrformData = $this->_request->getPost();
				   /// to be added
				   $this->view->search = 0;
				   $this->view->searchmon = $lintmon = $larrformData['Month'];
				   ///to be added
				   $lintyear = $larrformData['Year'];
				   $linttype = $larrformData['Type'];



				   if($lintmon == 1 || $lintmon == '01'){
				   	$lstrmonthname = "January";
				   }
				 if($lintmon == 2 || $lintmon == '02'){
				   	$lstrmonthname = "February";
				   }
				 if($lintmon == 3 || $lintmon == '03'){
				   	$lstrmonthname = "March";
				   }
				 if($lintmon == 4 || $lintmon == '04'){
				   	$lstrmonthname = "April";
				   }
				 if($lintmon == 5 || $lintmon == '05'){
				   	$lstrmonthname = "May";
				   }
				 if($lintmon == 6 || $lintmon == '06'){
				   	$lstrmonthname = "June";
				   }
				 if($lintmon == 7 || $lintmon == '07'){
				   	$lstrmonthname = "July";
				   }
				 if($lintmon == 8 || $lintmon == '08'){
				   	$lstrmonthname = "August";
				   }
				 if($lintmon == 9 || $lintmon == '09'){
				   	$lstrmonthname = "September";
				   }
				 if($lintmon == 10){
				   	$lstrmonthname = "October";
				   }
				 if($lintmon == 11){
				   	$lstrmonthname = "November";
				   }
				 if($lintmon == 12){
				   	$lstrmonthname = "December";
				   }
				   $this->view->month = $lintmon;
				   $this->view->monthname = $lstrmonthname;
				   $this->view->year = $lintyear;
				   $lintagetype = 0;$lintcategorytype = 0;$lintcentretype = 0;$lintresulttype = 0;$lintgendertype = 0;$lintracetype = 0; $lintqualificationtype = 0;
				    $lintsessiontype = 0;
				   switch($linttype)
				   {
				   	case 1: $lintagetype = 1;
				   	        break;
				    case 2: $lintcategorytype = 1;
				   	        break;
				   	case 3: $lintcentretype = 1;
				   	        break;
				   	case 4: $lintresulttype = 1;
				   	        break;
				   	case 5: $lintgendertype = 1;
				   	        break;
				   	case 6: $lintracetype = 1;
				   	        break;
				   	case 7: $lintqualificationtype = 1;
				   	        break;
				   	case 9: $lintsessiontype = 1;
				   	        break;
				   	default:$lintagetype = 1;$lintcategorytype = 1;$lintcentretype = 1;$lintresulttype = 1;$lintgendertype = 1;$lintracetype = 1; $lintqualificationtype = 1;$lintsessiontype = 1;
				   	        break;
				   }
				   $this->view->linttype = $linttype; 
				   $this->view->lintagetype = $lintagetype; 
				   $this->view->lintcategorytype = $lintcategorytype;
				   $this->view->lintcentretype = $lintcentretype;
				   $this->view->lintresulttype = $lintresulttype; 
				   $this->view->lintgendertype = $lintgendertype;
				   $this->view->lintracetype = $lintracetype;
				   $this->view->lintqualificationtype = $lintqualificationtype;
				   $this->view->lintsessiontype = $lintsessiontype;
//////////////////// Age/////////////////
		if($lintagetype==1)
		{      
				$larrages = $this->lobjallstatistics->fngetages($lintmon,$lintyear);
				$lintagecount = count($larrages);
				$larragescount['18-21']=0;$larragescount['22-25']=0;$larragescount['26-29']=0;$larragescount['30-34']=0;$larragescount['35-39']=0;$larragescount['40-44']=0;$larragescount['45-49']=0;$larragescount['50-54']=0;
				$larragescount['55-59']=0;$larragescount['60Above']=0;
			
				for($linti=0;$linti<$lintagecount;$linti++){
                                        if($larrages[$linti]['Age']>=18 && $larrages[$linti]['Age']<=21){
						$larragescount['18-21']++;
					if($larrages[$linti]['pass']==1){
							$larragescount['18-21sat']++;
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['18-21One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['18-21More']++;
							}
							$larragescount['18-21pass']++;
						}elseif($larrages[$linti]['pass']==2){
							$larragescount['18-21sat']++;
							//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['18-21One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['18-21More']++;
							}
							//attemptsend
							$larragescount['18-21fail']++;
						}elseif($larrages[$linti]['pass']==4){
							$larragescount['18-21absent']++;
						}
					}elseif($larrages[$linti]['Age']>=22 && $larrages[$linti]['Age']<=25){
						$larragescount['22-25']++;
						
						if($larrages[$linti]['pass']==1){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['22-25One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['22-25More']++;
							}
							//attemptsend
							$larragescount['22-25sat']++;
							$larragescount['22-25pass']++;
						}elseif($larrages[$linti]['pass']==2){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['22-25One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['22-25More']++;
							}
							//attemptsend
							$larragescount['22-25sat']++;
							$larragescount['22-25fail']++;
						}elseif($larrages[$linti]['pass']==4){
							$larragescount['22-25absent']++;
						}
					}
					elseif($larrages[$linti]['Age']>=26 && $larrages[$linti]['Age']<=29){
						$larragescount['26-29']++;
						
						if($larrages[$linti]['pass']==1){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['26-29One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['26-29More']++;
							}
							//attemptsend
							$larragescount['26-29sat']++;
							$larragescount['26-29pass']++;
						}elseif($larrages[$linti]['pass']==2){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['26-29One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['26-29More']++;
							}
							//attemptsend
							$larragescount['26-29sat']++;
							$larragescount['26-29fail']++;
						}elseif($larrages[$linti]['pass']==4){
							$larragescount['26-29absent']++;
						}
					}elseif($larrages[$linti]['Age']>=30 && $larrages[$linti]['Age']<=34){
						$larragescount['30-34']++;
						
						if($larrages[$linti]['pass']==1){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['30-34One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['30-34More']++;
							}
							//attemptsend
							$larragescount['30-34sat']++;
							$larragescount['30-34pass']++;
						}elseif($larrages[$linti]['pass']==2){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['30-34One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['30-34More']++;
							}
							//attemptsend
							$larragescount['30-34sat']++;
							$larragescount['30-34fail']++;
						}elseif($larrages[$linti]['pass']==4){
							$larragescount['30-34absent']++;
						}
					}elseif($larrages[$linti]['Age']>=35 && $larrages[$linti]['Age']<=39){
						$larragescount['35-39']++;
						
						if($larrages[$linti]['pass']==1){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['35-39One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['35-39More']++;
							}
							//attemptsend
							$larragescount['35-39sat']++;
							$larragescount['35-39pass']++;
						}elseif($larrages[$linti]['pass']==2){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['35-39One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['35-39More']++;
							}
							//attemptsend
							$larragescount['35-39sat']++;
							$larragescount['35-39fail']++;
						}elseif($larrages[$linti]['pass']==4){
							$larragescount['35-39absent']++;
						}
					}elseif($larrages[$linti]['Age']>=40 && $larrages[$linti]['Age']<=44){
						$larragescount['40-44']++;
						
						if($larrages[$linti]['pass']==1){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['40-44One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['40-44More']++;
							}
							//attemptsend
							$larragescount['40-44sat']++;
							$larragescount['40-44pass']++;
						}elseif($larrages[$linti]['pass']==2){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['40-44One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['40-44More']++;
							}
							//attemptsend
							$larragescount['40-44sat']++;
							$larragescount['40-44fail']++;
						}elseif($larrages[$linti]['pass']==4){
							$larragescount['40-44absent']++;
						}
					}elseif($larrages[$linti]['Age']>=45 && $larrages[$linti]['Age']<=49){
						$larragescount['45-49']++;
						
						if($larrages[$linti]['pass']==1){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['45-49One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['45-49More']++;
							}
							//attemptsend
							$larragescount['45-49sat']++;
							$larragescount['45-49pass']++;
						}elseif($larrages[$linti]['pass']==2){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['45-49One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['45-49More']++;
							}
							//attemptsend
							$larragescount['45-49sat']++;
							$larragescount['45-49fail']++;
						}elseif($larrages[$linti]['pass']==4){
							$larragescount['45-49absent']++;
						}
					}elseif($larrages[$linti]['Age']>=50 && $larrages[$linti]['Age']<=54){
						$larragescount['50-54']++;
						
						if($larrages[$linti]['pass']==1){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['50-54One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['50-54More']++;
							}
							//attemptsend
							$larragescount['50-54sat']++;
							$larragescount['50-54pass']++;
						}elseif($larrages[$linti]['pass']==2){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['50-54One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['50-54More']++;
							}
							//attemptsend
							$larragescount['50-54sat']++;
							$larragescount['50-54fail']++;
						}elseif($larrages[$linti]['pass']==4){
							$larragescount['50-54absent']++;
						}
					}elseif($larrages[$linti]['Age']>=55 && $larrages[$linti]['Age']<=59){
						$larragescount['55-59']++;
						
						if($larrages[$linti]['pass']==1){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['55-59One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['55-59More']++;
							}
							//attemptsend
							$larragescount['55-59sat']++;
							$larragescount['55-59pass']++;
						}elseif($larrages[$linti]['pass']==2){
						//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['55-59One']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['55-59More']++;
							}
							//attemptsend
							$larragescount['55-59sat']++;
							$larragescount['55-59fail']++;
						}elseif($larrages[$linti]['pass']==4){
							$larragescount['55-59absent']++;
						}
					}else{
							$larragescount['60Above']++;
							
							if($larrages[$linti]['pass']==1){
							//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['60AboveOne']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['60AboveMore']++;
							}
							//attemptsend
								$larragescount['60Abovesat']++;
								$larragescount['60Abovepass']++;
							}elseif($larrages[$linti]['pass']==2){
							//attempts
							if($larrages[$linti]['Attempts']==1)
							{
							  $larragescount['60AboveOne']++;
							}
						    if($larrages[$linti]['Attempts']>1)
							{
							  $larragescount['60AboveMore']++;
							}
							//attemptsend
								$larragescount['60Abovesat']++;
								$larragescount['60Abovefail']++;
							}elseif($larrages[$linti]['pass']==4){
								$larragescount['60Aboveabsent']++;
							}
						}
		}
		$larragescount['regtotal']=  $larragescount['18-21']+$larragescount['22-25']+$larragescount['26-29']+$larragescount['30-34']+$larragescount['35-39']+$larragescount['40-44']+$larragescount['45-49']+$larragescount['50-54']+$larragescount['55-59']+$larragescount['60Above'];
		$larragescount['sattotal']=  $larragescount['18-21sat']+$larragescount['22-25sat']+$larragescount['26-29sat']+$larragescount['30-34sat']+$larragescount['35-39sat']+$larragescount['40-44sat']+$larragescount['45-49sat']+$larragescount['50-54sat']+$larragescount['55-59sat']+$larragescount['60Abovesat'];

$larragescount['attemptstotalOne']= $larragescount['18-21One']+$larragescount['22-25One']+$larragescount['26-29One']+$larragescount['30-34One']+$larragescount['35-39One']+$larragescount['40-44One']+$larragescount['45-49One']+$larragescount['50-54One']+$larragescount['55-59One']+$larragescount['60AboveOne'];
		$larragescount['attemptstotalMore']= $larragescount['18-21More']+$larragescount['22-25More']+$larragescount['26-29More']+$larragescount['30-34More']+$larragescount['35-39More']+$larragescount['40-44More']+$larragescount['45-49More']+$larragescount['50-54More']+$larragescount['55-59More']+$larragescount['60AboveMore'];

		$larragescount['passtotal']=  $larragescount['18-21pass']+$larragescount['22-25pass']+$larragescount['26-29pass']+$larragescount['30-34pass']+$larragescount['35-39pass']+$larragescount['40-44pass']+$larragescount['45-49pass']+$larragescount['50-54pass']+$larragescount['55-59pass']+$larragescount['60Abovepass'];
		$larragescount['failtotal']=  $larragescount['18-21fail']+$larragescount['22-25fail']+$larragescount['26-29fail']+$larragescount['30-34fail']+$larragescount['35-39fail']+$larragescount['40-44fail']+$larragescount['45-49fail']+$larragescount['50-54fail']+$larragescount['55-59fail']+$larragescount['60Abovefail'];
		$larragescount['absenttotal']=  $larragescount['18-21absent']+$larragescount['22-25absent']+$larragescount['26-29absent']+$larragescount['30-34absent']+$larragescount['35-39absent']+$larragescount['40-44absent']+$larragescount['45-49absent']+$larragescount['50-54absent']+$larragescount['55-59absent']+$larragescount['60Aboveabsent'];
		
		$this->view->larragescount = $larragescount;
		$this->view->larragessession = $larragessession;
		}
		////////// Result///////////
		if($lintresulttype==1)
		{
		$larrprograms = $this->lobjallstatistics->fngetresultprogramnames();
		for($linti=0;$linti<count($larrprograms);$linti++)
		{
			$larrresult[$linti] = $this->lobjallstatistics->fngetresultgrade($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
			$larrregdetails[$linti] = $this->lobjallstatistics->fngetabsentdetails($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
			$lintgradeA[$linti] = 0;
                         $lintgradeB[$linti] = 0;
                          $lintgradeC[$linti] = 0;
                          $lintgradeF[$linti] = 0;
                           $lintabsent[$linti] = 0;

                        $lintgradeAone[$linti] = 0;
			$lintgradeAmore[$linti] = 0;
			$lintgradeBone[$linti] = 0;
			$lintgradeBmore[$linti] = 0;
			$lintgradeCone[$linti] = 0;
			$lintgradeCmore[$linti] = 0;
			$lintgradeAbsentone[$linti] = 0;
			$lintgradeAbsentmore[$linti] = 0;
			$lintgradeFailone[$linti] = 0;
			$lintgradeFailmore[$linti] = 0;
                       
			for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++){

				if($larrresult[$linti][$lintj]['Grade']=='A'){
                                        //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeAone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeAmore[$linti]++;
							}
							//attemptsend
					$lintgradeA[$linti]++;

				}elseif($larrresult[$linti][$lintj]['Grade']=='B'){
                                         //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeBone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeBmore[$linti]++;
							}
							//attemptsend
					$lintgradeB[$linti]++;

				}elseif($larrresult[$linti][$lintj]['Grade']=='C'){
                                        //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeCone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeCmore[$linti]++;
							}
							//attemptsend
					$lintgradeC[$linti]++;

				}else{
                                            //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeFailone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeFailmore[$linti]++;
							}
							//attemptsend
					$lintgradeF[$linti]++;
				}
			}
                       
		///////////
                 $larrsbsentdetails[$linti] = $this->lobjallstatistics->fngetabsentprogramwise($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
			 
                 for($lintj=0;$lintj<count($larrsbsentdetails[$linti]);$lintj++){
				if($larrsbsentdetails[$linti][$lintj]['pass']=='4'){
                                         //attempts
							if($larrsbsentdetails[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeAbsentone[$linti]++;
							}
						    if($larrsbsentdetails[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeAbsentmore[$linti]++;
							}
							//attemptsend
					$lintabsent[$linti]++;
				}
			}


		///////////



                      
			//$lintabsent[$linti] = $larrregdetails[$linti]['NoOfCandidates'] -($lintgradeA[$linti]+$lintgradeB[$linti]+$lintgradeC[$linti]+$lintgradeF[$linti]);
		}

		$this->view->regdetails = $larrregdetails;
		$this->view->programs = $larrprograms;
		$this->view->gradeA = $lintgradeA;
		$this->view->gradeB = $lintgradeB;
		$this->view->gradeC = $lintgradeC;
		$this->view->resultfail = $lintgradeF;
		$this->view->resultabsent = $lintabsent;


                $this->view->gradeAone = $lintgradeAone;
		$this->view->gradeAmore =	$lintgradeAmore;
		$this->view->gradeBone =	$lintgradeBone;
		$this->view->gradeBmore =	$lintgradeBmore;
		$this->view->gradeCone =	$lintgradeCone;
		$this->view->gradeCmore =	$lintgradeCmore;
		$this->view->resultAbsentone =	$lintgradeAbsentone;
		$this->view->resultAbsentmore =	$lintgradeAbsentmore;
		$this->view->resultFailone =	$lintgradeFailone;
		$this->view->resultFailmore =	$lintgradeFailmore;


		}
		/////////// Gender/////////////////
		if($lintgendertype==1){
		$lintmaleid = 1;$lintfemaleid = 0;
		$larrracescount = $this->lobjallstatistics->fngetgenderscount($lintmon,$lintyear);
		$lintregisteredtotal = $larrracescount[0]['NoOfCandidates']+$larrracescount[1]['NoOfCandidates'];
		$larrracescount[2]['regtotal1'] = $lintregisteredtotal;
		$larrmaleresult = $this->lobjallstatistics->fngetgenderresult($lintmon,$lintyear,$lintmaleid);
		$lintmalecount = count($larrmaleresult);
		$lintpass = 0;
		$lintfail = 0;
		$lintabsent = 0;
                $lintone = 0;
		$lintmore = 0;
		for($linti=0;$linti<$lintmalecount;$linti++){
			if($larrmaleresult[$linti]['pass']==1)
			{
                                //attempts
							if($larrmaleresult[$linti]['Attempts']==1)
							{
							  $lintone++;
							}
						    if($larrmaleresult[$linti]['Attempts']>1)
							{
							  $lintmore++;
							}
							//attemptsend
				$lintpass++;
			}elseif($larrmaleresult[$linti]['pass']==2){
                                 //attempts
							if($larrmaleresult[$linti]['Attempts']==1)
							{
							  $lintone++;
							}
						    if($larrmaleresult[$linti]['Attempts']>1)
							{
							  $lintmore++;
							}
							//attemptsend
				$lintfail++;
			}elseif($larrmaleresult[$linti]['pass']==4){
				$lintabsent++;
			}
		}
		$larrracescount[]['msat'] = $lintpass+$lintfail;
		$larrracescount[]['mpass'] = $lintpass;
		$larrracescount[]['mfail']= $lintfail;
		$larrracescount[]['mabsent']= $lintabsent;//$larrracescount[1]['NoOfCandidates']-($lintpass+$lintfail);//$lintabsent;
		$larrfemaleresult = $this->lobjallstatistics->fngetgenderresult($lintmon,$lintyear,$lintfemaleid);
		$lintfemalecount = count($larrfemaleresult);
		$lintpass = 0;
		$lintfail = 0;
		$lintfabsent = 0;
                $lintfone = 0;
		$lintfmore = 0;
		for($linti=0;$linti<$lintfemalecount;$linti++){
			if($larrfemaleresult[$linti]['pass']==1)
			{
                                 //attempts
							if($larrfemaleresult[$linti]['Attempts']==1)
							{
							  $lintfone++;
							}
						    if($larrfemaleresult[$linti]['Attempts']>1)
							{
							  $lintfmore++;
							}
							//attemptsend
				$lintpass++;
			}elseif ($larrfemaleresult[$linti]['pass']==2){
                                //attempts
							if($larrfemaleresult[$linti]['Attempts']==1)
							{
							   $lintfone++;
							}
						    if($larrfemaleresult[$linti]['Attempts']>1)
							{
							  $lintfmore++;
							}
							//attemptsend
				$lintfail++;
			}elseif ($larrfemaleresult[$linti]['pass']==4){
				$lintfabsent++;
			}
		}
		$larrracescount[]['fsat'] = $lintpass+$lintfail;
		$larrracescount[]['fpass'] = $lintpass;
		$larrracescount[]['ffail']= $lintfail;
		$larrracescount[]['fabsent']= $lintfabsent;//$larrracescount[0]['NoOfCandidates']-($lintpass+$lintfail);//$lintabsent;
		$larrracescount[]['sattotal']=  $larrracescount[3]['msat']+$larrracescount[7]['fsat'];
		$larrracescount[]['passtotal']=  $larrracescount[4]['mpass']+$larrracescount[8]['fpass'];
		$larrracescount[]['failtotal']=  $larrracescount[5]['mfail']+$larrracescount[9]['ffail'];
		$larrracescount[]['absenttotal']=  $larrracescount[6]['mabsent']+$larrracescount[10]['fabsent'];
                $larrracescount[]['mone'] = $lintone;
		$larrracescount[]['mmore']= $lintmore;
		$larrracescount[]['fone'] = $lintfone;
		$larrracescount[]['fmore']= $lintfmore;
		$this->view->larrgendercount   =  $larrracescount;
		$this->view->paginator = $larrracescount;
		}
		////// Qualification //////////
		if($lintqualificationtype==1)
		{
		$larrqualificationcount = $this->lobjallstatistics->fngetqualification($lintmon,$lintyear); //Call a function to get number of candidates registered for different qualification
		$larrqualification = $this->lobjallstatistics->fngetqualificationnames($lintmon,$lintyear); //Call a function to get different qualification names
		$this->view->larrqualificationcount=$larrqualificationcount;
		$lintregisteredtotal=0;
		for($linti=0;$linti<count($larrqualification);$linti++){
			$lintqualificationid= $larrqualification[$linti]['idDefinition'];
			$larrqualificationquali[$linti]  =	$larrqualification[$linti];
			$larrQualificationresult = $this->lobjallstatistics->fngetqualificationresult($lintmon,$lintyear,$lintqualificationid); //Call a function to get number of Diploma candidates details
			$lintregisteredtotal += $larrqualificationcount[$linti]['NoOfCandidates'];
			$lintcountQualification = count($larrQualificationresult);

			$lintqualificationreg[$linti] = $larrqualificationcount[$linti];
			//$lintqualificationsat[$linti] = $lintcountQualification;
			$lintqualificationpass[$linti] = 0;
			$lintqualificationfail[$linti] = 0;
			$lintqualificationabsent[$linti]=0;
$lintqualificationone[$linti]=0;
			$lintqualificationmore[$linti]=0;
            
			for($lintj=0;$lintj<$lintcountQualification;$lintj++){
				if($larrQualificationresult[$lintj]['pass']==1)
				{
                                       //attempts
							if($larrQualificationresult[$lintj]['Attempts']==1)
							{
							   $lintqualificationone[$linti]++;
							}
						    if($larrQualificationresult[$lintj]['Attempts']>1)
							{
							  $lintqualificationmore[$linti]++;
							}
							//attemptsend
					$lintqualificationpass[$linti]++;
				}elseif ($larrQualificationresult[$lintj]['pass']==2){
                                          //attempts
							if($larrQualificationresult[$lintj]['Attempts']==1)
							{
							   $lintqualificationone[$linti]++;
							}
						    if($larrQualificationresult[$lintj]['Attempts']>1)
							{
							  $lintqualificationmore[$linti]++;
							}
							//attemptsend
					$lintqualificationfail[$linti]++;
				}elseif ($larrQualificationresult[$lintj]['pass']==4){
					$lintqualificationabsent[$linti]++;
				}
			}
                         $lintqualificationsat[$linti] = $lintqualificationpass[$linti]+$lintqualificationfail[$linti];
			//$lintqualificationabsent[$linti] = $larrqualificationcount[$linti]['NoOfCandidates']-$lintqualificationsat[$linti];
		}
		$this->view->larrquali   =  $larrqualificationquali; //sending all count of candidates to the view
		$this->view->lintreg     =  $lintqualificationreg; //sending all count of candidates to the view
		$this->view->lintsat   =  $lintqualificationsat; //sending all count of candidates to the view
		$this->view->lintpass   =  $lintqualificationpass; //sending all count of candidates to the view
		$this->view->lintfail   =  $lintqualificationfail; //sending all count of candidates to the view
		$this->view->lintabsent   =  $lintqualificationabsent; //sending all count of candidates to the view

                $this->view->lintone =$lintqualificationone;
		$this->view->lintmore =$lintqualificationmore;
		}
		//////// Race  ////////
        if($lintracetype ==1){
        $larrracescount = $this->lobjallstatistics->fngetraces($lintmon,$lintyear);
        $larrraces = $this->lobjallstatistics->fngetracesnames($lintmon,$lintyear);

        for($linti=0;$linti<count($larrraces);$linti++){
        	$lintraceid = $larrraces[$linti]['idDefinition'];
        	$larrracename = $larrraces[$linti]['Race'];
        	$larrraceresult[$linti] = $this->lobjallstatistics->fngetracesresult($lintmon,$lintyear,$lintraceid);
        	$lintracecount[$linti] = count($larrraceresult[$linti]);
        	$lintracepass[$linti] = 0;
        	$lintracefail[$linti] = 0;
        	$lintraceabsent[$linti] = 0;
        	$lintracesat[$linti] = 0;

$lintraceone[$linti] = 0;
			$lintracemore[$linti] = 0;
        	for($lintj=0;$lintj<$lintracecount[$linti];$lintj++){
        		if($larrraceresult[$linti][$lintj]['pass']==1)
        		{
                                  //attempts
							if($larrraceresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintraceone[$linti]++;
							}
						    if($larrraceresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintracemore[$linti]++;
							}
							//attemptsend
        			$lintracepass[$linti]++;
        		}elseif ($larrraceresult[$linti][$lintj]['pass']==2){
                           //attempts
							if($larrraceresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintraceone[$linti]++;
							}
						    if($larrraceresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintracemore[$linti]++;
							}
							//attemptsend
        			$lintracefail[$linti]++;
        		}elseif ($larrraceresult[$linti][$lintj]['pass']==4){

 


        			$lintraceabsent[$linti]++;
        		}
        	}
        	$lintracesat[$linti] = $lintracepass[$linti]+$lintracefail[$linti];
        	//$lintraceabsent[$linti] = $larrracescount[$linti]['NoOfCandidates'] - $lintracesat[$linti];
        }
        $this->view->racename = $larrracescount;
        $this->view->racenames = $larrraces;
        $this->view->racepass = $lintracepass;
        $this->view->racefail = $lintracefail;
        $this->view->raceabsent = $lintraceabsent;
        $this->view->racesat = $lintracesat;

$this->view->raceone = $lintraceone;
		$this->view->racemore = $lintracemore;
        }
/////////// Category ////////////
        if($lintcategorytype == 1)
        {
        $larrprograms = $this->lobjallstatistics->fngetprogramnames();
        $larrprogramcount = $this->lobjallstatistics->fngetprograms($lintmon,$lintyear);
        for($linti=0;$linti<count($larrprograms);$linti++){
        	$lintpartabid = $larrprograms[$linti]['IdProgrammaster'];
        	$larrprogramname = $larrprograms[$linti]['ProgramName'];
        	$larrpartabresult[$linti] = $this->lobjallstatistics->fngetprogramresult($lintmon,$lintyear,$lintpartabid);
        	$lintprogrampartabcount[$linti] = count($larrpartabresult[$linti]);
        	$lintprogrampass[$linti] = 0;
        	$lintprogramfail[$linti] = 0;
        	$lintprogramabsent[$linti] = 0;
        	$lintprogramsat[$linti] = 0;
$lintprogramone[$linti] = 0;
			$lintprogrammore[$linti] = 0;
        	for($lintj=0;$lintj<$lintprogrampartabcount[$linti];$lintj++){
        		if($larrpartabresult[$linti][$lintj]['pass']==1)
        		{
                                                      //attempts
							if($larrpartabresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintprogramone[$linti]++;
							}
						    if($larrpartabresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintprogrammore[$linti]++;
							}
							//attemptsend
        			$lintprogrampass[$linti]++;
        		}elseif ($larrpartabresult[$linti][$lintj]['pass']==2){
 //attempts
							if($larrpartabresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintprogramone[$linti]++;
							}
						    if($larrpartabresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintprogrammore[$linti]++;
							}
							//attemptsend
        			$lintprogramfail[$linti]++;
        		}elseif ($larrpartabresult[$linti][$lintj]['pass']==4){
        			$lintprogramabsent[$linti]++;
        		}
        	}
        	$lintprogramsat[$linti] = $lintprogrampass[$linti]+$lintprogramfail[$linti];
        	//$lintprogramabsent[$linti] = $larrprogramcount[$linti]['NoOfCandidates'] - $lintprogramsat[$linti];
        }
        $this->view->programname = $larrprogramcount;
        $this->view->programs = $larrprograms;
        $this->view->pass = $lintprogrampass;
        $this->view->fail = $lintprogramfail;
        $this->view->absent = $lintprogramabsent;
        $this->view->sat = $lintprogramsat;
 $this->view->One = $lintprogramone;
		  $this->view->More =  $lintprogrammore;
		}
		///////// Centre ///////////
		if($lintcentretype == 1){
		$larrcentrenames = $this->lobjallstatistics->fngetcentrenamesparam($lintmon,$lintyear);
		for($linti=0;$linti<count($larrcentrenames);$linti++)
		{
			$larrresult[$linti] = $this->lobjallstatistics->fngetcentres($lintmon,$lintyear,$larrcentrenames[$linti]['idcenter']);
			//echo "<pre>";print_r($larrresult);die();
			$lintexamcentrereg[$linti] = count($larrresult[$linti]);
			$lintexamcentrepass[$linti] = 0;$lintexamcentrefail[$linti] = 0;
$lintexamcentreabsent[$linti] = 0;
$lintexamcentresat[$linti] = 0;
$lintexamcentreOne[$linti] = 0;
			$lintexamcentreMore[$linti] = 0;
			for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++){
				if($larrresult[$linti][$lintj]['pass']==1){
//attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintexamcentreOne[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintexamcentreMore[$linti]++;
							}
							//attemptsend
					$lintexamcentrepass[$linti]++;
				}elseif($larrresult[$linti][$lintj]['pass']==2){
 //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintexamcentreOne[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintexamcentreMore[$linti]++;
							}
							//attemptsend
					$lintexamcentrefail[$linti]++;
				}elseif($larrresult[$linti][$lintj]['pass']==4){
					$lintexamcentreabsent[$linti]++;
				}
			}
			$lintexamcentresat[$linti] = $lintexamcentrepass[$linti]+$lintexamcentrefail[$linti];
			//$lintexamcentreabsent[$linti] = $lintexamcentrereg[$linti] -($lintexamcentrepass[$linti]+$lintexamcentrefail[$linti]);
		}
		$this->view->centredata = $lintexamcentrereg;
		$this->view->centres = $larrcentrenames;
		$this->view->centrepass = $lintexamcentrepass;
		$this->view->centrefail = $lintexamcentrefail;
		$this->view->centreabsent = $lintexamcentreabsent;
		$this->view->centresat = $lintexamcentresat;

$this->view->CenterOne = $lintexamcentreOne;
		$this->view->CenterMore = $lintexamcentreMore;
		
		/////////	
	 }
	 //////////////////// Session Wise /////////////////////////
	 
		if($lintsessiontype == 1){
		      $larrsessions =  $this->lobjallstatistics->fngetsessions($lintmon,$lintyear);
/*$larrsessions =  $this->lobjallstatistics->fngetsessions($lintmon,$lintyear);
 echo "<pre>";print_r($larrsessions);die();*/
                     
			$this->view->availsessions = $larrsessions =  $this->lobjallstatistics->fngetsessions($lintmon,$lintyear);

		      $larrages = $this->lobjallstatistics->fngetages1($lintmon,$lintyear);
		      for($linti=0;$linti<count($larrsessions);$linti++)
				{
					$lintsessionpass[$linti]=0;
$lintsessionfail[$linti]=0;
$lintsessionabsent[$linti]=0;
$lintsessionsat[$linti]=0;
$lintsessionone[$linti]=0;
$lintsessionmore[$linti]=0;
					$larrresult[$linti] = $this->lobjallstatistics->fngetsessionresult($lintmon,$lintyear,$larrsessions[$linti]['idmangesession']);
					
					for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++)
					{
						
						if($larrresult[$linti][$lintj]['pass']==1){
							//attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintsessionone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintsessionmore[$linti]++;
							}
							//attemptsend
							$lintsessionpass[$linti]++;
						}elseif($larrresult[$linti][$lintj]['pass']==2){

							//attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintsessionone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintsessionmore[$linti]++;
							}
							//attemptsend
							$lintsessionfail[$linti]++;
						}elseif($larrresult[$linti][$lintj]['pass']==4){
							$lintsessionabsent[$linti]++;
						}
			        }
			        $lintsessionsat[$linti]= $lintsessionpass[$linti]+$lintsessionfail[$linti];
				}
                               

				$this->view->sessionpass = $lintsessionpass;
		                $this->view->sessionfail = $lintsessionfail;
				$this->view->sessionabsent = $lintsessionabsent;
				$this->view->sessionsat = $lintsessionsat;
                                $this->view->sessionone = $lintsessionone;
				$this->view->sessionmore = $lintsessionmore;
		    
		}
	 //////////////////  Session Wise /////////////////////////
		
		$year = 2012;
		$lintcuryear = date("Y");
		for($linti=$year;$linti<=$lintcuryear;$linti++){
			$larryears[$linti] = $linti;
		}
		$this->lobjStasticsForm->Type->setValue($linttype); 
		$this->lobjStasticsForm->Year->addmultioptions($larryears);
		$this->lobjStasticsForm->Year->setValue($lintyear);
		if($lintyear == $lintcuryear){
			    $larrmonthsresult = $this->lobjallstatistics->fngetmonthsearch(1);
			}elseif($lintyear == '2012'){
				$larrmonthsresult = $this->lobjallstatistics->fngetmonthsearch(3);
			}else{
				$larrmonthsresult = $this->lobjallstatistics->fngetmonthsearch(2);
			}
		$this->lobjStasticsForm->Month->addmultioptions($larrmonthsresult);
		$this->lobjStasticsForm->Month->setValue($lintmon);
		
   }
}
}

public function fnexportexcelAction()
{
	$this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$larrformData = $this->_request->getPost();
	$lintmon = $larrformData['lmonth'];
	$lintyear = $larrformData['lyear'];
	$linttype = $larrformData['linttype'];
	unset ( $larrformData['ExportToExcel']);
	$lday= date("d-m-Y");
	$ltime = date('h:i:s',time());
  if($lintmon == 1 || $lintmon == '01'){
				   	$lstrmonthname = "January";
				   }
				 if($lintmon == 2 || $lintmon == '02'){
				   	$lstrmonthname = "February";
				   }
				 if($lintmon == 3 || $lintmon == '03'){
				   	$lstrmonthname = "March";
				   }
				 if($lintmon == 4 || $lintmon == '04'){
				   	$lstrmonthname = "April";
				   }
				 if($lintmon == 5 || $lintmon == '05'){
				   	$lstrmonthname = "May";
				   }
				 if($lintmon == 6 || $lintmon == '06'){
				   	$lstrmonthname = "June";
				   }
				 if($lintmon == 7 || $lintmon == '07'){
				   	$lstrmonthname = "July";
				   }
				 if($lintmon == 8 || $lintmon == '08'){
				   	$lstrmonthname = "August";
				   }
				 if($lintmon == 9 || $lintmon == '09'){
				   	$lstrmonthname = "September";
				   }
				 if($lintmon == 10){
				   	$lstrmonthname = "October";
				   }
				 if($lintmon == 11){
				   	$lstrmonthname = "November";
				   }
				 if($lintmon == 12){
				   	$lstrmonthname = "December";
				   }
	$lstrmonthyear = $lstrmonthname.'_'.$lintyear;
	$lintagetype = 0;$lintcategorytype = 0;$lintcentretype = 0;$lintresulttype = 0;$lintgendertype = 0;$lintracetype = 0; $lintqualificationtype = 0;
	switch($linttype)
		{
		  case 1: $lintagetype = 1;$filename = 'Age_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 2: $lintcategorytype = 1;$filename = 'Exam_Category_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 3: $lintcentretype = 1;$filename = 'Exam_Centre_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 4: $lintresulttype = 1;$filename = 'Result_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 5: $lintgendertype = 1;$filename = 'Gender_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 6: $lintracetype = 1;$filename = 'Race_Wise_Statistics_Report_'.$lstrmonthyear;
				  break;
		  case 7: $lintqualificationtype = 1;$filename = 'Qualification_Wise_Statistics_Report_'.$lstrmonthyear;	
				  break;
	      case 9: $lintsessiontype = 1;$filename = 'Session_Wise_Statistics_Report_'.$lstrmonthyear;	
				  break;
		  default:$lintagetype = 1;$lintcategorytype = 1;$lintcentretype = 1;$lintresulttype = 1;$lintgendertype = 1;$lintracetype = 1; $lintqualificationtype = 1;$lintsessiontype = 1;
				  $filename = 'All_Statistics_Report_'.$lstrmonthyear;
		          break;
	   }
	$host = $_SERVER['SERVER_NAME'];
	$imgp = "http://".$host."/tbenew/images/reportheader.jpg";  
	$tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
	$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td align ='left' colspan = '5'><b>Date </b></td><td align ='left' colspan = '6'><b>$lday</b></td><td align ='left' colspan = '5'><b> Time</b></td><td align ='left' colspan = '7'><b>$ltime</b></td></tr></table>"; 
	
	
	if($lintagetype==1)
	{   
		$ReportName = $this->view->translate( "Age Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		$tabledata.="<table border=1 align=center width=100%><tr style='background-color:Gray'><td colspan = 23 align=center><b>{$ReportName}</b></td></tr></table><br>";
		$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td align ='center' colspan = 23><b>".$lstrmonthyear."</b></td></tr>";
		

$tabledata.="<tr style='background-color:Gray'> <td align ='left' colspan = 1><b>Age</b></td><td align ='center' colspan = 2><b>18-21</b></td><td align ='center' colspan = 2><b>22-25</b></td><td align ='center' colspan = 2><b>26-29</b></td><td align ='center' colspan = 2><b>30-34</b></td><td align ='center' colspan = 2><b>35-39</b></td><td align ='center' colspan = 2><b>40-44</b></td><td align ='center' colspan = 2><b>45-49</b></td><td align ='center' colspan = 2><b>50-54</b></td><td align ='center' colspan = 2><b>55-59</b></td><td align ='center' colspan = 3><b>60 Above</b></td><td align ='center' colspan = 1><b>Total</b></td></tr>";
		


$tabledata.="<tr style='background-color:#EEDD82'> <td align ='left' colspan = 1><b>Registered</b></td><td align ='center' colspan = 2>".$larrformData['18-21']."</td><td align ='center' colspan = 2>".$larrformData['22-25']."</td><td align ='center' colspan = 2>".$larrformData['26-29']."</td><td align ='center' colspan = 2>".$larrformData['30-34']."</td><td align ='center' colspan = 2>".$larrformData['35-39']."</td><td align ='center' colspan = 2>".$larrformData['40-44']."</td><td align ='center' colspan = 2>".$larrformData['45-49']."</td><td align ='center' colspan = 2>".$larrformData['50-54']."</td><td align ='center' colspan = 2>".$larrformData['55-59']."</td><td align ='center' colspan = 3>".$larrformData['60Above']."</td><td align ='center' colspan = 1>".$larrformData['regtotal']."</td></tr>";
$tabledata.='<tr style="background-color:#E6E6FA"> 


		<td align ="left" colspan = 1><b>Sat</b></td><td align ="center" colspan = 2>
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 2 align ="center">'.$larrformData['18-21sat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 2 align ="center" >'.'['.$larrformData['18-21satattempts'].'/'.$larrformData['18-21satattemptsmore'].']'.'</td></tr></table></b></td><td align ="center" colspan = 2>
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 2 align ="center">'.$larrformData['22-25sat'].'</td></tr><tr style="background-color:#E6E6FA" ><td  colspan = 2 align ="center">'.'['.$larrformData['22-25satattempts'].'/'.$larrformData['22-25satattemptsmore'].']'.'</td></tr></table></b></td><td align ="center" colspan = 2>
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td  colspan = 2 align ="center">'.$larrformData['26-29sat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 2 align ="center">'.'['.$larrformData['26-29satattempts'].'/'.$larrformData['26-29satattemptsmore'].']'.'</td></tr></table></b></td><td align ="center" colspan = 2>
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 2 align ="center">'.$larrformData['30-34sat'].'</td></tr><tr style="background-color:#E6E6FA" ><td  colspan = 2 align ="center">'.'['.$larrformData['30-34satattempts'].'/'.$larrformData['30-34satattemptsmore'].']'.'</td></tr></table></b></td><td align ="center" colspan = 2>
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 2 align ="center">'.$larrformData['35-39sat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 2  align ="center">'.'['.$larrformData['35-39satattempts'].'/'.$larrformData['35-39satattemptsmore'].']'.'</td></tr></table></b></td><td align ="center" colspan = 2>
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 2 align ="center">'.$larrformData['40-44sat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 2 align ="center">'.'['.$larrformData['40-44satattempts'].'/'.$larrformData['40-44satattemptsmore'].']'.'</td></tr></table></b></td><td align ="center" colspan = 2>
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 2 align ="center">'.$larrformData['45-49sat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 2 align ="center">'.'['.$larrformData['45-49satattempts'].'/'.$larrformData['45-49satattemptsmore'].']'.'</td></tr></table></b></td><td align ="center" colspan = 2>
		
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 2 align ="center">'.$larrformData['50-54sat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 2 align ="center">'.'['.$larrformData['50-54satattempts'].'/'.$larrformData['50-54satattemptsmore'].']'.'</td></tr></table></b></td><td align ="center" colspan = 2>
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 2 align ="center">'.$larrformData['55-59sat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 2 align ="center">'.'['.$larrformData['55-59satattempts'].'/'.$larrformData['55-59satattemptsmore'].']'.'</td></tr></table></b></td><td align ="center"  colspan = 3>
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 3 align ="center">'.$larrformData['60Abovesat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 3 align ="center">'.'['.$larrformData['60Abovesatattempts'].'/'.$larrformData['60Abovesatattemptsmore'].']'.'</td></tr></table></b></td><td align ="center" colspan = 1>
		
		<table  align=center width=100% >
		<tr style="background-color:#E6E6FA" align ="center"><td colspan = 1 align ="center">'.$larrformData['sattotal'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 1 align ="center">'.'['.$larrformData['sattotalattemptsone'].'/'.$larrformData['sattotalattemptsmore'].']'.'</td></tr></table></b></td></tr>';
		

		$tabledata.="<tr style='background-color:#ADFF2F'> <td align ='left' colspan = 1><b>Pass</b></td><td align ='center' colspan = 2>".$larrformData['18-21pass']."</td><td align ='center' colspan = 2>".$larrformData['22-25pass']."</td><td align ='center' colspan = 2>".$larrformData['26-29pass']."</td><td align ='center' colspan = 2>".$larrformData['30-34pass']."</td><td align ='center' colspan = 2>".$larrformData['35-39pass']."</td><td align ='center' colspan = 2>".$larrformData['40-44pass']."</td><td align ='center' colspan = 2>".$larrformData['45-49pass']."</td><td align ='center' colspan = 2>".$larrformData['50-54pass']."</td><td align ='center' colspan = 2>".$larrformData['55-59pass']."</td><td align ='center' colspan = 3>".$larrformData['60Abovepass']."</td><td align ='center' colspan = 1>".$larrformData['passtotal']."</td></tr>";
		$tabledata.="<tr style='background-color:#FF6347'> <td align ='left' colspan = 1><b>Fail</b></td><td align ='center' colspan = 2>".$larrformData['18-21fail']."</td><td align ='center' colspan = 2>".$larrformData['22-25fail']."</td><td align ='center' colspan = 2>".$larrformData['26-29fail']."</td><td align ='center' colspan = 2>".$larrformData['30-34fail']."</td><td align ='center' colspan = 2>".$larrformData['35-39fail']."</td><td align ='center' colspan = 2>".$larrformData['40-44fail']."</td><td align ='center' colspan = 2>".$larrformData['45-49fail']."</td><td align ='center' colspan = 2>".$larrformData['50-54fail']."</td><td align ='center' colspan = 2>".$larrformData['55-59fail']."</td><td align ='center' colspan = 3>".$larrformData['60Abovefail']."</td><td align ='center' colspan = 1>".$larrformData['failtotal']."</td></tr>";
		$tabledata.="<tr style='background-color:#BDB76B'> <td align ='left' colspan = 1><b>Absent</b></td><td align ='center' colspan = 2>".$larrformData['18-21absent']."</td><td align ='center' colspan = 2>".$larrformData['22-25absent']."</td><td align ='center' colspan = 2>".$larrformData['26-29absent']."</td><td align ='center' colspan = 2>".$larrformData['30-34absent']."</td><td align ='center' colspan = 2>".$larrformData['35-39absent']."</td><td align ='center' colspan = 2>".$larrformData['40-44absent']."</td><td align ='center' colspan = 2>".$larrformData['45-49absent']."</td><td align ='center' colspan = 2>".$larrformData['50-54absent']."</td><td align ='center' colspan = 2>".$larrformData['55-59absent']."</td><td align ='center' colspan = 3>".$larrformData['60Aboveabsent']."</td><td align ='center' colspan = 1>".$larrformData['absenttotal']."</td></tr></table><br>";
	}
	
	if($lintcategorytype == 1)
        {
	        $ReportName = $this->view->translate( "Exam Category Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
        	$larrprograms = $this->lobjallstatistics->fngetprogramnames();
	        $larrprogramcount = $this->lobjallstatistics->fngetprograms($lintmon,$lintyear);
	        for($linti=0;$linti<count($larrprograms);$linti++){
	        	$lintpartabid = $larrprograms[$linti]['IdProgrammaster'];
	        	$larrprogramname = $larrprograms[$linti]['ProgramName'];
	        	$larrpartabresult[$linti] = $this->lobjallstatistics->fngetprogramresult($lintmon,$lintyear,$lintpartabid);
	        	$lintprogrampartabcount[$linti] = count($larrpartabresult[$linti]);
	        	$lintprogrampass[$linti] = 0;
	        	$lintprogramfail[$linti] = 0;
	        	$lintprogramabsent[$linti] = 0;
	        	$lintprogramsat[$linti] = 0;
                        $lintprogramone[$linti] = 0;
			$lintprogrammore[$linti] = 0;
	        	for($lintj=0;$lintj<$lintprogrampartabcount[$linti];$lintj++){
	        		if($larrpartabresult[$linti][$lintj]['pass']==1)
	        		{
                                           //attempts
							if($larrpartabresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintprogramone[$linti]++;
							}
						    if($larrpartabresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintprogrammore[$linti]++;
							}
							//attemptsend
	        			$lintprogrampass[$linti]++;
	        		}elseif ($larrpartabresult[$linti][$lintj]['pass']==2){
                                                               //attempts
							if($larrpartabresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintprogramone[$linti]++;
							}
						    if($larrpartabresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintprogrammore[$linti]++;
							}
							//attemptsend
	        			$lintprogramfail[$linti]++;
	        		}elseif ($larrpartabresult[$linti][$lintj]['pass']==4){
	        			$lintprogramabsent[$linti]++;
	        		}
	        	}
	        	$lintprogramsat[$linti] = $lintprogrampass[$linti]+$lintprogramfail[$linti];
	        	//$lintprogramabsent[$linti] = $larrprogramcount[$linti]['NoOfCandidates'] - $lintprogramsat[$linti];
	        }
			$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td colspan = '23' align=center><b>{$ReportName}</b></td></tr></table><br>";
			$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td align ='center' colspan = '23'><b>".$lstrmonthyear."</b></td></tr>";
			 
			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:Gray"><td><b>ExamCategory/Type</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
				if($larrprograms[$lvars]['IdProgrammaster']==4){
					$tabledata.='<td align ="center" colspan = 6><b>'.$larrprograms[$lvars]['ProgramName'].'</b></td>';
				}else{
				$tabledata.='<td align ="center" colspan = 5><b>'.$larrprograms[$lvars]['ProgramName'].'</b></td>';
			}
			}
			$tabledata.='<td align ="center" colspan = 1><b>Total</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#EEDD82"><td><b>Registered</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
				$TotalNoOfCandidates = $TotalNoOfCandidates+ $larrprogramcount[$lvars]['NoOfCandidates'];
			if($larrprograms[$lvars]['IdProgrammaster']==4){
				if($larrprogramcount[$lvars]['NoOfCandidates'])
				$tabledata.='<td align ="center" colspan = 6><b>'.$larrprogramcount[$lvars]['NoOfCandidates'].'</b></td>';
				else
				$tabledata.='<td align ="center" colspan = 6><b>0</b></td>';
				}else{
				if($larrprogramcount[$lvars]['NoOfCandidates'])
				$tabledata.='<td align ="center" colspan = 5><b>'.$larrprogramcount[$lvars]['NoOfCandidates'].'</b></td>';
				else
				$tabledata.='<td align ="center" colspan = 5><b>0</b></td>';
			}
			}
			$tabledata.='<td align ="center" colspan = 1><b>'.$TotalNoOfCandidates.'</b></td></tr>';
			



$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#E6E6FA"><td><b>Sat</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
				if($larrprograms[$lvars]['IdProgrammaster']==4){
					$tabledata.='<td align ="center" colspan = 6><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = 6 align ="center">'.$lintprogramsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 6 align ="center">'.'['.$lintprogramone[$lvars].'/'.$lintprogrammore[$lvars].']'.'</td></tr></table></b></td>';
				}else{
				    $tabledata.='<td align ="center" colspan = 5><b><table  align=center width=100%><tr style="background-color:#E6E6FA"><td colspan = 5 align ="center">'.$lintprogramsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 5 align ="center">'.'['.$lintprogramone[$lvars].'/'.$lintprogrammore[$lvars].']'.'</td></tr></table></b></td>';
				}
			}
			$Totalsat = array_sum($lintprogramsat);
			$tabledata.='<td align ="center" colspan = 1><b><table  align=center width=100%><tr style="background-color:#E6E6FA" ><td align ="center">'.$Totalsat.'</td></tr><tr style="background-color:#E6E6FA" ><td align ="center">'.'['.array_sum($lintprogramone).'/'.array_sum($lintprogrammore).']'.'</td></tr></table></b></b></td></tr>';
			
			




			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#ADFF2F"><td><b>Pass</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
			if($larrprograms[$lvars]['IdProgrammaster']==4){
					$tabledata.='<td align ="center" colspan = 6><b>'.$lintprogrampass[$lvars].'</b></td>';
				}else{
				$tabledata.='<td align ="center" colspan = 5><b>'.$lintprogrampass[$lvars].'</b></td>';
				}
			}
			$Totalpass = array_sum($lintprogrampass);
			$tabledata.='<td align ="center" colspan = 1><b>'.$Totalpass.'</b></td></tr>';
		
			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#FF6347"><td><b>Fail</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
			if($larrprograms[$lvars]['IdProgrammaster']==4){
					$tabledata.='<td align ="center" colspan = 6><b>'.$lintprogramfail[$lvars].'</b></td>';
				}else{
				$tabledata.='<td align ="center" colspan = 5><b>'.$lintprogramfail[$lvars].'</b></td>';
				}
				
			}
			$Totalfail = array_sum($lintprogramfail);
			$tabledata.='<td align ="center" colspan = 1><b>'.$Totalfail.'</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#BDB76B"><td><b>Absent</b></td>';
			for($lvars=0;$lvars<count($larrprograms);$lvars++){
			if($larrprograms[$lvars]['IdProgrammaster']==4){
					$tabledata.='<td align ="center" colspan = 6><b>'.$lintprogramabsent[$lvars].'</b></td>';
				}else{
				$tabledata.='<td align ="center" colspan = 5><b>'.$lintprogramabsent[$lvars].'</b></td>';
				}
			}
			$Totalabsent = array_sum($lintprogramabsent);
			$tabledata.='<td align ="center" colspan = 1><b>'.$Totalabsent.'</b></td></tr></table><br>';
     }
	
	
		if($lintcentretype == 1)
	{
		$ReportName = $this->view->translate( "Exam Centre Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		$larrcentrenames = $this->lobjallstatistics->fngetcentrenamesparam($lintmon,$lintyear);
		$larrcentnames =  $this->lobjallstatistics->fngetcentrenames();
		$lintnoofcentres = count($larrcentnames)-2;
		$lintdiff = $lintnoofcentres - count($larrcentrenames);
		$colsp = (int)($lintnoofcentres / count($larrcentrenames));
		for($linti=0;$linti<count($larrcentrenames);$linti++)
		{
			$larrresult[$linti] = $this->lobjallstatistics->fngetcentres($lintmon,$lintyear,$larrcentrenames[$linti]['idcenter']);
			$lintexamcentrereg[$linti] = count($larrresult[$linti]);
			$lintexamcentrepass[$linti] = 0;
$lintexamcentrefail[$linti] = 0;
$lintexamcentreabsent[$linti] = 0;
$lintexamcentresat[$linti] = 0;
$lintexamcentreOne[$linti] = 0;
			$lintexamcentreMore[$linti] = 0;
			for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++){
				if($larrresult[$linti][$lintj]['pass']==1){
                                         if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintexamcentreOne[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintexamcentreMore[$linti]++;
							}
							//attemptsend
					$lintexamcentrepass[$linti]++;
				}elseif($larrresult[$linti][$lintj]['pass']==2){
                                             if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintexamcentreOne[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintexamcentreMore[$linti]++;
							}
							//attemptsend
					$lintexamcentrefail[$linti]++;
				}elseif($larrresult[$linti][$lintj]['pass']==4){
					$lintexamcentreabsent[$linti]++;
				}
			}
			$lintexamcentresat[$linti] = $lintexamcentrepass[$linti]+$lintexamcentrefail[$linti];
			//$lintexamcentreabsent[$linti] = $lintexamcentrereg[$linti] -($lintexamcentrepass[$linti]+$lintexamcentrefail[$linti]);
		}
		$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td colspan = 23 align=center><b>{$ReportName}</b></td></tr></table><br>";
		$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td align ='left' colspan = 1> </td><td align ='center' colspan = 22><b>".$lstrmonthyear."</b></td></tr>";
	
		$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:Gray"><td colspan = "1"><b>Exam Centre</b></td>';
		if(count($larrcentrenames) == $lintnoofcentres){
			for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
				
				$tabledata.='<td align ="center" colspan = 1><b>'.$larrcentrenames[$lvars]['centername'].'</b></td>';
			}
		}else{
			for($lvars=0;$lvars<$lintnoofcentres+1;$lvars++){
				$cspan = 1+ $colsp;
				$tabledata.='<td align ="center" colspan = ".$cspan."><b>'.$larrcentrenames[$lvars]['centername'].'</b></td>';
			}
		}
		$tabledata.='<td align ="center" colspan = "1"><b>Total</b></td></tr>';
		
		$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#EEDD82"><td><b>Registered</b></td>';
		if(count($larrcentrenames) == $lintnoofcentres){
			for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
				if($lintexamcentrereg[$lvars])
					$tabledata.='<td align ="center" colspan = "1"><b>'.$lintexamcentrereg[$lvars].'</b></td>';
				else
					$tabledata.='<td align ="center" colspan = "1"><b>0</b></td>';
			}
			}else{
			for($lvars=0;$lvars<$lintnoofcentres;$lvars++){
				$cspan = 1+ $colsp;
				$tabledata.='<td align ="center" colspan = ".$cspan."><b>'.$lintexamcentrereg[$lvars].'</b></td>';
			}
		}
		
		$tabledata.='<td align ="center" colspan = 1><b>'.array_sum($lintexamcentrereg).'</b></td></tr>';


		$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#E6E6FA"><td><b>Sat</b></td>';
		if(count($larrcentrenames) == $lintnoofcentres){
			for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
				$tabledata.='<td align ="center" colspan = 1><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = 1 align ="center">'.$lintexamcentresat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 1 align ="center">'.'['.$lintexamcentreOne[$lvars].'/'.$lintexamcentreMore[$lvars].']'.'</td></tr></table></b></td>';
			}
			}else{
			for($lvars=0;$lvars<$lintnoofcentres;$lvars++){
				$cspan = 1+ $colsp;
				$tabledata.='<td align ="center" colspan = ".$cspan."><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan =".$cspan." align ="center">'.$lintexamcentresat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = ".$cspan." align ="center">'.'['.$lintexamcentreOne[$lvars].'/'.$lintexamcentreMore[$lvars].']'.'</td></tr></table></b></td>';
			}
		}
		$Totalsat = array_sum($lintexamcentresat);
		$tabledata.='<td align ="center" colspan = 1><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = 1 align ="center">'.$Totalsat.'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 1 align ="center">'.'['.array_sum($lintexamcentreOne).'/'.array_sum($lintexamcentreMore).']'.'</td></tr></table></b></td></tr>';


		
		
		$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#ADFF2F"><td><b>Pass</b></td>';
		if(count($larrcentrenames) == $lintnoofcentres){
			for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
				$tabledata.='<td align ="center" colspan = 1><b>'.$lintexamcentrepass[$lvars].'</b></td>';
			}
			}else{
			for($lvars=0;$lvars<$lintnoofcentres;$lvars++){
				$cspan = 1+ $colsp;
				$tabledata.='<td align ="center" colspan = ".$cspan."><b>'.$lintexamcentrepass[$lvars].'</b></td>';
			}
		}
		$Totalpass = array_sum($lintexamcentrepass);
		$tabledata.='<td align ="center" colspan = 1><b>'.$Totalpass.'</b></td></tr>';
	
		$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#FF6347"><td><b>Fail</b></td>';
		if(count($larrcentrenames) == $lintnoofcentres){
			for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
				$tabledata.='<td align ="center" colspan = 1><b>'.$lintexamcentrefail[$lvars].'</b></td>';
			}
			}else{
			for($lvars=0;$lvars<$lintnoofcentres;$lvars++){
				$cspan = 1+ $colsp;
				$tabledata.='<td align ="center" colspan = ".$cspan."><b>'.$lintexamcentrefail[$lvars].'</b></td>';
			}
		}
		$Totalfail = array_sum($lintexamcentrefail);
		$tabledata.='<td align ="center" colspan = 1><b>'.$Totalfail.'</b></td></tr>';
		
		$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#BDB76B"><td><b>Absent</b></td>';
		if(count($larrcentrenames) == $lintnoofcentres){
				for($lvars=0;$lvars<count($larrcentrenames);$lvars++){
					$tabledata.='<td align ="center" colspan =1><b>'.$lintexamcentreabsent[$lvars].'</b></td>';
				}
			}else{
				for($lvars=0;$lvars<$lintnoofcentres;$lvars++){
					$cspan = 1+ $colsp;
					$tabledata.='<td align ="center" colspan = ".$cspan."><b>'.$lintexamcentreabsent[$lvars].'</b></td>';
			    }
		}
		$Totalabsent = array_sum($lintexamcentreabsent);
		$tabledata.='<td align ="center" colspan =1><b>'.$Totalabsent.'</b></td></tr></table><br>';
		}
	
		if($lintgendertype==1)
		{
			$ReportName = $this->view->translate( "Gender Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
			$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td colspan = '23' align=center><b>{$ReportName}</b></td></tr></table><br>";
			$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td align ='center' colspan = '23'><b>".$lstrmonthyear."</b></td></tr>";
			$tabledata.="<tr style='background-color:Gray'> <td align ='left' colspan = 1><b>Gender</b></td><td align ='center' colspan = '10'><b>Male</b></td><td align ='center' colspan = '11'><b>Female</b></td><td align ='center' colspan = '1'><b>Total</b></td></tr>";
			$tabledata.="<tr style='background-color:#EEDD82'> <td align ='left' colspan = 1><b>Registered</b></td><td align ='center' colspan = '10'>".$larrformData['malecand']."</td><td align ='center' colspan = '11'>".$larrformData['femalecand']."</td><td align ='center' colspan = '1'>".$larrformData['regtotal1']."</td></tr>";
			

$tabledata.='<tr style="background-color:#E6E6FA"> <td align ="left" colspan = 1><b>Sat</b></td><td align ="center" colspan = "10"><table  align=center ><tr style="background-color:#E6E6FA"><td colspan = "10" align ="center">'.$larrformData['msat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 10 align ="center">'.$larrformData['msatattempts'].'</td></tr></table></b></td>
			<td align ="center" colspan = "11"><table  align=center  ><tr style="background-color:#E6E6FA"><td colspan = "11" align ="center">'.$larrformData['fsat'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 11 align ="center">'.$larrformData['fsatattempts'].'</td></tr></table></b></td>
			<td align ="center" colspan = "1"><table  align=center  ><tr style="background-color:#E6E6FA"><td colspan = "1" align ="center">'.$larrformData['sattotal'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = 1 align ="center">'.$larrformData['sattotalattempts'].'</td></tr></table></b></td></tr>';
			


			$tabledata.="<tr style='background-color:#ADFF2F'> <td align ='left' colspan = 1><b>Pass</b></td><td align ='center' colspan = '10'>".$larrformData['mpass']."</td><td align ='center' colspan = '11'>".$larrformData['fpass']."</td><td align ='center' colspan = '1'>".$larrformData['passtotal']."</td></tr>";
			$tabledata.="<tr style='background-color:#FF6347'> <td align ='left' colspan = 1><b>Fail</b></td><td align ='center' colspan = '10'>".$larrformData['mfail']."</td><td align ='center' colspan = '11'>".$larrformData['ffail']."</td><td align ='center' colspan = '1'>".$larrformData['failtotal']."</td></tr>";
			$tabledata.="<tr style='background-color:#BDB76B'> <td align ='left' colspan = 1><b>Absent</b></td><td align ='center' colspan = '10'>".$larrformData['mabsent']."</td><td align ='center' colspan = '11'>".$larrformData['fabsent']."</td><td align ='center' colspan ='1'>".$larrformData['absenttotal']."</td></tr></table><br>";
		}	
			
		if($lintracetype ==1)
		{
	        $ReportName = $this->view->translate( "Race Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
			$larrracescount = $this->lobjallstatistics->fngetraces($lintmon,$lintyear);
	        $larrraces = $this->lobjallstatistics->fngetracesnames($lintmon,$lintyear);
	       // print_r($larrraces);die();
	        for($linti=0;$linti<count($larrraces);$linti++)
	        {
	        	$lintraceid = $larrraces[$linti]['idDefinition'];
	        	$larrracename = $larrraces[$linti]['Race'];
	        	$larrraceresult[$linti] = $this->lobjallstatistics->fngetracesresult($lintmon,$lintyear,$lintraceid);
	        	$lintracecount[$linti] = count($larrraceresult[$linti]);
	        	$lintracepass[$linti] = 0;
	        	$lintracefail[$linti] = 0;
	        	$lintraceabsent[$linti] = 0;
	        	$lintracesat[$linti] = 0;

$lintraceone[$linti] = 0;
			    $lintracemore[$linti] = 0;
	        	for($lintj=0;$lintj<$lintracecount[$linti];$lintj++){
	        		if($larrraceresult[$linti][$lintj]['pass']==1)
	        		{
                                        //attempts
							if($larrraceresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintraceone[$linti]++;
							}
						    if($larrraceresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintracemore[$linti]++;
							}
							//attemptsend
	        			$lintracepass[$linti]++;
	        		}elseif($larrraceresult[$linti][$lintj]['pass']==2){
                                   //attempts
							if($larrraceresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintraceone[$linti]++;
							}
						    if($larrraceresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintracemore[$linti]++;
							}
							//attemptsend

	        			$lintracefail[$linti]++;
	        		}
	        	    elseif($larrraceresult[$linti][$lintj]['pass']==4){
	        			$lintraceabsent[$linti]++;
	        		}
	        	}
	            $lintracesat[$linti] = $lintracepass[$linti]+$lintracefail[$linti];
	            //$lintraceabsent[$linti] = $larrracescount[$linti]['NoOfCandidates'] - $lintracesat[$linti];
            }
	        
	        $tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td colspan = '23' align=center><b>{$ReportName}</b></td></tr></table><br>";
			$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td align ='center' colspan = '23'><b>".$lstrmonthyear."</b></td></tr>";
			$tabledata.= '<table border=1 align=center width=100% colapan = "1"><tr style="background-color:Gray"><td><b>Race</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
				if($larrraces[$lvars]['idDefinition']==166){
					$tabledata.='<td align ="center" colspan = "6"><b>'.$larrraces[$lvars]['RaceName'].'</b></td>';
				}else{
					$tabledata.='<td align ="center" colspan = "5"><b>'.$larrraces[$lvars]['RaceName'].'</b></td>';
				}
			}
			$tabledata.='<td align ="center" colspan = "1"><b>Total</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%>
								<tr style="background-color:#EEDD82">
										<td><b>Registered</b></td>';
			$TotalNoOfCandidates = 0;
			for($lvars=0;$lvars<count($larrraces);$lvars++){
				
				$TotalNoOfCandidates = $TotalNoOfCandidates+ $larrracescount[$lvars]['NoOfCandidates'];
				if($larrraces[$lvars]['idDefinition']==166){
					if($larrracescount[$lvars]['NoOfCandidates'])
					$tabledata.='<td align ="center" colspan = "6"><b>'.$larrracescount[$lvars]['NoOfCandidates'].'</b></td>';
					else 
					$tabledata.='<td align ="center" colspan = "6"><b>0</b></td>';
				}else{
					if($larrracescount[$lvars]['NoOfCandidates'])
					$tabledata.='<td align ="center" colspan = "5"><b>'.$larrracescount[$lvars]['NoOfCandidates'].'</b></td>';
					else 
					$tabledata.='<td align ="center" colspan = "5"><b>0</b></td>';
				}
			}
			$tabledata.='<td align ="center" ><b>'.$TotalNoOfCandidates.'</b></td></tr>';
			



$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#E6E6FA"><td><b>Sat</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
			if($larrraces[$lvars]['idDefinition']==166){
			        
					$tabledata.='<td align ="center" colspan = "6"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "6" align ="center">'.$lintracesat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "6" align ="center">'.'['.$lintraceone[$lvars].'/'.$lintracemore[$lvars].']'.'</td></tr></table></b></td>';
				}else{
					$tabledata.='<td align ="center" colspan = "5"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "5" align ="center">'.$lintracesat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "5" align ="center">'.'['.$lintraceone[$lvars].'/'.$lintracemore[$lvars].']'.'</td></tr></table></b></td>';
				}
			}
			$Totalsat = array_sum($lintracesat);
			$tabledata.='<td align ="center" colspan = "1"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "1" align ="center">'.$Totalsat.'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "1" align ="center">'.'['.array_sum($lintraceone).'/'.array_sum($lintracemore).']'.'</td></tr></table></b></td></tr>';
			




			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#ADFF2F"><td><b>Pass</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
			if($larrraces[$lvars]['idDefinition']==166){
					$tabledata.='<td align ="center" colspan = "6"><b>'.$lintracepass[$lvars].'</b></td>';
				}else{
					$tabledata.='<td align ="center" colspan = "5"><b>'.$lintracepass[$lvars].'</b></td>';
				}
			}
			$Totalpass = array_sum($lintracepass);
			$tabledata.='<td align ="center" colspan = "1"><b>'.$Totalpass.'</b></td></tr>';
				
			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#FF6347"><td><b>Fail</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
			if($larrraces[$lvars]['idDefinition']==166){
					$tabledata.='<td align ="center" colspan = "6"><b>'.$lintracefail[$lvars].'</b></td>';
				}else{
					$tabledata.='<td align ="center" colspan = "5"><b>'.$lintracefail[$lvars].'</b></td>';
				}
			}
			$Totalfail = array_sum($lintracefail);
			$tabledata.='<td align ="center" colspan = "1"><b>'.$Totalfail.'</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#BDB76B"><td><b>Absent</b></td>';
			for($lvars=0;$lvars<count($larrraces);$lvars++){
			if($larrraces[$lvars]['idDefinition']==166){
					$tabledata.='<td align ="center" colspan = "6"><b>'.$lintraceabsent[$lvars].'</b></td>';
				}else{
					$tabledata.='<td align ="center" colspan = "5"><b>'.$lintraceabsent[$lvars].'</b></td>';
				}
			}
			$Totalabsent = array_sum($lintraceabsent);
			$tabledata.='<td align ="center" colspan = "1"><b>'.$Totalabsent.'</b></td></tr></table><br>';
	    }
	   
	    ////////////
	    
	if($lintsessiontype ==1)
		{
	        $ReportName = $this->view->translate( "Session Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
			$larrsessions =  $this->lobjallstatistics->fngetsessions($lintmon,$lintyear);
	        $larrages = $this->lobjallstatistics->fngetages1($lintmon,$lintyear);
	        // print_r($larrraces);die();
		    for($linti=0;$linti<count($larrsessions);$linti++)
				{
					$lintsessionpass[$linti]=0;
$lintsessionfail[$linti]=0;
$lintsessionabsent[$linti]=0;
$lintsessionsat[$linti]=0;
$lintsessionone[$linti]=0;
                    $lintsessionmore[$linti]=0;
					$larrresult[$linti] = $this->lobjallstatistics->fngetsessionresult($lintmon,$lintyear,$larrsessions[$linti]['idmangesession']);
					for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++)
					{
						if($larrresult[$linti][$lintj]['pass']==1){
                                                            //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintsessionone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintsessionmore[$linti]++;
							}
							//attemptsend
							$lintsessionpass[$linti]++;
						}elseif($larrresult[$linti][$lintj]['pass']==2){
                                                      //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintsessionone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintsessionmore[$linti]++;
							}
							//attemptsend

							$lintsessionfail[$linti]++;
						}elseif($larrresult[$linti][$lintj]['pass']==4){
							$lintsessionabsent[$linti]++;
						}
			        }
			        $lintsessionsat[$linti]= $lintsessionpass[$linti]+$lintsessionfail[$linti];
				}
	        
	        $tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td colspan = '23' align=center><b>{$ReportName}</b></td></tr></table><br>";
			$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td align ='center' colspan = '23'><b>".$lstrmonthyear."</b></td></tr>";
			$tabledata.= '<table border=1 align=center width=100% colapan = "1"><tr style="background-color:Gray"><td><b>Session</b></td>';
			for($lvars=0;$lvars<count($larrsessions);$lvars++){
				if(count($larrsessions)==1){
					$tabledata.='<td align ="center" colspan = "20"><b>'.$larrsessions[$lvars]['managesessionname'].'</b></td>';
				}
				if(count($larrsessions)==2){
					$tabledata.='<td align ="center" colspan = "10"><b>'.$larrsessions[$lvars]['managesessionname'].'</b></td>';
				}
				if(count($larrsessions)==3){
					$tabledata.='<td align ="center" colspan = "7"><b>'.$larrsessions[$lvars]['managesessionname'].'</b></td>';
				}
				if(count($larrsessions)==4){
					$tabledata.='<td align ="center" colspan = "6"><b>'.$larrsessions[$lvars]['managesessionname'].'</b></td>';
				}
				if(count($larrsessions)==5){
					$tabledata.='<td align ="center" colspan = "5"><b>'.$larrsessions[$lvars]['managesessionname'].'</b></td>';
				}
				if(count($larrsessions)==6){
					$tabledata.='<td align ="center" colspan = "4"><b>'.$larrsessions[$lvars]['managesessionname'].'</b></td>';
				}
			    if(count($larrsessions)==7){
					$tabledata.='<td align ="center" colspan = "3"><b>'.$larrsessions[$lvars]['managesessionname'].'</b></td>';
				}
			}
			$tabledata.='<td align ="center" colspan = "2"><b>Total</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%>
								<tr style="background-color:#EEDD82">
										<td><b>Registered</b></td>';
			$TotalNoOfCandidates = 0;
		    for($lvars=0;$lvars<count($larrsessions);$lvars++){
				if(count($larrsessions)==1){
					$tabledata.='<td align ="center" colspan = "20"><b>'.$larrsessions[$lvars]['NoOfCandidates'].'</b></td>';
					$TotalNoOfCandidates = $TotalNoOfCandidates + $larrsessions[$lvars]['NoOfCandidates'];
				}
				if(count($larrsessions)==2){
					$tabledata.='<td align ="center" colspan = "10"><b>'.$larrsessions[$lvars]['NoOfCandidates'].'</b></td>';
					$TotalNoOfCandidates = $TotalNoOfCandidates + $larrsessions[$lvars]['NoOfCandidates'];
				}
				if(count($larrsessions)==3){
					$tabledata.='<td align ="center" colspan = "7"><b>'.$larrsessions[$lvars]['NoOfCandidates'].'</b></td>';
					$TotalNoOfCandidates = $TotalNoOfCandidates + $larrsessions[$lvars]['NoOfCandidates'];
				}
				if(count($larrsessions)==4){
					$tabledata.='<td align ="center" colspan = "6"><b>'.$larrsessions[$lvars]['NoOfCandidates'].'</b></td>';
					$TotalNoOfCandidates = $TotalNoOfCandidates + $larrsessions[$lvars]['NoOfCandidates'];
				}
				if(count($larrsessions)==5){
					$tabledata.='<td align ="center" colspan = "5"><b>'.$larrsessions[$lvars]['NoOfCandidates'].'</b></td>';
					$TotalNoOfCandidates = $TotalNoOfCandidates + $larrsessions[$lvars]['NoOfCandidates'];
				}
				if(count($larrsessions)==6){
					$tabledata.='<td align ="center" colspan = "4"><b>'.$larrsessions[$lvars]['NoOfCandidates'].'</b></td>';
					$TotalNoOfCandidates = $TotalNoOfCandidates + $larrsessions[$lvars]['NoOfCandidates'];
				}
			    if(count($larrsessions)==7){
					$tabledata.='<td align ="center" colspan = "3"><b>'.$larrsessions[$lvars]['NoOfCandidates'].'</b></td>';
					$TotalNoOfCandidates = $TotalNoOfCandidates + $larrsessions[$lvars]['NoOfCandidates'];
				}
			}
			$tabledata.='<td align ="center" colspan = "2"><b>'.$TotalNoOfCandidates.'</b></td></tr>';
			



$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#E6E6FA"><td><b>Sat</b></td>';
			for($lvars=0;$lvars<count($larrsessions);$lvars++){
				if(count($larrsessions)==1){
				     
					$tabledata.='<td align ="center" colspan = "20"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "20" align ="center">'.$lintsessionsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "20" align ="center">'.'['.$lintsessionone[$lvars].'/'.$lintsessionmore[$lvars].']'.'</td></tr></table></b></td>';
				}
				if(count($larrsessions)==2){
					$tabledata.='<td align ="center" colspan = "10"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "10" align ="center">'.$lintsessionsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "10" align ="center">'.'['.$lintsessionone[$lvars].'/'.$lintsessionmore[$lvars].']'.'</td></tr></table></b></td>';
				}
				if(count($larrsessions)==3){
					$tabledata.='<td align ="center" colspan = "7"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "7" align ="center">'.$lintsessionsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "7" align ="center">'.'['.$lintsessionone[$lvars].'/'.$lintsessionmore[$lvars].']'.'</td></tr></table></b></td>';
				}
				if(count($larrsessions)==4){
					$tabledata.='<td align ="center" colspan = "6"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "6" align ="center">'.$lintsessionsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "6" align ="center">'.'['.$lintsessionone[$lvars].'/'.$lintsessionmore[$lvars].']'.'</td></tr></table></b></td>';
				}
				if(count($larrsessions)==5){
					$tabledata.='<td align ="center" colspan = "5"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "5" align ="center">'.$lintsessionsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "5" align ="center">'.'['.$lintsessionone[$lvars].'/'.$lintsessionmore[$lvars].']'.'</td></tr></table></b></td>';
				}
				if(count($larrsessions)==6){
					$tabledata.='<td align ="center" colspan = "4"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "4" align ="center">'.$lintsessionsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "4" align ="center">'.'['.$lintsessionone[$lvars].'/'.$lintsessionmore[$lvars].']'.'</td></tr></table></b></td>';
				}
			    if(count($larrsessions)==7){
					$tabledata.='<td align ="center" colspan = "3"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "3" align ="center">'.$lintsessionsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "3" align ="center">'.'['.$lintsessionone[$lvars].'/'.$lintsessionmore[$lvars].']'.'</td></tr></table></b></td>';
				}
			}
			$Totalsat = array_sum($lintsessionsat);
			
			$tabledata.='<td align ="center" colspan = "2"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "2" align ="center">'.$Totalsat.'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "2" align ="center">'.'['.array_sum($lintsessionone).'/'.array_sum($lintsessionmore).']'.'</td></tr></table></b></td></tr>';
			
			





			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#ADFF2F"><td><b>Pass</b></td>';
		    for($lvars=0;$lvars<count($larrsessions);$lvars++){
				if(count($larrsessions)==1){
					$tabledata.='<td align ="center" colspan = "20"><b>'.$lintsessionpass[$lvars].'</b></td>';
				}
				if(count($larrsessions)==2){
					$tabledata.='<td align ="center" colspan = "10"><b>'.$lintsessionpass[$lvars].'</b></td>';
				}
				if(count($larrsessions)==3){
					$tabledata.='<td align ="center" colspan = "7"><b>'.$lintsessionpass[$lvars].'</b></td>';
				}
				if(count($larrsessions)==4){
					$tabledata.='<td align ="center" colspan = "6"><b>'.$lintsessionpass[$lvars].'</b></td>';
				}
				if(count($larrsessions)==5){
					$tabledata.='<td align ="center" colspan = "5"><b>'.$lintsessionpass[$lvars].'</b></td>';
				}
				if(count($larrsessions)==6){
					$tabledata.='<td align ="center" colspan = "4"><b>'.$lintsessionpass[$lvars].'</b></td>';
				}
			    if(count($larrsessions)==7){
					$tabledata.='<td align ="center" colspan = "3"><b>'.$lintsessionpass[$lvars].'</b></td>';
				}
			}
			$Totalpass = array_sum($lintsessionpass);
			$tabledata.='<td align ="center" colspan = "2"><b>'.$Totalpass.'</b></td></tr>';
				
			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#FF6347"><td><b>Fail</b></td>';
			for($lvars=0;$lvars<count($larrsessions);$lvars++){
				if(count($larrsessions)==1){
					$tabledata.='<td align ="center" colspan = "20"><b>'.$lintsessionfail[$lvars].'</b></td>';
				}
				if(count($larrsessions)==2){
					$tabledata.='<td align ="center" colspan = "10"><b>'.$lintsessionfail[$lvars].'</b></td>';
				}
				if(count($larrsessions)==3){
					$tabledata.='<td align ="center" colspan = "7"><b>'.$lintsessionfail[$lvars].'</b></td>';
				}
				if(count($larrsessions)==4){
					$tabledata.='<td align ="center" colspan = "6"><b>'.$lintsessionfail[$lvars].'</b></td>';
				}
				if(count($larrsessions)==5){
					$tabledata.='<td align ="center" colspan = "5"><b>'.$lintsessionfail[$lvars].'</b></td>';
				}
				if(count($larrsessions)==6){
					$tabledata.='<td align ="center" colspan = "4"><b>'.$lintsessionfail[$lvars].'</b></td>';
				}
			    if(count($larrsessions)==7){
					$tabledata.='<td align ="center" colspan = "3"><b>'.$lintsessionfail[$lvars].'</b></td>';
				}
			}
			$Totalfail = array_sum($lintsessionfail);
			$tabledata.='<td align ="center" colspan = "2"><b>'.$Totalfail.'</b></td></tr>';
			$tabledata.= '<table border=1 align=center width=100%><tr style="background-color:#BDB76B"><td><b>Absent</b></td>';
			for($lvars=0;$lvars<count($larrsessions);$lvars++){
				if(count($larrsessions)==1){
					$tabledata.='<td align ="center" colspan = "20"><b>'.$lintsessionabsent[$lvars].'</b></td>';
				}
				if(count($larrsessions)==2){
					$tabledata.='<td align ="center" colspan = "10"><b>'.$lintsessionabsent[$lvars].'</b></td>';
				}
				if(count($larrsessions)==3){
					$tabledata.='<td align ="center" colspan = "7"><b>'.$lintsessionabsent[$lvars].'</b></td>';
				}
				if(count($larrsessions)==4){
					$tabledata.='<td align ="center" colspan = "6"><b>'.$lintsessionabsent[$lvars].'</b></td>';
				}
				if(count($larrsessions)==5){
					$tabledata.='<td align ="center" colspan = "5"><b>'.$lintsessionabsent[$lvars].'</b></td>';
				}
				if(count($larrsessions)==6){
					$tabledata.='<td align ="center" colspan = "4"><b>'.$lintsessionabsent[$lvars].'</b></td>';
				}
			    if(count($larrsessions)==7){
					$tabledata.='<td align ="center" colspan = "3"><b>'.$lintsessionabsent[$lvars].'</b></td>';
				}
			}
			$Totalabsent = array_sum($lintsessionabsent);
			$tabledata.='<td align ="center" colspan = "2"><b>'.$Totalabsent.'</b></td></tr></table><br>';
	    }
////////////
	    
	   if($lintqualificationtype==1)
		{
		$ReportName = $this->view->translate( "Qualification Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		
		$larrqualificationcount = $this->lobjallstatistics->fngetqualification($lintmon,$lintyear); //Call a function to get number of candidates registered for different qualification
		$larrqualification = $this->lobjallstatistics->fngetqualificationnames($lintmon,$lintyear); //Call a function to get different qualification names
		$this->view->larrqualificationcount=$larrqualificationcount;
		$lintregisteredtotal=0;
		for($linti=0;$linti<count($larrqualification);$linti++){
			$lintqualificationid= $larrqualification[$linti]['idDefinition'];
			$larrqualificationquali[$linti]  =	$larrqualification[$linti]['QualificationName'];
			$larrqualificationids[$linti] = $larrqualification[$linti];
			$larrQualificationresult = $this->lobjallstatistics->fngetqualificationresult($lintmon,$lintyear,$lintqualificationid); //Call a function to get number of Diploma candidates details
			$lintregisteredtotal += $larrqualificationcount[$linti]['NoOfCandidates'];
			$lintcountQualification = count($larrQualificationresult);

			$lintqualificationreg[$linti] = $larrqualificationcount[$linti]['NoOfCandidates'];
			//$lintqualificationsat[$linti] = $lintcountQualification;
			$lintqualificationpass[$linti] = 0;
			$lintqualificationfail[$linti] = 0;
			$lintqualificationabsent[$linti]=0;
 $lintqualificationone[$linti]=0;
			$lintqualificationmore[$linti]=0;

			for($lintj=0;$lintj<$lintcountQualification;$lintj++){
				if($larrQualificationresult[$lintj]['pass']==1)
				{
                                           //attempts
							if($larrQualificationresult[$lintj]['Attempts']==1)
							{
							   $lintqualificationone[$linti]++;
							}
						    if($larrQualificationresult[$lintj]['Attempts']>1)
							{
							  $lintqualificationmore[$linti]++;
							}
							//attemptsend
					$lintqualificationpass[$linti]++;
				}elseif ($larrQualificationresult[$lintj]['pass']==2){
                                            //attempts
							if($larrQualificationresult[$lintj]['Attempts']==1)
							{
							   $lintqualificationone[$linti]++;
							}
						    if($larrQualificationresult[$lintj]['Attempts']>1)
							{
							  $lintqualificationmore[$linti]++;
							}
							//attemptsend
					$lintqualificationfail[$linti]++;
				}elseif($larrQualificationresult[$lintj]['pass']==4){
					$lintqualificationabsent[$linti]++;
				}
			}
$lintqualificationsat[$linti] = $lintqualificationpass[$linti]+$lintqualificationfail[$linti];
			//$lintqualificationabsent[$linti] = $larrqualificationcount[$linti]['NoOfCandidates']-$lintqualificationsat[$linti];
		}
		$QualificationReportName = $this->view->translate( "Qualification" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
		$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td colspan = '23' align=center><b>{$ReportName}</b></td></tr></table><br>";
		$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td align ='center' colspan = '23'><b>".$lstrmonthyear."</b></td></tr>";
		$tabledata.="<tr style='background-color:Gray'> <td align ='left' colspan = '1'><b>Qualification</b></td>";
		for($linti=0;$linti<count($larrqualificationquali);$linti++){
			if($larrqualificationids[$linti]['idDefinition']==167 || $larrqualificationids[$linti]['idDefinition']==168 || $larrqualificationids[$linti]['idDefinition']==170){
			$tabledata.="<td align ='center' colspan = '4'><b>$larrqualificationquali[$linti]</b></td>";
		}else{
			$tabledata.="<td align ='center' colspan = '3'><b>$larrqualificationquali[$linti]</b></td>";
		}
		}
		$tabledata.="<td align ='center' colspan = '1'><b>TOTAL</b></td></tr>";
	
		$tabledata.="<tr style='background-color:#EEDD82'> <td align ='left' colspan = '1'><b>Registered</b></td>";
		$linttotalreg =0;
		for($linti=0;$linti<count($lintqualificationreg);$linti++){
			$linttotalreg+=$lintqualificationreg[$linti];
		if($larrqualificationids[$linti]['idDefinition']==167 || $larrqualificationids[$linti]['idDefinition']==168 || $larrqualificationids[$linti]['idDefinition']==170){
				if($lintqualificationreg[$linti])
				{
				$tabledata.="<td align ='center' colspan = '4'><b>$lintqualificationreg[$linti]</b></td>";
				}else{
				$tabledata.="<td align ='center' colspan = '4'><b>0</b></td>";
				}
		}else{
				if($lintqualificationreg[$linti])
				{
				$tabledata.="<td align ='center' colspan = '3'><b>$lintqualificationreg[$linti]</b></td>";
				}else{
				$tabledata.="<td align ='center' colspan = '3'><b>0</b></td>";
				}
			}
		}
		if($linttotalreg)
		$tabledata.="<td align ='center' colspan = '1'><b>$linttotalreg</b></td></tr>";
		else
		$tabledata.="<td align ='center' colspan = '1'><b>0</b></td></tr>";
		
		



$tabledata.="<tr style='background-color:#E6E6FA'> <td align ='left' colspan = '1'><b>Sat</b></td>";
		$linttotalsat =0;
		for($linti=0;$linti<count($lintqualificationsat);$linti++){
			$linttotalsat+=$lintqualificationsat[$linti];
			if($larrqualificationids[$linti]['idDefinition']==167 || $larrqualificationids[$linti]['idDefinition']==168 || $larrqualificationids[$linti]['idDefinition']==170)
				{
				
				    
					 $tabledata.='<td align ="center" colspan = "4"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "4" align ="center">'.$lintqualificationsat[$linti].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "4" align ="center">'.'['.$lintqualificationone[$linti].'/'.$lintqualificationmore[$linti].']'."</td></tr></table></b></td>";
				}else{
					$tabledata.='<td align ="center" colspan = "3"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "3" align ="center">'.$lintqualificationsat[$linti].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "3" align ="center">'.'['.$lintqualificationone[$linti].'/'.$lintqualificationmore[$linti].']'."</td></tr></table></b></td>";
				}
		}
		if($linttotalsat)
		$tabledata.='<td align ="center" colspan = "1"><b><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "1" align ="center">'.$linttotalsat.'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "1" align ="center">'.'['.array_sum($lintqualificationone).'/'.array_sum($lintqualificationmore).']'."</td></tr></table></b></td></tr>";
		else
		$tabledata.="<td align ='center' colspan = '1'><b>0</b></td></tr>";
	






	
		$tabledata.="<tr style='background-color:#ADFF2F'> <td align ='left' colspan = '1'><b>Pass</b></td>";
		$linttotalpass =0;
		for($linti=0;$linti<count($lintqualificationpass);$linti++){
			$linttotalpass+=$lintqualificationpass[$linti];
		if($larrqualificationids[$linti]['idDefinition']==167 || $larrqualificationids[$linti]['idDefinition']==168 || $larrqualificationids[$linti]['idDefinition']==170)
				{
					$tabledata.="<td align ='center' colspan = '4'><b>$lintqualificationpass[$linti]</b></td>";
				}else{
					$tabledata.="<td align ='center' colspan = '3'><b>$lintqualificationpass[$linti]</b></td>";
				}
		}
		if($linttotalpass)
		$tabledata.="<td align ='center' colspan = '1'><b>$linttotalpass</b></td></tr>";
		else
		$tabledata.="<td align ='center' colspan = '1'><b>0</b></td></tr>";
	
		$tabledata.="<tr style='background-color:#FF6347'> <td align ='left' colspan = '1'><b>Fail</b></td>";
		$linttotalfail =0;
		for($linti=0;$linti<count($lintqualificationfail);$linti++){
			$linttotalfail+=$lintqualificationfail[$linti];
		if($larrqualificationids[$linti]['idDefinition']==167 || $larrqualificationids[$linti]['idDefinition']==168 || $larrqualificationids[$linti]['idDefinition']==170)
				{
					$tabledata.="<td align ='center' colspan = '4'><b>$lintqualificationfail[$linti]</b></td>";
				}else{
					$tabledata.="<td align ='center' colspan = '3'><b>$lintqualificationfail[$linti]</b></td>";
				}
		}
		if($linttotalreg)
		$tabledata.="<td align ='center' colspan = '1'><b>$linttotalfail</b></td></tr>";
		else
		$tabledata.="<td align ='center' colspan = '1'><b>0</b></td></tr>";
	
		$tabledata.="<tr style='background-color:#BDB76B'> <td align ='left' colspan = 1><b>Absent</b></td>";
		$linttotalabsent =0;
		for($linti=0;$linti<count($lintqualificationabsent);$linti++){
			$linttotalabsent+=$lintqualificationabsent[$linti];
		if($larrqualificationids[$linti]['idDefinition']==167 || $larrqualificationids[$linti]['idDefinition']==168 || $larrqualificationids[$linti]['idDefinition']==170)
				{
					$tabledata.="<td align ='center' colspan = '4'><b>$lintqualificationabsent[$linti]</b></td>";
				}else{
					$tabledata.="<td align ='center' colspan = '3'><b>$lintqualificationabsent[$linti]</b></td>";
				}
		}
		if($linttotalreg)
		$tabledata.="<td align ='center' colspan = '1'><b>$linttotalabsent</b></td></tr></table><br>";
		else
		$tabledata.="<td align ='center' colspan = '1'><b>0</b></td></tr></table><br>";
	}
	
	if($lintresulttype==1)
			{
				$ReportName = $this->view->translate( "Result Wise" ).' '.$this->view->translate( "Statistics" ).' '.$this->view->translate( "Report" );
				$larrprograms = $this->lobjallstatistics->fngetprogramnames();
				for($linti=0;$linti<count($larrprograms);$linti++)
				{
					$larrresult[$linti] = $this->lobjallstatistics->fngetresultgrade($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
					$larrregdetails[$linti] = $this->lobjallstatistics->fngetabsentdetails($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
					$lintgradeA[$linti] = 0;
$lintgradeB[$linti] = 0;
$lintgradeC[$linti] = 0;
$lintgradeF[$linti] = 0;
$lintgradeAone[$linti] = 0;
			$lintgradeAmore[$linti] = 0;
			$lintgradeBone[$linti] = 0;
			$lintgradeBmore[$linti] = 0;
			$lintgradeCone[$linti] = 0;
			$lintgradeCmore[$linti] = 0;
			$lintgradeAbsentone[$linti] = 0;
			$lintgradeAbsentmore[$linti] = 0;
			$lintgradeFailone[$linti] = 0;
			$lintgradeFailmore[$linti] = 0;

					for($lintj=0;$lintj<count($larrresult[$linti]);$lintj++){
						if($larrresult[$linti][$lintj]['Grade']=='A'){
                                                          //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeAone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeAmore[$linti]++;
							}
							//attemptsend
							$lintgradeA[$linti]++;
						}elseif($larrresult[$linti][$lintj]['Grade']=='B'){
                                                         //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeBone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeBmore[$linti]++;
							}
							//attemptsend
							$lintgradeB[$linti]++;
						}elseif($larrresult[$linti][$lintj]['Grade']=='C'){
                                                         //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeCone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeCmore[$linti]++;
							}
							//attemptsend
							$lintgradeC[$linti]++;
						}else{
                                                                      //attempts
							if($larrresult[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeFailone[$linti]++;
							}
						    if($larrresult[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeFailmore[$linti]++;
							}
							//attemptsend
							$lintgradeF[$linti]++;
						}
					}
					$larrsbsentdetails[$linti] = $this->lobjallstatistics->fngetabsentprogramwise($lintmon,$lintyear,$larrprograms[$linti]['IdProgrammaster']);
			 
                 for($lintj=0;$lintj<count($larrsbsentdetails[$linti]);$lintj++){
				if($larrsbsentdetails[$linti][$lintj]['pass']=='4'){
                                                      //attempts
							if($larrsbsentdetails[$linti][$lintj]['Attempts']==1)
							{
							  $lintgradeAbsentone[$linti]++;
							}
						    if($larrsbsentdetails[$linti][$lintj]['Attempts']>1)
							{
							  $lintgradeAbsentmore[$linti]++;
							}
							//attemptsend
					$lintabsent[$linti]++;
				}
			}

					
				}
				$tabledata.="<br><br><table border=1 align=center width=100%><tr style='background-color:Gray'><td colspan = '23' align=center><b>{$ReportName}</b></td></tr></table><br>";
				$tabledata.="<br><table border=1 align=center width=100%><tr style='background-color:Gray'><td align ='center' colspan = '23'><b>".$lstrmonthyear."</b></td></tr>";
				$tabledata.="<tr> <td align ='left' colspan = 1><b>Examcategory/Result</b></td><td align ='center' colspan = 11><b>Pass/Grade</b></td><td align ='center' colspan = 5><b>Fail</b></td><td align ='center' colspan = 5><b>Absent</b></td><td align ='center' colspan = 1><b>Total</b></td></tr>";
				$tabledata.="<tr> <td align ='left' colspan = 1><b></b></td><td align ='center' colspan = 4><b>A</b></td><td align ='center' colspan = 4><b>B</b></td><td align ='center' colspan = 3><b>C</b></td><td align ='center' colspan = 5><b></b></td><td align ='center' colspan = 5<b></b></td><td align ='center' colspan = 1></td></tr>";
				for($linti=0;$linti<count($larrprograms);$linti++){
					$tabledata.='<tr><td align ="left" colspan = 1><b>'.$larrprograms[$linti]['ProgramName'].'</b></td>';
					if($lintgradeA[$linti]){


					//$tabledata.='<td align ="center" colspan = "5"><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "5" align ="center">'.$lintsessionsat[$lvars].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "5" align ="center">'.'['.$lintsessionone[$lvars].'/'.$lintsessionmore[$lvars].']'.'</td></tr></table></b></td>';
					$tabledata.='<td align="center" colspan = 4><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "4" align ="center">'.$lintgradeA[$linti].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "4" align ="center">'.'['.$lintgradeAone[$linti].'/'.$lintgradeAmore[$linti].']'.'</td></tr></table></b></td>';
					}else{
						$tabledata.='<td align="center" colspan = 4>0</td>';
					}
					if($lintgradeB[$linti]){
					$tabledata.='<td align="center" colspan = 4><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "4" align ="center">'.$lintgradeB[$linti].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "4" align ="center">'.'['.$lintgradeBone[$linti].'/'.$lintgradeBmore[$linti].']'.'</td></tr></table></b></td>';
					}else{
						$tabledata.='<td align="center" colspan = 4>0</td>';
					}
				    if($lintgradeC[$linti]){
					$tabledata.='<td align="center" colspan = 3><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "3" align ="center">'.$lintgradeC[$linti].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "3" align ="center">'.'['.$lintgradeCone[$linti].'/'.$lintgradeCmore[$linti].']'.'</td></tr></table></b></td>';
					}else{
						$tabledata.='<td align="center" colspan = 3>0</td>';
					}
				 	if($lintgradeF[$linti]){
					$tabledata.='<td align ="center" colspan = 5><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "5" align ="center">'.$lintgradeF[$linti].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "5" align ="center">'.'['.$lintgradeFailone[$linti].'/'.$lintgradeFailmore[$linti].']'.'</td></tr></table></b></td>';
					}else{
						$tabledata.='<td align="center" colspan = 5>0</td>';
					}
					if($lintabsent[$linti]){
					$tabledata.='<td align ="center" colspan = 5><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "5" align ="center">'.$lintabsent[$linti].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "5" align ="center">'.'['.$lintgradeAbsentone[$linti].'/'.$lintgradeAbsentmore[$linti].']'.'</td></tr></table></b></td>';
					}else{
						$tabledata.='<td align="center" colspan = 5>0</td>';
					}
					if($larrregdetails[$linti]['NoOfCandidates']){
					
					$Onetotal = $lintgradeAone[$linti]+$lintgradeBone[$linti]+$lintgradeCone[$linti]+$lintgradeFailone[$linti]+$lintgradeAbsentone[$linti];
		            $Moretotal = $lintgradeAmore[$linti]+$lintgradeBmore[$linti]+$lintgradeCmore[$linti]+$lintgradeFailmore[$linti]+$lintgradeAbsentmore[$linti];
		
					  $tabledata.='<td align ="left" colspan = 1><b><table  align=center width=100% ><tr style="background-color:#E6E6FA"><td colspan = "1" align ="center">'.$larrregdetails[$linti]['NoOfCandidates'].'</td></tr><tr style="background-color:#E6E6FA" ><td colspan = "1" align ="center">'.'['.$Onetotal.'/'.$Moretotal.']'.'</td></tr></table></b></td></tr>';	
					}else{
						$tabledata.='<td align ="left" colspan = 1>0</td></tr>';
					}
				}
				$tabledata.="</table><br>";
			}
			
	
	
	$ourFileName = realpath('.')."/data";
	$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
	ini_set('max_execution_time', 3600);
	fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
	fclose($ourFileHandle);
	header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
	header("Content-Disposition: attachment; filename=$filename.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	readfile($ourFileName);
	unlink($ourFileName);
}

	 public function fngetmonthnamesAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintyr = $this->_getParam('val');
$lintcuryear = date("Y"); 
		if($lintyr == $lintcuryear){
			    $larrresult = $this->lobjallstatistics->fngetmonthsajax(1);
			}elseif($lintyr == '2012'){
				$larrresult = $this->lobjallstatistics->fngetmonthsajax(3);
			}
	        else{
				$larrresult = $this->lobjallstatistics->fngetmonthsajax(2);
			}
		echo Zend_Json_Encoder::encode($larrresult);
	}

}
