<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_ManualmarksreportController extends Base_Base
{
	public function init()
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
		$this->registry = Zend_Registry::getInstance(); //get registry instance
		$this->locale = $this->registry->get('Zend_Locale'); //get locale
		$this->lobjmanualmarksreport = new Reports_Model_DbTable_Manualmarksreportmodel(); //model object
		$this->lobjmanualmarksreportform = new  Reports_Form_Manualmarksreport(); //form object
	}
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjmanualmarksreportform;
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				unset ( $larrformData ['Search']);
				$this->view->search = 1;
				$this->view->FromDate = $FromDate = $larrformData['FromDate'];
				$this->view->ToDate = $ToDate = $larrformData['ToDate'];
				$larrresultcomp = $this->lobjmanualmarksreport->fngetdetails($FromDate,$ToDate);
				$this->view->venues = $larrresultcomp;
				$this->view->lobjform->populate($larrformData);
			}
		}
	}
    public function fnexportexcelAction()
    {	
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$ldtfromdate = $larrformData['fromdate'];
		$ldttodate =$larrformData['todate'];
		$fdate = date('d-m-Y',strtotime($ldtfromdate));
		$tdate = date('d-m-Y',strtotime($ldttodate));
        $larrresultcomp = $this->lobjmanualmarksreport->fngetdetails($ldtfromdate,$ldttodate);
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'ManualmarksentryReport'.$ldtfromdate.'_'.$ldttodate;
		$ReportName = $this->view->translate( "Manual Marks Entry" ).' '.$this->view->translate( "Report" );
		 $tabledata = '<img width=100% src="../public/images/reportheader.jpg" /><br>';
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 2><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 2><b> Time</b></td><td align=left colspan = 2><b>$time</b></td></tr>";
        $tabledata.= "<tr><td align=left colspan = 2><b>From Date </b></td><td align=left colspan = 2><b>".$fdate."</b></td><td  align=left colspan = 2><b>To Date</b></td><td align=left colspan = 2><b>".$tdate."</b></td></td></tr></table><br>";
	    $tabledata.= '<table border=1 align=center width=100%>
	                       <tr>
	                          <th><b>Candidate Name</b></th>
	                          <th><b>ICNO</b></th>
	                          <th><b>Program Name</b></th>
	                          <th><b>Exam Date</b></th>
	                          <th><b>Exam Venue</b></th>
	                          <th><b>Exam Session</b></th>
	                          <th><b>Part A</b></th>
	                          <th><b>Part B</b></th>
	                          <th><b>Part C</b></th>
	                          <th><b>Obtained</b></th>
	                          <th><b>Entered By</b></th>
	                          <th><b>Status</b></th>
	                          <th><b>Grade</b></th>
	                       </tr>
	                 ';
	    foreach($larrresultcomp as $lobjCountry)
		{
			$tabledata.="<tr>
			               <td align = 'left'>".$lobjCountry['FName']."</td>
			               <td align = 'left'>".$lobjCountry['ICNO']."</td>
			               <td align = 'left' >".$lobjCountry['ProgramName']."</td>
			               <td align = 'left'>".$lobjCountry['Examdate']."</td>
			               <td align = 'left'>".$lobjCountry['centername']."</td>
			               <td align = 'left'>".$lobjCountry['session']."</td>
			               <td align = 'left'>".$lobjCountry['PartAmarks']."</td>
			               <td align = 'left'>".$lobjCountry['PartBmarks']."</td>
			               <td align = 'left'>".$lobjCountry['PartCmarks']."</td>
			               <td align = 'left'>".$lobjCountry['obtained']."</td>
			               <td align = 'left'>".$lobjCountry['EnteredBy']."</td>
			               <td align = 'left'>".$lobjCountry['Result']."</td>
			               <td align = 'left'>".$lobjCountry['Grade']."</td>
			              </tr>";
		}
		$tabledata.="</table><br>";
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
    }
}