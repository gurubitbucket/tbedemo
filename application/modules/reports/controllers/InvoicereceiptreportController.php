<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Reports_InvoicereceiptreportController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator		
		$this->lobjReceiptForm = new  Reports_Form_Invoicereceiptreport();//receipt form
		$this->lobjReceiptreportModel = new Reports_Model_DbTable_Invoicereceiptreport();//receipt model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjReceiptForm;	
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Company";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Takaful";
		$this->view->lobjform->field19->addMultiOptions($larrtypeofcompany);
		$this->view->lobjform->field19->setAttrib('onchange', 'emptythenamelist');
		$this->lobjReceiptForm->field3->setAttrib('onkeyup', 'fnGetOperatorNames');
	    $this->lobjReceiptForm->Date3->setAttrib('onChange', "dijit.byId('Date4').constraints.min = arguments[0];");
	    //$this->lobjReportsForm->Date4->setAttrib('onChange', "dijit.byId('Date3').constraints.min = arguments[0];");
	    $larrresult=array();
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))	{
			$larrformData = $this->_request->getPost ();		
			if ($this->lobjform->isValid($larrformData)){
				if($larrformData['field3'] && !$larrformData['field19']){
					echo '<script language="javascript">alert("Please Select Type Of Operator")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/reports/invoicereceiptreport/index/';</script>";					
					exit;
				}
				
				$lvarfromdate = $this->view->fromdate=$larrformData['Date3'];
				$lvartodate = $this->view->todate=$larrformData['Date4'];				
				$operatortype = $this->view->operatortype=$larrformData['field19'];
				$operatorname = $this->view->operatorname=$larrformData['field3'];
				unset ( $larrformData ['Search'] );
				$larrresult = $this->lobjReceiptreportModel->fnSearchCompanies($larrformData); //searching the values for the Companies
				//echo "<pre/>";print_r($larrresult);
				$this->view->receiptresult=$larrresult;
				$this->view->lobjform->populate($larrformData);
			}
		}		
	}
	
	public function getoperatornamesAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$newresult="";
		$opType = $this->_getParam('opType');	
		$namestring = $this->_getParam('namestring');	
		$larrresultopnames = $this->lobjReceiptreportModel->fnGetOperatorNames($opType,$namestring); 
		foreach ($larrresultopnames as $larrresnewarray){
			$opname=$larrresnewarray['name'];
			$newresult=$newresult."<tr><td><span id='idspan'   onclick='fnsetvalue(\"".$opname."\");'>".$larrresnewarray['name']."</span></td></tr></BR>";			
		}
		echo $newresult;
		exit;
	}
	
 	public function getdetailsAction()	{    
		$this->_helper->layout->disableLayout();
	  	$this->_helper->viewRenderer->setNoRender();
	  	$Idreceipt = $this->_getParam('Idreceipt');
	  	$opr = $this->_getParam('opr');
	  	$tabledata = '';	
	  	$result = $this->lobjReceiptreportModel->fngetreceiptdetails($Idreceipt,$opr);
	  	//echo "<pre/>";print_r($result);
	  	foreach($result as $lobjCountry){
	  		if ($lobjCountry['Flag'] == 1){ 
					$head="Company"; 
					$name=$lobjCountry['oprname'];
					break;
			}else if($lobjCountry['Flag'] == 2){ 
					$head= "Takaful";
					$name=$lobjCountry['oprname'];
					break;
			}
			
	  	}
	  	$tabledata.= '<fieldset><legend align = "left"> Receipt Details - ';
	  	$tabledata.=$head; 
	  	$tabledata.='</legend>		
				<table class="table" border=1 align = "center" width=100%>
					<tr>
						<th><b>Operator Name</b></th>
						<th width="50px"><b>Receipt Number</b></th>
						<th><b>Total Amount</b></th>
						<th><b>Payment mode</b></th>';
						if($result['0']['ReceiptType']==1){
		  $tabledata.= '<th><b>Cheque Number</b></th>
						<th><b>Cheque Date</b></th>
						<th><b>Bank Name</b></th>';	
						}
		  
			$tabledata.= '<th><b>Receipt Date</b></th>
						<th><b>Geenerated By</b></th>
						<th><b>Generated Date</b></th>
					</tr>';
	  	$tabledata.= '<tr>
	  					<td align = "left">'.$name.'</td>
						<td align = "left">'.$result['0']['ReceiptNum'].'</td>
						<td align="right">'.$result['0']['Totalamount'].'</td>';
						if($result['0']['ReceiptType']==1){
							$mode="Cheque";
							$tabledata.= '<td align = "center">'.$mode.'</td>
							<td align="center">'.$result['0']['ChkNum'].'</td>
							<td align="center">'.$result['0']['ChkDate'].'</td>
							<td align="center">'.$result['0']['Bankbranch'].'</td>';
						}else if($result['0']['ReceiptType']==2){
							$mode="Cash";
							$tabledata.= '<td align = "center">'.$mode.'</td>';
						}
			$tabledata.= '
						<td align="center">'.$result['0']['ReceiptDate'].'</td>
						<td align="center">'.$result['0']['fName'].'</td>
						<td align="center">'.$result['0']['generateddate'].'</td>
					</tr></table>';
	  	
	  	
	  	
	  	
	  	
	  	$tabledata.= '<table class="table" border=1 align = "center">
	  				<tr>
						<th><b>Batch Id</b></th>
						<th><b>Batch Amount</b></th>
					</tr>';
	  	foreach($result as $lobjCountry){
	  		$tabledata.= '<tr>';
	  		$tabledata.= '<td align = "left">'.$lobjCountry['BatchId'].'</td>';
		  	$tabledata.= '<td align = "right">'.$lobjCountry['Amount'].'</td><tr>';
		}
	  	$tabledata.="</table></fieldset><br>";
	  	$tabledata.="<tr><td colspan= '6'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'>&nbsp;&nbsp;</td></tr></fieldset>";	
		echo  $tabledata; 
	}

	
	public function pdfexportAction()
	{   
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	    $fromdate = date('d-m-Y',strtotime($this->_getParam('fromdate')));
	    $todate = date('d-m-Y',strtotime($this->_getParam('todate')));
	    $operatortype=$this->_getParam('operatortype');
	    $operatorname=$this->_getParam('operatorname');
	    if($operatortype==1){
	    	$operatortype="Company";
	    }else if($operatortype==2){
	    	$operatortype="Takafull";
	    }
	    if($operatorname==""){
	    	$operatorname="-----";
	    }
	    $larrformData = array();
	    $larrformData['Date3']=$this->_getParam('fromdate');
	    $larrformData['Date4']=$this->_getParam('todate');
	    $larrformData['field19']=$this->_getParam('operatortype');
	    $larrformData['field3']=$this->_getParam('operatorname');
		
	    $result1 = $this->lobjReceiptreportModel->fnSearchCompanies($larrformData); //searching the values for the Companies
	    $namespace = new Zend_Session_Namespace('pdfaction');
        $data=$namespace->data; 
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Receipt_Report_'.$fromdate.'_'.$todate;
		$ReportName = $this->view->translate( "Receipt" ).' '.$this->view->translate( "Details" ).' '.$this->view->translate( "Report" );
		$lstrreportytpe=$this->_getParam('reporttype');
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br/><br/<br/><br/><br/><br/>';
		}
		$tabledata.= "<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 8><b> {$ReportName}</b></td>	
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date</b></td>
								<td align= 'left' colspan= 2><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 4><b>$time</b></td>
							</tr>";
			$tabledata.= "<tr>	
							<td><b>Receipt From Date </b></td>
							<td align= 'left' colspan= 2><b>"."&nbsp;".$fromdate."</b></td>
							<td><b>Receipt To Date</b></td>
							<td align = 'left' colspan= 4><b>"."&nbsp;".$todate."</b></td>
						</tr>";
			if($operatortype!=''){
			$tabledata.="<tr>	
							<td><b>Type of Operator</b></td>
							<td align= 'left' colspan= 2><b>$operatortype</b></td>
							<td><b>Operator Name</b></td>
							<td align='left' colspan= 4><b>$operatorname</b></td>
						</tr>";
					
			}
			$tabledata.="</table><br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th align="left"><b>Company/Takaful</b></th>
							<th><b>Receipt Date</b></th>
							<th><b>Operator Name</b></th>
							<th><b>Receipt Number</b></th>
							<th><b>Receipt Amount</b></th>
							<th><b>Generated By</b></th>
						</tr>';  
	if (count($result1)): 
		$cnt = 0;$cnt1 = 0;$camt = 0;$tamt = 0; $ctc = 0;$ttc = 0;$prev1="";
        ///////////company
		foreach ($result1 as $invoiceresult ): 
			if($invoiceresult['Flag']==1){
	       		if($cnt==0){$name = 'Company';}else {$name = '';}
	   				$tabledata.= '<tr>
			      		<td><b>'.$name.'</b></td>
			    		<td>'.$invoiceresult['ReceiptDate'].'</td>
				     	<td>'.$invoiceresult['CompanyName'].'</td>
				     	<td>'.$invoiceresult['ReceiptNum'].'</td>
				     	<td align="right">'.$invoiceresult['Totalamount'].'</td>
				     	<td align="right">'.$invoiceresult['fName'].'</td>
				      	</tr>';
			      $camt=$camt+$invoiceresult['Totalamount'];
			      $cnt++;
				}
        endforeach;
		if($cnt>0){
				$tabledata.='<tr><td colspan="6"><hr><br/></td></tr>';
				$tabledata.= '<tr >
			      	<td colspan="3">'."Total Count".'</td><td ><b>'.$cnt.'</b></td>
			     	<td align="right"><b>'.number_format($camt,2).'</b></td>
			     	<td></td>
		     		</tr>';
	     		$tabledata.='<tr><td colspan="8"><hr></td></tr>';
			}
		
		////////takaful
		$prev="";
		foreach ($result1 as $invoiceresult ): 
       		if($invoiceresult['Flag']==2){
       			if($cnt1==0){$name = 'Takaful';}else {$name = '';}
	       			$tabledata.= '<tr>
			      	<td><b>'.$name.'</b></td>
		    		<td>'.$invoiceresult['ReceiptDate'].'</td>
			     	<td>'.$invoiceresult['TakafulName'].'</td>
			     	<td>'.$invoiceresult['ReceiptNum'].'</td>
			     	<td align="right">'.$invoiceresult['Totalamount'].'</td>
			     	<td align="right">'.$invoiceresult['fName'].'</td>
			      	</tr>';
			      $tamt=$tamt+$invoiceresult['Totalamount'];
			      $cnt1++;
			     	
				}
        endforeach;
		if($cnt1>0){
				$tabledata.='<tr><td colspan="8"><hr></td></tr>';
				$tabledata.= '<tr >
			      
			     	<td colspan="3">'."Total Count".'</td>
			     	<td ><b>'.$cnt1.'</b></td>
			     	<td align="right"><b>'.number_format($tamt,2).'</b></td>
			     	<td></td>
			     	</tr>';
	     		$tabledata.='<tr><td colspan="8"><hr></td></tr>';
			}
	endif;
			
	$tabledata.= '<tr >
	
	<td colspan="3"><b>'."Grand Total".'</b></td>
	<td ><b>'.number_format($cnt+$cnt1).'</b></td>
	<td align="right"><b>'.number_format($camt+$tamt,2).'</b></td>
	<td></td>
	</tr></table>';
		
	if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2013, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output("$filename.pdf",'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}
				
	}
				
}
