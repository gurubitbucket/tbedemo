<?php
class Reports_Model_DbTable_Manualmarksreportmodel extends Zend_Db_Table 
{ 
	
	public function fngetdetails($fromdate,$todate)
	{	
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "SELECT  a.IDApplication,a.ICNO,DATE_FORMAT(a.DateTime,'%d-%m-%Y') as Examdate,a.FName,if(a.pass=1,'Pass','Fail') as Result,b.Correct as obtained,b.Grade,
							   e.ProgramName,f.centername,h.fName as EnteredBy,c.A as PartAmarks,c.B as PartBmarks,c.C as PartCmarks,
							   d.managesessionname as session
					   FROM    tbl_manualmarksentry g
					           left outer join  tbl_studentapplication a on a.IDApplication = g.IDApplication
					           left outer join tbl_studentmarks b on a.IDApplication = b.IDApplication
					           left outer join tbl_studentdetailspartwisemarks c on a.IDApplication = c.IDApplication
					           left outer join tbl_managesession d on a.examsession = d.idmangesession 
			             	   left outer join tbl_programmaster e on a.Program = e.IdProgrammaster
							   left outer join tbl_center f on a.examvenue = f.idcenter
							   left outer join tbl_user h on g.UpdUser = h.iduser
					   WHERE   a.IDApplication>1148 and examvenue <> 000 and 
					           date(g.UpdDate) >= '$fromdate' and date(g.UpdDate) <= '$todate'
					   GROUP BY a.IDApplication
					   ORDER BY a.FName
					            ";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
}