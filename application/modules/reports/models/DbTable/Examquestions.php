<?php
class Reports_Model_DbTable_Examquestions extends Zend_Db_Table { 
	public function fndisplayquestionsforabovecode($questiongroup,$questionstatus,$fromqtn,$toqtn){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
			 		->from(array("c" => "tbl_questions"),array('c.idquestions','c.Question'))
				    ->join(array("d" => "tbl_groupnames"),'d.groupname=c.QuestionGroup',array(""));
	   				if(isset($questiongroup) && !empty($questiongroup) ){
						$lstrSelect = $lstrSelect->where("d.idgroupname= ?",$questiongroup);
					}
	   			 	if($questionstatus=='0' || $questionstatus=='1'){
	   			 		$lstrSelect = $lstrSelect->where("c.Active = ?",$questionstatus);
					}
	   				 if(isset($fromqtn) && !empty($fromqtn) ){
						$lstrSelect = $lstrSelect->where("c.idquestions>= ?",$fromqtn);
					}
	   				 if(isset($toqtn) && !empty($toqtn) ){
						$lstrSelect = $lstrSelect->where("c.idquestions<= ?",$toqtn);
					}
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	public function fndisplayanswersforquestions($ID){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
					  ->from(array("a" => "tbl_answers"),array("a.idanswers","a.answers","a.CorrectAnswer")) 
					  ->join(array("c" => "tbl_questions"),'c.idquestions=a.idquestion',array(""))
					  ->where("a.idquestion=?",$ID)
					  ->order("a.idanswers");	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	function fngetquestiongroup(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				   ->from(array("a" =>"tbl_groupnames"),array("key"=>"idgroupname","value"=>"groupname"))
				   ->order("a.groupname");	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
	}
	
	function fngetgroupname($groupid){
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
						  ->from(array("a" =>"tbl_groupnames"),array("a.groupname"))
					      ->where("a.idgroupname=?",$groupid);	
				$larrResult = $lobjDbAdpt->fetchrow($lstrSelect);
				return $larrResult;
		
	}
	
	function fngettotalquestions(){
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							->from(array("a" =>"tbl_questions"),array("count(a.idquestions) as count"));
				$larrResult = $lobjDbAdpt->fetchrow($lstrSelect);
				return $larrResult;
	}
}