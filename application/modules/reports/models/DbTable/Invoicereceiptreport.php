<?php
class Reports_Model_DbTable_Invoicereceiptreport extends Zend_Db_Table {	
	public function fnGetOperatorNames($opType,$namestring){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($opType==1){
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("tbl_companies"),array("CompanyName as name"))
			->where('CompanyName like ? "%"',$namestring)
			;
		}else if($opType==2){
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("tbl_takafuloperator"),array("TakafulName as name"))
			->where('TakafulName like ? "%"',$namestring);
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     	
     }
	
 	public function fnSearchCompanies($post = array()){//Function for searching the Company/Takaful Invoice Details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($post['field19']==1){//Search for Company 	 
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptmaster"),array("a.Idreceipt","a.ReceiptNum","DATE_FORMAT(a.ReceiptDate,'%d-%m-%Y') As ReceiptDate","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As generateddate","a.Amount As Totalamount","a.Companyflag As Flag"))
				//-> join(array("d"=>"tbl_receiptdetails"),"d.Idreceipt = a.Idreceipt",array("d.BatchId","d.InvoiceNum","d.Amount"))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.UpdUser",array("u.fName"))
				-> join(array("c"=>"tbl_companies"),"c.IdCompany = a.Idcompany",array("c.CompanyName"))
				-> where('a.Companyflag = 1')
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  >= '".$post['Date3']."'")
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  <= '".$post['Date4']."'")
				-> where('c.CompanyName like "%" ? "%"',$post['field3'])
				-> order('a.Idreceipt DESC');
		}else if($post['field19']==2){//Search for Takaful 	  
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptmaster"),array("a.Idreceipt","a.ReceiptNum","DATE_FORMAT(a.ReceiptDate,'%d-%m-%Y') As ReceiptDate","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As generateddate","a.Amount As Totalamount","a.Companyflag As Flag"))
				//-> join(array("d"=>"tbl_receiptdetails"),"d.Idreceipt = a.Idreceipt",array("d.BatchId","d.InvoiceNum","d.Amount"))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.UpdUser",array("u.fName"))
				-> join(array("c"=>"tbl_takafuloperator"),"c.idtakafuloperator = a.Idcompany",array("c.TakafulName"))
				-> where('a.Companyflag = 2')
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  >= '".$post['Date3']."'")
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  <= '".$post['Date4']."'")
				-> where('c.TakafulName like "%" ? "%"',$post['field3'])
				-> order('a.Idreceipt DESC');
		}else if($post['field19']=="" && $post['field3']==""){//Search for Company/Takaful 	  
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptmaster"),array("a.Idreceipt","(a.ReceiptNum) As ReceiptNum","DATE_FORMAT(a.ReceiptDate,'%d-%m-%Y') As ReceiptDate","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As generateddate","a.Amount As Totalamount","a.Companyflag As Flag"))
				//-> join(array("d"=>"tbl_receiptdetails"),"d.Idreceipt = a.Idreceipt",array("d.BatchId","d.InvoiceNum","d.Amount"))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.UpdUser",array("u.fName"))
				-> joinleft(array("c"=>"tbl_companies"),"c.IdCompany = a.Idcompany",array("c.CompanyName"))
				-> joinleft(array("e"=>"tbl_takafuloperator"),"e.idtakafuloperator = a.Idcompany",array("e.TakafulName"))
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  >= '".$post['Date3']."'")
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  <= '".$post['Date4']."'")
				-> where('a.Companyflag in(1,2)')
				-> order('a.Companyflag')
				-> order('a.Idreceipt DESC' );
				//-> group('a.ReceiptNum');
				
		/*$lstrselect="SELECT a.ReceiptNum FROM tbl_receiptmaster as a
					 JOIN (SELECT Idreceipt , BatchId FROM tbl_receiptdetails GROUP by Idreceipt) as b ON b.Idreceipt = a.Idreceipt
					 ";*/
					
				
				
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	
	public function fngetreceiptdetails($Idreceipt,$opr){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($opr==1){
		$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptmaster"),array("a.Idreceipt","(a.ReceiptNum) As ReceiptNum","DATE_FORMAT(a.ReceiptDate,'%d-%m-%Y') As ReceiptDate","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As generateddate","a.Amount As Totalamount","a.Companyflag As Flag","a.ReceiptType","a.ChkNum","a.Bankbranch","DATE_FORMAT(a.ChkDate,'%d-%m-%Y') As ChkDate"))
				-> join(array("d"=>"tbl_receiptdetails"),"d.Idreceipt = a.Idreceipt",array("d.BatchId","d.InvoiceNum","d.Amount"))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.UpdUser",array("u.fName"))
				-> joinleft(array("c"=>"tbl_companies"),"c.IdCompany = a.Idcompany",array("c.CompanyName As oprname"))
				-> where('a.Idreceipt=?',$Idreceipt);
		}else if($opr==2){
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptmaster"),array("a.Idreceipt","(a.ReceiptNum) As ReceiptNum","DATE_FORMAT(a.ReceiptDate,'%d-%m-%Y') As ReceiptDate","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As generateddate","a.Amount As Totalamount","a.Companyflag As Flag","a.ReceiptType","a.ChkNum","a.Bankbranch","DATE_FORMAT(a.ChkDate,'%d-%m-%Y') As ChkDate"))
				-> join(array("d"=>"tbl_receiptdetails"),"d.Idreceipt = a.Idreceipt",array("d.BatchId","d.InvoiceNum","d.Amount"))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.UpdUser",array("u.fName"))
				-> joinleft(array("e"=>"tbl_takafuloperator"),"e.idtakafuloperator = a.Idcompany",array("e.TakafulName As oprname"))
				-> where('a.Idreceipt=?',$Idreceipt);
		}		
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
	}
	
	public function fngetbatchamount($Idreceipt){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptdetails"),array("a.Idreceipt","a.BatchId","a.Amount","a.InvoiceNum"))
				-> where('a.Idreceipt ='.$Idreceipt);
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
	}
	
	

	

}