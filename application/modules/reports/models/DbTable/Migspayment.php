<?php
class Reports_Model_DbTable_Migspayment extends Zend_Db_Table{ 
	
	
	public function fngetdetailforstudentpayment($fromdate,$todate)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_migspayment"),array("a.*","a.vpc_TxnResponseCode as Status","Date(a.UpdDate) as Date"))
						 ->join(array('b'=>'tbl_studentapplication'),'b.IDApplication=a.vpc_MerchTxnRef',array("b.FName","b.DateTime as Examdate"))
						  ->join(array('c'=>'tbl_programmaster'),'c.IdProgrammaster=b.Program',array("c.ProgramName"))
						 ->join(array('d'=>'tbl_center'),'d.idcenter = b.Examvenue',array("d.centername"))
						//->where("b.Allotedseats >0")
						//->where("b.Active !=0")						 
						 ->where("Date(a.UpdDate) >= '$fromdate'")
						 ->where("Examvenue <> 000")
						 ->where("b.IDApplication>1148")
						 ->where("b.DateTime >= '2012-04-12'")
						 ->where("Date(a.UpdDate) <= '$todate'")
						 ->group("a.vpc_TransactionNo");
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
	}
	
	

}