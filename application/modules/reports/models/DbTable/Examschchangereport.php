<?php
	class Reports_Model_DbTable_Examschchangereport extends Zend_Db_Table {
		

		public function fngetexamschedulechange($larrformData)
	   {
	   	//print_r($larrformData);die(); 
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array("a.*")) 
				 				 ->join(array("c"=>"tbl_venuechange"),'a.IDApplication=c.IDapplication',array("c.DateTime as oldexamdate","c.NewExamDate","c.idvenuechange","c.prgcahngeflag","date(c.UpdDate) as progdate"))
				 				  ->join(array("d"=>"tbl_center"),'d.idcenter=c.Examvenue',array("d.centername"))
				 				   ->join(array("k"=>"tbl_center"),'k.idcenter=c.NewExamVenue',array("k.centername as newexamvenue"))
				 				    ->join(array("m"=>"tbl_managesession"),'m.idmangesession=c.ExamSession',array("m.managesessionname"))
				 				      ->join(array("n"=>"tbl_managesession"),'n.idmangesession=c.NewExamSession',array("n.managesessionname as newsession"))
				 				      ->joinleft(array("g"=>"tbl_programmaster"),'g.IdProgrammaster =c.oldprogramid',array("g.ProgramName as oldprg"))
				 				       ->joinleft(array("s"=>"tbl_programmaster"),'s.IdProgrammaster =c.newprogramid',array("s.ProgramName as newprg"))
				 				         ->join(array("t"=>"tbl_user"),'t.iduser =c.UpdUser',array("t.fName as username"))
				 				      ->order("a.Fname")
				 				      ->order("a.ICNO")
				 				      ->order("c.idvenuechange");
				 				  
				 				   if($larrformData['Coursename']) $lstrSelect->where("a.Fname like '%' ? '%'",$larrformData['Coursename']);	
				if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$larrformData['ICNO']);					  
		  if(isset($larrformData['Date']) && !empty($larrformData['Date']) && isset($larrformData['Date2']) && !empty($larrformData['Date2']))
                {
				  $lstrFromDate = $larrformData['Date'];
				  $lstrToDate = $larrformData['Date2'];
				  $lstrSelect = $lstrSelect->where("date(c.UpdDate) >= '$lstrFromDate' and date(c.UpdDate) <= '$lstrToDate'")
				   ->order("a.Fname")
				     ->order("a.ICNO");
		        }	
				//echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }

	   
		   public function fngetstudentnameschange()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.FName","value"=>"a.FName"))
				 				  ->join(array("c"=>"tbl_venuechange"),'a.IDApplication=c.IDapplication',array(""))
				 				  ->where("a.Payment=1")
										  ->where("a.pass=3")
										  ->group("a.FName")
										    ->order("a.FName")	;	 				 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	  
 
}
