<?php
class Reports_Model_DbTable_Attendancelistmodel extends Zend_Db_Table { 
	
	public function fngetexamanddatedetails($larrformData)
		{ 
			if($larrformData['Date3']) $fromdate = $larrformData['Date3'];
            if($larrformData['Date4']) $todate = $larrformData['Date4'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			if($larrformData['Coursename']) $course = $larrformData['Coursename'];
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect =  "SELECT b.idcenter AS idcenter,a.IDApplication,DATE_FORMAT(a.DateTime,'%d-%m-%Y') as Date,group_concat( a.Pass, '' ) AS Result, d.ProgramName AS Course, b.centername AS Venue, a.Payment, count( 'a.IDApplication' ) AS applied
                            FROM `tbl_studentapplication` AS `a` , `tbl_center` AS `b` , `tbl_newscheduler` AS `c` , `tbl_programmaster` AS `d`
                            WHERE (a.Examvenue = b.idcenter) AND (a.Year = c.idnewscheduler) AND (a.Program = d.IdProgrammaster) AND a.Payment = '1'  AND 
                                   a.DateTime <= curdate() and a.IDApplication >1148 and a.Examvenue <>000"; 
		    if($larrformData['Date3']) $lstrSelect .= " AND a.DateTime >='$fromdate'";
            if($larrformData['Date4']) $lstrSelect .= " AND a.DateTime <='$todate'";
		    if($larrformData['Venues']) $lstrSelect .= " AND b.idcenter =  ".$venue;
		    if($larrformData['Coursename']) $lstrSelect .= " AND d.IdProgrammaster =  ".$course;
		    $lstrSelect .= " GROUP BY a.Program, a.Examvenue, Date having Date 
							 ORDER BY a.DateTime desc ,b.centername,d.IdProgrammaster";
		    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	public function fnajaxgetcenternames($ldfromdate,$ldtodate){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
									 ->join(array("b"=>"tbl_venuedateschedule"),"a.idcenter = b.idvenue",array(""))
									 ->where("b.Allotedseats > 0")
									 ->where("b.Active = 1")
									 ->where("b.date >= '$ldfromdate'")
									 ->where("b.date <= '$ldtodate'")
									 ->group("a.idcenter")
									 ->order("a.centername"); 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
	public function fngetcentername($centid)
	{ 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Select centername,idcenter
		               from tbl_center
		               where idcenter = '$centid';";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
		return $larrResult;
	}
	public function fngetprogramnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
}