<?php
class Reports_Model_DbTable_Chronepushreport extends Zend_Db_Table { 
	
 public function fngetchronepushdatas($fromdate,$todate)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a"=>"tbl_automail"),array("a.*"))
							 			 ->join(array("b"=>"tbl_cronevenue"),'a.idautomail=b.idautomail',array("b.*"))
							 			 ->join(array("c"=>"tbl_user"),'a.Upduser=c.iduser',array("c.fName"))
							 			 ->join(array("d"=>"tbl_center"),'b.idcenter=d.idcenter',array("d.centername"))
							 			 ->where("date(a.UpdDate)>='$fromdate'")
							 			 ->where("date(a.UpdDate)<='$todate'")
							 			 ->order("b.ExamDate")
							 			 ->order("d.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }
 
 
 public function fngetchronescheduler($fromdate,$todate)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a"=>"tbl_automail"),array("a.*"))
							 			 ->join(array("b"=>"tbl_chronescheduler"),'a.idautomail=b.idautomail',array("b.*"))
							 			 ->join(array("c"=>"tbl_user"),'a.Upduser=c.iduser',array("c.fName"))
							 			 ->join(array("d"=>"tbl_center"),'b.idcenter=d.idcenter',array("d.centername"))
							 			 ->where("date(a.UpdDate)>='$fromdate'")
							 			 ->where("date(a.UpdDate)<='$todate'")
							 			 ->order("d.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }
 
 
 public function fngetchronequestions($fromdate,$todate)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a"=>"tbl_automail"),array("a.*"))
							 			 ->join(array("b"=>"tbl_chronequestions"),'a.idautomail=b.idautomail',array("b.*"))
							 			 ->join(array("c"=>"tbl_user"),'a.Upduser=c.iduser',array("c.fName"))
							 			 ->join(array("d"=>"tbl_center"),'b.idcenter=d.idcenter',array("d.centername"))
							 			 ->where("date(a.UpdDate)>='$fromdate'")
							 			 ->where("date(a.UpdDate)<='$todate'")
							 			 ->order("d.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }
 
}