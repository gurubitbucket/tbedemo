<?php
class Reports_Model_DbTable_Pullpushdata extends Zend_Db_Table 
{ 
	public function fngetcenternames($ldtedate)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername"))
				 				 ->join(array("b"=>"tbl_venuedateschedule"),'a.idcenter = b.idvenue',array())
				 				 ->where("b.date = '$ldtedate'")
				 				 ->where("b.Allotedseats > 0")
								 ->where("b.Active = 1")
				 				 ->group("a.idcenter") 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	  public function fngetdetails($larrformData)
	   {
	   	 	   	    if($larrformData['ExamDate']) $ldtexamdate = $larrformData['ExamDate'];
			if($larrformData['Centre']) $cname = $larrformData['Centre'];
			
	   	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = "SELECT count( c.IDApplication ) AS Registered,d.centername,d.idcenter,group_concat( c.VenueTime,'') AS Result,
			                e.idmangesession,e.managesessionname,DATE_FORMAT(c.DateTime,'%d-%m-%Y') AS ExamDate,
			                if(d.ipaddress = 1,'YES','') as Localserver
						   FROM tbl_studentapplication c,tbl_center d,tbl_managesession e
						   WHERE c.Examvenue = d.idcenter
						         AND c.Examsession = e.idmangesession
								 AND c.DateTime = '$ldtexamdate'
								 AND c.Payment = 1
								 AND c.VenueTime in (0,1,2)
							";
			if($larrformData['Centre']) $lstrSelect.= " AND c.examvenue = ".$cname;
			$lstrSelect .= " Group by d.idcenter,c.Examsession
			                 Order by d.centername,c.Examsession";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;

	   }
	  public function fngetpulleddetails($lintidcenter,$ldtexamdate,$lintidsess)
	   {    
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select  a.IDApplication, a.FName, a.ICNO,b.ProgramName,b.IdProgrammaster,c.centername as ExamVenue, 
			                 		DATE_FORMAT(a.DateTime,'%d-%m-%Y') AS ExamDate,
			                 		if(a.pass=1,'Pass',if(a.pass=2,'Fail',if(a.pass=3,'Applied','Absent'))) as 'Result',g.Regid as RegisteredID,
			                 		e.idmangesession,e.managesessionname
						 	from 	 tbl_studentapplication a
					         		left outer join tbl_registereddetails g on g.IDApplication= a.IDApplication
					         		left outer join tbl_programmaster b on a.Program = b.IdProgrammaster
							 		left outer join tbl_center c on a.examvenue = c.idcenter
							 		left outer join tbl_managesession e on a.Examsession = e.idmangesession	
						    where    a.DateTime = '$ldtexamdate' and a.examvenue = '$lintidcenter' and a.payment = 1 and a.VenueTime in (1,2) and a.Examsession = '$lintidsess'
					 		group by a.IDApplication
							order by a.ICNO";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
 	 public function fngetpullfaileddetails($lintidcenter,$ldtexamdate,$lintidsess)
	   {    
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select a.IDApplication, a.FName, a.ICNO,b.ProgramName,b.IdProgrammaster,c.centername as ExamVenue, 
			                 		DATE_FORMAT(a.DateTime,'%d-%m-%Y') AS ExamDate,
			                 		if(a.pass=1,'Pass',if(a.pass=2,'Fail',if(a.pass=3,'Applied','Absent'))) as 'Result',g.Regid as RegisteredID,
			                 		e.idmangesession,e.managesessionname
						 	from 	 tbl_studentapplication a
					         		left outer join tbl_registereddetails g on g.IDApplication= a.IDApplication
					         		left outer join tbl_programmaster b on a.Program = b.IdProgrammaster
							 		left outer join tbl_center c on a.examvenue = c.idcenter
							 		left outer join tbl_managesession e on a.Examsession = e.idmangesession		
						    where    a.DateTime = '$ldtexamdate' and a.examvenue = '$lintidcenter' and a.payment = 1 and a.VenueTime = 0 and a.Examsession = '$lintidsess'
							group by a.IDApplication					 		
							order by a.ICNO";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
 	public function fngetpusheddetails($lintidcenter,$ldtexamdate,$lintidsess)
	   {    
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select a.IDApplication, a.FName, a.ICNO,b.ProgramName,b.IdProgrammaster,c.centername as ExamVenue, 
			                 		DATE_FORMAT(a.DateTime,'%d-%m-%Y') AS ExamDate,
			                 		if(a.pass=1,'Pass',if(a.pass=2,'Fail',if(a.pass=3,'Applied','Absent'))) as 'Result',g.Regid as RegisteredID,
			                 		e.idmangesession,e.managesessionname
						 	from 	 tbl_studentapplication a
					         		left outer join tbl_registereddetails g on g.IDApplication= a.IDApplication
					         		left outer join tbl_programmaster b on a.Program = b.IdProgrammaster
							 		left outer join tbl_center c on a.examvenue = c.idcenter	
							 		left outer join tbl_managesession e on a.Examsession = e.idmangesession	
						    where    a.DateTime = '$ldtexamdate' and a.examvenue = '$lintidcenter' and a.payment = 1 and a.VenueTime = 2 and a.Examsession = '$lintidsess'
							group by a.IDApplication
					 		order by a.ICNO";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
 	public function fngetpushfaileddetails($lintidcenter,$ldtexamdate,$lintidsess)
	   {    
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select a.IDApplication, a.FName, a.ICNO,b.ProgramName,b.IdProgrammaster,c.centername as ExamVenue, 
			                 		DATE_FORMAT(a.DateTime,'%d-%m-%Y') AS ExamDate,
			                 		if(a.pass=1,'Pass',if(a.pass=2,'Fail',if(a.pass=3,'Applied','Absent'))) as 'Result',g.Regid as RegisteredID,
			                 		e.idmangesession,e.managesessionname
						 	from 	tbl_studentapplication a
					         		left outer join tbl_registereddetails g on g.IDApplication= a.IDApplication
					         		left outer join tbl_programmaster b on a.Program = b.IdProgrammaster
							 		left outer join tbl_center c on a.examvenue = c.idcenter
							 		left outer join tbl_managesession e on a.Examsession = e.idmangesession		
						    where    a.DateTime = '$ldtexamdate' and a.examvenue = '$lintidcenter' and a.payment = 1 and a.VenueTime = 1 and a.Examsession = '$lintidsess'
							group by a.IDApplication
					 		order by a.ICNO";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	public function fngetcentername($centid)
	{ 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Select centername,idcenter
		               from tbl_center
		               where idcenter = '$centid';
		               ";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	public function fnajaxgetsessions($ldfromdate){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a"=>"tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
									 ->join(array("b"=>"tbl_venuedateschedule"),"a.idmangesession = b.idsession",array(""))
									 ->where("b.Allotedseats > 0")
									 ->where("b.Active = 1")
									 ->where("b.date = '$ldfromdate'")
									 ->group("a.idmangesession")
									 ->order("a.idmangesession");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
	public function fnajaxgetsessbydtvenue($ldfromdate,$lintvenueid){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a"=>"tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
									 ->join(array("b"=>"tbl_venuedateschedule"),"a.idmangesession = b.idsession",array(""))
									 ->where("b.Allotedseats > 0")
									 ->where("b.Active = 1")
									 ->where("b.date = '$ldfromdate'")
									 ->where("b.idvenue = '$lintvenueid'")
									 ->group("b.idmangesession")
									 ->order("a.idmangesession");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
}