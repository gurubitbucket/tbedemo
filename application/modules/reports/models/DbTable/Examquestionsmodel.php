<?php
class Reports_Model_DbTable_Examquestionsmodel extends Zend_Db_Table { 
	
    public function fndisplayquestionsforabovecode($fromqtn,$toqtn){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("c" => "tbl_questions"),array('c.idquestions','c.Question'))
										  ->where("c.idquestions >=$fromqtn")
										  ->where("c.idquestions <=$toqtn");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	public function fndisplayanswersforquestions($ID){
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_answers"),array("a.*")) 
										  ->join(array("c" => "tbl_questions"),'c.idquestions=a.idquestion',array("c.*"))
										  ->where("a.idquestion=?",$ID)
										  ->order("a.idanswers");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
}