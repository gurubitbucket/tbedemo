<?php
class Reports_Model_DbTable_Studentquestions extends Zend_Db_Table {	
	public function fnSearchstudent($regid){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_studentapplication"),array("a.FName","a.EmailAddress","a.ICNO"))
				->join(array("b"=>"tbl_registereddetails"),"b.IDApplication=a.IDApplication",array("b.Regid"))
				->join(array("c"=>"tbl_questionsetforstudents"),"c.IDApplication=a.IDApplication",array("c.idquestions"))
				->join(array("d"=>"tbl_batchmaster"),"d.IdBatch=b.IdBatch",array("d.BatchName","d.BatchFrom","d.BatchTo"))
				-> where("b.Regid =?",$regid);
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
	}
	
	public  function fnfetchtosofstudent($idquestions){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_questions"),array("a.idquestions","a.Question","a.QuestionLevel","a.QuestionGroup","a.QuestionNumber"))
				-> where("a.idquestions in ($idquestions)")
				->order("a.QuestionNumber");
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
		
	}
}