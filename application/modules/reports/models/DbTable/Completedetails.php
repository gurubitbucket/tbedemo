<?php
class Reports_Model_DbTable_Completedetails extends Zend_Db_Table{ 
	
	
	
	public function fngetdetailforstudent($fromdate,$todate)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_studentapplication"),array("a.*","DATE_FORMAT(a.DateTime,'%d-%m-%Y') as Date"  ))
						 ->join(array('b'=>'tbl_managesession'),'b.idmangesession = a.Examsession',array("b.managesessionname"))
						 ->join(array('c'=>'tbl_center'),'c.idcenter = a.Examvenue',array("c.centername"))
						  ->join(array('d'=>'tbl_programmaster'),'d.IdProgrammaster = a.Program',array("d.ProgramName"))
						 //->where("b.Allotedseats >0")
						 //->where("b.Active !=0")
					 
						 ->where("a.DateTime >= '$fromdate'")
						 ->where("a.Examvenue <> 000")
						 ->where("a.IDApplication>1148")
						 ->where("a.DateTime >= '2012-04-12'")
						 ->where("a.DateTime <= '$todate'")
						 ->where("a.pass in (1,2)")
						 ->where(" a.Payment = 1")
						 
						 //->group("a.DateTime")
						 //->group("a.Examvenue")
						 //->group("b.idmangesession")
						 ->order("a.DateTime desc")
						 ->order("c.centername")
						 ->order("b.managesessionname")
						 ->order("a.FName");
						 
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
	}
	
	
	
		public function fngetattempts($fromdate,$todate)
	{
		
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_studentapplication"),array("distinct(a.PermCity) as attempts"))
						 //->where("b.Allotedseats >0")
						 //->where("b.Active !=0")
					 
						 ->where("a.DateTime >= '$fromdate'")
						 ->where("a.Examvenue <> 000")
						 ->where("a.IDApplication>1148")
						 ->where("a.DateTime >= '2012-04-12'")
						 ->where("a.DateTime <= '$todate'")
						 ->where("a.pass in (1,2)")
						 ->where("a.Payment = 1")
						 ->order("a.PermCity");
						 
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
		
	}
	
	
	
		public function fngetallattemptsforicno($icno,$cnt)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("b" => "tbl_studentapplication"),array("b.*","DATE_FORMAT(b.DateTime,'%d-%m-%Y') AS DateTime"))
										   ->join(array("f" => "tbl_registereddetails"),'f.IDApplication=b.IDApplication',array("f.Regid"))
										   ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster=b.Program',array("c.ProgramName"))
										    ->join(array('d'=>'tbl_center'),'d.idcenter = b.Examvenue',array("d.centername"))
											 ->join(array('e'=>'tbl_managesession'),'e.idmangesession  = b.Examsession',array("e.managesessionname"))
										     ->where("b.ICNO=?",$icno)
											->where("b.PermCity <=?",$cnt)
											->where("b.PermCity !=0")
											->order("b.PermCity")
											->order("b.DateTime")
											->order("c.ProgramName")
											->order("d.centername")
											->order("e.managesessionname")
											->group("b.IDApplication");
										    //->where("b.Program =?",$program);
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	

}
