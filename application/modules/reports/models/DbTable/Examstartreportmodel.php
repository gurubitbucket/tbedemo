<?php
class Reports_Model_DbTable_Examstartreportmodel extends Zend_Db_Table{ 
	
	
	
	public function fngetvenuestartdetails($post)
	{
		$idmonth=$post['Month'];
		$idyear=$post['Year'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a" =>"tbl_centerstartexam"),array("a.*","DATE_FORMAT(a.ExamDate,'%d-%m-%Y') AS ExamDate1"))
									 ->join(array("b"=>"tbl_center"),'b.idcenter=a.idcenter',array("b.centername"))
									  ->join(array("c"=>"tbl_managesession"),'c.idmangesession=a.idSession',array("c.starttime","c.managesessionname as sename","(
hour( c.starttime ) *60 + minute( c.starttime )+2 ) AS totalstarttime"))
									  
									  ->join(array("m"=>"tbl_studentapplication"),'m.Examvenue=a.idcenter',array("b.centername"))
									 ->where("month(a.ExamDate)='$idmonth'")
									 ->where("year(a.ExamDate)='$idyear'")
									  ->where("month(m.DateTime)='$idmonth'")
									 ->where("year(m.DateTime)='$idyear'")
									 ->where("m.Examsession=a.idSession")
									  ->where("m.DateTime=a.ExamDate")
									  ->where("m.VenueTime in (1,2)")
									 ->group("a.idcenter")
									 ->group("a.ExamDate")
									  ->group("a.idSession")
									  ->order("b.centername")
									  ->order("a.ExamDate")
									  ->order("c.managesessionname desc");	

									 //echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
			return $larrResult;
		
	}
	
	
	public function fnlocaldatesession($post)
	{
		
		
		$idmonth=$post['Month'];
		$idyear=$post['Year'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
									$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a" =>"tbl_studentapplication"),array("a.Examvenue","a.Examsession","a.DateTime"))
									 ->where("month(a.DateTime)='$idmonth'")
									 ->where("year(a.DateTime)='$idyear'")
									 ->where("payment=1")
									 ->where("VenueTime=0")
									 ->group("a.Examvenue")
									 ->group("a.DateTime")
									  ->group("a.Examsession")
									  ->order("a.Examvenue")
									  ->order("a.DateTime");	

									 //echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
			return $larrResult;
		
		
	}
	
  public function fngetmonthsajax($val){
   	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	 if($val==1){
   	        $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","name"=>"a.MonthName"))								 
								 ->where("a.idmonth <(SELECT month(curdate( )))");						 						
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
   	 }elseif($val==2){
   	 	    $lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","name"=>"a.MonthName"));			 						
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 	
   	 	
   	 }else{
   	 	$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","name"=>"a.MonthName"))
									 ->where("a.idmonth >= 11");				 						
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 	
   	 }
   	return $larrResult;
   }



public function fngetmonthsajaxss($val){
   	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	 if($val==1){
   	        $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"))								 
								 ->where("a.idmonth <(SELECT month(curdate( )))");						 						
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
   	 }elseif($val==2){
   	 	    $lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"));			 						
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 	
   	 	
   	 }else{
   	 	$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"))
									 ->where("a.idmonth >= 11");				 						
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 	
   	 }
   	return $larrResult;
   }
	
	
	public function fngetmonthname($month)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a" =>"tbl_newmonths"),array("a.MonthName"))
									 ->where("a.idmonth = '$month'");
									 //echo 	$lstrSelect;die();			 						
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 	
			return $larrResult;
	}
	
	
}