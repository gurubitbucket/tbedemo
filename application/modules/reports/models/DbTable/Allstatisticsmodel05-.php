<?php
class Reports_Model_DbTable_Allstatisticsmodel extends Zend_Db_Table{ 
	
	public function fngetages($lintmon,$lintyear){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.`IDApplication`,cast(((to_days(curdate()) - to_days(a.DateOfBirth)) / 365) as signed) AS 'Age',a.pass
 					   FROM tbl_studentapplication a ,tbl_newscheduler b
                       WHERE  a.Year = b.idnewscheduler and a.Exammonth = $lintmon  and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	
	public function fngetprograms($lintmon,$lintyear){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT count(a.IDApplication) as NoOfCandidates,c.ProgramName,c.IdProgrammaster 
 					   FROM tbl_studentapplication a ,tbl_newscheduler b, tbl_programmaster c
 					   WHERE  a.Year = b.idnewscheduler and a.Exammonth = $lintmon  and a.Program = c.IdProgrammaster and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148
 					   GROUP BY a.Program ORDER BY c.IdProgrammaster ";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetprogramresult($lintmon,$lintyear,$lintiddeftype){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.pass 
		               FROM  tbl_studentapplication a ,tbl_newscheduler b
		               WHERE a.Year = b.idnewscheduler and a.pass in(1,2) and a.Exammonth= $lintmon and a.Payment = 1 
		               and a.Program = $lintiddeftype and b.Year = $lintyear and Examvenue <> 000 and a.IDApplication>1148 ";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetresultprogramnames(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT c.ProgramName,c.IdProgrammaster 
		               FROM  tbl_programmaster c";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetcentres($lintmon,$lintyear,$lintcentre){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.`IDApplication`,c.centername,a.pass,c.idcenter
 					   FROM tbl_studentapplication a ,tbl_newscheduler b,tbl_center c
                       WHERE  a.Year = b.idnewscheduler and a.Examvenue = c.idcenter and a.Exammonth = $lintmon and c.idcenter = $lintcentre and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148
                       Order By c.centername";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetcentrenames(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT c.centername,c.idcenter
 					   FROM tbl_center c
                       Order By c.centername";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetresultgrade($lintmon,$lintyear,$lintidprogram){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.pass,c.Grade
		               FROM  tbl_studentapplication a ,tbl_newscheduler b, tbl_studentmarks c
		               WHERE a.Year = b.idnewscheduler and a.IDApplication = c.IDApplication and a.pass in(1,2) and a.Exammonth= $lintmon and a.Payment = 1 and a.Program = $lintidprogram and b.Year = $lintyear and Examvenue <> 000 and a.IDApplication>1148";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
	}
	public function fngetprogramnames(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT c.ProgramName,c.IdProgrammaster
		               FROM  tbl_programmaster c";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
	}
	public function fngetabsentdetails($lintmon,$lintyear,$lintidprogram){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT count(a.IDApplication) as NoOfCandidates
 					   FROM tbl_studentapplication a ,tbl_newscheduler b
                       WHERE  a.Year = b.idnewscheduler and a.Program = $lintidprogram and a.Exammonth = $lintmon  and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148
                       GROUP BY a.Program";
		$larrResult = $lobjDbAdpt->fetchRow($lstrselect);
		return $larrResult;
	}
	public function fngetgenderscount($lintmon,$lintyear){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT count(a.IDApplication) as NoOfCandidates,if(a.Gender = 1,'Male','Female') as Gender
 					   FROM tbl_studentapplication a ,tbl_newscheduler b
                       WHERE  a.Year = b.idnewscheduler and a.Exammonth = $lintmon  and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148
                       GROUP BY Gender";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetgenderresult($lintmon,$lintyear,$lintiddeftype){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.pass 
		               FROM  tbl_studentapplication a ,tbl_newscheduler b
		               WHERE a.Year = b.idnewscheduler and a.pass!=3 and a.pass!=4 and a.Exammonth= $lintmon and a.Payment = 1 and a.Gender = $lintiddeftype and b.Year = $lintyear and Examvenue <> 000 and a.IDApplication>1148";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetqualification($lintmonth,$lintYear){//Function to return number of candidates registered for different qualification
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	 $lstrselect = "SELECT count(tbl_studentapplication.idapplication) as NoOfCandidates,tbl_definationms.DefinitionDesc,`tbl_definationms`.`idDefinition`
 					   FROM tbl_studentapplication tbl_studentapplication ,
 					  	 	tbl_newscheduler tbl_newscheduler,
 					   		tbl_definationms tbl_definationms,
 					  		tbl_definationtypems tbl_definationtypems
                        WHERE  tbl_studentapplication.Year = tbl_newscheduler.idnewscheduler 
                        and tbl_definationms.idDefType=tbl_definationtypems.idDefType 
                        and tbl_definationms.idDefinition=tbl_studentapplication.Qualification 
                        and tbl_studentapplication.Exammonth = $lintmonth  
                        and tbl_studentapplication.Payment=1 
                        and tbl_newscheduler.Year = $lintYear 
                        and tbl_studentapplication.Examvenue!=000 
                        and tbl_studentapplication.IDApplication>1148
                       GROUP BY Qualification";
    	 /*$lstrselect = "SELECT
                             IF(tbl_studentapplication.idapplication, COUNT(tbl_studentapplication.idapplication),NULL) AS NoOfCandidates,
                             tbl_definationms.DefinitionDesc,`tbl_definationms`.`idDefinition` 
                             FROM tbl_definationms 
                             JOIN tbl_definationtypems tbl_definationtypems ON tbl_definationms.idDefType=tbl_definationtypems.idDefType AND tbl_definationtypems.defTypeDesc ='Education qualification' 
                             LEFT JOIN tbl_studentapplication tbl_studentapplication ON tbl_studentapplication.Exammonth = $lintmonth AND tbl_definationms.idDefinition=tbl_studentapplication.Qualification AND tbl_studentapplication.Payment=1 AND tbl_studentapplication.Examvenue!=000 AND tbl_studentapplication.IDApplication>1148 
                             LEFT JOIN tbl_newscheduler tbl_newscheduler ON tbl_studentapplication.Year = tbl_newscheduler.idnewscheduler AND tbl_newscheduler.Year = $lintYear 
                             GROUP BY tbl_definationms.DefinitionDesc  ORDER BY `tbl_definationms`.idDefinition";   */ 
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetqualificationresult($lintmonth,$lintyear,$lintqualificationid){ //Function to return number of candidates attended for different qualification
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT tbl_studentapplication.pass,tbl_studentapplication.Qualification
		               FROM  tbl_studentapplication tbl_studentapplication ,tbl_newscheduler tbl_newscheduler
		               WHERE tbl_studentapplication.Year = tbl_newscheduler.idnewscheduler and tbl_studentapplication.pass!=3 and tbl_studentapplication.pass!=4 and tbl_studentapplication.Exammonth= $lintmonth and tbl_studentapplication.Qualification= $lintqualificationid and tbl_newscheduler.Year = $lintyear  and tbl_studentapplication.Examvenue!=000 and tbl_studentapplication.IDApplication>1148";
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetqualificationnames(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT tbl_definationms.DefinitionDesc as QualificationName,tbl_definationms.idDefinition as idDefinition
 					   FROM tbl_definationtypems tbl_definationtypems ,tbl_definationms tbl_definationms
					   WHERE  tbl_definationtypems.idDefType = tbl_definationms.idDefType and  tbl_definationtypems.idDefType= 14";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetraces($lintmon,$lintyear){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT count(a.IDApplication) as NoOfCandidates,d.DefinitionDesc as Race,d.idDefinition 
 					   FROM tbl_studentapplication a ,tbl_newscheduler b , tbl_definationtypems c,tbl_definationms d
                       WHERE  a.Year = b.idnewscheduler and a.race = d.idDefinition and c.idDefType = d.idDefType  and c.idDefType = 13 and a.Exammonth = $lintmon  and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148
                       GROUP BY Race";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetracesresult($lintmon,$lintyear,$lintiddeftype){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.pass 
		               FROM  tbl_studentapplication a ,tbl_newscheduler b
		               WHERE a.Year = b.idnewscheduler and a.pass in (1,2) and a.Exammonth= $lintmon and a.Payment = 1 and a.Race = $lintiddeftype 
		               		 and b.Year = $lintyear and Examvenue <> 000 and a.IDApplication>1148";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetracesnames(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT b.DefinitionDesc as RaceName,b.idDefinition 
 					   FROM tbl_definationtypems a ,tbl_definationms b
					   WHERE  a.idDefType = b.idDefType and  a.idDefType= 13";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}

	public function fngetcentrenamesparam($lintmon){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT c.centername,c.idcenter
 					   FROM tbl_center c,tbl_venuedateschedule d
 					   WHERE c.idcenter = d.idvenue and d.Allotedseats > 0 and d.Active = 1 and MONTH(d.date) = '$lintmon'  
 					   GROUP BY c.idcenter
                       Order By c.centername";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}

}