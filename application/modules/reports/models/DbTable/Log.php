<?php
	class Reports_Model_DbTable_Log extends Zend_Db_Table {
						
	    //Get lobjStudentNameList
		public function fnGetloginnamesList(){
   			$db = Zend_Db_Table::getDefaultAdapter();
			$select =$db->select()
			             ->from(array("sm"=>"tbl_user"),array("key"=>"sm.loginName","value"=>"sm.loginName"))
			             ->order("sm.loginName");
			$result = $db->fetchAll($select);
			return $result;
   		}	
  
	    //Log Details Report Search Function
		public function fnReportSearchDetails($postData){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		 $lstrSelect = $lobjDbAdpt->select()
	   		                         ->from(array("tbl_studentregistration"=>"tbl_studentregistration"))
       								->join(array("tbl_studentapplication"=>"tbl_studentapplication"),'tbl_studentregistration.IdApplication = tbl_studentapplication.IdApplication',array("tbl_studentapplication.StudentId as StudentId","CONCAT(tbl_studentapplication.FName,' ',IFNULL(tbl_studentapplication.MName,' '),' ',IFNULL(tbl_studentapplication.LName,' ')) as StudentName"))
       								->join(array("tbl_invoicemaster"=>"tbl_invoicemaster"),'tbl_studentapplication.IdApplication=tbl_invoicemaster.IdStudent',array("tbl_invoicemaster.InvoiceNo as InvoiceNo","tbl_invoicemaster.InvoiceDt as InvoiceDt","tbl_invoicemaster.InvoiceAmt as InvoiceAmt"))
                                    ->join(array("tbl_invoicedetails"=>"tbl_invoicedetails"),'tbl_invoicedetails.IdInvoice=tbl_invoicemaster.IdInvoice',array("tbl_invoicedetails.idAccount as idAccount","tbl_invoicedetails.IdInvoiceDetails"))                                    
                                    ->join(array("tbl_accountmaster"=>"tbl_accountmaster"),'tbl_accountmaster.idAccount=tbl_invoicedetails.idAccount',array("tbl_accountmaster.AccountName as AccountName"))
                                    ->where("tbl_invoicemaster.Active=1")
                                    ->where("tbl_invoicedetails.Active=1")
                                    ->where("tbl_studentapplication.Active=1")
                                    ->where("tbl_accountmaster.Active=1");     					      				     								
       		if(isset($postData['StudentNameList']) && !empty($postData['StudentNameList']) ){
				$lstrSelect = $lstrSelect->where("tbl_studentapplication.IdApplication = ?",$postData['StudentNameList']);
			}
		    if(isset($postData['InvoiceNo']) && !empty($postData['InvoiceNo'])){
				$lstrSelect = $lstrSelect->where('tbl_invoicemaster.InvoiceNo like "%" ? "%"',$postData['InvoiceNo']);
			}			
			if(isset($postData['InvoiceDt']) && !empty($postData['InvoiceDt']) && isset($postData['ToDate']) && !empty($postData['ToDate'])){
				$lstrFromDate = date("Y-m-d",strtotime($postData['InvoiceDt']));
				$lstrToDate = date("Y-m-d",strtotime($postData['ToDate']));
				$lstrSelect = $lstrSelect->where("DATE_FORMAT(tbl_invoicemaster.InvoiceDt,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate'");				
			}		  			 				
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;			
		}		
}
