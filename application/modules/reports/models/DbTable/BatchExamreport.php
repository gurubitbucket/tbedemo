<?php
	class Reports_Model_DbTable_BatchExamreport extends Zend_Db_Table {
		public function fngetprogramnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   public function fngetcompanynames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"CompanyName")) 				 
				 				 ->order("a.CompanyName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   public function fngetcenternames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername")) 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   public function fngettakafulnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 				 
				 				 ->order("a.TakafulName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	 
	     
	   
	
		
	public function fngetcentername($centid)
	{ 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Select centername,idcenter
		               from tbl_center
		               where idcenter = '$centid';";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
		return $larrResult;
	}
	
         
	public function fngetallnames ()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.FName","value"=>"a.FName"))
				 				 ->where("a.IDApplication > 1148") 				 
				 				 ->order("a.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
	   public function fngetpinfortakaful($idtakaful)
	   {
	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	        $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("key" => "a.registrationPin","value" =>"a.registrationPin"))
										  ->join(array("b"=>"tbl_takafuloperator"),"b.idtakafuloperator=a.idCompany",array())
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										   ->join(array("d"=>"tbl_registereddetails"),"a.registrationPin = d.RegistrationPin",array())
										   ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication = d.IDApplication",array())
										  //->where("tbl_batchregistration.paymentStatus=1")
										   ->where("b.idtakafuloperator =?",$idtakaful)
										  ->where("c.companyflag =2")
										  ->where("d.Approved =1")
										    ->where("d.IDApplication>1148")
										  ->where("a.paymentStatus in (1,2)")
										  ->group("a.registrationPin");
										 // ->where("a.idBatchRegistration != ?",$idbatchregistration);
										 //echo $lstrSelect;die();
										   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
       
										  }
		 public function fngetpinforbatch($compflag,$idcompany)
	   {
		   
		  
	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         if($compflag==1)
	         {
	        $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("key" => "a.registrationPin","value" =>"a.registrationPin"))
										
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										   // ->join(array("d"=>"tbl_registereddetails"),"a.registrationPin = d.RegistrationPin",array())
											// ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication = d.IDApplication",array())										
										   ->where("a.idCompany =?",$idcompany)
										   // ->where("d.IDApplication>1148")
										  ->where("c.companyflag =1")
										  // ->where("d.Approved =1")
										  ->where("a.paymentStatus in (1,2)")
											->group("a.registrationPin");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			}
			
			
			if($compflag==2)
			{
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("key" => "a.registrationPin","value" =>"a.registrationPin"))
										
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										   //->join(array("d"=>"tbl_registereddetails"),"a.registrationPin = d.RegistrationPin",array())
										  // ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication = d.IDApplication",array())										  
										   ->where("a.idCompany =?",$idcompany)
										  ->where("c.companyflag =2")
										 // ->where("d.Approved =1")
										   // ->where("d.IDApplication>1148")
										  ->where("a.paymentStatus in (1,2)")
										  ->group("a.registrationPin");
								
										   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				
			}
										   
		return $larrResult;	
       
	}	
public function fngettakafulgroupwise($larrformData)
		{
		  // echo "<pre>";
		  // print_r($larrformData);die();
			if($larrformData['Date']) $fromdate = $larrformData['Date'];
            if($larrformData['Date2']) $todate = $larrformData['Date2'];
			
			if($larrformData['AFromDate']) $Afromdate = $larrformData['AFromDate'];
            if($larrformData['AToDate']) $Atodate = $larrformData['AToDate'];
			
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) 		 $icno = $larrformData['ICNO'];
			if($larrformData['Coursename'])  $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) 	 $venue = $larrformData['Venues'];
			if($larrformData['Takafulname']) $tname = $larrformData['Takafulname'];
			
			if($larrformData['Pin']) $Regpin = $larrformData['Pin'];
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select stdapp.IDApplication,FName,ICNO,
							prg.ProgramName,cnt.centername as ExamVenue,tak.TakafulName,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,
							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
							from tbl_studentapplication stdapp
							
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
							left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
							 left outer join tbl_takafuloperator tak on batch.idCompany = tak.idtakafuloperator 
							
							
							left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
							
							
							where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=2";
			 if($larrformData['Venues']) 		$lstrSelect .= " AND cnt.idcenter = $venue";
			 if($larrformData['Studentname'])	$lstrSelect .= " AND stdapp.fname like '%$name%'";
			 if($larrformData['ICNO']) 			$lstrSelect .= " AND stdapp.icno like '%$icno%'";
			 if($larrformData['Coursename'])	$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			 if($larrformData['Takafulname']) 	$lstrSelect .= " AND tak.idtakafuloperator = $tname";
			 if($larrformData['Date']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
             if($larrformData['Date2']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
			 if($larrformData['Pin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
			 
			 if($larrformData['AFromDate']) 	$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
             if($larrformData['AToDate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
			 
			// DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') <=
			 
             $lstrSelect .= " GROUP BY stdapp.IDApplication
			                  ORDER BY tak.TakafulName,batch.registrationPin,stdapp.DateTime,cnt.centername,prg.ProgramName,session.idmangesession"; 
                  //echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
public function fngetcompanytakafulgroupwise($larrformData)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			
			if($larrformData['Takcomp']==1)
			{
				if($larrformData['Date']) $fromdate = $larrformData['Date'];
	            if($larrformData['Date2']) $todate = $larrformData['Date2'];
				
				if($larrformData['AFromDate']) $Afromdate = $larrformData['AFromDate'];
	            if($larrformData['AToDate']) $Atodate = $larrformData['AToDate'];
				
				if($larrformData['Studentname']) $name = $larrformData['Studentname'];
				if($larrformData['ICNO']) 		 $icno = $larrformData['ICNO'];
				if($larrformData['Coursename'])  $Coursename = $larrformData['Coursename'];
				if($larrformData['Venues']) 	 $venue = $larrformData['Venues'];
				if($larrformData['Takcompnames']) $cname = $larrformData['Takcompnames'];
				
				if($larrformData['Pin']) $Regpin = $larrformData['Pin'];
				
	           	$lstrSelect = " select stdapp.IDApplication,FName,ICNO,
								prg.ProgramName,cnt.centername as ExamVenue,comp.CompanyName as Name,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,batch.totalNoofCandidates,
								if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
								from tbl_studentapplication stdapp
								left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
								left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
								left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
								left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
								left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
								left outer join tbl_companies comp on batch.idCompany = comp.IdCompany 	 
								left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
								where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=1";
								
		   	    
	             if($larrformData['Venues']) $lstrSelect .= " AND cnt.idcenter = $venue";
				 if($larrformData['Studentname'])$lstrSelect .= " AND stdapp.fname like '%$name%'";
				 if($larrformData['ICNO']) $lstrSelect .= " AND stdapp.icno like '%$icno%'";
				 if($larrformData['Coursename'])$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
				 if($larrformData['Takcompnames']) $lstrSelect .= " AND comp.IdCompany = $cname";
				 if($larrformData['Date']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
	             if($larrformData['Date2']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
				 if($larrformData['Pin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
				 
				 if($larrformData['AFromDate']) 	$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
	             if($larrformData['AToDate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
			    
	          		$lstrSelect .= " GROUP BY stdapp.IDApplication
				                    ORDER BY comp.CompanyName,batch.registrationPin,stdapp.DateTime,cnt.centername,prg.ProgramName,session.idmangesession"; 
	           		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			}
			
			
			if($larrformData['Takcomp']==2)
			{
				if($larrformData['Date']) $fromdate = $larrformData['Date'];
           		if($larrformData['Date2']) $todate = $larrformData['Date2'];
				if($larrformData['AFromDate']) $Afromdate = $larrformData['AFromDate'];
	            if($larrformData['AToDate']) $Atodate = $larrformData['AToDate'];
				if($larrformData['Studentname']) $name = $larrformData['Studentname'];
				if($larrformData['ICNO']) 		 $icno = $larrformData['ICNO'];
				if($larrformData['Coursename'])  $Coursename = $larrformData['Coursename'];
				if($larrformData['Venues']) 	 $venue = $larrformData['Venues'];
				if($larrformData['Takcompnames']) $tname = $larrformData['Takcompnames'];
				
				if($larrformData['Pin']) $Regpin = $larrformData['Pin'];
			
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = " select stdapp.IDApplication,FName,ICNO,
							prg.ProgramName,cnt.centername as ExamVenue,tak.TakafulName as Name,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,batch.totalNoofCandidates,
							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
							from tbl_studentapplication stdapp
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
							left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
							left outer join tbl_takafuloperator tak on batch.idCompany = tak.idtakafuloperator 
							left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
							where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=2";
			 
				if($larrformData['Venues']) 		$lstrSelect .= " AND cnt.idcenter = $venue";
			 	if($larrformData['Studentname'])	$lstrSelect .= " AND stdapp.fname like '%$name%'";
				if($larrformData['ICNO']) 			$lstrSelect .= " AND stdapp.icno like '%$icno%'";
				if($larrformData['Coursename'])	$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
				if($larrformData['Takcompnames']) 	$lstrSelect .= " AND tak.idtakafuloperator = $tname";
				if($larrformData['Date']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
	            if($larrformData['Date2']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
				if($larrformData['Pin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
			 
			 	if($larrformData['AFromDate']) 	$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
            	if($larrformData['AToDate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
			 
			// DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') <=
			 
             $lstrSelect .= " GROUP BY stdapp.IDApplication
			                  ORDER BY tak.TakafulName,batch.UpdDate desc,batch.registrationPin,stdapp.DateTime,cnt.centername,prg.ProgramName,session.idmangesession"; 
                  //echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		}
			
			
		    return $larrResult;              
		}		
										  
		 public function fngettakafuldetails($larrformData)
		{
			if($larrformData['fromdate']) $fromdate = $larrformData['fromdate'];
            if($larrformData['todate']) $todate = $larrformData['todate'];
			
			if($larrformData['Afromdate']) $Afromdate = $larrformData['Afromdate'];
            if($larrformData['Atodate']) $Atodate = $larrformData['Atodate'];
			
			if($larrformData['student']) $name = $larrformData['student'];
			if($larrformData['icno']) 		 $icno = $larrformData['icno'];
			if($larrformData['course'])  $Coursename = $larrformData['course'];
			if($larrformData['venue']) 	 $venue = $larrformData['venue'];
			if($larrformData['takafulname']) $tname = $larrformData['takafulname'];
			
			if($larrformData['takafulpin']) $Regpin = $larrformData['takafulpin'];
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select stdapp.IDApplication,FName,ICNO,
							prg.ProgramName,cnt.centername as ExamVenue,tak.TakafulName,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,
							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
							from tbl_studentapplication stdapp
							
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
							left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
							 left outer join tbl_takafuloperator tak on batch.idCompany = tak.idtakafuloperator 
							
							
							left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
							
							
							where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=2";
			 if($larrformData['venue']) 		$lstrSelect .= " AND cnt.idcenter = $venue";
			 if($larrformData['student'])	$lstrSelect .= " AND stdapp.fname like '%$name%'";
			 if($larrformData['icno']) 			$lstrSelect .= " AND stdapp.icno like '%$icno%'";
			 if($larrformData['course'])	$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			 if($larrformData['takafulname']) 	$lstrSelect .= " AND tak.idtakafuloperator = $tname";
			 if($larrformData['fromdate']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
             if($larrformData['todate']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
			   if($larrformData['takafulpin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
			 
			 if($larrformData['Afromdate']) 			$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
             if($larrformData['Atodate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
			 
			// DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') <=
			 
             $lstrSelect .= " GROUP BY stdapp.IDApplication
			                   ORDER BY tak.TakafulName,batch.registrationPin,stdapp.DateTime,cnt.centername,prg.ProgramName,session.idmangesession"; 
              // echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
		 public function fngetbatchdetails($larrformData)
		{
			//echo "<pre>";
			//print_r($larrformData);die();
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			if($larrformData['Takcomp']==1)
			{
				
			if($larrformData['fromdate']) $fromdate = $larrformData['fromdate'];
            if($larrformData['todate']) $todate = $larrformData['todate'];
			
			if($larrformData['Afromdate']) $Afromdate = $larrformData['Afromdate'];
            if($larrformData['Atodate']) $Atodate = $larrformData['Atodate'];
			
			if($larrformData['student']) $name = $larrformData['student'];
			if($larrformData['icno']) 		 $icno = $larrformData['icno'];
			if($larrformData['course'])  $Coursename = $larrformData['course'];
			if($larrformData['venue']) 	 $venue = $larrformData['venue'];
			if($larrformData['Companyname']) $tname = $larrformData['Companyname'];
			
			if($larrformData['companypin']) $Regpin = $larrformData['companypin'];
			
			
			$lstrSelect = " select stdapp.IDApplication,FName,ICNO,
							prg.ProgramName,cnt.centername as ExamVenue,comp.CompanyName as Name,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,batch.totalNoofCandidates,
							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
							from tbl_studentapplication stdapp
							
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
							left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
							 left outer join tbl_companies comp on batch.idCompany = comp.IdCompany 	 
							
							
							left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
							
							
							where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=1";
			 if($larrformData['venue']) 		$lstrSelect .= " AND cnt.idcenter = $venue";
			 if($larrformData['student'])	$lstrSelect .= " AND stdapp.fname like '%$name%'";
			 if($larrformData['icno']) 			$lstrSelect .= " AND stdapp.icno like '%$icno%'";
			 if($larrformData['course'])	$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			 if($larrformData['Companyname']) 	$lstrSelect .= " AND comp.IdCompany = $tname";
			 if($larrformData['fromdate']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
             if($larrformData['todate']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
			   if($larrformData['companypin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
			 
			 if($larrformData['Afromdate']) 			$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
             if($larrformData['Atodate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
			 
			// DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') <=
			 
             $lstrSelect .= " GROUP BY stdapp.IDApplication
			                  ORDER BY comp.CompanyName,batch.UpdDate desc,batch.registrationPin,stdapp.DateTime,cnt.centername,prg.ProgramName,session.idmangesession"; 
           
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		if($larrformData['Takcomp']==2)
		{
			
			if($larrformData['fromdate']) $fromdate = $larrformData['fromdate'];
            if($larrformData['todate']) $todate = $larrformData['todate'];
			
			if($larrformData['Afromdate']) $Afromdate = $larrformData['Afromdate'];
            if($larrformData['Atodate']) $Atodate = $larrformData['Atodate'];
			
			if($larrformData['student']) $name = $larrformData['student'];
			if($larrformData['icno']) 		 $icno = $larrformData['icno'];
			if($larrformData['course'])  $Coursename = $larrformData['course'];
			if($larrformData['venue']) 	 $venue = $larrformData['venue'];
			if($larrformData['Companyname']) $tname = $larrformData['Companyname'];
			
			if($larrformData['companypin']) $Regpin = $larrformData['companypin'];
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select stdapp.IDApplication,FName,ICNO,
							prg.ProgramName,cnt.centername as ExamVenue,tak.TakafulName as Name,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,batch.totalNoofCandidates,
							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
							from tbl_studentapplication stdapp
							
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
							left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
							 left outer join tbl_takafuloperator tak on batch.idCompany = tak.idtakafuloperator 
							
							
							left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
							
							
							where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=2";
			 if($larrformData['venue']) 		$lstrSelect .= " AND cnt.idcenter = $venue";
			 if($larrformData['student'])	$lstrSelect .= " AND stdapp.fname like '%$name%'";
			 if($larrformData['icno']) 			$lstrSelect .= " AND stdapp.icno like '%$icno%'";
			 if($larrformData['course'])	$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			 if($larrformData['Companyname']) 	$lstrSelect .= " AND tak.idtakafuloperator = $tname";
			 if($larrformData['fromdate']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
             if($larrformData['todate']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
			   if($larrformData['companypin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
			 
			 if($larrformData['Afromdate']) 			$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
             if($larrformData['Atodate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
			 
			// DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') <=
			 
             $lstrSelect .= " GROUP BY stdapp.IDApplication
			                   ORDER BY tak.TakafulName,batch.registrationPin,stdapp.DateTime,cnt.centername,prg.ProgramName,session.idmangesession"; 
              // echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		    return $larrResult;              
		}
	public function fnajaxgetcenternames($lintidname)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($lintidname == 1)
		{
		   $lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"CompanyName"))
									->order("a.CompanyName");
		}
		else
		{
		   $lstrSelect = $lobjDbAdpt->select()
				 				 	->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 				 
				 				 	->order("a.TakafulName");
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	}
public function fngetcompanyname($idflag,$idcompany)
{
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   if($idflag==1)
   {
    
                                               $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_companies"),array("a.CompanyName as Company")) 				 
				 				 ->where("a.IdCompany=?",$idcompany);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    }
    if($idflag==2)
   {
    
                                               $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_takafuloperator"),array("a.TakafulName as Company")) 				 
				 				 ->where("a.idtakafuloperator=?",$idcompany);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
    }
   return $larrResult;

}
public function fngetprogramname($idcourse)
{
     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("a.ProgramName")) 				 
				 				 ->where("a.IdProgrammaster=?",$idcourse);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
      return $larrResult;   

}
public function fngetvenuename($idvenue)
{
     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("a.centername"))				 
				 				 ->where("a.idcenter=?",$idvenue);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
      return $larrResult; 

}

									  
										  
	 public function fngetpinforbatchess($compflag,$idcompany)
	   {
		   
		  
	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         if($compflag==1)
	         {
	        $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("key" => "a.registrationPin","value" =>"a.registrationPin"))
										
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										   // ->join(array("d"=>"tbl_registereddetails"),"a.registrationPin = d.RegistrationPin",array())
											// ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication = d.IDApplication",array())										
										  // ->where("a.idCompany =?",$idcompany)
										   // ->where("d.IDApplication>1148")
										  ->where("c.companyflag =1")
										  // ->where("d.Approved =1")
										  ->where("a.paymentStatus in (1,2)");
			if($idcompany!=0){
				$lstrSelect .= " AND a.idCompany = $idcompany";
			}
			
			 $lstrSelect .= " GROUP BY a.registrationPin";
										   
										   
										   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			}
			if($compflag==2)
			{
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("key" => "a.registrationPin","value" =>"a.registrationPin"))
										
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										   //->join(array("d"=>"tbl_registereddetails"),"a.registrationPin = d.RegistrationPin",array())
										  // ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication = d.IDApplication",array())										  
										   //->where("a.idCompany =?",$idcompany)
										  ->where("c.companyflag =2")
										 // ->where("d.Approved =1")
										   // ->where("d.IDApplication>1148")
										   ->where("a.paymentStatus in (1,2)");
			if($idcompany!=0){
				$lstrSelect .= " AND a.idCompany = $idcompany";
			}
			
			 $lstrSelect .= " GROUP BY a.registrationPin";
									
								
				
										 // echo $lstrSelect;
										   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				
			}							   
		return $larrResult;	
       
										  }	


 public function fnbatchregcnt($batchids)
										  {
										  	if(empty($batchids))
										  	{
										  		$batchids="'a0'";
										  	}
										  		        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
										  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("d" => "tbl_registereddetails"),array("count(RegistrationPin) as regtotal","RegistrationPin"))
										  ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication=d.IDApplication",array())									  
										  ->where("d.RegistrationPin in ($batchids)")
										  ->where("d.Approved =1")
										  ->group("d.RegistrationPin");
										 // echo $lstrSelect;die();
										   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
										  	return $larrResult;	
										  	
										  	
										  }


										 
	
}
