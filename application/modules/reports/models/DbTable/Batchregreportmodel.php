<?php
class Reports_Model_DbTable_Batchregreportmodel extends Zend_Db_Table { 
	
	public function fngetcompanydetails($fromdate,$todate) 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();  
		$lstrSelect = "SELECT c.CompanyName as CompanyName,c.IdCompany,e.IdProgrammaster ,e.ProgramName,a.registrationPin as Regpin,a.idBatchRegistration,if(a.paymentStatus=1,'Paid',if(a.paymentStatus=2,'Paid','Pending')) as paymentstatus,
		                      DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as Date,a.totalNoofCandidates as Noofcandidates,b.modeofpayment,
		                      if((b.modeofpayment=1),'FPX',if((b.modeofpayment=2),'Credit Card',if((b.modeofpayment=5),'Money Order',if((b.modeofpayment=4),'Cheque', 
							  if((b.modeofpayment=6),'Postal Order',if((b.modeofpayment=7),'Credit/Bank to IBFIM account','Paylater')))))) as Modeofpayment,
							  DATE_FORMAT(f.ChequeDt,'%d-%m-%Y') as Paydate
					   FROM  tbl_batchregistration a
					         join tbl_studentpaymentoption b on a.idBatchRegistration = b.IDApplication
					         join tbl_companies c on a.idCompany = c.IdCompany
					         left outer join tbl_batchregistrationdetails d on a.idBatchRegistration = d.idBatchRegistration
					         left outer join tbl_programmaster e on d.idProgram = e.IdProgrammaster 
					         left outer join tbl_studentpaymentdetails f on f.IDApplication = a.idBatchRegistration
					   WHERE b.companyflag = 1 and a.paymentStatus = 2 and date(a.UpdDate)>='$fromdate' and date(a.UpdDate)<='$todate' and b.modeofpayment not in (1,2) 
					   group by a.registrationPin
					   order by c.CompanyName";
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fngettakafuldetails($fromdate,$todate) 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();  
		$lstrSelect = "SELECT c.idtakafuloperator,c.TakafulName as CompanyName,e.IdProgrammaster ,e.ProgramName,a.registrationPin as Regpin,a.idBatchRegistration,if(a.paymentStatus=1,'Paid',if(a.paymentStatus=2,'Paid','Pending')) as paymentstatus,
		                      DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as Date,a.totalNoofCandidates as Noofcandidates,b.modeofpayment,
							  if((b.modeofpayment=1),'FPX',if((b.modeofpayment=2),'Credit Card',if((b.modeofpayment=5),'Money Order',if((b.modeofpayment=4),'Cheque', 
							  if((b.modeofpayment=6),'Postal Order',if((b.modeofpayment=7),'Credit/Bank to IBFIM account','Paylater')))))) as Modeofpayment,
							  DATE_FORMAT(f.ChequeDt,'%d-%m-%Y') as Paydate                      
					   FROM  tbl_batchregistration a
				             join tbl_studentpaymentoption b on a.idBatchRegistration = b.IDApplication
				             join tbl_takafuloperator c on a.idCompany = c.idtakafuloperator
				             left outer join tbl_batchregistrationdetails d on a.idBatchRegistration = d.idBatchRegistration
				             left outer join tbl_programmaster e on d.idProgram = e.IdProgrammaster  
				             left outer join tbl_studentpaymentdetails f on f.IDApplication = a.idBatchRegistration
					   WHERE b.companyflag =2 and a.paymentStatus = 2 and date(a.UpdDate)>='$fromdate' and date(a.UpdDate)<='$todate' and b.modeofpayment not in (1,2) 
					   group by a.registrationPin
					   order by  c.TakafulName";
		//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fngetcompanycredit($fromdate,$todate) 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();  
		$lstrSelect = "SELECT c.CompanyName as CompanyName,c.IdCompany,e.IdProgrammaster ,e.ProgramName,a.registrationPin as Regpin,a.idBatchRegistration,if(a.paymentStatus=1,'Paid',if(a.paymentStatus=2,'Paid','Pending')) as paymentstatus,
		                      DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as Date,a.totalNoofCandidates as Noofcandidates,b.modeofpayment,
		                      if((b.modeofpayment=1),'FPX',if((b.modeofpayment=2),'Credit Card',if((b.modeofpayment=5),'Money Order',if((b.modeofpayment=4),'Cheque', 
							  if((b.modeofpayment=6),'Postal Order',if((b.modeofpayment=7),'Credit/Bank to IBFIM account','Paylater')))))) as Modeofpayment,
							  DATE_FORMAT(date(g.UpdDate),'%d-%m-%Y') as Paydate
					   FROM  tbl_batchregistration a
					         join tbl_studentpaymentoption b on a.idBatchRegistration = b.IDApplication
					         join tbl_companies c on a.idCompany = c.IdCompany
					         left outer join tbl_batchpaypal g on g.IDCompany = a.idBatchRegistration
					         left outer join tbl_batchregistrationdetails d on a.idBatchRegistration = d.idBatchRegistration
					         left outer join tbl_programmaster e on d.idProgram = e.IdProgrammaster 
					         left outer join tbl_studentpaymentdetails f on f.IDApplication = a.idBatchRegistration
					   WHERE b.companyflag =1 and a.paymentStatus = 2 and date(a.UpdDate)>='$fromdate' and date(a.UpdDate)<='$todate' and b.modeofpayment = 2 
					   group by a.registrationPin
					   order by c.CompanyName";
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fngettakafulcredit($fromdate,$todate) 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();  
		$lstrSelect = "SELECT c.idtakafuloperator,c.TakafulName as CompanyName,e.IdProgrammaster ,e.ProgramName,a.registrationPin as Regpin,a.idBatchRegistration,if(a.paymentStatus=1,'Paid',if(a.paymentStatus=2,'Paid','Pending')) as paymentstatus,
		                      DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as Date,a.totalNoofCandidates as Noofcandidates,b.modeofpayment,
							  if((b.modeofpayment=1),'FPX',if((b.modeofpayment=2),'Credit Card',if((b.modeofpayment=5),'Money Order',if((b.modeofpayment=4),'Cheque', 
							  if((b.modeofpayment=6),'Postal Order',if((b.modeofpayment=7),'Credit/Bank to IBFIM account','Paylater')))))) as Modeofpayment,
							  DATE_FORMAT(date(g.UpdDate),'%d-%m-%Y') as Paydate                     
					   FROM  tbl_batchregistration a
				             join tbl_studentpaymentoption b on a.idBatchRegistration = b.IDApplication
				             join tbl_takafuloperator c on a.idCompany = c.idtakafuloperator
				             left outer join tbl_batchpaypal g on g.IDCompany = a.idBatchRegistration
				             left outer join tbl_batchregistrationdetails d on a.idBatchRegistration = d.idBatchRegistration
				             left outer join tbl_programmaster e on d.idProgram = e.IdProgrammaster  
				             left outer join tbl_studentpaymentdetails f on f.IDApplication = a.idBatchRegistration
					   WHERE b.companyflag =2 and a.paymentStatus = 2 and date(a.UpdDate)>='$fromdate' and date(a.UpdDate)<='$todate' and b.modeofpayment = 2 
					   group by a.registrationPin
					   order by c.TakafulName";
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fngetcompanyfpx($fromdate,$todate) 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();  
		$lstrSelect = "SELECT c.CompanyName as CompanyName,c.IdCompany,e.IdProgrammaster ,e.ProgramName,a.registrationPin as Regpin,a.idBatchRegistration,if(a.paymentStatus=1,'Paid',if(a.paymentStatus=2,'Paid','Pending')) as paymentstatus,
		                      DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as Date,a.totalNoofCandidates as Noofcandidates,b.modeofpayment,
		                      if((b.modeofpayment=1),'FPX',if((b.modeofpayment=2),'Credit Card',if((b.modeofpayment=5),'Money Order',if((b.modeofpayment=4),'Cheque', 
							  if((b.modeofpayment=6),'Postal Order',if((b.modeofpayment=7),'Credit/Bank to IBFIM account','Paylater')))))) as Modeofpayment,
							  DATE_FORMAT(date(g.TxnDate),'%d-%m-%Y') as Paydate 
					   FROM  tbl_batchregistration a
					         join tbl_studentpaymentoption b on a.idBatchRegistration = b.IDApplication
					         join tbl_companies c on a.idCompany = c.IdCompany
					         left outer join tbl_registrationfpx g on g.IDApplication = a.idBatchRegistration
					         left outer join tbl_batchregistrationdetails d on a.idBatchRegistration = d.idBatchRegistration
					         left outer join tbl_programmaster e on d.idProgram = e.IdProgrammaster 
					         left outer join tbl_studentpaymentdetails f on f.IDApplication = a.idBatchRegistration
					   WHERE b.companyflag =1 and a.paymentStatus = 2  and date(a.UpdDate)>='$fromdate' and date(a.UpdDate)<='$todate' and b.modeofpayment = 1
					   group by a.registrationPin
					   order by c.CompanyName";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fngettakafulfpx($fromdate,$todate) 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();  
		$lstrSelect = "SELECT c.idtakafuloperator,c.TakafulName as CompanyName,e.IdProgrammaster ,e.ProgramName,a.registrationPin as Regpin,a.idBatchRegistration,if(a.paymentStatus=1,'Paid',if(a.paymentStatus=2,'Paid','Pending')) as paymentstatus,
		                      DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as Date,a.totalNoofCandidates as Noofcandidates,b.modeofpayment,
							  if((b.modeofpayment=1),'FPX',if((b.modeofpayment=2),'Credit Card',if((b.modeofpayment=5),'Money Order',if((b.modeofpayment=4),'Cheque', 
							  if((b.modeofpayment=6),'Postal Order',if((b.modeofpayment=7),'Credit/Bank to IBFIM account','Paylater')))))) as Modeofpayment,
							  DATE_FORMAT(date(g.TxnDate),'%d-%m-%Y') as Paydate                     
					   FROM  tbl_batchregistration a
				             join tbl_studentpaymentoption b on a.idBatchRegistration = b.IDApplication
				             join tbl_takafuloperator c on a.idCompany = c.idtakafuloperator
				             left outer join tbl_registrationfpx g on g.IDApplication = a.idBatchRegistration
				             left outer join tbl_batchregistrationdetails d on a.idBatchRegistration = d.idBatchRegistration
				             left outer join tbl_programmaster e on d.idProgram = e.IdProgrammaster  
				             left outer join tbl_studentpaymentdetails f on f.IDApplication = a.idBatchRegistration
					   WHERE b.companyflag =2 and a.paymentStatus = 2  and date(a.UpdDate)>='$fromdate' and date(a.UpdDate)<='$todate' and b.modeofpayment = 1 
					   group by a.registrationPin
					   order by c.TakafulName";
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fngetstuddetails($lintregpin){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect ="select a.IDApplication, a.FName, a.ICNO, cast(datediff(curdate(), a.DateOfBirth)/365 as signed) as age,
			                 e.DefinitionDesc as Qual,DATE_FORMAT(a.DateOfBirth,'%d-%m-%Y') as DateOfBirth, 
			                 a.EmailAddress as email,b.ProgramName,b.IdProgrammaster,c.centername as ExamVenue, 
			                 concat(f.managesessionname,'(' ,f.starttime, ' - ', f.endtime, ')' ) as session,
			                 DATE_FORMAT(a.DateTime,'%d-%m-%Y') AS ExamDate
					 from 	 tbl_studentapplication a
					         left outer join tbl_registereddetails g on g.IDApplication= a.IDApplication
					         left outer join tbl_programmaster b on a.Program = b.IdProgrammaster
							 left outer join tbl_center c on a.examvenue = c.idcenter	
							 left outer join tbl_takafuloperator d on a.Takafuloperator = d.idtakafuloperator
							 left outer join tbl_definationms e on a.Qualification = e.iddefinition
							 left outer join tbl_managesession f on a.examsession = f.idmangesession
					 where    g.RegistrationPin = '$lintregpin'
					 order by a.FName";
		//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult; 
	}
    public function fngetchequedetails($lintregpin){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect =" SELECT b.ChequeNo,DATE_FORMAT(date(b.ChequeDt),'%d-%m-%Y')as Date,c.BankName,b.Amount
					   FROM  tbl_batchregistration a
					         left outer join tbl_studentpaymentdetails b on b.IDApplication = a.idBatchRegistration
					         join tbl_bank c on c.IdBank = b.BankName
					   WHERE a.registrationPin = $lintregpin;";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult; 
	}
	public function fngetfpxdetails($lintregpin){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect =" SELECT b.grossAmount as Amount,DATE_FORMAT(date(b.TxnDate),'%d-%m-%Y') as Date,b.bankBranch,b.fpxTxnId
					   FROM  tbl_batchregistration a
					         left outer join tbl_registrationfpx b on b.IDApplication = a.idBatchRegistration
					   WHERE a.registrationPin = $lintregpin;";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult; 
	}
	public function fngetcreditcarddetails($lintregpin){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect =" SELECT b.grossAmount as Amount ,b.transactionId,b.payerId,DATE_FORMAT(date(b.UpdDate),'%d-%m-%Y') as Date
					   FROM  tbl_batchregistration a
					         left outer join tbl_batchpaypal b on b.IDCompany = a.idBatchRegistration
					   WHERE a.registrationPin = '$lintregpin';";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult; 
	}
}