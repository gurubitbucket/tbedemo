<?php
class Reports_Model_DbTable_Studentapplicationmodel extends Zend_Db_Table { 
	
	public function fngetprogramnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	public function fngetcenternames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername")) 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
           
            public function fngetalldetails($larrformData)
		{
		     
			if($larrformData['Army']) $ArmyNo = $larrformData['Army']; 
			
			if($larrformData['UpDate1']) $update1 = $larrformData['UpDate1'];
			if($larrformData['UpDate2']) $update2 = $larrformData['UpDate2'];
			if($larrformData['Dates']) $fromdate = $larrformData['Dates'];
			if($larrformData['Dates2']) $todate = $larrformData['Dates2'];
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	 $lstrSelect = "select prograte.Rate,prograte.ServiceTax,std.fname,std.Amount,std.MobileNo,std.ArmyNo,icno,prg.programname,cnt.centername,std.IDApplication,std.EmailAddress,paymode.modeofpayment,
						          date_format(std.DateTime,'%Y-%m-%d') AS 'ExamDate',
						          date_format(std.DateTime,'%d-%m-%Y') AS 'ExamDate1',
						          if((std.batchpayment = 0),'Individual',if((std.batchpayment = 1),'Company','Takaful')) as Type,
						          if((std.pass = 1),'Pass',if((std.pass = 2),'Fail',if((std.pass = 3),'Applied','Absent'))) as Result, 
						          if((paymode.modeofpayment=1),'FPX',if((paymode.modeofpayment=2),'Credit Card',if((paymode.modeofpayment=5),'Money Order', if((paymode.modeofpayment=6),'Postal Order',if((paymode.modeofpayment=7),'Credit/Bank to IBFIM account',if((paymode.modeofpayment=10),'Credit-Card','others')))))) as Modeofpayment,
						          if((std.Payment = 1),'Paid','Pending') AS 'PaymentStatus',
						          date_format(Date(std.UpdDate),'%d-%m-%Y') as Applieddate
						   from  tbl_studentapplication std
								left outer join tbl_programmaster prg on std.program = prg.idprogrammaster
                                                                
                                                                join tbl_programrate prograte on prograte.idProgram = prg.idprogrammaster
                                                                join tbl_accountmaster accmaster on accmaster.idAccount = prograte.IdAccountmaster
                                                                

								left outer join tbl_center cnt on std.examvenue = cnt.idcenter
								left outer join tbl_managesession ssn on std.examsession = ssn.idmangesession
								left outer join tbl_studentpaymentoption paymode on std.idapplication = paymode.idapplication
						   where std.examvenue <> '000' and std.examvenue<> '0' and std.idapplication>1148 and prograte.Active=1 and accmaster.idAccount=1"; 
			if($larrformData['Payment']==1){$lstrSelect .= " AND std.Payment = 1";}    
			if($larrformData['Payment']==2){$lstrSelect .= " AND std.Payment = 0";}   			         
	   	    if($larrformData['paymentmode']==1){$lstrSelect .= " AND paymode.modeofpayment= 1";}
		 	if($larrformData['paymentmode']==2){$lstrSelect .= " AND paymode.modeofpayment=2";}
			if($larrformData['paymentmode']==4){$lstrSelect .= " AND paymode.modeofpayment =4";}
			if($larrformData['paymentmode']==5){$lstrSelect .= " AND paymode.modeofpayment=5";} 
			if($larrformData['paymentmode']==6){$lstrSelect .= " AND paymode.modeofpayment =6";}
			if($larrformData['paymentmode']==7){$lstrSelect .= " AND paymode.modeofpayment=7";}
			if($larrformData['paymentmode']==10){$lstrSelect .= " AND paymode.modeofpayment=10";}
	   	    if($larrformData['Venues']) $lstrSelect .= " AND cnt.idcenter = $venue";
			if($larrformData['Studentname'])$lstrSelect .= " AND std.fname like '%$name%'";
			
			if($larrformData['Army'])$lstrSelect .= " AND std.ArmyNo like '%$ArmyNo%'";
			
			if($larrformData['ICNO']) $lstrSelect .= " AND std.icno like '%$icno%'";
			if($larrformData['Coursename'])$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
		    if($larrformData['Dates']) $lstrSelect .= " AND std.DateTime >=  '$fromdate'";
			if($larrformData['Dates2']) $lstrSelect .= " AND std.DateTime <=  '$todate'";
		    if($larrformData['UpDate1']) $lstrSelect .= " AND Date(std.UpdDate) >=  '$update1'";
			if($larrformData['UpDate2']) $lstrSelect .= " AND Date(std.UpdDate) <=  '$update2'";
			$lstrSelect .= " GROUP BY std.IDApplication
			                 Order By std.fname";
			                 //echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
           
           
//	public function fngetalldetails($larrformData)
//		{
//
//if($larrformData['Army']) $ArmyNo = $larrformData['Army']; 
//			
//                      
//			if($larrformData['UpDate1']) $update1 = $larrformData['UpDate1'];
//			if($larrformData['UpDate2']) $update2 = $larrformData['UpDate2'];
//			if($larrformData['Dates']) $fromdate = $larrformData['Dates'];
//			if($larrformData['Dates2']) $todate = $larrformData['Dates2'];
//			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
//			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
//			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
//			if($larrformData['Venues']) $venue = $larrformData['Venues'];
//			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
//		 	 $lstrSelect = "select std.fname,std.ArmyNo,icno,prg.programname,cnt.centername,std.MobileNo,std.IDApplication,std.EmailAddress,paymode.modeofpayment,
//						          date_format(std.DateTime,'%Y-%m-%d') AS 'ExamDate',
//						          date_format(std.DateTime,'%d-%m-%Y') AS 'ExamDate1',
//						          if((std.batchpayment = 0),'Individual',if((std.batchpayment = 1),'Company','Takaful')) as Type,
//						          if((std.pass = 1),'Pass',if((std.pass = 2),'Fail',if((std.pass = 3),'Applied','Absent'))) as Result, 
//						          if((paymode.modeofpayment=1),'FPX',if((paymode.modeofpayment=2),'Credit Card',if((paymode.modeofpayment=5),'Money Order', if((paymode.modeofpayment=6),'Postal Order',if((paymode.modeofpayment=7),'Credit/Bank to IBFIM account',if((paymode.modeofpayment=10),'Credit-Card','others')))))) as Modeofpayment,
//						          if((std.Payment = 1),'Paid','Pending') AS 'PaymentStatus',
//						          date_format(Date(std.UpdDate),'%d-%m-%Y') as Applieddate
//						   from  tbl_studentapplication std
//								left outer join tbl_programmaster prg on std.program = prg.idprogrammaster
//								left outer join tbl_center cnt on std.examvenue = cnt.idcenter
//								left outer join tbl_managesession ssn on std.examsession = ssn.idmangesession
//								left outer join tbl_studentpaymentoption paymode on std.idapplication = paymode.idapplication
//						   where std.examvenue <> '000' and std.examvenue<> '0' and std.idapplication>1148"; 
//			if($larrformData['Payment']==1){$lstrSelect .= " AND std.Payment = 1";}    
//			if($larrformData['Payment']==2){$lstrSelect .= " AND std.Payment = 0";}   			         
//	   	    if($larrformData['paymentmode']==1) {$lstrSelect .= " AND paymode.modeofpayment= 1";}
//		 	if($larrformData['paymentmode']==2) {$lstrSelect .= " AND paymode.modeofpayment=2";}
//			if($larrformData['paymentmode']==4){$lstrSelect .= " AND paymode.modeofpayment =4";}
//			if($larrformData['paymentmode']==5){$lstrSelect .= " AND paymode.modeofpayment=5";} 
//			if($larrformData['paymentmode']==6){$lstrSelect .= " AND paymode.modeofpayment =6";}
//			if($larrformData['paymentmode']==7){$lstrSelect .= " AND paymode.modeofpayment=7";}
//	   	    if($larrformData['Venues']) $lstrSelect .= " AND cnt.idcenter = $venue";
//			if($larrformData['Studentname'])$lstrSelect .= " AND std.fname like '%$name%'";
//
//
//if($larrformData['Army'])$lstrSelect .= " AND std.ArmyNo like '%$ArmyNo%'";
//
//			if($larrformData['ICNO']) $lstrSelect .= " AND std.icno like '%$icno%'";
//			if($larrformData['Coursename'])$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
//		    if($larrformData['Dates']) $lstrSelect .= " AND std.DateTime >=  '$fromdate'";
//			if($larrformData['Dates2']) $lstrSelect .= " AND std.DateTime <=  '$todate'";
//		    if($larrformData['UpDate1']) $lstrSelect .= " AND Date(std.UpdDate) >=  '$update1'";
//			if($larrformData['UpDate2']) $lstrSelect .= " AND Date(std.UpdDate) <=  '$update2'";
//			$lstrSelect .= " GROUP BY std.IDApplication
//			                 Order By std.fname";
//			                 //echo $lstrSelect;die();
//			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
//		    return $larrResult;              
//		}
		
		public function fngetpersonaldetails($id)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = "select std.fname,qual.definitiondesc as 'Qualification',race.definitiondesc as 'Race',
						          cast(((to_days(curdate()) - to_days(std.DateOfBirth)) / 365) as signed) AS 'age',
						           if((std.Gender = 1),'Male','Female') AS 'Gender',std.EmailAddress,std.batchpayment as type,
						           if((std.batchpayment = 0),'Individual',if((std.batchpayment = 1),'Company','Takaful')) as Typename 
						   from tbl_studentapplication std
						        left outer join tbl_definationms qual on std.qualification = qual.iddefinition
								left outer join tbl_definationms race on std.race = race.iddefinition
						   where std.idapplication = $id";
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		    return $larrResult; 
		}
		public function fngetregdetails($id,$type)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			if($type == 2){
			$lstrSelect = "Select takaful.TakafulName as name ,reg.RegistrationPin
			               from tbl_registereddetails reg, tbl_batchregistration batch,tbl_takafuloperator takaful 
			               where reg.RegistrationPin =  batch.registrationPin 
			                      and batch.idCompany = takaful.idtakafuloperator 
			                      and reg.IDApplication =  $id 
			                   ";
			}elseif($type == 1){
				$lstrSelect = "Select comp.CompanyName as name ,reg.RegistrationPin
			               from tbl_registereddetails reg, tbl_batchregistration batch,tbl_companies comp 
			               where reg.RegistrationPin =  batch.registrationPin 
			                      and batch.idCompany = comp.IdCompany 
			                      and reg.IDApplication =  $id 
			                   ";
			}else{
				$lstrSelect = "Select * from tbl_studentapplication  where IDApplication = $id ";
			}
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		    return $larrResult;
		}
		public function fngetcentername($centid)
		{ 
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = "Select centername,idcenter
			               from tbl_center
			               where idcenter = '$centid';";
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
			return $larrResult;
	  	}
	   public function fngetprogramname($prgid)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = "Select ProgramName,IdProgrammaster
			               from tbl_programmaster
			               where IdProgrammaster = '$prgid';
			               ";
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
 public function  fnGetstudentpaymentdetails($Idapp,$Paymentmode)
		 {
		   // echo $Idapp;
			//echo "<br/>";
			// echo $Paymentmode;die();
		        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				
				$result = self::fncheckforindividual($Idapp,$Paymentmode);
				if($result)
				{
				  if($Paymentmode ==4 || $Paymentmode ==5 || $Paymentmode ==6 || $Paymentmode ==7)
				  {
				    $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array                                                                                                                     ("a.IDApplication","a.FName","a.MName","a.LName","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("b.ModeofPayment"))
							 ->join(array("d" => "tbl_studentpaymentdetails"),'d.IDApplication = a.IDApplication',array("d.ChequeNo as                                                                           Reference","DATE_FORMAT(d.ChequeDt,'%d-%m-%Y') as AppliedDate","d.Amount"))
							 ->where("b.companyflag =0")							 
							 ->where("e.RegistrationPin = '0000000'")
							 ->where("a.IDApplication =?",$Idapp)
							 ->where("b.ModeofPayment =?",$Paymentmode);							
			        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				   
					}
					if($Paymentmode == 10)
					{
					   $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array                                                                                                                     ("a.IDApplication","a.FName","a.MName","a.LName","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("b.ModeofPayment"))
							 ->join(array("d" => "tbl_migspayment"),'d.vpc_MerchTxnRef = a.IDApplication',array("d.vpc_Card","DATE_FORMAT                                                           (d.UpdDate,'%d-%m-%Y') as AppliedDate","d.vpc_TransactionNo as Transaction","d.vpc_AuthorizeId"))
							 ->where("b.companyflag =0")							 
							 ->where("e.RegistrationPin = '0000000'")
							 ->where("a.IDApplication =?",$Idapp)
							 ->where("b.ModeofPayment =?",$Paymentmode)
                                                          ->order("d.UpdDate desc");							
			          $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
					   
					}
					if($Paymentmode == 2)
					{
					   $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array                                                                                                                      ("a.IDApplication","a.FName","a.MName","a.LName","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("b.ModeofPayment"))
							 ->join(array("d" => "tbl_paypaldetails"),'d.IDApplication = a.IDApplication',array("DATE_FORMAT(d.UpdDate,'%d-%m-%Y') as                                                            AppliedDate","d.transactionId as Transaction","d.grossAmount as Amount"))
							 ->where("b.companyflag =0")							 
							 ->where("e.RegistrationPin = '0000000'")
							 ->where("a.IDApplication =?",$Idapp)
							 ->where("b.ModeofPayment =?",$Paymentmode)
                                                          ->order("d.UpdDate desc");							
			          $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
					   
					}
					if($Paymentmode == 1)
					{
					   $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array                                                           ("a.IDApplication","a.FName","a.MName","a.LName","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("b.ModeofPayment"))
							 ->join(array("d" => "tbl_registrationfpx"),'d.IDApplication = a.IDApplication',array("DATE_FORMAT(d.UpdDate,'%d-%m-%Y')                                                            as AppliedDate","d.fpxTxnId as Transaction","d.grossAmount as Amount","d.orderNumber"))
							 ->where("b.companyflag =0")							 
							 ->where("e.RegistrationPin = '0000000'")
							 ->where("a.IDApplication =?",$Idapp)
							 ->where("b.ModeofPayment =?",$Paymentmode)
                                                        ->order("d.UpdDate desc");							
			          $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
					   
					}
					
					 return $larrResult;
				    //$larrResult = $result;
				}
				else
				{
				    $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array                                                           ("a.IDApplication","a.FName","a.MName","a.LName","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("b" => "tbl_registereddetails"),'a.IDApplication = b.IDApplication',array(""))
							 ->join(array("c" => "tbl_batchregistration"),'c.registrationPin = b.RegistrationPin',array(""))
							 ->join(array("d" => "tbl_studentpaymentoption"),'d.IDApplication = c.idBatchRegistration',array("b.ModeofPayment"))
                                                         ->where("d.companyflag !=0")							 
							 ->where("b.RegistrationPin != '0000000'")
							 ->where("a.IDApplication =?",$Idapp)
							 ->where("d.ModeofPayment =?",$Paymentmode);						
			       $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				  
				   
				}
				 return $larrResult;
				
		 }
		  public function  fncheckforindividual($Idapp,$Paymentmode)
		  {
		       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				
		       $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication","a.FName","a.MName","a.LName","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("b.ModeofPayment"))
                             ->where("b.companyflag =0")							 
							 ->where("e.RegistrationPin = '0000000'")
							 ->where("a.IDApplication =?",$Idapp)
							 ->where("b.ModeofPayment =?",$Paymentmode);							
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
		  
		  }
}
