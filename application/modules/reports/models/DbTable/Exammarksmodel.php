<?php
class Reports_Model_DbTable_Exammarksmodel extends Zend_Db_Table { 
	
	public function fngetprogramnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	public function fngetcenternames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername")) 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	public function fngetdetails($larrformData)
	{
		if($larrformData['Fromdate']) $Fromdate = $larrformData['Fromdate'];
		if($larrformData['Todate']) $Todate = $larrformData['Todate'];
		if($larrformData['Course']) $Coursename = $larrformData['Course'];
		if($larrformData['Venue']) $venue = $larrformData['Venue'];
		if($larrformData['Marksto']) $Marksto = $larrformData['Marksto'];
		if($larrformData['Marksfrom']) $Marksfrom = $larrformData['Marksfrom'];
		if($larrformData['attendedfrom']) $attandedfrom =  $larrformData['attendedfrom'];
		if($larrformData['attendedto']) $attandedto =  $larrformData['attendedto'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "SELECT  a.IDApplication,a.FName,if(a.pass=1,'Pass','Fail') as Result,b.NoofQtns as TotalQuestions,b.Attended as Attended,b.Correct,
							   (b.NoofQtns - b.Attended) as Unattended,(b.Attended -b.Correct) as Wrong,b.Grade,
							   c.Starttime,c.Endtime,if(c.Submittedby=1,'Candidate Submit',if(c.Submittedby=2,'Center Submit','Auto Submitted')) as Submittedby,
							   e.ProgramName,f.centername
					   FROM    tbl_studentapplication a 
			             	   left outer join tbl_studentmarks b on a.IDApplication = b.IDApplication
			             	   left outer join tbl_studentstartexamdetails c on a.IDApplication = c.IDApplication
			             	   left outer join tbl_managesession d on a.examsession = d.idmangesession
			             	   left outer join tbl_programmaster e on a.Program = e.IdProgrammaster
							   left outer join tbl_center f on a.examvenue = f.idcenter
					   WHERE   a.IDApplication>1148 and examvenue <> 000 and a.Payment = 1 and a.pass not in(3,4) and a.DateTime >= '$Fromdate' 
					           and a.DateTime <= '$Todate' and b.Correct >= $Marksfrom and b.Correct <= $Marksto 
					           and b.Attended >= '$attandedfrom' and b.Attended <= '$attandedto' ";
		if($larrformData['Course']) $lstrSelect.= " AND e.IdProgrammaster = ".$Coursename;
		if($larrformData['Venue']) $lstrSelect.= " AND f.idcenter = ".$venue;
		$lstrSelect.= " Group by a.IDApplication
					    Order by a.FName";
		//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	public function fngetprogramname($prgid)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Select ProgramName,IdProgrammaster
		               from tbl_programmaster
		               where IdProgrammaster = '$prgid';
		               ";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	public function fngetcentername($centid)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Select centername,idcenter
		               from tbl_center
		               where idcenter = '$centid';
		               ";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	public function fnajaxgetcenternames($ldfromdate,$ldtodate){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
									 ->join(array("b"=>"tbl_venuedateschedule"),"a.idcenter = b.idvenue",array(""))
									 ->where("b.Allotedseats > 0")
									 ->where("b.Active = 1")
									 ->where("b.date >= '$ldfromdate'")
									 ->where("b.date <= '$ldtodate'")
									 ->group("a.idcenter")
									 ->order("a.centername"); 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
}