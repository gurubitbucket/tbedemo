<?php
class Reports_Model_DbTable_Racestatistics extends Zend_Db_Table { 
	
	public function fngetraces($lintmon,$lintyear){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT count(a.IDApplication) as NoOfCandidates,if(a.Race=164,'Chinese',if(a.Race=165,'Indian',if(a.Race=166,'Malay','Others'))) as Race
 					   FROM tbl_studentapplication a ,tbl_newscheduler b
                       WHERE  a.Year = b.idnewscheduler and a.Exammonth = $lintmon  and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148
                       GROUP BY Race";
		//echo $lstrselect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetracesresult($lintmon,$lintyear,$lintiddeftype){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.pass 
		               FROM  tbl_studentapplication a ,tbl_newscheduler b
		               WHERE a.Year = b.idnewscheduler and a.pass in (1,2) and a.Exammonth= $lintmon and a.Payment = 1 and a.Race = $lintiddeftype and b.Year = $lintyear and Examvenue <> 000 and a.IDApplication>1148";
		//echo $lstrselect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetracesnames(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT b.DefinitionDesc as RaceName
 					   FROM tbl_definationtypems a ,tbl_definationms b
					   WHERE  a.idDefType = b.idDefType and  a.idDefType= 13";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
}