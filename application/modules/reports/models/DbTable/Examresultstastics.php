<?php
class Reports_Model_DbTable_Examresultstastics extends Zend_Db_Table { 
	
	public function fngetresultgrade($lintmon,$lintyear,$lintidprogram){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.pass,c.Grade 
		               FROM  tbl_studentapplication a ,tbl_newscheduler b, tbl_studentmarks c
		               WHERE a.Year = b.idnewscheduler and a.IDApplication = c.IDApplication and a.pass!=3 and a.pass!=4 and a.Exammonth= $lintmon and a.Payment = 1 and a.Program = $lintidprogram and b.Year = $lintyear and Examvenue <> 000 and a.IDApplication>1148";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetabsentdetails($lintmon,$lintyear){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT count(a.IDApplication) as NoOfCandidates
 					   FROM tbl_studentapplication a ,tbl_newscheduler b
                       WHERE  a.Year = b.idnewscheduler and a.Exammonth = $lintmon  and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148
                       GROUP BY a.Program";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
}