<?php
class Reports_Model_DbTable_Invoicereport extends Zend_Db_Table {	
	public function fnGetOperatorNames($opType,$namestring){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($opType==1){
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("tbl_companies"),array("CompanyName as name"))
			->where('CompanyName like ? "%"',$namestring)
			;
		}else if($opType==2){
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("tbl_takafuloperator"),array("TakafulName as name"))
			->where('TakafulName like ? "%"',$namestring);
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     	
     }
	
 	public function fnSearchCompanies($post = array()){//Function for searching the Company/Takaful Invoice Details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($post['field19']==1){//Search for Company 	 
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_invoicegeneration"),array("a.Idinvoice","a.Invoiceuniqueid As invoiceno","a.Amount","a.Totalcandidates","a.Flag","a.Regpin","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate"))
				-> join(array("d"=>"tbl_invoicedetails"),"d.Idinvoicemaster = a.Idinvoice",array(""))
				-> join(array("b"=>"tbl_batchregistration"),"b.registrationPin = d.Regpin",array(""))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.Upduser",array("u.fName"))
				-> join(array("c"=>"tbl_companies"),"c.IdCompany = b.idCompany",array("c.CompanyName"))
				-> where('a.Flag = 1')
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  >= '".$post['Date3']."'")
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  <= '".$post['Date4']."'")
				->where('c.CompanyName like "%" ? "%"',$post['field3'])
				-> order('a.Flag')
				-> order('a.UpdDate DESC')
				-> group('d.Idinvoicemaster');
		}else if($post['field19']==2){//Search for Takaful 	  
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_invoicegeneration"),array("a.Idinvoice","a.Invoiceuniqueid As invoiceno","a.Amount","a.Totalcandidates","a.Flag","a.Regpin","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate"))
				-> join(array("d"=>"tbl_invoicedetails"),"d.Idinvoicemaster = a.Idinvoice",array(""))
				-> join(array("b"=>"tbl_batchregistration"),"b.registrationPin = d.Regpin",array(""))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.Upduser",array("u.fName"))
				-> join(array("c"=>"tbl_takafuloperator"),"c.idtakafuloperator = b.idCompany",array("c.TakafulName"))
				-> where('a.Flag = 2')
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  >= '".$post['Date3']."'")
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  <= '".$post['Date4']."'")
				->where('c.TakafulName like "%" ? "%"',$post['field3'])
				-> order('a.Flag')
				-> order('a.UpdDate DESC')
				-> group('d.Idinvoicemaster');
		}else if($post['field19']=="" && $post['field3']==""){//Search for Company/Takaful 	  
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_invoicegeneration"),array("a.Idinvoice","a.Invoiceuniqueid As invoiceno","a.Amount","a.Totalcandidates","a.Flag","a.Regpin","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate"))
				-> join(array("e"=>"tbl_invoicedetails"),"e.Idinvoicemaster = a.Idinvoice",array(""))
				-> join(array("b"=>"tbl_batchregistration"),"b.registrationPin = e.Regpin",array(""))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.Upduser",array("u.fName"))
				-> joinLeft(array("c"=>"tbl_companies"),"c.IdCompany = b.idCompany",array("c.CompanyName"))
				-> joinLeft(array("d"=>"tbl_takafuloperator"),"d.idtakafuloperator = b.idCompany",array("d.TakafulName"))
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  >= '".$post['Date3']."'")
				-> where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d')  <= '".$post['Date4']."'")
				-> where('a.Flag in(1,2)')
				-> order('a.Flag')
				-> order('a.UpdDate DESC')
				-> group('e.Idinvoicemaster');
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	
	public function fngetinvoicedetails($idinvoice,$opr){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_invoicegeneration"),array("a.Idinvoice","a.Invoiceuniqueid As invoiceno","a.Flag","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate"))
				-> join(array("e"=>"tbl_invoicedetails"),"e.Idinvoicemaster = a.Idinvoice",array("e.Amount","e.Totalcandidates","e.Regpin"))
				-> join(array("b"=>"tbl_batchregistration"),"b.registrationPin = e.Regpin",array(""))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.Upduser",array("u.fName"))
				-> joinLeft(array("c"=>"tbl_companies"),"c.IdCompany = b.idCompany",array("c.CompanyName","c.Email"))
				-> joinLeft(array("d"=>"tbl_takafuloperator"),"d.idtakafuloperator = b.idCompany",array("d.TakafulName","d.ContactEmail","d.ContactName","d.ContactCell"))
				-> where('e.Idinvoicemaster ='.$idinvoice)
				-> where('a.Flag ='.$opr);
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
	}
	

}