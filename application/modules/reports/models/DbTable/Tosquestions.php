<?php
class Reports_Model_DbTable_Tosquestions extends Zend_Db_Table {	
	
public function fnGetBatchArraySearch()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt-> select()
		    					 -> from(array("a" => "tbl_batchmaster"),array("key"=>"a.IdBatch","value"=>"a.BatchName"))
							->join(array("c"=>"tbl_tosmaster"),'a.IdBatch=c.IdBatch')
		    					 	->where("c.Active=0||1")
								 -> where("a.BatchStatus = 0");									
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
function fnGetExamDetails($batchId){
		//echo $batchId;echo "<br/>";
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select ="SELECT b.IdBatch,t.IdTOS,b.BatchName,b.BatchFrom,b.BatchTo,t.NosOfQues,t.TimeLimit,t.AlertTime,t.Active
		  		FROM tbl_tosmaster AS t		  		
		  		INNER JOIN `tbl_batchmaster` AS `b` ON (t.IdBatch = b.IdBatch) 		  		
		  		WHERE b.BatchStatus = 0";     
		  if($batchId != 0)  { 
		  		$select .= " AND b.IdBatch=".$batchId; 
		  }
		  		$select .= " order by(t.Active) DESC";  

		  		
		  		//echo $select;
		  return	$result = $lobjDbAdpt->fetchAll($select);	
	}
	
	public function fnGetTOSquestionpool($idtos){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt-> select()
		    					 -> from(array("a" => "tbl_tosmaster"),array(""))
								 ->join(array("b"=>"tbl_tosdetail"),'b.IdTOS=a.IdTOS',array("b.IdSection","b.NosOfQuestion as chaprterquestions"))
								 ->join(array("c"=>"tbl_tossubdetail"),'c.IdTOSDetail =b.IdTOSDetail ',array("c.IdDiffcultLevel","c.NoofQuestions as levelquestions"))
								 ->join(array("d"=>"tbl_batchmaster"),'d.IdBatch=a.IdBatch',array("d.BatchName as tosname"))
								 -> where("a.IdTOS = ?",$idtos);									
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetRandomQuestions($tosquestions){
		/*$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt-> select()
		    					 -> from(array("a" =>"tbl_questions"),array("a.Question","a.idquestions"))
		    					 ->where("a.QuestionNumber=?",$tosquestions['IdSection'])
		    					 ->where("a.QuestionLevel=?",$tosquestions['IdDiffcultLevel'])
								->order("rand()")
								->limit($tosquestions['levelquestions'],0);
		echo $lstrSelect;							
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;*/
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$level=$tosquestions['IdDiffcultLevel'];
		$Questionnumber=$tosquestions['IdSection'];
		$easyquestion=$tosquestions['levelquestions'];
		 
		$select ="SELECT a.Question,a.idquestions,a.QuestionGroup FROM tbl_questions AS a WHERE (a.QuestionLevel = '$level') AND a.Active=1 AND (a.QuestionNumber = '$Questionnumber') ORDER BY RAND() LIMIT 0,$easyquestion ";
		//echo $select;echo "<br/>";
		$result1 = $lobjDbAdpt->fetchAll($select);

		return $result1;
	}
	
}