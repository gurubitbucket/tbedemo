<?php
class Reports_Model_DbTable_Chronepullreport extends Zend_Db_Table { 
	
 public function fngetchronepulldatas($fromdate,$todate)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a"=>"tbl_autochronepull"),array("a.*"))
							 			 ->join(array("b"=>"tbl_chronepull"),'a.idautochronepull=b.idautochronepull',array("b.*"))
							 			 ->join(array("c"=>"tbl_user"),'a.Upduser=c.iduser',array("c.fName"))
							 			  ->join(array("d"=>"tbl_center"),'b.Venue=d.idcenter',array("d.centername"))
							 			    ->join(array("e"=>"tbl_managesession"),'e.idmangesession=b.Session',array("e.managesessionname"))
							 			 ->where("date(a.UpdDate)>='$fromdate'")
							 			 ->where("date(a.UpdDate)<='$todate'")
							 			 ->where("a.Automated=1")
							 			 ->order("b.ExamDate");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }
 
 public function fngetchronepulldatasautos($fromdate,$todate)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a"=>"tbl_autochronepull"),array("a.*"))
							 			 ->join(array("b"=>"tbl_chronepull"),'a.idautochronepull=b.idautochronepull',array("b.*"))
							 			  ->join(array("d"=>"tbl_center"),'b.Venue=d.idcenter',array("d.centername"))
							 			    ->join(array("e"=>"tbl_managesession"),'e.idmangesession=b.Session',array("e.managesessionname"))
							 			 ->where("date(a.UpdDate)>='$fromdate'")
							 			 ->where("date(a.UpdDate)<='$todate'")
							 			 ->where("a.Automated=0")
							 			 ->order("b.ExamDate");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }
 
}