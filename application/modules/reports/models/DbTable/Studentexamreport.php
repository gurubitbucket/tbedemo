<?php
class Reports_Model_DbTable_Studentexamreport extends Zend_Db_Table{ 
	
	
	public function fngetdetailforstudent($fromdate,$todate,$larrformData)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   //echo "<pre/>";print_r($larrformData);die();
	    
	   if($larrformData['Studentname']) $name = $larrformData['Studentname'];
	   if($larrformData['ICNO']) 		 $icno = $larrformData['ICNO'];
	   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_studentapplication"),array("a.FName","a.ICNO","a.EmailAddress","a.pass","a.pass","a.Gender","DATE_FORMAT(a.DateTime,'%d-%m-%Y') as Date","cast(datediff(curdate(), DateOfBirth)/365 as signed) as Age"))
						 ->join(array('c'=>'tbl_center'),'c.idcenter = a.Examvenue',array("c.centername"))
						 ->join(array('d'=>'tbl_programmaster'),'d.IdProgrammaster = a.Program',array("d.ProgramName"))
						 ->joinLeft(array("e" => "tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array("e.TakafulName"))
						 ->joinLeft(array("f" => "tbl_studentmarks"),'a.IDApplication=f.IDApplication',array("f.Grade"))
						 ->joinLeft(array("g" => "tbl_definationms"),'a.Qualification=g.idDefinition',array("g.DefinitionDesc as Qualification"))
						 ->joinLeft(array("h" => "tbl_definationms"),'a.Race=h.idDefinition',array("h.DefinitionDesc as Race"))
						 ->where("a.DateTime >= '$fromdate'")
						 ->where("a.Examvenue <> 000")
						 ->where("a.IDApplication>1148")
						 ->where("a.DateTime >= '2012-04-12'")
						 ->where("a.DateTime <= '$todate'")
						 ->where("a.pass in (1,2,4)")
						 ->where(" a.Payment = 1")
						 ->order("a.DateTime desc")
						 ->order("c.centername")
						 ->order("a.FName")
						 ->group("a.IDApplication");
			 if($larrformData['Studentname'])$lstrSelect ->where("a.FName like '%$name%'");
			 if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%$icno%'");	
			  if($larrformData['examcenter']){	
						$lstrSelect->where("c.idcenter = ?",$larrformData['examcenter']);
					}
			 
			 
			 
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);	
		
	   return $larrResult;
	}
	Public function fngetchronestatus($fromdate,$todate,$center)
	{
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$selectData = $lobjDbAdpt ->select()
			   			          ->from(array("a" =>"tbl_studentapplication"),array("a.VenueTime","DATE_FORMAT(a.DateTime,'%d-%m-%Y') as Date"))
						          ->join(array('b'=>'tbl_center'),'b.idcenter = a.Examvenue',array("b.centername"))
								  ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.examsession',array("c.managesessionname"))
								  ->where("a.DateTime >='$fromdate'")
								  ->where("a.DateTime <='$todate'")
								  ->where("a.Examvenue <> 000")
								   ->where("a.IDApplication>1148")
						           ->where("a.DateTime >= '2012-04-12'")
								   ->where("a.Payment = 1")
								   ->where("a.VenueTime = 1")
->group("a.DateTime")
->group("a.Examvenue")
->group("a.examsession");
 if(isset($center) && !empty($center)){	
						$selectData->where("b.idcenter = ?",$center);
					}

								 // ->where("b.Status =1");
								  //->where("b.Examdate>= '$idcenter)
								 // ->where("a.ipaddress=?",$ipaddress);
								// echo $selectData;die();
		$larrResult = $lobjDbAdpt->fetchAll($selectData);
		return $larrResult;
	}
	
	///////////////////////////////////////////exam centers by paramesh
	 public function fngetcenternames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect1 = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername"))
				 				 ->where("a.Active=1")		 
				 				 ->order("a.centername");
			$larrResult1 = $lobjDbAdpt->fetchAll($lstrSelect1);
			
			$lstrSelect2 = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername"))
				 				 ->join(array("f"=>"tbl_takafuloperator"),'f.idtakafuloperator=a.centercode',array(""))	
				 				  ->where("a.Active=0")	
				 				  ->where("a.Nooffloors=1")			 
				 				 ->order("a.centername");
			$larrResult2 = $lobjDbAdpt->fetchAll($lstrSelect2);
			
			
			$larrResult=array_merge($larrResult1,$larrResult2);
			
			return $larrResult;
	   }
	   
	   public function fngetcenternameonid($id)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("a.centername")) 				 
				 				 ->where("a.idcenter=?",$id);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	   }
	   
	   		
	

}
