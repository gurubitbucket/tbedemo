<?php
class Reports_Model_DbTable_Batchactionreportmodel extends Zend_Db_Table 
 { 
 
 
 
 
    public function fngetdetailsformovedcandidates($formdata)
	{
	
	
         $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	if($formdata['Takcomp'] == 1){
				 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array(""))
										  ->join(array("b"=>"tbl_companies"),"b.IdCompany=a.idCompany",array("b.CompanyName as Company"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())	
										  ->join(array("d"=>"tbl_movebatchdetails"),"d.Idbatch = a.idBatchRegistration",array("d.*","DATE_FORMAT(d.UpdDate,'%d-%m-%Y') as Date"))
										        ->join(array("e"=>"tbl_user"),"e.iduser=d.UpdUser",array("e.loginName as User"))
										
										  ->where("c.companyflag =1")
										   ->where("d.idCompany=?",$formdata['Takcompnames'])
										  ->where("a.paymentStatus =2")
										  ->group("d.Newregpin");
										 
		}else{
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array(""))
										  ->join(array("b"=>"tbl_takafuloperator"),"b.idtakafuloperator=a.idCompany",array("b.TakafulName as Company"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										  ->join(array("d"=>"tbl_movebatchdetails"),"d.Newregpin=a.registrationPin",array("d.*","DATE_FORMAT(d.UpdDate,'%d-%m-%Y') as Date"))
										  ->join(array("e"=>"tbl_user"),"e.iduser=d.UpdUser",array("e.loginName as User"))
										
										   ->where("d.idCompany=?",$formdata['Takcompnames'])
										  ->where("c.companyflag =2")
										  ->where("a.paymentStatus=2")
										  ->group("d.Newregpin");
										 
		}	
	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	
	}
	public function fngetdetailsforblockedregistration($formdata)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
	     if($formdata['Takcomp'] == 1){
				 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array(""))
										  ->join(array("b"=>"tbl_companies"),"b.IdCompany=a.idCompany",array("b.CompanyName as Company"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())	
										  ->join(array("d"=>"tbl_closedbatchregistration"),"d.Idcompany = a.IdCompany and d.Idbatch=a.idBatchRegistration",array("d.*","DATE_FORMAT(d.UpdDate,'%d-%m-%Y') as Date"))
										        ->join(array("e"=>"tbl_user"),"e.iduser=d.UpdUser",array("e.loginName as User"))
										
										  ->where("c.companyflag =1")
										   ->where("d.idCompany=?",$formdata['Takcompnames'])
										  ->where("a.paymentStatus =2")
										   ->group("d.Idbatch");
										 
		}else{
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array(""))
										  ->join(array("b"=>"tbl_takafuloperator"),"b.idtakafuloperator=a.idCompany",array("b.TakafulName as Company"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										   ->join(array("d"=>"tbl_closedbatchregistration"),"d.IdCompany=a.idCompany",array("d.*","DATE_FORMAT(d.UpdDate,'%d-%m-%Y') as Date"))
										     ->join(array("e"=>"tbl_user"),"e.iduser=d.UpdUser",array("e.loginName as User"))
										
										   ->where("d.idCompany=?",$formdata['Takcompnames'])
										  ->where("c.companyflag =2")
										  ->where("a.paymentStatus=2")
										  ->group("d.Idbatch");
										 
		}	
	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	
	}
	public function fngetdetailsforcancelpayment($formdata)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    if($formdata['Takcomp'] == 1){
				 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array(""))
										  ->join(array("b"=>"tbl_companies"),"b.IdCompany=a.idCompany",array("b.CompanyName as Company"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())	
										  ->join(array("d"=>"tbl_cancelpayment"),"d.idcompany = a.IdCompany ",array("d.*","DATE_FORMAT(d.UpdDate,'%d-%m-%Y') as Date"))
										        ->join(array("e"=>"tbl_user"),"e.iduser=d.UpdUser",array("e.loginName as User"))
										
										  ->where("c.companyflag =1")
										   ->where("d.idcompany=?",$formdata['Takcompnames'])
										  ->where("a.paymentStatus =2")
										   ->group("d.idBatchRegistration");
										 
		}else{
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array(""))
										  ->join(array("b"=>"tbl_takafuloperator"),"b.idtakafuloperator=a.idCompany",array("b.TakafulName as Company"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										   ->join(array("d"=>"tbl_cancelpayment"),"d.idcompany=a.IdCompany",array("d.*","DATE_FORMAT(d.UpdDate,'%d-%m-%Y') as Date"))
										   ->join(array("e"=>"tbl_user"),"e.iduser=d.UpdUser",array("e.loginName as User"))
										   ->where("c.companyflag =2")
										   ->where("d.idcompany=?",$formdata['Takcompnames'])
										  ->where("a.paymentStatus=2")
										  ->group("d.idBatchRegistration");
										 
		}	
	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	
	
	
	}
	
	public function fnajaxgetcenternames($lintidname)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($lintidname == 1)
		{
		   $lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"CompanyName"))
									->order("a.CompanyName");
		}
		else
		{
		   $lstrSelect = $lobjDbAdpt->select()
				 				 	->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 				 
				 				 	->order("a.TakafulName");
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	}
	

}