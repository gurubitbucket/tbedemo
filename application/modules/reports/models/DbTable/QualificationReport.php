<?php
	class Reports_Model_DbTable_QualificationReport extends Zend_Db_Table {
	 	
	public function fngetqualification($lintmonth,$lintYear){//Function to return number of candidates registered for different qualification
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$lstrselect = "SELECT count(tbl_studentapplication.idapplication) as NoOfCandidates,tbl_definationms.DefinitionDesc,`tbl_definationms`.`idDefinition`
 					   FROM tbl_studentapplication tbl_studentapplication ,
 					  	 	tbl_newscheduler tbl_newscheduler,
 					   		tbl_definationms tbl_definationms,
 					  		tbl_definationtypems tbl_definationtypems
                        WHERE  tbl_studentapplication.Year = tbl_newscheduler.idnewscheduler 
                        and tbl_definationms.idDefType=tbl_definationtypems.idDefType 
                        and tbl_definationms.idDefinition=tbl_studentapplication.Qualification 
                        and tbl_studentapplication.Exammonth = $lintmonth  
                        and tbl_studentapplication.Payment=1 
                        and tbl_newscheduler.Year = $lintYear 
                        and tbl_studentapplication.Examvenue!=000 
                        and tbl_studentapplication.IDApplication>1148
                       GROUP BY Qualification";
    	 /*$lstrselect = "SELECT
                             IF(tbl_studentapplication.idapplication, COUNT(tbl_studentapplication.idapplication),NULL) AS NoOfCandidates,
                             tbl_definationms.DefinitionDesc,`tbl_definationms`.`idDefinition` 
                             FROM tbl_definationms 
                             JOIN tbl_definationtypems tbl_definationtypems ON tbl_definationms.idDefType=tbl_definationtypems.idDefType AND tbl_definationtypems.defTypeDesc ='Education qualification' 
                             LEFT JOIN tbl_studentapplication tbl_studentapplication ON tbl_studentapplication.Exammonth = $lintmonth AND tbl_definationms.idDefinition=tbl_studentapplication.Qualification AND tbl_studentapplication.Payment=1 AND tbl_studentapplication.Examvenue!=000 AND tbl_studentapplication.IDApplication>1148 
                             LEFT JOIN tbl_newscheduler tbl_newscheduler ON tbl_studentapplication.Year = tbl_newscheduler.idnewscheduler AND tbl_newscheduler.Year = $lintYear 
                             GROUP BY tbl_definationms.DefinitionDesc  ORDER BY `tbl_definationms`.idDefinition";*/                      
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	
	public function fngetqualificationresult($lintmonth,$lintyear,$lintqualificationid){ //Function to return number of candidates attended for different qualification
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT tbl_studentapplication.pass 
		               FROM  tbl_studentapplication tbl_studentapplication ,tbl_newscheduler tbl_newscheduler
		               WHERE tbl_studentapplication.Year = tbl_newscheduler.idnewscheduler and tbl_studentapplication.pass!=3 and tbl_studentapplication.pass!=4 and tbl_studentapplication.Exammonth= $lintmonth and tbl_studentapplication.Qualification= $lintqualificationid and tbl_newscheduler.Year = $lintyear  and tbl_studentapplication.Examvenue!=000 and tbl_studentapplication.IDApplication>1148";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetqualificationnames(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT tbl_definationms.DefinitionDesc as QualificationName,tbl_definationms.idDefinition 
 					   FROM tbl_definationtypems tbl_definationtypems ,tbl_definationms tbl_definationms
					   WHERE  tbl_definationtypems.idDefType = tbl_definationms.idDefType and  tbl_definationtypems.idDefType= 14";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
}

