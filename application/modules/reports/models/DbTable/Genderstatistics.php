<?php
class Reports_Model_DbTable_Genderstatistics extends Zend_Db_Table { 
	
	public function fngetgenderscount($lintmon,$lintyear){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT count(a.IDApplication) as NoOfCandidates,if(a.Gender = 1,'Male','Female') as Gender
 					   FROM tbl_studentapplication a ,tbl_newscheduler b
                       WHERE  a.Year = b.idnewscheduler and a.Exammonth = $lintmon  and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148
                       GROUP BY Gender";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetgenderresult($lintmon,$lintyear,$lintiddeftype){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.pass 
		               FROM  tbl_studentapplication a ,tbl_newscheduler b
		               WHERE a.Year = b.idnewscheduler and a.pass!=3 and a.pass!=4 and a.Exammonth= $lintmon and a.Payment = 1 and a.Gender = $lintiddeftype and b.Year = $lintyear and Examvenue <> 000 and a.IDApplication>1148";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
}