<?php
class Reports_Model_DbTable_Excelstudentapplicationmodel extends Zend_Db_Table { 
	
	public function fngetexlstuappdetails($larrformData){
		if($larrformData['ExamfromDate']) $examfromdate = $larrformData['ExamfromDate'];
        if($larrformData['ExamtoDate']) $examtodate = $larrformData['ExamtoDate'];
        if($larrformData['Appliedfrom']) $appfromdate = $larrformData['Appliedfrom'];
        if($larrformData['Appliedto']) $apptodate = $larrformData['Appliedto'];
		if($larrformData['Venues']) $venue = $larrformData['Venues'];
		if($larrformData['Coursename']) $course = $larrformData['Coursename'];
		if($larrformData['Studentname']) $name = $larrformData['Studentname'];
		if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect ="SELECT a.IDApplication,a.FName as Stuname,a.DateTime as examdate,a.ICNO,
							 if(a.pass = 1,'Pass',if(a.pass = 2,'Fail',if(a.pass = 4,'Absent','Applied'))) as 'Result',
							 if(a.Pushedfrom = 1,'XML','FromClient') as Pushedfrom,b.idcenter,b.centername,c.IdProgrammaster,c.ProgramName
		              FROM tbl_logxlstudentapplication a,tbl_center b,tbl_programmaster c
		              WHERE a.Program = c.IdProgrammaster and a.Examvenue = b.idcenter";
		if($larrformData['ExamfromDate']) $lstrSelect .= " AND (a.DateTime) >='$examfromdate'";
		if($larrformData['ExamtoDate']) $lstrSelect .= " AND (a.DateTime) <='$examtodate'";
		if($larrformData['Appliedfrom']) $lstrSelect .= " AND date(a.UpdDate) >='$appfromdate'";
        if($larrformData['Appliedto']) $lstrSelect .= " AND date(a.UpdDate) <='$apptodate'";
		if($larrformData['Venues']) $lstrSelect .= " AND b.idcenter =  ".$venue;
		if($larrformData['Coursename']) $lstrSelect .= " AND c.IdProgrammaster = ".$course;
		if($larrformData['Studentname']) $lstrSelect.= " AND a.FName like '%$name%'"; 
		if($larrformData['ICNO']) $lstrSelect.= " AND a.ICNO like '%$icno%'";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	    return $larrResult;
	}
	
	public function fnajaxgetcenternames($ldfromdate,$ldtodate){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
									 ->join(array("b"=>"tbl_venuedateschedule"),"a.idcenter = b.idvenue",array(""))
									 ->where("b.Allotedseats > 0")
									 ->where("b.Active = 1")
									 ->where("b.date >= '$ldfromdate'")
									 ->where("b.date <= '$ldtodate'")
									 ->group("a.idcenter")
									 ->order("a.centername"); 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
	public function fngetcentername($centid)
	{ 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Select centername,idcenter
		               from tbl_center
		               where idcenter = '$centid';";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
		return $larrResult;
	}
	public function fngetprgrname($centid)
	{ 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Select ProgramName,IdProgrammaster
		               from tbl_programmaster
		               where IdProgrammaster = '$centid';";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
		return $larrResult;
	}
	public function fngetprogramnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	public function fngetcenternames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername")) 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
}