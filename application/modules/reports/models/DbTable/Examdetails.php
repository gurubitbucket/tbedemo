<?php
     class  Reports_Model_DbTable_Examdetails extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	

	  
      public function fngetsearchdetails($larrformData){
				
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.FName as Studentname","a.IDApplication","DATE_FORMAT(a.DateTime,'%d-%m-%Y') as ExamDate","a.DateTime as exdate","a.Payment","a.pass","count(a.IDApplication) as Registered"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.idcenter","b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName","c.IdProgrammaster"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.idmangesession","d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue != 000")
							 ->where("a.DateTime != 0")
							 ->where("a.IDApplication > 1148");
				if(isset($larrformData['field8']) && !empty($larrformData['field8']) ){
					$lstrSelect = $lstrSelect->where("a.Program = ?",$larrformData['field8']);
				  }	

				  if(isset($larrformData['field5']) && !empty($larrformData['field5']) ){
					$lstrSelect = $lstrSelect->where("b.idcenter = ?",$larrformData['field5']);
				  }	
			
				
				if(isset($larrformData['FromDate']) && !empty($larrformData['FromDate']) && isset($larrformData['ToDate']) && !empty($larrformData['ToDate'])){
					$lstrFromDate = date("Y-m-d",strtotime($larrformData['FromDate']));
					$lstrToDate = date("Y-m-d",strtotime($larrformData['ToDate']));
					$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$lstrFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$lstrToDate'");
					
			    }
				
				 if(isset($larrformData['RegFromDate']) && !empty($larrformData['RegFromDate']) && isset($larrformData['RegToDate']) && !empty($larrformData['RegToDate'])){
					$lstrFromDate = date("Y-m-d",strtotime($larrformData['RegFromDate']));
					$lstrToDate = date("Y-m-d",strtotime($larrformData['RegToDate']));
					$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$lstrFromDate' and date(a.UpdDate)<='$lstrToDate'");
					
			    }		
				/*if($larrformData['field10']){
					$lstrSelect  ->  where( "a.DateTime = '$edate'" );
				}	*/				  
				$lstrSelect  -> group("b.idcenter")
							 ->group("c.IdProgrammaster")
							 ->group("d.idmangesession")
							 ->group("a.DateTime")
							 ->order("b.centername")
							 ->order("a.DateTime")
							 ->order("c.IdProgrammaster")
							 ->order("d.starttime");
			//echo $lstrSelect;die();				 
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
	  
      	public function fnGetRegistered($idcenter,$idprogram,$idsession,$exdate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("IF(a.Payment='1','Paid','Pending ') as PaymentStatus","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							// ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("f" => "tbl_studentpaymentoption"),'a.IDApplication = f.IDApplication',array("f.ModeofPayment"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 // ->where("a.Payment = 1")
							  ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
        public function fnGetAllRegisteredDtls($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
        	
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("IF(a.Payment='1','Paid','Pending ') as PaymentStatus","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"));
         			if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
					 }
	    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
						$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
					 }	 
					$lstrSelect->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Examvenue != 000")
							  ->where("a.IDApplication > 1148")
							 // ->where("a.Payment = 1")
							  ->order("a.FName")
							  ->order("a.UpdDate");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
	     public function fnGetAllBatchtakfulRegStudents($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							->from(array("a" => "tbl_studentapplication"),array("IF(a.Payment='1','Paid','Pending ') as PaymentStatus","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"));

	    		   if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
					 }
	    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
						$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
					 }		 
							 
				$lstrSelect	->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							  ->where("a.Payment = 1")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Examvenue != 000")
							 ->where("a.IDApplication > 1148")
							 ->where("a.Amount = 0")
							 // ->where("a.batchpayment != 0")
							  //->group("a.IDApplication")
							 ->order("a.FName")
							 ->order("a.UpdDate");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }  
	    
	    public function fnGetCountRegistered($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("COUNT(a.IDApplication) AS CountReg"));
				   	 if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
					 }
	    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
						$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
					 }
				$lstrSelect	 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Examvenue != 000")
							  ->where("a.IDApplication > 1148")
							  //->where("a.Payment = 1")
							   ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }
	    
    
      
       public function fnGetCountModeOfPaymenFPXtRegistered($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array(""))
							 ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("COUNT(b.ModeofPayment) AS FPX"));
       				if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
					 }
	    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
						$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
					 }	 
				$lstrSelect	 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.Payment = 1")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Examvenue != 000")
							 ->where("a.IDApplication > 1148")
							 ->where("b.companyflag = 0")
							  ->where("b.ModeofPayment = 1")
							 ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    } 
	   public function fnGetCountModeOfPaymenCreditCardtRegistered($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array(""))
							  ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("COUNT(b.ModeofPayment) AS CreditCard"));
					if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
					 }
	    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
						$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
					 }			  
				$lstrSelect	 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							  ->where("a.Payment = 1")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Examvenue != 000")
							  ->where("a.IDApplication > 1148")
							 ->where("b.companyflag = 0")
							 ->where("b.ModeofPayment = 2")
							 //->group("a.IDApplication") 
							 ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }       

	   public function fnGetCountModeOfPaymenMoneyOrderRegistered($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array(""))
							  ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("COUNT(b.ModeofPayment) AS MoneyOrder"));
					if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
					 }
	    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
						$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
					 }				  
							  
				$lstrSelect	 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.Payment = 1")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Examvenue != 000")
							 ->where("a.IDApplication > 1148")
							 ->where("b.companyflag = 0")
							 ->where("b.ModeofPayment = 5")
							  //->group("a.IDApplication") 
							 ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }  
	    
	    
	    
	   
	    
	      
	    public function fnGetCountModeOfPaymenPostalOrderRegistered($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array(""))
							  ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("COUNT(b.ModeofPayment) AS PostalOrder"));
							  
				   if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
					 }
	    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
						$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
					 }				  
							  
				$lstrSelect	 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							  ->where("a.Payment = 1")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Examvenue != 000")
							  ->where("a.IDApplication > 1148")
							 ->where("b.companyflag = 0")
							 ->where("b.ModeofPayment = 6")
							// ->group("a.IDApplication") 
							 ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }  
	    
	    public function fnGetCountModeOfPaymenCreditBankIBFIMRegistered($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array(""))
							  ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array("COUNT(b.ModeofPayment) AS CreditBankIBFIM"));
							  
					if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
					 }
	    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
						$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
					 }				  
							  
				$lstrSelect ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							  ->where("a.Payment = 1")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Examvenue != 000")
							 ->where("a.IDApplication > 1148")
							 ->where("b.companyflag = 0")
							 ->where("b.ModeofPayment = 7")
							  //->group("a.IDApplication")
							 ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }  
	    
	    public function fnGetCountBatchtakfulRegStudents($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("COUNT(a.IDApplication) AS BatchTakReg"));
						if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						   $lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
						 }
		    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
							$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
						 }		 
				$lstrSelect ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							  ->where("a.Payment = 1")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Examvenue != 000")
							 ->where("a.IDApplication > 1148")
							 ->where("a.Amount = 0")
							 // ->where("a.batchpayment != 0")
							  //->group("a.IDApplication")
							 ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }  
	    public function fnGetCountPendingdRegistered($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
			
	    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("COUNT(a.Payment = 0) AS Pending","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"));
	   					 if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						   $lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
						 }
		    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
							$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
						 }			 
				$lstrSelect	  ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Examvenue != 000")
							 ->where("a.IDApplication > 1148")
							 ->where("a.Payment = 0")
						     ->group("a.IDApplication") 
							 ->order("a.FName");
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }
	    
     
      public function fnGetPendingDetails($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("IF(a.Payment='1','Paid','Pending ') as PaymentStatus","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.PermAddressDetails","a.MobileNo","a.UpdDate"))
							// ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("f" => "tbl_studentpaymentoption"),'a.IDApplication = f.IDApplication',array("f.ModeofPayment"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"));
				if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
						   $lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
						 }
		    	if(!empty($RegFromDate) &&  !empty($RegToDate)){
							$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
						 }				 
							 
				$lstrSelect ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Examvenue != 000")
							 ->where("a.IDApplication > 1148")
							 ->where("a.Payment = 0")
							 ->group("a.IDApplication") 
							 ->order("a.FName")
							 ->order("a.UpdDate");
	
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
      
	    
        //Payment Mode Details
	   public function fnGetFpxpaymentmodedetails($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("IF(a.Payment='1','Paid','Pending ') as PaymentStatus","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							// ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("f" => "tbl_studentpaymentoption"),'a.IDApplication = f.IDApplication',array("f.ModeofPayment"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->join(array("g" => "tbl_registrationfpx"),'a.IDApplication = g.IDApplication',array("g.payerMailId","g.grossAmount AS PaidAmount","DATE_FORMAT(g.TxnDate,'%d-%m-%Y') as TrasactionDate","g.fpxTxnId AS TrasactionId"));
						if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
							$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
						 }
		    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
							$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
						 }								 
				 $lstrSelect->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Payment = 1")
							  ->where("a.IDApplication > 1148")
							 ->where("f.companyflag = 0")
							 ->where("f.ModeofPayment = 1")
							 ->where("g.paymentStatus = 1")
							 ->where("g.entryFrom = 1")
							 ->group("a.IDApplication")
							 ->order("a.FName")
							 ->order("a.UpdDate");
							 
					//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
      	public function fnGetCountPaidRegistered($idcenter,$idprogram,$idsession,$exdate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("COUNT(a.Payment) AS Paid","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Payment = 1")
							 ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }
	    
	     public function fnGetCountOthers($idcenter,$idprogram,$idsession,$exdate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("COUNT(a.IDApplication) AS Others"))
							  ->join(array("b" => "tbl_studentpaymentoption"),'a.IDApplication = b.IDApplication',array(""))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							  ->where("a.Payment = 1")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("b.companyflag = 0")
							 ->where("b.ModeofPayment != 1")
							 ->where("b.ModeofPayment != 2")
							 ->where("b.ModeofPayment != 5")
							 ->where("b.ModeofPayment != 6")
							 ->where("b.ModeofPayment != 7")
							 ->order("a.FName");
				//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	    }  
       public function fnGetCCpaymentmodedetails($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("IF(a.Payment='1','Paid','Pending ') as PaymentStatus","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							// ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("f" => "tbl_studentpaymentoption"),'a.IDApplication = f.IDApplication',array("f.ModeofPayment"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->join(array("g" => "tbl_paypaldetails"),'a.IDApplication = g.IDApplication',array("g.payerId","g.grossAmount AS PaidAmount","DATE_FORMAT(g.UpdDate,'%d-%m-%Y') as TrasactionDate","g.transactionId AS TrasactionId"));
						if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
							$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
						 }
		    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
							$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
						 }		 
							 
				$lstrSelect	 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Payment = 1")
							 ->where("a.IDApplication > 1148")
							 ->where("f.companyflag = 0")
							 ->where("f.ModeofPayment = 2")
							 ->where("g.paymentStatus = 1")
							  ->group("a.IDApplication") 
							 ->order("a.FName")
							 ->order("a.UpdDate");
					//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
       public function fnGetMOpaymentmodedetails($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("IF(a.Payment='1','Paid','Pending ') as PaymentStatus","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("f" => "tbl_studentpaymentoption"),'a.IDApplication = f.IDApplication',array("f.ModeofPayment"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->join(array("g" => "tbl_studentpaymentdetails"),'a.IDApplication = g.IDApplication',array("g.Amount AS PaidAmount","DATE_FORMAT(g.UpdDate,'%d-%m-%Y') as TrasactionDate","g.ChequeNo AS TrasactionId","DATE_FORMAT(g.ChequeDt,'%d-%m-%Y') as ChequeDate"));
						if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
							$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
						 }
		    			if(!empty($RegFromDate) &&  !empty($RegToDate)){
							$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
						 }		 
							 
				$lstrSelect	 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Payment = 1")
							 ->where("a.IDApplication > 1148")
							 ->where("f.companyflag = 0")
							 ->where("g.companyflag = 0")
							 ->where("f.ModeofPayment = 5")
							 ->group("a.IDApplication") 
							 ->order("a.FName")
							 ->order("a.UpdDate");
							 //->group("a.FName");
					//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
       public function fnGetPOpaymentmodedetails($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("IF(a.Payment='1','Paid','Pending ') as PaymentStatus","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("f" => "tbl_studentpaymentoption"),'a.IDApplication = f.IDApplication',array("f.ModeofPayment"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->join(array("g" => "tbl_studentpaymentdetails"),'a.IDApplication = g.IDApplication',array("g.Amount AS PaidAmount","DATE_FORMAT(g.UpdDate,'%d-%m-%Y') as TrasactionDate","g.ChequeNo AS TrasactionId","DATE_FORMAT(g.ChequeDt,'%d-%m-%Y') as ChequeDate"));
					  if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
							$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
						 }
		    		  if(!empty($RegFromDate) &&  !empty($RegToDate)){
							$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
						 }	
							 
							 
			 $lstrSelect	 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Payment = 1")
							 ->where("a.IDApplication > 1148")
							 ->where("f.companyflag = 0")
							 ->where("g.companyflag = 0")
							 ->where("f.ModeofPayment = 6")
							 ->group("a.IDApplication") 
							 ->order("a.FName")
							 ->order("a.UpdDate");
							 //->group("a.FName");
					//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
       public function fnGetCreditBankpaymentmodedetails($idcenter,$idprogram,$idsession,$exdate,$ExamFromDate,$ExamToDate,$RegFromDate,$RegToDate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("IF(a.Payment='1','Paid','Pending ') as PaymentStatus","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime","a.UpdDate"))
							 ->join(array("f" => "tbl_studentpaymentoption"),'a.IDApplication = f.IDApplication',array("f.ModeofPayment"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->join(array("g" => "tbl_studentpaymentdetails"),'a.IDApplication = g.IDApplication',array("g.Amount AS PaidAmount","DATE_FORMAT(g.UpdDate,'%d-%m-%Y') as TrasactionDate","g.ChequeNo AS TrasactionId","DATE_FORMAT(g.ChequeDt,'%d-%m-%Y') as ChequeDate"));
       				 if(!empty($ExamFromDate) &&  !empty($ExamToDate)){
							$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d')>='$ExamFromDate' and DATE_FORMAT(a.DateTime,'%Y-%m-%d')<= '$ExamToDate'");
						 }
		    		  if(!empty($RegFromDate) &&  !empty($RegToDate)){
							$lstrSelect = $lstrSelect->where("date(a.UpdDate) >= '$RegFromDate' and date(a.UpdDate)<='$RegToDate'");
						 }	 
							 
				$lstrSelect ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.Payment = 1")
							 ->where("a.IDApplication > 1148")
							 ->where("f.companyflag = 0")
							 ->where("g.companyflag = 0")
							 ->where("f.ModeofPayment = 7")
							 ->group("a.IDApplication") 
							 ->order("a.FName")
							 ->order("a.UpdDate");
							 //->group("a.FName");
					//echo $lstrSelect;die();
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
     
      	public function fnGetVenueList($examdate){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array(""))
							 	 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("key"=>"b.idcenter","value"=>"b.centername"))
				 				  ->where("a.DateTime = '$examdate'")
				 				 ->where("a.Payment = 1")
				 				 ->group("b.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		
		
	
	    
 }    
		  	 
	
