<?php
class Reports_Model_DbTable_Examcategorystatistics extends Zend_Db_Table { 
	
	public function fngetprograms($lintmon,$lintyear){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT count(a.IDApplication) as NoOfCandidates,if(a.Program=1,'Part A and B',if(a.Program=2,'Part A and C',if(a.Program=3,'Part B','Part C'))) as Program
 					   FROM tbl_studentapplication a ,tbl_newscheduler b
 					   WHERE  a.Year = b.idnewscheduler and a.Exammonth = $lintmon  and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148
 					   GROUP BY a.Program";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetprogramresult($lintmon,$lintyear,$lintiddeftype){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.pass 
		               FROM  tbl_studentapplication a ,tbl_newscheduler b
		               WHERE a.Year = b.idnewscheduler and a.pass!=3 and a.Exammonth= $lintmon and a.Payment = 1 and a.Program = $lintiddeftype and b.Year = $lintyear and Examvenue <> 000 and a.IDApplication>1148";
		//echo $lstrselect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	public function fngetprogramnames(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT IdProgrammaster,ProgramName
 					   FROM  tbl_programmaster
					   ";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
}