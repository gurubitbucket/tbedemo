<?php
	class Reports_Model_DbTable_BatchExamreport extends Zend_Db_Table {
		public function fngetprogramnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   public function fngetcompanynames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"CompanyName")) 				 
				 				 ->order("a.CompanyName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   public function fngetcenternames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername")) 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   public function fngettakafulnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 				 
				 				 ->order("a.TakafulName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	  public function fnAttended($Regid,$part)
       {
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    		 $sql = "Select count(idquestions) from tbl_questions where idquestions  in (
					SELECT  QuestionNo FROM tbl_answerdetails  where Regid = '$Regid'  and answer in(SELECT idanswers FROM tbl_answers where CorrectAnswer =1))
					and QuestionGroup = '$part'";
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
	  public function fngetmonthofdate($yearss,$monthss)
       {
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	     $sql = "Select  *  from tbl_newscheduler where year=$yearss"; 
    		 $result = $db->fetchAll($sql);    
			 return $result;
       }
	 public function fnGetCityList()
       {
			$lintidcountry = 121;
       	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_city"),array("key"=>"a.idCity","value"=>"CityName"))
					 				 ->join(array("b"=>"tbl_state"),'a.idState = b.idState',array())
					 				 ->where("b.idCountry = ?",$lintidcountry)
					 				 ->order("a.CityName");
    		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}       
	   public function fngettakafulcompanies($tid)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 		$lstrSelect = $lobjDbAdpt->select()
	   	                         ->from(array("a"=>"tbl_studentapplication"),array('a.Gender','a.IDApplication as IDApplication','a.FName as StudentName','year(curdate())-year(a.DateOfBirth) as Age','a.ICNO as ICNO',"DATE_FORMAT(CONCAT( IFNULL(e.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date"))
	   	                         ->join(array("b"=>"tbl_takafuloperator"),'b.idtakafuloperator= a.Takafuloperator',array("b.TakafulName as TakafulCompany"))
	   	                         ->join(array("c"=>"tbl_definationms"),'c.idDefinition = a.Qualification',array("c.DefinitionDesc as Qualification"))
	   	                         ->join(array("d"=>"tbl_definationms"),'d.idDefinition = a.Race',array("d.DefinitionDesc as Race"))
	   	                         ->join(array('e' => 'tbl_newscheduler'),'a.Year = e.idnewscheduler',array())
	   	                         ->join(array('g' => 'tbl_registereddetails'),'g.IDApplication = a.IDApplication',array('g.Regid as ExamNo'))
	   	                         ->join(array('f' => 'tbl_center'),'f.idcenter= a.Examvenue',array('f.centername as Venue'))
	   	                         ->where("b.idtakafuloperator= ?",$tid)
	   	                         ->where("c.idDefType=14")
	   	                         ->where("d.idDefType=13")
								 ->where("a.Payment=1")
	   	                         ->order("Date"); 
	   		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	    	return $larrResult;
	   }
		public function fnReportSearchDetails($larrformData)
		{ 
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","DATE_FORMAT(CONCAT( IFNULL(d.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date",'a.ICNO as ICNO','a.Pass as Result')) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_center'),'c.idcenter= a.Examvenue',array('c.centername as Venue'))			
			           ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler')
					   ->where("a.Payment=1");	
			if($larrformData['Venues']) $lstrSelect->where("c.idcenter = ?",$venue);
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName like '%' ? '%'",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename);
			
			$lstrSelect->order("Date");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
		public function fnReportSearchByState($larrformData)
		{ 
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['State']) $state = $larrformData['State'];
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","DATE_FORMAT(CONCAT( IFNULL(d.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date",'a.ICNO as ICNO','a.Pass as Result')) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_state'),'c.idState = a.ExamState',array('c.StateName as State'))			
			                ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler')
					->where("a.Payment=1");
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName like '%' ? '%'",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename); 
			if($larrformData['State']) $lstrSelect->where("c.StateName like '%' ? '%'",$state);
			$lstrSelect->order("Date");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);	
		    return $larrResult;              
		}
		public function fnReportSearchByTakaful($larrformData)
		{ 
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Takafulname']) $Takafulname = $larrformData['Takafulname'];
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","DATE_FORMAT(CONCAT( IFNULL(d.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date",'a.ICNO as ICNO','a.Pass as Result')) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_takafuloperator'),'c.idtakafuloperator = a.Takafuloperator',array('c.TakafulName as TakafulName'))			
			           ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler')
					   ->where("a.Payment=1");
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName like '%' ? '%'",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename); 
			if($larrformData['Takafulname']) $lstrSelect->where("c.idtakafuloperator= ?",$Takafulname);
			$lstrSelect->order("Date");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	  public function fngetstudentpaymentdetails($larrformData)
		{ 
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = " select std. IDApplication,std.fname,icno,prg.programname,cnt.centername,date_format(concat(convert(sch.Year using utf8),'-',std.Exammonth,'-',std.Examdate),'%d-%m-%y') AS 'ExamDate1',std.`Amount` as Amount,rgd.Regid,std.pass as result
							from tbl_studentapplication std
							left outer join tbl_registereddetails rgd on std.idapplication = rgd.idapplication
							left outer join tbl_programmaster prg on std.program = prg.idprogrammaster
							left outer join tbl_center cnt on std.examvenue = cnt.idcenter
							left join tbl_newscheduler sch on(std.Year = sch.idnewscheduler)							
							where std.examvenue <> '000' and std.idapplication>1148 and std.batchpayment=0 and std.`Payment`=1";
			if($larrformData['Studentname'])	$lstrSelect .= " AND std.fname like '%$name%'";
			if($larrformData['ICNO'])           $lstrSelect .= " AND std.icno like '%$icno%'";
			$lstrSelect .= " GROUP BY std.IDApplication
			                 ORDER BY ExamDate1";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	public function fngetcompanypaymentdetails($larrformData)
		{ 
			if($larrformData['Companyname']) $cname=$larrformData['Companyname'];
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = " select std.IDApplication,comp.IdCompany as IdCompany,comp.CompanyName as CompanyName,batch.idBatchRegistration as idBatchRegistration,batch.registrationPin as RegistrationPIN,batch.totalNoofCandidates as NoOfStudents,batch.totalAmount as Amount
							from  tbl_companies comp
                                  left outer join tbl_batchregistration batch on comp.IdCompany = batch.idCompany
                                  left outer join tbl_studentpaymentoption pay on pay.IDApplication = batch.idBatchRegistration
							      left outer join tbl_registereddetails reg on reg.RegistrationPin = batch.registrationPin 							
								  left outer join tbl_studentapplication std on std.idapplication = reg.idapplication 
                      		where pay.companyflag=1 and std.idapplication > 1148 and std.batchpayment =1 and std.payment =1 and std.examvenue <> '000'";
		    if($larrformData['Companyname'])  $lstrSelect .= " AND comp.IdCompany = '%$cname%'";
			$lstrSelect .= " group by idBatchRegistration
			                 ORDER BY IdCompany";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	public function fngettakafulpaymentdetails($larrformData)
		{ 
			if($larrformData['Takafulname']) $tname=$larrformData['Takafulname'];
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = " select std.IDApplication,std.fname,icno,prg.programname,cnt.centername,date_format(concat(convert(sch.Year using utf8),'-',std.Exammonth,'-',std.Examdate),'%d-%m-%y') AS 'ExamDate1',std.`Amount` as Amount,rgd.RegistrationPin,std.pass as result,tak.TakafulName
							from tbl_studentapplication std
							left outer join tbl_takafuloperator  tak on std.Takafuloperator = tak.idtakafuloperator
							left outer join tbl_batchregistration batch on tak.idtakafuloperator = batch.idCompany
							left outer join tbl_studentpaymentoption pay on pay.IDApplication = std.IDApplication
							left outer join tbl_registereddetails rgd on std.idapplication = rgd.idapplication
							left join tbl_newscheduler sch on(std.Year = sch.idnewscheduler)							
							where pay.companyflag=2 and std.examvenue <> '000' and std.idapplication>1148 and std.batchpayment=2 and std.`Payment`=1 ";
			if($larrformData['Takafulname']) $lstrSelect .= " AND tak.idtakafuloperator = '%$cname%'";
			$lstrSelect .= " GROUP BY std.IDApplication
			                 ORDER BY ExamDate1";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	public function fngetexamdetails($larrformData)
		{
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
	   	                         ->from(array("a"=>"tbl_studentapplication"),array('a.Gender','a.IDApplication as IDApplication','a.FName as StudentName','year(curdate())-year(a.DateOfBirth) as Age','a.ICNO as ICNO',"DATE_FORMAT(CONCAT( IFNULL(e.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date",'a.Pass as Result','a.EmailAddress as email','a.Takafuloperator as Takafuloperator'))
	   	                         ->joinleft(array("b"=>"tbl_takafuloperator"),'b.idtakafuloperator= a.Takafuloperator',array("b.TakafulName as TakafulCompany"))
	   	                         ->join(array("c"=>"tbl_definationms"),'c.idDefinition = a.Qualification',array("c.DefinitionDesc as Qualification"))
	   	                         ->join(array("d"=>"tbl_definationms"),'d.idDefinition = a.Race',array("d.DefinitionDesc as Race"))
	   	                         ->join(array('e' => 'tbl_newscheduler'),'a.Year = e.idnewscheduler',array())
	   	                         ->join(array('f' => 'tbl_center'),'f.idcenter= a.Examvenue',array('f.centername as Venue'))
	   	                         ->join(array('h' => 'tbl_programmaster'),'h.IdProgrammaster = a.Program',array('h.ProgramName as Coursename'))
	   	                         ->where("c.idDefType=14")
	   	                         ->where("d.idDefType=13")
	   	                         ->where("a.Payment=1");
	   	    if($larrformData['Venues']) $lstrSelect->where("f.idcenter = ?",$venue);
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName like '%' ? '%'",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("h.IdProgrammaster = ?",$Coursename); 
			$lstrSelect->order("Date");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	public function fngetnoprogramdetails($larrformData)
		{
			if($larrformData['Month']) $month = $larrformData['Month'];
			if($larrformData['Year'])  $year = $larrformData['Year'];
			if($larrformData['Day']) $day = $larrformData['Day'];	
			if($larrformData['Month1']) $month1 = $larrformData['Month1'];
			if($larrformData['Year1'])  $year1 = $larrformData['Year1'];
			if($larrformData['Day1']) $day1 = $larrformData['Day1'];
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			if($larrformData['Date'])   $date1 =$larrformData['Date'];
			if($larrformData['Date2'])   $date2 =$larrformData['Date2'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
	   	                         ->from(array("a"=>"tbl_studentapplication"),array('a.Gender','a.IDApplication as IDApplication','a.FName as StudentName','year(curdate())-year(a.DateOfBirth) as Age','a.ICNO as ICNO',"DATE_FORMAT(CONCAT( IFNULL(e.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date",'a.Pass as Result','a.EmailAddress as email'))
	   	                       	 ->joinleft(array("b"=>"tbl_takafuloperator"),'b.idtakafuloperator= a.Takafuloperator',array("b.TakafulName as TakafulCompany"))
	   	                         ->join(array("c"=>"tbl_definationms"),'c.idDefinition = a.Qualification',array("c.DefinitionDesc as Qualification"))
	   	                         ->join(array("d"=>"tbl_definationms"),'d.idDefinition = a.Race',array("d.DefinitionDesc as Race"))
	   	                         ->join(array('e' => 'tbl_newscheduler'),'a.Year = e.idnewscheduler',array())
	   	                         ->join(array('f' => 'tbl_center'),'f.idcenter= a.Examvenue',array('f.centername as Venue'))
	   	                         ->join(array('g' => 'tbl_registereddetails'),'g.IDApplication = a.IDApplication',array('g.IdBatch as batch'))
                                                                  ->join(array('k' => 'tbl_answerdetails'),'g.Regid= k.Regid',array('k.Regid as ExamNo'))
	   	                         ->join(array('h' => 'tbl_programmaster'),'h.IdProgrammaster = a.Program',array('h.ProgramName as Coursename'))
	   	                         ->join(array("i"=>"tbl_batchmaster"),'i.IdBatch= a.IdBatch',array(""))
	   	                         ->join(array("j"=>"tbl_batchdetail"),'i.IdBatch= j.IdBatch',array("IFNULL(GROUP_CONCAT(cast(CONCAT(j.IdPart)as char)SEPARATOR ','),'')as IdPart","j.IdBatchDetail as BatchDetailsid"))
	   	                         ->where("c.idDefType=14")
	   	                         ->where("d.idDefType=13")
	   	                         ->where("a.Payment=1");
	   	    if($larrformData['Venues']) $lstrSelect->where("f.idcenter = ?",$venue);
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName like '%' ? '%'",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("h.IdProgrammaster = ?",$Coursename); 
			if($larrformData['Date'])   $lstrSelect .= " AND a.DateTime  >=  '$date1' ";
			if($larrformData['Date2'])   $lstrSelect .= " AND a.DateTime  <=  '$date2' ";
		     $lstrSelect .= " GROUP BY a.IDApplication
		                      ORDER BY BatchDetailsid";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	public function fngetsummarydetailsapplication($larrformData)
		{
			if($larrformData['UpDate1']) $update1 = $larrformData['UpDate1'];
			if($larrformData['UpDate2']) $update2 = $larrformData['UpDate2'];
			if($larrformData['Month']) $month = $larrformData['Month'];
			if($larrformData['Year'])  $year = $larrformData['Year'];
			if($larrformData['Day']) $day = $larrformData['Day'];	
			if($larrformData['Month1']) $month1 = $larrformData['Month1'];
			if($larrformData['Year1'])  $year1 = $larrformData['Year1'];
			if($larrformData['Day1']) $day1 = $larrformData['Day1'];
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			if($larrformData['Takafulname']) $tname = $larrformData['Takafulname'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
	   	                         ->from(array("a"=>"tbl_studentapplication"),array('a.Gender','a.IDApplication as IDApplication','a.FName as StudentName','year(curdate())-year(a.DateOfBirth) as Age','a.ICNO as ICNO',"DATE_FORMAT(CONCAT( IFNULL(e.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date",'a.Pass as Result','a.EmailAddress as email','a.Payment as payment',"DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as Applieddate"))
	   	                         ->joinleft(array("b"=>"tbl_takafuloperator"),'b.idtakafuloperator= a.Takafuloperator',array("b.TakafulName as TakafulCompany"))
	   	                         ->join(array("c"=>"tbl_definationms"),'c.idDefinition = a.Qualification',array("c.DefinitionDesc as Qualification"))
	   	                         ->join(array("d"=>"tbl_definationms"),'d.idDefinition = a.Race',array("d.DefinitionDesc as Race"))
	   	                         ->join(array('e' => 'tbl_newscheduler'),'a.Year = e.idnewscheduler',array())
	   	                         ->join(array('f' => 'tbl_center'),'f.idcenter= a.Examvenue',array('f.centername as Venue'))
	   	                         ->join(array('h' => 'tbl_programmaster'),'h.IdProgrammaster = a.Program',array('h.ProgramName as Coursename'))
	   	                         ->joinLeft(array('g' => 'tbl_registereddetails'),'g.IDApplication = a.IDApplication',array('g.Regid as ExamNo','g.IdBatch as batch','g.Approved as approval'))
	   	                         ->join(array('i' => 'tbl_studentpaymentoption'),'a.IDApplication =i.IDApplication',array('i.ModeofPayment as paymentmode'))
	   	                         ->where("c.idDefType=14")
	   	                         ->where("i.companyflag=0")
	   	                         ->where("d.idDefType=13");
	   	                          ;  
	   	    if($larrformData['Venues']) $lstrSelect->where("f.idcenter = ?",$venue);
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName like '%' ? '%'",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("h.IdProgrammaster = ?",$Coursename); 
			if($larrformData['Takafulname']) $lstrSelect->where("b.idtakafuloperator = ?",$tname);
			 if($larrformData['Day'])   $lstrSelect .= " AND a.Examdate >=  ".$day;
		     if($larrformData['Month']) $lstrSelect .= " AND a.Exammonth >=  ".$month;
		     if($larrformData['Year'])  $lstrSelect .= " AND e.Year >=  ".$year;
		     if($larrformData['Day1']) $lstrSelect .= " AND a.Examdate <=  ".$day1;
		     if($larrformData['Month1']) $lstrSelect .= " AND a.Exammonth <=  ".$month1;
		     if($larrformData['Year1'])  $lstrSelect .= " AND e.Year <=  ".$year1;
		     if($larrformData['UpDate1']) $lstrSelect .= " AND date(a.UpdDate) >=  '$update1'";
			 if($larrformData['UpDate2']) $lstrSelect .= " AND date(a.UpdDate) <=  '$update2'";
			  $lstrSelect .= " GROUP BY a.IDApplication";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	public function fngetvisitorsdetails($larrformData)
		{
			if($larrformData['UpDate1']) $update1 = $larrformData['UpDate1'];
			if($larrformData['UpDate2']) $update2 = $larrformData['UpDate2'];
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
	   	                         ->from(array("a"=>"tbl_studentapplication"),array('a.Gender','a.IDApplication as IDApplication','a.FName as StudentName','year(curdate())-year(a.DateOfBirth) as Age','a.ICNO as ICNO',"DATE_FORMAT(CONCAT( IFNULL(e.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date",'a.Pass as Result','a.EmailAddress as email','a.Payment as payment','a.UpdDate as Applieddate'))
	   	                         ->joinleft(array("b"=>"tbl_takafuloperator"),'b.idtakafuloperator= a.Takafuloperator',array("b.TakafulName as TakafulCompany"))
	   	                         ->join(array("c"=>"tbl_definationms"),'c.idDefinition = a.Qualification',array("c.DefinitionDesc as Qualification"))
	   	                         ->join(array("d"=>"tbl_definationms"),'d.idDefinition = a.Race',array("d.DefinitionDesc as Race"))
	   	                         ->join(array('e' => 'tbl_newscheduler'),'a.Year = e.idnewscheduler',array())
	   	                         ->join(array('f' => 'tbl_center'),'f.idcenter= a.Examvenue',array('f.centername as Venue'))
	   	                         ->join(array('h' => 'tbl_programmaster'),'h.IdProgrammaster = a.Program',array('h.ProgramName as Coursename'))
	   	                         ->join(array('i' => 'tbl_studentpaymentoption'),'a.IDApplication =i.IDApplication',array('i.ModeofPayment as paymentmode'))
	   	                         ->where("c.idDefType=14")
	   	                         ->where("i.companyflag=0")
	   	                         ->where("d.idDefType=13");
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName like '%' ? '%'",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$icno);
		    if($larrformData['UpDate1']) $lstrSelect .= " AND date(a.UpdDate) >=  '$update1'";
			if($larrformData['UpDate2']) $lstrSelect .= " AND date(a.UpdDate) <=  '$update2'";
			$lstrSelect .= " GROUP BY a.IDApplication";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
   public function fngetallsummarydetails($larrformData)
   {
   	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	   		if($larrformData['Applicationtype']) $Applicationtype = $larrformData['Applicationtype'];
			if($larrformData['Approvaltype']) $Approvaltype = $larrformData['Approvaltype'];   
   	        if($larrformData['UpDate1']) $update1 = $larrformData['UpDate1'];
			if($larrformData['UpDate2']) $update2 = $larrformData['UpDate2'];
			if($larrformData['Month']) $month = $larrformData['Month'];
			if($larrformData['Year'])  $year = $larrformData['Year'];
			if($larrformData['Day']) $day = $larrformData['Day'];	
			if($larrformData['Month1']) $month1 = $larrformData['Month1'];
			if($larrformData['Year1'])  $year1 = $larrformData['Year1'];
			if($larrformData['Day1']) $day1 = $larrformData['Day1'];
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			if($larrformData['Companyname']) $cname = $larrformData['Companyname'];
			if($larrformData['Takafulname']) $tname = $larrformData['Takafulname'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select * from   v_summaryreport4";
			$lstrSelect .= " Where 1 ";
			if($larrformData['Day']) $lstrSelect .= " AND Examdate >=  ".$day;
		    if($larrformData['Month']) $lstrSelect .= " AND Exammonth >=  ".$month;
		    if($larrformData['Year'])  $lstrSelect .= " AND Year >=  ".$year;
		    if($larrformData['Day1']) $lstrSelect .= " AND Examdate <=  ".$day1;
		    if($larrformData['Month1']) $lstrSelect .= " AND Exammonth <=  ".$month1;
		    if($larrformData['Year1'])  $lstrSelect .= " AND Year <=  ".$year1;
		    if($larrformData['Venues']) $lstrSelect .= " AND idcenter = $venue";
			if($larrformData['Studentname'])	$lstrSelect .= " AND FName like '%$name%'";
			if($larrformData['ICNO']) $lstrSelect .= " AND ICNO like '%$icno%'";
			if($larrformData['Coursename']) $lstrSelect .= " AND IdProgrammaster = $Coursename"; 
			if($larrformData['Takafulname'])$lstrSelect .= " AND idtakafuloperator = $tname";
			if($larrformData['UpDate1']) $lstrSelect .= " AND update3 >=  '$update1'";
			if($larrformData['UpDate2']) $lstrSelect .= " AND update3 <=  '$update2'";
			if($larrformData['Applicationtype']==2) $lstrSelect .= " AND batchpayment=0";
			if($larrformData['Applicationtype']==3) $lstrSelect .= " AND batchpayment=1";
			if($larrformData['Applicationtype']==4) $lstrSelect .= " AND batchpayment=2";
			if($larrformData['Approvaltype']==2) $lstrSelect .= " AND ApprovalStatus='Approved'";
			if($larrformData['Approvaltype']==3) $lstrSelect .= " AND  ApprovalStatus='UnApproved'";
		    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;  
   }
	public function fngetavailableseats($larrformData)
	{
        	if($larrformData['Month']) $month = $larrformData['Month'];
			if($larrformData['Year'])  $year = $larrformData['Year'];
			if($larrformData['Day']) $day = $larrformData['Day'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " SELECT b.NumberofSeat, b.centername,b.idcenter, d.managesessionname, d.starttime, d.endtime,d.idmangesession, (b.NumberofSeat - IFNULL( count( a.IDApplication ) , 0 )) AS rem
                            FROM `tbl_studentapplication` a, tbl_center b, tbl_managesession d,tbl_newscheduler m
							WHERE a.Examvenue = b.idcenter
							AND a.Examdate = $day
							AND a.Exammonth = $month
							AND m.idnewscheduler =6
							AND m.Year = $year
							AND b.Active =1
							AND d.idmangesession = a.Examsession
							GROUP BY a.Examvenue, a.Examsession
							UNION
							SELECT b.NumberofSeat, b.centername,b.idcenter, j.managesessionname, j.starttime, j.endtime, j.idmangesession, b.NumberofSeat AS rem
							FROM tbl_center b,tbl_managesession e,`tbl_studentapplication` c,tbl_newschedulersession f, tbl_managesession j,tbl_newscheduler m
							WHERE f.idmanagesession NOT
							IN (
									SELECT b.Examsession
									FROM tbl_studentapplication b
									WHERE b.Examvenue = c.Examvenue
											AND b.Examdate = $day
											AND b.Exammonth = $month
											AND m.idnewscheduler =6
											AND m.Year = $year
											AND b.Examvenue !=000
											AND b.Examsession !=000
											GROUP BY b.Examvenue, b.Examsession
								)
							AND c.Examdate = $day
							AND c.Exammonth = $month
							AND m.idnewscheduler =6
							AND m.Year = $year
							AND c.Examvenue !=000
							AND f.idnewscheduler =6
							AND f.idmanagesession=j.idmangesession
							AND c.Examvenue = b.idcenter
							UNION
							SELECT b.NumberofSeat, b.centername,b.idcenter, e.managesessionname, e.starttime, e.endtime,e.idmangesession, b.NumberofSeat AS rem
							FROM tbl_center b, tbl_newschedulervenue c, tbl_managesession e
							WHERE b.idcenter NOT
								IN (
										SELECT Examvenue
										FROM tbl_studentapplication
										WHERE Examdate = $day
												AND Exammonth = $month
												AND Year = 6
												AND Examvenue !=000
												AND Examsession !=000
												GROUP BY Examvenue, Examsession
									)
									AND b.Active =1
									AND  c.idvenue = b.idcenter
									AND e.idmangesession
									IN (
										SELECT h.idmanagesession
										FROM tbl_newschedulersession h
										WHERE idnewscheduler =6
									)";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult; 
        }
	public function fngetexamanddatedetails($larrformData)
		{ 
			if($larrformData['Date3']) $fromdate = $larrformData['Date3'];
            if($larrformData['Date4']) $todate = $larrformData['Date4'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			if($larrformData['Coursename']) $course = $larrformData['Coursename'];
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect =  "SELECT b.idcenter AS idcenter,a.IDApplication,DATE_FORMAT(a.DateTime,'%d-%m-%Y') as Date,group_concat( a.Pass, '' ) AS Result, d.ProgramName AS Course, b.centername AS Venue, a.Payment, count( 'a.IDApplication' ) AS applied
                            FROM `tbl_studentapplication` AS `a` , `tbl_center` AS `b` , `tbl_newscheduler` AS `c` , `tbl_programmaster` AS `d`
                            WHERE (a.Examvenue = b.idcenter) AND (a.Year = c.idnewscheduler) AND (a.Program = d.IdProgrammaster) AND a.Payment = '1'  AND 
                                   a.DateTime <= curdate() and a.IDApplication >1148 and a.Examvenue <>000"; 
		    if($larrformData['Date3']) $lstrSelect .= " AND a.DateTime >='$fromdate'";
            if($larrformData['Date4']) $lstrSelect .= " AND a.DateTime <='$todate'";
		    if($larrformData['Venues']) $lstrSelect .= " AND b.idcenter =  ".$venue;
		    if($larrformData['Coursename']) $lstrSelect .= " AND d.IdProgrammaster =  ".$course;
		    $lstrSelect .= " GROUP BY a.Program, a.Examvenue, Date having Date 
							 ORDER BY a.DateTime desc ,b.centername,d.IdProgrammaster";
		    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
		
		public function fnajaxgetcenternames($ldfromdate,$ldtodate){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
									 ->join(array("b"=>"tbl_venuedateschedule"),"a.idcenter = b.idvenue",array(""))
									 ->where("b.Allotedseats > 0")
									 ->where("b.Active = 1")
									 ->where("b.date >= '$ldfromdate'")
									 ->where("b.date <= '$ldtodate'")
									 ->group("a.idcenter")
									 ->order("a.centername"); 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
	public function fngetcentername($centid)
	{ 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Select centername,idcenter
		               from tbl_center
		               where idcenter = '$centid';";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
		return $larrResult;
	}
	
         public function fnReportSearchStatus($larrformData)
		{ 
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Result']) $Result = $larrformData['Result'];
			if($larrformData['Dates']) $fromdate = $larrformData['Dates'];
            if($larrformData['Dates2']) $todate = $larrformData['Dates2'];
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","DATE_FORMAT(CONCAT( IFNULL(d.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date",'a.ICNO as ICNO','a.Pass as Result')) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_center'),'c.idcenter= a.Examvenue',array('c.centername as Venue'))	
			           ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler')
			           ->where("a.IDApplication >1148")
			           ->where("a.Examvenue !=0");
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName like '%' ? '%'",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename); 
			if($larrformData['Result']) $lstrSelect->where("a.Pass= ?",$Result);
			if($larrformData['Dates']) $lstrSelect->where("a.DateTime >= '$fromdate'");//$lstrSelect .= " AND a.DateTime >=  '$fromdate'";
            if($larrformData['Dates2']) $lstrSelect->where("a.DateTime <= '$todate'");//$lstrSelect .= " AND a.DateTime <=  '$todate'";
            $lstrSelect->group("a.IDApplication");
			$lstrSelect->order("Date");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;           
		}
		
	public function fngetnewsummarydetailsapplication($larrformData)
		{
			if($larrformData['UpDate1']) $update1 = $larrformData['UpDate1'];
			if($larrformData['UpDate2']) $update2 = $larrformData['UpDate2'];
			if($larrformData['Dates']) $fromdate = $larrformData['Dates'];
			if($larrformData['Dates2']) $todate = $larrformData['Dates2'];
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			if($larrformData['Takafulname']) $tname = $larrformData['Takafulname'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	$lstrSelect = "select std.fname,icno,prg.programname,cnt.centername,tak.takafulname,qual.definitiondesc as 'Qualification',race.definitiondesc as 'Race',
						cast(((to_days(curdate()) - to_days(std.DateOfBirth)) / 365) as signed) AS 'age',
						if((std.Gender = 1),'Male','Female') AS 'Gender',std.username,
						date_format(concat(convert(sch.Year using utf8),'-',std.Exammonth,'-',std.Examdate),'%Y-%m-%d') AS 'ExamDate',
						date_format(concat(convert(sch.Year using utf8),'-',std.Exammonth,'-',std.Examdate),'%d-%m-%y') AS 'ExamDate1',
						concat(ssn.managesessionname,'(' ,ssn.starttime, ' - ', ssn.endtime, ')' )'Session',std.mobileno 'ContactNo',
						usr.loginname,pmt.upddate,if((paymode.modeofpayment=1),'FPX',if((paymode.modeofpayment=2),'Credit Card',if((paymode.modeofpayment=5),'Money Order', if((paymode.modeofpayment=6),'Postal Order',if((paymode.modeofpayment=7),'Credit/Bank to IBFIM account','others'))))) as Modeofpayment,
						if(rgd.approved=1,'Approved','Not Approved') as ApprovalStatus,
						if((std.Payment = 1),'Paid','Pending') AS 'PaymentStatus',date_format(Date(std.UpdDate),'%e-%m-%y') as Applieddate
						from tbl_studentapplication std
						left outer join tbl_programmaster prg on std.program = prg.idprogrammaster
						left outer join tbl_center cnt on std.examvenue = cnt.idcenter
						left outer join tbl_takafuloperator tak on std.takafuloperator = tak.idtakafuloperator
						left outer join tbl_definationms qual on std.qualification = qual.iddefinition
						left outer join tbl_definationms race on std.race = race.iddefinition
						left join tbl_newscheduler sch on((std.Year = sch.idnewscheduler))
						left outer join tbl_managesession ssn on std.examsession = ssn.idmangesession
						left outer join tbl_studentpaymentdetails pmt on std.idapplication = pmt.idapplication
						left outer join tbl_user usr on pmt.upduser = usr.iduser
						left outer join tbl_studentpaymentoption paymode on std.idapplication = paymode.idapplication
						left outer join tbl_registereddetails rgd on std.idapplication = rgd.idapplication
						where std.examvenue <> '000' and std.idapplication>1148"; 
			if($larrformData['Payment']==1){$lstrSelect .= " AND std.Payment = 1";}    
			if($larrformData['Payment']==2){$lstrSelect .= " AND std.Payment = 0";}   			         
	   	    if($larrformData['paymentmode']==1) {$lstrSelect .= " AND paymode.modeofpayment= 1";}
		 	if($larrformData['paymentmode']==2) {$lstrSelect .= " AND paymode.modeofpayment=2";}
			if($larrformData['paymentmode']==4){$lstrSelect .= " AND paymode.modeofpayment =4";}
			if($larrformData['paymentmode']==5){$lstrSelect .= " AND paymode.modeofpayment=5";} 
			if($larrformData['paymentmode']==6){$lstrSelect .= " AND paymode.modeofpayment =6";}
			if($larrformData['paymentmode']==7){$lstrSelect .= " AND paymode.modeofpayment=7";}
			if($larrformData['approval']==2){ $lstrSelect .= " AND rgd.approved=0";}
			if($larrformData['approval']==1){$lstrSelect .= " AND rgd.approved=1";}  
	   	    if($larrformData['Venues']) $lstrSelect .= " AND cnt.idcenter = $venue";
			if($larrformData['Studentname'])$lstrSelect .= " AND std.fname like '%$name%'";
			if($larrformData['ICNO']) $lstrSelect .= " AND std.icno like '%$icno%'";
			if($larrformData['Coursename'])$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			if($larrformData['Takafulname']) $lstrSelect .= " AND tak.idtakafuloperator = $tname";
		    if($larrformData['Dates']) $lstrSelect .= " AND std.DateTime >=  '$fromdate'";
			if($larrformData['Dates2']) $lstrSelect .= " AND std.DateTime <=  '$todate'";
		    if($larrformData['UpDate1']) $lstrSelect .= " AND Date(std.UpdDate) >=  '$update1'";
			if($larrformData['UpDate2']) $lstrSelect .= " AND Date(std.UpdDate) <=  '$update2'";
			$lstrSelect .= " GROUP BY std.IDApplication";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
  public function fngettheidparts($idbatchresult)
   {
   			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_batchdetail"),array("GROUP_CONCAT(IdPart SEPARATOR ',')as partsids")) 	
				 				 ->where("a.IdBatch=?",$idbatchresult)			 
				 				 ->group("a.IdBatch");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
   }
 public function fngetsummarydetails($larrformData)
		{
			if($larrformData['Date']) $fromdate = $larrformData['Date'];
            if($larrformData['Date2']) $todate = $larrformData['Date2'];
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) 		 $icno = $larrformData['ICNO'];
			if($larrformData['Coursename'])  $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) 	 $venue = $larrformData['Venues'];
			if($larrformData['Takafulname']) $tname = $larrformData['Takafulname'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   	    $lstrSelect = "select 'Individual',stdapp.IDApplication,FName,ICNO, cast(datediff(curdate(), DateOfBirth)/365 as signed) as age,
							prg.ProgramName,cnt.centername as ExamVenue,tak.TakafulName,Qual.DefinitionDesc as Qual,Race.DefinitionDesc as race, stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,
							if(Gender=1,'Male','Female') as Gender,ExamDate,concat(ExamDate,'/',ExamMonth,'/',Sch.Year) as ExamDate,
							if(payment =1,'Paid','Unpaid')'PaymentStatus',
							if(reg.approved=1,'Approved','UnApproved') as 'ApprovalStatus',
							if(pay.modeofpayment = 1,'FPX',if(pay.modeofPayment=2,'Credit Card',if(pay.modeofPayment=5,'Money Order',if(pay.modeofPayment=6,'Postal Order',if(pay.modeofPayment=7,'Order to Bank','Others'))))) as 'Payment Mode',
							marks.Grade as Grade,
 							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=4,'Absent','Applied'))) as 'Result'
							from tbl_studentapplication stdapp
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							left outer join tbl_takafuloperator tak on stdapp.takafuloperator = tak.idtakafuloperator
							left outer join tbl_definationms Qual on stdapp.Qualification = Qual.iddEfinition
							left outer join tbl_definationms Race on stdapp.Race = Race.idDefinition
							left outer join tbl_newscheduler Sch on stdapp.Year = Sch.idnewScheduler
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_studentpaymentoption pay on stdapp.IDApplication = pay.idapplication
                            left outer join tbl_studentmarks marks on stdapp.IDApplication = marks.IDApplication 
							where stdapp.IDApplication>1148 and stdapp.payment=1  and examvenue <> 000";   
	   	     if($larrformData['Venues']) $lstrSelect .= " AND cnt.idcenter = $venue";
			 if($larrformData['Studentname'])$lstrSelect .= " AND stdapp.fname like '%$name%'";
			 if($larrformData['ICNO']) $lstrSelect .= " AND stdapp.icno like '%$icno%'";
			 if($larrformData['Coursename'])$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			 if($larrformData['Takafulname']) $lstrSelect .= " AND tak.idtakafuloperator = $tname";
			 if($larrformData['Date']) $lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
             if($larrformData['Date2']) $lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
		     $lstrSelect .= " GROUP BY stdapp.IDApplication
			                  ORDER BY ExamDate"; 
		     //echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	
	
public function fngettransactiondetails($larrformData)
   {
   	
   	if($larrformData['UpDate1']) $update1 = $larrformData['UpDate1'];
	if($larrformData['UpDate2']) $update2 = $larrformData['UpDate2'];
	if($larrformData['Date']) $fromdate = $larrformData['Date'];
	if($larrformData['Date2']) $todate = $larrformData['Date2'];
	if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$lstrSelect = "SELECT txn_id as 'TransactionID', payer_email as 'CreditCardEmail', mc_gross as 'Amount', item_name as 'Description', substring(item_name,18,length(item_name)-17) as 'ApplicatoinID',stdapp.idapplication,stdapp.fname as 'Name', stdapp.program, prg.programname, DATE_FORMAT(Date(stdapp.upddate),'%d-%m-%Y') as 'ApplicationDate',DATE_FORMAT(datetime,'%d-%m-%Y') as 'ExamDate'   
   	               FROM orders ord left outer join tbl_studentapplication stdapp on substring(item_name,18,length(item_name)-17) = stdapp.idapplication
				        left outer join tbl_programmaster prg on stdapp.program = prg.idprogrammaster 
				   Where order_id >3";
   	if($larrformData['Date']) $lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
	if($larrformData['Date2']) $lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
	if($larrformData['UpDate1']) $lstrSelect .= " AND Date(stdapp.UpdDate) >=  '$update1'";
	if($larrformData['UpDate2']) $lstrSelect .= " AND Date(stdapp.UpdDate) <=  '$update2'";
	if($larrformData['Coursename'])$lstrSelect .= " AND prg.idprogrammaster = $Coursename";
	$lstrSelect .= " order by datetime,fname";
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult; 
   }
    public function fngetexamdetailsdate($larrformData)
		{
			if($larrformData['Date']) $fromdate = $larrformData['Date'];
			if($larrformData['Date2']) $todate = $larrformData['Date2'];
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
	   	                         ->from(array("a"=>"tbl_studentapplication"),array('a.Gender','a.IDApplication as IDApplication','a.FName as StudentName','year(curdate())-year(a.DateOfBirth) as Age','a.ICNO as ICNO',"DATE_FORMAT(a.DateTime,'%d-%m-%Y') AS Date",'a.Pass as Result','a.EmailAddress as email','a.Takafuloperator as Takafuloperator'))
	   	                         ->joinleft(array("b"=>"tbl_takafuloperator"),'b.idtakafuloperator= a.Takafuloperator',array("b.TakafulName as TakafulCompany"))
	   	                         ->join(array("c"=>"tbl_definationms"),'c.idDefinition = a.Qualification',array("c.DefinitionDesc as Qualification"))
	   	                         ->join(array("d"=>"tbl_definationms"),'d.idDefinition = a.Race',array("d.DefinitionDesc as Race"))
	   	                         ->join(array('f'=> 'tbl_center'),'f.idcenter= a.Examvenue',array('f.centername as Venue'))
	   	                         ->join(array('h'=> 'tbl_programmaster'),'h.IdProgrammaster = a.Program',array('h.ProgramName as Coursename'))
	   	                         ->join(array('i'=> 'tbl_studentmarks'),'a.IDApplication = i.IDApplication',array('i.Grade as Grade','i.Correct as Correct','i.Attended as Attended'))
	   	                         ->where("c.idDefType=14")
	   	                         ->where("d.idDefType=13")
	   	                         ->where("a.Payment=1")
	   	                         ->where("a.IDApplication >1148");
		     if($larrformData['Date']) $lstrSelect .= " AND a.DateTime >= '$fromdate'";
			 if($larrformData['Date2']) $lstrSelect .= " AND a.DateTime <= '$todate'";
	   	     if($larrformData['Venues']) $lstrSelect.= " AND f.idcenter = '$venue'";
			 if($larrformData['Studentname']) $lstrSelect.= " AND a.FName like '%$name%'"; 
			 if($larrformData['ICNO']) $lstrSelect.= " AND a.ICNO like '%$icno%'";
			 if($larrformData['Coursename']) $lstrSelect.= " AND h.IdProgrammaster = '$Coursename'";
			 $lstrSelect .= " GROUP BY a.IDApplication
			                  ORDER BY a.DateTime";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
			public function fngetnewpartwisedetails($larrformData)
			{
				if($larrformData['Date7']) $fromdate = $larrformData['Date7'];
				if($larrformData['Date8']) $todate = $larrformData['Date8'];
				if($larrformData['Canditatename']) $name = $larrformData['Canditatename'];
				if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
				if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
				if($larrformData['Venues']) $venue = $larrformData['Venues'];
			   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			   	$lstrSelect = " select    a.IDApplication,a.FName,b.ProgramName,b.IdProgrammaster,c.centername as ExamVenue,DATE_FORMAT (a.DateTime, '%d-%m-%Y') AS ExamDate, 
										  d.Grade as Grade,d.Correct as TotalMarks,if(a.pass=1,'Pass','Fail') as Result,e.A as partA,e.B as partB,e.C as partC,
									      concat(f.managesessionname,' (' ,f.starttime, ' - ', f.endtime, ')' ) as session
								from 	 tbl_studentapplication a
										 left outer join tbl_programmaster b on a.Program = b.IdProgrammaster
										 left outer join tbl_center c on a.examvenue = c.idcenter	 					
										 left outer join tbl_studentmarks d on a.IDApplication = d.IDApplication
										 left outer join tbl_studentdetailspartwisemarks e on a.IDApplication = e.IDApplication
									     left outer join tbl_managesession f on a.examsession = f.idmangesession
								where    a.IDApplication>1148 and examvenue <> 000 and a.Payment = 1 and a.pass in (1,2)";
			   	if($larrformData['Date7']) $lstrSelect .= " AND a.DateTime >=  '$fromdate'";
				if($larrformData['Date8']) $lstrSelect .= " AND a.DateTime <=  '$todate'";
				if($larrformData['Venues']) $lstrSelect.= " AND c.idcenter = '$venue'";
				if($larrformData['Canditatename'])	$lstrSelect.= " AND a.FName like '%$name%'"; 
				if($larrformData['ICNO']) $lstrSelect.= " AND a.ICNO like '%$icno%'";
				if($larrformData['Coursename']) $lstrSelect.= " AND b.IdProgrammaster = '$Coursename'";
	        $lstrSelect .= " GROUP BY a.IDApplication
			                 ORDER BY b.IdProgrammaster,a.DateTime desc,c.centername ";
	        //echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult; 
	}
	public function fngetstuddetails($lintIdapp){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect ="select a.IDApplication, a.FName, a.ICNO, cast(datediff(curdate(), a.DateOfBirth)/365 as signed) as age,
			                 e.DefinitionDesc as Qual,DATE_FORMAT(a.DateOfBirth,'%d-%m-%Y') as DateOfBirth, 
			                 a.EmailAddress as email,b.ProgramName,b.IdProgrammaster,c.centername as ExamVenue, 
			                 concat(f.managesessionname,' (' ,f.starttime, ' - ', f.endtime, ')' ) as session,
			                 DATE_FORMAT (a.DateTime, '%d-%m-%Y') AS ExamDate
					from 	 tbl_studentapplication a
					         left outer join tbl_programmaster b on a.Program = b.IdProgrammaster
							 left outer join tbl_center c on a.examvenue = c.idcenter	
							 left outer join tbl_takafuloperator d on a.Takafuloperator = d.idtakafuloperator
							 left outer join tbl_definationms e on a.Qualification = e.iddefinition
							 left outer join tbl_managesession f on a.examsession = f.idmangesession
					where    a.IDApplication = $lintIdapp";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult; 
	}
	public function fngetallnames ()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.FName","value"=>"a.FName"))
				 				 ->where("a.IDApplication > 1148") 				 
				 				 ->order("a.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fngetexamstartdetails($larrformData)
	{    
		$date = $larrformData['Date9'];
		$program = $larrformData['Course'];
		$venue = $larrformData['Venue'];
		$session = $larrformData['session'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = " SELECT  a.IDApplication,a.FName,if(a.pass=1,'Pass','Fail') as Result,b.NoofQtns as TotalQuestions,b.Attended as Attended,b.Correct,
							   (b.NoofQtns - b.Attended) as Unattended,(b.Attended -b.Correct) as Wrong,b.Grade,
							    c.Starttime,c.Endtime,if(c.Submittedby=1,'Candidate Submit',if(c.Submittedby=2,'Center Submit','Auto Submitted')) as Submittedby
						FROM   tbl_studentapplication a 
             				   left outer join tbl_studentmarks b on a.IDApplication = b.IDApplication
             				   left outer join tbl_studentstartexamdetails c on a.IDApplication = c.IDApplication
             				   left outer join tbl_managesession d on a.examsession = d.idmangesession
             				   left outer join tbl_programmaster e on a.Program = e.IdProgrammaster
							   left outer join tbl_center f on a.examvenue = f.idcenter
						WHERE  a.IDApplication>1148 and examvenue <> 000 and a.Payment = 1 and a.pass != 3 and a.DateTime = '$date' and f.idcenter = $venue and e.IdProgrammaster = $program and d.idmangesession = $session
						Group by a.IDApplication";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
	}
	public function fnattendedquestions($id)
    {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       $lstrSelect = $lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Answer"))
                             ->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid',array())
                             ->join(array("c" => "tbl_answers"),'a.Answer = c.idanswers',array("c.answers AS StudAns","c.idanswers as Studidanswers"))
                             ->join(array("d" => "tbl_questions"),'a.QuestionNo = d.idquestions',array("d.Question"))
                             ->where("b.IDApplication = $id");
   	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
   	   return $larrResult;
    }
	public function fndisplayquestions($id)
    {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       $lstrSelect = $lobjDbAdpt->select()
                                ->from(array("a" => "tbl_studentapplication"),array(""))
                                ->join(array("b" => "tbl_questionsetforstudents"),'a.IDApplication = b.IDApplication',array('b.idquestions'))
                                ->where("b.IDApplication = $id")
                                ->order("b.idquestionsetforstudents desc");
   	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
   	   return $larrResult;
    }
    public function fngetunattendedquestions($qid)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_questions"),array("TRIM(a.Question) AS Question"))
                             ->where("a.idquestions = $qid");
   	    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
   	    return $larrResult;
    }
	public function fnCorrectquestions($id)
    {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       $lstrSelect = $lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Answer"))
                             ->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid',array())
                             ->join(array("c" => "tbl_answers"),'a.Answer = c.idanswers',array("c.answers AS StudAns","c.idanswers as Studidanswers"))
                             ->join(array("d" => "tbl_questions"),'a.QuestionNo = d.idquestions',array("d.Question"))
                             ->where("c.CorrectAnswer = 1")
                             ->where("b.IDApplication = $id");
   	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
   	   return $larrResult;
    }
	public function fnwrongquestions($id)
    {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       $lstrSelect = $lstrSelect = $lobjDbAdpt->select()
	                             ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Answer"))
	                             ->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid',array())
	                             ->join(array("c" => "tbl_answers"),'a.Answer = c.idanswers',array("c.answers AS StudAns","c.idanswers as Studidanswers"))
	                             ->join(array("d" => "tbl_questions"),'a.QuestionNo = d.idquestions',array("d.Question"))
	                             ->where("c.CorrectAnswer = 0")
	                             ->where("b.IDApplication = $id");
   	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
   	   return $larrResult;
    }
	public function fngetanswers($qid)
   {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
	                             ->from(array("a" => "tbl_answers"),array("a.answers","a.idanswers"))
	                             ->where("a.idquestion = $qid");
   	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
   	    return $larrResult;
   }
   
	public function fnGetStudentDetails($IDApp)
   {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();   
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("b" =>"tbl_studentapplication"),array("b.*","CONCAT(b.Examdate,'-',IFNULL(b.Exammonth,' '),'-',IFNULL(r.Year,' ')) as StudExamdate"))
								 ->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
								 ->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
								 ->join(array("r" =>"tbl_newscheduler"),'r.idnewscheduler=b.Year',array("r.Year as Years"))
								 ->join(array("i" =>"tbl_managesession"),'i.idmangesession=b.Examsession',array("i.managesessionname","i.starttime as ExamSessionTime"))
								 ->join(array("j" =>"tbl_studentmarks"),'j.IDApplication = b.IDApplication')
								 ->join(array("k" =>"tbl_studentstartexamdetails"),'k.IDApplication = b.IDApplication',array("k.Starttime as Studentstarttime"))
								 ->where("b.IDApplication = $IDApp");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	public function fngetcentrestartdetails ($IDApp){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 
		$lstrSelect = "SELECT a.IDApplication,b.ExamStartTime  
		               FROM tbl_studentapplication a, tbl_centerstartexam b 
		               WHERE  a.Examvenue = b.idcenter and a.Program = b.Program and a.DateTime = b.ExamDate and a.IDApplication = '$IDApp'
		               order by b.ExamStartTime desc";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	public function  fngetschedulerofdatevenuestudent($ids,$days,$iddate)
       {
       			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array(""))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulervenue"),'a.idnewscheduler=d.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_center"),'d.idvenue=f.idcenter',array("key"=>"f.idcenter","value"=>"f.centername"))
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										  ->where("c.Days=?",$days)
										  ->group("f.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       }
	public function newgetscheduleryear($year)
     {
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			  $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler","a.To","a.From"))
                                          ->where("a.Active = 1")
										  ->where("a.Year=?",$year); 
			  $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			  return $larrResult;
     }       
	public function  fngetschedulerofdatesessionstudent($ids,$days,$iddate,$venueid)
       {
        		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 			$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" =>"tbl_newscheduler"),array(""))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulervenue"),'a.idnewscheduler=d.idnewscheduler',array(""))
									      ->join(array("e"=>"tbl_newschedulersession"),'a.idnewscheduler=e.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_managesession"),'f.idmangesession=e.idmanagesession',array("key"=>"f.idmangesession","value"=>"f.managesessionname"))
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										  ->where("c.Days=?",$days)
										  ->where("d.idvenue=?",$venueid)
										  ->group("e.idmanagesession");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       }
	public function fngetdayofdate($iddate)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	  	 $sql = "select DAYOFWEEK('$iddate') as days"; 
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
	   public function fngetpinfortakaful($idtakaful)
	   {
	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	        $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("key" => "a.registrationPin","value" =>"a.registrationPin"))
										  ->join(array("b"=>"tbl_takafuloperator"),"b.idtakafuloperator=a.idCompany",array())
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										   ->join(array("d"=>"tbl_registereddetails"),"a.registrationPin = d.RegistrationPin",array())
										   ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication = d.IDApplication",array())
										  //->where("tbl_batchregistration.paymentStatus=1")
										   ->where("b.idtakafuloperator =?",$idtakaful)
										  ->where("c.companyflag =2")
										  ->where("d.Approved =1")
										    ->where("d.IDApplication>1148")
										  ->where("a.paymentStatus in (1,2)")
										  ->group("a.registrationPin");
										 // ->where("a.idBatchRegistration != ?",$idbatchregistration);
										 //echo $lstrSelect;die();
										   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
       
										  }
		 public function fngetpinforcompany($idcompany)
	   {
	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	        $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("key" => "a.registrationPin","value" =>"a.registrationPin"))
										  ->join(array("b"=>"tbl_companies"),"b.IdCompany=a.idCompany",array())
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										    ->join(array("d"=>"tbl_registereddetails"),"a.registrationPin = d.RegistrationPin",array())
											 ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication = d.IDApplication",array())
										  //->where("tbl_batchregistration.paymentStatus=1")
										   ->where("b.IdCompany =?",$idcompany)
										    ->where("d.IDApplication>1148")
										  ->where("c.companyflag =1")
										   ->where("d.Approved =1")
										  ->where("a.paymentStatus in (1,2)")
										   ->group("a.registrationPin");
										 // ->where("a.idBatchRegistration != ?",$idbatchregistration);
										   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
       
										  }	
public function fngettakafulgroupwise($larrformData)
		{
		  // echo "<pre>";
		  // print_r($larrformData);die();
			if($larrformData['Date']) $fromdate = $larrformData['Date'];
            if($larrformData['Date2']) $todate = $larrformData['Date2'];
			
			if($larrformData['AFromDate']) $Afromdate = $larrformData['AFromDate'];
            if($larrformData['AToDate']) $Atodate = $larrformData['AToDate'];
			
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) 		 $icno = $larrformData['ICNO'];
			if($larrformData['Coursename'])  $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) 	 $venue = $larrformData['Venues'];
			if($larrformData['Takafulname']) $tname = $larrformData['Takafulname'];
			
			if($larrformData['Pin']) $Regpin = $larrformData['Pin'];
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select stdapp.IDApplication,FName,ICNO,
							prg.ProgramName,cnt.centername as ExamVenue,tak.TakafulName,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,
							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
							from tbl_studentapplication stdapp
							
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
							left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
							 left outer join tbl_takafuloperator tak on batch.idCompany = tak.idtakafuloperator 
							
							
							left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
							
							
							where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=2";
			 if($larrformData['Venues']) 		$lstrSelect .= " AND cnt.idcenter = $venue";
			 if($larrformData['Studentname'])	$lstrSelect .= " AND stdapp.fname like '%$name%'";
			 if($larrformData['ICNO']) 			$lstrSelect .= " AND stdapp.icno like '%$icno%'";
			 if($larrformData['Coursename'])	$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			 if($larrformData['Takafulname']) 	$lstrSelect .= " AND tak.idtakafuloperator = $tname";
			 if($larrformData['Date']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
             if($larrformData['Date2']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
			 if($larrformData['Pin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
			 
			 if($larrformData['AFromDate']) 	$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
             if($larrformData['AToDate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
			 
			// DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') <=
			 
             $lstrSelect .= " GROUP BY stdapp.IDApplication
			                  ORDER BY tak.TakafulName,batch.registrationPin,stdapp.DateTime,prg.ProgramName,session.managesessionname"; 
           //echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
public function fngetcompanygroupwise($larrformData)
		{
			if($larrformData['Date']) $fromdate = $larrformData['Date'];
            if($larrformData['Date2']) $todate = $larrformData['Date2'];
			
			if($larrformData['AFromDate']) $Afromdate = $larrformData['AFromDate'];
            if($larrformData['AToDate']) $Atodate = $larrformData['AToDate'];
			
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) 		 $icno = $larrformData['ICNO'];
			if($larrformData['Coursename'])  $Coursename = $larrformData['Coursename'];
			if($larrformData['Venues']) 	 $venue = $larrformData['Venues'];
			if($larrformData['Companyname']) $cname = $larrformData['Companyname'];
			
			if($larrformData['Pin']) $Regpin = $larrformData['Pin'];
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
           $lstrSelect = " select stdapp.IDApplication,FName,ICNO,
							prg.ProgramName,cnt.centername as ExamVenue,comp.CompanyName,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,
							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
							from tbl_studentapplication stdapp
							
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
							left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
							 left outer join tbl_companies comp on batch.idCompany = comp.IdCompany 	 
							
							
							left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
							
							
							where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=1";
							
	   	    
             if($larrformData['Venues']) $lstrSelect .= " AND cnt.idcenter = $venue";
			 if($larrformData['Studentname'])$lstrSelect .= " AND stdapp.fname like '%$name%'";
			 if($larrformData['ICNO']) $lstrSelect .= " AND stdapp.icno like '%$icno%'";
			 if($larrformData['Coursename'])$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			 if($larrformData['Companyname']) $lstrSelect .= " AND comp.IdCompany = $cname";
			 if($larrformData['Date']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
             if($larrformData['Date2']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
			 if($larrformData['Pin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
			 
			 if($larrformData['AFromDate']) 	$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
             if($larrformData['AToDate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
		    
          $lstrSelect .= " GROUP BY stdapp.IDApplication
			                  ORDER BY comp.CompanyName,batch.registrationPin,stdapp.DateTime,prg.ProgramName,session.managesessionname";				  
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}		
										  
		 public function fngettakafuldetails($larrformData)
		{
			if($larrformData['fromdate']) $fromdate = $larrformData['fromdate'];
            if($larrformData['todate']) $todate = $larrformData['todate'];
			
			if($larrformData['Afromdate']) $Afromdate = $larrformData['Afromdate'];
            if($larrformData['Atodate']) $Atodate = $larrformData['Atodate'];
			
			if($larrformData['student']) $name = $larrformData['student'];
			if($larrformData['icno']) 		 $icno = $larrformData['icno'];
			if($larrformData['course'])  $Coursename = $larrformData['course'];
			if($larrformData['venue']) 	 $venue = $larrformData['venue'];
			if($larrformData['takafulname']) $tname = $larrformData['takafulname'];
			
			if($larrformData['takafulpin']) $Regpin = $larrformData['takafulpin'];
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select stdapp.IDApplication,FName,ICNO,
							prg.ProgramName,cnt.centername as ExamVenue,tak.TakafulName,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,
							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
							from tbl_studentapplication stdapp
							
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
							left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
							 left outer join tbl_takafuloperator tak on batch.idCompany = tak.idtakafuloperator 
							
							
							left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
							
							
							where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=2";
			 if($larrformData['venue']) 		$lstrSelect .= " AND cnt.idcenter = $venue";
			 if($larrformData['student'])	$lstrSelect .= " AND stdapp.fname like '%$name%'";
			 if($larrformData['icno']) 			$lstrSelect .= " AND stdapp.icno like '%$icno%'";
			 if($larrformData['course'])	$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			 if($larrformData['takafulname']) 	$lstrSelect .= " AND tak.idtakafuloperator = $tname";
			 if($larrformData['fromdate']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
             if($larrformData['todate']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
			   if($larrformData['takafulpin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
			 
			 if($larrformData['Afromdate']) 			$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
             if($larrformData['Atodate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
			 
			// DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') <=
			 
             $lstrSelect .= " GROUP BY stdapp.IDApplication
			                  ORDER BY tak.TakafulName,batch.registrationPin,stdapp.DateTime,prg.ProgramName,session.managesessionname"; 
            // echo $lstrSelect;die();
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
		 public function fngetcompanydetails($larrformData)
		{
			if($larrformData['fromdate']) $fromdate = $larrformData['fromdate'];
            if($larrformData['todate']) $todate = $larrformData['todate'];
			
			if($larrformData['Afromdate']) $Afromdate = $larrformData['Afromdate'];
            if($larrformData['Atodate']) $Atodate = $larrformData['Atodate'];
			
			if($larrformData['student']) $name = $larrformData['student'];
			if($larrformData['icno']) 		 $icno = $larrformData['icno'];
			if($larrformData['course'])  $Coursename = $larrformData['course'];
			if($larrformData['venue']) 	 $venue = $larrformData['venue'];
			if($larrformData['Companyname']) $tname = $larrformData['Companyname'];
			
			if($larrformData['companypin']) $Regpin = $larrformData['companypin'];
			
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = " select stdapp.IDApplication,FName,ICNO,
							prg.ProgramName,cnt.centername as ExamVenue,comp.CompanyName,stdapp.EmailAddress as email,DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') as upddate,batch.registrationPin,session.managesessionname,batch.paymentStatus,stdapp.DateTime,
							if(stdapp.pass=1,'Pass',if(stdapp.pass=2,'Fail',if(stdapp.pass=3,'Applied','Absent'))) as 'Result'
							from tbl_studentapplication stdapp
							
							left outer join tbl_programmaster prg on stdapp.Program = prg.IdProgrammaster
							
							left outer join tbl_center cnt on stdapp.examvenue = cnt.idcenter
							
							left outer join tbl_registereddetails reg on stdapp.IDApplication = reg.IDApplication
							left outer join tbl_batchregistration batch on reg.RegistrationPin = batch.registrationPin
							left outer join tbl_studentpaymentoption pay on batch.idBatchRegistration = pay.IDApplication 
							 left outer join tbl_companies comp on batch.idCompany = comp.IdCompany 	 
							
							
							left outer join tbl_managesession session on stdapp.Examsession = session.idmangesession
							
							
							where stdapp.IDApplication>1148  and batch.paymentStatus in(1,2) and reg.Approved=1 and  pay.companyflag=1";
			 if($larrformData['venue']) 		$lstrSelect .= " AND cnt.idcenter = $venue";
			 if($larrformData['student'])	$lstrSelect .= " AND stdapp.fname like '%$name%'";
			 if($larrformData['icno']) 			$lstrSelect .= " AND stdapp.icno like '%$icno%'";
			 if($larrformData['course'])	$lstrSelect .= " AND prg.idprogrammaster = $Coursename"; 
			 if($larrformData['Companyname']) 	$lstrSelect .= " AND comp.IdCompany = $tname";
			 if($larrformData['fromdate']) 			$lstrSelect .= " AND stdapp.DateTime >=  '$fromdate'";
             if($larrformData['todate']) 		$lstrSelect .= " AND stdapp.DateTime <=  '$todate'";
			   if($larrformData['companypin']) 		$lstrSelect .= " AND batch.registrationPin =  '$Regpin'";
			 
			 if($larrformData['Afromdate']) 			$lstrSelect .= " AND date(stdapp.UpdDate) >=  '$Afromdate'";
             if($larrformData['Atodate']) 		$lstrSelect .= " AND date(stdapp.UpdDate) <=  '$Atodate'";
			 
			// DATE_FORMAT(date(stdapp.UpdDate),'%d-%m-%Y') <=
			 
             $lstrSelect .= " GROUP BY stdapp.IDApplication
			                  ORDER BY comp.CompanyName,batch.registrationPin,stdapp.DateTime,prg.ProgramName,session.managesessionname"; 
           
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
										 
	
}
