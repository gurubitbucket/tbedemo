<?php
class Reports_Model_DbTable_Centerwisestatisticsmodel extends Zend_Db_Table { 
	
	public function fngetages($lintmon,$lintyear){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT a.`IDApplication`,cast(((to_days(curdate()) - to_days(a.DateOfBirth)) / 365) as signed) AS 'Age',a.pass
 					   FROM tbl_studentapplication a ,tbl_newscheduler b
                       WHERE  a.Year = b.idnewscheduler and a.Exammonth = $lintmon  and b.Year = $lintyear and a.Examvenue <> 000 and a.Payment = 1 and a.IDApplication>1148";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
        public function fngetresultfordates($larrformData)
        {
           // echo "<pre>";
            //print_r($larrformData);die();
            $Fromdate = $larrformData['Date1'];
            $Todate = $larrformData['Date2'];
            $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
             $lstrselect = "SELECT b.centername,
                sum( a.pass = 1 ) as `pass`,
                sum( a.pass = 2 ) as `fail`,
                sum( a.pass = 4 ) as `absent`,
                EXTRACT( YEAR_MONTH FROM a.DateTime ) as 'year-month'
                FROM `tbl_studentapplication` as a
                join tbl_center as b on b.idcenter=a.Examvenue
                where `DateTime`>= $Fromdate and `DateTime`<=$Todate
                group by b.idcenter,EXTRACT( YEAR_MONTH FROM a.DateTime ) order by b.centername";
            
            
            
            
            $larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
        }
}