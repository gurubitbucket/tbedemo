<?php
class Reports_Model_DbTable_Paypalentryreport extends Zend_Db_Table {	
	public function fngetstudentpaypalentrydetails($larrformData){	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	  
	    $lstrselect = $lobjDbAdpt->select()
					-> from(array('tbl_paypaldetails' => 'tbl_paypaldetails'),array("tbl_paypaldetails.idpaypalDetails","tbl_paypaldetails.grossAmount","tbl_paypaldetails.transactionId" , "tbl_paypaldetails.UpdDate AS PaymentDate","tbl_paypaldetails.ApprovedDate AS ApprovedDate"))
					-> join(array('tbl_studentapplication' => 'tbl_studentapplication'),"tbl_studentapplication.IDApplication = tbl_paypaldetails.IDApplication AND tbl_studentapplication.Payment =1",array('tbl_studentapplication.FName','tbl_studentapplication.IDApplication'))
					-> join(array('tbl_registereddetails' => 'tbl_registereddetails'),"tbl_registereddetails .IDApplication = tbl_studentapplication.IDApplication AND tbl_registereddetails.Approved =1",array('tbl_registereddetails.Regid','tbl_registereddetails.idregistereddetails',new Zend_Db_Expr('NULL AS idRegistrationFpx'),new Zend_Db_Expr('NULL AS orderNumber')))
					-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_studentapplication.IDApplication AND tbl_studentpaymentoption.companyflag = 0 AND tbl_studentpaymentoption.ModeofPayment =2",array('tbl_studentpaymentoption.ModeofPayment'))
					-> where('tbl_paypaldetails.paymentStatus =1')
					-> where("DATE_FORMAT(tbl_paypaldetails.UpdDate,'%Y-%m-%d')  >= '".$larrformData['Date3']."'")
					-> where("DATE_FORMAT(tbl_paypaldetails.UpdDate,'%Y-%m-%d')  <= '".$larrformData['Date4']."'")
					-> group('tbl_studentapplication.IDApplication')
					-> order('tbl_studentapplication.FName');		
								
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	
	/*public function fngetstudentmigsentrydetails($larrformData)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	  
	    $lstrselect = $lobjDbAdpt->select()
					-> from(array('tbl_migspayment' => 'tbl_migspayment'),array("tbl_migspayment.idmigspayment","tbl_paypaldetails.grossAmount","tbl_paypaldetails.transactionId" , "tbl_paypaldetails.UpdDate AS PaymentDate","tbl_paypaldetails.ApprovedDate AS ApprovedDate"))
					-> join(array('tbl_studentapplication' => 'tbl_studentapplication'),"tbl_studentapplication.IDApplication = tbl_paypaldetails.IDApplication AND tbl_studentapplication.Payment =1",array('tbl_studentapplication.FName','tbl_studentapplication.IDApplication',''))
					-> join(array('tbl_registereddetails' => 'tbl_registereddetails'),"tbl_registereddetails .IDApplication = tbl_studentapplication.IDApplication AND tbl_registereddetails.Approved =1",array('tbl_registereddetails.Regid','tbl_registereddetails.idregistereddetails',new Zend_Db_Expr('NULL AS idRegistrationFpx'),new Zend_Db_Expr('NULL AS orderNumber')))
					-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_studentapplication.IDApplication AND tbl_studentpaymentoption.companyflag = 0 AND tbl_studentpaymentoption.ModeofPayment =2",array('tbl_studentpaymentoption.ModeofPayment'))
					-> where('tbl_paypaldetails.paymentStatus =1')
					-> where("DATE_FORMAT(tbl_paypaldetails.UpdDate,'%Y-%m-%d')  >= '".$larrformData['Date3']."'")
					-> where("DATE_FORMAT(tbl_paypaldetails.UpdDate,'%Y-%m-%d')  <= '".$larrformData['Date4']."'")
					-> group('tbl_studentapplication.IDApplication')
					-> order('tbl_studentapplication.FName');		
								
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
		
	}*/
	
 public function fnbatchregcnt($batchids)
										  {
										  	if(empty($batchids))
										  	{
										  		$batchids="'a0'";
										  	}
										  		        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
										  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("d" => "tbl_registereddetails"),array("count(RegistrationPin) as regtotal","RegistrationPin"))
										  ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication=d.IDApplication",array())									  
										  ->where("d.RegistrationPin in ($batchids)")
										  ->where("d.Approved =1")
										  ->group("d.RegistrationPin");
										 // echo $lstrSelect;die();
										   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
										  	return $larrResult;	
										  	
										  	
										  }
										  
public function fngetcompanypaypalentrydetails($larrformData){	
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
					-> from(array('tbl_batchpaypal' => 'tbl_batchpaypal'),array("tbl_batchpaypal.idpaypalDetails","tbl_batchpaypal.grossAmount","tbl_batchpaypal.transactionId","tbl_batchpaypal.UpdDate AS PaymentDate","tbl_batchpaypal.ApprovedDate AS ApprovedDate"))
					-> join(array('tbl_batchregistration' => 'tbl_batchregistration'),"tbl_batchregistration.idBatchRegistration  =  tbl_batchpaypal.IDCompany",array("tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates"))
					-> join(array('tbl_companies' => 'tbl_companies'),"tbl_companies.IdCompany = tbl_batchregistration.idCompany",array("tbl_companies.CompanyName","tbl_companies.IdCompany",new Zend_Db_Expr('NULL AS idRegistrationFpx'),new Zend_Db_Expr('NULL AS orderNumber')))
					-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag = 1 AND tbl_studentpaymentoption.ModeofPayment =2",array('tbl_studentpaymentoption.ModeofPayment'))
					-> where('tbl_batchpaypal.paymentStatus =1')
					-> where('tbl_batchregistration.paymentStatus !=3')
					-> where("DATE_FORMAT(tbl_batchpaypal.UpdDate,'%Y-%m-%d')  >= '".$larrformData['Date3']."'")
					-> where("DATE_FORMAT(tbl_batchpaypal.UpdDate,'%Y-%m-%d')  <= '".$larrformData['Date4']."'")
					-> order('tbl_companies.CompanyName');
					//echo $lstrselect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
public function fngettakafulpaypalentrydetails($larrformData){	
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
					-> from(array('tbl_batchpaypal' => 'tbl_batchpaypal'),array("tbl_batchpaypal.idpaypalDetails","tbl_batchpaypal.grossAmount","tbl_batchpaypal.transactionId","tbl_batchpaypal.UpdDate AS PaymentDate","tbl_batchpaypal.ApprovedDate AS ApprovedDate"))
					-> join(array('tbl_batchregistration' => 'tbl_batchregistration'),"tbl_batchregistration.idBatchRegistration  =  tbl_batchpaypal.IDCompany",array("tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates"))
					-> join(array('tbl_takafuloperator' => 'tbl_takafuloperator'),"tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany",array("tbl_takafuloperator.TakafulName","tbl_takafuloperator.idtakafuloperator",new Zend_Db_Expr('NULL AS idRegistrationFpx'),new Zend_Db_Expr('NULL AS orderNumber')))
					-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag = 2 AND tbl_studentpaymentoption.ModeofPayment =2",array('tbl_studentpaymentoption.ModeofPayment'))
					-> where('tbl_batchpaypal.paymentStatus =1')
					-> where('tbl_batchregistration.paymentStatus !=3')
					-> where("DATE_FORMAT(tbl_batchpaypal.UpdDate,'%Y-%m-%d')  >= '".$larrformData['Date3']."'")
					-> where("DATE_FORMAT(tbl_batchpaypal.UpdDate,'%Y-%m-%d')  <= '".$larrformData['Date4']."'")
					-> order('tbl_takafuloperator.TakafulName');
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 		
	}
public function fngetstuddetails($idRegistredDetails){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	$lstrselect =  "SELECT tbl_studentapplication.FName,tbl_studentapplication.ICNO,tbl_studentapplication.DateOfBirth,tbl_studentapplication.EmailAddress , 
	 	tbl_paypaldetails.idpaypalDetails ,tbl_paypaldetails.payerId ,tbl_paypaldetails.verifySign , tbl_paypaldetails.grossAmount , tbl_paypaldetails.transactionId , tbl_paypaldetails.UpdDate AS PaymentDate, tbl_paypaldetails.ApprovedDate AS ApprovedDate, tbl_studentapplication.IDApplication ,tbl_registereddetails.Regid
		FROM tbl_registereddetails
		JOIN tbl_studentapplication ON tbl_registereddetails .IDApplication = tbl_studentapplication.IDApplication AND tbl_registereddetails.Approved =1 AND tbl_registereddetails.idregistereddetails = ".$idRegistredDetails."
	 	JOIN  tbl_paypaldetails ON (tbl_studentapplication.IDApplication = tbl_paypaldetails.IDApplication AND tbl_studentapplication.Payment =1 )
		JOIN tbl_studentpaymentoption ON tbl_studentpaymentoption.IDApplication = tbl_studentapplication.IDApplication AND tbl_studentpaymentoption.companyflag = 0 AND tbl_studentpaymentoption.ModeofPayment =2
		WHERE tbl_paypaldetails.paymentStatus =1 GROUP BY tbl_studentapplication.IDApplication"; 	
	    $larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}	
public function fngetcompanydetails($idBatchRegistration){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = "SELECT tbl_companies.CompanyName,tbl_companies.ShortName,tbl_companies.Email,tbl_companies.IdCompany ,tbl_batchregistration.totalNoofCandidates,
	    		tbl_batchpaypal.idpaypalDetails ,tbl_batchpaypal.payerId ,tbl_batchpaypal.verifySign ,
	    		 tbl_batchpaypal.grossAmount , tbl_batchpaypal.transactionId , tbl_batchpaypal.UpdDate AS PaymentDate,tbl_batchpaypal.ApprovedDate AS ApprovedDate,
	    		tbl_batchregistration.registrationPin,tbl_batchregistration.idBatchRegistration,tbl_batchregistration.totalNoofCandidates
 				FROM tbl_batchpaypal 
 				JOIN tbl_batchregistration ON tbl_batchregistration.idBatchRegistration = tbl_batchpaypal.IDCompany  AND tbl_batchregistration.idBatchRegistration= ".$idBatchRegistration."
 				JOIN tbl_companies ON tbl_companies.IdCompany = tbl_batchregistration.idCompany 
 				JOIN tbl_studentpaymentoption ON tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration AND  tbl_studentpaymentoption.ModeofPayment =2 AND tbl_studentpaymentoption.companyflag = 1
 				WHERE tbl_batchpaypal.paymentStatus =1 AND tbl_batchpaypal.entryFrom = 0 GROUP BY tbl_batchpaypal.transactionId";
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
}	
public function fngettakafuldetails($idBatchRegistration){
	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  $lstrselect = "SELECT tbl_takafuloperator.TakafulName,tbl_takafuloperator.TakafulShortName,tbl_takafuloperator.email,tbl_takafuloperator.idtakafuloperator ,
	    		tbl_batchpaypal.idpaypalDetails ,tbl_batchpaypal.payerId ,tbl_batchpaypal.verifySign ,tbl_batchregistration.totalNoofCandidates,
	    		 tbl_batchpaypal.grossAmount , tbl_batchpaypal.transactionId , tbl_batchpaypal.UpdDate AS PaymentDate,tbl_batchpaypal.ApprovedDate AS ApprovedDate,
	    		tbl_batchregistration.registrationPin,tbl_batchregistration.idBatchRegistration,tbl_batchregistration.totalNoofCandidates
 				FROM tbl_batchpaypal 
 				JOIN tbl_batchregistration ON tbl_batchregistration.idBatchRegistration = tbl_batchpaypal.IDCompany  AND tbl_batchregistration.idBatchRegistration= ".$idBatchRegistration."
 				JOIN tbl_takafuloperator ON tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany 
 				JOIN tbl_studentpaymentoption ON tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration AND  tbl_studentpaymentoption.ModeofPayment =2 AND tbl_studentpaymentoption.companyflag = 2 
 				WHERE tbl_batchpaypal.paymentStatus =1 AND tbl_batchpaypal.entryFrom = 1";
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
}
public function fngetstudentfpxentrydetails($larrformData){	
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();   
	   $lstrselect = $lobjDbAdpt->select()
					-> from(array('tbl_registrationfpx' => 'tbl_registrationfpx'),array("tbl_registrationfpx.idRegistrationFpx","tbl_registrationfpx.orderNumber","tbl_registrationfpx.grossAmount" ,"tbl_registrationfpx.fpxTxnId AS transactionId","tbl_registrationfpx.TxnDate AS PaymentDate","tbl_registrationfpx.ApprovedDate AS ApprovedDate"))
					-> join(array('tbl_studentapplication' => 'tbl_studentapplication'),"tbl_studentapplication.IDApplication = tbl_registrationfpx.IDApplication AND tbl_registrationfpx.entryFrom = 1 AND tbl_studentapplication.Payment =1",array('tbl_studentapplication.FName','tbl_studentapplication.IDApplication'))
					-> join(array('tbl_registereddetails' => 'tbl_registereddetails'),"tbl_registereddetails .IDApplication = tbl_studentapplication.IDApplication AND tbl_registereddetails.Approved =1",array('tbl_registereddetails.Regid','tbl_registereddetails.idregistereddetails',new Zend_Db_Expr('NULL AS idpaypalDetails')))
					-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_studentapplication.IDApplication AND tbl_studentpaymentoption.companyflag = 0 AND tbl_studentpaymentoption.ModeofPayment =1",	array('tbl_studentpaymentoption.ModeofPayment'))
					-> where('tbl_registrationfpx.paymentStatus =1')
					-> where("DATE_FORMAT(tbl_registrationfpx.TxnDate,'%Y-%m-%d')  >= '".$larrformData['Date3']."'")
					-> where("DATE_FORMAT(tbl_registrationfpx.TxnDate,'%Y-%m-%d')  <= '".$larrformData['Date4']."'")
					-> group('tbl_studentapplication.IDApplication')
					-> order('tbl_studentapplication.FName');		
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}	
	
public function fngetcompanypfxentrydetails($larrformData){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
	 	$lstrselect = $lobjDbAdpt->select()
					-> from(array('tbl_registrationfpx' => 'tbl_registrationfpx'),	array("tbl_registrationfpx.idRegistrationFpx","tbl_registrationfpx.orderNumber","tbl_registrationfpx.grossAmount" ,"tbl_registrationfpx.fpxTxnId AS transactionId","tbl_registrationfpx.TxnDate AS PaymentDate","tbl_registrationfpx.ApprovedDate AS ApprovedDate"))
					-> join(array('tbl_batchregistration' => 'tbl_batchregistration'),"tbl_batchregistration.idBatchRegistration  =   tbl_registrationfpx.IDApplication AND tbl_registrationfpx.entryFrom =2",array('tbl_batchregistration.registrationPin','tbl_batchregistration.idBatchRegistration','tbl_batchregistration.totalNoofCandidates'))
					-> join(array('tbl_companies' => 'tbl_companies'),"tbl_companies.IdCompany = tbl_batchregistration.idCompany",array('tbl_companies.IdCompany','tbl_companies.CompanyName',new Zend_Db_Expr('NULL AS idpaypalDetails')))
					-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication 	= tbl_batchregistration.idBatchRegistration AND  tbl_studentpaymentoption.ModeofPayment 	=1 AND tbl_studentpaymentoption.companyflag = 1",	array('tbl_studentpaymentoption.ModeofPayment'))
					-> where('tbl_registrationfpx.paymentStatus =1')
					-> where('tbl_batchregistration.paymentStatus !=3')
					-> where("DATE_FORMAT(tbl_registrationfpx.TxnDate,'%Y-%m-%d')  >= '".$larrformData['Date3']."'")
					-> where("DATE_FORMAT(tbl_registrationfpx.TxnDate,'%Y-%m-%d')  <= '".$larrformData['Date4']."'")
					-> order('tbl_companies.CompanyName');
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}		
public function fngettakafulfpxentrydetails($larrformData){	
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();   
	   $lstrselect = $lobjDbAdpt->select()
					-> from(array('tbl_registrationfpx' => 'tbl_registrationfpx'),array("tbl_registrationfpx.idRegistrationFpx","tbl_registrationfpx.orderNumber","tbl_registrationfpx.grossAmount" ,"tbl_registrationfpx.fpxTxnId AS transactionId","tbl_registrationfpx.TxnDate AS PaymentDate","tbl_registrationfpx.ApprovedDate AS ApprovedDate"))
					-> join(array('tbl_batchregistration' => 'tbl_batchregistration'),"tbl_batchregistration.idBatchRegistration  =   tbl_registrationfpx.IDApplication AND tbl_registrationfpx.entryFrom =3",array('tbl_batchregistration.registrationPin','tbl_batchregistration.idBatchRegistration','tbl_batchregistration.totalNoofCandidates'))
					-> join(array('tbl_takafuloperator' => 'tbl_takafuloperator'),"tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany",array('tbl_takafuloperator.idtakafuloperator','tbl_takafuloperator.TakafulName',new Zend_Db_Expr('NULL AS idpaypalDetails')))
					-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication 	= tbl_batchregistration.idBatchRegistration AND  tbl_studentpaymentoption.ModeofPayment 	=1 AND tbl_studentpaymentoption.companyflag = 2",	array('tbl_studentpaymentoption.ModeofPayment'))
					-> where('tbl_registrationfpx.paymentStatus =1')
					-> where('tbl_batchregistration.paymentStatus !=3')
					-> where("DATE_FORMAT(tbl_registrationfpx.TxnDate,'%Y-%m-%d')  >= '".$larrformData['Date3']."'")
					-> where("DATE_FORMAT(tbl_registrationfpx.TxnDate,'%Y-%m-%d')  <= '".$larrformData['Date4']."'")
					-> order('tbl_takafuloperator.TakafulName');
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}	
public function fngetfpxstuddetails($idRegistredDetails){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	    
	    $lstrselect = $lobjDbAdpt->select()
					-> from(array('tbl_registereddetails' => 'tbl_registereddetails'),array('tbl_registereddetails.Regid'))							
					-> join(array('tbl_studentapplication' => 'tbl_studentapplication'),"tbl_registereddetails .IDApplication = tbl_studentapplication.IDApplication AND tbl_registereddetails.Approved =1 AND tbl_registereddetails.idregistereddetails = ".$idRegistredDetails."",array('tbl_studentapplication.FName','tbl_studentapplication.IDApplication','tbl_studentapplication.ICNO','tbl_studentapplication.DateOfBirth','tbl_studentapplication.EmailAddress'))
					-> join(array('tbl_registrationfpx' => 'tbl_registrationfpx'),"tbl_studentapplication.IDApplication = tbl_registrationfpx.IDApplication AND tbl_studentapplication.Payment =1",	array("tbl_registrationfpx.bankCode","tbl_registrationfpx.bankBranch","tbl_registrationfpx.idRegistrationFpx","tbl_registrationfpx.orderNumber","tbl_registrationfpx.grossAmount" ,"tbl_registrationfpx.fpxTxnId AS transactionId","tbl_registrationfpx.TxnDate AS PaymentDate","tbl_registrationfpx.ApprovedDate AS ApprovedDate","tbl_registrationfpx.payerMailId AS payerId"))
					-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_studentapplication.IDApplication AND tbl_studentpaymentoption.companyflag = 0 AND tbl_studentpaymentoption.ModeofPayment =1",	array('tbl_studentpaymentoption.ModeofPayment'))
					-> where('tbl_registrationfpx.paymentStatus =1')
					-> group('tbl_studentapplication.IDApplication');
	    $larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}		
public function fngetfpxcompanydetails($idBatchRegistration){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
		$lstrselect = $lobjDbAdpt->select()
						-> from(array('tbl_registrationfpx' => 'tbl_registrationfpx'),array("tbl_registrationfpx.bankCode","tbl_registrationfpx.bankBranch","tbl_registrationfpx.idRegistrationFpx","tbl_registrationfpx.orderNumber","tbl_registrationfpx.grossAmount" ,"tbl_registrationfpx.fpxTxnId AS transactionId","tbl_registrationfpx.TxnDate AS PaymentDate","tbl_registrationfpx.ApprovedDate AS ApprovedDate","tbl_registrationfpx.payerMailId AS payerId"))						
						-> join(array('tbl_batchregistration' => 'tbl_batchregistration'),"tbl_batchregistration.idBatchRegistration = tbl_registrationfpx.IDApplication  AND tbl_batchregistration.idBatchRegistration= ".$idBatchRegistration,array('tbl_batchregistration.registrationPin','tbl_batchregistration.idBatchRegistration','tbl_batchregistration.totalNoofCandidates'))
						-> join(array('tbl_companies' => 'tbl_companies'),"tbl_companies.IdCompany = tbl_batchregistration.idCompany ",	array('tbl_companies.CompanyName','tbl_companies.ShortName','tbl_companies.Email','tbl_companies.IdCompany'))
						-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration AND  tbl_studentpaymentoption.ModeofPayment =1 AND tbl_studentpaymentoption.companyflag = 1",	array('tbl_studentpaymentoption.ModeofPayment'))
						-> where('tbl_registrationfpx.paymentStatus =1')
						-> where('tbl_batchregistration.paymentStatus !=3')
						-> where('tbl_registrationfpx.entryFrom = 2');					
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}	
public function fngetfpxtakafuldetails($idBatchRegistration){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
		$lstrselect = $lobjDbAdpt->select()
						-> from(array('tbl_registrationfpx' => 'tbl_registrationfpx'),array("tbl_registrationfpx.bankCode","tbl_registrationfpx.bankBranch","tbl_registrationfpx.idRegistrationFpx","tbl_registrationfpx.orderNumber","tbl_registrationfpx.grossAmount" ,"tbl_registrationfpx.fpxTxnId AS transactionId","tbl_registrationfpx.TxnDate AS PaymentDate","tbl_registrationfpx.ApprovedDate AS ApprovedDate","tbl_registrationfpx.payerMailId AS payerId"))						
						-> join(array('tbl_batchregistration' => 'tbl_batchregistration'),"tbl_batchregistration.idBatchRegistration = tbl_registrationfpx.IDApplication  AND tbl_batchregistration.idBatchRegistration= ".$idBatchRegistration,array('tbl_batchregistration.registrationPin','tbl_batchregistration.idBatchRegistration','tbl_batchregistration.totalNoofCandidates'))
						-> join(array('tbl_takafuloperator' => 'tbl_takafuloperator'),"tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany",array('tbl_takafuloperator.TakafulName','tbl_takafuloperator.TakafulShortName','tbl_takafuloperator.email','tbl_takafuloperator.idtakafuloperator'))
						-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration AND  tbl_studentpaymentoption.ModeofPayment =1 AND tbl_studentpaymentoption.companyflag = 2",	array('tbl_studentpaymentoption.ModeofPayment'))
						-> where('tbl_registrationfpx.paymentStatus =1')
						-> where('tbl_batchregistration.paymentStatus !=3')
						-> where('tbl_registrationfpx.entryFrom = 3');	
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}



	public function fngetstudentpaymententrydetails($larrformData){	
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrselect =  "SELECT tbl_studentapplication.FName, 
	    NULL AS idpaypalDetails , 
	    tbl_studentpaymentdetails.Amount AS grossAmount, 
	    tbl_studentpaymentdetails.ChequeNo  AS transactionId,
	    tbl_studentpaymentdetails.UpdDate AS PaymentDate,	   
	    NULL AS ApprovedDate, 
	    tbl_studentapplication.IDApplication ,
	    tbl_registereddetails.Regid,tbl_registereddetails.idregistereddetails, NULL AS idRegistrationFpx,NULL AS orderNumber,tbl_studentpaymentoption.ModeofPayment
		FROM tbl_studentpaymentdetails 
		JOIN tbl_studentapplication ON (tbl_studentapplication.IDApplication = tbl_studentpaymentdetails.IDApplication AND tbl_studentapplication.Payment =1 )
		JOIN tbl_registereddetails ON tbl_registereddetails .IDApplication = tbl_studentapplication.IDApplication AND tbl_registereddetails.Approved =1
		JOIN tbl_studentpaymentoption ON tbl_studentpaymentoption.IDApplication = tbl_studentapplication.IDApplication AND tbl_studentpaymentoption.companyflag = 0 AND tbl_studentpaymentoption.ModeofPayment !=1 AND tbl_studentpaymentoption.ModeofPayment != 2 
		WHERE tbl_studentpaymentdetails.companyflag =0 AND DATE_FORMAT(tbl_studentpaymentdetails.UpdDate,'%Y-%m-%d')  >= '".$larrformData['Date3']."' AND  DATE_FORMAT(tbl_studentpaymentdetails.UpdDate,'%Y-%m-%d')  <= '".$larrformData['Date4']."'";
		
		if($larrformData['Coursename'])  $lstrselect .= " AND tbl_studentpaymentoption.ModeofPayment =".$larrformData['Coursename'];
		$lstrselect .= " GROUP BY tbl_studentapplication.IDApplication 
		ORDER BY tbl_studentpaymentoption.ModeofPayment,tbl_studentapplication.FName";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
public function fngetcompanypaymententrydetails($larrformData){	
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  	 $lstrselect = "SELECT tbl_companies.CompanyName,
	 	  NULL AS idpaypalDetails ,
	 	   tbl_studentpaymentdetails.Amount AS grossAmount,tbl_batchregistration.totalNoofCandidates, 
	    tbl_studentpaymentdetails.ChequeNo  AS transactionId,
	    tbl_studentpaymentdetails.UpdDate AS PaymentDate,	   
	    NULL AS ApprovedDate  	 
	 	 ,tbl_companies.IdCompany ,
	 	 tbl_batchregistration.registrationPin,tbl_batchregistration.idBatchRegistration,NULL AS idRegistrationFpx,NULL AS orderNumber,tbl_studentpaymentoption.ModeofPayment
FROM tbl_studentpaymentdetails
JOIN tbl_batchregistration ON  tbl_batchregistration.idBatchRegistration = tbl_studentpaymentdetails.IDApplication AND tbl_batchregistration.paymentStatus !=3
JOIN tbl_companies ON tbl_companies.IdCompany = tbl_batchregistration.IDCompany 
JOIN tbl_studentpaymentoption ON tbl_studentpaymentoption.IDApplication 	= tbl_batchregistration.idBatchRegistration
		AND   tbl_studentpaymentoption.ModeofPayment not in (1,2,10,181) AND tbl_studentpaymentoption.companyflag = 1
WHERE tbl_studentpaymentdetails.companyflag =1  AND DATE_FORMAT(tbl_studentpaymentdetails.UpdDate,'%Y-%m-%d') >= '".$larrformData['Date3']."' AND  DATE_FORMAT(tbl_studentpaymentdetails.UpdDate,'%Y-%m-%d') <= '".$larrformData['Date4']."'";

if($larrformData['Coursename'])  $lstrselect .= " AND tbl_studentpaymentoption.ModeofPayment =".$larrformData['Coursename'];
 $lstrselect .= " ORDER BY tbl_studentpaymentoption.ModeofPayment,tbl_companies.CompanyName";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	/*
public function fngettakafulpaymententrydetails($larrformData){	
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrselect = "SELECT tbl_takafuloperator.TakafulName, 
	    NULL AS idpaypalDetails ,
	 	tbl_studentpaymentdetails.Amount AS grossAmount, 
	    tbl_studentpaymentdetails.ChequeNo  AS transactionId,
	    tbl_studentpaymentdetails.UpdDate AS PaymentDate,	   
	    NULL AS ApprovedDate,
	    tbl_takafuloperator.idtakafuloperator ,
	   tbl_batchregistration.registrationPin,tbl_batchregistration.idBatchRegistration,
	   NULL AS idRegistrationFpx,NULL AS orderNumber,tbl_studentpaymentoption.ModeofPayment
 FROM tbl_studentpaymentdetails
 JOIN tbl_batchregistration ON tbl_batchregistration.idBatchRegistration = tbl_studentpaymentdetails.IDApplication  AND tbl_batchregistration.paymentStatus !=3
 JOIN tbl_takafuloperator ON tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany 
 JOIN tbl_studentpaymentoption ON tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration 
 	AND   tbl_studentpaymentoption.ModeofPayment != 1 AND  tbl_studentpaymentoption.ModeofPayment !=2 AND tbl_studentpaymentoption.companyflag = 2 
 WHERE tbl_studentpaymentdetails.companyflag =2 AND DATE_FORMAT(tbl_studentpaymentdetails.UpdDate,'%Y-%m-%d') >= '".$larrformData['Date3']."' AND DATE_FORMAT(tbl_studentpaymentdetails.UpdDate,'%Y-%m-%d') <= '".$larrformData['Date4']."'";
 if($larrformData['Coursename'])  $lstrselect .= " AND tbl_studentpaymentoption.ModeofPayment =".$larrformData['Coursename'];
 $lstrselect .= " ORDER BY tbl_studentpaymentoption.ModeofPayment,tbl_takafuloperator.TakafulName ";
	//	echo $lstrselect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	*/
public function fngettakafulpaymententrydetails($larrformData){	
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrselect = "SELECT tbl_takafuloperator.TakafulName, 
	    NULL AS idpaypalDetails ,
	 	   tbl_studentpaymentdetails.Amount AS grossAmount, 
	    tbl_studentpaymentdetails.ChequeNo  AS transactionId,tbl_batchregistration.totalNoofCandidates, 
	    tbl_studentpaymentdetails.UpdDate AS PaymentDate,	   
	    NULL AS ApprovedDate,
	    tbl_takafuloperator.idtakafuloperator ,
	   tbl_batchregistration.registrationPin,tbl_batchregistration.idBatchRegistration,
	   NULL AS idRegistrationFpx,NULL AS orderNumber,tbl_studentpaymentoption.ModeofPayment
 FROM tbl_studentpaymentdetails
 JOIN tbl_batchregistration ON tbl_batchregistration.idBatchRegistration = tbl_studentpaymentdetails.IDApplication 
 JOIN tbl_takafuloperator ON tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany 
 JOIN tbl_studentpaymentoption ON tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration 
 	AND   tbl_studentpaymentoption.ModeofPayment not in (1,2,10,181) AND tbl_studentpaymentoption.companyflag = 2 
 WHERE tbl_studentpaymentdetails.companyflag =2 AND DATE_FORMAT(tbl_studentpaymentdetails.UpdDate,'%Y-%m-%d') >= '".$larrformData['Date3']."' AND DATE_FORMAT(tbl_studentpaymentdetails.UpdDate,'%Y-%m-%d') <= '".$larrformData['Date4']."'";
 if($larrformData['Coursename'])  $lstrselect .= " AND tbl_studentpaymentoption.ModeofPayment =".$larrformData['Coursename'];
 $lstrselect .= " ORDER BY tbl_studentpaymentoption.ModeofPayment,tbl_takafuloperator.TakafulName ";
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	
	
	
public function fngettakafulpaylaterdetails($larrformData)
{	
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrselect = "SELECT tbl_takafuloperator.TakafulName, 
	    NULL AS idpaypalDetails ,
	 	    tbl_batchregistration.totalAmount AS grossAmount,tbl_batchregistration.totalNoofCandidates,  
	   ''  AS transactionId,
	    tbl_batchregistration.UpdDate AS PaymentDate,	   
	    NULL AS ApprovedDate,
	    tbl_takafuloperator.idtakafuloperator ,
	   tbl_batchregistration.registrationPin,tbl_batchregistration.idBatchRegistration,
	   NULL AS idRegistrationFpx,NULL AS orderNumber,tbl_studentpaymentoption.ModeofPayment
 FROM tbl_studentpaymentoption
 JOIN tbl_batchregistration ON tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration    AND tbl_batchregistration.paymentStatus in (1,2)
 JOIN tbl_takafuloperator ON tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany 
 AND   tbl_studentpaymentoption.ModeofPayment = 181 AND tbl_studentpaymentoption.companyflag = 2 
 WHERE tbl_batchregistration.totalAmount!=0 and DATE_FORMAT(tbl_batchregistration.UpdDate,'%Y-%m-%d') >= '".$larrformData['Date3']."' AND DATE_FORMAT(tbl_batchregistration.UpdDate,'%Y-%m-%d') <= '".$larrformData['Date4']."'";
 if($larrformData['Coursename'])  $lstrselect .= " AND tbl_studentpaymentoption.ModeofPayment =".$larrformData['Coursename'];
 $lstrselect .= " ORDER BY tbl_studentpaymentoption.ModeofPayment,tbl_takafuloperator.TakafulName ";
	//	echo $lstrselect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}
	
	public function fngetcompanypaymentpaylaterdetails($larrformData)
	{
		
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  	 $lstrselect = "SELECT tbl_companies.CompanyName,
	 	  NULL AS idpaypalDetails ,
	 	   tbl_batchregistration.totalAmount AS grossAmount,tbl_batchregistration.totalNoofCandidates,  
	    ''  AS transactionId,
	    tbl_batchregistration.UpdDate AS PaymentDate,	   
	    NULL AS ApprovedDate  	 
	 	 ,tbl_companies.IdCompany ,
	 	 tbl_batchregistration.registrationPin,tbl_batchregistration.idBatchRegistration,NULL AS idRegistrationFpx,NULL AS orderNumber,tbl_studentpaymentoption.ModeofPayment
FROM tbl_studentpaymentoption
 JOIN tbl_batchregistration ON tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration    AND tbl_batchregistration.paymentStatus in (1,2)
JOIN tbl_companies ON tbl_companies.IdCompany = tbl_batchregistration.IDCompany 
AND   tbl_studentpaymentoption.ModeofPayment = 181 AND tbl_studentpaymentoption.companyflag = 1
WHERE  tbl_batchregistration.totalAmount!=0 AND DATE_FORMAT(tbl_batchregistration.UpdDate,'%Y-%m-%d') >= '".$larrformData['Date3']."' AND  DATE_FORMAT(tbl_batchregistration.UpdDate,'%Y-%m-%d') <= '".$larrformData['Date4']."'";
if($larrformData['Coursename'])  $lstrselect .= " AND tbl_studentpaymentoption.ModeofPayment =".$larrformData['Coursename'];
 $lstrselect .= " ORDER BY tbl_studentpaymentoption.ModeofPayment,tbl_companies.CompanyName";
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
		
	}
	
public function fngetstudotherpaymentdetails($idRegistredDetails){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	$lstrselect = $lobjDbAdpt->select()
						-> from(array('tbl_registereddetails' => 'tbl_registereddetails'),array("tbl_registereddetails.Regid"))						
						-> join(array('tbl_studentapplication' => 'tbl_studentapplication'),"tbl_registereddetails .IDApplication = tbl_studentapplication.IDApplication AND tbl_registereddetails.Approved =1 AND tbl_registereddetails.idregistereddetails = ".$idRegistredDetails,array('tbl_studentapplication.FName','tbl_studentapplication.ICNO','tbl_studentapplication.DateOfBirth','tbl_studentapplication.EmailAddress','tbl_studentapplication.IDApplication'))
						-> join(array('tbl_studentpaymentdetails' => 'tbl_studentpaymentdetails'),"tbl_studentapplication.IDApplication = tbl_studentpaymentdetails.IDApplication AND tbl_studentapplication.Payment =1 AND tbl_studentpaymentdetails.companyflag = 0",	array('tbl_studentpaymentdetails.Amount AS grossAmount','tbl_studentpaymentdetails.ChequeNo AS transactionId','tbl_studentpaymentdetails.UpdDate AS PaymentDate'))
						-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_studentapplication.IDApplication AND tbl_studentpaymentoption.companyflag = 0 AND tbl_studentpaymentoption.ModeofPayment !=2 ",	array('tbl_studentpaymentoption.ModeofPayment'))
						-> joinLeft(array('tbl_bank' => 'tbl_bank'),"tbl_bank.IdBank=tbl_studentpaymentdetails.BankName",	array('tbl_bank.BankName'))
						-> group('tbl_studentapplication.IDApplication')
						-> order('tbl_studentapplication.FName');						
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}	
public function fngetcompanyotherpaymentdetails($idBatchRegistration){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
						-> from(array('tbl_batchregistration' => 'tbl_batchregistration'),array("tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates"))						
						-> join(array('tbl_studentpaymentdetails' => 'tbl_studentpaymentdetails'),"tbl_batchregistration.idBatchRegistration = tbl_studentpaymentdetails.IDApplication  AND tbl_batchregistration.idBatchRegistration= ".$idBatchRegistration,array('tbl_studentpaymentdetails.Amount AS grossAmount','tbl_studentpaymentdetails.ChequeNo AS transactionId','tbl_studentpaymentdetails.UpdDate AS PaymentDate'))
						-> join(array('tbl_companies' => 'tbl_companies'),"tbl_companies.IdCompany = tbl_batchregistration.idCompany",array('tbl_companies.CompanyName','tbl_companies.ShortName','tbl_companies.Email','tbl_companies.IdCompany'))
						-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration AND  tbl_studentpaymentoption.ModeofPayment !=2 AND tbl_studentpaymentoption.companyflag = 1 ",	array('tbl_studentpaymentoption.ModeofPayment'))
						-> joinLeft(array('tbl_bank' => 'tbl_bank'),"tbl_bank.IdBank=tbl_studentpaymentdetails.BankName",	array('tbl_bank.BankName'))
						-> where('tbl_studentpaymentdetails.companyflag =1 ')
						-> where('tbl_batchregistration.idBatchRegistration= '.$idBatchRegistration);						
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}	
public function fngettakafulotherpaymentdetails($idBatchRegistration){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
						-> from(array('tbl_batchregistration' => 'tbl_batchregistration'),array("tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates"))						
						-> join(array('tbl_studentpaymentdetails' => 'tbl_studentpaymentdetails'),"tbl_batchregistration.idBatchRegistration = tbl_studentpaymentdetails.IDApplication  AND tbl_batchregistration.idBatchRegistration= ".$idBatchRegistration,array('tbl_studentpaymentdetails.Amount AS grossAmount','tbl_studentpaymentdetails.ChequeNo AS transactionId','tbl_studentpaymentdetails.UpdDate AS PaymentDate'))
						-> join(array('tbl_takafuloperator' => 'tbl_takafuloperator'),"tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany ",array('tbl_takafuloperator.TakafulName','tbl_takafuloperator.TakafulShortName','tbl_takafuloperator.email','tbl_takafuloperator.idtakafuloperator'))
						-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration AND  tbl_studentpaymentoption.ModeofPayment !=2 AND tbl_studentpaymentoption.companyflag = 2 ",	array('tbl_studentpaymentoption.ModeofPayment'))
						-> joinLeft(array('tbl_bank' => 'tbl_bank'),"tbl_bank.IdBank=tbl_studentpaymentdetails.BankName",	array('tbl_bank.BankName'))
						-> where('tbl_studentpaymentdetails.companyflag =2 ')
						-> where('tbl_batchregistration.idBatchRegistration= '.$idBatchRegistration);						
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}	
	
	public function fngettakafulpayleterpaymentdetails($idBatchRegistration){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
						-> from(array('tbl_batchregistration' => 'tbl_batchregistration'),array("tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates"))						
						-> join(array('tbl_takafuloperator' => 'tbl_takafuloperator'),"tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany ",array('tbl_takafuloperator.TakafulName','tbl_takafuloperator.TakafulShortName','tbl_takafuloperator.email','tbl_takafuloperator.idtakafuloperator'))
						-> join(array('tbl_studentpaymentoption' => 'tbl_studentpaymentoption'),"tbl_studentpaymentoption.IDApplication = tbl_batchregistration.idBatchRegistration AND  tbl_studentpaymentoption.ModeofPayment !=2 AND tbl_studentpaymentoption.companyflag = 2 ",	array('tbl_studentpaymentoption.ModeofPayment'))
						-> where('tbl_batchregistration.idBatchRegistration= '.$idBatchRegistration);						
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 
	}	
	
public function fngetcompanysttudentsdetails($idBatchRegistration){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
						-> from(array('tbl_batchregistration' => 'tbl_batchregistration'),array("tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates"))						
						-> join(array('tbl_registereddetails' => 'tbl_registereddetails'),"tbl_batchregistration.registrationPin = tbl_registereddetails.RegistrationPin AND tbl_batchregistration.idBatchRegistration= ".$idBatchRegistration,	array('tbl_registereddetails.Regid'))								
						-> join(array('tbl_studentapplication' => 'tbl_studentapplication'),"tbl_studentapplication.IDApplication = tbl_registereddetails.IDApplication ",	array('tbl_studentapplication.FName','tbl_studentapplication.ICNO','tbl_studentapplication.DateOfBirth','tbl_studentapplication.DateTime','tbl_studentapplication.EmailAddress','tbl_studentapplication.IDApplication'))
						-> join(array('tbl_programmaster' => 'tbl_programmaster'),"tbl_programmaster.IdProgrammaster = tbl_studentapplication.Program",	array('tbl_programmaster.ProgramName'))
						-> join(array('tbl_center' => 'tbl_center'),"tbl_center.idcenter = tbl_studentapplication.Examvenue ",array('tbl_center.centername'))
						-> join(array('tbl_managesession' => 'tbl_managesession'),"tbl_studentapplication.Examsession = tbl_managesession.idmangesession",array('tbl_managesession.managesessionname','tbl_managesession.starttime','tbl_managesession.endtime'))								
						-> where('tbl_batchregistration.idBatchRegistration= '.$idBatchRegistration);							
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult; 	
	}	
}