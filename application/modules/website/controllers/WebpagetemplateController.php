<?php
class Website_WebpagetemplateController extends Base_Base 
{
	private $lobjwebtemplate;//db variable
	private $lobjwebtemplateform;//Form variable
	private $_gobjlogger;
	
	// initialisation function
	public function init() 
	{		
		 $auth = Zend_Auth::getInstance();
		$role = $auth->getIdentity()->IdRole;
		 if($role == 169)
		 {
		 	$this->_helper->layout()->setLayout('/web/usty1');
		 }
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    $this->fnsetObj(); //call fnsetObj
   	    
	}
	
	//Function to set the objects
	public function fnsetObj()
	{					
		$this->lobjwebtemplate = new Website_Model_DbTable_Webpagetemplatemodel(); //intialize user db object
		$this->lobjwebtemplateform = new Website_Form_Webpagetemplate(); //intialize user Form
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
    //function to set and display the result
	public function indexAction() 
	{   	
		$this->view->lobjform = $this->lobjform; //send the Form  to the view
		$larrresult = $this->lobjwebtemplate->fngetwebtemplatelist(); //get template details		
		
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->programmasterpaginatorresult); // clear the search session
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->programmasterpaginatorresult)) 
		  {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->programmasterpaginatorresult,$lintpage,$lintpagecount);
		  } 
		else 
		  {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		  }
		
		// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		{
			$larrformData = $this->_request->getPost (); // get the form data  		    									   
		    unset ( $larrformData ['Search'] ); //unset the Search data in form data  	
		    $larrresult = $this->lobjwebtemplate->fnSearchwebtemplate ( $larrformData); //searching the values for the template							
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->programmasterpaginatorresult = $larrresult;
			
		}		
		// check if the user has clicked the Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/website/webpagetemplate/index');			
		}		
	}
	
	//function to add template
	public function addAction() 
	{   
		$this->view->lobjwebtemplateform = $this->lobjwebtemplateform;	//send the Form  to the view		
		// check if the user has clicked the save				
		if ($this->_request->isPost() && $this->_request->getPost('Save'))
		{
			$larrformData = $this->_request->getPost();	// get the form data  			
			unset ( $larrformData ['Save'] ); //unset the Save data in form data  		
			$this->lobjwebtemplate->fnaddwebtemplate($larrformData); // insert into the DB
			
			$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully added the webtemplate"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
			$this->_redirect( $this->baseUrl . '/website/webpagetemplate/index');	//redirect to index page after inserting			
        }     
    }
    
    //function to update template
	public function editAction()
	{   	
		$this->view->lobjwebtemplateform = $this->lobjwebtemplateform;	 //send the Form to the view
		$lintid=$this->_Getparam("id"); // get the id of the template to be updated			
		$larrresult = $this->lobjwebtemplate->fneditwebtemplate($lintid); // get the details from the DB into array variable
		/*print_r($larrresult);
		die();*/			
		$this->view->lobjwebtemplateform->populate($larrresult); // populate the result into the form
					
    	if ($this->_request->isPost() && $this->_request->getPost('Save'))
    	 {       	 			 	
    		$larrformData = $this->_request->getPost();	// get the form data     		    		    	
	   		$lintId = $lintid; //store the id to be updated   	   		
	   		unset ($larrformData ['Save']);	//unset the Save data in form data  	   		
			$this->lobjwebtemplate->fnupdatewebtemplate($lintId,$larrformData); //update webtemplate
			
			$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully edited the webtemplate with id = ".$lintid."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
			$this->_redirect( $this->baseUrl . '/website/webpagetemplate/index');	 //redirect to index page after updating						
    	}
	  }
}