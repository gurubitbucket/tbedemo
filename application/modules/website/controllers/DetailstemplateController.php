<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Website_DetailstemplateController extends Base_Base {
	//private $gobjsessionsis; //class session global variable
	//private $gintPageCount;
	private $_gobjlogger;
	
	public function init() { //initialization function
		$this->gsessionbatch = Zend_Registry::get('sis'); 		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
		$this->_helper->layout()->setLayout('/default/usty');
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		//$this->fnsetObj();
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    
		
	}

	public function indexAction() {			
 		$lobjsearchform = new App_Form_Search(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view				
        
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->detailstemplatepaginatorresult);
		
		$lobjemailTemplateModel = new Website_Model_DbTable_Detailstemplate();  // email template model object
		$larrresult = $lobjemailTemplateModel->fnGetTemplateDetails(); // get template details	
			
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->detailstemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->detailstemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {	
					$larrresult = $lobjemailTemplateModel->fnSearchTemplate($lobjsearchform->getValues());										
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		$this->gobjsessionsis->detailstemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/website/emailtemplate');
		 $this->_redirect( $this->baseUrl . '/website/detailstemplate/index');
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function newtemplateAction() { 	
 		$lobjemailTemplateForm = new Website_Form_Detailstemplate();  // email template form
		$this->view->form = $lobjemailTemplateForm;	
		$auth = Zend_Auth::getInstance();
		$this->view->form->UpdUser->setValue ( $auth->getIdentity()->iduser);	
//dojo.require("dijit.editor._plugins.TextColor");
		
	    $lobjemailTemplateModel = new Website_Model_DbTable_Detailstemplate();	// model object	
		$larrDropdownValues = $lobjemailTemplateModel->fnGetWebPageTemplate();  
		$this->view->form->iddefinition->addMultiOptions($larrDropdownValues);
		$this->view->form->languagess->addMultiOptions(array('0'=>'English','1'=>'Bahasa-Malay','2'=>'Others'));
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) { // save opeartion
			$larrformData = $this->_request->getPost();							
			if ($lobjemailTemplateForm->isValid($larrformData)) {		
				$editorData = $larrformData['content1'];	
				//echo 	$editorData;die(); 								   
			    $lintresult = $lobjemailTemplateModel->fnAddEmailTemplate($lobjemailTemplateForm->getValues(),$editorData); // add method	

			    $auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully added the Template"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
			  //  $this->_redirect($this->view->url(array('module'=>'website' ,'controller'=>'emailtemplate', 'action'=>'index'),'default',true));        
			 $this->_redirect( $this->baseUrl . '/website/detailstemplate/index');
			}			
		}
	}
public function newtemplate1Action() { 	
 		$lobjemailTemplateForm = new Website_Form_Detailstemplate();  // email template form
		$this->view->form = $lobjemailTemplateForm;	
		$auth = Zend_Auth::getInstance();
		$this->view->form->UpdUser->setValue ( $auth->getIdentity()->iduser);	
//dojo.require("dijit.editor._plugins.TextColor");
		
	    $lobjemailTemplateModel = new Website_Model_DbTable_Detailstemplate();	// model object	
		$larrDropdownValues = $lobjemailTemplateModel->fnGetWebPageTemplate();  
		$this->view->form->iddefinition->addMultiOptions($larrDropdownValues);
		$this->view->form->languagess->addMultiOptions(array('0'=>'English','1'=>'Bahasa-Malay'));
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) { // save opeartion
			$larrformData = $this->_request->getPost();							
			if ($lobjemailTemplateForm->isValid($larrformData)) {		
				$editorData = $larrformData['content1'];	
				//echo 	$editorData;die(); 								   
			    $lintresult = $lobjemailTemplateModel->fnAddEmailTemplate($lobjemailTemplateForm->getValues(),$editorData); // add method			
			  //  $this->_redirect($this->view->url(array('module'=>'website' ,'controller'=>'emailtemplate', 'action'=>'index'),'default',true));
			  $auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully added the Template"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);        
			 $this->_redirect( $this->baseUrl . '/website/detailstemplate/index');
			}			
		}
	}

	public function edittemplateAction() {		
 		
 		$idTemplate = $this->_getParam('id');
 		$lobjemailTemplateForm = new Website_Form_Detailstemplate(); // email template form
		$this->view->form = $lobjemailTemplateForm; 
		$this->view->form->iddetails->setValue($idTemplate);
		$this->view->id = $idTemplate;
		$auth = Zend_Auth::getInstance();
		$this->view->form->UpdUser->setValue($auth->getIdentity()->iduser);	
			$this->view->form->languagess->addMultiOptions(array('0'=>'English','1'=>'Bahasa-Malay'));
        if ($this->_request->isPost() && $this->_request->getPost('Save')) {  // update operation	
            $larrformData = $this->_request->getPost(); 
          /*  print_r($larrformData);
            die();*/
            unset($larrformData['Save']);            				
            if ($lobjemailTemplateForm->isValid($larrformData)) {
            		$idTemplate = $larrformData['iddetails'];
                	//$where = 'idTemplate = '. $idTemplate;					           
				 	$lobjemailTemplateModel = new Website_Model_DbTable_Detailstemplate();	
				 	$editorData = $larrformData['TemplateBody'];	
				 	$Templatebanhasa = $larrformData['Templatebanhasa'];	
				 		
				 	//echo $idTemplate;die();					 	
                    $lobjemailTemplateModel->fnUpdateTemplate($idTemplate,$larrformData,$editorData,$Templatebanhasa); // update email template details	

                    $auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully edited the Template with id = ".$idTemplate."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
                    $this->_redirect( $this->baseUrl . '/website/detailstemplate/index');
                    // $this->_redirect($this->view->url(array('module'=>'website' ,'controller'=>'emailtemplate', 'action'=>'index'),'default',true)); 
				} 			
        } else {
        		// 	get external panel row values by id and populate
				$lobjemailTemplateModel = new Website_Model_DbTable_Detailstemplate(); //email template model object
		        $larrresult = $lobjemailTemplateModel->fnViewTemplte($idTemplate);
		       /* print_r($larrresult);
		        die();*/
		$larrDropdownValues = $lobjemailTemplateModel->fnGetWebPageTemplate();  
		//$this->view->form->iddefinition->addMultiOptions($larrDropdownValues);
		      $this->view->form->iddefinition->addMultiOption($larrresult['iddefinition'],$larrresult['DefinitionDesc']);
		       $this->view->form->languagess->setValue($larrresult['language']);	
		      
		       	$this->view->TemplateBody = $larrresult['content'];
		       	 	$this->view->Templatebanhasa = $larrresult['banhasa'];
		       	
        }  	
  	}

  	
  	public function fngetlanguageAction()
  	{
  		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$language =$this->_getParam('language');
		$tempid =$this->_getParam('tempid');
		$lobjemailTemplateModel = new Website_Model_DbTable_Detailstemplate();	 

		$larrlangDetails = $lobjemailTemplateModel->fngetlanguagedetails($tempid);
		
		if($language==0)
		{
			echo $larrlangDetails['content'];
			exit;
		}
  	   if($language==1)
		{
			echo $larrlangDetails['banhasa'];
			exit;
		}
  	}
}