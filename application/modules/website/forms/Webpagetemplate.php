<?php
class Website_Form_Webpagetemplate extends Zend_Dojo_Form 
{    
	
    public function init() 
    {    
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    		
    	$idwebpagetemplate = new Zend_Form_Element_Hidden('idwebpagetemplate');
        $idwebpagetemplate->removeDecorator("DtDdWrapper");
        $idwebpagetemplate->removeDecorator("Label");
        $idwebpagetemplate->removeDecorator('HtmlTag');
        
        $english = new Zend_Form_Element_Text('content');	
		$english->setAttrib('dojoType',"dijit.form.ValidationTextBox");		
        $english->setAttrib('required',"true");       			        		  
        $english->removeDecorator("DtDdWrapper");
        $english->removeDecorator("Label");
        $english->removeDecorator('HtmlTag');
        		
       	$malay = new Zend_Form_Element_Text('banhasa');
		$malay->setAttrib('dojoType',"dijit.form.ValidationTextBox");    			         				      
        $malay->removeDecorator("DtDdWrapper");
        $malay->removeDecorator("Label");
        $malay->removeDecorator('HtmlTag');
        
       	$others= new Zend_Form_Element_Text('others');
		$others->setAttrib('dojoType',"dijit.form.ValidationTextBox");    			         				      
        $others->removeDecorator("DtDdWrapper");
        $others->removeDecorator("Label");
        $others->removeDecorator('HtmlTag');        

        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
         $Save = new Zend_Form_Element_Submit('Save');
         $Save->dojotype="dijit.form.Button";
         $Save->label = $gstrtranslate->_("Save");
         $Save->setAttrib('class', 'NormalBtn');
         $Save->setAttrib('id', 'submitbutton');
         $Save->removeDecorator("DtDdWrapper");
         $Save->removeDecorator("Label");
         $Save->removeDecorator('HtmlTag');
           		         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn');
		$Back->removeDecorator("Label");
		$Back->removeDecorator("DtDdWrapper");
		$Back->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($idwebpagetemplate,
        						 $english,
        						 $malay,
        						 $Active,       						
                                 $Save,
                                 $others,
                                 $Back
                                )
                          );

    }
}