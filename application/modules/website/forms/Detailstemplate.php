<?php
class Website_Form_Detailstemplate extends Zend_Dojo_Form {
    public function init() {    	
		//$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	$strSystemDate = date('Y-m-d H:i:s');
    	
   			
        $idTemplate = new Zend_Form_Element_Hidden('iddetails');
        $idTemplate->removeDecorator("DtDdWrapper");
        $idTemplate->removeDecorator("Label");
        $idTemplate->removeDecorator('HtmlTag');

      
            
    
        
        $idDefinition = new Zend_Dojo_Form_Element_FilteringSelect('iddefinition');
        $idDefinition->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			->setRegisterInArrayValidator(false)
        			-> setAttrib('style',"width:166px")
        			 ->setAttrib('dojoType',"dijit.form.FilteringSelect")
        			//->addMultiOptions($larrDropdownValues)        			        			       		        					
        			->setAttrib('required',"true") ;


		//$country->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $language = new Zend_Dojo_Form_Element_FilteringSelect('languagess');
        $language->setAttrib('dojoType',"dijit.form.FilteringSelect")
       			   ->setAttrib('OnChange', 'languagecontent(this.value)')
        			->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');  
        			
        			
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper")
        		->setvalue($strSystemDate)
        		->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        			
      
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = "Save";
		$Save->setAttrib('class', 'NormalBtn');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
        
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
        $Back->label = "Close";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        $this->addElements(array($idTemplate,$UpdDate,$UpdUser,$idDefinition,$Save,$Back,$language));

    }
}