<?php
     class Website_Model_DbTable_Webpagetemplatemodel extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	
	        protected $_name = 'tbl_webpagetemplate';
	
            //function to add the template
            public function fnaddwebtemplate($post)
	        {	         
	           $this->insert($post);	
	        }
	        
	        //Function to get the webpagetemplate details 	
            public function fngetwebtemplatelist()
            {             
   	         $result=$this->fetchAll();
   	         return $result;
            }
 	
             //Function to get the selected auhtor details 			 
            public function fneditwebtemplate($id)     
			{		      		
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		     $lstrSelect =$lobjDbAdpt->select()
									->from(array('b'=>'tbl_webpagetemplate'),array('b.*'))
									->where('idwebpagetemplate='.$id ); 
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
			} 
           
			//Function to update Author details 
           public function fnupdatewebtemplate($id,$udata) 
            {             
	         $where = "idwebpagetemplate = '$id'";
	         $this->update($udata,$where);   
            }  
           
            //Function for searching the author details	
           public function fnSearchwebtemplate($post = array())
	       { $field7 = "Active = ".$post["field7"];	       	  	
	         $select = $this->select()
			                ->from(array('a' => 'tbl_webpagetemplate'),array('a.*'))
			                ->where('a.content  like "%" ? "%"',$post['field2'])
			                ->where($field7);  
		     $result = $this->fetchAll($select);
		     return $result->toArray();
	       }
				
	         //Function To Get Pagination Count from Initial Config
	       public function fnGetPaginationCountFromInitialConfig()
	       {
		       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		       $lintPageCount = "";
		       $lstrSelect = $lobjDbAdpt->select()
								        ->from(array("a"=>"tbl_config"),array("noofrowsingrid") );
		       $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		       if($larrResult['noofrowsingrid'] == "" || $larrResult['noofrowsingrid'] == "0")
		       {
			    $lintPageCount = "5";
		       }
		       else
		       {
			    $lintPageCount = $larrResult['noofrowsingrid'];
		       }
		       return $lintPageCount;
	       }
	
	       //Function to Get Initial Config Data
	       public function fnGetInitialConfigData()
	       {
		    $lobjDbAdpt=Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect=$lobjDbAdpt->select()
								   ->from(array("a"=>"tbl_definationms"),array("LCASE(SUBSTRING(a.DefinitionCode,1,2)) AS Language") )
					 			   ->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType',array())
				 				   ->join(array("c"=>"tbl_config"),'c.Language = a.idDefinition',array("c.HtmlDir","c.DefaultCountry"));		  
		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		    return $larrResult;
	       }	
        }    
		  	 
	
