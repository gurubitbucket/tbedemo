<?php
class Finance_Form_Credittoibfim extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$idProgram = new Zend_Form_Element_Hidden('idProgram');
        $idProgram->removeDecorator("DtDdWrapper");
        $idProgram->removeDecorator("Label");
        $idProgram->removeDecorator('HtmlTag');
        
        $idProgramrate = new Zend_Form_Element_Hidden('idProgramrate');
        $idProgramrate->removeDecorator("DtDdWrapper");
        $idProgramrate->removeDecorator("Label");
        $idProgramrate->removeDecorator('HtmlTag');
        
        $Companyname = new Zend_Form_Element_Text('Companyname');	
		$Companyname->setAttrib('dojoType',"dijit.form.ValidationTextBox")
        //$Companyname->setAttrib('required',"true")       			 
        		//->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $Status = new Zend_Dojo_Form_Element_FilteringSelect('Active');
        $Status->removeDecorator("DtDdWrapper");
        				//->addMultioption('','Select');
        $Status->setAttrib('required',"true") ;
        $Status->removeDecorator("Label");
        $Status->removeDecorator('HtmlTag');
        $Status->setRegisterInArrayValidator(false);
		$Status->setAttrib('dojoType',"dijit.form.FilteringSelect");	
        	
	    $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class','NormalBtn');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = $gstrtranslate->_("Clear");
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator('HtmlTag');
		
        
        $Search = new Zend_Form_Element_Submit('Search');
		//$submit->setAttrib('Onclick', 'fncheckactiveornot()');
		$Search->dojotype="dijit.form.Button";
		$Search->label = $gstrtranslate->_("Search");
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator("Label");
		$Search->removeDecorator('HtmlTag');
		$Search->class = "NormalBtn";
		
       	$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype="dijit.form.Button";
		$Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('Onclick', 'return validateform()');		
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator("Label");
		$Save->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($idProgram,
        						 $idProgramrate,
								 $Companyname,
								 $Status,
								 $Clear,
								 $Search,
								 $Save
        						 ));

    }
}