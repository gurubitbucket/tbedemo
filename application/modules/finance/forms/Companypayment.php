<?php

class Finance_Form_Companypayment extends Zend_Dojo_Form 
{		
    public function init()
    { 
       						
        $CompanyName	=   new Zend_Form_Element_Text('CompanyName',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
        $CompanyName  ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $CompanyName	->	removeDecorator("DtDdWrapper");
        $CompanyName	->	removeDecorator("Label");
        $CompanyName	->	removeDecorator('HtmlTag');
        $CompanyName	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');         
        
               			
        $Search 		= 	new Zend_Form_Element_Submit('Search');
        $Search			->	setAttrib('class','NormalBtn');
        $Search			->	setAttrib('id', 'submitbutton');
        $Search			->	removeDecorator("DtDdWrapper");
        $Search			->	removeDecorator("Label");
        $Search			->	removeDecorator('HtmlTag');
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = ("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";
        
        $ChequeDate = new Zend_Dojo_Form_Element_DateTextBox('ChequeDt');
        $ChequeDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$ChequeDate->setAttrib('required',"true");
        $ChequeDate->removeDecorator("DtDdWrapper");
        $ChequeDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");;
        $ChequeDate->removeDecorator("Label");
        $ChequeDate->removeDecorator('HtmlTag'); 
               			
        $ChequeNo = new Zend_Form_Element_Text('ChequeNo');
        $ChequeNo  ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ChequeNo	->	removeDecorator("DtDdWrapper");
        $ChequeNo	->	removeDecorator("Label")
                    ->setAttrib('required',"true");
        $ChequeNo	->	removeDecorator('HtmlTag')
               			->	setAttrib('class', 'txt_put');

        $BankName = new Zend_Dojo_Form_Element_FilteringSelect('BankName');       
        $BankName  ->  setAttrib('dojoType',"dijit.form.FilteringSelect");
        $BankName	->	removeDecorator("DtDdWrapper");
        $BankName	->	removeDecorator("Label")
                     ->setAttrib('required',"true");                   
        $BankName	->	removeDecorator('HtmlTag')
               			->	setAttrib('class', 'txt_put');      			
               			
               			
         
        $Amount  = new Zend_Form_Element_Text('Amount',array('regExp'=>"[0-9]+[.]?[0-9]{0,2}",'invalidMessage'=>"Enter upto two decimal point"));
        $Amount  -> setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount	 ->	removeDecorator("DtDdWrapper");
        $Amount	 ->	removeDecorator("Label")
                 -> setAttrib('required',"true");
        $Amount	 ->	removeDecorator('HtmlTag');
        $Amount	 ->	setAttrib('class', 'txt_put'); 
               	    			
   		$this->addElements(array($CompanyName,$ChequeNo,$BankName,$Search,$ChequeDate,$Save,$Amount));
    }
}
        
        