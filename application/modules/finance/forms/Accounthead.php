<?php

class Finance_Form_Accounthead extends Zend_Dojo_Form 
{		
    public function init()
    { 
       						
        $AccountName1	=   new Zend_Form_Element_Text('AccountName1',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
        $AccountName1   ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AccountName1	->	removeDecorator("DtDdWrapper");
        $AccountName1	->	removeDecorator("Label");
        $AccountName1	->	removeDecorator('HtmlTag');
        $AccountName1	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');         
        
      

        $Active1 		= 	new Zend_Form_Element_Checkbox('Active1');
        //$Active			->	setAttrib('onClick','ToggleSelectBoxs()');
        $Active1        ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $Active1		->	setAttrib("checked","checked");
         $Active1		->	removeDecorator("DtDdWrapper");
        $Active1		->	removeDecorator("Label");
        $Active1		->	removeDecorator('HtmlTag');				
      
    
        $Search 		= 	new Zend_Form_Element_Submit('Search');
        $Search			->	setAttrib('class','NormalBtn');
        $Search			->	setAttrib('id', 'submitbutton');
        $Search			->	removeDecorator("DtDdWrapper");
        $Search			->	removeDecorator("Label");
        $Search			->	removeDecorator('HtmlTag');
         
   		$this->addElements(array($AccountName1,$Active1,$Search));
    }
}
        
        