<?php
class Finance_Form_Accountmasterentry extends Zend_Dojo_Form
{		
    public function init()
    { $gstrtranslate =Zend_Registry::get('Zend_Translate'); 
        $UpdDate 		= 	new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate		->	removeDecorator("DtDdWrapper");
        $UpdDate		->	removeDecorator("Label");
        $UpdDate		->	removeDecorator('HtmlTag');
     
        $UpdUser 		= 	new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser		->	removeDecorator("DtDdWrapper");
        $UpdUser		->	removeDecorator("Label");
        $UpdUser		->	removeDecorator('HtmlTag');
        
        $idConfig 		= 	new Zend_Form_Element_Hidden('idConfig');
        $idConfig		->	removeDecorator("DtDdWrapper");
        $idConfig		->	removeDecorator("Label");
        $idConfig		->	removeDecorator('HtmlTag');                    
         
/*        $IdGroup	 	= 	new Zend_Form_Element_Select('IdGroup');
       	$IdGroup		->	addMultiOptions('', 'Select');
        $IdGroup		->	setAttrib('class', 'txt_put MakeEditable')
						->	setAttrib('style','width:150px;')        		        
						->	removeDecorator("DtDdWrapper")
						->	removeDecorator("Label") 				
						->	removeDecorator('HtmlTag')
						->  setAttrib('dojoType',"dijit.form.FilteringSelect");*/
						
						
								
						
						
        $AccountName	=   new Zend_Form_Element_Text('AccountName');
        $AccountName    ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AccountName	->	removeDecorator("DtDdWrapper");
        $AccountName	->	removeDecorator("Label");
        $AccountName	->	removeDecorator('HtmlTag');
        $AccountName	->	setAttrib('required',"true");         
        
        $AccShortName	= 	new Zend_Form_Element_Text('AccShortName');
        $AccShortName   ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AccShortName	->	removeDecorator("DtDdWrapper");
        $AccShortName	->	removeDecorator("Label");
        $AccShortName	->	removeDecorator('HtmlTag');
        $AccShortName	->  setAttrib('required',"true"); 
        				 
        $PrefixCode		= 	new Zend_Form_Element_Text('PrefixCode');
        $PrefixCode     ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $PrefixCode		->	removeDecorator("DtDdWrapper");
        $PrefixCode		->	removeDecorator("Label");
        $PrefixCode		->	removeDecorator('HtmlTag');
       		//->	setAttrib('style','width:155px;')
        $PrefixCode		->	setAttrib('class', 'txt_put');
        				
        				//->  setValue('xxx-xxx-xxx'); 

        $Description	= 	new Zend_Form_Element_Text('Description');
        $Description    ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Description	->	removeDecorator("DtDdWrapper");
        $Description	->	removeDecorator("Label");
        $Description	->	removeDecorator('HtmlTag');
       // $Description	->	setAttrib('class', 'txt_put'); 	
        
        
        				
        
        				
       
        
        $duringRegistration 		= 	new Zend_Form_Element_Checkbox('duringRegistration');
        //$Active		->	setAttrib('onClick','ToggleSelectBoxs()');.CheckBox
        $duringRegistration     ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $duringRegistration		->	removeDecorator("DtDdWrapper");
        $duringRegistration		->	removeDecorator("Label");
        $duringRegistration		->	removeDecorator('HtmlTag');
        
        

        
        
        $programtype		= 	new Zend_Form_Element_Checkbox('programtype');
        //$Active			->	setAttrib('onClick','ToggleSelectBoxs()');
        $programtype        ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $programtype		->	removeDecorator("DtDdWrapper");
        $programtype		->	removeDecorator("Label");
        $programtype		->	removeDecorator('HtmlTag');
        				   // ->	setValue("0");		
        				
        				
        $coursetype 		= 	new Zend_Form_Element_Checkbox('coursetype');
        //$Active			->	setAttrib('onClick','ToggleSelectBoxs()');
        $coursetype         ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $coursetype		->	removeDecorator("DtDdWrapper");
        $coursetype		->	removeDecorator("Label");
        $coursetype		->	removeDecorator('HtmlTag');
        				//->	setValue("0");		
        				
        				
        $Active 		= 	new Zend_Form_Element_Checkbox('Active');
        //$Active			->	setAttrib('onClick','ToggleSelectBoxs()');
        $Active         ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $Active			->	removeDecorator("DtDdWrapper");
        $Active			->	removeDecorator("Label");
        $Active			->	removeDecorator('HtmlTag')
        				->	setValue("1");		
        						
        $Save 			= 	new Zend_Form_Element_Submit('Save');
       $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('class', 'NormalBtn');
        $Save			->	setAttrib('id', 'submitbutton');
        $Save			->	removeDecorator("DtDdWrapper");
        $Save			->	removeDecorator("Label");
        $Save			->	removeDecorator('HtmlTag');
        
        $Close 			= 	new Zend_Form_Element_Submit('Close');
		 $Close->dojotype="dijit.form.Button";
        $Close->label = $gstrtranslate->_("Close");
		$Close->setAttrib('class', 'NormalBtn')
						->	setAttrib('onclick', 'fnCloseLyteBox()')
						->	removeDecorator("Label")
						->	removeDecorator("DtDdWrapper")
						->	removeDecorator('HtmlTag');
         
   		$this->addElements(array($UpdDate,$UpdUser,$AccountName,$AccShortName,$PrefixCode,$Description,$programtype,$coursetype,$duringRegistration,$Active,$Save,$Close));
    }
}
        
        