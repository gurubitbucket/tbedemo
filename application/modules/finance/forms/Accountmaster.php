<?php

class Finance_Form_Accountmaster extends Zend_Dojo_Form
{		
    public function init()
    { 
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
       						
        $AccountName1	=   new Zend_Form_Element_Text('AccountName1');
        $AccountName1   ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AccountName1	->	removeDecorator("DtDdWrapper");
        $AccountName1	->	removeDecorator("Label");
        $AccountName1	->	removeDecorator('HtmlTag');
        $AccountName1	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');         
        
        $AccShortName1	= 	new Zend_Form_Element_Text('AccShortName1');
        $AccShortName1  ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AccShortName1	->	removeDecorator("DtDdWrapper");
        $AccShortName1	->	removeDecorator("Label");
        $AccShortName1	->	removeDecorator('HtmlTag');
        $AccShortName1	->	setAttrib('style','width:155px;')
        				->	setAttrib('class', 'txt_put');   

        $Active1 		= 	new Zend_Form_Element_Checkbox('Active1');
        //$Active			->	setAttrib('onClick','ToggleSelectBoxs()');
        $Active1        ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $Active1		->	setAttrib("checked","checked");
        $Active1		->	removeDecorator("DtDdWrapper");
        $Active1		->	removeDecorator("Label");
        $Active1		->	removeDecorator('HtmlTag');				
      
    
        $Search 		= 	new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search			->	setAttrib('class','NormalBtn');
        $Search			->	setAttrib('id', 'submitbutton');
        $Search			->	removeDecorator("DtDdWrapper");
        $Search			->	removeDecorator("Label");
        $Search			->	removeDecorator('HtmlTag');
         
   		$this->addElements(array($AccountName1,$AccShortName1,$Active1,$Search));
    }
}
        
        