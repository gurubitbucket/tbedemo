<?php
class Finance_Form_Fpxpayment extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
				      
        $paymentFee = new Zend_Form_Element_Text('paymentFee');
		$paymentFee->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $paymentFee->setAttrib('maxlength','10')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $payerId = new Zend_Form_Element_Text('payerId');
		$payerId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $payerId->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $verifySign = new Zend_Form_Element_Text('verifySign');
		$verifySign->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $verifySign->setAttrib('maxlength','255')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $transactionId = new Zend_Form_Element_Text('transactionId');
		$transactionId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $transactionId->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $Confirm = new Zend_Form_Element_Submit('Confirm');
        $Confirm->	setAttrib('onclick', 'return validateform()');
        $Confirm->dojotype="dijit.form.Button";
        $Confirm->label = $gstrtranslate->_("Confirm");
        $Confirm->removeDecorator("DtDdWrapper");
        $Confirm->removeDecorator("Label");
        $Confirm->removeDecorator('HtmlTag')
         		->class = "NormalBtn";				 		

        //form elements
        $this->addElements(array($paymentFee,$payerId,$transactionId,$verifySign,$UpdDate,$UpdUser,$Confirm
        						 ));

    }
}