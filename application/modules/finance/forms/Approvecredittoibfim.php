<?php

class Finance_Form_Approvecredittoibfim extends Zend_Dojo_Form 
{		
    public function init()
    { 
       						
        $CompanyName	=   new Zend_Form_Element_Text('CompanyName',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
        $CompanyName  ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $CompanyName	->	removeDecorator("DtDdWrapper");
        $CompanyName	->	removeDecorator("Label");
        $CompanyName	->	removeDecorator('HtmlTag');
        $CompanyName	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');  

        $Regpin = new Zend_Form_Element_Text('Regpin');	
		$Regpin->setAttrib('dojoType',"dijit.form.ValidationTextBox")
        //$Companyname->setAttrib('required',"true")       			 
        		//->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
				
        $Update = new Zend_Form_Element_Hidden('UpdDate');
        $Update	->removeDecorator("DtDdWrapper")
        			//->setvalue($strSystemDate)
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');
        		 	 
	    $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
		$UpdUser->setAttrib('id','UpdUser')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
				 	->removeDecorator('HtmlTag');
               			
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class','NormalBtn');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = ("Clear");
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator('HtmlTag');
               			
         $Search = new Zend_Form_Element_Submit('Search');
		//$submit->setAttrib('Onclick', 'fncheckactiveornot()');
		$Search->dojotype="dijit.form.Button";
		$Search->label = ("Search");
		$Search->removeDecorator("DtDdWrapper");
		$Search->removeDecorator("Label");
		$Search->removeDecorator('HtmlTag');
		$Search->class = "NormalBtn";
		
       	$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype="dijit.form.Button";
		$Save->label = ("Save");
		//$Save->setAttrib('Onclick', 'return validateform()');	
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator("Label");
		$Save->removeDecorator('HtmlTag');
               	    			
   		$this->addElements(array($CompanyName,$Clear,$Search,$Save,$Update,$UpdUser,$Regpin));
    }
}
        
        