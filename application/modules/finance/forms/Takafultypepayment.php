<?php

class Finance_Form_Takafultypepayment extends Zend_Dojo_Form 
{		
    public function init()
    { 

    	$Update = new Zend_Form_Element_Hidden('upddate');
        $Update	->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');
        		 	 
		$UpdUser = new Zend_Form_Element_Hidden('upduser');
		$UpdUser->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
				 	->removeDecorator('HtmlTag');

		$idtakafulpaymenttype = new Zend_Form_Element_Hidden('idtakafulpaymenttype');
		$idtakafulpaymenttype	->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
							
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = ("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";

        $idtakafuloperator = new Zend_Dojo_Form_Element_FilteringSelect('idtakafuloperator');       
        $idtakafuloperator->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $idtakafuloperator->removeDecorator("DtDdWrapper");
        $idtakafuloperator->removeDecorator("Label")
                    ->setAttrib('required',"true")                   
       				->removeDecorator('HtmlTag')
               		->setAttrib('class', 'txt_put');     

        $paymenttype = new Zend_Dojo_Form_Element_FilteringSelect('paymenttype');       
        $paymenttype->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $paymenttype->removeDecorator("DtDdWrapper");
        $paymenttype->removeDecorator("Label")
                    ->setAttrib('required',"true")                   
       				->removeDecorator('HtmlTag')
               		->setAttrib('class', 'txt_put');               		

               	    			
   		$this->addElements(array($idtakafuloperator,$paymenttype,$Save,$idtakafulpaymenttype,$UpdUser,$Update));
    }
}
        
        