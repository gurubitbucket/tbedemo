<?php
class Finance_Form_Paymentcancelation extends Zend_Dojo_Form {
    public function init() {

    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$CT= new Zend_Dojo_Form_Element_FilteringSelect('CT');
        $CT	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('1'=>'Company','2'=>'Takaful'))
							->setRegisterInArrayValidator(false)
							//->setAttrib('onChange','fnlist(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
        
        $Company = new Zend_Form_Element_Text('Company');
        $Company->setAttrib('dojoType',"dijit.form.ValidationTextBox")
        //$Company->setAttrib('style','width:700px');
        //$Company->setAttrib('required',"true")       			        
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag'); 
					
        $Regpin = new Zend_Form_Element_Text('Regpin');
        $Regpin->setAttrib('dojoType',"dijit.form.ValidationTextBox")
        //$Regpin->setAttrib('style','width:700px');
        //$Regpin->setAttrib('required',"true")       			        
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag'); 					
			
		/*$Company= new Zend_Dojo_Form_Element_FilteringSelect('Company');
        $Company	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							//->addmultioption(array('0'=>'Select All'))
							->setRegisterInArrayValidator(false)
							//->setAttrib('onChange','fnlist(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
        
        $Takaful= new Zend_Dojo_Form_Element_FilteringSelect('Takaful');
        $Takaful	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							//->addmultioption(array('0'=>'Select All'))
							->setRegisterInArrayValidator(false)
							//->setAttrib('onChange','fnlist(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
							*/
    	
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
       	$Questions = new Zend_Form_Element_Text('Questions');
        $Questions->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Questions->setAttrib('style','width:700px');
        $Questions->setAttrib('required',"true")       			        
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag'); 
        		
         
        $this->addElements(array($Company,$Regpin,$UpdDate,$UpdUser,$CT,                           
                                 $Save,$Questions));
    }
}
