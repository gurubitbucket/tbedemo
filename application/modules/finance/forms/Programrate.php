<?php
class Finance_Form_Programrate extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$idProgram = new Zend_Form_Element_Hidden('idProgram');
        $idProgram->removeDecorator("DtDdWrapper");
        $idProgram->removeDecorator("Label");
        $idProgram->removeDecorator('HtmlTag');
        
        $idProgramrate = new Zend_Form_Element_Hidden('idProgramrate');
        $idProgramrate->removeDecorator("DtDdWrapper");
        $idProgramrate->removeDecorator("Label");
        $idProgramrate->removeDecorator('HtmlTag');
        
        $ProgramName = new Zend_Form_Element_Text('ProgramName');	
		$ProgramName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ProgramName->setAttrib('required',"true")  
                    ->setAttrib('readonly',"true")     			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $url= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
        $regex="/\/edit\/[0-9]*/";
    	if(preg_match($regex,$url))
        {		
			$IdAccountmaster = new Zend_Dojo_Form_Element_FilteringSelect('IdAccountmaster');
			$IdAccountmaster->removeDecorator("DtDdWrapper");
			$IdAccountmaster->setAttrib('required',"true")
			                ->setAttrib('readonly',"true") ;
			$IdAccountmaster->removeDecorator("Label");
			$IdAccountmaster->removeDecorator('HtmlTag');
			$IdAccountmaster->setRegisterInArrayValidator(false);
			$IdAccountmaster->setAttrib('dojoType',"dijit.form.FilteringSelect");	
	    }
	    else
	    {
			$IdAccountmaster = new Zend_Dojo_Form_Element_FilteringSelect('IdAccountmaster');
			$IdAccountmaster->removeDecorator("DtDdWrapper");
			$IdAccountmaster->setAttrib('required',"true") ;
			$IdAccountmaster->removeDecorator("Label");
			$IdAccountmaster->removeDecorator('HtmlTag');
			$IdAccountmaster->setRegisterInArrayValidator(false);
			$IdAccountmaster->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		}	
        		
       	$Rate = new Zend_Form_Element_Text('Rate',array('regExp'=>"[0-9]*[.]?[0-9]+",'invalidMessage'=>"Only Amount"));
		$Rate->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','100') 
        				->setAttrib('required',"true")    
        				->setAttrib('onkeyup', "javascript:resettax()")     
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        
        $month= date("m"); // Month value
		$day=   date("d"); //today's date
		$year=  date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$currentdate = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
        $EffectiveDate = new Zend_Form_Element_Text('EffectiveDate');
		$EffectiveDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $EffectiveDate->setAttrib('required',"true")  
                 ->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}") 
                 ->setAttrib('constraints',"$currentdate")      			 
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');	
        		
        $ServiceTax = new Zend_Dojo_Form_Element_FilteringSelect('ServiceTax');
        $ServiceTax->removeDecorator("DtDdWrapper");
      			
        $ServiceTax->setAttrib('required',"true")
                    ->setAttrib('onChange', "javascript:calculatetotal(this.value)"); 
        $ServiceTax->removeDecorator("Label");
        $ServiceTax->removeDecorator('HtmlTag');
        $ServiceTax->setRegisterInArrayValidator(false);
		$ServiceTax->setAttrib('dojoType',"dijit.form.FilteringSelect");					
        				
        				
      	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
    		
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($idProgram,
        						 $idProgramrate,
        						 $IdAccountmaster,
        						 $ProgramName,
        						 $Rate,
        						 $EffectiveDate,
                                 $Active,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $Back,
                                 $ServiceTax));

    }
}
