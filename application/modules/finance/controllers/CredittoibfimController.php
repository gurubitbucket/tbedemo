<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Finance_CredittoibfimController extends Base_Base{

	public function init(){
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjCredittoibfimmodel = new Finance_Model_DbTable_Credittoibfim(); //venuescheduler model object
		$this->lobjCredittoibfimform = new Finance_Form_Credittoibfim();
	}

	public function indexAction(){
		
		$this->view->lobjCredittoibfimform = $this->lobjCredittoibfimform;
		$larrstatus[0][key]= 1;
		$larrstatus[0][value]= "Active";
		$larrstatus[1][key]= 0;
		$larrstatus[1][value]= "Inactive";
		$this->view->lobjCredittoibfimform->Active->addMultiOptions($larrstatus); //added program names to form dropdown element
		$ldtsystemDate = date( 'Y-m-d H:i:s' );   //current Date
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' ))
		{ 
			$larrformData = $this->_request->getPost ();
			
			if ($this->lobjform->isValid ( $larrformData )){
			    
				$this->view->activestatus = $larrformData['Active'];
				$larrresult = $this->lobjCredittoibfimmodel->fngetselectedcompany($larrformData); //Call a function to searching the values for the candidates
				$this->view->paginator = $larrresult;
			    $this->lobjCredittoibfimform->populate($larrformData);
				
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' ))
		{
			$this->_redirect( $this->baseUrl . '/finance/credittoibfim/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' ))
		{
			$larrformData = $this->_request->getPost ();	
            	
			$larrresult = $this->lobjCredittoibfimmodel->insertcompanystatus($larrformData['idcompany'],$larrformData['act']); //Call a function to searching the values for the candidates
				
		}
	}

}


