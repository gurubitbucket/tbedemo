<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_CompanytypepaymentController extends Base_Base 
{
    private $_gobjlogger;
    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
    }
    public function fnsetObj()
    {
		$this->lobjCompanytypepaymentModel = new Finance_Model_DbTable_Companytypepayment();
		$this->lobjCompanytypepaymentForm = new Finance_Form_Companytypepayment ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Company Payment Terms";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		/*$larrresult = $this->lobjCompanytypepaymentModel->fngetCompanyDetails(); //get user details
		$companyflag=0;
		$lstrTypeids = $this->_getParam('ids');
      if($lstrTypeids)
        {
        	$larrresult = $this->lobjCompanytypepaymentModel->fngettakafulsdetails();
        	$this->view->lobjform->companyflag->setValue(0);
        	$companyflag=1;
        }
		$this->view->cmpflag=$companyflag;
    if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}*/
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$companyflag=0;
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		    $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				if($larrformData['companyflag']!=1)
				{
					$companyflag=1;
				}
				$this->view->cmpflag=$companyflag;
				if($companyflag==1)
				{
				$larrresult = $this->lobjCompanytypepaymentModel->fnSearchtakafulsePayment($larrformData['field3']); //searching the values for the user	
				}
				else 
				{
				$larrresult = $this->lobjCompanytypepaymentModel->fnSearchCompanyTypePayment($larrformData['field3']); //searching the values for the user
				}
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->semesterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/finance/companytypepayment/index');
			 		//echo "<script>parent.location = '".$this->view->baseUrl()."/finance/companytypepayment/index';</script>";
			}
	
    }
    
    public function companytypepaymentaddAction()
    {
    	
    	$this->view->lobjCompanytypepaymentForm = $this->lobjCompanytypepaymentForm;
    	
    	$larrcompanyname = $this->lobjCompanytypepaymentModel->fnGetcompanyName();
		$this->lobjCompanytypepaymentForm->idcompany->addMultiOptions($larrcompanyname);
		
		$larrcompanyname = $this->lobjCompanytypepaymentModel->fnGetPaymentTerms();
		$this->lobjCompanytypepaymentForm->paymenttype->addMultiOptions($larrcompanyname);
		
		$ldtsystemDate = date('Y-m-d:H-i-s');
		$this->lobjCompanytypepaymentForm->upddate->setValue($ldtsystemDate);
		
		$upduser = 1;//$auth->getIdentity()->iduser;
		$this->lobjCompanytypepaymentForm->upduser->setValue($upduser);
		
       if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
		    $larrformData = $this->_request->getPost ();
		    $larrresult = $this->lobjCompanytypepaymentModel->fninsertdata($larrformData);
		    $auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added The companytype payment"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
			echo "<script>parent.location = '".$this->view->baseUrl()."/finance/companytypepayment/index';</script>";
		}
    }
    
    
 public function companytypepaymenteditAction()
    {
    	$idcompanypaymenttype = $this->_getParam('lvaredit');
    	

    	$this->view->lobjCompanytypepaymentForm = $this->lobjCompanytypepaymentForm;
    	$this->lobjCompanytypepaymentForm->idcompanypaymenttype->setValue($idcompanypaymenttype);
    	
    	$larrcompanyname = $this->lobjCompanytypepaymentModel->fnGetcompanyNameedit();
    	$this->lobjCompanytypepaymentForm->idcompany->setAttrib(readonly,true);
		$this->lobjCompanytypepaymentForm->idcompany->addMultiOptions($larrcompanyname);
		
		$larrcompanyname = $this->lobjCompanytypepaymentModel->fnGetPaymentTerms();
		$this->lobjCompanytypepaymentForm->paymenttype->addMultiOptions($larrcompanyname);
		
		$ldtsystemDate = date('Y-m-d:H-i-s');
		$this->lobjCompanytypepaymentForm->upddate->setValue($ldtsystemDate);
		
		$upduser = 1;//$auth->getIdentity()->iduser;
		$this->lobjCompanytypepaymentForm->upduser->setValue($upduser);
		
		$editresult = $this->lobjCompanytypepaymentModel->editcompanytypepayment($idcompanypaymenttype);
	/*	print_r($editresult);
		die();*/
	     $this->lobjCompanytypepaymentForm->populate($editresult);
			//$this->view->lobjCompanytypepaymentForm->idcompany->setValue($editresult['idcompany']);
		
       if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
		    $larrformData = $this->_request->getPost ();
		    $idcompanypmttype = $larrformData['idcompanypaymenttype'];
		    unset($larrformData['idcompanypaymenttype']);
		    unset($larrformData['Save']);
		    $larrresult = $this->lobjCompanytypepaymentModel->fnupdatecompanytypepayment($larrformData,$idcompanypmttype);
		    	$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated The companytypepayment with id = ".$idcompanypaymenttype."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
			echo "<script>parent.location = '".$this->view->baseUrl()."/finance/companytypepayment/index';</script>";
		}
		
    }
}

