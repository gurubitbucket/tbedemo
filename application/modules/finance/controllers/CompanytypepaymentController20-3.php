<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_CompanytypepaymentController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjCompanytypepaymentModel = new Finance_Model_DbTable_Companytypepayment();
		$this->lobjCompanytypepaymentForm = new Finance_Form_Companytypepayment ();  	
	}
    public function indexAction()
    {
    	
    	//echo"harsha";die();
       	$this->view->title="Company Payment Terms";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjCompanytypepaymentModel->fngetCompanyDetails(); //get user details
        //echo('<pre>');
		//print_r($larrresult);die();
		
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		    $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjCompanytypepaymentModel->fnSearchCompanyTypePayment($larrformData['field3']); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->semesterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 		echo "<script>parent.location = '".$this->view->baseUrl()."/finance/companytypepayment/index';</script>";
			}
	
    }
    
    public function companytypepaymentaddAction()
    {
    	
    	$this->view->lobjCompanytypepaymentForm = $this->lobjCompanytypepaymentForm;
    	
    	$larrcompanyname = $this->lobjCompanytypepaymentModel->fnGetcompanyName();
		$this->lobjCompanytypepaymentForm->idcompany->addMultiOptions($larrcompanyname);
		
		$larrcompanyname = $this->lobjCompanytypepaymentModel->fnGetPaymentTerms();
		$this->lobjCompanytypepaymentForm->paymenttype->addMultiOptions($larrcompanyname);
		
		$ldtsystemDate = date('Y-m-d:H-i-s');
		$this->lobjCompanytypepaymentForm->upddate->setValue($ldtsystemDate);
		
		$upduser = 1;//$auth->getIdentity()->iduser;
		$this->lobjCompanytypepaymentForm->upduser->setValue($upduser);
		
       if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
		    $larrformData = $this->_request->getPost ();
		    $larrresult = $this->lobjCompanytypepaymentModel->fninsertdata($larrformData);
			echo "<script>parent.location = '".$this->view->baseUrl()."/finance/companytypepayment/index';</script>";
		}
		
    }
    
    
 public function companytypepaymenteditAction()
    {
    	$idcompanypaymenttype = $this->_getParam('lvaredit');
    	

    	$this->view->lobjCompanytypepaymentForm = $this->lobjCompanytypepaymentForm;
    	$this->lobjCompanytypepaymentForm->idcompanypaymenttype->setValue($idcompanypaymenttype);
    	
    	$larrcompanyname = $this->lobjCompanytypepaymentModel->fnGetcompanyNameedit();
    		$this->lobjCompanytypepaymentForm->idcompany->setAttrib(readonly,true);
		$this->lobjCompanytypepaymentForm->idcompany->addMultiOptions($larrcompanyname);
		
		$larrcompanyname = $this->lobjCompanytypepaymentModel->fnGetPaymentTerms();
		$this->lobjCompanytypepaymentForm->paymenttype->addMultiOptions($larrcompanyname);
		
		$ldtsystemDate = date('Y-m-d:H-i-s');
		$this->lobjCompanytypepaymentForm->upddate->setValue($ldtsystemDate);
		
		$upduser = 1;//$auth->getIdentity()->iduser;
		$this->lobjCompanytypepaymentForm->upduser->setValue($upduser);
		
		$editresult = $this->lobjCompanytypepaymentModel->editcompanytypepayment($idcompanypaymenttype);
	     $this->lobjCompanytypepaymentForm->populate($editresult);
		
		
       if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
		    $larrformData = $this->_request->getPost ();
		    $idcompanypmttype = $larrformData['idcompanypaymenttype'];
		    unset($larrformData['idcompanypaymenttype']);
		    unset($larrformData['Save']);
		    $larrresult = $this->lobjCompanytypepaymentModel->fnupdatecompanytypepayment($larrformData,$idcompanypmttype);
			echo "<script>parent.location = '".$this->view->baseUrl()."/finance/companytypepayment/index';</script>";
		}
		
    }
}

