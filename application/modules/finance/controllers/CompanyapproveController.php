﻿<?php
class Finance_CompanyapproveController extends Base_Base 
{
	//public $gsessionregistration;
	
	//public $gsessionemail;
	public function init()
    {
    	$this->gsessionregistration = Zend_Registry::get('sis'); 	
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjCompanyapprovemodel = new Finance_Model_DbTable_Companyapprove();
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
		$this->lobjCompanyapprove = new Finance_Form_Companyapprove();  
       $this->auth = Zend_Auth::getInstance();		
	}
    public function indexAction()
	{
	
	   $this->view->lobjCompanyapprove = $this->lobjCompanyapprove; //send the lobjuniversityForm object to the view
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		    $larrformData = $this->_request->getPost ();
            $larrformData['UpdUser']= $this->auth->getIdentity()->iduser;;
		    $larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );				
          		
			if ($this->lobjform->isValid ( $larrformData )) 
			{
				unset($larrformData['Search']);
				$this->view->paginator = $this->lobjCompanyapprovemodel->fngetselectedcompany($larrformData); //searching the values for the user
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/finance/companyapprove/index');
		}
		
	
    }
	 public function editAction()
	 {
	      
	      $idbatch = $this->_getParam('idbatch');
		  //echo $idstudent;
		  $this->view->lobjCompanyapprove = $this->lobjCompanyapprove;
		  //$larrresult = $this->lobjCompanyapprovemodel->fetcompnydetails($idcompany);
		  $larrresult=$this->lobjCompanyapprovemodel->fngetCompanyname($idbatch);
		  $this->view->CompanyName = $larrresult['CompanyName'];
		  $this->view->RegistrationPin = $larrresult['registrationPin'];
		  $this->view->Mode = $larrresult['ModeofPayment'];
		  $this->view->Totalno = $larrresult['totalNoofCandidates'];
		  $this->lobjCompanyapprove->Amount->setValue($larrresult['totalAmount']);
          $this->view->lobjCompanyapprove->Amount->setAttrib('readonly','true'); 
		  $this->view->Idbatch = $larrresult['idBatchRegistration'];
		  $this->view->Idcompany = $larrresult['IdCompany'];
		  
		  if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		  {
			  $larrformData = $this->_request->getPost ();
		  
              $larrformData['UpdUser']= $this->auth->getIdentity()->iduser;
		      $larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );	
			  $larrresult = $this->lobjCompanyapprovemodel->insertapprovaldetails($larrformData); //Call a function to searching the values for the candidates			  
              $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$larrformData['Idcompany']);
			 // echo "<script>parent.location = '".$this->view->baseUrl()."/finance/companyapprove/index';</script>";		 
		 }
	}
    
    /*public function studentpaymenteditAction()
    {
    	
    	$this->view->lobjCompanypaymentForm = $this->lobjCompanypaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	
    	
    	$idstudent = $this->_getParam('idstudent');
    
    	if($idstudent)
    	{
    			//echo "Abc";die();
    			$lstrType=$idstudent;
    	}
    	
    	$this->view->idcompany = $lstrType;
    	
    		$ldtsystemDate =  date('Y-m-d h:i:s');

		$this->view->lobjCompanypaymentForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjCompanypaymentForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
    	print_r($lstrType);
    	die();
    	$larrstudentname=$this->lobjCompanypayment->fngetstudentname($lstrType);
    	
    	
    	
    	//$larrcoursename=$this->lobjCompanypayment->fngetcoursename($lstrType);
       //echo ('<pre>');
    	//print_r($larrstudentname);
   // die();
    	//$this->view->coursedetails = $larrcoursename;
    	
    	
    $todaydate=date('Y-m-d');
	
	
		if($todaydate>$larrstudentname['DateTime'])
		{
  	$this->_redirect( $this->baseUrl . '/finance/studentpayment/index');
		}
    	
    	
		$this->view->icno =$larrstudentname['ICNO'];
    	$this->view->ProgramName =$larrstudentname['ProgramName'];
    	$this->view->TakafulName =$larrstudentname['TakafulName'];
    	$this->view->mop=$larrstudentname['ModeofPayment'];
    	$this->view->FName=$larrstudentname['FName'];
    	$this->view->center =$larrstudentname['centername'];
    	$this->view->applieddate = $larrstudentname['Applieddate'];
    	$this->view->examdate = $larrstudentname['Examdate'].'-'.$larrstudentname['Exammonth'].'-'.$larrstudentname['years'];
        $this->lobjCompanypaymentForm->Amount->setValue($larrstudentname['Amount']);
        		$this->view->lobjCompanypaymentForm->Amount->setAttrib('readonly','true'); 	
        
        $larrbanknames=$this->lobjCompanypayment->fnGetBankDetails();        
        $this->view->lobjCompanypaymentForm->BankName->addMultiOptions($larrbanknames);
        
	
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
    			$larrformData = $this->_request->getPost ();  
    			//print_r($larrformData);
    			//die();		
    			$larrformData['IDApplication']=$lstrType;
    			$larrformData['companyflag']=0;    		 
    			
    			 
    			$larrcheckpayment=$this->lobjCompanypayment->CheckPaymentStatus($lstrType);
    			
    			if($larrcheckpayment)
    			{
    				//echo "Abc";die();
    					$this->_redirect( $this->baseUrl . '/finance/studentpayment/index');
    			}
    			if($larrstudentname['ModeofPayment']!=4)
    			{
    			$larrformData['BankName']=0;
    			}
    			$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption($larrformData);
    			$larrregdetails=$this->lobjCompanypayment->InsertRegisterdetails($larrstudentname['IDApplication'],$larrstudentname['Takafuloperator'],$larrstudentname['IdBatch']);
    			
    			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    						
					//$this->view->mess = "Payment Completed Sucessfully";
					//$this->view->mess = "Payment Completed Sucessfully <br/> Please check your mail box If you have not received a confirmation mail in next 30minutes<br/>Please check your spam folder Add ibfiminfo@gmail.com to the address book to ensure future communications doesn�t go to the spam folder";
					$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($larrstudentname['IDApplication']);	
					$larrregid = $this->lobjstudentmodel->fngetRegid($larrstudentname['IDApplication']);
					
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
									
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
                                   		$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'].' '.$larrresult['addr2'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
										//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'].'('.$larrresult['starttime'].'--'.$larrresult['endtime'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'].'('.$larrresult['ampmstart'].'--'.$larrresult['ampmend'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["username"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['password'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
									/*	$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
									$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									//echo '<script language="javascript">alert("Because of server problem mails cannot be send this time")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
								
								 //$this->_redirect( $this->baseUrl . "/registration/index");
								
    			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    			//$this->_redirect( $this->baseUrl.'/finance/studentpayment/index');
    			
								if($idstudent)
    	{
    		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/elapsedexamdate/index';</script>";
    	}
    	else 
    	{
    			echo "<script>parent.location = '".$this->view->baseUrl()."/finance/studentpayment/index';</script>";
    	}
    	}
    }*/


              
}

