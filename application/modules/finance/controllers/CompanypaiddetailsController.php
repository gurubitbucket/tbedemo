<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_CompanypaiddetailsController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjCompanypayment = new Finance_Model_DbTable_Companypaiddetails();
		$this->lobjCompanypaymentForm = new Finance_Form_Companypayment ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Company Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjCompanypayment->fngetCompanyDetails(); //get user details
      
		$lintpagecount =10000000;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->companypaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->companypaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
		 $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjCompanypayment->fnSearchCompanyPayment($larrformData['field3']); //searching the values for the user
				/*echo('<pre>');
				print_r($larrresult);
				die();*/
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->companypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/finance/companypayment/index');
			}
	
    }
    
    public function companypaymenteditAction()
    {
    	
    	$this->view->lobjCompanypaymentForm = $this->lobjCompanypaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	$this->view->idcompany = $lstrType;
    	/*print_r($lstrType);
    	die();*/
    	$larrstudentname=$this->lobjCompanypayment->fngetCompanyname($lstrType);
    	$this->view->studentdetails = $larrstudentname;
    	
    }
              
}

