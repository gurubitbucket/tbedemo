<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Finance_PaymentcomapnyController extends Base_Base{
	
	public function init(){
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjPaymentmodel = new Finance_Model_DbTable_Paymentcompanymodel();
		$this->lobjpaymentForm = new Finance_Form_Paymentform ();
	}
	public function indexAction(){
		//echo "asd";
		//die();
		$this->view->title="Paypal Company Payment Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjform object to the view
		$this->view->lobjpaymentForm = $this->lobjpaymentForm;
		$ldtsystemDate = date( 'Y-m-d H:i:s' );   //current Date
		$this->view->lobjpaymentForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();        //creating An Authentication object
		$this->view->idcompany = 0;
		$this->view->lobjpaymentForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		//$larrresult = $this->lobjPaymentmodel->fnGetCompanypaymentDetails(); //Call a function to get company payment details by paypal		
		if ($this->_getParam('idcompany') != 0){//for Clear
			$idcompany = $this->view->idcompany = $this->_getParam('idcompany');
			$larrresult = $this->lobjPaymentmodel->fngetpaymentcompanyidSearch($idcompany);			
		}
		
		$idapplication = (int) $this->_getParam('idapplication');
		
		if($idapplication) {			
			    $larrresult = $this->lobjPaymentmodel->fngetpaymentidSearch($idapplication); 
				$this->view->countcomp = $larrresult;
		}
				
				
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )){ //for Search
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )){		
				$larrresult = $this->lobjPaymentmodel->fngetpaymentSearch($larrformData); //Call a function to searching the values for the company								
			}
		}
		$this->view->count=count($larrresult);
		$this->view->countcomp = $larrresult;
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )){//for Clear
			$this->_redirect( $this->baseUrl . '/finance/paymentcomapny/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Confirm' )){
			$larrformData = $this->_request->getPost ();
			$this->lobjCompanyapplicationmodel = new App_Model_Companyapplication();
			$StudModel = new App_Model_Studentapplication();
			$larrformDatas['UpdUser']= 1;
			
			
			unset ( $larrformData ['Confirm'] );
			for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){
				$idPayment = $larrformData ['IDApplication'][$linti];
				$idCompany = $larrformData ['idCompany'][$idPayment];				 
				
				$ldate1=date('Y-m-d H:i:s',strtotime($larrformData ['UpdDate'][$idPayment]));	
				$larrformDatas['mc_gross']= $larrformData ['grossAmount'][$idPayment];
				$larrformDatas['payer_email']= $larrformData ['payerId'][$idPayment];
				$larrformDatas['txn_id']= $larrformData ['transactionId'][$idPayment];
				$larrformDatas['verify_sign']= $larrformData ['verifySign'][$idPayment];
				$larrformDatas['UpdDate']= $ldate1;				
				$larrformDatas['Regid']  = substr($larrformDatas['txn_id'], 1, 6).rand(1000, 9999).substr($larrformDatas['txn_id'], 5, 9);
					
				$this->lobjCompanyapplicationmodel->fnInsertPaypaldetails($larrformDatas,$idCompany,$idPayment);
				$larrStudentMailingDetails = $this->lobjCompanyapplicationmodel->fngetCompanyDetails($idCompany);
				
												
						//Get Email Template Description
						$larrEmailTemplateDesc =  $StudModel->fnGetEmailTemplateDescription("Batch Registration");					
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];													
							
							$lstrCompanyName = $larrStudentMailingDetails['CompanyName'];
							
							$lstrEmailTemplateBody = str_replace("[Company]",$lstrCompanyName,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Amount]",$larrformDatas['mc_gross'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[TransactionId]",$larrformDatas['txn_id'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[LoginId]",$larrformDatas['Regid'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
					    	$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;	
					    	
					    	$this->lobjPaymentmodel->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$larrStudentMailingDetails["Email"],$larrStudentMailingDetails['CompanyName']);
					    }
			}			
			if($this->_getParam('idcompany') != 0 ) $this->_redirect( $this->baseUrl . 'registrations/companystudentdetails/companystudentlist/idcompany/'.$this->_getParam('idcompany'));
			else $this->_redirect( $this->baseUrl . '/finance/paymentcomapny/index');
		}
	}	
}