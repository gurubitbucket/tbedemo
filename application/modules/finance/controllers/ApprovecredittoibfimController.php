<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_ApprovecredittoibfimController extends Base_Base 
{
	public $gsessionregistration;
	private $_gobjlogger;
	//public $gsessionemail;
	public function init()
    {
    	$this->gsessionregistration = Zend_Registry::get('sis'); 	
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->auth = Zend_Auth::getInstance();
    }
    public function fnsetObj()
    {
		$this->lobjCompanypayment = new Finance_Model_DbTable_Approvecredittoibfim();
		$this->lobjCompanypaymentForm = new Finance_Form_Approvecredittoibfim ();  	
	}
    public function indexAction()
    {
		$this->view->lobjCompanypaymentForm = $this->lobjCompanypaymentForm; //send the lobjuniversityForm object to the view
		$questions = $this->lobjCompanypayment->fngetquestion();
		for($k=0;$k<count($questions);$k++)
		{
		
		$str = $questions[$k]['Question'];
		echo  $k.$str;
		echo "<br/>";
		$newstring =  preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($str, ENT_QUOTES, 'UTF-8'));
		echo  $k.$newstring;
		echo "<br/>";
		//echo  $k.$str;
		//echo "<br/>";
		$invalid = array('�'=>'S', '�'=>'s', '�'=>'Dj', 'd'=>'dj', '�'=>'Z', '�'=>'z',
'C'=>'C', 'c'=>'c', 'C'=>'C', 'c'=>'c', '�'=>'A', '�'=>'A', '�'=>'A', '�'=>'A',
'�'=>'A', '�'=>'A', '�'=>'A', '�'=>'C', '�'=>'E', '�'=>'E', '�'=>'E', '�'=>'E',
'�'=>'I', '�'=>'I', '�'=>'I', '�'=>'I', '�'=>'N', '�'=>'O', '�'=>'O', '�'=>'O',
'�'=>'O', '�'=>'O', '�'=>'O', '�'=>'U', '�'=>'U', '�'=>'U', '�'=>'U', '�'=>'Y',
'�'=>'B', '�'=>'Ss', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a', '�'=>'a',
'�'=>'a', '�'=>'c', '�'=>'e', '�'=>'e', '�'=>'e',  '�'=>'e', '�'=>'i', '�'=>'i',
'�'=>'i', '�'=>'i', '�'=>'o', '�'=>'n', '�'=>'o', '�'=>'o', '�'=>'o', '�'=>'o',
'�'=>'o', '�'=>'o', '�'=>'u', '�'=>'u', '�'=>'u', '�'=>'y',  '�'=>'y', '�'=>'b',
'�'=>'y', 'R'=>'R', 'r'=>'r', "`" => "'", "�" => "'", "�" => ",", "`" => "'",
"�" => "'", "�" => "\"", "�" => "\"", "�" => "'", "&acirc;��" => "'", "{" => "",
"~" => "", "�" => "-", "�" => "'");
 
$str = str_replace(array_keys($invalid), array_values($invalid), $str);
		//echo $k.$str;
		//echo "<br/>";
		}
		die();
		
		/*echo "<pre>";
		print_r($questions);
		for($k=0;$k<count($questions);$k++)
		{
		$utf8=true;
		$str = $questions[$k]['Question'];
		$str = (string)$str;
    if( is_null($utf8) ) {
        if( !function_exists('mb_detect_encoding') ) {
            $utf8 = (strtolower( mb_detect_encoding($str) )=='utf-8');
        } else {
            $length = strlen($str);
            $utf8 = true;
            for ($i=0; $i < $length; $i++) {
                $c = ord($str[$i]);
                if ($c < 0x80) $n = 0; # 0bbbbbbb
                elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
                elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
                elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
                elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
                elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
                else return false; # Does not match any model
                for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
                    if ((++$i == $length)
                        || ((ord($str[$i]) & 0xC0) != 0x80)) {
                        $utf8 = false;
                        break;
                    }

                }
            }
        }

    }

    if(!$utf8)
        $str = utf8_encode($str);

    $transliteration = array(
    '?' => 'I', '�' => 'O','�' => 'O','�' => 'U','�' => 'a','�' => 'a',
    '?' => 'i','�' => 'o','�' => 'o','�' => 'u','�' => 's','?' => 's',
    '�' => 'A','�' => 'A','�' => 'A','�' => 'A','�' => 'A','�' => 'A',
    '�' => 'A','A' => 'A','A' => 'A','A' => 'A','�' => 'C','C' => 'C',
    'C' => 'C','C' => 'C','C' => 'C','D' => 'D','�' => 'D','�' => 'E',
    '�' => 'E','�' => 'E','�' => 'E','E' => 'E','E' => 'E','E' => 'E',
    'E' => 'E','E' => 'E','G' => 'G','G' => 'G','G' => 'G','G' => 'G',
    'H' => 'H','H' => 'H','�' => 'I','�' => 'I','�' => 'I','�' => 'I',
    'I' => 'I','I' => 'I','I' => 'I','I' => 'I','I' => 'I','J' => 'J',
    'K' => 'K','L' => 'K','L' => 'K','L' => 'K','?' => 'K','L' => 'L',
    '�' => 'N','N' => 'N','N' => 'N','N' => 'N','?' => 'N','�' => 'O',
    '�' => 'O','�' => 'O','�' => 'O','�' => 'O','O' => 'O','O' => 'O',
    'O' => 'O','R' => 'R','R' => 'R','R' => 'R','S' => 'S','S' => 'S',
    'S' => 'S','?' => 'S','�' => 'S','T' => 'T','T' => 'T','T' => 'T',
    '?' => 'T','�' => 'U','�' => 'U','�' => 'U','U' => 'U','U' => 'U',
    'U' => 'U','U' => 'U','U' => 'U','U' => 'U','W' => 'W','Y' => 'Y',
    '�' => 'Y','�' => 'Y','Z' => 'Z','Z' => 'Z','�' => 'Z','�' => 'a',
    '�' => 'a','�' => 'a','�' => 'a','a' => 'a','a' => 'a','a' => 'a',
    '�' => 'a','�' => 'c','c' => 'c','c' => 'c','c' => 'c','c' => 'c',
    'd' => 'd','d' => 'd','�' => 'e','�' => 'e','�' => 'e','�' => 'e',
    'e' => 'e','e' => 'e','e' => 'e','e' => 'e','e' => 'e','�' => 'f',
    'g' => 'g','g' => 'g','g' => 'g','g' => 'g','h' => 'h','h' => 'h',
    '�' => 'i','�' => 'i','�' => 'i','�' => 'i','i' => 'i','i' => 'i',
    'i' => 'i','i' => 'i','i' => 'i','j' => 'j','k' => 'k','?' => 'k',
    'l' => 'l','l' => 'l','l' => 'l','l' => 'l','?' => 'l','�' => 'n',
    'n' => 'n','n' => 'n','n' => 'n','?' => 'n','?' => 'n','�' => 'o',
    '�' => 'o','�' => 'o','�' => 'o','�' => 'o','o' => 'o','o' => 'o',
    'o' => 'o','r' => 'r','r' => 'r','r' => 'r','s' => 's','�' => 's',
    't' => 't','�' => 'u','�' => 'u','�' => 'u','u' => 'u','u' => 'u',
    'u' => 'u','u' => 'u','u' => 'u','u' => 'u','w' => 'w','�' => 'y',
    '�' => 'y','y' => 'y','z' => 'z','z' => 'z','�' => 'z','?' => 'A',
    '?' => 'A','?' => 'A','?' => 'A','?' => 'A','?' => 'A','?' => 'A',
    '?' => 'A','?' => 'A','?' => 'A','?' => 'A','?' => 'A','?' => 'A',
    '?' => 'A','?' => 'A','?' => 'A','?' => 'A','?' => 'A','?' => 'A',
    '?' => 'A','?' => 'A','?' => 'A','?' => 'B','G' => 'G','?' => 'D',
    '?' => 'E','?' => 'E','?' => 'E','?' => 'E','?' => 'E','?' => 'E',
    '?' => 'E','?' => 'E','?' => 'E','?' => 'Z','?' => 'I','?' => 'I',
    '?' => 'I','?' => 'I','?' => 'I','?' => 'I','?' => 'I','?' => 'I',
    '?' => 'I','?' => 'I','?' => 'I','?' => 'I','?' => 'I','?' => 'I',
    '?' => 'I','?' => 'I','?' => 'I','?' => 'I','?' => 'I','?' => 'I',
    'T' => 'T','?' => 'I','?' => 'I','?' => 'I','?' => 'I','?' => 'I',
    '?' => 'I','?' => 'I','?' => 'I','?' => 'I','?' => 'I','?' => 'I',
    '?' => 'I','?' => 'I','?' => 'I','?' => 'K','?' => 'L','?' => 'M',
    '?' => 'N','?' => 'K','?' => 'O','?' => 'O','?' => 'O','?' => 'O',
    '?' => 'O','?' => 'O','?' => 'O','?' => 'O','?' => 'O','?' => 'P',
    '?' => 'R','?' => 'R','S' => 'S','?' => 'T','?' => 'Y','?' => 'Y',
    '?' => 'Y','?' => 'Y','?' => 'Y','?' => 'Y','?' => 'Y','?' => 'Y',
    '?' => 'Y','?' => 'Y','F' => 'F','?' => 'X','?' => 'P','O' => 'O',
    '?' => 'O','?' => 'O','?' => 'O','?' => 'O','?' => 'O','?' => 'O',
    '?' => 'O','?' => 'O','?' => 'O','?' => 'O','?' => 'O','?' => 'O',
    '?' => 'O','?' => 'O','?' => 'O','?' => 'O','?' => 'O','?' => 'O',
    '?' => 'O','a' => 'a','?' => 'a','?' => 'a','?' => 'a','?' => 'a',
    '?' => 'a','?' => 'a','?' => 'a','?' => 'a','?' => 'a','?' => 'a',
    '?' => 'a','?' => 'a','?' => 'a','?' => 'a','?' => 'a','?' => 'a',
    '?' => 'a','?' => 'a','?' => 'a','?' => 'a','?' => 'a','?' => 'a',
    '?' => 'a','?' => 'a','?' => 'a','�' => 'b','?' => 'g','d' => 'd',
    'e' => 'e','?' => 'e','?' => 'e','?' => 'e','?' => 'e','?' => 'e',
    '?' => 'e','?' => 'e','?' => 'e','?' => 'z','?' => 'i','?' => 'i',
    '?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i',
    '?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i',
    '?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i',
    '?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 't','?' => 'i',
    '?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i',
    '?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i',
    '?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'i','?' => 'k',
    '?' => 'l','�' => 'm','?' => 'n','?' => 'k','?' => 'o','?' => 'o',
    '?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'o',
    '?' => 'o','p' => 'p','?' => 'r','?' => 'r','?' => 'r','s' => 's',
    '?' => 's','t' => 't','?' => 'y','?' => 'y','?' => 'y','?' => 'y',
    '?' => 'y','?' => 'y','?' => 'y','?' => 'y','?' => 'y','?' => 'y',
    '?' => 'y','?' => 'y','?' => 'y','?' => 'y','?' => 'y','?' => 'y',
    '?' => 'y','?' => 'y','f' => 'f','?' => 'x','?' => 'p','?' => 'o',
    '?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'o',
    '?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'o',
    '?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'o',
    '?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'o','?' => 'A',
    '?' => 'B','?' => 'V','?' => 'G','?' => 'D','?' => 'E','?' => 'E',
    '?' => 'Z','?' => 'Z','?' => 'I','?' => 'I','?' => 'K','?' => 'L',
    '?' => 'M','?' => 'N','?' => 'O','?' => 'P','?' => 'R','?' => 'S',
    '?' => 'T','?' => 'U','?' => 'F','?' => 'K','?' => 'T','?' => 'C',
    '?' => 'S','?' => 'S','?' => 'Y','?' => 'E','?' => 'Y','?' => 'Y',
    '?' => 'A','?' => 'B','?' => 'V','?' => 'G','?' => 'D','?' => 'E',
    '?' => 'E','?' => 'Z','?' => 'Z','?' => 'I','?' => 'I','?' => 'K',
    '?' => 'L','?' => 'M','?' => 'N','?' => 'O','?' => 'P','?' => 'R',
    '?' => 'S','?' => 'T','?' => 'U','?' => 'F','?' => 'K','?' => 'T',
    '?' => 'C','?' => 'S','?' => 'S','?' => 'Y','?' => 'E','?' => 'Y',
    '?' => 'Y','�' => 'd','�' => 'D','�' => 't','�' => 'T','?' => 'a',
    '?' => 'b','?' => 'g','?' => 'd','?' => 'e','?' => 'v','?' => 'z',
    '?' => 't','?' => 'i','?' => 'k','?' => 'l','?' => 'm','?' => 'n',
    '?' => 'o','?' => 'p','?' => 'z','?' => 'r','?' => 's','?' => 't',
    '?' => 'u','?' => 'p','?' => 'k','?' => 'g','?' => 'q','?' => 's',
    '?' => 'c','?' => 't','?' => 'd','?' => 't','?' => 'c','?' => 'k',
    '?' => 'j','?' => 'h'
    );
    $str = str_replace( array_keys( $transliteration ),
                        array_values( $transliteration ),
                        $str);
     echo $str;
		
	}	
	die();*/	
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		    $larrformData = $this->_request->getPost ();
            $larrformData['UpdUser']= $this->auth->getIdentity()->iduser;;
		    $larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );				
          		
			if ($this->lobjform->isValid ( $larrformData )) 
			{
				unset($larrformData['Search']);
				$this->view->paginator = $this->lobjCompanypayment->fngetselectedcompany($larrformData); //searching the values for the user
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/finance/approvecredittoibfim/index');
		}
		 if ($this->_request->isPost () && $this->_request->getPost ( 'Approve' )) 
		{
			  $larrformData = $this->_request->getPost (); 
			  //echo "<pre>";
			  //print_r($larrformData);die();
              $larrformData['UpdUser']= $this->auth->getIdentity()->iduser;
		      $larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );				 
			  $larrresult = $this->lobjCompanypayment->insertapprovaldetails($larrformData); //Call a function to searching the values for the candidates
		 }
    }
    
   

}

