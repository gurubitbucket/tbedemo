<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_FpxpaymentController extends Base_Base 
{

    public function init()
    {
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjPaymentmodel = new Finance_Model_DbTable_Fpxpayment();
		$this->lobjpaymentForm = new Finance_Form_Fpxpayment ();
    }
    
    public function indexAction()
    {
       	$this->view->title="Fpx Pyament Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjform object to the view
		$this->view->lobjpaymentForm = $this->lobjpaymentForm;
		$larrprogramname = $this->lobjPaymentmodel->fnGetprogramname();	//Call a function to get pargram names	
		$this->view->lobjform->field15->addMultiOptions($larrprogramname); //added program names to form dropdown element
		$ldtsystemDate = date( 'Y-m-d H:i:s' ); //current Date
		$this->view->lobjpaymentForm->UpdDate->setValue ( $ldtsystemDate ); 
		$auth = Zend_Auth::getInstance(); //creating An Authentication object
		$this->view->lobjpaymentForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$larrresult = $this->lobjPaymentmodel->fnGetStudentpaymentDetails(); //Call a function to get candidates payment details by Fpxpyment
		for($linti=0;$linti<count($larrresult);$linti++)
		{
		$lyear=substr($larrresult[$linti]['TxnOrderNo'] , 0 ,4);
		$lmonth=substr($larrresult[$linti]['TxnOrderNo'] , 4 ,2);
		$lday=substr($larrresult[$linti]['TxnOrderNo'] , 6 ,2);
		$larrresult[$linti]['TransactionDate']=$lyear."-".$lmonth."-".$lday;
		}
		$this->view->count=count($larrresult);//total number of candidates
		$this->view->countcomp = $larrresult; //sending candidates payment details to view
		
    $lintidstudent = $this->_getParam('idapplication');
		if($lintidstudent)
		{
			//echo abc;die();
				$larrresults = $this->lobjPaymentmodel->fngetpaymentidSearch($lintidstudent); //Call a function to searching the values for the candidates
				$this->view->count=count($larrresults);
				$this->view->countcomp = $larrresults;
				$this->view->flgs=1;
		}
		
        if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) { //for Search
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjPaymentmodel->fngetpaymentSearch($larrformData);  //Call a function to searching the values for the candidates
			for($linti=0;$linti<count($larrresult);$linti++){
		             $lyear=substr($larrresult[$linti]['TxnOrderNo'] , 0 ,4);
		             $lmonth=substr($larrresult[$linti]['TxnOrderNo'] , 4 ,2);
		             $lday=substr($larrresult[$linti]['TxnOrderNo'] , 6 ,2);
		             $larrresult[$linti]['TransactionDate']=$lyear."-".$lmonth."-".$lday;
		           }
				$this->view->count=count($larrresult);
				$this->view->countcomp = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) { //for Clear
			$this->_redirect( $this->baseUrl . '/finance/fpxpayment/index');			
		}
		 if ($this->_request->isPost () && $this->_request->getPost ( 'Confirm' )) {
		 	$larrformData = $this->_request->getPost ();
		 	unset ( $larrformData ['Confirm'] );
		    for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){    	
		    	    $lintidApplication = $larrformData ['IDApplication'][$linti];		    	   			    	
					$details=$this->lobjPaymentmodel->fnselectstudentforpayment($lintidApplication); //Call a function to get payment details of Selected students
					$larrformData['IdBatch'][$linti]=$details[0]['IdBatch'];	
		 			$larrformData['Takafuloperator'][$linti]=$details[0]['Takafuloperator'];
		 			$larrformData['FName'][$linti]=$details[0]['FName'];	
		 			$larrformData['ICNO'][$linti]=$details[0]['ICNO'];	
		 			$larrformData['Program'][$linti]=$details[0]['Program'];
		 			$DateTime=$details[0]['DateTime'];
		 		    $Examvenue=$details[0]['Examvenue'];
		 		    $Examsession=$details[0]['Examsession'];
		 	        $this->lobjPaymentmodel->fnUpdateAllotedseats($DateTime,$Examvenue,$Examsession);
		    	    $lintidApplication = $larrformData ['IDApplication'][$linti];    			    	
				    $this->lobjPaymentmodel->fnupdatestudentapplication($lintidApplication);//Call a function to updating the payment field of Studentapplication table	
		    }
		 	 $this->lobjPaymentmodel->fninsertfpxpaymentdetails($larrformData); //Call a function to adding the Fpx details to the Fpxpayment table
		 	 $this->lobjPaymentmodel->fninsertmanualfpxpaymentdetails($larrformData);//Call a function to adding the Fpx details to the manualfpxdetails table
		 	 $this->lobjPaymentmodel->fninsertregistereddetails($larrformData);//Call a function to adding the registration details to the registereddetails table
		 	 if($lintidstudent)
			{
				$this->_redirect( $this->baseUrl . '/examination/elapsedexamdate/index');	
			}
			else 
			{
		 	 $this->_redirect( $this->baseUrl . '/finance/fpxpayment/index');
			}
			}
		 	
		 }
    }


