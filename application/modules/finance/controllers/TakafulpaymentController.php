<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_TakafulpaymentController extends Base_Base 
{
    private $_gobjlogger;
    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
    }
    public function fnsetObj()
    {
		$this->lobjTakafulpayment = new Finance_Model_DbTable_Takafulpayment();
		$this->lobjTakafulpaymentForm = new Finance_Form_Takafulpayment ();  	
	}
    public function indexAction()
    {
    	
       	$this->view->title="Takaful Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		/*$larrresult = $this->lobjTakafulpayment->fngetTakafulDetails(); //get user details
		
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}*/
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		    $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjTakafulpayment->fnSearchTakafulPayment($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->takafulpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/finance/takafulpayment/index');
			}
	
    }
    
    public function takafulpaymenteditAction()
    {
    	
    	$this->view->lobjTakafulpaymentForm = $this->lobjTakafulpaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	$this->view->lstrType = $lstrType;
    	$larrstudentname=$this->lobjTakafulpayment->fngetTakafulname($lstrType);
    	
    	if($this->_getParam('idtakaful'))$this->view->idtakaful = $this->_getParam('idtakaful');
    	else $this->view->idtakaful = 0;
    	
    	if($this->_getParam('idcompany'))$this->view->idcompany = $this->_getParam('idcompany');
    	else $this->view->idcompany = 0;
    	
    	$larrcoursename=$this->lobjTakafulpayment->fngetcoursename($lstrType);
    	$this->view->coursedetails = $larrcoursename;
    	$this->view->takafulname = $larrstudentname['TakafulName'];
    	$this->view->takafulcontact = $larrstudentname['ContactName'];
    	$this->view->applied = $larrcoursename['0']['applied'];
    	$this->view->takafulemail = $larrstudentname['email'];
    	$this->view->progname = $larrstudentname['ProgramName'];
    	$this->lobjTakafulpaymentForm->Amount->setValue($larrstudentname['totalAmount']);
    	 
    	$larrbanknames=$this->lobjTakafulpayment->fnGetBankDetails();        
        $this->view->lobjTakafulpaymentForm->BankName->addMultiOptions($larrbanknames);
	
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
    		
    			$larrformData = $this->_request->getPost ();  
    		 			
    			$larrformData['IDApplication']=$lstrType;   			
    			$regid = $this->lobjTakafulpayment->fngeneraterandom();
    			$larrpaymentdetails = $this->lobjTakafulpayment->InsertPaymentOption($larrformData,$lstrType,$regid); 
    			$this->view->mess = $result;	
    			$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Completed the Takaful payment for id = ".$lstrType."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$lstrType = $auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				if($this->view->idcompany!=0)
				{
					$this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$larrformData['idcompany']);
					die();
				}			
				if($this->view->idtakaful != 0) $this->_redirect( $this->baseUrl . '/registrations/takafulstudentdetails/takafulstudentlist/idtakaful/'.$this->_getParam('idtakaful'));
				else $this->_redirect( $this->baseUrl . '/finance/takafulpayment/index');
				  //$this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$larrformData['Idcompany']);
				
    	}
    }

}

