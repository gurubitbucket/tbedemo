<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_ApprovecreditstudentController extends Base_Base 
{
	public $gsessionregistration;
	private $_gobjlogger;
	//public $gsessionemail;
	public function init()
    {
    	$this->gsessionregistration = Zend_Registry::get('sis'); 	
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
    }
    public function fnsetObj()
    {
		$this->lobjCompanypayment = new Finance_Model_DbTable_Approvecreditstudent();
		$this->lobjCompanypaymentForm = new Finance_Form_Approvecreditstudent ();  	
	}
    public function indexAction()
    {
   
    	$this->gsessionregistration->mails=0;
       	$this->view->title="Company Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		/*$larrresult = $this->lobjCompanypayment->fngetCompanyDetails(); //get user details
		
		if(isset($this->gobjsessionstudent->companypaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->companypaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}*/
		$lintpagecount =10000000;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
		 $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
			//	echo"<pre>";print_r($larrformData);die();
				unset($larrformData['Search']);
				$larrresult = $this->lobjCompanypayment->fnSearchCompanyPayment($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->companypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/finance/approvecreditstudent/index');
			}
	
    }
    
    public function companypaymenteditAction()
    {
    	
    	$this->view->lobjCompanypaymentForm = $this->lobjCompanypaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	$this->view->idcompany = $lstrType;
    	$larrstudentname=$this->lobjCompanypayment->fngetCompanyname($lstrType);
    	$larrcoursename=$this->lobjCompanypayment->fngetcoursename($lstrType);
    	$this->view->coursedetails = $larrcoursename;
    	$this->view->username = $larrstudentname['Login'];
    	$this->view->password = $larrstudentname['hint'];
    	$this->view->companyname = $larrstudentname['CompanyName'];
    	$this->view->address = $larrstudentname['Address'];
    	
    	$this->view->companyemail = $larrstudentname['Email'];
    	$this->view->prog = $larrstudentname['ProgramName'];
        $this->lobjCompanypaymentForm->Amount->setValue($larrstudentname['totalAmount']);
        
        $larrbanknames=$this->lobjCompanypayment->fnGetBankDetails();        
        $this->view->lobjCompanypaymentForm->BankName->addMultiOptions($larrbanknames);
        
	
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Approve' )) {
    			$larrformData = $this->_request->getPost ();      					
    			$larrformData['IDApplication']=$lstrType;
    			$larrformData['companyflag']=1;  
    			  		 
    			 
    			$regid = $this->lobjCompanypayment->fngeneraterandom();
    			$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption($larrformData,$lstrType,$regid);
    			
    			  if($this->gsessionregistration->mails == 0)
    			  {
		    			$result = $this->lobjCompanypayment->sendmails($larrformData['companyname'],$larrformData['companymail'],$larrformData['address'],$larrformData['Amount'],$larrformData['loginname'],$larrformData['password'],$regid);
		    			$this->view->mess = $result;		
		    			$this->gsessionregistration->mails=1;
    			  }
    			  $auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Approved the Application Details for id = ".$lstrType."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
    			  print_r($this->gsessionregistration->mails);
    			$this->_redirect( $this->baseUrl . '/finance/approvecreditstudent/index');
    	}
    }

}

