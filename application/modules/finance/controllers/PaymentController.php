<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Finance_PaymentController extends Base_Base{

	public function init(){
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjPaymentmodel = new Finance_Model_DbTable_Paymentmodel();
		$this->lobjpaymentForm = new Finance_Form_Paymentform ();
	}

	public function indexAction(){
		$this->view->title="Pyament Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjform object to the view
		$this->view->lobjpaymentForm = $this->lobjpaymentForm;
		$larrprogramname = $this->lobjPaymentmodel->fnGetprogramname(); //Call a function to get pargram names
		$this->view->lobjform->field15->addMultiOptions($larrprogramname); //added program names to form dropdown element
		$ldtsystemDate = date( 'Y-m-d H:i:s' );   //current Date
		$this->view->lobjpaymentForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();        //creating An Authentication object
		$this->view->lobjpaymentForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		/*$larrresult = $this->lobjPaymentmodel->fnGetStudentpaymentDetails(); //Call a function to get candidates payment details by paypal
		$this->view->count=count($larrresult); //total number of candidates
		$this->view->countcomp = $larrresult; //sending candidates payment details to view*/
		
		
		$lintidstudent = $this->_getParam('idapplication');
		if($lintidstudent)
		{
			//echo abc;die();
				$larrresults = $this->lobjPaymentmodel->fngetpaymentidSearch($lintidstudent); //Call a function to searching the values for the candidates
				$this->view->count=count($larrresults);
				$this->view->countcomp = $larrresults;
				$this->view->flgs=1;
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )){ //for Search
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )){
				//echo"<pre>";print_r($larrformData);die();
				$larrresult = $this->lobjPaymentmodel->fngetpaymentSearch($larrformData); //Call a function to searching the values for the candidates
				$this->view->count=count($larrresult);
				$this->view->countcomp = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )){//for Clear
			$this->_redirect( $this->baseUrl . '/finance/payment/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Confirm' )){
			$larrformData = $this->_request->getPost ();
		echo "<pre />";
			print_r($larrformData);
			//echo $larrformData['PartA'][$larrformData['IDApplication'][0]],
			die();
			unset ( $larrformData ['Confirm'] );
			for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){
				$lintidApplication = $larrformData ['IDApplication'][$linti];
				$larrdetails=$this->lobjPaymentmodel->fnselectstudentforpayment($lintidApplication);//Call a function to get payment details of Selected students
				$larrformData['IdBatch'][$linti]=$larrdetails[0]['IdBatch'];
				$larrformData['Takafuloperator'][$linti]=$larrdetails[0]['Takafuloperator'];
				$larrformData['FName'][$linti]=$larrdetails[0]['FName'];
				$larrformData['ICNO'][$linti]=$larrdetails[0]['ICNO'];
				$larrformData['Program'][$linti]=$larrdetails[0]['Program'];
				$lDateTime=$larrdetails[0]['DateTime'];
				$lintExamvenue=$larrdetails[0]['Examvenue'];
				$lintExamsession=$larrdetails[0]['Examsession'];
				$this->lobjPaymentmodel->fnUpdateAllotedseats($lDateTime,$lintExamvenue,$lintExamsession);//Call a function to updating the Allotedseats field of venuedateschedule table
				$lintidApplication = $larrformData ['IDApplication'][$linti];
				$this->lobjPaymentmodel->fnupdatestudentapplication($lintidApplication);//Call a function to updating the payment field of Studentapplication table
			$larricnoget=$this->lobjPaymentmodel->fngeticnoofstu($lintidApplication);
			}
			$this->lobjPaymentmodel->fninsertpaypaldetails($larrformData); //Call a function to adding the paypal details to the paypaldetails table
			$this->lobjPaymentmodel->fninsertmanualpaypaldetails($larrformData);//Call a function to adding the paypal details to the manualpaypaldetails table
			$this->lobjPaymentmodel->fninsertregistereddetails($larrformData);//Call a function to adding the registration details to the registereddetails table
			
			if($lintidstudent)
			{
				$this->_redirect( $this->baseUrl . '/examination/elapsedexamdate/index');	
			}
			else 
			{
			$this->_redirect( $this->baseUrl . '/finance/payment/index');
			}
			
			}
	}
}


