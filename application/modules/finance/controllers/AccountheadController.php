<?php
/**
 * InitialConfigController
 * 
 * @author
 * @version 
 */
class  Finance_AccountheadController extends Base_Base
{
	private $locale;
	private $registry;
	private $lobjaccountheadModel;
	private $lobjaccountheadentryform;
	private $_gobjlogger;  	
public function init() { //initialization function
	
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		
		$this->lobjaccountheadModel = new Finance_Model_DbTable_Accounthead(); //user model object
	    $this->lobjaccountheadentryform = new Finance_Form_Accountheadentry(); //intialize user lobjuserForm
	  
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		 
		
	}
  	
	public function indexAction() { // action for search and view
		 //echo "abc";die();
		$lobjform=$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		/*$larrresult = $this->lobjaccountheadModel->fngetaccounthead (); //get user details

		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->accountheadpaginatorresult);
			
		

		if(isset($this->gobjsessionsis->accountheadpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->accountheadpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}*/
		
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjaccountheadModel->fngetaccountheadsSearch($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->accountheadpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/finance/accounthead/index');
			//$this->_redirect($this->view->url(array('module'=>'finance' ,'controller'=>'accounthead', 'action'=>'index'),'default',true));
		}


	}
		
    public function accountheadAction()
    {   
    	$this->view->lobjaccountheadentryform = $this->lobjaccountheadentryform; 	
		
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
      
		$this->view->lobjaccountheadentryform->UpdDate->setValue($ldtsystemDate);
		 
		$auth = Zend_Auth::getInstance();
		$this->view->lobjaccountheadentryform->UpdUser->setValue($auth->getIdentity()->iduser);

	$larrGroupList	=	$this->lobjaccountheadModel->fnGetIdGroupSelect();
		$this->view->lobjaccountheadentryform->idAccount->addMultiOptions($larrGroupList);
		$this->view->lobjaccountheadentryform->idAccount->setRequired(true);
			$this->view->lobjaccountheadentryform->discounttype->setValue(0);
    	if($this->_getparam('lvaredit')){ 
    		
    		
			$lvaredit	 = 	$this->_getparam('lvaredit');
			$this->view->lvaredit	=	$this->_getparam('lvaredit');
			$lvardataEdit=	$this->lobjaccountheadModel-> fngetaccountEdit($lvaredit);	
			//print_r($lvardataEdit);die();
			
			
			///////////////////////////////////////
    		if($lvardataEdit['Discount']==1){
    		$larrresults=$this->lobjaccountheadModel->fnGetAllActiveGroupNameList();

    		}
    		else
    		{
    		$larrresults=$this->lobjaccountheadModel->fnGetAllActiveReligionNameList();	
    		}
    		////////////////////////////////////////
    		
    		
    		$this->view->lobjaccountheadentryform->discounttype->addMultiOptions($larrresults);	
			$this->view->lobjaccountheadentryform->idAccount->setValue($lvardataEdit['idAccount']);	
			$this->view->lobjaccountheadentryform->Discount->setValue($lvardataEdit['Discount']);
			$this->view->lobjaccountheadentryform->discounttype->setValue($lvardataEdit['discounttype']);		
			if($lvardataEdit['Discount']==0)
			{
				$this->view->lobjaccountheadentryform->minnostd->setValue('0');
			}
			else
			{
			$this->view->lobjaccountheadentryform->minnostd->setValue($lvardataEdit['minnostd']);	
			}
			$this->view->lobjaccountheadentryform->effectiveDate->setValue($lvardataEdit['effectiveDate']);
				
			$this->view->lobjaccountheadentryform->Amount->setValue($lvardataEdit['Amount']);			
			$this->view->lobjaccountheadentryform->Active->setValue($lvardataEdit['Active']);

			// $this->view->lobjaccountheadentryform->discounttype->addMultiOptions($larrresults);	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			unset ( $larrformData ['Close'] );
			if ($this->lobjaccountheadentryform->isValid ( $larrformData )) {
				$larrformData ['discounttype']=0;
				$result = $this->lobjaccountheadModel->fnupdateaccounthead($this->_getparam('lvaredit'),$larrformData); //instance for adding the lobjuserForm values to DB
				$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated The Account Details with id = ".$lvaredit."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				$this->_redirect( $this->baseUrl . '/finance/accounthead/index');
				//$this->_redirect($this->view->url(array('module'=>'finance' ,'controller'=>'accounthead', 'action'=>'index'),'default',true));
			}
		}			
		}
		else {
			if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			unset ( $larrformData ['Close'] );
			if ($this->lobjaccountheadentryform->isValid ($larrformData )) {
				$larrformData ['discounttype']=0;
				$result = $this->lobjaccountheadModel->fnInsert($larrformData); //instance for adding the lobjuserForm values to DB
				$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added The Account Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				$this->_redirect( $this->baseUrl . '/finance/accounthead/index');
				//$this->_redirect($this->view->url(array('module'=>'finance' ,'controller'=>'accounthead', 'action'=>'index'),'default',true));
			}
		}	
		}
   }
   public function getgroupdetailsAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrComDetails = "";
	
		//Initialize DMS Common Model
 		//$lobjCommonModel = new App_Model_Common();
				

		$lstrType = $this->_getParam('Type');
		//echo $lstrType;die();
		if($lstrType==1)
		{
			
			$larrComDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetAllActiveGroupNameList());	
		 
		}
		else 
		{
			$larrComDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetAllActiveReligionNameList());	
		}
		
		echo Zend_Json_Encoder::encode($larrComDetails);
	}
	
   
}
