<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_CompanypaymentController extends Base_Base 
{
	public $gsessionregistration;
	   private $_gobjlogger;
	//public $gsessionemail;
	public function init()
    {
    	$this->gsessionregistration = Zend_Registry::get('sis'); 	
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
    }
    public function fnsetObj()
    {
		$this->lobjCompanypayment = new Finance_Model_DbTable_Companypayment();
		$this->lobjCompanypaymentForm = new Finance_Form_Companypayment ();  	
	}
    public function indexAction()
    {   
    	$this->gsessionregistration->mails=0;
       	$this->view->title="Company Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		/*$larrresult = $this->lobjCompanypayment->fngetCompanyDetails(); //get user details
		
		if(isset($this->gobjsessionstudent->companypaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->companypaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}*/
		$lintpagecount =10000000;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
		 $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				//print_r($larrformData);die();
				$larrresult = $this->lobjCompanypayment->fnSearchCompanyPayment($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->companypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/finance/companypayment/index');
			}
	
    }
    
    public function companypaymenteditAction()
    {
    	
    	$this->view->lobjCompanypaymentForm = $this->lobjCompanypaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	$this->view->lvaredit = $lstrType;
    	
    	if($this->_getParam('idcompany') ) $this->view->idcompany = $this->_getParam('idcompany');
    	else $this->view->idcompany = 0;
    	
    	$larrstudentname=$this->lobjCompanypayment->fngetCompanyname($lstrType);
    	$larrcoursename=$this->lobjCompanypayment->fngetcoursename($lstrType);
    	$this->view->coursedetails = $larrcoursename;
    	$this->view->username = $larrstudentname['Login'];
    	$this->view->password = $larrstudentname['hint'];
    	$this->view->companyname = $larrstudentname['CompanyName'];
    	$this->view->ContactPerson = $larrstudentname['ContactPerson'];
    	$this->view->applied = $larrcoursename['0']['applied'];
    	$this->view->address = $larrstudentname['Address'];
    	$this->view->companyemail = $larrstudentname['Email'];
    	$this->view->prog = $larrstudentname['ProgramName'];
        $this->lobjCompanypaymentForm->Amount->setValue($larrstudentname['totalAmount']);
        $larrbanknames=$this->lobjCompanypayment->fnGetBankDetails();        
        $this->view->lobjCompanypaymentForm->BankName->addMultiOptions($larrbanknames);
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
    			$larrformData = $this->_request->getPost ();  
    			$larrformData['IDApplication']=$lstrType;
    			$larrformData['companyflag']=1;    		 
    			$regid = $this->lobjCompanypayment->fngeneraterandom();
    			$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption($larrformData,$lstrType,$regid);
    			  if($this->gsessionregistration->mails == 0)
    			  {
    			$result = $this->lobjCompanypayment->sendmails($larrformData['companyname'],$larrformData['companymail'],$larrformData['address'],$larrformData['Amount'],$larrformData['loginname'],$larrformData['password'],$regid);
    			$this->view->mess = $result;		
    			$this->gsessionregistration->mails=1;
    			  }
    			$auth = Zend_Auth::getInstance();// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Completed the Company payment"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
    			// print_r($this->gsessionregistration->mails);
    			if($this->view->idcompany != 0) $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$this->_getParam('idcompany'));
				else $this->_redirect( $this->baseUrl . '/finance/companypayment/index');
    	}
    }

public function printreportAction() 
	{			
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		
		$IDAPPLICATION =(int) $this->_getParam('idapplication');

		//$larrcompanydetails=$this->lobjCompanypayment->fngetCommpanyreportDetails($IDAPPLICATION);

		//$totamt=(int)$larrcompanydetails[0]['totalAmount'];
		
		//$Amount = $this->lobjCompanypayment->fnGetAmountInWords($totamt);

		//$AmountInWords=$Amount['Amount'];

		
		//object to initialize ini file
		$lobjAppconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini','development');
									
		    try 
		    {	
	            //java class
	            $lobjdbdriverclass = new Java("java.lang.Class");
	            
	            //set db driver
	            $lobjdbdriverclass->forName("com.mysql.jdbc.Driver");
	
	            //driver manager object
	            $lobjdrivermanager = new Java("java.sql.DriverManager");
	            
	            //get the db connection
				$lstrConnection  =  "jdbc:mysql://".
										$lobjAppconfig->resources->db->params->host."/".
										$lobjAppconfig->resources->db->params->dbname."?user=".
										$lobjAppconfig->resources->db->params->username."&password=".
										$lobjAppconfig->resources->db->params->password;
														
				$lobjconnection = $lobjdrivermanager->getConnection($lstrConnection);
	            
	            //Jasper Compile manager object
	            $lobjcompileManager = new Java(
	            					"net.sf.jasperreports.engine.JasperCompileManager");
	            
	            echo "CompileManager object created</br>";
	            $lstrreportdir = realpath(".") . "/report/";
	            $lstrimagepath = realpath(".") . "/images/";
	             
	             //compiled report path
	              $lobjreport = $lobjcompileManager->compileReport(realpath($lstrreportdir."companyapplicationreport.jrxml"));
	            
	            //Jasper Fill Manager object
	            $lobjfillManager = new Java(
	            					"net.sf.jasperreports.engine.JasperFillManager");
	            $int1 = new Java("java.lang.Integer");
	            //Hashmap object
	            //print_r($lstrreportdir);die();
	            $lobjparams = new Java("java.util.HashMap");
	          	$lobjparams->put("IDAPPLICATION",$IDAPPLICATION);
	          	//$lobjparams->put("AMOUNTINWORDS",$AmountInWords);
	          	$lobjparams->put ("IMAGEPATH", $lstrimagepath . "reportheader.jpg" );
	           
	           echo "Fill Manager</br>";
	            					
	            //Jasper Print Object
	            $lobjjasperPrint = $lobjfillManager->fillReport(
	            					$lobjreport, $lobjparams, $lobjconnection);
	            					
	            echo "Jasper Printed</br>";
	            
	            //Jasper Export Manager object
	            $lobjexportManager = new Java(
	            					"net.sf.jasperreports.engine.JasperExportManager");
	            
	            //output file path
	            $lstrhtmloutputPath = realpath(".") . "/" . "output.html";
	            echo "Before Export</br>";
	            $session = Zend_Session::getId();
	            $lstrpdfoutputPath = realpath(".") . "/" . "$session.pdf";
	            $objStream = new Java("java.io.ByteArrayOutputStream");
	            $lobjexportManager->exportReportToPdfFile($lobjjasperPrint,$lstrpdfoutputPath);
	            
	            //Export report to HTML	            
	            echo 'HTML Exported</br>';
	
				header("Content-type: application/pdf;charset=utf-8;encoding=utf-8");
				header('Content-Disposition: attachment; filename="Company_Application.pdf"');
				
	            readfile($lstrpdfoutputPath);
	            unlink($lstrpdfoutputPath);
				echo "finished";	
		 		 		            
		    } 
		    catch (JavaException $lobjexception) 
		    {
		    	echo 'Exception caught: ', $lobjexception->getMessage() . "\n";
		    }		    		   
			
	    }
              
}

