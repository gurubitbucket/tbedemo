<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Finance_PaymenttakafulController extends Base_Base{

	public function init(){
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjPaymentmodel = new Finance_Model_DbTable_Paymentcompanymodel();
		$this->lobjpaymentForm = new Finance_Form_Paymentform ();
	}

	public function indexAction(){
		$this->view->title="Paypal Company Payment Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjform object to the view
		$this->view->lobjpaymentForm = $this->lobjpaymentForm;
		$ldtsystemDate = date( 'Y-m-d H:i:s' );   //current Date
		$this->view->lobjpaymentForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();        //creating An Authentication object
		$this->view->lobjpaymentForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		//$larrresult = $this->lobjPaymentmodel->fnGettakafulpaymentDetails(); //Call a function to get company payment details by paypal	
		$this->view->idtakaful = 0;	
		if ($this->_getParam('idtakaful') != 0){//for Clear
			$idtakaful = $this->view->idtakaful = $this->_getParam('idtakaful');
			$larrresult = $this->lobjPaymentmodel->fngettakafulidtakafulpaymentSearch($idtakaful); //Call a function to searching the values for the company			
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )){ //for Search
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )){				
				$larrresult = $this->lobjPaymentmodel->fngettakafulpaymentSearch($larrformData); //Call a function to searching the values for the company				
			}
		}
		$this->view->count=count($larrresult);
		$this->view->countcomp = $larrresult;
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )){//for Clear
			$this->_redirect( $this->baseUrl . '/finance/paymenttakaful/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Confirm' )){
			$larrformData = $this->_request->getPost ();
			$this->lobjTakafulapplication = new App_Model_Takafulapplication();
			$StudModel = new App_Model_Studentapplication();
			$larrformDatas['UpdUser']= 1;			
			
			unset ( $larrformData ['Confirm'] );
			for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){
				$idPayment = $larrformData ['IDApplication'][$linti];
				$idtakafuloperator = $larrformData ['idtakafuloperator'][$idPayment];
				 
				
				$ldate1=date('Y-m-d H:i:s',strtotime($larrformData ['UpdDate'][$idPayment]));	
				$larrformDatas['mc_gross']= $larrformData ['grossAmount'][$idPayment];
				$larrformDatas['payer_email']= $larrformData ['payerId'][$idPayment];
				$larrformDatas['txn_id']= $larrformData ['transactionId'][$idPayment];
				$larrformDatas['verify_sign']= $larrformData ['verifySign'][$idPayment];
				$larrformDatas['UpdDate']= $ldate1;				
				$larrformDatas['Regid']  = substr($larrformDatas['txn_id'], 1, 6).rand(1000, 9999).substr($larrformDatas['txn_id'], 5, 9);
					
				$this->lobjTakafulapplication->fnInsertPaypaldetails($larrformDatas,$idtakafuloperator,$idPayment);
				
					//Mail Starts here
					//$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany);
				
				 	$larrStudentMailingDetails = $this->lobjTakafulapplication->fngetTakafulOperator($idtakafuloperator);												
						//Get Email Template Description
						$larrEmailTemplateDesc =  $StudModel->fnGetEmailTemplateDescription("Takaful Payment");					
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];								
																				
							
							$lstrCompanyName = $larrStudentMailingDetails['TakafulName'];
							
							$lstrEmailTemplateBody = str_replace("[Person]",$lstrCompanyName,$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Amount]",$dataArray['grossAmount'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Username]",$larrStudentMailingDetails["email"],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[RegPin]",$larrformDatas['Regid'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Password]", substr($larrStudentMailingDetails['hint'], 5),$lstrEmailTemplateBody);//substr($string, 5)
							$lstrEmailTemplateBody = str_replace("[Link]","http://www.takafuleexam.com/tbe/takafullogin/login",$lstrEmailTemplateBody);
					    	$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;					    	
					    	
					    	//$this->lobjPaymentmodel->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$larrStudentMailingDetails["email"],$larrStudentMailingDetails['TakafulName']);
					    }
			}	
			if($this->_getParam('idtakaful') != 0 ) $this->_redirect( $this->baseUrl . 'registrations/takafulstudentdetails/takafulstudentlist/idtakaful/'.$this->_getParam('idtakaful'));
			else $this->_redirect( $this->baseUrl . '/finance/paymenttakaful/index');
			
		}
	}	
}