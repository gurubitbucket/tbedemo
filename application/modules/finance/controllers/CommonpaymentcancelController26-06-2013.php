<?php
//error_reporting (E_ALL ^ E_WARNING);
//error_reporting (E_ALL ^ E_NOTICE);
class Finance_CommonpaymentcancelController extends Base_Base{

	public function init(){
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjPaymentcancelmodel = new Finance_Model_DbTable_Paymentcancelmodel();
		//$this->lobjpaymentForm = new Finance_Form_Paymentform ();
	}

	public function indexAction(){
		$this->view->title="Common Pyament Cancel Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjform object to the view		
		$larrprogramname[2]['key']= 3;
		$larrprogramname[2]['value']= "Indivdual";
		$larrprogramname[0]['key']= 1;
		$larrprogramname[0]['value']= "Comapny";
		$larrprogramname[1]['key']= 2;
		$larrprogramname[1]['value']= "Takaful";
		$this->view->lobjform->field15->addMultiOptions($larrprogramname); //added program names to form dropdown element
		$this->view->lobjform->field15->setAttrib('required','true');		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )){//for Clear
			$this->_redirect( $this->baseUrl . '/finance/commonpaymentcancel/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'submit' )){
			$larrformData = $this->_request->getPost ();			
			$larrformData['UpdDate'] = date( 'Y-m-d H:i:s' );
			$auth = Zend_Auth::getInstance(); 
			$larrformData['UpdUser'] =  $auth->getIdentity()->iduser;	
            if($larrformData['lvartype'] == 3){
	            $this->lobjPaymentcancelmodel->fnCancelPaymentIndidual($larrformData);			
				$larrresult = $this->lobjPaymentcancelmodel->fnGetpaymentsIndidual(); //Call a function to searching the values for the candidates											
			}
            else{
            	$this->lobjPaymentcancelmodel->fnCancelPayment($larrformData);			
				$larrresult = $this->lobjPaymentcancelmodel->fnGetpayments($larrformData['companyflag']); //Call a function to searching the values for the candidates
			}	
			$this->view->lvartype  = $larrformData['lvartype'];
            $this->view->lobjform->field15->setValue( $larrformData['lvartype'] );
			$this->view->count=count($larrresult);
			$this->view->countcomp = $larrresult;			
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )){ //for Search
			$larrformData = $this->_request->getPost ();
			$this->view->lvartype  = $larrformData['field15'];
			if ($this->lobjform->isValid ( $larrformData)){
				if($larrformData['field15'] == 3){
					//$larrresult = $this->lobjPaymentcancelmodel->fnGetpaymentsIndidual($larrformData[field2],$larrformData[field4],$larrformData[field3]); //Call a function to searching the values for the candidates
					$larrresult = $this->lobjPaymentcancelmodel->fnGetpaymentsIndidual(); //Call a function to searching the values for the candidates
				}
				else {
					$larrresult = $this->lobjPaymentcancelmodel->fnGetpayments($larrformData['field15']); //Call a function to searching the values for the candidates
					//$larrresult = $this->lobjPaymentcancelmodel->fnGetpayments($larrformData[field15],$larrformData[field4]); //Call a function to searching the values for the candidates
				}
				$this->view->lobjform->field15->setValue( $larrformData['field15'] );
				$this->view->count=count($larrresult);
				$this->view->countcomp = $larrresult;
			}
		}
	}
public function fncancelpaymentAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$idcompanyflag = $this->_getParam('companyflag');
		$idBatchRegistration = $this->_getParam('idBatchRegistration');
		$lobjpaymentdetails = $this->lobjPaymentcancelmodel->fngetpaymentdetails($idBatchRegistration,$idcompanyflag);
		//echo "<pre>";print_r($lobjpaymentdetails);
		
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Company Name</b></th>
					<th><b>Pin Number</b></th>
					<th><b>Amount</b></th>
					<th><b>Number Of Candidates</b></th>
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['registrationPin']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalAmount']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalNoofCandidates']."</b></td>
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		
		$tabledata.= "</b></td>
					</tr>
					</table><br>";
	  $tabledata.="<table  class='table' border=1 align='center' width=100%>
	  					<tr>
	  						<td>Remarks</td>
	  						<td><input type='text' name='remarks' id='remarks' value='' > <input type='hidden' name='idBatchRegistration' id='idBatchRegistration' value='".$idBatchRegistration."' > </td>
	  					</tr>
	  					<tr>
	  						<td>Payment Details</td>
	  						<td><input type='text' name='chequedetails' id='chequedetails' value='' ><input type='hidden' name='companyflag' id='companyflag' value='".$idcompanyflag."' > </td>
	  					</tr>
	  					<tr>
	  						<td></td>
	  						<td><input type='submit' id='submit' name='submit'  value='Cancel Payment' > &nbsp;<input type='button' id='close' name='close'  value='Close' onClick='Closefn();'> </td>
	  					</tr>
	  				</table>";	
	  					
	  
	  echo  $tabledata;
		die();
		
	}	
public function fncancelpaymentindudualAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$ICNO = $this->_getParam('ICNO');
		//$idBatchRegistration = $this->_getParam('idBatchRegistration');
		$lobjpaymentdetails = $this->lobjPaymentcancelmodel->fnGetpaymentsIndidual($ICNO);
		//echo "<pre>";print_r($lobjpaymentdetails);
		
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Student Name</b></th>
					
					<th><b>Amount</b></th>
					
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					
					<td><b>".$lobjpaymentdetails[0]['Amount']."</b></td>
					
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'Direct Debit FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 5) $tabledata.= 'Money Order';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 6) $tabledata.= 'Postal Order';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 7) $tabledata.= 'Credit/Bank to IBFIM account';		
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		
		$tabledata.= "</b></td>
					</tr>
					</table><br>";
	  $tabledata.="<table  class='table' border=1 align='center' width=100%>
	  					<tr>
	  						<td>Remarks</td>
	  						<td><input type='text' name='remarks' id='remarks' value='' > <input type='hidden' name='IDApplication' id='IDApplication' value='".$lobjpaymentdetails[0]['IDApplication']."' > </td>
	  					</tr>
	  					<tr>
	  						<td>Payment Details</td>
	  						<td><input type='text' name='chequedetails' id='chequedetails' value='' ><input type='hidden' name='companyflag' id='companyflag' value='3' > </td>
	  					</tr>
	  					<tr>
	  						<td></td>
	  						<td><input type='submit' id='submit' name='submit'  value='Cancel Payment' > &nbsp;<input type='button' id='close' name='close'  value='Close' onClick='Closefn();'> </td>
	  					</tr>
	  				</table>";	
	  					
	  
	  echo  $tabledata;
		die();
		
	}		
}


