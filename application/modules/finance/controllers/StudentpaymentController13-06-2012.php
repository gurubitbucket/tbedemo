﻿<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_StudentpaymentController extends Base_Base 
{
	public $gsessionregistration;
	private $_gobjlogger;
	//public $gsessionemail;
	public function init()
    {
    	$this->gsessionregistration = Zend_Registry::get('sis'); 	
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
    }
    public function fnsetObj()
    {
		$this->lobjCompanypayment = new Finance_Model_DbTable_Studentpayment();
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
		$this->lobjCompanypaymentForm = new Finance_Form_Studentpayment();  	
	}
    public function indexAction()
    {
    	$this->gsessionregistration->mails=0;
    	//print_r($this->gsessionregistration->mails);
       	$this->view->title="Company Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjCompanypayment->fngetCompanyDetails(); //get user details
        /*echo('<pre>');
		print_r($larrresult);die();*/
		$lintpagecount =10000000;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->studentpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->studentpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
		 $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjCompanypayment->fnSearchCompanyPayment($larrformData['field3']); //searching the values for the user
				/*echo('<pre>');
				print_r($larrresult);
				die();*/
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->studentpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/finance/studentpayment/index');
			}
	
    }
    
    public function studentpaymenteditAction()
    {
    	
    	$this->view->lobjCompanypaymentForm = $this->lobjCompanypaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	$this->view->idcompany = $lstrType;
    	$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
    	/*print_r($lstrType);
    	die();*/
    	$larrstudentname=$this->lobjCompanypayment->fngetstudentname($lstrType);
    	
    	
    	
    	//$larrcoursename=$this->lobjCompanypayment->fngetcoursename($lstrType);
      //  echo ('<pre>');
    	//print_r($larrstudentname);
    	//die();
    	//$this->view->coursedetails = $larrcoursename;
		$this->view->icno =$larrstudentname['ICNO'];
    	$this->view->ProgramName =$larrstudentname['ProgramName'];
    	$this->view->TakafulName =$larrstudentname['TakafulName'];
    	$this->view->mop=$larrstudentname['ModeofPayment'];
    	$this->view->FName=$larrstudentname['FName'];
    	$this->view->center =$larrstudentname['centername'];
    	$this->view->applieddate = $larrstudentname['Applieddate'];
    	$this->view->examdate = $larrstudentname['Examdate'].'-'.$larrstudentname['Exammonth'].'-'.$larrstudentname['years'];
        $this->lobjCompanypaymentForm->Amount->setValue($larrstudentname['Amount']);
        
        $larrbanknames=$this->lobjCompanypayment->fnGetBankDetails();        
        $this->view->lobjCompanypaymentForm->BankName->addMultiOptions($larrbanknames);
        
	
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
    			$larrformData = $this->_request->getPost ();  
    			/*print_r($larrformData);
    			die();*/  
    			$ldtsystemDate = date ( 'Y-m-d:H-i-s' );	
    			/*print_r($ldtsystemDate);
    			die();*/
    			$larrformData['UpdDate']=	$ldtsystemDate;	
    			$larrformData['IDApplication']=$lstrType;
    			$larrformData['companyflag']=0;    		 
    			 
    			if($larrstudentname['ModeofPayment']!=4)
    			{
    			$larrformData['BankName']=0;
    			}
    			$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption($larrformData);
    			$lobjexamstudentattended = $this->lobjCompanypayment->fncheckavailable($larrstudentname['IDApplication']);
    			if(count($lobjexamstudentattended)>0)
    			{
    				
    			}
    			else {
    			$larrregdetails=$this->lobjCompanypayment->InsertRegisterdetails($larrstudentname['IDApplication'],$larrstudentname['Takafuloperator'],$larrstudentname['IdBatch']);
    			}
    			
    			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    						
					//$this->view->mess = "Payment Completed Sucessfully";
					//$this->view->mess = "Payment Completed Sucessfully <br/> Please check your mail box If you have not received a confirmation mail in next 30minutes<br/>Please check your spam folder Add ibfiminfo@gmail.com to the address book to ensure future communications doesn�t go to the spam folder";
					$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($larrstudentname['IDApplication']);	
					$larrregid = $this->lobjstudentmodel->fngetRegid($larrstudentname['IDApplication']);
					
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
									
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'].' '.$larrresult['addr2'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['password'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
										$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
									//$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									//echo '<script language="javascript">alert("Because of server problem mails cannot be send this time")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
								
								 //$this->_redirect( $this->baseUrl . "/registration/index");
								if(mess){
									
								}
				$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Completed the Student payment for id = ".$lstrType."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);	
    			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    			$this->_redirect( $this->baseUrl . '/finance/studentpayment/index');
    	}
    }

public function printreportAction() 
	{			
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		
		$IDAPPLICATION =(int) $this->_getParam('idapplication');

		//$larrcompanydetails=$this->lobjCompanypayment->fngetCommpanyreportDetails($IDAPPLICATION);

		//$totamt=(int)$larrcompanydetails[0]['totalAmount'];
		
		//$Amount = $this->lobjCompanypayment->fnGetAmountInWords($totamt);

		//$AmountInWords=$Amount['Amount'];

		
		//object to initialize ini file
		$lobjAppconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini','development');
									
		    try 
		    {	
	            //java class
	            $lobjdbdriverclass = new Java("java.lang.Class");
	            
	            //set db driver
	            $lobjdbdriverclass->forName("com.mysql.jdbc.Driver");
	
	            //driver manager object
	            $lobjdrivermanager = new Java("java.sql.DriverManager");
	            
	            //get the db connection
				$lstrConnection  =  "jdbc:mysql://".
										$lobjAppconfig->resources->db->params->host."/".
										$lobjAppconfig->resources->db->params->dbname."?user=".
										$lobjAppconfig->resources->db->params->username."&password=".
										$lobjAppconfig->resources->db->params->password;
														
				$lobjconnection = $lobjdrivermanager->getConnection($lstrConnection);
	            
	            //Jasper Compile manager object
	            $lobjcompileManager = new Java(
	            					"net.sf.jasperreports.engine.JasperCompileManager");
	            
	            echo "CompileManager object created</br>";
	            $lstrreportdir = realpath(".") . "/report/";
	            $lstrimagepath = realpath(".") . "/images/";
	             
	             //compiled report path
	              $lobjreport = $lobjcompileManager->compileReport(realpath($lstrreportdir."companyapplicationreport.jrxml"));
	            
	            //Jasper Fill Manager object
	            $lobjfillManager = new Java(
	            					"net.sf.jasperreports.engine.JasperFillManager");
	            $int1 = new Java("java.lang.Integer");
	            //Hashmap object
	            //print_r($lstrreportdir);die();
	            $lobjparams = new Java("java.util.HashMap");
	          	$lobjparams->put("IDAPPLICATION",$IDAPPLICATION);
	          	//$lobjparams->put("AMOUNTINWORDS",$AmountInWords);
	          	$lobjparams->put ("IMAGEPATH", $lstrimagepath . "reportheader.jpg" );
	           
	           echo "Fill Manager</br>";
	            					
	            //Jasper Print Object
	            $lobjjasperPrint = $lobjfillManager->fillReport(
	            					$lobjreport, $lobjparams, $lobjconnection);
	            					
	            echo "Jasper Printed</br>";
	            
	            //Jasper Export Manager object
	            $lobjexportManager = new Java(
	            					"net.sf.jasperreports.engine.JasperExportManager");
	            
	            //output file path
	            $lstrhtmloutputPath = realpath(".") . "/" . "output.html";
	            echo "Before Export</br>";
	            $session = Zend_Session::getId();
	            $lstrpdfoutputPath = realpath(".") . "/" . "$session.pdf";
	            $objStream = new Java("java.io.ByteArrayOutputStream");
	            $lobjexportManager->exportReportToPdfFile($lobjjasperPrint,$lstrpdfoutputPath);
	            
	            //Export report to HTML	            
	            echo 'HTML Exported</br>';
	
				header("Content-type: application/pdf;charset=utf-8;encoding=utf-8");
				header('Content-Disposition: attachment; filename="Company_Application.pdf"');
				
	            readfile($lstrpdfoutputPath);
	            unlink($lstrpdfoutputPath);
				echo "finished";	
		 		 		            
		    } 
		    catch (JavaException $lobjexception) 
		    {
		    	echo 'Exception caught: ', $lobjexception->getMessage() . "\n";
		    }		    		   
			
	    }
              
}

