<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Finance_PaymentcancelController extends Base_Base{

	public function init(){
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjPaymentcancelmodel = new Finance_Model_DbTable_Paymentcancelmodel();
		$this->lobjpaymentForm = new Finance_Form_Paymentform ();
	}

	public function indexAction(){
		$this->view->title="Pyament Cancel Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjform object to the view
		$this->view->lobjpaymentForm = $this->lobjpaymentForm;
		$larrprogramname[0][key]= 1;
		$larrprogramname[0][value]= "Comapny";
		$larrprogramname[1][key]= 2;
		$larrprogramname[1][value]= "Takaful";
		$this->view->lobjform->field15->addMultiOptions($larrprogramname); //added program names to form dropdown element
		$ldtsystemDate = date( 'Y-m-d H:i:s' );   //current Date
		$this->view->lobjpaymentForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();        //creating An Authentication object
		$this->view->lobjpaymentForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		/*$larrresult = $this->lobjPaymentmodel->fnGetStudentpaymentDetails(); //Call a function to get candidates payment details by paypal
		$this->view->count=count($larrresult); //total number of candidates
		$this->view->countcomp = $larrresult; //sending candidates payment details to view*/
		
		
		$lintidstudent = $this->_getParam('idapplication');
		if($lintidstudent)
		{
			//echo abc;die();
				$larrresults = $this->lobjPaymentmodel->fngetpaymentidSearch($lintidstudent); //Call a function to searching the values for the candidates
				$this->view->count=count($larrresults);
				$this->view->countcomp = $larrresults;
				$this->view->flgs=1;
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )){ //for Search
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )){
				
				$larrresult = $this->lobjPaymentcancelmodel->fnGetpayments($larrformData[field15],$larrformData[field4]); //Call a function to searching the values for the candidates
				$this->view->lobjform->field15->setValue( $larrformData[field15] );
				$this->view->count=count($larrresult);
				$this->view->countcomp = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )){//for Clear
			$this->_redirect( $this->baseUrl . '/finance/paymentcancel/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'submit' )){
			$larrformData = $this->_request->getPost ();			
			$larrformData['UpdDate'] = date( 'Y-m-d H:i:s' );
			$larrformData['UpdUser'] =  $auth->getIdentity()->iduser;			
			$this->lobjPaymentcancelmodel->fnCancelPayment($larrformData);			
			$larrresult = $this->lobjPaymentcancelmodel->fnGetpayments($larrformData[companyflag]); //Call a function to searching the values for the candidates
			$this->view->lobjform->field15->setValue( $larrformData[companyflag] );
			$this->view->count=count($larrresult);
			$this->view->countcomp = $larrresult;			
		}
	}
public function fncancelpaymentAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$idcompanyflag = $this->_getParam('companyflag');
		$idBatchRegistration = $this->_getParam('idBatchRegistration');
		$lobjpaymentdetails = $this->lobjPaymentcancelmodel->fngetpaymentdetails($idBatchRegistration,$idcompanyflag);
		//echo "<pre>";print_r($lobjpaymentdetails);
		
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Company Name</b></th>
					<th><b>Pin Number</b></th>
					<th><b>Amount</b></th>
					<th><b>Number Of Candidates</b></th>
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['registrationPin']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalAmount']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalNoofCandidates']."</b></td>
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		
		$tabledata.= "</b></td>
					</tr>
					</table><br>";
	  $tabledata.="<table  class='table' border=1 align='center' width=100%>
	  					<tr>
	  						<td>Remarks</td>
	  						<td><input type='text' name='remarks' id='remarks' value='' > <input type='hidden' name='idBatchRegistration' id='idBatchRegistration' value='".$idBatchRegistration."' > </td>
	  					</tr>
	  					<tr>
	  						<td>Payment Details</td>
	  						<td><input type='text' name='chequedetails' id='chequedetails' value='' ><input type='hidden' name='companyflag' id='companyflag' value='".$idcompanyflag."' > </td>
	  					</tr>
	  					<tr>
	  						<td></td>
	  						<td><input type='submit' id='submit' name='submit'  value='Cancel Payment' > &nbsp;<input type='button' id='close' name='close'  value='Close' onClick='Closefn();'> </td>
	  					</tr>
	  				</table>";	
	  					
	  
	  echo  $tabledata;
		die();
		
	}	
}


