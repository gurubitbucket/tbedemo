<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_TakafultypepaymentController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjTakafultypepaymentModel = new Finance_Model_DbTable_Takafultypepayment();
		$this->lobjTakafultypepaymentForm = new Finance_Form_Takafultypepayment ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Company Payment Terms";
		$this->view->lobjform = $this->lobjform;
		$larrresult = $this->lobjTakafultypepaymentModel->fngetCompanyDetails(); //get user details
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		    $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjTakafultypepaymentModel->fnSearchCompanyTypePayment($larrformData['field3']); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->semesterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 		echo "<script>parent.location = '".$this->view->baseUrl()."/finance/takafultypepayment/index';</script>";
			}
	
    }
    
    public function takafultypepaymentaddAction()
    {
    	
    	$this->view->lobjTakafultypepaymentForm = $this->lobjTakafultypepaymentForm;
    	
    	$larrcompanyname = $this->lobjTakafultypepaymentModel->fnTakafuloperatorname();
		$this->lobjTakafultypepaymentForm->idtakafuloperator->addMultiOptions($larrcompanyname);
		
		$larrcompanyname = $this->lobjTakafultypepaymentModel->fnGetPaymentTerms();
		$this->lobjTakafultypepaymentForm->paymenttype->addMultiOptions($larrcompanyname);
		
		$ldtsystemDate = date('Y-m-d:H-i-s');
		$this->lobjTakafultypepaymentForm->upddate->setValue($ldtsystemDate);
		
		$upduser = 1;//$auth->getIdentity()->iduser;
		$this->lobjTakafultypepaymentForm->upduser->setValue($upduser);
		
       if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
		    $larrformData = $this->_request->getPost ();
		    $larrresult = $this->lobjTakafultypepaymentModel->fninsertdata($larrformData);
			echo "<script>parent.location = '".$this->view->baseUrl()."/finance/takafultypepayment/index';</script>";
		}
		
    }
    
    
 public function takafultypepaymenteditAction()
    {
    	$idcompanypaymenttype = $this->_getParam('lvaredit');
    	

    	$this->view->lobjTakafultypepaymentForm = $this->lobjTakafultypepaymentForm;
    	$this->lobjTakafultypepaymentForm->idtakafulpaymenttype->setValue($idcompanypaymenttype);
    	
    	$larrcompanyname = $this->lobjTakafultypepaymentModel->fnGetcompanyNameedit();
    		$this->lobjTakafultypepaymentForm->idtakafuloperator->setAttrib(readonly,true);
		$this->lobjTakafultypepaymentForm->idtakafuloperator->addMultiOptions($larrcompanyname);
		
		$larrcompanyname = $this->lobjTakafultypepaymentModel->fnGetPaymentTerms();
		$this->lobjTakafultypepaymentForm->paymenttype->addMultiOptions($larrcompanyname);
		
		$ldtsystemDate = date('Y-m-d:H-i-s');
		$this->lobjTakafultypepaymentForm->upddate->setValue($ldtsystemDate);
		
		$upduser = 1;//$auth->getIdentity()->iduser;
		$this->lobjTakafultypepaymentForm->upduser->setValue($upduser);
		
		$editresult = $this->lobjTakafultypepaymentModel->editcompanytypepayment($idcompanypaymenttype);
	/*	print_r($editresult);
		die();*/
	     $this->lobjTakafultypepaymentForm->populate($editresult);
			//$this->view->lobjTakafultypepaymentForm->idcompany->setValue($editresult['idcompany']);
		
       if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
		    $larrformData = $this->_request->getPost ();
		    $idcompanypmttype = $larrformData['idtakafulpaymenttype'];
		    unset($larrformData['idtakafulpaymenttype']);
		    unset($larrformData['Save']);
		    $larrresult = $this->lobjTakafultypepaymentModel->fnupdatecompanytypepayment($larrformData,$idcompanypmttype);
			echo "<script>parent.location = '".$this->view->baseUrl()."/finance/takafultypepayment/index';</script>";
		}
		
    }
}

