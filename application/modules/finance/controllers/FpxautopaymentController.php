<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_FpxautopaymentController extends Base_Base 
{

    public function init()
    {
        $this->lobjPaymentmodel = new Finance_Model_DbTable_Fpxpayment();
		$this->lobjpaymentForm = new Finance_Form_Fpxpayment (); 
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }   
    public function indexAction()   {
       	$this->view->title="Fpx Manual Pyament Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		
		$this->view->lobjpaymentForm = $this->lobjpaymentForm;
		$larrprogramname = $this->lobjPaymentmodel->fnGetprogramname();		
		$this->view->lobjform->field15->addMultiOptions($larrprogramname);
		$ldtsystemDate = date( 'Y-m-d H:i:s' );
		$this->view->lobjpaymentForm->UpdDate->setValue($ldtsystemDate);
		
		$auth = Zend_Auth::getInstance();
		//echo $auth->getIdentity()->iduser;
		$this->view->lobjpaymentForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		/*$larrresult = $this->lobjPaymentmodel->fnGetStudentresponsepaymentDetails(); //get user details
		for($i=0;$i<count($larrresult);$i++){
			$year=substr($larrresult[$i]['TxnOrderNo'] , 0 ,4);//explode(chr(8), $larrresult[$i]['TxnOrderNo']);
			$month=substr($larrresult[$i]['TxnOrderNo'] , 4 ,2);
			$day=substr($larrresult[$i]['TxnOrderNo'] , 6 ,2);
			$larrresult[$i]['TransactionDate']=$year."-".$month."-".$day;
		}
		$this->view->count=count($larrresult);
		$this->view->countcomp = $larrresult;*/
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjPaymentmodel->fngetautopaymentSearch($larrformData); //searching the values for the user
			//	print_r($larrresult);die();
				for($i=0;$i<count($larrresult);$i++)
		           {
		             $year=substr($larrresult[$i]['TxnOrderNo'] , 0 ,4);//explode(chr(8), $larrresult[$i]['TxnOrderNo']);
		             $month=substr($larrresult[$i]['TxnOrderNo'] , 4 ,2);
		             $day=substr($larrresult[$i]['TxnOrderNo'] , 6 ,2);
		             $larrresult[$i]['TransactionDate']=$year."-".$month."-".$day;
		           }
		          // print_r($larrresult);die();
				$this->view->count=count($larrresult);
				$this->view->countcomp = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/finance/fpxautopayment/index');			
		}
    if ($this->_request->isPost () && $this->_request->getPost ( 'Confirm' )) {
		 				 		
		 	$larrformData = $this->_request->getPost ();
		 	unset ( $larrformData ['field3'] );
		 	unset ( $larrformData ['field4'] );
		 	unset ( $larrformData ['Confirm'] );
		 	
		    for($i=0;$i<count($larrformData['IDApplication']);$i++){    	
		    	    $lintidApplication = $larrformData ['IDApplication'][$i];		    	   			    	
					$details=$this->lobjPaymentmodel->fnselectstudentforpayment($lintidApplication);
					/*echo "<pre>";
		 	         print_r($details);
		 	         die();*/
					$larrformData['IdBatch'][$i]=$details[0]['IdBatch'];	
		 			$larrformData['Takafuloperator'][$i]=$details[0]['Takafuloperator'];
		 			$larrformData['FName'][$i]=$details[0]['FName'];	
		 			$larrformData['ICNO'][$i]=$details[0]['ICNO'];	
		 			$larrformData['Program'][$i]=$details[0]['Program'];
		 			$larrformData['DateTime'][$i]=$details[0]['DateTime'];
		 			$larrformData['Examvenue'][$i]=$details[0]['Examvenue'];
		 			$larrformData['Examsession'][$i]=$details[0]['Examsession'];	

		 				$larricnoget=$this->lobjPaymentmodel->fngeticnoofstu($lintidApplication);	
		    }
		   /*echo "<pre>";
		 	print_r($larrformData);
		 	die();*/
		    
		 	 $this->lobjPaymentmodel->fninsertfpxpaymentdetails($larrformData);
		 	 $this->lobjPaymentmodel->fninsertmanualfpxpaymentdetails($larrformData);
		 	 $this->lobjPaymentmodel->fninsertregistereddetails($larrformData);
		 	 
		 /*	for($i=0;$i<count($larrformData['IDApplication']);$i++){
		 		$DateTime=$larrformData['DateTime'][$i];
		 		$Examvenue=$larrformData['Examvenue'][$i];
		 		$Examsession=$larrformData['Examsession'][$i];
		 	   // $this->lobjPaymentmodel->fnUpdateAllotedseats($DateTime,$Examvenue,$Examsession);	 	
		 	}	*/
		 	 			 	
		     for($i=0;$i<count($larrformData['IDApplication']);$i++){
		    	    $lintidApplication = $larrformData ['IDApplication'][$i];    			    	
					$this->lobjPaymentmodel->fnupdatestudentapplication($lintidApplication);												    	
		    }   
		         $this->_redirect( $this->baseUrl . '/finance/fpxautopayment/index');
		 	}
		 	
		 }
    }


