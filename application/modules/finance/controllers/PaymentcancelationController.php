<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Finance_PaymentcancelationController extends Base_Base{

	public function init()
	{
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		//$this->lobjPaymentcancelmodel = new Finance_Model_DbTable_Paymentcancelmodel();
		$this->lobjPaymentcancelmodel = new Finance_Model_DbTable_Paymentcancelation();
		$this->lobjPaymentcancelform = new Finance_Form_Paymentcancelation();
		$this->lobjform = new App_Form_Search ();
		//$this->lobjpaymentForm = new Finance_Form_Paymentform ();
	}

	public function indexAction()
	{
	    $this->view->lobjPaymentcancelform = $this->lobjPaymentcancelform;	
		$this->view->title="Common Pyament Cancel Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjform object to the view		
		$this->view->companytakaful = 0;	
        $this->lobjPaymentcancelform->Company->setAttrib('onkeyup', 'fnGetOperatorNames');		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' ))//for Clear
		{
			$this->_redirect( $this->baseUrl . '/finance/paymentcancelation/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' ))//for Search
		{ 
			$larrformData = $this->_request->getPost ();
			$this->view->companytakaful = $larrformData['CT'];
			$this->view->lobjPaymentcancelform->CT->setValue($larrformData['CT']);
			if ($this->lobjform->isValid ( $larrformData))
			{
			   $this->view->lobjPaymentcancelform->Company->setValue($larrformData['Company']);
			    if(!$larrformData['CT'])
				{
							echo '<script language="javascript">alert("Please Select Type Of Company")</script>';
							echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/paymentcancelation/index/';</script>";					
							exit;
				}
				if($larrformData['CT'] == 1)
				{
					
					$larrresult = $this->lobjPaymentcancelmodel->fnGetpaymentdetailscompany($larrformData); //Call a function to searching the values for the candidates
				    $this->view->count=count($larrresult);
				    $this->view->countcomp = $larrresult;
					$this->lobjPaymentcancelform->populate($larrresult);   //to populate form data	
				}
				if($larrformData['CT'] == 2)
				{
					//$this->view->lobjPaymentcancelform->Takaful->setValue($larrformData['Takaful']);
					$larrresult = $this->lobjPaymentcancelmodel->fnGetpaymentdetailstakaful($larrformData); //Call a function to searching the values for the candidates
				    $this->view->count=count($larrresult);
				    $this->view->countcomp = $larrresult;
					$this->lobjPaymentcancelform->populate($larrresult);   //to populate form data	
				}
			}
		}
		
		
	}
	//New auto search option in index page Ation 27-9-2012
	public function editAction()
	{
	    $this->view->idcompany = $idcompany = $this->_getParam('idcompany');	  
	    $this->view->idopt = $idopt = $this->_getParam('idct');
	 
	    $companyname = $this->lobjPaymentcancelmodel->fnGetName($idcompany,$idopt);
	    $this->view->Name = $companyname['Name'];
	  
	    $this->view->lobjPaymentcancelform = $this->lobjPaymentcancelform;
        $this->view->lobjform = $this->lobjform; //send the lobjform object to the view			   
	   
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' ))//for Search
		{ 
			  $larrformData = $this->_request->getPost ();		
			  $laresulttemppin=$this->lobjPaymentcancelmodel->fnGettempregpins($larrformData,$idcompany,$idopt);
		
			  $temppins=array();
			  for($temp=0;$temp<count($laresulttemppin);$temp++)
			  {
				   $temppins[]=$laresulttemppin[$temp]['pins'];
			  }
			  $this->view->temppins = $temppins;
			  $larrresult = $this->lobjPaymentcancelmodel->fnGetregpins($larrformData,$idcompany,$idopt);
			  
			  $this->view->countcomp = $larrresult;
			  $this->lobjPaymentcancelform->populate($larrformData);
		}
        if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' ))//for Clear
		{
			$this->_redirect( $this->baseUrl . '/finance/paymentcancelation/edit/idcompany/'.$idcompany.'/idct/'.$idopt);
		}	
        if ($this->_request->isPost () && $this->_request->getPost ( 'submit' ))
		{
			$larrformData = $this->_request->getPost ();	
            // echo "<pre>";print_r($larrformData);die();			
			$larrformData['UpdDate'] = date( 'Y-m-d H:i:s' );
			$auth = Zend_Auth::getInstance(); 
			$larrformData['UpdUser'] =  $auth->getIdentity()->iduser;	
		    $laresulttemppin=$this->lobjPaymentcancelmodel->fnGettempregpins($larrformData,$idcompany,$idopt);
		    //echo "<pre/>";		
			$temppins=array();
			for($temp=0;$temp<count($laresulttemppin);$temp++)
			{
			$temppins[]=$laresulttemppin[$temp]['pins'];
			}
			$this->view->temppins = $temppins;
			$this->lobjPaymentcancelmodel->fnCancelPayment($larrformData,$idcompany,$idopt);			
			$larrresult = $this->lobjPaymentcancelmodel->fnGetregpins($larrformData,$idcompany,$idopt); //Call a function to searching the values for the candidates
            
			$this->view->lvartype  = $larrformData['lvartype'];
            //$this->view->lobjform->field15->setValue( $larrformData['lvartype'] );
			$this->view->count=count($larrresult);
			$this->view->countcomp = $larrresult;
		}		
	}
	public function getoperatornamesAction()
	{
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$newresult="";
		$opType = $this->_getParam('opType');	
		$namestring = $this->_getParam('namestring');	
			
		$larrresultopnames = $this->lobjcompanystudentdetails->fnGetOperatorNames($opType,$namestring); 
		//echo Zend_Json_Encoder::encode ( $larrresultopnames );
		//exit;
		
		foreach ($larrresultopnames as $larrresnewarray){
						$opname=$larrresnewarray['name'];
			$newresult=$newresult."<tr><td><span id='idspan'   onclick='fnsetvalue(\"".$opname."\");'>".$larrresnewarray['name']."</span></td></tr></BR>";			
		}
		
		echo $newresult;
		exit;
		
		
	}
    public function fncancelpaymentAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$idcompanyflag = $this->_getParam('companyflag');
		$idBatchRegistration = $this->_getParam('idBatchRegistration');
		
		$lobjpaymentdetails = $this->lobjPaymentcancelmodel->fngetpaymentdetails($idBatchRegistration,$idcompanyflag);
		//echo "<pre>";print_r($lobjpaymentdetails);
		
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Company Name</b></th>
					<th><b>Pin Number</b></th>
					<th><b>Amount</b></th>
					<th><b>Number Of Candidates</b></th>
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['registrationPin']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalAmount']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalNoofCandidates']."</b></td>
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 7) $tabledata.= 'Credit to IBFIM';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 10) $tabledata.= 'MIGS';
		
		$tabledata.= "</b></td>
					</tr>
					</table><br>";
	    $tabledata.="<table  class='table' border=1 align='center' width=100%>
	  					<tr>
	  						<td>Remarks</td>
	  						<td><input type='text' name='remarks' id='remarks' value='' > <input type='hidden' name='idBatchRegistration' id='idBatchRegistration' value='".$idBatchRegistration."' > </td>
	  					</tr>
	  					<tr>
	  						<td>Payment Details</td>
	  						<td><input type='text' name='chequedetails' id='chequedetails' value='' ><input type='hidden' name='companyflag' id='companyflag' value='".$idcompanyflag."' > </td>
	  					</tr>
	  					<tr>
	  						<td></td>
	  						<td><input type='submit' id='submit' name='submit'  value='Payment Cancel' > &nbsp;<input type='button' id='close' name='close'  value='Close' onClick='Closefn();'> </td>
	  					</tr>
	  				</table>";	
	  					
	  
	  echo  $tabledata;
		die();
		
	}	
public function fncancelpaymentindudualAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$ICNO = $this->_getParam('ICNO');
		//$idBatchRegistration = $this->_getParam('idBatchRegistration');
		$lobjpaymentdetails = $this->lobjPaymentcancelmodel->fnGetpaymentsIndidual($ICNO);
		//echo "<pre>";print_r($lobjpaymentdetails);
		
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Student Name</b></th>
					
					<th><b>Amount</b></th>
					
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					
					<td><b>".$lobjpaymentdetails[0]['Amount']."</b></td>
					
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'Direct Debit FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 5) $tabledata.= 'Money Order';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 6) $tabledata.= 'Postal Order';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 7) $tabledata.= 'Credit/Bank to IBFIM account';		
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		
		$tabledata.= "</b></td>
					</tr>
					</table><br>";
	  $tabledata.="<table  class='table' border=1 align='center' width=100%>
	  					<tr>
	  						<td>Remarks</td>
	  						<td><input type='text' name='remarks' id='remarks' value='' > <input type='hidden' name='IDApplication' id='IDApplication' value='".$lobjpaymentdetails[0]['IDApplication']."' > </td>
	  					</tr>
	  					<tr>
	  						<td>Payment Details</td>
	  						<td><input type='text' name='chequedetails' id='chequedetails' value='' ><input type='hidden' name='companyflag' id='companyflag' value='3' > </td>
	  					</tr>
	  					<tr>
	  						<td></td>
	  						<td><input type='submit' id='submit' name='submit'  value='Cancel Payment' > &nbsp;<input type='button' id='close' name='close'  value='Close' onClick='Closefn();'> </td>
	  					</tr>
	  				</table>";	
	  					
	  
	  echo  $tabledata;
		die();
		
	}		
}


