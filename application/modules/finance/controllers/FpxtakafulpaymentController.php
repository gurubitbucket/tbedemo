<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Finance_FpxtakafulpaymentController extends Base_Base 
{
    public function init() {
        $this->lobjPaymentmodel = new Finance_Model_DbTable_Paymentcompanymodel();
		$this->lobjpaymentForm = new Finance_Form_Fpxpayment (); 
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }   
    public function indexAction(){
       	$this->view->title="Fpx Manual Pyament Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view		
		$this->view->lobjpaymentForm = $this->lobjpaymentForm;
		$ldtsystemDate = date( 'Y-m-d H:i:s' );
		$this->view->lobjpaymentForm->UpdDate->setValue($ldtsystemDate);		
		$auth = Zend_Auth::getInstance();		
		$this->view->lobjpaymentForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		//$larrresult = $this->lobjPaymentmodel->fnGetFPXTakafulpaymentDetails(); //get user details	
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjPaymentmodel->fnGetFPXTakafulpaymentSearchDetails($larrformData); //searching the values for the user			
			}
		}
		for($linti=0;$linti<count($larrresult);$linti++){
		             $year=substr($larrresult[$linti]['TxnOrderNo'] , 0 ,4);//explode(chr(8), $larrresult[$i]['TxnOrderNo']);
		             $month=substr($larrresult[$linti]['TxnOrderNo'] , 4 ,2);
		             $day=substr($larrresult[$linti]['TxnOrderNo'] , 6 ,2);
		             $larrresult[$linti]['TransactionDate']=$year."-".$month."-".$day;
        }
		$this->view->count=count($larrresult);
		
		$this->view->countcomp = $larrresult;
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/finance/fpxtakafulpayment/index');			
		}
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Confirm' )) {		 				 		
		 	$larrformData = $this->_request->getPost ();
		 	unset ( $larrformData ['field3'] );
		 	unset ( $larrformData ['Confirm'] );		 	
		 	$Takafulapplicationmodel = new App_Model_Takafulapplication();
		 	$StudModel 		  	  = new App_Model_Studentapplication();		 	
		 	$lvarTemplateD        = $StudModel->fnGetEmailTemplateDescription("Takaful Payment");		 	
		    for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){	    	
			    $idPayment = $larrformData ['IDApplication'][$linti];
				$idtakafuloperator = $larrformData ['idtakafuloperator'][$idPayment];
	
				$dataArray['payerMailId'] =	$larrformData ['Email'][$idPayment];
				$dataArray['grossAmount'] =$larrformData ['totalAmount'][$idPayment];
				$dataArray['orderNumber'] =	$larrformData ['TxnOrderNo'][$idPayment];
				$dataArray['TxnDate'] = 	$larrformData ['TransactionDate'][$idPayment];//date('Y-m-d:H-i-s');
				$dataArray['fpxTxnId'] =	$larrformData ['FPXTXNID'][$idPayment];
				$dataArray['bankCode'] =	$larrformData ['BUYERBANK'][$idPayment];
				$dataArray['bankBranch'] =	$larrformData ['BUYERBANKBRANCH'][$idPayment];
				$dataArray['debitAuthCode'] =	$larrformData ['DEBITAUTHCODE'][$idPayment];
				$dataArray['debitAuthNo'] =	$larrformData ['DEBITAUTHNO'][$idPayment];
				$dataArray['creditAuthCode'] =	$larrformData ['CREDITAUTHCODE'][$idPayment];
				$dataArray['creditAuthNo'] =	$larrformData ['CREDITAUTHNO'][$idPayment];			
				$dataArray['UpdUser'] = 1;
				$dataArray['UpdDate'] = date('Y-m-d:H-i-s');
				$dataArray['IDApplication'] = $idPayment;
		   		$dataArray['entryFrom'] = 3;					
				$dataArray['paymentStatus']	 =1;	
				
		    	 $larrresregpin=$this->lobjPaymentmodel->fnGetregpin($idPayment);
		    	 if($larrresregpin){		
		    	 	$larrformData1['registrationPin'] = substr($dataArray['fpxTxnId'], 1, 6).rand(1000, 9999).substr($dataArray['fpxTxnId'], 5, 9);
					$larrformData1['paymentStatus'] = 1;	
					$larrformData1['Approved'] = 1;
		 		}else{ 		
					$larrformData1['paymentStatus'] = 1;	
					$larrformData1['Approved'] = 1;
			 	}
				
				/*$larrformData1['registrationPin'] = substr($dataArray['fpxTxnId'], 1, 6).rand(1000, 9999).substr($dataArray['fpxTxnId'], 5, 9);
				$larrformData1['paymentStatus'] = 1;	
				$larrformData1['Approved'] = 1;	*/			
				
		    	$this->lobjPaymentmodel->fpxpayment($idPayment,$larrformData1,$dataArray);
				$larrStudentMailingDetails = $Takafulapplicationmodel->fngetTakafulOperator($idtakafuloperator);				
						
				//Get Email Template Description
				$larrEmailTemplateDesc =  $lvarTemplateD;						
				if($larrEmailTemplateDesc['TemplateFrom']!=""){
						$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
						$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
						$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
						$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
						$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];						
						$lstrCompanyName = $larrStudentMailingDetails['TakafulName'];
							
							$lstrEmailTemplateBody = str_replace("[Person]",$lstrCompanyName,$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Amount]",$dataArray['grossAmount'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Username]",$larrStudentMailingDetails["email"],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[RegPin]",$larrformData1['registrationPin'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Password]", substr($larrStudentMailingDetails['hint'], 5),$lstrEmailTemplateBody);//substr($string, 5)
							$lstrEmailTemplateBody = str_replace("[Link]","http://www.takafuleexam.com/tbe/takafullogin/login",$lstrEmailTemplateBody);
					    	$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;	
						//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
				    	$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;					    	
				    	$this->lobjPaymentmodel->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$larrStudentMailingDetails["email"],$larrStudentMailingDetails['TakafulName']);
		    	}		
		    }
		    $this->_redirect( $this->baseUrl . '/finance/fpxtakafulpayment/index');
		 }		 	
	}	 
}


