<?php
class Finance_Model_DbTable_Paymentmodel extends  Zend_Db_Table  {
	protected $_name = 'tbl_paypaldetails';			
	
	public function fnGetprogramname(){//Function to get program names 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_programmaster" => "tbl_programmaster"),array("key"=>"tbl_programmaster.IdProgrammaster","value"=>"tbl_programmaster.ProgramName"))
										  ->join(array("tbl_program" => "tbl_program"),'tbl_program.IdProgram=tbl_programmaster.idprog',array())
										  ->join(array("tbl_batchmaster"=>"tbl_batchmaster"),'tbl_batchmaster.IdProgrammaster=tbl_programmaster.IdProgrammaster',array())
										  ->join(array("tbl_tosmaster"=>"tbl_tosmaster"),'tbl_batchmaster.IdBatch=tbl_tosmaster.IdBatch',array())
  										  ->join(array("tbl_programrate"=>"tbl_programrate"),'tbl_programmaster.IdProgrammaster=tbl_programrate.idProgram',array())
										  ->where("tbl_programrate.Active=1")
										  ->where("tbl_tosmaster.Active=1")
										  ->where("tbl_program.Active =1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetStudentpaymentDetails(){ //Function to get payment details of students
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								  ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.*","DATE_FORMAT(tbl_studentapplication.UpdDate,'%d-%m-%Y %T') as applied"))
								  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentapplication.IDApplication=tbl_studentpaymentoption.IDApplication')	
								  ->join(array("tbl_programmaster" =>"tbl_programmaster"),'tbl_studentapplication.Program=tbl_programmaster.IdProgrammaster',array("tbl_programmaster.ProgramName"))										  
								  ->where("tbl_studentapplication.Payment =0")
								  ->where("tbl_studentpaymentoption.ModeofPayment=2")
								  ->where("tbl_studentpaymentoption.companyflag=0")
								  ->where("tbl_studentapplication.DateTime>curdate()");	
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
	}
	
    public function fnselectstudentforpayment($lintidApplication){//Function to get payment details of Selected students
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								  ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.IdBatch","tbl_studentapplication.Takafuloperator","tbl_studentapplication.FName","tbl_studentapplication.ICNO","tbl_studentapplication.Program","tbl_studentapplication.DateTime","tbl_studentapplication.Examvenue","tbl_studentapplication.Examsession"))									 											  
								  ->where("tbl_studentapplication.IDApplication = ?",$lintidApplication);
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
	}
	
public function fnUpdateAllotedseats($lDateTime,$lintExamvenue,$lintExamsession){//Function for updating the Allotedseats field of venuedateschedule table
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $larrformData1 = array("Allotedseats"=>new Zend_Db_Expr("Allotedseats+1"));		 
		 $where = 	$db->quoteInto('idvenue = ?', $lintExamvenue).
      				$db->quoteInto(' AND date = ?', $lDateTime). 	
      				$db->quoteInto(' AND idsession = ?', $lintExamsession); 
		 $db->update('tbl_venuedateschedule',$larrformData1,$where);	
	}
 
    public function fngetpaymentSearch($larrpostData){ //Function to Search the payment details of students by Search form
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$lstrSelect = 	$db->select()          
             	           ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.*","DATE_FORMAT(tbl_studentapplication.UpdDate,'%d-%m-%Y %T') as applied"))
						   ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentapplication.IDApplication=tbl_studentpaymentoption.IDApplication')	
						   ->join(array("tbl_programmaster" =>"tbl_programmaster"),'tbl_studentapplication.Program=tbl_programmaster.IdProgrammaster',array("tbl_programmaster.ProgramName"))										  
						   ->where("tbl_studentapplication.Payment =0")
						   ->where("tbl_studentpaymentoption.ModeofPayment=2")
					       ->where("tbl_studentpaymentoption.companyflag=0")
					        ->where("tbl_studentapplication.DateTime>=curdate()")
            	           -> where('tbl_studentapplication.FName like  ? "%"',$larrpostData['field3'])
            	           ->where('tbl_studentapplication.ICNO like  ? "%"',$larrpostData['field4'])
            	           ->group("tbl_studentapplication.IDApplication");
		if(isset($larrpostData['field15']) && !empty($larrpostData['field15']) ){
				$lstrSelect = $lstrSelect->where("tbl_studentapplication.Program = ?",$larrpostData['field15']);
		   }	                              
        if(isset($larrpostData['Date']) && !empty($larrpostData['Date']) && isset($larrpostData['Date2']) && !empty($larrpostData['Date2'])){
				$lstrFromDate = date("Y-m-d",strtotime($larrpostData['Date']));
				$lstrToDate = date("Y-m-d",strtotime($larrpostData['Date2']));
				$lstrSelect = $lstrSelect->where("DATE_FORMAT(tbl_studentapplication.UpdDate,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate'");
		  }	
		  if(isset($larrpostData['RegFromDate']) && !empty($larrpostData['RegFromDate']) && isset($larrpostData['RegToDate']) && !empty($larrpostData['RegToDate'])){
				$lstrExamFromDate = date("Y-m-d",strtotime($larrpostData['RegFromDate']));
				$lstrExamToDate = date("Y-m-d",strtotime($larrpostData['RegToDate']));
				$lstrSelect = $lstrSelect->where("DATE_FORMAT(tbl_studentapplication.DateTime,'%Y-%m-%d') BETWEEN '$lstrExamFromDate' and '$lstrExamToDate'");
		  }	 
		  $lstrSelect=$lstrSelect->order('tbl_studentapplication.FName');			     
        $result = $db->fetchAll($lstrSelect);	
		return $result;		
	}
	
    public function fngetpaymentidSearch($idapplication){ //Function to Search the payment details of students by Search form
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$lstrSelect = 	$db->select()          
             	           ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.*","DATE_FORMAT(tbl_studentapplication.UpdDate,'%d-%m-%Y %T') as applied"))
						   ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentapplication.IDApplication=tbl_studentpaymentoption.IDApplication')	
						   ->join(array("tbl_programmaster" =>"tbl_programmaster"),'tbl_studentapplication.Program=tbl_programmaster.IdProgrammaster',array("tbl_programmaster.ProgramName"))										  
						   ->where("tbl_studentapplication.Payment =0")
						   ->where("tbl_studentpaymentoption.ModeofPayment=2")
					       ->where("tbl_studentpaymentoption.companyflag=0")
					        ->where("tbl_studentapplication.DateTime>=curdate()")
            	           -> where('tbl_studentapplication.IDApplication = ?',$idapplication)
            	           ->group("tbl_studentapplication.IDApplication");
		
		  $lstrSelect=$lstrSelect->order('tbl_studentapplication.FName');			     
        $result = $db->fetchAll($lstrSelect);	
		return $result;		
	}

    public function fnupdatestudentapplication($lintidstudent) { //Function for updating the payment field of Studentapplication table    	
    	$db 	= 	Zend_Db_Table::getDefaultAdapter();
	    self::sendmails($lintidstudent);	//calling a function for sending a mail to candidates
	    $larrformData['Payment'] = 1;	
    	$ldata = array('Payment' =>$larrformData['Payment']);	
		$where = 'IDApplication = '.$lintidstudent;
		return $db->update('tbl_studentapplication',$ldata,$where); 
    }	
    
    public function fninsertpaypaldetails($larrformData){ //Function for adding the paypal details to the paypaldetails table
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();
	    for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){
		 $ldatess=$larrformData['payment5'][$larrformData['IDApplication'][$linti]];		
		 $ldate=date('Y-m-d H:i:s',strtotime($ldatess));
		 $table = "tbl_paypaldetails";
         $larrpostData = array( 'UpdUser' =>$larrformData['UpdUser'],
                            'UpdDate' =>$ldate,
                            'IDApplication' =>$larrformData['IDApplication'][$linti],	
           					'paymentFee' =>$larrformData['payment1'][$larrformData['IDApplication'][$linti]],	
         					'grossAmount' =>$larrformData['payment1'][$larrformData['IDApplication'][$linti]],	
         					'payerId'=>$larrformData['payment2'][$larrformData['IDApplication'][$linti]],
         					'verifySign'=>$larrformData['payment3'][$larrformData['IDApplication'][$linti]],
           					'transactionId' => $larrformData['payment4'][$larrformData['IDApplication'][$linti]],
                            'paymentStatus'=>'1');	
         			
	     $db->insert($table,$larrpostData);
		
		}
	}  
	  
	public function fninsertmanualpaypaldetails($larrformData){ //Function for adding the paypal details to the manualpaypaldetails table
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();
		for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){		
		 $ldatess=$larrformData['payment5'][$larrformData['IDApplication'][$linti]];
		 $ldate=date('Y-m-d',strtotime($ldatess));		
		 $ldate1=date('Y-m-d H:i:s',strtotime($ldatess));	
		 $table = "tbl_manualpaypaldetails";
         $larrpostData = array( 'UpdUser' =>$larrformData['UpdUser'],
                            'UpdDate' =>$larrformData['UpdDate'],
                            'FName'=>$larrformData['FName'][$linti],
                            'ICNO'=>$larrformData['ICNO'][$linti],
                            'Program'=>$larrformData['Program'][$linti],
                            'ApplicationDate'=>$ldate1,
                            'paymentDate'=>$ldate,
                            'IDApplication' =>$larrformData['IDApplication'][$linti],	
           					'paymentFee' =>$larrformData['payment1'][$larrformData['IDApplication'][$linti]],	
         					'grossAmount' =>$larrformData['payment1'][$larrformData['IDApplication'][$linti]],	
         					'payerId'=>$larrformData['payment2'][$larrformData['IDApplication'][$linti]],
         					'verifySign'=>$larrformData['payment3'][$larrformData['IDApplication'][$linti]],
           					'transactionId' => $larrformData['payment4'][$larrformData['IDApplication'][$linti]],
                            'paymentStatus'=>'1');
	     $db->insert($table,$larrpostData);
		}
	}  
	
	public function fninsertregistereddetails($larrformData) { //Function for adding the registration details to the registereddetails table
		$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		for($linti=0;$linti<count($larrformData['IDApplication']);$linti++){
		 $ModelBatchlogin = new App_Model_Batchlogin();
		 $lintRegid = $ModelBatchlogin->fnGenerateCode($larrformData['Takafuloperator'][$linti],$larrformData['IDApplication'][$linti]);	 
		 $table = "tbl_registereddetails";
         $larrpostData = array('Regid' =>   $lintRegid,	
           					'IdBatch' =>$larrformData['IdBatch'][$linti],	
         					'Approved' =>1,	
         					'RegistrationPin'=>'0000000',
         					'Cetreapproval'=>'0',
           					'IDApplication' => $larrformData['IDApplication'][$linti]);					
	     $db->insert($table,$larrpostData);
		}
	}

	
    
	public function fngeticnoofstu($studentId)
	{
		 ////////////********************/////////////////////////////////////
		 $db = Zend_Db_Table::getDefaultAdapter();
		 		$lstrSelect = $db->select()
		                         ->from(array("a" => "tbl_studentapplication"),array("a.ICNO"))
		                        
		                         ->where("a.IDApplication = ?",$studentId);
		$larrResult2 = $db->fetchRow($lstrSelect);
		 $icno=$larrResult2['ICNO'];
		 	$lstrSelect3 = $db->select()
		                         ->from(array("a" => "tbl_studentapplication"),array("max(a.PermCity) as attempts"))
		                         ->where("a.ICNO = ?",$icno);
		$larrResult3 = $db->fetchRow($lstrSelect3);
		 $attempts=$larrResult3['attempts']+1;
		  if(empty($larrResult3['attempts']))
		 {
		 	$attempts=1;
		 }
		 
		 
		 	
		 $larrformData1['PermCity'] = $attempts;	
		 $where = "IDApplication = '".$studentId."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
		 
		 
		 /////////////////////////////////////////////////////////////////////
		
	}
	
    public function sendmails($lintidstudent){//Function to send mail
		$lobjExamdetailsmodel = new App_Model_Examdetails();	
		require_once('Zend/Mail.php');
		require_once('Zend/Mail/Transport/Smtp.php');
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();
		$lstrSelect= $db->select()   
             	        ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.*"))
						->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentapplication.IDApplication=tbl_studentpaymentoption.IDApplication')	
						->join(array("tbl_programmaster" =>"tbl_programmaster"),'tbl_studentapplication.Program=tbl_programmaster.IdProgrammaster',array("tbl_programmaster.ProgramName"))
						->join(array("tbl_center" =>"tbl_center"),'tbl_studentapplication.Examvenue=tbl_center.idcenter',array("tbl_center.centername","tbl_center.addr1","tbl_center.addr2"))
						->join(array("tbl_managesession" =>"tbl_managesession"),'tbl_studentapplication.Examsession=tbl_managesession.idmangesession',array("tbl_managesession.managesessionname","tbl_managesession.ampmstart","tbl_managesession.ampmend"))
						->join(array("tbl_newscheduler" =>"tbl_newscheduler"),'tbl_newscheduler.idnewscheduler=tbl_studentapplication.Year',array("tbl_newscheduler.Year as years"))						
						->join(array("tbl_registereddetails" =>"tbl_registereddetails"),'tbl_registereddetails.IDApplication=tbl_studentapplication.IDApplication',array("tbl_registereddetails.Regid"))
						->where("tbl_studentapplication.Examvenue !=000")									  
						->where("tbl_studentapplication.Payment =0")
						->where("tbl_studentpaymentoption.ModeofPayment=2")
						->where("tbl_studentpaymentoption.companyflag=0")
            	        ->where('tbl_studentapplication.IDApplication = ?',$lintidstudent);  
	    $larrresult = $db->fetchRow($lstrSelect);  
	    $larrStudentMailingDetails = $larrresult;
        $larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Application");
		$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
		$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
		$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
		$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
		$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
		$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
		$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'].' '.$larrresult['addr2'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'].'('.$larrresult['ampmstart'].'--'.$larrresult['ampmend'].')',$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[LoginId]",$larrresult['Regid'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[username]",$larrresult["ICNO"],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['password'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
		$auth = 'ssl';
		$port = '465';
		$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
		try
		{
		  $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config); 
		}
		catch(Exception $e) 
		{
		  echo '<script language="javascript">alert("Unabsadfasfsa")</script>';
		}
		$mail = new Zend_Mail();
		try
		{
		  $mail->setBodyHtml($lstrEmailTemplateBody);
		}
		catch(Exception $e)
		{
		  echo '<script language="javascript">alert("aaaaaaaaaaaaaa")</script>';
		}
		$sender_email = 'ibfiminfo@gmail.com';
		$sender = 'ibfim';
		$receiver_email ='harsha.kodali044@gmail.com';//$larrresult["EmailAddress"]; //mail id of candidates
		$receiver = 'harsha';//$larrresult['FName'];               //candidate name
		$mail->setFrom($sender_email, $sender)
		     ->addTo($receiver_email, $receiver)
		     ->setSubject($lstrEmailTemplateSubject);
		 try 
		 {
			$result = $mail->send($transport);
		 } 
		 catch (Exception $e) 
		 {
			echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
		 }
		 $this->view->mess .= $lstrEmailTemplateBody;	
	}
}
