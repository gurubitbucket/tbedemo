<?php
class Finance_Model_DbTable_Paymentcancelmodel extends  Zend_Db_Table  {
	protected $_name = 'tbl_batchregistration';			
	
	public function fnGetpayments($companyflag){//Function to get program names 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_companies"=>"tbl_companies"),"tbl_companies.IdCompany=tbl_batchregistration.idCompany",array("tbl_companies.CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =1")
										  ->where("tbl_batchregistration.Approved=0");	
		}else{
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_takafuloperator"=>"tbl_takafuloperator"),"tbl_takafuloperator.idtakafuloperator=tbl_batchregistration.idCompany",array("tbl_takafuloperator.TakafulName AS CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =2")
										  ->where("tbl_batchregistration.Approved=0");
		}
		//if($regpin) $lstrSelect->where("tbl_batchregistration.registrationPin LIKE '".$regpin."%'");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}	
	public function fngetpaymentdetails($idBatchRegistration,$companyflag){//Function to get program names 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_companies"=>"tbl_companies"),"tbl_companies.IdCompany=tbl_batchregistration.idCompany",array("tbl_companies.CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =1")
										  ->where("tbl_batchregistration.Approved=0")
										   ->where("idBatchRegistration = ?",$idBatchRegistration);
		}else{
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_takafuloperator"=>"tbl_takafuloperator"),"tbl_takafuloperator.idtakafuloperator=tbl_batchregistration.idCompany",array("tbl_takafuloperator.TakafulName AS CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =2")
										  ->where("tbl_batchregistration.Approved=0")
										   ->where("idBatchRegistration = ?",$idBatchRegistration);
		}				
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnCancelPayment($larrformData){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$table = "tbl_paymentcancel";
		$postData = array ('idBatchRegistration' => $larrformData ['idBatchRegistration'],
						   'Remarks' => $larrformData ['remarks'],
						   'Details' => $larrformData ['chequedetails'],
		    			   'UpdDate' => $larrformData ['UpdDate'], 
		    			   'UpdUser' => $larrformData ['UpdUser'] 
		    			  );		
		$lobjDbAdpt->insert ( $table, $postData );
				
		$data = array ('paymentStatus' => 3 );
		$where ['idBatchRegistration = ?'] = $larrformData ['idBatchRegistration'];
		$lobjDbAdpt->update ( 'tbl_batchregistration', $data, $where );
	}	

	public function fnGetpaymentsIndidual(){//Function to get program names 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("tbl_studentapplication.IDApplication","tbl_studentapplication.FName AS CompanyName","tbl_studentapplication.ICNO","tbl_studentapplication.Amount AS Amount"))
										  ->join(array("tbl_registereddetails" => "tbl_registereddetails"),"tbl_studentapplication.IDApplication=tbl_registereddetails.IDApplication",array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.RegistrationPin","tbl_registereddetails.IdBatch","tbl_registereddetails.IDApplication","tbl_registereddetails.Regid"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_registereddetails.idregistereddetails",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  
										  ->where("tbl_studentpaymentoption.companyflag = 0")
										  ->where("tbl_studentapplication.Payment = 1")

										 // ->where("tbl_studentapplication.pass IN (3,4)")
										  ->where("tbl_studentapplication.batchpayment = 0")
										  ->where("tbl_studentapplication.IDApplication>1148");
										 
		/*if($studentname) $lstrSelect->where("tbl_studentapplication.FName LIKE '".$studentname."%'");
		if($icnum) $lstrSelect->where("tbl_studentapplication.ICNO LIKE '".$icnum."%'");
		if($regpin) $lstrSelect->where("tbl_registereddetails.Regid LIKE '".$regpin."%'");*/
		//echo $lstrSelect;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnCancelPaymentIndidual($larrformData){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$table = "tbl_paymentcancel";
		$postData = array ('idBatchRegistration' => $larrformData ['IDApplication'],
						   'Remarks' => $larrformData ['remarks'],
						   'Details' => $larrformData ['chequedetails'],
		    			   'UpdDate' => $larrformData ['UpdDate'], 
		    			   'UpdUser' => $larrformData ['UpdUser'] ,
						   'cancelFrom'=>1 
		    			  );		
		$lobjDbAdpt->insert ( $table, $postData );
				
		$data = array ('Payment' => 0 );
		$where ['IDApplication = ?'] = $larrformData ['IDApplication'];
		$lobjDbAdpt->update ('tbl_studentapplication', $data, $where );
	}	
	public function fnGetIndidualpayments($ICNO,$IDAppn){//Function to get program names 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("tbl_studentapplication.IDApplication","tbl_studentapplication.FName AS CompanyName","tbl_studentapplication.ICNO","tbl_studentapplication.Amount AS Amount"))
										  ->join(array("tbl_registereddetails" => "tbl_registereddetails"),"tbl_studentapplication.IDApplication=tbl_registereddetails.IDApplication",array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.RegistrationPin","tbl_registereddetails.IdBatch","tbl_registereddetails.IDApplication","tbl_registereddetails.Regid"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_registereddetails.idregistereddetails",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  
										  ->where("tbl_studentpaymentoption.companyflag = 0")
										  ->where("tbl_studentapplication.Payment = 1")
										  
										   ->where("tbl_studentapplication.IDApplication = ?",$IDAppn)
										  ->where("tbl_studentapplication.ICNO = ?",$ICNO)
										  
										  ->where("tbl_studentapplication.pass IN (3,4)")
										  ->where("tbl_studentapplication.batchpayment = 0")
										  ->where("tbl_studentapplication.IDApplication>1148");
										 
										 
		/*if($studentname) $lstrSelect->where("tbl_studentapplication.FName LIKE '".$studentname."%'");
		if($icnum) $lstrSelect->where("tbl_studentapplication.ICNO LIKE '".$icnum."%'");
		if($regpin) $lstrSelect->where("tbl_registereddetails.Regid LIKE '".$regpin."%'");*/
		//echo $lstrSelect;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	public function fnGetpaymentsforIndidual($larrformData){//Function to get program names 
	   //echo "<pre>";print_r($larrformData);die();
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		    if($larrformData['Fromdate']) $fromdate = $larrformData['Fromdate'];
			if($larrformData['Todate']) $todate = $larrformData['Todate'];
			if($larrformData['Student']) $name = $larrformData['Student'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
		
		
		
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("tbl_studentapplication.IDApplication","tbl_studentapplication.FName AS StudentName","tbl_studentapplication.ICNO","tbl_studentapplication.Amount AS Amount","DATE_FORMAT(date(tbl_studentapplication.DateTime),'%d-%m-%Y') as Examdate"))
										  ->join(array("tbl_registereddetails" => "tbl_registereddetails"),"tbl_studentapplication.IDApplication=tbl_registereddetails.IDApplication",array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.RegistrationPin","tbl_registereddetails.IdBatch","tbl_registereddetails.IDApplication","tbl_registereddetails.Regid"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_registereddetails.idregistereddetails",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  
										  ->join(array("tbl_managesession"=>"tbl_managesession"),"tbl_studentapplication.Examsession 	=tbl_managesession.idmangesession",array("tbl_managesession.managesessionname"))
										  ->join(array("tbl_center"=>"tbl_center"),"tbl_center.idcenter=tbl_studentapplication.Examvenue",array("tbl_center.centername"))
										  
										   
										  ->join(array("tbl_programmaster"=>"tbl_programmaster"),"tbl_programmaster.IdProgrammaster = tbl_studentapplication.Program",array("tbl_programmaster.ProgramName"))
                                           ->where("tbl_studentpaymentoption.companyflag = 0")
										  ->where("tbl_studentapplication.Payment = 1")
										  ->where("tbl_studentapplication.pass IN (3,4)")
										  ->where("tbl_studentapplication.batchpayment = 0")
										  ->where("tbl_studentapplication.IDApplication>1148");
                  if($larrformData['Student'])$lstrSelect .= " AND tbl_studentapplication.fname like '%$name%'";
			      if($larrformData['ICNO']) $lstrSelect .= " AND tbl_studentapplication.icno like '%$icno%'";			
		          if($larrformData['Fromdate']) $lstrSelect .= " AND tbl_studentapplication.DateTime >=  '$fromdate'";
			      if($larrformData['Todate']) $lstrSelect .= " AND tbl_studentapplication.DateTime <=  '$todate'";											  
                                           	  $lstrSelect.= "ORDER BY tbl_studentapplication.FName";
                   						//$lstrSelect->order("tbl_studentapplication.FName");
		/*if($studentname) $lstrSelect->where("tbl_studentapplication.FName LIKE '".$studentname."%'");
		if($icnum) $lstrSelect->where("tbl_studentapplication.ICNO LIKE '".$icnum."%'");
		if($regpin) $lstrSelect->where("tbl_registereddetails.Regid LIKE '".$regpin."%'");*/
		//echo $lstrSelect;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	public function CancelIndidualPayment($larrformData){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$table = "tbl_paymentcancel";
		$postData = array ('idBatchRegistration' => $larrformData ['IDApplication'],
						   'Remarks' => $larrformData ['remarks'],
						   'Details' => $larrformData ['chequedetails'],
		    			   'UpdDate' => $larrformData ['UpdDate'], 
		    			   'UpdUser' => $larrformData ['UpdUser'] ,
						   'Flag'=>5
		    			  );		
		$lobjDbAdpt->insert ( $table, $postData );
				
		$data = array ('Payment' => 3 );
		$where ['IDApplication = ?'] = $larrformData ['IDApplication'];
		$lobjDbAdpt->update ('tbl_studentapplication', $data, $where );
	}	
	public function fncheckexamdate($idapp,$examdate)
	{
	       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $currentdate = date('Y-m-d');
		    $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("tbl_studentapplication.IDApplication"))
										  ->where("tbl_studentapplication.Payment = 1")
										  ->where("tbl_studentapplication.IDApplication = ?",$idapp)
										  ->where("tbl_studentapplication.pass =3")
										   ->where("tbl_studentapplication.DateTime <= '$currentdate'")
										  ->where("tbl_studentapplication.batchpayment = 0")
										  ->where("tbl_studentapplication.IDApplication>1148");
										 
										 
	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	
	
	}
}
