<?php
class Finance_Model_DbTable_Companytypepayment extends  Zend_Db_Table  {
	protected $_name = 'tbl_companypaymenttype';			
	
	public function fnGetPaymentTerms()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										   ->where("a.idDefType  = ?","15");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}

public function fnGetcompanyNameedit()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_companies"),array("key"=>"a.IdCompany","value"=>"a.CompanyName"))
										  //->where("a.IdCompany  not in(select idcompany from tbl_companypaymenttype)") 
										   ->order("a.CompanyName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}

	
	public function fnGetcompanyName()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_companies"),array("key"=>"a.IdCompany","value"=>"a.CompanyName"))
										  ->where("a.IdCompany  not in(select idcompany from tbl_companypaymenttype)") 
										   ->order("a.CompanyName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fninsertdata($larrformdata)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_companypaymenttype";
		$postData = array(  'idcompany' =>$larrformdata['idcompany'],	
           					'paymenttype' =>$larrformdata['paymenttype'],	
           					'upduser' =>$larrformdata['upduser'], 
							'upddate' =>$larrformdata['upddate']);
		  $result = $lobjDbAdpt->insert($table,$postData);
			//$lastid  = $lobjDbAdpt->lastInsertId("tbl_studentapplication","IDApplication");	
						   return $result;
	}
	
	public function fngetCompanyDetails()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_companypaymenttype"),array("tbl_companypaymenttype.*"))
										  ->join(array("tbl_companies"),'tbl_companypaymenttype.idcompany=tbl_companies.IdCompany',array("tbl_companies.*"))
										  ->join(array("tbl_definationms"),'tbl_companypaymenttype.paymenttype=tbl_definationms.idDefinition',array("tbl_definationms.*"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnSearchCompanyTypePayment($id)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_companypaymenttype"),array("tbl_companypaymenttype.*"))
										  ->join(array("tbl_companies"),'tbl_companypaymenttype.idcompany=tbl_companies.IdCompany',array("tbl_companies.*"))
										  ->join(array("tbl_definationms"),'tbl_companypaymenttype.paymenttype=tbl_definationms.idDefinition',array("tbl_definationms.*"))
										  ->where("tbl_companies.CompanyName=?",$id);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function editcompanytypepayment($idedit)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_companypaymenttype"),array("tbl_companypaymenttype.*"))
										  ->where("tbl_companypaymenttype.idcompanypaymenttype=?",$idedit);		
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	public function fnupdatecompanytypepayment($larrformdata,$id)
	{
		 $db = Zend_Db_Table::getDefaultAdapter();
    	// $larrformdata['Examvenue'] = $idvenue;	
		 $where = "idcompanypaymenttype = '".$id."'"; 	
		 $db->update('tbl_companypaymenttype',$larrformdata,$where);
	}
	
}
