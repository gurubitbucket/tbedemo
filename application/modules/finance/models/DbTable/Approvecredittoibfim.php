<?php
class Finance_Model_DbTable_Approvecredittoibfim extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	  public function fngetquestion()
	  {
	    
	    // mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $conn);
           $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   //$lobjDbAdpt->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'"); 
             $lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_questions"),array("a.Question"));
				$lobjDbAdpt->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'"); 
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
	    return $larrResult;
	  }
	 public function fngetselectedcompany($larr) 
     {
           $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
             $lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_companies"),array("a.*"))
								  ->join(array("b" =>"tbl_batchregistration"),'a.IdCompany=b.idCompany',array("b.*"))								  
                                  ->join(array("c" =>"tbl_studentpaymentoption"),'c.IDApplication =b.idBatchRegistration',array("b.*"))
								 ->join(array("d" =>"tbl_credittobank"),'d.Idcompany =b.idCompany',array("b.*"))
								 ->where("b.Approved=0")
								  ->where("c.companyflag=1")
								  ->where("c.ModeofPayment =7")								  
								  ->where('a.CompanyName like "%" ? "%"',$larr['CompanyName'])
								   ->where('b.registrationPin like "%" ? "%"',$larr['Regpin'])
								   ->group("b.idBatchRegistration")
								  ->order('a.CompanyName');		 				
		  $larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
		  return $larrResult;
         }
	public function insertapprovaldetails($larrformdata)
	{
	       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		     
			 for($i=0;$i<count($larrformdata['idcompany']);$i++)
			 {			    
			    $companydetails = explode(',',$larrformdata['idcompany'][$i]);			
			    $idcompany = $companydetails['0'];
			    $idbatch =   $companydetails['1'];				
				$table = "tbl_creditapprovaldetails";
                $postData = array('Idcompany' => $idcompany,	
           					'Ibbatch' =>$idbatch,	
           					'Upduser' => $larrformdata['UpdUser'],
         					'Upddate'=>$larrformdata['UpdDate']
         					);					
	            $lobjDbAdpt->insert($table,$postData);
				
				 $where = 'idBatchRegistration  = '.$idbatch;
						 $postData1 = array(		
												'Approved' => 1,
                                                'paymentStatus'=>1												
											);
						 $table1 = "tbl_batchregistration";
						 
				$lobjDbAdpt->update($table1,$postData1,$where);
			}
			
    }
	
	}

