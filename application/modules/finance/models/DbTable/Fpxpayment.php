<?php
class Finance_Model_DbTable_Fpxpayment extends  Zend_Db_Table  {
	protected $_name = 'tbl_registrationfpx';			
	
	public function fnGetprogramname(){//Function to get program names
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_programmaster" => "tbl_programmaster"),array("key"=>"tbl_programmaster.IdProgrammaster","value"=>"tbl_programmaster.ProgramName"))
										  ->join(array("tbl_program" => "tbl_program"),'tbl_program.IdProgram=tbl_programmaster.idprog',array())
										  ->join(array("tbl_batchmaster"=>"tbl_batchmaster"),'tbl_batchmaster.IdProgrammaster=tbl_programmaster.IdProgrammaster',array())
										  ->join(array("tbl_tosmaster"=>"tbl_tosmaster"),'tbl_batchmaster.IdBatch=tbl_tosmaster.IdBatch',array())
  										  ->join(array("tbl_programrate"=>"tbl_programrate"),'tbl_programmaster.IdProgrammaster=tbl_programrate.idProgram',array())
										  ->where("tbl_programrate.Active=1")
										  ->where("tbl_tosmaster.Active=1")
										  ->where("tbl_program.Active =1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetStudentpaymentDetails(){ //Function to get payment details of students
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
											  ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.*","DATE_FORMAT(date(tbl_studentapplication.UpdDate),'%d-%m-%Y %T') as applied"))
											  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentapplication.IDApplication=tbl_studentpaymentoption.IDApplication')	
											  ->join(array("tbl_programmaster" =>"tbl_programmaster"),'tbl_studentapplication.Program=tbl_programmaster.IdProgrammaster',array("tbl_programmaster.ProgramName"))	
											  ->join(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'tbl_studentapplication.IdApplication=tbl_fpxpaymentlog.IdApplication',array("tbl_fpxpaymentlog.TxnOrderNo"))
											  ->where("tbl_studentapplication.Examvenue !=000")							  
											  ->where("tbl_studentapplication.Payment =0")
											   ->where("tbl_studentapplication.DateTime>=curdate()")
											  ->where("tbl_studentpaymentoption.ModeofPayment=1")
											  ->where("tbl_studentpaymentoption.companyflag=0"); 
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
	
    public function fnselectstudentforpayment($lintidApplication){ //Function to get payment details of Selected students
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								  ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.IdBatch","tbl_studentapplication.Takafuloperator","tbl_studentapplication.FName","tbl_studentapplication.ICNO","tbl_studentapplication.Program","tbl_studentapplication.DateTime","tbl_studentapplication.Examvenue","tbl_studentapplication.Examsession"))									 											  
								  ->where("tbl_studentapplication.IDApplication = ?",$lintidApplication);
		   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		   return $larrResult;
	}
	
    public function fngetpaymentSearch($postData){//Function to Search the payment details of students by Search form
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$lstrSelect = 	$db->select()          
             	           ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.*","DATE_FORMAT(date(tbl_studentapplication.UpdDate),'%d-%m-%Y %T') as applied"))
						   ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentapplication.IDApplication=tbl_studentpaymentoption.IDApplication')	
						   ->join(array("tbl_programmaster" =>"tbl_programmaster"),'tbl_studentapplication.Program=tbl_programmaster.IdProgrammaster',array("tbl_programmaster.ProgramName"))										  
						   ->join(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'tbl_studentapplication.IdApplication=tbl_fpxpaymentlog.IdApplication',array("tbl_fpxpaymentlog.TxnOrderNo"))
											  ->where("tbl_studentapplication.Examvenue !=000")							  
											  ->where("tbl_studentapplication.Payment =0")
											  ->where("tbl_studentpaymentoption.ModeofPayment=1")
											  ->where("tbl_studentpaymentoption.companyflag=0")
											     ->where("tbl_studentapplication.DateTime>=curdate()")
            	                              -> where('tbl_studentapplication.FName like  ? "%"',$postData['field3'])
            	                              ->where('tbl_studentapplication.ICNO like  ? "%"',$postData['field4']);
            	                              
		if(isset($postData['field15']) && !empty($postData['field15']) )
		   {
				$lstrSelect .= $lstrSelect->where("tbl_studentapplication.Program = ?",$postData['field15']);
		   }	                              
        if(isset($postData['Date']) && !empty($postData['Date']) && isset($postData['Date2']) && !empty($postData['Date2']))
          {
				$lstrFromDate = date("Y-m-d",strtotime($postData['Date']));
				$lstrToDate = date("Y-m-d",strtotime($postData['Date2']));
				$lstrSelect .= $lstrSelect->where("DATE_FORMAT(tbl_studentapplication.UpdDate,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate'");
		  }	 	      
		  //echo $lstrSelect;                        
        $result = $db->fetchAll($lstrSelect);	
		return $result;		
	}

	
   public function fngetpaymentidSearch($idapplication)
   {//Function to Search the payment details of students by Search form
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$lstrSelect = 	$db->select()          
             	           ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.*","DATE_FORMAT(date(tbl_studentapplication.UpdDate),'%d-%m-%Y %T') as applied"))
						   ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentapplication.IDApplication=tbl_studentpaymentoption.IDApplication')	
						   ->join(array("tbl_programmaster" =>"tbl_programmaster"),'tbl_studentapplication.Program=tbl_programmaster.IdProgrammaster',array("tbl_programmaster.ProgramName"))										  
						   ->join(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'tbl_studentapplication.IdApplication=tbl_fpxpaymentlog.IdApplication',array("tbl_fpxpaymentlog.TxnOrderNo"))
											  ->where("tbl_studentapplication.Examvenue !=000")							  
											  ->where("tbl_studentapplication.Payment =0")
											  ->where("tbl_studentpaymentoption.ModeofPayment=1")
											  ->where("tbl_studentpaymentoption.companyflag=0")
											  ->where("tbl_studentapplication.DateTime>=curdate()")
            	                              ->where('tbl_studentapplication.IDApplication =  ?',$idapplication);  
            	                             // echo    $lstrSelect;                 
        $result = $db->fetchAll($lstrSelect);	
		return $result;		
	}
	
    public function fnupdatestudentapplication($lintidstudent) { //Function for updating the payment field of Studentapplication table   	
    	 $db 	= 	Zend_Db_Table::getDefaultAdapter();
    	 self::sendmails($lintidstudent);   //calling a function for sending a mail to candidates
	    $larrformData['Payment'] = 1;	
    	$ldata = array('Payment' =>$larrformData['Payment']);
		$where = 'IDApplication = '.$lintidstudent;
		return $db->update('tbl_studentapplication',$ldata,$where);
    }	
    
    public function fninsertfpxpaymentdetails($larrformData) { //Function for adding the FpxPayment details to the FpxPayment details table
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();
	    
		for($linti=0;$linti<count($larrformData['IDApplication']);$linti++)
		{		
		 $table = "tbl_registrationfpx";
         $larrpostData = array('UpdUser' =>$larrformData['UpdUser'],
                            'UpdDate' =>$larrformData['UpdDate'],
                            'IDApplication' =>$larrformData['IDApplication'][$linti],
         					'grossAmount' =>$larrformData['payment1'][$larrformData['IDApplication'][$linti]],	
         					'payerMailId'=>$larrformData['payment2'][$larrformData['IDApplication'][$linti]],
                            'orderNumber' =>$larrformData['payment7'][$larrformData['IDApplication'][$linti]],
         					'TxnDate'=>$larrformData['payment5'][$larrformData['IDApplication'][$linti]],
           					'fpxTxnId' => $larrformData['payment4'][$larrformData['IDApplication'][$linti]],
                            'bankCode' => $larrformData['payment8'][$larrformData['IDApplication'][$linti]],
                            'bankBranch'=> $larrformData['payment9'][$larrformData['IDApplication'][$linti]],
                            'debitAuthCode'=>$larrformData['payment10'][$larrformData['IDApplication'][$linti]],
           					'debitAuthNo' => $larrformData['payment11'][$larrformData['IDApplication'][$linti]],
                            'creditAuthCode' => $larrformData['payment12'][$larrformData['IDApplication'][$linti]],
                            'creditAuthNo'=> $larrformData['payment13'][$larrformData['IDApplication'][$linti]],
                            'paymentStatus'=>'1'
                            );	
	     $db->insert($table,$larrpostData);
		
		}
	}  
	  
	public function fninsertmanualfpxpaymentdetails($larrformData) { //Function for adding the FpxPayment details to the manualFpxdetails table
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();
		for($linti=0;$linti<count($larrformData['IDApplication']);$linti++)
		{	
		 $ldatess=$larrformData['payment5'][$larrformData['IDApplication'][$linti]];		
		 $ldate=date('Y-m-d',strtotime($ldatess));
		 $table = "tbl_manualregistrationfpx";
         $larrpostData = array( 'UpdUser' =>$larrformData['UpdUser'],
                            'UpdDate' =>$larrformData['UpdDate'],
                            'IDApplication' =>$larrformData['IDApplication'][$linti],
                            'FName'=>$larrformData['FName'][$linti],
                            'ICNO'=>$larrformData['ICNO'][$linti],
                            'Program'=>$larrformData['Program'][$linti],
         					'grossAmount' =>$larrformData['payment1'][$larrformData['IDApplication'][$linti]],	
         					'payerMailId'=>$larrformData['payment2'][$larrformData['IDApplication'][$linti]],
                            'receiptid'=>$larrformData['payment3'][$larrformData['IDApplication'][$linti]],
                            'orderNumber' =>$larrformData['payment7'][$larrformData['IDApplication'][$linti]],
         					'TxnDate'=>$ldate,
           					'fpxTxnId' => $larrformData['payment4'][$larrformData['IDApplication'][$linti]],
                            'bankCode' => $larrformData['payment8'][$larrformData['IDApplication'][$linti]],
                            'bankAcc'=> $larrformData['payment9'][$larrformData['IDApplication'][$linti]],
                            'debitAuthCode'=>$larrformData['payment10'][$larrformData['IDApplication'][$linti]],
           					'debitAuthNo' => $larrformData['payment11'][$larrformData['IDApplication'][$linti]],
                            'creditAuthCode' => $larrformData['payment12'][$larrformData['IDApplication'][$linti]],
                            'creditAuthNo'=> $larrformData['payment13'][$larrformData['IDApplication'][$linti]],
                            'SettlementDate'=> $ldate,
                            'paymentStatus'=>'1'
         );	
	     $db->insert($table,$larrpostData);
		
		}
	}  
	
	public function fninsertregistereddetails($larrformData) { //Function for adding the registration details to the registereddetails table
		$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		for($linti=0;$linti<count($larrformData['IDApplication']);$linti++)
		{
		 $ModelBatchlogin = new App_Model_Batchlogin();
		 $lintRegid = $ModelBatchlogin->fnGenerateCode($larrformData['Takafuloperator'][$linti],$larrformData['IDApplication'][$linti]);	 
		 $table = "tbl_registereddetails";
         $larrpostData = array('Regid' =>   $lintRegid,	
           					'IdBatch' =>$larrformData['IdBatch'][$linti],	
         					'Approved' =>1,	
         					'RegistrationPin'=>'0000000',
         					'Cetreapproval'=>'0',
           					'IDApplication' => $larrformData['IDApplication'][$linti]);					
	     $db->insert($table,$larrpostData);
			
		}
	}
	
    public function fnUpdateAllotedseats($DateTime,$Examvenue,$Examsession) {	//Function for updating the Allotedseats field of venuedateschedule table
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $larrformData1 = array("Allotedseats"=>new Zend_Db_Expr("Allotedseats+1"));	
		 $where = 	$db->quoteInto('idvenue = ?', $Examvenue).
      				$db->quoteInto(' AND date = ?', $DateTime). 	
      				$db->quoteInto(' AND idsession = ?', $Examsession); 	 
		 $db->update('tbl_venuedateschedule',$larrformData1,$where);	
	}
	
    public function sendmails($lintidstudent){//Function to send mail
		$lobjExamdetailsmodel = new App_Model_Examdetails();	
		require_once('Zend/Mail.php');
		require_once('Zend/Mail/Transport/Smtp.php');
			
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();
		$larrresult= $db->select()          
             	        ->from(array("tbl_studentapplication" =>"tbl_studentapplication"),array("tbl_studentapplication.*"))
						->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentapplication.IDApplication=tbl_studentpaymentoption.IDApplication')	
						->join(array("tbl_programmaster" =>"tbl_programmaster"),'tbl_studentapplication.Program=tbl_programmaster.IdProgrammaster',array("tbl_programmaster.ProgramName"))
						->join(array("tbl_center" =>"tbl_center"),'tbl_studentapplication.Examvenue=tbl_center.idcenter',array("tbl_center.*"))
						->join(array("tbl_managesession" =>"tbl_managesession"),'tbl_studentapplication.Examsession=tbl_managesession.idmangesession',array("tbl_managesession.*"))
						->join(array("tbl_newscheduler" =>"tbl_newscheduler"),'tbl_newscheduler.idnewscheduler=tbl_studentapplication.Year',array("tbl_newscheduler.Year as years"))						
						->join(array("tbl_registereddetails" =>"tbl_registereddetails"),'tbl_registereddetails.IDApplication=tbl_studentapplication.IDApplication',array("tbl_registereddetails.Regid"))
						->join(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'tbl_studentapplication.IdApplication=tbl_fpxpaymentlog.IdApplication',array("tbl_fpxpaymentlog.TxnOrderNo"))
						->where("tbl_studentapplication.Examvenue !=000")									  
						->where("tbl_studentapplication.Payment =0")
						->where("tbl_studentpaymentoption.ModeofPayment=1")
						->where("tbl_studentpaymentoption.companyflag=0")
            	        ->where('tbl_studentapplication.IDApplication = ?',$lintidstudent);
            	        
	          $result = $db->fetchRow($larrresult);
	          $lastresult= $result;
     		  $larrStudentMailingDetails = $result;
              $larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Application");
			  $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
			  $lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
			  $lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
			  $lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
			  $lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
			  $lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
			  $lstrEmailTemplateBody = str_replace("[Candidate]",$result['FName'],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[ICNO]",$result['ICNO'],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[Program]",$result['ProgramName'],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[venue]",$result['centername'].' '.$result['addr1'].' '.$result['addr2'],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[Date]",$result['Examdate'].'-'.$result['Exammonth'].'-'.$result['years'],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[Address]",$result['PermAddressDetails'].'-'.$result['CorrAddress'],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[Amount]",$result['Amount'],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[Session]",$result['managesessionname'].'('.$result['ampmstart'].'--'.$result['ampmend'].')',$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[LoginId]",$result['Regid'],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[username]",$result["username"],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody = str_replace("[Password]",$result['password'],$lstrEmailTemplateBody);
			  $lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
			  $auth = 'ssl';
			  $port = '465';
			  $config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
			  try{
					$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
				 }
			  catch(Exception $e) {
									echo '<script language="javascript">alert("Unabsadfasfsa")</script>';
											
								  }
			  $mail = new Zend_Mail();
			  try{
					$mail->setBodyHtml($lstrEmailTemplateBody);
				 }
			  catch(Exception $e){
									echo '<script language="javascript">alert("aaaaaaaaaaaaaa")</script>';
			                     }
									
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email =$result["EmailAddress"];//'tony.uce@gmail.com'; //
										$receiver = harsha;//$result['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
											 try {
											 	 $mail->send($transport);
												 $db 	= 	Zend_Db_Table::getDefaultAdapter();
    											 $data1 = array('mailSent' => 1);
											  	 $where1 = 'SUBSTRING(`mesgFromFpx` FROM 228 FOR 14) = '.$lastresult['TxnOrderNo'];
												 $db->update('tbl_fpxxml',$data1,$where1);			
										
								} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
								}
								     $this->view->mess .= $lstrEmailTemplateBody;
	}
	
    public function fnGetStudentresponsepaymentDetails(){
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_studentapplication"),array("a.*","DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y %T') as applied"))
											  ->join(array("b" =>"tbl_studentpaymentoption"),'a.IDApplication=b.IDApplication')	
											  ->join(array("c" =>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.ProgramName"))	
											  ->join(array("d" =>"tbl_fpxpaymentlog"),'a.IdApplication=d.IdApplication',array("d.TxnOrderNo"))
											  ->join(array("e" =>"tbl_fpxxml"),'d.TxnOrderNo=SUBSTRING(e.mesgFromFpx FROM 228 FOR 14) AND e.mailSent=0',array("e.mesgFromFpx"))
											  ->where("a.Examvenue !=000")							  
											  ->where("a.Payment =0")
											  ->where("b.ModeofPayment=1")
											  ->where("a.DateTime>=curdate()")
											  ->where("b.companyflag=0"); 
											
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
    public function fngetautopaymentSearch($postData){
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$lstrSelect = 	$db->select()          
             	           ->from(array("a" =>"tbl_studentapplication"),array("a.*","DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y %T') as applied"))
						   ->join(array("b" =>"tbl_studentpaymentoption"),'a.IDApplication=b.IDApplication')	
						   ->join(array("c" =>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.ProgramName"))										  
						   ->join(array("d" =>"tbl_fpxpaymentlog"),'a.IdApplication=d.IdApplication',array("d.TxnOrderNo"))
							->join(array("e" =>"tbl_fpxxml"),'d.TxnOrderNo=SUBSTRING(e.mesgFromFpx FROM 228 FOR 14) AND e.mailSent=0',array("e.mesgFromFpx"))
											  ->where("a.Examvenue !=000")							  
											  ->where("a.Payment =0")
											  ->where("b.ModeofPayment=1")
											  ->where("b.companyflag=0")
											    ->where("a.DateTime>=curdate()")
            	                              -> where('a.FName like  ? "%"',$postData['field3'])
            	                              ->where('a.ICNO like  ? "%"',$postData['field4']);
            	                              
		if(isset($postData['field15']) && !empty($postData['field15']) )
		   {
				$lstrSelect = $lstrSelect->where("a.Program = ?",$postData['field15']);
		   }	                              
        if(isset($postData['Date']) && !empty($postData['Date']) && isset($postData['Date2']) && !empty($postData['Date2']))
          {
				$lstrFromDate = date("Y-m-d",strtotime($postData['Date']));
				$lstrToDate = date("Y-m-d",strtotime($postData['Date2']));
				$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate'");
		  }	 
		   if(isset($larrpostData['RegFromDate']) && !empty($larrpostData['RegFromDate']) && isset($larrpostData['RegToDate']) && !empty($larrpostData['RegToDate'])){
				$lstrExamFromDate = date("Y-m-d",strtotime($larrpostData['RegFromDate']));
				$lstrExamToDate = date("Y-m-d",strtotime($larrpostData['RegToDate']));
				$lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d') BETWEEN '$lstrExamFromDate' and '$lstrExamToDate'");
		  }	
		  $lstrSelect=$lstrSelect->order('a.FName');
		 // echo $lstrSelect;
        $result = $db->fetchAll($lstrSelect);	
		return $result;		
	}
}
