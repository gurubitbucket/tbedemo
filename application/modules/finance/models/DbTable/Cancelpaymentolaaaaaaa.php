<?php

class Finance_Form_Cancelpayment extends Zend_Dojo_Form 
{		
    public function init()
    { 		
		$ICNO = new Zend_Form_Element_Text('ICNO');
		$ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $ICNO->setAttrib('class','txt_put');  
	    $ICNO->removeDecorator("DtDdWrapper");
	    $ICNO->removeDecorator("Label");
	    $ICNO->removeDecorator('HtmlTag');
		
		$Student = new Zend_Form_Element_Text('Student');
		$Student->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Student->setAttrib('class','txt_put');  
	    $Student->removeDecorator("DtDdWrapper");
	    $Student->removeDecorator("Label");
	    $Student->removeDecorator('HtmlTag');
		
		$Fromdate = new Zend_Dojo_Form_Element_DateTextBox('Fromdate');
	    $Fromdate->setAttrib('dojoType',"dijit.form.DateTextBox")
	               ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Todate').constraints.min = arguments[0];")
                        ->setAttrib('onChange', "examtoDateSetting();")						
						->removeDecorator("Label")
						->setAttrib('required',"true") 	 
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
	    $Todate = new Zend_Dojo_Form_Element_DateTextBox('Todate');
	    $Todate->setAttrib('dojoType',"dijit.form.DateTextBox")
	                   ->setAttrib('title',"dd-mm-yyyy")
			           ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
			           ->setAttrib('onChange', "dijit.byId('Fromdate').constraints.max = arguments[0];")
			  		   ->removeDecorator("Label")
					   ->setAttrib('required',"true") 	 
			  		   ->removeDecorator("DtDdWrapper")
			  		   ->removeDecorator('HtmlTag');
					   
		$Clear = new Zend_Form_Element_Submit('Clear');
        		$Clear->dojotype="dijit.form.Button";
        		$Clear->label = "Clear";
				$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');	
         
        $Search 		= 	new Zend_Form_Element_Submit('Search');
        $Search			->	setAttrib('class','NormalBtn');
		  $Search       ->dojotype="dijit.form.Button";
		  $Search  ->label = "Search";
       // $Search			->	setAttrib('id', 'submitbutton');
        $Search			->	removeDecorator("DtDdWrapper");
        $Search			->	removeDecorator("Label");
        $Search			->	removeDecorator('HtmlTag');		 
               	    			
   		$this->addElements(array($ICNO,$Fromdate,$Todate,$Student,$Clear,$Search));
    }
}
        
        