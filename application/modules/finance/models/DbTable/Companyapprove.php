<?php
class Finance_Model_DbTable_Companyapprove extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	
	
	 public function fngetselectedcompany($larr) 
	 {
	       
           $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $select = $lobjDbAdpt->select()
                       ->from(array("a" =>"tbl_credittobank"),array("a.IdBatch"));
           $idbatcharray =$lobjDbAdpt->fetchAll($select);
		   $newarray = array();
		   for($i=0;$i<count($idbatcharray);$i++)
		   {
		     $newarray[] = $idbatcharray[$i]['IdBatch'];
		   }			
             $lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_companies"),array("a.*"))
								  ->join(array("b" =>"tbl_batchregistration"),'a.IdCompany=b.idCompany',array("b.*"))
                                  ->join(array("c" =>"tbl_studentpaymentoption"),'c.IDApplication =b.idBatchRegistration',array("c.*"))
								  ->where("b.Approved=0")
								  ->where("c.companyflag=1")
								  ->where("c.ModeofPayment =7")								  
								  ->where('a.CompanyName like "%" ? "%"',$larr['CompanyName'])
								  // ->where("b.idBatchRegistration NOT IN ?",$select)
								   //->where('b.registrationPin like "%" ? "%"',$larr['Regpin'])
								   //->group("b.idCompany")
								   //->group("b.idBatchRegistration")
								  ->order('a.CompanyName');					
		  $larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
		  return $larrResult;
         }
		 public function fngetCompanyname($lstrType)
	{	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" =>"tbl_batchregistration"))
		                         ->join(array("b" =>"tbl_companies"),'b.idCompany=a.IdCompany',array("b.*"))
								 ->join(array("c" =>"tbl_studentpaymentoption"),'c.IDApplication =a.idBatchRegistration',array("c.*"))
		                        // ->join(array("c"=>"tbl_batchregistrationdetails"),'a.idBatchRegistration=c.idBatchRegistration',array("c.*"))
		                        //->join(array("d"=>"tbl_programmaster"),'c.idProgram=d.IdProgrammaster',array("d.*"))
		                         ->where("a.idBatchRegistration  = ?",$lstrType);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;                        
	}
	public function insertapprovaldetails($larrformdata)
	{
	    
	       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		     			
				$table = "tbl_credittobank";
                $postData = array('IdBatch' => $larrformdata['Idbatch'],	
           					'Regpin' =>$larrformdata['regpin'],							
           					'ChkNumber' => $larrformdata['ChequeNo'],
         					'ChkAmount'=>$larrformdata['Amount'],
							'Chkdate' => $larrformdata['ChequeDt'],
                            'Idcompany' => $larrformdata['Idcompany'],	
           					'Upduser' =>$larrformdata['UpdUser'],
							'Upddate' =>$larrformdata['UpdDate']
         					);	
              			
	            $lobjDbAdpt->insert($table,$postData);					
				$table1 = "tbl_creditapprovaldetails";
                $postData1 = array('Idcompany' =>$larrformdata['Idcompany'],	
           					'Ibbatch' =>$larrformdata['Idbatch'],	
           					'Upduser' => $larrformdata['UpdUser'],
         					'Upddate'=>$larrformdata['UpdDate']
         					);					
	            $lobjDbAdpt->insert($table1,$postData1);				
				 $where = 'idBatchRegistration  = '.$larrformdata['Idbatch'];
						 $postData2 = array(		
												'Approved' => 1,
                                                'paymentStatus'=>1												
											);
						 $table2 = "tbl_batchregistration";
						 
				$lobjDbAdpt->update($table2,$postData2,$where);
		
    }
	
	public function fetcompnydetails($idcompany)
	{
	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
             $lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_companies"),array("a.*"))
								  ->join(array("b" =>"tbl_batchregistration"),'a.IdCompany=b.idCompany',array("b.*"))
                                  ->join(array("c" =>"tbl_studentpaymentoption"),'c.IDApplication =b.idBatchRegistration',array("c.*"))
								  ->where("b.Approved=0")
								  ->where("c.companyflag=1")
								  ->where("c.ModeofPayment =7")								  
								  ->where("a.IdCompany =$idcompany")
								   //->where('b.registrationPin like "%" ? "%"',$larr['Regpin'])
								   ->group("b.idCompany");
								  //->order('a.CompanyName');		 				
		  $larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
		  return $larrResult;
	
	
	}
}
