<?php
class Finance_Model_DbTable_Accounthead extends  Zend_Db_Table  { 
	protected $_name = 'tbl_accounthead';			
	/*
	 * Function To Get SELECT BOX OF ACCOUNT MASTER
	 */
	public function fnGetIdGroupSelect(){	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_accountmaster"),array("key"=>"a.idAccount","value"=>"a.AccountName"))
								  ->where("a.Active = ?","1");		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	/*
	 * function for inserting into the account head table
	 */
	public function fnInsert($insertData) {		
       	$db 	= Zend_Db_Table::getDefaultAdapter();
       	$table 	= "tbl_accounthead";
	   	$db->insert($table,$insertData);
	   	return $db->lastInsertId("tbl_accounthead","idAccount");	   
	}
	
	/*
	 * //getting for grid view 
	 */
	Public function fngetaccounthead() { 
	    		
		$db 	= Zend_Db_Table::getDefaultAdapter();
		$select = 	$db->select()          
             	->	from(array('a' => 'tbl_accountmaster'),
                   				 array('AccountName'))
            	->	join(array('b' => 'tbl_accounthead'),
                  			    'a.idAccount=b.idAccount');		
		$result = $db->fetchAll($select);	
		return $result;
	 }
	 /*
	  * for edit
	  */	
	 Public function fngetaccountEdit($lvaredit) {
	 		    $db 	= 	Zend_Db_Table::getDefaultAdapter();			   
	   	$select = $db->select()
				 ->from('tbl_accounthead')
				 ->where('idAccountHead = ?',$lvaredit);  				 
		$result = $db->fetchRow($select);		
		return $result;
	}	
	
	/*
	 * function for updating the acccount head
	 */
	Public function fnupdateaccounthead($lvarEdit,$larrformData) {  
	    //
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();	
	    $larrformData['effectiveDate']		= 	 date('Y-m-d',strtotime($larrformData['effectiveDate']));	
	 	$data = array('idAccount' => $larrformData['idAccount'],	 
	 	 			  'effectiveDate' => $larrformData['effectiveDate'],
					  'Amount' => $larrformData['Amount'],					 
				 	  'UpdDate' => $larrformData['UpdDate'],
				 	  'UpdUser' => $larrformData['UpdUser'],
				 	  'Active' => $larrformData['Active'],
	 	              'Discount'=>$larrformData['Discount'],
	 	              'discounttype'=>$larrformData['discounttype'],
	 	              'minnostd'=>$larrformData['minnostd'],
				 	  );
		$where['idAccountHead = ? ']= $lvarEdit;		
		return $db->update('tbl_accounthead', $data, $where);
	} 
	 
	/*
	 * search function for the account head
	 */ 
	public function fngetaccountheadsSearch($lobjgetarr){
		$db 	= Zend_Db_Table::getDefaultAdapter();
		if(!$lobjgetarr['field7']) $sqlquery ="  b.Active = 0 ";
		else $sqlquery	= " b.Active = 1 ";		
		$select = 	$db->select()          
             	->	from(array('a' => 'tbl_accountmaster'),
                   				 array('AccountName'))
            	->	join(array('b' => 'tbl_accounthead'),
                  			    'a.idAccount=b.idAccount')
            	-> where('a.AccountName like  ? "%"',$lobjgetarr['field3'])	
            	-> where($sqlquery)
            	->order('a.AccountName');       
		$result = $db->fetchAll($select);	
		return $result;		
	}	
function  fnGetAllActiveGroupNameList()
	{
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->where("a.idDefType  = 9")
								  //->where("a.idDefinition = ?",$lvardataEdit)
								  ->where("a.Status  = ?","1");	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}
function  fnGetAllActiveReligionNameList()
	{
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->where("a.idDefType  = 8")
								 // ->where("a.idDefinition = ?",$lvardataEdit)
								  ->where("a.Status  = ?","1");	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}
	
}
