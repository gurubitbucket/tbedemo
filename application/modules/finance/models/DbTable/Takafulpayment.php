<?php
class Finance_Model_DbTable_Takafulpayment extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	public function fngetTakafulDetails(){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_studentpaymentoption"))
								  ->join(array("b" => "tbl_batchregistration"),'a.IDApplication=b.idBatchRegistration')
								  ->join(array("c" =>"tbl_takafuloperator"),'b.idCompany=c.idtakafuloperator',array("c.*"))								  
								  ->join(array("d"=>"tbl_batchregistrationdetails"),'b.idBatchRegistration=d.idBatchRegistration',array("d.*"))
		                         ->join(array("e"=>"tbl_programmaster"),'d.idProgram=e.IdProgrammaster',array("e.*"))		                         
								  ->where("b.Approved=0")
								  //->where("a.IDApplication not in (select c.IDApplication from tbl_registereddetails as c)")
								  ->where("a.companyflag=2")
								   ->where("b.paymentStatus!=2")
								    ->group("b.idBatchRegistration");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fngetTakafulname($lstrType)
	{	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_batchregistration"))
		                         ->join(array("b" =>"tbl_takafuloperator"),'b.idtakafuloperator=a.IdCompany',array("b.*"))
		                           ->join(array("c"=>"tbl_batchregistrationdetails"),'a.idBatchRegistration=c.idBatchRegistration',array("c.*"))
		                         ->join(array("d"=>"tbl_programmaster"),'c.idProgram=d.IdProgrammaster',array("d.*"))
		                         ->where("a.idBatchRegistration = ?",$lstrType);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;                        
	}
	
public function fngetcoursename($lstrType)
	{	//echo $lstrType
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_batchregistrationdetails"),array("a.*"))
		                         ->join(array("c"=>"tbl_batchregistration"),'c.idBatchRegistration=a.idBatchRegistration',array("DATE_FORMAT(date(c.UpdDate),'%d-%m-%Y') as applied"))
		                        ->join(array("b" =>"tbl_takafuloperator"),'b.idtakafuloperator=c.IdCompany',array(""))
		                        ->join(array("d"=>"tbl_programmaster"),'a.idProgram=d.IdProgrammaster',array("d.ProgramName"))
		                         ->where("a.idBatchRegistration = ?",$lstrType);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;                        
	}
	
public function fngeneraterandom()
	{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $length = 10;
    $characters = '0123456789';
    $string = "";    

      for ($p = 0; $p < $length; $p++) {
        $string.= $characters[mt_rand(0, strlen($characters))];
        }

         $lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_registereddetails"),array("Regid"));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
			for($i=0;$i<count($larrResult);$i++)
				 	{
				 		
				 		if($string == $larrResult[$i]['Regid'])
				 		{
				 	           self::fngeneraterandom();
				 	    } 
				 	    
				 	
				 	}
    return $string;
    
	}
	
        public function fnSearchTakafulPayment($larr) 
         {
           $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	       $lstrSelect = $lobjDbAdpt ->select()
       								 ->from(array("a" => "tbl_batchregistration"),array("a.*","date(a.UpdDate) as applied"))
		                             ->join(array("b" =>"tbl_takafuloperator"),'b.idtakafuloperator=a.IdCompany',array("b.*"))
       								 ->join(array("c" => "tbl_studentpaymentoption"),'c.IDApplication=a.idBatchRegistration')
		                             ->where('b.TakafulName like "%" ? "%"',$larr['field3'])
       								 ->where('a.Approved=0')
       								   ->where("a.paymentStatus!=2")
       								 ->where('c.companyflag=2')
       								  ->where('c.ModeofPayment=4')
       								 ->order('b.TakafulName');

       								  if($larr['RegFromDate']) 
       {
       $lstrSelect->where("date(a.UpdDate) >= ?",$larr['RegFromDate'])
       ->order('b.TakafulName');
       }
       if($larr['RegToDate']) 
       {
     		  $lstrSelect->where("date(a.UpdDate) <= ?",$larr['RegToDate'])
      					 ->order('b.TakafulName');
       }	       			
		  $larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
		  return $larrResult;
    }
         
	public function InsertPaymentOption($larrformData,$lstrType,$regid)
	{
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $table = "tbl_studentpaymentdetails";
          $postData = array('Amount' =>$larrformData['Amount'],	
           					'ChequeNo' =>$larrformData['ChequeNo'],	
           					'IDApplication' => $larrformData['IDApplication'],
                            'ChequeDt' =>$larrformData['ChequeDt'],
                            'companyflag'=>2,
                            'BankName' =>$larrformData['BankName']);					
	     $lobjDbAdpt->insert($table,$postData);	     
	     
	     $lstrSelect = $lobjDbAdpt->select()
		                          ->from(array("tbl_batchregistration"),array("idBatchRegistration"))
		                          ->where("idBatchRegistration = ?",$lstrType)
		                          ->where("registrationPin=0");
		 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
	
		 if($larrResult){		 
		 	$post = array('Approved' => 1,
	     				  'paymentStatus'=>1,
	     				  'registrationPin'=>$regid);		 	
		 }else{
		 	$post = array('Approved' => 1,
	     				  'paymentStatus'=> 1);
		 }
		 
	      $where['idBatchRegistration = ? ']=$lstrType;
	      return $lobjDbAdpt->update('tbl_batchregistration',$post,$where);	      
	}
	
	
	public function sendmails($stname,$email,$regid)
	{
			/*echo "asfsafsad";
		die();*/
		 $this->lobjstudentmodel = new App_Model_Studentapplication();
						
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Takaful Payment");
					
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $email;
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
						
						
							$larrEmailIds[0] = $email;
							//$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							//$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							/*echo "asfsafsdfsd";
							die();*/
							
								for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
									if($larrEmailIds[$lintI] != ""){
														
										
										$serverName =	$_SERVER['SERVER_NAME'];					
										//replace tags with values
										$Link = "<a href='".$serverName.$this->baseUrl."/tbe/takaful/login'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Person]",$stname,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$address,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[RegPin]",$regid,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Username]",$username,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$password,$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Amount]",$amount,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
										
										
										/*$to 	 = $larrEmailIds[$lintI];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/

										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrEmailIds[$lintI];
										$receiver = $stname;
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										//$result = $mail->send($transport);
									
										//$messgae .= ". Login Details have been sent to user Email".$lstrEmailTemplateBody;;
										unset($larrEmailIds[$lintI]);
									}
								}									
							$messgae .= $lstrEmailTemplateBody;							
							
						}else{
							$lstrMsg = "No Template Found";
						}
						return $messgae;
	}

public function fnGetBankDetails() 
{    	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_bank"),array("key"=>"a.IdBank","value"=>"BankName"))					 				 
					 				 ->where("a.Active = 1")
					 				 ->order("a.BankName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;	         	 
    }
	
	
}
