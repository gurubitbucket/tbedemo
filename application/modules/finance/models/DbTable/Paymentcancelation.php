<?php
class Finance_Model_DbTable_Paymentcancelation extends  Zend_Db_Table  {
	protected $_name = 'tbl_batchregistration';			
	
	public function  fngetallcompanies()
	{   
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_companies"),array("key"=>"a.IdCompany","value"=>"a.CompanyName"));
	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnGetName($idcompany,$idopt)
	{
	           $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     			 if($idopt==1){     			 
				 		$lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_companies"),array("CompanyName as Name"))							 				
							 				     ->where("IdCompany =?",$idcompany)
							 				   ;
     			 }else if($idopt==2){
     			 	    $lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_takafuloperator"),array("TakafulName as Name"))							 				
							 				     ->where("idtakafuloperator =?",$idcompany);
     			 }
				 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				 return $larrResult;
	
	}
	public function  fngetalltakaful()
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"));
	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	 public function fnGetOperatorNames($opType,$namestring)
	 {
     	
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     			 if($opType==1){     			 
				 		$lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_companies"),array("CompanyName as name"))							 				
							 				     ->where('CompanyName like ? "%"',$namestring)
							 				   ;
     			 }else if($opType==2){
     			 	    $lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_takafuloperator"),array("TakafulName as name"))							 				
							 				     ->where('TakafulName like ? "%"',$namestring);
     			 }
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }
	 
	 
	  public function fnGettempregpins($larrresult,$idcomp,$idopt)
	 {
	   
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     			 if($idopt==1){     			 
				 		$lstrSelect = $lobjDbAdpt->select()
							 				      ->from(array("a"=>"tbl_companies"),array(""))
                                                  ->join(array("b" =>"tbl_batchregistration"),"a.IdCompany=b.idCompany",array("b.*","DATE_FORMAT(b.UpdDate,'%d-%m-%Y') as Date"))
									              ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                                  ->join(array("m"=>"tbl_tempexcelcandidates"),"b.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pins"))	
												  ->where("a.IdCompany=?",$idcomp)
												  ->where("d.companyflag=?",$idopt)
												  ->group("m.RegistrationPin");
												  if(!empty($larrresult['Regpin']))
												  {
												  $lstrSelect ->where('b.registrationPin  like ? "%"',$larrresult['Regpin']);
												  }
							 				   
     			 }else if($idopt==2){
     			 	     $lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("a"=>"tbl_takafuloperator"),array(""))
                                                  ->join(array("b" =>"tbl_batchregistration"),"b.idCompany=a.idtakafuloperator",array("b.*","DATE_FORMAT(b.UpdDate,'%d-%m-%Y') as Date"))
									              ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                                 ->join(array("m"=>"tbl_tempexcelcandidates"),"b.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pins"))	
												  ->where("a.idtakafuloperator=?",$idcomp)
												  ->where("d.companyflag=?",$idopt)
												  ->group("m.RegistrationPin");
												  
												    if(!empty($larrresult['Regpin']))
												  {
												  $lstrSelect ->where('b.registrationPin  like ? "%"',$larrresult['Regpin']);
												  }
							 				 
     			 }
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }
	 
	 
	 
	 
	 
	 
	 
	 
	 public function fnGetregpins($larrresult,$idcomp,$idopt)
	 {
	   
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     			 if($idopt==1){     			 
				 		$lstrSelect = $lobjDbAdpt->select()
							 				      ->from(array("a"=>"tbl_companies"),array(""))
                                                  ->join(array("b" =>"tbl_batchregistration"),"a.IdCompany=b.idCompany",array("b.*","DATE_FORMAT(b.UpdDate,'%d-%m-%Y') as Date"))
									              ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                                  ->joinLeft(array("m"=>"tbl_registereddetails"),"b.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pin"))	
												  ->where("a.IdCompany=?",$idcomp)
												  ->where("d.companyflag=?",$idopt)
												  ->group("b.idBatchRegistration");
												  if(!empty($larrresult['Regpin']))
												  {
												  $lstrSelect ->where('b.registrationPin  like ? "%"',$larrresult['Regpin']);
												  }
							 				   
     			 }else if($idopt==2){
     			 	     $lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("a"=>"tbl_takafuloperator"),array(""))
                                                  ->join(array("b" =>"tbl_batchregistration"),"b.idCompany=a.idtakafuloperator",array("b.*","DATE_FORMAT(b.UpdDate,'%d-%m-%Y') as Date"))
									              ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                                 ->joinLeft(array("m"=>"tbl_registereddetails"),"b.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pin"))												 
												  ->where("a.idtakafuloperator=?",$idcomp)
												  ->where("d.companyflag=?",$idopt)
												  ->group("b.idBatchRegistration");
												  
												    if(!empty($larrresult['Regpin']))
												  {
												  $lstrSelect ->where('b.registrationPin  like ? "%"',$larrresult['Regpin']);
												  }
							 				 
     			 }
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }
	public function fngetregistrationdetails($flag)
	{
	
	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
	    $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_batchregistration"),array("a.registrationPin"))
									 ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=a.idBatchRegistration",array(""))
                                    ->join(array("m"=>"tbl_registereddetails"),"a.registrationPin=m.RegistrationPin",array("m.IDApplication"))
                                   // ->where("a.registrationPin!=0")	
									 ->where("a.paymentStatus in(0,1)")									
									 ->where("d.companyflag =?",$flag)
									 ->group("a.registrationPin");
	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;
	
	}
	public function fnGetpaymentdetailscompany($larrformData)
	{
	   // echo "<pre>";
		//print_r($larrformData);die();
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
	
	             $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_companies"),array("a.*","a.CompanyName as Name"))
									 ->join(array("b"=>"tbl_batchregistration"),"a.IdCompany=b.idCompany",array("b.*"))
									 ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))                                    
									 ->where("d.companyflag =1")
									 ->group("a.IdCompany");
									 if(!empty($larrformData['Company']))
									 {
									  $lstrSelect->where('a.CompanyName like ? "%"',$larrformData['Company']);
									 }
									 $lstrSelect ->order("a.CompanyName");
	            $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		/*
		else
		{
		           $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_companies"),array("a.*","a.CompanyName as Name"))
									 ->join(array("b"=>"tbl_batchregistration"),"a.IdCompany=b.idCompany",array("b.*"))
									 ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))                                   						 
									 ->where("d.companyflag =1")									
									 ->where("a.IdCompany =?",$larrformData['Company']);
	              $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);		
		}*/
		return $larrResult;
	}
	public function fnGetpaymentdetailstakaful($larrformData)
	{
	    //echo "<pre>";
	    //print_r($larrformData);
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
	             $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_takafuloperator"),array("a.*","a.TakafulName as Name"))
									 ->join(array("b"=>"tbl_batchregistration"),"a.idtakafuloperator=b.idCompany",array("b.*"))
									 ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))                                    
									 ->where("d.companyflag =2")
									  ->group("a.idtakafuloperator");
				if(!empty($larrformData['Company']))
									 {
									  $lstrSelect->where('a.TakafulName like ? "%"',$larrformData['Company']);
									 }
									 $lstrSelect ->order("a.TakafulName");
	            $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		
		/*else
		{
		           $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_takafuloperator"),array("a.*","a.CompanyName as Name"))
									 ->join(array("b"=>"tbl_batchregistration"),"a.IdCompany=b.idCompany",array("b.*"))
									 ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))                                   						 
									 ->where("d.companyflag =1")									
									 ->where("a.IdCompany =?",$larrformData['Company']);
	              $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);		
		}*/
		return $larrResult;//die();
	}
	
	
	
	
	
	
	
	
	
	
	public function fnGetpaymentscompany($larrformData,$batchpin)
	{
	   // echo "<pre>";
		//print_r($larrformData);die();
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($larrformData['Company'] == 0){
	    $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_companies"),array("a.*","a.CompanyName as Name"))
									 ->join(array("b"=>"tbl_batchregistration"),"a.IdCompany=b.idCompany",array("b.*"))
									 //->join(array("c"=>"tbl_batchregistrationdetails"),"c.idBatchRegistration=b.idBatchRegistration",array("c.*"))
									 ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                     ->where("b.paymentStatus in(0,1)")		
->where("b.registrationPin not in($batchpin)")										 
									 ->where("d.companyflag =1");
									// ->where("b.totalNoofCandidates =0");
									//echo $lstrSelect;die();
	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		}else
		{
		  $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_companies"),array("a.*","a.CompanyName as Name"))
									 ->join(array("b"=>"tbl_batchregistration"),"a.IdCompany=b.idCompany",array("b.*"))
									 //->join(array("c"=>"tbl_batchregistrationdetails"),"c.idBatchRegistration=b.idBatchRegistration",array("c.*"))
									 ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                     ->where("b.paymentStatus in(0,1)")		
->where("b.registrationPin not in($batchpin)")											 
									 ->where("d.companyflag =1")
									 ->where("b.totalNoofCandidates  =0")
									 ->where("a.IdCompany =?",$larrformData['Company']);
	      $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		
		}
		return $larrResult;
	}
	public function fnGetpaymentstakaful($larrformData,$batchpin)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    if($larrformData['Takaful'] == 0){
	    $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_takafuloperator"),array("a.*","a.TakafulName as Name"))
									 ->join(array("b"=>"tbl_batchregistration"),"a.idtakafuloperator=b.idCompany",array("b.*"))
									 //->join(array("c"=>"tbl_batchregistrationdetails"),"c.idBatchRegistration=b.idBatchRegistration",array("c.*"))
									 ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
									  ->where("b.paymentStatus in(0,1)")	
									  ->where("b.registrationPin not in($batchpin)")		
									 ->where("d.companyflag =2")									
									 ->where("b.totalNoofCandidates  =0");
	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		else
		{
		   $lstrSelect = $lobjDbAdpt->select()
	                                 ->from(array("a" => "tbl_takafuloperator"),array("a.*","a.TakafulName as Name"))
									 ->join(array("b"=>"tbl_batchregistration"),"a.idtakafuloperator=b.idCompany",array("b.*"))
									 //->join(array("c"=>"tbl_batchregistrationdetails"),"c.idBatchRegistration=b.idBatchRegistration",array("c.*"))
									 ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
									  ->where("b.paymentStatus in(0,1)")	
									 ->where("d.companyflag =2")
									 ->where("b.registrationPin not in($batchpin)")		
									 ->where("b.totalNoofCandidates  =0")
									 ->where("a.idtakafuloperator  =?",$larrformData['Takaful']);
	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		return $larrResult;
	}
	public function fngetpaymentdetails($idBatchRegistration,$companyflag)//Function to get program names 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_companies"=>"tbl_companies"),"tbl_companies.IdCompany=tbl_batchregistration.idCompany",array("tbl_companies.CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										 // ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =1")
										 // ->where("tbl_batchregistration.Approved=0")
										   ->where("idBatchRegistration = ?",$idBatchRegistration);
		}else{
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_takafuloperator"=>"tbl_takafuloperator"),"tbl_takafuloperator.idtakafuloperator=tbl_batchregistration.idCompany",array("tbl_takafuloperator.TakafulName AS CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  //->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =2")
										  //->where("tbl_batchregistration.Approved=0")
										  ->where("idBatchRegistration = ?",$idBatchRegistration);
		}				
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnCancelPayment($larrformData,$idcompany,$flag)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		//echo "<pre>";
		//print_r($larrformData);die();
		$table = "tbl_cancelpayment";
		$postData = array ('idBatchRegistration' => $larrformData ['idBatchRegistration'],
		                   'idcompany'=>$idcompany,
						   'Remarks' => $larrformData ['remarks'],
						   'Details' => $larrformData ['chequedetails'],
		    			   'UpdDate' => $larrformData ['UpdDate'], 
		    			   'UpdUser' => $larrformData ['UpdUser'],
						   'Companyflag'=>$flag
		    			  );		
		$lobjDbAdpt->insert ( $table, $postData );
				
		$data = array ('paymentStatus' => 3 );
		$where ['idBatchRegistration = ?'] = $larrformData ['idBatchRegistration'];
		$lobjDbAdpt->update ( 'tbl_batchregistration', $data, $where );
	}	
	
	public function fnGetpayments($companyflag){//Function to get program names 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_companies"=>"tbl_companies"),"tbl_companies.IdCompany=tbl_batchregistration.idCompany",array("tbl_companies.CompanyName","tbl_companies.CompanyName as Name"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  //->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =1")
										  // ->where("tbl_batchregistration.totalNoofCandidates  =0")
										    ->where("tbl_batchregistration.paymentStatus in(0,1)");
										  //->where("tbl_batchregistration.Approved=0")	
		}else{
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_takafuloperator"=>"tbl_takafuloperator"),"tbl_takafuloperator.idtakafuloperator=tbl_batchregistration.idCompany",array("tbl_takafuloperator.TakafulName AS CompanyName","tbl_takafuloperator.TakafulName as Name"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										 // ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =2")
										  // ->where("tbl_batchregistration.totalNoofCandidates  =0")
										    ->where("tbl_batchregistration.paymentStatus in(0,1)");
										  //->where("tbl_batchregistration.Approved=0")
		}
		//if($regpin) $lstrSelect->where("tbl_batchregistration.registrationPin LIKE '".$regpin."%'");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
public function fnGetpaymentsIndidual(){//Function to get program names 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("tbl_studentapplication.IDApplication","tbl_studentapplication.FName AS CompanyName","tbl_studentapplication.ICNO","tbl_studentapplication.Amount AS Amount"))
										  ->join(array("tbl_registereddetails" => "tbl_registereddetails"),"tbl_studentapplication.IDApplication=tbl_registereddetails.IDApplication",array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.RegistrationPin","tbl_registereddetails.IdBatch","tbl_registereddetails.IDApplication","tbl_registereddetails.Regid"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_registereddetails.idregistereddetails",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  ->where("tbl_studentpaymentoption.companyflag = 0")
										  ->where("tbl_studentapplication.Payment = 1")
										  //->where("tbl_studentapplication.pass IN (3,4)")
										  ->where("tbl_studentapplication.batchpayment = 0")
										  ->where("tbl_studentapplication.IDApplication>1148");
										 
		/*if($studentname) $lstrSelect->where("tbl_studentapplication.FName LIKE '".$studentname."%'");
		if($icnum) $lstrSelect->where("tbl_studentapplication.ICNO LIKE '".$icnum."%'");
		if($regpin) $lstrSelect->where("tbl_registereddetails.Regid LIKE '".$regpin."%'");*/
		//echo $lstrSelect;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	public function fnCancelPaymentIndidual($larrformData){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$table = "tbl_paymentcancel";
		$postData = array ('idBatchRegistration' => $larrformData ['IDApplication'],
						   'Remarks' => $larrformData ['remarks'],
						   'Details' => $larrformData ['chequedetails'],
		    			   'UpdDate' => $larrformData ['UpdDate'], 
		    			   'UpdUser' => $larrformData ['UpdUser'] ,
						   'cancelFrom'=>1 
		    			  );		
		$lobjDbAdpt->insert ( $table, $postData );
				
		$data = array ('Payment' => 0 );
		$where ['IDApplication = ?'] = $larrformData ['IDApplication'];
		$lobjDbAdpt->update ('tbl_studentapplication', $data, $where );
	}	
}
