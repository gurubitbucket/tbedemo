<?php
class Finance_Model_DbTable_Takafultypepayment extends  Zend_Db_Table  {
	protected $_name = 'tbl_companypaymenttype';			
	
	public function fnGetPaymentTerms()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										   ->where("a.idDefType  = ?","15");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
public function fnGetcompanyNameedit()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"))
										  //->where("a.IdCompany  not in(select idcompany from tbl_companypaymenttype)") 
										   ->order("a.TakafulName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnTakafuloperatorname()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"))
										  ->where("a.idtakafuloperator  not in(select idtakafuloperator from tbl_takafulpaymenttype)") 
										   ->order("a.TakafulName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fninsertdata($larrformdata)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_takafulpaymenttype";
		$postData = array(  'idtakafuloperator' =>$larrformdata['idtakafuloperator'],	
           					'paymenttype' =>$larrformdata['paymenttype'],	
           					'upduser' =>$larrformdata['upduser'], 
							'upddate' =>$larrformdata['upddate']);
		  $result = $lobjDbAdpt->insert($table,$postData);
			//$lastid  = $lobjDbAdpt->lastInsertId("tbl_studentapplication","IDApplication");	
						   return $result;
	}
	
	public function fngetCompanyDetails()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_takafulpaymenttype"),array("tbl_takafulpaymenttype.*"))
										  ->join(array("tbl_takafuloperator"),'tbl_takafulpaymenttype.idtakafuloperator=tbl_takafuloperator.idtakafuloperator',array("tbl_takafuloperator.*"))
										  ->join(array("tbl_definationms"),'tbl_takafulpaymenttype.paymenttype=tbl_definationms.idDefinition',array("tbl_definationms.*"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnSearchCompanyTypePayment($id)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_takafulpaymenttype"),array("tbl_takafulpaymenttype.*"))
										 ->join(array("tbl_takafuloperator"),'tbl_takafulpaymenttype.idtakafuloperator=tbl_takafuloperator.idtakafuloperator',array("tbl_takafuloperator.*"))
										  ->join(array("tbl_definationms"),'tbl_takafulpaymenttype.paymenttype=tbl_definationms.idDefinition',array("tbl_definationms.*"))
										  ->where("tbl_takafuloperator.TakafulName=?",$id);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function editcompanytypepayment($idedit)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_takafulpaymenttype"),array("tbl_takafulpaymenttype.*"))
										  ->where("tbl_takafulpaymenttype.idtakafulpaymenttype=?",$idedit);		
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	public function fnupdatecompanytypepayment($larrformdata,$id)
	{
		 $db = Zend_Db_Table::getDefaultAdapter();
    	// $larrformdata['Examvenue'] = $idvenue;	
		 $where = "idtakafulpaymenttype = '".$id."'"; 	
		 $db->update('tbl_takafulpaymenttype',$larrformdata,$where);
	}
	
}
