<?php 
class Finance_Model_DbTable_Programrate extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_programrate';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetCoursemasterDetails() { //Function to get the user details
        $select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_programmaster'),array('IdProgrammaster'))
			   ->where('a.Active = 1');
		
		$result = $this->fetchAll($select);
        return $result;
     }
    function fngetProgramrateMaster($wher){
    	$select = $this->select()
			   ->setIntegrityCheck(false) 
			   ->from( array('b' => 'tbl_programmaster')) 	
			   ->joinLeft(array("a" => "tbl_programrate"),"a.idProgram = b.IdProgrammaster ")
			   ->where('b.Active = 1')
			   -> where($wher); 
			  
		$result = $this->fetchAll($select);
        return $result;
    }
 	function fngetProgramrateDetails($wher){
    	$select = $this->select()
			   ->setIntegrityCheck(false) 
			    ->from(array("a" => "tbl_programrate"),array("a.idProgramrate","a.idProgram","a.IdAccountmaster","a.Rate","a.EffectiveDate","a.Active as act","a.ServiceTax"))
			   ->joinLeft( array('b' => 'tbl_programmaster'),"a.idProgram = b.IdProgrammaster")
			   ->joinLeft( array('c' => 'tbl_accountmaster'),"c.idAccount = a.IdAccountmaster")			  
			   ->where('b.Active = 1')
			   -> where($wher)
			   ->where('c.Active = 1')
			   ->where('a.Active = 1'); 			  
		$result = $this->fetchAll($select);
        return $result->toArray(); 
    }
	function fngetProgramrateEditDetails($wher){
    	$select = $this->select()
			   ->setIntegrityCheck(false) 
			   ->from(array("a" => "tbl_programrate")) 				   
			   -> where($wher); 			  
		$result = $this->fetchRow($select);
        return $result; 
    }
	public function fnAccountArray($IdCourse){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_accountmaster"),array("key"=>"idAccount","value"=>"AccountName"))
				 				 ->where("`a`.`idAccount` NOT IN(select `tbl_programrate`.`IdAccountmaster` from `tbl_programrate` where '$IdCourse'=`tbl_programrate`.`idProgram` and tbl_programrate.Active = 1)" )
				 				 ->where("Active = 1")
				 				 ->where("a.programtype=1")
				 				 ->order("AccountName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnAccountArray1(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_accountmaster"),array("key"=>"idAccount","value"=>"AccountName"))
				 				 //->where("`a`.`idAccount` NOT IN(select `tbl_programrate`.`IdAccountmaster` from `tbl_programrate` where '$IdCourse'=`tbl_programrate`.`idProgram` and tbl_programrate.Active = 1)" )
				 				 ->where("Active = 1")
				 				 ->where("a.programtype=1")
				 				 ->order("AccountName");
				 				 //echo $lstrSelect;die();
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		//echo "<pre>";print_r($larrResult);die();
		return $larrResult;
	}
	
	
    
    public function fnaddProgramrate($formData) { //Function for adding the University details to the table
			$this->insert($formData);
	}
	
	public function fnupdateenddate($formData)
	{
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$data = array(
	 	 			  'UpdDate' => $formData['UpdDate'],
	 	 			  'UpdUser'=>$formData['UpdUser'],
	 	 			  'Active'=>$formData['Active']
	 	 			  );
			$where['idProgramrate = ? ']= $formData['idProgramrate'];	
			$db->update('tbl_programrate', $data, $where);
	}
    
    public function fnupdateenddate1($formData)
	{
		$formData['Active']=0;
		$idProgram=$formData['idProgram'];
		$IdAccountmaster=$formData['IdAccountmaster'];
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$data = array(
	 	 			  'Active'=>$formData['Active']
	 	 			  );
			$where="`tbl_programrate`.`idProgram`= ".$idProgram." AND `tbl_programrate`.`IdAccountmaster`= ".$IdAccountmaster." AND `tbl_programrate`.`Active`=1";	
			$db->update('tbl_programrate', $data, $where);
	}
    
    public function fnupdateProgramrate($formData,$idProgramrate) { //Function for updating the university
    	unset($formData ['idProgramrate']);
		$where = 'idProgramrate = '.$idProgramrate;
		$this->update($formData,$where);
    }
    
	public function fnSearchCourse($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_programmaster'),array('IdProgrammaster'))
			   ->where('a.ProgramName  like "%" ? "%"',$post['field3'])
			   ->where($field7)
			   ->order('a.ProgramName');
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fnupdatetaxedrate($formData)
{
	
	$db 	= 	Zend_Db_Table::getDefaultAdapter();
	$data = array('ServiceTax' => $formData['ServiceTax'],
	 	 			  'Totalcharges' => $formData['Totalcharges'],
	 	 			  'Active'=>$formData['Active'],
	 	 			  'UpdUser'=>$formData['UpdUser'],
	 	 			  'UpdDate'=>$formData['UpdDate'],
	 	 			  'Rate'=>$formData['Rate']
	 	 			  
					 );
			$where['idProgramrate = ? ']= $idProgramrate;	
			$db->update('tbl_programrate', $data, $where);			 
}


}
?>
