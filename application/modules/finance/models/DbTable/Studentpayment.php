<?php
class Finance_Model_DbTable_Studentpayment extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	public function fngetCompanyDetails()
	{		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_studentpaymentoption"),array("a.ModeofPayment"))
								  ->join(array("b" => "tbl_studentapplication"),'a.IDApplication=b.IDApplication',array("b.*"))
								  ->join(array("e"=>"tbl_programmaster"),'b.Program=e.IdProgrammaster',array("e.ProgramName"))
								  ->where("b.Payment=0")
								  ->where("a.companyflag=0")
								  ->where("a.ModeofPayment in(4,5,6,7)")
								  ->where("b.DateTime>=curdate()");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
    public function fnSearchCompanyPayment($larrformData) 
         {
           $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	       $lstrSelect = $lobjDbAdpt ->select()
       								->from(array("a" => "tbl_studentpaymentoption"),array("a.ModeofPayment"))
								  ->join(array("b" => "tbl_studentapplication"),'a.IDApplication=b.IDApplication',array("b.*","DATE_FORMAT(date(b.DateTime),'%d-%m-%Y') as examdate"))
								  ->join(array("e"=>"tbl_programmaster"),'b.Program=e.IdProgrammaster',array("e.ProgramName"))
								  ->where("b.Payment=0")
								  ->where("a.companyflag=0")
								  ->where("a.ModeofPayment in(4,5,6,7)")
								   ->where("b.DateTime>=curdate()");
 	if($larrformData['field3']) $lstrSelect->where("b.FName like  ? '%'",$larrformData['field3']);	
 	if($larrformData['field2']) $lstrSelect->where("b.ICNO like  ? '%'",$larrformData['field2']);
 	if($larrformData['field5']) $lstrSelect->where("b.Program = ?",$larrformData['field5']);	
  	if($larrformData['field35']) $lstrSelect->where("b.DateTime = ?",$larrformData['field35']);	
   	if($larrformData['modepayment']) $lstrSelect->where("a.ModeofPayment = ?",$larrformData['modepayment']);	
		     $lstrSelect=$lstrSelect->order('b.FName');                        
		             
		  $larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
		  return $larrResult;
         }
         
         
    public function fngetprogramnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	public function CheckPaymentStatus($lstrType)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		     $lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_registereddetails"),array("a.Regid"))
		                         ->where("a.IDApplication=?",$lstrType);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
		return $larrResult;     
	}
	
	public function fngetstudentname($lstrType)
	{	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_studentapplication"),array('a.*',"DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as Applieddate"))
		                         ->join(array("c" => "tbl_studentpaymentoption"),'c.IDApplication=a.IDApplication',array("c.ModeofPayment"))
		                         ->join(array("e"=>"tbl_programmaster"),'a.Program=e.IdProgrammaster',array("e.ProgramName"))
		                         ->joinLeft(array("f"=>"tbl_takafuloperator"),'a.Takafuloperator=f.idtakafuloperator',array("f.TakafulName"))
		                         ->join(array("d" =>"tbl_newscheduler"),'a.Year=d.idnewscheduler',array('d.Year as years'))
								 ->join(array("g"=>"tbl_center"),'a.Examvenue=g.idcenter',array('g.*'))
								 ->join(array("h"=>"tbl_managesession"),'a.Examsession=h.idmangesession',array('h.*'))
		                         ->where("c.companyflag=0")
		                         ->where("c.ModeofPayment in (4,5,6,7)")
		                         ->where("a.IDApplication= ?",$lstrType);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;                       
	}
	
public function fngetcoursename($lstrType)
	{	//echo $lstrType
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_batchregistrationdetails"),array("a.*"))
		                         ->join(array("c"=>"tbl_batchregistration"),'c.idBatchRegistration=a.idBatchRegistration',array(""))
		                        ->join(array("b" =>"tbl_companies"),'b.idCompany=c.IdCompany',array(""))
		                        ->join(array("d"=>"tbl_programmaster"),'a.idProgram=d.IdProgrammaster',array("d.ProgramName"))
		                         ->where("a.idBatchRegistration = ?",$lstrType);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;                        
	}
	public function fngetProgname()
	{
		
	}
	public function insertoption($regid,$idbatch,$studentId)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $table = "tbl_registereddetails";
         $postData = array('Regid' =>   $regid,	
           					'IdBatch' =>$idbatch,	
           					'IDApplication' => $studentId,
         					'RegistrationPin'=>'0000',
         					'Approved' => 1);					
	     $lobjDbAdpt->insert($table,$postData);
	     
	     
		// $lastid  = $db->lastInsertId("tbl_registereddetails","idregistereddetails");
	}
	
	public function fngeneraterandom()
	{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $length = 10;
    $characters = '0123456789';
    $string = "";    

      for ($p = 0; $p < $length; $p++) {
        $string.= $characters[mt_rand(0, strlen($characters))];
        }

         $lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_registereddetails"),array("Regid"));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
			for($i=0;$i<count($larrResult);$i++)
				 	{
				 		
				 		if($string == $larrResult[$i]['Regid'])
				 		{
				 	           self::fngeneraterandom();
				 	    } 
				 	    
				 	
				 	}
    return $string;
    
	}
	
	public function InsertPaymentOption($larrformData)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $table = "tbl_studentpaymentdetails";
          $postData = array('Amount' =>$larrformData['Amount'],	
           					'ChequeNo' =>$larrformData['ChequeNo'],	
           					'IDApplication' => $larrformData['IDApplication'],
                            'ChequeDt' =>$larrformData['ChequeDt'],
          					'companyflag'=>0,
                             'BankName' =>$larrformData['BankName'],
                             'UpdUser'=>$larrformData['UpdUser'],
                             'UpdDate'=>$larrformData['UpdDate'],
          );					
	     $lobjDbAdpt->insert($table,$postData);	     
	  	     
	}
	
public function InsertRegisterdetails($studentId,$Takafuloperator,$idbatch)
	{
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $larrformData1['Payment'] = 1;	
		 $where = "IDApplication = '".$studentId."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);

		 
		 $ModelBatchlogin = new App_Model_Batchlogin();
		 $Regid = $ModelBatchlogin->fnGenerateCode($Takafuloperator,$studentId);	
		 	 
		 $table = "tbl_registereddetails";
         $postData = array('Regid' =>   $Regid,	
           					'IdBatch' =>$idbatch,	
         					'Approved' =>1,	
         					'RegistrationPin'=>'0000000',
         					'Cetreapproval'=>'0',
           					'IDApplication' => $studentId);					
	     $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_registereddetails","idregistereddetails");
		 return $Regid;   
	  	     
	}


 
	
public function sendmails($stname,$email,$address,$amount,$username,$password,$regid)
	{
			/*echo "asfsafsad";
		die();*/
		 $this->lobjstudentmodel = new App_Model_Studentapplication();
											
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Company Payment");
					
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $email;
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
						
						
							$larrEmailIds[0] = $email;
							//$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							//$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							/*echo "asfsafsdfsd";
							die();*/
							
						
								for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
									if($larrEmailIds[$lintI] != ""){						
										$serverName =	$_SERVER['SERVER_NAME'];					
										//replace tags with values
										$Link = "<a href='".$serverName.$this->baseUrl."/tbe/batchlogin/login'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Person]",$stname,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$address,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[RegPin]",$regid,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Username]",$username,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$password,$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Amount]",$amount,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										

										/*$to 	 = $larrEmailIds[$lintI];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
									$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $email;
										$receiver = $stname;
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									        		 ->setSubject($lstrEmailTemplateSubject);
									 try {
									//$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}

										//$messgae .= ". Login Details have been sent to user Email".$lstrEmailTemplateBody;
										unset($larrEmailIds[$lintI]);
									}
								}
							
									//$Link = "<a href='".$this->Url."/registration'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Person]",$stname,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$address,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[RegPin]",$regid,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Username]",$username,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$password,$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Amount]",$amount,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);$address
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										$messgae .= ". Login Details are as follows".$lstrEmailTemplateBody;
								
								
							
						}else{
							$lstrMsg = "No Template Found";
						}
						
						return $messgae;
	}


public function fnGetBankDetails() 
{    	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_bank"),array("key"=>"a.IdBank","value"=>"BankName"))					 				 
					 				 ->where("a.Active = 1")
					 				 ->order("a.BankName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;	         	 
    }

////////////print	
public function fngetCommpanyterms(){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_termsandcondition"),array("a.*"));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
public function fnGetAmountInWords($Amount)
     {   echo $Amount;
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				echo  $lstrSelect = "SELECT CONCAT(ucwords(str_numtowords($Amount)),' ','Only') as Amount";die();	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
				return $larrResult;
     }

	
public function fngetCommpanyreportDetails($IDAPPLICATION){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_batchregistrationdetails"),array("a.*,DATE_FORMAT(CURDATE(), '%d-%m-%Y') as Todaydate"))
								  ->join(array("b" => "tbl_batchregistration"),'a.idBatchRegistration=b.idBatchRegistration',array("b.*"))
								  ->join(array("c" =>"tbl_companies"),'b.idCompany=c.IdCompany',array("c.*"))								  
								  ->join(array("d"=>"tbl_programmaster"),'a.idProgram=d.IdProgrammaster',array("d.*"))
		                          ->join(array("e"=>"tbl_studentpaymentoption"),'b.idBatchRegistration=e.IDApplication')
		                          ->join(array("f"=>"tbl_state"),'c.IdState=f.idState',array("f.*"))
		                          ->join(array("g"=>"tbl_countries"),'c.IdCountry=g.idCountry',array("g.*"))		                          		                         
								  ->where("a.idBatchRegistration = ?",$IDAPPLICATION)
								  //->where("a.IDApplication not in (select c.IDApplication from tbl_registereddetails as c)")
								  ->where("e.companyflag=1")
								   ->group ("d.ProgramName")
                                   ->order  ("d.ProgramName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function Chechforchronepushtolocal($examdate,$venue,$session)
	{
	
	             $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											 ->from(array("a"=>"tbl_centerstartexam"),array("a.*"))					 				 
											 ->where("a.idcenter = ?",$venue)
											  ->where("a.idSession = ?",$session)
											   ->where("a.ExamDate = ?",$examdate);					 				 
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					//echo "<pre>";
					//print_r($larrResult); 
					//echo "<pre>";
					if(empty($larrResult))
					{
					//echo "fine";die();
						$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a"=>"tbl_studentapplication"),array("a.*"))	
											  ->where("a.Examvenue = ?",$venue)
											  ->where("a.Examsession = ?",$session)
											   ->where("a.DateTime = ?",$examdate)
											   ->where("a.VenueTime = ?",1);					 				 
						$larrRes = $lobjDbAdpt->fetchAll($lstrSelect);
					}
	        return $larrRes;
	  
	}
	public function	updatechronepushtolocal($examdate,$venue,$session,$idappln)
{

     $lobjautovenuemodel = new Examination_Model_Autovenue();
	 $lobjchroneclientmodel = new Examination_Model_Chroneclient();
     $connected=1;
            try 
			{
			  $db = $lobjchroneclientmodel->getAdaptor($venue);
			 
			}
			catch (Exception $e)
			{
				 $connected=0;	
                 echo '<script language="javascript">alert("Unable to push candidate for local server because of some technical issues")</script>';				 
				
			}
			if($connected==1)
			{
			       // echo $connected;die();
					//echo "<pre>";
					//print_r($larrResult); 
					//echo "<pre>";
							$larresult = self::fngetcandidates($examdate,$venue,$session,$idappln);
							if($larresult)
							{
							       // echo "<pre>";
							       // print_r($larresult); 
									    
											 $idapplication = $larresult['IDApplication'];
											 $select1 = "Delete from tbl_studentapplication where IDApplication =$idapplication";
											 $db->query($select1);
											 
											  $select2 = "Delete from tbl_registereddetails  where IDApplication =$idapplication";
											 $db->query($select2);
											 
										   
											$table = "tbl_studentapplication";
											$postData = array(	
																'IDApplication' =>$larresult['IDApplication'],	
																'StudentId' =>$larresult['StudentId'],	
																'FName' =>$larresult['FName'],	
																'MName' =>$larresult['MName'], 
																'LName' =>$larresult['LName'],	
																'DateOfBirth' =>$larresult['DateOfBirth'],	
																'PermCity' =>$larresult['PermCity'], 
																'EmailAddress' =>$larresult['EmailAddress'],
																'username' =>$larresult['username'],	
																'UpdDate' => $larresult['UpdDate'], 
																'UpdUser' =>$larresult['UpdUser'],	
																'IdBatch' =>$larresult['IdBatch'],	
																'Venue' => $larresult['Venue'], 
																'VenueTime' =>$larresult['VenueTime'],	
																'Program' =>$larresult['Program'],	
																'idschedulermaster' =>$larresult['idschedulermaster'], 
																'Amount' =>$larresult['Amount'],	
																'ICNO' =>$larresult['ICNO'],	
																'password' =>$larresult['password'],	
																'Payment' =>$larresult['Payment'], 
																'DateTime'=>$larresult['DateTime'],	
																'PermAddressDetails' =>$larresult['PermAddressDetails'],	
																'Takafuloperator' =>$larresult['Takafuloperator'], 
																'VenueChange' =>$larresult['VenueChange'],	
																'ArmyNo' =>$larresult['ArmyNo'],	
																'batchpayment' =>$larresult['batchpayment'], 
																'Gender' =>$larresult['Gender'],	
																'Race' =>$larresult['Race'],	
																'Qualification' =>$larresult['Qualification'], 
																'State' =>$larresult['State'],	
																'CorrAddress' =>$larresult['CorrAddress'],	
																'PostalCode' =>$larresult['PostalCode'], 
																'ContactNo' =>$larresult['ContactNo'],	
																'MobileNo' =>$larresult['MobileNo'], 
																'ExamState'=>$larresult['ExamState'],
																'ExamCity'=>$larresult['ExamCity'],
																'Year'=>$larresult['Year'],
																'Examdate'=>$larresult['Examdate'],
																'Exammonth'=>$larresult['Exammonth'],
																'Examvenue'=>$larresult['Examvenue'],	
																'Examsession'=>$larresult['Examsession'],
																'Pass'=>$larresult['pass'],
																 'Religion'=>$larresult['Religion'],	  
															);
															// echo "<pre>";
							       // print_r($postData); 
															
										 $insertintostuapp =  $db->insert($table,$postData);	
											
											   $regtable = "tbl_registereddetails";
										   $registereddetailsarray = array(	
												 'idregistereddetails' =>$larresult['idregistereddetails'],	
												'Regid' =>$larresult['Regid'],	
												'IdBatch' =>$larresult['IdBatch'],	
												'IDApplication' =>$larresult['IDApplication'], 
												'Approved' =>$larresult['Approved'],	
												'RegistrationPin' =>$larresult['RegistrationPin'],	
												'Cetreapproval' =>$larresult['Cetreapproval']	  
											);
											// echo "<pre>";
							     
									  $insertintoregdetails =  $db->insert($regtable,$registereddetailsarray);
									  
									     // print_r($registereddetailsarray); 
									   if($insertintostuapp ==1 && $insertintoregdetails==1)
						                {  
                                                  echo '<script language="javascript">alert("Candidate Pushed To Local Server Successfully")</script>';	
						                           $larresultupdatedpullstatus = $lobjautovenuemodel->fnupdatedstudentapplication($larresult['IDApplication']);
				                                 //  $larresult = $lobjautovenuemodel->fnupdatedchronedstatus($idcenter,$iddate);
						                }
										}						
			     
            }

}

public function fngeticnoofstu($studentId)
	{
		//echo "abc";die();
		 ////////////********************/////////////////////////////////////
		 $db = Zend_Db_Table::getDefaultAdapter();
		 		$lstrSelect = $db->select()
		                         ->from(array("a" => "tbl_studentapplication"),array("a.ICNO"))
		                        
		                         ->where("a.IDApplication = ?",$studentId);
		$larrResult2 = $db->fetchRow($lstrSelect);
		 $icno=$larrResult2['ICNO'];
		 	$lstrSelect3 = $db->select()
		                         ->from(array("a" => "tbl_studentapplication"),array("max(a.PermCity) as attempts"))
		                         ->where("a.ICNO = ?",$icno);
		$larrResult3 = $db->fetchRow($lstrSelect3);
		 $attempts=$larrResult3['attempts']+1;
		  if(empty($larrResult3['attempts']))
		 {
		 	$attempts=1;
		 }
		 
		 
		 	
		 $larrformData1['PermCity'] = $attempts;	
		 $where = "IDApplication = '".$studentId."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
		 
		 
		 /////////////////////////////////////////////////////////////////////
		
	}
	
public function fngetcandidates($examdate,$venue,$session,$idappln)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_studentapplication"),array("a.*"))
							 			   ->join(array("b" => "tbl_registereddetails"),'a.IDApplication=b.IDApplication',array("b.*"))
							 			  ->where("a.Examvenue='$venue'")
							 			   ->where("a.VenueTime=0")
							 			   ->where("a.Payment=1")
							 			   ->where("a.pass=3")
							 			   ->where("a.DateTime='$examdate'")
										   ->where("a.Examsession=?",$session)
										   ->where("a.IDApplication=?",$idappln);
										   //->where("a.VenueTime=?",0);
							 			   //->group("a.IDApplication");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
    }
	


}
