<?php
class Finance_Model_DbTable_Paymentcompanymodel extends  Zend_Db_Table  {
	protected $_name = 'tbl_batchregistration';		
	
public function fnGetCompanypaymentDetails(){ //Function to get payment details of company
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								  ->from(array("tbl_companies" =>"tbl_companies"),array("tbl_companies.CompanyName","tbl_companies.idCompany","tbl_companies.Email"))
								  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_companies.idCompany AND tbl_batchregistration.paymentStatus =0',array("tbl_batchregistration.totalAmount","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
								  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=1 AND tbl_studentpaymentoption.ModeofPayment=2')										  
								  ;									
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
	}
	
	public function fngetpaymentSearch($larrpostData){ //Function to Search the payment details of company by Search form
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		 $lstrSelect = $db->select()
								  ->from(array("tbl_companies" =>"tbl_companies"),array("tbl_companies.CompanyName","tbl_companies.idCompany","tbl_companies.Email"))
								  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_companies.idCompany AND tbl_batchregistration.paymentStatus =0',array("tbl_batchregistration.totalAmount","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
								  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=1 AND tbl_studentpaymentoption.ModeofPayment=2')										  
								  ;	
		if($larrpostData['field3'] != "" ){
				$lstrSelect = $lstrSelect->where("tbl_companies.CompanyName LIKE '%".$larrpostData['field3']."%'");
		   }	
		   $lstrSelect=$lstrSelect->order('tbl_companies.CompanyName');	             	                              
        $result = $db->fetchAll($lstrSelect);	
		return $result;		
	}	
	public function fngetpaymentcompanyidSearch($companyid){ //Function to Search the payment details of company by Search form
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		 $lstrSelect = $db->select()
								  ->from(array("tbl_companies" =>"tbl_companies"),array("tbl_companies.CompanyName","tbl_companies.idCompany","tbl_companies.Email"))
								  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_companies.idCompany AND tbl_batchregistration.paymentStatus =0',array("tbl_batchregistration.totalAmount","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
								  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=1 AND tbl_studentpaymentoption.ModeofPayment=2')										  
								  ->where("tbl_companies.idCompany = ".$companyid);
		  	
		   $lstrSelect=$lstrSelect->order('tbl_companies.CompanyName');	             	                              
        $result = $db->fetchAll($lstrSelect);	
		return $result;		
	}	
	public function fnGettakafulpaymentDetails(){ //Function to get payment details of takaful
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								  ->from(array("tbl_takafuloperator" =>"tbl_takafuloperator"),array("tbl_takafuloperator.TakafulName","tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.email"))
								  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_takafuloperator.idtakafuloperator AND tbl_batchregistration.paymentStatus =0',array("tbl_batchregistration.totalAmount","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
								  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=2 AND tbl_studentpaymentoption.ModeofPayment=2')										  
								  ;								
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
	}
public function fngettakafulpaymentSearch($larrpostData){ //Function to Search the payment details of company by Search form
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		 $lstrSelect = $db->select()
								  ->from(array("tbl_takafuloperator" =>"tbl_takafuloperator"),array("tbl_takafuloperator.TakafulName","tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.email"))
								  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_takafuloperator.idtakafuloperator AND tbl_batchregistration.paymentStatus =0',array("tbl_batchregistration.totalAmount","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
								  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=2 AND tbl_studentpaymentoption.ModeofPayment=2')										  
								  ;	
		if($larrpostData['field3'] != "" ){
				$lstrSelect = $lstrSelect->where("tbl_takafuloperator.TakafulName LIKE '%".$larrpostData['field3']."%'");
		   }		          
		   $lstrSelect=$lstrSelect->order('tbl_takafuloperator.TakafulName');   	                              
        $result = $db->fetchAll($lstrSelect);	
		return $result;		
	}
public function fngettakafulidtakafulpaymentSearch($idtakaful){ //Function to Search the payment details of company by Search form
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		 $lstrSelect = $db->select()
								  ->from(array("tbl_takafuloperator" =>"tbl_takafuloperator"),array("tbl_takafuloperator.TakafulName","tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.email"))
								  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_takafuloperator.idtakafuloperator AND tbl_batchregistration.paymentStatus =0',array("tbl_batchregistration.totalAmount","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
								  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=2 AND tbl_studentpaymentoption.ModeofPayment=2')										  
								  ->where("tbl_takafuloperator.idtakafuloperator = ".$idtakaful);		 	          
		   $lstrSelect=$lstrSelect->order('tbl_takafuloperator.TakafulName');   	                              
        $result = $db->fetchAll($lstrSelect);	
		return $result;		
	}	
public function fnGetFPXCompanypaymentDetails(){
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
											  ->from(array("tbl_companies" =>"tbl_companies"),array("tbl_companies.CompanyName","tbl_companies.IdCompany","tbl_companies.Email"))
											  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_companies.IdCompany AND 	tbl_batchregistration.paymentStatus = 0',array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
											  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=1 AND tbl_studentpaymentoption.ModeofPayment=1')	
											  ->join(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'tbl_batchregistration.idBatchRegistration=tbl_fpxpaymentlog.IdApplication AND  tbl_fpxpaymentlog.entryFrom = 2',array("tbl_fpxpaymentlog.TxnOrderNo"))
											  ->join(array("tbl_fpxxml" =>"tbl_fpxxml"),'tbl_fpxpaymentlog.TxnOrderNo=SUBSTRING(tbl_fpxxml.mesgFromFpx FROM 228 FOR 14) AND tbl_fpxxml.mailSent=0',array("tbl_fpxxml.mesgFromFpx"))
											 ; 
											  
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
public function fnGetFPXCompanypaymentSearchDetails($larrpostData){
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
											  ->from(array("tbl_companies" =>"tbl_companies"),array("tbl_companies.CompanyName","tbl_companies.IdCompany","tbl_companies.Email"))
											  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_companies.IdCompany AND 	tbl_batchregistration.paymentStatus = 0',array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
											  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=1 AND tbl_studentpaymentoption.ModeofPayment=1')	
											  ->join(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'tbl_batchregistration.idBatchRegistration=tbl_fpxpaymentlog.IdApplication AND  tbl_fpxpaymentlog.entryFrom = 2',array("tbl_fpxpaymentlog.TxnOrderNo"))
											  ->join(array("tbl_fpxxml" =>"tbl_fpxxml"),'tbl_fpxpaymentlog.TxnOrderNo=SUBSTRING(tbl_fpxxml.mesgFromFpx FROM 228 FOR 14) AND tbl_fpxxml.mailSent=0',array("tbl_fpxxml.mesgFromFpx"))
											 ; 
		if($larrpostData['field3'] != "" ){
				$lstrSelect = $lstrSelect->where("tbl_companies.CompanyName LIKE '%".$larrpostData['field3']."%'");
		   }
		   $lstrSelect=$lstrSelect->order('tbl_companies.CompanyName');							  
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
public function fnGetFPXCompanypaymentgetDetails($lintidcompany,$lintlvaredit){
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
											  ->from(array("tbl_companies" =>"tbl_companies"),array("tbl_companies.CompanyName","tbl_companies.IdCompany","tbl_companies.Email"))
											  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_companies.IdCompany AND 	tbl_batchregistration.paymentStatus = 0',array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
											  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=1 AND tbl_studentpaymentoption.ModeofPayment=1')	
											  ->join(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'tbl_batchregistration.idBatchRegistration=tbl_fpxpaymentlog.IdApplication AND  tbl_fpxpaymentlog.entryFrom = 2',array("tbl_fpxpaymentlog.TxnOrderNo"))
											  ->join(array("tbl_fpxxml" =>"tbl_fpxxml"),'tbl_fpxpaymentlog.TxnOrderNo=SUBSTRING(tbl_fpxxml.mesgFromFpx FROM 228 FOR 14) AND tbl_fpxxml.mailSent=0',array("tbl_fpxxml.mesgFromFpx"))
											 ->where("tbl_companies.IdCompany = ".$lintidcompany)
											 ->where("tbl_batchregistration.idBatchRegistration=". $lintlvaredit)
											 ->order('tbl_companies.CompanyName');							  
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}	
public function fnGetFPXTakafulpaymentDetails(){
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
											  ->from(array("tbl_takafuloperator" =>"tbl_takafuloperator"),array("tbl_takafuloperator.TakafulName","tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.email"))
											  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_takafuloperator.idtakafuloperator AND 	tbl_batchregistration.paymentStatus = 0',array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
											  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=2 AND tbl_studentpaymentoption.ModeofPayment=1')	
											  ->join(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'tbl_batchregistration.idBatchRegistration=tbl_fpxpaymentlog.IdApplication AND  tbl_fpxpaymentlog.entryFrom = 3',array("tbl_fpxpaymentlog.TxnOrderNo"))
											  ->join(array("tbl_fpxxml" =>"tbl_fpxxml"),'tbl_fpxpaymentlog.TxnOrderNo=SUBSTRING(tbl_fpxxml.mesgFromFpx FROM 228 FOR 14) AND tbl_fpxxml.mailSent=0',array("tbl_fpxxml.mesgFromFpx")); 
											  
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
public function fnGetFPXTakafulpaymentSearchDetails($larrpostData){
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("tbl_takafuloperator" =>"tbl_takafuloperator"),array("tbl_takafuloperator.TakafulName","tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.email"))
											  ->join(array("tbl_batchregistration" =>"tbl_batchregistration"),'tbl_batchregistration.idCompany=tbl_takafuloperator.idtakafuloperator AND 	tbl_batchregistration.paymentStatus = 0',array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount","DATE_FORMAT(tbl_batchregistration.UpdDate,'%d-%m-%Y %T') AS UpdDate"))	
											  ->join(array("tbl_studentpaymentoption" =>"tbl_studentpaymentoption"),'tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration AND tbl_studentpaymentoption.companyflag=2 AND tbl_studentpaymentoption.ModeofPayment=1')	
											  ->join(array("tbl_fpxpaymentlog" =>"tbl_fpxpaymentlog"),'tbl_batchregistration.idBatchRegistration=tbl_fpxpaymentlog.IdApplication AND  tbl_fpxpaymentlog.entryFrom = 3',array("tbl_fpxpaymentlog.TxnOrderNo"))
											  ->join(array("tbl_fpxxml" =>"tbl_fpxxml"),'tbl_fpxpaymentlog.TxnOrderNo=SUBSTRING(tbl_fpxxml.mesgFromFpx FROM 228 FOR 14) AND tbl_fpxxml.mailSent=0',array("tbl_fpxxml.mesgFromFpx"))
											 ; 
		if($larrpostData['field3'] != "" ){
				$lstrSelect = $lstrSelect->where("tbl_takafuloperator.TakafulName LIKE '%".$larrpostData['field3']."%'");
		   }	
		   $lstrSelect=$lstrSelect->order('tbl_takafuloperator.TakafulName');						  
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}		
	public function fpxpayment($lintinsertedId,$larrformData1,$dataArray){
		$db = Zend_Db_Table::getDefaultAdapter();	
	 	$table = "tbl_registrationfpx";
		$db->insert($table,$dataArray);
		
		$where = "idBatchRegistration = '".$lintinsertedId."'"; 	
		$db->update('tbl_batchregistration',$larrformData1,$where); 
		
		$larrformDatanew2['mailSent'] = 1;	
		$where = "SUBSTRING(tbl_fpxxml.mesgFromFpx FROM 228 FOR 14) = '".$dataArray['orderNumber']."'"; 	
		$db->update('tbl_fpxxml',$larrformDatanew2,$where);
	}
	
	
	public function fnGetregpin($idPayment){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select()
		                          ->from(array("tbl_batchregistration"),array("idBatchRegistration"))
		                          ->where("idBatchRegistration = ?",$idPayment)
		                          ->where("registrationPin=0");
		 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
		 return $larrResult;
	}	
	
	public function sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$receiver_email,$receiver){
		//$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();		
		$auth = 'ssl';
		$port = '465';
		$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
		$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
		$mail = new Zend_Mail();
		$mail->setBodyHtml($lstrEmailTemplateBody);
		$sender_email = 'ibfiminfo@gmail.com';
		$sender = 'ibfim';
		$mail->setFrom($sender_email, $sender)
			 ->addTo("tony.uce@gmail.com", $receiver)
			// ->addBcc("h.rajkumarreddy@gmail.com",$receiver)
			 //->addBcc("sachingururaj@gmail.com",$receiver)
	         ->setSubject($lstrEmailTemplateSubject);
		//$mail->send($transport);			
	}		
}
