<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_QuestionsattendedController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjQuestionsattendedmodel = new Examination_Model_Questionsattended(); //intialize newscreen db object
		$this->lobjQuestionsattendedForm = new Examination_Form_Questionattended(); 
	}
	
	public function indexAction() 
	{
	
	$this->view->lobjQuestionsattendedForm = $this->lobjQuestionsattendedForm;
	$larrresultprog =$this->lobjQuestionsattendedmodel->fngetprog(); 
	$this->lobjQuestionsattendedForm->Program->addMultiOptions($larrresultprog);
	 

	if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformdata = $this->_request->getPost ();
			/*print_r($larrformdata);
			die();*/
			$this->lobjQuestionsattendedForm->FromDate->setValue($larrformdata['FromDate']);
			$this->lobjQuestionsattendedForm->Program->setValue($larrformdata['Program']);
			$larrresultquestions = $this->lobjQuestionsattendedmodel->fngetprogdetails($larrformdata);
			$larresutstudentid = $this->lobjQuestionsattendedmodel->fnmaxids($larrformdata);
			/*print_R(count($larresutstudentid));
			die();*/
			$this->view->cntofstudent = count($larresutstudentid);
			
			$values=0;
		for($idsech=0;$idsech<count($larresutstudentid);$idsech++)
		{
		
		$value=$larresutstudentid[$idsech]['Appeared'];
		$values=$values.','.$value;
		
		}	
		/*print_r($values);
		die();*/
		$this->view->maxids = $values;
			$this->view->examdate = $larrformdata['FromDate'];
			$this->view->questions = $larrresultquestions;
		}
		
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/schedulerutility/index');
			//$this->_redirect($this->view->url(array('module'=>'registrations' ,'controller'=>'batchapproval', 'action'=>'index'),'default',true));
		}
		
	 if ($this->_request->isPost() && $this->_request->getPost ( 'Approve' )) {
			$larrformdata = $this->_request->getPost ();
			$larrresult = $this->lobjSchedulerutilitymodel->fnUpdateCandidates($larrformdata); 
		/*	echo "<pre>";
			print_r($larrformdata);
			die();*/
		}
		
	}
	
	


	
}