<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');

class Examination_ExamreportController extends Base_Base 
{
	//private $lobjanswer; //db variable
	//private $lobjform;//Form variable
		private $_gobjlogger;
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
   	    $this->lobjCommon = new App_Model_Common();
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object 
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjanswer = new Examination_Model_Answer(); //intialize user db object
		$this->lobjform = new App_Form_Search(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjexamreport = new Examination_Model_Examreports(); //intialize user db object
	}
	
	//function to set and display the result
	public function indexAction() 
	{    
		
		
/*		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$todaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$examdate = "{max:'$todaydate',datePattern:'dd-MM-yyyy'}"; */
		
		$this->view->venufield=0;
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view

		$larrcenternames = $this->lobjanswer->fngetcenternames();
		$this->lobjform->field5->addMultiOptions($larrcenternames);
		$this->lobjform->field5->setRegisterInArrayValidator(false);
		$this->lobjform->field10->setAttrib('required',"true");
		$this->lobjform->field5->setAttrib('required',"false");
		//$this->lobjform->field10->setAttrib('constraints', "$examdate");
		$this->lobjform->field10->setAttrib('OnChange','fnGetVenuedetails(this)');
		
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->examreportpaginatorresult);
		
		
		$lintpagecount = $this->gintPageCount;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->examreportpaginatorresult)) {
			$this->view->venufield= $this->gobjsessionsis->examvenue;
			$this->view->datefield = $this->gobjsessionsis->examrvenuedate;
			
			$this->lobjform->field5->setValue($this->view->venufield);
			$this->lobjform->field10->setValue($this->view->datefield);
			
			
			
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->examreportpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		
		
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData)) 
			{
				    $venuefield = $this->view->venufield=$larrformData['field5'];
				    $examrvenuedate= $this->view->datefield=$larrformData['field10'];
					unset ( $larrformData ['Search'] );
					$larrresult = $this->lobjexamreport->fnGetsearchdetails($larrformData); //searching the values for the businesstype
					$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
					$this->gobjsessionsis->examreportpaginatorresult = $larrresult;
					$this->gobjsessionsis->examvenue=$venuefield;
					$this->gobjsessionsis->examrvenuedate = $examrvenuedate;
			}
		}
	}
	
	public function reportdetailsAction() 
	{    
		$this->_helper->layout->disableLayout();
		$idcenter = $this->_getParam('idcenter');
		$idprogram = $this->_getParam('idprogram');
		$idsession = $this->_getParam('idsession');
		$examdate = $this->_getParam('examdate');
		$type = $this->_getParam('type');
		
		switch($type) {
			case "registered":
				$this->view->larrresult= $larrresult = $this->lobjexamreport->fnGetRegistered($idcenter,$idprogram,$idsession,$examdate);
				break;
				
			case "approved":	
				$this->view->larrresult = $larrresult = $this->lobjexamreport->fnGetAttedndedStudentsApproved($idcenter,$idprogram,$idsession,$examdate);
				break;
				
				
			case "attended":	
				$this->view->larrresult=$larrresult = $this->lobjexamreport->fnGetAttedndedStudentsDetails($idcenter,$idprogram,$idsession,$examdate,'3');	
				break;
				
			case "absent":	
				$this->view->larrresult = $larrresult = $this->lobjexamreport->fnGetAbsent($idcenter,$idprogram,$idsession,$examdate);
				break;
				
			case "passed":	
				$this->view->larrresult =$larrresult = $this->lobjexamreport->fnGetAttedndedStudents($idcenter,$idprogram,$idsession,$examdate,'1');
				break;
				
			case "failed":	
				$this->view->larrresult = $larrresult = $this->lobjexamreport->fnGetAttedndedStudents($idcenter,$idprogram,$idsession,$examdate,'2');
				break;
				
		}
	}

	public function getvebudetailsAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$examdate = $this->_getParam('examdate');
		if($examdate =="") {
			$larrStateVenueDetails = Array();
		} else {
			$larrStateVenueDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjexamreport->fnGetVenueList($examdate));
		}
		echo Zend_Json_Encoder::encode($larrStateVenueDetails);
	}

	public function pdfexportAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$venu = $this->_getParam('venu');
		$takenexamdate = $this->_getParam('examdate');
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		//$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		  //$stylesheet = file_get_contents('../public/css/default.css');	
		  //$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Exam" ).' '.$this->view->translate( "Report" );
		$Venue = $this->view->translate( "Venue" );
		$Pogram = $this->view->translate( "Pogram" );
		$Session = $this->view->translate( "Session" );
		$Starttime = $this->view->translate( "Start Time" );
		$GraceTime = $this->view->translate( "Extra Time(Min)" );
		$closetime = $this->view->translate( "Close Time" );
		$Autoclosetime = $this->view->translate( "Auto Close Time" );
		$examdate = $this->view->translate( "Exam Date" );
		$registered = $this->view->translate( "Registered" );
		$Approved = $this->view->translate( "Approved" );
		$attended = $this->view->translate( "Attended" );
		$absent = $this->view->translate( "Absent" );
		$pass = $this->view->translate( "Pass" );
		$fail = $this->view->translate( "Fail" );
		//$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		//$mpdf->WriteHTML();
		
		$datelabel = "Date :";
		$timelabel = "Time :";
		$currentdates = date('d-m-Y');
		$currenttimes = date('H:i:s');
		
		
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		$tabledata.= "<br><table border=1 align=center width=100%>
						<tr>
							<th align=center><b> {$datelabel}</b></th>
							<th align=center><b> {$currentdates}</b></th>
							<th align=center><b> {$timelabel}</b></th>
							<th align=center><b> {$currenttimes}</b></th>
						</tr></table>";
		$tabledata.= "<br><table border=1 align=center width=100%>
							<tr>
								<th align=center><b> {$ReportName}</b></th>
							</tr></table>";
		$centerarray = array();
		$tabledata.= "<br><table border=1 width=100%>
							<tr>
								<th align=center><b> {$Venue}</b></th>
								<th align=center><b> {$Pogram}</b></th>
								<th align=center><b> {$Session}</b></th>
								<th align=center><b> {$Starttime}</b></th>
								<th align=center><b> {$GraceTime}</b></th>
								<th align=center><b> {$closetime}</b></th>
								<th align=center><b> {$Autoclosetime}</b></th>
								<th align=center><b> {$examdate}</b></th>
								<th align=center><b> {$registered}</b></th>
								<th align=center><b> {$Approved}</b></th>
								<th align=center><b> {$attended}</b></th>
								<th align=center><b> {$absent}</b></th>
								<th align=center><b> {$pass}</b></th>
								<th align=center><b> {$fail}</b></th>
							</tr>";
		
			$larrformData['field10'] = $takenexamdate;
			$larrformData['field5'] = $venu;
			$larrresult = $this->lobjexamreport->fnGetsearchdetails($larrformData); //searching the values for the businesstype
			//print_r($larrresult);die();
			foreach($larrresult as $larrresultss)	{ 
				$tabledata.="<tr>";
				
					$tabledata.="<td>";
				        if(!in_array($larrresultss['idcenter'],$centerarray)){
							$centerarray[] = $larrresultss['idcenter'];		
							$centerarrays[$larrresultss['idcenter']] = array();
				        	$tabledata.=$larrresultss['centername'];
	         			}
					$tabledata.="</td>";
				
									

					$tabledata.="<td>";
				        if(!in_array($larrresultss['ProgramName'],$centerarrays[$larrresultss['idcenter']])){
							$centerarrays[$larrresultss['idcenter']][] = $larrresultss['ProgramName'];
							$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']] = array();;	
				        	$tabledata.=$larrresultss['ProgramName'];
	         			}
					$tabledata.="</td>";
					
					

					$tabledata.="<td>";
				        if(!in_array($larrresultss['managesessionname'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
							$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['managesessionname'];	
				        	$tabledata.=$larrresultss['managesessionname'];
	         			}
					$tabledata.="</td>";
								
					$emptytime= "00:00:00";
					if($larrresultss['ExamStartTime'] == "" || $larrresultss['ExamStartTime']==NULL) {
						$tabledata.="<td>{$emptytime}</td>";
					} else {
						$tabledata.="<td>";
					        if(!in_array($larrresultss['ExamStartTime'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
								$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['ExamStartTime'];	
					        	$tabledata.=$larrresultss['ExamStartTime'];
		         			}
						$tabledata.="</td>";
					}
					
					$emptymin = "0";
        			$larrresultextratime = $this->lobjexamreport->fnGetExtratime($larrresultss['idcenter'],$larrresultss['idmangesession'],$larrresultss['exdate']);
					if(!empty($larrresultextratime)) {
						$tabledata.="<td>{$larrresultextratime['Gracetime']}</td>";
					} else {
						$tabledata.="<td>{$emptymin}</td>";
					}	
					
					
					
					
					if($larrresultss['CloseTime'] == "" || $larrresultss['CloseTime']==NULL) {
						$tabledata.="<td>{$emptytime}</td>";
					} else {
						$tabledata.="<td>";
					        if(!in_array($larrresultss['CloseTime'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
								$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['CloseTime'];	
					        	$tabledata.=$larrresultss['CloseTime'];
		         			}
						$tabledata.="</td>";	
					}
					
					
					if($larrresultss['AutoSubmitCloseTime'] == "" || $larrresultss['AutoSubmitCloseTime']==NULL) {
						$tabledata.="<td>{$emptytime}</td>";
					} else {
						$tabledata.="<td>";
					        if(!in_array($larrresultss['AutoSubmitCloseTime'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
								$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['AutoSubmitCloseTime'];	
					        	$tabledata.=$larrresultss['AutoSubmitCloseTime'];
		         			}
						$tabledata.="</td>";
					}	
					
			
				    $tabledata.="<td>{$larrresultss['ExamDate']}</td>
									<td>{$larrresultss['Registered']}</td>";
						$registeredsum[] = $larrresultss['Registered'];		

						
						$larrresultApprovedStudents = $this->lobjexamreport->fnGetCountOfApprovedStudents($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],$larrresultss['exdate']);	
						$Approved = COUNT($larrresultApprovedStudents);			
						$tabledata.="<td>{$Approved}</td>";
						$Approvedsum[] = count($larrresultApprovedStudents);
						
			
						$larrresultattended = $this->lobjexamreport->fnGetCountOfStudentsAttended($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],$larrresultss['exdate'],'3');	
						$attended = COUNT($larrresultattended);			
						$tabledata.="<td>{$attended}</td>";
						$attendedsum[] = count($larrresultattended);
						
						
						$absent = $larrresultss['Registered'] - $attended;
						$tabledata.="<td>{$absent}</td>";
						$absentsum[] = $larrresultss['Registered'] - $attended;
						
						$larrresultpass = $this->lobjexamreport->fnGetCountOfStudents($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],$larrresultss['exdate'],'1');
						$passed = COUNT($larrresultpass);		
						$tabledata.="<td>{$passed}</td>";
						$passedsum[] = count($larrresultpass);
						
						$larrresultfail = $this->lobjexamreport->fnGetCountOfStudents($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],$larrresultss['exdate'],'2');
						$failed = COUNT($larrresultfail);	
						$tabledata.="<td>{$failed}</td>";
						$failedsum[] = count($larrresultfail);
				$tabledata.="</tr>";	
			}
			
		  $regsum= array_sum($registeredsum);
		  $Apprsum= array_sum($Approvedsum);
		  $attsum=  array_sum($attendedsum);
		  $absum= array_sum($absentsum);
		  $passsum =  array_sum($passedsum);
		  $failsum =  array_sum($failedsum);
		  $total = "Total";
		  
		  $tabledata.="<tr>
		  				<td colspan='8' align='center'><b>{$total}</b></td>
		  				<td><b>{$regsum}</b></td>
		  				<td><b>{$Apprsum}</b></td>
		  				<td><b>{$attsum}</b></td>
		  				<td><b>{$absum}</b></td>
		  				<td><b>{$passsum}</b></td>
		  				<td><b>{$failsum}</b></td>
		  			</tr>";	
				
		
			$tabledata.="</table><br>";
		

		$examdateorig =  date ( "M_d_Y", strtotime ($takenexamdate) );
		$mpdf->WriteHTML($tabledata);  
		$mpdf->Output('Exam_Report_For_Exam_Date'.'_'.$examdateorig,'D');
		
		$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Generated the exam report"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	}
 }