<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_ExamprogramchangeController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	public function fnsetObj()
	{	
		$this->lobjnewscreenmodel = new Examination_Model_Examprogramchange(); //intialize newscreen db object
		$this->lobjnewscreenForm = new Examination_Form_Examprogramchange(); 
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object

       //$this->lobjstudentForm = new App_Form_Studentapplication();
	
	
	}
	
	public function indexAction() 
	{
		
		
		$this->view->lobjform = $this->lobjnewscreenForm;
		
	$larrresultmaximum = $this->lobjnewscreenmodel->fngetstudentinformationfromconfig(); 
	//	echo "<pre>";
		//print_r($larrresultmaximum['studedit']);
	//	die();
		
				$larrresult = $this->lobjnewscreenmodel->fngetstudentinformation($larrresultmaximum['studedit']); 
				$larrresult=0;
		//print_r($larrresult);die();
		
	if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->studentchangevenuepaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		
		$larrStudentnameresult = $this->lobjnewscreenmodel->fnGetStudentNames($larrresultmaximum['studedit']);	
		$this->lobjnewscreenForm->Studentname->addMultiOptions($larrStudentnameresult);
		
		
			$larrCourseresult = $this->lobjnewscreenmodel->fnGetCourseNames();	
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrCourseresult);
		$larrVenuesresult = $this->lobjnewscreenmodel->fnGetVenueNames();	
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrVenuesresult);
		$larrTakafulresult = $this->lobjnewscreenmodel->fnGetTakafulNames();	
		$this->lobjnewscreenForm->Takafulname->addMultiOptions($larrTakafulresult);
		
		if(isset($this->gobjsessionsis->studentchangevenuepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->studentchangevenuepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$this->view->paramsearch =  $this->_getParam('search');
				//echo "<pre>";
				//print_r($larrformData);
				//die();
				
				 $larrresult = $this->lobjnewscreenmodel->fnSearchStudent($larrformData,$larrresultmaximum['studedit']); //searching the values for the user
				$this->view->larrresult =$larrresult;
				 $this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->studentchangevenuepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/examination/examprogramchange/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
		
		
		
		
		
		
	}
	
	public function editstudentinfoprogAction() 
	{	
		
		$this->view->lobjnewscreenForm = $this->lobjnewscreenForm;
		
		$lintidstudent = $this->_getParam('id');
		$this->view->id = $lintidstudent;
		//echo $lintidstudent;die();
	
		
	$larrresultstudenttype=$this->lobjnewscreenmodel->fngetstudenttype($lintidstudent);
		//print_r($larrresultstudenttype);die();
		
		
		$this->view->batch=$larrresultstudenttype['batchpayment'];
		if($larrresultstudenttype['batchpayment'] !=0)
		{
			//echo "abc";
			$larrstudentinforesult= $this->lobjnewscreenmodel->fngetstudenteachinformationforbatch($lintidstudent); 
			//print_r($larrstudentinforesult);die();
		}
		else 
		{
		$larrstudentinforesult= $this->lobjnewscreenmodel->fngetstudenteachinformation($lintidstudent); 
		}
		
		//echo "<pre>";
		//print_r($larrstudentinforesult);
		$this->view->AppliedDate=$larrstudentinforesult['AppliedDate'];
	$todaydate=date('Y-m-d');
		//$todaydate='2013-01-01';
		//echo $todaydate;
	$expireddate=0;
		if($todaydate>$larrstudentinforesult['DateTime'])
		{
			
			$expireddate=1;
		
		}
		$this->view->exp=$expireddate;

		//echo "<pre>";
		//print_r($larrstudentinforesult);
		//die();
		$this->view->Fname=$larrstudentinforesult['FName'];
		$this->view->EmailAddress=$larrstudentinforesult['EmailAddress'];
		$this->view->ICNO=$larrstudentinforesult['ICNO'];
		$this->view->AppliedDate=$larrstudentinforesult['AppliedDate'];
		
		$larrcourse = $this->lobjnewscreenmodel->fnGetCourseNames();
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrcourse);
		$this->view->lobjnewscreenForm->Coursename->setValue($larrstudentinforesult['IdProgrammaster']);
		 
		$this->view->lobjnewscreenForm->Coursename1->setValue($larrstudentinforesult['IdProgrammaster']);
		//echo $larrstudentinforesult['IdProgrammaster'];die();
		//$this->view->lobjnewscreenForm->Coursename->setValue();
				$this->view->examdateDate=$larrstudentinforesult['DateTime'];
					
		$larrscheduler = $this->lobjnewscreenmodel->fnGetSchedulerDetails();
		$this->lobjnewscreenForm->schedulerename->addMultiOptions($larrscheduler);
		$this->view->lobjnewscreenForm->schedulerename->setValue($larrstudentinforesult['Year']);
		
			
		$larrstate = $this->lobjnewscreenmodel->fnGetStateName();
		$this->lobjnewscreenForm->examstate->addMultiOptions($larrstate);
		
		$this->view->lobjnewscreenForm->examstate->setValue($larrstudentinforesult['ExamState']);
		
		$larrcity = $this->lobjnewscreenmodel->fnGetCityName($larrstudentinforesult['ExamCity']);
		$this->lobjnewscreenForm->examcity->addMultiOptions($larrcity);
		$this->view->lobjnewscreenForm->examcity->setValue($larrstudentinforesult['ExamCity']);
			
			
		$larrvenue = $this->lobjnewscreenmodel->fnGetVenueName($larrstudentinforesult['ExamCity']);
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrvenue);
		$this->view->lobjnewscreenForm->Venues->setValue($larrstudentinforesult['Examvenue']);
			
		$larrschedulersession = $this->lobjnewscreenmodel->fnGetSchedulerSessionDetails($larrstudentinforesult['Examsession']);
		$this->lobjnewscreenForm->examsession->addMultiOptions($larrschedulersession);
		$this->view->lobjnewscreenForm->examsession->setValue($larrstudentinforesult['Examsession']);
			
		$larrscheduleryear = $this->lobjnewscreenmodel->fnGetSchedulerYearDetails($larrstudentinforesult['Year']);
		$examdate=$larrstudentinforesult['Examdate'].'-'.$larrstudentinforesult['Exammonth'].'-'.$larrscheduleryear['year'];
		$this->view->lobjnewscreenForm->Examdate->setValue($examdate);
			
		$this->view->lobjnewscreenForm->paymentmode->setValue($larrstudentinforesult['ModeofPayment']);
		$this->view->lobjnewscreenForm->Payment->setValue($larrstudentinforesult['Payment']);
		$this->view->lobjnewscreenForm->Coursename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->Coursename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->schedulerename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examstate->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examcity->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->Venues->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examsession->setAttrib('readonly','true');
												$this->view->lobjnewscreenForm->Examdate->setAttrib('readonly','true');
				                              $this->view->lobjnewscreenForm->paymentmode->setAttrib('readonly','true');
												$this->view->lobjnewscreenForm->Payment->setAttrib('readonly','true');
				
				$larrcourse = $this->lobjnewscreenmodel->fnGetCourseNames();
		$this->lobjnewscreenForm->Coursename1->addMultiOptions($larrcourse);
		
			
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$this->view->lobjnewscreenForm->examdateid->setValue($larrstudentinforesult['DateTime']);
	
	/*	$iddate=$larrstudentinforesult['DateTime'];
		
				$larrdates=explode('-',$iddate);
	
		$year= $larrdates[0];
		$month=$larrdates[1];
		$date=$larrdates[2];
		
		
   //echo $curmonth;die();

		$larrschedulerresults = $this->lobjexamtimemodel->newgetscheduleryear($year);
		
		//print_r($larrschedulerresults);die();
		
	$values=0;
			for($i=0;$i<count($larrschedulerresults);$i++)
			{
				if(( $month >= $larrschedulerresults[$i]['From']) && ($month <= $larrschedulerresults[$i]['To'] ))
		{
				$value=$larrschedulerresults[$i]['idnewscheduler'];
				$values=$values.','.$value;
		}
			}
			
			//echo $values;die();
			
	   	$larrweekday = $this->lobjexamtimemodel->fngetdayofdate($iddate);
	        if($larrweekday['days']==1)
			{
				$larrweekday['days']= 7;
			}
			else 
			{
			$larrweekday['days']=$larrweekday['days']-1;	
			}
			
			
	   	$larrschedulerdays=$this->lobjexamtimemodel->fngetschedulerofdatevenuestudent($values,$larrweekday['days'],$iddate);
			
			
			*/
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	if ($this->_request->isPost() && $this->_request->getPost('Yes')) {
			$larrformData = $this->_request->getPost();
			
			
				$auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
				
				$idvenue = $larrformData['idvenues'];		
			$arrworkphone = explode("-",$idvenue);
		      $idcenter= $arrworkphone [0];
	          $idsession= $arrworkphone[1];	
				//echo $iduser;
			//echo "<pre/>";
			//print_r($larrformData);die();
			//echo  $idcenter;
			//echo  $idsession;
				
				
				
		$larrformData['UpdDate']=date('Y-m-d H:i:s');
			$idapplication = $larrformData['IDApplication'];
			
			$studenteditresult = $this->lobjnewscreenmodel->fngetstudentoldinfo($idapplication);
			//print_r($studenteditresult);die();
			
			
			$change=$studenteditresult['Venue'];
			$venuechange=$change+1;
		
			$studenteditiinserlarr=$this->lobjnewscreenmodel->fngetstudentinsertinfo($studenteditresult,$iduser,$larrformData['UpdDate'],$larrformData['newexamcity'],$idsession,$larrformData);
			//print_r($studentgetcityandstate);die();
	$studentgetcityandstate=$this->lobjnewscreenmodel->fnGetvenuecity($larrformData['newexamcity']);
			$idstate=$studentgetcityandstate['state'];
			$idcity=$studentgetcityandstate['city'];
			//print_r($larrformData);die();
  			 	$this->lobjnewscreenmodel->fnUpdateStudentnewscreen($idapplication,$larrformData,$larrformData['hiddenscheduler'],$iduser,$larrformData['newexamcity'],$idsession,$idstate,$idcity,$venuechange);
  		






	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    						
					//$this->view->mess = "Payment Completed Sucessfully";
					//$this->view->mess = "Payment Completed Sucessfully <br/> Please check your mail box If you have not received a confirmation mail in next 30minutes<br/>Please check your spam folder Add ibfiminfo@gmail.com to the address book to ensure future communications doesnt go to the spam folder";
					$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($idapplication);	
					$larrregid = $this->lobjstudentmodel->fngetRegid($idapplication);
				//	print_r($larrregid);die();
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Venue Change");
						
						//echo "<pre />";
						//print_r($larrresult);
						//die();
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
									//print_R($larrresult);die();
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										 $lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
										//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'].'('.$larrresult['ampmstart'].'--'.$larrresult['ampmend'].')',$lstrEmailTemplateBody);
	                                  if($larrresult['batchpayment']!=0)
										{
											$lstrEmailTemplateBody = str_replace("[Amount]",'NA',$lstrEmailTemplateBody);
										}
										else 
										{
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										}
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										//print_r($lstrEmailTemplateBody);
										//die();
									/*	$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
									//$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									//echo '<script language="javascript">alert("Because of server problem mails cannot be send this time")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
								
								 //$this->_redirect( $this->baseUrl . "/registration/index");
								if(mess){
									
								}
					
    			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    		




  			 		$this->_redirect( $this->baseUrl . '/examination/newscreen/index');		
  		//echo "<pre>";
  		//print_r($larrformData);
  		//die();
  		}
			
		
	}
	
 

	
}