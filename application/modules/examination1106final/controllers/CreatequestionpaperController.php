<?php
class Examination_CreatequestionpaperController extends Base_Base 
{
	private $lobjGroup; //db variable
	private $lobjGroupform;//Form variable
	private $_gobjlogger;
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object 
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjGroup = new Examination_Model_Createquestionpaper(); //intialize user db object
		$this->lobjGroupform = new Examination_Form_Createquestionpaper(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		 
	}
	
	//function to set and display the result
	public function indexAction() 
	{    	 
		$lobjform=$this->view->lobjform = $this->lobjGroupform; //send the lobjForm object to the view
		
		//$this->view->lobjform->field28->setAttrib('validator', 'validateUsername');
		
		$larrresult = $this->lobjGroup->fngetquestionsetlist(); //get businesstype details
		//$this->view->checkEmpty = 1;
		//$this->view->lobjform->field28->setAttrib('validator', 'validateUsername');
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->Businesstypepaginatorresult); // clear the search session
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$larrbatchresult = $this->lobjGroup->fnGetProgramName();
		$this->lobjGroupform->field1->addMultiOption('','Select');
		$this->lobjGroupform->field1->addMultiOptions($larrbatchresult);
		
		if(isset($this->gobjsessionsis->Businesstypepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Businesstypepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		// if ($this->_request->isPost ()&& $this->lobjGroupform->isValid($_POST)) 
		
		// check if the user has clicked the Search
	    if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		{   
			    $larrformData = $this->_request->getPost ();
			    //$this->view->checkEmpty = $larrformData['Active1'];				    									   
				unset ( $larrformData ['Search'] );
				
				$larrresult = $this->lobjGroup->fnGetsearchdetails($larrformData); //searching the values for the setcode
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				
				$this->gobjsessionsis->Businesstypepaginatorresult = $larrresult;
				
			   $this->lobjGroupform->populate($larrformData);
		}
		
		// check if the user has clicked the Generate
	 if ($this->_request->isPost () && $this->_request->getPost( 'Generate' )) 
		{       $larrformData = $this->_request->getPost ();	
						   
				unset ($larrformData ['Search']);
				unset ($larrformData ['Generate']);
				
				$this->lobjGroupform->populate($larrformData);
				
				if(empty($larrformData['field1'])){
				 echo "<script> alert('Please Select Course Name')</script>";
				 
				} else if(empty($larrformData['field28'])){
					 echo "<script> alert('Please Enter Question Paper Code')</script>";
				}
				else{
						$larrresultPaperCode = $this->lobjGroup->fngetpaper($larrformData['field28']); 
						if(!empty($larrresultPaperCode['PaperCode'])){	
							echo "<script> alert('Duplicate of Question Paper Code Not Allowed')
							window.location ='".$this->view->baseUrl()."/examination/createquestionpaper'; 	
							</script>";
							
							exit();	
								
						}   
							$program=$larrformData['field1'];
							  //  $lobjRegistrationModel= new App_Model_Examdetails();
					
			 		 		$lobjExamdetailsmodel = new App_Model_Examdetails();
			  	  // $program = $studentdetailsarray['Program'];
			  	   $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
			  	  // print_R($larractiveidbatch);
			  	   $idIdBatch = $larractiveidbatch['IdBatch'];
			  	   $this->view->idbatch = $idIdBatch;
			  	   //die();
			  		//print_r($larractiveidbatch);
			  		//die();
			
			  		$larrresultaa = $lobjExamdetailsmodel->fnGetBatchName($idIdBatch);
				//echo"<pre/>";
					//print_r($larrresultaa[0]['NosOfQues']);die();
			  		
					$this->view->noofqtns = $larrresultaa[0]['NosOfQues'];
			  		$larrmultisections = $lobjExamdetailsmodel->fnGetSectionsss($idIdBatch);
			  		$larridqtnfrombatch = $lobjExamdetailsmodel->fnGetQtnfromidbatch($idIdBatch);
			  	
			  		for ($linta=0;$linta<count($larridqtnfrombatch);$linta++)
				  	{
				  		$larrsection[$linta]=$larrmultisections[$linta]['IdSection'];
				  		
				  		$larrqtnfrombatch[$larrsection[$linta]]=$larridqtnfrombatch[$linta]['NosOfQuestion'];
				  	}
			  		
			  		$this->view->sectionqtns = $larrqtnfrombatch;
			  	for ($sec=0;$sec<count($larrmultisections);$sec++)
			  	{
			  		$larrsection[$sec]=$larrmultisections[$sec]['IdSection'];
			  	}
			  //	echo "<pre />";
			  //	print_r($larrmultisections);die();
			  	for($s=0;$s<count($larrsection);$s++)
					{
						$value = $larrsection[$s];
						if($s==0)
						{
							$values=$value;
						}
						else
						$values.=','.$value;
						
					}
					
					$this->view->countsections = count($larrsection);
					$this->view->listsections = $values;
			  		$this->view->sectionsarray = $larrsection;
			  		$this->view->sections = $larrmultisections;
			  		
			  		 $g=0;
			  		for($i=0;$i<count($larrsection);$i++){
			  		$larrresulteasy = $lobjExamdetailsmodel->fnGetNoofQuestion($idIdBatch,$larrsection[$i]);
			  	 
			  		if($larrresulteasy[0]['NoofQuestions']!=0)
			  		{   
			  		$larrdef[$g]=$larrresulteasy[0]['IdDiffcultLevel'];
			  		$g++;
			  		}
			  		if($larrresulteasy[1]['NoofQuestions']!=0)
			  		{
			  		$larrdef[$g]=$larrresulteasy[1]['IdDiffcultLevel'];
			  		$g++;
			  		}
			  		if($larrresulteasy[2]['NoofQuestions']!=0)
			  		{
			  		$larrdef[$g]=$larrresulteasy[2]['IdDiffcultLevel'];
			  		$g++;
			  		}
			  		
			  	     for($k=0;$k<count($larrresulteasy);$k++)
			            {
			            	$questionArr  = $larrresulteasy[$k]['IdDiffcultLevel'];
			            	$noofquestion = $larrresulteasy[$k]['NoofQuestions'];
			            	if($noofquestion<0)
			            	 $noofquestion = 0;
			            	$idsection = $larrresulteasy[$k]['IdSection'];
							switch ($questionArr) 
							{
							    case 1:
							        $easyquestion = $noofquestion;
							        break;
							    case 2:
							        $Mediumquestion = $noofquestion;
							        break;
							    case 3:
							        $difficultquestion = $noofquestion;
							        break;
							}
	            	}
			  		
            /*echo "<pre />";
        echo $easyquestion."<br>";
           echo  $Mediumquestion."<br>";
           echo $difficultquestion."<br>";
            print_r($larrsection[$i]);*/
            $larreasyquestion[$larrsection[$i]] = $lobjExamdetailsmodel->fnGetRandomEasyQuestions($larrsection[$i],$easyquestion,$Mediumquestion,$difficultquestion);
       
          
  		}
		
  $this->view->difficultylevel=$larrdef;
 //echo "<pre/>";
// print_r(count($larreasyquestion));
 // print_r($larreasyquestion);
 // die();
for($i=0;$i<count($larrsection);$i++)
		{
			$qunnoforsection  = array();
			$arranssection[] =$larrsection[$i] ;
			$noofqtnssection[$larrsection[$i]] =count($larreasyquestion[$larrsection[$i]]);
			for($l=0;$l<count($larreasyquestion[$larrsection[$i]]);$l++)
			{
				$qunno[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
				$qunnoforsection[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
				//$noofquestionofsec = 
			}
				$arraysofqtnnowithsection[$larrsection[$i]]=$qunnoforsection;
			
			//$arrayqstn[$l] = $larreasyquestion[$l]['idquestions'];
		}
		//print_r($arraysofqtnnowithsection);
		//die();
		$this->view->questionno = $arraysofqtnnowithsection;
		//die();
			
		for($j=0;$j<count($qunno);$j++)
		{
			$value = $qunno[$j];
			if($j==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		$arrayqtns = $values;
		$this->view->arrayqtns = $values;
		//echo "<pre/>";

	//print_r($values);
		//$larrquestions  = $lobjExamdetailsmodel->fngetquestionpool($values);
		
		
		$larranswers = $lobjExamdetailsmodel->fnGetAnswers($values);
  		$this->view->answers = $larranswers;
  		/*print_r($larrquestions);
		die();*/
  //	$this->view->totalnumberofquestions = count($larrquestionid);
   //    $this->view->questionsdisplay = $larrquestions;
     ////////////////////////////////////////////////////////////////////////////////////////  
       
	$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
  		
  		$flagg = 1;
  		if(count($larrmultisections)>1)
  		{
  			$flagg = 0;
  		}
		    for ($linta=0;$linta<count($larrmultisections);$linta++)
		    {
		    	
		    	$larrparts[$linta]=$larrmultisections[$linta]['IdPart'];
		    	//die();
		  		  $larrquestions[$linta] = $lobjExamdetailsmodel->fngetquestionpool($values,$larrmultisections[$linta]['IdPart']);  
					shuffle ($larrquestions[$linta]);
		  		    for($totalqtn=0;$totalqtn<count($larrquestions[$linta]);$totalqtn++)
		  		    {
		  		    	 $larrquestionnumber[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestions'];
		  		    	/*$larrquestionnumbers[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestions'];
		  		    	$concatquestions = $larrquestionnumbers[$linta][$totalqtn];
		  		    	$concatquestions.= ','.$concatquestions;*/
		  		    	
		  		    }
		  		    $larrquestionid = array();
		  		   for($totalqtn=0;$totalqtn<count($larrquestionnumber);$totalqtn++)
		  		    {	  		    	
		  		     	 $array2  = $larrquestionnumber[$totalqtn];		  		 
			  		     $larrquestionid  = array_merge($larrquestionid,$array2);
		  		    }
		  	 }
       
      // echo "<br/>";
		//echo "<pre/>";
//print_r($larrquestionid);
//print_r($larrquestions);
		 for($i=0;$i<count($larrquestionid);$i++)
		 {
		 	if($i==0)
			{
				$larrquestionids=$larrquestionid[$i];
			}
			else
			$larrquestionids.=','.$larrquestionid[$i];
		 }
		 /////////////////////////////inserting obrtained questions for the student//////////
		//$idstudent = $this->gsessionregistration->IDApplication;
		//  echo "<br/>";
		// echo  $program;
		 // echo "<br/>";
		// echo  $idIdBatch;
		 // echo "<br/>";
		  //$larrstudentobtainedquestions = $lobjExamdetailsmodel->fnGetStudentobtainedquestions($idstudent,$program,$idIdBatch,$larrquestionids);
		//print_r($larrquestionids);die();
		//////////////////////////////////end of funciton/////////////////////////////
      // $this->view->totalnumberofquestions = count($larrquestionid);
      // $this->view->questionsdisplay = $larrquestions;  
	 //$auth = Zend_Auth::getInstance();
  		$iduser =1;

		$ldtsystemDate = date('Y-m-d H:i:s');
		
				$larrresult = $this->lobjGroup->fninserquestioncodes($larrformData,$idIdBatch,$iduser,$ldtsystemDate); //searching the values for the businesstype
				
				$larrresultcodes = $this->lobjGroup->fninserquestionsforabovecode($larrresult,$larrquestionid); 
				
				/*echo '<script language="javascript">alert("Your set is created and  the printing of question set is under development")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/examination/createquestionpaper/index';</script>";
                	die();*/
			   	//$displayquestions= $this->lobjGroup->fndisplayquestionsforabovecode($larrresult); 
				/*echo "<pre/>";
				print_r($displayquestions);
				die();*/
				
				
				$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Generated the Question paper"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
				 $this->_redirect( $this->baseUrl . '/examination/createquestionpaper/index');	
	//ssionsis->Businesstypepaginatorresult = $larrresult;
				
			    }
		}
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/examination/createquestionpaper/index');			
		}
		
	}
	
/////////
 /*public function addAction() 
	{    	 
		$this->view->lobjGroupform = $this->lobjGroupform; //send the lobjForm object to the view
		$larrresult = $this->lobjGroup->fngetquestionsetlist(); //get businesstype details
		
		//$this->view->lobjGroupform->codedescription->setAttrib('validator', 'validateUsername');
		
		// check if the user has clicked the Generate
		if ($this->_request->isPost () && $this->_request->getPost( 'Generate' )) 
		{
			    $larrformData = $this->_request->getPost ();		
			  								   
				unset ($larrformData ['Search']);
				unset ($larrformData ['Generate']);
				     
				$program=$larrformData['field1'];
				  
		
 		 		$lobjExamdetailsmodel = new App_Model_Examdetails();
  	  
  	     $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
  	 
  	    $idIdBatch = $larractiveidbatch['IdBatch'];
  	    $this->view->idbatch = $idIdBatch;
  	   
  		$larrresultaa = $lobjExamdetailsmodel->fnGetBatchName($idIdBatch);
		$this->view->noofqtns = $larrresultaa[0]['NosOfQues'];
  		$larrmultisections = $lobjExamdetailsmodel->fnGetSectionsss($idIdBatch);
  		$larridqtnfrombatch = $lobjExamdetailsmodel->fnGetQtnfromidbatch($idIdBatch);
  	
  		for ($linta=0;$linta<count($larridqtnfrombatch);$linta++)
	  	{
	  		$larrsection[$linta]=$larrmultisections[$linta]['IdSection'];
	  		
	  		$larrqtnfrombatch[$larrsection[$linta]]=$larridqtnfrombatch[$linta]['NosOfQuestion'];
	  	}
  		
  		$this->view->sectionqtns = $larrqtnfrombatch;
  	    for ($sec=0;$sec<count($larrmultisections);$sec++)
  	   {
  		 $larrsection[$sec]=$larrmultisections[$sec]['IdSection'];
  	   }
 
  	   for($s=0;$s<count($larrsection);$s++)
		{
			$value = $larrsection[$s];
			if($s==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
			
		}
		$this->view->countsections = count($larrsection);
		$this->view->listsections = $values;
  		$this->view->sectionsarray = $larrsection;
  		$this->view->sections = $larrmultisections;
  		
  		$g=0;
  		for($i=0;$i<count($larrsection);$i++){
  		$larrresulteasy = $lobjExamdetailsmodel->fnGetNoofQuestion($idIdBatch,$larrsection[$i]);
  		if($larrresulteasy[0]['NoofQuestions']!=0)
  		{   
  		$larrdef[$g]=$larrresulteasy[0]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[1]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[1]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[2]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[2]['IdDiffcultLevel'];
  		$g++;
  		}
  		
  	     for($k=0;$k<count($larrresulteasy);$k++)
            {
            	$questionArr  = $larrresulteasy[$k]['IdDiffcultLevel'];
            	$noofquestion = $larrresulteasy[$k]['NoofQuestions'];
            	if($noofquestion<0)
            	 $noofquestion = 0;
            	$idsection = $larrresulteasy[$k]['IdSection'];
				switch ($questionArr) 
				{
				    case 1:
				        $easyquestion = $noofquestion;
				        break;
				    case 2:
				        $Mediumquestion = $noofquestion;
				        break;
				    case 3:
				        $difficultquestion = $noofquestion;
				        break;
				}
            }
          
            $larreasyquestion[$larrsection[$i]] = $lobjExamdetailsmodel->fnGetRandomEasyQuestions($larrsection[$i],$easyquestion,$Mediumquestion,$difficultquestion);
  		}
 		 $this->view->difficultylevel=$larrdef;
		for($i=0;$i<count($larrsection);$i++)
		{
			$qunnoforsection  = array();
			$arranssection[] =$larrsection[$i] ;
			$noofqtnssection[$larrsection[$i]] =count($larreasyquestion[$larrsection[$i]]);
			for($l=0;$l<count($larreasyquestion[$larrsection[$i]]);$l++)
			{
				$qunno[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
				$qunnoforsection[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
				
			}
				$arraysofqtnnowithsection[$larrsection[$i]]=$qunnoforsection;
		}
		$this->view->questionno = $arraysofqtnnowithsection;
			
		for($j=0;$j<count($qunno);$j++)
		{
			$value = $qunno[$j];
			if($j==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		$arrayqtns = $values;
		$this->view->arrayqtns = $values;
		$larranswers = $lobjExamdetailsmodel->fnGetAnswers($values);
  		$this->view->answers = $larranswers;
       
		$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
  		
  		$flagg = 1;
  		if(count($larrmultisections)>1)
  		{
  			$flagg = 0;
  		}
		    for ($linta=0;$linta<count($larrmultisections);$linta++)
		    {
		    		$larrparts[$linta]=$larrmultisections[$linta]['IdPart'];
		  		    $larrquestions[$linta] = $lobjExamdetailsmodel->fngetquestionpool($values,$larrmultisections[$linta]['IdPart']);  
					shuffle ($larrquestions[$linta]);
		  		    for($totalqtn=0;$totalqtn<count($larrquestions[$linta]);$totalqtn++)
		  		    {
		  		    	 $larrquestionnumber[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestions'];
		  		    }
		  		    $larrquestionid = array();
		  		   for($totalqtn=0;$totalqtn<count($larrquestionnumber);$totalqtn++)
		  		    {	  		    	
		  		     	 $array2  = $larrquestionnumber[$totalqtn];		  		 
			  		     $larrquestionid  = array_merge($larrquestionid,$array2);
		  		    }
		  	 }
       
		 for($i=0;$i<count($larrquestionid);$i++)
		 {
		 	if($i==0)
			{
				$larrquestionids=$larrquestionid[$i];
			}
			else
			$larrquestionids.=','.$larrquestionid[$i];
		 }
  		$iduser =1;

		$ldtsystemDate = date('Y-m-d H:i:s');
		
				$larrresult = $this->lobjGroup->fninserquestioncodes($larrformData,$idIdBatch,$iduser,$ldtsystemDate); //searching the values for the businesstype
				$larrresultcodes = $this->lobjGroup->fninserquestionsforabovecode($larrresult,$larrquestionid); 
				$this->_redirect( $this->baseUrl . '/examination/createquestionpaper/index');	
		}
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Back' )) 
		{
			 $this->_redirect( $this->baseUrl . '/examination/createquestionpaper/index');			
		}
	}
/////////////*/
	
public function pdfexportAction()
	{           
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$RegID = $this->_getParam('id');
		$progname = $this->lobjGroup->fngetprgname($RegID);
		$tosdesc = $this->lobjGroup->fngettosdesc($RegID);
		$displayquestions= $this->lobjGroup->fndisplayquestionsforabovecode($RegID);
		$Active = $this->_getParam('Active');
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		//$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('SetcreationDate :'.$progname['creationdate'].'       '.'Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Question" ).' '.$this->view->translate( "Paper" );
		//$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table><br>";
		//$tabledata.= '<table width="100%"><tr><td width="10%"><b>Course Name&nbsp;</b></td><td valign="top" width="30%"><b>: </b>'.$progname['ProgramName'].'</td><td width="5%"><b>TOS&nbsp;</b></td><td width="25%"><b>: </b>'.$tosdesc['Setcode'].'</td><td width="10%"><b>Set Code&nbsp;</b></td><td valign="top" width="20%"><b>: </b>'.$progname['PaperCode'].'</td></tr></table>';
		
		$tabledata.= '<table width=100%><tr><td width="17%"><b>Course Name&nbsp;</b></td><td width="30%"><b>: </b>'.$progname['ProgramName'].'</td><td width="7%"><b>TOS &nbsp;</b></td><td width="23%"><b>: </b>'.$tosdesc['Setcode'].'</td><td width="12%"><b>Set Code&nbsp;</b></td><td width="28%"><b>: </b>'.$progname['PaperCode'].'</td></tr></table>';
		//$tabledata.= 'Course :&nbsp;'.$progname['ProgramName'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'TOS:&nbsp;'.$tosdesc['Setcode'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'Question Set:&nbsp;'.$progname['PaperCode'].'<br>';
		$tabledata.='<hr></hr><br>';
		?>
		<?php  for($i=1,$s=0;$i<=count($displayquestions),$s<count($displayquestions);$s++,$i++)
		  { ?>
			<?php if($Active==1)
		        {
	  				$tabledata.='<table ><tr>
     				<td valign="top" >'.'Q'.$i.'.'.'&nbsp;'.$displayquestions[$s]['idquestions'].')'.' </td><td align="left">'.trim($displayquestions[$s]['Question']).'<br></td>
	  				</tr></table><br>';
		            	$larranswers = $this->lobjGroup->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
		            	for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++)
		            	{
							$tabledata.='<table><tr><td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$i.'.'.$larranswers[$a]['idanswers'].')'.'</td><td>'.$larranswers[$a]['answers'].'<br></td></tr></table><br>';
		       			}
		       }else {
		     			$tabledata.='<table ><tr>
     					<td valign="top" >'.'Q'.$i.')'.'&nbsp;'.' </td><td align="left">'.trim($displayquestions[$s]['Question']).'<br></td></tr></table><br>';
		            	$larranswers = $this->lobjGroup->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
		            	for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++)
		            	{
		            		$tabledata.='<table><tr><td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$i.')&nbsp;</td><td>'.$larranswers[$a]['answers'].'</td><br></tr></table><br>';
		                }
				     }
		}
		?><?php 
		$mpdf->WriteHTML($tabledata);   
		$mpdf->Output($progname['PaperCode'].'_'.'Questions_Report.pdf','D');
		
				$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Printed the Question paper report for ".$progname['ProgramName']."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
		}
	
		///////////////////
	public function getvalidpaperAction(){
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$code = $this->_getParam('code');	
		$larrDetails = $this->lobjGroup->fngetpaper($code);
		echo $larrDetails['PaperCode'];
		
	}
		
	public function wordAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$RegID = $this->_getParam('id');
		$progname = $this->lobjGroup->fngetprgname($RegID);
		$tosdesc = $this->lobjGroup->fngettosdesc($RegID);
		$displayquestions= $this->lobjGroup->fndisplayquestionsforabovecode($RegID);
		$ReportName = $this->view->translate( "Question" ).' '.$this->view->translate( "Paper" );
		$filename = $progname['PaperCode'].'_'."Question_Paper.doc";
		$filename = str_replace(' ','_',$filename); 
		$Active = $this->_getParam('Active');
		$tabledata = '<table><img width=90% align="left" src = "http://192.168.1.103/tbe/images/reportheader.jpg"/></table>';
		$tabledata.="<table border = 1 width=100% align=center><tr><td align=center <b> {$ReportName} </b></td></tr></table><br>";
		$tabledata.= '<table width="100%"><tr><td width="10%"><b>Course Name&nbsp;</b></td><td valign="top" width="30%"><b>: </b>'.$progname['ProgramName'].'</td><td width="5%"><b>TOS&nbsp;</b></td><td width="25%"><b>: </b>'.$tosdesc['Setcode'].'</td><td width="10%"><b>Set Code&nbsp;</b></td><td valign="top" width="20%"><b>: </b>'.$progname['PaperCode'].'</td></tr></table>';
		//$tabledata.=('<table><tr><td width="10%"><b>'.'Course Name:&nbsp;</b></td><td width="35">'.$progname['ProgramName'].'<td width="5%"><b>TOS:&nbsp;<?b></td><td width="35">'.$tosdesc['Setcode'].'<td width="10%"><b>Question Set:&nbsp;</b></td><td width="30>'.$progname['PaperCode']).'</td></tr></table>';
		$tabledata.= '<hr>';
		?>
		<?php  for($i=1,$s=0;$i<=count($displayquestions),$s<count($displayquestions);$s++,$i++){ ?> 
		<?php  if($Active==1)
		       {
						$tabledata.='<table><tr><td valign="top">'.'Q'.$i.'.&nbsp;'.$displayquestions[$s]['idquestions'].')</td><td>&nbsp;'.trim($displayquestions[$s]['Question']).'</td></tr></table><br>';
		            	$larranswers = $this->lobjGroup->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
		            	for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++)
		            	{
						 	$tabledata.='<table><tr><td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$i.'.'.'&nbsp;'.$larranswers[$a]['idanswers'].')'.'</td><td>&nbsp;'.trim($larranswers[$a]['answers']).'</td></tr><br></table>';
		                }
		       }else 
		       {
		       			$tabledata.='<table><tr><td valign="top">'.'Q'.$i.'.'.')</td><td>&nbsp;'.trim($displayquestions[$s]['Question']).'</td></tr></table><br>';
		            	$larranswers = $this->lobjGroup->fndisplayanswersforquestions($displayquestions[$s]['idquestions']);
		            	for($d=1,$a=0;$d<=count($larranswers),$a<count($larranswers);$d++,$a++)
		            	{
						 	$tabledata.='<table><tr><td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.'A'.$i.')&nbsp;</td><td>'.trim($larranswers[$a]['answers']).'</td></table><br>';
		                }
		       }
		}
		?>
		<?php 
        header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename= $filename ");
		$ourFileName = realpath('.')."/data";
		ini_set('max_execution_time',3600);
		$ourFileHandle = fopen($ourFileName, 'a')or die("can't open file"); 
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
		fclose($ourFileHandle);
		readfile($ourFileName);
		unlink($ourFileName);
		}
}