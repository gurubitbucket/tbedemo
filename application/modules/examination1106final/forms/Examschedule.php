<?php
class Examination_Form_Examschedule extends Zend_Dojo_Form 
{    
	
    public function init() 
    {    
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    		
         $field5 = new Zend_Dojo_Form_Element_FilteringSelect('field5'); 
         $field5->setAttrib('dojoType',"dijit.form.FilteringSelect");
         $field5->setRegisterInArrayValidator(false);    
         $field5->setAttrib('required',"true"); 
         $field5->removeDecorator("DtDdWrapper");        
         $field5->removeDecorator("Label");
         $field5->removeDecorator('HtmlTag');
         
         $field1 = new Zend_Dojo_Form_Element_FilteringSelect('field1'); 
         $field1->setAttrib('dojoType',"dijit.form.FilteringSelect");
         $field1->setRegisterInArrayValidator(false);    
         $field1->setAttrib('required',"true"); 
         $field1->removeDecorator("DtDdWrapper");        
         $field1->removeDecorator("Label");
         $field1->removeDecorator('HtmlTag');
         
         $fromdate = new Zend_Dojo_Form_Element_DateTextBox('fromdate');
         $fromdate->setAttrib('dojoType',"dijit.form.DateTextBox");
         $fromdate->setAttrib('class', 'txt_put');
         $fromdate->removeDecorator("DtDdWrapper");
         $fromdate->setAttrib('title',"dd-mm-yyyy");
         $fromdate->removeDecorator("Label");
         $fromdate->removeDecorator('HtmlTag');
         $fromdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
         
         $todate = new Zend_Dojo_Form_Element_DateTextBox('todate');
         $todate->setAttrib('dojoType',"dijit.form.DateTextBox");
         $todate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
         $todate->setAttrib('class', 'txt_put');
         $todate->removeDecorator("DtDdWrapper");
         $todate->setAttrib('title',"dd-mm-yyyy");
         $todate->removeDecorator("Label");
         $todate->removeDecorator('HtmlTag');
        
         $Save = new Zend_Form_Element_Submit('Save');
         $Save->dojotype="dijit.form.Button";
         $Save->label = $gstrtranslate->_("Save");
         $Save->setAttrib('class', 'NormalBtn');
         $Save->setAttrib('id', 'submitbutton');
         $Save->removeDecorator("DtDdWrapper");
         $Save->removeDecorator("Label");
         $Save->removeDecorator('HtmlTag');
           		         		
         $Back = new Zend_Form_Element_Button('Back');
         $Back->dojotype="dijit.form.Button";
		 $Back->setAttrib('class', 'NormalBtn');
		 $Back->removeDecorator("Label");
		 $Back->removeDecorator("DtDdWrapper");
		 $Back->removeDecorator('HtmlTag');
		
		 $Search = new Zend_Form_Element_Submit('Search');
         $Search->dojotype="dijit.form.Button";
         $Search->label = $gstrtranslate->_("Search");
         $Search->removeDecorator("DtDdWrapper");
         $Search->removeDecorator("Label");
         $Search->removeDecorator('HtmlTag');
         $Search->class = "NormalBtn";
        
         $Generate = new Zend_Form_Element_Submit('Generate');
		 $Generate->dojotype="dijit.form.Button";
         $Generate->setAttrib('class', 'NormalBtn')
        		  ->removeDecorator("DtDdWrapper")
	        	  ->removeDecorator("Label")
    	    	  ->removeDecorator('HtmlTag');
		 $Generate->label = $gstrtranslate->_("Generate");
		
		 $Active1  = new Zend_Form_Element_Checkbox('Active1');
         $Active1->setAttrib('dojoType',"dijit.form.CheckBox");
         $Active1->setvalue('1');
         $Active1->removeDecorator("DtDdWrapper");
         $Active1->removeDecorator("Label");
         $Active1->removeDecorator('HtmlTag');

         $DescActive  = new Zend_Form_Element_Checkbox('DescActive');
         $DescActive->setAttrib('dojoType',"dijit.form.CheckBox");
         $DescActive->setvalue('1');
         $DescActive->removeDecorator("DtDdWrapper");
         $DescActive->removeDecorator("Label");
         $DescActive->removeDecorator('HtmlTag');
        
        //form elements
        $this->addElements(array($field1,$field5,$fromdate,$todate,$Save,$Back,$Search
                                )
                          );

    }
}