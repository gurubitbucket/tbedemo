<?php
	class Examination_Form_Questionattended extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
		
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
	  $yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 

		
				$session = new Zend_Dojo_Form_Element_FilteringSelect('session');
			    $session->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $session->removeDecorator("DtDdWrapper");
	            $session->removeDecorator("Label")
	            ->setAttrib('onChange', "getsessiondetails(this.value);");
	            $session->removeDecorator('HtmlTag');
	            $session->setAttrib('required',"true");	
         	    
	            $FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
	        	$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('required',"true")	
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
	            $ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
	        	$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('required',"true")	
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
												
			  
				$Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');
			    $Venue->setAttrib('dojoType',"dijit.form.FilteringSelect");
			   // $Venue->setAttrib('required',"true");
	            $Venue->removeDecorator("DtDdWrapper");
	           // $Venue->setAttrib('onChange', "newsessionlists();");
	            $Venue->removeDecorator("Label");
	            $Venue->removeDecorator('HtmlTag');
	            
	            $Program = new Zend_Dojo_Form_Element_FilteringSelect('Program');
			    $Program->setAttrib('dojoType',"dijit.form.FilteringSelect");
	          //  $Course->setAttrib('required',"true"); 	           	         		       		     
	            $Program->removeDecorator("DtDdWrapper");
	            $Program->removeDecorator("Label");
	            $Program->removeDecorator('HtmlTag');
	            
	              $Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $Clear = new Zend_Form_Element_Button('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');         		
         		
			$this->addElements(
        					array($session,
        					      $Date,
        					      $Venue,$FromDate,$ToDate,
        					      $Program,$Search,$Clear
        			
        						)
        			);
		}
}
