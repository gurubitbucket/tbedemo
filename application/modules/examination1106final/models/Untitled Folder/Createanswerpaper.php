<?php
     class Examination_Model_Createanswerpaper extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	
        
        public function fngetdetails(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.FName as Studentname","a.IDApplication","DATE_FORMAT(a.datetime,'%d-%m-%Y') as ExamDate","DATE_FORMAT(Date(a.upddate),'%d-%m-%Y') as ApplicationDate"))
										  ->join(array("b" => "tbl_studentmarks"),'a.IDApplication = b.IDApplication',array("b.Correct as Marksscored","b.Grade"))
										  ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName as Programname"))
										  ->join(array("d" => "tbl_center"),'a.Examvenue = d.idcenter',array("d.centername as Venue"));
				/*echo $lstrSelect;
				die();*/
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
      public function fncheckSuperAdminPwd($password)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $lobjDbAdpt->select()
						->from(array("a" => "tbl_superadmin"))
						->where("a.SuperAdminPassword =?",$password);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
          
    public function fncheckSuperUserPwd1($passwoord)
	{ 
		

		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $lobjDbAdpt->select()
						->from(array("a" => "tbl_user"))
						->join(array('b' => 'tbl_definationms'),'a.IdRole = b.idDefinition')
						->where("a.passwd =?",$password)
						->where("b.DefinitionDesc = 'Superadmin'");
						//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	  
	  
      public function fngetsearchdetails($larrformData){
      			
      	        if($larrformData['field2']) $name = $larrformData['field2'];
				
				if($larrformData['field1']) $Coursename = $larrformData['field1'];
				if($larrformData['field5']) $venue = $larrformData['field5'];
				if($larrformData['field10'])$edate = $larrformData['field10'];
			   
			
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.FName as Studentname","a.IDApplication","DATE_FORMAT(a.datetime,'%d-%m-%Y') as ExamDate","DATE_FORMAT(Date(a.upddate),'%d-%m-%Y') as ApplicationDate"))
										// ->join(array("b" => "tbl_studentmarks"),'a.IDApplication = b.IDApplication',array("b.Correct as Marksscored","b.Grade"))
										  ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName as Programname"))
										  ->join(array("d" => "tbl_center"),'a.Examvenue = d.idcenter',array("d.centername as Venue"));
			
				if($larrformData['field2']) $lstrSelect .= " AND a.FName like '%$name%'"; 
				if($larrformData['field1']) $lstrSelect .= " AND c.IdProgrammaster = $Coursename";
				if($larrformData['field5']) $lstrSelect .= " AND d.idcenter = $venue";
				if($larrformData['field10']) $lstrSelect .= " AND a.DateTime = '$edate'";					  
				
				$lstrSelect .= " ORDER BY a.FName";
				/*echo $lstrSelect;
				die();*/
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
      public function fnGetProgramName()
      {		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										  ->join(array("c" => "tbl_program"),'c.IdProgram=a.idprog')
										  ->join(array("b"=>"tbl_batchmaster"),'b.IdProgrammaster=a.IdProgrammaster',array())
										  ->join(array("d"=>"tbl_tosmaster"),'b.IdBatch=d.IdBatch',array())
  										  ->join(array("e"=>"tbl_programrate"),'a.IdProgrammaster=e.idProgram',array())
										  ->where("e.Active=1")
										  ->where("d.Active=1")
										  ->where("c.Active =1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	 }

	 public function fngetcenternames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername")) 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
   
  	public function fndisplayquestions($id)
    {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       $lstrSelect = $lobjDbAdpt->select()
                                ->from(array("a" => "tbl_studentapplication"),array(""))
                                ->join(array("b" => "tbl_questionsetforstudents"),'a.IDApplication = b.IDApplication',array('b.idquestions'))
                                ->where("b.IDApplication = $id")
                                ->order('b.idquestionsetforstudents desc') ;
   	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
   	   return $larrResult;
    }
   
    public function fngetquestions($qid)
    {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_questions"),array("TRIM(a.Question) AS Question"))
                             ->where("a.idquestions = $qid");
   	    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
   	    return $larrResult;
    }
        
   public function fngetanswers($qid)
   {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_answers"),array("a.answers","a.idanswers"))
                             ->where("a.idquestion = $qid");
   	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
   	    return $larrResult;
   }
   
 /* public function fngetStudentAnsweerActualAns($qid,$IdApplication)
   {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Answer"))
                             ->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid')
                             ->join(array("c" => "tbl_answers"),'a.Answer = c.idanswers',array("c.answers AS StudAns","c.idanswers as Studidanswers"))
                             ->join(array("d" => "tbl_answers"),'a.QuestionNo = d.idquestion',array("d.answers AS CorrectAns","d.idanswers as Correctidanswers"))
                             ->where("a.QuestionNo = $qid")
                             ->where("d.CorrectAnswer = 1")
                             ->where("d.idquestion = $qid")
                             ->where("b.IDApplication = $IdApplication");
                             echo $lstrSelect;die();
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
   	    return $larrResult;
   }
   
		SELECT `a`.`QuestionNo`, `a`.`Answer`, `b`.*, `c`.`answers` AS `StudAns`, `c`.`idanswers` AS `Studidanswers`, `d`.`answers` AS `CorrectAns`, `d`.`idanswers` AS `Correctidanswers` 
		FROM `tbl_answerdetails` AS `a` 
		INNER JOIN `tbl_registereddetails` AS `b` ON a.Regid = b.Regid 
		INNER JOIN `tbl_answers` AS `c` ON a.Answer = c.idanswers 
		INNER JOIN `tbl_answers` AS `d` ON a.QuestionNo = d.idquestion 
		WHERE (a.QuestionNo = 339) AND (d.idquestion = 339) AND (d.CorrectAnswer = 1) AND (b.IDApplication = 1287)
   */
   
       public function fngetStudentAnsweerActualAns($qid,$IdApplication)
   {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo","a.Answer"))
                             ->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid')
                             ->join(array("c" => "tbl_answers"),'a.Answer = c.idanswers',array("c.answers AS StudAns","c.idanswers as Studidanswers"))
                             ->join(array("d" => "tbl_answers"),'a.QuestionNo = d.idquestion',array("d.answers AS CorrectAns","d.idanswers as Correctidanswers"))
                             ->where("a.QuestionNo = $qid")
                             ->where("d.idquestion = $qid")
                             ->where("d.CorrectAnswer = 1")
                             ->where("b.IDApplication = $IdApplication");
                           //  echo $lstrSelect;die();
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
   	    return $larrResult;
   }
   
   public function fnGetStudentDetails($IDApp)
{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();   
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("b" =>"tbl_studentapplication"),array("b.*","CONCAT(b.Examdate,'-',IFNULL(b.Exammonth,' '),'-',IFNULL(r.Year,' ')) as StudExamdate"))
											  ->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
											  ->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
											  ->join(array("r" =>"tbl_newscheduler"),'r.idnewscheduler=b.Year',array("r.Year as Years"))
											  ->join(array("i" =>"tbl_managesession"),'i.idmangesession=b.Examsession',array("i.managesessionname","CONCAT(i.starttime,'-',IFNULL(i.endtime,' ')) as ExamSessionTime"))
											  ->join(array("studmrk" =>"tbl_studentmarks"),'studmrk.IDApplication = b.IDApplication')
											  //->where("i.active=1")
											  ->where("b.IDApplication = $IDApp");
											 // echo $lstrSelect;die();
					$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
					return $larrResult;
	}
   
  public function fngetanswersCheck($qid,$IdApplication)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_answerdetails"),array("a.Answer"))
                             ->join(array("b" => "tbl_registereddetails"),'a.Regid = b.Regid')
                             ->where("a.QuestionNo = $qid")
                             ->where("b.IDApplication = $IdApplication");
        $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
   	    return $larrResult;
   }
   
   public function fngetanswered($appid,$qid)
   {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_registereddetails"),array(""))
                             ->join(array("b" => "tbl_answerdetails"),'a.Regid = b.Regid',array('b.Answer'))
                             ->where("a.IDApplication = $appid")
                             ->where("b.QuestionNo = $qid");
   	    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
   	    return $larrResult;
   }
      public function fngetregid($qid)
   {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   		$lstrSelect = $lobjDbAdpt->select()
                             ->from(array("a" => "tbl_registereddetails"),array("a.*"))
                             //->join(array("b" => "tbl_answerdetails"),'a.Regid = b.Regid',array('b.Answer'))
                             ->where("a.IDApplication = $qid");
   	    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
   	    return $larrResult;
   }
   
         public function totalattendedquestions($regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
     $select = $db->select()
		            ->from(array('a'=>'tbl_answerdetails'),array("a.Answer"))
		            //->join(array('b'=>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array("b.*"))
		            ->where("a.Regid  ='$regid'");
		    $result = $db->fetchAll($select);
		    return $result;
    }
   
        }    
		  	 
	
