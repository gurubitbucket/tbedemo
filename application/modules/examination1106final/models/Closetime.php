<?php
	class Examination_Model_Closetime extends Zend_Db_Table {


	public function  fngetschedulerofdatevenuestudent($ids,$days,$iddate)
       {
       			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array(""))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulervenue"),'a.idnewscheduler=d.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_center"),'d.idvenue=f.idcenter',array("key"=>"f.idcenter","value"=>"f.centername"))
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days)
										   ->group("f.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       }
       
	public function newgetscheduleryear($year)
     {
     	
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler","a.To","a.From"))
                                          ->where("a.Active = 1")
										  ->where("a.Year=?",$year); 
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     	
     }       
     
	public function  fngetschedulerofdatesessionstudent($ids,$days,$iddate)
       {
       	
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array(""))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler',array(""))
										  ->join(array("d"=>"tbl_newschedulervenue"),'a.idnewscheduler=d.idnewscheduler',array(""))
									      ->join(array("e"=>"tbl_newschedulersession"),'a.idnewscheduler=e.idnewscheduler',array(""))
									      ->join(array("f"=>"tbl_managesession"),'f.idmangesession=e.idmanagesession',array("key"=>"f.idmangesession","value"=>"f.managesessionname"))
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days)
										   ->group("e.idmanagesession");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }
       
	public function fngetdayofdate($iddate)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	  $sql = "select DAYOFWEEK('$iddate') as days"; 
				//echo $sql; die();
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
       
       /*
        * function to find all the centres which has been started
        */
       public function fnsearchcentres($larrformdata)
       {
       	  $days=$larrformdata['Date'];
       	  $session = $larrformdata['session'];
       	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_centerstartexam"),array("a.*","a.idcenter as centreids"))
							 			  ->join(array("b"=>"tbl_center"),'a.idcenter=b.idcenter',array("b.*"))
							 			  ->join(array("c"=>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
										   ->where("a.ExamDate=?",$days)
										   ->where("a.idSession=?",$session)
										   //->where("a.CloseTime IS NULL")
										  // ->group("a.idcenter")
										   ->order("b.centername")
										   ->order("c.Programname")
										   ;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       }
       

	public function fnExampercentage($idbatch)
    {
    	   $db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->join(array('c'=>'tbl_tosmaster'),array("c.NosOfQues","c.TimeLimit","c.Pass"))
		             ->where("c.IdBatch = ?",$idbatch);
		    $result = $db->fetchRow($select);
		    return $result;
    }
	
       
	/*
	 * function for finding the correct answer
	 */
	public function fncorrectanswer($Regid)
    {
    	
    	    $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT * FROM tbl_answerdetails  where Regid = '$Regid'  and answer in(SELECT idanswers FROM tbl_answers where CorrectAnswer =1)";
    		$result = $db->fetchAll($sql);    
			return $result;
    }
    
    /*
     * function to update the student status
     */
	public function fnupdatepass($idstudent,$result)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Pass'] = $result;	
		 $where = "IDApplication = '".$idstudent."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
    }
    
		/*
	 * function to insert into the student marks table
	 */
	public function fninsertstudentmarks($studentid,$noofquestions,$totalanswered,$attended,$grade)
	{
		$db =  Zend_Db_Table::getDefaultAdapter();  
		 $table = "tbl_studentmarks";
           $postData = array(		
							'IDApplication' => $studentid,		
            				'NoofQtns' =>$noofquestions,		
		            		'Attended' =>$totalanswered,	
		            		'Correct'	=>$attended,
		            		'Grade'=>$grade															
						);			
	        $db->insert($table,$postData);
	}
	
	
	/*
	 * function to insert into partwise details
	 */
	public function fninsertstudentpartwise($studentid,$RegID)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        $table = "tbl_studentdetailspartwisemarks";
           $postData = array(		
							'IDApplication' => $studentid,		
            				'Regid' =>$RegID,		
		            		'A' =>0,	
		            		'B'=>0,
           					'C'=>0,
                            'UpdDate'=>date("Y-m-d H:i:s")
						);			
	        $db->insert($table,$postData);
    }
    
    
    /*
     * function to get the parts of the program
     */
	public function fngettheidparts($idbatchresult)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_batchdetail"),array("GROUP_CONCAT(IdPart SEPARATOR ',')as partsids")) 	
				 				 ->where("a.IdBatch=?",$idbatchresult);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;	
   }
   
   /*
    * function to get the partwise marks
    */
	public function partwiseattended($Regid,$part)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    		 $sql = "Select count(idquestions) from tbl_questions where idquestions  in (
					SELECT  QuestionNo FROM tbl_answerdetails  where Regid = '$Regid'  and answer in(SELECT idanswers FROM tbl_answers where CorrectAnswer =1))
					and QuestionGroup = '$part'";
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
       
     /*
      * function for inserting into the parwise
      */
	    public function updatepartwisemarksfora($RegID,$partbmarks,$part)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1[$part] = $partbmarks;	
		 $where = "Regid = '".$RegID."'"; 	
		 $db->update('tbl_studentdetailspartwisemarks',$larrformData1,$where);
    }
    
    public function updatepartwisemarksforb($RegID,$partbmarks)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['B'] = $partbmarks;	
		 $where = "Regid = '".$RegID."'"; 	
		 $db->update('tbl_studentdetailspartwisemarks',$larrformData1,$where);
    }
    
    public function updatepartwisemarksforc($RegID,$partbmarks)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['C'] = $partbmarks;	
		 $where = "Regid = '".$RegID."'"; 	
		 $db->update('tbl_studentdetailspartwisemarks',$larrformData1,$where);
    }
    
	public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}
    
	
   
   
  public function fnGetStudentDetailsPassfail($idapplication)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_studentapplication'),array("a.FName","a.ICNO","a.DateTime","a.EmailAddress","a.IDApplication"))
		            ->join(array('b'=>'tbl_registereddetails'),'a.IDApplication=b.IDApplication',array("b.Regid"))
		            ->join(array('f'=>'tbl_programmaster'),'f.IdProgrammaster=a.Program',array("f.ProgramName"))
		            ->where("a.IDApplication = ?",$idapplication);
		    $result = $db->fetchRow($select);
		    return $result;
    }
       
     public function insertintoanswerdetails($sql)
      {
      	 $db = Zend_Db_Table::getDefaultAdapter();
      	 	$select = "SELECT max(idanswerdetail)
   	 			FROM tbl_answerdetails";     	 
      	 $lselect = $db->query($sql);
      	 return $resulttempdetails = $db->fetchRow($select);
      }
      
      /*
       * function for finding the maximum total time
       */
      public function fnfindthemaxtimeofprogram($idvenue,$selecteddate,$selectedsession)
      {
      	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_centerstartexam"),array("max(CAST((a.Totaltime) AS UNSIGNED INT)) as maximumtime","a.Startedtime")) 	
				 				 ->where("a.Examdate=?",$selecteddate)
				 				 ->where("a.idSession=?",$selectedsession)
				 				 ->where("a.idcenter=?",$idvenue);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;	
      }
      
      
	public function fngetvenue($selecteddate,$selectedsession)
      {
      	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_centerstartexam"),array("a.idcenter")) 	
				 				 ->where("a.Examdate=?",$selecteddate)
				 				  //->where("a.CloseTime IS NULL")
				 				 ->where("a.idSession=?",$selectedsession);
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;	
      }    
      /*
       * function for finding the max startedtime
       */
	  public function fnfindthemaxstarttimeofvenue($idvenue,$selecteddate,$selectedsession)
      {
      	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_centerstartexam"),array("max(CAST((a.Startedtime) AS UNSIGNED INT)) as maximumstartedtime")) 	
				 				 ->where("a.Examdate=?",$selecteddate)
				 				 ->where("a.idSession=?",$selectedsession)
				 				 ->where("a.idcenter=?",$idvenue);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;	
      }
      
      /*
       * delete from the temporaray table
       */
		public function deletefromtempdetails($regid)
 	   {
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$select = "Delete from tbl_tempexamdetails where Regid in ($regid)";
 		$result = $db->query($select);
 	  }
      
 	  
 	  
	public function fnclosetime($idvenue,$selectedsession,$selecteddate)
    {
   	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	   $pass=3;
			$where = "ExamDate = '".$selecteddate."'  AND idcenter ='".$idvenue."'  AND idSession ='".$selectedsession."'"; 	 
			     $postData = array(		
							'CloseTime' => date('H:i:s')														
						);
				$table = "tbl_centerstartexam";
	            $lobjDbAdpt->update($table,$postData,$where);
    }
 	  /*
 	   * function for updateing students to absent
 	   */
	public function fnstudentsabsent($idvenue,$selectedsession,$selecteddate)
    {
   	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	   $pass=3;
			$where = "DateTime = '".$selecteddate."'  AND Examvenue ='".$idvenue."'  AND Examsession ='".$selectedsession."' AND Pass ='".$pass."'"; 	 
			     $postData = array(		
							'Pass' => 4														
						);
				$table = "tbl_studentapplication";
	            $lobjDbAdpt->update($table,$postData,$where);
    }
    
    /*
     * updating close time
     */
	    /* 
     * function to find the start time  
     */
    public function fnupdatestudentexamdetails($studentid,$endtime,$submittedby)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Endtime'] = $endtime;
    	$larrformData1['Submittedby'] = $submittedby;	
		 $where = "IDApplication = '".$studentid."'"; 	
		 $db->update('tbl_studentstartexamdetails',$larrformData1,$where);
    }
	}