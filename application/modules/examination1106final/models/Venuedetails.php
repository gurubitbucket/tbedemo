<?php
class Examination_Model_Venuedetails extends Zend_Db_Table {
	

	public function fngetallvenuesdetails($fromdate,$todate)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
										  ->join(array("b" => "tbl_center"),'a.idvenue=b.idcenter',array("key"=>"b.idcenter","value"=>"b.centername"))
										  ->where("a.date >=?",$fromdate)
										  ->where("a.date <=?",$todate)
										  ->where("a.Active=1")
										  ->where("a.centeractive=1")
										  ->Order("b.centername")
										  ->group("b.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fngetvenuedetails($larrformdata)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
										  ->join(array("b" => "tbl_managesession"),'a.idsession=b.idmangesession',array("b.*"))
										  ->where("a.date >=?",$larrformdata['FromDate'])
										  ->where("a.date <=?",$larrformdata['ToDate'])
										  ->where("a.idvenue=?",$larrformdata['Venue'])
										  ->where("a.AllotedSeats>0")
										  ->where("a.centeractive=1")
										  ->Order("a.date")
										  ->Order("b.idmangesession");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fngetemptyvenuedetails($larrformdata)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
										 ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
										 ->join(array("b" => "tbl_managesession"),'a.idsession=b.idmangesession',array("b.*"))
										 ->where("a.date >=?",$larrformdata['FromDate'])
										 ->where("a.date <=?",$larrformdata['ToDate'])
										 ->where("a.idvenue=?",$larrformdata['Venue'])
										 ->where("a.AllotedSeats=0")
										 ->where("a.centeractive=1")
										  ->group("a.date")
										 ->Order("a.date")
										 ->Order("b.idmangesession");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetProgramappliedcnt($date,$venue)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("c" => "tbl_studentapplication"),array("c.Program","c.*"))
										   ->join(array("d" => "tbl_programmaster"),'d.idprogrammaster=c.Program',array("d.*"))
										    ->join(array("b" => "tbl_managesession"),'c.Examsession=b.idmangesession',array("b.*"))
										  ->where("c.Examvenue=?",$venue)
										  //->where("c.Examsession=?",$session)
										  ->where("c.DateTime=?",$date)
										   ->where("c.Payment=1");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		
	}
	
	
	public function fnGetprogramsforsession($venue,$date,$session)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("c" => "tbl_studentapplication"),array("count(c.idapplication)"))
										  ->join(array("d" => "tbl_programmaster"),'d.idprogrammaster=c.Program',array("d.*"))
										  ->where("c.Examvenue=?",$venue)
										  ->where("c.DateTime=?",$date)
										   //->where("c.Payment=1")
										  ->where("c.Examsession=?",$session)
										  ->group("c.Program")
										  ;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetpaidstudentsforprograms($venue,$date,$session)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("c" => "tbl_studentapplication"),array("count(c.idapplication) as Registered"))
										  ->join(array("d" => "tbl_programmaster"),'d.idprogrammaster=c.Program',array(""))
										  ->where("c.Examvenue=?",$venue)
										  ->where("c.DateTime=?",$date)
										   ->where("c.Payment=1")
										  ->where("c.Examsession=?",$session)
										  ->group("c.Program")
										  ;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	

}
