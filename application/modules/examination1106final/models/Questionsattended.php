<?php
	class Examination_Model_Questionsattended extends Zend_Db_Table {


     

     public function fngetprogdetails($larrformData)
     {
     	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_programmaster"),array("a.*"))
							 			 ->join(array("b" => "tbl_batchmaster"),'a.IdProgrammaster=b.IdProgrammaster',array("b.*"))
							 			 ->join(array("t" => "tbl_tosmaster"),'b.IdBatch=t.IdBatch',array("t.*"))
							 			 ->join(array("c" => "tbl_batchdetail"),'b.IdBatch=c.IdBatch',array("c.*"))	
							 			 ->join(array("d" => "tbl_questions"),'c.IdPart=d.QuestionGroup',array("d.*"))							 			 							 			 
                                         ->where("t.Active=1")
                                         ->where("d.Active=1")
                                         ->where("a.IdProgrammaster=?",$larrformData['Program'])
                                         ->where("d.Active=1")
                                         ->group("d.idquestions");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     public function fngetprog()
     {
     			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("b" => "tbl_programmaster"),array("key"=>"b.IdProgrammaster","value"=>"b.ProgramName"));
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
	 public function fnGetQtnsapperedinexam($Questionid,$maxids){

 		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 
 		 $select = "
 		 SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '$Questionid,%'
Union 
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid' 
Union
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid,%'
 		 
 		 ";

		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		
		
/*		public function fnGetTotalattendedstudents($prog,$examdate)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		  $select = "SELECT b.idapplication 
FROM tbl_studentapplication AS b
WHERE b.Program =$prog
AND b.DateTime =  '$examdate' and b.payment=1 and b.pass in(1,2)";
 		 return	$result = $lobjDbAdpt->fetchAll($select);	
 		
		}*/
		
		
		
		public function fnmaxids($formdata)
		{
			$prog = $formdata['Program'];
			$examdate = $formdata['FromDate'];
			//$todate = '2012-06-12';
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		  $select = "SELECT MAX(idquestionsetforstudents) as Appeared
FROM  `tbl_questionsetforstudents` where idapplication in (		  
 		  SELECT b.idapplication 
						FROM tbl_studentapplication AS b
						WHERE b.Program =$prog
						AND b.DateTime ='$examdate'
					AND b.payment=1 and b.pass in(1,2)) group by idapplication";
 		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
   
	}