<?php
class BatchReg_Model_Companymaster extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_companies';
	

 public function fnaddcompany($larrformdata)
 {
 //	print_r($larrformdata);die();
 	$this->insert($larrformdata);
 }
	
 /*
  * function to fetch all the details of the takaful operator
  */
public function fngetcompanymaster()
{
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
						 ->from(array("a"=>"tbl_companies"),array("a.*"));
       $result = $db->fetchAll($lstrSelect);
       return $result;						 
}

/*
 * function to fetcha ll the details based on the id
 */
public function fnGetCompanyDetails($intidtakafuloperator)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_companies"),array("a.*"))
					->where("a.IdCompany=?",$intidtakafuloperator);
	$result = $db->fetchRow($lstrSelect);
	return $result;
}

/*
 * function to update the takaful operator
 */
public function fnupdatecompanymaster($formData,$lintidtakafuloperator)
{
	$db =Zend_Db_Table::getDefaultAdapter();
	$table = "tbl_companies";
	$where['IdCompany = ? '] = $lintidtakafuloperator;
	$db->update($table,$formData,$where);
				
}

/*
 * function to search the takaful operator
 */
public function fnSearchTakafulOperator($post)
{


	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_companies"),array("a.*"))
					->where('a.CompanyName like  ? "%"',$post['field3']);
	$result = $db->fetchAll($lstrSelect);
	return $result;
}
function fnGenerateCode($lintIdRefundMaster,$stateid){	
	//$accCode="";
			$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $db->select()
				->  from('tbl_config')
				-> where('idConfig  = 1');				 
		$result = 	$db->fetchRow($select);		
		$sepr	=	$result['Separator'];
		$str	=	"RegField";
		for($i=1;$i<=3;$i++){
			$check = $result[$str.$i];
			//echo $check;die();
			switch ($check){
				case 'year':
				  $code	= date('Y');
				  break;
				case 'state':
					 $select =  $db->select()
					 		 -> from(array('a'=>'tbl_state'),array("left(a.StateName,3) as StateName"))
					 		 ->	where('idState = ?',$stateid);  				 
					$resultstate = $db->fetchRow($select);	
				  $code	= $resultstate['StateName'];
				  break;
				case 'company':
					 $select =  $db->select()
					 		 -> from('tbl_companies')
					 		 ->	where('IdCompany = ?',$lintIdRefundMaster);  				 
					$resultCollage = $db->fetchRow($select);		
					$code		   = $resultCollage['ShortName'];
				  break;
				default:
				  break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}
		
				$accCode	.=	$sepr.$lintIdRefundMaster;
	 	$data = array('RegistrationNo' => $accCode);
		$where['IdCompany = ? ']= $lintIdRefundMaster;		
		return $db->update('tbl_companies', $data, $where);	
}

}