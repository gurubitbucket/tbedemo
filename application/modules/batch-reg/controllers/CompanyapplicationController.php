<?php
//error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class BatchReg_CompanyapplicationController extends Zend_Controller_Action { //Controller for the User Module
		private $gstrsessionbatch;//Global Session Name
	public function init() { //initialization function
		$this->gobjsessionstudent->idCompany = 1;
		echo $this->gstrsessionbatch->idCompany;
		$this->_helper->layout()->setLayout('/reg/usty1');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjstudentmodel = new BatchReg_Model_Companyapplication(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object		
		$this->lobjstudentForm = new BatchReg_Form_Companyapplication (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	public function indexAction() {
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gobjsessionstudent->idCompany); //get user details
 		$this->view->lobjstudentForm = $this->lobjstudentForm;
  		$this->view->companyDetails =  $larrresult;
  		if ($this->_request->isPost () && $this->_request->getPost ('Next')){  			
  			if($this->_request->getPost ('newapp') == 1) {
  				echo "<script>window.location = '".$this->view->baseUrl()."/batch-reg/companyapplication/newapplication";
  			}
  			else {
  				//echo "<script>parent.location = '".$this->view->baseUrl()."/batch-reg/companyapplication/index";
  			}
  			
  		}
	}
	public function newapplicationAction() { // action for search and view
	
		$lobjform=$this->view->lobsearchform = new App_Form_Search ();//send the lobjuserForm object to the view
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gobjsessionstudent->idCompany); //get user details
 		$this->view->lobjstudentForm = $this->lobjstudentForm;
  		$this->view->companyDetails =  $larrresult;
  		
  		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();		
		$this->lobjstudentForm->idPrograms->addMultiOption('0','Select');
		$this->lobjstudentForm->idPrograms->addMultiOptions($larrbatchresult);
		$this->lobjstudentForm->idCompany->setValue($this->gobjsessionstudent->idCompany);		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save')){
			$larrformData = $this->_request->getPost ();			
			$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );
			$lastInsId = $this->lobjstudentmodel->fnInsertPaymentdetails($larrformData);	
			$this->_redirect($this->view->url(array('module'=>'batch-reg' ,'controller'=>'companyapplication', 'action'=>'confirmpayment','insertedId'=>$lastInsId),'default',true));
			exit;			
		}
	}
public function confirmpaymentAction(){
		$lintinsertedId = $this->_getParam('insertedId');
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gobjsessionstudent->idCompany);		
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);	
		//echo "<pre>";
		//print_r($larrPaymentDetails);	
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $lintinsertedId;
		//Get SMTP Mailing Server Setting Details
		$postArray = $this->_request->getPost ();
		
		if($postArray){			
			print_r($postArray);exit;	
				if($postArray['payment_status'] = 'Completed'){
					$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					$postArray['Regid']  = substr($postArray['txn_id'], 1, 6).rand(1000, 9999).substr($postArray['txn_id'], 5, 9);
					$this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$lintidstudent);		
					$this->view->mess = "Payment Completed Sucessfully";
					
						$larrSMTPDetails  = $this->lobjstudentmodel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
					
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
						
						
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
							try{
								$lobjProtocol->connect();
						   		$lobjProtocol->helo($lstrSMTPUsername);
								$lobjTransport->setConnection($lobjProtocol);
						 	
								//Intialize Zend Mailing Object
								$lobjMail = new Zend_Mail();
						
								$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
								$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
								$lobjMail->addHeader('MIME-Version', '1.0');
								$lobjMail->setSubject($lstrEmailTemplateSubject);
						
								for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
									if($larrEmailIds[$lintI] != ""){
										$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);	
																
										//replace tags with values
										$Link = "<a href='".$this->Url."/reg/registration'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Candidate]",$lstrStudentName,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$postArray['payment_gross'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$postArray['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										$lobjMail->setBodyHtml($lstrEmailTemplateBody);
								
										try {
											$lobjMail->send($lobjTransport);
										} catch (Exception $e) {
											$lstrMsg = "error";      				
										}	
										$lobjMail->clearRecipients();
										$this->view->mess .= ". Login Details have been sent to user Email";
										unset($larrEmailIds[$lintI]);
									}
								}
							}catch(Exception $e){
								$lstrMsg = "error";
							}
						}else{
							$lstrMsg = "No Template Found";
						}
				}
				else {
					$this->view->mess = "Payment Failed";
				}				
			}		
	}
	public function newstudentapplicationAction() {exit; //Action for creating the new user
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		
	/*	$larrbatchresult = $this->lobjstudentmodel->fnGetBatchName();
		$this->lobjstudentForm->IdBatch->addMultiOptions($larrbatchresult);*/
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();
	
		$this->lobjstudentForm->idPrograms->addMultiOptions($larrbatchresult);
		//$lintidbatch = 1;//$this->_getParam('idbatch');fnTakafuloperator
		
	
		$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjstudentForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		$this->view->lobjstudentForm->UpdDate->setValue ( $ldtsystemDate );
		
		 $auth = Zend_Auth::getInstance();
		$this->view->lobjstudentForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		

		$lobjcountry = $this->lobjusermodel->fnGetCountryList();
	/*	$this->lobjstudentForm->PermCountry->addMultiOptions($lobjcountry);
		$this->lobjstudentForm->CorrsCountry->addMultiOptions($lobjcountry);*/


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			//print_r($larrformData);
			//die();
			unset($larrformData['Save']);
			unset($larrformData['Close']);
				$result = $this->lobjstudentmodel->fnAddStudent($larrformData); //instance for adding the lobjuserForm values to DB
				$this->_redirect($this->view->url(array('module'=>'stu-app' ,'controller'=>'studentapplication', 'action'=>'confirmpayment','insertedId'=>$result),'default',true));
		
		}

	}
	
	public function studentapplicationeditAction() { //Action for the updation and view of the user details
		$lintidstudent = (int)$this->_getParam('id');
		
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetails($lintidstudent); //

		
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();
		$this->lobjstudentForm->Program->addMultiOptions($larrbatchresult);
		//$lintidbatch = 1;//$this->_getParam('idbatch');
		
		$this->view->lobjstudentForm->UpdDate->setValue($ldtsystemDate);
		
		 $auth = Zend_Auth::getInstance();
		$this->view->lobjstudentForm->UpdUser->setValue($auth->getIdentity()->iduser);
		
		
		$this->view->lobjstudentForm->IDApplication->setValue($lintidstudent);
		//$lobjcountry = $this->lobjusermodel->fnGetCountryList();
	//	$this->lobjstudentForm->PermCountry->addMultiOptions($lobjcountry);
	//	$this->lobjstudentForm->CorrsCountry->addMultiOptions($lobjcountry);
		
/*		$arrhomephone = explode("-",$larrresult ['HomePhone']);
		$arrcellphone = explode("-",$larrresult ['CellPhone']);
		unset($larrresult['HomePhone']);
		unset($larrresult['CellPhone']);*/
	//print_r($larrresult);die();
		$this->lobjstudentForm->populate($larrresult);
		
			/*$this->view->lobjstudentForm->homecountrycode->setValue ( $arrhomephone [0] );
			$this->view->lobjstudentForm->homestatecode->setValue ( $arrhomephone [1] );
			$this->view->lobjstudentForm->HomePhone->setValue ( $arrhomephone [2] );
		
			
			$this->view->lobjstudentForm->countrycode->setValue ($arrcellphone[0]);
			$this->view->lobjstudentForm->statecode->setValue ($arrcellphone[1]);
			$this->view->lobjstudentForm->CellPhone->setValue ($arrcellphone[2]);*/
		//$this->view->permcountry = $larrresult['PermCountry'];
		//$this->view->CorrsCountry = $larrresult['PermCountry'];
			$lobjCommonModel = new App_Model_Common();
			//Get States List
			//$larrpermCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larrresult['PermCountry']);
			//$this->lobjstudentForm->PermState->addMultiOptions($larrpermCountyrStatesList);
			
			//$larrCorrsCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larrresult['CorrsCountry']);
			//$this->lobjstudentForm->CorrsState->addMultiOptions($larrCorrsCountyrStatesList);
			
					$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjstudentForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		
		
			$idunit = 1;
		$larrinitconfigdetilas = $this->lobjstudentmodel->fnGetInitialConfigDetails($idunit);
		$days = $larrinitconfigdetilas['ClosingBatch'];
		$datetocmp = date('Y-m-d', strtotime($days.'days'));
			
			$larrvenue = $this->lobjstudentmodel->fnGetbatch($larrresult['Program'],$datetocmp);
			$this->lobjstudentForm->IdBatch->addMultiOptions($larrvenue);			
			
		
			$larrvenue = $this->lobjstudentmodel->fnGetscheduler($larrresult['IdBatch']);
			$this->lobjstudentForm->idschedulermaster->addMultiOptions($larrvenue);
			
		    $larrvenue = $this->lobjstudentmodel->fnGetVenueName($larrresult['idschedulermaster']);
			$this->lobjstudentForm->Venue->addMultiOptions($larrvenue);
			
			
			$larrvenuetime = $this->lobjstudentmodel->fnGetVenueTime($larrresult['Venue']);
			$this->lobjstudentForm->VenueTime->addMultiOptions($larrvenuetime);
			
			$time = $this->lobjstudentmodel->fnGetTimings($larrresult['VenueTime'],$larrresult['Venue']);

			$this->lobjstudentForm->DateTime->addMultiOptions($time);
			
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
				unset($larrformData['Save']);
				unset($larrformData['Close']);
				if ($this->lobjstudentForm->isValid($larrformData)) {
						
					$lintstudentid = $larrformData['IDApplication'];
					
					unset($larrformData['IDApplication']);
					$this->lobjstudentmodel->updatestudent($lintstudentid,$larrformData);
					$this->_redirect($this->view->url(array('module'=>'stu-app' ,'controller'=>'studentapplication', 'action'=>'index'),'default',true));
		
			}
		}
		$this->view->lobjuserForm = $this->lobjuserForm;
	}

	//Action To Get List Of States From Country Id
	public function fngetvenueAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidscheduler = $this->_getParam('idscheduler');
		
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetVenueName($lintidscheduler);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrbatchresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	//Action To Get List Of States From Country Id
	public function fngetvenuetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidvenue = $this->_getParam('idvenue');
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetVenueTime($lintidvenue);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}
	
public function fngetbatchAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		
		$idunit = 1;
		$larrinitconfigdetilas = $this->lobjstudentmodel->fnGetInitialConfigDetails($idunit);
		$days = $larrinitconfigdetilas['ClosingBatch'];
		$datetocmp = date('Y-m-d', strtotime($days.'days'));

		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetbatch($lintidprog,$datetocmp);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}
	
	
	
public function fngetschedulerdetailsAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidbatch = $this->_getParam('idbatch');
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetscheduler($lintidbatch);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	
	
	
	public function fngetdatetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('date');
		$lintidvenue = $this->_getParam('idvenue');
		
	
		
		//echo $totalnoofstudents;
		
		/*if($noofseats == $totalnoofstudents)
		{
			echo "<script>alert('Select Other Date')</script>";
			$larrCountryStatesDetails="safsffsfsfsd";
			echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
			die();
		}*/
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetTimingsForDate($lintdate,$lintidvenue);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	/*
	 * functino to check the data and time 
	 */
	public function fngetdatetimevalueAction()
	{
			$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidvenuetime = $this->_getParam('idvenuetime');
		
		$larrresultnoofseats = $this->lobjstudentmodel->fnGetNoOfSeats($lintidvenuetime);
		$noofseats = $larrresultnoofseats['NoofSeats'];
	  // echo $noofseats;
		
		$larrnoofstudents = $this->lobjstudentmodel->fnGetNoofStudents($lintidvenuetime);
		$totalnoofstudents = $larrnoofstudents['count(IDApplication)'];
		// $totalnoofstudents;
		
		if($totalnoofstudents == $noofseats)
		{
			$value = '1';
			$alert =  'This Schedule has been completely filled please select other venue or time';
			$alert =$alert.'*****'.$value;
			echo $alert;
			
		}
		else 
		{
			$value = '0';
			$alert =  'This';
			$alert =$alert.'*****'.$value;
			echo $alert;
		}
		
		
	}
	
	/*
	 * function to find the takaful operator count
	 */
	public function fngettakafulidAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintididtakaful = $this->_getParam('idtakaful');
		$larrresultnoofseats = $this->lobjstudentmodel->fnGetNoOfTakafulOperator($lintididtakaful);
		$takafulseat = $larrresultnoofseats['NumberofSeat'];
		
		$larrresultnoofseatsinstudent = $this->lobjstudentmodel->fnGetNoOfTakafulOperatorForStudent($lintididtakaful);
		$studentoccupied = $larrresultnoofseatsinstudent['count(IDApplication)'];
		
	   if($takafulseat == $studentoccupied)
		{
			$value = '1';
			$alert =  'This Takaful Operator has been completed select other Takaful Operator';
			$alert =$alert.'*****'.$value;
			echo $alert;
			
		}
		else 
		{
			$value = '0';
			$alert =  'This';
			$alert =$alert.'*****'.$value;
			echo $alert;
		}
		
		
		
	}
	/*
	 * funcion for fees structure
	 */
	public function studentapplicationfeesAction()
	{
		$lintidstudent = (int)$this->_getParam('id');
		$larrresult = $this->lobjstudentmodel->fnGetStudentName($lintidstudent); 
		$this->view->studentname = $larrresult[''];
		$this->lobjstudentForm->populate($larrresult);
	}
	
	
	public function fngetprogramamountAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		$larrprogamountresult = $this->lobjstudentmodel->fnGetProgAmount($lintidprog);
		echo $larrprogamountresult['sum(abc.amount)'];	
	}

}