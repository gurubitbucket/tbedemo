<?php
class BatchReg_CompanymasterController extends Zend_Controller_Action {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjinitialconfigmodel;
	private $lobjdeftype;
	private $lobjform;
	private $lobjCommon;
	public function init() {
	
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function fnsetObj(){
		
		$this->lobjcenterModel = new GeneralSetup_Model_DbTable_Center(); //user model object
		$this->lobjTakafuloperatorModel = new BatchReg_Model_Companymaster();
		$this->lobjTakafuloperatorForm = new BatchReg_Form_Companymaster(); 
	  	$this->lobjinitialconfigmodel = new GeneralSetup_Model_DbTable_Initialconfiguration();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
	  	$this->lobjform=new App_Form_Search ();
	  	$this->lobjCommon = new App_Model_Common(); 
			
	}
	
	public function indexAction() {
    	$this->view->title="Company Master";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjTakafuloperatorModel->fngetcompanymaster(); //get user details
		//print_r($larrresult);
		//die();
		
		$lintpagecount = "5";// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->programpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->programpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjTakafuloperatorModel->fnSearchTakafulOperator($this->lobjform->getValues()); //searching the values for the user
				//$this->view->paginator=$larrresult;
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->programpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/batch-reg/companymaster/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'takafuloperator', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newcompanyAction() { //title
    	$this->view->title="Add New Profram";
		$this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm;
		$ldtsystemDate = date('Y-m-d H:i:s');
	
		$this->view->lobjTakafuloperatorForm->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjTakafuloperatorForm->UpdUser->setValue($auth->getIdentity()->iduser);
		
		$lobjcountry = $this->lobjcenterModel->fnGetCountryList();
		$this->lobjTakafuloperatorForm->IdCountry->addMultiOptions($lobjcountry);
		$idconfig=1;
		$larrconresults=$this->lobjinitialconfigmodel->fnGetInitialConfigDetails($idconfig);
		if($larrconresults['RegcodeType']==0)
		{
			$this->view->lobjTakafuloperatorForm->RegistrationNo->setAttrib('required',true);
		}
		else {
			$this->view->lobjTakafuloperatorForm->RegistrationNo->setAttrib ( 'readonly', true );
		}
		
		if ($this->getRequest()->isPost()) {
			$larrformData = $this->getRequest()->getPost();
				unset($larrformData['Save']);
					unset($larrformData['Back']);	
			if($larrformData['Password'] != $larrformData['ConPassword'])
			{
				
				echo '<script language="javascript">alert("New password & retype password is not matching")</script>';
			}
			else
			{    //print_r($larrformData);die();
				unset($larrformData['ConPassword']);
				$larrformData['Password'] = md5($larrformData['Password']); 
				if($larrconresults['RegcodeType']!=0){
				$larrformData['RegistrationNo'] = 0;
				$stateid=$larrformData['IdState'];
				}
				else 
				{
				$larrresults = $this->lobjTakafuloperatorModel->fngetcompanymaster();
				foreach($larrresults as $res)	
				{
					if($larrformData['RegistrationNo']==$res['RegistrationNo'])
					{
						echo "<script>alert('the Register No  already there plz change')</script>";
						die();
						break;
					}
				}
				}
				$larrresult=$this->lobjTakafuloperatorModel->fnaddcompany($larrformData);
				$lintIdRefundMaster = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_companies','IdCompany');
				//print_r($lintIdRefundMaster);die();
			if($larrconresults['RegcodeType']!=0){
				$this->lobjTakafuloperatorModel->fnGenerateCode($lintIdRefundMaster,$stateid);
				}
				
				$this->_redirect( $this->baseUrl . '/batch-reg/companymaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'companymaster', 'action'=>'index'),'default',true));	//redirect	
         }    
		} 
    }
    
	public function editcompanymasterAction(){
		
    	$this->view->title="Edit Company Master";  //title
		$this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm; //send the lobjuniversityForm object to the view
		
		$lobjcountry = $this->lobjcenterModel->fnGetCountryList();
		$this->lobjTakafuloperatorForm->IdCountry->addMultiOptions($lobjcountry);
		
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjTakafuloperatorForm->UpdDate->setValue ( $ldtsystemDate );		
		
    	$idtakafuloperator = $this->_getParam('id');
    	$larrtakafuloperator = $this->lobjTakafuloperatorModel->fnGetCompanyDetails($idtakafuloperator);
    	//print_r($idtakafuloperator);die();
    	$this->view->lobjTakafuloperatorForm->IdCompany->setValue ( $idtakafuloperator );	
    	$this->view->lobjTakafuloperatorForm->Login->setAttrib ( 'readonly', true );
    	$this->view->lobjTakafuloperatorForm->Password->setAttrib ( 'readonly', true );
		$this->view->lobjTakafuloperatorForm->ConPassword->setAttrib ( 'readonly', true );
		$this->view->lobjTakafuloperatorForm->RegistrationNo->setAttrib ( 'readonly', true );
    	$lobjCommonModel = new App_Model_Common();
			//Get States List
			$larrpermCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larrtakafuloperator['IdCountry']);
			$this->lobjTakafuloperatorForm->IdState->addMultiOptions($larrpermCountyrStatesList);

			
    	$this->lobjTakafuloperatorForm->populate($larrtakafuloperator);	
    	//$this->lobjTakafuloperatorForm->Password->setValue ( $larrtakafuloperator['Password'] );		
		$this->lobjTakafuloperatorForm->ConPassword->setValue ( $larrtakafuloperator['Password']);
		$this->view->lobjTakafuloperatorForm->Password->renderPassword = true;
			$this->view->lobjTakafuloperatorForm->ConPassword->renderPassword = true;
    	if ($this->getRequest()->isPost()) {
    		$larrformData = $this->getRequest()->getPost();
	    	if ($this->lobjTakafuloperatorForm->isValid($larrformData)) {
	    		unset($larrformData['Save']);
					unset($larrformData['Back']);
						//print_r($larrformData);die();
			if($larrformData['Password'] != $larrformData['ConPassword'])
			{
				
				echo '<script language="javascript">alert("New password & retype password is not matching")</script>';
			}
			else
			{  
				unset($larrformData['ConPassword']);
				//print_r($larrformData);die();
				//$larrformData['Password'] = md5($larrformData['Password']); 
	   			$lintidtakafuloperator = $larrformData['IdCompany'];
				$this->lobjTakafuloperatorModel->fnupdatecompanymaster($larrformData,$idtakafuloperator);//update university
				$this->_redirect( $this->baseUrl . '/batch-reg/companymaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'companymaster', 'action'=>'index'),'default',true));
			}
	    	}
    	}
    }
}