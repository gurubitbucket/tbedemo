<?php
class BatchReg_Form_Companymaster extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$IdCompany= new Zend_Form_Element_Hidden('IdCompany');
        $IdCompany->removeDecorator("DtDdWrapper");
        $IdCompany->removeDecorator("Label");
        $IdCompany->removeDecorator('HtmlTag');
        
        $TakafulName = new Zend_Form_Element_Text('CompanyName');	
		$TakafulName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $TakafulName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $TakafulShortName = new Zend_Form_Element_Text('ShortName');	
		$TakafulShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $TakafulShortName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');   

 		$paddr1 = new Zend_Form_Element_Text('RegistrationNo');
		$paddr1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
                 //->setAttrib('required',"true")       			 
        		//->setAttrib('readonly','true')       
        $paddr1	->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                   
        $paddr2 = new Zend_Form_Element_Text('Address');
		$paddr2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $paddr2->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        
        $city = new Zend_Form_Element_Text('City');
		$city->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $city->setAttrib('maxlength','20')     
               ->setAttrib('required',"true") 		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       
        
        $country = new Zend_Dojo_Form_Element_FilteringSelect('IdCountry');
        $country->removeDecorator("DtDdWrapper");
        $country->setAttrib('required',"true") ;
        $country->removeDecorator("Label");
        $country->removeDecorator('HtmlTag');
        $country->setAttrib('OnChange', 'fnGetCountryStateList');
        $country->setRegisterInArrayValidator(false);
		$country->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
       
        $state = new Zend_Dojo_Form_Element_FilteringSelect('IdState');
        $state->removeDecorator("DtDdWrapper");
        $state->setAttrib('required',"true") ;
        $state->removeDecorator("Label");
        $state->removeDecorator('HtmlTag');
        $state->setRegisterInArrayValidator(false);
		$state->setAttrib('dojoType',"dijit.form.FilteringSelect");
              
        
        $zipCode = new Zend_Form_Element_Text('Postcode');
		$zipCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $zipCode->setAttrib('maxlength','20');   
        $zipCode->removeDecorator("DtDdWrapper");
        $zipCode->removeDecorator("Label");
        $zipCode->removeDecorator('HtmlTag');
        
 		$workPhone = new Zend_Form_Element_Text('Phone1',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Work Phone No."));
		$workPhone->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workPhone->setAttrib('maxlength','20');   
       // $workPhone->setAttrib('style','width:93px');  
        $workPhone->removeDecorator("DtDdWrapper");
        $workPhone->removeDecorator("Label");
        $workPhone->removeDecorator('HtmlTag');

        $Phone2 = new Zend_Form_Element_Text('Phone2',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Work Phone No."));
		$Phone2->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Phone2->setAttrib('maxlength','20');   
       // $Phone2->setAttrib('style','width:93px');  
        $Phone2->removeDecorator("DtDdWrapper");
        $Phone2->removeDecorator("Label");
        $Phone2->removeDecorator('HtmlTag'); 

        $email = new Zend_Form_Element_Text('Email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $email->setAttrib('required',"true")  			 
        		->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');  

        		
        $altemail = new Zend_Form_Element_Text('AltEmail',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$altemail->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $altemail->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');        
        		
        		
        		
       	$Fax = new Zend_Form_Element_Text('Fax');
		$Fax->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        				
        				
        				
        $Contactperson = new Zend_Form_Element_Text('ContactPerson');	
		$Contactperson->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Contactperson->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        				
       	$Login = new Zend_Form_Element_Text('Login');
		$Login->setAttrib('dojoType',"dijit.form.TextBox")   
		               ->setAttrib('required',"true")  			 
        				->setAttrib('maxlength','50')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$Password = new Zend_Form_Element_Password('Password',array('regExp'=>"[\w]+",'invalidMessage'=>"No Spaces Allowed"));
		$Password->setAttrib('dojoType',"dijit.form.TextBox") 
		                ->setAttrib('required',"true")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$ConPassword = new Zend_Form_Element_Password('ConPassword',array('regExp'=>"[\w]+",'invalidMessage'=>"No Spaces Allowed"));
		$ConPassword->setAttrib('dojoType',"dijit.form.TextBox")   
		               ->setAttrib('required',"true")  			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

        				
        
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');         		
    
        $this->addElements(array($IdCompany,$TakafulName,$TakafulShortName,
        							$paddr1,$paddr2,$city,$altemail,$country,$state,$zipCode,$workPhone,$Phone2,			
        	$Fax,$Login,$Password,$ConPassword,$email,
        						$Contactperson,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $Back));

    }
}