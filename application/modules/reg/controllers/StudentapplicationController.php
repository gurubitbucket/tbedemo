<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Reg_StudentapplicationController extends Zend_Controller_Action { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;
	
	public function init() { //initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjstudentmodel = new StuApp_Model_Studentapplication(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
			//$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
		$this->lobjstudentForm = new StuApp_Form_Studentapplication (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

public function indexAction() { // action for search and view
		$lobjform=$this->view->lobsearchform = $this->lobjform; //send the lobjuserForm object to the view
		$larrresult = $this->lobjstudentmodel->fngetStudentDetails(); //get user details

		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionstudent->userpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->userpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjstudentmodel->fnSearchStudent($lobjform->getValues()); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->userpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/reg/studentapplication/index');
			//$this->_redirect($this->view->url(array('module'=>'stu-app' ,'controller'=>'studentapplication', 'action'=>'index'),'default',true));
		}


	}

	public function newstudentapplicationAction() { //Action for creating the new user
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		
	/*	$larrbatchresult = $this->lobjstudentmodel->fnGetBatchName();
		$this->lobjstudentForm->IdBatch->addMultiOptions($larrbatchresult);*/
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();
		$this->lobjstudentForm->Program->addMultiOptions($larrbatchresult);
		//$lintidbatch = 1;//$this->_getParam('idbatch');fnTakafuloperator
		
	
		$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjstudentForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		$this->view->lobjstudentForm->UpdDate->setValue ( $ldtsystemDate );
		
		 $auth = Zend_Auth::getInstance();
		$this->view->lobjstudentForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		

		$lobjcountry = $this->lobjusermodel->fnGetCountryList();
	/*	$this->lobjstudentForm->PermCountry->addMultiOptions($lobjcountry);
		$this->lobjstudentForm->CorrsCountry->addMultiOptions($lobjcountry);*/


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			//print_r($larrformData);
			//die();
			unset($larrformData['Save']);
			unset($larrformData['Close']);
				$result = $this->lobjstudentmodel->fnAddStudent($larrformData); //instance for adding the lobjuserForm values to DB
				$this->_redirect( $this->baseUrl . '/reg/studentapplication/index');
				//$this->_redirect($this->view->url(array('module'=>'stu-app' ,'controller'=>'studentapplication', 'action'=>'confirmpayment','insertedId'=>$result),'default',true));
		
		}

	}
	public function confirmpaymentAction(){
		$lintidstudent = $this->_getParam('insertedId');
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetails($lintidstudent);		
		$this->view->data = $larrresult;
		$this->view->idstudent = $lintidstudent;
		 $auth = Zend_Auth::getInstance();
		
			$postArray = $this->_request->getPost ();			
			
			if($postArray){								
				$postArray = $this->_request->getPost ();	
				if($postArray['payment_status'] = 'Completed'){
					$postArray['UpdUser']= $auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );;
					$this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$lintidstudent);		
					$this->view->mess = "Payment Completed Sucessfully";
				}
				else {
					$this->view->mess = "Payment Failed";
				}				
			}
		
	}
	public function studentapplicationeditAction() { //Action for the updation and view of the user details
		$lintidstudent = (int)$this->_getParam('id');
		
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetails($lintidstudent); //

		
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();
		$this->lobjstudentForm->Program->addMultiOptions($larrbatchresult);
		//$lintidbatch = 1;//$this->_getParam('idbatch');
		
		$this->view->lobjstudentForm->UpdDate->setValue($ldtsystemDate);
		
		 $auth = Zend_Auth::getInstance();
		$this->view->lobjstudentForm->UpdUser->setValue($auth->getIdentity()->iduser);
		
		
		$this->view->lobjstudentForm->IDApplication->setValue($lintidstudent);
		//$lobjcountry = $this->lobjusermodel->fnGetCountryList();
	//	$this->lobjstudentForm->PermCountry->addMultiOptions($lobjcountry);
	//	$this->lobjstudentForm->CorrsCountry->addMultiOptions($lobjcountry);
		
/*		$arrhomephone = explode("-",$larrresult ['HomePhone']);
		$arrcellphone = explode("-",$larrresult ['CellPhone']);
		unset($larrresult['HomePhone']);
		unset($larrresult['CellPhone']);*/
	//print_r($larrresult);die();
		$this->lobjstudentForm->populate($larrresult);
		
			/*$this->view->lobjstudentForm->homecountrycode->setValue ( $arrhomephone [0] );
			$this->view->lobjstudentForm->homestatecode->setValue ( $arrhomephone [1] );
			$this->view->lobjstudentForm->HomePhone->setValue ( $arrhomephone [2] );
		
			
			$this->view->lobjstudentForm->countrycode->setValue ($arrcellphone[0]);
			$this->view->lobjstudentForm->statecode->setValue ($arrcellphone[1]);
			$this->view->lobjstudentForm->CellPhone->setValue ($arrcellphone[2]);*/
		//$this->view->permcountry = $larrresult['PermCountry'];
		//$this->view->CorrsCountry = $larrresult['PermCountry'];
			$lobjCommonModel = new App_Model_Common();
			//Get States List
			//$larrpermCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larrresult['PermCountry']);
			//$this->lobjstudentForm->PermState->addMultiOptions($larrpermCountyrStatesList);
			
			//$larrCorrsCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larrresult['CorrsCountry']);
			//$this->lobjstudentForm->CorrsState->addMultiOptions($larrCorrsCountyrStatesList);
			
					$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjstudentForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		
		
			$idunit = 1;
		$larrinitconfigdetilas = $this->lobjstudentmodel->fnGetInitialConfigDetails($idunit);
		$days = $larrinitconfigdetilas['ClosingBatch'];
		$datetocmp = date('Y-m-d', strtotime($days.'days'));
			
			$larrvenue = $this->lobjstudentmodel->fnGetbatch($larrresult['Program'],$datetocmp);
			$this->lobjstudentForm->IdBatch->addMultiOptions($larrvenue);			
			
		
			$larrvenue = $this->lobjstudentmodel->fnGetscheduler($larrresult['IdBatch']);
			$this->lobjstudentForm->idschedulermaster->addMultiOptions($larrvenue);
			
		    $larrvenue = $this->lobjstudentmodel->fnGetVenueName($larrresult['idschedulermaster']);
			$this->lobjstudentForm->Venue->addMultiOptions($larrvenue);
			
			
			$larrvenuetime = $this->lobjstudentmodel->fnGetVenueTime($larrresult['Venue']);
			$this->lobjstudentForm->VenueTime->addMultiOptions($larrvenuetime);
			
			$time = $this->lobjstudentmodel->fnGetTimings($larrresult['VenueTime'],$larrresult['Venue']);

			$this->lobjstudentForm->DateTime->addMultiOptions($time);
			
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
				unset($larrformData['Save']);
				unset($larrformData['Close']);
				if ($this->lobjstudentForm->isValid($larrformData)) {
						
					$lintstudentid = $larrformData['IDApplication'];
					
					unset($larrformData['IDApplication']);
					$this->lobjstudentmodel->updatestudent($lintstudentid,$larrformData);
					//$this->_redirect($this->view->url(array('module'=>'stu-app' ,'controller'=>'studentapplication', 'action'=>'index'),'default',true));
		            $this->_redirect( $this->baseUrl . '/reg/studentapplication/index');
			}
		}
		$this->view->lobjuserForm = $this->lobjuserForm;
	}

	//Action To Get List Of States From Country Id
	public function fngetvenueAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidscheduler = $this->_getParam('idscheduler');
		
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetVenueName($lintidscheduler);
		$larrCountryStatesDetails = $this->fnResetArrayFromValuesToNames($larrbatchresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	//Action To Get List Of States From Country Id
	public function fngetvenuetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidvenue = $this->_getParam('idvenue');
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetVenueTime($lintidvenue);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}
	
public function fngetbatchAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		
		$idunit = 1;
		$larrinitconfigdetilas = $this->lobjstudentmodel->fnGetInitialConfigDetails($idunit);
		$days = $larrinitconfigdetilas['ClosingBatch'];
		$datetocmp = date('Y-m-d', strtotime($days.'days'));

		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetbatch($lintidprog,$datetocmp);
		$larrCountryStatesDetails = $this->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}
	
	
	
public function fngetschedulerdetailsAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidbatch = $this->_getParam('idbatch');
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetscheduler($lintidbatch);
		$larrCountryStatesDetails = $this->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	
	
	
	public function fngetdatetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('date');
		$lintidvenue = $this->_getParam('idvenue');
		
	
		
		//echo $totalnoofstudents;
		
		/*if($noofseats == $totalnoofstudents)
		{
			echo "<script>alert('Select Other Date')</script>";
			$larrCountryStatesDetails="safsffsfsfsd";
			echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
			die();
		}*/
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetTimingsForDate($lintdate,$lintidvenue);
		$larrCountryStatesDetails = $this->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	/*
	 * functino to check the data and time 
	 */
	public function fngetdatetimevalueAction()
	{
			$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidvenuetime = $this->_getParam('idvenuetime');
		
		$larrresultnoofseats = $this->lobjstudentmodel->fnGetNoOfSeats($lintidvenuetime);
		$noofseats = $larrresultnoofseats['NoofSeats'];
	  // echo $noofseats;
		
		$larrnoofstudents = $this->lobjstudentmodel->fnGetNoofStudents($lintidvenuetime);
		$totalnoofstudents = $larrnoofstudents['count(IDApplication)'];
		// $totalnoofstudents;
		
		if($totalnoofstudents == $noofseats)
		{
			$value = '1';
			$alert =  'This Schedule has been completely filled please select other venue or time';
			$alert =$alert.'*****'.$value;
			echo $alert;
			
		}
		else 
		{
			$value = '0';
			$alert =  'This';
			$alert =$alert.'*****'.$value;
			echo $alert;
		}
		
		
	}
	
	/*
	 * function to find the takaful operator count
	 */
	public function fngettakafulidAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintididtakaful = $this->_getParam('idtakaful');
		$larrresultnoofseats = $this->lobjstudentmodel->fnGetNoOfTakafulOperator($lintididtakaful);
		$takafulseat = $larrresultnoofseats['NumberofSeat'];
		
		$larrresultnoofseatsinstudent = $this->lobjstudentmodel->fnGetNoOfTakafulOperatorForStudent($lintididtakaful);
		$studentoccupied = $larrresultnoofseatsinstudent['count(IDApplication)'];
		
	   if($takafulseat == $studentoccupied)
		{
			$value = '1';
			$alert =  'This Takaful Operator has been completed select other Takaful Operator';
			$alert =$alert.'*****'.$value;
			echo $alert;
			
		}
		else 
		{
			$value = '0';
			$alert =  'This';
			$alert =$alert.'*****'.$value;
			echo $alert;
		}
		
		
		
	}
	/*
	 * funcion for fees structure
	 */
	public function studentapplicationfeesAction()
	{
		$lintidstudent = (int)$this->_getParam('id');
		$larrresult = $this->lobjstudentmodel->fnGetStudentName($lintidstudent); 
		$this->view->studentname = $larrresult[''];
		$this->lobjstudentForm->populate($larrresult);
	}
	
	
	public function fngetprogramamountAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		$larrprogamountresult = $this->lobjstudentmodel->fnGetProgAmount($lintidprog);
		echo $larrprogamountresult['sum(abc.amount)'];	
	}
	
	public function fnResetArrayFromValuesToNames($OrginialArray){
			$larrNewArr = array();
			$OrgnArray = @array_values($OrginialArray);
			for($lintI=0;$lintI<count($OrgnArray);$lintI++){
				$larrNewArr[$lintI]["name"] = $OrgnArray[$lintI]["value"];
				$larrNewArr[$lintI]["key"] = $OrgnArray[$lintI]["key"];
			}
			return $larrNewArr;
		}

}