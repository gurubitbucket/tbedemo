<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class StuApp_CenterconfirmationController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;
	
	public function init() { //initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjstudentmodel = new StuApp_Model_Centerconfirmation(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
			//$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
		$this->lobjstudentForm = new StuApp_Form_Studentapplicationapp (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

public function indexAction() { // action for search and view
		$lobjform=$this->view->lobsearchform = $this->lobjform; //send the lobjuserForm object to the view
		
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		//$larrresult = $this->lobjstudentmodel->fngetStudentDetails(); //get user details

		$this->lobjintialConfigmodel = new GeneralSetup_Model_DbTable_Initialconfiguration();		
		$InitialConfigDetails = $this->lobjintialConfigmodel->fnGetInitialConfigDetails(1);	
		$larrBatch = $this->lobjstudentmodel->fnGetBatchName($InitialConfigDetails['ClosingBatch']); //searching the values for the user
		$this->view->lobsearchform->field1->addMultioption('0','Select');
		$this->view->lobsearchform->field1->addMultioptions($larrBatch);
		$this->view->lobsearchform->field1->setAttrib('onChange','fngetschedulerdetails(this.value);');
		$this->view->lobsearchform->field1->setAttrib('required',"true");
		$this->view->lobsearchform->field1->setRegisterInArrayValidator(false);
		$this->view->lobsearchform->field5->setAttrib('onChange','fngetvenuedetails(this.value);') ;
		$this->view->lobsearchform->field5->setAttrib('required',"true");
		$this->view->lobsearchform->field5->setRegisterInArrayValidator(false);
		$this->view->lobsearchform->field8->setAttrib('onChange','fngettimefordatevalidation(this.value)');
		$this->view->lobsearchform->field8->setAttrib('required',"true");
		$this->view->lobsearchform->field8->setRegisterInArrayValidator(false);
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance

		/*if(isset($this->gobjsessionstudent->userpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->userpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}*/
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrShedule = $this->lobjstudentmodel->fnGetscheduler($larrformData['field1']);				
				$this->view->lobsearchform->field5->addMultioptions($larrShedule);
				$larrVenue = $this->lobjstudentmodel->fnGetVenueName($larrformData['field5']);				
				$this->view->lobsearchform->field8->addMultioptions($larrVenue);
				$larrresult = $this->lobjstudentmodel->fnSearchStudent($larrformData,$InitialConfigDetails['NoofStd']); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->userpaginatorresult = $larrresult;
			}
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/centerconfirmation/index');
			//$this->_redirect($this->view->url(array('module'=>'stu-app' ,'controller'=>'centerconfirmation', 'action'=>'index'),'default',true));
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'SendMail' )) {
			$larrformData = $this->_request->getPost ();			
			$larrShedule = $this->lobjstudentmodel->AdddatatoTable($larrformData);
		}


	}
	

}