<?php
class GeneralSetup_Model_DbTable_Initialconfiguration extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_config';
	
	public function fnSearchUniversity($post = array()) { //Function for searching the user details
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_universitymaster'),array('IdUniversity'))
			   ->where('a.Univ_Name like "%" ? "%"',$post['field3']);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fnGetInitialConfigDetails($iduniversity) {
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()
					->from(array("a" => "tbl_config"),array("a.*"))				
		            ->where("a.idUniversity = ?",$iduniversity);	
		 return $result = $lobjDbAdpt->fetchRow($select);
	}
	
	public function fnAddInitialConfig($larrformData) { //Function for adding the user details to the table
		$this->insert($larrformData);
	}
	
	public function fnUpdateInitialconfig($idconfig,$formData)
	{
		$where = 'idConfig = '.$idconfig;
		$this->update($formData,$where);
	}

}