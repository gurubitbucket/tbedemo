<?php
class StuApp_Model__DbTable_Studentapplicationapp extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
/*
 * function to fetch all the student application details details
 */
  public function fngetStudentDetails() { //Function to get the user details
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  $lstrSelect = $lobjDbAdpt->select()
   ->from(array("a"=>"tbl_studentapplication"),array("CONCAT(a.FName,' ',IFNULL(a.MName,' '),' ',IFNULL(a.LName,' ')) AS Name","IFNULL(a.StudentId,' ')","a.DateOfBirth") )
					 			 ->join(array("b"=>"tbl_registereddetails"),'a.IDApplication=b.IDApplication',array())
				 				 ->join(array("c"=>"tbl_BatchMaster"),'a.IdBatch=c.IdBatch',array("c.BatchName"))
				 				 ->join(array("h"=>"tbl_programmaster"),'a.Program=h.IdProgrammaster',array("h.ProgramName"));
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
			
 
}