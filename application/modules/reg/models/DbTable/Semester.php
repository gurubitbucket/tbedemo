<?php 
class GeneralSetup_Model_DbTable_Semester extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_semester';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetSemesterDetails() { //Function to get the user details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_semester'),array('IdSemester'))
                ->join(array('b'=>'tbl_semestermaster'),'a.Semester = b.IdSemesterMaster');
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
    
    public function fnaddSemester($formData) { //Function for adding the University details to the table
			$this->insert($formData);
	}
    
    public function fnupdateSemester($formData,$lintIdSemester) { //Function for updating the university
    	unset ( $formData ['Save'] );
		$where = 'IdSemester = '.$lintIdSemester;
		$this->update($formData,$where);
    }
    
	public function fnSearchSemester($post = array()) { //Function for searching the university details
		$field7 = "a.Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_semester'),array('IdSemester'))
			   ->join(array('b' => 'tbl_semestermaster'),'a.Semester 	= b.IdSemesterMaster')
			   ->where('b.SemesterMasterName   like "%" ? "%"',$post['field3'])
			    ->where('a.ShortName  like "%" ? "%"',$post['field2'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	


}
?>