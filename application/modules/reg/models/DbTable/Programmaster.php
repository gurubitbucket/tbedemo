<?php 
class GeneralSetup_Model_DbTable_Programmaster extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_programmaster';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetCoursemasterDetails() { //Function to get the user details
        $result = $this->fetchAll();
        return $result;
     }
     
	public function fnGetCourseList(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.ProgramName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
    
    public function fnaddCourse($formData) { //Function for adding the University details to the table
			$this->insert($formData);
	}
    
    public function fnupdateCourse($formData,$lintIdCoursemaster) { //Function for updating the university
    	unset ( $formData ['Save'] );
		$where = 'IdProgrammaster = '.$lintIdCoursemaster;
		$this->update($formData,$where);
    }
    
	public function fnSearchCourse($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_programmaster'),array('IdProgrammaster'))
			   ->where('a.ProgramName  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}

}
?>