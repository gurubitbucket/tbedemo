<?php 
class GeneralSetup_Model_DbTable_Semestermaster extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_semestermaster';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetSemestermasterDetails() { //Function to get the user details
        $result = $this->fetchAll('Active = 1');
        return $result;
     }
    
    public function fnaddSemester($formData) { //Function for adding the University details to the table
			$this->insert($formData);
	}
    
    public function fnupdateSemester($formData,$lintIdSemesterMaster) { //Function for updating the university
    	unset ( $formData ['Save'] );
		$where = 'IdSemesterMaster = '.$lintIdSemesterMaster;
		$this->update($formData,$where);
    }
    
	public function fnSearchSemester($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_semestermaster'),array('IdSemesterMaster'))
			   ->where('a.SemesterMasterName  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fnGetSemestermasterList(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_semestermaster"),array("key"=>"a.IdSemesterMaster","value"=>"SemesterMasterName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.SemesterMasterName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

}
?>