<?php 
class GeneralSetup_Model_DbTable_Staffmaster extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_staffmaster';

    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}    
	public function fngetStaffDetails() { //Function to get the user details
        $result = $this->fetchAll('Active = 1');
        return $result;
     }
     
	public function fnaddstaffmaster($result,$larrformData) { //Function for adding the University details to the table
		$data = array('FirstName'=>$larrformData['FirstName'],
					  'SecondName'=>$larrformData['FirstName'],
		  			  'ThirdName'=>$larrformData['ThirdName'],
		 			  'FourthName'=>$larrformData['FourthName'],
					  'FullName'=>$larrformData['FullName'],
					  'ArabicName'=>$larrformData['ArabicName'],
					  'Add1'=>$larrformData['Address1'],
					  'Add2'=>$larrformData['Address2'],
					  'City'=>$larrformData['Citystaff'],
					  'State'=>$larrformData['Statestaff'],
					  'Country'=>$larrformData['Countrystaff'],
					  'Zip'=>$larrformData['Zipcode'],
					  'Phone'=>$larrformData['Phone'],
					  'Mobile'=>$larrformData['Mobile'],
					  'Email'=>$larrformData['Emailstaff'],
					  'Active'=>$larrformData['Active'],
					  'StaffType'=>$larrformData['StaffType'],
					  'IdCollege'=>$result,
					  'IdLevel'=>$larrformData['IdLevel'],
					  'IdDepartment'=>'1',
					  'IdSubject'=>'1',
					  'UpdDate'=>$larrformData['UpdDate'],
					  'UpdUser'=>$larrformData['UpdUser']);
		$this->insert($data);
		$lobjdb = Zend_Db_Table::getDefaultAdapter();
		return $lobjdb->lastInsertId();
	}
     
	/*public function fngetStaffDetailsofUniversity($IdUniversity) { //Function for the view user 
    	//echo $lintidepartment;die();
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("a" => "tbl_staffmaster"),array("a.*"))				
		            	->where("a.IdCollege= ?",$IdUniversity);	
		return $result = $lobjDbAdpt->fetchAll($select);
    }
	public function fngetStaffDetailsofCollege($IdCollege) { //Function for the view user 
    	//echo $lintidepartment;die();
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("a" => "tbl_staffmaster"),array("a.*"))				
		            	->where("a.IdCollege= ?",$IdCollege);	
		return $result = $lobjDbAdpt->fetchAll($select);
    }*/
	public function fnSearchStaff($post = array()) { //Function for searching the user details
    	$db = Zend_Db_Table::getDefaultAdapter();
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_staffmaster'),array('IdStaff'))
			   ->where('a.FirstName like "%" ? "%"',$post['field2'])
			   ->where('a.SecondName like  "%" ? "%"',$post['field3'])
			   ->where('a.FourthName like  "%" ? "%"',$post['field4'])
			   ->where('a.Email like  "%" ? "%"',$post['field6'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	public function fnGetCollegeList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		 				 ->where("a.CollegeType = 0")
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnGetDepartmentList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_departmentmaster"),array("key"=>"a.IdDepartment","value"=>"a.DepartmentName"))
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	 public function fnviewStaffDetails($lintidstafft) { //Function for the view user 
    	//echo $lintidepartment;die();
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("a" => "tbl_staffmaster"),array("a.*"))				
		            	->where("a.IdStaff= ?",$lintidstafft);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
	public function fnGetSubjectList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_subjectmaster"),array("key"=>"a.IdSubject","value"=>"a.SubjectName"))
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnaddStaff($larrformData) { //Function for adding the user details to the table
		
		if($larrformData['StaffType'] == '0') {
			$larrformData['IdCollege'] = $larrformData['IdCollege'];
		} else if($larrformData['StaffType'] == '1') {
			$larrformData['IdCollege'] = $larrformData['IdBranch'];
		}
		unset($larrformData['IdBranch']);
		$this->insert($larrformData);
	}
	public function fnGetCollege($lstridbranch){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_collegemaster"),array("a.Idhead"))
       								->where('a.IdCollege = ?',$lstridbranch);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	} 
	public function fnupdateStaf($lintIdStaff,$larrformData) { //Function for updating the user
	if($larrformData['StaffType'] == '0') {
		$larrformData['IdCollege'] = $larrformData['IdCollege'];
	} else if($larrformData['StaffType'] == '1') {
		$larrformData['IdCollege'] = $larrformData['IdBranch'];
	}
	unset($larrformData['IdBranch']);
	$where = 'IdStaff = '.$lintIdStaff;
	$this->update($larrformData,$where);
    }
	public function fnGetBranchListofCollege($lintidCollege){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		 				 ->where("a.CollegeType = 1")
		 				 ->where("a.Idhead= ?",$lintidCollege)
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnGetLevelsList() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array('a'=>'tbl_definationms'),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->join(array('b' => 'tbl_definationtypems'),'a.idDefType = b.idDefType')
								  ->where('b.defTypeDesc like ?','Levels');
								  //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetStaffMasterListforDD()
	{
		$lstselectsql = $this->lobjDbAdpt->select()
								->from(array('staff'=>'tbl_staffmaster'),array('key'=>'IdStaff','value'=>"CONCAT_WS(' ',staff.FirstName,staff.SecondName,staff.ThirdName)"))
								->where('staff.Active = 1');
								
		return $this->lobjDbAdpt->fetchAll($lstselectsql); 						
							
	}
 
}
?>