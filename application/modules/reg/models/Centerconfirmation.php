<?php
class StuApp_Model_Centerconfirmation extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
	
    /*
      * functin to search the student
      */
	public function fnSearchStudent($post = array(),$NoofStd) { //Function for searching the user details    	
    	$db = Zend_Db_Table::getDefaultAdapter();		
		$select = $db->select() 	
			   ->from(array('a' => 'tbl_studentapplication'),array('count(a.IDApplication) cnt'))
				->where('a.Venue = ? ',$post['field8'])
			   ->group('a.Venue');
			   //			   ->where($field7);
		$result = $db->fetchRow($select);		
		if($result['cnt'] < $NoofStd){
			$select = $db->select() 	
			     ->from(array('a' => 'tbl_studentapplication'),array("CONCAT(a.FName,' ',IFNULL(a.MName,' '),' ',IFNULL(a.LName,' ')) AS Name","IFNULL(a.StudentId,' ')","a.DateOfBirth","a.IDApplication"))
			     ->join(array("c"=>"tbl_BatchMaster"),'a.IdBatch=c.IdBatch',array("c.BatchName"))
				 ->join(array("h"=>"tbl_programmaster"),'a.Program=h.IdProgrammaster',array("h.ProgramName"))
				 ->where('a.Venue = ? ',$post['field8'])
				 ->where('VenueChange = 0');
				$result1 = $db->fetchAll($select);
				
		}
		else {
			$result1 = array();
		}		
		return $result1;
	}
	function AdddatatoTable($arrayData){
		for($i=0;$i<count($arrayData['IdInvoices']);$i++){
			$formData['VenueChange']  = 1;
			$where = 'IDApplication = '.$arrayData['IdInvoices'][$i];
			$this->update($formData,$where);
		}
	}
/*
 * function to fetch all the batch details
 */
	public function fnGetBatchName($days){	
				$datetocmp = date('Y-m-d', strtotime($days.'days'));	
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_BatchMaster"),array("key"=>"a.IdBatch","value"=>"CONCAT(DATE_FORMAT(`a`.`BatchFrom`,'%d-%m-%Y'),'---',DATE_FORMAT(`a`.`BatchTo`,'%d-%m-%Y'))"))
										  ->where("a.BatchStatus  = ?","0")
										  ->where("a.BatchFrom between  curdate() AND '".$datetocmp."'");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}	
			
 public function fnGetscheduler($idbatch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("key"=>"a.idschedulermaster","value"=>("a.ScheduleName")))
										  ->where("a.idBatch  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }			
 public function fnGetVenueName($lintidscheduler)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"b.idschedulervenue","value"=>"a.centername"))
										  ->join(array("b" =>"tbl_schedulervenue"),'a.idcenter = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulermaster"),'b.idschedulermaster = c.idschedulermaster',array())
										  ->where("c.idschedulermaster  = ?",$lintidscheduler)
										  ->order("a.city");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }	
 
}