<?php

class App_Form_Registration extends Zend_Dojo_Form 
{		
    public function init()
    {             
    	$strSystemDate = date('Y-m-d');  
         $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
         $UpdDate->removeDecorator("DtDdWrapper");
         $UpdDate->removeDecorator("Label");
         $UpdDate->removeDecorator('HtmlTag');
     
         $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
         $UpdUser->removeDecorator("DtDdWrapper");
         $UpdUser->removeDecorator("Label");
         $UpdUser->removeDecorator('HtmlTag');
        
        

                   
        $id = new Zend_Form_Element_Text('id');
		$id->setAttrib('dojoType',"dijit.form.ValidationTextBox");
                $id->setAttrib('class', 'txt_put') ;
                $id->setAttrib('required',"true")      
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        $Date = new Zend_Form_Element_Hidden('Date');
		$Date->setAttrib('dojoType',"dijit.form.TextBox");
                $Date->setAttrib('class', 'txt_put') ;
                $Date->setValue($strSystemDate)     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                  
      
	         
         $Login = new Zend_Form_Element_Submit('Login');
         $Login->setAttrib('id', 'submitbutton');
         $Login->removeDecorator("DtDdWrapper");
          $Login->class = "buttonStyle";
          $Login->removeDecorator("Label");
         $Login->removeDecorator('HtmlTag');
         
          $Accept = new Zend_Form_Element_Submit('Accept');
         $Accept->setAttrib('class', 'buttonStyle');
         //$Accept->setAttrib('onClick','questiondisplay()');
         $Accept->removeDecorator("DtDdWrapper");
         $Accept->removeDecorator("Label");
         $Accept->removeDecorator('HtmlTag');
         
          $Back = new Zend_Form_Element_Button('Back');
         $Back->setAttrib('class', 'buttonStyle')
         ->setAttrib('onClick','questiondisplay()');
         $Back->removeDecorator("DtDdWrapper");
         $Back->removeDecorator("Label");
         $Back->removeDecorator('HtmlTag');
         
          $Reset = $this->createElement('Reset','Reset');
          $Reset->removeDecorator("DtDdWrapper");
          $Reset->class = "buttonStyle";
        
         $this->addElements(array($UpdDate,$UpdUser,$Date,
         						 $id,$Login,$Reset,$Back,$Accept));
    }
}
        
        