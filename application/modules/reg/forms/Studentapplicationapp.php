<?php
class StuApp_Form_Studentapplicationapp extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    
	    
		$Approve = new Zend_Form_Element_Submit('Approve');
		$Approve->dojotype="dijit.form.Button";
        $Approve->label = $gstrtranslate->_("Approve");
		$Approve->setAttrib('class', 'NormalBtn');
        $Approve	->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->setAttrib('style', 'width:auto;')
        			->setAttrib('onClick','return validateForm()')
        			->removeDecorator('HtmlTag');	
        $SendMail = new Zend_Form_Element_Submit('SendMail');
		$SendMail->dojotype="dijit.form.Button";
        $SendMail->label = $gstrtranslate->_("Send Mail");
		$SendMail->setAttrib('class', 'NormalBtn');
        $SendMail	->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->setAttrib('style', 'width:auto;')
        			->setAttrib('onClick','return validateForm()')
        			->removeDecorator('HtmlTag');					
        //form elements
        $this->addElements(array($Approve,$SendMail
        						
        						
                                 ));

    }
}