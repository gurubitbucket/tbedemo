<?php
class Scheduler_Model_DbTable_Chronepullscheduling extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_center';
	
	//Check Super User Authentication
	public function getintialconfig()
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
						->from(array("a" =>"tbl_config"),array('a.Scheduletime','a.Scheduleday'))
						->where("a.idUniversity =1");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	
	}
	
	public function fnexamslistforsinglevenue($fromdate,$todate,$idvenue)
	{
	//echo $fromdate;
	//echo "<br>";
	//echo $todate;
	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DATE_FORMAT(a.date,'%d-%m-%Y') as Date","DAYNAME(a.date) AS dayname"))
						 ->join(array('c'=>'tbl_center'),'c.idcenter = a.idvenue',array("c.centername"))
						 // ->join(array('d'=>'tbl_programmaster'),'d.IdProgrammaster = a.idprogram',array("d.ProgramName"))					
						 ->where("a.date >= '$fromdate'")
						 ->where("c.ipaddress =?",1)
						 ->where("a.date <= '$todate'")
						 ->where("c.idcenter= ?",$idvenue)
						 ->where("a.Active =1")	
						 //->order("a.date")
						//->order("c.centername");
						 ->group("a.date")
						 ->group("a.idvenue");
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
	
	}
	public function fngetvenuedetails($idvenue)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("b"=>"tbl_center"),array("b.idcenter","b.centername"))
								 ->where("b.idcenter =?",$idvenue);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
									 
	}
	public function fngetvenues()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("b"=>"tbl_center"),array("b.idcenter","b.centername"))
								 ->where("b.ipaddress =?",1);
								
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
									 
	}
	public function getvenuelist()
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
					 				 ->where("a.ipaddress =?",1)
									 ->order("a.centername");
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	   return $larrResult;
	}
	public function fnexamslist($fromdate)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DATE_FORMAT(a.date,'%d-%m-%Y') as Date","DAYNAME(a.date) AS dayname"))
						 ->join(array('c'=>'tbl_center'),'c.idcenter = a.idvenue',array("c.centername"))
						 // ->join(array('d'=>'tbl_programmaster'),'d.IdProgrammaster = a.idprogram',array("d.ProgramName"))
						 ->where("a.date >= '$fromdate'")
						 ->where("c.ipaddress =?",1)
						 //->where("a.date <= '$todate'")
						 //->where("a.Allotedseats >0")
						 ->where("a.Active =1")						
						 ->order("a.date")
						 ->order("c.centername")
						 ->group("a.date")
						 ->group("a.idvenue");
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
	}
	
	public function fngetallcandidates($idcenter,$iddate)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  	$lstrSelect = $lobjDbAdpt->select()
							 			   ->from(array("a" => "tbl_studentapplication"),array("a.*"))
							 			   ->join(array("b" => "tbl_registereddetails"),'a.IDApplication=b.IDApplication',array("b.*"))
							 			   ->where("a.Examvenue='$idcenter'")
							 			   ->where("a.VenueTime=0")
							 			   ->where("a.Payment=1")
							 			   ->where("a.DateTime='$iddate'")
							 			   ->group("a.IDApplication");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
	public function fnupdatedstudentapplication($idapplication)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_studentapplication"; 
			 $where1 = "IDApplication = '".$idapplication."'"; 	  
						   $larrupdatedatas = array(		
							'VenueTime' => 1
						);
	         $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
    }
	 
    public function fnupdatedchronedstatus($idcenter,$iddate)
    {
	
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_cronevenue"; 
			 $where1 = "idcenter = '".$idcenter."'  AND Examdate ='".$iddate."'"; 	  
						   $larrupdatedatas = array(		
							'Status' => 1
						);
	        $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
    }
	public function fnupdatedchronedstatusfailed($idcenter,$iddate)
    {
	
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$tablestartexamdetails = "tbl_cronevenue"; 
			 $where1 = "idcenter = '".$idcenter."'  AND Examdate ='".$iddate."'"; 	  
						   $larrupdatedatas = array('Status' => 2);
		
	        $lobjDbAdpt->update($tablestartexamdetails,$larrupdatedatas,$where1);
	         
	         
	         self::fngetvenuename($idcenter);
	     
    } 
 public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}	
		public function fngetreceivermail()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_config"));
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
	}
    public function fngetvenuename($idcenter)
    {
    		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    		$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_center"),array("a.*"))
							 			   ->where("a.idcenter='$idcenter'");
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			$venuename = $larrResult['centername'];			
			$larrEmailTemplateDesc = self::fnGetEmailTemplateDescription("Chrone Venue Fail");			
			$larconfigdetails = self::fngetreceivermail();
			
			require_once('Zend/Mail.php');
			require_once('Zend/Mail/Transport/Smtp.php');
			
			 $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$lstrEmailTemplateBody = str_replace("[Venue]",$venuename,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
							$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'tbeprasad';
										$receiver_email = $larconfigdetails['Schedulerpushemail'];
										$receiver = 'Admin';
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);							
									$result = $mail->send($transport);
											
    }	
	
	public function  fngetstatusofvenueforexamdates($fromdate,$todate)
	{
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_cronevenue"),array("a.*","DATE_FORMAT(a.Examdate,'%d-%m-%Y') as Examdate"))
										  ->join(array('b'=>'tbl_automail'),'b.idautomail  = a.idautomail',array("b.*"))
							 			  ->where("a.Examdate >= '$fromdate'")
										  ->where("a.Examdate <= '$todate'")
							 			  ->order("a.idcronevenue Desc");
										  //->group("a.Examdate");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);				
				return $larrResult;
	}
	public function fngetstatus($examdate)
	{
	             $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_cronevenue"),array("a.*","DATE_FORMAT(a.Examdate,'%d-%m-%Y') as Examdate"))
										  ->join(array('b'=>'tbl_automail'),'b.idautomail  = a.idautomail',array("b.*"))
							 			  ->where("DATE_FORMAT(a.Examdate,'%d-%m-%Y') = ?",$examdate)
										  //->where("a.idcenter =?",$idvenue)
							 			  ->order("a.idcronevenue Desc");
										  //->group("a.Examdate");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);				
				return $larrResult;
	
	
	}
	public function  fntodeletecrone($idauto)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Delete from tbl_autochronepull where idautochronepull = $idauto"; 
		$lstrSelect1 = "Delete from tbl_chronepull where idautochronepull = $idauto"; 
		
		$lobjDbAdpt->query($lstrSelect);
		$lobjDbAdpt->query($lstrSelect1);
	
	}
	public function fngettodate($todate)
	{
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = "select ADDDATE('$todate', INTERVAL 30 DAY) as todate";
	   //echo $lstrSelect;die();
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	   return $larrResult;
		
	}
	public function fnupdatechronepull($larrformdata,$examcenterdetails)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	//$examdate = $larrformdata['Date'];
	    // $noofstu = count($larrformdata['idapp']);	   
	    // echo "<pre>";
	   // print_r($examcenterdetails);die();
	   for($i=0;$i<count($examcenterdetails);$i++)
	   {
	       $interval=0;			
			$schedulerdate = $examcenterdetails[$i]['Scheduledate'];
			$schedulertime = explode('T',$examcenterdetails[$i]['Scheduletime']);
			 $schedulertime = $schedulertime[1];
			
			$tablestartexamdetails = "tbl_autochronepull"; 
			 $where1 = "idautochronepull = '".$examcenterdetails[$i]['Idautopull']."'"; 	 
						  $postdata = array('schdeduleddate'=>$schedulerdate,
							  'scheduledtime'=>$schedulertime,
							  'UpdDate' => $larrformdata['UpdDate'],
							  'UpdUser' => $larrformdata['UpdUser']);
		
	        $lobjDbAdpt->update($tablestartexamdetails,$postdata,$where1);
			
		   }
    }
	public function fngetvenuedates($examdate)
	{
	//echo $examdate;
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DATE_FORMAT(a.date,'%d-%m-%Y') as Date"  ))
						 //->join(array('b'=>'tbl_managesession'),'b.idmangesession = a.idsession',array("b.managesessionname"))
						 ->join(array('c'=>'tbl_center'),'c.idcenter = a.idvenue',array("c.centername"))
						 // ->join(array('d'=>'tbl_programmaster'),'d.IdProgrammaster = a.idprogram',array("d.ProgramName"))
						 //->where("b.Active !=0")
						 ->where("a.date = '$examdate'")
						 ->where("c.ipaddress =?",1)
						 //->where("a.Allotedseats >0")
						 ->where("a.Active =1")						
						 ->order("a.date")
						 ->order("c.centername")
						 ->order("b.managesessionname");
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
	  
	}
	public function fnexamdates($fromdate,$todate)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("DATE_FORMAT(a.date,'%d-%m-%Y') as Date"))
						 ->join(array('b'=>'tbl_managesession'),'b.idmangesession = a.idsession',array(""))
						 ->join(array('c'=>'tbl_center'),'c.idcenter = a.idvenue',array(""))
						 // ->join(array('d'=>'tbl_programmaster'),'d.IdProgrammaster = a.idprogram',array("d.ProgramName"))
						 //->where("b.Active !=0")
						 ->where("a.date >= '$fromdate'")
						 ->where("c.ipaddress =?",1)
						 ->where("a.date <= '$todate'")
						 //->where("a.Allotedseats >0")
						 ->where("a.Active =1")						 
						 ->order("a.date")
						 ->order("c.centername")
						 ->order("b.managesessionname")
						 ->group("a.date");
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
	}
	
	public function  fngetvenuedateschedule($iddate)
       {
       			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array())
							 			  ->join(array("b"=>"tbl_managesession"),'a.idsession=b.idmangesession',array("key"=>"b.idmangesession","value"=>"b.managesessionname"))
							 			  ->where("a.date='$iddate'")
										  ->where("a.Active = 1")
										    ->where("a.Reserveflag= 1")
										     ->group("b.idmangesession")
										     ->order("b.managesessionname");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
       }
	   public function fngetallvenues($larrformdata)
    {
    	$iddate = $larrformdata['Date'];
    	$session = $larrformdata['Session'];
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
							 			  ->join(array("b"=>"tbl_center"),'a.idvenue=b.idcenter',array("b.*"))
							 			  ->join(array("c"=>"tbl_managesession"),'a.idsession=c.idmangesession',array("c.*"))
							 			  ->where("a.date='$iddate'")
							 			  ->where("a.idsession='$session'")
							 			  ->where("b.ipaddress=1")
							 			    ->where("a.Allotedseats>0")
							 			  ->group("b.idcenter")
							 			  ->order("b.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
	public function fngetallsessions()
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("b"=>"tbl_managesession"),array("key"=>"b.idmangesession","value"=>"b.managesessionname"))
										;
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
	public function fngetchronepulldatasautos($date,$idsession)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a"=>"tbl_autochronepull"),array("a.*","DATE_FORMAT(a.schdeduleddate ,'%d-%m-%Y') as schdeduleddate"))
							 			 ->join(array("b"=>"tbl_chronepull"),'a.idautochronepull=b.idautochronepull',array("b.*","DAYNAME(b.Examdate) AS dayname","b.Status as Chronestatus"))
							 			  ->join(array("d"=>"tbl_center"),'b.Venue=d.idcenter',array("d.centername"))
							 			    ->join(array("e"=>"tbl_managesession"),'e.idmangesession=b.Session',array("e.managesessionname"))
							 			 ->where("date(a.UpdDate)='$date'")
										  ->where("a.Session=?",$idsession)
							 			// ->where("date(a.UpdDate)<='$todate'")
							 			 ->where("a.Automated in(0,1)")
							 			 ->order("b.ExamDate");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }
public function  fngetchronepulldatasautossinglevenue($date,$idvenue,$idsession)
{
   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a"=>"tbl_autochronepull"),array("a.*","DATE_FORMAT(a.schdeduleddate ,'%d-%m-%Y') as schdeduleddate"))
							 			 ->join(array("b"=>"tbl_chronepull"),'a.idautochronepull=b.idautochronepull',array("b.*","DAYNAME(b.Examdate) AS dayname","b.Status as Chronestatus"))
							 			  ->join(array("d"=>"tbl_center"),'b.Venue=d.idcenter',array("d.centername"))
							 			    ->join(array("e"=>"tbl_managesession"),'e.idmangesession=b.Session',array("e.managesessionname"))
							 			 ->where("date(a.UpdDate)='$date'")
										  ->where("a.Session=?",$idsession)
										   ->where("b.Venue =?",$idvenue)
							 			// ->where("date(a.UpdDate)<='$todate'")
							 			 ->where("a.Automated in(0,1)")
							 			 ->order("b.ExamDate");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;


}
public function fngetstatusofvenue($idvenue,$idsession,$date)
    {
	 //echo $idvenue;	 
	 //echo $date;
	 $formateddate = date("Y-m-d",strtotime($date));
    	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_autochronepull"),array("a.*","DATE_FORMAT(a.schdeduleddate ,'%d-%m-%Y') as schdeduleddate"))
										   ->join(array('b'=>'tbl_chronepull'),'b.idchronepull   = a.idautochronepull',array("b.*","b.Venue as Idvenue","b.Status as Chronestatus"))
							 			  ->where("b.Examdate='$date'")
							 			   ->where("b.Venue='$idvenue'")
										   ->where("b.Session='$idsession'")
							 			    ->order("a.idautochronepull");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				// 
				return $larrResult;
    }
	

}
