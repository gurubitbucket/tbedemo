<?php
class Scheduler_Model_DbTable_Approvescheduler extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_center';
	
	//Check Super User Authentication
	public function fnapprovetrainingcenterscheduler($schedulers)
	{
	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	    
	     for($i=0;$i<count($schedulers);$i++)
		 {
	               $data = array('idprogram'=>5,'Active'=>1,'UpdDate'=>date('Y-m-d H:i:s'));					 
		          $where['idvenuedateschedule = ? ']=$schedulers[$i];
		          $lobjDbAdpt->update('tbl_venuedateschedule', $data, $where);
		 }
	}

	public function fngettoschedulerlists()
	{
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt  ->select()
								  ->from(array("a" =>"tbl_venuedateschedule"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as createddate","DATE_FORMAT(a.date,'%d-%m-%Y') as Examdate"))
								  ->join(array('d'=>'tbl_newscheduler'),'d.idnewscheduler = a.idnewscheduler',array('d.*'))
								  ->join(array('b'=>'tbl_center'),'a.idvenue = b.idcenter',array('b.centername')) 
								  ->join(array('c'=>'tbl_takafuloperator'),'c.idtakafuloperator = b.centercode',array('c.TakafulName'))
								  ->join(array('e'=>'tbl_managesession'),'e.idmangesession = a.idsession',array('e.managesessionname'))
								  ->where("a.idprogram in ('5','10')");								 
								  //->where("a.Active 	 =0");								 
								 //->group("a.idvenue");							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;		
	 }
	public function gettoschedulernames($formdata)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $schedulerdesc =  $formdata['Description'];
		 $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_venuedateschedule"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as Approvedate","DATE_FORMAT(a.date,'%d-%m-%Y') as Examdate"))
								  ->join(array('d'=>'tbl_newscheduler'),'d.idnewscheduler = a.idnewscheduler',array('d.*',"DATE_FORMAT(d.UpdDate,'%d-%m-%Y') as createddate"))
								  ->join(array('b'=>'tbl_center'),'a.idvenue = b.idcenter',array('b.centername')) 
								  ->join(array('c'=>'tbl_takafuloperator'),'c.idtakafuloperator = b.centercode',array('c.TakafulName'))
                                  ->join(array('e'=>'tbl_managesession'),'e.idmangesession = a.idsession',array('e.managesessionname'))								  
								  ->where("a.idprogram in (5,10)")
								  //->where("a.Active 	 =0")
								  ->where("d.Description  LIKE '".$schedulerdesc."%'");
                      if($formdata['Status']==1)
					  {	
					     $lstrSelect->where("a.Active = 1");
					  }
                       if($formdata['Status']==2)
                      {
					     $lstrSelect->where("a.Active = 0");
                        }									  
								 //->group("a.idvenue");							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }      
	
}
