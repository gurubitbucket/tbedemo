<?php
class Scheduler_Model_DbTable_Schedulereportmodel extends Zend_Db_Table { 
	
	public function fngetschdeuledates($larrformData)
	{
		if($larrformData['FromDate']) $fromdate = $larrformData['FromDate'];
		if($larrformData['ToDate']) $todate = $larrformData['ToDate'];
		if($larrformData['day']) $day = $larrformData['day'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a"=>"tbl_venuedateschedule"),array("DATE_FORMAT(a.date,'%d-%m-%Y') AS Date","a.date","DAYNAME(a.date) AS dayname"))
		                         ->where("a.date >= '$fromdate'")
								 ->where("a.date <= '$todate'")
								 ->where("a.Reserveflag = 1")
								 ->where("a.Active = 1");
		if($larrformData['day']) $lstrSelect->where("DAYNAME(a.date) = '$day'");						   
	    $lstrSelect.= "GROUP BY a.date";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
   public function fngetschdeulevenues($fromdate,$todate)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a"=>"tbl_venuedateschedule"),array(""))
		                         ->join(array("b"=>"tbl_center"),"b.idcenter = a.idvenue",array("b.idcenter","b.centername"))
		                         ->where("a.date >= '$fromdate'")
								 ->where("a.date <= '$todate'")
								 ->where("a.Active = 1")
								 ->group("b.centername")
								 ->order("b.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
   public function fngetvenues()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("b"=>"tbl_center"),array("b.idcenter","b.centername"))
								 ->order("b.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
									 
	}
	
	public function fngetvenuedates($fromdate) 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a"=>"tbl_center"),array("a.idcenter","a.centername"))
								 ->join(array("b"=>"tbl_venuedateschedule"),"a.idcenter = b.idvenue",array("b.*"))
								 ->where("b.Active = 1")
								 ->where("b.date = '$fromdate'")
								 ->group("a.idcenter")
								 ->order("a.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
    }
	
	public function fngetseats($idcenter,$date) 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								  ->from(array("b"=>"tbl_venuedateschedule"),array("b.*"))
								  ->join(array("a"=>"tbl_managesession"),"a.idmangesession = b.idsession",array("a.*"))
								  ->where("b.Active = 1")
								  ->where("b.Reserveflag = 1")
								  ->where("b.idvenue = $idcenter")
								  ->where("b.date = '$date'");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		//echo "<pre>";print_r($larrResult);
		return $larrResult;
    }
}