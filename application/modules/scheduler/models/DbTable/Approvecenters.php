<?php
class Scheduler_Model_DbTable_Approvecenters extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_center';	
	//Check Super User Authentication
	public function fngettocenternames()
	{
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 	 ->from(array("a"=>"tbl_center"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as createddate")) 
					  ->join(array('b'=>'tbl_takafuloperator'),'b.idtakafuloperator = a.centercode',array('b.TakafulName')) 
                     //->where("a.Active =0")	
					 //->where("a.Nooffloors =0")
					 ->order("a.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	 public function gettocenternames($formdata)
	{
	    $center =  $formdata['Centername'];
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 	 ->from(array("a"=>"tbl_center"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as createddate")) 
					  ->join(array('b'=>'tbl_takafuloperator'),'b.idtakafuloperator = a.centercode',array('b.TakafulName')) 
                      ->where("a.centername  LIKE '".$center."%'");
					  if($formdata['Status']==1)
					  {	
					     $lstrSelect->where("a.Nooffloors = 1");
					  }
                       if($formdata['Status']==2)
                      {
					     $lstrSelect->where("a.Nooffloors = 0");
                        }					   
					 //->where("a.Nooffloors =0")					 
					 $lstrSelect->order("a.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
     public function fnapprovetrainingcenter($centers)
	 {
	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	    
	     for($i=0;$i<count($centers);$i++)
		 {
	              $data = array('Nooffloors'=>1);					
		          $where['idcenter = ? ']=$centers[$i];
		          $lobjDbAdpt->update('tbl_center', $data, $where);
		 }
	 }	
}
