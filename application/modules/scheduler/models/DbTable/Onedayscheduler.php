<?php 
class Scheduler_Model_DbTable_Onedayscheduler extends Zend_Db_Table_Abstract
{
  
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    

     public function fngetCourse() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programmaster"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
   
     public function fngetSessionDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	                            ->from(array("a"=>"tbl_managesession"),array("key"=>"a.idmangesession","value"=>"(CONCAT((a.managesessionname),'---',(a.ampmstart),' to ',(a.ampmend)))"))
								 ->where("a.active=1");							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
	
public function fngetdayofdate($iddate)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	    $sql = "select DAYOFWEEK('$iddate') as days"; 
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
       
   public function fnAddScheduler($larrformdata)
   {
 
   	$splitdate = explode('-',$larrformdata['Date']);
   	$larrformdata['Year'] = $splitdate[0];
   	$larrformdata['To']=$larrformdata['From'] = $splitdate['1'];
/*   	print_R($splitdate[1]);
   	print_R($splitdate[2]);	
   	echo "<pre/>";
   	print_r($larrformdata);*/
   
   
	      $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_newscheduler";
          $postData = array(	
                             'From'=>$larrformdata['From'],
          					 'To'=>$larrformdata['To'],
          					 'Year'=>$larrformdata['Year'],
            				 'UpdUser' =>$larrformdata['UpdUser'],		
		            		 'UpdDate' =>$larrformdata['UpdDate'],
                             'Description' =>$larrformdata['description'],
          					 'Active'=>1												
						);			
	       $db->insert($table,$postData);
	      $lastid  = $db->lastInsertId("tbl_newscheduler","idnewscheduler");	
	       
	       
	       
	       
   $idexamday=self::fngetdayofdate($larrformdata['Date']);
			
			
			if($idexamday['days']==1)
			{
				$idexamday['days']= 7;
			}
			else 
			{
			$idexamday['days']=$idexamday['days']-1;	
			}
	       /////////////////for weekdays////////////////////////////
	       
	          	 $tables = "tbl_newschedulerdays";
	       	     $arryresult['idnewscheduler']= $lastid;
	       	     $arryresult['Days'] = $idexamday['days'];
	       	     $db->insert($tables,$arryresult);
	      
	       ///////////////////end for weekdays///////////////////
	       
	         /////////////////for course////////////////////////////
	        for($i=0;$i<count($larrformdata['idprogram']);$i++)
	        {
	          	 $tablescourse = "tbl_newschedulercourse";
	       	     $arryresultcourse['idnewscheduler']= $lastid;
	       	     $arryresultcourse['IdProgramMaster'] = $larrformdata['idprogram'][$i];
	       	     $db->insert($tablescourse,$arryresultcourse);
	        }
	       ///////////////////end for course///////////////////
	       
	         /////////////////for venue session////////////////////////////
	        for($i=0;$i<count($larrformdata['session']);$i++)
	        {
	          	 $tablessession = "tbl_newschedulersession";
	       	     $arryresultsession['idnewscheduler']= $lastid;
	       	     $arryresultsession['idmanagesession'] = $larrformdata['session'][$i];
	       	     $db->insert($tablessession,$arryresultsession);
	        }
	       ///////////////////end for venue session///////////////////
	       
	         /////////////////for venue////////////////////////////
	        for($i=0;$i<count($larrformdata['venarr']);$i++)
	        {
	          	 $tablescenter = "tbl_newschedulervenue";
	       	     $arryresultcenter['idnewscheduler']= $lastid;
	       	     $arryresultcenter['idvenue'] = $larrformdata['venarr'][$i];
	       	     $db->insert($tablescenter,$arryresultcenter);
	        }

		   	  for($j=0;$j<count($larrformdata['venarr']);$j++)
			        {
			        	$Totalcapacity=$larrformdata['caparr'][$j];
			             for($k=0;$k<count($larrformdata['session']);$k++)
				        {
				        	
				        	
 											$table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>$larrformdata['Date'],
								          					 'idsession'=>$larrformdata['session'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>$larrformdata['UpdUser'],		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>1,
								                             'idvenue' =>$larrformdata['venarr'][$j],
								                              'idnewscheduler'=>$lastid												
														);			
									       $db->insert($table,$postData);
				        	
	       					 
				        }
			          	
			        } 
	        
   }
   
   public function fngetexceptiondate($iddate)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulerexception"),array("a.*"))
										  ->where("Date=?",$iddate)
										    ->where("Active=1");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;	 
   }
   
   public function fngetpresentschedulers($larrformdata,$idsessions)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
							  ->from(array("a" =>"tbl_venuedateschedule"),array("a.*"))
							  ->where("date=?",$larrformdata['Date'])
							  ->where("idsession in($idsessions)")
							  ->where("Reserveflag=1")
							  ->where("Active=1");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;	 
   }
   
   public function fngetcenternames($idcenters)
   {
   	       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
										  ->where("a.idcenter not in ($idcenters)")
										  ->order("a.centername");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
   }
   public function fninserttempsession($lintidsession,$lintsess)
   {
   	$db = Zend_Db_Table::getDefaultAdapter();
   	$tables = "tbl_tempsessions";
	$arryresult['idsession']= $lintidsession;
	$arryresult['session'] = $lintsess;
	$db->insert($tables,$arryresult);
   }
   public function fnajaxgetsession($id)
   {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	                            ->from(array("a"=>"tbl_managesession"),array("key"=>"a.idmangesession","value"=>"(CONCAT((a.managesessionname),'---',(a.ampmstart),' to ',(a.ampmend)))"))
								 ->where("a.active=1")
								 ->where("a.idmangesession not in ($id)");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
   }
   public function fnajaxgettempsession($idtemp)
   {
   	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
	                            ->from(array("a"=>"tbl_tempsessions"),array("a.*"))
	                            ->join(array("b"=>"tbl_managesession"),"a.idsession = b.idmangesession",array("b.*"))
	                            ->where("a.session ='$idtemp'");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
   }
   public function fndeleteaction($id)
   {
   	    $db = Zend_Db_Table::getDefaultAdapter();
   		$lstrselect  = "Delete from tbl_tempsessions where 	session = '$id'";
		$db->query($lstrselect);
   }
   
	public function fninserttempvenue($lintidvenue,$lintcapacity,$lintsess)
   {
   	$db = Zend_Db_Table::getDefaultAdapter();
   	$tables = "tbl_tempvenues";
	$arryresult['idvenues']= $lintidvenue;
	$arryresult['session'] = $lintsess;
	$arryresult['capacity'] = $lintcapacity;
	$db->insert($tables,$arryresult);
   }
	public function fnajaxgettempvenues($idtemp)
   {
   	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
	                            ->from(array("a"=>"tbl_tempvenues"),array("a.*"))
	                            ->join(array("b"=>"tbl_center"),"a.idvenues = b.idcenter",array("b.*"))
	                            ->where("a.session ='$idtemp'");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
   }
  public function fnajaxgetvenue($id)
   {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	                             ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
								 ->where("a.idcenter not in ($id)")
								 ->order("a.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
   }
 public function fndeletetempvenue($id)
   {
   	    $db = Zend_Db_Table::getDefaultAdapter();
   		$lstrselect  = "Delete from tbl_tempvenues where session = '$id'";
		$db->query($lstrselect);
   }
   //CONCAT( IFNULL(d.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date
   public function fngetdate($dat){
        $db = Zend_Db_Table::getDefaultAdapter();
   		$lstrselect  = "Select Concat(DAYNAME('$dat'),' ',DAY('$dat'),' ',MONTHNAME('$dat'),' ','Schedule') as date"; 
		$result = $db->fetchRow($lstrselect);
		
		
		return $result;
   }
}
?>