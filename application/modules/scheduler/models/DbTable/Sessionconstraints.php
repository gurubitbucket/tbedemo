<?php 
class Scheduler_Model_DbTable_Sessionconstraints extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_program';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetSessionDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_schedulerexception"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     public function fngetVenueDetails(){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	$lstrSelect = $lobjDbAdpt->select()
     	->from(array("tbl_center"=>"tbl_center"),array("tbl_center.idcenter","tbl_center.state","tbl_center.city","tbl_center.centername"))
     	->order("tbl_center.idcenter");
     	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
     	return $larrResult;
     }

     public function fngetcenters(){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	$lstrSelect = $lobjDbAdpt->select()
     	->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername"))
     	->order("a.centername");
     	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
     	return $larrResult;
     }

     public function fngetDaysDetails()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	   	             ->from(array("a"=>"tbl_days"),array("key"=>"a.iddays","value"=>"a.Days"));			 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
public function fngetSateDetails()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	   	             ->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
	   	             ->where("a.idCountry=121")
	   	             ->order("a.StateName"); 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     
     
public function fnGetVenueList($lintIdCountry)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	   	             ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
	   	             ->where("a.country=121")
	   	             ->where("a.city=?",$lintIdCountry)
	   	             ->where("a.Active=1")
	   	             ->order("a.centername"); 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     

 public function fnaddSessionconstrains($formData) { //Function for adding the University details to the table
    	$db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_schedulerexception";
          if($formData['Active']==1)
          {
          	 $reserveflag =0;
          }
          else 
          {
          	 $reserveflag =1;
          }
         
          for($lintcenter=0;$lintcenter<count($formData['idcenters']);$lintcenter++)
          {
            $postData = array(		
							'Date' => $formData['Date'],	
           					'idstate' => $formData['idstate'][$formData['idcenters'][$lintcenter]],           				                           
                            'idcity' => $formData['idcity'][$formData['idcenters'][$lintcenter]],	
            				'idvenue' =>$formData['idcenters'][$lintcenter],
                            'constraintname'=>$formData['constraintname'],
            				'Active' =>$formData['Active'],			
						);	
	      $db->insert($table,$postData);
	      
	       $data = array(
					   'Reserveflag'=>$reserveflag);
	     $where['date = ? ']= $formData['Date'];
	     $where['idvenue = ? ']= $formData['idcenters'][$lintcenter];
		 $db->update('tbl_venuedateschedule', $data, $where);
          }
	}
    
public function fnSearchSession($post) { //Function for searching the university details
		$field7 = "tbl_schedulerexception.Active = ".$post["field7"];
		$field10=$post['field10'];
		$field19=$post['field19'];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('tbl_schedulerexception' => 'tbl_schedulerexception'),array('tbl_schedulerexception.*'))
			   ->join(array("tbl_center" => "tbl_center"),'tbl_schedulerexception.idvenue = tbl_center.idcenter',array("tbl_center.centername"))
			   ->where('tbl_schedulerexception.constraintname  like "%" ? "%"',$post['field3'])
			    ->where($field7);
			   if(!empty($field10))$select=$select->where("DATE_FORMAT(tbl_schedulerexception.Date,'%Y-%m-%d')='$field10'");
			   if(!empty($field19))$select=$select->where("tbl_center.idcenter='$field19'");
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
public function  fnGetcityList($idstate)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
         	$select = $db->select()
			   ->from(array('a' => 'tbl_city'),array("key"=>"a.idCity","value"=>"CityName"))
			   ->where('a.idState=?',$idstate);
		$result = $db->fetchAll($select);
		return $result;
	}
	
	public function fnGetsessionList($IdProgram){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_schedulerexception"),array("a.constraintname","a.Date","a.idstate as state","a.idcity as city","a.Active"))
				 				   ->join(array("c"=>"tbl_state"),'a.idstate=c.idState',array("c.StateName as idstate"))
				 				 ->where("a.idschedulerconstraints = ?",$IdProgram);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
	
	
	
	public function fnupdateSession($formData) { //Function for updating the university
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$iddate=$formData['Date'];
		$idvenue=$formData['idvenue'];
		if($formData['Active']==1)
		{
			$reserveflag =0;
		}
		else
		{
			$reserveflag =1;
		}
		$data =  array('Date' => $formData['Date'],
           					'idstate' => $formData['idstate'],           				                           
                            'idcity' => $formData['idcity'],	
            				'idvenue' =>$formData['idvenue'],
                            'constraintname'=>$formData['constraintname'],
            				'Active' =>$formData['Active'],			
		);
		$where['idschedulerconstraints = ? ']= $formData['idschedulerconstraints'];
		$db->update('tbl_schedulerexception', $data, $where);

		///////////////////venue scheduler days////////////////////////////
		$data = array(
					   'Reserveflag'=>$reserveflag);
		$where1 = "date = '".$iddate."'  AND idvenue ='".$idvenue."'";
		$db->update('tbl_venuedateschedule', $data, $where1);
	}
	public function fnGetsearchdetails($formdata)
	{
	   $fromdate = $formdata['Date'];
	   $todate = $formdata['Schedulertodate'];
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DATE_FORMAT(a.date,'%d-%m-%Y') as date","DAYNAME(a.date) AS dayname"))						
						 ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                         ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))						 
						 ->where("a.date >= '$fromdate'")
						 ->where("a.date <= '$todate'")
						 ->where("a.idvenue=?",$formdata['Venues'])
						 ->where("a.Active=?",$formdata['Action'])
						 ->where("a.Reserveflag =?",1)
						 ->order("a.date")
						 ->order("b.centername");
							
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
	   
	  
	
	}
	public function fngetschedulerdetails($idscheduler)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DAYNAME(a.date) AS dayname"))						
						 ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                         ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))						 
						 //->where("a.date >= '$fromdate'")
						// ->where("a.date <= '$todate'")
						 ->where("a.idvenuedateschedule=?",$idscheduler)
						 ->where("a.Active=?",1)
						 ->where("a.Reserveflag =?",1)
						 ->order("a.date")
						 ->order("b.centername");
							
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);			
	   return $larrResult;
	}
	public function  fngetschedulerresult($idscheduler)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DAYNAME(a.date) AS dayname"))						
						 ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                         ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))						 
						 //->where("a.date >= '$fromdate'")
						// ->where("a.date <= '$todate'")
						 ->where("a.idvenuedateschedule=?",$idscheduler)
						 ->where("a.Active=?",1)
						 ->where("a.Reserveflag =?",1)
						 ->where("a.Allotedseats=0")
						 ->order("a.date")
						 ->order("b.centername");
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);			
	   return $larrResult;
	}
	public function  fnupdatescheduler($formdata)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_schedulerexception";
		$postData =  array('Date' => $formdata['Date'],
           					'idstate' => $formdata['idstate'],           				                           
                            'idcity' => $formdata['idcity'],	
            				'idvenue' =>$formdata['idcenter'],
                            'constraintname'=>$formdata['constraintname'],
            				'Active' =>$formdata['Active'],			
		);
		$lobjDbAdpt->insert($table,$postData);
	    $data = array(
					   'Totalcapacity'=>$formdata['Total'],
					   'Active'=>$formdata['Active'],
					   );
		$where1 = "idvenuedateschedule = ".$formdata['Idscheduler'];
		$lobjDbAdpt->update('tbl_venuedateschedule', $data, $where1);
	}
	 public function fngetVenueDetailsforidcenter($idscheduler)
	 {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	$lstrSelect = $lobjDbAdpt->select()
     	->from(array("tbl_center"=>"tbl_center"),array("tbl_center.idcenter","tbl_center.state","tbl_center.city","tbl_center.centername"))
		 ->join(array('b'=>'tbl_venuedateschedule'),'b.idvenue = tbl_center.idcenter',array("b.*"));     
     	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
     	return $larrResult;
     }
	 public function fnajaxgetcenternames($ldfromdate,$ldtodate)
	 {
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
									 ->join(array("b"=>"tbl_venuedateschedule"),"a.idcenter = b.idvenue",array(""))
									// ->where("b.Allotedseats > 0")
									// ->where("b.Active = 1")
									 ->where("b.date >= '$ldfromdate'")
									 ->where("b.date <= '$ldtodate'")
									 ->group("a.idcenter")
									 ->order("a.centername");									
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	public function fngetvenues()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("b"=>"tbl_center"),array("b.idcenter","b.centername"))
								->order("b.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
									 
	}
	public function fngetvenuedetailsforcenterid($idvenue)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("b"=>"tbl_center"),array("b.idcenter","b.centername"))
								 ->where("b.idcenter =?",$idvenue);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
									 
	}
	public function fngetschdeuledatesforchangeseats($larrformData)
	{
	    if($larrformData['Date']) $fromdate = $larrformData['Date'];
		if($larrformData['Schedulertodate']) $todate = $larrformData['Schedulertodate'];
		if($larrformData['Day']) $day = $larrformData['Day'];
		if($larrformData['Venues']) $center = $larrformData['Venues'];		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a"=>"tbl_venuedateschedule"),array("a.Reserveflag","a.idvenuedateschedule","a.Totalcapacity","a.Allotedseats","a.Active as Schedulerflag","DATE_FORMAT(a.date,'%d-%m-%Y') AS Date","a.date","DAYNAME(a.date) AS dayname"))
								  ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                                 ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))	
		                         ->where("a.date >= '$fromdate'")
								 ->where("a.date <= '$todate'")								
								 ->where("a.Reserveflag = 1")
								 ->where("a.Active = 1");
		if($larrformData['Day']) $lstrSelect->where("DAYNAME(a.date) = '$day'");
		 $lstrSelect->order("a.date");       	
		if($larrformData['Venues']=='')
		{
			$lstrSelect->order("b.centername");	
		}	
		else 
		{
			 $lstrSelect->where("a.idvenue =?",$center);
		}
$lstrSelect->order("c.idmangesession");	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);		
		return $larrResult;
	}
	public function fngetschdeuledates($larrformData)
	{
		if($larrformData['Date']) $fromdate = $larrformData['Date'];
		if($larrformData['Schedulertodate']) $todate = $larrformData['Schedulertodate'];
		if($larrformData['Day']) $day = $larrformData['Day'];
		if($larrformData['Venues']) $center = $larrformData['Venues'];
		if($larrformData['Action'] ==1) 
		{
		   $active = 1;
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a"=>"tbl_venuedateschedule"),array("a.Reserveflag","a.date","a.idvenuedateschedule","a.Totalcapacity","a.Allotedseats","a.Active as Schedulerflag","DATE_FORMAT(a.date,'%d-%m-%Y') AS Date","a.date","DAYNAME(a.date) AS dayname"))
								  ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                                 ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))	
		                         ->where("a.date >= '$fromdate'")
								 ->where("a.date <= '$todate'")								
								 ->where("a.Reserveflag = 1")
								 ->where("a.Active = ?",$active);
					if($larrformData['Day']) $lstrSelect->where("DAYNAME(a.date) = '$day'");
					 $lstrSelect->order("a.date");						
					if($larrformData['Venues']=='')
					{
						$lstrSelect->order("b.centername");	
					}	
					else 
					{
						 $lstrSelect->where("a.idvenue =?",$center);
					}
$lstrSelect->order("c.idmangesession");	
		   
		}
		if($larrformData['Action'] ==0) 
		{
		   $active = 0;
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a"=>"tbl_venuedateschedule"),array("a.Reserveflag","a.date","a.idvenuedateschedule","a.Totalcapacity","a.Allotedseats","a.Active as Schedulerflag","DATE_FORMAT(a.date,'%d-%m-%Y') AS Date","a.date","DAYNAME(a.date) AS dayname"))
								  ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                                 ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))	

 ->joinleft(array('d'=>'tbl_schedulerexception'),'d.Date = a.date and d.idvenue = a.idvenue and  d.Idsession = a.idsession and a.Active!=0',array("d.constraintname"))	
		                         ->where("a.date >= '$fromdate'")
								 ->where("a.date <= '$todate'")								
								 ->where("a.Reserveflag = 0");
								 //->ORwhere("a.Active = ?",$active);
					if($larrformData['Day']) $lstrSelect->where("DAYNAME(a.date) = '$day'");
					 $lstrSelect->order("a.date");
					//if($larrformData['Action']) $lstrSelect->where("a.Active = ?",$active);		
					if($larrformData['Venues']=='')
					{
						$lstrSelect->order("b.centername");	
					}	
					else 
					{
						 $lstrSelect->where("a.idvenue =?",$center);
					}	
					$lstrSelect->order("c.idmangesession");
				   /*$select="select a.Reserveflag,a.date,a.idvenuedateschedule,a.Totalcapacity,a.Allotedseats,a.Active as Schedulerflag,b.*,c.*,
					DATE_FORMAT(a.date,'%d-%m-%Y') AS Date,a.date,DAYNAME(a.date) AS dayname
					from tbl_venuedateschedule as a,tbl_center as b,tbl_managesession as c
					where b.idcenter = a.idvenue and c.idmangesession = a.idsession and a.date >= '$fromdate' and 	a.date <= '$todate' and a.Reserveflag=0";
	                if($larrformData['Venues']=='')
					{
						$select.=" order by b.centername";	
					}	
					else 
					{
						$select.="  and a.idvenue =$center";
					}	
					$select.="union ";
					$select.=" select a.Reserveflag,a.date,a.idvenuedateschedule,a.Totalcapacity,a.Allotedseats,a.Active as Schedulerflag,b.*,c.*,
					DATE_FORMAT(a.date,'%d-%m-%Y') AS Date,a.date,DAYNAME(a.date) AS dayname
					from tbl_venuedateschedule as a,tbl_center as b,tbl_managesession as c
					where b.idcenter = a.idvenue and c.idmangesession = a.idsession and a.date >= '$fromdate' and 	a.date <= '$todate' and a.Active=0";
	                if($larrformData['Venues']=='')
					{
						$select.=" order by b.centername";	
					}	
					else 
					{
						$select.="  and a.idvenue =$center";
					}	
					*/
					
		}		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);		
		return $larrResult;
	}
	public function fnSearchexception($post) //Function for searching the university details
	{ 
		$activeexception = "tbl_schedulerexception.Active = 1";
		$fromdate = $post['Date'];
		$todate =   $post['Schedulertodate'];
		$center = $post['Venues'];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('tbl_schedulerexception' => 'tbl_schedulerexception'),array('tbl_schedulerexception.*'))
			   ->join(array("tbl_center" => "tbl_center"),'tbl_schedulerexception.idvenue = tbl_center.idcenter',array("tbl_center.centername"))			
			    ->where($activeexception)
				->where("tbl_schedulerexception.Date >= '$fromdate'")
				->where("tbl_schedulerexception.Date <= '$todate'")				
				 ->order("tbl_schedulerexception.Date");
				if($center =='')
		        {
			        $select->order("tbl_center.centername");
		        }	
				else 
				{
					 $select->where("tbl_center.idcenter =?",$center);
				}
                				
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	public function  fnupdateschedulerexceptions($formdata,$iduser)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		  
		$table2 = "tbl_schedulerexception";
		for($i =0;$i<count($formdata);$i++)
		{
						 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
						 $where = 'idvenuedateschedule = '.$formdata[$i][0];
						 $postData = array(		
												'Active' => 1,
												'Reserveflag'=>1,										
											);
						 $table = "tbl_venuedateschedule";
						 $lobjDbAdpt->update($table,$postData,$where);
						 $date = $formdata[$i]['1'];
						 $idvenue = $formdata[$i]['2'];
						 $session = $formdata[$i]['5'];				 
						 $larresult = self::fncheckexception($date,$idvenue,$session);
						 if(empty($larresult))
						 {
									$postData3 =  array('Date' => $formdata[$i]['1'],
									  'idvenue' =>$formdata[$i]['2'],
									  'idstate' => $formdata[$i]['3'],           				                           
									  'idcity' => $formdata[$i]['4'],            				
									  'constraintname'=>Holiday,
									  'Active' =>0,
									  'UpdDate'=>date( 'Y-m-d H:i:s' ),
									  'UpdUser'=>$iduser,
									  'Idsession'=>$formdata[$i]['5'],							 
							  );
							 $lobjDbAdpt->insert($table2,$postData3);
								 
						  }
						  else
						  {
								  $where3 = "Date = '".$date."'  AND Idsession ='".$session."' AND 	idvenue ='".$idvenue."'"; 						
								  $postData3 =  array(
													  'Active' =>0,
													  'UpdDate'=>date( 'Y-m-d H:i:s' ),
													  'UpdUser'=>$iduser,                            						 
								   );
								  $lobjDbAdpt->update($table2,$postData3,$where3); 
						  }
	    }
			  
	}
	public function fnupdateschexceptnwithconstaints($formdata,$iduser,$constraint)
	{
	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		  
		    $table2 = "tbl_schedulerexception";
	            for($i =0;$i<count($formdata);$i++)
				{
				 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $where = 'idvenuedateschedule = '.$formdata[$i][0];
				 $postData = array(		
										'Active' => 1,
                                         'Reserveflag'=>0,									
									);
				  $table = "tbl_venuedateschedule";				  
				  $lobjDbAdpt->update($table,$postData,$where);
				  
				  $postData2 =  array( 'constraintname' => $constraint,
				              'Date' => $formdata[$i]['1'],
				              'idvenue' =>$formdata[$i]['2'],
           					  'idstate' => $formdata[$i]['3'],           				                           
                              'idcity' => $formdata[$i]['4'], 				
                              
            				  'Active' =>1,
                              'UpdDate'=>date('Y-m-d H:i:s' ),
                              'UpdUser'=>$iduser,
                              'Idsession'=>$formdata[$i]['5'],							 
		           );				 
		          $lobjDbAdpt->insert($table2,$postData2);
				 }
				 
	}			 
	public function fncheckexception($date,$idvenue,$session)
	{ 
	        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("a"=>"tbl_schedulerexception"),array("a.*"))									
									 ->where("a.idvenue= ?",$idvenue)
									 ->where("a.Idsession = ?",$session)
									 ->where("a.Date =?",$date); 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	
	}
	public function fnupdatescapacity($post,$upduser)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	   
		for($k=0;$k<count($post['idvenue']);$k++)
		{
		    $idvenue=$post['idvenue'][$k];
		    $where = 'idvenuedateschedule = '.$idvenue;
			$postData = array(		
                                    'Totalcapacity'=>$post['Totalseats'][$idvenue],
                                    'UpdDate'=>date( 'Y-m-d H:i:s' ),
									'Upduser'=>$upduser, 
								);
			$table = "tbl_venuedateschedule";				
			$lobjDbAdpt->update($table,$postData,$where);
		
		}
	}
	public function fnGetRegistered($idcenter,$idsession,$exdate)
	{
	 
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.pass","a.Payment","a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("f" => "tbl_venuedateschedule"),'a.Year = f.idvenuedateschedule',array("f.*"))
							 //->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue = '$idcenter'")							
							 ->where("DATE_FORMAT(a.DateTime,'%d-%m-%Y') = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  //->where("a.Payment = 1")
							  //->group("e.IDApplication")
							  ->order("a.FName");
	
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
}
?>