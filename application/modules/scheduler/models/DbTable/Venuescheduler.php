<?php
class Scheduler_Model_DbTable_Venuescheduler extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_center';
	
	//Check Super User Authentication
	public function fncheckSuperUserPwd($passwoord)
	{ 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
						->from(array("a" => "tbl_user"))
						->join(array('b' => 'tbl_definationms'),'a.IdRole = b.idDefinition')
						->where("a.passwd =?",$passwoord)
						->where("b.DefinitionDesc = 'Superadmin'");
						//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}

	public function fngetcenternames()
	{
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 	 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername")) 				 		 ->order("a.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }

	public function fngetsearchdetails($larrformData)
	{
		$lstrFromDate = date("Y-m-d",strtotime($larrformData['Date7']));
		$lstrToDate = date("Y-m-d",strtotime($larrformData['Date8']));
		$venue=$larrformData['Venue'];
		$active=$larrformData['Active'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($active == 1){
                $lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_venuedateschedule"),array("a.*,DATE_FORMAT(date(a.date),'%d-%m-%Y') as formatdate"))
					 ->join(array("b" => "tbl_center"),'a.idvenue = b.idcenter',array("b.centername"))
					 ->join(array("c" =>"tbl_managesession"),'a.idsession=c.idmangesession',array("c.managesessionname"))
					 ->where ("a.Active = 1")
					 ->where("DATE_FORMAT(a.date,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate'");
		if($larrformData['Venue']) $lstrSelect .= " AND a.idvenue = $venue";					  
		}
                else
                  {
			$lstrSelect = " SELECT a.*,DATE_FORMAT(date(a.date),'%d-%m-%Y') AS `formatdate`, `b`.`centername`, `c`.`managesessionname` 					        FROM  `tbl_venuedateschedule` AS `a` 
				        INNER JOIN `tbl_center` AS `b` ON a.idvenue = b.idcenter 
					INNER JOIN `tbl_managesession` AS `c` ON a.idsession=c.idmangesession 
					WHERE (a.Active = '0') AND (DATE_FORMAT(a.date,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate') 
					AND (a.idvenue , a.idsession , a.date) 
					NOT IN( SELECT a.idvenue,c.idmangesession,a.date FROM `tbl_venuedateschedule` AS `a` 
						INNER JOIN `tbl_center` AS `b` ON a.idvenue = b.idcenter 
						INNER JOIN `tbl_managesession` AS `c` ON a.idsession=c.idmangesession 
					WHERE (a.Active = '1') AND (DATE_FORMAT(a.date,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate') )"; 
                if($larrformData['Venue']) $lstrSelect .= " AND a.idvenue = $venue";
                }
                $lstrSelect .= " order by a.date,b.centername";	
	        $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	        return $larrResult;
	}

	public function	fngetvenueschedulerdetails($idvenuedateschedule)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
					->where ("a.idvenuedateschedule = ?" , $idvenuedateschedule);	
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}

	public function	fncheckvalidate($larrresult)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$formatdate=date('Y-m-d', strtotime($larrresult['date']));
		$idvenue=$larrresult['idvenue'];
		$idsession=$larrresult['idsession'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_venuedateschedule"),array("a.*"))
					 ->where ("a.date = ?", $formatdate)
					 ->where ("a.idvenue = ?", $idvenue)
					 ->where ("a.idsession = ?", $idsession)
					 ->where ("a.Active = ?", 1);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);	
		return $larrResult;
		
	}

	public function fnUpdateActiveFieldApproved($larrformdata)
	{
		//echo "<pre/>";print_r($larrformdata);die();
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   	$tableName = "tbl_venuedateschedule";
		if($larrformdata['activevalue']==0)
		{
			for($i = 0; $i<count($larrformdata['idvenuedateschedule']); $i++ )
			{
				$idvenuedateschedule = $larrformdata['idvenuedateschedule'][$i];
				$postData = array('Active' => $larrformdata['activevalue']);
				$where = "idvenuedateschedule='$idvenuedateschedule'";
				//echo "For InActive";echo "<br>";echo "<pre/>";print_r($postData);die();
				$lobjDbAdpt->update($tableName,$postData,$where);
			}
		}
		else
		{
			for($i = 0; $i<count($larrformdata['idvenuedateschedule']); $i++ )
			{
				
				//$lintvalidationresult=self::fncheckvalidate($larrformdata,$larrformdata['idvenuedateschedule']);
				//echo "<pre/>";print_r($lintvalidationresult);die();
				//if(!$lintvalidationresult)
				//{	
					$idvenuedateschedule = $larrformdata['idvenuedateschedule'][$i];
					$postData = array('Active' => $larrformdata['activevalue']);
					$where = "idvenuedateschedule='$idvenuedateschedule'";
					//echo "For Active";echo "<br>";echo "<pre/>";print_r($postData);die();
					//$lobjDbAdpt->update($tableName,$postData,$where);
				/*}
				else
				{
					echo "Don't Allow to Active the Venue Scheduler";die();
				}*/
				
				
			}
		}
	}

}
