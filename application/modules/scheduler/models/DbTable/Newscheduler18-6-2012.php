<?php 
class Scheduler_Model_DbTable_Newscheduler extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_program';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
	public function fnSearchSession($post = array())
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$field7 = "a.Active= ".$post["field7"];
		$select = $db->select() 	
			   ->from(array('a' => 'tbl_newscheduler'),array('a.*'))
			    ->join(array("b"=>"tbl_newmonths"),"a.From=b.idmonth",array("b.MonthName as From"))
										  ->join(array("c"=>"tbl_newmonths"),"a.To=c.idmonth",array("c.MonthName as To"))
			   ->where('a.Year like "%" ? "%"',$post['field3'])			  
			   ->where($field7);
		$result = $db->fetchAll($select);
		return $result;
	}
     public function fngetCourse() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programmaster"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     public function fngetActiveset()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_tosmaster"),array())
								 ->join(array("b"=>"tbl_batchmaster"),'b.IdBatch=a.IdBatch',array('b.BatchName'))
								 ->where("a.Active=1");							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     	
     }
     public function fngetSessionDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_managesession"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
	public function fnfetchvenue()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("a.*"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
	}
   public function fnAddScheduler($larrformdata)
   {
  /* 	echo "<pre/>";
   	print_r($larrformdata);
   	die();*/
   	
   
	      $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_newscheduler";
          $postData = array(	
                             'From'=>$larrformdata['From'],
          					 'To'=>$larrformdata['To'],
          					 'Year'=>$larrformdata['Year'],
            				 'UpdUser' =>$larrformdata['UpdUser'],		
		            		 'UpdDate' =>$larrformdata['UpdDate'],
                             'Description' =>$larrformdata['description'],
          					 'Active'=>1												
						);			
	       $db->insert($table,$postData);
	       $lastid  = $db->lastInsertId("tbl_newscheduler","idnewscheduler");	
	       
	       /////////////////for weekdays////////////////////////////
	        for($i=0;$i<count($larrformdata['iddays']);$i++)
	        {
	          	 $tables = "tbl_newschedulerdays";
	       	     $arryresult['idnewscheduler']= $lastid;
	       	     $arryresult['Days'] = $larrformdata['iddays'][$i];
	       	     $db->insert($tables,$arryresult);
	        }
	       ///////////////////end for weekdays///////////////////
	       
	         /////////////////for course////////////////////////////
	        for($i=0;$i<count($larrformdata['idprogram']);$i++)
	        {
	          	 $tablescourse = "tbl_newschedulercourse";
	       	     $arryresultcourse['idnewscheduler']= $lastid;
	       	     $arryresultcourse['IdProgramMaster'] = $larrformdata['idprogram'][$i];
	       	     $db->insert($tablescourse,$arryresultcourse);
	        }
	       ///////////////////end for course///////////////////
	       
	         /////////////////for venue session////////////////////////////
	        for($i=0;$i<count($larrformdata['idsession']);$i++)
	        {
	          	 $tablessession = "tbl_newschedulersession";
	       	     $arryresultsession['idnewscheduler']= $lastid;
	       	     $arryresultsession['idmanagesession'] = $larrformdata['idsession'][$i];
	       	     $db->insert($tablessession,$arryresultsession);
	        }
	       ///////////////////end for venue session///////////////////
	       
	         /////////////////for venue////////////////////////////
	        for($i=0;$i<count($larrformdata['idcenter']);$i++)
	        {
	          	 $tablescenter = "tbl_newschedulervenue";
	       	     $arryresultcenter['idnewscheduler']= $lastid;
	       	     $arryresultcenter['idvenue'] = $larrformdata['idcenter'][$i];
	       	     $db->insert($tablescenter,$arryresultcenter);
	        }
	       ///////////////////end for venue///////////////////	     
   if($larrformdata['From']<10)
				        	{
				        		$larrformdata['From']='0'.$larrformdata['From'];
				        	}
				        	if($larrformdata['To']<10)
				        	{
				        		$larrformdata['To']='0'.$larrformdata['To'];
				        	}
		   	  for($j=0;$j<count($larrformdata['idcenter']);$j++)
			        {
			        	
			        	
			   /*   $Totalcapacity =0;  	
				 $lstrSelect = $db->select()
										  ->from(array("a" =>"tbl_center"),array("a.NumberofSeat"))
										  ->where('a.idcenter = ?',$larrformdata['idcenter'][$j]);	
				$resultArray = $db->fetchRow($lstrSelect);
				if($resultArray['NumberofSeat'])$Totalcapacity = $resultArray['NumberofSeat'];*/
			      	
			        	$Totalcapacity=$larrformdata['capacity'][$larrformdata['idcenter'][$j]];
			          for($k=0;$k<count($larrformdata['idsession']);$k++)
				        {
				        	
				        	

				        	$lastdate = self::lastday($larrformdata['To'],$larrformdata['Year']);
				        	$fromdates = $larrformdata['Year'].'-'.$larrformdata['From'].'-'.'01';
				        	$todates = $lastdate;
				        	
				        	 $startDate = $fromdates;
							 $endDate = $todates;
							$endDate = strtotime($endDate);
							
							 for($m=0;$m<count($larrformdata['iddays']);$m++)
	       					 {
	        	
	       					    $exception = array();
	                            $exceptionvenue =self::getdaysfromexception($larrformdata['idcenter'][$j]);
	                          
	                            $ccnts =0;
	         					foreach($exceptionvenue as $exceptionvenue1){
	         						$exception[$ccnts] = $exceptionvenue1['Date'];
	         						$ccnts++;
	         					}
                        if($ccnts==0)
                        {
                        	$exception[0]=0;
                        }
                        

								switch ($larrformdata['iddays'][$m])
								{
								  Case 1:
										for($i = strtotime('Monday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											
						   				   $resultss = in_array(date('Y-m-d', $i),$exception);
						   				  
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>1,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								                              'idnewscheduler'=>	$lastid												
														);			
									       $db->insert($table,$postData);
						   				  }
										}
										 break;
	
										 Case 2:
										for($i = strtotime('Tuesday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											
						   				    //date('Y-m-d', $i);
															   				   $resultss = in_array(date('Y-m-d', $i),$exception);;
														   				
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>1,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								           'idnewscheduler'=>	$lastid												
														);	
															
									       $db->insert($table,$postData);
						   				  }
										}
										 break;
										 Case 3:
										for($i = strtotime('Wednesday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>1,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								                             'idnewscheduler'=>	$lastid											
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
										 Case 4:
										for($i = strtotime('Thursday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>1,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								           'idnewscheduler'=>	$lastid												
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
										 Case 5:
										for($i = strtotime('Friday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>1,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								           'idnewscheduler'=>	$lastid												
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
										 Case 6:
										for($i = strtotime('Saturday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>1,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								                              'idnewscheduler'=>	$lastid												
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
										 Case 7:
										for($i = strtotime('Sunday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>1,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								                             'idnewscheduler'=>	$lastid												
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
		       					  }
	       					 }
	       					 
				        }
			          	
			        }
			        
		   		
		  
			        
	       ////////////////for date//////////////////////////

	   
	        
   }
   
   
   
   
 function fninsertintoSchedulerdatevenue($larrformdata,$lintidscheduler)
   {
   	
   	
   	
   	 $db = Zend_Db_Table::getDefaultAdapter();
   	//print_r($larrformdata);die();
   	
   	
   	if($larrformdata['From']<10)
				        	{
				        		$larrformdata['From']='0'.$larrformdata['From'];
				        	}
				        	if($larrformdata['To']<10)
				        	{
				        		$larrformdata['To']='0'.$larrformdata['To'];
				        	}
		   	  for($j=0;$j<count($larrformdata['idcenter']);$j++)
			        {
			      $Totalcapacity =0;  	
				 $lstrSelect = $db->select()
										  ->from(array("a" =>"tbl_center"),array("a.NumberofSeat"))
										  ->where('a.idcenter = ?',$larrformdata['idcenter'][$j]);	
				$resultArray = $db->fetchRow($lstrSelect);
				if($resultArray['NumberofSeat'])$Totalcapacity = $resultArray['NumberofSeat'];
			      	
			        	
			          for($k=0;$k<count($larrformdata['idsession']);$k++)
				        {
				        	
				        	

				        	$lastdate = self::lastday($larrformdata['To'],$larrformdata['Year']);
				        	$fromdates = $larrformdata['Year'].'-'.$larrformdata['From'].'-'.'01';
				        	$todates = $lastdate;
				        	
				        	 $startDate = $fromdates;
							 $endDate = $todates;
							$endDate = strtotime($endDate);
							
							 for($m=0;$m<count($larrformdata['iddays']);$m++)
	       					 {
	        	
	       					    $exception = array();
	                            $exceptionvenue =self::getdaysfromexception($larrformdata['idcenter'][$j]);
	                          
	                            $ccnts =0;
	         					foreach($exceptionvenue as $exceptionvenue1){
	         						$exception[$ccnts] = $exceptionvenue1['Date'];
	         						$ccnts++;
	         					}
                        if($ccnts==0)
                        {
                        	$exception[0]=0;
                        }
                        

								switch ($larrformdata['iddays'][$m])
								{
								  Case 1:
										for($i = strtotime('Monday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											
						   				   $resultss = in_array(date('Y-m-d', $i),$exception);
						   				  
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>0,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								                             'idnewscheduler'=>	$lintidscheduler										
														);			
									       $db->insert($table,$postData);
						   				  }
										}
										 break;
	
										 Case 2:
										for($i = strtotime('Tuesday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											
						   				    //date('Y-m-d', $i);
															   				   $resultss = in_array(date('Y-m-d', $i),$exception);;
														   				
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>0,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								                'idnewscheduler'=>	$lintidscheduler											
														);	
															
									       $db->insert($table,$postData);
						   				  }
										}
										 break;
										 Case 3:
										for($i = strtotime('Wednesday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>0,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								           'idnewscheduler'=>	$lintidscheduler												
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
										 Case 4:
										for($i = strtotime('Thursday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>0,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								           'idnewscheduler'=>	$lintidscheduler													
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
										 Case 5:
										for($i = strtotime('Friday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>0,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								           'idnewscheduler'=>	$lintidscheduler													
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
										 Case 6:
										for($i = strtotime('Saturday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>0,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								           'idnewscheduler'=>	$lintidscheduler													
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
										 Case 7:
										for($i = strtotime('Sunday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
										{
											$resultss = in_array(date('Y-m-d', $i),$exception);
						   				  if($resultss != 1) {
								          $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>date('Y-m-d', $i),
								          					 'idsession'=>$larrformdata['idsession'][$k],
								          					 'Active'=>1,
								            				 'Upduser' =>1,		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>0,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>0,
								                             'idvenue' =>$larrformdata['idcenter'][$j],
								           'idnewscheduler'=>	$lintidscheduler													
														);			
									       $db->insert($table,$postData);
										}
										}
										 break;
		       					  }
	       					 }
	       					 
				        }
			          	
			        }
			        
   	
   	
   	
   	
   	
   	
   	
   }
   
function createDateRangeArray($strDateFrom,$strDateTo)
{
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    return $aryRange;
}
	
function lastday($month, $year) {
  		if (empty($month)) {
      		$month = date('m');
   		}
   	if (empty($year)) {
      $year = date('Y');
   }
   	$result = strtotime("{$year}-{$month}-01");
   	$result = strtotime('-1 second', strtotime('+1 month', $result));
   	return date('Y-m-d', $result);
}
   public function fnGetScheduler()
   {
   
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
										  ->join(array("b"=>"tbl_newmonths"),"a.From=b.idmonth",array("b.MonthName as From"))
										  ->join(array("c"=>"tbl_newmonths"),"a.To=c.idmonth",array("c.MonthName as To"))
										 ->where("a.Active=1");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
	
   }
   
   /*
    * functionto get the scheduler details
    */
   public function fnGetSchedulerDetails($idscheduler)
   {  
   	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;
   }
   
   
 public function fnupdateScheduler($lintidscheduler,$formData)
   {  
   	//echo "abc";die();
   	 /*  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;*/
		
		
		
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			   $where = 'idnewscheduler = '.$lintidscheduler;
			  $postData = array(		
			                'Description'=>$formData['description'],
							'Active' => $formData['Active']														
						);
				$table = "tbl_newscheduler"; 
	            $lobjDbAdpt->update($table,$postData,$where);
		
		
   }
   
   
   
    /*
    * function to get the scheduler days
    */
    public function fnGetSchedulerDays($idscheduler)
    {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newschedulerdays"),array("a.*"))
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     public function fngetDaysDetails()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_days"),array("a.*"));						 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
    /*
    * function to get the scheduler sessions
    */
    public function fnGetSchedulerSession($idscheduler)
    {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newschedulersession"),array("a.*"))								 
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
    /*
    * function to get the scheduler sessions
    */
    public function fnGetSchedulerCourse($idscheduler)
    {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newschedulercourse"),array("a.*"))								 
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
    /*   
     * function to get the scheduler sessions
    */
    public function fnGetSchedulerVenue($idscheduler)
    {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newschedulervenue"),array("a.*"))								 
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     public function fnGetMonthList()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"))								 
								 ->where("a.idmonth >=(SELECT month(curdate( )))");						 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     	
     }
  public function fnGetAllMonthList()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"));			 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     	
     }
     
 public function fnGetToMonthList($idmonth)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"))
								  ->where("a.idmonth >= ?",$idmonth);			 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     	
     }
     
     
public function fnGetfromList($idmonth)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName as From"))
								  ->where("a.idmonth = ?",$idmonth);			 						
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;
     	
     }
     
public function fnGettoList($idmonth)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName as To"))
								  ->where("a.idmonth = ?",$idmonth);			 						
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;
     	
     }

     public function getdaysfromexception($idvenue)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_schedulerexception"),array("a.Date"))
								 
								  ->where("a.idvenue = ?",$idvenue)
								  ->group("a.Date");			 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     	
     }
     
     public function fncheckidsechduler($idsec)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	     $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_venuedateschedule"),array("a.idnewscheduler"))
								 ->where("a.idnewscheduler = ?",$idsec);			 						
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;
     }
     
}
?>