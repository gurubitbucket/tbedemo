<?php
class Scheduler_Model_DbTable_Venueexamdetails extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_center';
	public function fnPagination($larrresult,$page,$lintpagecount) // Function for pagination
	{ 
			$paginator = Zend_Paginator::factory($larrresult); //instance of the pagination
			$paginator->setItemCountPerPage($lintpagecount);
			$paginator->setCurrentPageNumber($page);
			return $paginator;
	}
	public function getvenuelist()
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
					 				 ->order("a.centername");
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	   return $larrResult;
	}
	public function fnGetsearchdetails($fromdate,$todate,$venue,$active)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DATE_FORMAT(a.date,'%d-%m-%Y') as date","DAYNAME(a.date) AS dayname"))						
						 ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                         ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))						 
						 ->where("a.date >= '$fromdate'")
						 ->where("a.date <= '$todate'")
						 ->where("a.idvenue=?",$venue)
						 ->where("a.Active=?",$active)
						 ->where("a.Reserveflag =?",1)
						 ->order("a.date")
						 ->order("b.centername");
							
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
	
	}
	public function fngetcenteralloteddetails($lintidcenter,$ldtedate,$lintidsess,$active)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("z"=>"tbl_studentapplication"),array("z.*"))						
						 ->join(array("a"=>"tbl_venuedateschedule"),'a.date = z.DateTime',array("a.*","DATE_FORMAT(a.date,'%d-%m-%Y') as date"))
						 ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                         ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))						 
						// ->where("z.DateTime=?",$ldtedate)
						 ->where("a.idvenue=?",$lintidcenter)
						// ->where("c.idmangesession=?",$lintidsess)
						 //->where("a.Active=?",$active)
						 ->where("a.Reserveflag =?",1)
						 ->order("z.FName");
	    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	    return $larrResult;
	}
	public function fnupdatestatus($larrresult,$activestatus)
	{
	   //echo count($larrresult);die();
	    if(count($larrresult) >= 1)
		{
		    if($activestatus == 0)
			{
				for($i =0;$i<count($larrresult);$i++)
				{
				 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $where = 'idvenuedateschedule = '.$larrresult[$i];
				 $postData = array(		
										'Active' => 1														
									);
				 $table = "tbl_venuedateschedule";
				 $lobjDbAdpt->update($table,$postData,$where);
				 }
			 }
			 else
			 {
			    for($i =0;$i<count($larrresult);$i++)
				{
				 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $where = 'idvenuedateschedule = '.$larrresult[$i];
				 $postData = array(		
										'Active' => 0														
									);
				 $table = "tbl_venuedateschedule";
				 $lobjDbAdpt->update($table,$postData,$where);
				 }
			    
			 }
		}	 
	}
}