<?php 
class Scheduler_Model_DbTable_Sessionconstraints extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_program';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetSessionDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_schedulerexception"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     public function fngetVenueDetails(){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	$lstrSelect = $lobjDbAdpt->select()
     	->from(array("tbl_center"=>"tbl_center"),array("tbl_center.idcenter","tbl_center.state","tbl_center.city","tbl_center.centername"))
     	->order("tbl_center.idcenter");
     	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
     	return $larrResult;
     }

     public function fngetcenters(){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	$lstrSelect = $lobjDbAdpt->select()
     	->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername"))
     	->order("a.centername");
     	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
     	return $larrResult;
     }

     public function fngetDaysDetails()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	   	             ->from(array("a"=>"tbl_days"),array("key"=>"a.iddays","value"=>"a.Days"));			 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
public function fngetSateDetails()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	   	             ->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
	   	             ->where("a.idCountry=121")
	   	             ->order("a.StateName"); 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     
     
public function fnGetVenueList($lintIdCountry)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	   	             ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
	   	             ->where("a.country=121")
	   	             ->where("a.city=?",$lintIdCountry)
	   	             ->where("a.Active=1")
	   	             ->order("a.centername"); 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     

 public function fnaddSessionconstrains($formData) { //Function for adding the University details to the table
    	$db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_schedulerexception";
          if($formData['Active']==1)
          {
          	 $reserveflag =0;
          }
          else 
          {
          	 $reserveflag =1;
          }
         
          for($lintcenter=0;$lintcenter<count($formData['idcenters']);$lintcenter++)
          {
            $postData = array(		
							'Date' => $formData['Date'],	
           					'idstate' => $formData['idstate'][$formData['idcenters'][$lintcenter]],           				                           
                            'idcity' => $formData['idcity'][$formData['idcenters'][$lintcenter]],	
            				'idvenue' =>$formData['idcenters'][$lintcenter],
                            'constraintname'=>$formData['constraintname'],
            				'Active' =>$formData['Active'],			
						);	
	      $db->insert($table,$postData);
	      
	       $data = array(
					   'Reserveflag'=>$reserveflag);
	     $where['date = ? ']= $formData['Date'];
	     $where['idvenue = ? ']= $formData['idcenters'][$lintcenter];
		 $db->update('tbl_venuedateschedule', $data, $where);
          }
	}
    
public function fnSearchSession($post) { //Function for searching the university details
		$field7 = "tbl_schedulerexception.Active = ".$post["field7"];
		$field10=$post['field10'];
		$field19=$post['field19'];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('tbl_schedulerexception' => 'tbl_schedulerexception'),array('tbl_schedulerexception.*'))
			   ->join(array("tbl_center" => "tbl_center"),'tbl_schedulerexception.idvenue = tbl_center.idcenter',array("tbl_center.centername"))
			   ->where('tbl_schedulerexception.constraintname  like "%" ? "%"',$post['field3'])
			    ->where($field7);
			   if(!empty($field10))$select=$select->where("DATE_FORMAT(tbl_schedulerexception.Date,'%Y-%m-%d')='$field10'");
			   if(!empty($field19))$select=$select->where("tbl_center.idcenter='$field19'");
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
public function  fnGetcityList($idstate)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
         	$select = $db->select()
			   ->from(array('a' => 'tbl_city'),array("key"=>"a.idCity","value"=>"CityName"))
			   ->where('a.idState=?',$idstate);
		$result = $db->fetchAll($select);
		return $result;
	}
	
	public function fnGetsessionList($IdProgram){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_schedulerexception"),array("a.constraintname","a.Date","a.idstate as state","a.idcity as city","a.Active"))
				 				   ->join(array("c"=>"tbl_state"),'a.idstate=c.idState',array("c.StateName as idstate"))
				 				 ->where("a.idschedulerconstraints = ?",$IdProgram);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
	
	
	
	public function fnupdateSession($formData) { //Function for updating the university
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$iddate=$formData['Date'];
		$idvenue=$formData['idvenue'];
		if($formData['Active']==1)
		{
			$reserveflag =0;
		}
		else
		{
			$reserveflag =1;
		}
		$data =  array('Date' => $formData['Date'],
           					'idstate' => $formData['idstate'],           				                           
                            'idcity' => $formData['idcity'],	
            				'idvenue' =>$formData['idvenue'],
                            'constraintname'=>$formData['constraintname'],
            				'Active' =>$formData['Active'],			
		);
		$where['idschedulerconstraints = ? ']= $formData['idschedulerconstraints'];
		$db->update('tbl_schedulerexception', $data, $where);

		///////////////////venue scheduler days////////////////////////////
		$data = array(
					   'Reserveflag'=>$reserveflag);
		$where1 = "date = '".$iddate."'  AND idvenue ='".$idvenue."'";
		$db->update('tbl_venuedateschedule', $data, $where1);
	}
	public function fnGetsearchdetails($formdata)
	{
	   $fromdate = $formdata['Date'];
	   $todate = $formdata['Schedulertodate'];
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DATE_FORMAT(a.date,'%d-%m-%Y') as date","DAYNAME(a.date) AS dayname"))						
						 ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                         ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))						 
						 ->where("a.date >= '$fromdate'")
						 ->where("a.date <= '$todate'")
						// ->where("a.idvenue=?",$venue)
						 ->where("a.Active=?",1)
						 ->where("a.Reserveflag =?",1)
						 ->order("a.date")
						 ->order("b.centername");
							
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);			
	   return $larrResult;
	
	}
	public function fngetschedulerdetails($idscheduler)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DAYNAME(a.date) AS dayname"))						
						 ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                         ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))						 
						 //->where("a.date >= '$fromdate'")
						// ->where("a.date <= '$todate'")
						 ->where("a.idvenuedateschedule=?",$idscheduler)
						 ->where("a.Active=?",1)
						 ->where("a.Reserveflag =?",1)
						 ->order("a.date")
						 ->order("b.centername");
							
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);			
	   return $larrResult;
	}
	public function  fngetschedulerresult($idscheduler)
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_venuedateschedule"),array("a.*","DAYNAME(a.date) AS dayname"))						
						 ->join(array('b'=>'tbl_center'),'b.idcenter = a.idvenue',array("b.*"))
                         ->join(array('c'=>'tbl_managesession'),'c.idmangesession = a.idsession',array("c.*"))						 
						 //->where("a.date >= '$fromdate'")
						// ->where("a.date <= '$todate'")
						 ->where("a.idvenuedateschedule=?",$idscheduler)
						 ->where("a.Active=?",1)
						 ->where("a.Reserveflag =?",1)
						 ->where("a.Allotedseats=0")
						 ->order("a.date")
						 ->order("b.centername");
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);			
	   return $larrResult;
	}
	public function  fnupdatescheduler($formdata)
	{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_schedulerexception";
		$postData =  array('Date' => $formdata['Date'],
           					'idstate' => $formdata['idstate'],           				                           
                            'idcity' => $formdata['idcity'],	
            				'idvenue' =>$formdata['idcenter'],
                            'constraintname'=>$formdata['constraintname'],
            				'Active' =>$formdata['Active'],			
		);
		$lobjDbAdpt->insert($table,$postData);
	    $data = array(
					   'Totalcapacity'=>$formdata['Total'],
					   'Active'=>$formdata['Active'],
					   );
		$where1 = "idvenuedateschedule = ".$formdata['Idscheduler'];
		$lobjDbAdpt->update('tbl_venuedateschedule', $data, $where1);
	}
	 public function fngetVenueDetailsforidcenter($idscheduler){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	$lstrSelect = $lobjDbAdpt->select()
     	->from(array("tbl_center"=>"tbl_center"),array("tbl_center.idcenter","tbl_center.state","tbl_center.city","tbl_center.centername"))
		 ->join(array('b'=>'tbl_venuedateschedule'),'b.idvenue = tbl_center.idcenter',array("b.*"));
     	//->order("tbl_center.idcenter");
     	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
     	return $larrResult;
     }
}
?>