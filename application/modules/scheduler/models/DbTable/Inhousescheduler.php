<?php 
class Scheduler_Model_DbTable_Inhousescheduler extends Zend_Db_Table_Abstract
{
  
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    

     public function fngetCourse() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programmaster"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
   
    
	
public function fngetdayofdate($iddate)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	    $sql = "select DAYOFWEEK('$iddate') as days"; 
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
       
   public function fnAddScheduler($larrformdata)
   {
    //echo "<pre>";
	//print_r($larrformdata);
   	$splitdate = explode('-',$larrformdata['Date']);
	//print_r($splitdate);die();
   	$larrformdata['Year'] = $splitdate[0];
   	$larrformdata['To']=$larrformdata['From'] = $splitdate['1'];
/*   	print_R($splitdate[1]);
   	print_R($splitdate[2]);	
   	echo "<pre/>";
   	print_r($larrformdata);*/
   
   
	      $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_newscheduler";
          $postData = array(	
                             'From'=>$larrformdata['From'],
          					 'To'=>$larrformdata['To'],
          					 'Year'=>$larrformdata['Year'],
            				 'UpdUser' =>$larrformdata['UpdUser'],		
		            		 'UpdDate' =>$larrformdata['UpdDate'],
                             'Description' =>$larrformdata['description'],
          					 'Active'=>1												
						);			
	       $db->insert($table,$postData);
	      $lastid  = $db->lastInsertId("tbl_newscheduler","idnewscheduler");	
	       
	       
	       
	       
   $idexamday=self::fngetdayofdate($larrformdata['Date']);
			
			
			if($idexamday['days']==1)
			{
				$idexamday['days']= 7;
			}
			else 
			{
			$idexamday['days']=$idexamday['days']-1;	
			}
	       /////////////////for weekdays////////////////////////////
	       
	          	 $tables = "tbl_newschedulerdays";
	       	     $arryresult['idnewscheduler']= $lastid;
	       	     $arryresult['Days'] = $idexamday['days'];
	       	     $db->insert($tables,$arryresult);
	      
	       ///////////////////end for weekdays///////////////////
	       
	         /////////////////for course////////////////////////////
	        for($i=0;$i<count($larrformdata['idprogram']);$i++)
	        {
	          	 $tablescourse = "tbl_newschedulercourse";
	       	     $arryresultcourse['idnewscheduler']= $lastid;
	       	     $arryresultcourse['IdProgramMaster'] = $larrformdata['idprogram'][$i];
	       	     $db->insert($tablescourse,$arryresultcourse);
	        }
	       ///////////////////end for course///////////////////
	       
	         /////////////////for venue session////////////////////////////
	        for($i=0;$i<count($larrformdata['session']);$i++)
	        {
	          	 $tablessession = "tbl_newschedulersession";
	       	     $arryresultsession['idnewscheduler']= $lastid;
	       	     $arryresultsession['idmanagesession'] = $larrformdata['session'][$i];
	       	     $db->insert($tablessession,$arryresultsession);
	        }
	       ///////////////////end for venue session///////////////////
	       
	         /////////////////for venue////////////////////////////
	        for($i=0;$i<count($larrformdata['venarr']);$i++)
	        {
	          	 $tablescenter = "tbl_newschedulervenue";
	       	     $arryresultcenter['idnewscheduler']= $lastid;
	       	     $arryresultcenter['idvenue'] = $larrformdata['venarr'][$i];
	       	     $db->insert($tablescenter,$arryresultcenter);
	        }

		   	  for($j=0;$j<count($larrformdata['venarr']);$j++)
			        {
			        	$Totalcapacity=$larrformdata['caparr'][$j];
			             for($k=0;$k<count($larrformdata['session']);$k++)
				        {
 										  $table = "tbl_venuedateschedule";
								          $postData = array(	
								                             'date'=>$larrformdata['Date'],
								          					 'idsession'=>$larrformdata['session'][$k],
								          					 'Active'=>0,
								            				 'Upduser' =>$larrformdata['UpdUser'],		
										            		 'UpdDate' =>date('Y-m-d H:i:s'),
								                             'idprogram' =>10,
								          					 'Totalcapacity'=>$Totalcapacity,
								          					 'Allotedseats' =>0,		
										            		 'Reserveflag' =>1,
								                             'idvenue' =>$larrformdata['venarr'][$j],
								                              'idnewscheduler'=>$lastid												
														);			
									       $db->insert($table,$postData);		        	
	       					 
				        }
			          	
			        } 
	        
   }
   
   public function fngetexceptiondate($iddate)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulerexception"),array("a.*"))
										  ->where("Date=?",$iddate)
										    ->where("Active=1");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;	 
   }
   
   public function fngetsession($lintidsession,$lintsess)
   {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_tempsessions"),array("a.*"))
										  ->where("idsession=?",$lintidsession)
										    ->where("session='$lintsess'");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;	 
   	
   }
   
   public function fngettempvenue($lintidvenue,$lintsess)
   {
   	
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_tempvenues"),array("a.*"))
										  ->where("idvenues =?",$lintidvenue)
										    ->where("session='$lintsess'");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;	 
   	
   }
   
   public function fngetpresentschedulers($larrformdata,$idsessions)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
							  ->from(array("a" =>"tbl_venuedateschedule"),array("a.*"))
							  ->where("date=?",$larrformdata['Date'])
							  ->where("idsession in($idsessions)")
							  ->where("Reserveflag=1")
							  ->where("Active=1");
							 // echo $lstrSelect;die();
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;	 
   }
   
  
   public function fninserttempsession($lintidsession,$lintsess)
   {
   	$db = Zend_Db_Table::getDefaultAdapter();
   	$tables = "tbl_tempsessions";
	$arryresult['idsession']= $lintidsession;
	$arryresult['session'] = $lintsess;
	$db->insert($tables,$arryresult);
   }
   
   public function fnajaxgettempsession($idtemp)
   {
   	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
	                            ->from(array("a"=>"tbl_tempsessions"),array("a.*"))
	                            ->join(array("b"=>"tbl_managesession"),"a.idsession = b.idmangesession",array("b.*"))
	                            ->where("a.session ='$idtemp'");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
   }
   public function fndeleteaction($id)
   {
   	    $db = Zend_Db_Table::getDefaultAdapter();
   		$lstrselect  = "Delete from tbl_tempsessions where 	session = '$id'";
		$db->query($lstrselect);
   }
   
   
  
	public function fninserttempvenue($lintidvenue,$lintcapacity,$lintsess)
   {
   	$db = Zend_Db_Table::getDefaultAdapter();
   	$tables = "tbl_tempvenues";
	$arryresult['idvenues']= $lintidvenue;
	$arryresult['session'] = $lintsess;
	$arryresult['capacity'] = $lintcapacity;
	$db->insert($tables,$arryresult);
   }
	public function fnajaxgettempvenues($idtemp)
   {
   	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
	                            ->from(array("a"=>"tbl_tempvenues"),array("a.*"))
	                            ->join(array("b"=>"tbl_center"),"a.idvenues = b.idcenter",array("b.*"))
	                            ->where("a.session ='$idtemp'");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
   }
  
 public function fndeletetempvenue($id)
   {
   	    $db = Zend_Db_Table::getDefaultAdapter();
   		$lstrselect  = "Delete from tbl_tempvenues where session = '$id'";
		$db->query($lstrselect);
   }
   //CONCAT( IFNULL(d.Year, '' ) , '-', IFNULL( a.Exammonth, '' ) , '-', IFNULL(  a.Examdate, '' ) ),'%d-%m-%Y') AS Date
   public function fngetdate($dat){
        $db = Zend_Db_Table::getDefaultAdapter();
   		$lstrselect  = "Select Concat(DAYNAME('$dat'),' ',DAY('$dat'),' ',MONTHNAME('$dat'),' ','Schedule') as date"; 
		$result = $db->fetchRow($lstrselect);
		
		
		return $result;
   }
   public function fnSearchSession($post = array())
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$field7 =$post["field7"];
		$ser=$post['field3'];
		$select ="SELECT a . * , b.MonthName AS `From` , c.MonthName AS `To`
FROM tbl_newscheduler AS a, tbl_newmonths AS b, tbl_newmonths AS c
WHERE a.From = b.idmonth
AND a.To = c.idmonth
AND a.Active =$field7 and a.Year like '%$ser%'
AND CAST( a.To AS UNSIGNED ) >= month( curdate( ) )
AND CAST( a.Year AS UNSIGNED ) >= Year( curdate( ) )
UNION
SELECT a . * , b.MonthName AS `From` , c.MonthName AS `To`
FROM tbl_newscheduler AS a, tbl_newmonths AS b, tbl_newmonths AS c
WHERE a.From = b.idmonth
AND a.To = c.idmonth
AND a.Active =$field7 and a.Year like '%$ser%'
AND CAST( a.Year AS UNSIGNED ) > Year( curdate( ) )";  
	//	echo $select;die();
	/*	$db->select() 	
			   ->from(array('a' => 'tbl_newscheduler'),array('a.*'))
			    ->join(array("b"=>"tbl_newmonths"),"a.From=b.idmonth",array("b.MonthName as From"))
										  ->join(array("c"=>"tbl_newmonths"),"a.To=c.idmonth",array("c.MonthName as To"))
			   ->where('a.Year like "%" ? "%"',$post['field3'])			  
			   ->where($field7)
			   	 ->where("CAST( a.To AS UNSIGNED )>=month(curdate())")
				->where("CAST( a.Year AS UNSIGNED )>=Year(curdate())");*/
		$result = $db->fetchAll($select);
		return $result;
	}
	
  public function fngetcenternames($idcenters)
   {
   	       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
										   ->join(array('b'=>'tbl_takafuloperator'),'b.idtakafuloperator = a.centercode',array('b.TakafulName'))
										  ->where("a.idcenter not in ($idcenters)")
										   ->where("b.TakafulField6=1")
										  ->where("a.Active !=1")
										  ->where("a.Nooffloors = 1")
										  ->order("a.centername");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
   }     
	 public function gettoschedulernames($post = array())
	{
	
	//echo "<pre>";
	//print_r($post);
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $schedulerdesc =  $formdata['Description'];
		   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_venuedateschedule"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as createddate","DATE_FORMAT(a.date,'%d-%m-%Y') as Examdate","a.Active as Status"))
								  ->join(array('d'=>'tbl_newscheduler'),'d.idnewscheduler = a.idnewscheduler',array('d.*'))
								  ->join(array('b'=>'tbl_center'),'a.idvenue = b.idcenter',array('b.centername')) 
								  ->join(array('c'=>'tbl_takafuloperator'),'c.idtakafuloperator = b.centercode',array('c.TakafulName'))
								  ->join(array('e'=>'tbl_managesession'),'e.idmangesession = a.idsession',array('e.managesessionname'))	 
								  ->where("a.idprogram in (5,10)")
								  ->where("a.Active = ?",$post['field7'])
								  ->where("d.Description  LIKE '".$post['field3']."%'");								 
								 						 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		
		return $larrResult;
	 } 
public function fnajaxgetvenue($id)
   {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	                             ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
								 ->join(array('b'=>'tbl_takafuloperator'),'b.idtakafuloperator = a.centercode',array(''))
								 ->where("a.idcenter not in ($id)")
								  ->where("b.TakafulField6=1")
								 ->where("a.Active !=1")
							     ->where("a.Nooffloors = 1")
								 ->order("a.centername");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
   } 
   
    //inhouse changes for sessions 15/03/2014
    public function fngetSessionDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	                            ->from(array("a"=>"tbl_managesession"),array("key"=>"a.idmangesession","value"=>"(CONCAT((a.managesessionname),'---',(a.ampmstart),' to ',(a.ampmend)))"))
								 ->where("a.active=1")
								  ->where("a.Inhouse=1");							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     } 
     public function fnajaxgetsession($id)
     {
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	                            ->from(array("a"=>"tbl_managesession"),array("key"=>"a.idmangesession","value"=>"(CONCAT((a.managesessionname),'---',(a.ampmstart),' to ',(a.ampmend)))"))
								 ->where("a.active=1")
								  ->where("a.Inhouse=1")
								 ->where("a.idmangesession not in ($id)");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
   }	

//changes for inhouse validation on 15/03/2014
 public function fnvalidatevenue($lintidvenue,$idsessions,$dates)
   {
   	
   	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
	                            ->from(array("a"=>"tbl_venuedateschedule"),array("a.*"))
	                            ->where("a.idsession in ($idsessions)")
	                             ->where("a.idvenue = ?",$lintidvenue)
	                              ->where("a.date =?",$dates)
	                                ->where("a.Active in ('0','1')")
	                              ->where("a.Reserveflag in ('0','1')");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
   	
   }  
 public function  fngetmaxseatcapacity()
{
   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_config"),array("a.TakafulField6"));
										 // ->where("idvenues =?",$lintidvenue)
										   // ->where("session='$lintsess'");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  
} 
}
?>