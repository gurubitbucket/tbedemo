<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Scheduler_SchedulereportController extends Base_Base
{
	public function init()
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
		$this->registry = Zend_Registry::getInstance(); //get registry instance
		$this->locale = $this->registry->get('Zend_Locale'); //get locale
		$this->lobjSchedulereport = new Scheduler_Model_DbTable_Schedulereportmodel(); //model object
		$this->lobjSchedulereportform = new  Scheduler_Form_Schedulereport(); //form object
	}
	//function to set and display the result
	public function indexAction()
	{
		$this->view->lobjform = $this->lobjSchedulereportform;
		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->attendancereportpaginatorresult);
		$lintpagecount = 1000;
		$lintpage = $this->_getParam('page',1);//Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->attendancereportpaginatorresult))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->attendancereportpaginatorresult,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				unset ( $larrformData ['Search']);
				$this->view->search = 1;
				$this->view->FromDate = $FromDate = $larrformData['FromDate'];
				$this->view->ToDate = $ToDate = $larrformData['ToDate'];
				$this->view->day = $day = $larrformData['day'];
				$larrresultcomp = $this->lobjSchedulereport->fngetschdeuledates($larrformData);
				
				$larrresultvenues = $this->lobjSchedulereport->fngetvenues();
				//echo "<pre>";print_r($larrresultvenues);die();
				$this->view->dates = $larrresultcomp;
				$this->view->venues = $larrresultvenues;
				$this->view->lobjform->populate($larrformData);
			}
		}
	}
    public function fnexportexcelAction()
    {	
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$ldtfromdate = $larrformData['FromDate'];
		$ldttodate =$larrformData['ToDate'];
		if($larrformData['day']){
			$lstrday =$larrformData['day'];
		}else{
			$lstrday ='All Days';
		}
		$fdate = date('d-m-Y',strtotime($ldtfromdate));
		$tdate = date('d-m-Y',strtotime($ldttodate));
        $larrresultcomp = $this->lobjSchedulereport->fngetschdeuledates($larrformData);
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Schedulereport'.$ldtfromdate.'_'.$ldttodate;
		$ReportName = $this->view->translate( "Scheduler" ).' '.$this->view->translate( "Report" );
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 2><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 2><b> Time</b></td><td align=left colspan = 2><b>$time</b></td></tr>";
        $tabledata.= "<tr><td align=left colspan = 2><b>From Date </b></td><td align=left colspan = 2><b>".$fdate."</b></td><td  align=left colspan = 2><b>To Date</b></td><td align=left colspan = 2><b>".$tdate."</b></td><td  align=left colspan = 2><b>Day</b></td><td align=left colspan = 2><b>".$lstrday."</b></td></tr></table><br>";
	    $tabledata.= "<table border = '1' ><tr><td valign = 'top'><b>VENUES/DATE</b></td>";
	    $cols = count($larrresultcomp)+1; 
	    for($lvars=0;$lvars<count($larrresultcomp);$lvars++){
	    	$tabledata.="<td align = 'left' colspan = '2'><b>".$larrresultcomp[$lvars]['Date']."</b><br>";
	    	$tabledata.="<b>".$larrresultcomp[$lvars]['dayname']."</b></td>";
	    }
	    $tabledata.="</tr>";
	    $larrresultvenues = $this->lobjSchedulereport->fngetvenues();
	    for($lvar=0;$lvar<count($larrresultvenues);$lvar++)
	      {
	    	$tabledata.= "<tr><td valign = 'top'>".$larrresultvenues[$lvar]['centername']."</td>";
	    	for($j=0;$j<count($larrresultcomp);$j++)
	    	   {
	    		   $this->lobjSchedulereport = new Reports_Model_DbTable_Schedulereportmodel();
        	       $idcenter = $larrresultvenues[$lvar]['idcenter'];
        	       $date = $larrresultcomp[$j]['date'];
        	       $larresult =$this->lobjSchedulereport->fngetseats($idcenter,$date); 
        	       $cnt = count($larresult);
        	       if($cnt==2)
        	       { 
        	       	$tabledata.= "<td valign = 'top' colspan = '2'>
        	       	                <table width='100%'>
        	       		 			  <tr><td align='left'>".$larresult[0]['ampmstart']."</td><td align='right'>[".$larresult[0]['Totalcapacity']."]</td></tr>
        	       		 			  <tr><td align='left'>".$larresult[1]['ampmstart']."</td><td align='right'>[".$larresult[1]['Totalcapacity']."]</td></tr>
        	       		 			</table>  
        	       		 		  </td>";
        	       }
        	       else if($cnt==1)
        	       {  
        	       	$tabledata.= "<td valign = 'top' colspan = '2'><table width='100%'><tr><td align='left'>".$larresult[0]['ampmstart']."</td><td align='right'>[".$larresult[0]['Totalcapacity']."]</td></tr></table></td>"; 
        	       }
        	       else 
        	       {
        	       	$tabledata.= "<td valign = 'top' colspan = '2'>No Exam</td>";
        	       }
        	   }     
                $tabledata.= "</tr>";
	      }
	        $tabledata.="</table>";
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
    }
}