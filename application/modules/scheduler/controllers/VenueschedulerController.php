<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
//Controller for the Venuescheduler Module
class Scheduler_VenueschedulerController extends Base_Base 
{ 

	//initialization function
	public function init() 
	{ 
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	   	Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() 
	{
		$this->lobjvenuescheduler = new Scheduler_Model_DbTable_Venuescheduler(); //venuescheduler model object
		$this->lobjvenueschedulerForm = new Scheduler_Form_Venuescheduler(); //intialize venuescheduler lobjvenueschedulerForm
		$this->lobjform = new App_Form_Search(); //intialize Search lobjform
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	// action for search and view
	public function indexAction() 
	{   
		
		$checkEmpty = $this->view->checkEmpty = 1;
		$this->view->Active = 1;	 
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view
		if ($this->_request->isPost () && $this->_request->getPost ( 'Auth' )) 
		{
			$larrformData = $this->_request->getPost ();
			
			if ($this->lobjform->isValid($larrformData))
			{
				$lstrpassword = $larrformData ['passwd'];
				$larrformData ['passwd'] = md5 ( $lstrpassword );
				$resultpassword = $this->lobjvenuescheduler->fncheckSuperUserPwd($larrformData['passwd']);
				if(!empty($resultpassword['passwd']) && $larrformData['passwd'] == $resultpassword['passwd'])
				{
					$this->view->resultpassword =  $resultpassword['passwd'];
					$this->view->checkEmpty = 2;
				}
				else
				{
					 echo "<script> alert('Please Enter Valid Password of Super Admin')</script>";
				}
			}		
		}
		$lobjvenueschedulerForm=$this->view->lobjvenueschedulerForm = $this->lobjvenueschedulerForm; 
		$larrcenternames = $this->lobjvenuescheduler->fngetcenternames();
		$this->lobjvenueschedulerForm->Venue->addMultiOption('',"All");
		$this->lobjvenueschedulerForm->Venue->addMultiOptions($larrcenternames);
		
		if ($this->_request->isPost () && $this->_request->getPost('Search')) 
		{
			$this->view->checkEmpty = 2;
			$larrformData = $this->_request->getPost ();			    						
			unset ( $larrformData ['Search'] );
			//echo "<pre>";print_r($larrformData);die();
			$larrresult = $this->lobjvenuescheduler->fnGetsearchdetails($larrformData); //searching the values for the businesstype
			//echo "<pre>";print_r($larrresult);die();
			
			if(!empty($larrresult))
			{
				$this->view->paginatorsss =$larrresult;	
				$this->view->countcomp=count($larrresult);
				$this->view->checkEmpty = 2;
				$this->view->Active = $larrformData['Active'];
				if($larrformData['Active']==0)
				{
					$values=1;
				}
				else {
					$values=0;
				}
				$this->view->Activevalue = $values;
				$this->lobjvenueschedulerForm->populate($larrformData);
			}
			else
			{
				$this->view->larrNoResult = "No Data Found";
					$this->view->checkEmpty = 2;
				$this->lobjvenueschedulerForm->populate($larrformData);
			}
		}

		if ($this->_request->isPost () && $this->_request->getPost( 'Actives' )) 
		{
			$larrformData1 = $this->_request->getPost ();
			unset ( $larrformData1 ['Actives'] );
			//echo "<pre/>";print_r($larrformData1);die();
			$this->lobjvenuescheduler->fnUpdateActiveFieldApproved($larrformData1);
			$this->_redirect( $this->baseUrl . '/scheduler/venuescheduler/index');
		}

		if ($this->_request->isPost () && $this->_request->getPost( 'InActives' )) 
		{
			$larrformData2 = $this->_request->getPost ();
			unset ( $larrformData2 ['InActives'] );
			$this->lobjvenuescheduler->fnUpdateActiveFieldApproved($larrformData2);
			$this->_redirect( $this->baseUrl . '/scheduler/venuescheduler/index');
		}
		
	}
	
	public function venueschedulevalidationAction() 
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidvenuedateschedule = $this->_getParam('idvenuedateschedule');
		$larrresult=$this->lobjvenuescheduler->fngetvenueschedulerdetails($lintidvenuedateschedule);
		$larrvalidate=$this->lobjvenuescheduler->fncheckvalidate($larrresult);
		$flag=0;
		if(count($larrvalidate)>0)
		{
			$flag=1;
		}
		echo $flag;die();
	}

}
