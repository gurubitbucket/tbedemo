<?php
class Scheduler_ManagesessionController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function fnsetObj(){
		$this->lobjprogram = new Scheduler_Model_DbTable_Managesession();
		$this->lobjprogramForm = new Scheduler_Form_Managesession(); 
	  	//$this->lobjcoursemaster = new Scheduler_Model_DbTable_Coursemaster();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
		
	}
	
	public function indexAction() {
    	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = array();//$this->lobjprogram->fngetSessionDetails(); 	
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->managesessionpaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->managesessionpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->managesessionpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjprogram->fnSearchSession( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->managesessionpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/scheduler/managesession/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newsessionAction() { //title
		$this->view->lobjprogramForm = $this->lobjprogramForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjprogramForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjprogramForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$fromtimearray = $formData['starttime'];
			 	$pieces = explode("T", $fromtimearray);
			 	$fromtime = $pieces[1];
			 	$fromtimearray = $formData['endtime'];
			 	$pieces1 = explode("T", $fromtimearray);
			 	$totime = $pieces1[1];
				$larrresult = $this->lobjprogram->fnaddSession($formData,$fromtime,$totime);
				 $this->_redirect( $this->baseUrl . '/scheduler/managesession/index');
        }     
    }
    
	public function editsessionAction(){
		$this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjprogramForm->UpdDate->setValue ( $ldtsystemDate );		
    	$IdProgram = $this->_getParam('id');
    	$this->view->idmangesession=$IdProgram;
    	$result=$this->lobjprogram->fnGetsessionList($IdProgram);
    	$this->view->starttime= "Sat Nov 12 2011".' '.$result['starttime'];
    	$this->view->endtime= "Sat Nov 12 2011".' '.$result['endtime'];
		//echo "Sat Nov 12 2011".' '.$result['starttime'];
		
		
		$result=$this->lobjprogram->fnGetsessionList($IdProgram);
		$this->view->inhouseflag = $result['Inhouse'];
 
    	$this->lobjprogramForm->populate($result);	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjprogramForm->isValid($formData)) {
	    		unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$fromtimearray = $formData['starttime'];
			 	$pieces = explode("T", $fromtimearray);
			 	$fromtime = $pieces[1];
			 	$fromtimearray = $formData['endtime'];
			 	$pieces1 = explode("T", $fromtimearray);
			 	$totime = $pieces1[1];
	   			$lintIdProgram = $formData ['idmangesession'];
				$larrresults = $this->lobjprogram->fnupdateSession($formData,$fromtime,$totime);//update university
				$auth = Zend_Auth::getInstance();
				 $this->_redirect( $this->baseUrl . '/scheduler/managesession/index');
			}
    	}
    }
    
    public function fnajaxgetsessionnameAction()
    {
    	$this->_helper->layout->disableLayout();//disable layout
    	$this->_helper->viewRenderer->setNoRender();//do not render the view
    	$sessionname=$this->_getParam('sessionname'); // get the centername
    	$larremail = $this->lobjprogram->fngetexistingsessionname($sessionname); // search the DB for the centername
    	if($larremail)
    	{
    		echo 'The session name provided already exists, please provide with another session name';
    	}
    }
}