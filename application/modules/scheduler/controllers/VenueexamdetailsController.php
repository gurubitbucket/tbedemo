<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
//Controller for the Venuescheduler Module
class Scheduler_VenueexamdetailsController extends Base_Base 
{ 
	//initialization function
	public function init() 
	{ 
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	   	Zend_Form::setDefaultTranslator($this->view->translate);
		$this->lobjCommon = new App_Model_Common();
		$this->fnsetObj();
	}
	public function fnsetObj() 
	{
		$this->lobjvenueexamsmodel = new Scheduler_Model_DbTable_Venueexamdetails(); //venuescheduler model object
		$this->lobjvenueexamsform = new Scheduler_Form_Venueexamdetails(); //intialize venuescheduler lobjvenueschedulerForm
		$this->lobjform = new App_Form_Search(); //intialize Search lobjform
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	// action for search and view
	public function indexAction() 
	{   
	    $this->view->lobjvenueexamsform = $this->lobjvenueexamsform;
		$venuelist = $this->lobjvenueexamsmodel->getvenuelist();
		$this->lobjvenueexamsform->Venue->addMultiOption('','Select');
		$this->lobjvenueexamsform->Venue->addMultiOptions($venuelist);
		//$this->lobjvenueexamsform->addMultiOptions($venuelist);
	     if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->venueexams);
		$lintpagecount = 30;
		$lintpage = $this->_getParam('page',1); //Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->venueexams))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->venueexams,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
			
		if ($this->_request->isPost () && $this->_request->getPost('Search')) 
		{
			$larrformData = $this->_request->getPost ();
			unset ( $larrformData ['Search'] );
			$this->view->fromdatess=$larrformData['Fromdate'];
			$this->view->todatess=$larrformData['Todate'];
			$this->view->venuess=$larrformData['Venue'];
			$this->view->activestatus = $larrformData['Active'];
			$larrresultss = $this->lobjvenueexamsmodel->fnGetsearchdetails($larrformData['Fromdate'],$larrformData['Todate'],$larrformData['Venue'],$larrformData['Active']); //searching the values for the businesstype	
			//echo "<pre>";
			//print_r($larrresultss);die();
			$this->view->paginator = $this->lobjvenueexamsmodel->fnPagination($larrresultss,$lintpage,$lintpagecount);
			$this->gobjsessionstudent->studentupdatepaginatorresult = $larrresultss;
			$this->gobjsessionstudent->venueexams = $larrresultss;
			$this->view->paginator = $larrresultss;
			$this->lobjvenueexamsform->populate($larrformData);
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Clear' )) 
		{
			$this->_redirect( $this->baseUrl . '/scheduler/venueexamdetails/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Save' )) 
		{
			$larrformData = $this->_request->getPost();			
			$larrresultss = $this->lobjvenueexamsmodel->fnupdatestatus($larrformData['idvenue'],$larrformData['act']); //searching the values for the businesstype
		}
	}
	public function getalloteddetailsAction() 
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidcenter = $this->_getParam('idcenter');
		$ldtedate = $this->_getParam('edate');
		$active = $this->_getParam('active');
		$lintidsess = $this->_getParam('idsess');
		
		$result = $this->lobjvenueexamsmodel->fngetcenteralloteddetails($lintidcenter,$ldtedate,$lintidsess,$active);
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Alloted Seats Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>ICNO</b></th><th><b>Program</b></th><th><b>Exam Venue</b></th></th><th><b>Exam Date</b></th><th><b>Exam Session</b></th><th><b>Result</b></th><th><b>Status</b></th></tr>";
		foreach($result as $lobjCountry)
		{
			$tabledata.="<tr><td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".$lobjCountry['ProgramName']."</td><td align = 'left'>".$lobjCountry['ExamVenue']."</td><td align = 'left'>".$lobjCountry['ExamDate']."</td><td align = 'left'>".$lobjCountry['managesessionname']."</td></td><td align = 'left'>".$lobjCountry['payment']."</td></tr>";
		}
		$tabledata.="</table></form><br>";
		$tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
		
		echo  $tabledata;
	
	   
    }    
}
