<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
//Controller for the Venuescheduler Module
class Scheduler_ChronepullschedulingController extends Base_Base 
{ 
	//initialization function
	public function init() 
	{ 
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	   	Zend_Form::setDefaultTranslator($this->view->translate);
		$this->lobjCommon = new App_Model_Common();
		$this->fnsetObj();
		$this->auth = Zend_Auth::getInstance();
	}
	public function fnsetObj() 
	{
	    $this->lobjchroneclientmodel = new Scheduler_Model_DbTable_Clientchrone();
		$this->lobjchroneschedulemodel = new Scheduler_Model_DbTable_Chronepullscheduling(); //venuescheduler model object
		$this->lobjchronescheduleform = new Scheduler_Form_Chronepullscheduling(); //intialize venuescheduler lobjvenueschedulerForm
		$this->lobjform = new App_Form_Search(); //intialize Search lobjform
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	// action for search and view
	public function indexAction() 
	{   
	    $this->view->lobjchronescheduleform = $this->lobjchronescheduleform;
		$venuelist = $this->lobjchroneschedulemodel->getintialconfig();
		$time = $venuelist['Scheduletime'];
		$venues = $this->lobjchroneschedulemodel->getvenuelist();
		$this->lobjchronescheduleform->Venue->addMultiOption('0','All Venues');
		$this->lobjchronescheduleform->Venue->addMultiOptions($venues);
			
		$fromdate = $venuelist['Scheduleday'];
		
		   //adding constraint for the from date
		   $month= date("m"); // Month value
		   $day=  date("d"); //today's date
		   $year= date("Y"); // Year value		  
		   $newfromdate= date('Y-m-d', mktime(0,0,0,$month,($day+$fromdate),$year));		 
		   $newdate = "{min:'$newfromdate',datePattern:'dd-MM-yyyy'}"; 
		   $this->view->lobjchronescheduleform->FromDate->setAttrib('constraints',"$newdate");
		   //end of adding constraint
		   
		   if ($this->_request->isPost () && $this->_request->getPost ( 'Search' ))
		   {
			  $larrformData = $this->_request->getPost();
			  $larresultsessions = $this->lobjchroneschedulemodel->fngetallsessions();
			  $this->lobjchronescheduleform->Session->addMultiOptions($larresultsessions);
			   
			   if($larrformData['Venue'] == 0)//for all venues
			  {
			      $larrresult = $this->lobjchroneschedulemodel->fngetchronepulldatasautos($larrformData['Date'],$larrformData['Session']);			
			  }
			  else//for single venue
			  {
			     $larrresult = $this->lobjchroneschedulemodel->fngetchronepulldatasautossinglevenue($larrformData['Date'],$larrformData['Venue'],$larrformData['Session']);
			  }				   
			  if($larrresult)
			  {
					$this->view->data = 1;			
					$this->view->lobjchronescheduleform->Date->setValue($larrformData['Date']);
					$this->view->lobjchronescheduleform->Session->setValue($larrformData['Session']);
				  
			  }
			  else
			   {
				  $this->view->data = 0;
				 
				}
				for($i=0;$i<count($larrresult);$i++)
			    {
				
					$larrresult[$i]['sdate'] = ($date= date('Y-m-d',strtotime ($larrresult[$i]['schdeduleddate'])));
					$ST = ($start = date("T"."H:i", strtotime($larrresult[$i]['scheduledtime'])));		
					$lrresult = explode('IS',$ST);					
					$larrresult[$i]['stime'] = $lrresult[1];
					
			    }
				  $this->view->paginator = $larrresult;
				  $this->view->lobjchronescheduleform->populate($larrformData);
			  			
			  } 
			  
		if ($this->_request->isPost () && $this->_request->getPost( 'Clear' )) 
		{
			$this->_redirect( $this->baseUrl . '/scheduler/chronepullscheduling/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Update' )) 
		{
			$larrformData = $this->_request->getPost ();
			//echo "<pre>";
			//print_r($larrformData);
			if(isset($larrformData['idcenter']))
			{			
			for($i=0;$i<count($larrformData['idcenter']);$i++)
			{
			  $larraydetails[] =array();
			  $centerexamdate[] = explode(',',$larrformData['idcenter'][$i]);
			  for($j=0;$j<4;$j++)
			  {
			    $larraydetails[$i]['idcenter'] = $centerexamdate[$i][0];				
				$larraydetails[$i]['Examdate'] = date('Y-m-d', strtotime($centerexamdate[$i][1]));
				$larraydetails[$i]['Idautopull'] = $centerexamdate[$i][4];
				$larraydetails[$i]['Scheduledate'] = $larrformData['SDate'][$centerexamdate[$i][1]][$larraydetails[$i]['idcenter']];
				$larraydetails[$i]['Scheduletime'] = $larrformData['Stime'][$centerexamdate[$i][1]][$larraydetails[$i]['idcenter']];
			  } 
			  
			}
		
				$larrformData['UpdDate']=date('Y-m-d H:i:s');
				
				$larrformData['UpdUser']= $this->auth->getIdentity()->iduser;
				
				$larresultchronevenue = $this->lobjchroneschedulemodel->fnupdatechronepull($larrformData,$larraydetails);
		  }
		}
	    if ($this->_request->isPost () && $this->_request->getPost ( 'updatenow' )) 
		{
			     $larrformData = $this->_request->getPost ();
			     $larresult = $this->lobjchroneclientmodel->fnupdatevenuestudents($larrformData);
	    }
	}
	public function deletechroneAction()
	{
	    //$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$idauto = $this->_getParam('id');
		$idcvenue = $this->_getParam('idcvenue');
		echo $idauto = $this->_getParam('id');
    	//$this->lobjuploadmodule->fndocdelete($docid);
    	

		//echo $idauto;
		//echo $idcvenue;
	    $this->lobjchroneschedulemodel->fntodeletecrone($idauto);
		$this->_redirect($this->baseurl."scheduler/chronepullscheduling/index");
	}
	public function getsessionsAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$larresultvenuedateschedule = $this->lobjchroneschedulemodel->fngetvenuedateschedule($iddate);
		$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larresultvenuedateschedule);
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
	}
	
	
}
