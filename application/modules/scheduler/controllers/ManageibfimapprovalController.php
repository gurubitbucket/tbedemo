<?php
error_reporting(E_ALL);

//Controller for the Venuescheduler Module
class Scheduler_ManageibfimapprovalController extends Base_Base 
{ 
	//initialization function
	public function init() 
	{ 
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	   	Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}	
	public function fnsetObj() 
	{
		$this->lobjvenuescheduler = new Scheduler_Model_DbTable_Managetakafulcenters(); //venuescheduler model object
		//$this->lobjvenueschedulerForm = new Scheduler_Form_Managetakafulcenters(); //intialize venuescheduler lobjvenueschedulerForm
		$this->lobjform = new App_Form_Search(); //intialize Search lobjform
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}	
	// action for search and view
	public function indexAction() 
	{   
		
    }
}
