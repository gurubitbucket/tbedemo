<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Scheduler_SessionconstraintsController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	private $_gobjlogger;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
	
	public function fnsetObj(){
		$this->lobjprogram = new Scheduler_Model_DbTable_Sessionconstraints();
		$this->lobjprogramForm = new Scheduler_Form_Sessionconstraints(); 
	}
	
	public function indexAction() 
	{
    	$this->view->title="Session Constraints";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = array();//$this->lobjprogram->fngetSessionDetails();
		$larrcenters = $this->lobjprogram->fngetcenters();
		$this->lobjprogramForm->Venues->addMultiOption('',"All Venues");
		$this->lobjform->field19->addmultioptions($larrcenters); 	
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->sessionconpaginatorresult);
		$lintpagecount = 50;//$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance		
		if(isset($this->gobjsessionsis->sessionconpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->sessionconpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}		
		$this->view->lobjprogramForm = $this->lobjprogramForm;		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
					$formData = $this->getRequest()->getPost();	
					$ldfromdate = $formData['Date'];
					$ldtodate = $formData['Schedulertodate'];
                                          $fromdate = $formData['Date'];
					//print_r($larrtomonth);die();
					$dateofbirth = "{min:'$fromdate',datePattern:'dd-MM-yyyy'}";					
					$this->view->lobjprogramForm->Schedulertodate->setAttrib('constraints',"$dateofbirth");
					$larrcenters = $this->lobjprogram->fnajaxgetcenternames($ldfromdate,$ldtodate);
					$this->lobjprogramForm->Venues->addMultiOptions($larrcenters);
					$this->lobjprogramForm->Venues->setValue($formData['Venues']);
					$this->view->actions=$formData['Action'];
					unset ( $formData ['Save'] );
					unset ( $formData ['Back'] );
					$this->view->activestatus = $formData['Action'];			
					if($formData['Action'] =='2')
					{
						$this->view->displayexception = 1;
						$larrresult = $this->lobjprogram->fnSearchexception( $formData ); //searching the values for the user
					}
					elseif($formData['Action'] =='3')
					{
						$this->view->seatcapacitychange = 1;
						$larrresult = $this->lobjprogram->fngetschdeuledatesforchangeseats($formData);
					}
					else
					{
					   $this->view->displayexception = 0;
					   $larrresult = $this->lobjprogram->fngetschdeuledates($formData);	
					}
					$this->lobjprogramForm->populate($formData);		
					$this->view->paginator = $larrresult;			
		}
		 if ($this->_request->isPost() && $this->_request->getPost('Save'))
		 {
					$larrformData = $this->_request->getPost();
					$auth = Zend_Auth::getInstance(); 
					$upduser =  $auth->getIdentity()->iduser;			
					if($larrformData['actions']==0)
					{
							for($i=0;$i<count($larrformData['idvenue']);$i++)
							{
							   $larrresult[$i] = explode(',',$larrformData['idvenue'][$i]);						
							}
							$this->lobjprogram->fnupdateschedulerexceptions($larrresult,$upduser);
					}
					if($larrformData['actions']==1)
					{
							for($i=0;$i<count($larrformData['idvenue']);$i++)
							{
							   $larrresult[$i] = explode(',',$larrformData['idvenue'][$i]);						
							}
							$this->lobjprogram->fnupdateschexceptnwithconstaints($larrresult,$upduser,$larrformData['constraintname']);
					
					}
					if($larrformData['actions'] == 3)
					{			
						  $this->lobjprogram->fnupdatescapacity($larrformData,$upduser);
					}
		 }
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			       $this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
		}
	}
		public function newsessionconstraintsAction() 
		{
				$this->view->lobjprogramForm = $this->lobjprogramForm;
				$larrVenueresult = $this->lobjprogram->fngetVenueDetails();
				$this->view->larrVenueresult=$larrVenueresult;
				if ($this->getRequest()->isPost()) {
					$formData = $this->getRequest()->getPost();
					unset ( $formData ['Save'] );
					unset ( $formData ['Back'] );
					$larrresult = $this->lobjprogram->fnaddSessionconstrains($formData);
					$auth = Zend_Auth::getInstance();
					// Write Logs
					$priority=Zend_Log::INFO;
					$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
					$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Sessionconstraints Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
					$this->_gobjlogger->log($message,5);
					$this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
				}
		}
    	public function editschedulerconstraintsAction()
		{
		    $this->view->lobjprogramForm = $this->lobjprogramForm;		
			if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
			{
				$formData = $this->getRequest()->getPost();				
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );				
				$larrresult = $this->lobjprogram->fngetschdeuledates($formData);				
				if($formData['Venues'] == '')
					{
					   $this->view->center =0;
					   $larrvenues = $this->lobjprogram->fngetvenues();
					   $this->view->venuegroup = $larrvenues;
					   $this->view->bulkvenue = 1;
					}
					else
					{
					   $this->view->center =  $formData['Venues'];
					   $larrresultvenues = $this->lobjprogram->fngetvenuedetailsforcenterid($formData['Venues']);
					   $this->view->venues = $larrresultvenues;
					   $this->view->bulkvenue = 0;
					}
				$this->view->paginator = $larrresult;
				$this->lobjprogramForm->populate($formData);
				$auth = Zend_Auth::getInstance();
				// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Sessionconstraints Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				//$this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
			}
		
		  $this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view
		  $Idscheduler = $this->_getParam('id');
		  $larrVenueresult = $this->lobjprogram->fngetVenueDetailsforidcenter($Idscheduler);
		   $this->view->center = $larrVenueresult['idcenter'];
		   $this->view->state = $larrVenueresult['state'];
		   $this->view->city = $larrVenueresult['city'];
		  $this->view->larrVenueresult=$larrVenueresult;
		  $this->view->Idschedule = $Idscheduler;
		  $schedulerresult = $this->lobjprogram->fngetschedulerresult($Idscheduler);
		  if(empty($schedulerresult))
		  {
			   echo '<script language="javascript">alert("Some Candidates Already Registered To This Date Still U Want Deactivate This")</script>';		      
				 $larresult = $this->lobjprogram->fngetschedulerdetails($Idscheduler);
				//$this->view->lobjprogramForm->constraintname->setValue($result['constraintname']);
				$this->view->lobjprogramForm->Venue->setValue($larresult['centername']);
				$this->view->lobjprogramForm->Session->setValue($larresult['managesessionname']);
				$this->view->lobjprogramForm->Total->setValue($larresult['Totalcapacity']);
			   $this->view->lobjprogramForm->Alloted->setValue($larresult['Allotedseats']);
			   $this->view->lobjprogramForm->Date->setValue($larresult['date']);
			   $this->view->lobjprogramForm->Venue->setAttrib('readonly','readonly'); 
			   $this->view->lobjprogramForm->Alloted->setAttrib('readonly','readonly'); 
			   $this->view->lobjprogramForm->Session->setAttrib('readonly','readonly'); 
			   $this->view->lobjprogramForm->Date->setAttrib('readonly','readonly'); 
		   }
		 else
		 {
		     $larresult = $this->lobjprogram->fngetschedulerdetails($Idscheduler);
		    //$this->view->lobjprogramForm->constraintname->setValue($result['constraintname']);
		    $this->view->lobjprogramForm->Venue->setValue($larresult['centername']);
    	    $this->view->lobjprogramForm->Session->setValue($larresult['managesessionname']);
    	    $this->view->lobjprogramForm->Total->setValue($larresult['Totalcapacity']);
		   $this->view->lobjprogramForm->Alloted->setValue($larresult['Allotedseats']);
		   $this->view->lobjprogramForm->Date->setValue($larresult['date']);
		   $this->view->lobjprogramForm->Venue->setAttrib('readonly','readonly'); 
		   $this->view->lobjprogramForm->Alloted->setAttrib('readonly','readonly'); 
		   $this->view->lobjprogramForm->Session->setAttrib('readonly','readonly'); 
	       $this->view->lobjprogramForm->Date->setAttrib('readonly','readonly'); 
		    //echo "<script>parent.location = '".$this->view->baseUrl()."/scheduler/sessionconstraints/newsessionconstraints/id'".$Idscheduler."</script>";
		 }
		 if ($this->_request->isPost() && $this->_request->getPost('Save'))
		 {
			$larrformData = $this->_request->getPost();
			$this->lobjprogram->fnupdatescheduler($larrformData);
			 
			//echo "<pre>";
			//print_r($larrformData);die();
			$this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
		 }
			
		
		}
	public function editsessionconstraintsAction()
	{
		$this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view
		$this->view->lobjprogramForm->Date->setAttrib('readonly','readonly');		
    	$IdProgram = $this->_getParam('id');
    	$this->view->lobjprogramForm->idschedulerconstraints->setValue($IdProgram);
    	$larrresults = $this->lobjprogram->fngetDaysDetails();
		$larrresult2 = $this->lobjprogram->fngetSateDetails();
		$this->lobjprogramForm->idstate->addMultiOptions($larrresult2);
    	$result=$this->lobjprogram->fnGetsessionList($IdProgram);
    	$this->lobjprogramForm->idcity->addMultiOptions($this->lobjprogram->fnGetcityList($result['state']));
        $this->lobjprogramForm->idvenue->addMultiOptions($this->lobjprogram->fnGetVenueList($result['city']));
    	$this->view->lobjprogramForm->constraintname->setValue($result['constraintname']);
    	$this->view->lobjprogramForm->idstate->setValue($result['state']);
    	$this->view->lobjprogramForm->idcity->setValue($result['city']);
    	$this->view->lobjprogramForm->idvenue->setValue($result['idstate']);
    	$this->view->lobjprogramForm->Date->setValue($result['Date']);
    	$this->view->lobjprogramForm->Active->setValue($result['Active']);
		
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjprogramForm->isValid($formData)) {
	    		//unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
	   			$lintIdProgram = $formData['idschedulerconstraints'];
				$larrresults = $this->lobjprogram->fnupdateSession($formData);//update university
				$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the New Sessionconstraints with Id = ".$IdProgram."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
			}
    	}
    }
    public function getvenuelistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		//get all the city that correspond to the selected state
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjprogram->fnGetVenueList($lintIdCountry));
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}
public function fngetcentresAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$ldfromdate = $this->_getParam('regFromDate');
		$ldtodate = $this->_getParam('toDate');
		$larrcenters = $this->lobjprogram->fnajaxgetcenternames($ldfromdate,$ldtodate);
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}	
	public function studentdetailsAction() 
	{    
		$this->_helper->layout->disableLayout();
		$idcenter = $this->_getParam('idcenter');		
		$idsession = $this->_getParam('idsession');
		$examdate = $this->_getParam('examdate');
		$this->view->larrresult= $larrresult = $this->lobjprogram->fnGetRegistered($idcenter,$idsession,$examdate);
		
	}
}