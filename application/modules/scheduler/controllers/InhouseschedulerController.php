<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Scheduler_InhouseschedulerController extends Base_Base {

	
	public function init() {
		$this->fnsetObj();
	}
	
	public function fnsetObj()
	{
		$this->lobjonedayschedulermodel = new Scheduler_Model_DbTable_Inhousescheduler();
		$this->lobjonedayschedulermodelForm = new Scheduler_Form_Inhouseschedulerform(); 

	}
	
	public function indexAction() {
    	$this->view->title="Manage Inhouse Scheduler";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		
		$lintsess = Zend_Session::getId();
		$larrdeleteresult = $this->lobjonedayschedulermodel->fndeleteaction($lintsess);
		$larrdeletetempvenue = $this->lobjonedayschedulermodel->fndeletetempvenue($lintsess);
		$larrresult = array();//$this->lobjschedulermodel->fnGetScheduler(); 
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->newschedulerresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->newschedulerresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->newschedulerresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				
				$larrresult = $this->lobjonedayschedulermodel->gettoschedulernames( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->newschedulerresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/scheduler/inhousescheduler/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function addspecialschedulerAction() { //title
		$this->view->lobjonedayschedulermodelForm = $this->lobjonedayschedulermodelForm;
		$this->view->onload = 1;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjonedayschedulermodelForm->UpdDate->setValue($ldtsystemDate);
		
		
		
		$auth = Zend_Auth::getInstance();
		$this->view->lobjonedayschedulermodelForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		
		$larrresult = $this->lobjonedayschedulermodel->fngetCourse();
		$this->view->courses=$larrresult;
		$this->view->editsessionarray = array();
		$larrresults = $this->lobjonedayschedulermodel->fngetSessionDetails();
		//echo "<pre>";print_r($larrresults);die();
		$this->lobjonedayschedulermodelForm->session->addMultioption('','Select');
		$this->lobjonedayschedulermodelForm->session->addMultioptions($larrresults);
		
		//$this->lobjonedayschedulermodelForm->session->addMultioption('','Select');
		//$this->lobjonedayschedulermodelForm->schedulertype->addMultioptions(array('key'=>));
		//$this->view->mansession=$larrresults;
		$this->view->centerarray = array();
		$this->view->schedulercoursearray= array();
		/*$larrresult2 = $this->lobjonedayschedulermodel->fnfetchvenue();
		$this->view->centerarray=$larrresult2;*/
		$lintsess = Zend_Session::getId();
		$larresult = $this->lobjonedayschedulermodel->fnajaxgettempsession($lintsess);
		$this->view->managesession= $larresult;
		
		
		$Seatcapacity = $this->lobjonedayschedulermodel->fngetmaxseatcapacity();		
		$this->view->seatcapacity = $Seatcapacity['TakafulField6'];

	if ($this->_request->isPost () && $this->_request->getPost ( 'Next' )) 
	  {
			$larrformData = $this->_request->getPost (); 
			//echo "<pre>"; print_r($larrformData);die();
			$this->view->onload = 0;
			
			$this->view->editsessionarray = $larrformData['sessionarr'];
			$this->view->schedulercoursearray = $larrformData['idprogram'];
			$this->view->lobjonedayschedulermodelForm->Date->setvalue($larrformData['Date']);
			$this->view->lobjonedayschedulermodelForm->description->setvalue($larrformData['description']);
		    $this->view->lobjonedayschedulermodelForm->schedulertype->setvalue($larrformData['schedulertype']);
			$idsessions=0;
			for($i=0;$i<count($larrformData['sessionarr']);$i++)
	        {
	        	 $value=$larrformData['sessionarr'][$i];
				 $idsessions=$idsessions.','.$value;
	        }
			$larresultofpresentcenters = $this->lobjonedayschedulermodel->fngetpresentschedulers($larrformData,$idsessions);
			$centerresults = 0;
			for($center=0;$center<count($larresultofpresentcenters);$center++)
	        {
	        	 $value=$larresultofpresentcenters[$center]['idvenue'];
				 $centerresults=$centerresults.','.$value;
	        }
	        
	        $larrresultcenter = $this->lobjonedayschedulermodel->fngetcenternames($centerresults);
	        $this->lobjonedayschedulermodelForm->venue->addMultioption('','Select');
		    $this->lobjonedayschedulermodelForm->venue->addMultioptions($larrresultcenter);
	        $this->view->centerarray=$larrresultcenter;
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
	  {
	  	$larrformData = $this->_request->getPost (); 
	  	$larresult = $this->lobjonedayschedulermodel->fnAddScheduler($larrformData);
	  	echo "<script>parent.location = '".$this->view->baseUrl()."/scheduler/managetakafulcenters/index';</script>";
	  }
    }
    

	
	public function getexceptionaldateAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$iddate = $this->_getParam('iddate');
		$exceptionaldate = $this->lobjonedayschedulermodel->fngetexceptiondate($iddate);
		if(count($exceptionaldate)>2)
		{
			echo "1";
		}
		else 
		{
			echo "0";
		};
		
	}	
	
   public function fninserttempsessionAction()
    {   
   		
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidsession =  $this->_getParam('idsession');
		$lintsess = Zend_Session::getId();
		$validatesession = $this->lobjonedayschedulermodel->fngetsession($lintidsession,$lintsess);
		if($validatesession)
		{
			
		}
		else 
		{
		$result = $this->lobjonedayschedulermodel->fninserttempsession($lintidsession,$lintsess);
		}
		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempsession($lintsess);
		
        $idsessions = 0;
		for($i=0;$i<count($larrtemp);$i++)
	     {
	       $value = $larrtemp[$i]['idsession'];		  
		   $idsessions = $idsessions.','.$value;
		   
	     }
		
		 
		$larrresult = $this->lobjonedayschedulermodel->fnajaxgetsession($idsessions);
		
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresult);
		echo  Zend_Json_Encoder::encode($larrcentreDetailss);
		
    }	
   public function fninserttextAction()
    {   
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintsess = Zend_Session::getId();
		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempsession($lintsess);
		$tabledata = '';
		for($linti = 0;$linti<count($larrtemp);$linti++){
		       $tabledata.= '<table><tr><td>'.$larrtemp[$linti]['managesessionname'].'('.$larrtemp[$linti]['starttime'].' - '.$larrtemp[$linti]['endtime'].')
		                      <input type ="hidden" id="session" name = "sessionarr[]" value = "'.$larrtemp[$linti]['idmangesession'].'"/><td></tr>';
		}
		$tabledata.= '</table>';
		echo  $tabledata;
		
    }
    public function fninserttempvenueAction()
    {   
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidvenue =  $this->_getParam('idvenue');
		$lintcapacity =  $this->_getParam('capacity');
		
		$dates =  $this->_getParam('dates');
		
		$lintsess = Zend_Session::getId();
		
		
    		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempsession($lintsess);
		
        $idsessions = 0;
		for($i=0;$i<count($larrtemp);$i++)
	     {
	       $value = $larrtemp[$i]['idsession'];		  
		   $idsessions = $idsessions.','.$value;
		   
	     }
	     
		$validatevenues = $this->lobjonedayschedulermodel->fnvalidatevenue($lintidvenue,$idsessions,$dates);
		//echo "<pre>";
		//print_r($validatevenues);
		$flag=1;
		if($validatevenues)
		{
			$flag=2;
		}
		else 
		{
			$larrvalidatevenue=$this->lobjonedayschedulermodel->fngettempvenue($lintidvenue,$lintsess);
			if($larrvalidatevenue)
			{
				
			}
			else 
			{
		          $result = $this->lobjonedayschedulermodel->fninserttempvenue($lintidvenue,$lintcapacity,$lintsess);
			}
		}
		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempvenues($lintsess);
        $idvens = 0;
		for($i=0;$i<count($larrtemp);$i++)
	     {
	       $value = $larrtemp[$i]['idvenues'];
		   $idvens = $idvens.','.$value;
	     }
		$larrresultvenues = $this->lobjonedayschedulermodel->fnajaxgetvenue($idvens);
		//print_r($larrresult);
		if($flag==2)
		{
			$larrresult=2;
			echo $larrresult;
		}
		
		else if(empty($larrresultvenues))
		{
		  echo 1;
		}
		else
		{		
		  $larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresultvenues);
		  echo  Zend_Json_Encoder::encode($larrcentreDetailss);
		}
		
    }
    public function fninsertvenuetextAction()
    {   
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintsess = Zend_Session::getId();
		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempvenues($lintsess);
		$tabledata = '';
		for($linti = 0;$linti<count($larrtemp);$linti++){
		       $tabledata.= '<table><tr><td>'.$larrtemp[$linti]['centername'].'('.$larrtemp[$linti]['capacity'].')
		                      <input type ="hidden" id="venue" name = "venarr[]" value = "'.$larrtemp[$linti]['idcenter'].'"/>
		                      <input type ="hidden" id="capacities" name = "caparr[]" value = "'.$larrtemp[$linti]['capacity'].'"/><td></tr>';
		}
		$tabledata.= '</table>';
		echo  $tabledata;
		
    }
    public function fngetdateinwordsAction()
    {
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$ldtdate =  $this->_getParam('dat');
		$larrtemp = $this->lobjonedayschedulermodel->fngetdate($ldtdate);
		echo $larrtemp['date'];
    }	
	
}
