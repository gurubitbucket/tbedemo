<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
//Controller for the Venuescheduler Module
class Scheduler_ChroneschedulingController extends Base_Base 
{ 
	//initialization function
	public function init() 
	{ 
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	   	Zend_Form::setDefaultTranslator($this->view->translate);
		$this->lobjCommon = new App_Model_Common();
		$this->fnsetObj();
		$this->auth = Zend_Auth::getInstance();
	}
	public function fnsetObj() 
	{
	    $this->lobjchroneclientmodel = new Scheduler_Model_DbTable_Clientchrone();
		$this->lobjchroneschedulemodel = new Scheduler_Model_DbTable_Chronescheduling(); //venuescheduler model object
		$this->lobjchronescheduleform = new Scheduler_Form_Chronescheduling(); //intialize venuescheduler lobjvenueschedulerForm
		$this->lobjform = new App_Form_Search(); //intialize Search lobjform
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	// action for search and view
	public function indexAction() 
	{   
	    $this->view->lobjchronescheduleform = $this->lobjchronescheduleform;
		$venuelist = $this->lobjchroneschedulemodel->getintialconfig();
		$time = $venuelist['Scheduletime'];
		$venues = $this->lobjchroneschedulemodel->getvenuelist();
		$this->lobjchronescheduleform->Venue->addMultiOption('0','All Venues');
		$this->lobjchronescheduleform->Venue->addMultiOptions($venues);
			
		$fromdate = $venuelist['Scheduleday'];
		
		   //adding constraint for the from date
		   $month= date("m"); // Month value
		   $day=  date("d"); //today's date
		   $year= date("Y"); // Year value		  
		   $newfromdate= date('Y-m-d', mktime(0,0,0,$month,($day+$fromdate),$year));		 
		   $newdate = "{min:'$newfromdate',datePattern:'dd-MM-yyyy'}"; 
		   $this->view->lobjchronescheduleform->FromDate->setAttrib('constraints',"$newdate");
		   //end of adding constraint
		   
		   
		   
		if ($this->_request->isPost() && $this->_request->getPost('Search'))
		{
			$larrformData = $this->_request->getPost();
			//echo "<pre>";
			//print_r($larrformData);die();
			$this->view->search = 1;
			$larrtomonth= $larrformData['FromDate'];
			$yesterdaydate=$larrtomonth['todate'];
			//print_r($larrtomonth);die();
			$dateofbirth = "{min:'$larrtomonth',datePattern:'dd-MM-yyyy'}";
			$this->view->lobjchronescheduleform->ToDate->setAttrib('constraints',"$dateofbirth");
			
			
			if($larrformData['Venue'] == 0)//for all venues
				{
				  
				   $larrvenues = $this->lobjchroneschedulemodel->fngetvenues();
				   $this->view->venuegroup = $larrvenues;				  
				}
				else
				{				   
				   $larrresultvenues = $this->lobjchroneschedulemodel->fngetvenuedetails($larrformData['Venue']);
				   $this->view->venuegroup = $larrresultvenues;
				   $this->view->bulkvenue = 0;
				}
				
				
			$fromdate = $this->view->fromdate=$larrformData['FromDate'];
			$todate = $this->view->todate=$larrformData['ToDate'];
		
			if($larrformData['Venue'] == 0)//for all venues
			{
			   $larrresult = $this->lobjchroneschedulemodel->fnexamslist($larrformData['FromDate'],$larrformData['ToDate']);			
			}
			else//for single venue
			{
			   $larrresult = $this->lobjchroneschedulemodel->fnexamslistforsinglevenue($larrformData['FromDate'],$larrformData['ToDate'],$larrformData['Venue']);
			}			
			$larresultvenuestatus = $this->lobjchroneschedulemodel->fngetstatusofvenueforexamdates($larrformData['FromDate'],$larrformData['ToDate']);//for the exam dates in automail
			$newarray123= array();
			$newarrayexamdated = array();
			$newarraycenters = array();
			$larraydateover = array();
			$Schedulerdate= array();
			$Schedulertime= array();			
			for($j=0;$j<count($larresultvenuestatus);$j++)
			{
			   $newarrayexamdated[$j] = $larresultvenuestatus[$j]['Examdate'];			  
			   $newarraycenters[$j]   = $larresultvenuestatus[$j]['idcenter'];			
			}			
			$minute = 0;
			$minuteforauto=0;
			$centerarray= array();
			$centerarrayvenue= array();
			$new = array();			
			for($i=0;$i<count($larrresult);$i++)
			{
			      if(in_array($larrresult[$i]['Date'],$newarrayexamdated))//for the exam dates in cronevenue
				  { 
				          // $minuteforauto =0;
						   $larresultvenues = $this->lobjchroneschedulemodel->fngetstatus($larrresult[$i]['Date']);						
						   for($k=0;$k<count($larresultvenues);$k++)
						   {
						      $larresultvenuess[$larresultvenues[$k]['idcenter']] = $larresultvenues[$k]['idcenter'];
						   }						  
						   if(!in_array($larrresult[$i]['idvenue'],$larresultvenuess))
						   {
							   $venuelist = $this->lobjchroneschedulemodel->getintialconfig();
							   $time = $venuelist['Scheduletime'];
                               $minuteforauto =0;							   
                               //$minuteforauto = $minuteforauto+1;
					           $larrresult[$i]['SDate'] = ($date= date('Y-m-d',strtotime ( '-'.$venuelist['Scheduleday'].'day' , strtotime ($larrresult[$i]['Date']))));;
							   $larrresult[$i]['ST'] = ($start = date("T"."H:i", strtotime( "$time + $minuteforauto mins")));
						       $larrresult[$i]['oldst'] = explode('IS',$larrresult[$i]['ST']);
					           $larrresult[$i]['ST'] = $larrresult[$i]['oldst'][1];								
					           $larraydateover = $larrresult[$i]['Date'];
						   }
						   else
						   {
						       $minuteforauto =0;
							   $larrresult[$i]['SDate'] = ($date= date('Y-m-d',strtotime ( '-'.$venuelist['Scheduleday'].'day' , strtotime ($larrresult[$i]['Date']))));;
						       $larrresult[$i]['ST'] = ($start = date("T"."H:i", strtotime( "$time + $minuteforauto mins")));
						       $larrresult[$i]['oldst'] = explode('IS',$larrresult[$i]['ST']);
					           $larrresult[$i]['ST'] = $larrresult[$i]['oldst'][1];								
					           $larraydateover = $larrresult[$i]['Date'];
						    }
						  } 
						  else
						  {
							   if(!in_array($larrresult[$i]['Date'],$centerarray))
								   { 						       
										$centerarray[] = $larrresult[$i]['Date'];
										$venuelist = $this->lobjchroneschedulemodel->getintialconfig();
										$time = $venuelist['Scheduletime'];
										$minute =0;								
									}
								   $minute = $minute+1;
								   $larrresult[$i]['SDate'] = ($date= date('Y-m-d',strtotime ( '-'.$venuelist['Scheduleday'].'day' , strtotime ($larrresult[$i]['Date']))));;
									
								   $larrresult[$i]['ST'] = ($start = date("T"."H:i", strtotime( "$time + $minute mins")));
									
								$larrresult[$i]['oldst'] = explode('IS',$larrresult[$i]['ST']);
								
								
								 $larrresult[$i]['ST'] = $larrresult[$i]['oldst'][1];								
								   $larraydateover = $larrresult[$i]['Date'];
							}
			}			
			$this->view->paginator = $larrresult;
			$this->view->lobjchronescheduleform->populate($larrformData);
		
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Clear' )) 
		{
			$this->_redirect( $this->baseUrl . '/scheduler/chronescheduling/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Approve' )) 
		{
			$larrformData = $this->_request->getPost ();
			if(isset($larrformData['idcenter']))
			{			
			for($i=0;$i<count($larrformData['idcenter']);$i++)
			{
			  $larraydetails[] =array();
			  $centerexamdate[] = explode(',',$larrformData['idcenter'][$i]);
			  for($j=0;$j<4;$j++)
			  {
			    $larraydetails[$i]['idcenter'] = $centerexamdate[$i][0];				
				$larraydetails[$i]['Examdate'] = date('Y-m-d', strtotime($centerexamdate[$i][1]));				
				$larraydetails[$i]['Scheduledate'] = $larrformData['SDate'][$centerexamdate[$i][1]][$larraydetails[$i]['idcenter']];
				$larraydetails[$i]['Scheduletime'] = $larrformData['Stime'][$centerexamdate[$i][1]][$larraydetails[$i]['idcenter']];
			  } 
			  
			}
				$larrformData['status'] = 0;
				$larrformData['UpdDate']=date('Y-m-d H:i:s');
				//$iduser=
				$larrformData['UpdUser']= $this->auth->getIdentity()->iduser;
				//$larrformData['UpdUser']=1;
				$larresultchronevenue = $this->lobjchroneschedulemodel->fninsertintochronevenue($larrformData,$larraydetails);
		  }
		}
	    if ($this->_request->isPost () && $this->_request->getPost ( 'updatenow' )) 
		{
			     $larrformData = $this->_request->getPost ();
			     $larresult = $this->lobjchroneclientmodel->fnupdatevenuestudents($larrformData);
	    }
	}
	public function deletechroneAction()
	{
	    //$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$idauto = $this->_getParam('id');
		$idcvenue = $this->_getParam('idcvenue');
		echo $idauto = $this->_getParam('id');
    	//$this->lobjuploadmodule->fndocdelete($docid);
    	

		//echo $idauto;
		//echo $idcvenue;
	    $this->lobjchroneschedulemodel->fntodeletecrone($idauto);
		$this->_redirect($this->baseurl."scheduler/chronescheduling/index");
	}
	
	
}
