<?php
class Scheduler_SessionconstraintsController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	private $_gobjlogger;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
	
	public function fnsetObj(){
		$this->lobjprogram = new Scheduler_Model_DbTable_Sessionconstraints();
		$this->lobjprogramForm = new Scheduler_Form_Sessionconstraints(); 
	}
	
	public function indexAction() {
    	$this->view->title="Session Constraints";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = array();//$this->lobjprogram->fngetSessionDetails();
		$larrcenters = $this->lobjprogram->fngetcenters();
		$this->lobjform->field19->addmultioptions($larrcenters); 	
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->sessionconpaginatorresult);
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance		
		if(isset($this->gobjsessionsis->sessionconpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->sessionconpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
			$larrformData = $this->_request->getPost ();
			if($this->lobjform->isValid ( $larrformData ))
			{
				$larrresult = $this->lobjprogram->fnSearchSession( $larrformData ); //searching the values for the user
				//echo "<pre>";print_r($larrresult);die();
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->sessionconpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
		}
		
	}
	
public function newsessionconstraintsAction() {
		$this->view->lobjprogramForm = $this->lobjprogramForm;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
			$formData = $this->getRequest()->getPost();
			//echo "<pre>";
			//print_r($formData);die();
			unset ( $formData ['Save'] );
			unset ( $formData ['Back'] );
			$larrresult = $this->lobjprogram->fnGetsearchdetails($formData);
			$this->view->paginator = $larrresult;
			$this->lobjprogramForm->populate($formData);
			
			
			$auth = Zend_Auth::getInstance();
			// Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Sessionconstraints Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
			//$this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
		}
	}
    	public function editschedulerconstraintsAction()
		{
		  $this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view
		  $Idscheduler = $this->_getParam('id');
		  $larrVenueresult = $this->lobjprogram->fngetVenueDetailsforidcenter($Idscheduler);
		   $this->view->center = $larrVenueresult['idcenter'];
		   $this->view->state = $larrVenueresult['state'];
		   $this->view->city = $larrVenueresult['city'];
		   
		 // echo "<pre>";
		 // print_R($larrVenueresult);die();
		  $this->view->larrVenueresult=$larrVenueresult;
		  $this->view->Idschedule = $Idscheduler;
		  $schedulerresult = $this->lobjprogram->fngetschedulerresult($Idscheduler);
		  if(empty($schedulerresult))
		  {
			   echo '<script language="javascript">alert("Some Candidates Already Registered To This Date Still U Want Deactivate This")</script>';
		      // echo "<script>parent.location = '".$this->view->baseUrl()."/scheduler/sessionconstraints/newsessionconstraints/search/1'</script>";
 		      // die();
			  
		     $larresult = $this->lobjprogram->fngetschedulerdetails($Idscheduler);
		    //$this->view->lobjprogramForm->constraintname->setValue($result['constraintname']);
		    $this->view->lobjprogramForm->Venue->setValue($larresult['centername']);
    	    $this->view->lobjprogramForm->Session->setValue($larresult['managesessionname']);
    	    $this->view->lobjprogramForm->Total->setValue($larresult['Totalcapacity']);
		   $this->view->lobjprogramForm->Alloted->setValue($larresult['Allotedseats']);
		   $this->view->lobjprogramForm->Date->setValue($larresult['date']);
		   $this->view->lobjprogramForm->Venue->setAttrib('readonly','readonly'); 
		   $this->view->lobjprogramForm->Alloted->setAttrib('readonly','readonly'); 
		   $this->view->lobjprogramForm->Session->setAttrib('readonly','readonly'); 
	       $this->view->lobjprogramForm->Date->setAttrib('readonly','readonly'); 
		 }
		 else
		 {
		     $larresult = $this->lobjprogram->fngetschedulerdetails($Idscheduler);
		    //$this->view->lobjprogramForm->constraintname->setValue($result['constraintname']);
		    $this->view->lobjprogramForm->Venue->setValue($larresult['centername']);
    	    $this->view->lobjprogramForm->Session->setValue($larresult['managesessionname']);
    	    $this->view->lobjprogramForm->Total->setValue($larresult['Totalcapacity']);
		   $this->view->lobjprogramForm->Alloted->setValue($larresult['Allotedseats']);
		   $this->view->lobjprogramForm->Date->setValue($larresult['date']);
		   $this->view->lobjprogramForm->Venue->setAttrib('readonly','readonly'); 
		   $this->view->lobjprogramForm->Alloted->setAttrib('readonly','readonly'); 
		   $this->view->lobjprogramForm->Session->setAttrib('readonly','readonly'); 
	       $this->view->lobjprogramForm->Date->setAttrib('readonly','readonly'); 
		    //echo "<script>parent.location = '".$this->view->baseUrl()."/scheduler/sessionconstraints/newsessionconstraints/id'".$Idscheduler."</script>";
		 }
		 if ($this->_request->isPost() && $this->_request->getPost('Save'))
		 {
			$larrformData = $this->_request->getPost();
			$this->lobjprogram->fnupdatescheduler($larrformData);
			 
			//echo "<pre>";
			//print_r($larrformData);die();
			$this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
		 }
			
		
		}
	public function editsessionconstraintsAction()
	{
		$this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view
		$this->view->lobjprogramForm->Date->setAttrib('readonly','readonly');		
    	$IdProgram = $this->_getParam('id');
    	$this->view->lobjprogramForm->idschedulerconstraints->setValue($IdProgram);
    	$larrresults = $this->lobjprogram->fngetDaysDetails();
		$larrresult2 = $this->lobjprogram->fngetSateDetails();
		$this->lobjprogramForm->idstate->addMultiOptions($larrresult2);
    	$result=$this->lobjprogram->fnGetsessionList($IdProgram);
    	$this->lobjprogramForm->idcity->addMultiOptions($this->lobjprogram->fnGetcityList($result['state']));
        $this->lobjprogramForm->idvenue->addMultiOptions($this->lobjprogram->fnGetVenueList($result['city']));
    	$this->view->lobjprogramForm->constraintname->setValue($result['constraintname']);
    	$this->view->lobjprogramForm->idstate->setValue($result['state']);
    	$this->view->lobjprogramForm->idcity->setValue($result['city']);
    	$this->view->lobjprogramForm->idvenue->setValue($result['idstate']);
    	$this->view->lobjprogramForm->Date->setValue($result['Date']);
    	$this->view->lobjprogramForm->Active->setValue($result['Active']);
		
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjprogramForm->isValid($formData)) {
	    		unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
	   			$lintIdProgram = $formData['idschedulerconstraints'];
				$larrresults = $this->lobjprogram->fnupdateSession($formData);//update university
				$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the New Sessionconstraints with Id = ".$IdProgram."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
			}
    	}
    }
    public function getvenuelistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		//get all the city that correspond to the selected state
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjprogram->fnGetVenueList($lintIdCountry));
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}			
}