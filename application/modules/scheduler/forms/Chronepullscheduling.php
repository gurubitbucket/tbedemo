<?php
//Formclass for the Venuescheduler module
class Scheduler_Form_Chronepullscheduling extends Zend_Dojo_Form 
{ 
	public function init() 
	{
	        
		   $month= date("m"); // Month value
		   $day=  date("d"); //today's date
		  
		   $year= date("Y"); // Year value
		   //$this->lobjchroneschedulemodel = new Scheduler_Model_DbTable_Chronescheduling();
		   //$venuelist = $this->lobjchroneschedulemodel->getintialconfig();
		
		   $yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day+2),$year));
		   $yesterday= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		   
		   $dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		   $dateofbirth1 = "{min:'$yesterday',datePattern:'dd-MM-yyyy'}";
    	   $gstrtranslate =Zend_Registry::get('Zend_Translate'); 
		   
		   
		  
			$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "newsessionlists();")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
			
							  
							  
							  
			$Status = new Zend_Form_Element_Text('Status');
		    $Status	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		    $Status  	->setAttrib('maxlength','1')
						      ->setAttrib('required',"true")
							  ->removeDecorator("Label")
							  ->removeDecorator("DtDdWrapper")
							  ->removeDecorator('HtmlTag');				  
							  
            $Scheduletime = new Zend_Form_Element_Text('Scheduletime');
            $Scheduletime->removeDecorator("DtDdWrapper"); 
            $Scheduletime->removeDecorator("Label");
            $Scheduletime->removeDecorator('HtmlTag');
		    $Scheduletime->setAttrib('dojoType',"dijit.form.TimeTextBox")
					       ->setAttrib('constraints',"{timePattern: 'HH:mm'}");		
 
            $Scheduleday = new Zend_Dojo_Form_Element_DateTextBox('Scheduleday');
		    $Scheduleday	->setAttrib('dojoType',"dijit.form.DateTextBox");
		    $Scheduleday->setAttrib('title',"dd-mm-yyyy");
		    $Scheduleday->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		    //$Schedulerdate->setAttrib('constraints', "$dateofbirth1");
		    $Scheduleday->removeDecorator("Label");
		    $Scheduleday->removeDecorator("DtDdWrapper");
		    $Scheduleday->removeDecorator('HtmlTag');	 
							  
			$FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
		    $FromDate->setAttrib('dojoType',"dijit.form.DateTextBox")
			         ->setAttrib('onChange',"dijit.byId('ToDate').constraints.min = arguments[0];")
		             ->setAttrib('title',"dd-mm-yyyy")
	  	             ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
		             ->setAttrib('constraints',"$dateofbirth")
                     ->setAttrib('required',"true")					 
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');							
						
	        $ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
	        $ToDate ->setAttrib('dojoType',"dijit.form.DateTextBox")
			        //->setAttrib('onChange', "dijit.byId('FromDate').constraints.max = arguments[0];")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						 ->setAttrib('required',"true")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');

		    
    
		    $Active  = new Zend_Form_Element_Checkbox('Active');
        	$Active->setAttrib('dojoType',"dijit.form.CheckBox");
        	$Active->setvalue('1');
        	$Active->removeDecorator("DtDdWrapper");
        	$Active->removeDecorator("Label");
        	$Active->removeDecorator('HtmlTag');
		
		    $Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');        
        	$Venue->removeDecorator("DtDdWrapper");
        	$Venue->setAttrib('required',"true");
        	$Venue->removeDecorator("Label");
        	$Venue->removeDecorator('HtmlTag');        
		    $Venue->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		    $Search = new Zend_Form_Element_Submit('Search');
        	$Search->dojotype="dijit.form.Button";
        	$Search->label = $gstrtranslate->_("Search");
       		$Search->removeDecorator("DtDdWrapper");
        	$Search->removeDecorator("Label");
       		$Search->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
		$Schedulerdate = new Zend_Dojo_Form_Element_DateTextBox('Schedulerdate');
		$Schedulerdate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Schedulerdate->setAttrib('title',"dd-mm-yyyy");
		$Schedulerdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Schedulerdate->setAttrib('constraints', "$dateofbirth1");
		$Schedulerdate->removeDecorator("Label");
		$Schedulerdate->removeDecorator("DtDdWrapper");
		$Schedulerdate->removeDecorator('HtmlTag');							

		
		$ScheduleStartTime = new Zend_Form_Element_Text('ScheduleStartTime');
        $ScheduleStartTime->removeDecorator("DtDdWrapper"); 
        $ScheduleStartTime->removeDecorator("Label");
        $ScheduleStartTime->removeDecorator('HtmlTag');
		$ScheduleStartTime->setAttrib('dojoType',"dijit.form.TimeTextBox")
					->setAttrib('constraints',"{timePattern: 'HH:mm'}");
					
					$Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');
		$Venue->removeDecorator("DtDdWrapper");
		$Venue->setAttrib('required',"true") ;
		$Venue->removeDecorator("Label");
		$Venue->removeDecorator('HtmlTag');
		//$Venue->setAttrib('OnChange', 'fnGetCountryStateList');
		$Venue->setRegisterInArrayValidator(false);
		$Venue->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		
		
				$Session = new Zend_Dojo_Form_Element_FilteringSelect('Session');
			    $Session->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Session->removeDecorator("DtDdWrapper");
	            $Session->removeDecorator("Label");
	            $Session->removeDecorator('HtmlTag');
	            
		
		
    		//form elements
        	$this->addElements(array($Date,$Session,$Status,$Schedulerdate,$ScheduleStartTime,$Venue,$Active,$Search,$FromDate,$ToDate,$Scheduleday,$Scheduletime,$Venue));

    	}
}
