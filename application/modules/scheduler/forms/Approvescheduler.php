<?php
//Formclass for the Venuescheduler module
class Scheduler_Form_Approvescheduler extends Zend_Dojo_Form 
{ 
	public function init() 
	{
    		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

		
    
		   $Active  = new Zend_Form_Element_Checkbox('Active');
        	$Active->setAttrib('dojoType',"dijit.form.CheckBox");
        	$Active->setvalue('1');
        	$Active->removeDecorator("DtDdWrapper");
        	$Active->removeDecorator("Label");
        	$Active->removeDecorator('HtmlTag');
		
	
		
		 $Description = new Zend_Form_Element_Text('Description');
		$Description->setAttrib('dojoType',"dijit.form.ValidationTextBox")
        //$Venue->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
		
		    $Search = new Zend_Form_Element_Submit('Search');
        	$Search->dojotype="dijit.form.Button";
        	$Search->label = $gstrtranslate->_("Search");
       		$Search->removeDecorator("DtDdWrapper");
        	$Search->removeDecorator("Label");
       		$Search->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
		
		    $Clear = new Zend_Form_Element_Submit('Clear');
        	$Clear->dojotype="dijit.form.Button";
        	$Clear->label = $gstrtranslate->_("Clear");
       		$Clear->removeDecorator("DtDdWrapper");
        	$Clear->removeDecorator("Label");
       		$Clear->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
				
			$Save = new Zend_Form_Element_Submit('Save');
        	$Save->dojotype="dijit.form.Button";
        	$Save->label = $gstrtranslate->_("Approve");
       		$Save->removeDecorator("DtDdWrapper");
        	$Save->removeDecorator("Label");
       		$Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";

            $Status = new Zend_Dojo_Form_Element_FilteringSelect('Status');
	        $Status->setAttrib('dojoType',"dijit.form.FilteringSelect")
	    	   ->setAttrib('required',"false")
	    	   ->setRegisterInArrayValidator(false);
	        $Status->addMultiOptions(array('0'=>'Select','1'=>'Approved','2'=>'Not Approved')); 	
            //$Action ->setAttrib('onChange', "checkforconstrainsname(this.value);");		 
	        $Status->removeDecorator("DtDdWrapper");
	        $Status->removeDecorator("Label");
	        $Status->removeDecorator('HtmlTag');				
		
    		//form elements
        	$this->addElements(array($Description,$Active,$Search,$Clear,$Save,$Status));

    	}
}
