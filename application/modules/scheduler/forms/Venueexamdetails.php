<?php
class Scheduler_Form_Venueexamdetails extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
        
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		$dateofbirth1 = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$iduser = new Zend_Form_Element_Hidden('idcenter');
        $iduser->removeDecorator("DtDdWrapper");
        $iduser->removeDecorator("Label");
        $iduser->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
		
		$Fromdate = new Zend_Dojo_Form_Element_DateTextBox('Fromdate');
	    $Fromdate ->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Todate').constraints.min = arguments[0];")
						//->setAttrib('onChange', "examtoDateSetting();")
						->setAttrib('required','true')
						->setAttrib('constraints',"$dateofbirth")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
		
		$Todate = new Zend_Dojo_Form_Element_DateTextBox('Todate');
	    $Todate ->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						//->setAttrib('onChange', "fngetexamcentre();")
						->setAttrib('onChange', "dijit.byId('Fromdate').constraints.max = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth1")
						->setAttrib('required',"true")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
		$Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');
        $Venue->removeDecorator("DtDdWrapper");
        $Venue->setAttrib('required',"true") ;
        $Venue->removeDecorator("Label");
        $Venue->removeDecorator('HtmlTag');
        //$Venue->setAttrib('OnChange', 'fnGetCountryStateList');
        $Venue->setRegisterInArrayValidator(false);
		$Venue->setAttrib('dojoType',"dijit.form.FilteringSelect");	
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class','NormalBtn');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = $gstrtranslate->_("Clear");
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator('HtmlTag');
		
		$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
		
		$submit = new Zend_Form_Element_Submit('Search');
		//$submit->setAttrib('Onclick', 'fncheckactiveornot()');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag');
		$submit->class = "NormalBtn";
		
		$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype="dijit.form.Button";
		$Save->label = $gstrtranslate->_("Save");
		$Save->setAttrib('Onclick', 'return validateform()');		
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator("Label");
		$Save->removeDecorator('HtmlTag');
		
        //form elements
        $this->addElements(array($iduser,$UpdDate,$UpdUser,$Fromdate,$Todate,$Venue,$Clear,$submit,$Save,$Active
                                 ));

    }
}