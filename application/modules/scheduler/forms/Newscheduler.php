<?php
class Scheduler_Form_Newscheduler extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$idnewscheduler = new Zend_Form_Element_Hidden('idnewscheduler');
        $idnewscheduler->removeDecorator("DtDdWrapper");
        $idnewscheduler->removeDecorator("Label");
        $idnewscheduler->removeDecorator('HtmlTag');
        
        
       $Year= new Zend_Dojo_Form_Element_FilteringSelect('Year');
       $Year->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('2012'=>'2012',
													'2013'=>'2013',
													'2014'=>'2014',
													'2015'=>'2015',
													'2016'=>'2016',
													'2017'=>'2017',
													'2018'=>'2018',
													'2019'=>'2019',
													'2020'=>'2020',
													))
							->setAttrib('OnChange', 'fnGetMonthList')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
     
        
        
       $From= new Zend_Dojo_Form_Element_FilteringSelect('From');
       $From	->setAttrib('dojoType',"dijit.form.FilteringSelect")
       ->addmultioptions(array())
                            ->setAttrib('OnChange', 'fnGetToMonthList')						
							->removeDecorator("DtDdWrapper")
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');

	   $To= new Zend_Dojo_Form_Element_FilteringSelect('To');
       $To->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');							

							
							
	    
		     
        $description = new Zend_Form_Element_Text('description');
		$description->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $description->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
      	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
    		
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($idnewscheduler,
        						 $Year,
        						 $From,
        						 $To,
        						 $description,
                                 $Active,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $Back));

    }
}