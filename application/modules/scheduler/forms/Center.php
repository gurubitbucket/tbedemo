<?php
class Scheduler_Form_Center extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    
		$iduser = new Zend_Form_Element_Hidden('idcenter');
        $iduser->removeDecorator("DtDdWrapper");
        $iduser->removeDecorator("Label");
        $iduser->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
   
        $loginName = new Zend_Form_Element_Text('centername');
      //$loginName->addValidator(new Zend_Validate_Db_NoRecordExists('tbl_user', 'loginName'));	
		$loginName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $loginName->setAttrib('required',"true")      
               ->setAttrib('propercase','true') 
               ->setAttrib('OnChange','duplicatecenter(this.value)')       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
       

        
        $lName = new Zend_Form_Element_Text('centercode');
		$lName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $lName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                  
       
        $mName = new Zend_Form_Element_Text('contactperson');
		$mName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $mName->setAttrib('maxlength','20')      
               ->setAttrib('required',"true")
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
               
        $fName = new Zend_Form_Element_Text('fName');
		$fName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $fName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        	
             
        $addr1 = new Zend_Form_Element_Text('addr1');
		$addr1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $addr1->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                   
        $addr2 = new Zend_Form_Element_Text('addr2');
		$addr2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $addr2->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                  
        		
        		
      	$ipaddress =  new Zend_Dojo_Form_Element_FilteringSelect('ipaddress');
		$ipaddress->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $ipaddress->setAttrib('required',"true")  
                  	->addmultioptions(array('0'=>'No',
											'1'=>'Yes',))   
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $centerlogin = new Zend_Form_Element_Text('centerlogin');
		$centerlogin->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $centerlogin->setAttrib('maxlength','50')
        ->setAttrib('OnBlur', 'duplicatecenter(this.value)')  
                  ->setAttrib('required',"true")     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $centerpassword = new Zend_Form_Element_Password('centerpassword');	
        $centerpassword->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $centerpassword->setAttrib('required',"true") ;
        $centerpassword->removeDecorator("DtDdWrapper");
        $centerpassword->removeDecorator("Label");
        $centerpassword->removeDecorator('HtmlTag');
        
           		
        $paddr1 = new Zend_Form_Element_Text('paddr1');
		$paddr1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $paddr1->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                   
        $paddr2 = new Zend_Form_Element_Text('paddr2');
		$paddr2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $paddr2->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        
        $city = new Zend_Dojo_Form_Element_FilteringSelect('city');
		$city->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $city->setAttrib('maxlength','20')     
               ->setAttrib('required',"true") 		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        $city->setRegisterInArrayValidator(false);
        $city->setAttrib('dojoType',"dijit.form.FilteringSelect"); 
        		
        $seats = new Zend_Form_Element_Hidden('NumberofSeat');     
        $seats	->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        
        $country = new Zend_Dojo_Form_Element_FilteringSelect('country');
        $country->removeDecorator("DtDdWrapper");
        $country->setAttrib('required',"true") ;
        $country->removeDecorator("Label");
        $country->removeDecorator('HtmlTag');
        $country->setAttrib('OnChange', 'fnGetCountryStateList');
        $country->setRegisterInArrayValidator(false);
		$country->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
       
        $state = new Zend_Dojo_Form_Element_FilteringSelect('state');
        $state->removeDecorator("DtDdWrapper");
        $state->setAttrib('required',"true") ;
        $state->removeDecorator("Label");
        $state->removeDecorator('HtmlTag');
        $state->setRegisterInArrayValidator(false);
		$state->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $state->setAttrib('OnChange', 'fnGetCityList');     
        
        $zipCode = new Zend_Form_Element_Text('zipCode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$zipCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $zipCode->setAttrib('maxlength','6')
        ->setAttrib('required',"true");   
        $zipCode->removeDecorator("DtDdWrapper");
        $zipCode->removeDecorator("Label");
        $zipCode->removeDecorator('HtmlTag');

        
        $workcountrycode = new Zend_Form_Element_Text('workcountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$workcountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workcountrycode->setAttrib('maxlength','3');  
        $workcountrycode->setAttrib('style','width:30px');  
        $workcountrycode->removeDecorator("DtDdWrapper");
        $workcountrycode->removeDecorator("Label");
        $workcountrycode->removeDecorator('HtmlTag');
        
        $workstatecode = new Zend_Form_Element_Text('workstatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$workstatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workstatecode->setAttrib('maxlength','5');  
        $workstatecode->setAttrib('style','width:30px');  
        $workstatecode->removeDecorator("DtDdWrapper");
        $workstatecode->removeDecorator("Label");
        $workstatecode->removeDecorator('HtmlTag');
        
        
        $workPhone = new Zend_Form_Element_Text('workPhone',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Work Phone No."));
		$workPhone->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workPhone->setAttrib('maxlength','10');   
        $workPhone->setAttrib('style','width:93px');  
        $workPhone->removeDecorator("DtDdWrapper");
        $workPhone->removeDecorator("Label");
        $workPhone->removeDecorator('HtmlTag');
        
        
        $countrycode = new Zend_Form_Element_Text('countrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$countrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $countrycode->setAttrib('maxlength','3');  
        $countrycode->setAttrib('style','width:30px');
                   //  ->setAttrib('required',"true");   
        $countrycode->removeDecorator("DtDdWrapper");
        $countrycode->removeDecorator("Label");
        $countrycode->removeDecorator('HtmlTag');
        
        $statecode = new Zend_Form_Element_Text('statecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$statecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $statecode->setAttrib('maxlength','5');  
        $statecode->setAttrib('style','width:30px');
                 // ->setAttrib('required',"true");  
        $statecode->removeDecorator("DtDdWrapper");
        $statecode->removeDecorator("Label");
        $statecode->removeDecorator('HtmlTag');
        
        
        $cellPhone = new Zend_Form_Element_Text('cellPhone',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$cellPhone->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $cellPhone->setAttrib('maxlength','10');  
        $cellPhone->setAttrib('style','width:93px')
                  ->setAttrib('required',"true");  
        $cellPhone->removeDecorator("DtDdWrapper");
        $cellPhone->removeDecorator("Label");
        $cellPhone->removeDecorator('HtmlTag');

        $Nooffloors = new Zend_Form_Element_Text('Nooffloors',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$Nooffloors->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Nooffloors->setAttrib('maxlength','20');  
        $Nooffloors ->setAttrib('required',"true")
                    ->setAttrib('onBlur','fngetFloors(this.value)');  
        $Nooffloors->removeDecorator("DtDdWrapper");
        $Nooffloors->removeDecorator("Label");
        $Nooffloors->removeDecorator('HtmlTag');
       
        $fax = new Zend_Form_Element_Text('fax',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Fax"));
		$fax->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $fax->setAttrib('maxlength','20');
        $fax->removeDecorator("DtDdWrapper");
        $fax->removeDecorator("Label");
        $fax->removeDecorator('HtmlTag');
        
       
        $email = new Zend_Form_Element_Text('email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $email->setAttrib('required',"true")  			 
        		->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        
        $UserStatus  = new Zend_Form_Element_Checkbox('Active');
        $UserStatus->setAttrib('dojoType',"dijit.form.CheckBox");
        $UserStatus->setvalue('1');
        $UserStatus->removeDecorator("DtDdWrapper");
        $UserStatus->removeDecorator("Label");
        $UserStatus->removeDecorator('HtmlTag');
        
 
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
        	
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class','NormalBtn');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = $gstrtranslate->_("Clear");
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator('HtmlTag');
				
		$Close = new Zend_Form_Element_Button('Close');
		$Close->dojotype="dijit.form.Button";
       	$Close->label = $gstrtranslate->_("Back");
		$Close->setAttrib('class', 'NormalBtn')				
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Venuenew = new Zend_Dojo_Form_Element_FilteringSelect('Venuenew',array('hasDownArrow'=>'false'));
        $Venuenew->removeDecorator("DtDdWrapper");
        $Venuenew->removeDecorator("Label");
        $Venuenew->removeDecorator('HtmlTag');
		$Venuenew->setAttrib('dojoType',"dijit.form.FilteringSelect"); 
  
	
		
		 $submit = new Zend_Form_Element_Submit('Search');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag');
		$submit->class = "NormalBtn";
    
        
    $hostname = new Zend_Form_Element_Text('hostname',array(""));
		$hostname->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $hostname->removeDecorator("DtDdWrapper");
        $hostname->removeDecorator("Label");
        $hostname->removeDecorator('HtmlTag');
        
    
        //form elements
        $this->addElements(array($iduser,$loginName,$UpdDate,$lName,$UpdUser,
                                 $mName,$fName,$Nooffloors,$centerlogin,$centerpassword,
                                 $addr1,$ipaddress,$Venuenew,
                                 $addr2,$paddr1,$paddr2,$city,$state,
                                 $country,$zipCode,$UserStatus,$workcountrycode,$workstatecode,$countrycode,$statecode,
                                 $workPhone,$seats,$submit,
                                 $cellPhone,$fax,$Save,$email,
                                 $Clear,$Close,$Add,$hostname
                                 ));

    }
}