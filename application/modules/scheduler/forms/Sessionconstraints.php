<?php
class Scheduler_Form_Sessionconstraints extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	
    	
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day+1),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
    	
		$IdProgram = new Zend_Form_Element_Hidden('idschedulerconstraints');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');
        
        $ProgramName = new Zend_Form_Element_Text('constraintname');	
		$ProgramName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ProgramName->setAttrib('required',"true")  
                ->setAttrib('propercase','true')      			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        		
       /* $idday= new Zend_Dojo_Form_Element_FilteringSelect('idday');
        $idday	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');	*/
							
		$state = new Zend_Dojo_Form_Element_FilteringSelect('idstate');
        $state->removeDecorator("DtDdWrapper");
        $state->setAttrib('required',"true") ;
        $state->removeDecorator("Label");
        $state->removeDecorator('HtmlTag');
		$state->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$state->setRegisterInArrayValidator(false);
        $state->setAttrib('OnChange', 'fnGetCityList');
        		
        
        $city = new Zend_Dojo_Form_Element_FilteringSelect('idcity');
        $city->removeDecorator("DtDdWrapper");
        $city->setAttrib('required',"true") ;
        $city->removeDecorator("Label");
        $city->removeDecorator('HtmlTag');
        $city->setRegisterInArrayValidator(false);
		$city->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $city->setAttrib('OnChange', 'fnGetvenueList');
        
        $idvenue= new Zend_Dojo_Form_Element_FilteringSelect('idvenue');
        $idvenue	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->setAttrib('required',"true") 	 
							->setRegisterInArrayValidator(false)
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');	
        
        
        $Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
        $Date->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Date->setAttrib('required',"true");
        $Date->removeDecorator("DtDdWrapper");
        $Date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
            ->setAttrib('constraints', "$dateofbirth");
        $Date->removeDecorator("Label");
        $Date->removeDecorator('HtmlTag');
		
		$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

      
				
			

        $Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
        	
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class','NormalBtn');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = $gstrtranslate->_("Clear");
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator('HtmlTag');		
    		
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Venue = new Zend_Form_Element_Text('Venue');	
		$Venue->setAttrib('dojoType',"dijit.form.ValidationTextBox")
                // $Venue->setAttrib('required',"true")  
               // ->setAttrib('propercase','true')      			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');	
				
        $Session = new Zend_Form_Element_Text('Session');	
		$Session ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
                // $Venue->setAttrib('required',"true")  
               // ->setAttrib('propercase','true')      			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        
        $Total = new Zend_Form_Element_Text('Total');	
		$Total ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
                ->setAttrib('required',"true")  
				->setAttrib('width','5px') 
               // ->setAttrib('propercase','true') 
                ->setAttrib('OnBlur','fnvalidate(this.value)')						   
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
				
	    
				
        $Alloted = new Zend_Form_Element_Text('Alloted');	
		$Alloted ->setAttrib('dojoType',"dijit.form.ValidationTextBox")
                // $Venue->setAttrib('required',"true")  
               // ->setAttrib('propercase','true')      			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');	


        $Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	    $Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->setAttrib('onChange', "fngetexamcentre();")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
		
		$Date1 = new Zend_Dojo_Form_Element_DateTextBox('Date');
	    $Date1->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Schedulertodate').constraints.min = arguments[0];")
						//->setAttrib('onChange', "examtoDateSetting();")
						->setAttrib('required','true')
						->setAttrib('constraints',"$dateofbirth")	    
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');	
						
        $Schedulertodate = new Zend_Dojo_Form_Element_DateTextBox('Schedulertodate');
        $Schedulertodate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Schedulertodate->setAttrib('required',"true");
        $Schedulertodate->removeDecorator("DtDdWrapper");
		//->setAttrib('onChange', "dijit.byId('Date').constraints.max = arguments[0];")
        $Schedulertodate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
           $Schedulertodate ->setAttrib('onChange', "fngetexamcentre();");
        $Schedulertodate->removeDecorator("Label");
        $Schedulertodate->removeDecorator('HtmlTag');		

         $Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
	    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect")
	    	   ->setAttrib('required',"false")
	    	   ->setRegisterInArrayValidator(false);
	    $Venues->removeDecorator("DtDdWrapper");
	    $Venues->removeDecorator("Label");
	    $Venues->removeDecorator('HtmlTag');
		
		
        $Action = new Zend_Dojo_Form_Element_FilteringSelect('Action');
	    $Action->setAttrib('dojoType',"dijit.form.FilteringSelect")
	    	   ->setAttrib('required',"false")
	    	   ->setRegisterInArrayValidator(false);
	    $Action->addMultiOptions(array('1'=>'Active','0'=>'Inactive','2'=>'View Exceptions','3'=>'Change Seat Capacity')); 	
        //$Action ->setAttrib('onChange', "checkforconstrainsname(this.value);");		 
	    $Action->removeDecorator("DtDdWrapper");
	    $Action->removeDecorator("Label");
	    $Action->removeDecorator('HtmlTag');

        $day = new Zend_Dojo_Form_Element_FilteringSelect('Day');
	    $day->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $day->addMultiOptions(array(''=>'All Days','Sunday'=>'Sunday','Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday')); 	           	         		       		     
	    $day->removeDecorator("DtDdWrapper");
	    $day->removeDecorator("Label");
	    $day->removeDecorator('HtmlTag');	

        $Apply = new Zend_Form_Element_Submit('Apply');
        $Apply->label = $gstrtranslate->_("Save");
        $Apply->dojotype="dijit.form.Button";
		$Apply->label = $gstrtranslate->_("Save");
		//$Apply->setAttrib('Onclick', 'return validateform()');
        $Apply->removeDecorator("DtDdWrapper");
        $Apply->removeDecorator('HtmlTag')
         		->class = "NormalBtn";	
				
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
		$Save->setAttrib('Onclick', 'return validateform()');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";					

        //form elements
        $this->addElements(array($IdProgram,
        						 $ProgramName,$Date,$Date1,$Date2,
        						 $state,$Apply,
        						 $city,$day,$Action,
        						 $idvenue,
                                 $Active,
                                 $UpdDate,
                                 $UpdUser,$Venues,
                                 $Save,$Venue,$Session,$Total,$Alloted,
                                 $Back,$Schedulertodate,$Search,$Clear));

    }
}