<?php
//Formclass for the Venuescheduler module
class Scheduler_Form_Venuescheduler extends Zend_Dojo_Form 
{ 
	public function init() 
	{
    		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 

		 $Date7 = new Zend_Dojo_Form_Element_DateTextBox('Date7');
	   	 $Date7 ->setAttrib('dojoType',"dijit.form.DateTextBox")
	        	->setAttrib('title',"dd-mm-yyyy")
			->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
			->setAttrib('class', 'txt_put')
			->setAttrib('required',"true")
			->setAttrib('onChange', "examtoDateSetting();")
			->removeDecorator("Label")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag');
		
		$Date8 = new Zend_Dojo_Form_Element_DateTextBox('Date8');
	    	$Date8  ->setAttrib('dojoType',"dijit.form.DateTextBox")
	                ->setAttrib('title',"dd-mm-yyyy")
			->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
			->setAttrib('class', 'txt_put')
			->setAttrib('required',"true")
			->removeDecorator("Label")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag'); 
    
		$Active  = new Zend_Form_Element_Checkbox('Active');
        	$Active->setAttrib('dojoType',"dijit.form.CheckBox");
        	$Active->setvalue('1');
        	$Active->removeDecorator("DtDdWrapper");
        	$Active->removeDecorator("Label");
        	$Active->removeDecorator('HtmlTag');
		
		$Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');        
        	$Venue->removeDecorator("DtDdWrapper");
        	$Venue->setAttrib('required',"true");
        	$Venue->removeDecorator("Label");
        	$Venue->removeDecorator('HtmlTag');        
		$Venue->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Search = new Zend_Form_Element_Submit('Search');
        	$Search->dojotype="dijit.form.Button";
        	$Search->label = $gstrtranslate->_("Search");
       		$Search->removeDecorator("DtDdWrapper");
        	$Search->removeDecorator("Label");
       		$Search->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
		
		
		
    		//form elements
        	$this->addElements(array($Date7,$Date8,$Venue,$Active,$Search));

    	}
}
