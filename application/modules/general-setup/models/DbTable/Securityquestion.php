<?php
class GeneralSetup_Model_DbTable_Securityquestion extends  Zend_Db_Table  { 
	protected $_name = 'tbl_securityquestions';				
	/*
	 * function for inserting into the account head table
	 */
	public function fnaddnewsecurityquestion($insertData) {		
       	$db= Zend_Db_Table::getDefaultAdapter();
       	$table 	= "tbl_securityquestions";
	   	$db->insert($table,$insertData);
	   		   
	}
	
	/*
	 * //getting for grid view 
	 */
	Public function fngetSecurityquestionDetails() { 
	    		
		$db 	= Zend_Db_Table::getDefaultAdapter();
		$select = 	$db->select()          
             	->	from(array('a' => 'tbl_securityquestions'),
                   				 array('a.*'));		
		$result = $db->fetchAll($select);	
		return $result;
	 }
    public function fnSecurityquestionSearch($lobjgetarr)
    {
		// search function for the Securityquestion
		$db 	= Zend_Db_Table::getDefaultAdapter();
		if(!$lobjgetarr['field7']) $sqlquery ="  a.Active = 0";
		else $sqlquery	= " a.Active = 1 ";		
		$select = 	$db->select()          
             	->	from(array('a' => 'tbl_securityquestions'),
                   				 array('a.SecurityQuestions'))
                -> where('a.SecurityQuestions like  ? "%"',$lobjgetarr['field3'])
            	-> where($sqlquery);       
		$result = $db->fetchAll($select);	
		return $result;			
	 }
	 
      public function fnupdatefunction($id)
			{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect =$lobjDbAdpt->select()
									->from(array('b'=>'tbl_securityquestions'),array('b.*'))
									->where('b.idsecurityquestions='.$id);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
			}
			
        public function fnupdateSecurityquestion($lintsecurityquestions,$data) { 
	        $lintwhere = "idsecurityquestions = ".$lintsecurityquestions;
	         $this->update($data,$lintwhere);
            }
}
