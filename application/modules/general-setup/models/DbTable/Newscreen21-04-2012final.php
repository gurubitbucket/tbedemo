<?php
	class GeneralSetup_Model_DbTable_Newscreen extends Zend_Db_Table {
		
		
		protected $_name = 'tbl_studentapplication';
	

 public function fngetstudentinformation()
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										     ->join(array("f" =>"tbl_studentpaymentoption"),'f.IDApplication=a.IDApplication',array('f.*'))
										    ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										    ->joinleft(array("e" =>"tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array('e.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->where("a.pass=3")
										  ->where("a.VenueChange<=3")
										  ->group("d.IDApplication");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
		
     
     
     
     
	 public function fnSearchStudent($larrformData)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										     ->join(array("f" =>"tbl_studentpaymentoption"),'f.IDApplication=a.IDApplication',array('f.*'))
										    ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										    ->joinleft(array("e" =>"tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array('e.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										   ->where("a.VenueChange<=3")
										  ->where("a.pass=3")
										  ->group("d.IDApplication");	
										  
										  if($larrformData['Studentname']) $lstrSelect->where("a.Fname like '%' ? '%'",$larrformData['Studentname']);	
				if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$larrformData['ICNO']);					  
				if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$larrformData['Coursename']);
				if($larrformData['Venues']) $lstrSelect->where("c.idcenter = ?",$larrformData['Venues']);
				if($larrformData['Takafulname']) $lstrSelect->where("e.idtakafuloperator = ?",$larrformData['Takafulname']);
				if($larrformData['paymentmode']) $lstrSelect->where("f.ModeofPayment = ?",$larrformData['paymentmode']);
                if(isset($larrformData['UpDate1']) && !empty($larrformData['UpDate1']) && isset($larrformData['UpDate2']) && !empty($larrformData['UpDate2']))
                {
				  $lstrFromDate = date("Y-m-d",strtotime($larrformData['UpDate1']));
				  $lstrToDate = date("Y-m-d",strtotime($larrformData['UpDate2']));
				  $lstrSelect = $lstrSelect->where("DATE_FORMAT(a.UpdDate,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate'");
		        }	
                if(isset($larrformData['Date']) && !empty($larrformData['Date']) && isset($larrformData['Date2']) && !empty($larrformData['Date2']))
                {
				  $lstrFromDate = date("Y-m-d",strtotime($larrformData['Date']));
				  $lstrToDate = date("Y-m-d",strtotime($larrformData['Date2']));
				  $lstrSelect = $lstrSelect->where("DATE_FORMAT(a.DateTime,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate'");
		        }					  
		        
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     
     
     
     
     
     
     
     
     public function  fngetstudenteachinformation($lintidstudent)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as AppliedDate"))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.IdProgrammaster','b.ProgramName'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("w" =>"tbl_studentpaymentoption"),'w.IDApplication=a.IDApplication',array('w.*'))
										  ->join(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->join(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }

		
     
     
		 public function fnGetVenueNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername")) 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	 public function fnGetTakafulNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName")) 				 
				 				 ->order("a.TakafulName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
     public function fnGetCourseNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
		
	   
	    public function fnGetSchedulerDetails()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Description")) 				 
				 				 ->order("a.Year");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
		public function fnGetStateName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
										   ->where("a.idCountry  = ?","121")
										   ->order("a.StateName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			public function fnGetCityName($idcity){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_city"),array("key"=>"a.idCity","value"=>"a.CityName"))
										   ->where("a.idCity  = ?",$idcity);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			
			
		public function fnGetVenueName($idcity){	
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
										   ->where("a.city = ?",$idcity);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
		public function fnGetSchedulerSessionDetails($idschdulersesion){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
										   ->where("a.idmangesession = ?",$idschdulersesion);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			public function fnGetSchedulerYearDetails($idsch)
			{
				
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.year"))
										   ->where("a.idnewscheduler = ?",$idsch);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
			}
			
	public function fnGetYearlistforcourse($idprog){		
			

$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Description"))
										  //->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										    ->join(array("d"=>"tbl_newmonths"),"a.From=d.idmonth",array())
										  ->join(array("e"=>"tbl_newmonths"),"a.To=e.idmonth",array())
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())

                                          ->where("a.Active   = 1")
										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;





	}
	
	
	
	public function fnGetStatelistforcourse($idprog,$idyear){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->group("e.idState")
										  ->where("a.idnewscheduler=?",$idyear);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetCitylistforcourse($idstate,$idprog,$idseched){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array("key"=>"e.idCity","value"=>"e.CityName"))
										  ->where("e.idState  = ?",$idstate)
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler  = ?",$idseched)
										  ->group("e.CityName");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetVenuelistforcourse($idcity,$idprog,$idseched){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array())
										  ->where("d.city = ?",$idcity)
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler  = ?",$idseched)
										  ->group("d.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnbetweenmonths($id)
{
$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
										  ->where("a.idnewscheduler=?",$id);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	
}


	public function fnGetmonthsbetween2($tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >=(SELECT month(curdate( ))) and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
	public function fnGetmonthsbetween($frommonth,$tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >='$frommonth' and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
	
	
   public function fngetschedulerexception($days,$lintcity)
   {
   	  $db 	= 	Zend_Db_Table::getDefaultAdapter();	
   		$lstrSelect = $db->select()
	                         ->from(array("a" =>"tbl_schedulerexception"))
	                         ->where('a.Date=?',$days)
	                         ->where('a.idcity=?',$lintcity);
	    $larrResult = $db->fetchRow($lstrSelect);
	    return $larrResult;
   	   
   }
   
	 public function fnGetVenuedetailsgetsecid($idsech)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
						                  ->where("a.Active=1")
										  ->where("a.idnewscheduler =?",$idsech); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
	public function fnGetVenuedetailsRemainingseats($year,$idsech,$city,$month,$Date)
  {
  	
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $select ="SELECT b.NumberofSeat, b.centername,b.idcenter, d.managesessionname, d.starttime, d.endtime,d.idmangesession, (
b.NumberofSeat - IFNULL( count( a.IDApplication ) , 0 )
) AS rem
FROM `tbl_studentapplication` a, tbl_center b, tbl_managesession d,tbl_newscheduler m
WHERE a.Examvenue = b.idcenter
AND a.Examdate =$Date
AND a.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND b.Active =1
and  b.city =$city
AND d.idmangesession = a.Examsession
GROUP BY a.Examvenue, a.Examsession
UNION
SELECT b.NumberofSeat, b.centername,b.idcenter, j.managesessionname, j.starttime, j.endtime, j.idmangesession, b.NumberofSeat AS rem
FROM tbl_center b,tbl_managesession e,`tbl_studentapplication` c,tbl_newschedulersession f, tbl_managesession j,tbl_newscheduler m
WHERE f.idmanagesession NOT
IN (
SELECT b.Examsession
FROM tbl_studentapplication b
WHERE b.Examvenue = c.Examvenue
AND b.ExamCity =$city
AND b.Examdate =$Date
AND b.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND b.Examvenue !=000
AND b.Examsession !=000
GROUP BY b.Examvenue, b.Examsession
)
AND c.ExamCity =$city
AND b.city =$city
AND c.Examdate =$Date
AND c.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND c.Examvenue !=000
AND c.Examsession!=000
AND f.idnewscheduler =$idsech
AND f.idmanagesession=j.idmangesession
AND c.Examvenue = b.idcenter
UNION
SELECT b.NumberofSeat, b.centername,b.idcenter, e.managesessionname, e.starttime, e.endtime,e.idmangesession, b.NumberofSeat AS rem
FROM tbl_center b, tbl_newschedulervenue c, tbl_managesession e
WHERE b.idcenter NOT
IN (
SELECT Examvenue
FROM tbl_studentapplication
WHERE ExamCity =$city
AND Examdate =$Date
AND Exammonth =$month
AND Year =$idsech
)
AND b.city =$city
AND b.Active =1
AND  c.idvenue = b.idcenter
AND e.idmangesession
IN (
SELECT h.idmanagesession
FROM tbl_newschedulersession h
WHERE idnewscheduler =$idsech
)" ;
 
//echo  $select;die();
		 return	$result = $lobjDbAdpt->fetchAll($select);	
  }
  
  
	  public function fnGetsesssiondetails($year,$city,$venue,$idsession,$day,$month)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler')
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession',array("key"=>"g.idmangesession","value"=>"g.managesessionname"))
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter')
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array())
										 // ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										   ->where("a.idnewscheduler =?",$year)
										     ->where("b.idvenue =?",$venue)
										     ->where("f.idmanagesession not in (select Examsession from  tbl_studentapplication where  Examsession in ($idsession) and ExamCity=$city and Examdate=$day and Examvenue=$venue and Year=$year)")
										   ->group("g.idmangesession");
									
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
	 public function fngetyearforthescheduler($id)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
										  ->where("a.idnewscheduler=?",$id);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
public function fnUpdateStudentnewscreen($idapplication,$larrformData,$scheduleryear,$iduser,$idcenter,$idsession,$venuechange)
   {
   	
   if($larrformData['newexammonth']<10)
   	{
   		$months = '0'.$larrformData['newexammonth'];
   		//$larrformData['setmonth']=
   	}
   	else 
   	{
   		$months = $larrformData['newexammonth'];
   	}
   	
    if($larrformData['newexamdates']<10)
   	{
   		$date = '0'.$larrformData['newexamdates'];
   		//$larrformData['setmonth']=
   	}
   	else {
   		 		$date = $larrformData['newexamdates'];
   	}
  
   	
   $db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(
		                    'UpdUser' =>$iduser,	
           					'UpdDate' => $larrformData['UpdDate'], 
		                    'VenueChange' =>$venuechange, 
							'DateTime' =>$scheduleryear.'-'.$months.'-'.$date,	
		  					'ExamState'=>$larrformData['newexamstate'],
		  					'ExamCity'=>$larrformData['newexamcity'],
		  					'Year'=>$larrformData['schedulerename1'],
		  					'Examdate'=>$larrformData['newexamdates'],
		  					'Exammonth'=>$larrformData['newexammonth'],
		                   	'Examvenue'=>$idcenter,	
		  					'Examsession'=>$idsession,	  
						);
		$where['IDApplication = ? ']= $idapplication;		
		return $db->update('tbl_studentapplication', $postData, $where);	
   }
			
	   
		
}
