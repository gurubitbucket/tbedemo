<?php
class GeneralSetup_Model_DbTable_Takafuloperator extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_takafuloperator';
	

 public function fnaddtakafuloperator($larrformdata)
 {
 	$larrformdata['workphone'] = $larrformdata['workcountrycode']."-".$larrformdata['workstatecode']."-".$larrformdata['workphone'];
 		unset($larrformdata['workcountrycode']);
		unset($larrformdata['workstatecode']);
 	$this->insert($larrformdata);
 }
	
 /*
  * function to fetch all the details of the takaful operator
  */
public function fngetTakafulOperator()
{
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
						 ->from(array("a"=>"tbl_takafuloperator"),array("a.*"));
       $result = $db->fetchAll($lstrSelect);
       return $result;						 
}

/*
 * function to fetcha ll the details based on the id
 */
public function fnGetTakafulDetails($intidtakafuloperator)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
					->where("a.idtakafuloperator=?",$intidtakafuloperator);
	$result = $db->fetchRow($lstrSelect);
	return $result;
}

/*
 * function to update the takaful operator
 */
public function fnupdatetakafuloperator($formData,$lintidtakafuloperator)
{
	$formData['workphone'] = $formData['workcountrycode']."-".$formData['workstatecode']."-".$formData['workphone'];
 		unset($formData['workcountrycode']);
		unset($formData['workstatecode']);
	$db =Zend_Db_Table::getDefaultAdapter();
	$table = "tbl_takafuloperator";
	$where['idtakafuloperator = ? '] = $lintidtakafuloperator;
	$db->update($table,$formData,$where);
				
}

/*
 * function to search the takaful operator
 */
public function fnSearchTakafulOperator($post)
{


	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
					->where('a.TakafulName like  ? "%"',$post['field3']);
	$result = $db->fetchAll($lstrSelect);
	return $result;
}
public function fnGetCountryList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_countries"),array("key"=>"a.idCountry","value"=>"CountryName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.CountryName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
		
	//Get State List
	public function fnGetStateList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"StateName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.StateName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
public function  fnGetcityList($idstate)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
         	$select = $db->select()
			   ->from(array('a' => 'tbl_city'),array("key"=>"a.idCity","value"=>"CityName"))
			   ->where('a.idState=?',$idstate);
		$result = $db->fetchAll($select);
		return $result;
	}
}