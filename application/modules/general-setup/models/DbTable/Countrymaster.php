<?php
class GeneralSetup_Model_DbTable_Countrymaster extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_countries';
	
	public function fngetCountryDetails() 
	 {
        $result = $this->fetchAll('Active = 1', "CountryName ASC");
        return $result;
     }
     
    public function fnSearchCountries($post = array()) 
    { 
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
       $lstrSelect = $lobjDbAdpt->select()
       								->from(array("c"=>"tbl_countries"),array("c.*"))
       								->where('c.CountryName like "%" ? "%"',$post['field2'])       								
       								->where("c.Active = ".$post["field7"])
       								->group("c.CountryName")
       			   					->order("c.CountryName");	        					       					
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
	 
	public function fnaddCountrymaster($larrformData) 
	{ 
    	$this->insert($larrformData);
	}
	
	 public function fnviewCountrymaster($lintidCountry) {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("c"=>"tbl_countries"),array("c.*"))			
		            	->where("c.idCountry= ?",$lintidCountry);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
   public function fnupdateCountrymaster($lintidCountry,$larrformData) { 
    	$where = 'idCountry = '.$lintidCountry;
		$this->update($larrformData,$where);
    } 
	
	
}