<?php 
class GeneralSetup_Model_DbTable_Programrate extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_programrate';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetCoursemasterDetails() { //Function to get the user details
        $select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_programmaster'),array('IdProgrammaster'))
			   ->where('a.Active = 1');
		
		$result = $this->fetchAll($select);
        return $result;
     }
    function fngetProgramrateMaster($wher){
    	$select = $this->select()
			   ->setIntegrityCheck(false) 
			   ->from( array('b' => 'tbl_programmaster')) 	
			   ->joinLeft(array("a" => "tbl_programrate"),"a.idProgram = b.IdProgrammaster ")
			   ->where('b.Active = 1')
			   -> where($wher); 
			  
		$result = $this->fetchAll($select);
        return $result;
    }



	public function fnAddGetGradesetList(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_gradesetmaster"),array("key"=>"a.Idgradeset","value"=>"a.Gradesetname"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.Gradesetname");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

 	function fngetProgramrateDetails($wher){
    	$select = $this->select()
			   ->setIntegrityCheck(false) 
			   ->from(array("a" => "tbl_programrate")) 	
			   ->joinLeft( array('b' => 'tbl_programmaster'),"a.idProgram = b.IdProgrammaster")
			   ->joinLeft( array('c' => 'tbl_accountmaster'),"c.idAccount = a.IdAccountmaster")			  
			   ->where('b.Active = 1')
			   -> where($wher)
			   ->where('c.Active = 1'); 			  
		$result = $this->fetchAll($select);
        return $result->toArray(); 
    }
	function fngetProgramrateEditDetails($wher){
    	$select = $this->select()
			   ->setIntegrityCheck(false) 
			   ->from(array("a" => "tbl_programrate")) 				   
			   -> where($wher); 			  
		$result = $this->fetchRow($select);
        return $result; 
    }
	public function fnAccountArray(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_accountmaster"),array("key"=>"idAccount","value"=>"AccountName"))
				 				 ->where("Active = 1")
				 				 ->where("a.programtype=1")
				 				 ->order("AccountName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
    
    public function fnaddProgramrate($formData) { //Function for adding the University details to the table
			$this->insert($formData);
	}
    
    public function fnupdateProgramrate($formData,$idProgramrate) { //Function for updating the university
    	unset($formData ['idProgramrate']);
		$where = 'idProgramrate = '.$idProgramrate;
		$this->update($formData,$where);
    }
    
	public function fnSearchCourse($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_programmaster'),array('IdProgrammaster'))
			   ->where('a.ProgramName  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}

}
?>