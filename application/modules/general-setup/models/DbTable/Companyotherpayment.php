<?php
class GeneralSetup_Model_DbTable_Companyotherpayment extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	public function fngetCompanyDetails(){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_studentpaymentoption"))
								  ->join(array("b" => "tbl_batchregistration"),'a.IDApplication=b.idBatchRegistration',array("b.*"))
								  ->join(array("c" =>"tbl_companies"),'b.idCompany=c.IdCompany',array("c.*"))
								  ->where("b.Approved=0")
								  //->where("a.IDApplication not in (select c.IDApplication from tbl_registereddetails as c)")
								  ->where("a.companyflag=1")
								  ->where("a.ModeofPayment=5");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
    public function fnSearchCompanyotherPayment($larr) 
         {
           $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	       $lstrSelect = $lobjDbAdpt ->select()
       								 ->from(array("a" => "tbl_batchregistration"))
		                             ->join(array("b" =>"tbl_companies"),'b.IdCompany=a.IdCompany',array("b.CompanyName"))
       								 ->join(array("c" => "tbl_studentpaymentoption"),'c.IDApplication=a.idBatchRegistration')
		                             ->where('b.CompanyName like "%" ? "%"',$larr)
       								 ->where('a.Approved=0')
       								 ->where('c.companyflag=1')
       								 ->where('c.ModeofPayment=5');       					
		  $larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
		  return $larrResult;
         }
	public function fngetCompanyname($lstrType)
	{	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_batchregistration"))
		                         ->join(array("b" =>"tbl_companies"),'b.idCompany=a.IdCompany',array("b.*"))
		                         ->where("a.idBatchRegistration = ?",$lstrType);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;                        
	}
	public function insertoption($regid,$idbatch,$studentId)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $table = "tbl_registereddetails";
         $postData = array('Regid' =>   $regid,	
           					'IdBatch' =>$idbatch,	
           					'IDApplication' => $studentId,
         					'RegistrationPin'=>'0000',
         					'Approved' => 1);					
	     $lobjDbAdpt->insert($table,$postData);
	     
	     
		// $lastid  = $db->lastInsertId("tbl_registereddetails","idregistereddetails");
	}
	
	public function fngeneraterandom()
	{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $length = 10;
    $characters = '0123456789';
    $string = "";    

      for ($p = 0; $p < $length; $p++) {
        $string.= $characters[mt_rand(0, strlen($characters))];
        }

         $lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_registereddetails"),array("Regid"));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
			for($i=0;$i<count($larrResult);$i++)
				 	{
				 		
				 		if($string == $larrResult[$i]['Regid'])
				 		{
				 	           self::fngeneraterandom();
				 	    } 
				 	    
				 	
				 	}
    return $string;
    
	}
	
	public function InsertPaymentOption($larrformData,$lstrType,$regid)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $table = "tbl_studentpaymentdetails";
          $postData = array('Amount' =>$larrformData['Amount'],	
           					'ChequeNo' =>$larrformData['ChequeNo'],	
           					'IDApplication' => $larrformData['IDApplication'],
                            'ChequeDt' =>"0000-00-00",
          					'companyflag'=>1,
                             'BankName' =>$larrformData['BankName']);					
	     $lobjDbAdpt->insert($table,$postData);	     
	     $post = array('Approved' => 1,
	     				'paymentStatus'=>1,
	     				'registrationPin'=>$regid);
	      $where['idBatchRegistration = ? ']=$lstrType;
	     return $lobjDbAdpt->update('tbl_batchregistration',$post,$where);	     
	}
	
public function sendmails($stname,$email,$regid)
	{
			/*echo "asfsafsad";
		die();*/
		 $this->lobjstudentmodel = new App_Model_Studentapplication();
	$larrSMTPDetails  = $this->lobjstudentmodel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Company Payment");
					
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $email;
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
						
						
							$larrEmailIds[0] = $email;
							//$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							//$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							/*echo "asfsafsdfsd";
							die();*/
							try{
								$lobjProtocol->connect();
						   		$lobjProtocol->helo($lstrSMTPUsername);
								$lobjTransport->setConnection($lobjProtocol);
						 	
								//Intialize Zend Mailing Object
								$lobjMail = new Zend_Mail();
						
								$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
								$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
								$lobjMail->addHeader('MIME-Version', '1.0');
								$lobjMail->setSubject($lstrEmailTemplateSubject);
						
								for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
									if($larrEmailIds[$lintI] != ""){
										$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);	
																
										//replace tags with values
										//$Link = "<a href='".$this->Url."/registration'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Person]",$stname,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[RegPin]",$regid,$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										$lobjMail->setBodyHtml($lstrEmailTemplateBody);
								
										try {
											$lobjMail->send($lobjTransport);
										} catch (Exception $e) {
											$lstrMsg = "error";      				
										}	
										$lobjMail->clearRecipients();
										$this->view->mess .= ". Login Details have been sent to user Email";
										unset($larrEmailIds[$lintI]);
									}
								}
							}catch(Exception $e){
								$lstrMsg = "error";
							}
						}else{
							$lstrMsg = "No Template Found";
						}
	}
/*public function fnSearchcompany($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_registereddetails'),array('IdProgrammaster'))
			   ->where('a.ProgramName  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}*/
}
