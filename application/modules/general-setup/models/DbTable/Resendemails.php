<?php
class GeneralSetup_Model_DbTable_Resendemails extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
 
   /*
      * functin to search the student
      */
	public function fnSearchStudent($larrformData) { //Function for searching the user details
    	//$db = Zend_Db_Table::getDefaultAdapter();
    	
    	$totallength = strlen($larrformData['ICNO']);
    	$arricno = explode('*',$larrformData['ICNO']);
    	$icno = $arricno[0];

    if($larrformData['Emailtypes']==1)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										 ->from(array("a" =>"tbl_studentapplication"),array('a.*','date(a.UpdDate) as Applieddate'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
   ->join(array("e"=>"tbl_managesession"),'a.Examsession=e.idmangesession',array("e.*"))
										  ->where("a.Payment=1")
										  ->where("a.datetime>=curdate()")						
										  ->where("a.examsession!=000")
										  ->where("a.pass=3")
										  ->where("a.IDApplication>1148")	
										  ->group("d.IDApplication");
										  
										  if(!empty($larrformData['FName']))
										  {
			   							$lstrSelect ->where('a.FName like  "%" ? "%"',$larrformData['FName']);
										  }
										  if(!empty($icno))
										  {
										  $lstrSelect->where('a.ICNO like  "%" ? "%"',$icno);
										  }

  
										  if(!empty($larrformData['VenueTypes']))
										  {
			   							$lstrSelect ->where("a.Examvenue=?",$larrformData['VenueTypes']);
										  }
										   if(!empty($larrformData['SessionTypes']))
										  {
										$lstrSelect ->where("a.Examsession=?",$larrformData['SessionTypes']);
										  }
			   							
			if($larrformData['Regtype']==1) $lstrSelect->where("a.batchpayment =0");
			if($larrformData['Regtype']==2) $lstrSelect->where("a.batchpayment =1");	
			if($larrformData['Regtype']==3) $lstrSelect->where("a.batchpayment =2");
			if($larrformData['Regdate']) $lstrSelect->where("date(a.UpdDate) =?",$larrformData['Regdate']);	
			if($larrformData['Examdate']) $lstrSelect->where("date(a.DateTime) =?",$larrformData['Examdate']);	

			$lstrSelect->order("c.centername");
			$lstrSelect->order("a.FName");
		/*	echo $lstrSelect;
			die();*/
			
			//echo $lstrSelect;die();
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
    }
    else if($larrformData['Emailtypes']==2)
    {
    	    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										 ->from(array("a" =>"tbl_studentapplication"),array('a.*','date(a.UpdDate) as Applieddate'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
  ->join(array("e"=>"tbl_managesession"),'a.Examsession=e.idmangesession',array('e.*'))
										  ->where("a.Payment=1")
										    ->where("a.IDApplication>1148")
 ->where("a.pass=3")	
  ->where("a.datetime>=curdate()")
										  ->group("d.IDApplication");
										 // ->where("a.Venue>0")	;
			   							  
										  if(!empty($larrformData['FName']))
										  {
			   							$lstrSelect ->where('a.FName like  "%" ? "%"',$larrformData['FName']);
										  }
										  if(!empty($icno))
										  {
										  $lstrSelect->where('a.ICNO like  "%" ? "%"',$icno);
										  }

  
										  if(!empty($larrformData['VenueTypes']))
										  {
			   							$lstrSelect ->where("a.Examvenue=?",$larrformData['VenueTypes']);
										  }
										   if(!empty($larrformData['SessionTypes']))
										  {
										$lstrSelect ->where("a.Examsession=?",$larrformData['SessionTypes']);
										  }
			if($larrformData['Regtype']==1) $lstrSelect->where("a.batchpayment =0");
			if($larrformData['Regtype']==2) $lstrSelect->where("a.batchpayment =1");	
			if($larrformData['Regtype']==3) $lstrSelect->where("a.batchpayment =2");	
			if($larrformData['Regdate']) $lstrSelect->where("date(a.UpdDate) =?",$larrformData['Regdate']);	
			if($larrformData['Examdate']) $lstrSelect->where("date(a.DateTime) =?",$larrformData['Examdate']);
			$lstrSelect->order("c.centername");
			$lstrSelect->order("a.FName");				
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
    }
    else if($larrformData['Emailtypes']==3)
    {
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										 ->from(array("a" =>"tbl_studentapplication"),array('a.*','date(a.UpdDate) as Applieddate'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
  ->join(array("e"=>"tbl_managesession"),'a.Examsession=e.idmangesession',array('e.*'))
										  ->where("a.Payment=1")
										    ->where("a.IDApplication>1148")	
										  ->group("d.IDApplication")
										   ->where("a.pass in (1,2)");	
			   						 if(!empty($larrformData['FName']))
										  {
			   							$lstrSelect ->where('a.FName like  "%" ? "%"',$larrformData['FName']);
										  }
										  if(!empty($icno))
										  {
										  $lstrSelect->where('a.ICNO like  "%" ? "%"',$icno);
										  }

  
										  if(!empty($larrformData['VenueTypes']))
										  {
			   							$lstrSelect ->where("a.Examvenue=?",$larrformData['VenueTypes']);
										  }
										   if(!empty($larrformData['SessionTypes']))
										  {
										$lstrSelect ->where("a.Examsession=?",$larrformData['SessionTypes']);
										  }
										  if(!empty($larrformData['resendmailtype']))
										  {
										  	if($larrformData['resendmailtype']==2) 
										  	$lstrSelect->join(array("f" =>"tbl_resultresent"),'f.IDApplication=a.IDApplication',array(""))
										  	->where("f.Resend=0");
										  	
										  
										  }
			   							
	    			if($larrformData['Regtype']==1) $lstrSelect->where("a.batchpayment =0");
			if($larrformData['Regtype']==2) $lstrSelect->where("a.batchpayment =1");	
			if($larrformData['Regtype']==3) $lstrSelect->where("a.batchpayment =2");	
			if($larrformData['Regdate']) $lstrSelect->where("date(a.UpdDate) =?",$larrformData['Regdate']);	
			if($larrformData['Examdate']) $lstrSelect->where("date(a.DateTime) =?",$larrformData['Examdate']);	
			$lstrSelect->order("c.centername");
			$lstrSelect->order("a.FName");		
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
    }
		
	}
	

public function fnGetAllVenues()
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }



public function fnGetAllSessions()
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
										  ->where("a.active = 1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }


	
	public function fnsendbulkmails($larrformData)
	{
		$db=Zend_Db_Table::getDefaultAdapter();
		for($i=0;$i<count($larrformData['IDApplication']);$i++)
		{
		
			switch ($larrformData['Emailtypes'])
			{
			  Case 1:self::registrationmailstudent($larrformData['IDApplication'][$i],1);
			      
			         break;
			         
              Case 2:self::registrationmailstudent($larrformData['IDApplication'][$i],2);
			         break;	

              Case 3:self::resultmails($larrformData['IDApplication'][$i]);
			         break;				         
			}
			
		}
		
		if($larrformData['resendmailtype']==2){
				
				$apps=implode(',',$larrformData["IDApplication"]);
				$where = "IDApplication in ($apps)";
				     $postData = array(		
								'Resend' => 1														
							);
				$db->update("tbl_resultresent",$postData,$where);
			}
	}
 public function resultmails($lintidstudent)
     {

     		$lobjExamdetailsmodel = new App_Model_Examdetails();	
		require_once('Zend/Mail.php');
		require_once('Zend/Mail/Transport/Smtp.php');
			
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();
	    
		 $larrresult= $db->select()          
             	        ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
						->join(array("c" =>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
						->join(array("d" =>"tbl_studentmarks"),'a.IDApplication=d.IDApplication',array("d.*"))
            	        ->where('a.IDApplication = ?',$lintidstudent);
	          $result = $db->fetchRow($larrresult);  
       /*  print_r($result);
         die();*/
	          
						    $larrStudentMailingDetails = $result;
						    if($result['pass']==1)
						    {
						    	$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Pass");
						    }
						    else if($result['pass']==2)
						    {
						    	$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Fail");
						    	
						    }
							
						    $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
								$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
					
										$lstrEmailTemplateBody = str_replace("[NAME]",$result['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[NRIC]",$result['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Course]",$result['ProgramName'],$lstrEmailTemplateBody);
	                                    $lstrEmailTemplateBody = str_replace("[EXAMDATE]",date('d-m-Y',strtotime($result['DateTime'])),$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[GRADE]",$result['Grade'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
											$lstrEmailTemplateBody = str_replace("[NAME]",$result['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[NRIC]",$result['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Course]",$result['ProgramName'],$lstrEmailTemplateBody);
	                                    $lstrEmailTemplateBody = str_replace("[EXAMDATE]",date('d-m-Y',strtotime($result['DateTime'])),$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[GRADE]",$result['Grade'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
								
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'tbe@ibfim.com', 'password' => 'ibfim2oi2');
										try
										{
											$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										}
										catch(Exception $e) 
										{
											echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
											
										}
										$mail = new Zend_Mail();
									   try
									   {
											$mail->setBodyHtml($lstrEmailTemplateBody);
									   }
									catch(Exception $e)
									   {
												echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
									   }
										
										$sender_email = 'tbe@ibfim.com';
										$sender = 'tbe';
										$receiver_email =$result["EmailAddress"];
										$receiver = $result['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
											 //->addBcc('tony.uce@gmail.com','itwine')
									         ->setSubject($lstrEmailTemplateSubject);
									try 
									{
									  $result = $mail->send($transport);
     								} 
     								catch (Exception $e) 
     								{
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
  								   }

       }
	
     public function registrationmailstudent($lintidstudent,$type)
     {

     		$lobjExamdetailsmodel = new App_Model_Examdetails();	
		require_once('Zend/Mail.php');
		require_once('Zend/Mail/Transport/Smtp.php');
			
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();
	    
		 $larrresult= $db->select()          
             	        ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
						->join(array("c" =>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
						->join(array("d" =>"tbl_center"),'a.Examvenue=d.idcenter',array("d.*"))
						->join(array("e" =>"tbl_managesession"),'a.Examsession=e.idmangesession',array("e.*"))				
						->join(array("f" =>"tbl_registereddetails"),'f.IDApplication=a.IDApplication',array("f.*"))
            	        ->where('a.IDApplication = ?',$lintidstudent);
	          $result = $db->fetchRow($larrresult);  

	          
						    $larrStudentMailingDetails = $result;
						    if($type==1)
						    {
						    	$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Application");
						    }
						    else if($type==2)
						    {
						    	$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Venue Change");
						    	
						    }
							
						    $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
								$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
					
										$lstrEmailTemplateBody = str_replace("[Candidate]",$result['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$result['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$result['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$result['centername'].' '.$result['addr1'].''.$result['addr2'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Date]",$result['DateTime'],$lstrEmailTemplateBody);
	                                $lstrEmailTemplateBody = str_replace("[Date]",date('d-m-Y',strtotime($result['DateTime'])),$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$result['PermAddressDetails'].'-'.$result['CorrAddress'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$result['managesessionname'].'('.$result['ampmstart'].'--'.$result['ampmend'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$result['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$result["username"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$result['password'],$lstrEmailTemplateBody);
										if($result['batchpayment']!=0)
										{
											$lstrEmailTemplateBody = str_replace("[Amount]",'NA',$lstrEmailTemplateBody);
										}
										else 
										{
										$lstrEmailTemplateBody = str_replace("[Amount]",$result['Amount'],$lstrEmailTemplateBody);
										}
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
								
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										try
										{
											$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										}
										catch(Exception $e) 
										{
											echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
											
										}
										$mail = new Zend_Mail();
									   try
									   {
											$mail->setBodyHtml($lstrEmailTemplateBody);
									   }
									catch(Exception $e)
									   {
												echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
									   }
										
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'itwine';
										$receiver_email =$result["EmailAddress"];
										$receiver = $result['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
											 //->addBcc('tony.uce@gmail.com','itwine')
									         ->setSubject($lstrEmailTemplateSubject);
									try 
									{
									  $result = $mail->send($transport);
     								} 
     								catch (Exception $e) 
     								{
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
  								   }

       }
	
 
}