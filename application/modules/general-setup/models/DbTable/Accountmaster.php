<?php
class GeneralSetup_Model_DbTable_Accountmaster extends Zend_Db_Table{
	protected $_name = 'tbl_accountmaster';			
	
	/*
	 * fuction for getting the drop down of the account name for the group
	 */
	public function fnGetIdGroupSelect(){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_accountmaster"),array("key"=>"a.idAccount","value"=>"a.AccountName"))
								  ->where("a.Active = ?","1")
								  ->where("a.IdGroup = ?","NULL");		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	/*
	 * functin for insertnig into the acccountmaster table
	 */
	public function fnInsert($insertData) {		
       	$db = Zend_Db_Table::getDefaultAdapter();
       	$table = "tbl_accountmaster";
	   	$db->insert($table,$insertData);
	   	return $db->lastInsertId("tbl_accountmaster","idAccount");	   
	}
	
	/*
	 * fucntion for getting the financial year to grid view
	 */
	Public function fngetaccountdeatils() { 
		$select = $this->select() 
				 ->from('tbl_accountmaster');
		$result = $this->fetchAll($select);			
		return $result;
	 }	
	 
	 /*
	  * function for getting financial year for the edit
	  */
	 Public function fngetaccountEdit($lvaredit) {	 	
	   	$select = $this->select()
			     ->setIntegrityCheck(false)  
				 ->from('tbl_accountmaster')
				 ->where('idAccount = ?',$lvaredit);  				 
		$result = $this->fetchRow($select);		
		return $result;
	}	
	
	/*
	 * function for updating the account master table
	 */
	Public function fnupdateaccountmaster($lvarEdit,$larrformData) {  
	    if(!$larrformData['BillingModule']) $larrformData['BillingModule'] = 0;	
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();		
	 	$data = array('AccountName' => $larrformData['AccountName'],
	 	 			  'AccShortName' => $larrformData['AccShortName'],
					  'Description' => $larrformData['Description'],
	 				  'duringRegistration'=>$larrformData['duringRegistration'],	
				 	  'UpdDate' => $larrformData['UpdDate'],
				 	  'UpdUser' => $larrformData['UpdUser'],
				 	  'Active' => $larrformData['Active'],
				 	  'programtype' => $larrformData['programtype'],
				 	  'coursetype' => $larrformData['coursetype'],
				 	  );
		$where['idAccount = ? ']= $lvarEdit;		
		return $db->update('tbl_accountmaster', $data, $where);
	}   
	
	/*
	 * function for the account details search
	 */
	public function fngetaccountdeatilsSearch($lobjgetarr){	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		if(!$lobjgetarr['field7']) $sqlquery ="  Active = 0 ";
		else $sqlquery	= " Active = 1 ";	
		
		$select = $this->select()
					-> setIntegrityCheck(false)
					-> join(array('a' => 'tbl_accountmaster'),array('idAccount'))
					-> where('a.AccountName like  ? "%"',$lobjgetarr['field3'])
					-> where('a.AccShortName like  ? "%"',$lobjgetarr['field2'])
					-> where($sqlquery);
		$result = $this->fetchAll($select);
		return $result->toArray();
		
	}
	
	/*
	 * function for getting drop down value for billing modules
	 */
	function fnGetBillingModuleSelect(){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$select = $this->select()
			     ->setIntegrityCheck(false)  
				 ->from('tbl_definationtypems')
				 ->where('defTypeDesc = ?','BillingModule');				 		 
		$result = $this->fetchRow($select);
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->where("a.idDefType  = ?",$result['idDefType'])
								  ->where("a.Status  = ?","1");	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	/*
	 * 
	 */
	function fnGetPeriodSelect(){
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$select = $this->select()
			     ->setIntegrityCheck(false)  
				 ->from('tbl_definationtypems')
				 ->where('defTypeDesc = ?','Charging Type');				 		 
		$result = $this->fetchRow($select);
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->where("a.idDefType  = ?",$result['idDefType'])
								  ->where("a.Status  = ?","1");	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}	
	
	/*
	 * function for generating the code for the account master
	 */
	function fnGenerateCode($collageId,$uniqId){	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $this->select()
			    ->  setIntegrityCheck(false)  
				->  from('tbl_sfs_config')
				->  where('idCollege = ?',$collageId);  				 
		$result = 	$this->fetchRow($select);		
		$sepr	=	$result['Separator'];
		$str	=	"ReceiptField";
		for($i=1;$i<=3;$i++){
			$check = $result[$str.$i];
			switch ($check){
				case 'year':
				  $code	= date('My');
				  break;
				case 'uniqId':
				  $code	= $uniqId;
				  break;
				case 'college':
					 $select =  $this->select()
				     		 -> setIntegrityCheck(false)  
					 		 -> from('tbl_collegemaster')
					 		 ->	where('idCollege  = ?',$collageId);  				 
					$resultCollage = $this->fetchRow($select);		
					$code		   = $resultCollage['ShortName'];
				  break;
				default:
				  break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}		
	 	$data = array('PrefixCode' => $accCode);
		$where['idAccount = ? ']= $uniqId;		
		return $db->update('tbl_accountmaster', $data, $where);			
	}
	
}
