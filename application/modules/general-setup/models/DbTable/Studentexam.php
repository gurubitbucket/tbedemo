<?php
class GeneralSetup_Model_DbTable_Studentexam extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	

 public function fnviewstudentdexamdetails()
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->join(array("e"=>"tbl_tempexamdetails"),'d.Regid=e.Regid',array('e.*'))
										  ->where("a.Payment=1")
										  //->where("d.RegistrationPin=0000000")
										  ->group("d.IDApplication")
										  ->where("a.pass=3");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
 public function fneditdetails($lintidstudent)
 {
 	
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->join(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->join(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
     
   /*
      * functin to search the student
      */
	public function fnSearchStudent($larrformData) { //Function for searching the user details
    	
               if($larrformData['field3']) $name = $larrformData['field3'];
			   if($larrformData['field6']) $icno = $larrformData['field6'];
			   if($larrformData['field8']) $Coursename = $larrformData['field8'];
			   if($larrformData['field5']) $venue = $larrformData['field5'];
			   
		        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->where("d.RegistrationPin=0000000")
										  ->group("d.IDApplication")
										  ->where("a.pass=3");
										  //->where('a.FName like  "%" ? "%"',$name);	
			   if($larrformData['field3']) $lstrSelect->where('a.FName like  "%" ? "%"',$name);
			   if($larrformData['field6']) $lstrSelect->where("a.ICNO like '%' ? '%'",$icno);
			   if($larrformData['field8']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename);
			   if($larrformData['field5']) $lstrSelect->where("c.idcenter = ?",$venue);					  
			   							  
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
	
   public function fnGetFromTempDetail($RegId)
   {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$select = "SELECT * 
   	 			FROM tbl_tempexamdetails 
   	 			WHERE Regid='$RegId'";
   	 $resulttempdetails = $lobjDbAdpt->fetchAll($select);
   	  for($linti=0;$linti<count($resulttempdetails);$linti++)
		 {
		 	 $larrcreditnotedetails['Regid'] = $resulttempdetails[$linti]['Regid'];
		 	 $larrcreditnotedetails['QuestionNo'] = $resulttempdetails[$linti]['QuestionNo'];
		 	 $larrcreditnotedetails['Answer'] = $resulttempdetails[$linti]['Answer'];
		 	  $result = $lobjDbAdpt->insert('tbl_answerdetails',$larrcreditnotedetails);
		 	 
		 }
   	 return $resulttempdetails;
   }
   
public function fnDeleteTempDetails($RegId)
 	{
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$where = "Regid = '".$RegId."'";
 		$db->delete('tbl_tempexamdetails',$where);
 		
 	}
 	
  public function fnGetStudentdetails($idstudent)
  {
  	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_studentapplication'),array("a.*"))
		            ->join(array('b'=>'tbl_managesession'),'b.idmangesession = a.Examsession',array("b.*"))
		            ->join(array("c" =>"tbl_newscheduler"),'a.Year=c.idnewscheduler',array('c.Year as years'))
		            ->join(array("d" =>"tbl_programmaster"),'a.Program=d.idprogrammaster',array('d.*'))
		            ->where("a.IDApplication = ?",$idstudent);
		    $result = $db->fetchRow($select);
		    return $result;
  }
  
	public function fninsertstudentmarks($studentid,$noofquestions,$totalanswered,$attended,$grade)
	{
		$db =  Zend_Db_Table::getDefaultAdapter();  
		 $table = "tbl_studentmarks";
           $postData = array(		
							'IDApplication' => $studentid,		
            				'NoofQtns' =>$noofquestions,		
		            		'Attended' =>$totalanswered,	
		            		'Correct'	=>$attended,
		            		'Grade'=>$grade															
						);			
	        $db->insert($table,$postData);
	}
 
}