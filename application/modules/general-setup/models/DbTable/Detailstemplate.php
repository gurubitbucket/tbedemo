<?php
class GeneralSetup_Model_DbTable_Detailstemplate extends Zend_Db_Table {
	
	protected $_name = 'tbl_details'; // table name
	
	/*
	 * get all active email templates rows 
	 */
   	public function fnGetTemplateDetails() {   			    
		    $select = $this->select()
				->setIntegrityCheck(false)  	
				->from(array('a' => 'tbl_details'),array('a.*'))	
				->join(array('b'=>'tbl_webpagetemplate'),'a.iddefinition=b.idwebpagetemplate',array("b.content as TemplateName"));	
				//->where("Deleteflag = 1");
			$result = $this->fetchAll($select);		
			return $result->toArray(); 		    
   	}

   	/*
   	 * search by criteria   
   	 */
	public function fnSearchTemplate($post = array()) {		
		    $db = Zend_Db_Table::getDefaultAdapter();
			$select = $this->select()
							->setIntegrityCheck(false)  	
							->from(array('a' => 'tbl_details'),array(''))	
							->join(array('b'=>'tbl_webpagetemplate'),'a.iddefinition=b.idwebpagetemplate',array("b.content as TemplateName"))													
							->where('b.content like "%" ? "%"',$post['field3']);									
			$result = $this->fetchAll($select);
			return $result->toArray();			
	}
	
	/*
	 * Add Email Template
	 */
	public function fnAddEmailTemplate($post,$editorData) {
		//unset($post['idDefination']);
/*		print_r($post);
		die();*/
		 $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_details";
            $post = array(		
							'content' => $editorData,		
            				'iddefinition'=>$post['iddefinition'],
            				'language'=>$post['languagess'],
            				'UpdUser'=>$post['UpdUser'],
            				'UpdDate'=>$post['UpdDate']											
						);			
	        $db->insert($table,$post);
		//$post['content'] = $editorData;			
		//$this->insert($post);
	}
	
	/*
	 * get single email template row values bu $id
	 */
    public function fnViewTemplte($idTemplate) {
    			
    	 $select = $this->select()
				->setIntegrityCheck(false)  	
				->from(array('a' => 'tbl_details'),array('a.content','a.iddefinition'))	
				->join(array('b'=>'tbl_webpagetemplate'),'a.iddefinition=b.idwebpagetemplate',array("b.content as DefinitionDesc"))
				->where( "iddetails = '$idTemplate'");
		$result = $this->fetchRow($select);	
		//$result = $this->fetchRow( "iddetails = '$idTemplate'") ;
        return @$result->toArray();     // @symbol is used to avoid warning    	
    }
    
    /*
     * update email template 
     */
    public function fnUpdateTemplate($idTemplate,$formData,$editorData) {
    	//	unset($formData['idDefination']);
    			    		
    	$lang = $formData['languagess'];
    	if($lang==1)
    	{
    		$postData = array(		
							'iddefinition' => $formData['iddefinition'],		
            				'banhasa' =>$editorData,		
		            		'UpdUser' =>$formData['UpdUser'],
                            'UpdDate' => $formData['UpdDate'],											
						);
    	}
    	elseif($lang==0)
    	{
    		$postData = array(		
							'iddefinition' => $formData['iddefinition'],		
            				'content' =>$editorData,		
		            		'UpdUser' =>$formData['UpdUser'],
                            'UpdDate' => $formData['UpdDate'],											
						);
    	}
       	
   	 	$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$where = "iddetails = '".$idTemplate."'"; 	
		$db->update('tbl_details', $postData, $where);
    	
    }
    
public function fnGetDefinationMss($defms) { 
	$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$select = $db->select()
							//->setIntegrityCheck(false) 
							->from(array('dtms' => 'tbl_definationtypems'),array())
                       		->join(array('dms' => 'tbl_definationms'),'dms.idDefType = dtms.idDefType',array('key' => 'dms.idDefinition','value' => 'dms.DefinitionDesc'))
                       		->where('dtms.defTypeDesc = ?', $defms)
                       		->Where('dms.idDefinition not in (select iddefinition from tbl_details)');               
		$result = $db->fetchAll($select);
		return $result;
	}
	
	
    public function fngetlanguagedetails($idTemplate) {
    			
    	 $db 	= 	Zend_Db_Table::getDefaultAdapter();	
    	 $select = $db->select()
					->from(array('a' => 'tbl_details'),array('a.*'))	
					->where( "a.iddetails = '$idTemplate'");
		$result = $db->fetchRow($select);	
		//$result = $this->fetchRow( "iddetails = '$idTemplate'") ;
        return $result;     // @symbol is used to avoid warning    	
    }
    /*
     * function to get the web page template
     */
    public function fnGetWebPageTemplate()
    {
    	$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$select = $db->select()
                       		->from(array('a' => 'tbl_webpagetemplate'),array('key' => 'a.idwebpagetemplate','value' => 'a.content'));               
		$result = $db->fetchAll($select);
		return $result;
    }
	
    	
}
