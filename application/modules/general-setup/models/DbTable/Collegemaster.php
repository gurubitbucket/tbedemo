<?php 
class GeneralSetup_Model_DbTable_Collegemaster extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_collegemaster';
    
    public function fnaddCollege($larrformData) { //Function for adding the University details to the table
    	   unset ( $larrformData ['Arabic_Name'] );
    	   unset ( $larrformData ['FirstName'] );
    	   unset ( $larrformData ['SecondName'] );
    	   unset ( $larrformData ['ThirdName'] );
    	   unset ( $larrformData ['FourthName'] );
    	   unset ( $larrformData ['FullName'] );
    	   unset ( $larrformData ['Address1'] );
    	   unset ( $larrformData ['Address2'] );
    	   unset ( $larrformData ['Citystaff'] );
    	   unset ( $larrformData ['Countrystaff'] );
    	   unset ( $larrformData ['Statestaff'] );
    	   unset ( $larrformData ['Zipcode'] );
    	   unset ( $larrformData ['Phone'] );
    	   unset ( $larrformData ['Mobile'] );
    	   unset ( $larrformData ['Emailstaff'] );
    	   unset ( $larrformData ['StaffType'] );
    	   unset ( $larrformData ['IdLevel'] );
    	   unset ( $larrformData ['FromDate'] );
    	   unset ( $larrformData ['ToDate'] );
    	   
    	   if($larrformData ['CollegeType'] == '0') {
	    	   	$larrformData ['Idhead']  = '0';
    	   } else if($larrformData ['CollegeType'] == '1') {
    	   		$larrformData ['Idhead']  = $larrformData ['Idhead'];
    	   }
    	   
			$this->insert($larrformData);
			$lobjdb = Zend_Db_Table::getDefaultAdapter();
			return $lobjdb->lastInsertId();
	}
	
	
     public function fngetCollegemasterDetails() { //Function to get the user details
        $result = $this->fetchAll('Active = 1');
        return $result;
     }

   public function fneditCollege($IdCollege) { //Function for the view University 
	$select = $this->select()
			->setIntegrityCheck(false)  
			->join(array('col' => 'tbl_collegemaster'),array('col.IdCollege'))
			->join(array('dean'=>'tbl_deanlist'),'col.IdCollege = dean.IdCollege',array('dean.IdStaff','dean.FromDate','dean.ToDate'))
            ->where('col.IdCollege = ?',$IdCollege)
            ->where('dean.FromDate < now() AND dean.ToDate > now()');
			
	$result = $this->fetchAll($select);
	return $result->toArray();
    }
    
    public function fnupdateCollege($formData,$lintIdCollege) { //Function for updating the university
    	unset ( $formData ['Save'] );
    	unset ( $formData ['IdStaff'] );
    	unset ( $formData ['FromDate'] );
    	unset ( $formData ['ToDate'] );
    	
		$where = 'IdCollege = '.$lintIdCollege;
		$this->update($formData,$where);
    }
    
	public function fnSearchCollege($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_collegemaster'),array('IdCollege'))
			   ->where('a.CollegeName  like "%" ? "%"',$post['field3'])
			   ->where('a.ShortName like  "%" ? "%"',$post['field2'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fnGetListofCollege(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		 				 ->where("a.CollegeType = 0")
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetCollegeList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		 				 ->where("a.CollegeType = 1")
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

}
?>