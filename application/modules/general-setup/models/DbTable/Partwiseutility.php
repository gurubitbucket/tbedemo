<?php 
class GeneralSetup_Model_DbTable_Partwiseutility extends Zend_Db_Table_Abstract
{
	

    
	public function fnSearchStudent($larrformdata) { //Function for searching the university details
		
		if($larrformdata['Date3']) $fromdate = $larrformdata['Date3'];
		if($larrformdata['Date4']) $todate = $larrformdata['Date4'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
									->from(array("a" => "tbl_studentapplication"),array("a.*"))
									->join(array("b" => "tbl_registereddetails"),'a.IDApplication=b.IDApplication',array("b.*"))
								    ->join(array("c" => "tbl_answerdetails"),'b.Regid=c.Regid',array("c.*"))
								    ->join(array("d" => "tbl_programmaster"),'a.Program=d.IdProgrammaster',array("d.*"))
									->where("a.payment=1")
									->where("a.pass  != 3")
									->group("a.IDApplication");
	   if($larrformdata['Date3'])$lstrSelect=$lstrSelect	->where("a.DateTime >='$fromdate'");
	   if($larrformdata['Date3'])		$lstrSelect=$lstrSelect->where("a.DateTime <='$todate'");
									
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	/*
	 * inserting for partwise marks
	 */
 public function fninsertstudentpartwise($studentid,$RegID)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        $table = "tbl_studentdetailspartwisemarks";
           $postData = array(		
							'IDApplication' => $studentid,		
            				'Regid' =>$RegID,		
		            		'A' =>0,	
		            		'B'=>0,
           					'C'=>0,
                            'UpdDate'=>date("Y-m-d H:i:s")
						);			
	        $db->insert($table,$postData);
    }
  /*
   * function for gettingthe parts id
   */  
    
  public function fngettheidparts($idbatchresult)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_batchdetail"),array("GROUP_CONCAT(IdPart SEPARATOR ',')as partsids")) 	
				 				 ->where("a.IdBatch=?",$idbatchresult);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;	
   }
   
public function partwiseattended($Regid,$part)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    		 $sql = "Select count(idquestions) from tbl_questions where idquestions  in (
					SELECT  QuestionNo FROM tbl_answerdetails  where Regid = '$Regid'  and answer in(SELECT idanswers FROM tbl_answers where CorrectAnswer =1))
					and QuestionGroup = '$part'";
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
    public function updatepartwisemarksfora($RegID,$partbmarks,$part)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1[$part] = $partbmarks;	
		 $where = "Regid = '".$RegID."'"; 	
		 $db->update('tbl_studentdetailspartwisemarks',$larrformData1,$where);
    }
    
/*    public function updatepartwisemarksforb($RegID,$partbmarks)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['B'] = $partbmarks;	
		 $where = "Regid = '".$RegID."'"; 	
		 $db->update('tbl_studentdetailspartwisemarks',$larrformData1,$where);
    }
    
    public function updatepartwisemarksforc($RegID,$partbmarks)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['C'] = $partbmarks;	
		 $where = "Regid = '".$RegID."'"; 	
		 $db->update('tbl_studentdetailspartwisemarks',$larrformData1,$where);
    }*/
    
	
	public function fnsubmittheresults($larrformdata)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		for($i=0;$i<count($larrformdata['IDApplication']);$i++)
		{
		// echo "<Pre/>";
			//print_R($larrformdata['IDApplication'][$i]);
			
			 $lstrSelect = $lobjDbAdpt->select()
									->from(array("a" => "tbl_studentapplication"),array("a.*"))
									->join(array("b" => "tbl_registereddetails"),'a.IDApplication=b.IDApplication',array("b.*","b.IdBatch as ExamBatch"))
									->where("b.IDApplication =?",$larrformdata['IDApplication'][$i]);
				$larrResultregid = $lobjDbAdpt->fetchRow($lstrSelect);
				/*print_r($larrResultregid);
				die();*/
				$Regid= $larrResultregid['Regid'];
				$idbatch = $larrResultregid['ExamBatch'];
				
				self::fninsertstudentpartwise($larrformdata['IDApplication'][$i],$Regid);
				
				     $idbatchresult=$idbatch;
					$larresultidparts= self::fngettheidparts($idbatchresult);
				
					
					$idpartsresultsarray=$larresultidparts['partsids'];
					$resarr= explode(',',$idpartsresultsarray);

					for($j=0;$j<3;$j++)
					{
						switch($resarr[$j])
						{
							Case 'A':$part= 'A';
								  	
								  	 $subdetails = self::partwiseattended($Regid,$part);
								  	 $partamarks = $subdetails['count(idquestions)'];
								  	  	 $resultupdatequery = self::updatepartwisemarksfora($Regid,$partamarks,$part);
								    Break;
								    
						    Case 'B':$part= 'B';
								 
								  	 $subdetails = self::partwiseattended($Regid,$part);
								  	 $partbmarks = $subdetails['count(idquestions)'];
								  	 $resultupdatequery = self::updatepartwisemarksfora($Regid,$partbmarks,$part);
								    Break;
								    
						    Case 'C':$part= 'C';
								  	
								  	 $subdetails = self::partwiseattended($Regid,$part);
								  	 $partcmarks = $subdetails['count(idquestions)'];
								  	 $resultupdatequery = self::updatepartwisemarksfora($Regid,$partcmarks,$part);
								    Break;							    
						}//end of switch
					}//end of for loop
		     }//end of main for loop
		
 	}//end of the function


}
?>