<?php 
class GeneralSetup_Model_DbTable_Gradebase extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_gradename';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetProgramDetails() { //Function to get the user details
      $lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_gradename"))
				 				 ->join(array('b' => 'tbl_programmaster'),'a.IdProgrammaster=b.IdProgrammaster',array('b.*'));
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     }
    
    public function fnaddProgram($formData) { //Function for adding the University details to the table
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $table = "tbl_gradename";
         $postData = array('IdProgrammaster' =>$formData['IdCourseMaster'],	
           					'GradeName' =>$formData['Gradename'],	
           					'Minmarks' => $formData['MinMarks'],
         					'Maxmarks'=>$formData['Maxmarks']);					
	     $lobjDbAdpt->insert($table,$postData);
	}
    
    public function fnupdateProgram($formData,$lintIdProgram) { //Function for updating the university
    	unset ( $formData ['Save'] );
		$where = 'IdProgram = '.$lintIdProgram;
		$this->update($formData,$where);
    }
    
	public function fnSearchProgram($post = array()) { //Function for searching the university details
		
	 $lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_gradename"))
				 				 ->join(array('b' => 'tbl_programmaster'),'a.IdProgrammaster=b.IdProgrammaster',array('b.*'))
				 				  ->where('b.ProgramName  like "%" ? "%"',$post['field3']);
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetCourseList(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.ProgramName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function updateactive($idprog)
	{
		//$lstrSelect ="Update tbl_program set Active=0 where idprogram !=$idprog";
		$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$post = array('Active'=>0);	
		//$where = "idprogram != '".$idprog."'"; 	
	     $where['idprogram != ? ']= $idprog;
		return $db->update('tbl_program',$post,$where);
		
	}

}
?>