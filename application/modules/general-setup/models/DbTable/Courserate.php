<?php 
class GeneralSetup_Model_DbTable_Courserate extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_courserate';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
 	public function fnProgramArray(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.ProgramName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fnSearchCourse($post = array()) { //Function for searching the university details
		$field7 = "a.Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_coursemaster'),array('IdCoursemaster'))
			   ->join(array('b' => 'tbl_programmaster'),'a.IdProgrammaster = b.IdProgrammaster',array('b.ProgramName','b.IdProgrammaster'))
			   ->where('a.CourseName  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		if($post['field5'])	$select ->where('a.IdProgrammaster  = ? ',$post['field5']);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
     public function fngetCoursemasterDetails() { //Function to get the user details
       $select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_coursemaster'),array('IdCoursemaster'))
			   ->join(array('b' => 'tbl_programmaster'),'a.IdProgrammaster = b.IdProgrammaster',array('b.ProgramName','b.IdProgrammaster'));
        $result = $this->fetchAll($select);
        return $result;
     }
    function fngetProgramrateMaster($wher){
    	$select = $this->select()
			   ->setIntegrityCheck(false) 
			   ->from( array('b' => 'tbl_coursemaster')) 	
			   ->joinLeft(array("a" => "tbl_courserate"),"a.idCourse = b.IdCoursemaster")
			   ->where('b.Active = 1')
			   -> where($wher); 
			  
		$result = $this->fetchAll($select);
        return $result;
    }
 	function fngetProgramrateDetails($wher){
    	$select = $this->select()
			   ->setIntegrityCheck(false) 
			   ->from(array("a" => "tbl_courserate")) 	
			   ->joinLeft( array('b' => 'tbl_coursemaster'),"a.idCourse = b.IdCoursemaster")
			   ->joinLeft( array('c' => 'tbl_accountmaster'),"c.idAccount = a.IdAccountmaster")
			   ->where('b.Active = 1')
			   -> where($wher); 			  
		$result = $this->fetchAll($select);
        return $result->toArray(); 
    }
	function fngetProgramrateEditDetails($wher){
    	$select = $this->select()
			   ->setIntegrityCheck(false) 
			   ->from(array("a" => "tbl_courserate")) 				   
			   -> where($wher); 			  
		$result = $this->fetchRow($select);
        return $result; 
    }
	public function fnAccountArray(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_accountmaster"),array("key"=>"idAccount","value"=>"AccountName"))
				 				 ->where("Active = 1")
				 				 ->where("a.coursetype=1")
				 				 ->order("AccountName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
    
    public function fnaddProgramrate($formData) { //Function for adding the University details to the table
			$this->insert($formData);
	}
    
    public function fnupdateProgramrate($formData,$idProgramrate) { //Function for updating the university
    	unset($formData ['idProgramrate']);
		$where = 'idCourserate = '.$idProgramrate;
		$this->update($formData,$where);
    }
}
?>