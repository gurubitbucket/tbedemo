<?php
class GeneralSetup_Model_DbTable_Subjectmaster extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_subjectmaster';
	
	public function fngetSubjectDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("sm"=>"tbl_subjectmaster"),array("sm.*"))
       								->joinleft(array("dm"=>"tbl_departmentmaster"),'sm.IdDepartment = dm.IdDepartment',array("dm.*"))
       								->joinleft(array("cm"=>"tbl_collegemaster"),'dm.IdCollege = cm.IdCollege',array("cm.CollegeName as BranchName","cm.CollegeType"))
       								->joinleft(array("cm2"=>"tbl_collegemaster"),'cm.Idhead =cm2.IdCollege',array("cm2.CollegeName"))
       								//->where('cm.CollegeType = 1')
       								//->where('cm2.CollegeType = 0')
       								->group("sm.IdSubject")
       								->order("dm.IdDepartment");
       								//->where("dm.DepartmentType  = 0");
       					//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
     }
     
	public function fnSearchSubject($post = array()) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("sm"=>"tbl_subjectmaster"),array("sm.*"))
       								->joinleft(array("dm"=>"tbl_departmentmaster"),'sm.IdDepartment = dm.IdDepartment',array("dm.*"))
       								->joinleft(array("cm"=>"tbl_collegemaster"),'dm.IdCollege = cm.IdCollege',array("cm.CollegeName as BranchName","cm.CollegeType"))
       								->joinleft(array("cm2"=>"tbl_collegemaster"),'cm.Idhead =cm2.IdCollege',array("cm2.CollegeName"));
       								
		if(isset($post['field5']) && !empty($post['field5']) ){
				$lstrSelect = $lstrSelect->where("cm.Idhead = ?",$post['field5']);
										
		}
		if(isset($post['field8']) && !empty($post['field8']) ){
				$lstrSelect = $lstrSelect->where("cm.IdCollege = ?",$post['field8']);
										
		}
		if(isset($post['field20']) && !empty($post['field20']) ){
				$lstrSelect = $lstrSelect->where("sm.IdDepartment = ?",$post['field20']);
										
		}
       							//	->where("cm.IdCollege= ?",$post['field5'])
       								//->where("cm.Idhead= ?",$post['field5'])
       								//->where('sm.IdDepartment like "%" ? "%"',$post['field20'])
       								
       	$lstrSelect	->where('sm.SubjectName like "%" ? "%"',$post['field2'])
       				->where('sm.SubCode like "%" ? "%"',$post['field3'])
       				->where("sm.Active = ".$post["field7"])
       				->group("sm.IdSubject")
       			    ->order("dm.IdDepartment");
       					echo $lstrSelect;die();
       					
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	 }
    /* public function fnSearchSubject($post = array()) { //Function for searching the user details
    	$db = Zend_Db_Table::getDefaultAdapter();
		$field7 = "a.Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_subjectmaster'),array('IdSubject'))
			   ->join(array("dm"=>"tbl_departmentmaster"),'dm.IdDepartment = a.IdDepartment')
			   ->join(array("cm"=>"tbl_collegemaster"),'cm.IdCollege = dm.IdCollege')
			   //->join(array("um"=>"tbl_universitymaster"),'um.IdUniversity = cm.AffiliatedTo')
			   ->where('cm.AffiliatedTo like "%" ? "%"',$post['field1'])
			   ->where('cm.IdCollege like "%" ? "%"',$post['field5'])
			   ->where('a.SubjectName like "%" ? "%"',$post['field2'])
			   ->where('a.SubCode like  "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}*/
	
     
/*public function fngetSubjectDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("sm"=>"tbl_subjectmaster"),array("sm.*"))
       								->join(array("dm"=>"tbl_departmentmaster"),'dm.IdDepartment = sm.IdDepartment',array("dm.*"))
       								->join(array("cm"=>"tbl_collegemaster"),'cm.IdCollege = dm.IdCollege',array("cm.CollegeName as CollegeName"))
       								->join(array("cm1"=>"tbl_collegemaster"),'cm1.IdCollege = dm.IdCollege',array("cm1.CollegeName as BranchName"))
       								->where("cm.CollegeType = 0")
       								->where("cm1.CollegeType = 1");
       								echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
     }*/
     
	public function fnGetUniversityMasterList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("um"=>"tbl_universitymaster"),array("key"=>"um.IdUniversity","value"=>"um.Univ_Name"))
		 				 ->where("um.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnaddSubject($larrformData) { //Function for adding the user details to the table
		$this->insert($larrformData);
	}
	
	public function fnGetDepartmentList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_departmentmaster"),array("key"=>"a.IdDepartment","value"=>"a.DepartmentName"))
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
		public function fnGetCollegeList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		 				 ->where("a.CollegeType = 0")
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetBranchList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		 				 ->where("a.CollegeType = 1")
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
		
    public function fnviewSubject($lintisubject) { //Function for the view user 
    	//echo $lintidepartment;die();
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("a" => "tbl_subjectmaster"),array("a.*"))				
		            	->where("a.IdSubject= ?",$lintisubject);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
    
    public function fnupdateSubject($lintIdDepartment,$larrformData) { //Function for updating the user
	$where = 'IdSubject = '.$lintIdDepartment;
	$this->update($larrformData,$where);
    }
	
}