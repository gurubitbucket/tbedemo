<?php
class GeneralSetup_Model_DbTable_Takafuloperator extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_takafuloperator';
	
public function fnGetBusinesstypeList()
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_businesstype"),array("key"=>"a.idbusinesstype","value"=>"a.BusinessType"))
					 				 ->where("a.Active = 1")
					 				 ->order("a.idbusinesstype");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;	
}


	
 /*
  * function to fetch all the details of the takaful operator
  */
public function fngetTakafulOperator(){//function to fetch all the details of the takaful operator
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
		->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
		->joinleft(array("b"=>"tbl_takafulpaymenttype"),"a.idtakafuloperator=b.idtakafuloperator",array("b.*"))
		->order("a.TakafulName");
		$result = $db->fetchAll($lstrSelect);
		return $result;
	}

/*
 * function to fetcha ll the details based on the id
 */
public function fnGetTakafulDetails($intidtakafuloperator)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
					->where("a.idtakafuloperator=?",$intidtakafuloperator);
	$result = $db->fetchRow($lstrSelect);
	return $result;
}

public function fngetexistingtakafull($takafull)
{  	
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
					->where("a.LoginId='$takafull'");
					//echo $lstrSelect;die();
	$result = $db->fetchRow($lstrSelect);
	return $result;
}

public function fngetexistingtakafullname($takafullname)
{  	
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
					->where("a.TakafulName='$takafullname'");
	$result = $db->fetchRow($lstrSelect);
	return $result;
}

/*
 * function to update the takaful operator
 */


/*
 * function to search the takaful operator
 */
public function fnSearchTakafulOperator($post)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
					->joinleft(array("b"=>"tbl_takafulpaymenttype"),"a.idtakafuloperator=b.idtakafuloperator",array("b.*"))
					->where('a.TakafulName like  ? "%"',$post['Takafuldrop'])
					->where('a.TakafulShortName like  ? "%"',$post['TakafulSN'])
					->order("a.TakafulName");
					
		if(isset($post['TakafulId']) && !empty($post['TakafulId']) ){
				$lstrSelect = $lstrSelect->where("a.idtakafuloperator = ?",$post['TakafulId']);
		}
					
					
	$result = $db->fetchAll($lstrSelect);
	return $result;
}
public function fnGetCountryList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_countries"),array("key"=>"a.idCountry","value"=>"CountryName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.CountryName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
		
	//Get State List
	public function fnGetStateList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"StateName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.StateName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
public function  fnGetcityList($idstate)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
         	$select = $db->select()
			   ->from(array('a' => 'tbl_city'),array("key"=>"a.idCity","value"=>"CityName"))
			   ->where('a.idState=?',$idstate);
		$result = $db->fetchAll($select);
		return $result;
	}
	
public function fnGetOperatorNames($namestring){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				->from(array("tbl_takafuloperator"),array("TakafulName as name"))
				->where('TakafulName like ? "%"',$namestring);
		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     	
     }
     
public function fngettakafulids(){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				->from(array("tbl_takafuloperator"),array("key"=>"tbl_takafuloperator.idtakafuloperator","value"=>"tbl_takafuloperator.idtakafuloperator"))
				->order("tbl_takafuloperator.idtakafuloperator");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     	
     }
	  public function fnaddtakafuloperator($larrformdata)
 {
 	$larrformdata['workphone'] = $larrformdata['workcountrycode']."-".$larrformdata['workstatecode']."-".$larrformdata['workphone'];
 		unset($larrformdata['workcountrycode']);
		unset($larrformdata['workstatecode']);
        unset($larrformdata['Active']);
    if(isset($larrformdata['TakafulField6']))
		{
		   $larrformdata['TakafulField6']=1;
		}
		else
		{
		  $larrformdata['TakafulField6']=0;
		}
 	return $this->insert($larrformdata);
 }
	 public function fnupdatetakafuloperator($formData,$lintidtakafuloperator)
{
	$formData['workphone'] = $formData['workcountrycode']."-".$formData['workstatecode']."-".$formData['workphone'];
 		unset($formData['workcountrycode']);
		unset($formData['workstatecode']);
                unset($formData['Active']);
				 if(isset($formData['TakafulField6']))
		{
		   $formData['TakafulField6']=1;
		}
		else
		{
		  $formData['TakafulField6']=0;
		}

	$db =Zend_Db_Table::getDefaultAdapter();
	$table = "tbl_takafuloperator";
	$where['idtakafuloperator = ? '] = $lintidtakafuloperator;
	$db->update($table,$formData,$where);
				
}
}