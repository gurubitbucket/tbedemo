<?php
class GeneralSetup_Model_DbTable_Takafuloperator extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_takafuloperator';
	
	public function fnGetBusinesstypeList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("a"=>"tbl_businesstype"),array("key"=>"a.idbusinesstype","value"=>"a.BusinessType"))
		->where("a.Active = 1")
		->order("a.idbusinesstype");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnaddtakafuloperator($larrformdata){
		$larrformdata['workphone'] = $larrformdata['workcountrycode']."-".$larrformdata['workstatecode']."-".$larrformdata['workphone'];
		unset($larrformdata['workcountrycode']);
		unset($larrformdata['workstatecode']);
		return $this->insert($larrformdata);
	}
  
	public function fngetTakafulOperator(){//function to fetch all the details of the takaful operator
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
		->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
		->joinleft(array("b"=>"tbl_takafulpaymenttype"),"a.idtakafuloperator=b.idtakafuloperator",array("b.*"));
		$result = $db->fetchAll($lstrSelect);
		return $result;
	}
 
	public function fnGetTakafulDetails($intidtakafuloperator){// function to fetcha ll the details based on the id
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
		->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
		->where("a.idtakafuloperator=?",$intidtakafuloperator);
		$result = $db->fetchRow($lstrSelect);
		return $result;
	}

	public function fnupdatetakafuloperator($formData,$lintidtakafuloperator){// function to fetcha ll the details based on the id
		$formData['workphone'] = $formData['workcountrycode']."-".$formData['workstatecode']."-".$formData['workphone'];
		unset($formData['workcountrycode']);
		unset($formData['workstatecode']);
		$db =Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_takafuloperator";
		$where['idtakafuloperator = ? '] = $lintidtakafuloperator;
		$db->update($table,$formData,$where);
	}
 
	public function fnSearchTakafulOperator($post){// function to search the takaful operator
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
		->from(array("a"=>"tbl_takafuloperator"),array("a.*"))
		->where('a.TakafulName like  ? "%"',$post['field3']);
		$result = $db->fetchAll($lstrSelect);
		return $result;
	}
	
	public function fnGetCountryList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("a"=>"tbl_countries"),array("key"=>"a.idCountry","value"=>"CountryName"))
		->where("a.Active = 1")
		->order("a.CountryName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetStateList(){//Get State List
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"StateName"))
		->where("a.Active = 1")
		->order("a.StateName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function  fnGetcityList($idstate){
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
		->from(array('a' => 'tbl_city'),array("key"=>"a.idCity","value"=>"CityName"))
		->where('a.idState=?',$idstate);
		$result = $db->fetchAll($select);
		return $result;
	}
}