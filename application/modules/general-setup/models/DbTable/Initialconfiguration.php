<?php
class GeneralSetup_Model_DbTable_Initialconfiguration extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_config';
	
	public function fnSearchUniversity($post = array()) { //Function for searching the user details
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_universitymaster'),array('IdUniversity'))
			   ->where('a.Univ_Name like "%" ? "%"',$post['field3']);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fnGetInitialConfigDetails($iduniversity) {
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()
					->from(array("a" => "tbl_config"),array("a.*"))				
		            ->where("a.idUniversity = ?",1);	
		 return $result = $lobjDbAdpt->fetchRow($select);
	}
	
	public function fnAddInitialConfig($larrformData) { //Function for adding the user details to the table
		$this->insert($larrformData);
	}
	
	public function fnUpdateInitialconfig($idconfig,$formData)
	{
		
		
		unset($formData['Univ_Name']);
		$where = 'idConfig = '.$idconfig;
		$this->update($formData,$where);
		
		
		/*$db = Zend_Db_Table::getDefaultAdapter();
		$larrformData1['Univ_Name'] = $unv;	
		 $where = "IdUniversity  = 1'"; 	
		 $db->update('tbl_universitymaster',$larrformData1,$where);*/
		
		
	}
	
	public function  fnupdateuniversity($larrFormData)
	{
		$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$data = array('Univ_Name' => $larrFormData );
		$where['IdUniversity = ? ']= 1;		
		return $db->update('tbl_universitymaster', $data, $where);
	}
	
public function fnGetReceiptGenerationValues(){
			$larrResult = array("company" =>"Company","state"=>"State","year"=>"Year");
			return $larrResult;
		}
	
		
public function fnGetCurrencyValues(){
			$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
				 				 ->from(array("a"=>"tbl_countries"),array("key"=>"a.CurrencyShortName","value"=>"a.CurrencyShortName"))
				 				 ->where("a.CurrencyName is not null")
				 				 ->group('a.CurrencyShortName');
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
		}
		
		public function  fnGetSturegGenerationValues()
		{
			$larrResults = array("uniqueid" =>"Unique Id","year"=>"Year","takaful"=>"Takaful Operator");
			return $larrResults;
		}
public function fnGetdropGenerationValues()
{
	$larrResults = array("0" =>"Numeric","1"=>"Alpha-Numeric");
			return $larrResults;
}
public function fnregistrationdatechange($idconfig,$formData)
{
        $db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
				 				 ->from(array("a"=>"tbl_config"),array("a.*"))
				 				 ->where("a.idUniversity =?",$idconfig)
				 				 ->where("a.MinAge=?",$formData['MinAge'])
								 ->where("a.ClosingBatch=?",$formData['ClosingBatch'])
								 ->where("a.ClosingBatchCompany=?",$formData['ClosingBatchCompany'])
								 ->where("a.ClosingBatchTakaful=?",$formData['ClosingBatchTakaful']);
		$larrResult = $db->fetchAll($lstrSelect);
		if(empty($larrResult))
		{
		    //echo "fine";die();
		    $db = Zend_Db_Table::getDefaultAdapter();
          	$tables = "tbl_registrationdatechangedetails";
			$arraydata = array(	
		                     'idUniversity' =>$idconfig,	
							 'MinAge' =>$formData['MinAge'],	
		                     'ClosingBatch'=>$formData['ClosingBatch'],
           					 'ClosingBatchCompany' =>$formData['ClosingBatchCompany'],
		                     'ClosingBatchTakaful'=>$formData['ClosingBatchTakaful'],
							  'UpdDate' =>date('Y-m-d H:i:s' ),
		                     'UpdUser'=>$formData['UpdUser']
						);
	      
		  $db->insert($tables,$arraydata);//die();
		 
		}
		return $larrResult;

}
public function fngetinitialconfigchangedetails()
{
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_registrationdatechangedetails"),array("a.MinAge","a.ClosingBatch","a.ClosingBatchCompany","a.ClosingBatchTakaful","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as Changedate","a.UpdUser"))							
							 ->join(array("b" => "tbl_user"),'b.iduser = a.UpdUser',array("b.fName as User"))
							 ->join(array("c" => "tbl_config"),'c.idUniversity  = a.idUniversity',array(""))
							 ->order("a.Idchangedetails desc ")
							 ->limit(10, 0);;							
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;		   

}

}