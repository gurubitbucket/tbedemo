<?php
     class GeneralSetup_Model_DbTable_Studenttype extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	
	        protected $_name = 'tbl_termsandcondition';
	
            //function to add the template
            public function fnaddtermsandconditions($post)
	        {	
	           	         
	           $this->insert($post);	
	        }
	        
	        //Function to get the webpagetemplate details 	
            public function fngettermsandconditions()
            {             
   	         $result=$this->fetchAll();
   	         return $result;
            }
 	
             //Function to get the selected auhtor details 			 
            public function fnedittermsandconditions($id)     
			{		      		
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect =$lobjDbAdpt->select()
									->from(array('b'=>'tbl_businesstype'),array('b.*'))
									->where('idbusinesstype='.$id );
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
			} 
           
			//Function to update Author details 
           public function fnupdatebusinesstype($id,$udata) 
            {             
	         $where = "idbusinesstype = '$id'";
	         $this->update($udata,$where);   
            }  
           
            //Function for searching the author details	
           public function fnSearchbusinesstype($post = array())
	       { $field7 = "Active = ".$post["field7"];	       	  	
	         $select = $this->select()
			                ->from(array('a' => 'tbl_businesstype'),array('a.*'))
			                ->where('a.BusinessType  like "%" ? "%"',$post['field2'])
			                ->where($field7);  
		     $result = $this->fetchAll($select);
		     return $result->toArray();
	       }
				
	         //Function To Get Pagination Count from Initial Config
	       public function fnGetPaginationCountFromInitialConfig()
	       {
		       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		       $lintPageCount = "";
		       $lstrSelect = $lobjDbAdpt->select()
								        ->from(array("a"=>"tbl_config"),array("noofrowsingrid") );
		       $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		       if($larrResult['noofrowsingrid'] == "" || $larrResult['noofrowsingrid'] == "0")
		       {
			    $lintPageCount = "5";
		       }
		       else
		       {
			    $lintPageCount = $larrResult['noofrowsingrid'];
		       }
		       return $lintPageCount;
	       }
	
	       //Function to Get Initial Config Data
	       public function fnGetInitialConfigData()
	       {
		    $lobjDbAdpt=Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect=$lobjDbAdpt->select()
								   ->from(array("a"=>"tbl_definationms"),array("LCASE(SUBSTRING(a.DefinitionCode,1,2)) AS Language") )
					 			   ->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType',array())
				 				   ->join(array("c"=>"tbl_config"),'c.Language = a.idDefinition',array("c.HtmlDir","c.DefaultCountry"));		  
		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		    return $larrResult;
	       }	
        }    
		  	 
	
