<?php
class GeneralSetup_Model_DbTable_Companieslist extends Zend_Db_Table { //Model Class for Companies List Details
	protected $_name = 'tbl_companies';
	private $lobjDbAdpt;
	
	public function init() { 		//Intialising Default adapter
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
	}
	
	public function fngetCompanyDetailsById($lintidcompany) { //Function to get the Company Details
		$db = Zend_Db_Table::getDefaultAdapter ();
		$SqlQuery = $db->select ()->from ( array ("tbl_companies" => "tbl_companies" ), array ("tbl_companies.IdCompany", "tbl_companies.CompanyName", "tbl_companies.ShortName", "tbl_companies.Address", "tbl_companies.RegistrationNo", "tbl_companies.Address", "tbl_companies.Phone1", "tbl_companies.Postcode", "tbl_companies.Phone1", "tbl_companies.Phone2", "tbl_companies.Email", "tbl_companies.AltEmail", "tbl_companies.Fax", "tbl_companies.ContactPerson", "tbl_companies.Password", "tbl_companies.Login" ) )
								->join ( array ("tbl_businesstype" => "tbl_businesstype" ), "tbl_companies.businesstype= tbl_businesstype.idbusinesstype", array ("tbl_businesstype.idbusinesstype", "tbl_businesstype.BusinessType" ) )
								->join ( array ("tbl_countries" => "tbl_countries" ), "tbl_companies.IdCountry = tbl_countries.idCountry", array ("tbl_countries.idCountry", "tbl_countries.CountryName" ) )
								->join ( array ("tbl_state" => "tbl_state" ), "tbl_companies.IdState = tbl_state.idState", array ("tbl_state.idState", "tbl_state.StateName" ) )
								->join ( array ("tbl_city" => "tbl_city" ), "tbl_companies.City = tbl_city.idCity", array ("tbl_city.idCity", "tbl_city.CityName" ) )
								->where ( "tbl_businesstype.Active = 1" )
								->where ( "tbl_state.Active = 1" )->where ( "tbl_city.Active = 1" )
								->where ( 'tbl_companies.IdCompany = ?', $lintidcompany )
								->order ( "tbl_companies.CompanyName" );
					$result = $db->fetchRow( $SqlQuery );
			return $result;
	}
	
	public function fnupdateCompanyDetails($lintIdCompany, $larrformData) { //Function for updating the Company Details
		$where = 'IdCompany = ' . $lintIdCompany;
		$this->update ( $larrformData, $where );
	}

}