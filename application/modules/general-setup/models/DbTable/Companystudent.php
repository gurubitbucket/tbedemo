<?php
     //Model Class for Users Details	
     class GeneralSetup_Model_DbTable_Companystudent extends Zend_Db_Table_Abstract 
  {    
            //function to add the template
            public function fngetcompanynames()
	   		{
	   			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"CompanyName")) 				 
				 				 ->order("a.CompanyName");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	   		}	
	   		
  			public function fngetcompanystudent()
			{ 
				/*$companyname=$larrformData['field5'];
				$studentname= $larrformData['field2'];
				$registrationpin=$larrformData['field3'];*/
				
		    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    	$lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_companies"),array()) 	   			   
			           ->join(array('b' => 'tbl_batchregistration'),'b.idCompany = a.IdCompany',array('b.registrationPin as RegistrationPIN'))
			           ->join(array('c' => 'tbl_registereddetails'),'b.registrationPin = c.RegistrationPin',array())
			           ->join(array('d' => 'tbl_studentapplication'),'c.IDApplication = d.IDApplication',array('d.FName','d.IDApplication'))	
			           ->join(array('e' => 'tbl_programmaster'),'d.Program= e.IdProgrammaster',array('e.ProgramName'));
			           
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    	return $larrResult;              
		   }
  }    
		  	 
	
