<?php
class GeneralSetup_Model_DbTable_Studentedit extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	

 public function fnviewstudentdetailssss()
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->where("d.RegistrationPin=0000000")
										  ->group("d.IDApplication")
										  ->where("a.pass=3")
										  ->where("a.Datetime>=curdate()");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
 public function fneditdetails($lintidstudent)
 {
 	
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->joinleft(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->joinleft(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->joinleft(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->joinleft(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->joinleft(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										 ->where("a.IDApplication=?",$lintidstudent);
										// ->where("datetime>'2012-04-14'") 
										// ->where("payment =1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
     
   /*
      * functin to search the student
      */
	public function fnSearchStudent($post = array()) { //Function for searching the user details
    	//$db = Zend_Db_Table::getDefaultAdapter();
    
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										 ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->where("d.RegistrationPin=0000000")
										  ->group("d.IDApplication")
										  ->where("a.pass=3")	
			   							->where('a.FName like  "%" ? "%"',$post['field3']);
			   //->where('a.MName like "%" ? "%"',$post['field4']);
			   //
			  // ->where($field7);exit;
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
	
	 public function fnAddStudent($larrformData,$idapplication) { //Function to get the user details
   	

   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_studentapplication";
    	 $where = "IDApplication = '".$idapplication."'"; 	
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['FName'],	
           					'DateOfBirth' =>$larrformData['DateOfBirth'],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['EmailAddress'],	
           					'PermAddressDetails' =>$larrformData['PermAddressDetails'],	
           					'Takafuloperator' =>$larrformData['Takafuloperator'], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 0, 
							'Gender' =>$larrformData['Gender'],	
           					'Race' =>$larrformData['Race'],	
           					'Qualification' =>$larrformData['Qualification'], 
							'State' =>$larrformData['State'],	
           					'CorrAddress' =>$larrformData['CorrAddress'],	
           					'PostalCode' =>$larrformData['PostalCode'], 
		 					'ContactNo' =>$larrformData['ContactNo'],	
           					'MobileNo' =>$larrformData['MobileNo'], 
		  							  						);
						 $lobjDbAdpt->update('tbl_studentapplication',$postData,$where);				 
						 
     }
     
     public function mailstudent($idapplication)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			echo	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										   ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->join(array("e"=>"tbl_managesession"),'a.Examsession=e.idmangesession',array('e.*'))
										   ->join(array("f" =>"tbl_newscheduler"),'a.Year=f.idnewscheduler',array('f.Year as years'))
										 // ->where("a.IDApplication=?",$idapplication);
										  ->where("datetime>'2012-04-14'") 
										 ->where("payment =1"); die();
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
 
}