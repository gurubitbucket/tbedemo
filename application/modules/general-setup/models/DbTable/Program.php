<?php 
class GeneralSetup_Model_DbTable_Program extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_program';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetProgramDetails() { //Function to get the user details
        $result = $this->fetchAll();
        return $result;
     }
    
    public function fnaddProgram($formData) { //Function for adding the University details to the table
			return $this->insert($formData);
			
			//self::updateactive();
	}
    
    public function fnupdateProgram($formData,$lintIdProgram) { //Function for updating the university
    	unset ( $formData ['Save'] );
		$where = 'IdProgram = '.$lintIdProgram;
		$this->update($formData,$where);
    }
    
	public function fnSearchProgram($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_program'),array('IdProgram'))
			   ->where('a.ProgramName  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fnGetProgramList(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_program"),array("key"=>"a.IdProgram","value"=>"ProgramName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.ProgramName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function updateactive($idprog)
	{
		//$lstrSelect ="Update tbl_program set Active=0 where idprogram !=$idprog";
		$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$post = array('Active'=>0);	
		//$where = "idprogram != '".$idprog."'"; 	
	     $where['idprogram != ? ']= $idprog;
		return $db->update('tbl_program',$post,$where);
		
	}

}
?>