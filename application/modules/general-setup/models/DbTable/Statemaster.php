<?php
   class GeneralSetup_Model_DbTable_Statemaster extends Zend_Db_Table 
   {	
	    protected $_name = 'tbl_state';

	    //Function to get the state details  
        public function fncityinfodtl($idCountry,$idState) 
        {      
        $result = $this->fetchRow( "idCountry = '$idCountry' and idState = '$idState'") ;
        return $result->toArray();
        } 	
        
        //Function to get the state list
        public function fnGetStatelist($idCountry) 
        {
		 $db = Zend_Db_Table::getDefaultAdapter();
	     $select = "SELECT a.* 
                    FROM tbl_state AS a ,tbl_countries AS b
                    where a.idCountry=b.idCountry And a.idCountry=$idCountry AND a.Active=1 AND b.Active=1
                    ORDER BY StateName ASC";
		 /*$select = "SELECT a.Countryname,b.* ";
		 $select .=" FROM tbl_state b, tbl_countries a";
		 $select .=" WHERE a.idCountry = b.idCountry and a.Active ='1' and b.Active ='1'";
		 $select .=" AND a.idCountry =".$idCountry;	*/					
		 $result = $db->fetchAll($select);		
		 return $result;
	    }
	     
	    //Function to add the states
         public function fnAddState($insertData) 
        {
		 return $this->insert($insertData);
	    }
	     
	    //Function to get the countryname based on id
         public function fnCountryName($idCountry)
	    {
	     $db = Zend_Db_Table::getDefaultAdapter();
	     $select =$db->select('CountryName')
	            ->from(array('a' => 'tbl_countries'),'CountryName')
				->where('a.idCountry='.$idCountry);
	     $result = $db->fetchRow($select);
	     return $result;
        }
        
        //Function for updating the states
        public function fnUpdateState($UpdateData,$idState)
         {           
 	       $where = "idState = '$idState'";	       
	       $this->update($UpdateData,$where);
         }
	     
         //Function for searching the state
         public function fnSearchState($larr) 
         {
           $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   	   $lstrSelect = $lobjDbAdpt->select()
       								->from(array("c"=>"tbl_countries"),array("c.*"))
       								->where('c.CountryName like "%" ? "%"',$larr);       					
		   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		   return $larrResult;
         }
        
       //////////
   public function fnGetStateslist($idCountry) 
        {
		// Function to fetch the State  for dropdown
        	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()
					 		   ->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
			                   ->join(array("b"=>"tbl_city"),"a.idState=b.idState",array(""))
								->where("a.idCountry=121")
								->group('a.StateName')
								->order('a.StateName');
			$result = $db->fetchAll($sql); 
			return $result;		
		
	    }  
         
   public function fnCountryNames($idCountry)
	    {
	     $db = Zend_Db_Table::getDefaultAdapter();
	     $select =$db->select('CountryName')
	            ->from('tbl_countries',array('key' =>'idCountry', 'value'=>'CountryName'))
				->where('a.idCountry='.$idCountry);
	     $result = $db->fetchRow($select);
	     return $result;
        }
         
       
         
         
   }
