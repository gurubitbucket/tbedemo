<?php
class GeneralSetup_Model_DbTable_Promote extends Zend_Db_Table { //Model Class for Users Details

	
	//Function to Get Initial Config Data
	public function fngetPromoteDetails(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a"=>"tbl_studentapplication"),array("CONCAT(a.FName,' ',IFNULL(a.MName,' '),' ',IFNULL(a.LName,' ')) AS Name","IFNULL(a.StudentId,' ')","a.DateOfBirth") )
					 			 ->join(array("b"=>"tbl_registereddetails"),'a.IDApplication=b.IDApplication',array())
				 				 ->join(array("c"=>"tbl_batchmaster"),'a.IdBatch=c.IdBatch',array("c.BatchName"))
				 				 ->join(array("d"=>"tbl_tosmaster"),'a.IdBatch=d.IdBatch',array("d.NosOfQues"))
				 				 //->join(array("e"=>"tbl_answerdetails"),'e.Regid=b.Regid',array("count(e.Regid) AS Attended"))
				 		      ->join(array("g"=>"tbl_schedulervenuetime"),'g.idschedulervenuetime=a.VenueTime',array("g.From"))
				 				->join(array("h"=>"tbl_programmaster"),'a.Program=h.IdProgrammaster',array("h.ProgramName"))
				 				 ->join(array("i"=>"tbl_schedulermaster"),'i.idschedulermaster=a.idschedulermaster' ,array("i.ScheduleName"))
				 				 ->join(array("k"=>"tbl_schedulervenue"),'k.idschedulervenue=a.Venue and k.idschedulermaster = i.idschedulermaster',array())
				 				 ->join(array("f"=>"tbl_center"),'k.idcenter=f.idcenter',array("f.centername"));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
        
	public function fnPromoteSearch($post = array()) {
		//print_r($post);die(); //Function for searching the user details
    	$db = Zend_Db_Table::getDefaultAdapter();
		//$field7 = "Active= ".$post["field7"];
		$select = $db->select() 	
			  ->from(array("a"=>"tbl_studentapplication"),array("CONCAT(a.FName,' ',IFNULL(a.MName,' '),' ',IFNULL(a.LName,' ')) AS Name","a.StudentId","a.DateOfBirth") )
					 			 ->join(array("b"=>"tbl_registereddetails"),'a.IDApplication=b.IDApplication',array())
				 				 ->join(array("c"=>"tbl_batchmaster"),'c.IdBatch=a.IdBatch',array("c.BatchName"))
				 				 ->join(array("d"=>"tbl_tosmaster"),'d.IdBatch=a.IdBatch',array("d.NosOfQues"))
				 				// ->join(array("e"=>"tbl_answerdetails"),'b.Regid=e.Regid',array("count( e.Regid ) AS Attended"))
				 		    	  ->join(array("g"=>"tbl_schedulervenuetime"),'g.idschedulervenuetime=a.VenueTime',array("g.From"))
				 				->join(array("h"=>"tbl_programmaster"),'a.Program=h.IdProgrammaster',array("h.ProgramName"))
				 				 ->join(array("i"=>"tbl_schedulermaster"),'i.idschedulermaster=a.idschedulermaster' ,array("i.ScheduleName"))
				 				 ->join(array("k"=>"tbl_schedulervenue"),'k.idschedulervenue=a.Venue and k.idschedulermaster = i.idschedulermaster',array())
				 				 ->join(array("f"=>"tbl_center"),'k.idcenter=f.idcenter',array("f.centername"))
			   ->where('a.FName like  ?"%"',$post['field3']);
			  // ->where('a.city like  ?"%"',$post['field2'])
			  // ->where('a.mName like "%" ? "%"',$post['field4'])
			   //->where('a.lName like "%" ? "%"',$post['field6'])
			  // ->where($field7);
		$result = $db->fetchAll($select);
		return $result;
	}
	
}