<?php
class GeneralSetup_Model_DbTable_Racesreport extends Zend_Db_Table { //Model Class for Users Details

	
  public function fnGetbatch()
  {
  			    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 			
	            $lstrSelect="SELECT `a`.`IdBatch` AS `key`, CONCAT(DATE_FORMAT(`a`.`BatchFrom`,'%d-%m-%Y'),'---',DATE_FORMAT(`a`.`BatchTo`,'%d-%m-%Y')) AS `value` FROM `tbl_batchmaster` as a";			
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  public function fngetnoofstudents($batch)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  		$select ="SELECT COUNT(IdBatch) AS Batch FROM tbl_studentapplication where IdBatch=$batch";
  		$larrResult = $lobjDbAdpt->fetchRow($select);
		return $larrResult;
  }
  
  public function fnTotalAttended($batch)
  {
  	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" =>"tbl_answerdetails"),array('a.*'))
								  ->join(array("b"=>"tbl_registereddetails"),'a.Regid=b.Regid')
								  ->where("b.IdBatch  = ?",$batch);	
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
  }
  
  public function fnGetBatchDetails($batch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" =>"tbl_batchmaster"),array('a.*'))
								  ->join(array("b"=>"tbl_programmaster"),'a.IdProgrammaster=b.IdProgrammaster')
								  ->where("a.IdBatch  = ?",$batch);	
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
  }
	
  public function fnGetracevalues()
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$lstrSelect = $lobjDbAdpt->select()
  	                         ->from(array("a" =>"tbl_definationms"),array('a.*'))
  	                         ->where("a.idDefType = ?",13);
  	                         
    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;                        
  }
}