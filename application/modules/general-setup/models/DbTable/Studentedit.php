<?php
class GeneralSetup_Model_DbTable_Studentedit extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	

 public function fnviewstudentdetailssss()
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->group("d.IDApplication")
										  ->order("a.IDApplication desc")
										 ->group("a.ICNO");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
 public function fneditdetails($lintidstudent)
 {
 	
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				      $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->joinleft(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->joinleft(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->joinleft(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->joinleft(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->joinleft(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										  //->joinleft(array("g"=>"tbl_batchregistration"),'a.idCompany')
										 ->where("a.IDApplication=?",$lintidstudent)
										 ->group("a.IDApplication");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
     
   /*
      * functin to search the student
      */
	public function fnSearchStudent($post = array()) { //Function for searching the user details
    	//$db = Zend_Db_Table::getDefaultAdapter();
    
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										 ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										 // ->where("a.Payment=0")
										   ->where('a.ICNO like  "%" ? "%"',$post['field3'])
			   							   ->order("a.IDApplication desc")
			   							    ->Limit("1");
			   //->where('a.MName like "%" ? "%"',$post['field4']);
			   //
			  // ->where($field7);exit;
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
	
	 public function fnAddStudent($larrformData,$idapplication) { //Function to get the user details
   	

   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_studentapplication";
    	 $where = "IDApplication = '".$idapplication."'"; 	
		  $postData = array(		
           					'FName' =>$larrformData['FName'],	
           					'DateOfBirth' =>$larrformData['DateOfBirth'],	
           					'EmailAddress' =>$larrformData['EmailAddress'],	
           					'PermAddressDetails' =>$larrformData['PermAddressDetails'],	
           					'Takafuloperator' =>$larrformData['Takafuloperator'], 
							'Gender' =>$larrformData['Gender'],	
           					'Race' =>$larrformData['Race'],	
           					'Qualification' =>$larrformData['Qualification'], 
							'State' =>$larrformData['State'],	
           					'CorrAddress' =>$larrformData['CorrAddress'],	
           					'PostalCode' =>$larrformData['PostalCode'], 
		 					'ContactNo' =>$larrformData['ContactNo'],	
           					'MobileNo' =>$larrformData['MobileNo'], 
		  							  						);
						 $lobjDbAdpt->update('tbl_studentapplication',$postData,$where);				 
						 
     }
     
   
     
	public function fnajaxgetcenternames($lintidname)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($lintidname == 1)
		{
		   $lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"CompanyName"))
									->order("a.CompanyName");
		}
		else
		{
		   $lstrSelect = $lobjDbAdpt->select()
				 				 	->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 				 
				 				 	->order("a.TakafulName");
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	}
	
	public function fninsertstudentchangeddetails($larrformdata,$idapplication)
	{
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				
		 $where = "ICNO = '".$larrformdata['ICNO']."'"; 	
		  $postData = array(		
							'Active' =>'0'
		  );
						 $lobjDbAdpt->update('tbl_studentchangeddetails',$postData,$where);				 
		

		  $table = "tbl_studentchangeddetails";
          $postData = array('IDApplication' =>$idapplication,	
           					'companytype' =>$larrformdata['Cmptype'],	
           					'Idcompany' => $larrformdata['cmpnames'],
                            'UpdDate' =>date('Y-m-d H:i:s'),
          					'UpdUser'=>$larrformdata['UpdUser'],
          					'ICNO'=>$larrformdata['ICNO'],
                             'pass' =>$larrformdata['pass']);					
	     $lobjDbAdpt->insert($table,$postData);	 
		
	}
	
	public function fngetCompanydetails($regpin)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_batchregistration"),array('a.*'))
								->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany',array('b.*'))
								->where("a.registrationPin='$regpin'");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;						
	}
	public function fngetTakafuldetails($regpin)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_batchregistration"),array('a.*'))
								->join(array("b" =>"tbl_takafuloperator"),'a.idCompany=b.idtakafuloperator',array('b.*'))
								->where("a.registrationPin='$regpin'");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;						
	}
     
 
}
