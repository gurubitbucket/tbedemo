<?php
class GeneralSetup_Model_DbTable_Companieslist extends Zend_Db_Table { //Model Class for Companies List Details
	protected $_name = 'tbl_companies';
	private $lobjDbAdpt;
	
	  public function fnGenerateQueries($intQueryType,$varAllOne,$varOthers = NULL)
	   {
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

			switch ($intQueryType)
		   		{ 
		   			//To get the details of a particular Company				
		   			case 1:	$strQuery = $lobjDbAdpt->select()
						 				 ->from ( array ("tbl_companies" => "tbl_companies" ), array ("tbl_companies.IdCompany", "tbl_companies.CompanyName", "tbl_companies.ShortName", "tbl_companies.Address", "tbl_companies.RegistrationNo", "tbl_companies.Address", "tbl_companies.Phone1", "tbl_companies.Postcode", "tbl_companies.Phone1", "tbl_companies.Phone2", "tbl_companies.Email", "tbl_companies.AltEmail", "tbl_companies.Fax", "tbl_companies.ContactPerson", "tbl_companies.Password", "tbl_companies.Login" ) )
										->join ( array ("tbl_businesstype" => "tbl_businesstype" ), "tbl_companies.businesstype= tbl_businesstype.idbusinesstype", array ("tbl_businesstype.idbusinesstype", "tbl_businesstype.BusinessType" ) )
										->join ( array ("tbl_countries" => "tbl_countries" ), "tbl_companies.IdCountry = tbl_countries.idCountry", array ("tbl_countries.idCountry", "tbl_countries.CountryName" ) )
										->join ( array ("tbl_state" => "tbl_state" ), "tbl_companies.IdState = tbl_state.idState", array ("tbl_state.idState", "tbl_state.StateName" ) )
										->join ( array ("tbl_city" => "tbl_city" ), "tbl_companies.City = tbl_city.idCity", array ("tbl_city.idCity", "tbl_city.CityName" ) )
										->where ( "tbl_businesstype.Active = 1" )
										->where ( "tbl_state.Active = 1" )
										->where ( "tbl_city.Active = 1" )
										->where ( 'tbl_companies.IdCompany = ?', $varOthers)
										->order ( "tbl_companies.CompanyName" );
									break;		
		   		}
	
		   		if($varAllOne == '0') 
		   		{
		   			$larrResult = $lobjDbAdpt->fetchRow($strQuery);
		   		}
			return $larrResult;
	   }
	public function fnupdateCompanyDetails($lintIdCompany, $larrformData) { //Function for updating the Company Details
		$larrformData['hint']=$larrformData ['NewPassword'] ;
		unset ( $larrformData ['NewPassword'] );
		$where = 'IdCompany = ' . $lintIdCompany;
		$this->update ( $larrformData, $where );
	}
public function fngetexistingCompanyname($Companyname)
{  	
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_companies"),array("a.*"))
					->where("a.CompanyName='$Companyname'");
	$result = $db->fetchRow($lstrSelect);
	return $result;
}
public function fnGetcompanyNameedit()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_companies"),array("key"=>"a.IdCompany","value"=>"a.CompanyName"))
										  //->where("a.IdCompany  not in(select idcompany from tbl_companypaymenttype)") 
										   ->order("a.CompanyName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
public function fnGetPaymentTerms()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										   ->where("a.idDefType  = ?","15");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
public function editcompanytypepayment($idedit)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_companypaymenttype"),array("tbl_companypaymenttype.*"))
										  ->where("tbl_companypaymenttype.idcompanypaymenttype=?",$idedit);		
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	public function fnSearchCompaniesnew($larrformData) 
	   {
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
			   ->joinleft(array("tbl_companypaymenttype"=>"tbl_companypaymenttype"),"tbl_companies.IdCompany=tbl_companypaymenttype.idcompany",array("tbl_companypaymenttype.*"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_companies.CompanyName like "%" ? "%"',$larrformData['field3'])
			   ->where('tbl_companies.ShortName like  "%" ? "%"',$larrformData['field2'])
			   ->where('tbl_companies.Address like "%" ? "%"',$larrformData['field6'])
			   ->where('tbl_companies.Phone1 like "%" ? "%"',$larrformData['field12'])
			   ->where('tbl_companies.Email like "%" ? "%"',$larrformData['field13']);
			   
			   
			   if(isset($larrformData['field4']) && !empty($larrformData['field4']) ){
			   $select=$select->where ( 'tbl_companies.IdCompany = ?', $larrformData['field4']);
			}

			if(isset($larrformData['field5']) && !empty($larrformData['field5']) ){
				$select = $select->where("tbl_companies.businesstype = ?",$larrformData['field5']);
			}
			
			if(isset($larrformData['field1']) && !empty($larrformData['field1']) ){
				$select = $select->where("tbl_companies.IdState = ?",$larrformData['field1']);
			}
			
			if(isset($larrformData['field8']) && !empty($larrformData['field8']) ){
				$select = $select->where("tbl_companies.City = ?",$larrformData['field8']);
			}
		
			$select ->order("tbl_companies.CompanyName");
			$result = $this->fetchAll($select);
			return $result;
	   }
	
public function fnupdatecompanytypepayment($larrformdata,$id)
	{
		 $db = Zend_Db_Table::getDefaultAdapter();
    	// $larrformdata['Examvenue'] = $idvenue;	
		 $where = "idcompanypaymenttype = '".$id."'"; 	
		 $db->update('tbl_companypaymenttype',$larrformdata,$where);
	}
public function fninsertdata($larrformdata)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_companypaymenttype";
		$postData = array(  'idcompany' =>$larrformdata['idcompany'],	
           					'paymenttype' =>$larrformdata['paymenttype'],	
           					'upduser' =>$larrformdata['upduser'], 
							'upddate' =>$larrformdata['upddate']);
		  $result = $lobjDbAdpt->insert($table,$postData);
						   return $result;
	}

}
