<?php
class GeneralSetup_Model_DbTable_Scheduler extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_user';
	
	/*
	 * function to fetch all the batch details
	 */
	public function fnGetBatchName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchmaster"),array("key"=>"a.IdBatch","value"=>"a.BatchName"))
										  ->where("a.BatchStatus  = ?","0");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}	
			
	/*
	 * function to fetch the from and to date
	 */
public function fnGetBatchDetailsAjax($idbatch){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchmaster"),array("a.*"))
										  ->where("a.idBatch  = ?",$idbatch);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;		
	}
	
	/*
	 * funcion to insert into the temp table
	 */
	public function fninsertintotemp($from,$to,$sessionID,$iddates,$date)
	{
		//echo"asfsdaf";
		 $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_tempvenuetimings";
            $postData = array(		
							'SessionId' => $sessionID,		
            				'Fromtime' =>$from,		
		            		'Totime' =>$to,	
		            		'iddate'=>$iddates,
            				'Date'=>$date														
						);			
	        $db->insert($table,$postData);
	}
	
	/*
	 * function to fetch all from the temp table
	 */
	public function fnfetchalltempdetails($sessionID,$Iddate)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = "SELECT * 
   	 			FROM tbl_tempvenuetimings 
   	 			WHERE SessionId='$sessionID' AND iddate=$Iddate";
   	    $resulttempdetails = $lobjDbAdpt->fetchAll($select);
   	   	 return $resulttempdetails;
	}
	
	/*
	 * function to fetch all centre
	 */
	public function fnfetchvenue()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("a.*"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
	}
	
	
public function fnGetSchedulerVenuess($idbatch){		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenue"),array("a.*"))
										  ->where("a.idschedulervenue  = ?",$idbatch);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;		
	}	
	/*
	 * function to insert into scheduler
	 */
	public function fninsertscheduler($larrformdata)
	{
		
     /*   echo"<pre/>";
        print_r($larrformdata);
        die();*/
		 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_schedulermaster";
            $postData = array(		
							'idBatch' => $larrformdata['idBatch'],	
           					 'ScheduleName' => $larrformdata['ScheduleName'],	
            				'UpdUser' =>$larrformdata['UpdUser'],		
		            		'UpdDate' =>$larrformdata['UpdDate']												
						);			
	       $db->insert($table,$postData);
	       $lastid  = $db->lastInsertId("tbl_schedulermaster","idschedulermaster");	 
	      /* print_R(count($larrformdata['idcenter']));
	       die();*/
	       for($i=0;$i<count($larrformdata['idcenter']);$i++)
	       {
	          	 $tables = "tbl_schedulervenue";
	       	     $arryresult['idschedulermaster']= $lastid;
	       	     $arryresult['idcenter'] = $larrformdata['idcenter'][$i];
	       	     $var = $arryresult['idcenter'];	       	     
	       	     $arryresult['NoofSeats']= $larrformdata['noofseats'][$var];
	       	     $arryresult['UpdDate']=$larrformdata['UpdDate'];
	       	     $arryresult['UpdUser']=$larrformdata['UpdUser'];
	       	   /*  print_R($arryresult);
	       	     die();*/
	       	     $db->insert($tables,$arryresult);
	       	     $lastidschedulervenue  = $db->lastInsertId("tbl_schedulervenue","idschedulervenue");
	       	     $larrresultvenue= self::fnGetSchedulerVenuess($lastidschedulervenue);
	       	     $larrresultvenue['NoofSeats'];
	       	     
	       	      for($j=0;$j<count($larrformdata['rowss']);$j++)
	       	      {
	       	      	$sessionID = Zend_Session::getId();
	       	      	
	       	      	$date = $larrformdata['rowss'][$j];
 
	       	    
	       	      	$tempdetails =self::fngetdetailsfromtemp($sessionID,$date);
	       	      	//print_r($tempdetails);
	       	      	//die();
	       	      	
	       	      	for($k=0;$k<count($tempdetails);$k++)
	       	      	{
	       	      		//echo "asdfsdaf";
	       	      		//die();
	       	      		
	       	      		 $time = $tempdetails[$k]['Fromtime'].'---'.$tempdetails[$k]['Totime'];
	       	      		 $tabless = "tbl_schedulervenuetime";
			       	     $arrytime['idschedulervenue']= $lastidschedulervenue;
			       	     $arrytime['From'] = $time;
			       	     $arrytime['Date'] = $tempdetails[$k]['Date'];
			       	     $arrytime['UpdDate']=$larrformdata['UpdDate'];
			       	     $arrytime['UpdUser']=$larrformdata['UpdUser'];
			       	     $arrytime['idcentre']=$larrformdata['idcenter'][$i];
			       	     $arrytime['NoofSeats']=$larrresultvenue['NoofSeats'];
			       	     $db->insert($tabless,$arrytime);
	       	      	}
	       	      	
	       	      }
	       	     
	       }

	      // self::
	       
	}
	
	public function fngetdetailsfromtemp($sessionid,$date)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = "SELECT * 
   	 			FROM tbl_tempvenuetimings 
   	 			WHERE SessionId='$sessionid' AND Date='$date'";
   	 	 $resulttempdetails = $lobjDbAdpt->fetchAll($select);
   	   	 return $resulttempdetails;
	}
	
	/*
	 * function to delete the temp details
	 */
  public function fnDeleteTempDetails($sessionID)
 	{
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$where = "SessionId = '".$sessionID."'";
 		$db->delete('tbl_tempvenuetimings',$where);
 		
 	}
 	
 	/*
 	 * function to fetch abased on the details
 	 */
 	public function fnfetchdetails($lintiddate)
 	{
 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_tempvenuetimings"),array("a.*"))
										  ->where("a.idtempvenuetimings = ?",$lintiddate);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 	}
 	
 	/*
 	 * function to fetch the scheduler details
 	 */
 	public function fngetSchedulerDetails()
 	{
 				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("a.*"));
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 	}
 	
 public function fnSearchScheduler($post)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("a.*"))
										  ->where('a.ScheduleName like ?"%"',$post['field3']);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }
 
 /*
  * functin for updting the temp venue timings
  */
 public function fnupdateintotemp($fromtime,$totime,$sessionID,$iddates,$date,$idtempvenuetimings)
 {
 	 $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_tempvenuetimings";
			$where['idtempvenuetimings = ? ']= $idtempvenuetimings;		            
            $postData = array(		
							'SessionId' => $sessionID,		
            				'Fromtime' =>$fromtime,		
		            		'Totime' =>$totime,	
		            		'iddate'=>$iddates,
            				'Date'=>$date														
						);			
	     	return $db->update($table,$postData, $where);	
 }
 
 /*
  * getdetails from scheduler
  */
   public function fngetScheduleredit($lintidscheduler)
 	{
 				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("a.*"))
										  //->join(array("b" =>"tbl_batchmaster"),'a.IdBatch=b.IdBatch')
										  ->where('a.idschedulermaster=?',$lintidscheduler);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 	}
	 	
	/*
	 * function to fetch all the centres and time 
	 */ 	
	public function fngetschedulercentretime($lintidscheduler)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_schedulermaster"),array("a.*"))
											  ->join(array("b" =>"tbl_schedulervenue"),'a.idschedulermaster=b.idschedulermaster')
											   ->join(array("c" =>"tbl_schedulervenuetime"),'b.idschedulervenue=c.idschedulervenue')
											  ->where('a.idschedulermaster=?',$lintidscheduler);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
	/*
	 * function to fetch all the distinct center 
	 */
	public function fngetschedulervenue($lintidscheduler)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_schedulermaster"),array("a.*"))
											  ->join(array("b" =>"tbl_schedulervenue"),'a.idschedulermaster=b.idschedulermaster')
											   ->join(array("c" =>"tbl_center"),'b.idcenter=c.idcenter')
											  ->where('a.idschedulermaster=?',$lintidscheduler);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
	/*
	 * function to fetch all the timings
	 */
	public function fngetschedulervenuetime($lintidschedulervenue)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  //->from(array("a" =>"tbl_schedulermaster"),array("a.*"))
											  ->from(array("b" =>"tbl_schedulervenue"),array())
											  ->join(array("c" =>"tbl_schedulervenuetime"),'b.idschedulervenue=c.idschedulervenue',array("c.*"))
											  ->where('b.idschedulervenue=?',$lintidschedulervenue)
											  ->group('c.Date');
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			    return $larrResult;
				
		
	}
	
	
	/*
	 * function to fetch for the inserting into the temp table
	 */
	public function fngetdetailsforvenue($venuedate)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  //->from(array("a" =>"tbl_schedulermaster"),array("a.*"))
											  ->from(array("b" =>"tbl_schedulervenue"),array('b.*'))
											  ->join(array("c" =>"tbl_schedulervenuetime"),'b.idschedulervenue=c.idschedulervenue')
											  ->where('b.idschedulervenue=?',$venuedate);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			    return $larrResult;
	}
	
	/*
	 * function to insert into the temp table
	 */
	public function fninsertintotempduringedit($larrschedulervenuetime,$sessionID)
	{
		//print_r($larrschedulervenuetime);
		//print_r(count($larrschedulervenuetime));
		//exit;
	      $db 	= Zend_Db_Table::getDefaultAdapter();
	      $larrinsertsession = array();
 	       for($linti = 0;$linti<count($larrschedulervenuetime);$linti++)
		  	 { 
		  	    $cnt =$linti+1;
		  	 	//echo "<script>alert(1);</script>";
		  	 	
		  	 	$time = explode('---',$larrschedulervenuetime[$linti]['From']);
		  	 	
		  	 	$fromtime = $time[0];
		  	 	$totime = $time[1];
		  	 	
		  	 	$larrinsertsession['SessionId'] = $sessionID;
		  	 	$larrinsertsession['iddate'] = $cnt;
		  	 	$larrinsertsession['Fromtime'] = $fromtime;
			    $larrinsertsession['Totime'] = $totime;
			    $larrinsertsession['Date'] = $larrschedulervenuetime[$linti]['Date'];
			    $larrinsertsession['idschedulervenuetime'] = $larrschedulervenuetime[$linti]['idschedulervenuetime'];
			 
			  $result =  $db->insert('tbl_tempvenuetimings',$larrinsertsession); 
			  
		  	 }
		  	 
		  	 
	}
	
	
	/*
	 * function to fetch the details from the temp table for editing
	 */
	public function fnfetchalltempdetailsinedit($sessionID,$date)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = "SELECT * 
   	 			FROM tbl_tempvenuetimings 
   	 			WHERE SessionId='$sessionID' AND Date='$date'";
   	 	 $resulttempdetails = $lobjDbAdpt->fetchAll($select);
   	   	 return $resulttempdetails;
	}

}