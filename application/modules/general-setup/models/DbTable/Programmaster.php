<?php 
class GeneralSetup_Model_DbTable_Programmaster extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_programmaster';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetCoursemasterDetails() { //Function to get the user details
        $result = $this->fetchAll();
        return $result;
     }
     
	public function fnGetCourseList(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.ProgramName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnAddGetGradesetList(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_gradesetmaster"),array("key"=>"a.Idgradeset","value"=>"a.Gradesetname"))
				 				 ->where("a.Active = 1")
								 ->where("a.flag!=2")
				 				 ->order("a.Gradesetname");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}

	public function fnShowGradesList($idgrade)
	{
			$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_gradesetmaster"),array(""))
				 				 ->join(array("b"=>"tbl_gradesetdetails"),'a.Idgradeset=b.Idgradeset',array("b.*"))
				 				// ->where("a.Active = 1")
				 				 ->where("a.Idgradeset = ?",$idgrade)
				 				 ->order("b.Percentagefrom");
				 				// echo $lstrSelect;die();
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
	}
	
public function fnGetCourseListEdit($idprog){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName"))
				 				 ->where("a.Active = 1")
				 				 ->where("a.IdProgrammaster != $idprog")
				 				 ->order("a.ProgramName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	} 
    public function fnaddCourse($formData) { //Function for adding the University details to the table
			$this->insert($formData);
	}
    
    public function fnupdateCourse($formData,$lintIdCoursemaster) { //Function for updating the university
    
    	unset ( $formData ['Save'] );
		$where = 'IdProgrammaster = '.$lintIdCoursemaster;
		$this->update($formData,$where);
    }
    
	public function fnSearchCourse($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_programmaster'),array('IdProgrammaster'))
			   ->where('a.ProgramName  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}

	public function fnAddGetProgramList(){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_program"),array("key"=>"a.IdProgram","value"=>"ProgramName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.ProgramName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnEditGetProgramList()
	{
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_program"),array("key"=>"a.IdProgram","value"=>"ProgramName"))
				 				 //->where("a.Active = 1")
				 				 ->order("a.ProgramName");
		$larrResult = $this->lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
}
?>