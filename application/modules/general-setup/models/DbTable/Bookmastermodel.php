<?php
class GeneralSetup_Model_DbTable_Bookmastermodel extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_ebook';
    
	public function fnGetAutherlistSelect()
    {		//Function To Get SELECT BOX OF ACCOUNT MASTER
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" => "tbl_author"),array("key"=>"a.idauthor","value"=>"CONCAT(a.AuthorFirstName,' ',IFNULL(a.AuthorMiddleName,' '),' ',IFNULL(a.AuthorLastName,' '))"));		
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}	
    public function fnInsert($insertData) 
    {	//function for inserting into the account head table
       	$db 	= Zend_Db_Table::getDefaultAdapter();
       	$table 	= "tbl_ebook";
	   	$db->insert($table,$insertData);
	   	/*return $db->lastInsertId("tbl_accounthead","idAccount");*/	   
	}	
	public function fnInsertdate($insertData) 
    {	//function for inserting into the account head table
       	$db 	= Zend_Db_Table::getDefaultAdapter();
       	$table 	= "tbl_date";
	   	$db->insert($table,$insertData);
	   	/*return $db->lastInsertId("tbl_accounthead","idAccount");*/	   
	}	
	Public function fnGetAllCourseNameList()
	{  //function for geting list of group names
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_coursemaster"),array("key"=>"a.IdCoursemaster","value"=>"a.CourseName"));	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}
    Public function fnGetAllprogramNameList()
	{   //function for geting list of religion names
		$db 	= 	Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"));	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}
    Public function fngetbookdetails() 
    {    //getting for grid view 	    		
		$db 	= Zend_Db_Table::getDefaultAdapter();
		$select = 	$db->select()          
             	->	from(array('a' => 'tbl_ebook'),
                   				 array('a.Title','a.idebook'))
            	->	join(array('b' => 'tbl_author'),
                  			    'a.idauthor=b.idauthor',array("CONCAT(b.AuthorFirstName,' ',IFNULL(b.AuthorMiddleName,' '),' ',IFNULL(b.AuthorLastName,' ')) AS author"));		
		$result = $db->fetchAll($select);	
		return $result;
	}
    public function fngetbookSearch($lobjgetarr)
    {
		// search function for the account head	
		$db 	= Zend_Db_Table::getDefaultAdapter();
		if(!$lobjgetarr['field7']) $sqlquery ="  b.Active = 0";
		else $sqlquery	= " b.Active = 1 ";		
		$select = 	$db->select()          
             	->	from(array('a' => 'tbl_ebook'),
                   				 array('a.Title','a.idebook','a.UploadBook'))
                ->	join(array('c' => 'tbl_date'),'a.idebook=c.idebook',array("c.Amount AS Amount"))   				 
            	->	join(array('b' => 'tbl_author'),
                  			    'a.idauthor=b.idauthor',array("CONCAT(b.AuthorFirstName,' ',IFNULL(b.AuthorMiddleName,' '),' ',IFNULL(b.AuthorLastName,' ')) AS author"))
            	-> where('a.Title like  ? "%"',$lobjgetarr['field3'])
            	->where('a.idauthor like  ? "%"',$lobjgetarr['field15'])	
            	-> where($sqlquery);       
		$result = $db->fetchAll($select);	
		return $result;			
	 }
		 
}