<?php
	class GeneralSetup_Model_DbTable_Examtime extends Zend_Db_Table {
		
		
		protected $_name = 'tbl_studentapplication';
	

		
		
	 public function fncheckSuperUserPwd($password)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $lobjDbAdpt->select()
						->from(array("a" => "tbl_user"))
						->join(array('b' => 'tbl_definationms'),'a.IdRole = b.idDefinition')
						->where("a.passwd =?",$password)
						->where("b.DefinitionDesc = 'Superadmin'");
						//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
     
     
	 public function fnSearchCentergracetime($larrformData)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array('a.centername'))
										  	->join(array('b' => 'tbl_gracetime'),'a.idcenter=b.Idvenue',array('b.ExamDate','b.Gracetime','b.Idgracetime'))
										  	->join(array('c' => 'tbl_managesession'),'c.idmangesession=b.Idsession',array('c.managesessionname'));				
				if($larrformData['Venues']) $lstrSelect->where("a.idcenter = ?",$larrformData['Venues']);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     
     
     
     
		  public function newfnGetcitydetailsgetsecid($venue)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_center"),array("a.city"))
						                  ->where("a.Active=1")
										  ->where("a.idcenter =?",$venue); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  	
	   public function fngetschedulerexception($days,$lintcity)
   {
   	  $db 	= 	Zend_Db_Table::getDefaultAdapter();	
	$lstrSelect = $db->select()
	                         ->from(array("a" =>"tbl_schedulerexception"))
	                         ->where('a.Date=?',$days)
	                         ->where('a.idcity=?',$lintcity);
	    $larrResult = $db->fetchRow($lstrSelect);
	    return $larrResult;
   	   
   }
   
     
    

     
     
		 public function fnGetVenueNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
				 				// ->join(array("c"=>"tbl_newschedulervenue"),'c.idvenue=a.idcenter',array(''))	
				 				// ->join(array("e"=>"tbl_newscheduler"),'e.idnewscheduler=c.idnewscheduler',array(''))
				 				// ->where("e.Active =1")			 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	public function newfncurrentgetyear($prog)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect="select `year`as `key` , `year` as `value` from tbl_newscheduler where active=1 group by `year`";
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
}



	
	public function newfnGetCitylistforyearvenues($idyear,$curmonth){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->where("a.Year  = ?",$idyear)
										  ->where("a.To>=?",$curmonth)
										  ->where("a.Active  = 1")
										  ->group("d.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
		
	public function fnnewmonthcaleshowlatest($idvenue,$year)
{
	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
	 $lstrSelect = "SELECT a.*
FROM tbl_newscheduler as a
where  a.Active=1
and a.idnewscheduler in (SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
and a.Year=$year";
	// echo $lstrSelect;die();
	 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}
	
	   
		public function fnnewmonths($values)
{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
 		$lstrSelect = "SELECT min( CAST( `From` AS UNSIGNED ) ) AS minimum, max( CAST( `To` AS UNSIGNED ) ) AS maximum FROM tbl_newscheduler where idnewscheduler in ($values)";
 		
 		//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	
  	return $larrResult;
}


		public function fnGetmonthsbetweenvalid($expiredmonth,$tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >= $expiredmonth and idmonth <='$tomonth'";
		//echo $lselect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	   
	
	 public function fngetcountofsessions($NewCity,$year)
    {
       $db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_newschedulervenue"),array("a.idvenue"))
			         ->join(array("b"=>"tbl_newscheduler"),'a.idnewscheduler=b.idnewscheduler',array(""))
			         ->join(array("c"=>"tbl_newschedulersession"),'b.idnewscheduler=c.idnewscheduler',array("COUNT(c.idmanagesession) as countidmanagesession","c.idnewscheduler"))
			          ->join(array("d"=>"tbl_newschedulerdays"),'a.idnewscheduler=d.idnewscheduler',array("d.*"))
                     ->where("a.idvenue =?",$NewCity)
                     ->where("b.Active =1")
                     ->where("b.Year =?",$year)
                     ->group("d.days"); 
	$result = $db->fetchAll($select);
	return $result;
    }

    
	  public function fngetmonthcalendar($idmonth,$year,$NewCity)
{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect ="SELECT a.*,c.*
FROM tbl_newscheduler as a,tbl_newschedulerdays as c
where  a.idnewscheduler=c.idnewscheduler
and a.Active=1
and a.idnewscheduler in (SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$NewCity)
and a.Year=$year
and a.from<=$idmonth and a.To>=$idmonth";
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
}
	


		public function fnnewmonthcaleshow($idvenue,$year)
{
	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
	 $lstrSelect = "SELECT a.*
FROM tbl_newscheduler as a
where a.Active=1
and a.idnewscheduler in (SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
and a.Year=$year group by a.idnewscheduler";
	// echo $lstrSelect;die();
	  	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}



		 public function  fngetschedulerofdate($ids,$days)
       {
       	
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler"))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler')
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }
       
       
		
 public function fngetdayofdate($iddate)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	  $sql = "select DAYOFWEEK('$iddate') as days"; 
				//echo $sql; die();
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
     public function   fngetschedulerofdatesessions($idscheduler)
     {
     	// ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.idcenter","value"=>"a.centername"))
     	
     	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
										  ->join(array("c"=>"tbl_newschedulersession"),'a.idmangesession=c.idmanagesession')
										  ->where("c.idnewscheduler in ($idscheduler)");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     	 
     }
     
     
     public function fngetgracetimeinsertinfo($larrformData,$iduser)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_gracetime";
		$postData = array(	
							'Idvenue'=>$larrformData['newexamcity'],	
		                    'Idsession'=>$larrformData['examsession'],	
		  					'ExamDate'=>$larrformData['Examdate'],
		  					'Gracetime'=>$larrformData['examtime'],
		                    'UpdUser' =>$iduser,	
		                    'UpdDate' => $larrformData['UpdDate'], 
						);
						$result=  $lobjDbAdpt->insert($table,$postData);
	
						   return $result;
     	
     	
     	
     }
       

   
		
}
