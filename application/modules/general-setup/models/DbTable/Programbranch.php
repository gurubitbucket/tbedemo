<?php 
class GeneralSetup_Model_DbTable_Programbranch extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_programbranchlink';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
	
    public function fnaddProgrambranch($formData) { //Function for adding the Program Branch details to the table
    	 $count = count($formData['IdCollege']);
    	for($i = 0;$i<$count;$i++) {
    		$data = array('IdCollege' => $formData['IdCollege'][$i],
    					  'IdProgram'=> $formData['IdProgram'],
    					  'Active'=> $formData['Active'],
    					  'UpdDate'=> $formData['UpdDate'],
    					  'UpdUser'=> $formData['UpdUser']);
    				$this->insert($data);
    	}	
	}
    
     public function fngetProgrambranchDetails() { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_programbranchlink'),array('IdProgramBranchLink'))
                ->join(array('b'=>'tbl_program'),'a.IdProgram  = b.IdProgram')
                ->join(array('c'=>'tbl_collegemaster'),'a.IdCollege = c.IdCollege')
                ->group('a.IdProgram');
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
     
     public function fnEditProgrambranchDetails($IdProgramBranchLink) { //Function to get the Program Branch details
 		$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_programbranchlink'),array('IdProgramBranchLink'))
                ->join(array('b'=>'tbl_program'),'a.IdProgram  = b.IdProgram')
                ->join(array('c'=>'tbl_collegemaster'),'a.IdCollege = c.IdCollege')
                ->where('a.IdProgramBranchLink = ?',$IdProgramBranchLink);
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
}
?>