<?php
class GeneralSetup_Model_DbTable_Racereportmodel extends Zend_Db_Table { //Model Class for Users Details

	
  public function fnGetbatch()
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 			
	 $lstrSelect="SELECT `a`.`IdBatch` AS `key`, CONCAT(DATE_FORMAT(`a`.`BatchFrom`,'%d-%m-%Y'),'---',DATE_FORMAT(`a`.`BatchTo`,'%d-%m-%Y')) AS `value` FROM `tbl_batchmaster` as a";			
	 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	 return $larrResult;
  }
    
  public function fnracetypes()
  {      
  	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = "SELECT *
                        FROM tbl_definationms
                        WHERE idDefType =13";
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
  }
  
  public function fnGetBatchDetails($batch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								  ->from(array("a" =>"tbl_batchmaster"),array('a.*'))
								  ->join(array("b"=>"tbl_programmaster"),'a.IdProgrammaster=b.IdProgrammaster')
								  ->where("a.IdBatch  = ?",$batch);	
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
  }
	
  public function fngetracedetails($batch)
  {      
  	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = "SELECT count( race ) AS total, race
                        FROM `tbl_studentapplication`
                        WHERE idbatch =$batch
                        GROUP BY `Race`
                        ORDER BY `Race`";
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
  }
  
public function fngetracecount($batch)
  {      
  	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = "SELECT count( race ) AS total
                        FROM `tbl_studentapplication`
                        WHERE idbatch =$batch";                       
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
  }
}