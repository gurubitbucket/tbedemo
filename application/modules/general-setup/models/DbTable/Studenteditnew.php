<?php
class GeneralSetup_Model_DbTable_Studenteditnew extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	

 public function fnviewstudentdetailssss()
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->where("d.RegistrationPin=0000000")
										  ->group("d.IDApplication")
										  ->where("a.pass=3");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
 public function fneditdetails($lintidstudent)
 {
 	
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->join(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->join(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										  ->where("a.payment=1")
										  ->where("a.datetime>'2012-04-14'")
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
     
   /*
      * functin to search the student
      */
	public function fnSearchStudent($post = array()) { //Function for searching the user details
    	//$db = Zend_Db_Table::getDefaultAdapter();
    
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										 ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->where("d.RegistrationPin=0000000")
										  ->group("d.IDApplication")
										  ->where("a.pass=3")	
			   							->where('a.FName like  "%" ? "%"',$post['field3']);
			   //->where('a.MName like "%" ? "%"',$post['field4']);
			   //
			  // ->where($field7);exit;
		$result = $lobjDbAdpt->fetchAll($lstrSelect);
		return $result;
	}
	
	 public function fnAddStudent($larrformData,$idapplication) { //Function to get the user details
   	

   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_studentapplication";
    	 $where = "IDApplication = '".$idapplication."'"; 	
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['FName'],	
           					'DateOfBirth' =>$larrformData['DateOfBirth'],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['EmailAddress'],	
           					'PermAddressDetails' =>$larrformData['PermAddressDetails'],	
           					'Takafuloperator' =>$larrformData['Takafuloperator'], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 0, 
							'Gender' =>$larrformData['Gender'],	
           					'Race' =>$larrformData['Race'],	
           					'Qualification' =>$larrformData['Qualification'], 
							'State' =>$larrformData['State'],	
           					'CorrAddress' =>$larrformData['CorrAddress'],	
           					'PostalCode' =>$larrformData['PostalCode'], 
		 					'ContactNo' =>$larrformData['ContactNo'],	
           					'MobileNo' =>$larrformData['MobileNo'], 
		  							  						);
						 $lobjDbAdpt->update('tbl_studentapplication',$postData,$where);				 
						 
     }
     
     public function mailstudent($lintidstudent)
     {
     		$lobjExamdetailsmodel = new App_Model_Examdetails();	
		require_once('Zend/Mail.php');
		require_once('Zend/Mail/Transport/Smtp.php');
			
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();
	    
		 $larrresult= $db->select()          
             	        ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
						->join(array("c" =>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
						->join(array("d" =>"tbl_center"),'a.Examvenue=d.idcenter',array("d.*"))
						->join(array("e" =>"tbl_managesession"),'a.Examsession=e.idmangesession',array("e.*"))
						->join(array("g" =>"tbl_newscheduler"),'g.idnewscheduler=a.Year',array("g.Year as years"))						
						->join(array("f" =>"tbl_registereddetails"),'f.IDApplication=a.IDApplication',array("f.*"))
            	        ->where('a.IDApplication = ?',$lintidstudent);
            	        
	          $result = $db->fetchRow($larrresult);  
	   //echo "harsha";die();
	  /*  echo "<pre>";	
		print_r($result);die();*/
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $result;

							$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Application");
						    //Get Student's Mailing Details
						   //$larrStudentMailingDetails = $result;					
										
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  'This is to reconfirm your exam schedule. For any clarification please reply to this.</br>'.$larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$result['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$result['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$result['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$result['centername'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$result['Examdate'].'-'.$result['Exammonth'].'-'.$result['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$result['PermAddressDetails'].'-'.$result['CorrAddress'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$result['managesessionname'].'('.$result['starttime'].'--'.$result['endtime'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$result['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$result["EmailAddress"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$result['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$result['Amount'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
								
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										try{
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										}
										catch(Exception $e) {
											echo '<script language="javascript">alert("Unabsadfasfsa")</script>';
											
										}
										$mail = new Zend_Mail();
									try{
										
									
										$mail->setBodyHtml($lstrEmailTemplateBody);
									}
									catch(Exception $e){
											echo '<script language="javascript">alert("aaaaaaaaaaaaaa")</script>';
									}
									
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email =$result["EmailAddress"];
										$receiver = $result['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
											 ->addBcc('itwinecall@gmail.com','itwine')
									         ->setSubject($lstrEmailTemplateSubject);
									        // $result = $mail->send($transport);
											 try {
									$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
										//$result = $mail->send($transport);
										
								   //  $this->view->mess .= $lstrEmailTemplateBody;							
						
			

     }
     
     public function fnsendbulkmails($larrformdata)
     {
     	
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();

     	for($i=0;$i<count($larrformdata['IDApplication']);$i++)
		{
			
			self::mailstudent($larrformdata['IDApplication'][$i]);
			
		}
     }
     
 
}