<?php
class GeneralSetup_Form_Accountheadentry extends Zend_Dojo_Form 
{		
    public function init()
    { 
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
        $UpdDate 		= 	new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate		->	removeDecorator("DtDdWrapper");
        $UpdDate		->	removeDecorator("Label");
        $UpdDate		->	removeDecorator('HtmlTag');
     
        $UpdUser 		= 	new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser		->	removeDecorator("DtDdWrapper");
        $UpdUser		->	removeDecorator("Label");
        $UpdUser		->	removeDecorator('HtmlTag');                    
         
        $idAccount	 	= 	new Zend_Dojo_Form_Element_FilteringSelect('idAccount');
       	//$idAccount		->	addMultiOption('', '');
        $idAccount		->	removeDecorator("DtDdWrapper")
						->	removeDecorator("Label") 
						->	setAttrib('required',"true") 	
						->setRegisterInArrayValidator(false)			
						->	removeDecorator('HtmlTag')
						->  setAttrib('dojoType',"dijit.form.FilteringSelect");
						
						
		$discounttype	 = 	new Zend_Dojo_Form_Element_FilteringSelect('discounttype');
        $discounttype   ->	removeDecorator("DtDdWrapper")
						->	removeDecorator("Label") 	
						->	addMultiOption('0', 'Select')		
						->	removeDecorator('HtmlTag')
						->  setAttrib('dojoType',"dijit.form.FilteringSelect");
						
        $effectiveDate	=   new Zend_Form_Element_Text('effectiveDate');
        $effectiveDate  ->  setAttrib('dojoType',"dijit.form.DateTextBox");  
        $effectiveDate	->	removeDecorator("DtDdWrapper");
        $effectiveDate	->	removeDecorator("Label");
        $effectiveDate	->	removeDecorator('HtmlTag');
        $effectiveDate	 ->	setAttrib('required',"true") 
        				//->	addValidator(new Zend_Validate_Date('dd-mm-yy'),true)
        				//->	setJQueryParam('dateFormat', 'dd-mm-yy')
        				//->	setAttrib('style','width:155px;')
               			//->	setAttrib('class', 'txt_put')   
               		    ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        

        $Amount	= new Zend_Form_Element_Text('Amount',array('regExp'=>"[0-9]*[.]?[0-9]+",'invalidMessage'=>"Only Amount"));
        $Amount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount->removeDecorator("DtDdWrapper");
        $Amount->removeDecorator("Label");
        $Amount->removeDecorator('HtmlTag');
        $Amount->setAttrib('maxlength','4')
        		->setAttrib('onKeyPress','return fnNumericInputOnly(this.value,event)')
        		//->	setAttrib('class', 'txt_put')
        	    ->	setAttrib('required',"true") ;	    

        $Active 		= 	new Zend_Form_Element_Checkbox('Active');
        //$Active			->	setAttrib('onClick','ToggleSelectBoxs()');
        $Active			->  setAttrib('dojoType',"dijit.form.CheckBox");     
        $Active			->	removeDecorator("DtDdWrapper");
        $Active			->	removeDecorator("Label");
        $Active			->	removeDecorator('HtmlTag')
        				->	setValue("1");		     
        				
        					
		 $discount = new Zend_Form_Element_Hidden('Discount');
	      $discount->removeDecorator("DtDdWrapper");
	      $discount->removeDecorator("Label");
	      $discount->removeDecorator('HtmlTag')
        			 ->setValue("1");
        			 
        			 
        $minnostd	= new Zend_Form_Element_Text('minnostd',array('regExp'=>"[0-9]*",'invalidMessage'=>"Only Numbers"));
        $minnostd->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $minnostd->removeDecorator("DtDdWrapper");
        $minnostd->removeDecorator("Label");
        $minnostd->removeDecorator('HtmlTag');
        $minnostd->setAttrib('maxlength','10')
        		->setAttrib('onKeyPress','return fnNumericInputOnly(this.value,event)');
        		//->	setAttrib('class', 'txt_put')
        	   // ->	setAttrib('required',"true") ;
    
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        
        $Close 			= 	new Zend_Form_Element_Submit('Close');
		$Close			->	setAttrib('class', 'buttonStyle')
						->	setAttrib('onclick', 'fnCloseLyteBox()')
						->	removeDecorator("Label")
						->	removeDecorator("DtDdWrapper")
						->	removeDecorator('HtmlTag');
   		$this->addElements(array($UpdDate,$UpdUser,$idAccount,$effectiveDate,$Amount,$discounttype,$minnostd,$Active,$discount,$Save,$Close));
    }
}
        
        