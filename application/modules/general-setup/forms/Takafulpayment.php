<?php

class GeneralSetup_Form_Takafulpayment extends Zend_Dojo_Form 
{		
    public function init()
    { 
       						
        $TakafulName	=   new Zend_Form_Element_Text('TakafulName',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
        $TakafulName  ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $TakafulName	->	removeDecorator("DtDdWrapper");
        $TakafulName	->	removeDecorator("Label");
        $TakafulName	->	removeDecorator('HtmlTag');
        $TakafulName	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');         
        
               			
        $Search 		= 	new Zend_Form_Element_Submit('Search');
        $Search			->	setAttrib('class','NormalBtn');
        $Search			->	setAttrib('id', 'submitbutton');
        $Search			->	removeDecorator("DtDdWrapper");
        $Search			->	removeDecorator("Label");
        $Search			->	removeDecorator('HtmlTag');
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = ("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";
        
        $ChequeDate = new Zend_Dojo_Form_Element_DateTextBox('ChequeDt');
        $ChequeDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$ChequeDate->setAttrib('required',"true");
        $ChequeDate->removeDecorator("DtDdWrapper");
        $ChequeDate->setAttrib('title',"dd-mm-yyyy");
        $ChequeDate->removeDecorator("Label");
        $ChequeDate->removeDecorator('HtmlTag'); 
               			
        $ChequeNo = new Zend_Form_Element_Text('ChequeNo');
        $ChequeNo  ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ChequeNo	->	removeDecorator("DtDdWrapper");
        $ChequeNo	->	removeDecorator("Label")
                    ->setAttrib('required',"true");
        $ChequeNo	->	removeDecorator('HtmlTag');
        $ChequeNo	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');

        $BankName = new Zend_Form_Element_Text('BankName');
        $BankName  ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $BankName	->	removeDecorator("DtDdWrapper");
        $BankName	->	removeDecorator("Label")
                    ->setAttrib('required',"true");
        $BankName	->	removeDecorator('HtmlTag');
        $BankName	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');      			
               			
               			
         
        $Amount  = new Zend_Form_Element_Text('Amount');
        $Amount  -> setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount	 ->	removeDecorator("DtDdWrapper");
        $Amount	 ->	removeDecorator("Label")
                 -> setAttrib('required',"true");
        $Amount	 ->	removeDecorator('HtmlTag');
        $Amount	 ->	setAttrib('style','width:155px;')
               	 ->	setAttrib('class', 'txt_put')
               	  -> setAttrib('readonly',true); 
               	    			
   		$this->addElements(array($TakafulName,$ChequeNo,$BankName,$Search,$ChequeDate,$Save,$Amount));
    }
}
        
        