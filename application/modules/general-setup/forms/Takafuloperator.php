<?php
class GeneralSetup_Form_Takafuloperator extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$idtakafuloperator= new Zend_Form_Element_Hidden('idtakafuloperator');
        $idtakafuloperator->removeDecorator("DtDdWrapper");
        $idtakafuloperator->removeDecorator("Label");
        $idtakafuloperator->removeDecorator('HtmlTag');
        
        $TakafulName = new Zend_Form_Element_Text('TakafulName');	
		$TakafulName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $TakafulName->setAttrib('required',"true") 
                ->setAttrib('OnChange','duplicatetakafulname(this.value)')      			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        $ContactName = new Zend_Form_Element_Text('ContactName');	
		$ContactName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ContactName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');		
        		
        $ContactCell = new Zend_Form_Element_Text('ContactCell',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$ContactCell	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$ContactCell  	->setAttrib('maxlength','20')
		                ->setAttrib('required',"true")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');

		$ContactEmail = new Zend_Form_Element_Text('ContactEmail',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
        $ContactEmail->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ContactEmail  	->setAttrib('maxlength','50')
						->removeDecorator("Label")
						->setAttrib('required',"true")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
								
        $TakafulShortName = new Zend_Form_Element_Text('TakafulShortName');	
		$TakafulShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $TakafulShortName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');   

 		$paddr1 = new Zend_Form_Element_Text('paddr1');
		$paddr1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $paddr1->setAttrib('required',"true")       			      
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                   
        $paddr2 = new Zend_Form_Element_Text('paddr2');
		$paddr2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $paddr2->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        
        $city = new Zend_Dojo_Form_Element_FilteringSelect('city');
		$city->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $city->setAttrib('maxlength','20')     
               ->setAttrib('required',"true") 		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        $city->setRegisterInArrayValidator(false);
        $city->setAttrib('dojoType',"dijit.form.FilteringSelect");
        		
        $NumberofSeat = new Zend_Form_Element_Hidden('NumberofSeat',array('regExp'=>"[0-9]*",'invalidMessage'=>"Digits Only"));
        $NumberofSeat->setAttrib('maxlength','20') 
               ->setValue('100000000')    
               ->setAttrib('required',"true") 		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        
        $country = new Zend_Dojo_Form_Element_FilteringSelect('country');
        $country->removeDecorator("DtDdWrapper");
        $country->setAttrib('required',"true") ;
        $country->removeDecorator("Label");
        $country->removeDecorator('HtmlTag');
        $country->setAttrib('OnChange', 'fnGetCountryStateList');
        $country->setRegisterInArrayValidator(false);
		$country->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
       
        $state = new Zend_Dojo_Form_Element_FilteringSelect('state');
        $state->removeDecorator("DtDdWrapper");
        $state->setAttrib('required',"true") ;
        $state->removeDecorator("Label");
        $state->removeDecorator('HtmlTag');
        $state->setRegisterInArrayValidator(false);
		$state->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $state->setAttrib('OnChange', 'fnGetCityList'); 
              
        
        $zipCode = new Zend_Form_Element_Text('zipCode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$zipCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $zipCode->setAttrib('maxlength','5');   
        $zipCode->removeDecorator("DtDdWrapper");
        $zipCode->removeDecorator("Label");
        $zipCode->removeDecorator('HtmlTag');
        
        $workcountrycode = new Zend_Form_Element_Text('workcountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$workcountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workcountrycode->setAttrib('maxlength','3');  
        $workcountrycode->setAttrib('style','width:30px');  
        $workcountrycode->removeDecorator("DtDdWrapper");
        $workcountrycode->removeDecorator("Label");
        $workcountrycode->removeDecorator('HtmlTag');
        
        $workstatecode = new Zend_Form_Element_Text('workstatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$workstatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workstatecode->setAttrib('maxlength','5');  
        $workstatecode->setAttrib('style','width:30px');  
        $workstatecode->removeDecorator("DtDdWrapper");
        $workstatecode->removeDecorator("Label");
        $workstatecode->removeDecorator('HtmlTag');
        
        
 		$workPhone = new Zend_Form_Element_Text('workphone',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Work Phone No."));
		$workPhone->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workPhone->setAttrib('maxlength','20');   
        $workPhone->setAttrib('style','width:93px');  
        $workPhone->removeDecorator("DtDdWrapper");
        $workPhone->removeDecorator("Label");
        $workPhone->removeDecorator('HtmlTag');     

        $email = new Zend_Form_Element_Text('email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $email->setAttrib('required',"true")  			 
        		->setAttrib('maxlength','50')  
        		->setAttrib('onBlur','setlogin(this.value)')       		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');        
        		
        		
       	$TakafulField1 = new Zend_Form_Element_Text('TakafulField1');
		$TakafulField1->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        				
       	$TakafulField2 = new Zend_Form_Element_Text('TakafulField2');
		$TakafulField2->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$TakafulField3 = new Zend_Form_Element_Text('TakafulField3');
		$TakafulField3->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$TakafulField4 = new Zend_Form_Element_Text('TakafulField4');
		$TakafulField4->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$TakafulField5 = new Zend_Form_Element_Text('TakafulField5');
		$TakafulField5->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$TakafulField6 = new Zend_Form_Element_Text('TakafulField6');
		$TakafulField6->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$TakafulField7 = new Zend_Form_Element_Text('TakafulField7');
		$TakafulField7->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$TakafulField8 = new Zend_Form_Element_Text('TakafulField8');
		$TakafulField8->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$TakafulField9 = new Zend_Form_Element_Text('TakafulField9');
		$TakafulField9->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

        				
        $TakafulField10 = new Zend_Form_Element_Text('TakafulField10');
		$TakafulField10->setAttrib('dojoType',"dijit.form.TextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        				

       	$LoginId = new Zend_Form_Element_Text('LoginId');
		$LoginId->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','20')  
        				->setAttrib('OnChange', 'duplicatetakafull(this.value)')
        				//->setAttrib('readonly','readonly')
        				->setAttrib('required',"true")     
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

        				
        $Password = new Zend_Form_Element_Password('Password');
		$Password->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','20')  
        				->setAttrib('required',"true")     
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');  

        $NewPassword = new Zend_Form_Element_Password('NewPassword');
		$NewPassword->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','20')  
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        				  
        $RetypePassword = new Zend_Form_Element_Password('$RetypePassword');
		$RetypePassword->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','20')  
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag'); 


   				
       $Active1 		= 	new Zend_Form_Element_Checkbox('Active');
        //$Active			->	setAttrib('onClick','ToggleSelectBoxs()');
        $Active1        ->  setAttrib('dojoType',"dijit.form.CheckBox");
       // $Active1		->	setAttrib("checked","checked");
        $Active1		->	removeDecorator("DtDdWrapper");
        $Active1		->	removeDecorator("Label");
        $Active1		->	removeDecorator('HtmlTag');	


        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');   

		$Takafultype= new Zend_Dojo_Form_Element_FilteringSelect('Takafultype');
        $Takafultype	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('1'=>'Bank','2'=>'Takaful Operator'))	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');

        $Businesstype = new Zend_Dojo_Form_Element_FilteringSelect('Businesstype');
        $Businesstype->removeDecorator("DtDdWrapper");
        $Businesstype->setAttrib('required',"true") ;
        $Businesstype->removeDecorator("Label");
        $Businesstype->removeDecorator('HtmlTag');
		$Businesstype->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		
		
		
		////////////////search form			
        $TakafulId= new Zend_Dojo_Form_Element_FilteringSelect('TakafulId');
        $TakafulId	->setAttrib('dojoType',"dijit.form.FilteringSelect")
        			->removeDecorator("DtDdWrapper") 
					->removeDecorator("Label") 				
					->removeDecorator('HtmlTag');
        				
      	$Takafuldrop = new Zend_Form_Element_Text('Takafuldrop');
      	$Takafuldrop->setAttrib('dojoType',"dijit.form.TextBox");
      	$Takafuldrop->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $TakafulSN = new Zend_Form_Element_Text('TakafulSN');	
		$TakafulSN->setAttrib('dojoType',"dijit.form.ValidationTextBox")  			        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        $submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
        				
		
       
    
        $this->addElements(array($idtakafuloperator,$TakafulName,$TakafulShortName,$TakafulId,$Takafuldrop,$submit,$Clear,$TakafulSN,
        							$paddr1,$paddr2,$city,$NumberofSeat,$country,$state,$zipCode,$workcountrycode,$workstatecode,$workPhone,			
        	$TakafulField1,$TakafulField2,$TakafulField3,$TakafulField4,$email,
        						$TakafulField5,$TakafulField6,$TakafulField7,$TakafulField8,$TakafulField9,$TakafulField10,
                                 $UpdDate,$Active1,
                                 $UpdUser,
                                 $Save,$ContactName,$ContactCell,$ContactEmail,
                                 $Back,$LoginId,$Password,$Takafultype,$Businesstype,$NewPassword,$RetypePassword));

    }
}