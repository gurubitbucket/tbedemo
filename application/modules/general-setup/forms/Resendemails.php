<?php
class GeneralSetup_Form_Resendemails extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    

    	    $Emailtypes= new Zend_Dojo_Form_Element_FilteringSelect('Emailtypes');
        $Emailtypes->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('1'=>'Registration','2'=>'Change Venue/Date','3'=>'Results'))
							->setRegisterInArrayValidator(false)
							//->setAttrib('onChange','getthestudents(this.value);')
							->setAttrib('onChange','getthestudents(this.value),fndisplayresen(this.value)')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
							
    	    $Regtype= new Zend_Dojo_Form_Element_FilteringSelect('Regtype');
        $Regtype->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array(''=>'All','1'=>'Individual','2'=>'Company Registration','3'=>'Takaful Registration'))
							->setRegisterInArrayValidator(false)
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
														
							
	$ICNO = new Zend_Form_Element_Text('ICNO',array());			
        $ICNO->setAttrib('dojoType',"dijit.form.TextBox");
        $ICNO->setAttrib('class', 'txt_put') 
        	  ->setAttrib('maxlength','250')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag'); 

	        		
	    $Regdate = new Zend_Dojo_Form_Element_DateTextBox('Regdate');
        $Regdate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $Regdate->setAttrib('class', 'txt_put');
        $Regdate->removeDecorator("DtDdWrapper")
        	    ->setAttrib('title',"dd-mm-yyyy");
        $Regdate->removeDecorator("Label");
        $Regdate->removeDecorator('HtmlTag');
        $Regdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        
        
        $Examdate = new Zend_Dojo_Form_Element_DateTextBox('Examdate');
        $Examdate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $Examdate->setAttrib('class', 'txt_put');
        $Examdate->removeDecorator("DtDdWrapper")
        	    ->setAttrib('title',"dd-mm-yyyy");
        $Examdate->removeDecorator("Label");
        $Examdate->removeDecorator('HtmlTag');
        $Examdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        
        $Changedate = new Zend_Dojo_Form_Element_DateTextBox('Changedate');
        $Changedate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $Changedate->setAttrib('class', 'txt_put');
        $Changedate->removeDecorator("DtDdWrapper")
        	    ->setAttrib('title',"dd-mm-yyyy");
        $Changedate->removeDecorator("Label");
        $Changedate->removeDecorator('HtmlTag');
        $Changedate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}"); 

        $Changeexamdate = new Zend_Dojo_Form_Element_DateTextBox('Changeexamdate');
        $Changeexamdate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $Changeexamdate->setAttrib('class', 'txt_put');
        $Changeexamdate->removeDecorator("DtDdWrapper")
        	    ->setAttrib('title',"dd-mm-yyyy");
        $Changeexamdate->removeDecorator("Label");
        $Changeexamdate->removeDecorator('HtmlTag');
        $Changeexamdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");   






    	    $VenueTypes= new Zend_Dojo_Form_Element_FilteringSelect('VenueTypes');
        $VenueTypes->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->setRegisterInArrayValidator(false)
							->setAttrib('onChange','getthestudents(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');



 $SessionTypes= new Zend_Dojo_Form_Element_FilteringSelect('SessionTypes');
        $SessionTypes->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->setRegisterInArrayValidator(false)
							->setAttrib('onChange','getthestudents(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');

       

       
        			
        			
		$FName = new Zend_Form_Element_Text('FName',array());
        $FName->setAttrib('dojoType',"dijit.form.TextBox");
        $FName->setAttrib('class', 'txt_put')   			 
	        		->setAttrib('maxlength','250') 
	        		->setAttrib('propercase','true')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');								
      
        
 
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
        	$Clear->dojotype="dijit.form.Button";
        	 	$Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
     $Search = new Zend_Form_Element_Submit('Search');
        	$Search->dojotype="dijit.form.Button";
        	 	$Search->label = $gstrtranslate->_("Search");
		$Search->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');				
				
		$Close = new Zend_Form_Element_Button('Close');
		$Close->dojotype="dijit.form.Button";
       	$Close->label = $gstrtranslate->_("Back");
		$Close->setAttrib('class', 'NormalBtn')				
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
	
        $resendmailtype= new Zend_Dojo_Form_Element_FilteringSelect('resendmailtype');
        $resendmailtype->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('1'=>'Resend to ALL','2'=>'Only for not Sent'))
							->setRegisterInArrayValidator(false)
							//->setAttrib('onChange','getthestudents(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
  
        
    
        //form elements
        $this->addElements(array($Save,$Emailtypes,$ICNO,$FName,$Search,$Regdate,$Examdate,$Changedate,$Changeexamdate,$Regtype,$VenueTypes,$SessionTypes,
                                 $Clear,$Close,$Add,$resendmailtype
                                 ));

    }
}