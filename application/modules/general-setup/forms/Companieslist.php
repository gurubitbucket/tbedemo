<?php
class GeneralSetup_Form_Companieslist extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$Update = new Zend_Form_Element_Hidden('upddate');
        $Update	->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');
        		 	 
		$UpdUser = new Zend_Form_Element_Hidden('upduser');
		$UpdUser->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
				 	->removeDecorator('HtmlTag');

		$idcompanypaymenttype = new Zend_Form_Element_Hidden('idcompanypaymenttype');
		$idcompanypaymenttype	->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
		
		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = ("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";

        	 

        $idcompany = new Zend_Dojo_Form_Element_FilteringSelect('idcompany');       
        $idcompany->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $idcompany->removeDecorator("DtDdWrapper");
        $idcompany->removeDecorator("Label")
                    ->setAttrib('required',"true")                   
       				->removeDecorator('HtmlTag')
               		->setAttrib('class', 'txt_put');     

        $paymenttype = new Zend_Dojo_Form_Element_FilteringSelect('paymenttype');       
        $paymenttype->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $paymenttype->removeDecorator("DtDdWrapper");
        $paymenttype->removeDecorator("Label")
                    ->setAttrib('required',"true")                   
       				->removeDecorator('HtmlTag')
               		->setAttrib('class', 'txt_put');               		

               	    			
   		$this->addElements(array($idcompany,$paymenttype,$Save,$idcompanypaymenttype,$UpdUser,$Update));

    }
}
