<?php
class GeneralSetup_Form_Studentedit extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	//$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    
		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		//echo $month;
		$minmumage=new App_Model_Studentapplication();
		//echo "<pre />";
				$larr=$minmumage->fngetminimumage();
				//print_r($larr);
				$age=$larr[0]['MinAge'];
				$eligibility = ($year)-($age);
//				echo $eligibility;die();
				
			$year=$eligibility;	
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));

		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 

	    
		    $Update = new Zend_Form_Element_Hidden('UpdDate');
        	$Update	->removeDecorator("DtDdWrapper")
        			//->setvalue($strSystemDate)
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');
        		 	 
			$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
			$UpdUser->setAttrib('id','UpdUser')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
				 	->removeDecorator('HtmlTag');

			$IDApplication = new Zend_Form_Element_Hidden('IDApplication');
			$IDApplication	->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
        
		$FName = new Zend_Form_Element_Text('FName',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
        $FName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $FName->setAttrib('class', 'txt_put') ;
        $FName->setAttrib('required',"true")       			 
	        		->setAttrib('maxlength','250') 
	        		->setAttrib('propercase','true')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');			

			//$MName = new Zend_Form_Element_Text('MName');
		$MName = new Zend_Form_Element_Text('MName',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
        $MName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $MName->setAttrib('class', 'txt_put') ;
        $MName->setAttrib('maxlength','250')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');
										
			//$LName = new Zend_Form_Element_Text('LName');
		$LName = new Zend_Form_Element_Text('LName',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));			
        $LName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $LName->setAttrib('class', 'txt_put');
        $LName ->setAttrib('maxlength','250')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');
					
					
			//$DateOfBirth = new ZendX_JQuery_Form_Element_DatePicker('DateOfBirth');
        $DateOfBirth = new Zend_Dojo_Form_Element_DateTextBox('DateOfBirth');
        $DateOfBirth -> setAttrib('dojoType',"dijit.form.DateTextBox");
        $DateOfBirth -> setAttrib('constraints', $dateofbirth);
        $DateOfBirth -> setAttrib('class', 'txt_put');
		$DateOfBirth -> setAttrib('required',"true");
		$DateOfBirth ->setAttrib('OnClick' , 'fnsetdate');
        $DateOfBirth -> removeDecorator("DtDdWrapper");
        $DateOfBirth -> removeDecorator("Label");
        $DateOfBirth -> removeDecorator('HtmlTag')
        			->setAttrib('required',"true"); 
        			
        $ContactNo = new Zend_Form_Element_Text('ContactNo',array('regExp'=>"[+-]?[0-9-]*",'invalidMessage'=>"Digits Only"));
        $ContactNo->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ContactNo->setAttrib('class', 'txt_put') 
       			 -> setAttrib('required',"true");
        $ContactNo->setAttrib('maxlength','15')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');		
	        		
	    $MobileNo = new Zend_Form_Element_Text('MobileNo',array('regExp'=>"[+-]?[0-9-+]*",'invalidMessage'=>"Digits Only"));
        $MobileNo->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $MobileNo->setAttrib('class', 'txt_put') ;
        $MobileNo->setAttrib('maxlength','10')  
                   -> setAttrib('required',"true")   
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');
						
	  $Qualification= new Zend_Dojo_Form_Element_FilteringSelect('Qualification');
      $Qualification->setAttrib('dojoType',"dijit.form.FilteringSelect");
	  $Qualification	->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag')
			            ->setAttrib('required',"true");				
            
			
			
			$Gender  = new Zend_Form_Element_Radio('Gender');
        	$Gender		->addMultiOptions(array(
									'1' => 'Male',
									'0' => 'Female'))
        				->setvalue('1')
       				->setAttrib('dojoType',"dijit.form.RadioButton")     
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');
	        		
			$Race = new Zend_Dojo_Form_Element_FilteringSelect('Race');
        	$Race->setAttrib('dojoType',"dijit.form.FilteringSelect")	
							->removeDecorator("DtDdWrapper")
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
								        		
        /*				
			$MaritalStatus = new Zend_Dojo_Form_Element_FilteringSelect('MaritalStatus');
        	$MaritalStatus	->addMultiOptions(array('0'=>'Married',
        											'1'=>'Un Married'))        	        	
							//->setAttrib('class','txt_put MakeEditable')	
							->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							//->setAttrib('OnChange', 'fnGetValueOfMaritalStatus')
							//->setAttrib('style','width:150px')
						//	->addMultiOptions($larrMaritalStatus)					
							->removeDecorator("DtDdWrapper")
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
							
			$Nationality = new Zend_Form_Element_Text('Nationality',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
			$Nationality->setAttrib('maxlength','50')
						->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('class','txt_put')
						->setAttrib('required',"true")					
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 					
						->removeDecorator('HtmlTag');*/
						
																			
			$PermAddressDetails = new Zend_Form_Element_Text('PermAddressDetails',array());
			$PermAddressDetails	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
								->setAttrib('class','txt_put')
								//->setAttrib('required',"true")
								->setAttrib('maxlength','100')
								->removeDecorator("DtDdWrapper")
								->removeDecorator("Label") 					
								->removeDecorator('HtmlTag');
		
			$PermCity = new Zend_Form_Element_Text('PermCity',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
			$PermCity	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','50')
						//->setAttrib('required',"true")
						->setAttrib('class','txt_put')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
					
			$Takafuloperator = new Zend_Dojo_Form_Element_FilteringSelect('Takafuloperator');
        	$Takafuloperator->addMultiOptions(array())
        				->setAttrib('required',"true")
						->setAttrib('dojoType',"dijit.form.FilteringSelect")
						->setAttrib('OnChange', 'fnValidateTakafulOperator(this.value)')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
		/*			
			$PermCountry = new Zend_Dojo_Form_Element_FilteringSelect('PermCountry');
	        $PermCountry->setAttrib('maxlength','250');
	        $PermCountry->removeDecorator("DtDdWrapper");
	        $PermCountry->setAttrib('required',"true") ;
	        $PermCountry->removeDecorator("Label");
	        $PermCountry->removeDecorator('HtmlTag');
	        $PermCountry->setAttrib('OnChange','fnGetPermCountryStateList');
	        $PermCountry->setRegisterInArrayValidator(false);
			$PermCountry->setAttrib('dojoType',"dijit.form.FilteringSelect") ;
												
			//$PermZip = new Zend_Form_Element_Text('PermZip',array());
			$PermZip = new Zend_Form_Element_Text('PermZip',array('regExp'=>"[0-9]+",'invalidMessage'=>"Digits Only"));
			$PermZip	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setAttrib('class','txt_put')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
																
$CorrsAddressDetails = new Zend_Form_Element_Text('CorrsAddressDetails',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
			$CorrsAddressDetails->setAttrib('dojoType',"dijit.form.ValidationTextBox")			
						->setAttrib('maxlength','100')
						->setAttrib('class','txt_put')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
		
			$CorrsCity = new Zend_Form_Element_Text('CorrsCity',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
			$CorrsCity	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','100')
						->setAttrib('class','txt_put')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
					
		
					
			$CorrsCountry = new Zend_Dojo_Form_Element_FilteringSelect('CorrsCountry');
			$CorrsCountry->setAttrib('maxlength','250');
	        $CorrsCountry->removeDecorator("DtDdWrapper");
	        $CorrsCountry->removeDecorator("Label");
	        $CorrsCountry->removeDecorator('HtmlTag');
	        $CorrsCountry->setAttrib('OnChange','fnGetCorrsCountryStateList');
	        $CorrsCountry->setRegisterInArrayValidator(false);
			$CorrsCountry->setAttrib('dojoType',"dijit.form.FilteringSelect");
												
			$CorrsZip = new Zend_Form_Element_Text('CorrsZip',array('regExp'=>"[0-9]+",'invalidMessage'=>"Digits Only"));
			$CorrsZip	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setAttrib('class','txt_put')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
						*/
						
		  /*  $homecountrycode = new Zend_Form_Element_Text('homecountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		    $homecountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
            $homecountrycode->setAttrib('maxlength','3');  
            $homecountrycode->setAttrib('style','width:30px');  
            $homecountrycode->removeDecorator("DtDdWrapper");
            $homecountrycode->removeDecorator("Label");
            $homecountrycode->removeDecorator('HtmlTag');
        
            $homestatecode = new Zend_Form_Element_Text('homestatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
			$homestatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
       	 	$homestatecode->setAttrib('maxlength','5');  
        	$homestatecode->setAttrib('style','width:30px');  
        	$homestatecode->removeDecorator("DtDdWrapper");
        	$homestatecode->removeDecorator("Label");
        	$homestatecode->removeDecorator('HtmlTag');

			//$HomePhone = new Zend_Form_Element_Text('HomePhone');
			$HomePhone = new Zend_Form_Element_Text('HomePhone',array('regExp'=>"[0-9]+",'invalidMessage'=>"Digits Only"));
			$HomePhone	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setAttrib('style','width:93px')
						->setAttrib('class','txt_put')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
						
						
						
						
						
			$countrycode = new Zend_Form_Element_Text('countrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
			$countrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
       	 	$countrycode->setAttrib('maxlength','3');  
        	$countrycode->setAttrib('style','width:30px');  
        	$countrycode->removeDecorator("DtDdWrapper");
        	$countrycode->removeDecorator("Label");
        	$countrycode->removeDecorator('HtmlTag');
        
        	$statecode = new Zend_Form_Element_Text('statecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
			$statecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        	$statecode->setAttrib('maxlength','5');  
        	$statecode->setAttrib('style','width:30px');  
        	$statecode->removeDecorator("DtDdWrapper");
        	$statecode->removeDecorator("Label");
       		$statecode->removeDecorator('HtmlTag');
        */
						
			$NewState = new Zend_Dojo_Form_Element_FilteringSelect('NewState');
			$NewState->setAttrib('maxlength','250');
	        $NewState->removeDecorator("DtDdWrapper");
	        $NewState->removeDecorator("Label");
	        $NewState->removeDecorator('HtmlTag');
	        $NewState->setAttrib('OnChange','fnGetCitylist(this.value)');
	        $NewState->setRegisterInArrayValidator(false);
			$NewState->setAttrib('dojoType',"dijit.form.FilteringSelect");	

			$NewCity = new Zend_Dojo_Form_Element_FilteringSelect('NewCity');
			$NewCity->setAttrib('maxlength','250');
	        $NewCity->removeDecorator("DtDdWrapper");
	        $NewCity->removeDecorator("Label");
	        $NewCity->removeDecorator('HtmlTag');
	          $NewCity->setAttrib('OnChange','fndisplaycalender(this.value)');
			$NewCity->setAttrib('dojoType',"dijit.form.FilteringSelect");	
        
			$State = new Zend_Dojo_Form_Element_FilteringSelect('State');
        	$State->addMultiOptions(array('' => 'Select'))
						->setAttrib('dojoType',"dijit.form.FilteringSelect")
						->removeDecorator("DtDdWrapper")
						->setAttrib('required',"true")  
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
						
			$PostalCode = new Zend_Form_Element_Text('PostalCode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Digits Only"));
			$PostalCode	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','5')
						->setAttrib('required',"true")  
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');	

			$CorrAddress = new Zend_Form_Element_Text('CorrAddress',array());
			$CorrAddress->setAttrib('dojoType',"dijit.form.TextBox")
								->setAttrib('maxlength','250')
								->removeDecorator("DtDdWrapper")
								->removeDecorator("Label") 					
								->removeDecorator('HtmlTag');						
        
			//$CellPhone = new Zend_Form_Element_Text('CellPhone');
			$CellPhone = new Zend_Form_Element_Text('CellPhone',array('regExp'=>"[0-9]+",'invalidMessage'=>"Digits Only"));
			$CellPhone	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setAttrib('class','txt_put')
						->setAttrib('style','width:93px')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
						
						
			//$Fax = new Zend_Form_Element_Text('Fax');
			$Fax = new Zend_Form_Element_Text('Fax',array('regExp'=>"[0-9]+",'invalidMessage'=>"Digits Only"));
			$Fax	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setAttrib('class','txt_put')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');
						
			//$EmailAddress = new Zend_Form_Element_Text('EmailAddress');
        $EmailAddress = new Zend_Form_Element_Text('EmailAddress',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$EmailAddress->setAttrib('dojoType',"dijit.form.ValidationTextBox");
                $EmailAddress->setAttrib('class', 'txt_put')     			 
        		->setAttrib('maxlength','50') 
                                       ->setAttrib('onBlur','fngetEmaildetails(this.value);')  
        		->setAttrib('required',"true") 	      		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');


        $IdBatch = new Zend_Dojo_Form_Element_FilteringSelect('IdBatch');
        $IdBatch->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->setAttrib('onChange','fngetschedulerdetails(this.value);');
								        $IdBatch->setAttrib('required',"true") 
								        ->setRegisterInArrayValidator(false)				
							->removeDecorator("DtDdWrapper")
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
							        		
        $idschedulermaster = new Zend_Dojo_Form_Element_FilteringSelect('idschedulermaster');
        $idschedulermaster	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->setAttrib('onChange','fngetvenuedetails(this.value);') 				
							->removeDecorator("DtDdWrapper")
							->setRegisterInArrayValidator(false)
								->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');	
							
        $DateTime = new Zend_Dojo_Form_Element_FilteringSelect('DateTime');
        $DateTime	->setAttrib('dojoType',"dijit.form.FilteringSelect")										
							->removeDecorator("DtDdWrapper")
							->setRegisterInArrayValidator(false)
								->setAttrib('required',"true") 	 
							->setAttrib('onChange','fngettimefordatevalidation(this.value)')
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');							
							

        $Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');
        $Venue	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
								->setAttrib('required',"true") 	 
								->setRegisterInArrayValidator(false)
							->setAttrib('onChange','fngetvenuetime();')			
							->removeDecorator("DtDdWrapper")
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');

        $VenueTime = new Zend_Dojo_Form_Element_FilteringSelect('VenueTime');
        $VenueTime->setAttrib('dojoType',"dijit.form.FilteringSelect")						
								->setAttrib('required',"true") 
								->setRegisterInArrayValidator(false)	 
							->setAttrib('onChange','fngettimefordate(this.value);') 					
							->removeDecorator("DtDdWrapper")
							->removeDecorator("Label") 
							->removeDecorator('HtmlTag');

        $Program = new Zend_Dojo_Form_Element_FilteringSelect('Program');
        $Program->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							//->setAttrib('onChange','fngetyear(this.value);')	
							->setAttrib('onChange','fngetstatedetails(this.value);')					
							->removeDecorator("DtDdWrapper")
							->setRegisterInArrayValidator(false)
							->removeDecorator("Label") 
								->setAttrib('required',"true") 	 				
							->removeDecorator('HtmlTag');		

		$Amount = new Zend_Form_Element_Text('Amount',array());
		$Amount	->setAttrib('dojoType',"dijit.form.TextBox")
						->setAttrib('maxlength','20')
						->setAttrib('class','txt_put')
						->setAttrib('readonly',true)
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');	

		$ICNO = new Zend_Form_Element_Text('ICNO',array());			
        $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ICNO->setAttrib('class', 'txt_put') 
        	  ->setAttrib('maxlength','250')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag'); 		

	    $idPayment= new Zend_Dojo_Form_Element_FilteringSelect('ModeofPayment');
        $idPayment	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('1'=>'Direct Debit FPX','2'=>'Credit Card'))
							->setRegisterInArrayValidator(false)
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
							
        $Year = new Zend_Dojo_Form_Element_FilteringSelect('Year');
        $Year->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							//->setAttrib('onChange','showcalender(this.value);')	
							->setAttrib('onChange','fngetstatelist(this.value);')					
							->removeDecorator("DtDdWrapper")
							->setRegisterInArrayValidator(false)
							->removeDecorator("Label") 
								->setAttrib('required',"true") 	 				
							->removeDecorator('HtmlTag');	
														
	   

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label =("Yes");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";			

        $ArmyNo = new Zend_Form_Element_Text('ArmyNo',array());			
        $ArmyNo->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ArmyNo->setAttrib('class', 'txt_put') 
        	->setAttrib('onFocus','icnoid')
        	  ->setAttrib('maxlength','250')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        		->removeDecorator('HtmlTag');

  $login = new Zend_Form_Element_Text('login');
			$login->setAttrib('dojoType',"dijit.form.TextBox")
								->setAttrib('maxlength','250')
								->setAttrib('readonly','true')  
								->removeDecorator("DtDdWrapper")
								->removeDecorator("Label") 					
								->removeDecorator('HtmlTag');		
								
								
			$password = new Zend_Form_Element_Text('password');
			$password->setAttrib('dojoType',"dijit.form.TextBox")
								->setAttrib('maxlength','250')
								->setAttrib('readonly','true')  
								->removeDecorator("DtDdWrapper")
								->removeDecorator("Label") 					
								->removeDecorator('HtmlTag');	
         		
        //form elements
        $this->addElements(array($FName,$MName,$LName,$DateOfBirth,$Gender,$IDApplication,$idPayment,$ContactNo,$MobileNo,
        						$PermAddressDetails,$PermCity,$ICNO,
        					$Amount,$Year,$login,$password,$CellPhone,$Fax,$EmailAddress,$IdBatch,$Venue,$VenueTime,$UpdUser,$Update,$Save,$NewState,$NewCity,
        						$Program,$idschedulermaster,$DateTime,$Takafuloperator,$ArmyNo,$Race,$Qualification,$State,$PostalCode,$CorrAddress
        						
        						
                                 ));

    }
}