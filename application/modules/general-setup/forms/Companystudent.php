<?php
class GeneralSetup_Form_Companystudent extends Zend_Dojo_Form { //Formclass for the user module
    public function init() 
    {
    	$Companyname = new Zend_Dojo_Form_Element_FilteringSelect('Companyname');
		$Companyname->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Companyname->addMultiOption('','Select'); 	           	         		       		     
	    $Companyname->removeDecorator("DtDdWrapper");
	    $Companyname->removeDecorator("Label");
	    $Companyname->removeDecorator('HtmlTag');  
    	
       	$StudentName =  new Zend_Form_Element_Text('StudentName',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
        $StudentName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $StudentName->removeDecorator("DtDdWrapper");
        $StudentName->removeDecorator("Label");
        $StudentName->removeDecorator('HtmlTag');
        $StudentName->setAttrib('class','txt_put');	
        
        $Registrationpin =  new Zend_Form_Element_Text('Registrationpin');
        $Registrationpin->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Registrationpin->removeDecorator("DtDdWrapper");
        $Registrationpin->removeDecorator("Label");
        $Registrationpin->removeDecorator('HtmlTag');
        $Registrationpin->	setAttrib('class', 'txt_put');	
        
        
        $this->addElements(
        					array(
        						  $Companyname,
        					      $StudentName,
        					      $Registrationpin,
        					      
        						  )
        			);
    
    
    }
    	
}