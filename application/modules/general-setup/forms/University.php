<?php
class GeneralSetup_Form_University extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$IdUniversity = new Zend_Form_Element_Hidden('IdUniversity');
        $IdUniversity->removeDecorator("DtDdWrapper");
        $IdUniversity->removeDecorator("Label");
        $IdUniversity->removeDecorator('HtmlTag');
        
        $Univ_Name = new Zend_Form_Element_Text('Univ_Name');
        //$Univ_Name->addValidator(new Zend_Validate_Db_NoRecordExists('tbl_universitymaster', 'Univ_Name'));	
		$Univ_Name->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Univ_Name->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$Univ_ArabicName = new Zend_Form_Element_Text('Univ_ArabicName');
		$Univ_ArabicName->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        				
    	$ShortName = new Zend_Form_Element_Text('ShortName');
		$ShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ShortName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $Add1 = new Zend_Form_Element_Text('Add1');
		$Add1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Add1->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$Add2 = new Zend_Form_Element_Text('Add2');
		$Add2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Add2->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$City = new Zend_Form_Element_Text('City');
		$City->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $City->setAttrib('maxlength','20')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $State = new Zend_Dojo_Form_Element_FilteringSelect('State');
        $State->removeDecorator("DtDdWrapper");
        $State->setAttrib('required',"true") ;
        $State->removeDecorator("Label");
        $State->removeDecorator('HtmlTag');
        $State->setRegisterInArrayValidator(false);
		$State->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		        
        $Country = new Zend_Dojo_Form_Element_FilteringSelect('Country');
        $Country->removeDecorator("DtDdWrapper");
        $Country->setAttrib('required',"true") ;
        $Country->removeDecorator("Label");
        $Country->removeDecorator('HtmlTag');
        $Country->setAttrib('OnChange', "fnGetCountryStateList(this,'State')");
        $Country->setRegisterInArrayValidator(false);
		$Country->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Zip = new Zend_Form_Element_Text('Zip');
		$Zip->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Zip->setAttrib('maxlength','20');   
        $Zip->removeDecorator("DtDdWrapper");
        $Zip->removeDecorator("Label");
        $Zip->removeDecorator('HtmlTag');
        
        $Phone1 = new Zend_Form_Element_Text('Phone1',array('regExp'=>"^[0-9]\d{2}-\d{3}-\d{4}$",'invalidMessage'=>"Not a valid  Phone No."));
		$Phone1->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Phone1->setAttrib('maxlength','20');   
        $Phone1->removeDecorator("DtDdWrapper");
        $Phone1->removeDecorator("Label");
        $Phone1->removeDecorator('HtmlTag'); 
        
        $Phone2 = new Zend_Form_Element_Text('Phone2',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Home Phone No."));
		$Phone2->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Phone2->setAttrib('maxlength','20');   
        $Phone2->removeDecorator("DtDdWrapper");
        $Phone2->removeDecorator("Label");
        $Phone2->removeDecorator('HtmlTag'); 
                 
        $Fax = new Zend_Form_Element_Text('Fax',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Fax"));
		$Fax->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Fax->setAttrib('maxlength','20');   
        $Fax->removeDecorator("DtDdWrapper");
        $Fax->removeDecorator("Label");
        $Fax->removeDecorator('HtmlTag');
        
        $Email = new Zend_Form_Element_Text('Email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$Email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Email->setAttrib('required',"true")  			 
        		->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$Url = new Zend_Form_Element_Text('Url');
		$Url->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Url->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
		$FromDate = new Zend_Form_Element_Text('FromDate');
		$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $FromDate->setAttrib('required',"true")
        		->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}")       			 
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

		$ToDate = new Zend_Form_Element_Text('ToDate');
		$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox")  
        		->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}")  			 
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');        		

        $IdStaff = new Zend_Dojo_Form_Element_FilteringSelect('IdStaff');
        $IdStaff->removeDecorator("DtDdWrapper");
        $IdStaff->setAttrib('required',"true") ;
        $IdStaff->removeDecorator("Label");
        $IdStaff->removeDecorator('HtmlTag');
        $IdStaff->setRegisterInArrayValidator(false);
		$IdStaff->setAttrib('dojoType',"dijit.form.FilteringSelect");
        		
        		
      	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

       
       
    
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
    		
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = "Back";
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($IdUniversity,
        						 $Univ_Name,
        						 $Univ_ArabicName,
        						 $ShortName,
        						 $Add1,
                                 $Add2,
                                 $City,
                                 $State,
                                 $Country,
                                 $Zip,
                                 $Phone1,
                                 $Phone2,
                                 $Fax,
                                 $Email,
                                 $Url,
                                 $Active,
                                 $UpdDate,
                                 $UpdUser,
                                 $FromDate,
                                 $ToDate,
                                 $IdStaff,
                                 $Save,
                                 $Back));

    }
}