<?php
class GeneralSetup_Form_Subjectmaster extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$IdSubject = new Zend_Form_Element_Hidden('IdSubject');
        $IdSubject->removeDecorator("DtDdWrapper");
        $IdSubject->removeDecorator("Label");
        $IdSubject->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
   		$IdDepartment = new Zend_Dojo_Form_Element_FilteringSelect('IdDepartment');
   		//$IdDepartment->addMultiOption('','Select');
        $IdDepartment->removeDecorator("DtDdWrapper");
        $IdDepartment->setAttrib('required',"true") ;
        $IdDepartment->removeDecorator("Label");
        $IdDepartment->removeDecorator('HtmlTag');
        $IdDepartment->setRegisterInArrayValidator(false);
		$IdDepartment->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$SubjectName = new Zend_Form_Element_Text('SubjectName');
		$SubjectName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $SubjectName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
		$ArabicName = new Zend_Form_Element_Text('ArabicName');
		$ArabicName->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        $SubCode = new Zend_Form_Element_Text('SubCode');
		$SubCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $SubCode->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        
        $RequiredHours = new Zend_Form_Element_Text('RequiredHours');
        //$RequiredHours->setAttrib('maxlength','50');
        $RequiredHours->setAttrib('required',"true");
        $RequiredHours->removeDecorator("DtDdWrapper"); 
        $RequiredHours->removeDecorator("Label");
        $RequiredHours->removeDecorator('HtmlTag');
		$RequiredHours->setAttrib('dojoType',"dijit.form.TimeTextBox")
					->setAttrib('constraints',"{timePattern: 'HH:mm'}");
					
					
		 $MinHours = new Zend_Form_Element_Text('MinHours');
        //$MinHours->setAttrib('maxlength','50');
        $MinHours->setAttrib('required',"true");
        $MinHours->removeDecorator("DtDdWrapper"); 
        $MinHours->removeDecorator("Label");
        $MinHours->removeDecorator('HtmlTag');
		$MinHours->setAttrib('dojoType',"dijit.form.TimeTextBox")
					->setAttrib('constraints',"{timePattern: 'HH:mm'}");
							
		$AmtPerHour = new Zend_Form_Element_Text('AmtPerHour',array('regExp'=>"[0-9]*\.[0-9]+|[0-9]+",'invalidMessage'=>"Digits Only"));
		$AmtPerHour->setAttrib('required',"true");
		$AmtPerHour//->setAttrib('maxlength','50')
						->setAttrib('dojoType',"dijit.form.ValidationTextBox")				
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 					
						->removeDecorator('HtmlTag');
		$IdDefaultSyllabus = new Zend_Dojo_Form_Element_FilteringSelect('IdDefaultSyllabus');	
        $IdDefaultSyllabus->addMultiOptions(array(//''  => 'Select',
        										  '0' => 'MBA 2007-2008 Sylabus',
									   			  '1' => 'MBA 2009-2010 Sylabus'));
       	$IdDefaultSyllabus->removeDecorator("DtDdWrapper");
        $IdDefaultSyllabus->removeDecorator("Label");
        $IdDefaultSyllabus->removeDecorator('HtmlTag');
		$IdDefaultSyllabus->setAttrib('dojoType',"dijit.form.FilteringSelect");
														
       	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		
				
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
	
		
        //form elements
        $this->addElements(array($IdSubject,$UpdDate,$UpdUser,$IdDepartment,$SubjectName,$ArabicName,$SubCode,
        						 $RequiredHours,$MinHours,$AmtPerHour,$IdDefaultSyllabus,
        						$Active,$Save,$Clear,$Add	
                                 ));

    }
}