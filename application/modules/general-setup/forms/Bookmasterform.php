<?php

class GeneralSetup_Form_Bookmasterform extends Zend_Dojo_Form 
{		
    public function init()
    { 
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$idebook	= 	new Zend_Form_Element_Text('idebook');
        $idebook	->	removeDecorator("DtDdWrapper")//wraps content in an HTML definition list
					-> setAttrib('dojoType',"dijit.form.ValidationTextBox")//seting DojoType validation
                    ->	removeDecorator("Label") //renders from element labels
					->	setAttrib('required',"true") 	//this element is required		
					->	removeDecorator('HtmlTag'); 
    	
        $title	 	= 	new Zend_Form_Element_Text('Title');
        $title		->	removeDecorator("DtDdWrapper")//wraps content in an HTML definition list
					-> setAttrib('dojoType',"dijit.form.ValidationTextBox")//seting DojoType validation
                    ->	removeDecorator("Label") //renders from element labels
					->	setAttrib('required',"true") 	//this element is required		
					->	removeDecorator('HtmlTag');  //wraps content in an HTML elements define 
					

	    $alias	=   new Zend_Form_Element_Text('Alias'); 
        $alias	->	removeDecorator("DtDdWrapper")
         -> setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $alias	->	removeDecorator("Label");
        $alias	->	removeDecorator('HtmlTag');
        $alias	->	setAttrib('required',"true");   
                
        

        $Keywords	= new Zend_Form_Element_Text('Keywords');
        $Keywords -> setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Keywords -> removeDecorator("DtDdWrapper");
        $Keywords -> removeDecorator("Label");
        $Keywords -> removeDecorator('HtmlTag');
        $Keywords -> setAttrib('maxlength','10')
        	      ->	setAttrib('required',"true") ;	 
        
        $Author	 = 	new Zend_Dojo_Form_Element_FilteringSelect('idauthor');
        $Author  -> addMultiOption('', 'Select');
        $Author	->	removeDecorator("DtDdWrapper")//wraps content in an HTML definition list
				->	removeDecorator("Label") //renders from element labels
				->	setAttrib('required',"true") 	//this element is required
				->  setRegisterInArrayValidator(false)			
				->	removeDecorator('HtmlTag')  //wraps content in an HTML elements define 
				->  setAttrib('dojoType',"dijit.form.FilteringSelect");	    
         	    
       
        $baseon	 = 	new Zend_Dojo_Form_Element_FilteringSelect('BookDetails');
        $baseon   ->	removeDecorator("DtDdWrapper")
						->	removeDecorator("Label") 
						->	setAttrib('required',"true") 	
						->  setRegisterInArrayValidator(false)			
						->	removeDecorator('HtmlTag')
						->  setAttrib('dojoType',"dijit.form.FilteringSelect");	    

        $Active 		= 	new Zend_Form_Element_Checkbox('Active');
        $Active			->  setAttrib('dojoType',"dijit.form.CheckBox");     
        $Active			->	removeDecorator("DtDdWrapper");
        $Active			->	removeDecorator("Label");
        $Active			->	removeDecorator('HtmlTag')
        				->	setValue("1");		     
        				
        					
		$relation = new Zend_Form_Element_Radio('BookType');
	    $relation->setAttrib('dojoType',"dijit.form.RadioButton");
	    $relation->removeDecorator("DtDdWrapper");
	    $relation->addMultiOptions(array("0"=>"Program", "1" => "Course"));
	    $relation->removeDecorator("Label");
	    $relation->removeDecorator('HtmlTag')
	        	 ->setAttrib('onClick','BaseType(this.value)') 
        		 ->setValue("0");						
        
        		 
		$Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";
        
        $date= new Zend_Dojo_Form_Element_DateTextBox('Date');
        $date->setAttrib('dojoType',"dijit.form.DateTextBox");
        $date->setAttrib('class', 'txt_put');
        $date->removeDecorator("DtDdWrapper")
        	 ->setAttrib('title',"dd-mm-yyyy");
        $date->removeDecorator("Label");
        $date->removeDecorator('HtmlTag');
        $date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}"); 	

        $Amount	= new Zend_Form_Element_Text('Amount');
        $Amount -> setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount -> removeDecorator("DtDdWrapper");
        $Amount -> removeDecorator("Label");
        $Amount -> removeDecorator('HtmlTag');
        $Amount -> setAttrib('maxlength','10')
        	      ->	setAttrib('required',"true") ;	
         		
        $uploadbook = new Zend_Form_Element_File('UploadBook');
	    $uploadbook->setAttrib('required',"true");
	    //$uploadbook->setAttrib('dojoType',"dijit.form.File");   
	    $previewbook = new Zend_Form_Element_File('PreviewBook');
	    $previewbook->setAttrib('required',"true");
	    //$previewbook->setAttrib('dojoType',"dijit.form.File");      		      
						
   		$this->addElements(array($idebook,$title,$alias,$Keywords,$Author,$Active,$relation,$baseon,$Save,$uploadbook,$previewbook,$date,$Amount));				
						
    }
}
        
        