<?php
class GeneralSetup_Form_Studenttype extends Zend_Dojo_Form 
{    //Formclass for the user module
    public function init() 
     {   
        $gstrtranslate =Zend_Registry::get('Zend_Translate'); 
     	
     	$Terms = new Zend_Form_Element_Text('Terms');	
		$Terms->setAttrib('dojoType',"dijit.form.ValidationTextBox");		
        $Terms->setAttrib('required',"true");       			        		  
        $Terms->removeDecorator("DtDdWrapper");
        $Terms->removeDecorator("Label");
        $Terms->removeDecorator('HtmlTag');
        
     	$studenttype= new  Zend_Dojo_Form_Element_FilteringSelect('studenttype');
        $studenttype->addMultiOptions(array(' '=>' Select','1'=>'Student','2'=>'Company','3'=>'Takaful'));       
	    $studenttype->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $studenttype->removeDecorator("DtDdWrapper");
	    $studenttype->removeDecorator("Label");	    
	    $studenttype->removeDecorator('HtmlTag');	

	    $type = new Zend_Form_Element_Text('type');	
		$type->setAttrib('dojoType',"dijit.form.ValidationTextBox");		
        $type->setAttrib('required',"true");       			        		  
        $type->removeDecorator("DtDdWrapper");
        $type->removeDecorator("Label");
        $type->removeDecorator('HtmlTag');
     	     	     	   	     	
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
        $Save->class = "NormalBtn";
        $Save->label = $gstrtranslate->_("Save");         		         		
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
        $Clear->dojotype="dijit.form.Button";
		$Clear->setAttrib('class', 'NormalBtn');
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator('HtmlTag');
				
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
		$Add->setAttrib('class', 'NormalBtn');
		$Add->removeDecorator("Label");
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag');	
	       			      
        $this->addElements(array(
                                 $studenttype,
                                 $Save,
                                 $Clear,
                                 $Add,                                 
                                 $Terms,
                                 $type
                                )
                          );
     }
}