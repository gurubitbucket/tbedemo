<?php
class GeneralSetup_Form_Racereport extends Zend_Dojo_Form { //Formclass for the user module
    public function init() 
     {    	       
        $Batch = new Zend_Dojo_Form_Element_FilteringSelect('Batch');
        $Batch->removeDecorator("DtDdWrapper");
        $Batch->setAttrib('required',"true") ;
        $Batch->removeDecorator("Label");
        $Batch->removeDecorator('HtmlTag');
        $Batch->setAttrib('style','width:200px');	
		$Batch->setAttrib('dojoType',"dijit.form.FilteringSelect");
                        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
        $Save->class = "NormalBtn";         		         		
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
        $Clear->dojotype="dijit.form.Button";
		$Clear->setAttrib('class', 'NormalBtn');
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator('HtmlTag');
				
		$Close = new Zend_Form_Element_Button('Close');
		$Close->dojotype="dijit.form.Button";
		$Close->setAttrib('class', 'NormalBtn');				
		$Close->removeDecorator("Label");
		$Close->removeDecorator("DtDdWrapper");
		$Close->removeDecorator('HtmlTag');
				
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
		$Add->setAttrib('class', 'NormalBtn');
		$Add->removeDecorator("Label");
		$Add->removeDecorator("DtDdWrapper");
		$Add->removeDecorator('HtmlTag');
	
	    $Chart = new  Zend_Dojo_Form_Element_FilteringSelect('Chart');
        $Chart->addMultiOptions(array('0'=>'Bar Graph','1'=>'Line Chart','4'=>'Pie Chart'));       
	    $Chart->removeDecorator("DtDdWrapper");
	    $Chart->removeDecorator("Label");
	    $Chart->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $Chart->removeDecorator('HtmlTag');	        			
      
        $this->addElements(array($Batch,
                                 $Save,
                                 $Clear,
                                 $Add,
                                 $Close,
                                 $Chart
                                )
                          );
     }
}