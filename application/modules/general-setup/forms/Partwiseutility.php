<?php
class GeneralSetup_Form_Partwiseutility extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	  $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
	    
		$month3= 03; // Month value
		$day3=  21; //today's date
		$year3=2012; // Year value
		
		
		$yesterdaydate3= date('Y-m-d', mktime(0,0,0,$month3,($day3),$year3));
		$dateofbirth3 = "{min:'$yesterdaydate3',datePattern:'dd-MM-yyyy'}"; 
    	
		
		 $Date4 = new Zend_Dojo_Form_Element_DateTextBox('Date4');
	     $Date4->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Date3').constraints.max = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth")
						->setAttrib('required',"true")	
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
			$Date3 = new Zend_Dojo_Form_Element_DateTextBox('Date3');
	        $Date3->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Date4').constraints.min = arguments[0];")
						//->setAttrib('constraints', "$dateofbirth")
						->setAttrib('required',"true")	
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');			
						
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');		

        //form elements
        $this->addElements(array($Date4,$Date3,$submit,$Clear));
    }
}