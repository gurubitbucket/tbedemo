<?php
class GeneralSetup_Form_Programmaster extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
		$IdProgrammaster = new Zend_Form_Element_Hidden('IdProgrammaster');
        $IdProgrammaster->removeDecorator("DtDdWrapper");
        $IdProgrammaster->removeDecorator("Label");
        $IdProgrammaster->removeDecorator('HtmlTag');
        
        $ProgramName = new Zend_Form_Element_Text('ProgramName');	
		$ProgramName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ProgramName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $ProgramCode = new Zend_Form_Element_Text('ProgramCode');	
		$ProgramCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ProgramCode->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$Description = new Zend_Form_Element_Text('Description');
		$Description->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','100')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        		

	$idgradeset = new Zend_Dojo_Form_Element_FilteringSelect('Idgradeset');
        $idgradeset->removeDecorator("DtDdWrapper");
        $idgradeset->setAttrib('required',"true");
 	$idgradeset->setAttrib('onChange',"fngetgradevalues(this.value)");
        $idgradeset->removeDecorator("Label");
        $idgradeset->removeDecorator('HtmlTag');
        $idgradeset->setRegisterInArrayValidator(false);
		$idgradeset->setAttrib('dojoType',"dijit.form.FilteringSelect");   
		


      	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $PreRequesition = new Zend_Dojo_Form_Element_FilteringSelect('PreRequesition');
        $PreRequesition->removeDecorator("DtDdWrapper");
        $PreRequesition->setAttrib('required',"true");
        $PreRequesition->removeDecorator("Label");
         $PreRequesition->addMultiOptions(array('0'=>'No Pre-Requesite'));
        $PreRequesition->removeDecorator('HtmlTag');
        $PreRequesition->setRegisterInArrayValidator(false);
		$PreRequesition->setAttrib('dojoType',"dijit.form.FilteringSelect");         		
    		
         $idprog = new Zend_Dojo_Form_Element_FilteringSelect('idprog');
        $idprog->removeDecorator("DtDdWrapper");
        $idprog->setAttrib('required',"true");
        $idprog->removeDecorator("Label");
        $idprog->removeDecorator('HtmlTag');
        $idprog->setRegisterInArrayValidator(false);
		$idprog->setAttrib('dojoType',"dijit.form.FilteringSelect");   
        
		$Marks	= new Zend_Form_Element_Text('Marks',array('regExp'=>"[0-9]*",'invalidMessage'=>"Only Numbers"));
        $Marks->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Marks->removeDecorator("DtDdWrapper");
        $Marks->removeDecorator("Label");
        $Marks->removeDecorator('HtmlTag');
        $Marks->setAttrib('onKeyPress','return fnNumericInputOnly(this.value,event)')
        	    ->	setAttrib('required',"true") ;
		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($IdProgrammaster,
        						 $ProgramName,$Marks,$idgradeset,
        						 $ProgramCode,
        						 $Description,
                                 $Active,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $PreRequesition,
                                 $idprog,
                                 $Back));

    }
}