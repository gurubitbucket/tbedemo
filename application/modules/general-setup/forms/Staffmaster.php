<?php
class GeneralSetup_Form_Staffmaster extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	 $gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	    	
		$IdStaff = new Zend_Form_Element_Hidden('IdStaff');
        $IdStaff->removeDecorator("DtDdWrapper");
        $IdStaff->removeDecorator("Label");
        $IdStaff->removeDecorator('HtmlTag');
        
        $FirstName = new Zend_Form_Element_Text('FirstName',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
		$FirstName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $FirstName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $SecondName = new Zend_Form_Element_Text('SecondName');
		$SecondName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $SecondName->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $ThirdName = new Zend_Form_Element_Text('ThirdName');
		$ThirdName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ThirdName->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$FourthName = new Zend_Form_Element_Text('FourthName');
		$FourthName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $FourthName->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$FullName = new Zend_Form_Element_Text('FullName');
		$FullName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $FullName->setAttrib('maxlength','200')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$ArabicName = new Zend_Form_Element_Text('ArabicName');
		$ArabicName->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','200')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        		
        $Add1 = new Zend_Form_Element_Text('Add1');
		$Add1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Add1->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$Add2 = new Zend_Form_Element_Text('Add2');
		$Add2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Add2->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$City = new Zend_Form_Element_Text('City');
		$City->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $City->setAttrib('maxlength','100')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $State = new Zend_Dojo_Form_Element_FilteringSelect('State');
        $State->removeDecorator("DtDdWrapper");
        $State->setAttrib('required',"true") ;
        $State->removeDecorator("Label");
        $State->removeDecorator('HtmlTag');
        $State->setRegisterInArrayValidator(false);
		$State->setAttrib('dojoType',"dijit.form.FilteringSelect");
		        
        $Country = new Zend_Dojo_Form_Element_FilteringSelect('Country');
        $Country->removeDecorator("DtDdWrapper");
        $Country->setAttrib('required',"true") ;
        $Country->removeDecorator("Label");
        $Country->removeDecorator('HtmlTag');
        $Country->setAttrib('OnChange', 'fnGetCountryStateList');
        $Country->setRegisterInArrayValidator(false);
		$Country->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Zip = new Zend_Form_Element_Text('Zip');
		$Zip->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Zip->setAttrib('maxlength','20');   
        $Zip->removeDecorator("DtDdWrapper");
        $Zip->removeDecorator("Label");
        $Zip->removeDecorator('HtmlTag');
        
        $Phone = new Zend_Form_Element_Text('Phone',array('regExp'=>"^[0-9]\d{2}-\d{3}-\d{4}$",'invalidMessage'=>"Not a valid  Phone No."));
		$Phone->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Phone->setAttrib('maxlength','20');   
        $Phone->removeDecorator("DtDdWrapper");
        $Phone->removeDecorator("Label");
        $Phone->removeDecorator('HtmlTag'); 
                 
        $Mobile = new Zend_Form_Element_Text('Mobile',array('regExp'=>"^[0-9]\d{2}-\d{3}-\d{4}$",'invalidMessage'=>"Not a valid  Phone No."));
		$Mobile->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Mobile->setAttrib('maxlength','20');   
        $Mobile->removeDecorator("DtDdWrapper");
        $Mobile->removeDecorator("Label");
        $Mobile->removeDecorator('HtmlTag'); 
        
        $Email = new Zend_Form_Element_Text('Email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$Email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Email->setAttrib('required',"true")  			 
        		->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
      	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
       	$StaffType  = new Zend_Form_Element_Radio('StaffType');
		$StaffType->setAttrib('dojoType',"dijit.form.RadioButton");
        $StaffType->addMultiOptions(array('0' => 'College','1' => 'Branch'))
        			->setvalue('0')
        			->setSeparator('&nbsp;')
        			->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			->setAttrib('onclick', 'fnToggleCollegeDetails(this.value)');
		
		$IdCollege = new Zend_Dojo_Form_Element_FilteringSelect('IdCollege');
        $IdCollege->removeDecorator("DtDdWrapper");
        $IdCollege->setAttrib('required',"true") ;
        $IdCollege->removeDecorator("Label");
        $IdCollege->removeDecorator('HtmlTag');
        $IdCollege->setAttrib('OnChange', 'fnGetBrancheList');
        $IdCollege->setRegisterInArrayValidator(false);
		$IdCollege->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$IdBranch = new Zend_Dojo_Form_Element_FilteringSelect('IdBranch');
        $IdBranch->removeDecorator("DtDdWrapper");
        $IdBranch->removeDecorator("Label");
        $IdBranch->removeDecorator('HtmlTag');
        $IdBranch->setRegisterInArrayValidator(false);
		$IdBranch->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$IdLevel = new Zend_Dojo_Form_Element_FilteringSelect('IdLevel');
        $IdLevel->removeDecorator("DtDdWrapper");
        $IdLevel->setAttrib('required',"true") ;
        $IdLevel->removeDecorator("Label");
        $IdLevel->removeDecorator('HtmlTag');
        $IdLevel->setAttrib('placeholder', 'Select');
        $IdLevel->setRegisterInArrayValidator(false);
		$IdLevel->setAttrib('dojoType',"dijit.form.FilteringSelect"); 	
		
		$IdDepartment = new Zend_Dojo_Form_Element_FilteringSelect('IdDepartment');
        $IdDepartment->removeDecorator("DtDdWrapper");
        $IdDepartment->setAttrib('required',"true") ;
        $IdDepartment->removeDecorator("Label");
        $IdDepartment->removeDecorator('HtmlTag');
        $IdDepartment->setRegisterInArrayValidator(false);
		$IdDepartment->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$IdSubject = new Zend_Dojo_Form_Element_FilteringSelect('IdSubject');
        $IdSubject->removeDecorator("DtDdWrapper");
        $IdSubject->setAttrib('required',"true") ;
        $IdSubject->removeDecorator("Label");
        $IdSubject->removeDecorator('HtmlTag');
        $IdSubject->setRegisterInArrayValidator(false);
		$IdSubject->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
       $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
    		
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = "Back";
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($IdStaff,
        						 $FirstName,
        						 $SecondName,
        						 $ThirdName,
        						 $FourthName,
                                 $FullName,
                                 $ArabicName,
                                 $Add1,
                                 $Add2,
                                 $City,
                                 $State,
                                 $Country,
                                 $Zip,
                                 $Phone,
                                 $Email,
                                 $Active,
                                 $StaffType,
                                 $IdCollege,
                                 $IdBranch,
                                 $IdLevel,
                                 $IdDepartment,
                                 $IdSubject,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $Back));

    }
}