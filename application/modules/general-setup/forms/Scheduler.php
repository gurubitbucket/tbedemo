<?php
class GeneralSetup_Form_Scheduler extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    
		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
	    
		$iduser = new Zend_Form_Element_Hidden('iduser');
        $iduser->removeDecorator("DtDdWrapper");
        $iduser->removeDecorator("Label");
        $iduser->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
   
        $ScheduleName = new Zend_Form_Element_Text('ScheduleName',array());
		$ScheduleName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ScheduleName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        		
        $TimeFrom = new Zend_Form_Element_Text('TimeFrom',array());
		$TimeFrom->setAttrib('dojoType',"dijit.form.TimeTextBox")     			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        $TimeTo = new Zend_Form_Element_Text('TimeTo',array());
		$TimeTo->setAttrib('dojoType',"dijit.form.TimeTextBox")		 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');        		
       
        $idBatch = new Zend_Dojo_Form_Element_FilteringSelect('idBatch');
        $idBatch->removeDecorator("DtDdWrapper");
        $idBatch->setAttrib('required',"true");
        $idBatch->removeDecorator("Label");
        $idBatch->removeDecorator('HtmlTag');
        $idBatch->setAttrib('OnChange', 'fnGetDatedetails(this.value)');
        $idBatch->setRegisterInArrayValidator(false);
		$idBatch->setAttrib('dojoType',"dijit.form.FilteringSelect");

        $BatchFrom = new Zend_Dojo_Form_Element_DateTextBox('BatchFrom');
        $BatchFrom->setAttrib('dojoType',"dijit.form.DateTextBox");
		$BatchFrom->setAttrib('required',"true")
		->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$BatchFrom->setAttrib('onChange','fngetdate');
        $BatchFrom->removeDecorator("DtDdWrapper");
        $BatchFrom->setAttrib('title',"dd-mm-yyyy");
        $BatchFrom->removeDecorator("Label");
        $BatchFrom->removeDecorator('HtmlTag');       

        $BatchTo = new Zend_Dojo_Form_Element_DateTextBox('BatchTo');
        $BatchTo->setAttrib('dojoType',"dijit.form.DateTextBox");
		$BatchTo->setAttrib('required',"true");
        $BatchTo->removeDecorator("DtDdWrapper");
        $BatchTo->setAttrib('title',"dd-mm-yyyy");
        $BatchTo->removeDecorator("Label");
        $BatchTo->removeDecorator('HtmlTag');          

        $AddDate = new Zend_Form_Element_Button('AddDate');
		$AddDate->dojotype="dijit.form.Button";
		$AddDate->setAttrib('class', 'NormalBtn')	
				->setAttrib('onClick','getdatedetails')			
				->removeDecorator("Label")
				
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
				
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = "Save";
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";								
        //form elements
        $this->addElements(array($iduser,$ScheduleName,$UpdDate,$BatchTo,$BatchFrom,$idBatch,$UpdUser,$AddDate,$TimeFrom,$TimeTo,$Save
                                 ));

    }
}