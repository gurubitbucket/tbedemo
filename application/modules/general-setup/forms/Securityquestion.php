<?php

class GeneralSetup_Form_Securityquestion extends Zend_Dojo_Form 
{		
    public function init()
    { 

    	$idsecurityquestions=
    	
    	
    	
        $SecurityQuestions	=   new Zend_Form_Element_Text('SecurityQuestions');
        $SecurityQuestions   ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $SecurityQuestions	->	removeDecorator("DtDdWrapper");
        $SecurityQuestions	->	removeDecorator("Label");
        $SecurityQuestions	->	removeDecorator('HtmlTag');
        $SecurityQuestions	->	setAttrib('style','width:700px;')
        					->setAttrib('required',true)
               			    ->	setAttrib('class', 'txt_put');         
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label ="Save";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
        
        $Active		= 	new Zend_Form_Element_Checkbox('Active');      			
        $Active     ->  setAttrib('dojoType',"dijit.form.CheckBox");
        $Active		->	setAttrib("checked","checked");
        $Active		->	removeDecorator("DtDdWrapper");
        $Active		->	removeDecorator("Label");
        $Active		->	removeDecorator('HtmlTag');				
      
    
        $Search 		= 	new Zend_Form_Element_Submit('Search');
        $Search			->	setAttrib('class','NormalBtn');
        $Search			->	setAttrib('id', 'submitbutton');
        $Search			->	removeDecorator("DtDdWrapper");
        $Search			->	removeDecorator("Label");
        $Search			->	removeDecorator('HtmlTag');
         
   		$this->addElements(array($SecurityQuestions,$Active,$Search,$Save));
    }
}
        
        