<?php
class GeneralSetup_Form_Program extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$IdProgram = new Zend_Form_Element_Hidden('IdProgram');
        $IdProgram->removeDecorator("DtDdWrapper");
        $IdProgram->removeDecorator("Label");
        $IdProgram->removeDecorator('HtmlTag');
        
        $ProgramName = new Zend_Form_Element_Text('ProgramName');	
		$ProgramName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ProgramName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       	$ArabicName = new Zend_Form_Element_Text('ArabicName');
		$ArabicName->setAttrib('dojoType',"dijit.form.ValidationTextBox")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        				
        $IdCourseMaster = new Zend_Dojo_Form_Element_FilteringSelect('IdCourseMaster');
        $IdCourseMaster->removeDecorator("DtDdWrapper");
        $IdCourseMaster->setAttrib('required',"true") ;
        $IdCourseMaster->removeDecorator("Label");
        $IdCourseMaster->removeDecorator('HtmlTag');
        $IdCourseMaster->setRegisterInArrayValidator(false);
		$IdCourseMaster->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$LearningMode = new Zend_Dojo_Form_Element_FilteringSelect('LearningMode');
        $LearningMode->removeDecorator("DtDdWrapper");
        $LearningMode->setAttrib('required',"true") ;
        $LearningMode->removeDecorator("Label");
        $LearningMode->removeDecorator('HtmlTag');
        $LearningMode->setRegisterInArrayValidator(false);
		$LearningMode->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$Award = new Zend_Dojo_Form_Element_FilteringSelect('Award');
        $Award->removeDecorator("DtDdWrapper");
        $Award->setAttrib('required',"true") ;
        $Award->removeDecorator("Label");
        $Award->removeDecorator('HtmlTag');
        $Award->setRegisterInArrayValidator(false);
		$Award->setAttrib('dojoType',"dijit.form.FilteringSelect");
        		
      	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
    		
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($IdProgram,
        						 $ProgramName,
        						 $ArabicName,
        						 $IdCourseMaster,
        						 $LearningMode,
        						 $Award,
                                 $Active,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $Back));

    }
}