<?php

class GeneralSetup_Form_Sendemailtemplate extends Zend_Dojo_Form
{		
    public function init()
    { 
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
       	$Emailtemplate = new Zend_Dojo_Form_Element_FilteringSelect('Emailtemplate');
        $Emailtemplate->setAttrib('maxlength','50');
        $Emailtemplate->removeDecorator("DtDdWrapper");
        $Emailtemplate->setAttrib('required',"true") ;
        $Emailtemplate->removeDecorator("Label");
        $Emailtemplate->removeDecorator('HtmlTag');
		$Emailtemplate->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$email = new Zend_Form_Element_Text('email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
		$email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $email->setAttrib('required',"true")  			 
        		->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $Save = new Zend_Form_Element_Submit('Send');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Send");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";		
         
   		$this->addElements(array($Emailtemplate,$email,$Save));
    }
}
        
        