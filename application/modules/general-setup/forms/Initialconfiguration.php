<?php
class GeneralSetup_Form_Initialconfiguration extends Zend_Dojo_Form
{
	public function init()
	{
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
		$idConfig = new Zend_Form_Element_Hidden('idConfig');
		$idConfig	->setAttrib('id','idConfig')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate ->removeDecorator("DtDdWrapper")
        		 ->removeDecorator("Label")
        		 ->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser ->removeDecorator("DtDdWrapper")
        		 ->removeDecorator("Label")
        		 ->removeDecorator('HtmlTag');
        		 
        $idUniversity  = new Zend_Form_Element_Hidden('idUniversity');
        $idUniversity ->removeDecorator("DtDdWrapper")
        		 ->removeDecorator("Label")
        		 ->removeDecorator('HtmlTag');
					
        $noofrowsingrid = new Zend_Form_Element_Text('noofrowsingrid',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only Digits"));
		$noofrowsingrid->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $noofrowsingrid->setAttrib('maxlength','7');   
        $noofrowsingrid->removeDecorator("DtDdWrapper");
        $noofrowsingrid->removeDecorator("Label");
        $noofrowsingrid->removeDecorator('HtmlTag');
        
		$CollegeAliasName = new Zend_Form_Element_Text('CollegeAliasName');
		$CollegeAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$CollegeAliasName  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
		$UniversityName=new Zend_Form_Element_Text('Univ_Name');
		$UniversityName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$UniversityName  	->setAttrib('maxlength','255')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	

        							   
						   					
		$Scheduletime = new Zend_Form_Element_Text('Scheduletime');
        $Scheduletime->removeDecorator("DtDdWrapper"); 
        $Scheduletime->removeDecorator("Label");
        $Scheduletime->removeDecorator('HtmlTag');
		$Scheduletime->setAttrib('dojoType',"dijit.form.TimeTextBox")
					       ->setAttrib('constraints',"{timePattern: 'HH:mm'}");		

        $Scheduleday = new Zend_Form_Element_Text('Scheduleday');
		$Scheduleday	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Scheduleday  	->setAttrib('maxlength','1')
						      ->setAttrib('required',"true")
							  ->removeDecorator("Label")
							  ->removeDecorator("DtDdWrapper")
							  ->removeDecorator('HtmlTag');  							
							
		$DepartmentAliasName = new Zend_Form_Element_Text('DepartmentAliasName');
		$DepartmentAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$DepartmentAliasName  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
		$DeanAliasName = new Zend_Form_Element_Text('DeanAliasName');
		$DeanAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$DeanAliasName  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
	   $BatchAliasName = new Zend_Form_Element_Text('BatchAliasName');
		$BatchAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$BatchAliasName  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
		$RegisterAliasName = new Zend_Form_Element_Text('RegisterAliasName');
		$RegisterAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$RegisterAliasName  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');

		$SubjectAliasName = new Zend_Form_Element_Text('SubjectAliasName');
		$SubjectAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$SubjectAliasName  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
		$CourseAliasName = new Zend_Form_Element_Text('CourseAliasName');
		$CourseAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$CourseAliasName   ->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');

		$PPField1 = new Zend_Form_Element_Text('PPField1');
		$PPField1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PPField1->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
		
		$PPField2 = new Zend_Form_Element_Text('PPField2');
		$PPField2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PPField2->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$PPField3 = new Zend_Form_Element_Text('PPField3');
		$PPField3->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PPField3->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$PPField4 = new Zend_Form_Element_Text('PPField4');
		$PPField4->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PPField4->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
					
		$PPField5 = new Zend_Form_Element_Text('PPField5');
		$PPField5->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PPField5	->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');

		$VisaDetailField1 = new Zend_Form_Element_Text('VisaDetailField1');
		$VisaDetailField1->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$VisaDetailField1->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');

		$VisaDetailField2 = new Zend_Form_Element_Text('VisaDetailField2');
		$VisaDetailField2->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$VisaDetailField2->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');			

		$VisaDetailField3 = new Zend_Form_Element_Text('VisaDetailField3');
		$VisaDetailField3->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$VisaDetailField3->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$VisaDetailField4 = new Zend_Form_Element_Text('VisaDetailField4');
		$VisaDetailField4->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$VisaDetailField4->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');		

		$VisaDetailField5 = new Zend_Form_Element_Text('VisaDetailField5');
		$VisaDetailField5->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$VisaDetailField5->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$PayPalUserid = new Zend_Form_Element_Text('PayPalUserid');
		$PayPalUserid->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$PayPalUserid->setAttrib('maxlength','50')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$PaypalCurrency= new Zend_Form_Element_Select('PaypalCurrency');
		$PaypalCurrency->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$PaypalCurrency 	->setAttrib('class','txt_put')
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');	
					
		$NoofStd= new Zend_Form_Element_Text('NoofStd');
		$NoofStd->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$NoofStd->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$ClosingBatch= new Zend_Form_Element_Text('ClosingBatch',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only Digits"));
		$ClosingBatch->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ClosingBatch->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$ClosingBatchCompany = new Zend_Form_Element_Text('ClosingBatchCompany',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only Digits"));
		$ClosingBatchCompany->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ClosingBatchCompany->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
					
		$ClosingBatchTakaful= new Zend_Form_Element_Text('ClosingBatchTakaful',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only Digits"));
		$ClosingBatchTakaful->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ClosingBatchTakaful->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
		$RoomCapacity= new Zend_Form_Element_Text('RoomCapacity');
		$RoomCapacity->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$RoomCapacity->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
					
		$Center = new Zend_Form_Element_Text('CenterAliasName');
		$Center->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Center->setAttrib('maxlength','20')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');
					
					
		/*$Separator = new Zend_Form_Element_Text('Separator');
		$Separator->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Separator  ->setAttrib('class','txt_put')
							->setAttrib('maxlength','1')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');

		$ReceiptField1 = new Zend_Form_Element_Select('ReceiptField1');
		$ReceiptField1->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$ReceiptField1 	->setAttrib('class','txt_put')
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
						
		$ReceiptField2 = new Zend_Form_Element_Select('ReceiptField2');
		$ReceiptField2->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$ReceiptField2 	->setAttrib('class','txt_put')
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
		$ReceiptField3 = new Zend_Form_Element_Select('ReceiptField3');
		$ReceiptField3->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$ReceiptField3 	->setAttrib('class','txt_put')
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
						
		$ReceiptField4 = new Zend_Form_Element_Select('ReceiptField4');
		$ReceiptField4->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$ReceiptField4 	->setAttrib('class','txt_put')
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');*/
						
		$SMTPServer = new Zend_Form_Element_Text('SMTPServer');
		$SMTPServer->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$SMTPServer  ->setAttrib('maxlength','50')
					 ->removeDecorator("Label")
					 ->removeDecorator("DtDdWrapper")
					 ->removeDecorator('HtmlTag');
					 
		$SMTPUsername = new Zend_Form_Element_Text('SMTPUsername');
		$SMTPUsername->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$SMTPUsername->setAttrib('maxlength','50')
					 ->removeDecorator("Label")
					 ->removeDecorator("DtDdWrapper")
					 ->removeDecorator('HtmlTag');
					 
		$SMTPPassword = new Zend_Form_Element_Text('SMTPPassword',array('regExp'=>"[\w]+",'invalidMessage'=>"No Spaces Allowed"));
		$SMTPPassword->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$SMTPPassword  	->setAttrib('maxlength','150')
					 	->removeDecorator("Label")
					 	->removeDecorator("DtDdWrapper")
					 	->removeDecorator('HtmlTag');
					 
		$SMTPPort = new Zend_Form_Element_Text('SMTPPort',array('regExp'=>"[0-9]*",'invalidMessage'=>"Only Numbers"));
		$SMTPPort->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$SMTPPort 	 ->setAttrib('maxlength','5')
					 ->removeDecorator("Label")
					 ->removeDecorator("DtDdWrapper")
					 ->removeDecorator('HtmlTag');
					 
		$SSL  = new Zend_Form_Element_Checkbox('SSL');
		$SSL->setAttrib('dojoType',"dijit.form.CheckBox");
        $SSL	->removeDecorator("DtDdWrapper")
        		->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $DefaultEmail = new Zend_Form_Element_Text('DefaultEmail',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
        $DefaultEmail->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$DefaultEmail  	->setAttrib('maxlength','50')
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
						
		$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
		$Save	->setAttrib('id', 'save')
				->removeDecorator("Label")
					->setAttrib('class', 'NormalBtn')
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$ProgramAliasName = new Zend_Form_Element_Text('ProgramAliasName');
		$ProgramAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$ProgramAliasName  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
		$BranchAliasName = new Zend_Form_Element_Text('BranchAliasName');
		$BranchAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$BranchAliasName  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
		
						
		
		$LandscapeAliasName = new Zend_Form_Element_Text('LandscapeAliasName');
		$LandscapeAliasName	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$LandscapeAliasName  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
					
							
/////////////////////////////////////////////////////////							
		$TakafulField1 = new Zend_Form_Element_Text('TakafulField1');
		$TakafulField1	->setAttrib('dojoType',"dijit.form.TextBox");	
		$TakafulField1  	->setAttrib('maxlength','50')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	
							
		$TakafulField2 = new Zend_Form_Element_Text('TakafulField2');
		$TakafulField2	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$TakafulField2  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	
														
		$TakafulField3 = new Zend_Form_Element_Text('TakafulField3');
		$TakafulField3	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$TakafulField3  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	
		$TakafulField4 = new Zend_Form_Element_Text('TakafulField4');
		$TakafulField4	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$TakafulField4  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	
							
		$TakafulField5 = new Zend_Form_Element_Text('TakafulField5');
		$TakafulField5	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$TakafulField5  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	
							
		$TakafulField6 = new Zend_Form_Element_Text('TakafulField6');
		$TakafulField6	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$TakafulField6  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');

		
		$Paymentorder1 = new Zend_Form_Element_Text('Paymentorder1');
		$Paymentorder1	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Paymentorder1  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	

		$Paymentorder2 = new Zend_Form_Element_Text('Paymentorder2');
		$Paymentorder2	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Paymentorder2  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	

		$Paymentorder3 = new Zend_Form_Element_Text('Paymentorder3');
		$Paymentorder3	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Paymentorder3  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	

		$Paymentorder4 = new Zend_Form_Element_Text('Paymentorder4');
		$Paymentorder4	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Paymentorder4  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	
		
		$Paymentorder5 = new Zend_Form_Element_Text('Paymentorder5');
		$Paymentorder5	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Paymentorder5  	->setAttrib('maxlength','20')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');										
		
		$PassSection = new Zend_Form_Element_Radio('PassSection');				
		$PassSection->removeDecorator("DtDdWrapper")
	        	   ->addMultiOptions(array( "0" => "Each Section" ,	
	        	   							"1" => "Total"));
	    $PassSection->removeDecorator("Label");
	    $PassSection->removeDecorator('HtmlTag')
	        	   	->setAttrib('dojoType',"dijit.form.RadioButton")   
        			 ->setValue("1");
							
							
		$RegcodeType = new Zend_Form_Element_Radio('RegcodeType');
        $RegcodeType->addMultiOptions(array('0' => 'Manual', '1' => 'Auto'))
						->setValue('1')
						->setAttrib('dojoType',"dijit.form.RadioButton")
						->setAttrib('onClick','ledgerCodeType(this.value)')
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag')
						->setseparator('&nbsp;');
		
		$ReceiptField1 = new Zend_Dojo_Form_Element_FilteringSelect('RegField1');
        $ReceiptField1->removeDecorator("DtDdWrapper");
		//$ReceiptField1->addMultiOptions(array(""=>"Select"));
       // $ReceiptField1->addMultiOptions($larrreceipttype);
        $ReceiptField1->removeDecorator("Label");
        $ReceiptField1->removeDecorator('HtmlTag');
        $ReceiptField1->setRegisterInArrayValidator(false);
		$ReceiptField1->setAttrib('dojoType',"dijit.form.FilteringSelect");
						
        $ReceiptField2 = new Zend_Dojo_Form_Element_FilteringSelect('RegField2');
        $ReceiptField2->removeDecorator("DtDdWrapper");
		//$ReceiptField2->addMultiOptions(array(""=>"Select"));
       // $ReceiptField2->addMultiOptions($larrreceipttype);
        $ReceiptField2->removeDecorator("Label");
        $ReceiptField2->removeDecorator('HtmlTag');
        $ReceiptField2->setRegisterInArrayValidator(false);
		$ReceiptField2->setAttrib('dojoType',"dijit.form.FilteringSelect");
						
        $ReceiptField3 = new Zend_Dojo_Form_Element_FilteringSelect('RegField3');
        $ReceiptField3->removeDecorator("DtDdWrapper");
		//$ReceiptField3->addMultiOptions(array(""=>"Select"));
     //   $ReceiptField3->addMultiOptions($larrreceipttype);
        $ReceiptField3->removeDecorator("Label");
        $ReceiptField3->removeDecorator('HtmlTag');
        $ReceiptField3->setRegisterInArrayValidator(false);
		$ReceiptField3->setAttrib('dojoType',"dijit.form.FilteringSelect");				

		$Separator = new Zend_Form_Element_Text('Separator');
		$Separator->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Separator->setAttrib('class', 'txt_put')
        		->setAttrib('maxlength','1');   
        $Separator->removeDecorator("DtDdWrapper");
        $Separator->removeDecorator("Label");
        $Separator->removeDecorator('HtmlTag');
        
        
        
        
        $SturegField1 = new Zend_Dojo_Form_Element_FilteringSelect('SturegField1');
        $SturegField1->removeDecorator("DtDdWrapper");
		//$ReceiptField1->addMultiOptions(array(""=>"Select"));
       // $ReceiptField1->addMultiOptions($larrreceipttype);
        $SturegField1->removeDecorator("Label");
        $SturegField1->removeDecorator('HtmlTag');
        $SturegField1->setRegisterInArrayValidator(false);
		$SturegField1->setAttrib('dojoType',"dijit.form.FilteringSelect");
						
        $SturegField2 = new Zend_Dojo_Form_Element_FilteringSelect('SturegField2');
        $SturegField2->removeDecorator("DtDdWrapper");
		//$ReceiptField2->addMultiOptions(array(""=>"Select"));
       // $ReceiptField2->addMultiOptions($larrreceipttype);
        $SturegField2->removeDecorator("Label");
        $SturegField2->removeDecorator('HtmlTag');
        $SturegField2->setRegisterInArrayValidator(false);
		$SturegField2->setAttrib('dojoType',"dijit.form.FilteringSelect");
						
        $SturegField3 = new Zend_Dojo_Form_Element_FilteringSelect('SturegField3');
        $SturegField3->removeDecorator("DtDdWrapper");
		//$ReceiptField3->addMultiOptions(array(""=>"Select"));
     // $ReceiptField3->addMultiOptions($larrreceipttype);
        $SturegField3->removeDecorator("Label");
        $SturegField3->removeDecorator('HtmlTag');
        $SturegField3->setRegisterInArrayValidator(false);
		$SturegField3->setAttrib('dojoType',"dijit.form.FilteringSelect");				

		$Sturegtext = new Zend_Form_Element_Text('Sturegtext');
		$Sturegtext->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Sturegtext->setAttrib('class', 'txt_put')
        		->setAttrib('maxlength','5');   
        $Sturegtext->removeDecorator("DtDdWrapper");
        $Sturegtext->removeDecorator("Label");
        $Sturegtext->removeDecorator('HtmlTag');
       
        
        $FldTxt1 = new Zend_Form_Element_Text('FldTxt1',array('regExp'=>"[0-9]*",'invalidMessage'=>"Only Numbers"));
		$FldTxt1->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $FldTxt1->setAttrib('class', 'txt_put');
        		//->setAttrib('maxlength','5');   
        $FldTxt1->removeDecorator("DtDdWrapper");
        $FldTxt1->removeDecorator("Label");
        $FldTxt1->removeDecorator('HtmlTag');
        
        $FldTxt2 = new Zend_Form_Element_Text('FldTxt2',array('regExp'=>"[0-9]*",'invalidMessage'=>"Only Numbers"));
		$FldTxt2->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $FldTxt2->setAttrib('class', 'txt_put');
        		//->setAttrib('maxlength','5');   
        $FldTxt2->removeDecorator("DtDdWrapper");
        $FldTxt2->removeDecorator("Label");
        $FldTxt2->removeDecorator('HtmlTag');
        
        $FldTxt3 = new Zend_Form_Element_Text('FldTxt3',array('regExp'=>"[0-9]*",'invalidMessage'=>"Only Numbers"));
		$FldTxt3->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $FldTxt3->setAttrib('class', 'txt_put');
        		//->setAttrib('maxlength','5');   
        $FldTxt3->removeDecorator("DtDdWrapper");
        $FldTxt3->removeDecorator("Label");
        $FldTxt3->removeDecorator('HtmlTag');
        
        $FldTxt4 = new Zend_Form_Element_Text('FldTxt4',array('regExp'=>"[0-9]*",'invalidMessage'=>"Only Numbers"));
		$FldTxt4->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $FldTxt4->setAttrib('class', 'txt_put');
        		//->setAttrib('maxlength','5');   
        $FldTxt4->removeDecorator("DtDdWrapper");
        $FldTxt4->removeDecorator("Label");
        $FldTxt4->removeDecorator('HtmlTag');
        
        $FldTxt5 = new Zend_Form_Element_Text('FldTxt5',array('regExp'=>"[0-9]*",'invalidMessage'=>"Only Numbers"));
		$FldTxt5->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $FldTxt5->setAttrib('class', 'txt_put');
        		//->setAttrib('maxlength','5');   
        $FldTxt5->removeDecorator("DtDdWrapper");
        $FldTxt5->removeDecorator("Label");
        $FldTxt5->removeDecorator('HtmlTag');
        
        $FldTxt6 = new Zend_Form_Element_Text('FldTxt6',array('regExp'=>"[0-9]*",'invalidMessage'=>"Only Numbers"));
		$FldTxt6->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $FldTxt6->setAttrib('class', 'txt_put');
        		//->setAttrib('maxlength','5');   
        $FldTxt6->removeDecorator("DtDdWrapper");
        $FldTxt6->removeDecorator("Label");
        $FldTxt6->removeDecorator('HtmlTag');
        
        $FldDdwn1 = new Zend_Dojo_Form_Element_FilteringSelect('FldDdwn1');
        $FldDdwn1->removeDecorator("DtDdWrapper");
        $FldDdwn1->removeDecorator("Label");
        $FldDdwn1->removeDecorator('HtmlTag');
        $FldDdwn1->setRegisterInArrayValidator(false);
		$FldDdwn1->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		
		$FldDdwn2 = new Zend_Dojo_Form_Element_FilteringSelect('FldDdwn2');
        $FldDdwn2->removeDecorator("DtDdWrapper");
        $FldDdwn2->removeDecorator("Label");
        $FldDdwn2->removeDecorator('HtmlTag');
        $FldDdwn2->setRegisterInArrayValidator(false);
		$FldDdwn2->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		
		$FldDdwn3 = new Zend_Dojo_Form_Element_FilteringSelect('FldDdwn3');
        $FldDdwn3->removeDecorator("DtDdWrapper");
        $FldDdwn3->removeDecorator("Label");
        $FldDdwn3->removeDecorator('HtmlTag');
        $FldDdwn3->setRegisterInArrayValidator(false);
		$FldDdwn3->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		
		$country = new Zend_Dojo_Form_Element_FilteringSelect('Country');
        $country->removeDecorator("DtDdWrapper");
        $country->setAttrib('required',"true") ;
        $country->removeDecorator("Label");
        $country->removeDecorator('HtmlTag');
        $country->setRegisterInArrayValidator(false);
		$country->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		
		$FldDdwn4 = new Zend_Dojo_Form_Element_FilteringSelect('FldDdwn4');
        $FldDdwn4->removeDecorator("DtDdWrapper");
        $FldDdwn4->removeDecorator("Label");
        $FldDdwn4->removeDecorator('HtmlTag');
        $FldDdwn4->setRegisterInArrayValidator(false);
		$FldDdwn4->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		
		 $FldDdwn5 = new Zend_Dojo_Form_Element_FilteringSelect('FldDdwn5');
        $FldDdwn5->removeDecorator("DtDdWrapper");
        $FldDdwn5->removeDecorator("Label");
        $FldDdwn5->removeDecorator('HtmlTag');
        $FldDdwn5->setRegisterInArrayValidator(false);
		$FldDdwn5->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		
		$FldDdwn6 = new Zend_Dojo_Form_Element_FilteringSelect('FldDdwn6');
        $FldDdwn6->removeDecorator("DtDdWrapper");
        $FldDdwn6->removeDecorator("Label");
        $FldDdwn6->removeDecorator('HtmlTag');
        $FldDdwn6->setRegisterInArrayValidator(false);
		$FldDdwn6->setAttrib('dojoType',"dijit.form.FilteringSelect");	
       
        $Manulreg = new Zend_Form_Element_Text('Manulreg');
		$Manulreg->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Manulreg->setAttrib('class', 'txt_put')
        		->setAttrib('maxlength','1');   
        $Manulreg->removeDecorator("DtDdWrapper");
        $Manulreg->removeDecorator("Label");
        $Manulreg->removeDecorator('HtmlTag');
							
	/////////////////////////////////////////////////////////////											
		$minage = new Zend_Form_Element_Text('MinAge');
		$minage	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$minage	->setAttrib('maxlength','3')
		                    ->setAttrib('required',"true")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
							
		 $discount = new Zend_Form_Element_Radio('Discount');
	     //$discount->setAttrib('onClick','displayRelativeNames(this.value)');
	      $discount->removeDecorator("DtDdWrapper")
	        	   ->addMultiOptions(array( "1" => "Amount" ,
										   "0" => "Percentage"));
	      $discount->removeDecorator("Label");
	      $discount->removeDecorator('HtmlTag')
	        	   	->setAttrib('dojoType',"dijit.form.RadioButton")   
        			 ->setValue("1");	
        			 
		$StudentPass = new Zend_Form_Element_Text('StudentPass');
		$StudentPass	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$StudentPass  	->setAttrib('maxlength','1000')
						->setAttrib('style','width:500px')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');	
							
		$StudentFails = new Zend_Form_Element_Text('StudentFails');
		$StudentFails	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$StudentFails  	->setAttrib('maxlength','1000')
						->setAttrib('style','width:500px')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');     

		$Studentmaxedit = new Zend_Form_Element_Text('studedit');
		$Studentmaxedit	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Studentmaxedit ->setAttrib('maxlength','50')
		                ->setAttrib('required',"true")
						//->setAttrib('style','width:500px')
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');  
							
							
		$noofdayspre = new Zend_Form_Element_Text('examgracetime');
		$noofdayspre	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$noofdayspre  	->setAttrib('maxlength','100')
						//->setAttrib('style','width:500px')
						 ->setAttrib('required',"true")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');  
							
		$Schedulerpushtime = new Zend_Form_Element_Text('Schedulerpushtime');
        $Schedulerpushtime->removeDecorator("DtDdWrapper"); 
        $Schedulerpushtime->removeDecorator("Label");
        $Schedulerpushtime->removeDecorator('HtmlTag');
		$Schedulerpushtime->setAttrib('dojoType',"dijit.form.TimeTextBox")
					    ->setAttrib('constraints',"{timePattern: 'HH:mm'}");						
							
		$Schedulerlpushtimeinterval = new Zend_Form_Element_Text('Schedulerlpushtimeinterval');
		$Schedulerlpushtimeinterval	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Schedulerlpushtimeinterval  	->setAttrib('maxlength','1')
						      ->setAttrib('required',"true")
							  ->removeDecorator("Label")
							  ->removeDecorator("DtDdWrapper")
							  ->removeDecorator('HtmlTag');  
							
		$Schedulerpushemail = new Zend_Form_Element_Text('Schedulerpushemail');
		$Schedulerpushemail	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Schedulerpushemail  	->setAttrib('maxlength','100')
						//->setAttrib('style','width:500px')
						 ->setAttrib('required',"true")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');  
  
                $Bccpushpull = new Zend_Form_Element_Text('Bccpushpull');
		$Bccpushpull	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Bccpushpull  	->setAttrib('maxlength','100')
						//->setAttrib('style','width:500px')
						 ->setAttrib('required',"true")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag'); 
                $Examreport = new Zend_Form_Element_Text('Examreport');
		$Examreport	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Examreport  	->setAttrib('maxlength','100')
						//->setAttrib('style','width:500px')
						 ->setAttrib('required',"true")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');  


                $Bccexamreport = new Zend_Form_Element_Text('Bccexamreport');
		$Bccexamreport	->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
		$Bccexamreport  	->setAttrib('maxlength','100')
						//->setAttrib('style','width:500px')
						 ->setAttrib('required',"true")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');  
							
		
        $this->addElements(array(
        					$idConfig,$Scheduleday,$Scheduletime,$UpdDate,$UpdUser,$RegcodeType,$FldTxt1,$FldTxt2,$FldTxt3,$FldTxt4,$FldTxt5,$FldTxt6,$FldDdwn1,$FldDdwn2,$FldDdwn3,$FldDdwn4,$FldDdwn5,$FldDdwn6,
        					$noofrowsingrid,$CollegeAliasName,$CourseAliasName,$ReceiptField1,$ReceiptField2,$ReceiptField3,$Separator,
        					$PPField1,$PPField2,$PPField3,$PPField4,$PPField5,$country,
        					$VisaDetailField1,$VisaDetailField2,$VisaDetailField3,$VisaDetailField4,
        					$VisaDetailField5,$Center,$Manulreg,$BatchAliasName,
        					$SMTPServer,$SMTPUsername,$SMTPPassword,$SMTPPort,$SSL,$Sturegtext,$SturegField3,$SturegField2,$SturegField1,
        					$DefaultEmail,$Save,$idUniversity,$SubjectAliasName,$DepartmentAliasName,$DeanAliasName,$RegisterAliasName,
        					$ProgramAliasName,$BranchAliasName,$LandscapeAliasName,$minage,$discount,
        					$RoomCapacity,$ClosingBatch,$PayPalUserid,$PaypalCurrency,$NoofStd,
        					$TakafulField1,$TakafulField2,$TakafulField3,$TakafulField4,$TakafulField5,$TakafulField6,
        					$PassSection,$UniversityName,$Paymentorder1,$Paymentorder2,$Paymentorder3,$Paymentorder4,$Paymentorder5,
        					$StudentFails,$StudentPass,$Studentmaxedit,$noofdayspre,$Schedulerpushtime,$Schedulerlpushtimeinterval,$Schedulerpushemail,$Examreport,$Bccpushpull,$Bccexamreport,$ClosingBatchCompany,$ClosingBatchTakaful
        					
        				));
	}	
}