<?php
class GeneralSetup_Form_Gradebase extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$IdGrade = new Zend_Form_Element_Hidden('IdGrade');
        $IdGrade->removeDecorator("DtDdWrapper");
        $IdGrade->removeDecorator("Label");
        $IdGrade->removeDecorator('HtmlTag');
/*        				
        $IdCourseMaster = new Zend_Dojo_Form_Element_FilteringSelect('IdCourseMaster');
        $IdCourseMaster->removeDecorator("DtDdWrapper");
        $IdCourseMaster->setAttrib('required',"true") ;
        $IdCourseMaster->removeDecorator("Label");
        $IdCourseMaster->removeDecorator('HtmlTag');
        $IdCourseMaster->setRegisterInArrayValidator(false);
		$IdCourseMaster->setAttrib('dojoType',"dijit.form.FilteringSelect");*/
		
		$IdCourseMaster = $this->createElement('multiselect','IdCourseMaster', 
							array( 'style' => 'width: 10em; height: 8em; margin-left:-15px;' ));	
			$IdCourseMaster->setRequired(true);	
			//$idUnit->addMultiOptions($larrUnitNamesList);
		
        $Gradename = new Zend_Form_Element_Text('Gradename');
        $Gradename->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Gradename->setAttrib('required',"true")
        		 ->setAttrib('maxlength','50');                                                  
        $Gradename->removeDecorator("DtDdWrapper");
        $Gradename->removeDecorator("Label");
        $Gradename->removeDecorator('HtmlTag');

        $MinMarks = new Zend_Form_Element_Text('MinMarks');
        $MinMarks->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $MinMarks->setAttrib('required',"true")
        		 ->setAttrib('maxlength','50');                                                  
        $MinMarks->removeDecorator("DtDdWrapper");
        $MinMarks->removeDecorator("Label");
        $MinMarks->removeDecorator('HtmlTag');

        $Maxmarks = new Zend_Form_Element_Text('Maxmarks');
        $Maxmarks->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Maxmarks->setAttrib('required',"true")
        		 ->setAttrib('maxlength','50');                                                  
        $Maxmarks->removeDecorator("DtDdWrapper");
        $Maxmarks->removeDecorator("Label");
        $Maxmarks->removeDecorator('HtmlTag');        
        		
      	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
    		
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($IdGrade,
        						 $IdCourseMaster,$Gradename,$MinMarks,$Maxmarks,
                                 $Active,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $Back));

    }
}