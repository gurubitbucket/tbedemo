<?php

class GeneralSetup_Form_Studentpayment extends Zend_Dojo_Form 
{		
    public function init()
    { 
       						
        $StudentName	=   new Zend_Form_Element_Text('StudentName',array('regExp'=>"[A-Za-z ]+",'invalidMessage'=>"Alphabets Only"));
        $StudentName  ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $StudentName	->	removeDecorator("DtDdWrapper");
        $StudentName	->	removeDecorator("Label");
        $StudentName	->	removeDecorator('HtmlTag');
        $StudentName	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');         
        $Search 		= 	new Zend_Form_Element_Submit('Search');
        $Search			->	setAttrib('class','NormalBtn');
        $Search			->	setAttrib('id', 'submitbutton');
        $Search			->	removeDecorator("DtDdWrapper");
        $Search			->	removeDecorator("Label");
        $Search			->	removeDecorator('HtmlTag');
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = ("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         	 ->class = "NormalBtn";
        
        $ChequeName = new Zend_Form_Element_Text('ChequeName');
        $ChequeName  ->  setAttrib('dojoType',"dijit.form.TextBox");
        $ChequeName	->	removeDecorator("DtDdWrapper");
        $ChequeName	->	removeDecorator("Label");
        $ChequeName	->	removeDecorator('HtmlTag');
        $ChequeName	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');
         
        $Amount= new Zend_Form_Element_Text('Amount');
        $Amount  ->  setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount	->	removeDecorator("DtDdWrapper");
        $Amount	->	removeDecorator("Label")
              ->setAttrib('required',"true");
        $Amount	->	removeDecorator('HtmlTag');
        $Amount	->	setAttrib('style','width:155px;')
               			->	setAttrib('class', 'txt_put');    			
   		$this->addElements(array($StudentName,$Search,$ChequeName,$Save,$Amount));
    }
}
        
        