<?php
class GeneralSetup_Form_Semester extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$IdSemester = new Zend_Form_Element_Hidden('IdSemester');
        $IdSemester->removeDecorator("DtDdWrapper");
        $IdSemester->removeDecorator("Label");
        $IdSemester->removeDecorator('HtmlTag');
        
        $Semester  = new Zend_Dojo_Form_Element_FilteringSelect('Semester');
        $Semester->removeDecorator("DtDdWrapper");
        $Semester->setAttrib('required',"true") ;
        $Semester->removeDecorator("Label");
        $Semester->removeDecorator('HtmlTag');
        $Semester->setRegisterInArrayValidator(false);
		$Semester->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$year = new Zend_Dojo_Form_Element_FilteringSelect('year');
        $year->removeDecorator("DtDdWrapper");
        $year->setAttrib('required',"true") ;
        $year->setAttrib('size',"5") ;
        $year->addMultiOptions(array('2011' => '2011',
        							 '2012' => '2012',
        							 '2013' => '2013',
        							 '2014' => '2014',
        							 '2015' => '2015',
        							 '2016' => '2016',
       								 '2017' => '2017',
        							 '2018' => '2018',
        							 '2019' => '2019',
        							 '2020' => '2020'));
        $year->removeDecorator("Label");
        $year->removeDecorator('HtmlTag');
        $year->setRegisterInArrayValidator(false);
		$year->setAttrib('dojoType',"dijit.form.FilteringSelect");
        		
       	$ShortName = new Zend_Form_Element_Text('ShortName');
		$ShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ShortName->setAttrib('required',"true")  			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        				
   		$SemesterCode = new Zend_Form_Element_Text('SemesterCode');
		$SemesterCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$SemesterCode->setAttrib('required',"true")  			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        				
        $StartDate = new Zend_Form_Element_Text('StartDate');
		$StartDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $StartDate->setAttrib('required',"true")  
                 ->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}")       			 
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

		$EndDate = new Zend_Form_Element_Text('EndDate');
		$EndDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $EndDate->setAttrib('required',"true")       			 
        		->removeDecorator("DtDdWrapper")
        		->setAttrib('constraints',"{datePattern:'dd-MM-yyyy'}")  
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');   
        		
        $StudentIntake = new Zend_Dojo_Form_Element_FilteringSelect('StudentIntake');
        $StudentIntake->removeDecorator("DtDdWrapper");
        $StudentIntake->setAttrib('required',"true") ;
        $StudentIntake->addMultiOptions(array('0' => 'No, There will be no intake for this semester.',
        							          '1' => 'Yes, There will be student intake for this semester.'));
        $StudentIntake->removeDecorator("Label");
        $StudentIntake->removeDecorator('HtmlTag');
        $StudentIntake->setRegisterInArrayValidator(false);
		$StudentIntake->setAttrib('dojoType',"dijit.form.FilteringSelect");
        		
      	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
    		
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($IdSemester,
        						 $Semester,
        						 $year,
        						 $ShortName,
        						 $SemesterCode,
        						 $StartDate,
        						 $StudentIntake,
        						 $EndDate,
                                 $Active,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $Back));

    }
}