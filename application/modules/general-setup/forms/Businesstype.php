<?php
class GeneralSetup_Form_Businesstype extends Zend_Dojo_Form 
{    
	
    public function init() 
    {    
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    		
    	$idbusinesstype = new Zend_Form_Element_Hidden('idbusinesstype');
        $idbusinesstype->removeDecorator("DtDdWrapper");
        $idbusinesstype->removeDecorator("Label");
        $idbusinesstype->removeDecorator('HtmlTag');
        
        $BusinessName = new Zend_Form_Element_Text('BusinessType');	
		$BusinessName->setAttrib('dojoType',"dijit.form.ValidationTextBox");		
        $BusinessName->setAttrib('required',"true");       			        		  
        $BusinessName->removeDecorator("DtDdWrapper");
        $BusinessName->removeDecorator("Label");
        $BusinessName->removeDecorator('HtmlTag');
        		
       	$Description = new Zend_Form_Element_Text('Description');
		$Description->setAttrib('dojoType',"dijit.form.ValidationTextBox");    			         				      
        $Description->removeDecorator("DtDdWrapper");
        $Description->removeDecorator("Label");
        $Description->removeDecorator('HtmlTag');
        
        $Days = new Zend_Form_Element_Text('Days',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only Digits"));
		$Days->setAttrib('dojoType',"dijit.form.ValidationTextBox");    			         				      
        $Days->removeDecorator("DtDdWrapper");
        $Days->removeDecorator("Label");
        $Days->removeDecorator('HtmlTag');
        $Days->setAttrib('maxlength','2');   

        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
         $Save = new Zend_Form_Element_Submit('Save');
         $Save->dojotype="dijit.form.Button";
         $Save->label = $gstrtranslate->_("Save");
         $Save->setAttrib('class', 'NormalBtn');
         $Save->setAttrib('id', 'submitbutton');
         $Save->removeDecorator("DtDdWrapper");
         $Save->removeDecorator("Label");
         $Save->removeDecorator('HtmlTag');
           		         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn');
		$Back->removeDecorator("Label");
		$Back->removeDecorator("DtDdWrapper");
		$Back->removeDecorator('HtmlTag');

        //form elements
        $this->addElements(array($idbusinesstype,
        						 $BusinessName,
        						 $Description,
        						 $Days,
        						 $Active,       						
                                 $Save,
                                 $Back
                                )
                          );

    }
}