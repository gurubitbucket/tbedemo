<?php
class GeneralSetup_Form_Departmentmaster extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$IdDepartment = new Zend_Form_Element_Hidden('IdDepartment');
        $IdDepartment->removeDecorator("DtDdWrapper");
        $IdDepartment->removeDecorator("Label");
        $IdDepartment->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
   		
        $DepartmentName = new Zend_Form_Element_Text('DepartmentName');
		$DepartmentName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $DepartmentName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $ArabicName = new Zend_Form_Element_Text('ArabicName');
		$ArabicName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ArabicName//->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','50')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');		
        		
        $DeptCode = new Zend_Form_Element_Text('DeptCode');
		$DeptCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $DeptCode->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','25')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');	

        $DepartmentType  = new Zend_Form_Element_Radio('DepartmentType');
		$DepartmentType->setAttrib('dojoType',"dijit.form.RadioButton");
        $DepartmentType->addMultiOptions(array('0' => 'College','1' => 'Branch'))
        			->setvalue('0')
        			->setSeparator('&nbsp;')
        			->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			->setAttrib('onclick', 'fnToggleCollegeDetails(this.value)');	

        $IdCollege = new Zend_Dojo_Form_Element_FilteringSelect('IdCollege');
        $IdCollege->removeDecorator("DtDdWrapper");
        $IdCollege->setAttrib('required',"true") ;
        $IdCollege->removeDecorator("Label");
        $IdCollege->removeDecorator('HtmlTag');
        $IdCollege->setAttrib('OnChange', 'fnGetBrancheList');
        $IdCollege->setRegisterInArrayValidator(false);
		$IdCollege->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$IdBranch = new Zend_Dojo_Form_Element_FilteringSelect('IdBranch');
        $IdBranch->removeDecorator("DtDdWrapper");
        $IdBranch->removeDecorator("Label");
        $IdBranch->removeDecorator('HtmlTag');
        $IdBranch->setRegisterInArrayValidator(false);
		$IdBranch->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
         		
        $Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		
				
		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
        $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
	
		
        //form elements
        $this->addElements(array($IdDepartment,$UpdDate,$UpdUser,$DepartmentName,$ArabicName,$DeptCode,$DepartmentType,
        						$IdCollege,$IdBranch,$Active,
        						 $Save,$Clear,$Add
                                 ));

    }
}