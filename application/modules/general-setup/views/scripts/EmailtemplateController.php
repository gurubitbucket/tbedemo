<?php
class GeneralSetup_EmailtemplateController extends Zend_Controller_Action {
	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	
	public function init() { //initialization function
		$this->gobjsessionsis = Zend_Registry::get('sis'); //initialize session variable
		$lobjinitialconfigModel = new GeneralSetup_Model_DbTable_Initialconfiguration(); //user model object
		$larrInitialSettings = $lobjinitialconfigModel->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);
		$this->gintPageCount = isset($larrInitialSettings['noofrowsingrid'])?$larrInitialSettings['noofrowsingrid']:"5";
	}

	public function indexAction()
	{			
 		$lobjsearchform = new App_Form_Search(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view				

		$lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Emailtemplate();  // email template model object
		$larrresult = $lobjemailTemplateModel->fnGetTemplateDetails(); // get template details	

		
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->emailtemplatepaginatorresult);
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->emailtemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->emailtemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {	
					$larrresult = $lobjemailTemplateModel->fnSearchTemplate($lobjsearchform->getValues());										
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		$this->gobjsessionsis->emailtemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/general-setup/emailtemplate');
		$this->_redirect( $this->baseUrl . '/general-setup/emailtemplate/index');
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function newtemplateAction() { 	
 		$lobjemailTemplateForm = new GeneralSetup_Form_Emailtemplate();  // email template form
		$this->view->form = $lobjemailTemplateForm;	
		$auth = Zend_Auth::getInstance();
		$this->view->form->UpdUser->setValue ( $auth->getIdentity()->iduser);			        		       						 
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) // save opeartion
		{ 
			$larrformData = $this->_request->getPost();	
            echo "<pre>";
            print_r($larrformData);die();			
			if ($lobjemailTemplateForm->isValid($larrformData)) {		
				$editorData = $larrformData['content1'];		
																
				$lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Emailtemplate();	// email template row 								   
			    $lintresult = $lobjemailTemplateModel->fnAddEmailTemplate($lobjemailTemplateForm->getValues(),$editorData); // add method			
			  //  $this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'emailtemplate', 'action'=>'index'),'default',true));        
			 $this->_redirect( $this->baseUrl . '/general-setup/emailtemplate/index');
			}			
		}
		elseif($this->_request->isPost() && $this->_request->getPost('Send'))
        {
                              $larrformData = $this->_request->getPost(); 
                              unset($larrformData['Send']);   
                      if ($lobjemailTemplateForm->isValid($larrformData)) 
					  {	
                              $editorData = $larrformData['content1'];		
				              $lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Emailtemplate();	// email template row 								   
			                  $idtemplate = $lobjemailTemplateModel->fnAddEmailTemplate($lobjemailTemplateForm->getValues(),$editorData); // add method
					          require_once('Zend/Mail.php');
		                      require_once('Zend/Mail/Transport/Smtp.php');			
	                          $db 	= 	Zend_Db_Table::getDefaultAdapter();	    
		                      $larrresult= $db->select()          
									->from(array("a" =>"tbl_studentapplication"),array("a.*"))
									->join(array("c" =>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
									->join(array("d" =>"tbl_center"),'a.Examvenue=d.idcenter',array("d.*"))
									->join(array("e" =>"tbl_managesession"),'a.Examsession=e.idmangesession',array("e.*"))				
									->join(array("f" =>"tbl_registereddetails"),'f.IDApplication=a.IDApplication',array("f.*"))
									->where('a.IDApplication = 40000');
	                            $result = $db->fetchRow($larrresult);  
								$larrStudentMailingDetails = $result;						   
								$larrEmailTemplateDesc =  $lobjemailTemplateModel->fnGetEmailTemplateDescription($idtemplate);						    
								$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
								$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
								$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
								$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
								$lstrEmailTemplateBody    =  $larrformData['content1'];							
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										try
										{
											    $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										}
										catch(Exception $e) 
										{
											     echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
											
										}
										$mail = new Zend_Mail();
									   try
									   {
											     $mail->setBodyHtml($lstrEmailTemplateBody);
									   }
									   catch(Exception $e)
									   {
												 echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
									   }										
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'itwine';
										$receiver_email =$larrformData['EmailAddress'];
										$receiver = $result['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
											 //->addBcc('tony.uce@gmail.com','itwine')
									         ->setSubject($lstrEmailTemplateSubject);
								    echo "<pre>";			
									try 
									{
									                $result = $mail->send($transport);
     								} 
     								catch (Exception $e) 
     								{
									                echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
  								    }
					$this->_redirect( $this->baseUrl . '/general-setup/emailtemplate/index');					
                }
		}
	}

	public function edittemplateAction() {		
 		
 		$idTemplate = $this->_getParam('id');
 		$lobjemailTemplateForm = new GeneralSetup_Form_Emailtemplate(); // email template form
		$this->view->form = $lobjemailTemplateForm; 
				//send the lobjemailTemplateForm object to the view		
        if ($this->_request->isPost() && $this->_request->getPost('Save')) {  // update operation	
            $larrformData = $this->_request->getPost();   
            unset($larrformData['Save']);            				
            if ($lobjemailTemplateForm->isValid($larrformData)) 
			{
            		$idTemplate = $larrformData['idTemplate'];
                	$where = 'idTemplate = '. $idTemplate;					           
				 	$lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Emailtemplate();	
				 	$editorData = $larrformData['TemplateBody'];							 	
                    $lobjemailTemplateModel->fnUpdateTemplate($where,$larrformData,$editorData); // update email template details		 	
                    $this->_redirect( $this->baseUrl . '/general-setup/emailtemplate/index');
                    // $this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'emailtemplate', 'action'=>'index'),'default',true)); 
				} 			
        } 
		 elseif($this->_request->isPost() && $this->_request->getPost('Send'))
       {
                    $larrformData = $this->_request->getPost();  
					$lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Emailtemplate();
                    unset($larrformData['Send']);   
                    $idTemplate = $larrformData['idTemplate'];
                	$where = 'idTemplate = '. $idTemplate;	
				 	$editorData = $larrformData['TemplateBody'];							 	
                    $lobjemailTemplateModel->fnUpdateTemplate($where,$larrformData,$editorData); // update email template details
					 require_once('Zend/Mail.php');
		             require_once('Zend/Mail/Transport/Smtp.php');			
	                 $db 	= 	Zend_Db_Table::getDefaultAdapter();	    
		             $larrresult= $db->select()          
									->from(array("a" =>"tbl_studentapplication"),array("a.*"))
									->join(array("c" =>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
									->join(array("d" =>"tbl_center"),'a.Examvenue=d.idcenter',array("d.*"))
									->join(array("e" =>"tbl_managesession"),'a.Examsession=e.idmangesession',array("e.*"))				
									->join(array("f" =>"tbl_registereddetails"),'f.IDApplication=a.IDApplication',array("f.*"))
									->where('a.IDApplication = 40000');
	                        $result = $db->fetchRow($larrresult); 
						    $larrStudentMailingDetails = $result;						   
						    $larrEmailTemplateDesc =  $lobjemailTemplateModel->fnGetEmailTemplateDescription($larrformData['idTemplate']);						    
						    //print_r($larrformData['TemplateName']);
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							
							$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										try
										{
											       $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										}
										catch(Exception $e) 
										{
											       echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
											
										}
										$mail = new Zend_Mail();
									    try
									    {
											       $mail->setBodyHtml($lstrEmailTemplateBody);
									    }
									   catch(Exception $e)
									    {
												   echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
									    }
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'itwine';
										$receiver_email =$larrformData['EmailAddress'];
										$receiver = $result['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
											 //->addBcc('tony.uce@gmail.com','itwine')
									         ->setSubject($lstrEmailTemplateSubject);
											 	echo "<pre>";
									try 
									{
									            $result = $mail->send($transport);
     								} 
     								catch (Exception $e) 
     								{
									             echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
  								    }
					$this->_redirect( $this->baseUrl . '/general-setup/emailtemplate/index');
        } 
		
		else {
        		// 	get external panel row values by id and populate
				$lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Emailtemplate(); //email template model object
		        $larrresult = $lobjemailTemplateModel->fnViewTemplte($idTemplate);
		       	$this->view->TemplateBody = $larrresult['TemplateBody'];

		        
				$lobjemailTemplateForm->populate($larrresult); // populate all the values to the form according to id			
  		}  	
  	}	  	
}