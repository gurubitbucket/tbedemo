<?php
  class GeneralSetup_BookmasterController extends Base_Base
  {
    public function init()
    {  
       //initialization function
       $this->view->translate =Zend_Registry::get('Zend_Translate');    //get translation registry
   	   Zend_Form::setDefaultTranslator($this->view->translate);
	   $this->fnsetObj();	 //calling a function for creating objects
    }
    
	public function fnsetObj() 
	{   //function for creating objects		
		$this->lobjbookModel = new GeneralSetup_Model_DbTable_Bookmastermodel();   //account model object
	    $this->lobjbookform = new GeneralSetup_Form_Bookmasterform();    //intialize account Form
		$this->registry = Zend_Registry::getInstance();        // get a registry instance
		$this->locale = $this->registry->get('Zend_Locale');   //initialise locale	
	}
    
    public function indexAction()
    {   
    	// Action For Search and View 
        $this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
        $larrresult = $this->lobjbookModel->fngetbookdetails(); //get Account details
        $larrGroupList	=	$this->lobjbookModel->fnGetAutherlistSelect();
		$this->view->lobjform->field15->addMultiOptions($larrGroupList);
		$lintpagecount =5;       
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionstudent->userpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->userpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		$larrformData = $this->_request->getPost();
			if ($this->lobjform->isValid ( $larrformData )) 
			{
				$larrresult = $this->lobjbookModel->fngetbookSearch($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->userpaginatorresult = $larrresult;
			}
		}
		 //code for clearing the index page
		 if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		 {
			$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'bookmaster', 'action'=>'index'),'default',true));
		 }
    	
    }
    
    public function newbookAction()
    {   
    	//Action For Adding New Booksdetails And Updating Booksdetails
    	$this->view->lobjbookform = $this->lobjbookform; 			
		$larrGroupList	=	$this->lobjbookModel->fnGetAutherlistSelect();
	   /*print_r($larrGroupList);die();*/
		$this->view->lobjbookform->idauthor->addMultiOptions($larrGroupList);
		//$this->view->lobjbookform->idauthor->setRequired(true);
    if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
      {
      	//echo "harsha";die();
       $larrformData = $this->_request->getPost();      
       unset($larrformData['Save']);     
       unset ($larrformData['MAX_FILE_SIZE']);
       /*print_r($larrformData);
       die();*/
      /* $UploadDate= $_FILES['UploadDate'];  
       print_r($UploadDate);die();*/
       /////////////////////////////////
       
       $lintfilecount['Count'] =0;
	   $lstruploaddir = "/uploads/book/";
	   $larrformData['FileLocation'] = $lstruploaddir;
	   //$larrformData['UploadDate'] = $larrformData['UpdDate'];
	   if($_FILES['UploadBook']['error'] != UPLOAD_ERR_NO_FILE)
				{
					$lintfilecount['Count']++; 
			        //$larrext = explode(".", basename($_FILES['uploadedfiles']['name'][$linti]));
					$lstrfilename = pathinfo(basename($_FILES['UploadBook']['name']), PATHINFO_FILENAME);					
					$lstrext = pathinfo(basename($_FILES['UploadBook']['name']), PATHINFO_EXTENSION);				    
				    $filename = $lintfilecount['Count'].".".date('YmdHis').".".$lstrext;
				    $filename = str_replace(' ','_',$lstrfilename).$filename;
				    //print_r($filename);die();				 
					$file = realpath('.').$lstruploaddir . $filename;
					/*print_r($file);die();*/	
					if (move_uploaded_file($_FILES['UploadBook']['tmp_name'],$file)) 
					{
						//echo "success";						
							$larrformData['UploadBook'] = $filename;
										
					}
				else 
					{
			    		//echo "error";
			    		//die();
					}									
				}
	   $lintfilecount['Count'] =0;		
       $larrformData['FileLocation'] = $lstruploaddir;
      
	   if($_FILES['PreviewBook']['error'] != UPLOAD_ERR_NO_FILE)
				{
					$lintfilecount['Count']++; 
			        //$larrext = explode(".", basename($_FILES['uploadedfiles']['name'][$linti]));
					$lstrfilename = pathinfo(basename($_FILES['PreviewBook']['name']), PATHINFO_FILENAME);					
					$lstrext = pathinfo(basename($_FILES['PreviewBook']['name']), PATHINFO_EXTENSION);				    
				    $filename = $lintfilecount['Count'].".".date('YmdHis').".".$lstrext;
				    $filename = str_replace(' ','_',$lstrfilename)."_".$filename;				 
					$file = realpath('.').$lstruploaddir . $filename;
					/*print_r($file);die();*/	
					if (move_uploaded_file($_FILES['PreviewBook']['tmp_name'],$file)) 
					{
						//echo "success";die();							
							$larrformData['PreviewBook'] =  $filename;
										
					}
				else 
					{
			    		//echo "error";
			    		//die();
					}									
				}
       //////////////////////////////////  
          
	   $this->lobjbookModel->fnInsert($larrformData);
	   $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_ebook','idebook');
	  // echo $lastid;die();
	   $this->_redirect( $this->baseUrl . '/general-setup/bookmaster/adddate/id/'.$lastid);
	  
      }
    }
   
   public function getgroupdetailsAction()
   {
		$this->_helper->layout->disableLayout();  //for don't want to write phtml code(view) for this Action
		$this->_helper->viewRenderer->setNoRender();
		$larrComDetails = "";							
		$lstrType = $this->_getParam('idprog');
		
		if($lstrType==1)
		{	
			$result=$this->lobjbookModel->fnGetAllCourseNameList();		
			$larrComDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($result);		 
		}
		else 
		{
			$result=$this->lobjbookModel->fnGetAllprogramNameList();
			$larrComDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($result);	
		}
		echo Zend_Json_Encoder::encode($larrComDetails);
	}
	
   public function adddateAction()
   {
   	$this->view->lobjbookform = $this->lobjbookform;
   	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
      {
       $larrformData = $this->_request->getPost();        
   	   $lintid = $this->_getParam('id');
   	   $larrformData['idebook']=$lintid;
   	   unset($larrformData['Save']);
   	  /* Print_r($larrformData);die();*/
   	   $this->lobjbookModel->fnInsertdate($larrformData);
   	   $this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'bookmaster', 'action'=>'index'),'default',true));
      }
   }
   	
   public function downloadfileAction()
   {
		//disable layout
		$this->_helper->layout->disableLayout(); 
		//disable view
		$this->_helper->viewRenderer->setNoRender(true);
		//store  the filename 
		$lstrfilepath = realpath('.')."/uploads/book/kashi.xls";
		//set the content type
		header("Content-type: application/octet-stream;charset=utf-8;encoding=utf-8");//MIME type
		//set filename to the downloaded file
		header("Content-Disposition: attachment; filename='kashi.xls'");
		//read the specified file 
		readfile($lstrfilepath);
  	}
  	
   
   
   
   
}



