<?php
class GeneralSetup_CountrymasterController extends Base_Base 
{   
    //Controller for the User Module	
	private $lobjCountrymaster;
	private $lobjCountrymasterForm;
	private $_gobjlogger;
	
	public function init() 
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    $this->fnsetObj();
	}
	public function fnsetObj()
	{
		$this->lobjform = new App_Form_Search (); //searchform
		$this->lobjCountrymaster = new GeneralSetup_Model_DbTable_Countrymaster();
		$this->lobjCountrymasterForm = new GeneralSetup_Form_Countrymaster(); 
	}
	
	public function indexAction() 
	{ 
		// action for search and view
		$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		
		$larrresult = $this->lobjCountrymaster->fngetCountryDetails (); //get user details
		if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->countrymasterpaginatorresult);
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->countrymasterpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->countrymasterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
			$larrformData = $this->_request->getPost ();
			/*print_r($larrformData);
			die();*/
			if ($this->lobjform->isValid ( $larrformData )) 
			{   
				unset ( $larrformData ['Search'] );
				/*print_r($larrformData);
			    die();*/
				$larrresult = $this->lobjCountrymaster->fnSearchCountries( $larrformData ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->countrymasterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
			$this->_redirect( $this->baseUrl . '/general-setup/countrymaster/index');
		}


	}

	public function newcountrymasterAction()
	 {  
	 	//Action for creating the new user
		$this->view->lobjCountrymasterForm = $this->lobjCountrymasterForm; 
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjCountrymasterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjCountrymasterForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' ))
	     {
			$larrformData = $this->_request->getPost (); //get the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			if ($this->lobjCountrymasterForm->isValid ( $larrformData )) 
			{
				$result = $this->lobjCountrymaster->fnaddCountrymaster($larrformData); //instance for adding the lobjuserForm values to DB		

				// Write to Log
			    $auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Country"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
				$this->_redirect( $this->baseUrl . '/general-setup/countrymaster/index');
			}
		 }

	}

	public function countrymasterlistAction() 
	{   
		//Action for the updation and view of the user details
		$this->view->lobjCountrymasterForm = $this->lobjCountrymasterForm; 
		
		$lintidCountry = ( int ) $this->_getParam ( 'id' );
		$this->view->idCountry = $lintidCountry;
		
		$larrresult = $this->lobjCountrymaster->fnviewCountrymaster($lintidCountry); //getting user details based on userid		
		$this->lobjCountrymasterForm->populate($larrresult);		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjCountrymasterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjCountrymasterForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$this->view->lobjCountrymasterForm->idCountry->setValue($lintidCountry );
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' ))
		{
			$larrformData = $this->_request->getPost ();
			if($this->_request->isPost ()) 
			{
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($this->lobjCountrymasterForm->isValid ( $larrformData )) 
				{						
					$lintidCountry = $larrformData ['idCountry'];
					$this->lobjCountrymaster->fnupdateCountrymaster($lintidCountry, $larrformData );
					
					$auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the Country with Id = ".$lintidCountry."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
					//$this->_redirect($this->view->url(array('module'=>'generalsetup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
					$this->_redirect( $this->baseUrl . '/general-setup/countrymaster/index');
				}
			}
		}
		$this->view->lobjCountrymasterForm = $this->lobjCountrymasterForm;
	}	
}