<?php
class GeneralSetup_CollegemasterController extends Base_Base { //Controller for the User Module
	
	private $locale;
	private $registry;
	
	
	public function init() { //initialization function
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$lobjCollegemaster = new GeneralSetup_Model_DbTable_Collegemaster(); //user model object
		$larrresult = $lobjCollegemaster->fngetCollegemasterDetails(); //get user details
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionstudent->userpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionstudent->userpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $lobjCollegemaster->fnSearchCollege( $lobjform->getValues ()); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->userpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/collegemaster/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'collegemaster', 'action'=>'index'),'default',true));
		}


	}

	public function newcollegeAction() { //Action for creating the new University
		$this->view->title="Add New University";
		$lobjcollegeForm = new GeneralSetup_Form_Collegemaster (); //intialize user lobjuserForm
		$this->view->lobjcollegeForm = $lobjcollegeForm; //send the lobjuserForm object to the view
		

		$lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$ldtsystemDate = date ( 'Y-m-d' );

		$this->view->lobjcollegeForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjcollegeForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		$lobjcountry = $lobjUser->fnGetCountryList();
		$lobjcollegeForm->Country->addMultiOptions($lobjcountry);
		$lobjcollegeForm->Countrystaff->addMultiOptions($lobjcountry);
		
		if($this->locale == 'ar_YE')  {
			$this->view->lobjcollegeForm->FromDate->setAttrib('datePackage',"dojox.date.islamic");
			$this->view->lobjcollegeForm->ToDate->setAttrib('datePackage',"dojox.date.islamic");
		}
		
		$lobjUniversity = new GeneralSetup_Model_DbTable_University(); //intialize user Model
		$lobjUniversity = $lobjUniversity->fnGetUniversityList();
		$lobjcollegeForm->AffiliatedTo->addMultiOptions($lobjUniversity);
		
		$this->lobjdeftype = new App_Model_Definitiontype();
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Levels');
		foreach($larrdefmsresultset as $larrdefmsresult)
		{
			$this->view->lobjcollegeForm->IdLevel->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			unset ( $larrformData ['Close'] );
			if ($lobjcollegeForm->isValid ( $larrformData )) {
				$lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster ();
				$result = $lobjcollegemaster->fnaddCollege ( $larrformData ); //instance for adding the lobjuserForm values to DB
				
				$lobjstaffmaster = new GeneralSetup_Model_DbTable_Staffmaster ();
				$resultstaff = $lobjstaffmaster->fnaddstaffmaster ($result,$larrformData ); //instance for adding the lobjuserForm values to DB
				
				$lobjdeanmaster = new GeneralSetup_Model_DbTable_Deanmaster ();
				$lobjdeanmaster->fnaddDean($result,$resultstaff,$larrformData ); //instance for adding the lobjuserForm values to DB
                $this->_redirect( $this->baseUrl . '/general-setup/collegemaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'collegemaster', 'action'=>'index'),'default',true));
			}
		}

	}

	public function editcollegeAction(){
    	$this->view->title="Edit College";  //title
		$lobjcollegeForm = new GeneralSetup_Form_Collegemaster (); //intialize user lobjuserForm
		$this->view->lobjcollegeForm = $lobjcollegeForm; //send the lobjuserForm object to the view
		$lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster(); //user model object
		
		$lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$lobjcountry = $lobjUser->fnGetCountryList();
		$lobjcollegeForm->Country->addMultiOptions($lobjcountry);
		$lobjstate = $lobjUser->fnGetStateList();
		$lobjcollegeForm->State->addMultiOptions($lobjstate);
		
		if($this->locale == 'ar_YE')  {
			$this->view->lobjcollegeForm->FromDate->setAttrib('datePackage',"dojox.date.islamic");
			$this->view->lobjcollegeForm->ToDate->setAttrib('datePackage',"dojox.date.islamic");
		}
		
		$lobjUniversity = new GeneralSetup_Model_DbTable_University(); //intialize user Model
		$lobjUniversity = $lobjUniversity->fnGetUniversityList();
		$lobjcollegeForm->AffiliatedTo->addMultiOptions($lobjUniversity);
		
		$lobjstaffmaster = new GeneralSetup_Model_DbTable_Staffmaster(); //intialize user Model
		$lobjstaffmaster = $lobjstaffmaster->fngetStaffMasterListforDD();
		$lobjcollegeForm->IdStaff->addMultiOptions($lobjstaffmaster);
    	$IdCollege = $this->_getParam('id', 0);
    	
	    	$larrDetails = $lobjcollegemaster->fnGetListofCollege();
	    	$lobjcollegeForm->Idhead->addMultiOptions($larrDetails);
    	

    	$result = $lobjcollegemaster->fneditCollege($IdCollege);
		foreach($result as $colresult){
		}
		
	    $this->view->collegetype=$colresult['CollegeType'];
    	if($colresult['CollegeType']== '0') {
    		$lobjcollegeForm->Idhead->addMultiOptions(array('0' => 'Select'));
    		$lobjcollegeForm->Idhead->setValue('0');
    		
    	} else if($colresult['CollegeType']== '1') {
	    	$larrDetails = $lobjcollegemaster->fnGetListofCollege();
	    	$lobjcollegeForm->Idhead->addMultiOptions($larrDetails);
    	}
    	$lobjcollegeForm->populate($colresult);	

    	
    	$lobjcollegeForm->CollegeName->removeValidator ('Db_NoRecordExists' );
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($lobjcollegeForm->isValid($formData)) {
	   			$lintIdCollege = $formData ['IdCollege'];
				$college= new GeneralSetup_Model_DbTable_Collegemaster(); 
				$college->fnupdateCollege($formData,$lintIdCollege);
				
				$lobjldeanistmodel = new GeneralSetup_Model_DbTable_Deanmaster();
				$lobjldeanistmodel->fnupdateDeanList($formData,$lintIdCollege);//update registrar
				
				$lobjldeanistmodel->fninsertDeanList($formData,$lintIdCollege);//insert new registrar
				$this->_redirect( $this->baseUrl . '/general-setup/collegemaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'collegemaster', 'action'=>'index'),'default',true));
			}
    	}
    }
    
	public function getcollegelistAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$lobjCommonModel = new App_Model_Common();
    	$lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster(); //user model object
		//Get Country Id
		$lintidCollege = $this->_getParam('coltype');
		if($lintidCollege == '1') {
			$larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjcollegemaster->fnGetListofCollege());
			echo Zend_Json_Encoder::encode($larrDetails);
		}else if($lintidCollege == '0') {
			echo '[{"name":"Select","key":"0"}]';
		}
	}

}