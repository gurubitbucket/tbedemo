<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class GeneralSetup_TakafulpaymentController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjTakafulpayment = new GeneralSetup_Model_DbTable_Takafulpayment();
		$this->lobjTakafulpaymentForm = new GeneralSetup_Form_Takafulpayment ();  	
	}
    public function indexAction()
    {
    	
    	//echo"harsha";die();
       	$this->view->title="Takaful Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjTakafulpayment->fngetTakafulDetails(); //get user details
        /*echo('<pre>');
		print_r($larrresult);die();*/
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		    $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjTakafulpayment->fnSearchTakafulPayment($larrformData['field3']); //searching the values for the user
				/*echo('<pre>');
				print_r($larrresult);
				die();*/
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->takafulpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/general-setup/takafulpayment/index');
			}
	
    }
    
    public function takafulpaymenteditAction()
    {
    	
    	$this->view->lobjTakafulpaymentForm = $this->lobjTakafulpaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	//$this->view->idcompany = $lstrType;
    	/*print_r($lstrType);
    	die();*/
    	$larrstudentname=$this->lobjTakafulpayment->fngetTakafulname($lstrType);
     /*   echo('<pre>');
    	print_r($larrstudentname);
    	die();*/
    	$this->view->takafulname = $larrstudentname['TakafulName'];
    	
    	$this->view->takafulemail = $larrstudentname['email'];
    	$this->view->progname = $larrstudentname['ProgramName'];
    	 $this->lobjTakafulpaymentForm->Amount->setValue($larrstudentname['totalAmount']);
	
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
    			$larrformData = $this->_request->getPost ();    			
    			$larrformData['IDApplication']=$lstrType;   			
    	/*		echo('<pre>');
    			print_r($larrformData);
    			die();*/
    			$regid = $this->lobjTakafulpayment->fngeneraterandom();
    			$larrpaymentdetails = $this->lobjTakafulpayment->InsertPaymentOption($larrformData,$lstrType,$regid); 
    			$result = $this->lobjTakafulpayment->sendmails($larrformData['takafulname'],$larrformData['takafulemail'],$regid);  			
    			 $this->_redirect( $this->baseUrl . '/general-setup/takafulpayment/index');
    	}
    }

}

