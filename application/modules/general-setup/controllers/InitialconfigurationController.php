<?php
class GeneralSetup_InitialconfigurationController extends Base_Base { //Controller for the initialconfiguration Module

	private $locale;
	private $registry;
	private $lobjinitialconfig;
	private $lobjuniversityModel;
	private $lobjinitialconfigForm;
	private $_gobjlogger;
	
	public function init(){ //initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj(){
		$this->lobjinitialconfig = new GeneralSetup_Model_DbTable_Initialconfiguration(); //user model object
		$this->lobjuniversityModel = new GeneralSetup_Model_DbTable_University(); //user model object
		$this->lobjinitialconfigForm = new GeneralSetup_Form_Initialconfiguration (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}

	public function indexAction() { // action for search and view
		
		$lobjform=$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		$larrresult = $this->lobjuniversityModel->fngetUniversityDetails (); //get user details
		
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->universitypaginatorresult);
			
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->universitypaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->universitypaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjinitialconfig->fnSearchUniversity ( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->universitypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/initialconfiguration/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'initialconfiguration', 'action'=>'index'),'default',true));
		}
	}

	public function initialconfiglistAction() { //Action for creating the new user
		$this->view->lobjInitialConfigForm = $this->lobjinitialconfigForm; //send the lobjuserForm object to the view
		$this->view->lobjInitialConfigModel = $this->lobjinitialconfig;
		$auth = Zend_Auth::getInstance();
		$lintiduniversity = ( int ) $this->_getParam ( 'idUniversity' );
		$lobjcountry=$this->lobjuniversityModel->fnGetCountryList();
		$this->lobjinitialconfigForm->Country->addMultiOptions($lobjcountry);
		//echo $lintiduniversity;die();
		//print_r($larrunv);die();
		$this->lobjinitialconfigForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$this->lobjinitialconfigForm->UpdDate->setValue(date('Y-m-d H:i:s'));
		$this->lobjinitialconfigForm->idUniversity->setValue($lintiduniversity);
		
		
		
		$larrCurrencyFields=$this->lobjinitialconfig->fnGetCurrencyValues();
	    /*echo('<pre>');
		print_r($larrCurrencyFields);die();*/
		$this->lobjinitialconfigForm->PaypalCurrency->addMultiOptions($larrCurrencyFields);
		$larrReceiptFields = $this->lobjinitialconfig->fnGetReceiptGenerationValues();
		$this->lobjinitialconfigForm->RegField1->addMultiOptions($larrReceiptFields);
		$this->lobjinitialconfigForm->RegField2->addMultiOptions($larrReceiptFields);
		$this->lobjinitialconfigForm->RegField3->addMultiOptions($larrReceiptFields);
		
		
		$larrStuRegFields = $this->lobjinitialconfig->fnGetSturegGenerationValues();
		$this->lobjinitialconfigForm->SturegField1->addMultiOptions($larrStuRegFields);
		$this->lobjinitialconfigForm->SturegField2->addMultiOptions($larrStuRegFields);
		$this->lobjinitialconfigForm->SturegField3->addMultiOptions($larrStuRegFields);
		
		
		$larrdropFields = $this->lobjinitialconfig->fnGetdropGenerationValues();
		$this->lobjinitialconfigForm->FldDdwn1->addMultiOptions($larrdropFields);
		$this->lobjinitialconfigForm->FldDdwn2->addMultiOptions($larrdropFields);
		$this->lobjinitialconfigForm->FldDdwn3->addMultiOptions($larrdropFields);
		$this->lobjinitialconfigForm->FldDdwn4->addMultiOptions($larrdropFields);
		$this->lobjinitialconfigForm->FldDdwn5->addMultiOptions($larrdropFields);
		$this->lobjinitialconfigForm->FldDdwn6->addMultiOptions($larrdropFields);
		
		
		$larrInitialConfigDetails = $this->lobjinitialconfig->fnGetInitialConfigDetails($lintiduniversity);
		
		
		//for schedule time change IS to MY for updating to server
		$ST = ($start = date("T"."H:i", strtotime($larrInitialConfigDetails['Scheduletime'])));		
		$lrresult = explode('IS',$ST);
		$NewST = $lrresult[1];	
		$this->view->stime = $NewST;
		//end of schedule time
		
		
		$this->view->Regcode=$larrInitialConfigDetails['RegcodeType'];
		if(count($larrInitialConfigDetails) > 0 && $larrInitialConfigDetails != ""){
			$this->lobjinitialconfigForm->populate($larrInitialConfigDetails);	
			$larrunv=$this->lobjuniversityModel->fnGetUniversityName($lintiduniversity);
			$this->lobjinitialconfigForm->Univ_Name->setValue( $larrunv['Univ_Name'] );
		}
		
		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrFormData = $this->_request->getPost();
			
			//explode of stime
			$schedulertime = explode('T',$larrFormData['Scheduletime']);
			$larrFormData['Scheduletime'] = $schedulertime[1];
			//end
			
				unset($larrFormData['Save']);
				
				$larrFormData['PassSection']=0;
				if(count($larrInitialConfigDetails) > 0 && $larrInitialConfigDetails != ""){
					if($larrFormData['RegcodeType']==0)
					{
						$larrFormData['RegField1']=Null;
						$larrFormData['RegField2']=Null;
						$larrFormData['RegField3']=Null;
						$larrFormData['Separator']=Null;
					}
					$this->lobjinitialconfig->fnregistrationdatechange($larrFormData['idConfig'],$larrFormData);
					$this->lobjinitialconfig->fnUpdateInitialconfig($larrFormData['idConfig'],$larrFormData);
					$this->lobjinitialconfig->fnupdateuniversity($larrFormData['Univ_Name']);	
				} else {
					
				if($larrFormData['RegcodeType']==0)
					{
						$larrFormData['RegField1']=Null;
						$larrFormData['RegField2']=Null;
						$larrFormData['RegField3']=Null;
						$larrFormData['Separator']=Null;
					}
					$this->lobjinitialconfig->fnAddInitialConfig($larrFormData);	
				}
				
			$auth = Zend_Auth::getInstance();
    	 	// Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the InitialConfig"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);	
			$this->_redirect( $this->baseUrl . '/general-setup/initialconfiguration/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'initialconfiguration', 'action'=>'index'),'default',true));
		}

	}
public function configchangedetailsAction() 
	{    
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//$Idapp = $this->_getParam('idapplication');		
		//$Paymentmode = $this->_getParam('paymentmode');	
         //echo $Paymentmode;die();
			
		 $larrresult = $this->lobjinitialconfig->fngetinitialconfigchangedetails();
		// echo "<pre>";
		 //print_r($larrresult);die();
		
			$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Registration Date Change Details </legend>
		</fieldset>';		
		$tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Minimum Age</b></th><th><b>Individual Closing Days</b></th><th><b>Takaful Closing Days</b></th><th><b>Company Closing Days</b></th><th><b>Edited Date</b></th><th><b>Edited User</b></th></tr>";
		 foreach($larrresult as $lobjCountry)
			{
		$tabledata.="<tr><td align = 'left'>".$lobjCountry['MinAge']."</td><td align = 'left'>".$lobjCountry['ClosingBatch']."</td><td align = 'left'>".$lobjCountry['ClosingBatchTakaful']."</td><td align = 'left'>".$lobjCountry['ClosingBatchCompany']."</td><td align = 'left'>".$lobjCountry['Changedate']."</td><td align = 'left'>".$lobjCountry['User']."</td></tr>";
		
		

		}
		
		$tabledata.="<tr align = 'left'><td colspan= '8' ><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
		$tabledata.="</table><br>";
			echo  $tabledata;
		//echo "<pre>";
		//print_r($larrresult);die();
	}
}