<?php

error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);

class GeneralSetup_TakafuloperatorController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	private $_gobjlogger;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
	
	public function fnsetObj(){
		$this->lobjcenterModel = new GeneralSetup_Model_DbTable_Center(); //user model object
		$this->lobjTakafuloperatorModel = new GeneralSetup_Model_DbTable_Takafuloperator();
		$this->lobjTakafuloperatorForm = new GeneralSetup_Form_Takafuloperator(); 
	  	$this->lobjcoursemaster = new GeneralSetup_Model_DbTable_Coursemaster();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
	  	$this->lobjTakafultypepaymentModel = new Finance_Model_DbTable_Takafultypepayment();
		
	}
	
	public function indexAction() {
		$this->view->title="Takaful Operator";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjTakafuloperatorModel->fngetTakafulOperator(); //get user details
		//echo "<pre>";print_r($larrresult);die();
		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->takafulpaginatorresult);
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->takafulpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->takafulpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjTakafuloperatorModel->fnSearchTakafulOperator($this->lobjform->getValues()); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->takafulpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/takafuloperator/index');
		}
	}
	
	public function newtakafuloperatorAction() { //title
		$this->view->title="Add New Profram";
		$this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm;
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjTakafuloperatorForm->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjTakafuloperatorForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$lobjcountry = $this->lobjcenterModel->fnGetCountryList();
		$this->lobjTakafuloperatorForm->country->addMultiOptions($lobjcountry);
		$lobjbusiness = $this->lobjTakafuloperatorModel->fnGetBusinesstypeList();
		$this->lobjTakafuloperatorForm->Businesstype->addMultiOptions($lobjbusiness);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			unset($formData['Save']);
			unset($formData['Back']);
			$password = $formData['Password'];
			$formData['Password'] = md5($formData['Password']);
			$formData['Takafultype']=1;
			$lintlastId=$this->lobjTakafuloperatorModel->fnaddtakafuloperator($formData);
			$larrformdata['idtakafuloperator']=$lintlastId;
			$larrformdata['paymenttype']=181;
			$larrformdata['upduser']= $auth->getIdentity()->iduser;
			$larrformdata['upddate']= $ldtsystemDate = date('Y-m-d H:i:s');
			$this->lobjTakafultypepaymentModel->fninsertdata($larrformdata);
			$auth = Zend_Auth::getInstance();
			$priority=Zend_Log::INFO;// Write Logs
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Takaful Operator"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
			$this->_redirect( $this->baseUrl . '/general-setup/takafuloperator/index');
		}
	}
    
	public function edittakafuloperatorAction(){
		$this->view->title="Edit Takaful Operatpr";  //title
		$this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm; //send the lobjuniversityForm object to the view
		$lobjcountry = $this->lobjcenterModel->fnGetCountryList();
		$this->lobjTakafuloperatorForm->country->addMultiOptions($lobjcountry);
		$lobjbusiness = $this->lobjTakafuloperatorModel->fnGetBusinesstypeList();
		$this->lobjTakafuloperatorForm->Businesstype->addMultiOptions($lobjbusiness);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjTakafuloperatorForm->UpdDate->setValue ( $ldtsystemDate );
		$idtakafuloperator = $this->_getParam('id');
		$larrtakafuloperator = $this->lobjTakafuloperatorModel->fnGetTakafulDetails($idtakafuloperator);
		$lobjCommonModel = new App_Model_Common();
		$larrpermCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larrtakafuloperator['country']);//Get States List
		$this->lobjTakafuloperatorForm->state->addMultiOptions($larrpermCountyrStatesList);
		$this->lobjTakafuloperatorForm->city->addMultiOptions(array('0'=>'others'));
		$this->lobjTakafuloperatorForm->city->addMultiOptions($this->lobjTakafuloperatorModel->fnGetcityList($larrtakafuloperator['state']));
		$arrworkphone = explode("-",$larrtakafuloperator['workphone']);
		unset($larrtakafuloperator['workphone']);
		$this->lobjTakafuloperatorForm->populate($larrtakafuloperator);
		$this->view->lobjTakafuloperatorForm->workcountrycode->setValue ( $arrworkphone [0] );
		$this->view->lobjTakafuloperatorForm->workstatecode->setValue ( $arrworkphone [1] );
		$this->view->lobjTakafuloperatorForm->workphone->setValue ( $arrworkphone [2] );
		$this->view->lobjTakafuloperatorForm->Password->setAttrib('readonly',true);
		$this->lobjTakafuloperatorForm->Password->setValue ( $larrtakafuloperator['Password']);
		$this->view->lobjTakafuloperatorForm->Password->renderPassword = true;
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			if ($this->lobjTakafuloperatorForm->isValid($formData)) {
				unset($formData['Save']);
				unset($formData['Back']);
				if($formData['NewPassword']!= '')
				{
					$password = $formData['NewPassword'];
					$formData['Password'] = md5($password);
					$formData['Takafultype']=1;
					$lintidtakafuloperator = $formData['idtakafuloperator'];
					unset($formData['NewPassword']);
					$this->lobjTakafuloperatorModel->fnupdatetakafuloperator($formData,$lintidtakafuloperator);//update university
				}
				else
				{
					unset($formData['NewPassword']);
					$formData['Takafultype']=1;
					$lintidtakafuloperator = $formData['idtakafuloperator'];
					$this->lobjTakafuloperatorModel->fnupdatetakafuloperator($formData,$lintidtakafuloperator);//update university
				}
				$auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;// Write Logs
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Edited the Takaful Operator with id = ".$idtakafuloperator."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				$this->_redirect( $this->baseUrl . '/general-setup/takafuloperator/index');
			}
		}
	}
	
	public function getcountrystateslistAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetCountryStateList($lintIdCountry));
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	public function getstafflistAction(){//Action To Get List Of States From Country Id
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();//Get Country Id
		$idstaff =$this->_getParam('idStaff');
		$larrStaffDetails = $this->lobjuser->getstaffdetails($idstaff);
		echo Zend_Json_Encoder::encode($larrStaffDetails);
	}
		
	public function getcitylistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjcenter->fnGetStateCityList($lintIdCountry));//get all the city that correspond to the selected state
		$larrCountryStatesDetails[]=array('key'=>'0',name=>'Others');//if the key is 0 set city as others
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}
}