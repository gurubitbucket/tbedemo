<?php
class GeneralSetup_CoursemasterController extends Base_Base {
	private $lobjprogrammaster;
	private $lobjcoursemasterForm;
	private $_gobjlogger;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
	
	public function fnsetObj(){
		$this->lobjcoursemaster = new GeneralSetup_Model_DbTable_Coursemaster();
		$this->lobjcoursemasterForm = new GeneralSetup_Form_Coursemaster (); //intialize user lobjuniversityForm
		
	}
	
	public function indexAction() {
    	$this->view->title="Course Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjcoursemaster->fngetCoursemasterDetails (); //get user details
		
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->coursemasterpaginatorresult);
		
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$ProgramArray = $this->lobjcoursemaster->fnProgramArray();		
		$this->view->lobjform->field5->addMultioption('0','Select');
		$this->view->lobjform->field5->addMultioptions($ProgramArray);
		if(isset($this->gobjsessionsis->coursemasterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->coursemasterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjcoursemaster->fnSearchCourse ( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->coursemasterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/coursemaster/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'coursemaster', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newcourseAction() { //title
    	$this->view->title="Add New Course";
		
		$this->view->lobjcoursemasterForm = $this->lobjcoursemasterForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjcoursemasterForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjcoursemasterForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		$ProgramArray = $this->lobjcoursemaster->fnProgramArray();
		$this->view->lobjcoursemasterForm->IdProgrammaster->addMultioptions($ProgramArray);
		if($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$this->lobjcoursemaster->fnaddCourse($formData);
				
				$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Completed the Company payment"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$larrstudentname."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
				$this->_redirect( $this->baseUrl . '/general-setup/coursemaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'coursemaster', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
    
	public function editcourseAction(){
    	$this->view->title="Edit Course";  //title
		$this->view->lobjcoursemasterForm = $this->lobjcoursemasterForm; //send the lobjuniversityForm object to the view
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjcoursemasterForm->UpdDate->setValue ( $ldtsystemDate );		
    	$IdCourse = $this->_getParam('id', 0);
    	$result = $this->lobjcoursemaster->fetchAll('IdCoursemaster ='.$IdCourse);
    	$result = $result->toArray();
    	$ProgramArray = $this->lobjcoursemaster->fnProgramArray();
		$this->view->lobjcoursemasterForm->IdProgrammaster->addMultioptions($ProgramArray);
		foreach($result as $courseresult){
		}
    	$this->lobjcoursemasterForm->populate($courseresult);	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjcoursemasterForm->isValid($formData)) {
	   			$lintIdCoursemaster = $formData ['IdCoursemaster'];
				$this->lobjcoursemaster->fnupdateCourse($formData,$lintIdCoursemaster);//update university
				
				$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Completed the Company payment"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$larrstudentname."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
				$this->_redirect( $this->baseUrl . '/general-setup/coursemaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'coursemaster', 'action'=>'index'),'default',true));
			}
    	}
    }
}