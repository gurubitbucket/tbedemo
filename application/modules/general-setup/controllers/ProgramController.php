<?php
class GeneralSetup_ProgramController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	private $_gobjlogger;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function fnsetObj(){
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjprogramForm = new GeneralSetup_Form_Program (); 
	  	$this->lobjcoursemaster = new GeneralSetup_Model_DbTable_Coursemaster();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
		
	}
	
	public function indexAction() {
    	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjprogram->fngetProgramDetails (); //get user details
		
	
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->programpaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->programpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->programpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjprogram->fnSearchProgram ( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->programpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/general-setup/program/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newprogramAction() { //title
    	$this->view->title="Add New Profram";
		$this->view->lobjprogramForm = $this->lobjprogramForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
/*		$lobjcourse = $this->lobjcoursemaster->fnGetCourseList();
		$this->lobjprogramForm->IdCourseMaster->addMultiOptions($lobjcourse);
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Award');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjprogramForm->Award->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Learning Mode');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjprogramForm->LearningMode->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}*/
		$this->view->lobjprogramForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjprogramForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$formData['IdCourseMaster']=1;
				$formData['LearningMode']=1;
				$formData['Award']=1;
				$larrresult = $this->lobjprogram->fnaddProgram($formData);
				$lintidprog = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_program','IdProgram');
				/* print_R($lintidprog);
				   die();*/
				  if($formData['Active']==1)
				  {
				  	$larrupdateprog = $this->lobjprogram->updateactive($lintidprog);
				  }
				// Write to Log
			    $auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Program"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/general-setup/program/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'program', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
    
	public function editprogramAction(){
    	$this->view->title="Edit Program";  //title
		$this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjprogramForm->UpdDate->setValue ( $ldtsystemDate );		
		$lobjcourse = $this->lobjcoursemaster->fnGetCourseList();
		$this->lobjprogramForm->IdCourseMaster->addMultiOptions($lobjcourse);
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Award');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjprogramForm->Award->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Learning Mode');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjprogramForm->LearningMode->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
    	$IdProgram = $this->_getParam('id', 0);
    	$result = $this->lobjprogram->fetchAll('IdProgram ='.$IdProgram);
    	$result = $result->toArray();
		foreach($result as $programresult){
		}
    	$this->lobjprogramForm->populate($programresult);	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjprogramForm->isValid($formData)) {
	   			$lintIdProgram = $formData ['IdProgram'];
				$larrresults = $this->lobjprogram->fnupdateProgram($formData,$lintIdProgram);//update university
	    	    if($formData['Active']==1)
				  {
				  	$larrupdateprog = $this->lobjprogram->updateactive($lintIdProgram);
				  }
				  // Write to Log
			    $auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the Program with Id = ".$IdProgram."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/general-setup/program/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'program', 'action'=>'index'),'default',true));
			}
    	}
    }
}