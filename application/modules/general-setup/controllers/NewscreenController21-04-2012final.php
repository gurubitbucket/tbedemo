<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class GeneralSetup_NewscreenController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	public function fnsetObj()
	{	
		$this->lobjnewscreenmodel = new GeneralSetup_Model_DbTable_Newscreen(); //intialize newscreen db object
		$this->lobjnewscreenForm = new GeneralSetup_Form_Newscreen(); 
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
       //$this->lobjstudentForm = new App_Form_Studentapplication();
	}
	
	public function indexAction() 
	{
		$this->view->lobjform = $this->lobjnewscreenForm;
		$larrresult = $this->lobjnewscreenmodel->fngetstudentinformation(); 
		/*echo "<pre>";
		print_r($larrresult);
		die();*/
		
		$larrCourseresult = $this->lobjnewscreenmodel->fnGetCourseNames();	
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrCourseresult);
		$larrVenuesresult = $this->lobjnewscreenmodel->fnGetVenueNames();	
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrVenuesresult);
		$larrTakafulresult = $this->lobjnewscreenmodel->fnGetTakafulNames();	
		$this->lobjnewscreenForm->Takafulname->addMultiOptions($larrTakafulresult);
		
	if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->studentchangevenuepaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->studentchangevenuepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->studentchangevenuepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				/*echo "<pre>";
				print_r($larrformData);
				die();*/
				$larrresult = $this->lobjnewscreenmodel->fnSearchStudent($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->studentchangevenuepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/general-setup/newscreen/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
		
		
		
		
		
		
	}
	
	public function editstudentinfoAction() 
	{	
		$this->view->lobjnewscreenForm = $this->lobjnewscreenForm;
		
		$lintidstudent = $this->_getParam('id');
		$this->view->id = $lintidstudent;
		//echo $lintidstudent;die();
		$larrstudentinforesult= $this->lobjnewscreenmodel->fngetstudenteachinformation($lintidstudent); 
		//echo "<pre>";
		//print_r($larrstudentinforesult);
		//die();
		$venuechange=$larrstudentinforesult['VenueChange'];
		$this->view->Fname=$larrstudentinforesult['FName'];
		$this->view->EmailAddress=$larrstudentinforesult['EmailAddress'];
		$this->view->ICNO=$larrstudentinforesult['ICNO'];
		$this->view->AppliedDate=$larrstudentinforesult['AppliedDate'];
		
		$larrcourse = $this->lobjnewscreenmodel->fnGetCourseNames();
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrcourse);
		$this->view->lobjnewscreenForm->Coursename->setValue($larrstudentinforesult['IdProgrammaster']);
		//$this->view->lobjnewscreenForm->Coursename->setValue();
				
					
		$larrscheduler = $this->lobjnewscreenmodel->fnGetSchedulerDetails();
		$this->lobjnewscreenForm->schedulerename->addMultiOptions($larrscheduler);
		$this->view->lobjnewscreenForm->schedulerename->setValue($larrstudentinforesult['Year']);
		
			
		$larrstate = $this->lobjnewscreenmodel->fnGetStateName();
		$this->lobjnewscreenForm->examstate->addMultiOptions($larrstate);
		
		$this->view->lobjnewscreenForm->examstate->setValue($larrstudentinforesult['ExamState']);
		
		$larrcity = $this->lobjnewscreenmodel->fnGetCityName($larrstudentinforesult['ExamCity']);
		$this->lobjnewscreenForm->examcity->addMultiOptions($larrcity);
		$this->view->lobjnewscreenForm->examcity->setValue($larrstudentinforesult['ExamCity']);
			
			
		$larrvenue = $this->lobjnewscreenmodel->fnGetVenueName($larrstudentinforesult['ExamCity']);
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrvenue);
		$this->view->lobjnewscreenForm->Venues->setValue($larrstudentinforesult['Examvenue']);
			
		$larrschedulersession = $this->lobjnewscreenmodel->fnGetSchedulerSessionDetails($larrstudentinforesult['Examsession']);
		$this->lobjnewscreenForm->examsession->addMultiOptions($larrschedulersession);
		$this->view->lobjnewscreenForm->examsession->setValue($larrstudentinforesult['Examsession']);
			
		$larrscheduleryear = $this->lobjnewscreenmodel->fnGetSchedulerYearDetails($larrstudentinforesult['Year']);
		$examdate=$larrstudentinforesult['Examdate'].'-'.$larrstudentinforesult['Exammonth'].'-'.$larrscheduleryear['year'];
		$this->view->lobjnewscreenForm->Examdate->setValue($examdate);
			
		$this->view->lobjnewscreenForm->paymentmode->setValue($larrstudentinforesult['ModeofPayment']);
		$this->view->lobjnewscreenForm->Payment->setValue($larrstudentinforesult['Payment']);
		$this->view->lobjnewscreenForm->Coursename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->Coursename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->schedulerename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examstate->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examcity->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->Venues->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examsession->setAttrib('readonly','true');
												$this->view->lobjnewscreenForm->Examdate->setAttrib('readonly','true');
				                              $this->view->lobjnewscreenForm->paymentmode->setAttrib('readonly','true');
												$this->view->lobjnewscreenForm->Payment->setAttrib('readonly','true');
				
				$larrcourse = $this->lobjnewscreenmodel->fnGetCourseNames();
		$this->lobjnewscreenForm->Coursename1->addMultiOptions($larrcourse);
		
		
		//$auth = Zend_Auth::getInstance();
		//$this->view->lobjCompanypaymentForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
	if ($this->_request->isPost() && $this->_request->getPost('Yes')) {
			$larrformData = $this->_request->getPost();
			
				$auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
				
				$idvenue = $larrformData['idvenues'];		
			$arrworkphone = explode("-",$idvenue);
		      $idcenter= $arrworkphone [0];
	          $idsession= $arrworkphone[1];	
				//echo $iduser;die();
				//echo "<pre/>";
			//echo  $idcenter;
			//echo  $idsession;
			//	print_r($larrformData);
				//die();
				$venuechange=$venuechange+1;
		$larrformData['UpdDate']=date('Y-m-d H:i:s');
			$idapplication = $larrformData['IDApplication'];
  			 $scheduleryear = $this->lobjnewscreenmodel->fngetyearforthescheduler($larrformData['schedulerename1']);
  			 	$this->lobjnewscreenmodel->fnUpdateStudentnewscreen($idapplication,$larrformData,$scheduleryear['Year'],$iduser,$idcenter,$idsession,$venuechange);
  		






	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    						
					//$this->view->mess = "Payment Completed Sucessfully";
					//$this->view->mess = "Payment Completed Sucessfully <br/> Please check your mail box If you have not received a confirmation mail in next 30minutes<br/>Please check your spam folder Add ibfiminfo@gmail.com to the address book to ensure future communications doesnt go to the spam folder";
					$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($idapplication);	
					$larrregid = $this->lobjstudentmodel->fngetRegid($idapplication);
				//	print_r($larrregid);die();
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
									//print_R($larrresult);die();
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
										 $lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
										//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'].'('.$larrresult['ampmstart'].'--'.$larrresult['ampmend'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
									/*	$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email ='shashi991988@gmail.com'; //$larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
									//$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									//echo '<script language="javascript">alert("Because of server problem mails cannot be send this time")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
								
								 //$this->_redirect( $this->baseUrl . "/registration/index");
								if(mess){
									
								}
					
    			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    		




  			 		$this->_redirect( $this->baseUrl . '/general-setup/newscreen/index');		
  		//echo "<pre>";
  		//print_r($larrformData);
  		//die();
  		}
			
		
	}
	
 public function fngetyearAction()
 {
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjnewscreenmodel->fnGetYearlistforcourse($Program);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
 }
 
 
 public function fngetstatenameAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
			$year= $this->_getParam('year');

		$larrvenuetimeresult = $this->lobjnewscreenmodel->fnGetStatelistforcourse($Program,$year);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}

public function fngetcitynamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('idstate');
		$Program = $this->_getParam('Program');
		$idseched = $this->_getParam('idsecheduler');

		$larrvenuetimeresults = $this->lobjnewscreenmodel->fnGetCitylistforcourse($lintdate,$Program,$idseched);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}


public function fngetvenuenamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('idcity');
		$Program = $this->_getParam('Program');
		$idseched = $this->_getParam('idsecheduler');

		$larrvenuetimeresults = $this->lobjnewscreenmodel->fnGetVenuelistforcourse($lintdate,$Program,$idseched);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}


public function fngetmonthnameAction()
{
	 $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
			$year= $this->_getParam('year');

		$larrvenuetimeresult = $this->lobjnewscreenmodel->fnbetweenmonths($year);
		//print_r($larrvenuetimeresult);
		//die();
		
		$frommonth = $larrvenuetimeresult['From'];
		$tomonth = $larrvenuetimeresult['To'];
		$year = $larrvenuetimeresult['Year'];
		$curyear=date('Y');
		if($curyear==$year)
		{
			$curmonth=date('m');
			
			if($frommonth<=$curmonth)
			{
				//echo $frommonth;die();
			$larrresults = $this->lobjnewscreenmodel->fnGetmonthsbetween2($tomonth);
			}
			else 
			{
				$larrresults = $this->lobjnewscreenmodel->fnGetmonthsbetween($frommonth,$tomonth);
			}
			}
		else 
		{
		$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween($frommonth,$tomonth);
		}
		//$larrmonthslist = $this->lobjstudentmodel->fnGetMonthlistofcourse($from,$to);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresults);
		//$larrCountryStatesDetails[]=array('key'=>'0','name'=>'Entire Calender');
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}


public function caleshowmonthAction()
{
	$this->lobjstudentmodel = new App_Model_Studentapplication(); 
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$idmonth = $this->_getParam('idmonth');
		$no = $this->_getParam('no');
		$year = $this->_getParam('Year');
		$Program = $this->_getParam('Program');
		$applieddate = $this->_getParam('applieddate');
		$applieddate2=explode('-',$applieddate);
		//echo $applieddate2[0];
		//echo $applieddate2[1];
		//echo $applieddate2[2];die();
		
		$applied=$applieddate2[2].'-'.$applieddate2[1].'-'.$applieddate2[0];
		//echo $applied;die();
		$larrdaysarresult = $this->lobjstudentmodel->fnGetDaysforcourse($Program,$year);
		//echo "<pre/>";
		//print_R($larrdaysarresult);
		for($i=0;$i<count($larrdaysarresult);$i++)
		{
			$days[$i]=$larrdaysarresult[$i]['Days'];
		}
		//print_R($days);
		//die();
	
		$larrmonthresult = $this->lobjstudentmodel->fnGetMonths($year,$Program);		
		$frommonth = $larrmonthresult[0]['From'];
		$tomonth = $larrmonthresult[0]['To'];
		$yearss = $larrmonthresult[0]['Year'];
		//print_r($frommonth);
		//print_r($tomonth);
		//die();
		$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<7;$j++)
		{
			if($days[$j]==1)
			  $monday=1;
			 if($days[$j]==2)
			  $tuesday=1;
			 if($days[$j]==3)
			  $wednesday=1;
			  if($days[$j]==4)
			  $thursday=1;
			  if($days[$j]==5)
			  $friday=1;
			  if($days[$j]==6)
			  $saturday=1;
			  if($days[$j]==7)
			  $sunday=1;
		}
		/*$monday =empty($larrdaysarresult[0]['Days'])? '0':'1';
		$tuesday = empty($larrdaysarresult[1]['Days'])?'0':'1';
		$wednesday = empty($larrdaysarresult[2]['Days'])?'0':'1';
		$thursday = empty($larrdaysarresult[3]['Days'])?'0':'1';
		$friday = empty($larrdaysarresult[4]['Days'])?'0':'1';
		$saturday = empty($larrdaysarresult[5]['Days'])?'0':'1';
		$sunday = empty($larrdaysarresult[6]['Days'])?'0':'1';*/
		
/*		
		
                                  $monday = $this->monday;
                                  $tuesday = $this->tuesday;
                                  $wednesday = $this->wednesday;
                                  $thursday = $this->thursday;
                                  $friday = $this->friday;
                                  $saturday = $this->saturday;
                                   $sunday = $this->sunday;
                                   
                    */  
		 $curmonth = date('m');      
$monat=date('n');
$jahr=$yearss;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0  width=25% align=center>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{
	$cnt++;
		//print_r($cnt);  
	if($idmonth==$cnt)
	{
		
	
		if($idmonth==$curmonth)
		{
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				
				$curdate = date('d')+14;
				if($i<$curdate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
				//if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				else {
				
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
		else
			 {
			$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				if($cnt==4)
				{
						//$curdate = date('d')+14;
						$i=1;
						while($i<=$insgesamt)
						{
						$rest=($i+$erster-1)%7;
						if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
						else{echo '<td  align=center ';}
						//$curdate = date('d')+14;
						if($i<12)
						{
							 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{}
							echo "</td>\n";
						}
								
						else 
						
						{
						if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
						else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else{echo $i;}
						echo "</td>\n";
						}
						if($rest==0){echo "</tr>\n<tr>\n";}
						$i++;
						}
						
				}
				else 
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
      ///////
     }
    
}
echo '</tr>';
}
echo '</table>';
}


public function schedulerexceptionAction()
{
	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintday = $this->_getParam('day');//city
		$lintcity = $this->_getParam('city');
		$lintmonth = $this->_getParam('month');
		$days = '2012-'.$lintmonth.'-'.$lintday;
		$resultsss = $this->lobjnewscreenmodel->fngetschedulerexception($days,$lintcity);
	$counts = count($resultsss);
	if($counts>1)
	{
		echo "No exams are offerred on the selected date. It can be a public holiday, please select a different date.";
		die();
		
	}
	else 
	{
		
	}
	
}

public function tempdaysAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day = $this->_getParam('day');
		$year = $this->_getParam('year');
		$month = $this->_getParam('month');
		$city = $this->_getParam('city');
		$dateid = $day.''.$month;
		
		
		$idsechduler=$this->lobjnewscreenmodel->fnGetVenuedetailsgetsecid($year);
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjnewscreenmodel->fnGetVenuedetailsRemainingseats($idsechduler['Year'],$idsechduler['idnewscheduler'],$city,$month,$day);
		//print_r($venueselect);die();
		$flag=0;
		foreach($venueselect as $ven)
		{
			if($ven['rem']>0)
			{
				$flag=1;
			}
			
		}
		echo $flag;die();
		//print_r($venueselect);die();
     /*  $larrresultinserted = $this->lobjstudentmodel->fngetdetails();
       
		$id = $larrresultinserted[0]['dateid'];
		
		
		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetTempDays($day,$year,$month,$dateid);
		$lintidstudenttempday = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studenttempday','idstudenttempday');
		
		$larrstudenttempdetials = $this->lobjstudentmodel->fngetstudenttempdays($lintidstudenttempday);
		
		
		
		 $larrdeleteddata= $this->lobjstudentmodel->fnDeleteTempDetails($larrstudenttempdetials[0]['dateid']);
		
		echo $id;*/
}


public function fngetvenuesessiondetailsAction()
	{
		$this->lobjstudentmodel = new App_Model_Studentapplication(); 
		     $this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
	
			//Get Country Id
			$Year= $this->_getParam('Year');
			//$Program = $this->_getParam('Program');
			$idcity = $this->_getParam('idcity');
			$venue = $this->_getParam('venue');
			$day = $this->_getParam('day');
			$month = $this->_getParam('month');
	//echo $day."<br>";
	//	echo $Year."<br>";
		//	echo $idcity."<br>";
			//echo $venue."<br>";
			//echo $month."<br>";die();
		$idsechduler=$this->lobjnewscreenmodel->fnGetVenuedetailsgetsecid($Year);
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjnewscreenmodel->fnGetVenuedetailsRemainingseats($idsechduler['Year'],$idsechduler['idnewscheduler'],$idcity,$month,$day);
		$flag=0;
		$idsession=0;
		$venueid=0;
		//$idsession="";
		foreach($venueselect as $ven)
		{
			if($ven['rem']>0)
			{
				$flag=1;
			}
			if($ven['rem']==0 && ($ven['idcenter']==$venue))
			{
				//$idsession=$ven['idmangesession'];
				 $idsession=$idsession.','.$ven['idmangesession'];
				// $idsessions=$ven['idmangesession'];
			}

			if($ven['rem']<0 && ($ven['idcenter']==$venue))
			{	
				$idsession=$idsession.','.$ven['idmangesession'];
			} 
			
			
		}
	//	echo $idsession;die();
			$venueselect = $this->lobjnewscreenmodel->fnGetsesssiondetails($Year,$idcity,$venue,$idsession,$day,$month);
			
			$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($venueselect);
			echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
	}
	
	
	
	
	public function selectvenueAction()
	{
		
		$this->_helper->layout->disableLayout();
		if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post		
			$idvenue = $larrformData['idvenues'];		
			$arrworkphone = explode("-",$idvenue);
		      $idcenter= $arrworkphone [0];
	          $idsession= $arrworkphone[1];			
			
		$alredyidthere=$this->lobjstudentmodel->fncheckStudentPaymentdetails($larrformData['idapplication']);	
			if($alredyidthere)	
			{
				echo '<script language="javascript">alert("You already registered once")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/introduction/index';</script>";
                	die();
			}
			else 
			{ 

				$larrresuls = $this->lobjstudentmodel->fnupdateexamvenuess($idcenter,$idsession,$larrformData['idapplication']);
                        $larrpaymentmode=$this->lobjstudentmodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$larrformData['idapplication']);		
			
			}
		    
			
		}
		$this->view->lobjstudentForm = $this->lobjnewscreenForm; //send the lobjuserForm object to the view
		
		
		
		$day = $this->_getParam('day');
		$year = $this->_getParam('year');
		$month = $this->_getParam('month');
		$city = $this->_getParam('city');
		//echo "<pre/>";
		//print_r($larrresult7);die();
		$idsechduler=$this->lobjnewscreenmodel->fnGetVenuedetailsgetsecid($year);
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjnewscreenmodel->fnGetVenuedetailsRemainingseats($idsechduler['Year'],$idsechduler['idnewscheduler'],$city,$month,$day);
	//echo "<pre/>";
	 //	print_r($venueselect);
	 //die();
		
		$larrdate=$idsechduler['Year'].'-'.$month.'-'.$day;
		//echo $larrdate;die();
		//$contvenueselect = $this->lobjstudentmodel->fnCountVenuedetails($larrdate);
		
		$this->view->dates = $day.'-'.$month.'-'.$idsechduler['Year'];
		$dates=$idsechduler['Year'].'-'.$month.'-'.$day;
		
		//$datesel=".$dates.";
		//	$day=day($dates);
		$result5 = $this->lobjstudentmodel->fngetdayStudent($dates); 
		//echo $result5[0]['days'];die();
		$this->view->daystu= $result5[0]['days'];
		$this->view->larrvenues = $venueselect;
		
		
	}
	///////////////////
	
 
	
	
}