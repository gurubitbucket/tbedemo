<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class GeneralSetup_StudenteditController extends Base_Base {
	private $lobjprogram;
	private $lobjStudenteditForm;
	private $lobjStudenteditModel;
	private $lobjdeftype;
	public $gsessionregistration;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function fnsetObj(){
		$this->lobjStudenteditModel = new GeneralSetup_Model_DbTable_Studentedit();
		$this->lobjStudenteditForm = new GeneralSetup_Form_Studentedit();
		$this->lobjstudentmodel = new App_Model_Studentapplication();
		$this->lobjdeftype = new App_Model_Definitiontype();
				$this->lobjCommon=new App_Model_Common();
	  	
		
	}
	
	public function indexAction() {
		$this->gsessionregistration->mails=0;
    	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		//$larrresult = $this->lobjStudenteditModel->fnviewstudentdetailssss(); //get user details
	$larrresult=array();
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->programpaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->programpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->programpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjStudenteditModel->fnSearchStudent($this->lobjform->getValues()); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->programpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/general-setup/studentedit/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function editstudentAction() { //title
			$this->view->lobjStudenteditForm = $this->lobjStudenteditForm;
		
		
		$larrstate = $this->lobjstudentmodel->fnGetStateName();
		$this->lobjStudenteditForm->State->addMultiOptions($larrstate);
		
		$larrQualification = $this->lobjstudentmodel->fnGetEducationDetails();
		$this->lobjStudenteditForm->Qualification->addMultiOptions($larrQualification);
		
	   $larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Race');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjStudenteditForm->Race->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}		
		
			$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		$this->view->lobjStudenteditForm->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
		$this->view->lobjStudenteditForm->UpdUser->setValue($iduser);
			
	$lintidstudent = $this->_getParam('id');
	$this->view->id = $lintidstudent;
	$larrresults = $this->lobjStudenteditModel->fneditdetails($lintidstudent);
	
	
	$companytype = $larrresults['batchpayment'];
	$this->view->registeredthrough = $companytype;
	if($companytype==1)
	{
		$larrtakafuldetails = $this->lobjStudenteditModel->fngetCompanydetails($larrresults['RegistrationPin']);
		$companytypename = $larrtakafuldetails['CompanyName'];
		$this->view->companyname = $companytypename;
		$this->view->registrationpins = $larrresults['RegistrationPin'];
		
	}
	else if($companytype==2)
	{
		$larrtakafuldetails = $this->lobjStudenteditModel->fngetTakafuldetails($larrresults['RegistrationPin']);
		$companytypename = $larrtakafuldetails['TakafulName'];
		$this->view->companyname = $companytypename;
			$this->view->registrationpins = $larrresults['RegistrationPin'];
	}
	if($larrresults['PostalCode']=='')
	{
		$larrresults['PostalCode']=0;
	}
	if($larrresults['ContactNo']=='')
	{
		$larrresults['ContactNo']=0;
	}
	if($larrresults['MobileNo']=='')
	{
		$larrresults['MobileNo']=0;
	}
	
 	$this->view->lobjStudenteditForm->FName->setValue($larrresults['FName']);
	$this->view->lobjStudenteditForm->MName->setValue($larrresults['MName']);
	$this->view->lobjStudenteditForm->LName->setValue($larrresults['LName']);
	$this->view->lobjStudenteditForm->DateOfBirth->setValue($larrresults['DateOfBirth']);
	$this->view->lobjStudenteditForm->PermCity->setValue($larrresults['PermCity']);
	$this->view->lobjStudenteditForm->EmailAddress->setValue($larrresults['EmailAddress']);		 	
	//$this->view->lobjStudenteditForm->Takafuloperator->setValue($larrresults['Takafuloperator']);
	$this->view->lobjStudenteditForm->ICNO->setValue($larrresults['ICNO']);
	$this->view->lobjStudenteditForm->PermAddressDetails->setValue($larrresults['PermAddressDetails']);	
	$this->view->lobjStudenteditForm->Gender->setValue($larrresults['Gender']);	
	$this->view->lobjStudenteditForm->CorrAddress->setValue($larrresults['CorrAddress']);	
	$this->view->lobjStudenteditForm->ArmyNo->setValue($larrresults['ArmyNo']);	
	$this->view->lobjStudenteditForm->State->setValue($larrresults['State']);
	$this->view->lobjStudenteditForm->PostalCode->setValue($larrresults['PostalCode']);
	$this->view->lobjStudenteditForm->Race->setValue($larrresults['Race']);
    $this->view->lobjStudenteditForm->ContactNo->setValue($larrresults['ContactNo']);
    $this->view->lobjStudenteditForm->MobileNo->setValue($larrresults['MobileNo']);
    $this->view->lobjStudenteditForm->login->setValue($larrresults['EmailAddress']);
    $this->view->lobjStudenteditForm->password->setValue($larrresults['ICNO']);
       
    
    $this->view->programs = $larrresults['ProgramName'];
    $this->view->passstatus = $larrresults['pass'];
    $this->view->companytypename = $companytypename;
    if($larrresults['pass']==1)
    {
    	$Pass = 'Pass';
    }
    else if($larrresults['pass']==2)
    {
    	$Pass='Fail';
    }
	 else if($larrresults['pass']==4)
    {
    	$Pass='Absent';
    }
	 else if($larrresults['pass']==3)
    {
    	$Pass='Applied';
    }
 	$this->view->pass = $Pass;
 	
 	$this->view->centernames = $larrresults['centername'];
 	$this->view->examdate = $larrresults['DateTime'];
 	
 	$this->view->idapplication = $larrresults['IDApplication'];
 	$this->view->managesession = $larrresults['managesessionname'].'('.$larrresults['starttime'].'--'.$larrresults['endtime'].')';
    
    
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			
			$idapplication = $larrformData['studid'];
			
				$larrresultupdate = $this->lobjStudenteditModel->fnAddStudent($larrformData,$idapplication);
			if($larrformData['Cmptype']=='')
			{
				
			}
			else 
			{
				
			$larrresultupdate = $this->lobjStudenteditModel->fninsertstudentchangeddetails($larrformData,$idapplication);
			}
			
    	   

			 
				
				 $this->_redirect($this->baseUrl . '/general-setup/studentedit/index');
					
        }     
    }
    
    public function fnajaxgettakcompnamesAction()
    {
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidname = $this->_getParam('idname');
		$larrcenters = $this->lobjStudenteditModel->fnajaxgetcenternames($lintidname);
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);
		$larrcentreDetailss[]=array('key'=>'0','name'=>'Select');
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
    }
    
	
}