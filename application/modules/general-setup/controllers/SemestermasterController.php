<?php
class GeneralSetup_SemestermasterController extends Base_Base {
	private $lobjsemestermaster;
	private $lobjsemestermasterForm;
	
	public function init() {
		$this->fnsetObj();
	}
	
	public function fnsetObj(){
		$this->lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster();
		$this->lobjsemestermasterForm = new GeneralSetup_Form_Semestermaster(); //intialize user lobjuniversityForm
		
	}
	
	public function indexAction() {
    	$this->view->title="Semester Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjsemestermaster->fngetSemestermasterDetails (); //get user details
		$lintpagecount = "5";// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semetsermasterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semetsermasterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjsemestermaster->fnSearchSemester ( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->semetsermasterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'semestermaster', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newsemesterAction() { //title
    	$this->view->title="Add New Semester";
		
		$this->view->lobjsemestermasterForm = $this->lobjsemestermasterForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjsemestermasterForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjsemestermasterForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$this->lobjsemestermaster->fnaddSemester($formData);
				$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'semestermaster', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
    
	public function editsemesterAction(){
    	$this->view->title="Edit Semester";  //title
		$this->view->lobjsemestermasterForm = $this->lobjsemestermasterForm; //send the lobjuniversityForm object to the view
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjsemestermasterForm->UpdDate->setValue ( $ldtsystemDate );		
    	$IdSemesterMaster = $this->_getParam('id', 0);
    	$result = $this->lobjsemestermaster->fetchAll('IdSemesterMaster ='.$IdSemesterMaster);
    	$result = $result->toArray();
		foreach($result as $courseresult){
		}
    	$this->lobjsemestermasterForm->populate($courseresult);	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjsemestermasterForm->isValid($formData)) {
	   			$lintIdSemesterMaster = $formData ['IdSemesterMaster'];
				$this->lobjsemestermaster->fnupdateSemester($formData,$lintIdSemesterMaster);//update university
				$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'semestermaster', 'action'=>'index'),'default',true));
			}
    	}
    }
}