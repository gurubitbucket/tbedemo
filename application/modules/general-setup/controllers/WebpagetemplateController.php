<?php
class GeneralSetup_WebpagetemplateController extends Base_Base 
{
	private $lobjwebtemplate;//db variable
	private $lobjwebtemplateform;//Form variable
	
	// initialisation function
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
	
	//Function to set the objects
	public function fnsetObj()
	{					
		$this->lobjwebtemplate = new GeneralSetup_Model_DbTable_Webpagetemplatemodel(); //intialize user db object
		$this->lobjwebtemplateform = new GeneralSetup_Form_Webpagetemplate(); //intialize user Form
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
    //function to set and display the result
	public function indexAction() 
	{   	
		$this->view->lobjform = $this->lobjform; //send the Form  to the view
		$larrresult = $this->lobjwebtemplate->fngetwebtemplatelist(); //get template details		
		
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->programmasterpaginatorresult); // clear the search session
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->programmasterpaginatorresult)) 
		  {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->programmasterpaginatorresult,$lintpage,$lintpagecount);
		  } 
		else 
		  {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		  }
		
		// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		{
			$larrformData = $this->_request->getPost (); // get the form data  		    									   
		    unset ( $larrformData ['Search'] ); //unset the Search data in form data  	
		    $larrresult = $this->lobjwebtemplate->fnSearchwebtemplate ( $larrformData); //searching the values for the template							
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
			$this->gobjsessionsis->programmasterpaginatorresult = $larrresult;
			
		}		
		// check if the user has clicked the Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/general-setup/Webpagetemplate/index');			
		}		
	}
	
	//function to add template
	public function addAction() 
	{   
		$this->view->lobjwebtemplateform = $this->lobjwebtemplateform;	//send the Form  to the view		
		// check if the user has clicked the save				
		if ($this->_request->isPost() && $this->_request->getPost('Save'))
		{
			$larrformData = $this->_request->getPost();	// get the form data  			
			unset ( $larrformData ['Save'] ); //unset the Save data in form data  		
			$this->lobjwebtemplate->fnaddwebtemplate($larrformData); // insert into the DB
			$this->_redirect( $this->baseUrl . '/general-setup/webpagetemplate/index');	//redirect to index page after inserting			
        }     
    }
    
    //function to update template
	public function editAction()
	{   	
		$this->view->lobjwebtemplateform = $this->lobjwebtemplateform;	 //send the Form to the view
		$lintid=$this->_Getparam("id"); // get the id of the template to be updated			
		$larrresult = $this->lobjwebtemplate->fneditwebtemplate($lintid); // get the details from the DB into array variable			
		$this->view->lobjwebtemplateform->populate($larrresult); // populate the result into the form
					
    	if ($this->_request->isPost() && $this->_request->getPost('Save'))
    	 {       	 			 	
    		$larrformData = $this->_request->getPost();	// get the form data     		    		    	
	   		$lintId = $lintid; //store the id to be updated   	   		
	   		unset ($larrformData ['Save']);	//unset the Save data in form data  	   		
			$this->lobjwebtemplate->fnupdatewebtemplate($lintId,$larrformData); //update webtemplate
			$this->_redirect( $this->baseUrl . '/general-setup/webpagetemplate/index');	 //redirect to index page after updating						
    	}
	  }
}