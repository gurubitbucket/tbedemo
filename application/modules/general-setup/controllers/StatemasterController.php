<?php
class GeneralSetup_StatemasterController extends Base_Base
{
	private $lobjcountry;
	private $lobjstateForm;
	private $lobjstate;
	private $_gobjlogger;
	
	public function init() 
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	     $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    $this->fnsetObj();
   	    
	}
	public function fnsetObj()
	{
		$this->lobjform = new App_Form_Search (); //searchform
		$this->lobjstate = new GeneralSetup_Model_DbTable_Statemaster();
		$this->lobjstateForm = new GeneralSetup_Form_Statemaster(); 
		$this->lobjCountry = new GeneralSetup_Model_DbTable_Countrymaster();
	}
	    
	public function indexAction() 
	{
		$lobjform=$this->view->lobjform = $this->lobjform;		
		$larrresult = $this->lobjCountry->fngetCountryDetails();									
		if(!$this->_getParam('search'))
   	    unset($this->gobjsessionsis->citypaginatorresult);     	     	    	
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->citypaginatorresult)) 
		{
		  $this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->citypaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
		  $this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost() && $this->_request->getPost('Search')) 
		{
			    $larrformData = $this->_request->getPost();			 							
				if ($this->lobjform->isValid($larrformData))
				 {					 	
				 	unset ( $larrformData ['Search'] );					 				 				
					//checking the data and calling the search fuction
					$larrresult = $this->lobjstate->fnSearchState($larrformData['field3']);	
					/*echo('<pre>');
					print_r($larrresult);
					die();	*/													
				    $this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				    $this->gobjsessionsis->citypaginatorresult = $larrresult;
				}			
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{			
			$this->_redirect( $this->baseUrl . '/general-setup/statemaster/index');
		}
	}
        	
  	public function statelistAction() 
  	{
  		$this->view->lobjstateForm = $this->lobjstateForm;
  		$idCountry = $this->_getParam("id"); 
  				
        $lobjcountryname=$this->lobjstate->fnCountryName($idCountry);        	
        $ldtsystemDate = date ( 'Y-m-d H:i:s' );
        $this->view->lobjstateForm->UpdDate->setValue($ldtsystemDate);
        $auth = Zend_Auth::getInstance();
		$this->view->lobjstateForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$this->view->lobjstateForm->idCountry->setValue($lobjcountryname['CountryName']);
        $this->view->lobjstateForm->idCountry->setAttrib('readonly',true);       
        $larrresult = $this->lobjstate->fnGetStatelist($idCountry);            
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpagecount = $this->gintPageCount;;
		$lintpage = $this->_getParam('page',1);				
		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		$this->gobjsessionstudent->statepaginatorresult = $larrresult;				       
		if($this->_getParam('edit'))
		{   			
			$idcon= $this->_getParam('idCountry');							
			$larrresult= $this->lobjstate->fncityinfodtl($idcon,$this->_getParam('id'));			
			$lobjcountryname=$this->lobjstate->fnCountryName($idcon);
			unset($larrresult['idCountry']);			 								    		 						                        
			$this->lobjstateForm->populate($larrresult);
			$this->view->lobjstateForm->idCountry->setValue($lobjcountryname['CountryName']);					
		}
         
        //on click of save button 
  		if ($this->_request->isPost() && $this->_request->getPost('Save'))
  		 { 
		   $larrformData = $this->_request->getPost();		   
		   			   					   						
		   if($this->_getParam('edit'))
			{   
				if ($this->lobjstateForm->isValid($larrformData)) 
				{   					
					unset($larrformData['Save']);					
					$idState = $this->_getParam('id');															
					$larrformData['idCountry']=$idcon;				
					$UpdateData =array('idCountry'=>$idcon,
									   'StateName'=>$this->_getParam('StateName'),
									   'UpdDate'=>$this->_getparam('UpdDate'),
	                                   'UpdUser'=>$this->_getparam('UpdUser'),
									   'Active'=>$this->_getparam('Active'));															
					$larrresult = $this->lobjstate->fnUpdateState($UpdateData,$idState);

					$auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the State with Id = ".$idState."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
				
              	    $this->_redirect( $this->baseUrl.'/general-setup/statemaster/statelist/id/'.$idcon);
				   }				
			}
			else 
			{
			  //checks for the form validation
			  if ($this->lobjstateForm->isValid($larrformData)) 
			  {				
				unset($larrformData['Save']);					
				$larrformData['idCountry']=$idCountry;			
				$insertData =array('idCountry'=>$idCountry,
								   'StateName'=>$this->_getParam('StateName'),
								   'UpdDate'=>$this->_getparam('UpdDate'),
                                   'UpdUser'=>$this->_getparam('UpdUser'),
								   'Active'=>$this->_getparam('Active'));														
				$larrresult = $this->lobjstate->fnAddState($insertData);	

				// Write to Log
			    $auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New State"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
                $this->_redirect( $this->baseUrl.'/general-setup/statemaster/statelist/id/'.$idCountry);			
			   }
	         }   		  
		}

}	
}  	
