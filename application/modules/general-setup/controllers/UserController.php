<?php
class GeneralSetup_UserController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;	
	private $_gobjlogger;
	
	public function init() 
	{   
		
		//initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->fnsetObj();
		
			$auth = Zend_Auth::getInstance();
		if($auth->getIdentity()->iduser == 17)
		{
		   $this->_helper->layout()->setLayout('/web/usty');
		}
	}
	
	public function fnsetObj() 
	{
		$this->lobjuser = new GeneralSetup_Model_DbTable_User(); //user model object
		$this->lobjuserForm = new GeneralSetup_Form_User (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}

	public function indexAction() 
	{   
/*		$startDate = '2012-04-01';
		$endDate = '2012-05-31';
		$endDate = strtotime($endDate);
		for($i = strtotime('Wednesday', strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
   		 echo date('l Y-m-d', $i);*/
		
     /*  // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged Out"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);*/

		// action for search and view
		$lobjform=$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		$larrresult = $this->lobjuser->fngetUserDetails (); //get user details
		
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->userpaginatorresult);
			
		$lintpagecount = $this->gintPageCount;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
        		
		if(isset($this->gobjsessionsis->userpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->userpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjuser->fnSearchUser ( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->userpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
		//	$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'user', 'action'=>'index'),'default',true));
		 $this->_redirect( $this->baseUrl . '/general-setup/user/index');
		}
	}

	public function newuserAction() 
	{   
		//Action for creating the new user
		$this->view->lobjuserForm = $this->lobjuserForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d' );

		$this->view->lobjuserForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjuserForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		$lobjcountry = $this->lobjuser->fnGetCountryList();
		$this->lobjuserForm->country->addMultiOptions($lobjcountry);
		
		if($this->locale == 'ar_YE')  {
			$this->view->lobjuserForm->DOB->setAttrib('datePackage',"dojox.date.islamic");
		}
		
		$lobjIdRole = new App_Model_Common (); //dropdown for roles
		$lobjIdRole = $lobjIdRole->fnGetRoleDetails ();
		foreach ( $lobjIdRole as $lobjIdRole ) {
			$this->lobjuserForm->IdRole->addMultiOption ( $lobjIdRole ['idDefinition'], $lobjIdRole ['DefinitionDesc'] );
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			unset ( $larrformData ['Close'] );
			if ($this->lobjuserForm->isValid ( $larrformData )) {
				$larrformData ['IdStaff'] = 0 ;
				$lstrpassword = $larrformData ['passwd'];
				$larrformData ['passwd'] = md5 ( $lstrpassword );
				$result = $this->lobjuser->fnaddUser ( $larrformData ); //instance for adding the lobjuserForm values to DB
				$auth = Zend_Auth::getInstance();
    	    // Write to Log
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added The User Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				$this->_redirect( $this->baseUrl . '/general-setup/user/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'user', 'action'=>'index'),'default',true));
			}
			
		}
	}

	public function userlistAction() 
	{ 
		//Action for the updation and view of the user details
		$this->view->lobjuserForm = $this->lobjuserForm; //send the lobjuserForm object to the view
		$lobjcountry = $this->lobjuser->fnGetCountryList(); //get the countries list 
		$this->lobjuserForm->country->addMultiOptions($lobjcountry);// populate in the drop down		
		
		if($this->locale == 'ar_YE')  
		{
			$this->view->lobjuserForm->DOB->setAttrib('datePackage',"dojox.date.islamic");
		}
		
		$this->view->lobjuserForm->loginName->setAttrib ( 'readonly', true );//make the username as readonly
		$this->view->lobjuserForm->passwd->setAttrib ( 'readonly', true );//make the password as readonly
		$lintiduser = ( int ) $this->_getParam ( 'id' ); // get the id of the user to be edited
		$this->view->iduser	= $lintiduser; //send it to view				
		$larrresult = $this->lobjuser->fnviewUser($lintiduser ); //getting user details based on userid
		
		$lobjIdRole = new App_Model_Common (); //dropdown for roles
		$lobjIdRole = $lobjIdRole->fngetRoleDetails();
		foreach ( $lobjIdRole as $lobjIdRole ) {
		$this->lobjuserForm->IdRole->addMultiOption($lobjIdRole['idDefinition'], $lobjIdRole['DefinitionDesc'] );
		}
  				
		//Initialize SAM Common Model
		$lobjCommonModel = new App_Model_Common();
		foreach ($larrresult as $larruser) 
		{
			$this->view->lobjuserForm->iduser->setValue ( $larruser ['iduser'] );
			$this->view->lobjuserForm->loginName->setValue ( $larruser ['loginName'] );
			$this->view->lobjuserForm->UpdDate->setValue ( $larruser ['UpdDate'] );
			$this->view->lobjuserForm->UpdUser->setValue ( $larruser ['UpdUser'] );
			$this->view->lobjuserForm->fName->setValue ( $larruser ['fName'] );
			$this->view->lobjuserForm->mName->setValue ( $larruser ['mName'] );
			$this->view->lobjuserForm->lName->setValue ( $larruser ['lName'] );
			$this->view->lobjuserForm->email->setValue ( $larruser ['email'] );
			
			//Get States List
			$larrCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larruser['country']);
			$this->lobjuserForm->state->addMultiOptions($larrCountyrStatesList);

			$this->view->lobjuserForm->DOB->setValue ( $larruser['DOB'] );
			$this->view->lobjuserForm->gender->setValue ( $larruser ['gender'] );
			$this->view->lobjuserForm->addr1->setValue ( $larruser ['addr1'] );
			$this->view->lobjuserForm->addr2->setValue ( $larruser ['addr2'] );
			$this->view->lobjuserForm->city->setValue ( $larruser ['city'] );
			$this->view->lobjuserForm->state->setValue ( $larruser ['state'] );
			$this->view->lobjuserForm->country->setValue ( $larruser ['country'] );
			
			$this->view->lobjuserForm->city->addMultiOptions(array('0'=>'others'));
		    $this->view->lobjuserForm->city->addMultiOptions($this->lobjuser->fnGetcityList($larruser['state']));
		    
			$this->view->lobjuserForm->zipCode->setValue ( $larruser ['zipCode'] );
			$this->view->lobjuserForm->userArabicName->setValue ( $larruser ['userArabicName'] );
			
			$arrhomephone = explode("-",$larruser ['homePhone']);
			$this->view->lobjuserForm->homecountrycode->setValue ( $arrhomephone [0] );
			$this->view->lobjuserForm->homestatecode->setValue ( $arrhomephone [1] );
			$this->view->lobjuserForm->homePhone->setValue ( $arrhomephone [2] );
			
			$arrworkphone = explode("-",$larruser ['workPhone']);
			$this->view->lobjuserForm->workcountrycode->setValue ( $arrworkphone [0] );
			$this->view->lobjuserForm->workstatecode->setValue ( $arrworkphone [1] );
			$this->view->lobjuserForm->workPhone->setValue ( $arrworkphone [2] );
			
			$arrcellphone = explode("-",$larruser ['cellPhone']);
			$this->view->lobjuserForm->countrycode->setValue ($arrcellphone[0]);
			$this->view->lobjuserForm->statecode->setValue ($arrcellphone[1]);
			$this->view->lobjuserForm->cellPhone->setValue ($arrcellphone[2]);
			
			$this->view->lobjuserForm->IdRole->setValue ( $larruser ['IdRole'] );
			$this->view->lobjuserForm->fax->setValue ( $larruser ['fax'] );
			
			$this->view->lobjuserForm->passwd->setValue ( $larruser ['passwd'] );
			$this->view->lobjuserForm->UserStatus->setValue ( $larruser ['UserStatus'] );
			$this->view->lobjuserForm->passwd->renderPassword = true;
		}

		$this->lobjuserForm->loginName->removeValidator ('Db_NoRecordExists' );
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$this->view->lobjuserForm->passwd->renderPassword = true;
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				unset ( $larrformData ['Close'] );
				if ($this->lobjuserForm->isValid ( $larrformData )) {
						
					$lintiduser = $larrformData ['iduser'];
					$this->lobjuser->fnupdateUser($lintiduser, $larrformData );
					$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Edited The User Details with id = $lintiduser"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
					$this->_redirect( $this->baseUrl . '/general-setup/user/index');
					//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'user', 'action'=>'index'),'default',true));
				}
			}
		}
		$this->view->lobjuserForm = $this->lobjuserForm;
	}

	//Action To Get List Of States From Country Id
	public function getcountrystateslistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetCountryStateList($lintIdCountry));
		Zend_Json_Encoder::encode($larrCountryStatesDetails);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
   public function getcitylistAction()
	{    		
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idState');//Get Country Id			
		//get all the city that correspond to the selected state
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjuser->fnGetStateCityList($lintIdCountry));
		$larrCountryStatesDetails[]=array('key'=>'0','name'=>'Others');//if the key is 0 set city as others 
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}				
	
	//Action To Get List Of States From Country Id
	public function getstafflistAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$idstaff =$this->_getParam('idStaff');

		$larrStaffDetails = $this->lobjuser->getstaffdetails($idstaff);
		echo Zend_Json_Encoder::encode($larrStaffDetails);
	}
	
	function lastday($month = '5', $year = '2011') {
  		if (empty($month)) {
      		$month = date('m');
   		}
   	if (empty($year)) {
      $year = date('Y');
   }
   	$result = strtotime("{$year}-{$month}-01");
   	$result = strtotime('-1 second', strtotime('+1 month', $result));
   	return date('Y-m-d', $result);
}
	

}
