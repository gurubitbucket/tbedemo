<?php

error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);

class GeneralSetup_GradebaseController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function fnsetObj(){
		$this->lobjcenterModel = new GeneralSetup_Model_DbTable_Center(); //user model object
		$this->lobjGradebaseModel = new GeneralSetup_Model_DbTable_Gradebase();
		$this->lobjGradebaseForm = new GeneralSetup_Form_Gradebase(); 
	  	$this->lobjcoursemaster = new GeneralSetup_Model_DbTable_Coursemaster();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
		
	}
	
	public function indexAction() {
    	$this->view->title="Takaful Operator";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjGradebaseModel->fngetProgramDetails(); //get user details

		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->takafulpaginatorresult); 
			
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->takafulpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->takafulpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjGradebaseModel->fnSearchProgram($this->lobjform->getValues()); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->takafulpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/gradebase/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'takafuloperator', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newgradebaseAction() { //title
    	$this->view->title="Add New Profram";
		$this->view->lobjGradebaseForm = $this->lobjGradebaseForm;
		$ldtsystemDate = date('Y-m-d H:i:s');
	
		$this->view->lobjGradebaseForm->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjGradebaseForm->UpdUser->setValue($auth->getIdentity()->iduser);
		
		$lobjcountry = $this->lobjGradebaseModel->fnGetCourseList();
		$this->lobjGradebaseForm->IdCourseMaster->addMultiOptions($lobjcountry);
		
		
		
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			/*echo "<pre/>";
			print_r($formData);
			die();*/
				$this->lobjGradebaseModel->fnaddProgram($formData);
				$this->_redirect( $this->baseUrl . '/general-setup/gradebase/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'takafuloperator', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
    
	public function edittakafuloperatorAction(){
    	$this->view->title="Edit Takaful Operatpr";  //title
		$this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm; //send the lobjuniversityForm object to the view
		
		$lobjcountry = $this->lobjcenterModel->fnGetCountryList();
		$this->lobjTakafuloperatorForm->country->addMultiOptions($lobjcountry);
		
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjTakafuloperatorForm->UpdDate->setValue ( $ldtsystemDate );		
		
    	$idtakafuloperator = $this->_getParam('id');
    	$larrtakafuloperator = $this->lobjTakafuloperatorModel->fnGetTakafulDetails($idtakafuloperator);
	    	/*print_r($larrtakafuloperator);
	    	die();*/
      // 	$larrtakafuloperator = $this->lobjTakafuloperatorModel->fnGetTakafulDetails($idtakafuloperator); 	
    	$lobjCommonModel = new App_Model_Common();
		//Get States List
		$larrpermCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larrtakafuloperator['country']);
		$this->lobjTakafuloperatorForm->state->addMultiOptions($larrpermCountyrStatesList);
		$this->lobjTakafuloperatorForm->city->addMultiOptions(array('0'=>'others'));
		$this->lobjTakafuloperatorForm->city->addMultiOptions($this->lobjTakafuloperatorModel->fnGetcityList($larrtakafuloperator['state']));
		
			$arrworkphone = explode("-",$larrtakafuloperator['workphone']);
			unset($larrtakafuloperator['workphone']);
			
    	$this->lobjTakafuloperatorForm->populate($larrtakafuloperator);	
    	$this->view->lobjTakafuloperatorForm->workcountrycode->setValue ( $arrworkphone [0] );
			$this->view->lobjTakafuloperatorForm->workstatecode->setValue ( $arrworkphone [1] );
			$this->view->lobjTakafuloperatorForm->workphone->setValue ( $arrworkphone [2] );
		$this->view->lobjTakafuloperatorForm->Password->setAttrib('readonly',true);	
		
			$this->lobjTakafuloperatorForm->Password->setValue ( $larrtakafuloperator['Password']);
		$this->view->lobjTakafuloperatorForm->Password->renderPassword = true;
		
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjTakafuloperatorForm->isValid($formData)) {
	    		unset($formData['Save']);
					unset($formData['Back']);
				/*	print_r($formData);
					die();*/
	   			$lintidtakafuloperator = $formData['idtakafuloperator'];
				$this->lobjTakafuloperatorModel->fnupdatetakafuloperator($formData,$lintidtakafuloperator);//update university
				$this->_redirect( $this->baseUrl . '/general-setup/takafuloperator/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'takafuloperator', 'action'=>'index'),'default',true));
			}
    	}
    }
    public function getcountrystateslistAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Get Country Id
		$lintIdCountry = $this->_getParam('idCountry');
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetCountryStateList($lintIdCountry));
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	//Action To Get List Of States From Country Id
	public function getstafflistAction()
	 {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Get Country Id
		$idstaff =$this->_getParam('idStaff');
		$larrStaffDetails = $this->lobjuser->getstaffdetails($idstaff);
		echo Zend_Json_Encoder::encode($larrStaffDetails);
	 }	
		
	public function getcitylistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		//get all the city that correspond to the selected state
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjcenter->fnGetStateCityList($lintIdCountry));
		$larrCountryStatesDetails[]=array('key'=>'0',name=>'Others');//if the key is 0 set city as others 
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}				
}