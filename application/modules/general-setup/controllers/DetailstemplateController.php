<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class GeneralSetup_DetailstemplateController extends Base_Base {
	//private $gobjsessionsis; //class session global variable
	//private $gintPageCount;
	
	public function init() { //initialization function
		$this->gsessionbatch = Zend_Registry::get('sis'); 		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		//$this->fnsetObj();
	}

	public function indexAction() {			
 		$lobjsearchform = new App_Form_Search(); //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjsearchform object to the view				
        
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->detailstemplatepaginatorresult);
		
		$lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Detailstemplate();  // email template model object
		$larrresult = $lobjemailTemplateModel->fnGetTemplateDetails(); // get template details	
			
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model\
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if(isset($this->gobjsessionsis->detailstemplatepaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->detailstemplatepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}	
						
		if ($this->_request->isPost() && $this->_request->getPost('Search')) { // search operation
			$larrformData = $this->_request->getPost();						
				if ($lobjsearchform->isValid($larrformData)) {	
					$larrresult = $lobjemailTemplateModel->fnSearchTemplate($lobjsearchform->getValues());										
		    		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		    		$this->gobjsessionsis->detailstemplatepaginatorresult = $larrresult;						
				}			
		}
		//Clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			//$this->_redirect('/general-setup/emailtemplate');
		 $this->_redirect( $this->baseUrl . '/general-setup/detailstemplate/index');
		}
	}
        	
	/*
	 * Add New Template
	 */
  	public function newtemplateAction() { 	
 		$lobjemailTemplateForm = new GeneralSetup_Form_Detailstemplate();  // email template form
		$this->view->form = $lobjemailTemplateForm;	
		$auth = Zend_Auth::getInstance();
		$this->view->form->UpdUser->setValue ( $auth->getIdentity()->iduser);	
//dojo.require("dijit.editor._plugins.TextColor");
		
	    $lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Detailstemplate();	// model object	
		$larrDropdownValues = $lobjemailTemplateModel->fnGetWebPageTemplate();  
		$this->view->form->iddefinition->addMultiOptions($larrDropdownValues);
		$this->view->form->languagess->addMultiOptions(array('0'=>'English','1'=>'Bahasa-Malay'));
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) { // save opeartion
			$larrformData = $this->_request->getPost();							
			if ($lobjemailTemplateForm->isValid($larrformData)) {		
				$editorData = $larrformData['content1'];	
				//echo 	$editorData;die(); 								   
			    $lintresult = $lobjemailTemplateModel->fnAddEmailTemplate($lobjemailTemplateForm->getValues(),$editorData); // add method			
			  //  $this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'emailtemplate', 'action'=>'index'),'default',true));        
			 $this->_redirect( $this->baseUrl . '/general-setup/detailstemplate/index');
			}			
		}
	}

	public function edittemplateAction() {		
 		
 		$idTemplate = $this->_getParam('id');
 		$lobjemailTemplateForm = new GeneralSetup_Form_Detailstemplate(); // email template form
		$this->view->form = $lobjemailTemplateForm; 
		$this->view->form->iddetails->setValue($idTemplate);
		$this->view->id = $idTemplate;
		$auth = Zend_Auth::getInstance();
		$this->view->form->UpdUser->setValue($auth->getIdentity()->iduser);	
				 $this->view->form->languagess->addMultiOptions(array('0'=>'English','1'=>'Bahasa-Malay'));	
        if ($this->_request->isPost() && $this->_request->getPost('Save')) {  // update operation	
            $larrformData = $this->_request->getPost(); 
            unset($larrformData['Save']);            				
            if ($lobjemailTemplateForm->isValid($larrformData)) {
            		$idTemplate = $larrformData['iddetails'];
                	//$where = 'idTemplate = '. $idTemplate;					           
				 	$lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Detailstemplate();	
				 	$editorData = $larrformData['TemplateBody'];		
				 	//echo $idTemplate;die();					 	
                    $lobjemailTemplateModel->fnUpdateTemplate($idTemplate,$larrformData,$editorData); // update email template details		 	
                    $this->_redirect( $this->baseUrl . '/general-setup/detailstemplate/index');
                    // $this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'emailtemplate', 'action'=>'index'),'default',true)); 
				} 			
        } else {
        		// 	get external panel row values by id and populate
				$lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Detailstemplate(); //email template model object
		        $larrresult = $lobjemailTemplateModel->fnViewTemplte($idTemplate);
		$larrDropdownValues = $lobjemailTemplateModel->fnGetWebPageTemplate();  
		//$this->view->form->iddefinition->addMultiOptions($larrDropdownValues);
		      $this->view->form->iddefinition->addMultiOption($larrresult['iddefinition'],$larrresult['DefinitionDesc']);
		       $this->view->form->languagess->setValue($larrresult['language']);	
		      
		       	$this->view->TemplateBody = $larrresult['content'];
		       	
        }  	
  	}

  	
  	public function fngetlanguageAction()
  	{
  		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$language =$this->_getParam('language');
		$tempid =$this->_getParam('tempid');
		$lobjemailTemplateModel = new GeneralSetup_Model_DbTable_Detailstemplate();	 

		$larrlangDetails = $lobjemailTemplateModel->fngetlanguagedetails($tempid);
		
		if($language==0)
		{
			echo $larrlangDetails['content'];
			exit;
		}
  	   if($language==1)
		{
			echo $larrlangDetails['banhasa'];
			exit;
		}
  	}
}