<?php

class GeneralSetup_RacereportController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;	
	
	public function init() 
	{   
		//initialization function
		$this->gobjsessionstudent = Zend_Registry::get('sis');		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() 
	{
		$this->lobjRacereportmodel = new GeneralSetup_Model_DbTable_Racereportmodel(); //user model object
		$this->lobjRacereportForm = new GeneralSetup_Form_Racereport (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

	public function indexAction() 
	{   
		// action for search and view
		$this->view->lobjRacereportForm = $this->lobjRacereportForm;
		$larresult = $this->lobjRacereportmodel->fnGetbatch();
		$this->lobjRacereportForm->Batch->addMultiOptions($larresult);
	}

	public function raceAction() 
	{   
				
		$this->view->InitBatch = $this->view->Batch;
		$result = $this->_request->getPost();										
		$this->view->Batch = $result['Batch'];
		$this->view->Chart = $result['Chart'];		
		$larrbatchdetails = $this->lobjRacereportmodel->fnGetBatchDetails($result['Batch']);  		     
		$this->view->batchname = $larrbatchdetails[0]['BatchName'];
		$this->view->BatchFrom = $larrbatchdetails[0]['BatchFrom'];
		$this->view->BatchTo = $larrbatchdetails[0]['BatchTo'];
		$this->view->ProgramName = $larrbatchdetails[0]['ProgramName'];
		
		$larrracetypes = $this->lobjRacereportmodel->fnracetypes();		
		$this->view->racearray=$larrracetypes;//X-Axis
		
		$larrracedetails=$this->lobjRacereportmodel->fngetracedetails($result['Batch']);
		/*echo('<pre>');
		print_r($larrracedetails);
		die();*/
		$var=count($larrracedetails);	
		$this->view->countvalue = $var;		
	/*	print_r(count($larrracedetails));
		die();	*/
			
		$black=0;
		$chinese=0;
		$white=0;
		$chinese = $larrracedetails[0]['total'];
		if(count($larrracedetails)>1)
		{
			$white= $larrracedetails[1]['total'];
		}
	    if(count($larrracedetails)>2)
		{
			$black= $larrracedetails[2]['total'];
		}				
		$this->view->others=0;
		$this->view->others1=0;
		$this->view->others2=0;
		$this->view->others3=0;
		$this->view->others4=0;
		$this->view->others5=0;
		
		if(count($larrracedetails)>3)
		{
			$otheres= $larrracedetails[3]['total'];
			$this->view->others = $otheres;	
		}
	   if(count($larrracedetails)>4)
		{
			$otheres1= $larrracedetails[4]['total'];
			$this->view->others1 = $otheres1;	
		}
	   if(count($larrracedetails)>5)
		{
			$otheres2= $larrracedetails[5]['total'];
			$this->view->others2 = $otheres2;	
		}
	   if(count($larrracedetails)>6)
		{
			$otheres3= $larrracedetails[6]['total'];
			$this->view->others3 = $otheres3;	
		}
	   if(count($larrracedetails)>7)
		{
			$otheres4= $larrracedetails[7]['total'];
			$this->view->others4 = $otheres4;	
		}	
	   if(count($larrracedetails)>8)
		{
			$otheres5= $larrracedetails[8]['total'];
			$this->view->others5 = $otheres5;	
		}
		$this->view->chinese = $chinese;
		$this->view->white = $white;
		$this->view->black = $black;
							
	}

	public function printAction()
	{
		$this->_helper->layout()->setLayout('/reg/usty1');		
		$batchprint = $this->_getParam('batchprint');	
		$batchChart = $this->_getParam('batchChart');
		$this->view->Batch = $batchprint;
		$this->view->Chart = $batchChart;		
					    				      				
		$larrracetypes = $this->lobjRacereportmodel->fnracetypes();		
		$this->view->racearray=$larrracetypes;//X-Axis	
			
		$larrbatchdetails = $this->lobjRacereportmodel->fnGetBatchDetails($batchprint);  
		$larrracedetails=$this->lobjRacereportmodel->fngetracedetails($batchprint);
	    
		$var=count($larrracedetails);	
		$this->view->countvalue = $var;								
		
		$larrracecount=$this->lobjRacereportmodel->fngetracecount($batchprint);		
		
		$larrracedetails=$this->lobjRacereportmodel->fngetracedetails($batchprint);
		$var=count($larrracedetails);		
		$this->view->countvalue = $var;	
						
		$Total=$larrracecount[0]['total'];	
		$this->view->total = $Total;
			
		$chinese = $larrracedetails[0]['total'];
		$black=0;
			$chinese=0;
			$white=0;
		$chinese = $larrracedetails[0]['total'];
		if(count($larrracedetails)>1)
		{
			$white= $larrracedetails[1]['total'];
		}
	    if(count($larrracedetails)>2)
		{
			$black= $larrracedetails[2]['total'];
		}
		$this->view->others=0;
		$this->view->others1=0;
		$this->view->others2=0;
		$this->view->others3=0;
		$this->view->others4=0;
		$this->view->others5=0;
		
		if(count($larrracedetails)>3)
		{
			$otheres= $larrracedetails[3]['total'];
			$this->view->others = $otheres;	
		}
		
	   if(count($larrracedetails)>4)
		{
			$otheres1= $larrracedetails[4]['total'];
			$this->view->others1 = $otheres1;	
		}
		
	   if(count($larrracedetails)>5)
		{
			$otheres2= $larrracedetails[5]['total'];
			$this->view->others2 = $otheres2;	
		}
		
	   if(count($larrracedetails)>6)
		{
			$otheres3= $larrracedetails[6]['total'];
			$this->view->others3 = $otheres3;	
		}
		
	   if(count($larrracedetails)>7)
		{
			$otheres4= $larrracedetails[7]['total'];
			$this->view->others4 = $otheres4;	
		}
			
	   if(count($larrracedetails)>8)
		{
			$otheres5= $larrracedetails[8]['total'];
			$this->view->others5 = $otheres5;	
		}
		
		$this->view->chinese=$chinese;
		$this->view->white=$white;
		$this->view->black=$black;
	}

}