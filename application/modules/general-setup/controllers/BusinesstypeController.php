<?php
class GeneralSetup_BusinesstypeController extends Base_Base 
{
	private $lobjBusinesstype; //db variable
	private $lobjBusinesstypeform;//Form variable
	private $_gobjlogger;
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	     $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    $this->fnsetObj(); //call fnsetObj
   	   
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjBusinesstype = new GeneralSetup_Model_DbTable_Businesstypemodel(); //intialize user db object
		$this->lobjBusinesstypeform = new GeneralSetup_Form_Businesstype(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction() 
	{    	 
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view
		$larrresult = $this->lobjBusinesstype->fngetBusinesstypelist(); //get businesstype details		
		
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->Businesstypepaginatorresult); // clear the search session
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->Businesstypepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Businesstypepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		{
			    $larrformData = $this->_request->getPost ();			    									   
				unset ( $larrformData ['Search'] );
				$larrresult = $this->lobjBusinesstype->fnSearchbusinesstype($larrformData); //searching the values for the businesstype					
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Businesstypepaginatorresult = $larrresult;
		}
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/general-setup/businesstype/index');			
		}
		
	}
	
	//function to add a businesstype
	public function addAction() 
	{   
		$this->view->lobjBusinesstypeform = $this->lobjBusinesstypeform; //send the form to the view			
		// check if the user has clicked the save
		if ($this->_request->isPost() && $this->_request->getPost('Save'))
		{
			$larrformData = $this->_request->getPost();						
			unset ( $larrformData ['Save'] );
			$this->lobjBusinesstype->fnaddbusinesstype($larrformData);
			
			$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Businesstype"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
			$this->_redirect( $this->baseUrl . '/general-setup/businesstype/index'); //after adding Redirect to index page				
        }     
    }
    
    //function to update the businesstype
	public function editAction()
	{   	
		$this->view->lobjBusinesstypeform = $this->lobjBusinesstypeform; //send the Form the view
		$id=$this->_Getparam("id"); // get the id of the businesstype to be updated		
		$larrresult = $this->lobjBusinesstype->fneditbusinesstype($id);	// get the details from the DB into array variable	
		$this->view->lobjBusinesstypeform->populate($larrresult); // populate the result into the form
			
	    // check if the user has clicked the save
    	if ($this->_request->isPost() && $this->_request->getPost('Save'))
    	 {       	 			 	
    		$larrformData = $this->_request->getPost();	 // get the form data  		    		    	
	   		$lintId = $id;	//store the id to be updated    		
	   		unset ($larrformData ['Save']);	 //unset the save data in form data  		
			$this->lobjBusinesstype->fnupdatebusinesstype($lintId,$larrformData); //update businesstype
			
			$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Edited the Businesstype with id = ".$lintId."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
			$this->_redirect( $this->baseUrl . '/general-setup/businesstype/index'); //redirect to index page after updating							
    	}
	  }
}