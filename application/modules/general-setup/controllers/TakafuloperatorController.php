<?php

error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);

class GeneralSetup_TakafuloperatorController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function fnsetObj(){
		$this->lobjcenterModel = new GeneralSetup_Model_DbTable_Center(); //user model object
		$this->lobjTakafuloperatorModel = new GeneralSetup_Model_DbTable_Takafuloperator();
		$this->lobjTakafuloperatorForm = new GeneralSetup_Form_Takafuloperator(); 
	  	$this->lobjcoursemaster = new GeneralSetup_Model_DbTable_Coursemaster();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
	  	$this->lobjTakafultypepaymentForm= new Finance_Form_Takafultypepayment();
	  	$this->lobjTakafultypepaymentModel = new Finance_Model_DbTable_Takafultypepayment();
	}
	
	public function indexAction() {
    	$this->view->title="Takaful Operator";
		$this->view->lobjform = $this->lobjTakafuloperatorForm; //send the lobjuniversityForm object to the view
		$this->lobjTakafuloperatorForm->Takafuldrop->setAttrib('onkeyup', 'fnGetOperatorNames');
		$takafulids=$this->lobjTakafuloperatorModel->fngettakafulids();
		$this->lobjTakafuloperatorForm->TakafulId->addMultiOptions(array(""=>"Select"));
		$this->lobjTakafuloperatorForm->TakafulId->addMultiOptions($takafulids);
		
		/*$larrresult = $this->lobjTakafuloperatorModel->fngetTakafulOperator(); //get user details
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->takafulpaginatorresult); 
		if(isset($this->gobjsessionsis->takafulpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->takafulpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		*/
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjTakafuloperatorForm->isValid ( $larrformData )) {
				$larrformData = $this->_request->getPost ();
				//echo "<pre/>";print_r($larrformData);die();
				$larrresult = $this->lobjTakafuloperatorModel->fnSearchTakafulOperator($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->takafulpaginatorresult = $larrresult;
				$this ->view->count=count($larrresult);
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/takafuloperator/index');
		}
	}
	
	public function newtakafuloperatorAction() { //title
		$this->view->title="Add New Profram";
		$this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm;
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjTakafuloperatorForm->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjTakafuloperatorForm->UpdUser->setValue($auth->getIdentity()->iduser);
		$lobjcountry = $this->lobjcenterModel->fnGetCountryList();
		$this->lobjTakafuloperatorForm->country->addMultiOptions($lobjcountry);
		$lobjbusiness = $this->lobjTakafuloperatorModel->fnGetBusinesstypeList();
		$this->lobjTakafuloperatorForm->Businesstype->addMultiOptions($lobjbusiness);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			unset($formData['Save']);
			unset($formData['Back']);
			$password = $formData['Password'];
			$formData['hint']     = "CidF2".$formData['Password'];
			$formData['Password'] = md5($formData['Password']);
			$formData['Takafultype']=1;
   $formData['TakafulField10']=$formData['Active'];

			$lintlastId=$this->lobjTakafuloperatorModel->fnaddtakafuloperator($formData);
			$larrformdata['idtakafuloperator']=$lintlastId;
			$larrformdata['paymenttype']=181;
			$larrformdata['upduser']= $auth->getIdentity()->iduser;
			$larrformdata['upddate']= $ldtsystemDate = date('Y-m-d H:i:s');
			$this->lobjTakafultypepaymentModel->fninsertdata($larrformdata);
			$this->_redirect( $this->baseUrl . '/general-setup/takafuloperator/index');
		}
	}
    
	public function edittakafuloperatorAction(){
    	$this->view->title="Edit Takaful Operatpr";  //title
		$this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm; //send the lobjuniversityForm object to the view
		$lobjcountry = $this->lobjcenterModel->fnGetCountryList();
		$this->lobjTakafuloperatorForm->country->addMultiOptions($lobjcountry);
		$lobjbusiness = $this->lobjTakafuloperatorModel->fnGetBusinesstypeList();
		$this->lobjTakafuloperatorForm->Businesstype->addMultiOptions($lobjbusiness);
		$ldtsystemDate = date('Y-m-d H:i:s');
		$this->view->lobjTakafuloperatorForm->UpdDate->setValue ( $ldtsystemDate );		
    	$idtakafuloperator = $this->_getParam('id');
    	$larrtakafuloperator = $this->lobjTakafuloperatorModel->fnGetTakafulDetails($idtakafuloperator);
    	$lobjCommonModel = new App_Model_Common();
		//Get States List
		$larrpermCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larrtakafuloperator['country']);
		$this->lobjTakafuloperatorForm->state->addMultiOptions($larrpermCountyrStatesList);
		$this->lobjTakafuloperatorForm->city->addMultiOptions(array('0'=>'others'));
		$this->lobjTakafuloperatorForm->city->addMultiOptions($this->lobjTakafuloperatorModel->fnGetcityList($larrtakafuloperator['state']));
		$arrworkphone = explode("-",$larrtakafuloperator['workphone']);
		unset($larrtakafuloperator['workphone']);
    	$this->lobjTakafuloperatorForm->populate($larrtakafuloperator);	
    	$this->view->lobjTakafuloperatorForm->workcountrycode->setValue ( $arrworkphone [0] );
		$this->view->lobjTakafuloperatorForm->workstatecode->setValue ( $arrworkphone [1] );
		$this->view->lobjTakafuloperatorForm->workphone->setValue ( $arrworkphone [2] );
		$this->view->lobjTakafuloperatorForm->Password->setAttrib('readonly',true);	
		$this->lobjTakafuloperatorForm->Password->setValue ( $larrtakafuloperator['Password']);


		$this->lobjTakafuloperatorForm->Active->setValue ( $larrtakafuloperator['TakafulField10']);
		
		
		//start inhouse
		
		$this->view->Inhouse = $larrtakafuloperator['TakafulField6'];
        //end inhouse

		$this->view->lobjTakafuloperatorForm->Password->renderPassword = true;
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjTakafuloperatorForm->isValid($formData)) {
	    		unset($formData['Save']);
					unset($formData['Back']);
					$formData['TakafulField10']=$formData['Active'];
					$newpass = $formData['NewPassword'];
					$conpass = $formData['RetypePassword'];
					
				if($formData['NewPassword'] != $formData['RetypePassword'])
				{
					echo '<script language="javascript">alert("New password and confirm password do not match")</script>';
					return false;
				}	
				elseif($formData['NewPassword']!= '')
					{
						$password = $formData['NewPassword'];
						$formData['Password'] = md5($password); 
						$formData['Takafultype']=1;
	   					$lintidtakafuloperator = $formData['idtakafuloperator'];
	   					unset($formData['NewPassword']);
	   					unset($formData['RetypePassword']);
						$this->lobjTakafuloperatorModel->fnupdatetakafuloperator($formData,$lintidtakafuloperator);//update university
	    			}
	    			else 
	    	        {   
	    	        	unset($formData['NewPassword']);
	    	        	unset($formData['RetypePassword']);
						$formData['Takafultype']=1;
	   					$lintidtakafuloperator = $formData['idtakafuloperator'];
						$this->lobjTakafuloperatorModel->fnupdatetakafuloperator($formData,$lintidtakafuloperator);//update university
	    			}
				} 
				
	    			$this->_redirect( $this->baseUrl . '/general-setup/takafuloperator/index');
			}
    	}
    
    public function getcountrystateslistAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Get Country Id
		$lintIdCountry = $this->_getParam('idCountry');
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetCountryStateList($lintIdCountry));
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	//Action To Get List Of States From Country Id
	public function getstafflistAction()
	 {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Get Country Id
		$idstaff =$this->_getParam('idStaff');
		$larrStaffDetails = $this->lobjuser->getstaffdetails($idstaff);
		echo Zend_Json_Encoder::encode($larrStaffDetails);
	 }	
		
	public function getcitylistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		//get all the city that correspond to the selected state
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjcenter->fnGetStateCityList($lintIdCountry));
		$larrCountryStatesDetails[]=array('key'=>'0',name=>'Others');//if the key is 0 set city as others 
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}
	
	public function takafultypepaymentchangeAction() {
		$idcompanypaymenttype = $this->_getParam('id');
		$this->view->lobjTakafultypepaymentForm = $this->lobjTakafultypepaymentForm;
		$this->lobjTakafultypepaymentForm->idtakafulpaymenttype->setValue($idcompanypaymenttype);
		$larrcompanyname = $this->lobjTakafultypepaymentModel->fnGetcompanyNameedit();
		$this->lobjTakafultypepaymentForm->idtakafuloperator->setAttrib(readonly,true);
		$this->lobjTakafultypepaymentForm->idtakafuloperator->addMultiOptions($larrcompanyname);
		$larrcompanyname = $this->lobjTakafultypepaymentModel->fnGetPaymentTerms();
		$this->lobjTakafultypepaymentForm->paymenttype->addMultiOptions($larrcompanyname);
		$ldtsystemDate = date('Y-m-d:H-i-s');
		$this->lobjTakafultypepaymentForm->upddate->setValue($ldtsystemDate);
		$upduser = 1;//$auth->getIdentity()->iduser;
		$this->lobjTakafultypepaymentForm->upduser->setValue($upduser);
		$editresult = $this->lobjTakafultypepaymentModel->editcompanytypepayment($idcompanypaymenttype);
		$this->lobjTakafultypepaymentForm->populate($editresult);
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' ))
		{
			$larrformData = $this->_request->getPost ();
			$idcompanypmttype = $larrformData['idtakafulpaymenttype'];
			unset($larrformData['idtakafulpaymenttype']);
			unset($larrformData['Save']);
			$larrresult = $this->lobjTakafultypepaymentModel->fnupdatecompanytypepayment($larrformData,$idcompanypmttype);
			$this->_redirect( $this->baseUrl . '/general-setup/takafuloperator/index');
		}
	}
	
	public function fnajaxgettakafullAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view
		$takafull=$this->_getParam('takafull'); // get the centername
		$larrtakafull = $this->lobjTakafuloperatorModel->fngetexistingtakafull($takafull); // search the DB for the centername
		if($larrtakafull)
		{
			echo 'The Takafull login provided already exists, please provide with another Takafull login';
		}
	}
		
	  public function fnajaxgettakafullnamesAction()
		{   
			$this->_helper->layout->disableLayout();//disable layout
			$this->_helper->viewRenderer->setNoRender();//do not render the view	
			$takafullname=$this->_getParam('takafullname'); // get the centername 
			$larrtakafullname = $this->lobjTakafuloperatorModel->fngetexistingtakafullname($takafullname); // search the DB for the centername
		    if($larrtakafullname)
		    {
		    	echo 'The Takafull name provided already exists, please provide with another Takafull name';
		    }
		}
		
 	public function getakafulnamesAction()
		{   
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$newresult="";	
			$namestring = $this->_getParam('namestring');
			$larrresultopnames = $this->lobjTakafuloperatorModel->fnGetOperatorNames($namestring); 
			foreach ($larrresultopnames as $larrresnewarray){
				$opname=$larrresnewarray['name'];
				$newresult=$newresult."<tr><td><span id='idspan'   onclick='fnsetvalue(\"".$opname."\");'>".$larrresnewarray['name']."</span></td></tr></BR>";			
			}
			echo $newresult;
			exit;
		    
		}
}