<?php
class GeneralSetup_StaffmasterController extends Zend_Controller_Action {

	private $gobjsessionsis; //class session global variable
	private $gintPageCount;
	
	public function init() { //initialization function
		$this->gobjsessionsis = Zend_Registry::get('sis'); //initialize session variable
		$lobjinitialconfigModel = new GeneralSetup_Model_DbTable_Initialconfiguration(); //user model object

		$larrInitialSettings = $lobjinitialconfigModel->fnGetInitialConfigDetails($this->gobjsessionsis->idUniversity);
		$this->gintPageCount = isset($larrInitialSettings['noofrowsingrid'])?$larrInitialSettings['noofrowsingrid']:"5";
		$this->view->College = isset($larrInitialSettings['CollegeAliasName']) ? $larrInitialSettings['CollegeAliasName']:"College";
		$this->view->Department = isset($larrInitialSettings['DepartmentAliasName']) ? $larrInitialSettings['DepartmentAliasName']:"Department";
		$this->view->Subject = isset($larrInitialSettings['SubjectAliasName']) ? $larrInitialSettings['SubjectAliasName']:"Subject";
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	   
	}
	
	public function indexAction() {
    	$this->view->title="Staff Master"; 	//title
    	$lobjform = new App_Form_Search (); //intialize search lobjstaffmasterForm
		$this->view->lobjform = $lobjform; //send the lobjstaffmasterForm object to the view
		
		$lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster(); //staffmaster model object
		
		/*$lobjsession = Zend_Registry::get ( 'sis' );
		$RoleText=$lobjsession->collegeId;
		$Utype=$lobjsession->collegeId; */
		
		$larrresult = $lobjStaffmaster->fngetStaffDetails(); //get staffmaster details
		
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionstudent->staffpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionstudent->staffpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ($larrformData)) {
				$larrresult = $lobjStaffmaster->fnSearchStaff($lobjform->getValues ()); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'staffmaster', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newstaffmasterAction() { //title
    	$this->view->title="Add New Staff";
		$lobjstaffmasterForm = new GeneralSetup_Form_Staffmaster(); //intialize user lobjstaffmasterForm
		$this->view->lobjstaffmasterForm = $lobjstaffmasterForm; //send the lobjstaffmasterForm object to the view
		$lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster();
		
		
		$lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$lobjcountry = $lobjUser->fnGetCountryList();
		$lobjstaffmasterForm->Country->addMultiOptions($lobjcountry);
		
		$lobjCollegeList = $lobjStaffmaster->fnGetCollegeList();
		$lobjstaffmasterForm->IdCollege->addMultiOptions($lobjCollegeList);
		
		$lobjDepartmentList = $lobjStaffmaster->fnGetDepartmentList();
		$lobjstaffmasterForm->IdDepartment->addMultiOptions($lobjDepartmentList);
		
		$lobjLevelsList = $lobjStaffmaster->fnGetLevelsList();
		$lobjstaffmasterForm->IdLevel->addMultiOptions($lobjLevelsList);
		
		$lobjSubjectList = $lobjStaffmaster->fnGetSubjectList();
		$lobjstaffmasterForm->IdSubject->addMultiOptions($lobjSubjectList);
		
		$ldtsystemDate = date ( 'Y-m-d' );
		$this->view->lobjstaffmasterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjstaffmasterForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			$larrformData = $this->getRequest()->getPost();
			if ($lobjstaffmasterForm->isValid($larrformData)) {//process form 
				unset ( $larrformData ['Save'] );
				unset ( $larrformData ['Back'] );
				$lobjStaffmaster->fnaddStaff($larrformData);
				$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'staffmaster', 'action'=>'index'),'default',true));	//redirect	
			} 	
        }     
    }
    
	public function staffmasterlistAction(){
    	$lobjstaffmasterForm = new GeneralSetup_Form_Staffmaster(); //intialize user lobjstaffmasterForm
		$this->view->lobjstaffmasterForm = $lobjstaffmasterForm; //send the lobjuserForm object to the view
		$lobjStaffmaster = new GeneralSetup_Model_DbTable_Staffmaster();
		
		$lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$lobjcountry = $lobjUser->fnGetCountryList();
		$lobjstaffmasterForm->Country->addMultiOptions($lobjcountry);
		$lobjstate = $lobjUser->fnGetStateList();
		$lobjstaffmasterForm->State->addMultiOptions($lobjstate);
		
		$lobjDepartmentList = $lobjStaffmaster->fnGetDepartmentList();
		$lobjstaffmasterForm->IdDepartment->addMultiOptions($lobjDepartmentList);
		
		$lobjLevelsList = $lobjStaffmaster->fnGetLevelsList();
		$lobjstaffmasterForm->IdLevel->addMultiOptions($lobjLevelsList);
		
		$lobjSubjectList = $lobjStaffmaster->fnGetSubjectList();
		$lobjstaffmasterForm->IdSubject->addMultiOptions($lobjSubjectList);
		
		$lintidstafft = ( int ) $this->_getParam ( 'id' );
		$this->view->idstaff = $lintidstafft;
		
		
		$larrresult = $lobjStaffmaster->fnviewStaffDetails($lintidstafft); //getting user details based on userid
		$lobjCollegeList = $lobjStaffmaster->fnGetCollegeList();
		$lobjstaffmasterForm->IdCollege->addMultiOptions($lobjCollegeList);
		$lstrStaffType= $larrresult['StaffType'];
		$this->view->stafftype = $lstrStaffType;
		
    	$lobjstaffmasterForm->populate($larrresult);	

		if ($lstrStaffType == '0' ) {
			//$lstridcollege= $larrresult['IdCollege']; 
			$lobjstaffmasterForm->IdCollege->setValue($larrresult['IdCollege']);
			$lobjstaffmasterForm->IdBranch->addMultiOptions(array('0' => 'Select'));
			$lobjstaffmasterForm->IdBranch->setValue('0');
		} else if($lstrStaffType == '1' ){
			$lstridbranch= $larrresult['IdCollege'];
			$idcoll = $lobjStaffmaster->fnGetCollege($lstridbranch);
			$lobjstaffmasterForm->IdCollege->setValue($idcoll['Idhead']);
			
			$larrDetails = $lobjStaffmaster->fnGetBranchListofCollege($idcoll['Idhead']);
			$lobjstaffmasterForm->IdBranch->addMultiOptions($larrDetails);
			$lobjstaffmasterForm->IdBranch->setValue($lstridbranch);
			
			
			
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($lobjstaffmasterForm->isValid ( $larrformData )) {
						
					$lintIdStaff = $larrformData ['IdStaff'];
					$lobjStaffmaster->fnupdateStaf($lintIdStaff, $larrformData );
					$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'staffmaster', 'action'=>'index'),'default',true));
				}
			}
		}
		$this->view->lobjstaffmasterForm = $lobjstaffmasterForm;
	}
}