<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class GeneralSetup_CompanieslistController extends Base_Base { //Controller for the Companieslist Module
	
	public function init() {
		$this->view->translate = Zend_Registry::get ( 'Zend_Translate' );
		Zend_Form::setDefaultTranslator ( $this->view->translate );
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->fnsetObj();
		
	}
	private function fnsetObj() 
	{
		$this->lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails (); //Company student details model object
		$this->lobjCompanieslistModel = new GeneralSetup_Model_DbTable_Companieslist (); //Company CompaniesList  model object
		$this->lobjcompanymaster = new App_Model_Companymaster (); //Company student details model object
		$this->lobjsearchForm = new App_Form_Search (); //intialize Search lobjsearchForm
		$this->lobjTakafuloperatorForm = new App_Form_Companymaster (); //intialize Companymaster lobjTakafuloperatorForm
		$this->lobjCompanieslistForm = new GeneralSetup_Form_Companieslist ();
	}
	
	public function indexAction() {
		// action for search and view
		$lobjform = $this->view->lobjform = $this->lobjsearchForm; //send the lobjsearchForm object to the view
		$larrrbusinesstypelist = $this->lobjcompanymaster->fnGetBusinesstypeList (); //get Business type list details
		$lobjform->field5->addMultiOption ('','Select');
		//$lobjform->field1->addMultiOption ('','Select');
		//$lobjform->field8->addMultiOption ('','Select');
		$lobjform->field5->addMultioptions ( $larrrbusinesstypelist );
		$lobjform->field1->setAttrib ( 'OnChange', 'fnGetCityList' );
		$lobjform->field8->setRegisterInArrayValidator ( false );
		
		if (! $this->_getParam ( 'search' ))
			unset ( $this->gobjsessionsis->companielistpaginatorresult );
		
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam ( 'page', 1 ); //Paginator instance
		if(isset($this->gobjsessionsis->companielistpaginatorresult)) {
		$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->companielistpaginatorresult,$lintpage,$lintpagecount);
		} 
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				//echo "<pre/>";print_r($larrformData);die();
				$larrresult = $this->lobjCompanieslistModel->fnSearchCompaniesnew ( $larrformData ); //searching the values for the Companies
				//echo "<pre/>";print_r($larrresult);die();
				$this->view->paginator = $this->lobjCommon->fnPagination ( $larrresult, $lintpage, $lintpagecount );
				$this->gobjsessionsis->companielistpaginatorresult = $larrresult;
			}
			//echo "<pre/>";print_r($larrformData);
			$this->lobjform->populate($larrformData);
		}
	}
	
	public function editcompanyAction() {
		$lobjTakafuloperatorForm = $this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm; //send the lobjTakafuloperatorForm object to the view
		$lintIdCompany = $this->_getParam ( 'idcompany' );
		
		$this->view->lintIdCompany = $lintIdCompany;
		
		$larrresult = $this->lobjCompanieslistModel->fnGenerateQueries(1,0,$lintIdCompany); //get Company details
	
		
		$larrrbusinesstypelist = $this->lobjcompanymaster->fnGetBusinesstypeList (); //get Business type list details
		$lobjTakafuloperatorForm->businesstype->addMultioptions ( $larrrbusinesstypelist );
		$lobjTakafuloperatorForm->businesstype->setValue ( $larrresult ['idbusinesstype'] );
		
		$lintIdCountry = 121;
		$this->lobjstate = new GeneralSetup_Model_DbTable_Statemaster ();
		$lobjcountryname = $this->lobjstate->fnCountryName ( $lintIdCountry );
		$this->view->lobjTakafuloperatorForm->CompanyName->setAttrib('OnChange','duplicaCompanyname(this.value)');
		$this->view->lobjTakafuloperatorForm->IdCountry->setValue ( $lobjcountryname ['CountryName'] );
		$this->view->lobjTakafuloperatorForm->RegistrationNo->setAttrib ( 'readonly', true );
		$this->view->lobjTakafuloperatorForm->IdCountry->setAttrib ( 'readonly', true );
		$this->view->lobjTakafuloperatorForm->Login->setAttrib ( 'readonly', true );
		
		
		$lobjstatesname = $this->lobjstate->fnGetStateslist ( $larrresult ['idCountry'] );
		
		$this->view->lobjTakafuloperatorForm->IdState->addMultiOptions ( $lobjstatesname );
		$this->view->lobjTakafuloperatorForm->IdState->setValue ( $larrresult ['idState'] );
		
		$lobjCityNameList = $this->lobjcompanymaster->fnGetStateCityList ( $larrresult ['idState'] );
		$this->view->lobjTakafuloperatorForm->City->addMultiOptions ( $lobjCityNameList );
		$this->view->lobjTakafuloperatorForm->City->setValue ( $larrresult ['idCity'] );
		
		$this->lobjTakafuloperatorForm->populate ( $larrresult );
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($this->lobjTakafuloperatorForm->isValid ( $larrformData )) {
					
					
					$larrformData ['Phone1'] = $larrformData ['Phone1'];
					$larrformData ['Phone2'] = $larrformData ['Phone2'];
					$larrformData ['IdCountry'] = 121;
					$ldtsystemDate = date ( 'Y-m-d H:i:s' );
					$larrformData ['UpdDate'] = $ldtsystemDate;
					$auth = Zend_Auth::getInstance();
					$larrformData ['UpdUser'] = $auth->getIdentity()->iduser;
					
					$this->lobjCompanieslistModel->fnupdateCompanyDetails ( $lintIdCompany, $larrformData ); //Function for updating the Company Details
					
				}
				echo "<script>parent.location = '".$this->view->baseUrl()."/general-setup/companieslist/index';</script>";
			}
			
		}
	}
	
	
public function resetpasswordAction() {
		$this->_helper->layout->disableLayout();

		$lobjTakafuloperatorForm = $this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm; //send the lobjTakafuloperatorForm object to the view
		$lintIdCompany = $this->_getParam ( 'lintIdCompany' );
		
		$this->view->lintIdCompany = $lintIdCompany;
		
		$larrresult = $this->lobjCompanieslistModel->fnGenerateQueries(1,0,$lintIdCompany); //get Company details
		
		$this->view->lobjTakafuloperatorForm->Login->setValue ( $larrresult ['Login'] );
		$this->view->lobjTakafuloperatorForm->Login->setAttrib ( 'readonly', true );
		
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			
			if ($this->_request->isPost ()) {				
			
				unset ( $larrformData ['Save'] );
					$ldtsystemDate = date ( 'Y-m-d H:i:s' );
					$larrformData ['UpdDate'] = $ldtsystemDate;
					$larrformData ['UpdUser'] = '1';
					$larrformData ['Password'] = md5($larrformData ['NewPassword']);
					
					
					
					$this->lobjCompanieslistModel->fnupdateCompanyDetails ( $lintIdCompany, $larrformData ); //Function for updating the Company Details
					
						
						//Get Email Template Description
						$StudModel = new App_Model_Studentapplication();
						$larrEmailTemplateDesc =  $StudModel->fnGetEmailTemplateDescription("Company Reset Password");
										
			
						
						//Get Company's Mailing Details
						$larrCompanyMailingDetails = $larrresult;
						
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							
							$lstrEmailTemplateBody = str_replace("[Person]",$larrCompanyMailingDetails['ContactPerson'],$lstrEmailTemplateBody);	
							$lstrEmailTemplateBody = str_replace("[AdminName]",'ibfiminfo@gmail.com',$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Company]",$larrCompanyMailingDetails['CompanyName'],$lstrEmailTemplateBody);		
							$lstrEmailTemplateBody = str_replace("[LoginId]",$larrCompanyMailingDetails['Login'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Password]",$larrformData['NewPassword'],$lstrEmailTemplateBody);
							
						
							
							$larrEmailId = $larrCompanyMailingDetails['Email'];
							$larrName 	 = $larrCompanyMailingDetails['CompanyName'];
			
							$auth = 'ssl';
							$port = '465';
							$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
							//$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
							
							try{
								$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
								}
							catch(Exception $e) {
											echo '<script language="javascript">alert("Invalid Mail Settings")</script>';
											
							}
						
							$mail = new Zend_Mail();
							$mail->setBodyHtml($lstrEmailTemplateBody);
							$sender_email = 'ibfiminfo@gmail.com';
							$sender = 'ibfim';
							$receiver_email = $larrEmailId;			
							$receiver = $larrName;
							$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
									         
										         
							//$mail = new Zend_Mail();
							try{
								$mail->setBodyHtml($lstrEmailTemplateBody);
								}
							catch(Exception $e){
											echo '<script language="javascript">alert("Invalid Mail Content")</script>';
								}
								
							 try {
									//$result = $mail->send($transport);
								} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
				               }			
				
			}
			$this->_redirect ( $this->baseUrl . '/general-setup/companieslist/editcompany/idcompany/'.$lintIdCompany );
		}
	}
	public function companytypepaymentchangeAction() {
		$idcompanypaymenttype = $this->_getParam('id');
		
    	$this->view->lobjCompanytypepaymentForm = $this->lobjCompanieslistForm;
    	$this->lobjCompanieslistForm->idcompanypaymenttype->setValue($idcompanypaymenttype);
    	
    	$larrcompanyname = $this->lobjCompanieslistModel->fnGetcompanyNameedit();
    	$this->lobjCompanieslistForm->idcompany->setAttrib(readonly,true);
		$this->lobjCompanieslistForm->idcompany->addMultiOptions($larrcompanyname);
		
		$larrcompanyname = $this->lobjCompanieslistModel->fnGetPaymentTerms();
		$this->lobjCompanieslistForm->paymenttype->addMultiOptions($larrcompanyname);
		
		$ldtsystemDate = date('Y-m-d:H-i-s');
		$this->lobjCompanieslistForm->upddate->setValue($ldtsystemDate);
		
		$upduser = 1;//$auth->getIdentity()->iduser;
		$this->lobjCompanieslistForm->upduser->setValue($upduser);
		if($idcompanypaymenttype){
		$editresult = $this->lobjCompanieslistModel->editcompanytypepayment($idcompanypaymenttype);
		}else{
			$idcompany = $this->_getParam('idcompany');
			$editresult['idcompany']=$idcompany;
			$editresult['paymenttype']="";
			$editresult['upduser']=1;
			$editresult['upddate']=$ldtsystemDate;
		}
	    $this->lobjCompanieslistForm->populate($editresult);
		
       if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
		    $larrformData = $this->_request->getPost ();
		    $idcompanypmttype = $larrformData['idcompanypaymenttype'];
		    unset($larrformData['idcompanypaymenttype']);
		    unset($larrformData['Save']);
		    if($idcompanypmttype){
		    $larrresult = $this->lobjCompanieslistModel->fnupdatecompanytypepayment($larrformData,$idcompanypmttype);
		    }else{
		    	 $larrresult = $this->lobjCompanieslistModel->fninsertdata($larrformData);
		    }
			echo "<script>parent.location = '".$this->view->baseUrl()."/general-setup/companieslist/index';</script>";
		}
		
	}
	
public function fnajaxgetcompanynamesAction()
		{   
			$this->_helper->layout->disableLayout();//disable layout
			$this->_helper->viewRenderer->setNoRender();//do not render the view	
			$Companyname=$this->_getParam('Companyname'); // get the centername 
			$larrCompanyname = $this->lobjCompanieslistModel->fngetexistingCompanyname($Companyname); // search the DB for the centername
		    if($larrCompanyname)
		    {
		    	echo 'The Company name provided already exists, please provide with another Takafull name';
		    }
		}
}