<?php
class GeneralSetup_PromoteController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjpromote;
	//private $lobjcenterForm;
	
	public function init() { //initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		
		$this->lobjpromote = new GeneralSetup_Model_DbTable_Promote(); //user model object
		//$this->lobjcenterForm = new GeneralSetup_Form_Center (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		
	}
	

	public function indexAction() { // action for search and view
		
		$lobjform=$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		$larrresult = $this->lobjpromote->fngetPromoteDetails (); //get user details

		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->promotepaginatorresult);
			
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->promotepaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->promotepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjpromote->fnPromoteSearch($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->promotepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/general-setup/promote/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'promote', 'action'=>'index'),'default',true));
		}


	}

	

}