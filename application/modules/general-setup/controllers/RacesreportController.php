<?php
class GeneralSetup_RacesreportController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;	
	
	public function init() { //initialization function
		$this->gobjsessionstudent = Zend_Registry::get('sis');				
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() 
	{
		$this->lobjRacesreportModel = new GeneralSetup_Model_DbTable_Racesreport(); //Racesreport model object
		$this->lobjRacesreportForm = new GeneralSetup_Form_Racesreport (); //intialize Racesreport lobjRacesreportForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

	public function indexAction() 
	{ // action for search and view
		$this->view->lobjRacesreportForm = $this->lobjRacesreportForm;
		$larresult = $this->lobjRacesreportModel->fnGetbatch();
		$this->lobjRacesreportForm->Batch->addMultiOptions($larresult);
	}

	public function racesAction() 
	{ //Action for creating the new user
		//$this->_helper->layout->disableLayout();
		
		$this->view->InitBatch = $this->view->Batch;
		$result =  $this->_request->getPost();		
		/*echo"<pre/>";
		print_r($result);
		die();*/
		$this->view->Batch = $result['Batch'];
		$this->view->Chart = $result['Chart'];
		$larrracedetails=$this->lobjRacesreportModel->fnGetracevalues();
		/*echo"<pre/>";
		print_r($larrxvalues);
		die();*/
		$this->view->larrracedetails=$larrracedetails;
		$larrbatchdetails = $this->lobjRacesreportModel->fnGetBatchDetails($result['Batch']);
/*		print_R($larrbatchdetails);
		die();*/
		$this->view->batchname = $larrbatchdetails[0]['BatchName'];
		$this->view->BatchFrom = $larrbatchdetails[0]['BatchFrom'];
		$this->view->BatchTo = $larrbatchdetails[0]['BatchTo'];
		$this->view->ProgramName = $larrbatchdetails[0]['ProgramName'];
		
		$larrtotalnoofstudents = $this->lobjRacesreportModel->fngetnoofstudents($result['Batch']);
		$totalnoofstudents = $larrtotalnoofstudents['Batch'];
		$this->view->totalnoofstudents = $totalnoofstudents;
		
		$string = $totalnoofstudents;
		
		$larrtotalattended = $this->lobjRacesreportModel->fnTotalAttended($result['Batch']);
		$totalattended = count($larrtotalattended);
		$this->view->totalattended = $totalattended;
		
		$string = $string.','.$totalattended;
		
		
		$absent = $totalnoofstudents-$totalattended;
		$this->view->absent = $absent;
		
	}

	public function printAction()
	{
		$this->_helper->layout()->setLayout('/reg/usty1');
			$batchprint = $this->_getParam('batchprint');	
			$batchChart = $this->_getParam('batchChart');
			$this->view->Batch = $batchprint;
		$this->view->Chart = $batchChart;
		
		
		$larrbatchdetails = $this->lobjRacesreportModel->fnGetBatchDetails($batchprint);
/*		print_R($larrbatchdetails);
		die();*/
		$this->view->batchname = $larrbatchdetails[0]['BatchName'];
		$this->view->BatchFrom = $larrbatchdetails[0]['BatchFrom'];
		$this->view->BatchTo = $larrbatchdetails[0]['BatchTo'];
		$this->view->ProgramName = $larrbatchdetails[0]['ProgramName'];
		
		$larrtotalnoofstudents = $this->lobjRacesreportModel->fngetnoofstudents($batchprint);
		$totalnoofstudents = $larrtotalnoofstudents['Batch'];
		$this->view->totalnoofstudents = $totalnoofstudents;
		
		$string = $totalnoofstudents;
		
		$larrtotalattended = $this->lobjRacesreportModel->fnTotalAttended($batchprint);
		$totalattended = count($larrtotalattended);
		$this->view->totalattended = $totalattended;
		
		$string = $string.','.$totalattended;
		
		
		$absent = $totalnoofstudents-$totalattended;
		$this->view->absent = $absent;	
		
		
		//////////////////percentage///////////////////
		
		
		$attendpercentage = (($totalattended)*100)/$totalnoofstudents;
		$this->view->attendpercentage =$attendpercentage;
		$this->view->absentpercentage = 100-($attendpercentage);
	}

}