<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class GeneralSetup_NewscreenController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	public function fnsetObj()
	{	
		$this->lobjnewscreenmodel = new GeneralSetup_Model_DbTable_Newscreen(); //intialize newscreen db object
		$this->lobjnewscreenForm = new GeneralSetup_Form_Newscreen(); 
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object

       //$this->lobjstudentForm = new App_Form_Studentapplication();
	
	
	}
	
	public function indexAction() 
	{
		
		
		$this->view->lobjform = $this->lobjnewscreenForm;
		
	$larrresultmaximum = $this->lobjnewscreenmodel->fngetstudentinformationfromconfig(); 
	//	echo "<pre>";
		//print_r($larrresultmaximum['studedit']);
	//	die();
		
				$larrresult = $this->lobjnewscreenmodel->fngetstudentinformation($larrresultmaximum['studedit']); 
				$larrresult=0;
		//print_r($larrresult);die();
		
	if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->studentchangevenuepaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		
		$larrStudentnameresult = $this->lobjnewscreenmodel->fnGetStudentNames($larrresultmaximum['studedit']);	
		$this->lobjnewscreenForm->Studentname->addMultiOptions($larrStudentnameresult);
		
		
		$larrCourseresult = $this->lobjnewscreenmodel->fnGetCourseNames();	
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrCourseresult);
		$larrVenuesresult = $this->lobjnewscreenmodel->fnGetVenueNames();	
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrVenuesresult);
		$larrTakafulresult = $this->lobjnewscreenmodel->fnGetTakafulNames();	
		$this->lobjnewscreenForm->Takafulname->addMultiOptions($larrTakafulresult);
		
		if(isset($this->gobjsessionsis->studentchangevenuepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->studentchangevenuepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$this->view->paramsearch =  $this->_getParam('search');
				//echo "<pre>";
				//print_r($larrformData);
				//die();
				
				 $larrresult = $this->lobjnewscreenmodel->fnSearchStudent($larrformData,$larrresultmaximum['studedit']); //searching the values for the user
				$this->view->larrresult =$larrresult;
				 $this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->studentchangevenuepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/general-setup/newscreen/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
		
		
		
		
		
		
	}
	
	public function editstudentinfoAction() 
	{	
		
		$this->view->lobjnewscreenForm = $this->lobjnewscreenForm;
		
		$lintidstudent = $this->_getParam('id');
		$this->view->id = $lintidstudent;
		//echo $lintidstudent;die();
		$larrstudentinforesult= $this->lobjnewscreenmodel->fngetstudenteachinformation($lintidstudent); 
		//echo "<pre>";
		//print_r($larrstudentinforesult);
		$this->view->AppliedDate=$larrstudentinforesult['AppliedDate'];
	$todaydate=date('Y-m-d');
		//$todaydate='2013-01-01';
		//echo $todaydate;
	$expireddate=0;
		if($todaydate>$larrstudentinforesult['DateTime'])
		{
			
			$expireddate=1;
		
		}
		$this->view->exp=$expireddate;

		//echo "<pre>";
		//print_r($larrstudentinforesult);
		//die();
		$this->view->Fname=$larrstudentinforesult['FName'];
		$this->view->EmailAddress=$larrstudentinforesult['EmailAddress'];
		$this->view->ICNO=$larrstudentinforesult['ICNO'];
		$this->view->AppliedDate=$larrstudentinforesult['AppliedDate'];
		
		$larrcourse = $this->lobjnewscreenmodel->fnGetCourseNames();
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrcourse);
		$this->view->lobjnewscreenForm->Coursename->setValue($larrstudentinforesult['IdProgrammaster']);
		 
		$this->view->lobjnewscreenForm->Coursename1->setValue($larrstudentinforesult['IdProgrammaster']);
		//echo $larrstudentinforesult['IdProgrammaster'];die();
		//$this->view->lobjnewscreenForm->Coursename->setValue();
				$this->view->examdateDate=$larrstudentinforesult['DateTime'];
					
		$larrscheduler = $this->lobjnewscreenmodel->fnGetSchedulerDetails();
		$this->lobjnewscreenForm->schedulerename->addMultiOptions($larrscheduler);
		$this->view->lobjnewscreenForm->schedulerename->setValue($larrstudentinforesult['Year']);
		
			
		$larrstate = $this->lobjnewscreenmodel->fnGetStateName();
		$this->lobjnewscreenForm->examstate->addMultiOptions($larrstate);
		
		$this->view->lobjnewscreenForm->examstate->setValue($larrstudentinforesult['ExamState']);
		
		$larrcity = $this->lobjnewscreenmodel->fnGetCityName($larrstudentinforesult['ExamCity']);
		$this->lobjnewscreenForm->examcity->addMultiOptions($larrcity);
		$this->view->lobjnewscreenForm->examcity->setValue($larrstudentinforesult['ExamCity']);
			
			
		$larrvenue = $this->lobjnewscreenmodel->fnGetVenueName($larrstudentinforesult['ExamCity']);
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrvenue);
		$this->view->lobjnewscreenForm->Venues->setValue($larrstudentinforesult['Examvenue']);
			
		$larrschedulersession = $this->lobjnewscreenmodel->fnGetSchedulerSessionDetails($larrstudentinforesult['Examsession']);
		$this->lobjnewscreenForm->examsession->addMultiOptions($larrschedulersession);
		$this->view->lobjnewscreenForm->examsession->setValue($larrstudentinforesult['Examsession']);
			
		$larrscheduleryear = $this->lobjnewscreenmodel->fnGetSchedulerYearDetails($larrstudentinforesult['Year']);
		$examdate=$larrstudentinforesult['Examdate'].'-'.$larrstudentinforesult['Exammonth'].'-'.$larrscheduleryear['year'];
		$this->view->lobjnewscreenForm->Examdate->setValue($examdate);
			
		$this->view->lobjnewscreenForm->paymentmode->setValue($larrstudentinforesult['ModeofPayment']);
		$this->view->lobjnewscreenForm->Payment->setValue($larrstudentinforesult['Payment']);
		$this->view->lobjnewscreenForm->Coursename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->Coursename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->schedulerename->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examstate->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examcity->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->Venues->setAttrib('readonly','true'); 
		$this->view->lobjnewscreenForm->examsession->setAttrib('readonly','true');
												$this->view->lobjnewscreenForm->Examdate->setAttrib('readonly','true');
				                              $this->view->lobjnewscreenForm->paymentmode->setAttrib('readonly','true');
												$this->view->lobjnewscreenForm->Payment->setAttrib('readonly','true');
				
				$larrcourse = $this->lobjnewscreenmodel->fnGetCourseNames();
		$this->lobjnewscreenForm->Coursename1->addMultiOptions($larrcourse);
		$this->view->lobjnewscreenForm->Coursename1->setAttrib('readonly','true');
		
		//$auth = Zend_Auth::getInstance();
		//$this->view->lobjCompanypaymentForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		
	if ($this->_request->isPost() && $this->_request->getPost('Yes')) {
			$larrformData = $this->_request->getPost();
			
			
				$auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
				
				$idvenue = $larrformData['idvenues'];		
			$arrworkphone = explode("-",$idvenue);
		      $idcenter= $arrworkphone [0];
	          $idsession= $arrworkphone[1];	
				//echo $iduser;
				//echo "<pre/>";
			//echo  $idcenter;
			//echo  $idsession;
				
				
				
		$larrformData['UpdDate']=date('Y-m-d H:i:s');
			$idapplication = $larrformData['IDApplication'];
			
			$studenteditresult = $this->lobjnewscreenmodel->fngetstudentoldinfo($idapplication);
			//print_r($studenteditresult);die();
			
			
			$change=$studenteditresult['Venue'];
			$venuechange=$change+1;
		
			$studenteditiinserlarr=$this->lobjnewscreenmodel->fngetstudentinsertinfo($studenteditresult,$iduser,$larrformData['UpdDate'],$larrformData['newexamcity'],$idsession,$larrformData);
			//print_r($studentgetcityandstate);die();
	$studentgetcityandstate=$this->lobjnewscreenmodel->fnGetvenuecity($larrformData['newexamcity']);
			$idstate=$studentgetcityandstate['state'];
			$idcity=$studentgetcityandstate['city'];
			//print_r($larrformData);die();
  			 	$this->lobjnewscreenmodel->fnUpdateStudentnewscreen($idapplication,$larrformData,$larrformData['hiddenscheduler'],$iduser,$larrformData['newexamcity'],$idsession,$idstate,$idcity,$venuechange);
  		






	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    						
					//$this->view->mess = "Payment Completed Sucessfully";
					//$this->view->mess = "Payment Completed Sucessfully <br/> Please check your mail box If you have not received a confirmation mail in next 30minutes<br/>Please check your spam folder Add ibfiminfo@gmail.com to the address book to ensure future communications doesnt go to the spam folder";
					$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($idapplication);	
					$larrregid = $this->lobjstudentmodel->fngetRegid($idapplication);
					//print_r($larrregid);die();
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Venue Change");
						
						//echo "<pre />";
						//print_r($larrresult);
						//die();
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
									//print_R($larrresult);die();
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										 $lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
										//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'].'('.$larrresult['ampmstart'].'--'.$larrresult['ampmend'].')',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										//print_r($lstrEmailTemplateBody);
										//die();
									/*	$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
									//$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									//echo '<script language="javascript">alert("Because of server problem mails cannot be send this time")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
								
								 //$this->_redirect( $this->baseUrl . "/registration/index");
								if(mess){
									
								}
					
    			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    		




  			 		$this->_redirect( $this->baseUrl . '/general-setup/newscreen/index');		
  		//echo "<pre>";
  		//print_r($larrformData);
  		//die();
  		}
			
		
	}
	
 public function fngetyearAction()
 {
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjnewscreenmodel->fnGetYearlistforcourse($Program);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
 }
 
 
 public function fngetstatenameAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
			$year= $this->_getParam('year');

		$larrvenuetimeresult = $this->lobjnewscreenmodel->fnGetStatelistforcourse($Program,$year);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}

public function fngetcitynamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('idstate');
		$Program = $this->_getParam('Program');
		$idseched = $this->_getParam('idsecheduler');

		$larrvenuetimeresults = $this->lobjnewscreenmodel->fnGetCitylistforcourse($lintdate,$Program,$idseched);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}


public function fngetvenuenamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('idcity');
		$Program = $this->_getParam('Program');
		$idseched = $this->_getParam('idsecheduler');

		$larrvenuetimeresults = $this->lobjnewscreenmodel->fnGetVenuelistforcourse($lintdate,$Program,$idseched);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}



public function fngetmonthofsessionAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$idmonth = $this->_getParam('idmonth');
		$Program = $this->_getParam('Program');
		$Year = $this->_getParam('Year');

      $Newvenue = $this->_getParam('NewCity');
      
		$larrsessiontimeresults = $this->lobjnewscreenmodel->fnGetSessionforthemonth($Program,$Year,$Newvenue);
		//print_r($larrsessiontimeresults);die();
		$values=0;
foreach($larrsessiontimeresults as $months)
		{
		if(( $idmonth >= $months['From']) && ($idmonth<= $months['To'] ))
		{
			
			$value=$months['idnewscheduler'];
				$values=$values.','.$value;
				
			
		}
		}
		//echo ($values);die();
			$larrexectdaysresults = $this->lobjnewscreenmodel->fnGetExectSessiondays($values);
		//print_r($larrexectdaysresults);die();
		//$i=0;
			foreach($larrexectdaysresults as $days)
			{
			$larrexectsessiontimeresults = $this->lobjnewscreenmodel->fnGetExectSession($values,$days['iddays']);
			//print_r($larrexectsessiontimeresults);die();
			$sessioncount[]=count($larrexectsessiontimeresults);	
			//echo count($larrexectsessiontimeresults);die();
			//$i++;
			}
		
			$maximumsesiion=max($sessioncount);
		//print_r($maximumsesiion);die();
		echo $maximumsesiion;die();
		
		//$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}


public function fngetmonthnameAction()
{
	 $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
			$year= $this->_getParam('year');

		$larrvenuetimeresult = $this->lobjnewscreenmodel->fnbetweenmonths($year);
		//print_r($larrvenuetimeresult);
		//die();
		
		$frommonth = $larrvenuetimeresult['From'];
		$tomonth = $larrvenuetimeresult['To'];
		$year = $larrvenuetimeresult['Year'];
		$curyear=date('Y');
		if($curyear==$year)
		{
			$curmonth=date('m');
			
			if($frommonth<=$curmonth)
			{
				//echo $frommonth;die();
			$larrresults = $this->lobjnewscreenmodel->fnGetmonthsbetween2($tomonth);
			}
			else 
			{
				$larrresults = $this->lobjnewscreenmodel->fnGetmonthsbetween($frommonth,$tomonth);
			}
			}
		else 
		{
		$larrresults = $this->lobjnewscreenmodel->fnGetmonthsbetween($frommonth,$tomonth);
		}
		//$larrmonthslist = $this->lobjstudentmodel->fnGetMonthlistofcourse($from,$to);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresults);
		//$larrCountryStatesDetails[]=array('key'=>'0','name'=>'Entire Calender');
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}

public function caleshowmonthAction()
{
	//$this->lobjstudentmodel = new App_Model_Studentapplication(); 
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$NewCity = $this->_getParam('NewCity');
		$idmonth = $this->_getParam('idmonth');
		//$no = $this->_getParam('no');
		$year = $this->_getParam('Year');
		$Program = $this->_getParam('Program');
		$expiredate= $this->_getParam('applieddate');
	            $expiredate2=explode('-',$expiredate);
	            
	            //echo $expiredate2[1];die();
	           // $expiredmonth=$expiredate2[1];
	            //$expiredyear=$expiredate2[0];
	            //$expireddate=$expiredate2[2];
		$expiredmonth=date('m');
		$expireddate=date('d')+1;
		$expiredyear=date('Y');
		//$this->lobjCenterloginmodel = new App_Model_Centerlogin(); 
     $resultcount = $this->lobjnewscreenmodel->fngetcountofsessions($NewCity,$year);

		$mondays="#FFFFF";
		$tuesdays="#FFFFF";
		$wednesdays="#FFFFF";
		$thursdays="#FFFFF";
		$fridays="#FFFFF";
		$saturdays="#FFFFF";
		$sundays="#FFFFF";
		for($k=0;$k<count($resultcount);$k++)
				{
			
					switch($resultcount[$k]['countidmanagesession'])
					{

						 Case 1:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="green";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="green";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="green";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="green";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="green";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="green";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="green";
							 	   	  break;
						 		}
						 		break;   

						 Case 2:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="skyblue";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="skyblue";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="skyblue";
							 	   	  break;
						 		}
						 		break; 
						 		
						 Case 3:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="violet";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="violet";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="violet";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="violet";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="violet";
							 	   	  break;
						 		}
						 		break; 
						
						 Case 4:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="pink";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="pink";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="pink";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="pink";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="pink";
							 	   	  break;
						 		}
						 		break; 
						}
				}
		//////////
		
		
		$larrresult = $this->lobjnewscreenmodel->fngetmonthcalendar($idmonth,$year,$Program,$NewCity);
		//print_r($larrresult);
	
		$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<count($larrresult);$j++)
		{
			/*print_r($larrdays);
			die();*/
			if($larrresult[$j]['Days']==1)
			  $monday=1;
			 if($larrresult[$j]['Days']==2)
			  $tuesday=1;
			 if($larrresult[$j]['Days']==3)
			  $wednesday=1;
			  if($larrresult[$j]['Days']==4)
			  $thursday=1;
			  if($larrresult[$j]['Days']==5)
			  $friday=1;
			  if($larrresult[$j]['Days']==6)
			  $saturday=1;
			  if($larrresult[$j]['Days']==7)
			  $sunday=1;
		}
		

		 $curmonth = date('m');      
$monat=date('n');
$jahr=$year;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0  width=25% align=center>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{
	$cnt++;
		//print_r($cnt);  
	if($idmonth==$cnt)
	{
		
	
		if($idmonth==$curmonth)
		{
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				
				$curdate = date('d')+1;
				if($curdate>$expireddate)
				{
					$expireddate=$curdate;
				}
				
				if($i<$expireddate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
				//if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				else {
				
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
		else
			 {
			$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				if($cnt==$expiredmonth)
				{
						
						$i=1;
						while($i<=$insgesamt)
						{
						$rest=($i+$erster-1)%7;
						if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
						else{echo '<td  align=center ';}
						//$curdate = date('d')+14;
						if($i<$expireddate+1)
						{
							 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{}
							echo "</td>\n";
						}
								
						else 
						
						{
						if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
						}
						if($rest==0){echo "</tr>\n<tr>\n";}
						$i++;
						}
						
				}
				else 
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
      ///////
     }
    
}
echo '</tr>';
}
echo '</table>';
}




public function schedulerexceptionAction()
{
$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintday = $this->_getParam('day');//city
		$lintcity = $this->_getParam('city');
		$lintmonth = $this->_getParam('month');
		$year = $this->_getParam('year');
		$days = $year.'-'.$lintmonth.'-'.$lintday;
		//echo $days;die();
		$larrresultcity=$this->lobjnewscreenmodel->newfnGetcitydetailsgetsecid($lintcity);
		//print_r($larrresultcity);die();
		$resultsss = $this->lobjnewscreenmodel->fngetschedulerexception($days,$larrresultcity['city']);
	$counts = count($resultsss);
	if($counts>1)
	{
		echo "No exams are offerred on the selected date. It can be a public holiday, please select a different date.";
		die();
		
	}
	else 
	{
		
	}	
	
}

public function tempdaysAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day = $this->_getParam('day');
		$year = $this->_getParam('year');
		$month = $this->_getParam('month');
		$city = $this->_getParam('city');
		$prog = $this->_getParam('Program');
		$dateid = $day.''.$month;
		
		$idsechduler=$this->lobjnewscreenmodel->fnnewmonthcaleshow($prog,$city,$year);
		
		
		$values=0;
			for($i=0;$i<count($idsechduler);$i++)
			{
				
							if(( $month >= $idsechduler[$i]['From']) && ($month <= $idsechduler[$i]['To'] ))
		{
				$value=$idsechduler[$i]['idnewscheduler'];
				$values=$values.','.$value;
		}
			}
		
			if($day<10)
			{
				$day='0'.$day;
			}
		if($month<10)
			{
				$month='0'.$month;
			}
			
			$examdate=$year.'-'.$month.'-'.$day;
			
			$idexamday=$this->lobjnewscreenmodel->fngetdayofdate($examdate);
			
			
			if($idexamday['days']==1)
			{
				$idexamday['days']= 7;
			}
			else 
			{
			$idexamday['days']=$idexamday['days']-1;	
			}
			
			
			
			$idcorrectsechduler=$this->lobjnewscreenmodel->fngetschedulerofdate($values,$idexamday['days']);
		
		
		
		
	
		$idcorrectsec=$idcorrectsechduler[0]['idnewscheduler'];
		//echo $idcorrectsec;	
		//print_r($idexamday);
			//print_r($larrschedulerdays);
			//print_r($idcorrectsechduler);
		
		//die();
		$idexamcity=$this->lobjnewscreenmodel->newfnGetcitydetailsgetsecid($city);
		
		//$idcorrectsechduler=$this->lobjstudentmodel->newfnGetVenuedetailsgetsecid($year);
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjnewscreenmodel->fnGetVenuedetailsRemainingseats($year,$idcorrectsec,$idexamcity['city'],$month,$day,$city);
		//echo "<pre />";
		//print_r($venueselect);
		//die();
		
		$flag=0;
		foreach($venueselect as $ven)
		{
			if($ven['rem']>0)
			{
				$flag=1;
			}
			
		}
		if($flag==1)
		{
		$flag=$idcorrectsec;
		}
		echo $flag;die();
}



public function fngetvenuesessiondetailsAction()
	{
		
		     $this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
	
			//Get Country Id
			$Year= $this->_getParam('Year');
			//$Program = $this->_getParam('Program');
			$idcity = $this->_getParam('idcity');
			$venue = $this->_getParam('venue');
			$day = $this->_getParam('day');
			$month = $this->_getParam('month');
	//echo $day."<br>";
	//	echo $Year."<br>";
		//	echo $idcity."<br>";
			//echo $venue."<br>";
			//echo $month."<br>";die();
		$idsechduler=$this->lobjnewscreenmodel->fnGetVenuedetailsgetsecid($Year);
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjnewscreenmodel->fnGetVenuedetailsRemainingseats($idsechduler['Year'],$idsechduler['idnewscheduler'],$idcity,$month,$day);
		$flag=0;
		$idsession=0;
		$venueid=0;
		//$idsession="";
		foreach($venueselect as $ven)
		{
			if($ven['rem']>0)
			{
				$flag=1;
			}
			if($ven['rem']==0 && ($ven['idcenter']==$venue))
			{
				//$idsession=$ven['idmangesession'];
				 $idsession=$idsession.','.$ven['idmangesession'];
				// $idsessions=$ven['idmangesession'];
			}

			if($ven['rem']<0 && ($ven['idcenter']==$venue))
			{	
				$idsession=$idsession.','.$ven['idmangesession'];
			} 
			
			
		}
	//	echo $idsession;die();
			$venueselect = $this->lobjnewscreenmodel->fnGetsesssiondetails($Year,$idcity,$venue,$idsession,$day,$month);
			
			$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($venueselect);
			echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
	}
	
	
	
	
	public function selectvenueAction()
	{
		
		$this->_helper->layout->disableLayout();
	
		$this->view->lobjstudentForm = $this->lobjnewscreenForm; //send the lobjuserForm object to the view
		
		//$venueselect = $this->lobjstudentmodel->fnGetVenuedetailsRemainingseats($larrresult['yearss'],$larrresult['Year'],$larrresult['ExamCity'],$larrresult['Exammonth'],$larrresult['Examdate'],$larrresult['Examvenue']);
		
		$day = $this->_getParam('day');
		$year = $this->_getParam('year');
		$idsechduler = $this->_getParam('idsechduler');
		$month = $this->_getParam('month');
		$city = $this->_getParam('city');
		//echo "<pre/>";
		//print_r($larrresult7);die();
		$idvenuecity=$this->lobjnewscreenmodel->fnGetvenuecity($city);
		//print_r($idvenuecity['city']);die();
		//print_r($idsechduler['idnewscheduler']);
		$examcity=$idvenuecity['city'];
		$venueselect = $this->lobjnewscreenmodel->fnGetVenuedetailsRemainingseats($year,$idsechduler,$examcity,$month,$day,$city);
	//echo "<pre/>";
	 //	print_r($venueselect);
	 //die();
		
		$larrdate=$year.'-'.$month.'-'.$day;
		//echo $larrdate;die();
		//$contvenueselect = $this->lobjstudentmodel->fnCountVenuedetails($larrdate);
		
		$this->view->dates = $day.'-'.$month.'-'.$year;
		$dates=$year.'-'.$month.'-'.$day;
		
		//$datesel=".$dates.";
		//	$day=day($dates);
		$result5 = $this->lobjstudentmodel->fngetdayStudent($dates); 
		//echo $result5[0]['days'];die();
		$this->view->daystu= $result5[0]['days'];
		$this->view->larrvenues = $venueselect;
		
		
	}
	///////////////////
	
	
	
	public function newfngetyearAction()
 {
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjnewscreenmodel->newfngetyear($Program);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
 }
public function newfngetcitynamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
		$year = $this->_getParam('year');
		//echo $Program;die();
		//$idseched = $this->_getParam('idsecheduler');
   $curmonth=date('m');
      
		if($curmonth<10)
		{
			$curmonth = $curmonth[1];

		}
		
   //echo $curmonth;die();
		$larrvenuetimeresults = $this->lobjnewscreenmodel->newfnGetCitylistforcourse($Program,$year,$curmonth);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}


public function newfnnewcaleshowAction()
{
	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		//Get Country Id
		$Program = $this->_getParam('Program');//cityyear
		$NewCity = $this->_getParam('NewCity');
			$year = $this->_getParam('year');
		$resultsss = $this->lobjstudentmodel->fnnewmonthcaleshow($Program,$NewCity,$year);
		
/*		$this->lobjCenterloginmodel = new App_Model_Centerlogin();
		$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($NewCity,date('Y'));
		foreach($resultcount as $resultcounts) {
			$maxcolourcodes[] = $resultcounts['countidmanagesession'];
		}
		$this->view->maxcolourcodes = max($maxcolourcodes);*/
		
		
		//////////////
		$this->lobjCenterloginmodel = new App_Model_Centerlogin(); 
$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($NewCity,$year);

		$mondays="#FFFFF";
		$tuesdays="#FFFFF";
		$wednesdays="#FFFFF";
		$thursdays="#FFFFF";
		$fridays="#FFFFF";
		$saturdays="#FFFFF";
		$sundays="#FFFFF";
		for($k=0;$k<count($resultcount);$k++)
				{
			
					switch($resultcount[$k]['countidmanagesession'])
					{

						 Case 1:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="green";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="green";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="green";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="green";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="green";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="green";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="green";
							 	   	  break;
						 		}
						 		break;   

						 Case 2:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="skyblue";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="skyblue";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="skyblue";
							 	   	  break;
						 		}
						 		break; 
						 		
						 Case 3:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="violet";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="violet";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="violet";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="violet";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="violet";
							 	   	  break;
						 		}
						 		break; 
						
						 Case 4:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="pink";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="pink";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="pink";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="pink";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="pink";
							 	   	  break;
						 		}
						 		break; 
						}
				}
		//////////
		
		
		
		
		
		
		
		//print_r($resultsss);die();
	$values=0;
			for($i=0;$i<count($resultsss);$i++)
			{
				$value=$resultsss[$i]['idnewscheduler'];
				$values=$values.','.$value;
			}
			
			
		$larresultofmonths = $this->lobjstudentmodel->fnnewmonths($values);
		
	/*for($i=0;$i<count($resultsss);$i++)
	{
		$minfrommonth = $resultsss[$i]['From'];
		if($i!=0)
		{
			if($minfrommonth>$resultsss[$i]['From'])
				{
					$minfrommonth=$resultsss[$i]['From'];
				}
				
		      if($maxtomonth<$resultsss[$i]['To'])
				{
					$maxtomonth=$resultsss[$i]['To'];
				}
		}
		
	}*/
	$frommonth = $larresultofmonths[0]['minimum'];

	
			$tomonth = $larresultofmonths[0]['maximum'];

			
			//$resultsss22 = $this->lobjstudentmodel->fnnewmonthsrange($frommonth,$tomonth);
			//print_r($resultsss22);die();
     
		    $curmonth = date('m');  
$monat=date('n');
$jahr=$year;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0  width=100% align=center>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{

	$cnt++;
	
	
	$larrdays = $this->lobjstudentmodel->fngetdays($NewCity,$cnt,$year);
	if(count($larrdays)<1)
	{
	$larrdays = $this->lobjstudentmodel->fngetdaysto($NewCity,$cnt,$year);
	}
 	if(count($larrdays)<1)
	{
	$larrdays = $this->lobjstudentmodel->fngetdaysbetween($NewCity,$cnt,$year);
	}
/*	print_R($larrdays);
	die();*/
$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<count($larrdays);$j++)
		{
			/*print_r($larrdays);
			die();*/
			if($larrdays[$j]['Days']==1)
			  $monday=1;
			 if($larrdays[$j]['Days']==2)
			  $tuesday=1;
			 if($larrdays[$j]['Days']==3)
			  $wednesday=1;
			  if($larrdays[$j]['Days']==4)
			  $thursday=1;
			  if($larrdays[$j]['Days']==5)
			  $friday=1;
			  if($larrdays[$j]['Days']==6)
			  $saturday=1;
			  if($larrdays[$j]['Days']==7)
			  $sunday=1;
		}


 $curmonth = date('m');
if($curmonth==$cnt)
{
			
		        $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = date('d');
				if($i<$curdate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
						
				else 
				
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
				//}
			
}
else
 {
 	       if($cnt>=$curmonth)
 	       {
                $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = 0;
				if($i<$curdate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
						
				else 
				
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
 	       }
 	       else 
 	       {
 	       	 $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = 0;
			
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
			
				
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
 	       }
		}
  
}
echo '</tr>';
}
echo '</table>';
	
}
	
public function newfnnewmonthslistAction()
{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	$Program = $this->_getParam('Program');//cityyear
		$NewCity = $this->_getParam('NewCity');
			$year = $this->_getParam('year');
		$resultsss = $this->lobjnewscreenmodel->fnnewmonthcaleshowlatest($Program,$NewCity,$year);
		//print_r($resultsss);die();
	$values=0;
			for($i=0;$i<count($resultsss);$i++)
			{
				$value=$resultsss[$i]['idnewscheduler'];
				$values=$values.','.$value;
			}
			
			
	$larresultofmonths = $this->lobjnewscreenmodel->fnnewmonths($values);
	$frommonth = $larresultofmonths[0]['minimum'];

	
			$tomonth = $larresultofmonths[0]['maximum'];
    $curmonth = date('m');
			if($frommonth<$curmonth)
			{
				$frommonth = $curmonth;
			}
			
			$larrresults = $this->lobjnewscreenmodel->fnnewmonthsrange($frommonth,$tomonth);
			$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresults);
		//$larrCountryStatesDetails[]=array('key'=>'0','name'=>'Entire Calender');
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}



public function newfnnewcaleshowmonthsallAction()
{
	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		//Get Country Id
		$Program = $this->_getParam('Program');//cityyear
		$NewCity = $this->_getParam('NewCity');
			$year = $this->_getParam('year');
		$resultsss = $this->lobjstudentmodel->fnnewmonthcaleshow($Program,$NewCity,$year);
		
/*		$this->lobjCenterloginmodel = new App_Model_Centerlogin();
		$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($NewCity,date('Y'));
		foreach($resultcount as $resultcounts) {
			$maxcolourcodes[] = $resultcounts['countidmanagesession'];
		}
		$this->view->maxcolourcodes = max($maxcolourcodes);*/
		
		
		//////////////
		$this->lobjCenterloginmodel = new App_Model_Centerlogin(); 
$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($NewCity,$year);

		$mondays="#FFFFF";
		$tuesdays="#FFFFF";
		$wednesdays="#FFFFF";
		$thursdays="#FFFFF";
		$fridays="#FFFFF";
		$saturdays="#FFFFF";
		$sundays="#FFFFF";
		for($k=0;$k<count($resultcount);$k++)
				{
			
					switch($resultcount[$k]['countidmanagesession'])
					{

						 Case 1:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="green";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="green";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="green";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="green";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="green";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="green";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="green";
							 	   	  break;
						 		}
						 		break;   

						 Case 2:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="skyblue";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="skyblue";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="skyblue";
							 	   	  break;
						 		}
						 		break; 
						 		
						 Case 3:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="violet";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="violet";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="violet";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="violet";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="violet";
							 	   	  break;
						 		}
						 		break; 
						
						 Case 4:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="pink";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="pink";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="pink";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="pink";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="pink";
							 	   	  break;
						 		}
						 		break; 
						}
				}
		//////////
		
		
		
		
		
		
		
		//print_r($resultsss);die();
	$values=0;
			for($i=0;$i<count($resultsss);$i++)
			{
				$value=$resultsss[$i]['idnewscheduler'];
				$values=$values.','.$value;
			}
			
			
		$larresultofmonths = $this->lobjstudentmodel->fnnewmonths($values);
		
	/*for($i=0;$i<count($resultsss);$i++)
	{
		$minfrommonth = $resultsss[$i]['From'];
		if($i!=0)
		{
			if($minfrommonth>$resultsss[$i]['From'])
				{
					$minfrommonth=$resultsss[$i]['From'];
				}
				
		      if($maxtomonth<$resultsss[$i]['To'])
				{
					$maxtomonth=$resultsss[$i]['To'];
				}
		}
		
	}*/
	$frommonth = $larresultofmonths[0]['minimum'];

	
			$tomonth = $larresultofmonths[0]['maximum'];

			
			//$resultsss22 = $this->lobjstudentmodel->fnnewmonthsrange($frommonth,$tomonth);
			//print_r($resultsss22);die();
     
		    $curmonth = date('m');  
$monat=date('n');
$jahr=$year;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0  width=100% align=center>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{

	$cnt++;
	
	
	$larrdays = $this->lobjstudentmodel->fngetdays($NewCity,$cnt,$year);
	if(count($larrdays)<1)
	{
	$larrdays = $this->lobjstudentmodel->fngetdaysto($NewCity,$cnt,$year);
	}
 	if(count($larrdays)<1)
	{
	$larrdays = $this->lobjstudentmodel->fngetdaysbetween($NewCity,$cnt,$year);
	}
/*	print_R($larrdays);
	die();*/
$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<count($larrdays);$j++)
		{
			/*print_r($larrdays);
			die();*/
			if($larrdays[$j]['Days']==1)
			  $monday=1;
			 if($larrdays[$j]['Days']==2)
			  $tuesday=1;
			 if($larrdays[$j]['Days']==3)
			  $wednesday=1;
			  if($larrdays[$j]['Days']==4)
			  $thursday=1;
			  if($larrdays[$j]['Days']==5)
			  $friday=1;
			  if($larrdays[$j]['Days']==6)
			  $saturday=1;
			  if($larrdays[$j]['Days']==7)
			  $sunday=1;
		}


 $curmonth = date('m');
if(5==$cnt)
{
			
		        $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = date('d');
				if($i<12)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
						
				else 
				
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
				//}
			
}
else
 {
 	       if($cnt>=$curmonth)
 	       {
                $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = 0;
				if($i<12)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
						
				else 
				
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
 	       }
 	       else 
 	       {
 	       	 $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = 0;
			
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
			
				
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
 	       }
		}
  
}
echo '</tr>';
}
echo '</table>';
	
}

public function newfnnewmonthslistvalidateAction()
{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	$Program = $this->_getParam('Program');//cityyear
		$NewCity = $this->_getParam('NewCity');
			$year = $this->_getParam('year');
			$expiredate= $this->_getParam('applieddate');
	            $expiredate2=explode('-',$expiredate);
	            
	            //echo $expiredate2[1];die();
	            $expiredmonth=$expiredate2[1];
	            $expiredyear=$expiredate2[0];
		$resultsss = $this->lobjnewscreenmodel->fnnewmonthcaleshowlatest($Program,$NewCity,$year);
		//print_r($resultsss);die();
	$values=0;
			for($i=0;$i<count($resultsss);$i++)
			{
				$value=$resultsss[$i]['idnewscheduler'];
				$values=$values.','.$value;
			}
			
			
	$larresultofmonths = $this->lobjnewscreenmodel->fnnewmonths($values);
	$frommonth = $larresultofmonths[0]['minimum'];

	
			$tomonth = $larresultofmonths[0]['maximum'];
    $curmonth = date('m');
			if($frommonth<$curmonth)
			{
				$frommonth = $curmonth;
			}
			
		$larrresults = $this->lobjnewscreenmodel->fnGetmonthsbetweenvalid($frommonth,$tomonth);
			$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresults);
		//$larrCountryStatesDetails[]=array('key'=>'0','name'=>'Entire Calender');
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}




public function applieddatevalidAction()
{
$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintday = $this->_getParam('day');//city
		$applieddate = $this->_getParam('applieddate');
		$lintmonth = $this->_getParam('month');
		$year = $this->_getParam('year');
		$days = $year.'-'.$lintmonth.'-'.$lintday;
		//echo $days;
		//echo "a".$applieddate;die();
		$larrresultdates=$this->lobjnewscreenmodel->newfnGetdatedifference($days,$applieddate);
		//print_r($larrresultdates);
		$larrresultvalidatedays=$this->lobjnewscreenmodel->fngetstudentinformationfromconfig();
		//print_r($larrresultvalidatedays);die();
		
	
	if($larrresultdates['nodays']<$larrresultvalidatedays['ClosingBatch'])
	{
		$daysvalidateflag=$larrresultvalidatedays['ClosingBatch'];
		
	}
	else 
	{
		$daysvalidateflag=0;
	}	
	echo $daysvalidateflag;
	
}


	
}