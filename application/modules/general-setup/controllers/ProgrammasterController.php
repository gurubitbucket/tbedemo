<?php
class GeneralSetup_ProgrammasterController extends Base_Base {
	private $lobjprogrammaster;
	private $lobjcoursemasterForm;
	private $_gobjlogger;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
	
	public function fnsetObj(){
		$this->lobjcoursemaster = new GeneralSetup_Model_DbTable_Programmaster();
		$this->lobjcoursemasterForm = new GeneralSetup_Form_Programmaster (); //intialize user lobjuniversityForm
		
	}
	
	public function indexAction() {
    	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjcoursemaster->fngetCoursemasterDetails (); //get user details
		
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->programmasterpaginatorresult);
			
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->programmasterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->programmasterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjcoursemaster->fnSearchCourse ( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->programmasterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {

			 $this->_redirect( $this->baseUrl . '/general-setup/programmaster/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'programmaster', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newprogramAction() { //title
    	$this->view->title="Add New Program";
		
		$this->view->lobjcoursemasterForm = $this->lobjcoursemasterForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjcoursemasterForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjcoursemasterForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		$larrresult = $this->lobjcoursemaster->fnGetCourseList();
		
		$this->view->lobjcoursemasterForm->PreRequesition->addMultiOptions($larrresult);
		$larrprog = $this->lobjcoursemaster->fnAddGetProgramList();
		$this->view->lobjcoursemasterForm->idprog->addMultiOptions($larrprog);


			$larrgrades = $this->lobjcoursemaster->fnAddGetGradesetList();
			
			$this->view->lobjcoursemasterForm->Idgradeset->addMultiOptions($larrgrades);
			$this->view->lobjcoursemasterForm->idprog->setAttrib('readonly','readonly'); 
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$this->lobjcoursemaster->fnaddCourse($formData);
				// Write to Log
				$auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the Course "."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/general-setup/programmaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'programmaster', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
    
	public function editprogramAction(){
    	$this->view->title="Edit Program";  //title
		$this->view->lobjcoursemasterForm = $this->lobjcoursemasterForm; //send the lobjuniversityForm object to the view
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjcoursemasterForm->UpdDate->setValue ( $ldtsystemDate );		
    	$IdCourse = $this->_getParam('id', 0);
    	$result = $this->lobjcoursemaster->fetchAll('IdProgrammaster ='.$IdCourse);
    	$result = $result->toArray();
		foreach($result as $courseresult){
		}
		$larrresult = $this->lobjcoursemaster->fnGetCourseListEdit($IdCourse);
		
		$this->view->lobjcoursemasterForm->PreRequesition->addMultiOptions($larrresult);
		
		$larrprog = $this->lobjcoursemaster->fnEditGetProgramList();
		$this->view->lobjcoursemasterForm->idprog->addMultiOptions($larrprog);

			$larrgrades = $this->lobjcoursemaster->fnAddGetGradesetList();
			
			$this->view->lobjcoursemasterForm->Idgradeset->addMultiOptions($larrgrades);
		$this->view->lobjcoursemasterForm->idprog->setAttrib('readonly','readonly'); 
    	$this->lobjcoursemasterForm->populate($courseresult);	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjcoursemasterForm->isValid($formData)) {
	   			$lintIdCoursemaster = $formData ['IdProgrammaster'];
				$this->lobjcoursemaster->fnupdateCourse($formData,$lintIdCoursemaster);//update university
				
				$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully edited the Course with id = ".$IdCourse."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
				 $this->_redirect( $this->baseUrl . '/general-setup/programmaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'programmaster', 'action'=>'index'),'default',true));
			}
    	}
    }

    	public function showgradesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idgrade = $this->_getParam('idgrade');
		
		$larrgrades = $this->lobjcoursemaster->fnShowGradesList($idgrade);
	           
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Grade Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Grade Name</b></th><th><b>From Percentage</b></th><th><b>To Percentage</b></th></tr>";
		foreach($larrgrades as $lobjCountry)
		{
			$tabledata.="<tr><td align = 'left'>".$lobjCountry['Gradename']."</td><td align = 'left'>".$lobjCountry['Percentagefrom']."</td><td align = 'left'>".$lobjCountry['Percentageto']."</td></tr>";
		}
		$tabledata.="</table></form><br>";
		$tabledata.="<tr><td colspan= '6'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
		//$tabledata.="<tr><td colspan= '8'><input type='button' id='printpdf' name='printpdf'  value='PrintPdf' onClick=Printfn(".$lintidcenter.",'".$ldtedate."','".$lstrtype."',".$lintidsess.");></td></tr>";
		echo  $tabledata;
	}
}