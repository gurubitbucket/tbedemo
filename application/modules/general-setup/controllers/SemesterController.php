<?php
class GeneralSetup_SemesterController extends Base_Base {
	private $lobjsemester;
	private $lobjsemesterForm;
	private $lobjsemestermaster;
	private $locale;
	private $registry;
	
	public function init() {
		$this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	public function fnsetObj(){
		$this->lobjsemester = new GeneralSetup_Model_DbTable_Semester();
		$this->lobjsemesterForm = new GeneralSetup_Form_Semester (); 
		$this->lobjsemestermaster = new GeneralSetup_Model_DbTable_Semestermaster (); 

		
	}
	
	public function indexAction() {
    	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjsemester->fngetSemesterDetails (); //get user details
		$lintpagecount = "5";// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjsemester->fnSearchSemester( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->semesterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/general-setup/semester/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'semester', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newsemesterAction() { //title
    	$this->view->title="Add New Semester";
		$this->view->lobjsemesterForm = $this->lobjsemesterForm;
		
		if($this->locale == 'ar_YE')  {
			$this->view->lobjsemesterForm->StartDate->setAttrib('datePackage',"dojox.date.islamic");
			$this->view->lobjsemesterForm->EndDate->setAttrib('datePackage',"dojox.date.islamic");
		}
		
		$lobjsemester = $this->lobjsemestermaster->fnGetSemestermasterList();
		$this->lobjsemesterForm->Semester->addMultiOptions($lobjsemester);
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjsemesterForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjsemesterForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$this->lobjsemester->fnaddSemester($formData);
				$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'program', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
    
	public function editsemesterAction(){
    	$this->view->title="Edit Semester";  //title
		$this->view->lobjsemesterForm = $this->lobjsemesterForm;
		 //send the lobjuniversityForm object to the view
		if($this->locale == 'ar_YE')  {
			$this->view->lobjsemesterForm->StartDate->setAttrib('datePackage',"dojox.date.islamic");
			$this->view->lobjsemesterForm->EndDate->setAttrib('datePackage',"dojox.date.islamic");
		}
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$lobjsemester = $this->lobjsemestermaster->fnGetSemestermasterList();
		$this->lobjsemesterForm->Semester->addMultiOptions($lobjsemester);
    	$IdSemester = $this->_getParam('id', 0);
    	$result = $this->lobjsemester->fetchAll('IdSemester  ='.$IdSemester);
    	$result = $result->toArray();
		foreach($result as $semesterresult){
		}
    	$this->lobjsemesterForm->populate($semesterresult);	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjsemesterForm->isValid($formData)) {
	   			$lintIdSemester = $formData ['IdSemester'];
				$this->lobjsemester->fnupdateSemester($formData,$lintIdSemester);//update university
				$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'semester', 'action'=>'index'),'default',true));
			}
    	}
    }
}