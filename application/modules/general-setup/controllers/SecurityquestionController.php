<?php

class GeneralSetup_SecurityquestionController extends Base_Base
{
private $_gobjlogger;
    public function init()
    { //initialization function
       $this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->fnsetObj();
    }
    public function fnsetObj() 
    {
		$this->lobjSecurityquestion = new GeneralSetup_Model_DbTable_Securityquestion(); //Securityquestion model object
		$this->lobjSecurityquestionForm = new GeneralSetup_Form_Securityquestion (); //intialize Securityquestion lobjSecurityquestionForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		
	}
    public function indexAction()
    {
    	$lobjform=$this->view->lobjform = $this->lobjform;
        $lobjform=$this->view->lobjSecurityquestionForm = $this->lobjSecurityquestionForm; //send the lobjSecurityquestionForm object to the view
		$larrresult = $this->lobjSecurityquestion->fngetSecurityquestionDetails (); //get user details
       // print_r($larrresult);die();
       
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->Securityquestionpaginatorresult);
			
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->Securityquestionpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Securityquestionpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjSecurityquestion->fnSecurityquestionSearch($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->promotepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/general-setup/securityquestion/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'promote', 'action'=>'index'),'default',true));
		}
    }
    
	public function addsecurityquestionAction()  
	{
		$this->view->lobjSecurityquestionForm=$this->lobjSecurityquestionForm ;//send the form to view		

	    if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) //if the user clicks save button enter the loop
        {
          $larrformData = $this->_request->getPost();//get the posted data
          //print_r($larrformData);die();
          unset($larrformData['Save']);   // unset the save field
	  	  $this->lobjSecurityquestion->fnaddnewsecurityquestion($larrformData);//Insert program details
	  	  
	  	  // Write to Log
			    $auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the security question"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	  	  // redirect to index page
	  	 $this->_redirect( $this->baseUrl . '/general-setup/securityquestion/index');	
        }		
	
	}
	public function editsecurityquestionAction()
    {
    	//echo "harsha";die();
    	$id= $this->_getparam('id');
    	$larrresult = $this->lobjSecurityquestion->fnupdatefunction($id);
    	//print_r($larrresult);die();
    	$this->view->lobjSecurityquestionForm=$this->lobjSecurityquestionForm ;
    	$this->lobjSecurityquestionForm->populate($larrresult);
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
        {
        	    $larrformData = $this->_request->getPost ();
        	    //print_r($larrformData);die();
        	    unset($larrformData['Save']);
        	    $this->lobjSecurityquestion->fnupdateSecurityquestion($id,$larrformData); 
        	    
        	    // Write to Log
			    $auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the Security question with Id = ".$id."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
        	   	$this->_redirect( $this->baseUrl . '/general-setup/securityquestion/index');	
        }	     
    }

}

