<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class GeneralSetup_CompanyotherpaymentController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjCompanyotherpayment = new GeneralSetup_Model_DbTable_Companyotherpayment();
		$this->lobjCompanyotherpaymentForm = new GeneralSetup_Form_Companypayment ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Companyothers Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjCompanyotherpayment->fngetCompanyDetails(); //get user details
/*		echo('<pre>');
		print_r($larrresult);die();*/
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->companypaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
		   $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjCompanyotherpayment->fnSearchCompanyotherPayment($larrformData['field3']); //searching the values for the user
				/*echo('<pre>');
				print_r($larrresult);
				die();*/
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->companypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/general-setup/companyotherpayment/index');
			}
	
    }
    
    public function companyotherpaymenteditAction()
    {
    	
    	$this->view->lobjCompanyotherpaymentForm = $this->lobjCompanyotherpaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	$this->view->idcompany = $lstrType;
    	/*print_r($lstrType);
    	die();*/
    	$larrstudentname=$this->lobjCompanyotherpayment->fngetCompanyname($lstrType);
    /*	print_r($larrstudentname);
    	die();*/
    	$this->view->companyname = $larrstudentname['CompanyName'];
    	
    	$this->view->companyemail = $larrstudentname['Email'];
        $this->lobjCompanyotherpaymentForm->Amount->setValue($larrstudentname['totalAmount']);
	
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
    			$larrformData = $this->_request->getPost ();    			
    			$larrformData['IDApplication']=$lstrType;
    			$larrformData['companyflag']=1;
    			/*echo('<pre>');
				print_r($larrformData);
				die();*/
    			$regid = $this->lobjCompanyotherpayment->fngeneraterandom();
    			$larrpaymentdetails = $this->lobjCompanyotherpayment->InsertPaymentOption($larrformData,$lstrType,$regid);
    			$result = $this->lobjCompanyotherpayment->sendmails($larrformData['companyname'],$larrformData['companymail'],$regid);			
    			 $this->_redirect( $this->baseUrl . '/general-setup/companyotherpayment/index');
    	}
    }

}

