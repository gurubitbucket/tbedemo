<?php
class GeneralSetup_DepartmentmasterController extends Base_Base { //Controller for the User Module

	
	public function init() { //initialization function
	}
	

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$lobjDepartmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster(); //user model object
		$larrresult = $lobjDepartmentmaster->fngetDepartmentDetails (); //get user details

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionstudent->userpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionstudent->userpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $lobjDepartmentmaster->fnSearchDepartment( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->userpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			$this->_redirect( $this->baseUrl . '/general-setup/departmentmaster/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'departmentmaster', 'action'=>'index'),'default',true));
		}


	}

	public function newdepartmentmasterAction() { //Action for creating the new user
		$lobjDepartmentmasterForm = new GeneralSetup_Form_Departmentmaster(); //intialize user lobjuserForm
		$this->view->lobjdepartmentmasterForm = $lobjDepartmentmasterForm; //send the lobjuserForm object to the view


		$lobjDepartmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster(); //intialize user Model
		
		$ldtsystemDate = date ( 'Y-m-d' );
		$this->view->lobjdepartmentmasterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjdepartmentmasterForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		$lobjCollegeList = $lobjDepartmentmaster->fnGetCollegeList();
		$lobjDepartmentmasterForm->IdCollege->addMultiOptions($lobjCollegeList);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			if ($lobjDepartmentmasterForm->isValid ( $larrformData )) {
				$result = $lobjDepartmentmaster->fnaddDepartment($larrformData); //instance for adding the lobjuserForm values to DB
				$this->_redirect( $this->baseUrl . '/general-setup/departmentmaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'departmentmaster', 'action'=>'index'),'default',true));
			}
		}

	}

	public function departmentmasterlistAction() { //Action for the updation and view of the user details
		$lobjDepartmentmasterForm = new GeneralSetup_Form_Departmentmaster(); //intialize user lobjuserForm
		$this->view->lobjdepartmentmasterForm = $lobjDepartmentmasterForm; //send the lobjuserForm object to the view
		$lobjDepartmentmaster = new GeneralSetup_Model_DbTable_Departmentmaster(); //intialize user Model
		
		$lintidepartment = ( int ) $this->_getParam ( 'id' );
		$this->view->iddepartment = $lintidepartment;
		
		
		$larrresult = $lobjDepartmentmaster->fnviewDepartment($lintidepartment); //getting user details based on userid
		$lobjCollegeList = $lobjDepartmentmaster->fnGetCollegeList();
		$lobjDepartmentmasterForm->IdCollege->addMultiOptions($lobjCollegeList);
		$lstrdepartmenttype= $larrresult['DepartmentType'];
		$this->view->departmenttype = $lstrdepartmenttype;
		
    	$lobjDepartmentmasterForm->populate($larrresult);	

		if ($lstrdepartmenttype == '0' ) {
			//$lstridcollege= $larrresult['IdCollege']; 
			$lobjDepartmentmasterForm->IdCollege->setValue($larrresult['IdCollege']);
			$lobjDepartmentmasterForm->IdBranch->addMultiOptions(array('0' => 'Select'));
			$lobjDepartmentmasterForm->IdBranch->setValue('0');
		} else if($lstrdepartmenttype == '1' ){
			$lstridbranch= $larrresult['IdCollege'];
			$idcoll = $lobjDepartmentmaster->fnGetCollege($lstridbranch);
			$lobjDepartmentmasterForm->IdCollege->setValue($idcoll['Idhead']);
			
			$larrDetails = $lobjDepartmentmaster->fnGetBranchListofCollege($idcoll['Idhead']);
			$lobjDepartmentmasterForm->IdBranch->addMultiOptions($larrDetails);
			$lobjDepartmentmasterForm->IdBranch->setValue($lstridbranch);
			
			
			
		}

		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				unset ( $larrformData ['Save'] );
				if ($lobjDepartmentmasterForm->isValid ( $larrformData )) {
						
					$lintIdDepartment = $larrformData ['IdDepartment'];
					$lobjDepartmentmaster->fnupdateDepartment($lintIdDepartment, $larrformData );
					$this->_redirect( $this->baseUrl . '/general-setup/departmentmaster/index');
					//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'departmentmaster', 'action'=>'index'),'default',true));
				}
			}
		}
		$this->view->lobjdepartmentmasterForm = $lobjDepartmentmasterForm;
	}

	public function getbranchlistAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$lobjCommonModel = new App_Model_Common();
		$lobjDepartmentModel = new GeneralSetup_Model_DbTable_Departmentmaster();
		//Get Country Id
		$lintidCollege = $this->_getParam('idCollege');
		$lintvalue= $this->_getParam('value');
		if($lintvalue == '1') {
			$larrDetails = $lobjCommonModel->fnResetArrayFromValuesToNames($lobjDepartmentModel->fnGetBranchListofCollege($lintidCollege));
			echo Zend_Json_Encoder::encode($larrDetails);
		}else if($lintvalue == '0') {
			echo '[{"name":"Select","key":"0"}]';
		}
	}
	
}