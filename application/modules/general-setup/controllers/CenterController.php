<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class GeneralSetup_CenterController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjcenter;
	private $lobjcenterForm;
	
	public function init() { //initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		
		$this->lobjcenter = new GeneralSetup_Model_DbTable_Center(); //user model object
		$this->lobjcenterForm = new GeneralSetup_Form_Center (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		
	}
	

	public function indexAction() 
	{   
		// action for search and view		
		$lobjform=$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		$larrresult = $this->lobjcenter->fngetCenterDetails (); //get center details

		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->centerpaginatorresult);	
		
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->centerpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->centerpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) 
			{   
				
				unset ( $larrformData ['Search'] );				
				$larrresult = $this->lobjcenter->fnSearchCenter($larrformData); //searching the values for the user				
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->centerpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/center/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'center', 'action'=>'index'),'default',true));
		}
	}

	public function newcenterAction() 
	{   
		//Action for creating the new user
		$this->view->lobjcenterForm = $this->lobjcenterForm; //send the lobjuserForm object to the view		
		$ldtsystemDate = date ( 'Y-m-d' );
		$this->view->lobjcenterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjcenterForm->UpdUser->setValue ($auth->getIdentity()->iduser);
		
		$lobjcountry = $this->lobjcenter->fnGetCountryList();//get the countrynames from the DB
		$this->lobjcenterForm->country->addMultiOptions($lobjcountry);//Add to the country drop down
		
		if($this->locale == 'ar_YE')  
		{
			$this->view->lobjcenterForm->DOB->setAttrib('datePackage',"dojox.date.islamic");
		}			
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //get posted Formdata 
			unset ( $larrformData ['Save'] );
			unset ( $larrformData ['Close'] );
			
			if ($this->lobjcenterForm->isValid ( $larrformData )) 
			{				
				$larrformData['centerpassword'] = md5($larrformData['centerpassword']); 
				$result = $this->lobjcenter->fnaddCenter( $larrformData ); //insert into DB
				$this->_redirect( $this->baseUrl . '/general-setup/center/index');				
			}
		}
	  }

	 //funtcion to edit the center info 
	  public function centerlistAction() 
	  {   
		//Action for the updation and view of the user details
		$this->view->lobjcenterForm = $this->lobjcenterForm; //send the lobjuserForm object to the view

		$lobjcountry = $this->lobjcenter->fnGetCountryList();
		$this->lobjcenterForm->country->addMultiOptions($lobjcountry);
	    $ldtsystemDate = date ( 'Y-m-d' );

		$this->view->lobjcenterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjcenterForm->UpdUser->setValue (1);
		$lintidcenter = ( int ) $this->_getParam ( 'id' );
		//echo $lintidcenter;die();
		$this->view->lobjcenterForm->idcenter->setValue($lintidcenter);
		$larrresult = $this->lobjcenter->fnviewCenter($lintidcenter); //getting user details based on userid
		$larrresultfloor = $this->lobjcenter->fnviewCenterfloor($lintidcenter);
		$larridfloors="";
		for($i=0;$i<count($larrresultfloor);$i++)
		  {
			if($i==0)
			 {
				$larridfloors=$larrresultfloor[$i]['idcentrefloor'];
			 }
			else
			 {
			  $larridfloors=$larridfloors.','.$larrresultfloor[$i]['idcentrefloor'];
			 }
		   }		
		$floorids=$larridfloors;
		$larrresultroom = $this->lobjcenter->fnviewCenterrooms($larridfloors);
		//echo "<pre />";
		//print_r($larrresultfloor);
		//print_r($larrresultroom);
		//print_r($larrresultroom[0]['RoomName']);
		//print_r(count($larrresultroom['idcentrefloor']));
		//for($i=0;$i<$larrresultfloor;$i++)
		$this->view->cnnt =	count($larrresultfloor);		
		$this->view->result=$larrresultfloor;
		$this->view->resultroom=$larrresultroom;
		$lobjCommonModel = new App_Model_Common();
		//Get States List
		$larrpermCountyrStatesList = $lobjCommonModel->fnGetCountryStateList($larrresult['country']);
		$this->lobjcenterForm->state->addMultiOptions($larrpermCountyrStatesList);
		$this->lobjcenterForm->city->addMultiOptions(array('0'=>'others'));
		$this->lobjcenterForm->city->addMultiOptions($this->lobjcenter->fnGetcityList($larrresult['state']));
			
	    $arrworkphone = explode("-",$larrresult['workphone']);
	    $arrcellphone = explode("-",$larrresult['cellphone']);
			unset($larrresult['workphone']);
			unset($larrresult['cellphone']);
			
			
		$this->lobjcenterForm->populate($larrresult);	
			
		$this->view->lobjcenterForm->workcountrycode->setValue ( $arrworkphone [0] );
		$this->view->lobjcenterForm->workstatecode->setValue ( $arrworkphone [1] );
		$this->view->lobjcenterForm->workPhone->setValue ( $arrworkphone [2] );
			
		$this->view->lobjcenterForm->countrycode->setValue ($arrcellphone[0]);
		$this->view->lobjcenterForm->statecode->setValue ($arrcellphone[1]);
		$this->view->lobjcenterForm->cellPhone->setValue ($arrcellphone[2]);
			
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
			$larrformData = $this->_request->getPost ();
			//print_r($larrformData);die();
			unset ( $larrformData ['Save'] );
			unset ( $larrformData ['Close'] );
			//echo "<pre />";
			//print_r($larrformData);die();
						//print_r($floorids);die();
			if(count($larrformData['floorname'])!=$larrformData['Nooffloors'])
				{
					echo '<script language="javascript">alert("Number of floors and actual floors are not matching")</script>';
				}
			else 
				{
					$lintidcenter = $larrformData ['idcenter'];
					$this->lobjcenter->fnupdateCenter($lintidcenter, $larrformData,$floorids);
					$this->lobjcenter->fninsertCenterfloor($lintidcenter, $larrformData);				
				    $this->_redirect( $this->baseUrl . '/general-setup/center/index');			
		        }
		 }
		    $this->view->lobjcenterForm = $this->lobjcenterForm;
	}

	//Action To Get List Of States From Country Id
	public function getcountrystateslistAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Get Country Id
		$lintIdCountry = $this->_getParam('idCountry');
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetCountryStateList($lintIdCountry));
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	//Action To Get List Of States From Country Id
	public function getstafflistAction()
	 {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Get Country Id
		$idstaff =$this->_getParam('idStaff');
		$larrStaffDetails = $this->lobjuser->getstaffdetails($idstaff);
		echo Zend_Json_Encoder::encode($larrStaffDetails);
	 }	
		
	public function getcitylistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		//get all the city that correspond to the selected state
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjcenter->fnGetStateCityList($lintIdCountry));
		//$larrCountryStatesDetails[]=array('key'=>'0',name=>'Others');//if the key is 0 set city as others 
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}				
}