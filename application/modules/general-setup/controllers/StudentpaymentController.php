<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class GeneralSetup_StudentpaymentController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjStudentpayment = new GeneralSetup_Model_DbTable_Studentpayment();
		$this->lobjStudentpaymentForm = new GeneralSetup_Form_Studentpayment ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjStudentpayment->fngetStudentDetails(); //get user details
/*		echo('<pre>');
		print_r($larrresult);die();*/
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/studentpayment/index');
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/general-setup/studentpayment/index');
			}
	
    }
    
    public function studentpaymenteditAction()
    {
    	
    	$this->view->lobjStudentpaymentForm = $this->lobjStudentpaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	$this->view->idstudent = $lstrType;
    	/*print_r($lstrType);
    	die();*/
    	$larrstudentname=$this->lobjStudentpayment->fngetStudentname($lstrType);
    	/*print_r($larrstudentname);
    	die();*/
    	$this->view->studentname = $larrstudentname['studentname'];
    	
    	$this->view->studentemail = $larrstudentname['EmailAddress'];
    	$this->view->studentname = $larrstudentname['FName'];
    	$idbatch = $larrstudentname['IdBatch'];
	
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
    			$larrformData = $this->_request->getPost ();
    		/*	print_r($larrformData);
    			die();*/
    			$larrpaymentdetails = $this->lobjStudentpayment->InsertPaymentOption($larrformData);
    			$regid = $this->lobjStudentpayment->fngeneraterandom();
    			$larrresult = $this->lobjStudentpayment->insertoption($regid,$idbatch,$lstrType);
    			$larrresultmail = $this->lobjStudentpayment->sendmails($larrformData['studentname'],$larrformData['studentemail'],$regid);
    			 $this->_redirect( $this->baseUrl . '/general-setup/studentpayment/index');
    	}
    }

}

