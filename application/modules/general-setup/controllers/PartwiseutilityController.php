<?php
class GeneralSetup_PartwiseutilityController extends Base_Base {
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	private function fnsetObj(){
		$this->lobjPartwiseutilityModel = new GeneralSetup_Model_DbTable_Partwiseutility();
		$this->lobjCommon=new App_Model_Common();
				$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjPartwiseForm = new  GeneralSetup_Form_Partwiseutility();
				$this->lobjStudenteditModel = new GeneralSetup_Model_DbTable_Studentedit();
		$this->lobjStudenteditForm = new GeneralSetup_Form_Studentedit();
		$this->lobjstudentmodel = new App_Model_Studentapplication();
	}
	
	public function indexAction() {
		$this->gsessionregistration->mails=0;
    	$this->view->title="Program Setup";
		$this->view->lobjPartwiseForm = $this->lobjPartwiseForm;
		$this->view->lobjform = $this->lobjform;
	    $larrresult=0;
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->programpaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->programpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->programpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost();
			if ($this->lobjPartwiseForm->isValid ( $larrformData )) {
			    //$this->view->lobjPartwiseForm->Date3->setValue($larrformData['Date3']);
				//$this->view->lobjPartwiseForm->Date4->setValue($larrformData['Date4']);
				$larrresult = $this->lobjPartwiseutilityModel->fnSearchStudent($larrformData); //searching the values
				//echo count($larrresult);
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->programpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/general-setup/partwiseutility/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Approve' )) {
			$larrformData = $this->_request->getPost ();
		/*	print_r($larrformData);
			die();*/
		
			$larresult = $this->lobjPartwiseutilityModel->fnsubmittheresults($larrformData);
			 $this->_redirect( $this->baseUrl . '/general-setup/partwiseutility/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function editstudentAction() { //title
			$this->view->lobjStudenteditForm = $this->lobjStudenteditForm;
		$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjStudenteditForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		$larrstate = $this->lobjstudentmodel->fnGetStateName();
		$this->lobjStudenteditForm->State->addMultiOptions($larrstate);
		
		$larrQualification = $this->lobjstudentmodel->fnGetEducationDetails();
		$this->lobjStudenteditForm->Qualification->addMultiOptions($larrQualification);
		
	   $larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Race');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjStudenteditForm->Race->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}		
		
			$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
	
		$lintidstudent = $this->_getParam('id');
		$this->view->id = $lintidstudent;
		$larrresults = $this->lobjStudenteditModel->fneditdetails($lintidstudent);
		
		$this->view->lobjStudenteditForm->FName->setAttrib('readonly','true'); 	
		$this->view->lobjStudenteditForm->DateOfBirth->setAttrib('readonly','true'); 	
		$this->view->lobjStudenteditForm->PermCity->setAttrib('readonly','true');
		$this->view->lobjStudenteditForm->EmailAddress->setAttrib('readonly','true'); 	
		$this->view->lobjStudenteditForm->Takafuloperator->setAttrib('readonly','true'); 												
		$this->view->lobjStudenteditForm->ICNO->setAttrib('readonly','true'); 	
		$this->view->lobjStudenteditForm->PermAddressDetails->setAttrib('readonly','true'); 	
		$this->view->lobjStudenteditForm->CorrAddress->setAttrib('readonly','true'); 	
		$this->view->lobjStudenteditForm->State->setAttrib('readonly','true'); 	
		$this->view->lobjStudenteditForm->Race->setAttrib('readonly','true'); 			
		$this->view->lobjStudenteditForm->ContactNo->setAttrib('readonly','true'); 			
		$this->view->lobjStudenteditForm->login->setAttrib('readonly','true'); 	
		$this->view->lobjStudenteditForm->password->setAttrib('readonly','true'); 	
		$this->view->lobjStudenteditForm->Qualification->setAttrib('readonly','true'); 
		$this->view->lobjStudenteditForm->MobileNo->setAttrib('readonly','true');
		$this->view->lobjStudenteditForm->PostalCode->setAttrib('readonly','true'); 	 		
		
 	$this->view->lobjStudenteditForm->FName->setValue($larrresults['FName']);
	$this->view->lobjStudenteditForm->MName->setValue($larrresults['MName']);
	$this->view->lobjStudenteditForm->LName->setValue($larrresults['LName']);
	$this->view->lobjStudenteditForm->DateOfBirth->setValue($larrresults['DateOfBirth']);
	$this->view->lobjStudenteditForm->PermCity->setValue($larrresults['PermCity']);
	$this->view->lobjStudenteditForm->EmailAddress->setValue($larrresults['EmailAddress']);		 	
	$this->view->lobjStudenteditForm->Takafuloperator->setValue($larrresults['Takafuloperator']);
	$this->view->lobjStudenteditForm->ICNO->setValue($larrresults['ICNO']);
	$this->view->lobjStudenteditForm->PermAddressDetails->setValue($larrresults['PermAddressDetails']);	
	$this->view->lobjStudenteditForm->Gender->setValue($larrresults['Gender']);	
	$this->view->lobjStudenteditForm->CorrAddress->setValue($larrresults['CorrAddress']);	
	$this->view->lobjStudenteditForm->ArmyNo->setValue($larrresults['ArmyNo']);	
	$this->view->lobjStudenteditForm->State->setValue($larrresults['State']);
	$this->view->lobjStudenteditForm->PostalCode->setValue($larrresults['PostalCode']);
	$this->view->lobjStudenteditForm->Race->setValue($larrresults['Race']);
    $this->view->lobjStudenteditForm->ContactNo->setValue($larrresults['ContactNo']);
    $this->view->lobjStudenteditForm->MobileNo->setValue($larrresults['MobileNo']);
    $this->view->lobjStudenteditForm->login->setValue($larrresults['EmailAddress']);
    $this->view->lobjStudenteditForm->password->setValue($larrresults['ICNO']);
    $this->view->programs = $larrresults['ProgramName'];
 	$this->view->StateNames = $larrresults['StateName'];
 	$this->view->centernames = $larrresults['centername'];
 	$this->view->idapplication = $larrresults['IDApplication'];
 	$this->view->managesession = $larrresults['managesessionname'].'('.$larrresults['starttime'].'--'.$larrresults['endtime'].')';  
    }
    
	
}