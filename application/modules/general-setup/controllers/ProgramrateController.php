<?php
class GeneralSetup_ProgramrateController extends Base_Base {
	private $lobjprogrammaster;
	private $lobjcoursemasterForm;	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}	
	public function fnsetObj(){
		$this->lobjcoursemaster = new GeneralSetup_Model_DbTable_Programrate();
		$this->lobjcoursemasterForm = new GeneralSetup_Form_Programrate (); //intialize user lobjuniversityForm		
	}	
	public function indexAction() {
    	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjcoursemaster->fngetCoursemasterDetails (); //get user details
		
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->programratepaginatorresult);
			
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionsis->programratepaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->programratepaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjcoursemaster->fnSearchCourse ( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->programratepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/general-setup/programrate/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'programrate', 'action'=>'index'),'default',true));
		}		
	}	    
	public function editprogramAction(){
    	$this->view->title="Edit Program";  //title
		$this->view->lobjcoursemasterForm = $this->lobjcoursemasterForm; //send the lobjuniversityForm object to the view
		$ldtsystemDate = date ('Y-m-d H:i:s');
		$this->view->lobjcoursemasterForm->UpdDate->setValue($ldtsystemDate);		
    	$IdCourse = $this->_getParam('id', 0);
    	$auth = Zend_Auth::getInstance();
		$this->view->lobjcoursemasterForm->UpdUser->setValue( $auth->getIdentity()->iduser);
    	$this->view->editId = 0;
    	$this->view->IdCourse = $IdCourse;
    	$result = $this->lobjcoursemaster->fngetProgramrateMaster('b.IdProgrammaster ='.$IdCourse);
    	$result = $result->toArray();    	
    	$this->view->lobjcoursemasterForm->ProgramName->setValue($result[0]['ProgramName']);
    	$this->view->lobjcoursemasterForm->idProgram->setValue($IdCourse);
    	$AccountArray = $this->lobjcoursemaster->fnAccountArray();
		$this->view->lobjcoursemasterForm->IdAccountmaster->addMultioptions($AccountArray);
		$larrresult = $this->lobjcoursemaster->fngetProgramrateDetails('b.IdProgrammaster ='.$IdCourse);
		$this->view->result = $larrresult;		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$this->view->paginator = $larrresult;
		if($this->_getParam('edit')){
			$editId  = $this->_getParam('edit');
			$this->view->editId = $editId;
			$editArray = $this->lobjcoursemaster->fngetProgramrateEditDetails('a.idProgramrate ='.$editId);
			$this->view->lobjcoursemasterForm->IdAccountmaster->setValue($editArray['IdAccountmaster']);
			$this->view->lobjcoursemasterForm->Rate->setValue($editArray['Rate']);
			$this->view->lobjcoursemasterForm->EffectiveDate->setValue($editArray['EffectiveDate']);
			$this->view->lobjcoursemasterForm->idProgramrate->setValue($editArray['idProgramrate']);		
		}
		if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();      		  		
	    	if ($this->lobjcoursemasterForm->isValid($formData)) {    		
	    		unset($formData['ProgramName']);
	    		unset($formData['Save']);
	    		if($formData['idProgramrate']){
	    			$this->lobjcoursemaster->fnupdateProgramrate($formData,$formData['idProgramrate']);
	    		}
	    		else{
	    			$this->lobjcoursemaster->fnaddProgramrate($formData);
	    		}
	    		 $this->_redirect( $this->baseUrl . "/general-setup/programrate/editprogram/id/".$IdCourse);
	   			//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'programrate', 'action'=>'editprogram','id'=>$IdCourse),'default',true));
			}
    	}
    }
}