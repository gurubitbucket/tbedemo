<?php
class GeneralSetup_UniversityController extends Base_Base 
{
	private $lobjuniversity;
	private $lobjuniversityForm;
	private $lobjUser;
	private $lobjStaffForm;
	private $lobjdeftype;
	private $lobjcommonmodel;
	private $lobjstaffmodel;
	private $lobjreglistmodel;
	private $registry;
	private $locale;
	
	public function init() 
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->fnsetObj();
	}
	
	public function fnsetObj()
	{
		$this->lobjform = new App_Form_Search ();
		$this->lobjPaginator = new App_Model_Definitiontype();
		$this->lobjuniversity = new GeneralSetup_Model_DbTable_University();
		$this->lobjuniversityForm = new GeneralSetup_Form_University (); //intialize user lobjuniversityForm
		$this->lobjUser = new GeneralSetup_Model_DbTable_User(); //intialize user Model
		$this->lobjStaffForm = new GeneralSetup_Form_Staffmaster();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjcommonmodel = new App_Model_Common();
		$this->lobjstaffmodel = new GeneralSetup_Model_DbTable_Staffmaster();
		$this->lobjreglistmodel = new GeneralSetup_Model_DbTable_RegistrarList();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		
	}
	
	public function indexAction() {
    	 //intialize search lobjuniversityForm
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		 //user model object
		$larrresult = $this->lobjuniversity->fngetUniversityDetails (); //get user details
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->userpaginatorresult);
		$lintpagecount = "5";
		 // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionsis->universitypaginatorresult)) {
			$this->view->paginator = $this->lobjPaginator->fnPagination($this->gobjsessionsis->universitypaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjuniversity->fnSearchUniversity ( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->universitypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/university/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'university', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newuniversityAction() { //title
    	$this->view->title="Add New University";
		
		$this->view->lobjuniversityForm = $this->lobjuniversityForm; //send the lobjuniversityForm object to the view
		$this->view->lobjstaffmasterForm = $this->lobjStaffForm;
		$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Levels');
		
		foreach($larrdefmsresultset as $larrdefmsresult)
		{
			$this->lobjStaffForm->IdLevel->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
		
		if($this->locale == 'ar_YE') 
		{
			$this->lobjuniversityForm->FromDate->setAttrib('datePackage',"dojox.date.islamic");
			$this->lobjuniversityForm->ToDate->setAttrib('datePackage',"dojox.date.islamic");
		}
		
		$this->lobjStaffForm->StaffType->setValue(0);
		$this->lobjStaffForm->StaffType->setAttrib('readonly','readonly');
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		$this->lobjuniversityForm->Country->addMultiOptions($lobjcountry);
		$larrcountrylist = $this->lobjcommonmodel->fnResetArrayFromValuesToNames($lobjcountry);
		$this->view->jsondata = 
				 '{
    				"label":"name",
					"identifier":"key",
					"items":'.Zend_Json_Encoder::encode($larrcountrylist).
				  '}';
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjuniversityForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjuniversityForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			//if ($this->lobjuniversityForm->isValid($formData)) {//process form 
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				
				$this->lobjuniversity->fnaddUniversity($formData);
				
				$this->_redirect( $this->baseUrl . '/general-setup/university/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'university', 'action'=>'index'),'default',true));	//redirect	
			//}  	
        }     
    }
    
	public function edituniversityAction(){
    	$this->view->title="Edit University";  //title
    	
		$this->view->lobjuniversityForm = $this->lobjuniversityForm; //send the lobjuniversityForm object to the view
		
		if($this->locale == 'ar_YE') 
		{
			$this->lobjuniversityForm->FromDate->setAttrib('datePackage',"dojox.date.islamic");
			$this->lobjuniversityForm->ToDate->setAttrib('datePackage',"dojox.date.islamic");
		}
		
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjuniversityForm->UpdDate->setValue ( $ldtsystemDate );		
		$lobjcountry = $this->lobjUser->fnGetCountryList();
		$this->lobjuniversityForm->Country->addMultiOptions($lobjcountry);
		$lobjstate = $this->lobjUser->fnGetStateList();
		$this->lobjuniversityForm->State->addMultiOptions($lobjstate);
    	$IdUniversity = $this->_getParam('id', 0);
    	$this->lobjuniversityForm->IdStaff->addMultiOptions($this->lobjstaffmodel->fngetStaffMasterListforDD());
    	
    	$result = $this->lobjuniversity->fneditUniversity($IdUniversity);
		foreach($result as $unvresult){
		}
    	$this->lobjuniversityForm->populate($unvresult);	
    	
    	//$this->lobjuniversityForm->Univ_Name->removeValidator ('Db_NoRecordExists' );
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjuniversityForm->isValid($formData)) {
	   			$lintIdUniversity = $formData ['IdUniversity'];
				
				$this->lobjuniversity->fnupdateUniversity($formData,$lintIdUniversity);//update university
				
				$this->lobjreglistmodel->fnupdateRegistrarList($formData,$lintIdUniversity);//update registrar
				
				$this->lobjreglistmodel->fninsertRegistrarList($formData,$lintIdUniversity);//insert new registrar
				$this->_redirect( $this->baseUrl . '/general-setup/university/index');
			//	$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'university', 'action'=>'index'),'default',true));
			}
    	}
    }
}