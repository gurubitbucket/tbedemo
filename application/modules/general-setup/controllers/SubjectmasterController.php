<?php
class GeneralSetup_SubjectmasterController extends Base_Base { //Controller for the User Module	
	public function init() { //initialization function
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
	}

	public function indexAction() { // action for search and view
		$lobjform = new App_Form_Search (); //intialize search lobjuserForm
		$this->view->lobjform = $lobjform; //send the lobjuserForm object to the view
		$lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster(); //user model object
		$larrresult = $lobjSubjectmaster->fngetSubjectDetails (); //get user details
		
		$lobjUniversityMasterList = $lobjSubjectmaster->fnGetUniversityMasterList();
		$lobjform->field1->addMultiOptions($lobjUniversityMasterList);
		
		$lobjCollegeList = $lobjSubjectmaster->fnGetCollegeList();
		$lobjform->field5->addMultiOptions($lobjCollegeList);
		
		$lobjBranchList = $lobjSubjectmaster->fnGetBranchList();
		$lobjform->field8->addMultiOptions($lobjBranchList);
		
		$lobjDepartmentList = $lobjSubjectmaster->fnGetDepartmentList();
		$lobjform->field20->addMultiOptions($lobjDepartmentList);

		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance

		if(isset($this->gobjsessionstudent->userpaginatorresult)) {
			$this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionstudent->userpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $lobjSubjectmaster->fnSearchSubject( $lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->userpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
		}


	}

	public function newsubjectmasterAction() { //Action for creating the new user
		$lobjSubjectmasterForm = new GeneralSetup_Form_Subjectmaster(); //intialize user lobjuserForm
		$this->view->lobjsubjectmasterForm = $lobjSubjectmasterForm; //send the lobjuserForm object to the view


		$lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster(); //intialize user Model
		
		$ldtsystemDate = date ( 'Y-m-d' );
		$this->view->lobjsubjectmasterForm->UpdDate->setValue ( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjsubjectmasterForm->UpdUser->setValue ( $auth->getIdentity()->iduser);

		$lobjDepartmentList = $lobjSubjectmaster->fnGetDepartmentList();
		$lobjSubjectmasterForm->IdDepartment->addMultiOptions($lobjDepartmentList);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			//print_r($larrformData);die();
			$larrreqhrs = explode('T',$larrformData['RequiredHours']);
			$larrminhrs = explode('T',$larrformData['MinHours']);
			$larrformData['RequiredHours'] = $larrreqhrs[1];
			$larrformData['MinHours'] = $larrminhrs[1];
			//MinHours
			unset ( $larrformData ['Save'] );
			if ($lobjSubjectmasterForm->isValid ( $larrformData )) {
				$result = $lobjSubjectmaster->fnaddSubject($larrformData); //instance for adding the lobjuserForm values to DB
				$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
			}
		}

	}

	public function subjectmasterlistAction() { //Action for the updation and view of the user details
		$lobjSubjectmasterForm = new GeneralSetup_Form_Subjectmaster(); //intialize user lobjuserForm
		$this->view->lobjsubjectmasterForm = $lobjSubjectmasterForm; //send the lobjuserForm object to the view
		$lobjSubjectmaster = new GeneralSetup_Model_DbTable_Subjectmaster(); //intialize user Model
		
		$lintisubject = ( int ) $this->_getParam ( 'id' );
		$this->view->idsubject = $lintisubject;
		
		
		$larrresult = $lobjSubjectmaster->fnviewSubject($lintisubject); //getting user details based on userid
		$lobjDepartmentList = $lobjSubjectmaster->fnGetDepartmentList();
		$lobjSubjectmasterForm->IdDepartment->addMultiOptions($lobjDepartmentList);

		$larrresult['RequiredHours'] = 'T'.$larrresult['RequiredHours'];
		$larrresult['MinHours'] = 'T'.$larrresult['MinHours'];
    	$lobjSubjectmasterForm->populate($larrresult);	


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->_request->isPost ()) {
				$larrformData = $this->_request->getPost ();
				$larrreqhrs = explode('T',$larrformData['RequiredHours']);
				$larrminhrs = explode('T',$larrformData['MinHours']);
				$larrformData['RequiredHours'] = $larrreqhrs[1];
				$larrformData['MinHours'] = $larrminhrs[1];
				unset ( $larrformData ['Save'] );
				if ($lobjSubjectmasterForm->isValid ( $larrformData )) {
						
					$lintisubject = $larrformData ['IdSubject'];
					$lobjSubjectmaster->fnupdateSubject($lintisubject, $larrformData );
					$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'subjectmaster', 'action'=>'index'),'default',true));
				}
			}
		}
		$this->view->lobjdepartmentmasterForm = $lobjSubjectmasterForm;
	}

	
}