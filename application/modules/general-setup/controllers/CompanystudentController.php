<?php
class GeneralSetup_CompanystudentController extends Base_Base {
	private $lobjprogram;
	private $lobjcompanyStudentForm;
	private $lobjcompanyStudentModel;
	private $lobjdeftype;
	
	public function init() 
	{
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function fnsetObj()
	{
		$this->lobjcompanyStudentModel = new GeneralSetup_Model_DbTable_Companystudent();
		$this->lobjcompanyStudentForm = new GeneralSetup_Form_Companystudent();
		$this->lobjstudentmodel = new App_Model_Studentapplication();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjStudenteditModel = new GeneralSetup_Model_DbTable_Studentedit();
		$this->lobjStudenteditForm = new GeneralSetup_Form_Studentedit();
		$this->lobjstudentmodel = new App_Model_Studentapplication();
		$this->lobjdeftype = new App_Model_Definitiontype();
		$this->lobjCommon=new App_Model_Common();
	}
	
	public function indexAction() {
    	//$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjcompanyStudentModel->fngetcompanynames(); //get user details
		$this->lobjform->field5->addMultiOption('','Select'); 	
		$this->lobjform->field5->addmultioptions($larrresult);

		$larrdetails=$this->lobjcompanyStudentModel->fngetcompanystudent();
		
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->programpaginatorresult);
		$lintpagecount = 10;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->programpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->programpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrdetails,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
			$larrformData = $this->_request->getPost ();
			unset($larrformData['Search']);
			if ($this->lobjform->isValid ( $larrformData )) 
			{
				$larrresult = $this->lobjcompanyStudentModel->fnSearchcompanystudent($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->programpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/general-setup/companystudent/index');
		}
	}
	
	
	public function editstudentAction() 
	{    //title
    
		$this->view->lobjStudenteditForm = $this->lobjStudenteditForm;
		$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjStudenteditForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		$larrstate = $this->lobjstudentmodel->fnGetStateName();
		$this->lobjStudenteditForm->State->addMultiOptions($larrstate);
		
		$larrQualification = $this->lobjstudentmodel->fnGetEducationDetails();
		$this->lobjStudenteditForm->Qualification->addMultiOptions($larrQualification);
		
	   $larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Race');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjStudenteditForm->Race->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}		
		
			$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		$this->view->lobjStudenteditForm->UpdDate->setValue($ldtsystemDate);
		$this->view->lobjStudenteditForm->UpdUser->setValue(1);
			
		$lintidstudent = $this->_getParam('id');
		$this->view->id = $lintidstudent;
		$larrresults = $this->lobjStudenteditModel->fneditdetails($lintidstudent);
 	$this->view->lobjStudenteditForm->FName->setValue($larrresults['FName']);
	$this->view->lobjStudenteditForm->MName->setValue($larrresults['MName']);
	$this->view->lobjStudenteditForm->LName->setValue($larrresults['LName']);
	$this->view->lobjStudenteditForm->DateOfBirth->setValue($larrresults['DateOfBirth']);
	$this->view->lobjStudenteditForm->PermCity->setValue($larrresults['PermCity']);
	$this->view->lobjStudenteditForm->EmailAddress->setValue($larrresults['EmailAddress']);		 	
	$this->view->lobjStudenteditForm->Takafuloperator->setValue($larrresults['Takafuloperator']);
	$this->view->lobjStudenteditForm->ICNO->setValue($larrresults['ICNO']);
	$this->view->lobjStudenteditForm->PermAddressDetails->setValue($larrresults['PermAddressDetails']);	
	$this->view->lobjStudenteditForm->Gender->setValue($larrresults['Gender']);	
	$this->view->lobjStudenteditForm->CorrAddress->setValue($larrresults['CorrAddress']);	
	$this->view->lobjStudenteditForm->ArmyNo->setValue($larrresults['ArmyNo']);	
	$this->view->lobjStudenteditForm->State->setValue($larrresults['State']);
	$this->view->lobjStudenteditForm->PostalCode->setValue($larrresults['PostalCode']);
	$this->view->lobjStudenteditForm->Race->setValue($larrresults['Race']);
    $this->view->lobjStudenteditForm->ContactNo->setValue($larrresults['ContactNo']);
    $this->view->lobjStudenteditForm->MobileNo->setValue($larrresults['MobileNo']);
    $this->view->lobjStudenteditForm->login->setValue($larrresults['EmailAddress']);
    $this->view->lobjStudenteditForm->password->setValue($larrresults['ICNO']);
    
     	$this->view->programs = $larrresults['ProgramName'];
 	$this->view->StateNames = $larrresults['StateName'];
 	$this->view->centernames = $larrresults['centername'];
 	$this->view->idapplication = $larrresults['IDApplication'];
 	$this->view->managesession = $larrresults['managesessionname'].'('.$larrresults['starttime'].'--'.$larrresults['endtime'].')';
    
    
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			$idapplication = $larrformData['studid'];
		/*	print_r($larrformData);
			die();
*/				$larrresultupdate = $this->lobjStudenteditModel->fnAddStudent($larrformData,$idapplication);

			  $larrresult = $this->lobjStudenteditModel->mailstudent($idapplication);
			  
			
			    $larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
									
										
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							//$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							//$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							//$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'],$lstrEmailTemplateBody);
										
										
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
										$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);
								$this->view->mess .= $lstrEmailTemplateBody;
				
				 $this->_redirect($this->baseUrl . '/general-setup/studentedit/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'program', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
	
}