<?php
/**
 * InitialConfigController
 * 
 * @author
 * @version 
 */
class GeneralSetup_AccountmasterController extends Base_Base
{
    private $locale;
	private $registry;
	private $lobjaccountmasterModel;
	private $lobjaccountmasterentryform;
	
	   	public function init() { //initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
public function fnsetObj() {
		
		$this->lobjaccountmasterModel = new GeneralSetup_Model_DbTable_Accountmaster(); //user model object
		$this->lobjaccountmasterentryform = new GeneralSetup_Form_Accountmasterentry(); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		
	}
	
  	
 public function indexAction() { // action for search and view
		
		$lobjform=$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		$larrresult = $this->lobjaccountmasterModel->fngetaccountdeatils (); //get user details

          if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->accountmasterpaginatorresult);	
		
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance

 if(isset($this->gobjsessionsis->accountmasterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->accountmasterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjaccountmasterModel->fngetaccountdeatilsSearch($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->accountmasterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/general-setup/accountmaster/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'accountmaster', 'action'=>'index'),'default',true));
		}


	}
	
	public function accountmasterAction() { //Action for creating the new user
		$this->view->lobjaccountmasterentryform = $this->lobjaccountmasterentryform; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
      
		$this->view->lobjaccountmasterentryform->UpdDate->setValue($ldtsystemDate);
		 
		$auth = Zend_Auth::getInstance();
		$this->view->lobjaccountmasterentryform->UpdUser->setValue($auth->getIdentity()->iduser);
 
		
	//echo "abc";die();
	
	if($this->_getparam('lvaredit')){ 
			$lvaredit	 = 	$this->_getparam('lvaredit');
			$this->view->lvaredit	=	$this->_getparam('lvaredit');
			$this->view->lobjaccountmasterentryform->PrefixCode->setAttrib('readonly','readonly');
			//$this->view->lobjaccountmasterentryform->UpdUser->setValue (1);
	    	//Fetching Contact Details For Edit
			$lvardataEdit=	$this->lobjaccountmasterModel-> fngetaccountEdit($lvaredit);	
		$this->view->lobjaccountmasterentryform->Description->setValue($lvardataEdit['Description']);
			//$this->view->lobjaccountmasterform->populate($lvardataEdit);		
			$this->view->lobjaccountmasterentryform->AccountName->setValue($lvardataEdit['AccountName']);		
			$this->view->lobjaccountmasterentryform->AccShortName->setValue($lvardataEdit['AccShortName']);	
			$this->view->lobjaccountmasterentryform->PrefixCode->setValue($lvardataEdit['PrefixCode']);		
			$this->view->lobjaccountmasterentryform->Description->setValue($lvardataEdit['Description']);	
			$this->view->lobjaccountmasterentryform->duringRegistration->setValue($lvardataEdit['duringRegistration']);
			//$this->view->lobjaccountmasterentryform->BillingModule->setAttrib('disabled','disabled');	
			$this->view->lobjaccountmasterentryform->Active->setValue($lvardataEdit['Active']);	
			$this->view->lobjaccountmasterentryform->programtype->setValue($lvardataEdit['programtype']);
			$this->view->lobjaccountmasterentryform->coursetype->setValue($lvardataEdit['coursetype']);
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			unset ( $larrformData ['Close'] );
			if ($this->lobjaccountmasterentryform->isValid ( $larrformData )) {
				
				$result = $this->lobjaccountmasterModel->fnupdateaccountmaster($this->_getparam('lvaredit'),$larrformData); //instance for adding the lobjuserForm values to DB
			    $this->_redirect( $this->baseUrl . '/general-setup/accountmaster/index');
				//	$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'accountmaster', 'action'=>'index'),'default',true));
			}
		}			
		}
		else {
			if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			unset ( $larrformData ['Close'] );
			if ($this->lobjaccountmasterentryform->isValid ( $larrformData )) {
				
				$result = $this->lobjaccountmasterModel->fnInsert($larrformData); //instance for adding the lobjuserForm values to DB
				$this->_redirect( $this->baseUrl . '/general-setup/accountmaster/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'accountmaster', 'action'=>'index'),'default',true));
			}
		}	
		}
		

	}

   
}
