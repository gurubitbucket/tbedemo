<?php
class GeneralSetup_StudenttypeController extends Base_Base 
{
	private $lobjStudenttype; //db variable
	private $lobjStudenttypeform;//Form variable
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjStudenttype = new GeneralSetup_Model_DbTable_Studenttype(); //intialize user db object
		$this->lobjStudenttypeform = new GeneralSetup_Form_Studenttype(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction() 
	{    	 
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view
		/*$larrresult = $this->$lobjStudenttype->fngetBusinesstypelist(); //get businesstype details		
		
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->Businesstypepaginatorresult); // clear the search session
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->Businesstypepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Businesstypepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		{
			    $larrformData = $this->_request->getPost ();			    									   
				unset ( $larrformData ['Search'] );
				$larrresult = $this->$lobjStudenttype->fnSearchbusinesstype($larrformData); //searching the values for the businesstype					
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Businesstypepaginatorresult = $larrresult;
			
		}
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/general-setup/businesstype/index');			
		}
		*/
	}
	
	//function to add a businesstype
	public function addAction() 
	{   
		$this->view->lobjStudenttypeform = $this->lobjStudenttypeform; //send the form to the view
		//$this->view->lobjStudenttypeform->type->setValue(0);			
		// check if the user has clicked the save
		if ($this->_request->isPost() && $this->_request->getPost('Save'))
		{
			$larrformData = $this->_request->getPost();	
								
			unset ( $larrformData ['Save'] );
			$larrformData ['type']=0;
			$this->lobjStudenttype->fnaddtermsandconditions($larrformData);
			$this->_redirect( $this->baseUrl . '/general-setup/studenttype/index'); //after adding Redirect to index page				
        }     
    }
    
    //function to update the businesstype
	public function editAction()
	{   	
		$this->view->lobjStudenttypeform = $this->lobjStudenttypeform; //send the Form the view
		$id=$this->_Getparam("id"); // get the id of the studenttype to be updated		
		$larrresult = $this->lobjStudenttype->fnedittermsandconditions($id);	// get the details from the DB into array variable	
		unset ($larrresult ['type']);
		print_r($larrresult);
		die();
		$this->view->lobjStudenttypeform->populate($larrresult); // populate the result into the form
			
	    // check if the user has clicked the save
    	/*if ($this->_request->isPost() && $this->_request->getPost('Save'))
    	 {       	 			 	
    		$larrformData = $this->_request->getPost();	 // get the form data  		    		    	
	   		$lintId = $id;	//store the id to be updated    		
	   		unset ($larrformData ['Save']);	 //unset the save data in form data  		
			$this->$lobjStudenttype->fnupdatebusinesstype($lintId,$larrformData); //update businesstype
			$this->_redirect( $this->baseUrl . '/general-setup/businesstype/index'); //redirect to index page after updating							
    	}*/
	  }
}