<?php
class GeneralSetup_SchedulerController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;
	private $cccnt;
	
	public function init() 
	{ 
		//initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();		
	}
	
	public function fnsetObj() 
	{
		$this->lobjuser = new GeneralSetup_Model_DbTable_User(); //user model object
		$this->lobjschedulermodel = new GeneralSetup_Model_DbTable_Scheduler(); //scheduler model object
		$this->lobjschedulerForm = new GeneralSetup_Form_Scheduler (); //schedulerForm object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	public function indexAction()
	 { 
	 	// action for search and view
	 
		
		$lobjform=$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		$larrresult = $this->lobjschedulermodel->fngetSchedulerDetails(); //get user details
		
		$sessionID = Zend_Session::getId();
		$larrresults = $this->lobjschedulermodel->fnDeleteTempDetails($sessionID);
		
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->schedulerpaginatorresult);
			
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
  
		if(isset($this->gobjsessionsis->schedulerpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->schedulerpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ('Search'))
	    {
			$larrformData = $this->_request->getPost();
			if ($lobjform->isValid ($larrformData)) 
			{
				$larrresult = $this->lobjschedulermodel->fnSearchScheduler($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->schedulerpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
				 $this->_redirect( $this->baseUrl . '/general-setup/scheduler/index');
			
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'scheduler', 'action'=>'index'),'default',true));
		}
 
	}

	public function newschedulerAction() { //Action for creating the new user
		//$larrresults = $this->lobjschedulermodel->fnDeleteTempDetails($sessionID);
		
		$sessionID = Zend_Session::getId();
		//$larrresults = $this->lobjschedulermodel->fnDeleteTempDetails($sessionID);
		
		$this->view->lobjschedulerForm = $this->lobjschedulerForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date('Y-m-d:H-i-s');

		$larrbatch = $this->lobjschedulermodel->fnGetBatchName();
		$this->lobjschedulerForm->idBatch->addMultiOptions($larrbatch);
		
		$this->view->lobjschedulerForm->UpdDate->setValue($ldtsystemDate);
		$auth = Zend_Auth::getInstance();
		$this->view->lobjschedulerForm->UpdUser->setValue($auth->getIdentity()->iduser);


		$larrvenueresult = $this->lobjschedulermodel->fnfetchvenue();
		
		$this->view->centerarray = $larrvenueresult;
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
		//print_r($larrformData);die();
			$sessionID = Zend_Session::getId();
			$larrresult = $this->lobjschedulermodel->fninsertscheduler($larrformData);
			
			
			$larrresults = $this->lobjschedulermodel->fnDeleteTempDetails($sessionID);
	
			 $this->_redirect( $this->baseUrl . '/general-setup/scheduler/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'scheduler', 'action'=>'index'),'default',true));			
		}

	}

	
	//Action To Get List Of States From Country Id
	public function fngetdatesAction(){

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$IdBatch = $this->_getParam('idbatch');	
		
  		$larrSections  = $this->lobjschedulermodel->fnGetBatchDetailsAjax($IdBatch);
  		echo date('d-m-Y', strtotime($larrSections['BatchFrom']))."#@#".date('d-m-Y', strtotime($larrSections['BatchTo']))."#@#".(($larrSections['BatchFrom']))."#@#".(($larrSections['BatchTo']));
  		exit;
	}
	
	//Action To Get List Of States From Country Id
	public function getstafflistAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$idstaff =$this->_getParam('idStaff');

		$larrStaffDetails = $this->lobjuser->getstaffdetails($idstaff);
		echo Zend_Json_Encoder::encode($larrStaffDetails);	
	}
	
	/*
	 * function 
	 */
       public function tempvenuetimeAction(){
      		$edittrue=0;
			$this->_helper->layout->disableLayout();				
			$sessionID = Zend_Session::getId();
			$date = $this->_getParam('datea');	
			$Iddate = $this->_getParam('iddate');	
		
			$linteditid = $this->_getParam('editid');
			$this->view->idtempvenuetimings = 0;
			$this->view->lobjschedulerForm = $this->lobjschedulerForm;
			$sessionID = Zend_Session::getId();
			//$larrresultstemp = $this->lobjschedulermodel->fnDeleteTempDetails($sessionID);
			$this->view->datetime = $date;
			$this->view->iddate = $Iddate;
			
	        if($Iddate == 0)
			{
				$larrresultinedit = $this->lobjschedulermodel->fnfetchalltempdetailsinedit($sessionID,$date);
				$this->view->larrtempresult = $larrresultinedit;
			
			}
			else 
			{
				$larrresult = $this->lobjschedulermodel->fnfetchalltempdetails($sessionID,$Iddate);
				$this->view->larrtempresult = $larrresult;
			}
			if($linteditid)
			{
				$edittrue=1;
				$larrresult = $this->lobjschedulermodel->fnfetchdetails($linteditid);
				
				$this->view->fromtime= "Sat Nov 12 2011".' '.$larrresult['Fromtime'];
				$this->view->totime= "Sat Nov 12 2011".' '.$larrresult['Totime'];
				$this->view->idtempvenuetimings = $linteditid;
				//Sat Nov 12 2011 11:46:26
				
			}
			$this->view->edittrue = $edittrue;
	
		
			if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			 	$larrformdata = $this->_request->getPost();
			 	//print_r($larrformdata);
			 //	die();
			 	$fromtimearray = $larrformdata['prog_val'];
			 	$pieces = explode("T", $fromtimearray);
			 	$fromtime = $pieces[1];
			 	
			 
			 	//print_r($totime);die();
			 	$sessionID = Zend_Session::getId();
			 	$iddates = $larrformdata['iddates'];
			 	$date = $larrformdata['date'];
				$idtempvenuetimings = $larrformdata['idtempvenuetimings'];
			
			 	if($larrformdata['idtempvenuetimings'] ==0)
			 	{
			 			$totimearray = $larrformdata['prog_val1'];
			 	//echo "<pre/>";
			 	$piecesss = explode("T", $totimearray);
			 	$totime = $piecesss[1];
		        	$larrresult = $this->lobjschedulermodel->fninsertintotemp($fromtime,$totime,$sessionID,$iddates,$date);
			 	}
			 	else 
			 	{
			 		$totimearray = $larrformdata['prog_va1l'];
			 		$piecesss = explode("T", $totimearray);
			 		$totime = $piecesss[1];

			 		$larrresult = $this->lobjschedulermodel->fnupdateintotemp($fromtime,$totime,$sessionID,$iddates,$date,$idtempvenuetimings);
			 	} 	
			 	//$this->_redirect( $this->baseUrl . '/general-setup/scheduler/tempvenuetime/$date/iddate/$iddates');
			 
			 echo "<script>window.location =  '".$this->view->baseUrl()."/general-setup/scheduler/tempvenuetime/datea/$date/iddate/$iddates';</script>";		
			}
		//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'scheduler', 'action'=>'tempvenuetime/datea/2011-11-18/iddate/1'),'default',true));
  //echo "<script>parent.location = 'http://www.tbe.com/general-setup/scheduler/tempvenuetime/datea/2011-11-18/iddate/1';</script>";
	}
	
	
	/*
	 * function ot insert into temp table
	 */

 public function fninserttimeAction(){
 		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$from= $this->_getParam('from');	
		$to = $this->_getParam('to');
		$iddates = $this->_getParam('iddates');
		$sessionID = Zend_Session::getId();
		
		$larrresult = $this->lobjschedulermodel->fninsertintotemp($from,$to,$sessionID,$iddates);

		//http://www.tbe.com/general-setup/scheduler/tempvenuetime/datea/2011-11-18/iddate/1
	}
	
	
	public function schedulerlistAction() { //Action for creating the new user
		//$this->_helper->layout->disableLayout();	
		//echo "asdfsdfsdafsadf";
		
				$sessionID = Zend_Session::getId();
			//$larrresultstemp = $this->lobjschedulermodel->fnDeleteTempDetails($sessionID);
		$this->view->lobjschedulerForm = $this->lobjschedulerForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date('Y-m-d:H-i-s');

		$lintidscheduler= $this->_getParam('id');
		$larrschedulername = $this->lobjschedulermodel->fngetScheduleredit($lintidscheduler);
	
			
			$larrvenue = $this->lobjschedulermodel->fnGetBatchName();
			$this->lobjschedulerForm->idBatch->addMultiOptions($larrvenue);
			$this->lobjschedulerForm->idBatch->setValue($larrschedulername['idBatch']);
			$this->view->idBatch = $larrschedulername['idBatch'];
			$this->lobjschedulerForm->ScheduleName->setValue($larrschedulername['ScheduleName']);
			
		$larrschedulerdetails = $this->lobjschedulermodel->fngetschedulervenue($lintidscheduler);
		$arrIdCenter = array();
		foreach ($larrschedulerdetails as $larrcenter)
		{
			$arrIdCenter[] = $larrcenter['idcenter'];
		}
		
		//print_r($larrschedulerdetails);
		//die();
		
		
		$venuedate = $larrschedulerdetails[0]['idschedulervenue'];
		//print_r($venuedate);
		//die();
		$larrschedulervenuetime = $this->lobjschedulermodel->fngetschedulervenuetime($venuedate);

		$larrinsertintotemp = $this->lobjschedulermodel->fngetdetailsforvenue($venuedate);
		//echo "<pre/>";
		//print_r($larrinsertintotemp);
		//die();
		$sessionID = Zend_Session::getId();
		
		
	    $larrvenueresult = $this->lobjschedulermodel->fnfetchvenue();
		
		//echo "<pre/>";
		   
		$this->lobjschedulermodel->fninsertintotempduringedit($larrinsertintotemp,$sessionID);
	
		$this->view->larrcenter = $arrIdCenter;
		$this->view->centerarray = $larrvenueresult;
		$this->view->venuetime = $larrschedulervenuetime;
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post


			$sessionID = Zend_Session::getId();
			$larrresult = $this->lobjschedulermodel->fninsertscheduler($larrformData);
			
			
			//$larrresults = $this->lobjschedulermodel->fnDeleteTempDetails($sessionID);
		}

	}
}