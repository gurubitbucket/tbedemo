<?php
class GeneralSetup_AttendencereportController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;	
	
	public function init() { //initialization function
		$this->gobjsessionstudent = Zend_Registry::get('sis');
		/*$this->gobjsessionsis = Zend_Registry::get('sis');
		if(empty($this->gobjsessionsis->iduser)){ 
			$this->_redirect( $this->baseUrl . '/index/logout');					
		}	*/	
			
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() 
	{
		$this->lobjAttendencereportModel = new GeneralSetup_Model_DbTable_Attendencereport(); //user model object
		$this->lobjAttendencereportForm = new GeneralSetup_Form_Attendencereport (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

	public function indexAction() 
	{ // action for search and view
		$this->view->lobjAttendencereportForm = $this->lobjAttendencereportForm;
		$larresult = $this->lobjAttendencereportModel->fnGetbatch();
		$this->lobjAttendencereportForm->Batch->addMultiOptions($larresult);
	}

	public function newuserAction() 
	{ //Action for creating the new user
		//$this->_helper->layout->disableLayout();
		
		$this->view->InitBatch = $this->view->Batch;
		$result =  $this->_request->getPost();		
		
		/*print_R($this->_request->getPost('Print'));
		die();*/
		$this->view->Batch = $result['Batch'];
		$this->view->Chart = $result['Chart'];
		
		$larrbatchdetails = $this->lobjAttendencereportModel->fnGetBatchDetails($result['Batch']);
		/*print_R($larrbatchdetails);
		die();*/
		$this->view->batchname = $larrbatchdetails[0]['BatchName'];
		$this->view->BatchFrom = $larrbatchdetails[0]['BatchFrom'];
		$this->view->BatchTo = $larrbatchdetails[0]['BatchTo'];
		$this->view->ProgramName = $larrbatchdetails[0]['ProgramName'];
		
		$larrtotalnoofstudents = $this->lobjAttendencereportModel->fngetnoofstudents($result['Batch']);
		$totalnoofstudents = $larrtotalnoofstudents['Batch'];
		/*print_r($totalnoofstudents);
		die();*/
		$this->view->totalnoofstudents = $totalnoofstudents;
		
		$string = $totalnoofstudents;
		
		$larrtotalattended = $this->lobjAttendencereportModel->fnTotalAttended($result['Batch']);
		$totalattended = count($larrtotalattended);
		
		/*print_r($totalattended);
		die();*/
		$this->view->totalattended = $totalattended;
		
		$string = $string.','.$totalattended;
		
		
		$absent = $totalnoofstudents-$totalattended;
		$this->view->absent = $absent;
		
	}

	public function printAction()
	{
		$this->_helper->layout()->setLayout('/reg/usty1');
			$batchprint = $this->_getParam('batchprint');	
			$batchChart = $this->_getParam('batchChart');
			$this->view->Batch = $batchprint;
		$this->view->Chart = $batchChart;
		
		
		$larrbatchdetails = $this->lobjAttendencereportModel->fnGetBatchDetails($batchprint);
/*		print_R($larrbatchdetails);
		die();*/
		$this->view->batchname = $larrbatchdetails[0]['BatchName'];
		$this->view->BatchFrom = $larrbatchdetails[0]['BatchFrom'];
		$this->view->BatchTo = $larrbatchdetails[0]['BatchTo'];
		$this->view->ProgramName = $larrbatchdetails[0]['ProgramName'];
		
		$larrtotalnoofstudents = $this->lobjAttendencereportModel->fngetnoofstudents($batchprint);
		$totalnoofstudents = $larrtotalnoofstudents['Batch'];
		$this->view->totalnoofstudents = $totalnoofstudents;
		
		$string = $totalnoofstudents;
		
		$larrtotalattended = $this->lobjAttendencereportModel->fnTotalAttended($batchprint);
		$totalattended = count($larrtotalattended);
		$this->view->totalattended = $totalattended;
		
		$string = $string.','.$totalattended;
		
		
		$absent = $totalnoofstudents-$totalattended;
		$this->view->absent = $absent;	
		
		
		//////////////////percentage///////////////////
		
		
		$attendpercentage = (($totalattended)*100)/$totalnoofstudents;
		$this->view->attendpercentage =$attendpercentage;
		$this->view->absentpercentage = 100-($attendpercentage);
	}

}