<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class GeneralSetup_CompanypaymentController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjCompanypayment = new GeneralSetup_Model_DbTable_Companypayment();
		$this->lobjCompanypaymentForm = new GeneralSetup_Form_Companypayment ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Company Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjCompanypayment->fngetCompanyDetails(); //get user details
/*		echo('<pre>');
		print_r($larrresult);die();*/
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->companypaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->companypaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
		 $larrformData = $this->_request->getPost ();		    
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjCompanypayment->fnSearchCompanyPayment($larrformData['field3']); //searching the values for the user
				/*echo('<pre>');
				print_r($larrresult);
				die();*/
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->companypaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			
			 $this->_redirect( $this->baseUrl . '/general-setup/companypayment/index');
			}
	
    }
    
    public function companypaymenteditAction()
    {
    	
    	$this->view->lobjCompanypaymentForm = $this->lobjCompanypaymentForm; 
    	$lstrType = $this->_getParam('lvaredit');
    	$this->view->idcompany = $lstrType;
    	/*print_r($lstrType);
    	die();*/
    	$larrstudentname=$this->lobjCompanypayment->fngetCompanyname($lstrType);
        /*echo ('<pre>');
    	print_r($larrstudentname);
    	die();*/
    	$this->view->companyname = $larrstudentname['CompanyName'];
    	
    	$this->view->companyemail = $larrstudentname['Email'];
    	$this->view->prog = $larrstudentname['ProgramName'];
        $this->lobjCompanypaymentForm->Amount->setValue($larrstudentname['totalAmount']);
	
    	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
    			$larrformData = $this->_request->getPost ();    			
    			$larrformData['IDApplication']=$lstrType;
    			$larrformData['companyflag']=1;    		 
    			 
    			$regid = $this->lobjCompanypayment->fngeneraterandom();
    			$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption($larrformData,$lstrType,$regid);
    			$result = $this->lobjCompanypayment->sendmails($larrformData['companyname'],$larrformData['companymail'],$regid);			
    			 $this->_redirect( $this->baseUrl . '/general-setup/companypayment/index');
    	}
    }

}

