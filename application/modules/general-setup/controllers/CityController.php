<?php
class GeneralSetup_CityController extends Base_Base{
	private $lobjstate;
	private $lobjcityForm;
	private $lobjCountry;
	private $_gobjlogger;
	
	public function init() 
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    $this->fnsetObj();
   	    
	}
	public function fnsetObj()
	{
		$this->lobjform = new App_Form_Search (); //searchform
		$this->lobjstate = new GeneralSetup_Model_DbTable_State();
		$this->lobjcityForm = new GeneralSetup_Form_City(); 
		$this->lobjCountry = new GeneralSetup_Model_DbTable_City();
	}
	    
	public function indexAction() 
	{
		$lobjform=$this->view->lobjform = $this->lobjform;
		$larrresult = $this->lobjstate->fnGetStateListDetails();
		
		
		 if(!$this->_getParam('search'))
   	    	unset($this->gobjsessionsis->citypaginatorresult);
   	    	
		$lintpagecount = $this->gintPageCount;
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->citypaginatorresult)) 
		{
		  $this->view->paginator = $lobjPaginator->fnPagination($this->gobjsessionsis->citypaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
		  $this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost() && $this->_request->getPost('Search')) 
		{
			$larrformData = $this->_request->getPost();
/*			print_r($larrformData);
			die();	*/						
				if ($this->lobjform->isValid($larrformData))
				 {	
				 	
				 	unset ( $larrformData ['Search'] );	
				 /*	print_r($larrformData);
				 	die();*/
				 				
					//checking the data and calling the search fuction
					$larrresult = $this->lobjCountry->fnSearchState($larrformData['field3']);
					
										
				    $this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				    $this->gobjsessionsis->citypaginatorresult = $larrresult;
				}
			
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{			
			$this->_redirect( $this->baseUrl . '/general-setup/city/index');
		}
	}
        	
  	public function citylistAction() 
  	{
  		$this->view->lobjcityForm = $this->lobjcityForm;
  		$idCountry = $this->_getParam("id");
  		
        $lobjName =$this->lobjCountry->fnStateName($idCountry); 
        	
        $ldtsystemDate = date ( 'Y-m-d H:i:s' );
        $this->view->lobjcityForm->UpdDate->setValue($ldtsystemDate);
        $auth = Zend_Auth::getInstance();
		$this->view->lobjcityForm->UpdUser->setValue ( $auth->getIdentity()->iduser);
		$this->view->lobjcityForm->idState->setValue($lobjName['StateName']);
        $this->view->lobjcityForm->idState->setAttrib('readonly',true);
        
        $larrresult = $this->lobjCountry->fnGetcitydetailslist($idCountry);
        /*echo('<pre>');
        print_r($larrresult);
        die();*/
       
		$lobjPaginator = new App_Model_Common(); // Definitiontype model
		$lintpagecount = $this->gintPageCount;;
		$lintpage = $this->_getParam('page',1);				
		$this->view->paginator = $lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
		$this->gobjsessionstudent->statepaginatorresult = $larrresult;
		
        
		if($this->_getParam('edit'))
		{   
			//$var=$this->_getParam('idCity');
			
			$arrresult = $this->lobjCountry->fncityinfodtl($idCountry,$this->_getParam('idCity'));
			
		    $idCountry = $arrresult['idState'];
		   
            $lobjName =$this->lobjCountry->fnStateName($idCountry);             
			$this->lobjcityForm->populate($arrresult);
			$this->view->lobjcityForm->idState->setValue($lobjName['StateName']);			
		}
         
        //on click of save button 
  		if ($this->_request->isPost() && $this->_request->getPost('Save'))
  		 {
		   $larrformData = $this->_request->getPost();	
					   						
			if($this->_getParam('edit'))
			{   
				if ($this->lobjcityForm->isValid($larrformData)) 
				{   					
					unset($larrformData['Save']);					
					$idCity = $this->_getParam('idCity');					
					$larrformData['idState']=$idCountry;				
					$UpdateData =array('idState'=>$idCountry,
									   'CityName'=>$this->_getParam('CityName'),
									   'UpdDate'=>$this->_getparam('UpdDate'),
	                                   'UpdUser'=>$this->_getparam('UpdUser'),
									   'Active'=>$this->_getparam('Active'));
					
					$larrresult = $this->lobjCountry->fnUpdateCity($UpdateData,$idCity);

					$auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the City with Id = ".$idCity."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
              	    $this->_redirect( $this->baseUrl.'/general-setup/city/citylist/id/'.$idCountry);
				   }
				
			}else {
			//checks for the form validation
			if ($this->lobjcityForm->isValid($larrformData)) {
				
				unset($larrformData['Save']);
				
				$larrformData['idState']=$idCountry;
			
				$insertData =array('idState'=>$idCountry,
								   'CityName'=>$this->_getParam('CityName'),
								   'UpdDate'=>$this->_getparam('UpdDate'),
                                   'UpdUser'=>$this->_getparam('UpdUser'),
								   'Active'=>$this->_getparam('Active'));
				//print_r($insertData);die();

				$larrresult = $this->lobjCountry->fnAddCity($insertData);
				
				$auth = Zend_Auth::getInstance();
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New City"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
               $this->_redirect( $this->baseUrl.'/general-setup/city/citylist/id/'.$idCountry);
			
			   }
	       }   
		  
		}

	}
  	
}