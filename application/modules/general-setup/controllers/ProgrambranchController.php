<?php
class GeneralSetup_ProgrambranchController extends Base_Base {
	private $lobjprogrambranch;
	private $lobjprogrambranchForm;
	private $lobjprogram;
	private $lobjcollegemaster;
	
	public function init() {
		$this->fnsetObj();
	}
	
	public function fnsetObj(){
		$this->lobjcollegemaster = new GeneralSetup_Model_DbTable_Collegemaster();
		$this->lobjprogram = new GeneralSetup_Model_DbTable_Program();
		$this->lobjprogrambranchForm = new GeneralSetup_Form_Programbranch ();		
		$this->lobjprogrambranch = new GeneralSetup_Model_DbTable_Programbranch();
	}
	
	public function indexAction() {
    	$this->view->title="Program Branch Setup";
		$this->view->lobjform = $this->lobjform; 
		$larrresult = $this->lobjprogrambranch->fngetProgrambranchDetails ();
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->programbranchpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->programbranchpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->$lobjprogrambranch->fnSearchProgrambranch ( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjPaginator->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->programbranchpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
				 $this->_redirect( $this->baseUrl . '/general-setup/programbranch/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'programbranch', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newprogrambranchAction() { //title
    	$this->view->title="Add New Program Branch";
    	$programlist = $this->lobjprogram->fnGetProgramList();
    	$collegemasterlist = $this->lobjcollegemaster->fnGetCollegeList();	
		$this->lobjprogrambranchForm->IdProgram->addMultiOptions($programlist);
		$this->lobjprogrambranchForm->IdCollege->addMultiOptions($collegemasterlist);
		$this->view->lobjprogrambranchForm = $this->lobjprogrambranchForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjprogrambranchForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjprogrambranchForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$this->lobjprogrambranch->fnaddProgrambranch($formData);
				
				 $this->_redirect( $this->baseUrl . '/general-setup/programbranch/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'programbranch', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
    
	public function editprogrambranchAction(){
    	$this->view->title="Edit Program Branch";  //title
		$this->view->lobjprogrambranchForm = $this->lobjprogrambranchForm;
		$programlist = $this->lobjprogram->fnGetProgramList();
    	$collegemasterlist = $this->lobjcollegemaster->fnGetCollegeList();	
		$this->lobjprogrambranchForm->IdProgram->addMultiOptions($programlist);
		$this->lobjprogrambranchForm->IdCollege->addMultiOptions($collegemasterlist);
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjprogrambranchForm->UpdDate->setValue ( $ldtsystemDate );		
    	$IdProgramBranchLink = $this->_getParam('id', 0);
    	$result = $this->lobjprogrambranch->fnEditProgrambranchDetails($IdProgramBranchLink);
		foreach($result as $courseresult){
			$this->view->ArabicName = $courseresult['ArabicName'];
		}
    	$this->lobjprogrambranchForm->populate($courseresult);	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjcoursemasterForm->isValid($formData)) {
	   			$lintIdCoursemaster = $formData ['IdCoursemaster'];
				$this->lobjcoursemaster->fnupdateCourse($formData,$lintIdCoursemaster);
				 $this->_redirect( $this->baseUrl . '/general-setup/programbranch/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'programbranch', 'action'=>'index'),'default',true));
			}
    	}
    }
}