<?php
class Examination_Model_Questionnoutility extends Zend_Db_Table {
	


	
    public function fnGetQuestionDetails() {
    	  $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
					   ->from(array('a' => 'tbl_questions'),array('a.*'))
					   ->group("a.QuestionNumber");
		  $result = $db->fetchAll($select);
		return $result;    	  
    }
  
    public function fnupdatequestionids($questionids,$idquestions)
    {
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['QuestionId'] = $questionids;	
		 $where = "idquestions = '".$idquestions."'"; 	
		 $db->update('tbl_questions',$larrformData1,$where);
    }
    
    public function fngetquestionsgroup($questionnumber)
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
					   ->from(array('a' => 'tbl_questions'),array('a.*'))
					   ->where("a.QuestionNumber='$questionnumber'");
		  $result = $db->fetchAll($select);
		return $result; 
    }
    
    public function fnupdateanswerids($idquestions,$questionids)
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
					   ->from(array('a' => 'tbl_answers'),array('a.*'))
					   ->where("a.idquestion='$idquestions'");
		  $result = $db->fetchAll($select);
		  
		  for($i=0;$i<count($result);$i++)
		  {
					$answerids = $questionids.'-'.($i+1);
			    	$larrformData1['Answersid'] = $answerids;	
					$where = "idanswers = '".$result[$i]['idanswers']."'"; 	
					$db->update('tbl_answers',$larrformData1,$where);
		  }
		return $result;
    }
    
    public function fngetAllquestions()
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
		  $select = $db->select()	
					   ->from(array('a' => 'tbl_questions'),array('a.*'));
		  $result = $db->fetchAll($select);
		return $result; 
    }
    
    public function fngetAnswers($idquestion)
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
    	 $select = $db->select()	
					   ->from(array('a' => 'tbl_answers'),array('a.*'))
					   ->where("a.idquestion='$idquestion'");
		  $result = $db->fetchAll($select);
		  return $result; 
    }
    
}
