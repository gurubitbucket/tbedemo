<?php
	class Examination_Model_Questionsattended extends Zend_Db_Table {


     

     public function fngetprogdetails($prog,$date)
     {
     	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     	
		  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_programmaster"),array("a.*"))
							 			 ->join(array("b" => "tbl_batchmaster"),'a.IdProgrammaster=b.IdProgrammaster',array("b.*"))
							 			 ->join(array("t" => "tbl_tosmaster"),'b.IdBatch=t.IdBatch',array("t.*"))
							 			 ->join(array("c" => "tbl_batchdetail"),'b.IdBatch=c.IdBatch',array("c.*"))	
							 			 ->join(array("d" => "tbl_questions"),'c.IdPart=d.QuestionGroup',array("d.*"))							 			 							 			 
                                         ->where("t.Active=1")
                                         ->where("d.Active=1")
                                         ->where("a.IdProgrammaster=?",$prog)
                                         ->where("d.Active=1")
                                         ->group("d.idquestions");
                                         
      if($date!=0)
     	 {
     	 	$lstrSelect->where("d.QuestionLevel = ?",$date);
     	 }
         // if($post['field1']){	$select->where("b.idgroupname = ?",$post['field1']);}	                               
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     public function fngetprog()
     {
     			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("b" => "tbl_programmaster"),array("key"=>"b.IdProgrammaster","value"=>"b.ProgramName"));
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
	 public function fnGetQtnsapperedinexam($Questionid,$maxids){

 		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 
 		 $select = "
 		 SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '$Questionid,%'
Union 
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid' 
Union
SELECT (a.IDApplication) as cnt FROM `tbl_questionsetforstudents` AS a WHERE a.idquestionsetforstudents in($maxids) AND a.`idquestions` LIKE '%,$Questionid,%'
 		 
 		 ";

		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		

		
		
		public function fnmaxids($prog,$examdate)
		{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		    $select = "SELECT MAX(idquestionsetforstudents) as Appeared,Idapplication
					 FROM  `tbl_questionsetforstudents` where idapplication in (		  
 		  			 SELECT b.idapplication 
						FROM tbl_studentapplication AS b
						WHERE b.Program =$prog
						AND b.DateTime ='$examdate'
					AND b.payment=1 and b.pass in(1,2)) group by idapplication";
 		 return	$result = $lobjDbAdpt->fetchAll($select);	
		}
		
			
		public function fngetregids($studentid)
		{
             $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_registereddetails"),array("a.Regid"))
                                         ->where("a.Idapplication in ($studentid)");
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);	
 		 	
		}
		
		public function fnGettotalattended($Questionid,$regid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo"))
                                         ->where("a.Regid in ($regid)")
                                          ->where("a.QuestionNo=?",$Questionid);
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		
		public function fnGettotalCorrectattended($Questionid,$regid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_answerdetails"),array("a.QuestionNo"))
							 			 ->join(array("b"=>"tbl_answers"),'b.idanswers=a.Answer',array())
                                         ->where("a.Regid in ($regid)")
                                          ->where("a.QuestionNo=?",$Questionid)
                                          ->where("b.CorrectAnswer=1");
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}
		public function fngetquestionanswerdetails($qtnid)
		{
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			  $lstrSelect = $lobjDbAdpt->select()
							 			 ->from(array("a" => "tbl_questions"),array("a.Question"))
							 			  ->join(array("b"=>"tbl_answers"),'b.idquestion=a.idquestions',array("b.answers","b.CorrectAnswer"))
                                          ->where("a.idquestions=?",$qtnid);
 		 	return $result = $lobjDbAdpt->fetchAll($lstrSelect);
		}

   
	}