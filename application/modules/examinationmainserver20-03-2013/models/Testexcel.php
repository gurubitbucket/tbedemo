<?php
class Examination_Model_Testexcel extends Zend_Db_Table{
		
		public function fngetalldetails()
		{
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		                 ->from(array("a"=>"tbl_tempapplication"),array("a.*"));
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			//echo "<pre>";
			//print_r($larrResult);die();
			return $larrResult;
		}
		public function fninsertintotemp($informaticnum,$studentnamefield,$Gender,$email,
					$race,$education,$dateofbirth,$Address,$CorrAddress,$Postal,$Country,$State,$Contactno,$Mobile,$mailingaddrs)
		{
		   //echo $Gendererror;die();
		 
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		     $newpostdata = array('ICNO'=>$informaticnum,
		    	               'Studentname'=>$studentnamefield,
							   'Email'=>$email,
							   'Race'=>$race,
							   'Education'=>$education,
							   'DateofBirth'=>$dateofbirth,
		    	               'Gender'=>$Gender,
							   'Mailing Address'=>$Address,
							   'Correspondance Address'=>$CorrAddress,
							   'Postal Code'=>$Postal,
		    	               'Country'=>$Country,
							   'State'=>$State,
							   'Contact No'=>$Contactno,
							   'Mobile No'=>$Mobile,
							   'Mailing Address'=>$mailingaddrs,
							   'UpdDate'=>date('Y-m-d H:i:s')
							   
							   
							   );
		   $larrResult = $lobjDbAdpt->insert('tbl_tempapplication',$newpostdata); 
		    return $larrResult;
		}
		public function getdatedifference($dateofbirth,$currentdate)
		{
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
		   $select = "SELECT DATEDIFF($dateofbirth,$currentdate) as days"; 
		   $result = $lobjDbAdpt->fetchAll($select);
		   return $result;
		}
		public function fncheckicnum($icnum)
		{
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.*"))
					   ->where("a.ICNO=?",$icnum);
		   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		   return $larrResult;
		}
		public function fncheckicnumfromtemp($icnum)
		{
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_tempapplication"),array("a.*"))
					   ->where("a.ICNO=?",$icnum);
		   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		   return $larrResult;
		}
		public function fninsertduplicateicnum($icnum,$student)
		{
		  $error1 ="Duplicate ICNO";
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  
		  $newpostdata = array('Icno'=>$icnum,
		    	               'Studentname'=>$student,
							   'UpdDate'=>date('Y-m-d H:i:s'),
							   'Error'=>$error1
							   
							   );
		 $larrResult = $lobjDbAdpt->insert('tbl_tempapplication',$newpostdata); 
		  return $larrResult;
		   
		}
		public function fnDeleteTempDetails()
 	   {
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$db->delete('tbl_tempapplication');
		}
 		
		public function fninserticnumnotformat($icnum,$student)
		{
		 //echo $icnum;
		 // echo $student;
		   $error2 ="Informat ICNO";
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   
		  $newpostdata = array('Icno'=>$icnum,
		    	               'Studentname'=>$student,
							   'UpdDate'=>date('Y-m-d H:i:s'),
							   'Error'=>$error2
							   );
		 // print_r($newpostdata);die();
		  $larrResult= $lobjDbAdpt->insert('tbl_tempapplication',$newpostdata); 
		  return $larrResult;
		}
		public function fninsertincorrectgender($icnum,$student)
		{
		   $error3 ="Incorrect Gender";
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   
		   $newpostdata = array('Icno'=>$icnum,
		    	               'Studentname'=>$student,
							   'UpdDate'=>date('Y-m-d H:i:s'),
							   'Error'=>$error3
							   );
		  $larrResult= $lobjDbAdpt->insert('tbl_tempapplication',$newpostdata); 
		  return $larrResult;
		}
		
		
}
