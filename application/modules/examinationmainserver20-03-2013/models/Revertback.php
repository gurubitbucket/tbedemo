<?php
	class Examination_Model_Revertback extends Zend_Db_Table {
	
	public function fngetallvenues($iddate)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $select = $lobjDbAdpt->select()				  
						->from(array('a' => 'tbl_venuedateschedule'),array())
						->join(array('b' =>'tbl_center'),'a.idvenue = b.idcenter',array('key'=>'b.idcenter','value'=>'b.centername'))
                        ->where("a.date = '$iddate'")
                           ->where("a.Active = 1")
                        ->group("b.idcenter")
                        ->order("b.centername");                            
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
	}
	
	public function fngetallsessions($selecteddate,$venue)
	{
		
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $select = $lobjDbAdpt->select()				  
						->from(array('a' => 'tbl_venuedateschedule'),array())
						->join(array('b' =>'tbl_managesession'),'a.idsession = b.idmangesession',array('key'=>'b.idmangesession','value'=>'b.managesessionname'))
                        ->where("a.date = '$selecteddate'")
                        ->where("a.idvenue = '$venue'")
                         ->where("a.Active = 1")
                        ->group("b.idmangesession");                            
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
	}
		
	public function fngetstudents($larrformdata)
	{
		$date = $larrformdata['Date'];
		$venue=$larrformdata['Venue'];
		$session=$larrformdata['session'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $select = $lobjDbAdpt->select()				  
						->from(array('a' => 'tbl_studentapplication'),array('a.*'))
						->join(array('b' => 'tbl_programmaster'),'a.Program = b.IdProgrammaster',array('b.*'))
						->join(array('c' => 'tbl_center'),'c.idcenter = a.Examvenue',array('c.*'))
						->join(array('d' => 'tbl_managesession'),'d.idmangesession = a.Examsession',array('d.*'))
						->where("a.DateTime = '$date'")
						->where("a.VenueTime =1")
                        ->where("a.Examvenue = '$venue'")
                        ->where("a.Examsession='$session'");                            
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
	
	}
	
	
	public function fnupdatetherevertstatus($larrformData)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		for($i=0;$i<count($larrformData['IDApplication']);$i++)
		{
			
		  $db = Zend_Db_Table::getDefaultAdapter();
    	  $larrformData1['VenueTime'] = 0;	
		  $where = "IDApplication ='".$larrformData['IDApplication'][$i]."'"; 	
		  $lobjDbAdpt->update('tbl_studentapplication',$larrformData1,$where);
		}
	}
}
