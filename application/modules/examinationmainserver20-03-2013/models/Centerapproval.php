<?php
class Examination_Model_Centerapproval extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';
				
	public function fnGetExamCenterList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array('tbl_center'),array("key"=>"idcenter","value"=>"centername"))								  
                       			  ->where('Active = 1')
                       			  ->order("centername");
								  //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
	}
	
	public function fnGetStudentDetails()
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();   //DATE_FORMAT(i.starttime,'%T')
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											  ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
											  ->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
											  ->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
											  ->join(array("r" =>"tbl_newscheduler"),'r.idnewscheduler=b.Year',array("r.Year as Years"))
											  ->join(array("i" =>"tbl_managesession"),'i.idmangesession=b.Examsession',array("i.managesessionname","CONCAT(i.starttime,'-',IFNULL(i.endtime,' ')) as ExamSessionTime")) 
											  ->where("a.Cetreapproval=0")
											   ->where("i.active=1")
											->where("b.VenueTime in (0,2)")
											 ->where("b.Datetime>=curdate()")	
											 ->group("a.IDApplication");
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
public function fngetStudentSearch($lobjgetarr){
		
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$select = 	$db->select()          
             	 ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
				->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
				->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
				->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
				->join(array("r" =>"tbl_newscheduler"),'r.idnewscheduler=b.Year',array("r.Year as Years"))
				 ->join(array("i" =>"tbl_managesession"),'i.idmangesession=b.Examsession',array("i.managesessionname","CONCAT(i.starttime,'-',IFNULL(i.endtime,' ')) as ExamSessionTime"))
				-> where('b.FName like  ? "%"',$lobjgetarr['field3'])
            	->where('b.ICNO like  ? "%"',$lobjgetarr['field4'])      
				 ->where("b.VenueTime in (0,2)")
				 ->where("b.Datetime>=curdate()")      	
				->where("a.Cetreapproval=0")
				->where("i.active=1");

			if(isset($lobjgetarr['field8']) && !empty($lobjgetarr['field8']) ){
				$select = $select->where("e.idcenter = ?",$lobjgetarr['field8']);
				 			
				}	
				
			$select->group("a.IDApplication");
				
		//echo $select;die();		
		$result = $db->fetchAll($select);	
		return $result;		
	}
	
public function fnGetStudent($lstrType)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											 ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
											  ->join(array("f" =>"tbl_programmaster"),'b.Program=f.IdProgrammaster')
											  ->join(array("c" =>"tbl_state"),'b.ExamState=c.idState',array("c.*"))
											  ->join(array("d" =>"tbl_city"),'b.ExamCity=d.idCity',array("d.*"))
											  ->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
											  ->join(array("i" =>"tbl_managesession"),'i.idmangesession=b.Examsession',array("i.managesessionname","CONCAT(i.starttime,'-',IFNULL(i.endtime,' ')) as ExamSessionTime"))
											 	//->where("a.Approved=1")
											    ->where("a.Cetreapproval=0")
											    ->where("i.active=1")
											  //->where("a.Registrationpin=0000000")
											  ->where("b.IDApplication = ?",$lstrType);
					$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
					return $larrResult;
	}
	
	
}
