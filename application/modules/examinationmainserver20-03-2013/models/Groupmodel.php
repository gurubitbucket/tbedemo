<?php
     class Examination_Model_Groupmodel extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	
	        protected $_name = 'tbl_groupnames';
	
            //function to add the template
            public function fnaddgroup($post)
	        {	 
	       // print_r($post) ;  die();    
	           $this->insert($post);	
	        }
	        
	        //Function to get the webpagetemplate details 	
            public function fngetgrouplist()
            {             
   	         $result=$this->fetchAll();
   	         return $result;
            }
 	
            //Function for searching the author details	
           public function fnSearchgroup($post = array())
	       { 
	       	 $field7 = "Active = ".$post["field7"];	       	  	
	         $select = $this->select()
			                ->from(array('a' => 'tbl_groupnames'),array('a.*'))
			                ->where('a.groupname  like "%" ? "%"',$post['field2'])
			                ->where($field7);  
		     $result = $this->fetchAll($select);
		     return $result->toArray();
	       }
	       
	        public function fneditgroup($id)     
			{		      		
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect =$lobjDbAdpt->select()
									->from(array('a'=>'tbl_groupnames'),array('a.*'))
									->where('idgroupname='.$id );
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
			} 
	       
			public function fnupdategroup($id,$udata) 
            {             
	         $where = "idgroupname = '$id'";
	         $this->update($udata,$where);   
            }

       public function getactivesets(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_tosmaster'),array('a.IdTOS'))
								->where("a.Active=1");
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}
		
         public function gettossection($values,$groupname){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_tosdetail'),array('a.*'))
								->where("a.IdTOS in ($values)")
								->where('a.IdSection  like ? "-%"',$groupname);
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}
		
        public function getgroupsection($idgroupname){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$select = $lobjDbAdpt->select()				  
								->from(array('a' => 'tbl_questions'),array('a.QuestionGroup'))
								->where("a.QuestionType=?",$idgroupname)
								->where("a.Active=1");
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
		}
		
	         //Function To Get Pagination Count from Initial Config
	       public function fnGetPaginationCountFromInitialConfig()
	       {
		       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		       $lintPageCount = "";
		       $lstrSelect = $lobjDbAdpt->select()
								        ->from(array("a"=>"tbl_config"),array("noofrowsingrid") );
		       $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		       if($larrResult['noofrowsingrid'] == "" || $larrResult['noofrowsingrid'] == "0")
		       {
			    $lintPageCount = "5";
		       }
		       else
		       {
			    $lintPageCount = $larrResult['noofrowsingrid'];
		       }
		       return $lintPageCount;
	       }
	
	       //Function to Get Initial Config Data
	       public function fnGetInitialConfigData()
	       {
		    $lobjDbAdpt=Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect=$lobjDbAdpt->select()
								   ->from(array("a"=>"tbl_definationms"),array("LCASE(SUBSTRING(a.DefinitionCode,1,2)) AS Language") )
					 			   ->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType',array())
				 				   ->join(array("c"=>"tbl_config"),'c.Language = a.idDefinition',array("c.HtmlDir","c.DefaultCountry"));		  
		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		    return $larrResult;
	       }	
        }    
		  	 
	
