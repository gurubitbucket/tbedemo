<?php
	class Examination_Model_Newscreen extends Zend_Db_Table {
		
		
		protected $_name = 'tbl_studentapplication';
	

 public function fngetstudentinformation($maxtimes)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("f" =>"tbl_studentpaymentoption"),'f.IDApplication=a.IDApplication',array('f.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->joinleft(array("e" =>"tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array('e.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										  ->where("a.pass=3")
										  ->where("a.Venue<=?",$maxtimes)
										  ->group("d.IDApplication");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
		
     
     
     
     
	 public function fnSearchStudent($larrformData,$maxtimes)
     {
     	
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										    // ->join(array("f" =>"tbl_studentpaymentoption"),'f.IDApplication=a.IDApplication',array('f.*'))
										    ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										    ->joinleft(array("e" =>"tbl_takafuloperator"),'e.idtakafuloperator=a.Takafuloperator',array('e.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("d"=>"tbl_registereddetails"),'a.IDApplication=d.IDApplication',array('d.*'))
										  ->where("a.Payment=1")
										   ->where("a.Venue<=?",$maxtimes)
										 // ->where("a.pass=3")
										  ->where("a.pass in (3,4)")
										    ->where("a.IDApplication>1148")
										   ->where("a.Examvenue!=000")
										  ->group("d.IDApplication")
										   ->order("a.FName");	
										  
										  if($larrformData['Studentname']) $lstrSelect->where("a.Fname like '%' ? '%'",$larrformData['Studentname']);	
				if($larrformData['ICNO']) $lstrSelect->where("a.ICNO like '%' ? '%'",$larrformData['ICNO']);					  
				if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$larrformData['Coursename']);
				if($larrformData['Venues']) $lstrSelect->where("c.idcenter = ?",$larrformData['Venues']);
				if($larrformData['Takafulname']) $lstrSelect->where("e.idtakafuloperator = ?",$larrformData['Takafulname']);
				if($larrformData['Applicationtype']==2) $lstrSelect->where("a.batchpayment = 0");
				if($larrformData['Applicationtype']==3) $lstrSelect->where("a.batchpayment = 1");
				if($larrformData['Applicationtype']==1) $lstrSelect->where("a.batchpayment = 2");
				
                if(isset($larrformData['Date']) && !empty($larrformData['Date']) && isset($larrformData['Date2']) && !empty($larrformData['Date2']))
                {
				  $lstrFromDate = $larrformData['Date'];
				  $lstrToDate = $larrformData['Date2'];
				  $lstrSelect = $lstrSelect->where("a.DateTime >= '$lstrFromDate' and a.DateTime <= '$lstrToDate'");
		        }					  
		        //echo $lstrSelect;die();
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     
     
     
     
     
     
     
     
     
     public function  fngetstudenteachinformation($lintidstudent)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as AppliedDate"))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.IdProgrammaster','b.ProgramName'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("w" =>"tbl_studentpaymentoption"),'w.IDApplication=a.IDApplication',array('w.*'))
										  ->join(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->join(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }
     
     	public function  fngetstudenteachinformationforbatch($lintidstudent)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as AppliedDate"))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.IdProgrammaster','b.ProgramName'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->join(array("e"=>"tbl_state"),'a.ExamState=e.idState',array('e.*'))
										  ->join(array("f"=>"tbl_managesession"),'a.Examsession=f.idmangesession',array('f.*'))
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }
     
     
     
     
	    public function fngetstudenttype($lintidstudent)
     {
     	
     	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.batchpayment"))
										  ->where("a.IDApplication=?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     }
     
     

	     public function  newfnGetdatedifference($days,$applieddate)
     {
     	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	  $sql = "select DATEDIFF('$days','$applieddate') as nodays"; 
    	  //SELECT DATEDIFF( '2006-07-01', '2006-06-01' ) 
				//echo $sql; die();
    		 $result = $db->fetchRow($sql);    
			 return $result;
     	
     	
     }
     
     
	 public function  fngetstudentinformationfromconfig()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_config"),array("a.studedit","a.ClosingBatch"))
										  ->where("a.idConfig =1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     	
     }
     
     
		 public function fnGetVenueNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
				 				// ->join(array("c"=>"tbl_newschedulervenue"),'c.idvenue=a.idcenter',array(''))	
				 				// ->join(array("e"=>"tbl_newscheduler"),'e.idnewscheduler=c.idnewscheduler',array(''))
				 				// ->where("e.Active =1")			 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
	   
	
		 public function fnGetSessionforthemonth($Program,$Year,$Newvenue)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
			                       ->from(array("e"=>"tbl_newscheduler"),array('e.*'))
			                      ->join(array("r"=>"tbl_newschedulercourse"),'r.idnewscheduler=e.idnewscheduler',array(''))
			                      ->join(array("c"=>"tbl_newschedulervenue"),'c.idnewscheduler = e.idnewscheduler',array(''))
			                      ->where("e.Year=?",$Year)	
				 				  ->where("e.Active =1")			 
				 				  ->where("r.IdProgramMaster=?",$Program)
				 				  ->where("c.idvenue=?",$Newvenue);
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
	
		 public function fnGetExectSession($values,$days)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
			                       ->from(array("e"=>"tbl_newschedulersession"),array(''))
			                      ->join(array("r"=>"tbl_managesession"),'r.idmangesession=e.idmanagesession',array('r.*'))
			                         ->join(array("m"=>"tbl_newschedulerdays"),'m.idnewscheduler=e.idnewscheduler',array(''))
			                      ->where("e.idnewscheduler in ($values)")
			                        ->where("m.Days = ?",$days)
			                      ->group("r.idmangesession");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
	 public function fnGetExectSessiondays($values)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
			                       ->from(array("e"=>"tbl_newschedulerdays"),array(''))
			                      ->join(array("r"=>"tbl_days"),'r.iddays = e.Days',array('r.iddays'))
			                      ->where("e.idnewscheduler in ($values)")
			                      ->group("r.Days");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
		 public function fnGetStudentNames($maxtimes)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array("key"=>"a.FName","value"=>"a.FName"))
				 				  ->where("a.Payment=1")
										   ->where("a.VenueChange<=?",$maxtimes)
										  //->where("a.pass=3")
										  ->where("a.pass in (3,4)")
										  ->group("a.FName")
										    ->order("a.FName")	;	 				 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	 public function fnGetTakafulNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName")) 				 
				 				 ->order("a.TakafulName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
     public function fnGetCourseNames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
		
	   
	    public function fnGetSchedulerDetails()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Description")) 				 
				 				 ->order("a.Year");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	   
	   
		public function fnGetStateName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
										   ->where("a.idCountry  = ?","121")
										   ->order("a.StateName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			public function fnGetCityName($idcity){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_city"),array("key"=>"a.idCity","value"=>"a.CityName"))
										   ->where("a.idCity  = ?",$idcity);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			
			
		public function fnGetVenueName($idcity){	
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
										   ->where("a.city = ?",$idcity);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
		public function fnGetSchedulerSessionDetails($idschdulersesion){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_managesession"),array("key"=>"a.idmangesession","value"=>"a.managesessionname"))
										   ->where("a.idmangesession = ?",$idschdulersesion);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			public function fnGetSchedulerYearDetails($idsch)
			{
				
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.year"))
										   ->where("a.idnewscheduler = ?",$idsch);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
			}
			
	public function fnGetYearlistforcourse($idprog){		
			

$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Description"))
										  //->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										    ->join(array("d"=>"tbl_newmonths"),"a.From=d.idmonth",array())
										  ->join(array("e"=>"tbl_newmonths"),"a.To=e.idmonth",array())
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())

                                          ->where("a.Active   = 1")
										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;





	}
	
	
	
	public function fnGetStatelistforcourse($idprog,$idyear){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->group("e.idState")
										  ->where("a.idnewscheduler=?",$idyear);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetCitylistforcourse($idstate,$idprog,$idseched){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array("key"=>"e.idCity","value"=>"e.CityName"))
										  ->where("e.idState  = ?",$idstate)
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler  = ?",$idseched)
										  ->group("e.CityName");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetVenuelistforcourse($idcity,$idprog,$idseched){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array())
										  ->where("d.city = ?",$idcity)
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler  = ?",$idseched)
										  ->group("d.centername");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnbetweenmonths($id)
{
$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
										  ->where("a.idnewscheduler=?",$id);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	
}


	public function fnGetmonthsbetween2($tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >=(SELECT month(curdate( ))) and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
		public function fnGetmonthsbetweenvalid($expiredmonth,$tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >= $expiredmonth and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
	public function fnGetmonthsbetween($frommonth,$tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >='$frommonth' and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
	
	
	   public function fngetschedulerexception($days,$lintcity)
   {
   	  $db 	= 	Zend_Db_Table::getDefaultAdapter();	
	$lstrSelect = $db->select()
	                         ->from(array("a" =>"tbl_schedulerexception"))
	                         ->where('a.Date=?',$days)
	                         ->where('a.idcity=?',$lintcity);
	    $larrResult = $db->fetchRow($lstrSelect);
	    return $larrResult;
   	   
   }
   
	 public function fnGetVenuedetailsgetsecid($idsech)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
						                  ->where("a.Active=1")
										  ->where("a.idnewscheduler =?",$idsech); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
	public function fnGetVenuedetailsRemainingseatsoldsss($year,$idsech,$city,$month,$Date)
  {
  	
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $select ="SELECT b.NumberofSeat, b.centername,b.idcenter, d.managesessionname, d.starttime, d.endtime,d.idmangesession, (
b.NumberofSeat - IFNULL( count( a.IDApplication ) , 0 )
) AS rem
FROM `tbl_studentapplication` a, tbl_center b, tbl_managesession d,tbl_newscheduler m
WHERE a.Examvenue = b.idcenter
AND a.Examdate =$Date
AND a.Exammonth =$month
AND m.idnewscheduler =$idsech
AND a.Year =$idsech
AND m.Year =$year
AND b.Active =1
and  b.city =$city
AND d.idmangesession = a.Examsession
GROUP BY a.Examvenue, a.Examsession
UNION
SELECT b.NumberofSeat, b.centername,b.idcenter, j.managesessionname, j.starttime, j.endtime, j.idmangesession, b.NumberofSeat AS rem
FROM tbl_center b,tbl_managesession e,`tbl_studentapplication` c,tbl_newschedulersession f, tbl_managesession j,tbl_newscheduler m
WHERE f.idmanagesession NOT
IN (
SELECT b.Examsession
FROM tbl_studentapplication b
WHERE b.Examvenue = c.Examvenue
AND b.ExamCity =$city
AND b.Examdate =$Date
AND b.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND b.Examvenue !=000
AND b.Examsession !=000
GROUP BY b.Examvenue, b.Examsession
)
AND c.ExamCity =$city
AND b.city =$city
AND c.Examdate =$Date
AND c.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND c.Examvenue !=000
AND c.Examsession!=000
AND f.idnewscheduler =$idsech
AND f.idmanagesession=j.idmangesession
AND c.Examvenue = b.idcenter
UNION
SELECT b.NumberofSeat, b.centername,b.idcenter, e.managesessionname, e.starttime, e.endtime,e.idmangesession, b.NumberofSeat AS rem
FROM tbl_center b, tbl_newschedulervenue c, tbl_managesession e
WHERE b.idcenter NOT
IN (
SELECT Examvenue
FROM tbl_studentapplication
WHERE ExamCity =$city
AND Examdate =$Date
AND Exammonth =$month
AND Year =$idsech
)
AND b.city =$city
AND b.Active =1
AND  c.idvenue = b.idcenter
AND e.idmangesession
IN (
SELECT h.idmanagesession
FROM tbl_newschedulersession h
WHERE idnewscheduler =$idsech
)" ;
 
//echo  $select;die();
		 return	$result = $lobjDbAdpt->fetchAll($select);	
  }
  
  
	public function fnGetVenuedetailsRemainingseats($year,$idsech,$city,$month,$Date,$idvenue)
  {
  	
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
						$select = "SELECT b.NumberofSeat, b.centername,b.idcenter, d.managesessionname, d.starttime, d.endtime,d.idmangesession, (
							b.NumberofSeat - IFNULL( count( a.IDApplication ) , 0 )
						) AS rem
						FROM `tbl_studentapplication` a, tbl_center b, tbl_managesession d,tbl_newscheduler m
						WHERE a.Examvenue = b.idcenter
						AND a.Examdate =$Date
						AND a.Exammonth =$month
						AND m.idnewscheduler =$idsech
						AND a.Year =$idsech
						AND m.Year =$year
						AND b.Active =1
						and b.city =$city
						and a.Examvenue=$idvenue
						AND d.idmangesession = a.Examsession
						GROUP BY a.Examvenue, a.Examsession
						UNION
						SELECT b.NumberofSeat, b.centername,b.idcenter, j.managesessionname, j.starttime, j.endtime, j.idmangesession, b.NumberofSeat AS rem
						FROM tbl_center b,tbl_managesession e,`tbl_studentapplication` c,tbl_newschedulersession f, tbl_managesession j,tbl_newscheduler m
						WHERE f.idmanagesession NOT
						IN (
						SELECT b.Examsession
						FROM tbl_studentapplication b
						WHERE b.Examvenue = c.Examvenue
						AND b.ExamCity =$city
						AND b.Examvenue=$idvenue
						AND b.Examdate =$Date
						AND b.Exammonth =$month
						AND m.idnewscheduler =$idsech
						AND m.Year =$year
						AND b.Examvenue !=000
						AND b.Examsession !=000
						GROUP BY b.Examvenue, b.Examsession
						)
						AND c.ExamCity =$city
						AND b.city =$city
						AND c.Examvenue=$idvenue
						AND c.Examdate =$Date
						AND c.Exammonth =$month
						AND m.idnewscheduler =$idsech
						AND m.Year =$year
						AND c.Examvenue !=000
						AND f.idnewscheduler =$idsech
						AND f.idmanagesession=j.idmangesession
						AND c.Examvenue = b.idcenter
						UNION
						SELECT b.NumberofSeat, b.centername,b.idcenter, e.managesessionname, e.starttime, e.endtime,e.idmangesession, b.NumberofSeat AS rem
						FROM tbl_center b, tbl_newschedulervenue c, tbl_managesession e
						WHERE b.idcenter NOT
						IN (
						SELECT Examvenue
						FROM tbl_studentapplication
						WHERE ExamCity =$city
						AND Examvenue=$idvenue
						AND Examdate =$Date
						AND Exammonth =$month
						AND Year =$idsech
						)
						AND b.city =$city
						AND b.idcenter=$idvenue
						AND b.Active =1
						AND  c.idvenue = b.idcenter
						AND e.idmangesession
						IN (
						SELECT h.idmanagesession
						FROM tbl_newschedulersession h
						WHERE idnewscheduler =$idsech
						)" ;
 
// echo $select;die();
		 return	$result = $lobjDbAdpt->fetchAll($select);	
  }
  
  
  
  
	  public function fnGetsesssiondetails($year,$city,$venue,$idsession,$day,$month)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler')
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession',array("key"=>"g.idmangesession","value"=>"g.managesessionname"))
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter')
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array())
										 // ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										   ->where("a.idnewscheduler =?",$year)
										     ->where("b.idvenue =?",$venue)
										     ->where("f.idmanagesession not in (select Examsession from  tbl_studentapplication where  Examsession in ($idsession) and ExamCity=$city and Examdate=$day and Examvenue=$venue and Year=$year)")
										   ->group("g.idmangesession");
									
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
	 public function fngetyearforthescheduler($id)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
										  ->where("a.idnewscheduler=?",$id);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
		 public function fngetstudentoldinfo($idapplication)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										  ->where("a.IDApplication =?",$idapplication);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
 public function fnUpdateStudentnewscreen($idapplication,$larrformData,$hiddenscheduler,$iduser,$newexamvenue,$idsession,$idstate,$idcity,$venuechange)
   {
   	
   if($larrformData['newexammonth']<10)
   	{
   		$months = '0'.$larrformData['newexammonth'];
   		//$larrformData['setmonth']=
   	}
   	else 
   	{
   		$months = $larrformData['newexammonth'];
   	}
   	
    if($larrformData['newexamdates']<10)
   	{
   		$date = '0'.$larrformData['newexamdates'];
   		//$larrformData['setmonth']=
   	}
   	else {
   		 		$date = $larrformData['newexamdates'];
   	}
  
   	
   $db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(
		                    'UpdUser' =>$iduser,	
		                    'Venue' =>$venuechange, 
							'DateTime' =>$larrformData['schedulerename1'].'-'.$months.'-'.$date,	
		  					'ExamState'=>$idstate,
		  					'ExamCity'=>$idcity,
		  					'Year'=>$hiddenscheduler,
		  					'Examdate'=>$larrformData['newexamdates'],
		  					'Exammonth'=>$larrformData['newexammonth'],
		                   	'Examvenue'=>$newexamvenue,	
		  					'Examsession'=>$idsession,
							'pass'=>'3'		  
						);
		$where['IDApplication = ? ']= $idapplication;		
		return $db->update('tbl_studentapplication', $postData, $where);	
   }
			

	public function newfngetyear($prog)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect="select `year`as `key` , `year` as `value` from tbl_newscheduler where idnewscheduler in (Select idnewscheduler from tbl_newschedulercourse where idprogrammaster=$prog) and active=1 group by `year`";
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
}


	public function newfnGetCitylistforcourse($idprog,$idyear,$curmonth){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array())
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.Year  = ?",$idyear)
										  ->where("a.To>=?",$curmonth)
										  ->where("a.Active  = 1")
										  ->group("d.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnnewmonthcaleshowlatest($prog,$idvenue,$year)
{
	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
	 $lstrSelect = "SELECT a.*,b.*
FROM tbl_newscheduler as a,tbl_newschedulercourse  as b,tbl_newschedulercourse as c
where a.idnewscheduler=b.idnewscheduler
and a.idnewscheduler=c.idnewscheduler
and a.Active=1
and b.IdProgramMaster=$prog
and a.idnewscheduler in (SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
and a.Year=$year";
	  	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}

	public function fnnewmonths($values)
{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
 		$lstrSelect = "SELECT min( CAST( `From` AS UNSIGNED ) ) AS minimum, max( CAST( `To` AS UNSIGNED ) ) AS maximum FROM tbl_newscheduler where idnewscheduler in ($values)";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}

public function fnnewmonthsrange($frommonth,$tomonth)
{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
  $lstrSelect = "Select a.idmonth as `key`,a.MonthName as `value`
from tbl_newmonths  as a
where a.idmonth>=$frommonth and a.idmonth<=$tomonth";	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}
public function fnGetvenuecity($idvenue)
{
	
	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
 		$lstrSelect = "SELECT  * FROM tbl_center where idcenter=$idvenue";
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
  	return $larrResult;
}


	public function fngetstudentinsertinfo($studenteditresult,$iduser,$UpdDate,$newexamvenue,$idsession,$larrformData) { //Function to get the user details
   	
		
	   if($larrformData['newexammonth']<10)
   	{
   		$months = '0'.$larrformData['newexammonth'];
   		//$larrformData['setmonth']=
   	}
   	else 
   	{
   		$months = $larrformData['newexammonth'];
   	}
   	
    if($larrformData['newexamdates']<10)
   	{
   		$date = '0'.$larrformData['newexamdates'];
   		//$larrformData['setmonth']=
   	}
   	else {
   		 		$date = $larrformData['newexamdates'];
   	}
  
  
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_venuechange";
		$postData = array(		
							'IDapplication' =>$studenteditresult['IDApplication'],	
           					'UpdDate' => $UpdDate, 
							'UpdUser' =>$iduser,	
							'DateTime'=>$studenteditresult['DateTime'],	
		  					'ExamState'=>$studenteditresult['ExamState'],
		  					'ExamCity'=>$studenteditresult['ExamCity'],
		  					'year'=>$studenteditresult['Year'],
		  					'ExamDate'=>$studenteditresult['Examdate'],
		  					'ExamMonth'=>$studenteditresult['Exammonth'],
		  					'Examvenue'=>$studenteditresult['Examvenue'],	
		  					'ExamSession'=>$studenteditresult['Examsession'],	
		                    'oldupddate'=>$studenteditresult['UpdDate'],
		                    'NewExamDate'=>$larrformData['schedulerename1'].'-'.$months.'-'.$date,
		                    'NewExamVenue'=>$newexamvenue,
		                    'NewExamSession'=>$idsession,
						);
						$result=  $lobjDbAdpt->insert($table,$postData);
	
						   return $result;
     }
   
     
	public function fnnewmonthcaleshow($prog,$idvenue,$year)
{
	 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
	 $lstrSelect = "SELECT a.*,b.*
FROM tbl_newscheduler as a,tbl_newschedulercourse  as b,tbl_newschedulercourse as c
where a.idnewscheduler=b.idnewscheduler
and a.idnewscheduler=c.idnewscheduler
and a.Active=1
and b.IdProgramMaster=$prog
and a.idnewscheduler in (SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
and a.Year=$year group by a.idnewscheduler";
	  	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}


	
 public function fngetdayofdate($iddate)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    	  $sql = "select DAYOFWEEK('$iddate') as days"; 
				//echo $sql; die();
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
	 public function  fngetschedulerofdate($ids,$days)
       {
       	
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("a.idnewscheduler"))
										  ->join(array("c"=>"tbl_newschedulerdays"),'a.idnewscheduler=c.idnewscheduler')
                                          ->where("a.Active = 1")
										  ->where("a.idnewscheduler in ($ids)")
										   ->where("c.Days=?",$days);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }

       
	  public function newfnGetcitydetailsgetsecid($venue)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_center"),array("a.city"))
						                  ->where("a.Active=1")
										  ->where("a.idcenter =?",$venue); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
  public function fngetmonthcalendar($idmonth,$year,$Program,$NewCity)
{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect ="SELECT a.*,c.*
FROM tbl_newscheduler as a,tbl_newschedulercourse  as b,tbl_newschedulerdays as c
where a.idnewscheduler=b.idnewscheduler
and a.idnewscheduler=c.idnewscheduler
and a.Active=1
and b.IdProgramMaster=$Program
and a.idnewscheduler in (SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$NewCity)
and a.Year=$year
and a.from<=$idmonth and a.To>=$idmonth";
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
}



 public function fngetcountofsessions($NewCity,$year)
    {
       $db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_newschedulervenue"),array("a.idvenue"))
			         ->join(array("b"=>"tbl_newscheduler"),'a.idnewscheduler=b.idnewscheduler',array(""))
			         ->join(array("c"=>"tbl_newschedulersession"),'b.idnewscheduler=c.idnewscheduler',array("COUNT(c.idmanagesession) as countidmanagesession","c.idnewscheduler"))
			          ->join(array("d"=>"tbl_newschedulerdays"),'a.idnewscheduler=d.idnewscheduler',array("d.*"))
                     ->where("a.idvenue =?",$NewCity)
                     ->where("b.Active =1")
                     ->where("b.Year =?",$year)
                     ->group("d.days"); 
	$result = $db->fetchAll($select);
	return $result;
    } 


   public function fngetstudentvalidatealloted($examdate,$examvenue,$examsession)
     {
     	
     	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.Allotedseats"))
                                          ->where("a.Active = 1")
										  ->where("a.date=?",$examdate)
										  ->where("a.idvenue=?",$examvenue)
										  ->where("a.idsession=?",$examsession); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     	
     } 


  	   public function fngetstudentdecrease($studenteditresult)
   {
   	$examdate=$studenteditresult['DateTime'];
   	$examvenue=$studenteditresult['Examvenue'];
   	$examsession=$studenteditresult['Examsession'];
   	
          $db = Zend_Db_Table::getDefaultAdapter();
    $larrformData1 = array("Allotedseats"=>new Zend_Db_Expr("Allotedseats-1"));	
    $where = 	$db->quoteInto('idvenue = ?',$examvenue).
      				$db->quoteInto(' AND date = ?',$examdate). 	
      				$db->quoteInto(' AND idsession = ?',$examsession); 	 
    $db->update('tbl_venuedateschedule',$larrformData1,$where);
   
   
   }
   
	   public function fngetstudentincrease($studenteditresult)
   {
   	$examdate=$studenteditresult['DateTime'];
   	$examvenue=$studenteditresult['Examvenue'];
   	$examsession=$studenteditresult['Examsession'];
   	
   	$db = Zend_Db_Table::getDefaultAdapter();
    $larrformData1 = array("Allotedseats"=>new Zend_Db_Expr("Allotedseats+1"));	
    $where = 	$db->quoteInto('idvenue = ?',$examvenue).
      				$db->quoteInto(' AND date = ?',$examdate). 	
      				$db->quoteInto(' AND idsession = ?',$examsession); 	 
    $db->update('tbl_venuedateschedule',$larrformData1,$where);
		
   	
   }
  
   
		
}
