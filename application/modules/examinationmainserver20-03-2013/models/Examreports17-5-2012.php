<?php
     class Examination_Model_Examreports extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	

      	
    public function fnGetValues($strQry)
	{
		 $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$result = $db->fetchAll("$strQry");   
			return $result;
	}
	
     public function fngetsearchdetails($larrformData){
				if($larrformData['field5']){
					$venue = $larrformData['field5'];
				}
				if($larrformData['field10']){ 
					$edate = $larrformData['field10'];
				}

				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.FName as Studentname","a.IDApplication","DATE_FORMAT(a.DateTime,'%d-%m-%Y') as ExamDate","a.DateTime as exdate","a.Payment","a.pass","count(a.IDApplication) as Registered"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.idcenter","b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName","c.IdProgrammaster"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.idmangesession","d.managesessionname","d.starttime","d.endtime"))
							 ->joinLeft(array("e" => "tbl_centerstartexam"),'a.Examvenue = e.idcenter AND a.Examsession = e.idSession AND a.Program = e.Program AND a.DateTime = e.ExamDate',array("e.ExamStartTime","e.CloseTime","e.AutoSubmitCloseTime"))
							 ->where("a.Payment = 1");
							 
			
				if($larrformData['field5']) {
					$lstrSelect  ->  where( "b.idcenter = ".$venue );
				}
				if($larrformData['field10']){
					$lstrSelect  ->  where( "a.DateTime = '$edate'" );
				}					  
				$lstrSelect  -> group("b.idcenter")
							 ->group("c.IdProgrammaster")
							 ->group("d.idmangesession")
							 ->group("a.DateTime")
							 ->order("b.centername")
							 ->order("c.IdProgrammaster")
							 ->order("d.starttime");
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
	    public function fnGetAttedndedStudents($idcenter,$idprogram,$idsession,$exdate,$pass) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Payment = 1")
							  ->group("e.IDApplication")
							  ->order("a.FName");
							 
					if($pass == 3) {		 
						$lstrSelect ->where("a.pass != '$pass'");
					} elseif($pass == 1) {
						$lstrSelect ->where("a.pass = '$pass'");
					} elseif($pass == 2)	{
						$lstrSelect ->where("a.pass = '$pass'");
					} 
							
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }

      	public function fnGetRegistered($idcenter,$idprogram,$idsession,$exdate) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Payment = 1")
							  ->group("e.IDApplication")
							  ->order("a.FName");
	
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
	    
        public function fnGetAbsent($idcenter,$idprogram,$idsession,$exdate) {
        	    $consistantresult = 'SELECT i.IDApplication  from tbl_studentapplication i where i.Examvenue = "'.$idcenter.'" and i.Program = "'.$idprogram.'" and i.DateTime="'.$exdate.'" and i.Examsession = "'.$idsession.'" and i.pass !=3 and i.Payment=1';
        	
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication","a.username","a.FName","a.MName","a.LName","a.DateOfBirth","a.EmailAddress","a.ICNO","a.DateTime"))
							 ->join(array("e" => "tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid"))
							 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("b.centername"))
							 ->join(array("c" => "tbl_programmaster"),'a.Program = c.IdProgrammaster',array("c.ProgramName"))
							 ->join(array("d" => "tbl_managesession"),'a.Examsession = d.idmangesession',array("d.managesessionname","d.starttime","d.endtime"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							 ->where("a.IDApplication NOT IN ?",new Zend_Db_Expr('('.$consistantresult.')'))
 						     ->where("a.Payment = 1")
 						     ->group("e.IDApplication")
 						     ->order("a.FName");
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
      	public function fnGetVenueList($examdate){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_studentapplication"),array(""))
							 	 ->join(array("b" => "tbl_center"),'a.Examvenue = b.idcenter',array("key"=>"b.idcenter","value"=>"b.centername"))
				 				  ->where("a.DateTime = '$examdate'")
				 				 ->where("a.Payment = 1")
				 				 ->group("b.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
		
      	public function fnGetCountOfStudents($idcenter,$idprogram,$idsession,$exdate,$pass) {
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 ->from(array("a" => "tbl_studentapplication"),array("a.IDApplication"))
							 ->where("a.Examvenue = '$idcenter'")
							 ->where("a.Program = '$idprogram'")
							 ->where("a.DateTime = '$exdate'")
							 ->where("a.Examsession = '$idsession'")
							  ->where("a.Payment = 1")
							  ->order("a.FName");
					if($pass == 3) {		 
						$lstrSelect ->where("a.pass != '$pass'");
					} elseif($pass == 1) {
						$lstrSelect ->where("a.pass = '$pass'");
					} elseif($pass == 2)	{
						$lstrSelect ->where("a.pass = '$pass'");
					} 
							
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	    }
	    
 }    
		  	 
	
