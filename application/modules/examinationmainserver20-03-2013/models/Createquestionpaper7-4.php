<?php
     class Examination_Model_Createquestionpaper extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	
	        protected $_name = 'tbl_questiongroup';
	
        
        public function fngetquestionsetlist(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array("a.*"))
										  ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster=a.Program',array("c.*"));
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	  
            
            
      public function fnGetProgramName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										  
										  ->join(array("c" => "tbl_program"),'c.IdProgram=a.idprog')
										  ->join(array("b"=>"tbl_batchmaster"),'b.IdProgrammaster=a.IdProgrammaster',array())
										  ->join(array("d"=>"tbl_tosmaster"),'b.IdBatch=d.IdBatch',array())
  										->join(array("e"=>"tbl_programrate"),'a.IdProgrammaster=e.idProgram',array())
										  ->where("e.Active=1")
										  ->where("d.Active=1")
										  ->where("c.Active =1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
            //Function for searching the author details	      
      public function fninserquestioncodes($larrformData,$idIdBatch,$iduser,$ldtsystemDate)
   {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   
		 	 $larrcreditnotedetails['Program'] = $larrformData['field1'];
		 	 $larrcreditnotedetails['Activeset'] = $idIdBatch;
		 	  $larrcreditnotedetails['PaperCode'] = $larrformData['field28'];
		 	 $larrcreditnotedetails['UpdDate'] = $ldtsystemDate;
		 	 $larrcreditnotedetails['UpdUser'] = $iduser;
		 	  $result = $lobjDbAdpt->insert('tbl_questiongroup',$larrcreditnotedetails);
		 	  $lastid  = $lobjDbAdpt->lastInsertId("tbl_questiongroup","idquestiongroup");	
   	 return $lastid;
   }
   
  public function fninserquestionsforabovecode($larrresult,$larrquestionid)
   {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  
   	  for($linti=0;$linti<count($larrquestionid);$linti++)
		 {
		 	 $larrcreditnotedetails['idquestiongroup'] = $larrresult;
		 	 $larrcreditnotedetails['idquestions'] = $larrquestionid[$linti];
		 	  $result = $lobjDbAdpt->insert('tbl_questiongroupdetails',$larrcreditnotedetails);
		 	 
		 }
   	 return $result;
   }
   
   
        public function fndisplayquestionsforabovecode($larrresult){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroupdetails"),array("")) 
										  ->join(array("c" => "tbl_questions"),'c.idquestions=a.idquestions')
										  ->where("a.idquestiongroup=?",$larrresult);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
        }    
		  	 
	
