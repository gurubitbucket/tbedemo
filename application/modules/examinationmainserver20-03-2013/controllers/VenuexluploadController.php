<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_VenuexluploadController extends Base_Base {
	
	public function init(){		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator   	   
		$this->lobjloadfilesForm = new Examination_Form_Uploadfiles ();		
		$this->lobjloadfilesmodel = new Examination_Model_Venuexlcreation;
		$this->lobjvenuexlcreationmodel = new Examination_Model_Venuexlcreation;	
	}
    
	
	
	public function indexAction(){		
		$sessionID = Zend_Session::getId ();
		//$resulttemp = $this->lobjBatchcandidatesmodel->fnDeletetempdetails($sessionID);
		
		$lobjUploadfilesForm = $this->lobjloadfilesForm; //intialize upload form
		$this->view->lobjUploadfilesForm = $lobjUploadfilesForm;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {			
			$larrformData = $this->_request->getPost (); //getting the values of  from post 
			//print_r($larrformData);exit;
			require_once 'Excel/excel_reader2.php';			
			$lintfilecount ['Count'] = 0;
			$lstruploaddir = "/uploads/venuewiswstudentexel/";
			$larrformData ['FileLocation'] = $lstruploaddir;
			$larrformData ['UploadDate'] = date ( 'Y-m-d:H:i:s' );			
			if ($_FILES ['FileName'] ['error'] != UPLOAD_ERR_NO_FILE) {
				$lintfilecount ['Count'] ++;				
				$lstrfilename = pathinfo ( basename ( $_FILES ['FileName'] ['name'] ), PATHINFO_FILENAME );
				$lstrext = pathinfo ( basename ( $_FILES ['FileName'] ['name'] ), PATHINFO_EXTENSION );
				
				$filename = $lintfilecount ['Count'] . "." . date ( 'YmdHis' ) . "." . $lstrext;
				$filename = str_replace ( ' ', '_', $lstrfilename ) . "_" . $filename;
				$file = realpath ( '.' ) . $lstruploaddir . $filename;
				if (move_uploaded_file($_FILES ['FileName'] ['tmp_name'], $file )) {
					//echo "success";
					$larrformData ['FilePath'] = $filename;
					$larrData ['FileName'] = $lstrfilename;
					$larrData ['FilePath'] = $filename;		
				} else {
					//echo "error";
				}		
			}
			//require_once 'Excel/excel_reader2.php';			
			$userDoc = realpath ( APPLICATION_PATH . '/../public/uploads/venuewiswstudentexel/' . $filename );
			$config = new Zend_Config_Xml($userDoc);
			/*echo "<pre>";
			echo $config->Venue;
			echo $config->dateExam;*/
		
			$this->lobjloadfilesmodel->fninsertdata($config,$sessionID);	 							
			$this->_redirect ( $this->baseUrl . "/examination/venuexlupload/index/data/yes/Venue/".$config->Venue."/dateExam/".$config->dateExam);
		}
		else if ($this->_request->isPost () && $this->_request->getPost ( 'Confirm' )) {
			//$larrformData = $this->_request->getPost (); //getting the values of  from post			
			$this->lobjloadfilesmodel->fninsertMaindetails($sessionID);
			
		}
		else if (!$this->_request->getParam ( 'data' )){
			$this->lobjloadfilesmodel->fnDeletetempdetails($sessionID);	   		
		}	
		if ($this->_request->getParam ( 'data' )){
				$larrformData['Date'] = $this->_request->getParam ( 'dateExam' );
				$this->view->venufield = $venuefield 	=  $this->_request->getParam ( 'Venue' );
 				$this->view->datefield = $examrvenuedate = $this->_request->getParam ( 'dateExam' );
 				
 				$larrresult = array();
 				$larrresult1 = array(); 				
 				$larrre = $this->lobjloadfilesmodel->fnselectdata($sessionID,$larrformData,$venuefield);
 				if($larrre){
 					/*$lvarresultp['pass']    = 0;
 					$lvarresultp['failed']  = 0;
 					$lvarresultp['absent']  = 0;*/
 					$larrresult = $this->lobjvenuexlcreationmodel->fnReportDownloadedSearchDetails($larrformData,$venuefield);	 					
 					for($lavrs=0;$lavrs<count($larrre);$lavrs++){
 						if($larrre[$lavrs]['pass'] == 1 ) $lvarresultp[$larrre[$lavrs]['Program']]['pass']++; 
 						else if($larrre[$lavrs]['pass'] == 2) $lvarresultp[$larrre[$lavrs]['Program']]['failed']++;
 						else if($larrre[$lavrs]['pass'] == 4) $lvarresultp[$larrre[$lavrs]['Program']]['absent']++; 												
 					}	
 					$this->view->passresult = $lvarresultp;
 				}
 			 	$larrresult1 = $this->lobjvenuexlcreationmodel->fnReportSearchDetails($larrformData,$venuefield);			
 			 
 			 	
 				$this->view->paginar = $larrresult;
				$this->gsessionidCenter->examreportpaginatorresult = $larrresult;
				
				
				
 				$this->view->paginar1 = $larrresult1;
				$this->gsessionidCenter->examreportpaginatorresult1 = $larrresult1;
				
				$this->gsessionidCenter->examvenue=$venuefield;
				$this->gsessionidCenter->examrvenuedate = $examrvenuedate;			
		}
		
	}	
}