<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_VenuedetailsController extends Base_Base {

	/*
	 * initialization function	
	 */
	public function init() {	
		
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);  
	}
	
	public function fnsetObj(){
		$this->lobjVenuedetailsModel = new Examination_Model_Venuedetails();
		$this->lobjVenuedetailsForm = new Examination_Form_Venuedetails(); //intialize user lobjuniversityForm
			$this->lobjform = new App_Form_Search(); //intialize user lobjbusinesstypeForm
	}
	
	/*
	 *  search form & grid display  
	 */
	public function indexAction() {
				
	$this->view->lobjform = $this->lobjform; 
		$this->view->lobjVenuedetailsForm = $this->lobjVenuedetailsForm; 
			$this->lobjVenuedetailsForm->FromDate->setAttrib('required',"true");
		$this->lobjVenuedetailsForm->ToDate->setAttrib('required',"true");
			
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			$this->lobjVenuedetailsForm->FromDate->setValue($larrformData['FromDate']);
				$this->lobjVenuedetailsForm->ToDate->setValue($larrformData['ToDate']);
			
				$larresultsvenues = $this->lobjVenuedetailsModel->fngetallvenuesdetails($larrformData['FromDate'],$larrformData['ToDate']);
				$this->lobjVenuedetailsForm->Venue->addMultiOptions($larresultsvenues);
					$this->lobjVenuedetailsForm->Venue->setValue($larrformData['Venue']);
			$larresult = $this->lobjVenuedetailsModel->fngetvenuedetails($larrformData);
			$larresultempty = $this->lobjVenuedetailsModel->fngetemptyvenuedetails($larrformData);
			//echo "<pre/>";
			//print_R($larresult);die();
			$this->view->totalresult = $larresult;
				$this->view->totalemptyresult = $larresultempty;
			//print_R($larresultempty);die();
		
			
		}
	
     }
     
     public function fngetallvenueAction()
     {
     	$this->_helper->layout->disableLayout();
     	$this->_helper->viewRenderer->setNoRender();
		$fromdate = $this->_getParam('fromdate');
		$todate = $this->_getParam('todate');
		
		$larresult = $this->lobjVenuedetailsModel->fngetallvenuesdetails($fromdate,$todate);
		
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larresult);
		
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
     }
}