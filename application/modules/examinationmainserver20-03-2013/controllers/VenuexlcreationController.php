<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_VenuexlcreationController extends Base_Base {
	
	public function init(){		

		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    	Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator   	   
   	   	$this->lobjnewscreenmodel = new Examination_Model_Newscreen(); //intialize newscreen db object
		$this->lobjnewscreenForm  = new Examination_Form_Newscreenxl(); 
		
		//$this->lobjnewscreenForm  = new Examination_Form_Venuexlcreation(); 
		$this->lobjstudentmodel   = new App_Model_Studentapplication(); //user model object
		$this->lobjvenuexlcreationmodel = new Examination_Model_Venuexlcreation;
	}
    
	
	
	public function indexAction(){		


		$this->view->lobjform = $this->lobjnewscreenForm;		
		$larrresultmaximum = $this->lobjnewscreenmodel->fngetstudentinformationfromconfig(); 	
		//$larrresult = $this->lobjnewscreenmodel->fngetstudentinformation($larrresultmaximum['studedit']); 
		$larrresult = 0;
		//print_r($larrresult);die();
		$this-> view->count = 0;		
		if(!$this->_getParam('search')) unset($this->gobjsessionsis->studentvenuepaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		
		$larrStudentnameresult = $this->lobjnewscreenmodel->fnGetStudentNames($larrresultmaximum['studedit']);	
		$this->lobjnewscreenForm->Studentname->addMultiOptions($larrStudentnameresult);
		
		
		$larrCourseresult = $this->lobjnewscreenmodel->fnGetCourseNames();	
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrCourseresult);
		$larrVenuesresult = $this->lobjnewscreenmodel->fnGetVenueNames();	
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrVenuesresult);
		$larrSchedulerSessionDetails = $this->lobjvenuexlcreationmodel->fnGetSchedulerSessionDetails();	
		$this->lobjnewscreenForm->examsession->addMultiOptions($larrSchedulerSessionDetails);
		$this->lobjnewscreenForm->Date->setAttrib('required',"true");
		$this->lobjnewscreenForm->Coursename->setAttrib('required',"true");
		$this->lobjnewscreenForm->Venues->setAttrib('required',"true");
		$this->lobjnewscreenForm->examsession->setAttrib('required',"true");
		if(isset($this->gobjsessionsis->studentvenuepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->studentvenuepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ('Search')) {
			$larrformData = $this->_request->getPost ();
			
			if ($this->lobjform->isValid ( $larrformData )) {
				$this->view->paramsearch =  $this->_getParam('search');			
				if($larrformData['Studentname'])	$this->lobjnewscreenForm->Studentname->setValue($larrformData['Studentname']);			
				if($larrformData['ICNO'])	$this->lobjnewscreenForm->ICNO->setValue($larrformData['ICNO']);				
				$this->lobjnewscreenForm->Date->setValue($larrformData['Date']);			
				$this->lobjnewscreenForm->Coursename->setValue($larrformData['Coursename']);
				
			//	$larrVenuesresult = $this->lobjvenuexlcreationmodel->fngetschedulerofdatevenuestudent($larrformData['Date']);
		//$this->lobjnewscreenForm->Venues->addMultiOptions($larrVenuesresult);
				$this->lobjnewscreenForm->Venues->setValue($larrformData['Venues']);
				
				
		//		$larrSchedulerSessionDetails = 	$larrschedulerdays=$this->lobjvenuexlcreationmodel->fngetschedulersessions($larrformData['Date']);
		//$this->lobjnewscreenForm->examsession->addMultiOptions($larrSchedulerSessionDetails);
		
		
		  
				$this->lobjnewscreenForm->examsession->setValue($larrformData['examsession']);
					
				$larrresult = $this->lobjvenuexlcreationmodel->fnSearchStudent($larrformData);							
				$this->view->larrresult = $larrresult;
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->studentvenuepaginatorresult = $larrresult;
				$this-> view->count = count($larrresult);
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/examination/venuexlcreation/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ('Create')) {
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$larrformData = $this->_request->getPost ();
			
			$larrresult = $this->lobjvenuexlcreationmodel->fngetStudentsdata($larrformData);
			$larrotherdata = $this->lobjvenuexlcreationmodel->fngetotherdata($larrresult[0]['Program'],$larrresult[0]['Examvenue'],$larrresult[0]['Examsession']);
		
			
	 	
			if (count($larrresult)){
				$larrotherdata['centername']		 = str_replace(' ', '', $larrotherdata['centername']);
				$larrotherdata['ProgramName'] 		 = str_replace(' ', '', $larrotherdata['ProgramName']);	
				$larrotherdata['managesessionname']  = str_replace(' ', '', $larrotherdata['managesessionname']);			
				
				$day= $larrresult[0]['DateTime'];
				$host = $_SERVER['SERVER_NAME'];
				$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
				$time = date('d-m-Y_h:i:s',time());
			 	$filename = "Pull_".$day."_".$larrotherdata['centername']."_".$larrotherdata['managesessionname']."_".$time;	
			 		
				$tabledata.= '<?xml version="1.0" encoding="UTF-8"?>
					<xml>';
						
				  $lvars =0 ;
	      		  foreach ($larrresult as $lobjStudent ){
	      		    $lvars++;
	      		  	$stdstr1 .= $lobjStudent['IDApplication'];
					if($lvars != count($larrresult))$stdstr1 .=  ",";	
	      		  	 $tabledata.= "<data_".$lvars.">";
	      		  	 	$tabledata.=   "<IDApplication>".$lobjStudent['IDApplication']."</IDApplication>
					      		  	 	<StudentId>".$lobjStudent['StudentId']."</StudentId>
					      		  	 	<username>".$lobjStudent['username']."</username>
					      		  	 	<password>".$lobjStudent['password']."</password>
					      		  	 	<FName>".$lobjStudent['FName']."</FName>
					      		  	 	<MName>".$lobjStudent['MName']."</MName>
					      		  	 	<LName>".$lobjStudent['LName']."</LName>
					      		  	 	<DateOfBirth>".$lobjStudent['DateOfBirth']."</DateOfBirth>
					      		  	 	
					      		  	 	<PermCity>".$lobjStudent['PermCity']."</PermCity>
					      		  	 	<EmailAddress>".$lobjStudent['EmailAddress']."</EmailAddress>
					      		  	 	<UpdDate>".$lobjStudent['UpdDate']."</UpdDate>
					      		  	 	<UpdUser>".$lobjStudent['UpdUser']."</UpdUser>
					      		  	 	<IdBatch>".$lobjStudent['IdBatch']."</IdBatch>
					      		  	 	<Venue>".$lobjStudent['Venue']."</Venue>
					      		  	 	<VenueTime>".$lobjStudent['VenueTime']."</VenueTime>
					      		  	 	<Program>".$lobjStudent['Program']."</Program>
					      		  	 	
					      		  	 	<idschedulermaster>".$lobjStudent['idschedulermaster']."</idschedulermaster>
					      		  	 	<Amount>".$lobjStudent['Amount']."</Amount>
					      		  	 	<ICNO>".$lobjStudent['ICNO']."</ICNO>
					      		  	 	<Payment>".$lobjStudent['Payment']."</Payment>
					      		  	 	<DateTime>".$lobjStudent['DateTime']."</DateTime>
					      		  	 	<PermAddressDetails>".$lobjStudent['PermAddressDetails']."</PermAddressDetails>
					      		  	 	<Takafuloperator>".$lobjStudent['Takafuloperator']."</Takafuloperator>
					      		  	 	<VenueChange>".$lobjStudent['VenueChange']."</VenueChange>
					      		  	 	
					      		  	 	<ArmyNo>".$lobjStudent['ArmyNo']."</ArmyNo>
					      		  	 	<batchpayment>".$lobjStudent['batchpayment']."</batchpayment>
					      		  	 	<Gender>".$lobjStudent['Gender']."</Gender>
					      		  	 	<Race>".$lobjStudent['Race']."</Race>
					      		  	 	<Qualification>".$lobjStudent['Qualification']."</Qualification>
					      		  	 	<State>".$lobjStudent['State']."</State>
					      		  	 	<CorrAddress>".$lobjStudent['CorrAddress']."</CorrAddress>
					      		  	 	<PostalCode>".$lobjStudent['PostalCode']."</PostalCode>
					      		  	 	
					      		  	 	<ContactNo>".$lobjStudent['ContactNo']."</ContactNo>
					      		  	 	<MobileNo>".$lobjStudent['MobileNo']."</MobileNo>
					      		  	 	<ExamState>".$lobjStudent['ExamState']."</ExamState>
					      		  	 	<ExamCity>".$lobjStudent['ExamCity']."</ExamCity>
					      		  	 	<Year>".$lobjStudent['Year']."</Year>
					      		  	 	<Examdate>".$lobjStudent['Examdate']."</Examdate>
					      		  	 	<Exammonth>".$lobjStudent['Exammonth']."</Exammonth>
					      		  	 	<Examvenue>".$lobjStudent['Examvenue']."</Examvenue>
					      		  	 	
					      		  	 	<Examsession>".$lobjStudent['Examsession']."</Examsession>
					      		  	 	<pass>".$lobjStudent['pass']."</pass>
					      		  	 	<Religion>".$lobjStudent['Religion']."</Religion>
					      		  	 	<idregistereddetails>".$lobjStudent['idregistereddetails']."</idregistereddetails>
					      		  	 	<Regid>".$lobjStudent['Regid']."</Regid>
					      		  	 	<Approved>".$lobjStudent['Approved']."</Approved>
					      		  	 	<RegistrationPin>".$lobjStudent['RegistrationPin']."</RegistrationPin>					      		  	 	
					      		  	 	<Cetreapproval>".$lobjStudent['Cetreapproval']."</Cetreapproval>";
	      		  	 	
	      		  	 $tabledata.= "</data_".$lvars.">";
	      		  	
	      		  	
	      		  	
	      		  	
	      		  	
	      		  	 		   			
	      		  }	 
	      		$tabledata.= "</xml>";
			
	      		$this->lobjvenuexlcreationmodel->fnUpdateStudentApplication($stdstr1,1);
	      		 
				$ourFileName = realpath('.')."/data";
				$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
				ini_set('max_execution_time', 3600);
				fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
				fclose($ourFileHandle);
				header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
				header("Content-Disposition: attachment; filename=$filename.xml");
				header("Pragma: no-cache");
				header("Expires: 0");
				readfile($ourFileName);
				unlink($ourFileName);     
			 }	
			 exit;	
			//$this->_redirect( $this->baseUrl . '/examination/venuexlcreation/index');						
		}
		
	}

public function newvenuelistsnamesAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
//echo $iddate;die();
	   	$larrschedulerdays=$this->lobjvenuexlcreationmodel->fngetschedulerofdatevenuestudent($iddate);
	   	
	   	if($larrschedulerdays)
		{
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdays);
		}
		else 
		{
		$larrschedulerdays="";
		}
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}

public function newsessionlistsnamesAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
//echo $iddate;die();
	   	$larrschedulerdays=$this->lobjvenuexlcreationmodel->fngetschedulersessions($iddate);
	   	
	   	if($larrschedulerdays)
		{
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdays);
		}
		else 
		{
		$larrschedulerdays="";
		}
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}



}