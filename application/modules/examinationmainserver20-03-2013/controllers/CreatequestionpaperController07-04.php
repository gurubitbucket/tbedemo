<?php
class Examination_CreatequestionpaperController extends Base_Base 
{
	private $lobjGroup; //db variable
	private $lobjGroupform;//Form variable
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjGroup = new Examination_Model_Createquestionpaper(); //intialize user db object
		//$this->lobjGroupform = new Examination_Form_Createquestionpaper(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction() 
	{    	 
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view
		$larrresult = $this->lobjGroup->fngetquestionsetlist(); //get businesstype details
			
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->Businesstypepaginatorresult); // clear the search session
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		
			$larrbatchresult = $this->lobjGroup->fnGetProgramName();
		$this->lobjform->field1->addMultiOptions($larrbatchresult);
		
		if(isset($this->gobjsessionsis->Businesstypepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Businesstypepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost( 'Generate' )) 
		{
			    $larrformData = $this->_request->getPost ();		
			  								   
				unset ($larrformData ['Search']);
				unset ($larrformData ['Generate']);
				  //print_r($larrformData);die();	   

				$program=$larrformData['field1'];
				  
				  
				  
				  
				  
				  //  $lobjRegistrationModel= new App_Model_Examdetails();
		
 		 		$lobjExamdetailsmodel = new App_Model_Examdetails();
 		
  	  // $program = $studentdetailsarray['Program'];
  	   
  	   $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
  	  // print_R($larractiveidbatch);
  	   $idIdBatch = $larractiveidbatch['IdBatch'];
  	   $this->view->idbatch = $idIdBatch;
  	   //die();
  	
	  
  		//print_r($larractiveidbatch);
  		//die();

  		$larrresultaa = $lobjExamdetailsmodel->fnGetBatchName($idIdBatch);
	//echo"<pre/>";
		//print_r($larrresultaa[0]['NosOfQues']);die();
  		
		$this->view->noofqtns = $larrresultaa[0]['NosOfQues'];

  		
  		$larrmultisections = $lobjExamdetailsmodel->fnGetSectionsss($idIdBatch);
  		  		
  		$larridqtnfrombatch = $lobjExamdetailsmodel->fnGetQtnfromidbatch($idIdBatch);
  
  	
  		for ($linta=0;$linta<count($larridqtnfrombatch);$linta++)
	  	{
	  		$larrsection[$linta]=$larrmultisections[$linta]['IdSection'];
	  		
	  		$larrqtnfrombatch[$larrsection[$linta]]=$larridqtnfrombatch[$linta]['NosOfQuestion'];
	  	}
  			
  		
  		$this->view->sectionqtns = $larrqtnfrombatch;
  	for ($sec=0;$sec<count($larrmultisections);$sec++)
  	{
  		$larrsection[$sec]=$larrmultisections[$sec]['IdSection'];
  	}
  //	echo "<pre />";
  //	print_r($larrmultisections);die();
  	for($s=0;$s<count($larrsection);$s++)
		{
			$value = $larrsection[$s];
			if($s==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
			
		}
		
		$this->view->countsections = count($larrsection);
		$this->view->listsections = $values;
 
  	
  	$this->view->sectionsarray = $larrsection;
  		$this->view->sections = $larrmultisections;
  		
  		 $g=0;
  		for($i=0;$i<count($larrsection);$i++){
  		
  		$larrresulteasy = $lobjExamdetailsmodel->fnGetNoofQuestion($idIdBatch,$larrsection[$i]);
  		
  	 
  		if($larrresulteasy[0]['NoofQuestions']!=0)
  		{   
  		$larrdef[$g]=$larrresulteasy[0]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[1]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[1]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[2]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[2]['IdDiffcultLevel'];
  		$g++;
  		}
  		
  		
  	     for($k=0;$k<count($larrresulteasy);$k++)
            {
            	$questionArr  = $larrresulteasy[$k]['IdDiffcultLevel'];
            	$noofquestion = $larrresulteasy[$k]['NoofQuestions'];
            	if($noofquestion<0)
            	 $noofquestion = 0;
            	$idsection = $larrresulteasy[$k]['IdSection'];
				switch ($questionArr) 
				{
				    case 1:
				        $easyquestion = $noofquestion;
				        break;
				    case 2:
				        $Mediumquestion = $noofquestion;
				        break;
				    case 3:
				        $difficultquestion = $noofquestion;
				        break;
				}
			
				
            }
            /*echo "<pre />";
        echo $easyquestion."<br>";
           echo  $Mediumquestion."<br>";
           echo $difficultquestion."<br>";
            print_r($larrsection[$i]);*/
            $larreasyquestion[$larrsection[$i]] = $lobjExamdetailsmodel->fnGetRandomEasyQuestions($larrsection[$i],$easyquestion,$Mediumquestion,$difficultquestion);
       
          
  		}
		
  		
  $this->view->difficultylevel=$larrdef;
 //echo "<pre/>";
// print_r(count($larreasyquestion));
 // print_r($larreasyquestion);
 // die();
for($i=0;$i<count($larrsection);$i++)
		{
			$qunnoforsection  = array();
			$arranssection[] =$larrsection[$i] ;
			$noofqtnssection[$larrsection[$i]] =count($larreasyquestion[$larrsection[$i]]);
			for($l=0;$l<count($larreasyquestion[$larrsection[$i]]);$l++)
			{
				
				$qunno[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
				$qunnoforsection[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
		
				//$noofquestionofsec = 
			}
					
				$arraysofqtnnowithsection[$larrsection[$i]]=$qunnoforsection;
			
			//$arrayqstn[$l] = $larreasyquestion[$l]['idquestions'];
		}
		//print_r($arraysofqtnnowithsection);
		//die();
		$this->view->questionno = $arraysofqtnnowithsection;
		//die();
			
		for($j=0;$j<count($qunno);$j++)
		{
			$value = $qunno[$j];
			if($j==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		$arrayqtns = $values;
		$this->view->arrayqtns = $values;
		
		
		
		//echo "<pre/>";

	//print_r($values);
		//$larrquestions  = $lobjExamdetailsmodel->fngetquestionpool($values);
		
		
		$larranswers = $lobjExamdetailsmodel->fnGetAnswers($values);
  		$this->view->answers = $larranswers;
  		/*print_r($larrquestions);
		die();*/
  //	$this->view->totalnumberofquestions = count($larrquestionid);
   //    $this->view->questionsdisplay = $larrquestions;
     ////////////////////////////////////////////////////////////////////////////////////////  
       
       
       
	$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
  		
  		$flagg = 1;
  		if(count($larrmultisections)>1)
  		{
  			$flagg = 0;
  		}
  		
		    for ($linta=0;$linta<count($larrmultisections);$linta++)
		    {
		    	
		    	$larrparts[$linta]=$larrmultisections[$linta]['IdPart'];
		    	//die();
		    	
		  		  $larrquestions[$linta] = $lobjExamdetailsmodel->fngetquestionpool($values,$larrmultisections[$linta]['IdPart']);  
					shuffle ($larrquestions[$linta]);
		  		    for($totalqtn=0;$totalqtn<count($larrquestions[$linta]);$totalqtn++)
		  		    {
		  		    	 $larrquestionnumber[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestions'];
		  		    	
		  		    	 
		  		    	/*$larrquestionnumbers[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestions'];
		  		    	$concatquestions = $larrquestionnumbers[$linta][$totalqtn];
		  		    	$concatquestions.= ','.$concatquestions;*/
		  		    	
		  		    }
		  		    $larrquestionid = array();
		  		   for($totalqtn=0;$totalqtn<count($larrquestionnumber);$totalqtn++)
		  		    {	  		    	
		  		     	 $array2  = $larrquestionnumber[$totalqtn];		  		 
			  		     $larrquestionid  = array_merge($larrquestionid,$array2);
		  		    }
		  	 }
       
      // echo "<br/>";
  
		//echo "<pre/>";
//print_r($larrquestionid);
//print_r($larrquestions);
		 for($i=0;$i<count($larrquestionid);$i++)
		 {
		 	if($i==0)
			{
				$larrquestionids=$larrquestionid[$i];
			}
			else
			$larrquestionids.=','.$larrquestionid[$i];
		 }
		 /////////////////////////////inserting obrtained questions for the student//////////
		//$idstudent = $this->gsessionregistration->IDApplication;
		//  echo "<br/>";
		// echo  $program;
		 // echo "<br/>";
		// echo  $idIdBatch;
		 // echo "<br/>";
		  //$larrstudentobtainedquestions = $lobjExamdetailsmodel->fnGetStudentobtainedquestions($idstudent,$program,$idIdBatch,$larrquestionids);
		//print_r($larrquestionids);die();
		
		//////////////////////////////////end of funciton/////////////////////////////
		
		 
      // $this->view->totalnumberofquestions = count($larrquestionid);
      // $this->view->questionsdisplay = $larrquestions;  
	  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  
				  	//$auth = Zend_Auth::getInstance();
  		$iduser =1;

		$ldtsystemDate = date('Y-m-d H:i:s');
		
				$larrresult = $this->lobjGroup->fninserquestioncodes($larrformData,$idIdBatch,$iduser,$ldtsystemDate); //searching the values for the businesstype
				
				$larrresultcodes = $this->lobjGroup->fninserquestionsforabovecode($larrresult,$larrquestionid); 
				
				echo '<script language="javascript">alert("Your set is created and  the printing of question set is under development")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/examination/createquestionpaper/index';</script>";
                	die();
			//	$displayquestions= $this->lobjGroup->fndisplayquestionsforabovecode($larrresult); 
				/*echo "<pre/>";
				print_r($displayquestions);
				die();*/
	//ssionsis->Businesstypepaginatorresult = $larrresult;
			
		}
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/examination/createquestionpaper/index');			
		}
		
	}
	
	//function to add a businesstype
public function addAction() 
	{   
		$this->view->lobjGroupform = $this->lobjGroupform; //send the form to the view			
		// check if the user has clicked the save
		if ($this->_request->isPost() && $this->_request->getPost('Save'))
		{
			$larrformData = $this->_request->getPost();	
			unset ( $larrformData ['Save'] );
			$this->lobjGroup->fnaddgroup($larrformData);
			$this->_redirect( $this->baseUrl . '/examination/group/index'); //after adding Redirect to index page				
        }     
    }
    
    //function to update the businesstype
	public function editAction()
	{   	
		$this->view->lobjGroupform = $this->lobjGroupform; //send the Form the view
		$id=$this->_Getparam("id"); // get the id of the group to be updated
		$larrresult = $this->lobjGroup->fneditgroup($id);	// get the details from the DB into array variable	
		$this->view->lobjGroupform->populate($larrresult); // populate the result in the edit form
			
	    // check if the user has clicked the save
    	if ($this->_request->isPost() && $this->_request->getPost('Save'))
    	 {       	 			 	
    		$larrformData = $this->_request->getPost();	 // get the form data  		    		    	
	   		$lintId = $id;	//store the id to be updated    		
	   		unset ($larrformData ['Save']);	 //unset the save data in form data  
    	 $result25=$this->lobjGroup->getactivesets();
	     	
	       
			$values=0;
			for($i=0;$i<count($result25);$i++)
			{
				$value=$result25[$i]['IdTOS'];
				$values=$values.','.$value;
			}
			$resultgroup=$this->lobjGroup->getgroupsection($larrresult['idgroupname']);
			//echo "<pre />";
	   		//print_r($resultgroup[0]['QuestionGroup']);
	   		//die();
	   		$groupname=$resultgroup[0]['QuestionGroup'];
	   		$result99=$this->lobjGroup->gettossection($values,$groupname);
	   		
	   		if($result99 && $larrformData['Active']==0)
	   		{
	   		echo '<script language="javascript">alert("The system cannot allow to remove this question group as its  reffered by active set");</script>';   	
			echo "<script>parent.location = '".$this->view->baseUrl()."/examination/group/index';</script>";
                	die();
	   			
	   		}
	   		else 
	   		{
	   				$this->lobjGroup->fnupdategroup($lintId,$larrformData); //update businesstype
			$this->_redirect( $this->baseUrl . '/examination/group/index'); //redirect to index page after updating		
	   		}
							
    	}
	  }
}