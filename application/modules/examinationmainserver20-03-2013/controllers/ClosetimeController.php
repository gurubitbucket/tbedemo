<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_ClosetimeController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjClosetimemodel = new Examination_Model_Closetime(); //intialize newscreen db object
		$this->lobjClosetimeForm = new Examination_Form_Closetime(); 
       // $lobjcommonmodel = GeneralSetup_Model_DbTable_Common();
	}
	
	public function indexAction() 
	{
	
		$lobjcommonmodel = new GeneralSetup_Model_DbTable_Common();
		$this->view->lobjClosetimeForm = $this->lobjClosetimeForm;
		$date = date('Y-m-d');
		$this->view->lobjClosetimeForm->Date->setValue($date);
		$this->view->flag=1;
		//$this->view->lobjClosetimeForm->Date->setValue(date('Y-m-d'));
	    if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();

			$iddate = $larrformData['Date'];
			$session = $larrformData['session'];
			$this->view->flag=2;
			$this->view->lobjClosetimeForm->Date->setValue($iddate);
			$larrdates=explode('-',$iddate);
		$year= $larrdates[0];
		$month=$larrdates[1];
		$date=$larrdates[2];

		$larrschedulerresults = $this->lobjClosetimemodel->newgetscheduleryear($year);
	    $values=0;
			for($i=0;$i<count($larrschedulerresults);$i++)
			{
				if(( $month >= $larrschedulerresults[$i]['From']) && ($month <= $larrschedulerresults[$i]['To'] ))
		        {
				 $value=$larrschedulerresults[$i]['idnewscheduler'];
				 $values=$values.','.$value;
		        }
			}
	   	$larrweekday = $this->lobjClosetimemodel->fngetdayofdate($iddate);
	        if($larrweekday['days']==1)
			{
				$larrweekday['days']= 7;
			}
			else 
			{
				$larrweekday['days']=$larrweekday['days']-1;	
			}
		$larrschedulerdayssession=$this->lobjClosetimemodel->fngetschedulerofdatesessionstudent($values,$larrweekday['days'],$iddate);
				$this->lobjClosetimeForm->session->addMultiOptions($larrschedulerdayssession);

 //print

	$this->view->lobjClosetimeForm->session->setValue($larrformData['session']);
		
			$larrresult = $this->lobjClosetimemodel->fnsearchcentres($larrformData); //searching the values for the bank
$larrresultnotstarted = $this->lobjClosetimemodel->fnsearchcentresnotstarted($larrformData); //searching the values for the bank
		/*	print_r($larrresultnotstarted);
			die();*/
			$this->view->centreslistnotstarted = $larrresultnotstarted;	
			$this->view->centreslist = $larrresult;	
			$this->view->datesresult =$iddate;
			$this->view->sessionsss = $session;

	    }

	if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/closetime/index');
			//$this->_redirect($this->view->url(array('module'=>'registrations' ,'controller'=>'batchapproval', 'action'=>'index'),'default',true));
		}
		
	 if ($this->_request->isPost() && $this->_request->getPost ( 'Approve' )) {
			$larrformdata = $this->_request->getPost ();

			//$larrresult = $this->lobjClosetimemodel->fnstudentssubmitexamdetails($larrformData); //searching the values for the bank
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_answerdetails"),array("a.*"))
										  ->order("a.idanswerdetail desc");
			$larrstudentdetials = $lobjcommonmodel->fnGetValuesRow($lstrSelect);	
		   
			$lvarinitiallastanswerdetails = $larrstudentdetials['idanswerdetail'];
			
             $examdate = $larrformdata['datess'];
	       	 $session = $larrformdata['sessionsss'];

	       	 for($linti=0;$linti<count($larrformdata['centreids']);$linti++)
	       	 { 

		         $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.*"))
										  ->join(array("c" => "tbl_registereddetails"),'a.IDApplication=c.IDapplication',array("c.*","c.IdBatch as Exambatch"))
										  ->join(array("d"=> "tbl_tempexamdetails"),'c.Regid=d.Regid',array("d.*"))
										  ->where("a.payment=1")
										  ->where("a.datetime=?",$examdate)
										  ->where("a.pass  = 3")
										  ->where("a.Examsession=?",$session)
										  ->where("a.Examvenue  = ?",$larrformdata['centreids'][$linti])
										  ->group("d.Regid");
										/*  echo $lstrSelect;
										  die();*/
				 $larrstudentdetials = $lobjcommonmodel->fnGetValues($lstrSelect);
				
				 

				 if(count($larrstudentdetials)<1)
				 {
				 	/*echo "update student to absent";
				 	die();*/
				 }
				 else 
				 {
				 	$values=0;
				 	for($lintm=0;$lintm<count($larrstudentdetials);$lintm++)
				 	{
				 		
				 	 $values=$values.','."'".$larrstudentdetials[$lintm]['Regid']."'";
				 	}
				 
					  $query = "INSERT INTO tbl_answerdetails (Regid, QuestionNo,Answer) select Regid,QuestionNo,Answer from tbl_tempexamdetails where Regid in($values)";
					  $larrresults = $this->lobjClosetimemodel->insertintoanswerdetails($query);
					/*  print_r($larrresults);
					  die();*/
					  $lastidanswerdetails = $larrresults['max(idanswerdetail)'];
					  $lstrSelectanswer = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_answerdetails"),array("count(idanswerdetail)"))
										  ->join(array("c" => "tbl_registereddetails"),'a.Regid=c.Regid',array("c.idapplication","c.IdBatch","c.Regid"))
										  ->where("a.idanswerdetail>$lvarinitiallastanswerdetails")
										  ->where("a.idanswerdetail<=$lastidanswerdetails")
										  ->group("a.Regid");
					   $larrstudentanswerdetials = $lobjcommonmodel->fnGetValues($lstrSelectanswer);
					   for($lintj=0;$lintj<count($larrstudentanswerdetials);$lintj++)
					    {
					    	$studentid = $larrstudentanswerdetials[$lintj]['idapplication'];
					    	$totalattended = $larrstudentanswerdetials[$lintj]['count(idanswerdetail)'];
					    	$lvarIdBatch = $larrstudentanswerdetials[$lintj]['IdBatch'];
					    	$lvarRegid = $larrstudentanswerdetials[$lintj]['Regid'];
					    	
					    	
					    	$larrcorrectanswer = $this->lobjClosetimemodel->fncorrectanswer($lvarRegid);
					    	$lvarcorrectanswer = count($larrcorrectanswer);
					    	/*print_R($lvarcorrectanswer);
					    	die();*/
							    	
				       	 	 $larrpercentageset =$this->lobjClosetimemodel->fnExampercentage($lvarIdBatch);
					         $pass = $larrpercentageset['Pass'];
					         $noofquestions = $larrpercentageset['NosOfQues'];
					    	
						     $percentage = ($lvarcorrectanswer/$noofquestions)*100;
				        	 if($percentage>=$pass)
					         {
					         	 $passresult=1;
					         	 $larrpass = $this->lobjClosetimemodel->fnupdatepass($studentid,$passresult);
					             if($percentage>=85)
								  {
								  	$grade = 'A';
								  }
								  if($percentage >= 70  &&  $percentage<=84)
								  {
								  	$grade = 'B';
								  }
								  if($percentage >= 55  &&  $percentage<=69)
								  {
								  	$grade = 'C';
								  }
								  $larrEmailTemplateDesc =  $this->lobjClosetimemodel->fnGetEmailTemplateDescription("Student Pass");
					         }
					         else 
					         {
					         	 $grade = 'F';
					         	 $failresult=2;
					         	 $larrpass = $this->lobjClosetimemodel->fnupdatepass($studentid,$failresult);
					         	 $larrEmailTemplateDesc =  $this->lobjClosetimemodel->fnGetEmailTemplateDescription("Student Fail");	
					         }
				       	 	 
					         $larrresultinsertedmarks = $this->lobjClosetimemodel->fninsertstudentmarks($studentid,$noofquestions,$totalattended,$lvarcorrectanswer,$grade);
				       	 	 $endtime=date('H:i:s');
				       	 	 $submittedby=2;
  							 $larrresultsstudentime = $this->lobjClosetimemodel->fnupdatestudentexamdetails($studentid,$endtime,$submittedby);
					         $larrresultinsertedpartwisemarks = $this->lobjClosetimemodel->fninsertstudentpartwise($studentid,$lvarRegid);
						     $larresultidparts= $this->lobjClosetimemodel->fngettheidparts($lvarIdBatch);
				       	 	 $idpartsresultsarray=$larresultidparts['partsids'];
							 $resarr= explode(',',$idpartsresultsarray);
			 				 for($lintl=0;$lintl<3;$lintl++)
							 {
									switch($resarr[$lintl])
									{
										Case 'A':$part= 'A';
								  				 $subdetails = $this->lobjClosetimemodel->partwiseattended($lvarRegid,$part);
								  				 $partamarks = $subdetails['count(idquestions)'];
								  	  	 		 $resultupdatequery =$this->lobjClosetimemodel->updatepartwisemarksfora($lvarRegid,$partamarks,$part);
								   				 Break;
								    
									    Case 'B':$part= 'B';
											 
											  	 $subdetails = $this->lobjClosetimemodel->partwiseattended($lvarRegid,$part);
											  	 $partbmarks = $subdetails['count(idquestions)'];
											  	 $resultupdatequery =$this->lobjClosetimemodel->updatepartwisemarksfora($lvarRegid,$partbmarks,$part);
											    Break;
											    
									    Case 'C':$part= 'C';
											  	
											  	 $subdetails = $this->lobjClosetimemodel->partwiseattended($lvarRegid,$part);
											  	 $partcmarks = $subdetails['count(idquestions)'];
											  	 $resultupdatequery =$this->lobjClosetimemodel->updatepartwisemarksfora($lvarRegid,$partcmarks,$part);
											    Break;								    
									}//end of the switch
							}
						    	
					    	/////mail functionality////////////////////////////
					    require_once('Zend/Mail.php');
				        require_once('Zend/Mail/Transport/Smtp.php');
			                $larrstudentdetails = $this->lobjClosetimemodel->fnGetStudentDetailsPassfail($studentid);
			                $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$lstrEmailTemplateBody = str_replace("[NAME]",$larrstudentdetails['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[NRIC]",$larrstudentdetails['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[EXAMDATE]",$larrstudentdetails['DateTime'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[GRADE]",$grade,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Course]",$larrstudentdetails['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'tbe@ibfim.com', 'password' => 'ibfim2oi2');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'tbe@ibfim.com';
										$sender = 'tbe';
										$receiver_email = $larrstudentdetails['EmailAddress'];
										$receiver = $larrstudentdetails['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
									/*print_R($lstrEmailTemplateBody);
									die();  */       
									 try {
									$result = $mail->send($transport);
										
									} catch (Exception $e) {
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
								    }
								
					    }//end for each student 
					    
				 }

				 
			         $larrupdatestudenttoabsent =$this->lobjClosetimemodel->fnstudentsabsent($larrformdata['centreids'][$linti],$session,$examdate);
			         $larrupdateclosetime = $this->lobjClosetimemodel->fnclosetime($larrformdata['centreids'][$linti],$session,$examdate);
	       	 }
	       	 
	       	// $larrupdatestudentstoabsend
			
		}
		
	}
	
	
	public function findthetimeAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$selecteddate = $this->_getParam('selecteddate');
		$selectedsession = $this->_getParam('selectedsession');
		$idvenue = $this->_getParam('idvenue');		
		$larresultsmaxtime = $this->lobjClosetimemodel->fnfindthemaxtimeofprogram($idvenue,$selecteddate,$selectedsession);
		$maxprogramtime = $larresultsmaxtime['maximumtime'];
		
		$larresultsmaxtime = $this->lobjClosetimemodel->fnfindthemaxstarttimeofvenue($idvenue,$selecteddate,$selectedsession);
		$maxstartedtime = $larresultsmaxtime['maximumstartedtime'];
		
		$totaltime=$maxprogramtime+$maxstartedtime; 
		
		$currenttime = date('H');
		$min = date('i');
		
		//echo $selecteddate;
				$currenttimeinmin = $currenttime*60+$min;
		$currentdate =  date('Y-m-d');
		if($currentdate>$selecteddate)
		{
			echo '1';
 			exit;
		}
		
else {
		if($currenttimeinmin>$totaltime)
		{
			echo '1';
			exit;
		}
		else
		{
 			echo '0';
 			exit;
		}
		//die();
      }	
		
				
	}
	
	
	
public function checkthevenuependingAction()
{
	 $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$selecteddate = $this->_getParam('selecteddate');
		$selectedsession = $this->_getParam('selectedsession');
		$larrschedulerresults = $this->lobjClosetimemodel->fngetvenue($selecteddate,$selectedsession);
		$cntdata = count($larrschedulerresults);
		if($cntdata>0)
		{
			echo '1';
			exit;
		}
		else
		 {
 	       echo '0';
 	       exit;			
		}
}
	public function getsessionsAction()
    {
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$iddate = $this->_getParam('iddate');
		$larrdates=explode('-',$iddate);
		$year= $larrdates[0];
		$month=$larrdates[1];
		$date=$larrdates[2];

		$larrschedulerresults = $this->lobjClosetimemodel->newgetscheduleryear($year);
	    $values=0;
			for($i=0;$i<count($larrschedulerresults);$i++)
			{
				if(( $month >= $larrschedulerresults[$i]['From']) && ($month <= $larrschedulerresults[$i]['To'] ))
		        {
				 $value=$larrschedulerresults[$i]['idnewscheduler'];
				 $values=$values.','.$value;
		        }
			}
	   	$larrweekday = $this->lobjClosetimemodel->fngetdayofdate($iddate);
	        if($larrweekday['days']==1)
			{
				$larrweekday['days']= 7;
			}
			else 
			{
				$larrweekday['days']=$larrweekday['days']-1;	
			}
		$larrschedulerdayssession=$this->lobjClosetimemodel->fngetschedulerofdatesessionstudent($values,$larrweekday['days'],$iddate);
			
		if($larrschedulerdayssession)
		{
	    	$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdayssession);
		}
		else 
		{
		 	$larrCountrysessionDetailss='';	
		}
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
    }

    

	
}