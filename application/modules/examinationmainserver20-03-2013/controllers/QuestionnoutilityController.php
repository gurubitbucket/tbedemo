<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_QuestionnoutilityController extends Base_Base {


	/*
	 * initialization function	
	 */
	public function init() {	
		
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);  
   	   
	}
	public function fnsetObj(){
		$this->lobjquestionnoutilityModel = new Examination_Model_Questionnoutility();

	}
	/*
	 *  search form & grid display  
	 */
	public function indexAction() {
 		$lobjsearchform = new App_Form_Search();  //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjuserForm object to the view
		
	/*	$larresult = $this->lobjquestionnoutilityModel->fngetAllquestions();
		for($i=0;$i<count($larresult);$i++)
		{
			$questionnumber = $larresult[$i]['QuestionNumber'];
			

			  
					$questiongroup = $larresult[$i]['QuestionGroup'];
					$idquestions = $larresult[$i]['idquestions'];
					$number = $i+1;
					$stringcount = strlen($number);
					
					if($stringcount==1)
					{
						$number = '000'.$number;
					}
					else if($stringcount==2)
					{
						$number = '00'.$number;
					}
					else if($stringcount==3)
					{
						$number = '0'.$number;
					
					}
					
					$questionids = $number.'/'.$questiongroup.'/'.$questionnumber;
					//print_R($questionids);
					$larrupdateresult = $this->lobjquestionnoutilityModel->fnupdatequestionids($questionids,$idquestions);
					
					$larresultanswers = $this->lobjquestionnoutilityModel->fnupdateanswerids($idquestions,$questionids);
					
			 
			 
			
		}*/
		
		
		
		
	}
	
public function fnpdfexportAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$type = $this->_getParam('Type');
		if($type=='Questions')
		{
			$filename='questions'; 
			$host = $_SERVER['SERVER_NAME'];
	        $imgp = "http://".$host."/tbe/images/reportheader.jpg";  
	        $html ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';	
		    
		    $html.= '<table class="table" width="100%" cellpadding="5" cellspacing="1" border="1">
		
	    		<tr>
	    	       <th>Sl No</th>
	    	       <th>Question Id</th>
	    	       <th>Question Group</th>
	    	       <th>Status</th>
	        	     <th>Question Chapter</th>
	    	        <th>Question (English)</th>
	    	       <th>Answer Id</th>
	    	        <th>Answer(English)</th>
	    	       
	    	       
	    	     </tr>'	;
		    $larquestions = $this->lobjquestionnoutilityModel->fngetAllquestions();
		    for($i=0;$i<count($larquestions);$i++)
		    {
		    	$idquestions = $larquestions[$i]['idquestions'];
		    	$questions = $larquestions[$i]['Question'];
		    	$QuestionId = $larquestions[$i]['QuestionId'];
		    	$QuestionGroup = $larquestions[$i]['QuestionGroup'];
		    	$QuestionNumber = $larquestions[$i]['QuestionNumber'];
		    	if($larquestions[$i]['Active']==1)
		    	{
		    		$Active='Active';
		    	}
		    	else 
		    	{
		    		$Active='In-Active';
		    	}
		    	$larresultsanswers = $this->lobjquestionnoutilityModel->fngetAnswers($idquestions);
		    	for($j=0;$j<count($larresultsanswers);$j++)
		    	{
		    		if($i==0 && $j==0)
		    		{
		    		$slno = 1;
		    		}
		    		else 
		    		{
		    			
		    		}
		    		$answerid = $larresultsanswers[$j]['Answersid'];
		    		$answers = $larresultsanswers[$j]['answers'];
		    		if($j==0)
		    		{
		    			$html.='<tr><td>'.$slno.'</td>
		    			            <td>'.$QuestionId.'</td> 
		    			            <td>'.$QuestionGroup.'</td>
		    			              <td>'.$Active.'</td>
		    			            <td>'.$QuestionNumber.'</td>
		    			            <td>'.$questions.'</td> 
		    			            <td>'.$answerid.'</td>
		    			            <td>'.$answers.'</td> 
		    			            
		    			            </tr>';
		    		}
		    		else 
		    		{
		    			$html.='<tr><td>'.$slno.'</td>
		    			            <td></td> 
		    			            <td></td>
		    			            <td></td>
		    			            <td></td>
		    			            <td></td> 
		    			            <td>'.$answerid.'</td>
		    			            <td>'.$answers.'</td> 
		    			            
		    			            </tr>';
		    		}
		    		$slno++;
		    	}
		    	
		    }
		      $html.='</table>';
	      
	        /*echo $html;
	        die();*/
	    $filename='Questions_And_Answers'; 
	    $ourFileName = realpath('.')."/data"; 
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); //open a file to write a text
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($html));//write the content of htmlcode into text file
		fclose($ourFileHandle); //closeing a file 
		header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
		header("Content-Disposition: attachment; filename=$filename.xls");
		readfile($ourFileName);
		unlink($ourFileName);   
		}
		
	}
}