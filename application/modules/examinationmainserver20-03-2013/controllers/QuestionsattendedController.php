<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_QuestionsattendedController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjQuestionsattendedmodel = new Examination_Model_Questionsattended(); //intialize newscreen db object
		$this->lobjQuestionsattendedForm = new Examination_Form_Questionattended(); 
	}
	
	public function indexAction() 
	{
	
	$this->view->lobjQuestionsattendedForm = $this->lobjQuestionsattendedForm;
	$larrresultprog =$this->lobjQuestionsattendedmodel->fngetprog(); 
	$this->lobjQuestionsattendedForm->Program->addMultiOptions($larrresultprog);
	 

	if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformdata = $this->_request->getPost ();
			$this->lobjQuestionsattendedForm->FromDate->setValue($larrformdata['FromDate']);
				$this->lobjQuestionsattendedForm->ToDate->setValue($larrformdata['ToDate']);
			$this->view->fromdate = $larrformdata['FromDate'];
			$this->view->todate = $larrformdata['ToDate'];
			$this->view->prog = $larrformdata['Program'];
			$this->view->difftype = $larrformdata['Difficultytype'];
			$this->view->questionids = $larrformdata['questionids'];
			$this->lobjQuestionsattendedForm->Program->setValue($larrformdata['Program']);
			$this->lobjQuestionsattendedForm->Difficultytype->setValue($larrformdata['Difficultytype']);
			$this->lobjQuestionsattendedForm->questionids->setValue($larrformdata['questionids']);
			$larrresultquestions = $this->lobjQuestionsattendedmodel->fngetprogdetails($larrformdata['Program'],$larrformdata['Difficultytype']);
			$larresutstudentid = $this->lobjQuestionsattendedmodel->fnmaxids($larrformdata['Program'],$larrformdata['FromDate'],$larrformdata['ToDate']);
			$this->view->cntofstudent = count($larresutstudentid);
			$values=0;$studentid=0;
		for($idsech=0;$idsech<count($larresutstudentid);$idsech++)
		{
		
		$value=$larresutstudentid[$idsech]['Appeared'];
		$idapplication = $larresutstudentid[$idsech]['Idapplication'];
		$studentid = $studentid.','.$idapplication;
		$values=$values.','.$value;
		
		}	
		$larresutregids = $this->lobjQuestionsattendedmodel->fngetregids($studentid);
		
			$regids = 0;
		for($linti=0;$linti<count($larresutregids);$linti++)
		{
		
		$value=$larresutregids[$linti]['Regid'];
		$regids = $regids.','."'$value'";		
		}	
		$this->view->regids = $regids;
		$this->view->maxids = $values;
			$this->view->examdate = $larrformdata['FromDate'];
			$this->view->questions = $larrresultquestions;
		}
		
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/schedulerutility/index');
			//$this->_redirect($this->view->url(array('module'=>'registrations' ,'controller'=>'batchapproval', 'action'=>'index'),'default',true));
		}
		
	}
	
	
	public function fngetquestiondetailsAction()
	{
		
		 $this->_helper->layout->disableLayout();
		 $this->_helper->viewRenderer->setNoRender();
		 $qtnid = $this->_getParam('qtnid');		
		 $result = $this->lobjQuestionsattendedmodel->fngetquestionanswerdetails($qtnid);
		
		  $tabledata.= '<br><fieldset><legend align = "left"> Question and Answer:<b>Question Id'.$qtnid.'</b></legend>
					                    <table class="table" border=1 align = "center" width=100%>
					                    	<tr>
					                    	   <th><b>Question</b></th>
					                    		<th></th>
					                    		
					                    	</tr><tr>';
		  
		                
						  $tabledata.= '<td align = "left">'.$result[0]['Question'].'</td></tr><tr>
					                    		<th><b>Answers</b></th>
					                    		
					                    	</tr><tr>';
						    $tabledata.= '<td align = "left">'.$result[0]['answers'].'</td></tr><tr>';
						      $tabledata.= '<td align = "left">'.$result[1]['answers'].'</td></tr><tr>';
						        $tabledata.= '<td align = "left">'.$result[2]['answers'].'</td></tr><tr>';
						          $tabledata.= '<td align = "left">'.$result[3]['answers'].'</td></tr>';
						          for($i=0;$i<4;$i++)
						          {
						          	  if($result[$i]['CorrectAnswer']==1)
						          	  {
						          	    $tabledata.= '<tr><th><b>Correct Answer</b></th></tr><tr>';
						          	      $tabledata.= '<td align = "left">'.$result[$i]['answers'].'</td></tr>';
						          	  }
						          }
						           $tabledata.="<tr><td align='right'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
			 echo  $tabledata;	   
	}
	
	public function fnpdfexportAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$frmdate = $this->_getParam('frmdate');
		$todate = $this->_getParam('todate');
		$prog = $this->_getParam('prog');
		$difftype = $this->_getParam('difftype');
	$questionids = $this->_getParam('questionids');
	    $larrresultquestions = $this->lobjQuestionsattendedmodel->fngetprogdetails($prog,$difftype);
	    $progname = $larrresultquestions[0]['ProgramName'];
	  
		$larresutstudentid = $this->lobjQuestionsattendedmodel->fnmaxids($prog,$frmdate,$todate);
	$totalstudent = count($larresutstudentid);
		$values=0;$studentid=0;
		for($idsech=0;$idsech<count($larresutstudentid);$idsech++)
		{
		$value=$larresutstudentid[$idsech]['Appeared'];
		$idapplication = $larresutstudentid[$idsech]['Idapplication'];
		$studentid = $studentid.','.$idapplication;
		$values=$values.','.$value;
		}	
		$larresutregids = $this->lobjQuestionsattendedmodel->fngetregids($studentid);
		
			$regids = 0;
		for($linti=0;$linti<count($larresutregids);$linti++)
		{
		
		$value=$larresutregids[$linti]['Regid'];
		$regids = $regids.','."'$value'";		
		}	
		$this->view->regids = $regids;
		$this->view->maxids = $values;
		$this->view->examdate = $larrformdata['FromDate'];
		$this->view->questions = $larrresultquestions;
			
		$host = $_SERVER['SERVER_NAME'];
	$imgp = "http://".$host."/tbe/images/reportheader.jpg";  
	$html ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';	
		$html.="<br><br><table border=1 align=center width=100%><tr><td align ='left' colspan = 1><b>Exam From Date</b></td><td align ='left' colspan = 1><b>$frmdate</b></td><td align ='left' colspan = 1><b>Exam ToDate</b></td><td align ='left' colspan = 1><b>$todate</b></td><td align ='left' colspan = 2><b> Program</b></td><td align ='left' colspan = 3><b>$progname</b></td></tr></table><br><br>";
		$html.= '<table class="table" width="100%" cellpadding="5" cellspacing="1" border="1">
		
	    		<tr>
	    	
	    	       <th>Question </th>
	    	       <th>Question Group</th>
	    	       <th>Chapter</th>
	    	            <th>Question Level</th>
	    	     <th>Total Candidate</th>
	    	    	    	       <th>Appeared</th>
     <th>% Usage</th>
	    	         <th>Attended</th>
	    	    
	    	         <th>Correct</th>
	    	         <th>% Correct</th>
	    	         <th>Wrong</th>
	    	         <th>% Wrong</th>
	        	
	    	</tr>'	;
		 $cnts = count($larrresultquestions);
			     		$larrquestiondetails = $larrresultquestions; 
			     		$examdate = $frmdate;$sum=0;
   			     $regids;
	       for($i=0;$i<$cnts;$i++){
	        	$questionlevel = $larrquestiondetails[$i]['QuestionLevel'];
	        	if($questionlevel=='1')
	        	  $questionanswelevel = "Easy";
	        	else if($questionlevel=='2')
	        	  $questionanswelevel = "Medium";
	        	else 
	        	    $questionanswelevel = "Difficult";

 if($questionids==1)
	        	    {
	        	    	$larrquestios = $larrquestiondetails[$i]['idquestions'];
	        	    }
	        	    else
	        	    {
	        	    	$larrquestios = $larrquestiondetails[$i]['Question'];
	        	    } 
	      
	        $html.='<tr>
	  			<td>'.$larrquestios.'</td>
	  		      <td>'.$larrquestiondetails[$i]['QuestionGroup'].'</td>
	  		     <td>'.$larrquestiondetails[$i]['QuestionNumber'].'</td>
	  		     <td>'.$questionanswelevel.'</td>';
	  		 $lobjexamquestionsattendedmodel = new Examination_Model_Questionsattended();
	  		       $maxidss = $values;
	  		       $larrresultattended = $lobjexamquestionsattendedmodel->fnGetQtnsapperedinexam($larrquestiondetails[$i]['idquestions'],$maxidss);
	  	           $cnt = count($larrresultattended);
	  	           $sum = $sum+$cnt;
	  	           $larrresultstudentattended = $lobjexamquestionsattendedmodel->fnGettotalattended($larrquestiondetails[$i]['idquestions'],$regids);
	  	           $totalattendedquestion = count($larrresultstudentattended);
	  	           $larrresultstudentcorrectattended = $lobjexamquestionsattendedmodel->fnGettotalCorrectattended($larrquestiondetails[$i]['idquestions'],$regids);
	  	           $totalcorrectattended = count($larrresultstudentcorrectattended);
	  	           $totalwrong = $totalattendedquestion-$totalcorrectattended;
 $totalusage=0.00;$correctusage=0.00;$wrongusage=0.00;
	  	           if($totalattendedquestion>0)
	  	           {
	  	           	
	  	           $totalusage = number_format(($totalattendedquestion/$totalstudent*100),2);
	  	           } 
	  	           
	  	            if($totalcorrectattended>0)
	  	           {
	  	           	
	  	           $correctusage = number_format(($totalcorrectattended/$totalattendedquestion*100),2);
	  	           } 
	  	             if($totalwrong>0)
	  	           {
	  	           	
	  	           $wrongusage = number_format(($totalwrong/$totalattendedquestion*100),2);
	  	           } 

	  	         $html.='
	          <td>'.$totalstudent.'</td>
	  	      <td>'.$cnt.'</td>
    <td>'.$totalusage.'</td>  
	  		   <td>'.$totalattendedquestion.'</td>  
	  		  
	  		       <td>'.$totalcorrectattended.'</td>  
	  		        <td>'.$correctusage.'</td>  
	  		        <td>'.$totalwrong.'</td> 
	  		         <td>'.$wrongusage.'</td> 
	  		</tr>';
	        
	        }
	       $html.='</table>';
	      
	        /*echo $html;
	        die();*/
	       $filename='questions'; 
	     $ourFileName = realpath('.')."/data"; 
		$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file"); //open a file to write a text
		ini_set('max_execution_time', 3600);
		fwrite($ourFileHandle,htmlspecialchars_decode($html));//write the content of htmlcode into text file
		fclose($ourFileHandle); //closeing a file 
		header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
		header("Content-Disposition: attachment; filename=$filename.xls");
		readfile($ourFileName);
		unlink($ourFileName);   

			
		}

	
}