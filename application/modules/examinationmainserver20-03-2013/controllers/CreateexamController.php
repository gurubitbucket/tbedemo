<?php 
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_CreateexamController extends Base_Base {
		
	/*private $gobjroles;//class roles global variable
	private $gobjlog;//class logwriter object global variable
	private $gobjsessionsfs; //class session global variable	
	private $gstrtranslate;//Global translation variable
	private $gstrHTMLDir;//Global String for HTML Direction
	private $gstrHTMLLang;//Global String for HTML Langauage
	private $gstrDefaultCountry;//Global String for Default Country
	private $gintPageCount;//Global integer for Pagination Count	
	private $gintDefaultCountry;//Global Integer For Default Country
	*/
   	private $lobjcreateexamModel;
	private $lobjcreateexamForm;
	/*
	 * initialization function	
	 */
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);  
	}
	public function fnsetObj(){
		$this->lobjcreateexamModel = new Examination_Model_Createexam();
		$this->lobjcreateexamForm = new Examination_Form_Createexam(); //intialize user lobjuniversityForm
	}
	/*
	 *  search form & grid display  
	 */
	public function indexAction() {
						
 		$lobjsearchform = new App_Form_Search();  //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjuserForm object to the view
				
						
		$lobjbankmodel = $this->lobjcreateexamModel; //bank model object
		$larrresult = $lobjbankmodel->fnGetExamDetails(0); // get bank details

		
  		$larrresulteasy = $lobjbankmodel->fnGetBatchArraySearch();
  		$this->view->form->field5->addMultiOptions($larrresulteasy);
		
  		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->createexampaginatorresult);
			
		$lintpagecount = $this->gintPageCount; 
		$lintpage = $this->_getParam('page',1);
		if(isset($this->gobjsessionsis->createexampaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->createexampaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->view->form->isValid ( $larrformData )) {
				$larrresult = $lobjbankmodel->fnGetExamDetails($larrformData['field5']); // get bank details
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->createexampaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/createexam/index');
			//$this->_redirect($this->view->url(array('module'=>'examination' ,'controller'=>'createexam', 'action'=>'index'),'default',true));
		}
		// search operation
	/*	if ($this->_request->isPost() && $this->_request->getPost('Search')) {
			$larrformData = $this->_request->getPost();
			if ($this->_request->isPost()) {
				$larrformData = $this->_request->getPost();
				if ($lobjsearchform->isValid($larrformData)) {
					$larrresult = $lobjbankmodel->fnGetExamDetails($larrformData['field5']); // get bank details
					$lintpage = $this->_getParam('page',1);
					$lobjpaginator = Zend_Paginator::factory($larrresult);
					$lobjpaginator->setItemCountPerPage($lintpagecount);
					$lobjpaginator->setCurrentPageNumber($lintpage);
					$this->view->paginator = $lobjpaginator;
					$this->gobjsessionsfs->paginatorresult = $larrresult;
					
					$lobjLogs = new Logs_log(); // object for log & insertion to log table
					$lobjLogs->fnLogs('BANK','SRCH','Bank Search',$this->gobjsessionsfs->primaryuserid,$this->getRequest()->getServer('REMOTE_ADDR'));
				}
			}
		}*/
	}
        	
	/*
	 * create new bank
	 */
  	public function createexamsAction() {  		
		
  		$IdBatch = $this->_getParam('id');
  		//echo "<pre/>";
  		//echo $IdBatch;
  		$this->view->idbat=$IdBatch;
  	
  		$lobjExamdetailsmodel = $this->lobjcreateexamModel;
  		$larrresulteasy = $lobjExamdetailsmodel->fnGetBatchArray(0);
  		
  		
  		//print_r($larrrcrsMaster);
  		
  		$larrrcrsprogram = $lobjExamdetailsmodel->fnGetcrs($IdBatch);
  		//print_r($larrrcrsprogram);die();
  		
  		//$this->view->idhiddenprog = $larrrcrsprogram['IdProgrammaster'];
  		
  	$larrrcrsMasters = $this->lobjcreateexamModel->fnGetCoursesws($larrrcrsprogram['IdProgrammaster'],$IdBatch);
		//print_r($larrrcrsMasters);
		//$this->view->idhiddenprog = $larrrcrsprograms['IdProgrammaster'];
		$this->view->coursename3= $larrrcrsprogram['ProgramName'];
		//print_r($larrrcrsprograms);die();
  	  
  	   
  	   for($s=0;$s<count($larrrcrsMasters);$s++)
		{
			$value = $larrrcrsMasters[$s]['IdBatch'];
			if($s==0)
			{
				$values1=$value;
			}
			else
			$values1.=','.$value;
		}
  		
  		
 		$lobjbankForm = $this->lobjcreateexamForm;//intialize bank form
		$this->view->form = $lobjbankForm; //send the lobjuserForm object to the view
		
		$this->view->form->Course->setValue($larrrcrsprogram['ProgramName']);	
	    $this->view->form->Course->setAttrib('readonly','true'); 
		$ldtsystemDate = date('Y-m-d H:i:s');
        $this->view->form->UpdDate->setValue($ldtsystemDate);
        $auth = Zend_Auth::getInstance();
        $this->view->form->UpdUser->setValue( $auth->getIdentity()->iduser);
       	$this->view->form->Batch->addMultiOptions($larrresulteasy);
        $this->view->form->Batch->setAttrib('readonly','readonly'); 
        // save opearation
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost(); //getting the values of bank from post 
			if ($this->lobjcreateexamForm->isValid($larrformData)) {	
			if ($this->_request->isPost()) {
					echo "<pre>";
			/*print_r($larrformData);
				die();	*/		
					$lobjbankmodel = new Examination_Model_Createexam(); //Exam model object	
					$larrExamData['IdBatch'] 	= 	$larrformData['Batch'];
					$larrExamData['NosOfQues']  =	$larrformData['NoOfQuestion'];
					$larrExamData['TimeLimit']  =	$larrformData['ReqTime'];
					$larrExamData['AlertTime']  =	$larrformData['AlertTime'];
					$larrExamData['TOSStatus']  =	0;
					$larrExamData['Pass']  =	$larrformData['Pass'];
					$larrExamData['UpdDate'] 	=   date('Y-m-d h:i:s');
					$larrExamData['UpdUser'] 	=	1; 
					$larrExamData['Active'] 	=	0; 
					$larrExamData['Setcode'] 	=	$larrformData['Setcode'];
			        $examId = $lobjbankmodel->fnCreateexam($larrExamData); //instance for adding the lobjuserForm values to DB
					$arrIdpart = $larrformData['Idpart'];
					$larrExamDetailsData['IdTOS']  = $examId; 
					//print_r($arrIdpart);die();	
					for($m=0;$m<count($arrIdpart);$m++){	
						$larrBatchDetailsData['NoOfQuestions']	 = round($larrformData['percent'][$arrIdpart[$m]][0]);	
						$larrBatchDetailsCond['IdPart']	 = $arrIdpart[$m];	
						$larrBatchDetailsCond['IdBatch'] = $larrformData['Batch'];						
						
						for($s=0;$s<count($larrformData['idquestions'][$arrIdpart[$m]]);$s++){
							
							
							$secper   =  $larrformData['secper'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][0];	 		
							if($secper)
							$larrExamDetailsData['sectionpercentage']=$secper;
							else 
							$larrExamDetailsData['sectionpercentage']=1000;
							$larrExamDetailsData['IdSection'] = $larrformData['idquestions'][$arrIdpart[$m]][$s];
							$larrExamDetailsData['NosOfQuestion'] = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][0]);
						    //$larrExamDetailsData['NosOfQuestion'] = $larrformData['hidden'.$arrIdpart[$m].$larrformData['idquestions'][$arrIdpart[$m]][$s]];
							$larrExamDetailsData['TOSDetailStatus'] = 0;
							$larrExamDetailsData['UpdDate']  = date('Y-m-d h:i:s');
							$larrExamDetailsData['UpdUser']  = 1;
							$examDetailsId = $lobjbankmodel->fnCreateexamDetails($larrExamDetailsData); //instance for adding the examDetails values to DB
							
							$larrExamSubDetailsData['IdTOSDetail'] = $examDetailsId;							
							$larrExamSubDetailsData['TOSDetailStatus'] = 0;
							$larrExamSubDetailsData['UpdDate'] = date('Y-m-d h:i:s');
							$larrExamSubDetailsData['UpdUser'] = 1;
							
							$larrExamSubDetailsData['IdDiffcultLevel'] = 1;
							$larrExamSubDetailsData['NoofQuestions'] = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][1]);
							$noqust1   = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][1]);
							$lobjbankmodel->fnCreateexamSubDetails($larrExamSubDetailsData);
							
							$larrExamSubDetailsData['IdDiffcultLevel'] = 2;
							$larrExamSubDetailsData['NoofQuestions'] = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][2]);
							$noqust2   = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][2]);
							$lobjbankmodel->fnCreateexamSubDetails($larrExamSubDetailsData);
							
							$larrExamSubDetailsData['IdDiffcultLevel'] = 3;
							$larrExamSubDetailsData['NoofQuestions'] = $larrExamDetailsData['NosOfQuestion']-($noqust2+$noqust1);
							//round($larrExamDetailsData['NosOfQuestion']*($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][3]/100));
							$lobjbankmodel->fnCreateexamSubDetails($larrExamSubDetailsData);	
							//exit;				
						}						
					}		
					
					if($values1)
				{
			$larrdeactiveset=$lobjbankmodel->fndeactivateset($values1);
				} 
				$larractiveset=$lobjbankmodel->fnactivateset($larrExamData['IdBatch']);
					$this->_redirect( $this->baseUrl . "/examination/createexam/index");	
					//$this->_redirect( $this->baseUrl . "/examination/createexam/displayquestion/idtos/$examId");			   
			        //echo "<script>parent.location = '".$this->view->baseUrl()."/examination/createexam/index';</script>";	
			        exit;			
			}
		}
  		}
	}
public function editcreateexamAction(){
	
  	$lobjExamdetailsmodel = $this->lobjcreateexamModel;
  	
$idTOSMaster = $this->_getParam('id');
$this->view->idTOSMaster = $idTOSMaster;
			$idbatches= $this->lobjcreateexamModel->fnGetBatch($idTOSMaster);
			
			
  	$lobjExamdetailsmodel = $this->lobjcreateexamModel;
  	$larrrcrsprogram = $lobjExamdetailsmodel->fnGetcrs($idbatches['IdBatch']);
  	
  		//$larrrcrsprograms = $this->lobjcreateexamModel->fnGetcrs($idbatches['IdBatch']);
		
		$larrrcrsMasters = $this->lobjcreateexamModel->fnGetCoursesws($larrrcrsprogram['IdProgrammaster'],$idbatches['IdBatch']);
		//print_r($larrrcrsMasters);die();
		//$this->view->idhiddenprog = $larrrcrsprograms['IdProgrammaster'];
		$this->view->coursename3= $larrrcrsprogram['ProgramName'];
		//print_r($larrrcrsprograms);die();
  	  
  	   
  	   for($s=0;$s<count($larrrcrsMasters);$s++)
		{
			$value = $larrrcrsMasters[$s]['IdBatch'];
			if($s==0)
			{
				$values1=$value;
			}
			else
			$values1.=','.$value;
		}
  	  
  	if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost(); //getting the values of bank from post 	
			
			
			if ($this->lobjcreateexamForm->isValid($larrformData)) {
			if ($this->_request->isPost()) {
				
				$larrExamCond['IdBatch'] = $larrformData['Batch'];
				$larrExamCond['idTOSMaster'] = $larrformData['idTOSMaster'];
					$lobjbankmodel = new Examination_Model_Createexam(); //Exam model object					
					$larrExamData['NosOfQues']  =	$larrformData['NoOfQuestion'];
					$larrExamData['TimeLimit']  =	$larrformData['ReqTime'];
					$larrExamData['AlertTime']  =	$larrformData['AlertTime'];	
					$larrExamData['Pass']  =	$larrformData['Pass'];				
					$larrExamData['UpdDate'] 	=   date('Y-m-d h:i:s');
					$larrExamData['UpdUser'] 	=	1; 		
					$larrExamData['Active'] 	=	$larrformData['Active'];	
					$larrExamData['Setcode'] 	=	$larrformData['Setcode'];
				
			        $examId = $lobjbankmodel->fnUpdateexam($larrExamData,$larrExamCond); //instance for adding the lobjuserForm values to DB		       
					$arrIdpart = $larrformData['Idpart'];
					$larrExamDetailsCond['IdTOS']  = $examId; 					
					for($m=0;$m<count($arrIdpart);$m++){		
//print_r($larrformData['secper'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][0]);die();
						$secper   =  $larrformData['secper'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][0];	 		
							if($secper)
							$larrExamDetailsData['sectionpercentage']=$secper;
							else 
							$larrExamDetailsData['sectionpercentage']=1000;
							
						$larrBatchDetailsData['NoOfQuestions']	 = round($larrformData['percent'][$arrIdpart[$m]][0]);	
						
						$larrBatchDetailsCond['IdPart']	 = $arrIdpart[$m];	
						$larrBatchDetailsCond['IdBatch'] = $larrformData['Batch'];						
						$lobjbankmodel->fnUpdateBatchDetails($larrBatchDetailsData,$larrBatchDetailsCond);	
						for($s=0;$s<count($larrformData['idquestions'][$arrIdpart[$m]]);$s++){
							$larrExamDetailsCond['IdSection'] = $larrformData['idquestions'][$arrIdpart[$m]][$s];
							$larrExamDetailsData['NosOfQuestion'] = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][0]);
							$larrExamDetailsData['UpdDate']  = date('Y-m-d h:i:s');
							$larrExamDetailsData['UpdUser']  = 1;
							$examDetailsId = $lobjbankmodel->fnUpdateexamDetails($larrExamDetailsData,$larrExamDetailsCond); //instance for adding the examDetails values to DB
							
							$larrExamSubDetailsCond['IdTOSDetail'] = $examDetailsId;							
							$larrExamSubDetailsData['UpdDate'] = date('Y-m-d h:i:s');
							$larrExamSubDetailsData['UpdUser'] = 1;	
													
							$larrExamSubDetailsCond['IdDiffcultLevel'] = 1;							
							$larrExamSubDetailsData['NoofQuestions'] = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][1]);
							$noqust1   = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][1]);
							$lobjbankmodel->fnUpdateexamSubDetails($larrExamSubDetailsData,$larrExamSubDetailsCond);
							
							$larrExamSubDetailsCond['IdDiffcultLevel'] = 2;
							$larrExamSubDetailsData['NoofQuestions'] = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][2]);
							$noqust2   = round($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][2]);
							$lobjbankmodel->fnUpdateexamSubDetails($larrExamSubDetailsData,$larrExamSubDetailsCond);
							
							$larrExamSubDetailsCond['IdDiffcultLevel'] = 3;
							$larrExamSubDetailsData['NoofQuestions'] = $larrExamDetailsData['NosOfQuestion']-($noqust2+$noqust1);
							//$larrExamSubDetailsData['NoofQuestions'] = round($larrExamDetailsData['NosOfQuestion']*($larrformData['percent'][$arrIdpart[$m]][$larrformData['idquestions'][$arrIdpart[$m]][$s]][3]/100));
							$lobjbankmodel->fnUpdateexamSubDetails($larrExamSubDetailsData,$larrExamSubDetailsCond);							
						}						
					}	
					if($values1)
				{
			$larrdeactiveset=$lobjbankmodel->fndeactivateset($values1);
				} 
				$larractiveset=$lobjbankmodel->fnactivateset($larrExamCond['IdBatch']);
					$this->_redirect( $this->baseUrl . "/examination/createexam/index");					   
			     //echo "<script>parent.location = '".$this->view->baseUrl()."/examination/createexam/index';</script>";	
			     exit;					
			}
			}
						
	}
	else if($this->_getParam('id')){
	  	$idTOSMaster = $this->_getParam('id');	
	  	$this->view->idTOSMaster = $idTOSMaster;
		$larrrTOSMaster = $lobjExamdetailsmodel->fnGetExamDetailsEdit($idTOSMaster);  
		$larrrcrsMaster = $lobjExamdetailsmodel->fnGetCourse($idTOSMaster);
		//print_r($larrrTOSMaster);die();
	  	$larrrTOSFullDetails = $lobjExamdetailsmodel->fnGetTOSFullDetails($idTOSMaster);  
		$lobjbankForm = $this->lobjcreateexamForm;//intialize bank form	
		$this->view->form = $lobjbankForm; //send the lobjuserForm object to the view
		$larrresulteasy = $lobjExamdetailsmodel->fnGetBatchArray($larrrTOSMaster[0]['IdBatch']); 
		$this->view->form->Batch->addMultiOptions($larrresulteasy);
		$this->view->editidbatch = $larrrTOSMaster[0]['IdBatch'];
		$arrIdpart['IdPart'] = array();
		$arrIdpart[]['QuestionNumber'] = array();
		$cnt = 0;
	/*	echo "<pre>";
		print_r($larrrTOSFullDetails);
		exit;*/
  		foreach($larrrTOSFullDetails as $larrSectionsarr)	{ 	  		
  			if(!$arrIdpart[$larrSectionsarr['IdPart']]['QuestionNumber'])	$arrIdpart[$larrSectionsarr['IdPart']]['QuestionNumber'] = array();		
  			if(!in_array($larrSectionsarr['IdPart'],$arrIdpart['IdPart'])){
  				$arrIdpart['IdPart'][] = $larrSectionsarr['IdPart'];
  				$arrIdpart['id'][] = $larrSectionsarr['IdBatchDetail'];
  			}
  			if(!in_array($larrSectionsarr['QuestionNumber'],$arrIdpart[$larrSectionsarr['IdPart']]['QuestionNumber'])){
	  			$arrIdpart[$larrSectionsarr['IdPart']]['QuestionNumber'][] = $larrSectionsarr['QuestionNumber'];
	  			$arrIdpart[$larrSectionsarr['IdPart']]['idquestions'][]    = $larrSectionsarr['idquestions'];
	  			$arrIdpart[$larrSectionsarr['IdPart']]['NOQ']    = $arrIdpart[$larrSectionsarr['IdPart']]['NOQ'] +$larrSectionsarr['tdNosOfQuestion'];  				
  			}
  			$arrIdpart[$larrSectionsarr['IdPart']][$larrSectionsarr['QuestionNumber']][0] = $larrSectionsarr['tdNosOfQuestion'];
  			$arrIdpart[$larrSectionsarr['IdPart']][$larrSectionsarr['QuestionNumber']][$larrSectionsarr['IdDiffcultLevel']] = $larrSectionsarr['NoofQuestions'];
  			$cnt++;
  		} 
  		$larrSections  = $lobjExamdetailsmodel->fnGetBatchDetailsAjax($larrrTOSMaster[0]['IdBatch']);
  		foreach($larrSections as $larrSectionsarr)	{
  			$arrIdpart['count'] = $arrIdpart['count'] + $larrSectionsarr['cnt'];
  			$arrIdpart[$larrSectionsarr['QuestionNumber']]['count'] = $arrIdpart[$larrSectionsarr['QuestionNumber']]['count'] + $larrSectionsarr['cnt'];
  			$arrIdpart[$larrSectionsarr['IdPart']]['count'] = $arrIdpart[$larrSectionsarr['IdPart']]['count'] + $larrSectionsarr['cnt'];  			
  		}
  		$larrCntLevel  = $lobjExamdetailsmodel->fnGetQuestionDetailsAjax($larrrTOSMaster[0]['IdBatch']); 
  		foreach($larrCntLevel as $larrSectionsarr)	{  	
  			$arrIdpart[$larrSectionsarr['QuestionNumber']][$larrSectionsarr['QuestionLevel']]['count'] = $larrSectionsarr['qcnt'];
  		} 	
  		//print_r($larrrTOSMaster) ;die(); 		
	  	$this->view->larrSections = $arrIdpart;		
		$this->view->form->Batch->setValue($larrrTOSMaster[0]['IdBatch']);
		$this->view->form->Batch->setAttrib('readonly','readonly'); 
		$this->view->form->BatchFrom->setValue(date('d-m-Y', strtotime($larrrTOSMaster[0]['BatchFrom'])));
		$this->view->form->BatchTo->setValue(date('d-m-Y', strtotime($larrrTOSMaster[0]['BatchTo'])));
		$this->view->form->NoOfQuestion->setValue($larrrTOSMaster[0]['NosOfQues']);
		$this->view->form->Pass->setValue($larrrTOSMaster[0]['Pass']);
		$this->view->form->Active->setValue($larrrTOSMaster[0]['Active']);
		$this->view->NoOfTotalQuestions = $larrrTOSMaster[0]['NosOfQues'];
		$this->view->thirdquestion=$larrrTOSMaster[0]['NosOfQues'];
		$this->view->form->ReqTime->setValue($larrrTOSMaster[0]['TimeLimit']);
		$this->view->form->AlertTime->setValue($larrrTOSMaster[0]['AlertTime']);
		$this->view->form->Setcode->setValue($larrrTOSMaster[0]['Setcode']);	
		$this->view->form->Course->setValue($larrrcrsMaster['ProgramName']);		
	}	
}        
public function getbatchdetailsAction()
	{		
		$this->_helper->layout->disableLayout();		
		$IdBatch = $this->_getParam('IdBatch');	
		$lobjExamdetailsmodel = $this->lobjcreateexamModel;
  		$larrSections  = $lobjExamdetailsmodel->fnGetBatchDetailsAjax($IdBatch); 
  		$larrCntLevel  = $lobjExamdetailsmodel->fnGetQuestionDetailsAjax($IdBatch); 
  		//echo "<pre>";//print_r($larrCntLevel); exit;		
  		$arrIdpart['IdPart'] = array();
  		foreach($larrSections as $larrSectionsarr)	{  			
  			if(!in_array($larrSectionsarr['IdPart'],$arrIdpart['IdPart'])){  				
  				$arrIdpart['IdPart'][] = $larrSectionsarr['IdPart'];
  				$arrIdpart['id'][] = $larrSectionsarr['IdBatchDetail'];
  			} 
  			$arrIdpart['count'] = $arrIdpart['count'] + $larrSectionsarr['cnt'];
  			$arrIdpart[$larrSectionsarr['QuestionNumber']]['count'] = $arrIdpart[$larrSectionsarr['QuestionNumber']]['count'] + $larrSectionsarr['cnt'];
  			$arrIdpart[$larrSectionsarr['IdPart']]['count'] = $arrIdpart[$larrSectionsarr['IdPart']]['count'] + $larrSectionsarr['cnt'];
  			$arrIdpart[$larrSectionsarr['IdPart']]['QuestionNumber'][] = $larrSectionsarr['QuestionNumber'];
  			$arrIdpart[$larrSectionsarr['IdPart']]['idquestions'][]    = $larrSectionsarr['idquestions'];
  		}  
  		foreach($larrCntLevel as $larrSectionsarr)	{  	
  			$arrIdpart[$larrSectionsarr['QuestionNumber']][$larrSectionsarr['QuestionLevel']]['count'] = $larrSectionsarr['qcnt'];
  		}    		
  		$this->view->larrSections = $arrIdpart;  		
	}
public function getbatchinputdatasAction()
	{				
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$IdBatch = $this->_getParam('IdBatch');	
		$lobjExamdetailsmodel = $this->lobjcreateexamModel;
  		$larrSections  = $lobjExamdetailsmodel->fnGetBatchDetailsAjax($IdBatch);
  		echo date('d-m-Y', strtotime($larrSections[0]['BatchFrom']))."#@#".date('d-m-Y', strtotime($larrSections[0]['BatchTo']));
  		exit;
	}
	public function displayquestionAction()
	{
		$lobjbankForm = $this->lobjcreateexamForm;//intialize bank form
		$this->view->form = $lobjbankForm; 
		$IdTos = $this->_getParam('idtos');
		$this->view->idt = $IdTos;
		
		$idbatches= $this->lobjcreateexamModel->fnGetBatch($IdTos);
		
		
		$idbatchres= $this->lobjcreateexamModel->fnGetBatchDetailsAjax($idbatches['IdBatch']);
		
	
		
		$larrrcrsprograms = $this->lobjcreateexamModel->fnGetcrs($idbatches['IdBatch']);
		
		$larrrcrsMasters = $this->lobjcreateexamModel->fnGetCoursesws($larrrcrsprograms['IdProgrammaster'],$idbatches['IdBatch']);
		
		$this->view->idhiddenprog = $larrrcrsprograms['IdProgrammaster'];
		$this->view->coursename3= $larrrcrsprograms['ProgramName'];
		//print_r($larrrcrsprograms);die();
  	  
  	   
  	   for($s=0;$s<count($larrrcrsMasters);$s++)
		{
			$value = $larrrcrsMasters[$s]['IdBatch'];
			if($s==0)
			{
				$values1=$value;
			}
			else
			$values1.=','.$value;
		}
		
			
  		$this->view->idhiddenprog = $larrrcrsprograms['IdProgrammaster'];
		
		$this->view->idsectioncnt = $idbatchres;
		
		$lobjExamdetailsmodel = new App_Model_Examdetails();
		$idIdBatch= $idbatches['IdBatch'];
		
  		$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
  		
  		$larridqtnfrombatch = $lobjExamdetailsmodel->fnGetQtnfromidbatch($idIdBatch);
  	
  		for ($linta=0;$linta<count($larridqtnfrombatch);$linta++)
	  	{
	  		$larrsection[$linta]=$larrmultisections[$linta]['IdSection'];
	  		
	  		$larrqtnfrombatch[$larrsection[$linta]]=$larridqtnfrombatch[$linta]['NosOfQuestion'];
	  	}
  			
  		
  		$this->view->sectionqtns = $larrqtnfrombatch;
  	for ($sec=0;$sec<count($larrmultisections);$sec++)
  	{
  		$larrsection[$sec]=$larrmultisections[$sec]['IdSection'];
  	}
  	
  	
  	for($s=0;$s<count($larrsection);$s++)
		{
			$value = $larrsection[$s];
			if($s==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		
		$this->view->countsections = count($larrsection);
		$this->view->listsections = $values;
 
  	
  	$this->view->sectionsarray = $larrsection;
  		$this->view->sections = $larrmultisections;
  		
  		 $g=0;
  		for($i=0;$i<count($larrsection);$i++){
  		
  		$larrresulteasy = $lobjExamdetailsmodel->fnGetNoofQuestion($idIdBatch,$larrsection[$i]);
  		
  	
  		if($larrresulteasy[0]['NoofQuestions']!=0)
  		{   
  		$larrdef[$g]=$larrresulteasy[0]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[1]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[1]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[2]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[2]['IdDiffcultLevel'];
  		$g++;
  		}
  		
  		
  	     for($k=0;$k<count($larrresulteasy);$k++)
            {
            	$questionArr  = $larrresulteasy[$k]['IdDiffcultLevel'];
            	$noofquestion = $larrresulteasy[$k]['NoofQuestions'];
            	$idsection = $larrresulteasy[$k]['IdSection'];
				switch ($questionArr) 
				{
				    case 1:
				        $easyquestion = $noofquestion;
				        break;
				    case 2:
				        $Mediumquestion = $noofquestion;
				        break;
				    case 3:
				        $difficultquestion = $noofquestion;
				        break;
				}
				
            }
            	
            $larreasyquestion[$larrsection[$i]] = $lobjExamdetailsmodel->fnGetRandomEasyQuestions($larrsection[$i],$easyquestion,$Mediumquestion,$difficultquestion);
       
          
  		}

  $this->view->difficultylevel=$larrdef;
 
  		$this->view->arrQuestions=$larreasyquestion;
  		
  		$m=0;
  		for($i=0;$i<count($larreasyquestion);$i++)
  		{
  			
  			$sec=$larrsection[$i];
  		
  			$count= $idbatchres[$i]['cnt'];
  			for($j=0;$j<$count;$j++)
  			{
  				if($larreasyquestion[$sec][$j]['idquestions'])
  				{
	  				$larrssss[$m]=$larreasyquestion[$sec][$j]['idquestions'];
	  				$m++;
  				}
  			}
  		}
  		//echo $m;
  		//print_r($larreasyquestion);
  		$checkboxses=count($larrssss);
  		
  		for($i=0;$i<count($larrssss);$i++)
  		{
  			$value = $larrssss[$i];
			if($i==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
  		}
  		$idq=$values;
  		$larrquestions= $this->lobjcreateexamModel->fnGetquestions($idq);
  		
  		$this->view->cntid=$m;
  		$this->view->countcomp=count($larrquestions);
  		
  		//print_r($larrquestions);die();
  		$this->view->remquestion=$larrquestions;
  		
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost(); //getting the values of bank from post 	
			if ($this->lobjcreateexamForm->isValid($larrformData)) {
				if ($this->_request->isPost()) {
					//$lobjbankmodel = new Examination_Model_Createexam(); //Exam model object	
					$cntquestions=count($larrformData['idques']);
					
					$larrid=$larrformData['idques'];
					if($larrformData['hiddenprogramid']=='')
					{
						$larrformData['hiddenprogramid']=$larrrcrsprograms['IdProgrammaster'];
					}
					$porgid=$larrformData['hiddenprogramid'];
			if($checkboxses==$cntquestions)
			{ 
				 $questionid =$this->lobjcreateexamModel->fnaddExamquestions($larrid,$idbatches['IdBatch'],$porgid);
				if($values1)
				{
			$larrdeactiveset=$this->lobjcreateexamModel->fndeactivateset($values1);
				} 
				$larractiveset=$this->lobjcreateexamModel->fnactivateset($idbatches['IdBatch']);
		
				$this->_redirect( $this->baseUrl . '/examination/createexam/index');	
				
			}
			else {
				echo '<script language="javascript">alert("Please only check '.$checkboxses.' Number of questions");</script>';   
			}
			
				}
				
			}
  		}
  	
	}
	
public function displayeditquestionAction()
	{
		$lobjbankForm = $this->lobjcreateexamForm;//intialize bank form
		$this->view->form = $lobjbankForm; 
		$IdTos = $this->_getParam('idtos');
		$this->view->idt = $IdTos;
		
		$idbatches= $this->lobjcreateexamModel->fnGetBatch($IdTos);
		
		
		$idbatchres= $this->lobjcreateexamModel->fnGetBatchDetailsAjax($idbatches['IdBatch']);
		
	
		
		$larrrcrsprograms = $this->lobjcreateexamModel->fnGetcrs($idbatches['IdBatch']);
		
		$larrrcrsMasters = $this->lobjcreateexamModel->fnGetCoursesws($larrrcrsprograms['IdProgrammaster'],$idbatches['IdBatch']);
		
		$this->view->idhiddenprog = $larrrcrsprograms['IdProgrammaster'];
		$this->view->coursename3= $larrrcrsprograms['ProgramName'];
		//print_r($larrrcrsprograms);die();
  	  
  	   
  	   for($s=0;$s<count($larrrcrsMasters);$s++)
		{
			$value = $larrrcrsMasters[$s]['IdBatch'];
			if($s==0)
			{
				$values1=$value;
			}
			else
			$values1.=','.$value;
		}
		
			
  		$this->view->idhiddenprog = $larrrcrsprograms['IdProgrammaster'];
		
		$this->view->idsectioncnt = $idbatchres;
		
		$lobjExamdetailsmodel = new App_Model_Examdetails();
		$idIdBatch= $idbatches['IdBatch'];
		
  		$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
  		
  		$larridqtnfrombatch = $lobjExamdetailsmodel->fnGetQtnfromidbatch($idIdBatch);
  	
  		for ($linta=0;$linta<count($larridqtnfrombatch);$linta++)
	  	{
	  		$larrsection[$linta]=$larrmultisections[$linta]['IdSection'];
	  		
	  		$larrqtnfrombatch[$larrsection[$linta]]=$larridqtnfrombatch[$linta]['NosOfQuestion'];
	  	}
  			
  		
  		$this->view->sectionqtns = $larrqtnfrombatch;
  	for ($sec=0;$sec<count($larrmultisections);$sec++)
  	{
  		$larrsection[$sec]=$larrmultisections[$sec]['IdSection'];
  	}
  	
  	
  	for($s=0;$s<count($larrsection);$s++)
		{
			$value = $larrsection[$s];
			if($s==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		
		$this->view->countsections = count($larrsection);
		$this->view->listsections = $values;
 
  	
  	$this->view->sectionsarray = $larrsection;
  		$this->view->sections = $larrmultisections;
  		
  		 $g=0;
  		for($i=0;$i<count($larrsection);$i++){
  		
  		$larrresulteasy = $lobjExamdetailsmodel->fnGetNoofQuestion($idIdBatch,$larrsection[$i]);
  		
  	
  		if($larrresulteasy[0]['NoofQuestions']!=0)
  		{   
  		$larrdef[$g]=$larrresulteasy[0]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[1]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[1]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[2]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[2]['IdDiffcultLevel'];
  		$g++;
  		}
  		
  		
  	     for($k=0;$k<count($larrresulteasy);$k++)
            {
            	$questionArr  = $larrresulteasy[$k]['IdDiffcultLevel'];
            	$noofquestion = $larrresulteasy[$k]['NoofQuestions'];
            	$idsection = $larrresulteasy[$k]['IdSection'];
				switch ($questionArr) 
				{
				    case 1:
				        $easyquestion = $noofquestion;
				        break;
				    case 2:
				        $Mediumquestion = $noofquestion;
				        break;
				    case 3:
				        $difficultquestion = $noofquestion;
				        break;
				}
				
            }
            	
            $larreasyquestion[$larrsection[$i]] = $lobjExamdetailsmodel->fnGetRandomEasyQuestions($larrsection[$i],$easyquestion,$Mediumquestion,$difficultquestion);
       
          
  		}

  $this->view->difficultylevel=$larrdef;
 
  		$this->view->arrQuestions=$larreasyquestion;
  		
  		$m=0;
  		for($i=0;$i<count($larreasyquestion);$i++)
  		{
  			
  			$sec=$larrsection[$i];
  		
  			$count= $idbatchres[$i]['cnt'];
  			for($j=0;$j<$count;$j++)
  			{
  				if($larreasyquestion[$sec][$j]['idquestions'])
  				{
	  				$larrssss[$m]=$larreasyquestion[$sec][$j]['idquestions'];
	  				$m++;
  				}
  			}
  		}
  		//echo $m;
  		//print_r($larreasyquestion);
  		$checkboxses=count($larrssss);
  		
  		for($i=0;$i<count($larrssss);$i++)
  		{
  			$value = $larrssss[$i];
			if($i==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
  		}
  		$idq=$values;
  		$larrquestions= $this->lobjcreateexamModel->fnGetquestions($idq);
  		
  		$this->view->cntid=$m;
  		$this->view->countcomp=count($larrquestions);
  		
  		//print_r($larrquestions);die();
  		$this->view->remquestion=$larrquestions;
  		
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost(); //getting the values of bank from post 	
			if ($this->lobjcreateexamForm->isValid($larrformData)) {//print_r($larrformData);die();
				if ($this->_request->isPost()) {
					//$lobjbankmodel = new Examination_Model_Createexam(); //Exam model object	
					$cntquestions=count($larrformData['idques']);
					
					$larrid=$larrformData['idques'];
					if($larrformData['hiddenprogramid']=='')
					{
						$larrformData['hiddenprogramid']=$larrrcrsprograms['IdProgrammaster'];
					}
					$porgid=$larrformData['hiddenprogramid'];
			if($checkboxses==$cntquestions)
			{ 
				 $questionid2 =$this->lobjcreateexamModel->fnupdateExamquestions($idbatches['IdBatch']);
				// print_r($idbatches['IdBatch']);die();
				 $questionid =$this->lobjcreateexamModel->fnaddExamquestions($larrid,$idbatches['IdBatch'],$porgid);
				if($values1)
				{
			$larrdeactiveset=$this->lobjcreateexamModel->fndeactivateset($values1);
				} 
				$larractiveset=$this->lobjcreateexamModel->fnactivateset($idbatches['IdBatch']);
		
				$this->_redirect( $this->baseUrl . '/examination/createexam/index');	
				
			}
			else {
				echo '<script language="javascript">alert("Please only check '.$checkboxses.' Number of questions");</script>';   
			}
			
				}
				
			}
  		}
  	
	}
public function viewquestionAction()
	{
		$IdTos = $this->_getParam('id');
		
		$idbatches= $this->lobjcreateexamModel->fnGetBatch($IdTos);
		
		
		$larrquestions= $this->lobjcreateexamModel->fnGetViewquestions($idbatches['IdBatch']);
  		$this->view->remquestion=$larrquestions;
		
		
		
	}
	
	
}