<?php
class Examination_ResendemailsController extends Base_Base {
	private $lobjprogram;
	private $lobjStudenteditForm;
	private $lobjStudenteditModel;
	private $lobjdeftype;
		public $gsessionregistration;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function fnsetObj(){
		$this->lobjResendemailsModel = new GeneralSetup_Model_DbTable_Resendemails();
		$this->lobjResendemailsForm = new GeneralSetup_Form_Resendemails();
		$this->lobjstudentmodel = new App_Model_Studentapplication();
		$this->lobjdeftype = new App_Model_Definitiontype();
				$this->lobjCommon=new App_Model_Common();
				$this->lobjStudenteditModel = new GeneralSetup_Model_DbTable_Studenteditnew();
		$this->lobjStudenteditForm = new GeneralSetup_Form_Studentedit();
		$this->lobjstudentmodel = new App_Model_Studentapplication();
		$this->lobjdeftype = new App_Model_Definitiontype();
				$this->lobjCommon=new App_Model_Common();
	  	
	  	
		
	}
	
	public function indexAction() {
		$this->gsessionregistration->mails=0;
    	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjResendemailsForm; //send the lobjuniversityForm object to the view
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		   $datecurdate = date('Y-m-d');
 $this->view->lobjform->Examdate->setValue($datecurdate);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
				$this->view->lobjform->Emailtypes->setValue($larrformData['Emailtypes']);
				$this->view->lobjform->Regtype->setValue($larrformData['Regtype']);
				$this->view->lobjform->FName->setValue($larrformData['FName']);
				$larrresult = $this->lobjResendemailsModel->fnSearchStudent($larrformData); //searching the values for the user

				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->programpaginatorresult = $larrresult;
			
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/examination/resendemails/index');
		}
		
	if ($this->_request->isPost () && $this->_request->getPost ('sendemails')) {
			$larrformData = $this->_request->getPost();
			$larresult = $this->lobjResendemailsModel->fnsendbulkmails($larrformData);
			 $this->_redirect( $this->baseUrl . '/examination/resendemails/index');
			
		}
		
	}
	
	public function editstudentAction() { //title
		$this->view->lobjStudenteditForm = $this->lobjStudenteditForm;
		$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjStudenteditForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		$larrstate = $this->lobjstudentmodel->fnGetStateName();
		$this->lobjStudenteditForm->State->addMultiOptions($larrstate);
		
		$larrQualification = $this->lobjstudentmodel->fnGetEducationDetails();
		$this->lobjStudenteditForm->Qualification->addMultiOptions($larrQualification);
		
	   $larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Race');
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjStudenteditForm->Race->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}		
		
			$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		$this->view->lobjStudenteditForm->UpdDate->setValue($ldtsystemDate);
		$this->view->lobjStudenteditForm->UpdUser->setValue(1);
			
		$lintidstudent = $this->_getParam('id');
		$this->view->id = $lintidstudent;
		$larrresults = $this->lobjStudenteditModel->fneditdetails($lintidstudent);
 	$this->view->FName=$larrresults['FName'];
	$this->view->MName=$larrresults['MName'];
	$this->view->LName=$larrresults['LName'];
	$this->view->DateOfBirth=$larrresults['DateOfBirth'];
	$this->view->PermCity=$larrresults['PermCity'];
	$this->view->EmailAddress=$larrresults['EmailAddress'];		 	
	$this->view->Takafuloperator=$larrresults['Takafuloperator'];
	$this->view->ICNO=$larrresults['ICNO'];
	$this->view->PermAddressDetails=$larrresults['PermAddressDetails'];	
	$this->view->Gender=$larrresults['Gender'];	
	$this->view->CorrAddress=$larrresults['CorrAddress'];	
	$this->view->ArmyNo=$larrresults['ArmyNo'];	
	$this->view->State=$larrresults['State'];
	$this->view->PostalCode=$larrresults['PostalCode'];
	$this->view->Race=$larrresults['Race'];
    $this->view->ContactNo=$larrresults['ContactNo'];
    $this->view->MobileNo=$larrresults['MobileNo'];
    $this->view->login=$larrresults['EmailAddress'];
    $this->view->password=$larrresults['ICNO'];
	
		$this->view->programs = $larrresults['ProgramName'];
 	$this->view->StateNames = $larrresults['StateName'];
 	$this->view->centernames = $larrresults['centername'];
 	$this->view->idapplication = $larrresults['IDApplication'];
 	$this->view->managesession = $larrresults['managesessionname'].'('.$larrresults['starttime'].'--'.$larrresults['endtime'].')';
	}
    
	
}