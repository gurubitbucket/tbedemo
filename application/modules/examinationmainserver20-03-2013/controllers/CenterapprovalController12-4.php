<?php
//error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class  Examination_CenterapprovalController extends Zend_Controller_Action { 	
public $gsessionidCenter;//Global Session Name
	public function init() { //initialization function
				
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjCommon = new App_Model_Common();
		$this->lobjCenterloginmodel = new App_Model_Centerlogin(); 
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates(); 		
		$this->lobjBatchcandidatesForm = new App_Form_Batchcandidates (); 
		$this->lobjDocumentverificationmodel = new Examination_Model_Centerapproval(); 
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	public function indexAction() {
		
		$lobjsearchform = new App_Form_Search();
		$this->view->lobjform = $lobjsearchform;
		$examcentermodel = new App_Model_Examcenterapproval();
		$lobjExamCenterList = $this->lobjDocumentverificationmodel->fnGetExamCenterList();
	
		$lobjsearchform->field8->addMultiOption('','Select'); 
		$lobjsearchform->field8->addMultiOptions($lobjExamCenterList);
		
		$larrresult = $this->lobjDocumentverificationmodel->fnGetStudentDetails(); //get user details
		
		 if(!$this->_getParam('Search')) 
			unset($this->gobjsessionstudent->semesterpaginatorresult);
		
		$lintpagecount =10000000;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
	 		
  		
		
		
  		$this->view->countcomp = count($larrresult);
  		$this->view->studentdetails = $larrresult;
  		
  		
	    if ($this->_request->isPost() && $this->_request->getPost('Approve')) {
			$lobjFormData = $this->_request->getPost();
			$larrresult = $examcentermodel->fnStudentApproved($lobjFormData);
			$this->_redirect($this->baseUrl . 'examination/centerapproval/index');
		}
		
		
		
		
	 	if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$lobjFormData = $this->_request->getPost ();
			if ($lobjsearchform->isValid ( $lobjFormData )) {
				$larrresult = $this->lobjDocumentverificationmodel->fngetStudentSearch($lobjFormData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->view->studentdetails = $larrresult;
			}
  		
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/centerapproval/index');
			
		}
	
 }
	public function studentapprovallistAction()
	{
		
 		$lstrType = $this->_getParam('lvaredit');
    	$larrstudentdetails=$this->lobjDocumentverificationmodel->fnGetStudent($lstrType);
    	
    	$this->view->FName =$larrstudentdetails['FName'];
    	$this->view->EmailAddress =$larrstudentdetails['EmailAddress'];
    	$this->view->ProgramName =$larrstudentdetails['ProgramName'];
    	
    	$this->view->StateName =$larrstudentdetails['StateName'];
    	$this->view->CityName =$larrstudentdetails['CityName'];
    	$this->view->centername =$larrstudentdetails['centername'];
    	//$this->view->studentdetails = $larrstudentdetails;
    	
   	}
		
}