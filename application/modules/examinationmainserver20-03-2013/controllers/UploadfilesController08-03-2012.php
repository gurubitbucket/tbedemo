<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_UploadfilesController extends Base_Base {

	private $lobjuploadfilesModel;
	private $lobjloadfilesForm;
  	
	/*
	 * initialization function	
	 */
	public function init() {	
		
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);   	   
	}
	public function fnsetObj(){
				$this->lobjbatchmasterModel = new Examination_Model_Batchmaster();
		$this->lobjuploadfilesModel = new Examination_Model_Uploadfiles();
		$this->lobjloadfilesForm = new Examination_Form_Uploadfiles(); //intialize user lobjuniversityForm
	}
	/*
	 *  search form & grid display  
	 */
public function indexAction() {
		$this->view->id = $this->_getParam('id'); 
 		$lobjsearchform = new App_Form_Search();  //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjuserForm object to the view
				
		//$this->view->roles = $this->gobjroles; //Send roles to the view
				
		$lobjUploadfilesForm =  $this->lobjloadfilesForm;
		$lobjUploadfilesmodel =  $this->lobjuploadfilesModel; //bank model object
		$larrresult = $lobjUploadfilesmodel->fnGetQuestionDetails(); // get bank details
		$larr= $lobjUploadfilesmodel->fnGetcorrectQuestionDetails(); // get bank details
	//echo "<pre/>";
	//print_r($larrresult);
	//print_r($larr);
 //die();
	//$this->view->correct=$larr;
		
	
	$z=0;
$NewArray = array();
foreach($larrresult as $value) {
	
    $NewArray[] = array_merge($value,array($larr[$z]['corect']));
    $z++;
  //  $newArray[$i] = $value;
 // $newArray[$i][] = $larr['corect'][$i];
 // $i++;
	
}
	//echo "<pre/>";
	//print_r($NewArray);
	//print_r($larr);
	//die();
           
       

        $lobjDefinitionTypeModel = new App_Model_Definitiontype();	// model object		
	    $larrSection = $lobjDefinitionTypeModel->fnGetDefinationMs('Section');
		
		$this->view->form->field1->addMultiOptions($larrSection);	
		
		
		//$lobjDefinitionTypeModel = new App_Model_Definitiontype();	// model object		
	    $larrquestiongroup = $lobjUploadfilesmodel->fnGetQuestionGroup();
		
		$this->view->form->field8->addMultiOptions($larrquestiongroup);
		
			    $larrquestionchapter = $lobjUploadfilesmodel->fnGetChapterGroup();
		
		$this->view->form->field15->addMultiOptions($larrquestionchapter);

		
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->uploadfilespaginatorresult);
			
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1);
		if(isset($this->gobjsessionsis->uploadfilespaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->uploadfilespaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($NewArray,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->view->form->isValid ( $larrformData )) {
				$larrresult = $lobjUploadfilesmodel->fnSearchQuestion($lobjsearchform->getValues()); //searching the values for the bank
					$larr= $lobjUploadfilesmodel->fncorrectQuestion($lobjsearchform->getValues()); // get bank details
		
				$z=0;
$NewArray = array();
foreach($larrresult as $value) {
    $NewArray[] = array_merge($value,array($larr[$z]['corect']));
    $z++;
  //  $newArray[$i] = $value;
 // $newArray[$i][] = $larr['corect'][$i];
 // $i++;
	
}
					//echo "<pre/>";
	//print_r($larrresult);
	//print_r($larr);
	//die();
	
				$this->view->paginator = $this->lobjCommon->fnPagination($NewArray,$lintpage,$lintpagecount);
				$this->gobjsessionsis->uploadfilespaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/uploadfiles/index');
			//$this->_redirect($this->view->url(array('module'=>'examination' ,'controller'=>'uploadfiles', 'action'=>'index'),'default',true));
		}
	}  	
	/*
	 * create new course
	 */
  	public function newfilesAction() {
       	$this->view->title="Add New Program";	
       	
		$this->view->from     = 0;
		$this->view->editId   = 0;
		if($this->_getParam('from'))	$this->view->from     = 1;	
		if($this->_getParam('editId'))	$this->view->editId   = $this->_getParam('editId');			
  		//$this->_helper->layout->disableLayout();  // disable layout 
		$lobjUploadfilesmodel =  $this->lobjuploadfilesModel;
 		$lobjUploadfilesForm = $this->lobjloadfilesForm;//intialize bank form
		$this->view->lobjUploadfilesForm = $lobjUploadfilesForm; //send the lobjuserForm object to the view
		
		$lobjDefinitionTypeModel = new App_Model_Definitiontype();	// model object		
		$larrSection = $lobjDefinitionTypeModel->fnGetDefinationMs('Section');
		
		 $this->view->lobjUploadfilesForm->Section->addMultiOptions($larrSection);
		//print_r($larrSection);
		//die(); 	
  		$auth = Zend_Auth::getInstance();
  		$iduser = $auth->getIdentity()->iduser;

		$ldtsystemDate = date('Y-m-d H:i:s');
        $this->view->lobjUploadfilesForm->UpdDate->setValue($ldtsystemDate);
        $this->view->lobjUploadfilesForm->UpdUser->setValue($iduser);

        // save opearation
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost(); //getting the values of bank from post 
			//
			//print_r($larrformData);die();
			
			require_once 'Excel/excel_reader2.php';
				if ($lobjUploadfilesForm->isValid($larrformData)) {
					unset($larrformData['Save']);	

					$lintfilecount['Count'] =0;
					$lstruploaddir = "/uploads/questions/";
					$larrformData['FileLocation'] = $lstruploaddir;
					$larrformData['UploadDate'] = $larrformData['UpdDate'];
					
				if($_FILES['FileName']['error'] != UPLOAD_ERR_NO_FILE)
				{
					$lintfilecount['Count']++; 
					//$larrext = explode(".", basename($_FILES['uploadedfiles']['name'][$linti]));
					$lstrfilename = pathinfo(basename($_FILES['FileName']['name']), PATHINFO_FILENAME);					
					$lstrext = pathinfo(basename($_FILES['FileName']['name']), PATHINFO_EXTENSION);
					
					$filename = $lintfilecount['Count'].".".date('YmdHis').".".$lstrext;
				    $filename = str_replace(' ','_',$lstrfilename)."_".$filename;				 
					$file = realpath('.').$lstruploaddir . $filename;					
					if (move_uploaded_file($_FILES['FileName']['tmp_name'],$file)) 
					{
						//echo "success";
						
							$larrformData['FilePath'] = $filename;
							$larrData['FileName'] =  $lstrfilename;
							$larrData['FilePath'] = $filename;
							$larrData['UpdDate'] =  $larrformData['UpdDate'];
							$larrData['UpdUser'] =  $larrformData['UpdUser'];
							$larrData['Active'] = $larrformData['Active'];
						//$filesData .=	"<tr><td align='center'>".($linti+1)."</td><td>".$filename."</td><td>".$filenames[$linti]['size']."</td></tr>";
					} 
					else 
					{
			    		//echo "error";
					}
				}
				$larrformData['Name']=$lstrfilename;
				$section = $larrformData['Section'];
				$lobjRead =  $this->lobjuploadfilesModel; 
				 $larrresultdetails = $lobjRead->fnDeleteTempDetails();
				/* echo "<pre/>";
				 print_r($larrData);
				 die();
				 */
				$lintresult = $lobjUploadfilesmodel->fnAddUploadfiles($larrData);
					
					
				$userDoc = realpath(APPLICATION_PATH . '/../public/uploads/questions/'.$filename);
			    $data = new Spreadsheet_Excel_Reader($userDoc);
			 
			    
     		    $arr = $data->sheets;
       		 /* print_r($data->sheets);
			    die()*/;
       		  ///////////////////////////////////////////////////
					
       		$min = (array_keys($arr[0]['cells']));
       		
       		$firstrowvalue = $min[1];
       		//print_r($firstrowvalue);die();
       		$cnt = max(array_keys($arr[0]['cells']));
//////////////////////////////////function to set the questionand answer//////////////////////////////////////////
				$firstvalue = count($arr[0]['cells'][1]);
$maxfirstvalue = max(array_keys($arr[0]['cells'][1]));

for($i=1;$i<=$maxfirstvalue;$i++)
{
	if(($arr[0]['cells'][1][$i]) == 'No')
	{
	     $var = $i;
	}
	
}
//print_r($questionandanswer);//
//die();
$No =$var;
$questionandanswer = $var+1;	

$correctanswer = $var+2;
$field1 = $var+3;
$field2 = $var+4;
$field3 = $var+5;
$field4 = $var+6;
$field5 = $var+7;
$malay = $var+8;
//print_r($field1);
//die();
//////////////////////////////////end of the function////////////////////////////
//$section = 143;
$firstrowmaxvalue = max(array_keys($arr[0]['cells'][$firstrowvalue]));
$answercount =5;
$Updateanswer =1;
$qflag = 0;
$temptable=1;
$questionArr = array(); 
//for reading from first row to end of the xl
        for($i=$firstrowvalue;$i<=$cnt;$i++)
        {
        	 /*     $tamil = $arr[0]['cells'][$i][$tamil];
        		$arabic = $arr[0]['cells'][$i][$arabic];*/
        	$flagsss=0;
            $fieldone = $arr[0]['cells'][$i][$field1];
            $fieldtwo = $arr[0]['cells'][$i][$field2];
            $fieldthree = $arr[0]['cells'][$i][$field3];
            $fieldfour = $arr[0]['cells'][$i][$field4];
            $fieldfive = $arr[0]['cells'][$i][$field5];
            
           // print_r(($arr[0]['cells'][6]));
           //die();
        	//if array has question
        	
            
            
            $larrquestion = $arr[0]['cells'][$i][$questionandanswer];
            $larrquestionfromedb = $lobjRead->fnGetAllQuestion();
           
            for($k=0;$k<count($larrquestionfromedb);$k++)
            {
            	$questionArr[$k]  = $larrquestionfromedb[$k]['Question'];            	
            }
            if(in_array($larrquestion,$questionArr)){ 
            	$flagsss = 1;
            	echo '<script language="javascript">alert("Some questions are duplicated")</script>';
           			 $qflag = 1;
            }
            
          // echo $flagsss."<br>";
            if($flagsss == 0)
           {
        	if(($arr[0]['cells'][$i][$No]) == 'Q')
        	{
        		/*$tamilquestion = $arr[0]['cells'][$i][$tamil];
        		$arabicquestion = $arr[0]['cells'][$i][$arabic];*/
        		$malayquestion = $arr[0]['cells'][$i][$malay];
	        		/////////////////////function for getting the Levels and Group Name////////////
					for($j=1;$j<=$firstrowmaxvalue;$j++)
					{
						$questiongroup =($arr[0]['cells'][$i][1]);
						
						if((($arr[0]['cells'][$i][$j]) == 'Easy') || (($arr[0]['cells'][$i][$j]) == 'Medium') ||
						(($arr[0]['cells'][$i][$j]) == 'Difficult'))
						{
							
						    Break;
						}
						
						elseif(empty($arr[0]['cells'][$i][$j]))
						{
							//echo"asdf";
							 Break;
						}
						else 
						{
							//echo "sdfas";
							
							$index=($arr[0]['cells'][$i][$j]);
							if(empty($concat))
							{
								$concat = $index;
							}
							else
						    $concat = $concat.'-'.$index;
						}
					}
					///////////////////////End of function//////////////////////////////////////

				if(empty($questiongroup))
				{
				     $temptable=0;
					$larrresult = $lobjRead->fnGetQuestion($lintidquestion);
	        	 	$larrquestionalert = $larrresult['Question'];
					echo '<script language="javascript">alert("After this Question'.$larrquestionalert.' is not having any  part So Not able to upload the document completely from this part")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/examination/uploadfiles';</script>";
        		    // $this->_redirect( $this->baseUrl . '/examination/uploadfiles/index');
        		    die();
        		  
				}
				/*print_r($questiongroup);
			     die();*/
				$questionnumber = $concat;
        		/////////////////////////
        		$questionlevel = $No-1;
        		$questiontypes = ($arr[0]['cells'][$i][$questionlevel]);
	        	//var_dump($questiontypes);die();
	        	if($questiontypes == 'Easy')
	        	{
	        	         $QuestionLevel =1;
	        	}
	        	elseif($questiontypes == 'Medium')
	        	{
	        	          $QuestionLevel = 2;
	        	}
	        	elseif($questiontypes == 'Difficult') 
	        	{  
	        	          $QuestionLevel = 3;
	        	}
	        	else  
	        				$QuestionLevel = 4;
/*print_R($arr[0]['cells'][$i][$questionandanswer]);
die();*/
	        	          
	        	 if($Updateanswer==1 && $QuestionLevel <=3)
	        	 {
	        	 /*	$tamilquestion = $arr[0]['cells'][$i][$tamil];
        			$arabiquestion = $arr[0]['cells'][$i][$arabic];*/
        			$malayquestion = $arr[0]['cells'][$i][$malay];
        		      $larrresult = $lobjRead->fninsertquestion($arr[0]['cells'][$i][$questionandanswer],$section,$QuestionLevel,$questionnumber,$questiongroup,$fieldone,$fieldtwo,$fieldthree,$fieldfour,$fieldfive,$malayquestion);
                      $lintidquestion = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_questions','idquestions');
                      $qflag = 0;
	        	 }
	        	 else 
	        	 {
	        	 	$temptable=0;
	        	 	$larrresult = $lobjRead->fnGetQuestion($lintidquestion);
	        	 	$larrquestionalert = $larrresult['Question'];
	        	 	if($Updateanswer==0)
	        	 	{
	        	 		echo '<script language="javascript">alert("Before This question'.$larrquestionalert.'question will not the the correct answer")</script>';
	        	 	}
	        	 	else 
	        	 	{
	        	 		echo '<script language="javascript">alert("After This question'.$larrquestionalert.'does not have the proper question type")</script>';
	        	 	}
	        	 	
	        	 	echo "<script>parent.location = '".$this->view->baseUrl()."/examination/uploadfiles';</script>";
	        	 	die();
	        	    
	        	 }
                $concat = '';
                $answercount = 1;
                $Updateanswer =0;
        	 }
        	 else if($arr[0]['cells'][$i][$No] == 'A' && $qflag == 0)
        	 {
        		if($answercount<=4 )
        		{
        			echo "<pre/>";
        			
        			
        			
        			/*$tamilanswers = $arr[0]['cells'][$i][$tamil];
        			$arabicanswers = $arr[0]['cells'][$i][$arabic];*/
        			$malayanswers = $arr[0]['cells'][$i][$malay];
	        		 $larrresultanswer= $lobjRead->fninsertanswers($arr[0]['cells'][$i][$questionandanswer],$lintidquestion,$malayanswers);
	        		 $answercount++;         		
        		}        		
        		else 
        		{
        			 $temptable=0;
        			 echo '<script language="javascript">alert("This question'.$larrquestionalert.'has more choices So Not able to upload the document completely from this part")</script>';
        			 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/uploadfiles';</script>";
        		}
        		  
        		 if($arr[0]['cells'][$i][$correctanswer] == 'Ans')   
        		 {        		 		
	        		 $larrresultanswers = $lobjRead->fnupdateanswerkey($larrresultanswer);
	        		 $Updateanswer =1;         		 	
        		 }   
        	  } 
        	
           }     	
        }	    
  ////////////////////
    $larrresulttempquestions = $lobjRead->fnGetAllQuestionFromTemp();
    $larrresulttempanswers = $lobjRead->fnGetAllAnswersFromTemp();

    $larrresult = $lobjRead->fninsertmain($larrresulttempquestions,$larrresulttempanswers);
   
    $larrresultdetails = $lobjRead->fnDeleteTempDetails();
					
			}//inserting
			$this->_redirect( $this->baseUrl . '/examination/uploadfiles/index');
			//$this->_redirect($this->view->url(array('module'=>'examination' ,'controller'=>'uploadfiles', 'action'=>'index'),'default',true));
			 //echo "<script>parent.location = '".$this->view->baseUrl()."/uploadfiles/index';</script>";
		}
		
	}
        
public function editquestionAction()
{$Questionid = $this->_getParam('Questionid');
	$lobjUploadfilesmodel =  $this->lobjuploadfilesModel;
 	$lobjUploadfilesForm = $this->lobjloadfilesForm;//intialize bank form
	$this->view->lobjUploadfilesForm = $lobjUploadfilesForm; 
	$lobjDefinitionTypeModel = new App_Model_Definitiontype();	// model object		
	$larrSection = $lobjDefinitionTypeModel->fnGetDefinationMs('Section');
	$this->view->lobjUploadfilesForm->Section->addMultiOptions($larrSection);
	$lobjBatchmasterModel = $this->lobjbatchmasterModel;	// model object	
	$larrDropdownValues = $lobjBatchmasterModel->fnGetparts();  // get all 'Room Type' Definitions from DMS Table 
	$this->view->lobjUploadfilesForm->QuestionGroup->addMultiOptions($larrDropdownValues);
		 
	$questionresult = $lobjUploadfilesmodel->fnGetQtnsDetails($Questionid);
	//echo "<pre />";
	//print_r($questionresult);
	//die();
	$quesidchecked = $lobjUploadfilesmodel->fnCheckQtnsDetails($Questionid);
	$flag3=0;
	if($quesidchecked)
	{
		$flag3=1;
		//$this->view->questionthere=$quesidchecked;
		//echo '<script language="javascript">alert("This Question is referred by Students");</script>';   
				
	}
	
		$this->view->flag3=$flag3;
		
	//print_r($quesidchecked);die();
	

	$this->view->lobjUploadfilesForm->Questionid->setValue($Questionid);
	$this->view->Questions=$questionresult['Question'];
	$this->view->lobjUploadfilesForm->QuestionGroup->setValue($questionresult['QuestionGroup']);
	$this->view->lobjUploadfilesForm->QuestionLevel->setValue($questionresult['QuestionLevel']);
	$this->view->lobjUploadfilesForm->AnswerStyle->setValue($questionresult['AnswerStyle']);
	$this->view->lobjUploadfilesForm->QuestionNumber->setValue($questionresult['QuestionNumber']);	
	$this->view->lobjUploadfilesForm->Section->setValue($questionresult['QuestionType']);
	$this->view->lobjUploadfilesForm->Field1->setValue($questionresult['Field1']);
	$this->view->lobjUploadfilesForm->Field2->setValue($questionresult['Field2']);
	$this->view->lobjUploadfilesForm->Field3->setValue($questionresult['Field3']);
	$this->view->lobjUploadfilesForm->Field4->setValue($questionresult['Field4']);
	$this->view->lobjUploadfilesForm->Field5->setValue($questionresult['Field5']);	
	$this->view->lobjUploadfilesForm->Active->setValue($questionresult['Active']);	
	$this->view->MalayQuestion=$questionresult['Malay'];
	
	
	
	$larranswerresult = $lobjUploadfilesmodel->fnGetAnswerforQtn($Questionid);
	//echo "<pre/>";
	//	print_r($questionresult);
	//die();
   for($i=0;$i<count($larranswerresult);$i++)	
   {
   	$idanswers[]=$larranswerresult[$i]['idanswers'];
   	if( $larranswerresult[$i]['CorrectAnswer']==1)
     $k=$i+1;
   }
   
   $this->view->cortanswerss=$k;
   	$a=0;
   if($questionresult['Active']==1)
   {
   	$a=1;
   }
   	$this->view->active=$a;
 //  print_r($idanswers);
   //	die();
	//echo $k;die();
	$this->view->lobjUploadfilesForm->Answers1->setValue($larranswerresult[0]['answers']);
	$this->view->lobjUploadfilesForm->Answers2->setValue($larranswerresult[1]['answers']);
	$this->view->lobjUploadfilesForm->Answers3->setValue($larranswerresult[2]['answers']);
	$this->view->lobjUploadfilesForm->Answers4->setValue($larranswerresult[3]['answers']);	
	$this->view->lobjUploadfilesForm->Hiddenidanswer1->setValue($larranswerresult[0]['idanswers']);
	$this->view->lobjUploadfilesForm->Hiddenidanswer2->setValue($larranswerresult[1]['idanswers']);
	$this->view->lobjUploadfilesForm->Hiddenidanswer3->setValue($larranswerresult[2]['idanswers']);
	$this->view->lobjUploadfilesForm->Hiddenidanswer4->setValue($larranswerresult[3]['idanswers']);
	$this->view->lobjUploadfilesForm->MalayAnswers1->setValue($larranswerresult[0]['Malay']);
	$this->view->lobjUploadfilesForm->MalayAnswers2->setValue($larranswerresult[1]['Malay']);
	$this->view->lobjUploadfilesForm->MalayAnswers3->setValue($larranswerresult[2]['Malay']);
	$this->view->lobjUploadfilesForm->MalayAnswers4->setValue($larranswerresult[3]['Malay']);
	$this->view->lobjUploadfilesForm->CorrectAnswer->setValue($k);
	
	/*echo "<pre/>";
	print_r($larranswerresult);
	die();*/
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost();
	
	$idanswer3=$larrformData['CorrectAnswer']-1;
		//echo "<pre/>";
	//print_r($larrformData);
	//echo $idanswer3;
	//print_r($idanswers[$idanswer3]);
	//die();
	        $crranswer=$idanswers[$idanswer3];
			$idquestion = $larrformData['Questionid'];
			
			$result = $lobjUploadfilesmodel->fnUpdateQutns($larrformData,$idquestion);
			$result33 = $lobjUploadfilesmodel->fnUpdateQutnstochange($idquestion,$crranswer);
		   //$result44 = $lobjUploadfilesmodel->fnUpdateanswertocrrct($crranswer);
			$this->_redirect( $this->baseUrl . '/examination/uploadfiles/index');
	}
}

public function addquestionAction()
{
	$lobjUploadfilesmodel =  $this->lobjuploadfilesModel;
 	$lobjUploadfilesForm = $this->lobjloadfilesForm;//intialize bank form
	$this->view->lobjUploadfilesForm = $lobjUploadfilesForm; 
	$lobjDefinitionTypeModel = new App_Model_Definitiontype();	// model object		
	$larrSection = $lobjDefinitionTypeModel->fnGetDefinationMs('Section');
	$this->view->lobjUploadfilesForm->Section->addMultiOptions($larrSection);
	$lobjBatchmasterModel = $this->lobjbatchmasterModel;	// model object	
	$larrDropdownValues = $lobjBatchmasterModel->fnGetparts();  // get all 'Room Type' Definitions from DMS Table 
	$this->view->lobjUploadfilesForm->QuestionGroup->addMultiOptions($larrDropdownValues);
    if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost();
/*		echo "<pre/>";
	print_r($larrformData);
	die();*/
	$larrresult = $lobjUploadfilesmodel->fnAddSingleQuestion($larrformData);
	
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/uploadfiles/index';</script>";		
	}
	
	
}
}