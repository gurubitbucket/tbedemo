<?php
class Examination_GroupController extends Base_Base 
{
	private $lobjGroup; //db variable
	private $lobjGroupform;//Form variable
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjGroup = new Examination_Model_Groupmodel(); //intialize user db object
		$this->lobjGroupform = new Examination_Form_Group(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	//function to set and display the result
	public function indexAction() 
	{    	 
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view
		$larrresult = $this->lobjGroup->fngetgrouplist(); //get businesstype details
			
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->Businesstypepaginatorresult); // clear the search session
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->Businesstypepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->Businesstypepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		// check if the user has clicked the search
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) 
		{
			    $larrformData = $this->_request->getPost ();			    									   
				unset ( $larrformData ['Search'] );
				$larrresult = $this->lobjGroup->fnSearchgroup($larrformData); //searching the values for the businesstype
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->Businesstypepaginatorresult = $larrresult;
			
		}
		// check if the user has clicked the clear
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) 
		{
			 $this->_redirect( $this->baseUrl . '/examination/group/index');			
		}
		
	}
	
	//function to add a businesstype
	public function addAction() 
	{   
		$this->view->lobjGroupform = $this->lobjGroupform; //send the form to the view			
		// check if the user has clicked the save
		if ($this->_request->isPost() && $this->_request->getPost('Save'))
		{
			$larrformData = $this->_request->getPost();	
			unset ( $larrformData ['Save'] );
			$this->lobjGroup->fnaddgroup($larrformData);
			$this->_redirect( $this->baseUrl . '/examination/group/index'); //after adding Redirect to index page				
        }     
    }
    
    //function to update the businesstype
	public function editAction()
	{   	
		$this->view->lobjGroupform = $this->lobjGroupform; //send the Form the view
		$id=$this->_Getparam("id"); // get the id of the group to be updated
		$larrresult = $this->lobjGroup->fneditgroup($id);	// get the details from the DB into array variable	
		$this->view->lobjGroupform->populate($larrresult); // populate the result in the edit form
			
	    // check if the user has clicked the save
    	if ($this->_request->isPost() && $this->_request->getPost('Save'))
    	 {       	 			 	
    		$larrformData = $this->_request->getPost();	 // get the form data  		    		    	
	   		$lintId = $id;	//store the id to be updated    		
	   		unset ($larrformData ['Save']);	 //unset the save data in form data  
    	 $result25=$this->lobjGroup->getactivesets();
	     	
	       
			$values=0;
			for($i=0;$i<count($result25);$i++)
			{
				$value=$result25[$i]['IdTOS'];
				$values=$values.','.$value;
			}
			$resultgroup=$this->lobjGroup->getgroupsection($larrresult['idgroupname']);
			//echo "<pre />";
	   		//print_r($resultgroup[0]['QuestionGroup']);
	   		//die();
	   		$groupname=$resultgroup[0]['QuestionGroup'];
	   		$result99=$this->lobjGroup->gettossection($values,$groupname);
	   		
	   		if($result99 && $larrformData['Active']==0)
	   		{
	   		echo '<script language="javascript">alert("The system cannot allow to remove this question group as its  reffered by active set");</script>';   	
			echo "<script>parent.location = '".$this->view->baseUrl()."/examination/group/index';</script>";
                	die();
	   			
	   		}
	   		else 
	   		{
	   				$this->lobjGroup->fnupdategroup($lintId,$larrformData); //update businesstype
			$this->_redirect( $this->baseUrl . '/examination/group/index'); //redirect to index page after updating		
	   		}
							
    	}
	  }
}