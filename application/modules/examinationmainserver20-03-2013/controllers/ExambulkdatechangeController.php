<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_ExambulkdatechangeController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	public function fnsetObj()
	{	
		$this->lobjnewscreenmodel = new Examination_Model_Exambulkdatechange(); //intialize newscreen db object
		$this->lobjnewscreenForm = new Examination_Form_Exambulkdatechange(); 
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object

       //$this->lobjstudentForm = new App_Form_Studentapplication();
	
	
	}
	
	public function indexAction() 
	{
		
		
		$this->view->lobjform = $this->lobjnewscreenForm;
		
	$larrresultmaximum = $this->lobjnewscreenmodel->fngetstudentinformationfromconfig(); 
	//	echo "<pre>";
		//print_r($larrresultmaximum['studedit']);
	//	die();
		
				$larrresult = $this->lobjnewscreenmodel->fngetstudentinformation($larrresultmaximum['studedit']); 
				$larrresult=0;
		//print_r($larrresult);die();
		
	if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->studentchangevenuepaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		
		$larrStudentnameresult = $this->lobjnewscreenmodel->fnGetStudentNames($larrresultmaximum['studedit']);	
		$this->lobjnewscreenForm->Studentname->addMultiOptions($larrStudentnameresult);
		
		
			$larrCourseresult = $this->lobjnewscreenmodel->fnGetCourseNames();	
		$this->lobjnewscreenForm->Coursename->addMultiOptions($larrCourseresult);
		$larrVenuesresult = $this->lobjnewscreenmodel->fnGetVenueNames();	
		$this->lobjnewscreenForm->Venues->addMultiOptions($larrVenuesresult);
		$larrTakafulresult = $this->lobjnewscreenmodel->fnGetTakafulNames();	
		$this->lobjnewscreenForm->Takafulname->addMultiOptions($larrTakafulresult);
		
		if(isset($this->gobjsessionsis->studentchangevenuepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->studentchangevenuepaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$this->view->paramsearch =  $this->_getParam('search');
				
				
				
			if($larrformData['Date'])
					{
					$this->lobjnewscreenForm->Date->setValue($larrformData['Date']);
					}
					if($larrformData['Coursename'])
					{
					//$larrCourseresult = $this->lobjnewscreenmodel->fnGetCourseNames();	
		          //  $this->lobjnewscreenForm->Coursename->addMultiOptions($larrCourseresult);
					$this->lobjnewscreenForm->Coursename->setValue($larrformData['Coursename']);
					}
					if($larrformData['Venues'])
					{
					$larrVenuesresult = $this->lobjnewscreenmodel->fnGetschdelerdates2($larrformData['Date'],$larrformData['Coursename']);	
		           $this->lobjnewscreenForm->Venues->addMultiOptions($larrVenuesresult);
					$this->lobjnewscreenForm->Venues->setValue($larrformData['Venues']);
					}
			if($larrformData['newvenues'])
					{
					$larrVenuesresult = $this->lobjnewscreenmodel->fngetschedulerofsessionstudent($larrformData['Date'],$larrformData['Coursename']);	
		           $this->lobjnewscreenForm->newvenues->addMultiOptions($larrVenuesresult);
					$this->lobjnewscreenForm->newvenues->setValue($larrformData['newvenues']);
					}
					
				 $larrresult = $this->lobjnewscreenmodel->fnSearchStudent($larrformData,$larrresultmaximum['studedit']); //searching the values for the user
				$this->view->larrresult =$larrresult;
				$this->view->countcomp = count($larrresult);
				 $this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->studentchangevenuepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/examination/exambulkdatechange/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Change' )) {
			$larrformData = $this->_request->getPost ();
//print_r($larrformData);	die();	
		}
		
		
		
		
	}
	
	public function editstudentinfoAction() 
	{	
			
		
		if ($this->_request->isPost() && $this->_request->getPost('Yes')) {
			$larrformData = $this->_request->getPost();
			
			
				$auth = Zend_Auth::getInstance();
				$iduser=$auth->getIdentity()->iduser;
				
				
				
				
			
	$larrstudentresult=$this->lobjnewscreenmodel->newfnstudentbulkinfos($larrformData['IDApplication']);
	
	$totalseat = count($larrstudentresult);
	
	
		$larrresultsprog=$this->lobjnewscreenmodel->fnGetProgAmount($larrstudentresult[0]['Program']);
	
			$oldprgamount=$larrresultsprog['sum(abc.amount)'];
			
			$programflag=0;
			
			$amountflag=0;
			
			
			if($larrformData['setmonth']<10)
				{
				$months='0'.$larrformData['setmonth'];	
				}
				else 
				{
				$months=$larrformData['setmonth'];		
				}
		if($larrformData['setdate']<10)
				{
			$dates='0'.$larrformData['setdate'];	
				}
				else 
				{
					$dates=$larrformData['setdate'];	
				}
		
				$selecteddate=$larrformData['year'].'-'.$months.'-'.$dates;
			
				
					if(($selecteddate==$larrstudentresult[0]['DateTime'])&&($larrformData['newexamcity']==$larrstudentresult[0]['Examvenue']) &&($larrformData['idsession']==$larrstudentresult[0]['Examsession']))
				{
					//echo 'abc';die();
					$this->_redirect( $this->baseUrl . '/examination/exambulkdatechange/index');		
					die();
				}	
			
			$idsecheduler=$this->lobjnewscreenmodel->fnGetidschdeler($selecteddate,$larrformData['newexamcity'],$larrformData['idsession']);	
		if($totalseat > $idsecheduler['rem']){
				echo '<script language="javascript">alert("The No of Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
					echo "<script>parent.location = '".$this->view->baseUrl()."/examination/exambulkdatechange/index';</script>";
				exit;
			}
				
			if($idsecheduler)
			{
			$idsche=$idsecheduler['idnewscheduler']	;
		
			
			$larrformData['UpdDate']=date('Y-m-d H:i:s');
			//$idapplication = $larrformData['IDApplication'];
			
			
			
			//$studenteditresult = $this->lobjnewscreenmodel->fngetstudentoldinfo($idapplication);
			
	//$validateallotedseats=$this->lobjnewscreenmodel->fngetstudentvalidatealloted($larrstudentresult[0]['DateTime'],$larrstudentresult[0]['Examvenue'],$larrstudentresult[0]['Examsession']);
			
			
		   
			
			$studentseatdecrement=$this->lobjnewscreenmodel->fngetstudentbulkdecrease($larrstudentresult[0]['DateTime'],$larrstudentresult[0]['Examvenue'],$larrstudentresult[0]['Examsession'],$totalseat);
		
			
			
			
			
			
			
			
			$change=$larrstudentresult['Venue'];
			$venuechange=$change+1;
		
			$studentgetcityandstate=$this->lobjnewscreenmodel->fnGetvenuecity($larrformData['newexamcity']);
			$idstate=$studentgetcityandstate['state'];
			$idcity=$studentgetcityandstate['city'];
		
	
			
			$insertingintotemp = $this->lobjnewscreenmodel->fninserttempvenuechange($larrstudentresult,$iduser,$oldprgamount,$larrformData,$selecteddate,$idcity,$idstate,$idsche);
			
			
  			 	//$this->lobjnewscreenmodel->fnUpdateStudentnewscreenbulk($larrstudentresult,$idapplication,$larrformData,$selecteddate,$idsche,$iduser,$idstate,$idcity);
  		
  			 	   //$studenteditresultss = $this->lobjnewscreenmodel->newfnstudentbulkinfos($larrformData['IDApplication']);
                $studentseatincrement=$this->lobjnewscreenmodel->fngetstudentbulkincrease($selecteddate,$larrformData['newexamcity'],$larrformData['idsession'],$totalseat);
  			 	





	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    						
     for($i=0;$i<$totalseat;$i++)
     {
     	
     	$idapplication = $larrstudentresult[$i]['IDApplication'];
      $larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($idapplication);	
					$larrregid = $this->lobjstudentmodel->fngetRegid($idapplication);
			
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Venue Change");
						
						
						$larrStudentMailingDetails = $larrresult;	
									//print_R($larrresult);die();
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										 $lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'].'  '.$larrresult['addr2'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
									$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'].'('.$larrresult['ampmstart'].'--'.$larrresult['ampmend'].')',$lstrEmailTemplateBody);
	                                  if($larrresult['batchpayment']!=0)
										{
											$lstrEmailTemplateBody = str_replace("[Amount]",'NA',$lstrEmailTemplateBody);
										}
										else 
										{
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										}
									
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
									
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'tbe@ibfim.com', 'password' => 'ibfim2oi2');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'tbe@ibfim.com';
										$sender = 'tbe';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
								//$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
								
										
								} catch (Exception $e) {
									
								
								}
     	
     }
				

  			 		$this->_redirect( $this->baseUrl . '/examination/exambulkdatechange/index');	
  			 		

			} 		
  			else 
			{
			echo '<script language="javascript">alert("There is no scheduler defined for this venue for this date");</script>';   
		//	echo "<script>parent.location = '".$this->view->baseUrl()."/examination/newscreen/index';</script>";
 		//die();
			
			}
			
  		//echo "<pre>";
  		//print_r($larrformData);
  		//die();
  		}
		
		
		
		
		
  		
  		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Change' )) {
			$larrformData = $this->_request->getPost ();
//unset($larrformData['change'])
//echo count($larrformData['IDApplication']);die();	
				
			if($larrformData['IDApplication']=='')
			{
				echo '<script language="javascript">alert("Please check atleast one candidate")</script>';	
					echo "<script>parent.location = '".$this->view->baseUrl()."/examination/exambulkdatechange/index';</script>";
				exit;
			}
			
			$values=0;
for($i=0;$i<count($larrformData['IDApplication']);$i++)
		{
	
			
			$value=$larrformData['IDApplication'][$i];
				$values=$values.','.$value;
				
			$this->view->idapplicationvalues=$values;
		
		}
	//	echo $values;die();
		}
		
		
		$larrstudentresult=$this->lobjnewscreenmodel->newfnstudentinfos($values);
		
			$this->view->lobjnewscreenForm = $this->lobjnewscreenForm;
		
		$this->view->idprograms=$larrstudentresult['Program'];
			$larrnewfngetyear = $this->lobjnewscreenmodel->newfngetyear($larrstudentresult['Program']);
			//print_r($larrnewfngetyear);die();
		$this->lobjnewscreenForm->year->addMultiOptions($larrnewfngetyear);
		$this->view->lobjnewscreenForm->year->setValue($larrstudentresult['yearss']);
		
		
		$larrvenue = $this->lobjnewscreenmodel->newfnGetCitylistforcourse($larrstudentresult['Program'],$larrstudentresult['yearss']);
		$this->lobjnewscreenForm->newexamcity->addMultiOptions($larrvenue);
		$this->view->lobjnewscreenForm->newexamcity->setValue($larrstudentresult['Examvenue']);
		
		

		
		
	}
	
	
	
	
public function newfnnewcaleshowAction()
{
	    $this->_helper->layout->disableLayout();
	    $Program = $this->_getParam('Program');
	    $year = $this->_getParam('year');
	    $NewVenue = $this->_getParam('NewCity');
	
	    
	     $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);	
    	$db_link  = mysql_connect("localhost", "ibfim","1bf1m") or die('Cannot connect to the DB');
		mysql_select_db("backupclienttbe",$db_link) or die('Cannot select the DB');
	    
	     
		//$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');	
    	//$db_link  = mysql_connect($config->resources->db->params->host, $config->resources->db->params->username, $config->resources->db->params->password) or die('Cannot connect to the DB');
		//mysql_select_db($config->resources->db->params->dbname,$db_link) or die('Cannot select the DB');
		
		
			
		//$monthquery = "SELECT DATE_FORMAT( date,  '%m' ) AS monthk FROM  `tbl_venuedateschedule` where idvenue = '$NewVenue' AND year(date) = '$year' GROUP BY monthk";
       $monthquery = "SELECT DATE_FORMAT( date,  '%m' ) AS monthk FROM  `tbl_venuedateschedule` as a,tbl_newschedulercourse as b,tbl_newscheduler c where a.idvenue = '$NewVenue' AND year(a.date) = '$year' AND b.idnewscheduler=c.idnewscheduler and b.idnewscheduler=a.idnewscheduler and c.Active=1 AND a.Reserveflag=1 and a.Active=1 AND a.centeractive=1 and b.IdProgramMaster='$Program'  and  a.date >curdate()  GROUP BY monthk";
		//echo $monthquery;die();
		
		$resultmonth = mysql_query($monthquery,$db_link) or die('cannot get results!');
    	while($rowsk = mysql_fetch_assoc($resultmonth)) {
		  		$monthk['monthk'][]=  $rowsk;

			}
			
		foreach($monthk as $monthk)

		$monthcount = count($monthk); 

		echo "<table ><tr>";
		for($month=0;$month<$monthcount;$month++){
		   	$monthp =  $monthk[$month]['monthk'];

if($monthp==01 || $monthp==1)
		   	{
		   		 $monthname='January';
		   	}
		if($monthp==02 || $monthp==2 )
		   	{
		   		 $monthname='Feburary';
		   	}
		if($monthp==03 || $monthp==3 )
		   	{
		   		 $monthname='March';
		   	}
		   	if($monthp==04 || $monthp==4)
		   	{
		   		 $monthname='April';
		   	}
		if($monthp==05 || $monthp==5)
		   	{
		   		 $monthname='May';
		   	}
		if($monthp==06 || $monthp==6 )
		   	{
		   		 $monthname='June';
		   	}
		if($monthp==07 || $monthp==7 )
		   	{
		   		 $monthname='July';
		   	}
		if($monthp==08 || $monthp==8)
		   	{
		   		 $monthname='August';
		   	}
		if($monthp==09 || $monthp==9 )
		   	{
		   		 $monthname='September';
		   	}
		if($monthp==10)
		   	{
		   		 $monthname='October';
		   	}
		if($monthp==11)
		   	{
		   		 $monthname='November';
		   	}
		if($monthp==12)
		   	{
		   		 $monthname='December';
		   	}
		   	
		$events = array();
		//$query = "SELECT idvenuedateschedule,idsession AS title, DATE_FORMAT(date,'%Y-%m-%d') AS event_date FROM tbl_venuedateschedule WHERE date LIKE '$year-$monthp%' AND Active='1' AND idvenue = '$NewVenue' ";
 	$query = "SELECT idvenuedateschedule,idsession AS title, DATE_FORMAT(date,'%Y-%m-%d') AS event_date FROM tbl_venuedateschedule as a,tbl_newschedulercourse as b,tbl_newscheduler c  WHERE a.date LIKE '$year-$monthp%' AND a.Active='1' AND a.Reserveflag=1  AND a.centeractive=1  AND a.idvenue = '$NewVenue' and b.idnewscheduler=c.idnewscheduler and b.idnewscheduler=a.idnewscheduler and c.Active=1 and b.IdProgramMaster='$Program'  and  a.date >curdate() ";
	
		$result = mysql_query($query,$db_link) or die('cannot get results!');
		while($row = mysql_fetch_assoc($result)) {
		  	$events[$row['event_date']][]=  $row;
		}
			
		if($month%3 == 1  && $month !=1)echo "<tr>";
			echo "<td  style='border : 1px solid black;' cellpadding='10px' align = 'center' valign = 'top'>";
			//echo date('F', mktime(0,0,0,$monthp)).$year;
                                                          echo $monthname.$year;
			echo self::draw_calendar($monthp,$year,$events,$Program,$NewVenue);
			echo "</td>";
			if($month%3 == 0 && $month !=0 ) echo "</tr>";
		}
		//echo $query;die();
		echo "</table>";die();
	}
	
function draw_calendar($month,$year,$events = array(),$Program,$NewVenue)
{


		/* draw table */
		$calendar = '<table width="80%" border="1" align="center" style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
		/* table headings */
		$headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
		$calendar.= '<tr><td class="calendar-day-head" >'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';
			
		 /* days and weeks vars now ... */
		$running_day = date('w',mktime(0,0,0,$month,1,$year));
		$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
		$days_in_this_week = 1;
		$day_counter = 0; 
		$dates_array = array();
		/* row for week one */
		$calendar.= '<tr class="calendar-row">';
		/* print "blank" days until the first of the current week */
		//echo "<pre>";
		for($x = 0; $x < $running_day; $x++):
		$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
		$days_in_this_week++;
		endfor;
			  
		//$event_day = $year.'-'.$month.�-�.$list_day;
		/* keep going with days.... */
		for($list_day = 1; $list_day <= $days_in_month; $list_day++):
			$calendar.= '<td class="calendar-day">';
			/* add in the day number */
			if($list_day < 10) {
				$list_day = str_pad($list_day, 2, '0', STR_PAD_LEFT);
			}
			//$calendar.= $list_day;
			$event_day = $year.'-'.$month.'-'.$list_day;

	
 			if(isset($events[$event_day])) {
						
				foreach($events[$event_day] as $event) {
					
			    	$event1[$event['event_date']]  = $event['title'];
			    	
			     }

	
			     $mnc = "#FFFFFF";
			      if(count($events[$event_day]) == 1){
			       	$mnc = "green";
			      }
			      elseif(count($events[$event_day]) == 2){
			      	$mnc = "skyblue";
			      }
			      if(count($events[$event_day]) == 3){
			      	$mnc = "violet";
			      }elseif(count($events[$event_day]) == 4) {
			      	$mnc = "pink";
			      }
			      $calendar.= '<a href="#" ><div style = "background-color:'.$mnc.'; font-color:'.$mnc.'; width :30px; cursor:pointer; text-align:center; font-size:10pt;  border : 1px solid black;" " onclick="functp('.$list_day.','.$month.');">'.$list_day.'</div></a> ';
			   } else {
			     	$mnc = "#f6f6f6";
			      	 $calendar.= '<div style = "background-color:'.$mnc.';font-size:10pt;" >'.$list_day.'</div>';
			        //$calendar.= str_repeat('<p>&nbsp;</p>',2);
			   }
			   $calendar.= '</td>';
			   if($running_day == 6):
			      $calendar.= '</tr>';
			      if(($day_counter+1) != $days_in_month):
			      		$calendar.= '<tr class="calendar-row">';
			     endif;
			     $running_day = -1;
			     $days_in_this_week = 0;
			    endif;
			    $days_in_this_week++; $running_day++; $day_counter++;
			  	endfor;
			
			  /* finish the rest of the days in the week */
			  if($days_in_this_week < 8):
			    for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			      if($days_in_this_week != 1) $calendar.= '<td class="calendar-day-np">&nbsp;</td>';
			    endfor;
			  endif;
			
			  /* final row */
			  $calendar.= '</tr>';
			  /* end the table */
			  $calendar.= '</table><br>';
			
			  /** DEBUG **/
			  //$calendar = str_replace('</td>','</td>'."\n",$calendar);
			 // $calendar = str_replace('</tr>','</tr>'."\n",$calendar);
			  
			  /* all done, return result */
			  return $calendar;
}
	
	
	
	
public function newfngetyearofsessionAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();


		$Program = $this->_getParam('Program');
		$Year = $this->_getParam('Year');

      $Newvenue = $this->_getParam('NewCity');
      
      
      
		$larrsessiontimeresults = $this->lobjnewscreenmodel->fnGetSessionforthesechedulers($Program,$Year,$Newvenue);
			
		//$larrexectdaysresults = $this->lobjnewscreenmodel->fnGetExectSessiondays($values);
		//print_r($larrexectdaysresults);die();
		//$i=0;
			foreach($larrsessiontimeresults as $days)
			{
			$sessioncount[]=$days['total'];	
			//echo count($larrexectsessiontimeresults);die();
			//$i++;
			}
		
			$maximumsesiion=max($sessioncount);
		//print_r($maximumsesiion);die();
		echo $maximumsesiion;die();
		
		//$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}
	
	
	
	
	
	
	public function newtempdaysAction() 
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day = $this->_getParam('day');
		$year = $this->_getParam('year');
		$month = $this->_getParam('month');
		$venue = $this->_getParam('city');
		if($month <10){
			$month = '0'.$month;
		}
		if($day <10){
			$day = '0'.$day;
		}
		$selecteddate = $year.'-'.$month.'-'.$day;
		$noofseats = $this->lobjnewscreenmodel->fnvalidateseats($venue,$selecteddate);
		
		

		$flag=0;
		foreach($noofseats as $ven){
		if($ven['Allotedseats']<$ven['Totalcapacity']) {
			$flag=1;
		}
		}
		if($flag==1){
			$flag=1;
		}
		echo $flag;
	}
	
	
	
	
public function selectvenueAction(){
		
			$this->_helper->layout->disableLayout();
				$this->view->lobjnewscreenForm = $this->lobjnewscreenForm;
		$day = $this->_getParam('day');
		$month = $this->_getParam('month');
		$year = $this->_getParam('year');
		$venue = $this->_getParam('city');
		if($month <10){
			$month = '0'.$month;
		}
		if($day <10){
			$day = '0'.$day;
		}
		 $selecteddate = $year.'-'.$month.'-'.$day;

		$studentapp = new App_Model_Studentapplication();
		$sessresult = $studentapp->fnGetvenuedatescheduleDetails($selecteddate,$venue);
		$this->view->sessresult = $sessresult;
		$this->view->regdate=$selecteddate;
		
			$result5 = $this->lobjnewscreenmodel->fngetdayStudent($selecteddate); 
		$this->view->daystu= $result5[0]['days'];
	}
	
	
	
	
	
	
	public function newvenuelistsnamesAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$idprg = $this->_getParam('idprog');
	
		
		
		
		
		
			
		$larrschedulerdays=$this->lobjnewscreenmodel->fnGetschdelerdates2($iddate,$idprg);
	   	
	   	if($larrschedulerdays)
		{
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdays);
		}
		else 
		{
		$larrschedulerdays="";
		}
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}
	
	
	public function newsessionlistsnamesAction()
	{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$iddate = $this->_getParam('iddate');
		$idprog = $this->_getParam('idprog');
		
		
			$larrschedulerdayssession=$this->lobjnewscreenmodel->fngetschedulerofsessionstudent($iddate,$idprog);
		
		if($larrschedulerdayssession)
		{
		$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdayssession);
		}
		else 
		{
		$larrCountrysessionDetailss='';	
		}
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
	}

	public function newfngetcitynamesAction()
	{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Get Country Id
		$Program = $this->_getParam('Program');
		$year = $this->_getParam('year');
		//$idseched = $this->_getParam('idsecheduler');
   $curmonth=date('m');
      
		if($curmonth<10)
		{
			$curmonth = $curmonth[1];
		}
	
		$larrvenuetimeresults = $this->lobjnewscreenmodel->newfnGetCitylistforcourse($Program,$year);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
	}

	
}