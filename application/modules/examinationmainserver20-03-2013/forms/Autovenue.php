<?php
	class Examination_Form_Autovenue extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
		
		$month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
	  $yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 

		

         	    
	            $Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						//->setAttrib('constraints', "$dateofbirth")
						->setAttrib('onChange', "newsessionlists();")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
	            $FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
	        	$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');							
						
	            $ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
	        	$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');						
			  
				$Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');
			    $Venue->setAttrib('dojoType',"dijit.form.FilteringSelect");
			   // $Venue->setAttrib('required',"true");
	            $Venue->removeDecorator("DtDdWrapper");
	           // $Venue->setAttrib('onChange', "newsessionlists();");
	            $Venue->removeDecorator("Label");
	            $Venue->removeDecorator('HtmlTag');
	            
	            $Course = new Zend_Dojo_Form_Element_FilteringSelect('Course');
			    $Course->setAttrib('dojoType',"dijit.form.FilteringSelect");
	          //  $Course->setAttrib('required',"true"); 	           	         		       		     
	            $Course->removeDecorator("DtDdWrapper");
	            $Course->removeDecorator("Label");
	            $Course->removeDecorator('HtmlTag');
	            
	              $Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search for Venue");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag');
         		
        $Clear = new Zend_Form_Element_Button('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');    

	    $Type= new Zend_Dojo_Form_Element_FilteringSelect('Type');
        $Type	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('0'=>'Select','1'=>'Push Candidates','2'=>'Questions','3'=>'Scheduler'))
							->setRegisterInArrayValidator(false)
							->setAttrib('onChange','fnshowdate(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');




$Schedulerdate = new Zend_Dojo_Form_Element_DateTextBox('Schedulerdate');
		$Schedulerdate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$Schedulerdate->setAttrib('title',"dd-mm-yyyy");
		$Schedulerdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Schedulerdate->setAttrib('constraints', "$dateofbirth");
		$Schedulerdate->removeDecorator("Label");
		$Schedulerdate->removeDecorator("DtDdWrapper");
		$Schedulerdate->removeDecorator('HtmlTag');							

		
		$ScheduleStartTime = new Zend_Form_Element_Text('ScheduleStartTime');
        $ScheduleStartTime->removeDecorator("DtDdWrapper"); 
        $ScheduleStartTime->removeDecorator("Label");
        $ScheduleStartTime->removeDecorator('HtmlTag');
		$ScheduleStartTime->setAttrib('dojoType',"dijit.form.TimeTextBox")
					->setAttrib('constraints',"{timePattern: 'HH:mm'}");
			$this->addElements(
        					array($Date,
        					      $Venue,
        					      $Course,$Search,$Clear,$Type,$ToDate,$FromDate,$Schedulerdate,$ScheduleStartTime
        			
        						)
        			);
		}
}
