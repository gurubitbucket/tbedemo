<?php
	class Examination_Form_Manualmarksentry extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
	    
	 $month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
	           

	            $Result = new Zend_Dojo_Form_Element_FilteringSelect('Result');
			    $Result->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Result->addMultiOptions(array(''=>'Select','3'=>'Applied','1'=>'Pass','2'=>'Fail')); 	           	         		       		     
	            $Result->removeDecorator("DtDdWrapper");
	            $Result->removeDecorator("Label");
	            $Result->removeDecorator('HtmlTag');  
	            
	            $ICNO = new Zend_Form_Element_Text('ICNO');
			    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $ICNO->setAttrib('class','txt_put');  
	            $ICNO->removeDecorator("DtDdWrapper");
	            $ICNO->removeDecorator("Label");
	            $ICNO->removeDecorator('HtmlTag');
	            
	              
	            $Examdate = new Zend_Form_Element_Text('Examdate');
			    $Examdate->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $Examdate->setAttrib('class','txt_put');  
	            $Examdate->removeDecorator("DtDdWrapper");
	            $Examdate->removeDecorator("Label");
	            $Examdate->removeDecorator('HtmlTag');
	            
	            $Studentname = new Zend_Dojo_Form_Element_FilteringSelect('Studentname');
			    $Studentname->setAttrib('dojoType',"dijit.form.FilteringSelect");
			    $Studentname->addMultiOption('','Select'); 
	            $Studentname->setAttrib('class','txt_put');  
	            $Studentname->removeDecorator("DtDdWrapper");
	            $Studentname->removeDecorator("Label");
	            $Studentname->removeDecorator('HtmlTag');
	            
	            
	            $newexamdates = new Zend_Form_Element_Text('newexamdates');
			    $newexamdates->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $newexamdates->setAttrib('class','txt_put')
	               -> setAttrib('required',"true"); 
	            $newexamdates->removeDecorator("DtDdWrapper");
	            $newexamdates->removeDecorator("Label");
	            $newexamdates->removeDecorator('HtmlTag');
	            
	            
	            
	            
	                     
	            $Time = new Zend_Form_Element_Text('examtime');
			    $Time->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $Time->setAttrib('class','txt_put')
	              ->setAttrib('OnBlur','fnvalidatetime(this.value)')
	              -> setAttrib('required',"true"); 
	            $Time->removeDecorator("DtDdWrapper");
	            $Time->removeDecorator("Label");
	            $Time->removeDecorator('HtmlTag');
	            
	            
	          /*  $newexamdates = new Zend_Dojo_Form_Element_DateTextBox('newexamdates');
	        	$newexamdates->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');*/
	            
				$Dates = new Zend_Dojo_Form_Element_DateTextBox('Dates');
	        	$Dates->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
	        		        ->setAttrib('onChange','newvenuelists(this.value);')
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							 ->setAttrib('constraints', "$dateofbirth")	
						->removeDecorator("Label")
						  -> setAttrib('required',"true")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$Dates2 = new Zend_Dojo_Form_Element_DateTextBox('Dates2');
	        	$Dates2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
									
		       $UpDate1 = new Zend_Dojo_Form_Element_DateTextBox('UpDate1');
	           $UpDate1->setAttrib('dojoType',"dijit.form.DateTextBox")
	               ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('UpDate2').constraints.min = arguments[0];")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
	        
			  		   
			  	$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Date2').constraints.min = arguments[0];")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	        	$Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->setAttrib('onChange', "dijit.byId('Date').constraints.max = arguments[0];")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
								   
			  /*$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	        	$Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');*/
				/*$UpDate1 = new Zend_Dojo_Form_Element_DateTextBox('UpDate1');
	        	$UpDate1->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$UpDate2 = new Zend_Dojo_Form_Element_DateTextBox('UpDate2');
	        	$UpDate2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');*/
					
				$Date4 = new Zend_Dojo_Form_Element_DateTextBox('Date4');
	        	$Date4->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
							
							
							
		        $Date3 = new Zend_Dojo_Form_Element_DateTextBox('Date3');
	        	$Date3->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	        	
	            $Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
			    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Venues->addMultiOption('','Select')
	             ->setAttrib('OnChange','newsessionlists(this.value)');
	            $Venues->removeDecorator("DtDdWrapper");
	            $Venues->removeDecorator("Label");
	            $Venues->removeDecorator('HtmlTag');
	            
	           
	            
	            
	            	
	            $examstate = new Zend_Dojo_Form_Element_FilteringSelect('examstate');
			    $examstate->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $examstate->addMultiOption('','Select');  
	            $examstate->removeDecorator("DtDdWrapper");
	            $examstate->removeDecorator("Label");
	            $examstate->removeDecorator('HtmlTag');
	            
	            
	            	
	            $examcity = new Zend_Dojo_Form_Element_FilteringSelect('examcity');
			    $examcity->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $examcity->addMultiOption('','Select');  
	            $examcity->removeDecorator("DtDdWrapper");
	            $examcity->removeDecorator("Label");
	            $examcity->removeDecorator('HtmlTag');
	            
	             $examsession = new Zend_Dojo_Form_Element_FilteringSelect('examsession');
			    $examsession->setAttrib('dojoType',"dijit.form.FilteringSelect");
			     // ->setAttrib('OnChange','emptysession(this.value)');
	           // $examsession->addMultiOption('','Select');  
	            $examsession->removeDecorator("DtDdWrapper");
	            $examsession->removeDecorator("Label");
	            $examsession->removeDecorator('HtmlTag');
			
	            
	            $newexamstate = new Zend_Dojo_Form_Element_FilteringSelect('newexamstate');
			    $newexamstate->setAttrib('dojoType',"dijit.form.FilteringSelect")
			     ->setAttrib('OnChange','fnGetCitylist(this.value)')
			        -> setAttrib('required',"true");
	           // $newexamstate->addMultiOption('','Select');  
	            $newexamstate->removeDecorator("DtDdWrapper");
	            $newexamstate->removeDecorator("Label");
	            $newexamstate->removeDecorator('HtmlTag');
	            
	            
	            	
	            $newexamcity = new Zend_Dojo_Form_Element_FilteringSelect('newexamcity');
			    $newexamcity->setAttrib('dojoType',"dijit.form.FilteringSelect")
			    ->setAttrib('OnChange','newsessionlists(this.value)');
	            $newexamcity-> setAttrib('required',"true");
	            $newexamcity->removeDecorator("DtDdWrapper");
	            $newexamcity->removeDecorator("Label");
	            $newexamcity->removeDecorator('HtmlTag');
	            
	            
	             $Venues1 = new Zend_Dojo_Form_Element_FilteringSelect('Newexamvenue');
			    $Venues1->setAttrib('dojoType',"dijit.form.FilteringSelect")
			    ->setAttrib('OnChange','fnGetVenuesessionname(this.value)');
	            $Venues1  -> setAttrib('required',"true");  
	            $Venues1->removeDecorator("DtDdWrapper");
	            $Venues1->removeDecorator("Label");
	            $Venues1->removeDecorator('HtmlTag');
			
	            
	            $newexamsession = new Zend_Dojo_Form_Element_FilteringSelect('newexamsession');
			    $newexamsession->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $newexamsession -> setAttrib('required',"true");  
	            $newexamsession->removeDecorator("DtDdWrapper");
	            $newexamsession->removeDecorator("Label");
	            $newexamsession->removeDecorator('HtmlTag');
	            
	            
	            $newexammonth = new Zend_Dojo_Form_Element_FilteringSelect('newexammonth');
			    $newexammonth->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $newexammonth->setAttrib('OnChange','fnShowCalendarnew(this.value)')
	            -> setAttrib('required',"true");  
	            $newexammonth->removeDecorator("DtDdWrapper");
	            $newexammonth->removeDecorator("Label");
	            $newexammonth->removeDecorator('HtmlTag');
	            
				$FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
	        	$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	       	 	$FromDate->removeDecorator("DtDdWrapper")
						 ->setAttrib('title',"dd-mm-yyyy")
	       				 ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	        	$FromDate->removeDecorator("Label");
	        	$FromDate->removeDecorator('HtmlTag'); 
        				
			  
				$ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
	        	$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	       	 	$ToDate->removeDecorator("DtDdWrapper")
						->setAttrib('title',"dd-mm-yyyy")
	       				 ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");;
	        	$ToDate->removeDecorator("Label");
	        	$ToDate->removeDecorator('HtmlTag'); 
         	 
    	   	
				$schedulerename = new Zend_Dojo_Form_Element_FilteringSelect('schedulerename');
			    $schedulerename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $schedulerename->removeDecorator("DtDdWrapper");
	            $schedulerename->removeDecorator("Label");
	            $schedulerename->removeDecorator('HtmlTag');   
			
	            
	            $schedulerename1 = new Zend_Dojo_Form_Element_FilteringSelect('schedulerename1');
			    $schedulerename1->setAttrib('dojoType',"dijit.form.FilteringSelect")
			    ->setAttrib('onChange','newcitylistss(this.value);');
	            $schedulerename1->removeDecorator("DtDdWrapper");
	            $schedulerename1->removeDecorator("Label");
	            $schedulerename1->removeDecorator('HtmlTag'); 
	            
	            $Clear = new Zend_Form_Element_Submit('Clear');
        		$Clear->dojotype="dijit.form.Button";
        		$Clear->label = $gstrtranslate->_("Clear");
				$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
				$search = new Zend_Form_Element_Submit('Search');
        		$search->dojotype="dijit.form.Button";
        		$search->label = $gstrtranslate->_("Search");
        		$search->removeDecorator("DtDdWrapper");
       			$search->removeDecorator("Label");
        		$search->removeDecorator('HtmlTag')
         		       ->class = "NormalBtn";
         		       
         	$Clear = new Zend_Form_Element_Submit('Clear');
        		$Clear->dojotype="dijit.form.Button";
        		$Clear->removeDecorator("DtDdWrapper");
        		$Clear->removeDecorator("Label");
        		$Clear->removeDecorator('HtmlTag');
        		$Clear->class = "NormalBtn";
        		$Clear->label = $gstrtranslate->_("Clear"); 
         		       
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->setAttrib('onClick','return validateform();');
        $Save->dojotype="dijit.form.Button";
        $Save->label =("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";	
         		
         		     		
        $passwd = new Zend_Form_Element_Password('passwd');	
        $passwd->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $passwd->setAttrib('required',"true") ;
        $passwd->removeDecorator("DtDdWrapper");
        $passwd->removeDecorator("Label");
        $passwd->removeDecorator('HtmlTag');	
        
           $ExamDatetime = new Zend_Dojo_Form_Element_DateTextBox('ExamDatetime');
	       $ExamDatetime->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
	        		   
	        		      -> setAttrib('required',"true")
	        		    ->setAttrib('onChange','newvenuelists(this.value);')
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")	
						 ->setAttrib('constraints', "$dateofbirth")	
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
         		
			$this->addElements(
        					array(
        					      $schedulerename,
        					      $schedulerename1,
        					      $newexamdates,
        						  $ICNO,
        						  $Time,
        					      $Studentname,
        					      $Venues,
        					      $Venues1,
        					      $newexamsession,
        					      $examsession,
        					      $Date,
        					      $Date2,
        					      $examstate,
        					      $newexammonth,
        					      $examcity,
        					      $Examdate,
        					      $newexamstate,
        					      $newexamcity,
        					      $Result,
        					      $FromDate,
        					      $ToDate,
        						  $Date3,$Date4,
        						  $UpDate1,
        						  $Dates,
        						  $Dates2,
        						  $Clear,
        						  $search,
        						  $passwd,
        						  $Clear,
        						  $ExamDatetime,
        						  $Save
        						
        						)
        			);
		}
}
