<?php
class Examination_Form_Createexam extends Zend_Dojo_Form  {
    public function init() {

    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
        $Batch = new Zend_Form_Element_Select('Batch');
        $Batch->setAttrib('maxlength','250')
        	->setAttrib('dojoType',"dijit.form.FilteringSelect");
        $Batch->removeDecorator("DtDdWrapper");
        $Batch->setAttrib('required',"true") ;
        $Batch->removeDecorator("Label");
        $Batch->removeDecorator('HtmlTag');
        $Batch->setAttrib('OnChange', 'fnUpdateData(this.value)');        
        $Batch->setRegisterInArrayValidator(false);
				
        
        $BatchFrom = new Zend_Form_Element_Text('BatchFrom');
        $BatchFrom->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $BatchFrom->setAttrib('maxlength','100'); 
        $BatchFrom->setAttrib('readonly','readonly')
        	->setAttrib('required',"true");                                 
        $BatchFrom->removeDecorator("DtDdWrapper");
        $BatchFrom->removeDecorator("Label");
        $BatchFrom->removeDecorator('HtmlTag');
        
        $BatchTo = new Zend_Form_Element_Text('BatchTo');
        $BatchTo->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $BatchTo->setAttrib('maxlength','100');
	    $BatchTo->setAttrib('readonly','readonly')
	    	->setAttrib('required',"true");                                   
        $BatchTo->removeDecorator("DtDdWrapper");
        $BatchTo->removeDecorator("Label");
        $BatchTo->removeDecorator('HtmlTag');
        
        $NoOfQuestion = new Zend_Form_Element_Text('NoOfQuestion',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
        $NoOfQuestion->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $NoOfQuestion->setAttrib('maxlength','100');                                 
        $NoOfQuestion->removeDecorator("DtDdWrapper")
        	->setAttrib('required',"true")
        	->setAttrib('onChange', 'getnewtree(this.value)')
        	->setAttrib('OnBlur', 'fnCheckAvailability(this.value)');
        	
        $NoOfQuestion->removeDecorator("Label");
        $NoOfQuestion->removeDecorator('HtmlTag');
        
        $ReqTime = new Zend_Form_Element_Text('ReqTime',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
        $ReqTime->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $ReqTime->setAttrib('maxlength','100');                                 
        $ReqTime->removeDecorator("DtDdWrapper")
        	->setAttrib('required',"true");
        $ReqTime->removeDecorator("Label");
        $ReqTime->removeDecorator('HtmlTag');
        
        $pass = new Zend_Form_Element_Text('Pass',array('regExp'=>"^([0-9]|[0-9]\d|100)$",'invalidMessage'=>"Only Percentage"));
        $pass->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $pass->setAttrib('maxlength','100');                                 
        $pass->removeDecorator("DtDdWrapper")
         ->setAttrib('required',"true");
        $pass->removeDecorator("Label");
        $pass->removeDecorator('HtmlTag');
        
        $Course = new Zend_Form_Element_Text('Course');
        $Course->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Course->setAttrib('readOnly','true');                                 
        $Course->removeDecorator("DtDdWrapper")
               ->setAttrib('required',"true");
        $Course->removeDecorator("Label");
        $Course->removeDecorator('HtmlTag');
        
        $AlertTime = new Zend_Form_Element_Text('AlertTime',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
        $AlertTime->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $AlertTime->setAttrib('maxlength','100');                                 
        $AlertTime->removeDecorator("DtDdWrapper")
        ->setAttrib('required',"true");
        $AlertTime->removeDecorator("Label");
        $AlertTime->removeDecorator('HtmlTag');
        
        $Active = new Zend_Form_Element_Checkbox('Active');
		$Active->setRequired(true)
					->setChecked(0)
					->setAttrib('readOnly','true')
					->removeDecorator("DtDdWrapper")
					->removeDecorator("Label") 					
					->removeDecorator('HtmlTag');
					
					
					
		$Setcode = new Zend_Form_Element_Text('Setcode');
        $Setcode->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Setcode->setAttrib('maxlength','100');                                 
        $Setcode->removeDecorator("DtDdWrapper")
        ->setAttrib('required',"true");
        $Setcode->removeDecorator("Label");
        $Setcode->removeDecorator('HtmlTag');
        
        
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
      
        $Close = new Zend_Form_Element_Button('Close');
        $Close->dojotype="dijit.form.Button";
        $Close->label = $gstrtranslate->_("Close");
        $Close->removeDecorator("DtDdWrapper");
        $Close->removeDecorator("Label");
        $Close->removeDecorator('HtmlTag')
         		->class = "NormalBtn";        
      
			
        $this->addElements(array($UpdDate,$UpdUser,$Active,$Batch,$BatchFrom,$BatchTo,$NoOfQuestion,$ReqTime,$AlertTime,$pass,$Course,$Setcode,
                                $Save,$Close));
    }
}