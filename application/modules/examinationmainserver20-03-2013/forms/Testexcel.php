<?php
class Examination_Form_Testexcel extends Zend_Dojo_Form {
	public function init(){
		$gstrtranslate =Zend_Registry::get('Zend_Translate');

		
		 
		$Upload = new Zend_Form_Element_Submit('Upload');
		$Upload->dojotype="dijit.form.Button";
		$Upload->removeDecorator("DtDdWrapper");
		$Upload->removeDecorator("Label");
		$Upload->removeDecorator('HtmlTag');
		$Upload->class = "NormalBtn";
		$Upload->label = $gstrtranslate->_("Test");
		
		$FileName = new Zend_Form_Element_File('FileName');
		$FileName->setAttrib('dojoType',"dijit.form.TextBox")
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');  
        
		
		$Clear = new Zend_Form_Element_Button('Clear');
		$Clear->dojotype="dijit.form.Button";
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator('HtmlTag');
		$Clear->setAttrib('onclick','fnclear()');
		$Clear->class = "NormalBtn";
		$Clear->label = $gstrtranslate->_("Clear");
		
		$this->addElements(
							array(
									$Upload,$FileName,
									$Clear
							     )
						  );
	}
}
