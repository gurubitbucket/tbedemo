<?php
class Registrations_Model_DbTable_Studentupdate extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	
	  public function fnStudentdetails($idapp)
	  {
	     $db 	= Zend_Db_Table::getDefaultAdapter();	
		 $select = 	$db->select()          
             	 ->from(array("b" =>"tbl_studentapplication"),array("b.*"))
				 ->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
				 ->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
				 ->join(array("r" =>"tbl_newscheduler"),'r.idnewscheduler=b.Year',array("r.Year as Years"))
			     ->join(array("i" =>"tbl_managesession"),'i.idmangesession=b.Examsession',array("i.managesessionname"))				
            	 ->where('b.IDApplication=?',$idapp)  
                 ->where("b.pass=3");
				 
		$result = $db->fetchRow($select);	
		return $result;	
	  }
	  public function fnGetStudentDetails()
	{
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$select = 	$db->select()          
             	
				->join(array("b" =>"tbl_studentapplication"),'b.*')
				->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
				->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
				->join(array("r" =>"tbl_newscheduler"),'r.idnewscheduler=b.Year',array("r.Year as Years"))
			    ->join(array("i" =>"tbl_managesession"),'i.idmangesession=b.Examsession',array("i.managesessionname"))
				//->where("a.Registrationpin=0000000")
                ->where("b.pass=3")
				->order("b.IDApplication desc")
				->group("b.ICNO");					
				//->where("a.Approved=0");	           	       
		$result = $db->fetchAll($select);	
		return $result;		
	}

	  public function fngetStudentSearch($larrpostdata)
	  {
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$select = 	$db->select()          
             	 ->from(array("b" =>"tbl_studentapplication"),array("b.*"))
				->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
				->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
				->join(array("r" =>"tbl_newscheduler"),'r.idnewscheduler=b.Year',array("r.Year as Years"))
			    ->join(array("i" =>"tbl_managesession"),'i.idmangesession=b.Examsession',array("i.managesessionname"))
				//->where("a.Registrationpin=0000000")	
            	->where('b.ICNO like  ? "%"',$larrpostdata['ICNO1']) 
                ->where("b.pass=3")
				 ->order("b.IDApplication desc")
				 ->group("b.ICNO");				
				//->where("a.Approved=0");	
		$result = $db->fetchAll($select);
		return $result;		
	}
	   public  function  fnupdateStudentdetails($name,$icno,$dateofbirth,$idappn)
	   {
	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $table ="tbl_studentapplication";		
          $where = "IDApplication = ".$idappn;	
		  $bind = array(
                         'ICNO'	=>$icno,
                         'FName'=>$name,
						 'DateOfBirth'=>$dateofbirth
						            );
		  $lobjDbAdpt->update($table,$bind,$where);
	   }
	  public  function fncheckduplicateicno($id,$icno)
	   {
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a"=>"tbl_studentapplication"),array("a.*"))					 			 
					 
	 	                         ->where("a.IDApplication !=?",$id)
	 	                         ->where("a.ICNO =?",$icno);
		  
		   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		  
		   return $larrResult;	 
	    }
      
	public function fngetyear()
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_config"),array("a.MinAge as age"))
					   ->where("a.idUniversity=1");
		   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		   return $larrResult;
	}
		
	public function fngetvalidate($age)
	{
		   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		   $lstrSelect = "SELECT DATE_SUB( curdate( ) , INTERVAL $age YEAR ) AS validdate";
		   //echo $lstrSelect;die();
		   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		   return $larrResult;
	}
	
}
