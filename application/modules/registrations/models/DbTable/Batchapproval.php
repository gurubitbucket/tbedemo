<?php
class Registrations_Model_DbTable_Batchapproval extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	
	public function fnGetCompanyDetails()
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
											  ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany')
											  ->join(array("c" =>"tbl_studentpaymentoption"),'c.IDApplication=a.idBatchRegistration')
											  ->where("a.paymentStatus =2")
											  ->where("a.Approved=0")
											  ->where("c.companyflag=1");
											//  ->where("c.Modeofpayment=4");
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
  public function fnGetStudentDetails($regpin)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											  ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
											  ->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
											  ->where("a.RegistrationPin =?",$regpin);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
	
	public function fnBatchApproved($larrformdata)
	{
		/*echo "<pre/>";
		print_r($larrformdata);
		die();*/
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$tableName = "tbl_batchregistration";

	    for($i = 0; $i<count($larrformdata['registrationPin']); $i++ )
			{		
				$idBatchRegistration = $larrformdata['registrationPin'][$i];
				
				$postData = array('Approved' => 1);			
                $where = "registrationPin='$idBatchRegistration'";
				$lobjDbAdpt->update($tableName,$postData,$where);
				self::fnGetStudentapproved($idBatchRegistration);
				//self::sendmailforcompany($idBatchRegistration);
			}
			$result = self::fnGetStudentDetails($larrformdata['registrationPin'][0]);
		return $result;
			
	}
	
	
	public function fnGetStudentapproved($idbatch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											  ->where("a.RegistrationPin =?",$idbatch);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					/*echo"<pre/>";
					print_r(count($larrResult));
					die();*/
	      for($i = 0; $i<count($larrResult); $i++ )
			{		
				$tableName = "tbl_registereddetails";
				$idregistereddetails = $larrResult[$i]['idregistereddetails'];
				$postData = array('Approved' => 1);			
                $where = "idregistereddetails=$idregistereddetails";
				$resutls = $lobjDbAdpt->update($tableName,$postData,$where);
			}
		
		return $resutls;
	}
public function fngetBatchSearch($lobjgetarr){
		//echo "harsha";die();
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$select = 	$db->select()          
             	 ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
				 ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany')
            	 -> where('b.CompanyName like  ? "%"',$lobjgetarr['field3'])
            	 ->where('a.registrationPin like  ? "%"',$lobjgetarr['field4'])
            	 ->where("a.paymentStatus =2")
				 ->where("a.Approved=0");	           	       
		$result = $db->fetchAll($select);	
		return $result;		
	}
  public function sendmailforcompany($idBatchRegistration)
  {
  	
  	    $db 	= Zend_Db_Table::getDefaultAdapter();	
		$select = 	$db->select()          
             	 ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
             	 ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany',array("b.*"))
            	 ->where("a.registrationPin =?",$idBatchRegistration);           	       
		$result = $db->fetchRow($select);	
		
		 $this->lobjstudentmodel = new App_Model_Studentapplication();
		$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Company Batch Approval");
/*		print_r($larrEmailTemplateDesc);
		die();*/
require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');
		$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$lstrEmailTemplateBody = str_replace("[Company]",$result['CompanyName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[RigPin]",$result['registrationPin'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[NoOfStud]",$result['totalNoofCandidates'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										/*print_r($lstrEmailTemplateBody);
										echo '<br/>';
											print_r($result['Email']);
											echo '<br/>';
											print_r($lstrEmailTemplateSubject);
											echo '<br/>';
											print_r($lstrEmailTemplateFrom);
											echo '<br/>';*/
											
										//die();
									
										/*$to 	 = $result['Email'];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $result['Email'];
										$receiver = $result['CompanyName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									       		  ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);

						//echo "asdf";
  }
}
