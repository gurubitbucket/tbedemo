<?php
class Registrations_Model_DbTable_Batchreportmodel extends Zend_Db_Table 
 { 
	public function fnajaxgetcenternames($lintidname)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($lintidname == 1)
		{
		   $lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"CompanyName"))
									->order("a.CompanyName");
		}
		else
		{
		   $lstrSelect = $lobjDbAdpt->select()
				 				 	->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 				 
				 				 	->order("a.TakafulName");
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	}
	public function fnajaxgetpins($lintidname,$lintidcomp)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($lintidname == 1)
		{
           $lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_batchregistration"),array("key"=>"a.registrationPin","value"=>"a.registrationPin"))
									->join(array("b"=>"tbl_studentpaymentoption"),'a.idBatchRegistration= b.IDApplication',array(""))
									->join(array("c"=>"tbl_registereddetails"),'c.RegistrationPin = a.registrationPin',array(""))
									->join(array("d"=>"tbl_studentapplication"),'c.IDApplication= d.IDApplication',array(""))
									->where("d.IDApplication > 1148")
									->where("b.companyflag = 1")
									->where("a.paymentStatus in (2)")
									->where("a.idCompany = $lintidcomp")
									->group("a.registrationPin")
									->order("a.registrationPin");
		}
		else
		{
			$lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_batchregistration"),array("key"=>"a.registrationPin","value"=>"a.registrationPin"))
									->join(array("b"=>"tbl_studentpaymentoption"),'a.idBatchRegistration = b.IDApplication',array(""))
									->join(array("c"=>"tbl_registereddetails"),'c.RegistrationPin = a.registrationPin',array(""))
									->join(array("d"=>"tbl_studentapplication"),'c.IDApplication= d.IDApplication',array(""))
									->where("d.IDApplication > 1148")
									->where("b.companyflag = 2")
									->where("a.paymentStatus in (2)")
									->where("a.idCompany = $lintidcomp")
									->group("a.registrationPin")
									->order("a.registrationPin");
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	}
	public function fngetstuddetails($lintregpin){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect ="select a.IDApplication, a.FName, a.ICNO, cast(datediff(curdate(), a.DateOfBirth)/365 as signed) as age,
			                 DATE_FORMAT(a.DateOfBirth,'%d-%m-%Y') as DateOfBirth, 
			                 b.ProgramName,b.IdProgrammaster,c.centername as ExamVenue, 
			                 concat(f.managesessionname,'(' ,f.starttime, ' - ', f.endtime, ')' ) as session,
			                 DATE_FORMAT(a.DateTime,'%d-%m-%Y') AS ExamDate,
			                  if((a.pass=1),'Pass',if((a.pass=2),'Fail',if((a.pass=3),'Applied','Absent'))) as Result,
			                  if((d.Grade != 'NULL'),d.Grade,'NA') as Grade
					 from 	 tbl_studentapplication a
					         left outer join tbl_registereddetails g on g.IDApplication= a.IDApplication
					         left outer join tbl_programmaster b on a.Program = b.IdProgrammaster
							 left outer join tbl_center c on a.examvenue = c.idcenter	
							 left outer join tbl_managesession f on a.examsession = f.idmangesession
							 left outer join tbl_studentmarks d on a.IDApplication = d.IDApplication  
					 where    g.RegistrationPin = '$lintregpin' and a.IDApplication > 1148
					 order by a.FName";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult; 
	}
	public function fngettakcompname($lintidname,$lintidcomp)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($lintidname == 1)
		{
		   $lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_companies"),array("a.CompanyName"))
									->where("a.IdCompany = $lintidcomp");
		}
		else
		{
		   $lstrSelect = $lobjDbAdpt->select()
				 				 	->from(array("a"=>"tbl_takafuloperator"),array("a.TakafulName AS CompanyName")) 				 
				 				 	->where("a.idtakafuloperator = $lintidcomp");
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	}
 	public function fngetstudmarks($lintidapp)
	{
		$lobjDbAdapt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = "Select a.Correct as Obtained,b.A as PartAmarks, b.B as PartBmarks, b.C as PartCmarks
		               from tbl_studentmarks a, tbl_studentdetailspartwisemarks b 
		               where a.IDApplication = b.IDApplication and a.IDApplication = $lintidapp";
		$larrresult = $lobjDbAdapt->fetchRow($lstrSelect);
		return $larrresult;
	}
}