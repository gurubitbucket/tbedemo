<?php
class Registrations_Model_DbTable_Paymentdistribution extends Zend_Db_Table {	
	public function fnGetOperatorNames($opType,$namestring){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($opType==1){
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("tbl_companies"),array("CompanyName as name"))
			->where('CompanyName like ? "%"',$namestring)
			;
		}else if($opType==2){
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("tbl_takafuloperator"),array("TakafulName as name"))
			->where('TakafulName like ? "%"',$namestring);
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     	
     }
	
 	public function fnSearchpaymentdistribution($opType,$opname){//Function for searching the Company/Takaful Payment Details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($opType==1){//Search for Company 	 
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_invoicegeneration"),array("a.Idinvoice","a.Invoiceuniqueid As invoiceno","a.Flag","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate"))
				-> join(array("d"=>"tbl_invoicedetails"),"d.Idinvoicemaster = a.Idinvoice",array("d.Idinvoicedetails","d.Amount","d.Totalcandidates","d.Regpin"))
				-> join(array("b"=>"tbl_batchregistration"),"b.registrationPin = d.Regpin",array(""))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.Upduser",array("u.fName"))
				-> join(array("c"=>"tbl_companies"),"c.IdCompany = b.idCompany",array("c.CompanyName","c.IdCompany As idoperator"))
				-> joinleft(array("e"=>"tbl_receiptdetails"),'e.BatchId = d.Regpin',array("(e.Amount) As batchamount"))
				-> joinleft(array("f"=>"tbl_receiptmaster"),"f.Idreceipt = e.Idreceipt",array(""))
				-> where('a.Flag = 1')
				->where('c.CompanyName =?',$opname)
				-> order('a.Flag')
				-> order('a.UpdDate');
			$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		}else if($opType==2){//Search for Takaful 	  
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_invoicegeneration"),array("a.Idinvoice","a.Invoiceuniqueid As invoiceno","a.Flag","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate"))
				-> join(array("d"=>"tbl_invoicedetails"),"d.Idinvoicemaster = a.Idinvoice",array("d.Idinvoicedetails","d.Amount","d.Totalcandidates","d.Regpin"))
				-> join(array("b"=>"tbl_batchregistration"),"b.registrationPin = d.Regpin",array(""))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.Upduser",array("u.fName"))
				-> join(array("c"=>"tbl_takafuloperator"),"c.idtakafuloperator = b.idCompany",array("c.TakafulName","c.idtakafuloperator As idoperator"))
				-> joinleft(array("e"=>"tbl_receiptdetails"),'e.BatchId = d.Regpin',array("(e.Amount) As batchamount"))
				-> joinleft(array("f"=>"tbl_receiptmaster"),"f.Idreceipt = e.Idreceipt",array(""))
				-> where('a.Flag = 2')
				->where('c.TakafulName =?',$opname)
				-> order('a.Flag')
				-> order('a.UpdDate');
			$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		}
		return $larrResult; 
	}
	
	
	public function fninsertreceiptdetails($larrformData,$Uniqueid,$companyflag,$idcompany,$upduser){
			//echo "<pre/>";print_r($larrformData);die();
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
            $tablemaster = "tbl_receiptmaster";
            $consdata['operator']=$larrformData['operator'];
			$consdata['operatorname']=$larrformData['operatorname'];
			$consdata['Branchname']=$larrformData['Branchname'];
			$consdata['ChequeDate']=$larrformData['ChequeDate'];
			$consdata['ChequeNumber']=$larrformData['ChequeNumber'];
			$consdata['ReceiptDate']=$larrformData['ReceiptDate'];
			$consdata['PaymentMode']=$larrformData['PaymentMode'];
			$consdata['Amount']=$larrformData['Amount'];
			$consdata['TotalAmountReceived']=$larrformData['TotalAmountReceived'];
			$consdata['totalpayableamount']=$larrformData['totalpayableamount'];
			$consdata['Totaloutstandingamount']=$larrformData['Totaloutstandingamount'];
			
			unset ( $larrformData ['operator'] );
			unset ( $larrformData ['operatorname'] );
			unset ( $larrformData ['Branchname'] );
			unset ( $larrformData ['ChequeDate'] );
			unset ( $larrformData ['ChequeNumber'] );
			unset ( $larrformData ['ReceiptDate'] );
			unset ( $larrformData ['PaymentMode'] );
			unset ( $larrformData ['Amount'] );
			unset ( $larrformData ['TotalAmountReceived'] );
			unset ( $larrformData ['totalpayableamount'] );
			unset ( $larrformData ['Totaloutstandingamount'] );
			unset ( $larrformData ['totalrows'] );
			unset ( $larrformData ['totalpaid'] );
			unset ( $larrformData ['chk'] );
			$postData = array(				    
				'ReceiptNum' =>$Uniqueid,	
	       		'ReceiptDate' =>$consdata['ReceiptDate'],	
	      		'Companyflag' =>$companyflag, 
				'Idcompany' =>$idcompany,	
	         	'Bankbranch' =>$consdata['Branchname'],	
	        	'ChkNum' =>$consdata['ChequeNumber'], 
				'ReceiptType' =>$consdata['PaymentMode'],
	          	'ChkDate' =>$consdata['ChequeDate'],	
	          	'Amount' =>$consdata['TotalAmountReceived'], 
			  	'Active'=>'1',
			 	'Printstatus'=>'1',
				'UpdDate' =>date("Y-m-d"),
			  	'UpdUser'=>$upduser,		    				
			);
			//echo "<pre/>";print_r($larrformData);die();
				
				
				$table = "tbl_receiptdetails";
				$onemaster=1;
				foreach($larrformData as $larrformData){
					if($larrformData['chk']==1){
						if($larrformData['paidamount']>0){
							//insert into tbl_receiptmaster
							if($onemaster==1){
								$lobjDbAdpt->insert($tablemaster,$postData);
								$onemaster++;
								//update receipt number format
								$lastid = $lobjDbAdpt->lastInsertId('tbl_receiptmaster','Idreceipt');
							   	if(strlen($lastid) ==1){$lastidnew = "000".$lastid;}
								else if(strlen($lastid) ==2){$lastidnew = "00".$lastid;}
								else if(strlen($lastid) ==3){$lastidnew = "0".$lastid;}
								else if(strlen($lastid) ==4){$lastidnew = $lastid;}
								$ids=$Uniqueid.$lastidnew;
								$data1 = array ('ReceiptNum'=>$ids);
								$where1 ['Idreceipt = ?'] = $lastid;
								$lobjDbAdpt->update ('tbl_receiptmaster', $data1, $where1 );
							}
							//insert into tbl_receiptdetails
							if($larrformData['paidamount']==$larrformData['outstandingamount']){$active=1;}else{$active=2;}
							$postData = array(				    
									'Idreceipt' =>$lastid,	
		           					'Idinvoicedetails' =>$larrformData['Idinvoicedetails'],	
		           					'BatchId' =>$larrformData['regpin'], 
									'InvoiceNum' =>$larrformData['invoiceno'],	
		           					'Amount' =>$larrformData['paidamount'],	
		           					'Active' =>$active, 	
		           					'UpdDate' =>date("Y-m-d"),
				    				'UpdUser'=>$upduser,
				    	 	);
				    	 	$lobjDbAdpt->insert($table,$postData);
						}
					}
				}
		return $data1;
	}
	
	function fngetamount($regpin){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptdetails"),array("sum(a.Amount) as sum"))
				-> where('a.BatchId ='.$regpin);
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
		
	}
	

	public function fngetreceiptdetails($receiptnum,$compflag){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($compflag == 1)
		{
			$lstrSelect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptmaster"),array("a.Idreceipt","a.ReceiptNum","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate","DATE_FORMAT(a.ReceiptDate,'%d-%m-%Y') As ReceiptDate","a.Bankbranch","a.ChkNum","a.Amount As totalamount","a.Idcompany","a.ReceiptType As PaymentMode"))
				-> join(array("b"=>"tbl_receiptdetails"),"b.Idreceipt = a.Idreceipt",array("b.BatchId","b.InvoiceNum","b.Amount"))
				-> join(array("i"=>"tbl_invoicedetails"),"i.Regpin = b.BatchId",array("i.Amount as batchamount"))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.UpdUser",array("u.fName As Generatedby"))
				-> join(array("c"=>"tbl_companies"),"c.IdCompany = a.Idcompany",array("c.CompanyName As operatorname","c.Email","c.ContactPerson As ContactName","c.Address","c.Postcode","c.Phone1 As workphone","c.Fax"))
				->join(array("e"=>"tbl_businesstype"),"e.idbusinesstype= c.Businesstype",array("e.idbusinesstype","e.BusinessType"))
				->join(array("f"=>"tbl_state"),"f.idState= c.IdState",array("f.StateName"))
             	->join(array("d"=>"tbl_countries"),"d.idCountry= c.IdCountry",array("d.CountryName"))
				-> where("a.ReceiptNum =?",$receiptnum);
		}
		else if($compflag == 2)
		{
			 $lstrSelect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptmaster"),array("a.Idreceipt","a.ReceiptNum","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate","DATE_FORMAT(a.ReceiptDate,'%d-%m-%Y') As ReceiptDate","a.Bankbranch","a.ChkNum","a.Amount As totalamount","a.Idcompany","a.ReceiptType As PaymentMode"))
				-> join(array("b"=>"tbl_receiptdetails"),"b.Idreceipt = a.Idreceipt",array("b.BatchId","b.InvoiceNum","b.Amount"))
				-> join(array("i"=>"tbl_invoicedetails"),"i.Regpin = b.BatchId",array("i.Amount as batchamount"))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.UpdUser",array("u.fName As Generatedby"))
				-> join(array("c"=>"tbl_takafuloperator"),"c.idtakafuloperator = a.Idcompany",array("c.TakafulName As operatorname","c.ContactEmail As Email","c.ContactName","c.ContactCell","c.paddr1 As Address","c.zipCode as Postcode","c.workphone"))
				->join(array("e"=>"tbl_businesstype"),"e.idbusinesstype= c.Businesstype",array("e.idbusinesstype","e.BusinessType"))
				->join(array("f"=>"tbl_state"),"f.idState= c.state",array("f.StateName"))
             	->join(array("d"=>"tbl_countries"),"d.idCountry= c.country",array("d.CountryName"))
				-> where("a.ReceiptNum =?",$receiptnum);								    ;
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
public function fngetreceivermail($idflag)
{
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			if($idflag ==1)
			{
			    $lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_config"),array("a.ClosingBatchCompany as Duration","a.Schedulerpushemail"));
       		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			}
			else if($idflag ==2)
			{
			    $lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_config"),array("a.ClosingBatchTakaful as Duration","a.Schedulerpushemail"));
       		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			}
       		return $larrResult;
}
	

}