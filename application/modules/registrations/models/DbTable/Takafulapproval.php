<?php
class Registrations_Model_DbTable_Takafulapproval extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	
	public function fnGetCompanyDetails()
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
											  ->join(array("b" =>"tbl_takafuloperator"),'a.idCompany=b.idtakafuloperator')
											  ->join(array("c" =>"tbl_studentpaymentoption"),'c.IDApplication=a.idBatchRegistration')
											  ->where("a.paymentStatus =2")
											  ->where("a.Approved=0")
											  ->where("c.companyflag=1")
											  ->where("c.Modeofpayment=4");
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
  public function fnGetStudentDetails($regpin)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											  ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
											  ->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
											  ->where("a.RegistrationPin =?",$regpin);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
	
	public function fnBatchApproved($larrformdata)
	{
		/*echo "<pre/>";
		print_r($larrformdata);
		die();*/
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$tableName = "tbl_batchregistration";

	    for($i = 0; $i<count($larrformdata['registrationPin']); $i++ )
			{		
				$idBatchRegistration = $larrformdata['registrationPin'][$i];
				
				$postData = array('Approved' => 1);			
                $where = "registrationPin=$idBatchRegistration";
				$lobjDbAdpt->update($tableName,$postData,$where);
				self::fnGetStudentapproved($idBatchRegistration);
			}
			
	}
	
	
	public function fnGetStudentapproved($idbatch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											  ->where("a.RegistrationPin =?",$idbatch);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					/*echo"<pre/>";
					print_r(count($larrResult));
					die();*/
	      for($i = 0; $i<count($larrResult); $i++ )
			{		
				$tableName = "tbl_registereddetails";
				$idregistereddetails = $larrResult[$i]['idregistereddetails'];
				$postData = array('Approved' => 1);			
                $where = "idregistereddetails=$idregistereddetails";
				$resutls = $lobjDbAdpt->update($tableName,$postData,$where);
			}
		
		return $resutls;
	}
public function fngetBatchSearch($lobjgetarr){
		//echo "harsha";die();
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$select = 	$db->select()          
             	 ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
				 ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany')
            	 -> where('b.CompanyName like  ? "%"',$lobjgetarr['field3'])
            	 ->where('a.registrationPin like  ? "%"',$lobjgetarr['field4'])
            	 ->where("a.paymentStatus =2")
				 ->where("a.Approved=0");	           	       
		$result = $db->fetchAll($select);	
		return $result;		
	}
   /*public function fnGetcourse()
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
											  ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany')
											  ->join(array("c" =>"tbl_registereddetails"),'a.registrationPin=c.RegistrationPin',array("c.*"))
											  ->join(array("d" =>"tbl_studentapplication"),'c.IDApplication=d.IDApplication')
											  ->join(array("e" =>"tbl_programmaster"),'d.Program=e.IdProgrammaster')
											  ->where("a.paymentStatus =2")
											  ->where("a.Approved=0");
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}*/
}
