<?php
class Registrations_Model_DbTable_Bulkinvoicemodel extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	
	public function fnGetCompanyDetails()
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
											  ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany')
											  ->join(array("c" =>"tbl_studentpaymentoption"),'c.IDApplication=a.idBatchRegistration')
											  ->where("a.paymentStatus =2")
											  ->where("a.Approved=0")
											  ->where("c.companyflag=1");
											//  ->where("c.Modeofpayment=4");
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
  public function fnGetStudentDetails($regpin)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											  ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
											  ->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
											  ->where("a.RegistrationPin =?",$regpin);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
	
	public function fnBatchApproved($larrformdata)
	{
		/*echo "<pre/>";
		print_r($larrformdata);
		die();*/
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$tableName = "tbl_batchregistration";

	    for($i = 0; $i<count($larrformdata['registrationPin']); $i++ )
			{		
				$idBatchRegistration = $larrformdata['registrationPin'][$i];
				
				$postData = array('Approved' => 1);			
                $where = "registrationPin='$idBatchRegistration'";
				$lobjDbAdpt->update($tableName,$postData,$where);
				self::fnGetStudentapproved($idBatchRegistration);
				//self::sendmailforcompany($idBatchRegistration);
			}
			$result = self::fnGetStudentDetails($larrformdata['registrationPin'][0]);
		return $result;
			
	}
	
	
	public function fnGetStudentapproved($idbatch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											  ->where("a.RegistrationPin =?",$idbatch);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					/*echo"<pre/>";
					print_r(count($larrResult));
					die();*/
	      for($i = 0; $i<count($larrResult); $i++ )
			{		
				$tableName = "tbl_registereddetails";
				$idregistereddetails = $larrResult[$i]['idregistereddetails'];
				$postData = array('Approved' => 1);			
                $where = "idregistereddetails=$idregistereddetails";
				$resutls = $lobjDbAdpt->update($tableName,$postData,$where);
			}
		
		return $resutls;
	}
public function fngetBatchSearch($lobjgetarr){
		//echo "harsha";die();
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$select = 	$db->select()          
             	 ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
				 ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany')
            	 -> where('b.CompanyName like  ? "%"',$lobjgetarr['field3'])
            	 ->where('a.registrationPin like  ? "%"',$lobjgetarr['field4'])
            	 ->where("a.paymentStatus =2")
				 ->where("a.Approved=0");	           	       
		$result = $db->fetchAll($select);	
		return $result;		
	}
  public function sendmailforcompany($idBatchRegistration)
  {
  	
  	    $db 	= Zend_Db_Table::getDefaultAdapter();	
		$select = 	$db->select()          
             	 ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
             	 ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany',array("b.*"))
            	 ->where("a.registrationPin =?",$idBatchRegistration);           	       
		$result = $db->fetchRow($select);	
		
		 $this->lobjstudentmodel = new App_Model_Studentapplication();
		$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Company Batch Approval");
/*		print_r($larrEmailTemplateDesc);
		die();*/
require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');
		$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$lstrEmailTemplateBody = str_replace("[Company]",$result['CompanyName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[RigPin]",$result['registrationPin'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[NoOfStud]",$result['totalNoofCandidates'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										/*print_r($lstrEmailTemplateBody);
										echo '<br/>';
											print_r($result['Email']);
											echo '<br/>';
											print_r($lstrEmailTemplateSubject);
											echo '<br/>';
											print_r($lstrEmailTemplateFrom);
											echo '<br/>';*/
											
										//die();
									
										/*$to 	 = $result['Email'];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $result['Email'];
										$receiver = $result['CompanyName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									       		  ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
  }
  
  
  
  //updated for bulkinvoice
  public function fngetpinforbatch($compflag, $idcompany) {


        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        if ($compflag == 1) {
            $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_batchregistration"), array("key" => "a.registrationPin", "value" => "a.registrationPin"))
                    ->join(array("c" => "tbl_studentpaymentoption"), "c.IDApplication=a.idBatchRegistration", array())
                    ->where("a.idCompany =?", $idcompany)
                    ->where("c.companyflag =1")
                    ->where("a.AdhocVenue = 'Blocked' or a.AdhocVenue ='others'")
                    ->where("a.paymentStatus = 2")
                    ->where("c.ModeofPayment =181")
                    ->group("a.registrationPin");
            $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        }
        if ($compflag == 2) {
            $lstrSelect = $lobjDbAdpt->select()
                    ->from(array("a" => "tbl_batchregistration"), array("key" => "a.registrationPin", "value" => "a.registrationPin"))
                    ->join(array("c" => "tbl_studentpaymentoption"), "c.IDApplication=a.idBatchRegistration", array())
                    ->where("a.idCompany =?", $idcompany)
                    ->where("a.AdhocVenue = 'Blocked' or a.AdhocVenue ='others'")
                    ->where("c.companyflag =2")
                    ->where("a.paymentStatus = 2")
                    ->where("c.ModeofPayment =181")
                    ->group("a.registrationPin");
            $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
        }
        return $larrResult;
    }

    public function fnGetOperatorNames($opType,$namestring)
{	
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     			 if($opType==1)
				 {     			 
				 		$lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_companies"),array("CompanyName as name"))							 				
							 				     ->where('CompanyName like ? "%"',$namestring)
							 				   ;
     			 }else if($opType==2)
				 {
     			 	    $lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_takafuloperator"),array("TakafulName as name"))							 				
							 				     ->where('TakafulName like ? "%"',$namestring);
     			 }
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;     	
}
public function fnSearchCompanies($post = array()) 
{
    $db = Zend_Db_Table::getDefaultAdapter();    
    if($post['field19']==1)
	{	 
	$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
			   ->joinleft(array("tbl_companypaymenttype"=>"tbl_companypaymenttype"),"tbl_companies.IdCompany=tbl_companypaymenttype.idcompany",array("tbl_companypaymenttype.*"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_companies.CompanyName like "%" ? "%"',$post['field3'])
			   ->where('tbl_companies.ShortName like  "%" ? "%"',$post['field2']);			  
	$select ->order("tbl_companies.CompanyName");
		
     }
	 else if($post['field19']==2)
	 { 		
     		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator as IdCompany","tbl_takafuloperator.TakafulName as CompanyName","tbl_takafuloperator.TakafulShortName as ShortName","tbl_takafuloperator.paddr1 as Address","tbl_takafuloperator.workphone as Phone1","tbl_takafuloperator.email as Email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.Businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state = tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city = tbl_city.idCity",array("tbl_city.CityName"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_takafuloperator.TakafulName like "%" ? "%"',$post['field3'])
			   ->where('tbl_takafuloperator.TakafulShortName like  "%" ? "%"',$post['field2']);
		  $select ->order("tbl_takafuloperator.TakafulName");
    	 
     }
		$result = $this->fetchAll($select);
		return $result->toArray();
}
public function fngettotalbatchcount($regpins)
{
	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	      $strQuery = $lobjDbAdpt->select()->from(array("a" =>"tbl_batchregistration"),array("sum(a.totalNoofCandidates) as totalNoofCandidates"))
										  				  
										   ->where("a.registrationPin in ($regpins)");
										  // ->group('b.IDApplication');
										 //  echo $strQuery;die();
	      $larrResult = $lobjDbAdpt->fetchRow($strQuery);	
          return $larrResult;			  
}
public function fngettotalapplied($regpins)
{
	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	      $strQuery = $lobjDbAdpt->select()//->from(array("a" =>"tbl_batchregistration"),array("sum(a.totalNoofCandidates) as totalNoofCandidates"))
										   ->from(array("b" =>"tbl_registereddetails"),array(''))
										   ->join(array("c" =>"tbl_studentapplication"),'b.IDApplication = c.IDApplication' ,array('count(distinct c.IDApplication) as Totalappln'))
										   ->where("c.Examvenue!=000")
                                           ->where("b.Approved=1")										   
										   ->where("b.RegistrationPin in ($regpins)");
										  // ->group('b.IDApplication');
										   //echo $strQuery;die();
	      $larrResult = $lobjDbAdpt->fetchRow($strQuery);	
          return $larrResult;			  
}	
public function Fngetactualcandidates($regpins)
{
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_registereddetails"=>"tbl_registereddetails"),array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.Regid"))
					 				 ->join(array("tbl_studentapplication"=>"tbl_studentapplication"),"tbl_registereddetails.IDApplication = tbl_studentapplication.IDApplication",array("CONCAT_WS(' ',IFNULL(tbl_studentapplication.FName,''),IFNULL(tbl_studentapplication.MName,''),IFNULL(tbl_studentapplication.LName,'')) as Candidatename","DATE_FORMAT(tbl_studentapplication.DateOfBirth, '%d-%m-%Y') as DOB","DATE_FORMAT(tbl_studentapplication.DateTime, '%d-%m-%Y') as DateTime","tbl_studentapplication.ICNO","tbl_studentapplication.EmailAddress","tbl_studentapplication.pass"))
					 				 ->where("tbl_registereddetails.RegistrationPin in ($regpins)")
					 				 ->where('tbl_registereddetails.Approved=1')
					 				 ->order('tbl_studentapplication.FName')
					 				 ->group('tbl_registereddetails.IDApplication');									
		$larrResult = $lobjDbAdpt->fetchAll($strQuery);		
        return $larrResult;			
}	
public function gettotalcandidates($regpins)
{
	
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    $lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_batchregistration"),array("sum(a.totalNoofCandidates) as totalNoofCandidates","sum(a.totalAmount) as Totalamount"))
					 // ->join(array("b"=>"tbl_batchregistrationdetails"),"b.idBatchRegistration=a.idBatchRegistration",array(""))
					   ->where("a.registrationPin in ($regpins)");
	    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	    return $larrResult;	
}
public function fngetstudentdetailsforpin($regpins)
{      
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
     	$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("tbl_registereddetails"=>"tbl_registereddetails"),array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.Regid","tbl_registereddetails.RegistrationPin"))
					 				 ->join(array("tbl_studentapplication"=>"tbl_studentapplication"),"tbl_registereddetails.IDApplication = tbl_studentapplication.IDApplication",array("CONCAT_WS(' ',IFNULL(tbl_studentapplication.FName,''),IFNULL(tbl_studentapplication.MName,''),IFNULL(tbl_studentapplication.LName,'')) as Candidatename","DATE_FORMAT(tbl_studentapplication.DateOfBirth, '%d-%m-%Y') as DOB","DATE_FORMAT(tbl_studentapplication.DateTime, '%d-%m-%Y') as DateTime","tbl_studentapplication.ICNO","tbl_studentapplication.EmailAddress","tbl_studentapplication.pass"))
					 				 ->join(array("tbl_programmaster"=>"tbl_programmaster"),"tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster",array("tbl_programmaster.ProgramName"))
					 				 ->join(array("tbl_managesession"=>"tbl_managesession"),"tbl_studentapplication.Examsession = tbl_managesession.idmangesession",array("tbl_managesession.managesessionname","tbl_managesession.starttime"))
					 				 ->join(array("tbl_center"=>"tbl_center"),"tbl_studentapplication.Examvenue= tbl_center.idcenter",array("tbl_center.centername"))
					 				 ->where("tbl_registereddetails.RegistrationPin in ($regpins)")
					 				 ->where('tbl_registereddetails.Approved=1')
					 				 
					 				 ->group('tbl_registereddetails.IDApplication')
									 ->order('tbl_registereddetails.RegistrationPin')
									 ->order('tbl_studentapplication.FName');
									 //echo $lstrSelect;die();
       $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	    return $larrResult;	
}

public function fngetbatchaddress($idcomp,$compflag)
{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    if($compflag == 1)
		{
			$lstrSelect = $lobjDbAdpt->select()
						             ->from(array("a" => "tbl_companies"),array("a.Postcode","a.ContactPerson as Contact","a.Address","a.Phone1 as Phone","a.Fax"))
									->join(array("b"=>"tbl_businesstype"),"a.Businesstype= b.idbusinesstype",array("b.idbusinesstype","b.BusinessType"))
					 				 ->join(array("c"=>"tbl_state"),"c.idState= a.IdState",array("c.StateName"))
                                      ->join(array("d"=>"tbl_countries"),"d.idCountry= a.IdCountry",array("d.CountryName"))									 
									->where("a.idCompany =?",$idcomp);
		}
		else
		{
			 $lstrSelect = $lobjDbAdpt->select()
							          ->from(array("a" => "tbl_takafuloperator"),array("a.paddr1 as Address","a.ContactName as Contact","a.zipCode as Postcode","a.workphone as Phone"))
									 ->join(array("b"=>"tbl_businesstype"),"a.businesstype= b.idbusinesstype",array("b.idbusinesstype","b.BusinessType"))
					 				 ->join(array("c"=>"tbl_state"),"c.idState= a.state",array("c.StateName"))
									  ->join(array("d"=>"tbl_countries"),"d.IdCountry = a.country",array("d.CountryName"))
									 ->where("a.idtakafuloperator=?",$idcomp);										    ;
		}
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
}
public function fngetbatchinvoicedetails($Regpins)
{
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $strQuery = $lobjDbAdpt->select()->from(array("a" =>"tbl_registereddetails"),array(""))
										  
										   ->join(array("c" =>"tbl_studentapplication"),'a.IDApplication = c.IDApplication' ,array('count(c.IDApplication) as Totalappln','c.Program'))
										 
										   ->where("a.Approved =1")
										   ->where("c.Examvenue!=000")									  
										   ->where("a.registrationPin in ($Regpins)")
										   ->group("c.Program");
										 
	    $larrResult = $lobjDbAdpt->fetchAll($strQuery);	
        return $larrResult;	
}
public function fngetprogramdetails($idprograms)
{
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
						            ->from(array("a" =>"tbl_programmaster"),array("a.*"))
									->join(array("b"=>"tbl_programrate"),"a.IdProgrammaster= b.idProgram",array("b.Rate"))								 
									->where("a.IdProgrammaster in ($idprograms)")
									->group("a.IdProgrammaster");
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);	
    return $larrResult;
}	
public function fngettotalcandidates($Regpin) {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $strQuery = $lobjDbAdpt->select()->from(array("a" => "tbl_registereddetails"), array(""))
                ->join(array("b" => "tbl_batchregistration"), 'a.registrationPin = b.RegistrationPin', array("key" => "b.registrationPin", "value" => "b.totalNoofCandidates", "b.totalAmount as Amount","b.ServiceTax as taxAmount"))
                 //->join(array("d" => "tbl_batchregistrationdetails"), 'd.idBatchRegistration = b.idBatchRegistration', array("d.eachAmount as Amount"))
                ->join(array("c" => "tbl_studentapplication"), 'a.IDApplication = c.IDApplication', array(''))
                ->where("a.Approved =1")
                ->where("c.Examvenue!=000")
                ->where("b.registrationPin =?", $Regpin)
                ->group("c.Program");
        $larrResult = $lobjDbAdpt->fetchRow($strQuery);
        return $larrResult;
    }
    public function fngettaxrate() {
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $strQuery = $lobjDbAdpt->select()->from(array("a" => "tbl_accountmaster"), array(""))
                ->join(array("b" => "tbl_programrate"), 'a.idAccount = b.IdAccountmaster', array("b.ServiceTax as GST"))
                ->where("a.idAccount =1")
                ->where("b.Active =1");    
        $larrResult = $lobjDbAdpt->fetchRow($strQuery);
        return $larrResult;
    }
    public function fngettotalamount($Regpin) {

        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        $strQuery = $lobjDbAdpt->select()->from(array("a" => "tbl_registereddetails"), array(""))
                ->join(array("b" => "tbl_batchregistration"), 'a.registrationPin = b.RegistrationPin', array("b.registrationPin", "b.totalAmount as Amount"))
                //->join(array("d" => "tbl_batchregistrationdetails"), 'd.idBatchRegistration = b.idBatchRegistration', array("d.eachAmount as Amount"))
                ->join(array("c" => "tbl_studentapplication"), 'a.IDApplication = c.IDApplication', array(''))
                ->where("a.Approved =1")
                ->where("c.Examvenue!=000")
                ->where("b.registrationPin =?", $Regpin)
                ->group("c.Program");

        $larrResult = $lobjDbAdpt->fetchRow($strQuery);
        return $larrResult;
    }
    
    

    /*public function fnupdatebulkinvoicegenerated($larrformData,$Uniqueid,$upduser,$lintidcompany,$companyflag)
{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        for($i=0;$i<count($larrformData['Pins']);$i++)		
        {
           $pin = $larrformData['Pins'][$i];
		   $Checkforpin = self::fncheckpindetails($pin);
		   if(empty($Checkforpin))
		   {
							$table1 = "tbl_invoicegeneration";
							$postData1 = array ('Invoiceuniqueid'=>$Uniqueid,
						    'Regpin' =>$pin,
						    'Amount'=>$larrformData['Amount'][$pin],
						    'Totalcandidates'=>$larrformData['Candidates'][$pin],
		    			    'Upddate' => date('Y-m-d H:i:s'), 
		    			    'Upduser' => $upduser,
                            'Idcompany'=>$lintidcompany,
							'Flag'=>$companyflag,
							'GenerationMethod'=>2
						   
						  
		    			  );
//echo "<pre>";
//print_r($postData1);die();						  
		        $lobjDbAdpt->insert ($table1,$postData1);
				if($i==0)
				{
		           
				}
				 $lastid = $lobjDbAdpt->lastInsertId('tbl_invoicegeneration','Idinvoice');
                if(strlen($lastid) ==1)
				{
				   $lastidnew = "000".$lastid;
				}
				else if(strlen($lastid) ==2)
				{
				    $lastidnew = "00".$lastid;
				}
				else if(strlen($lastid) ==3)
				{
				    $lastidnew = "0".$lastid;
				}
				else if(strlen($lastid) ==4)
				{
				   $lastidnew = $lastid;
				}	
				$data = array (
				'AdhocVenue'=>'Generated'
				);
				$where ['registrationPin = ?'] = $pin;
				$lobjDbAdpt->update ('tbl_batchregistration', $data, $where );
				if($i==0)
				{
				$ids=$Uniqueid.$lastidnew;
				}
				$data1 = array (
				'Invoiceuniqueid'=>$ids
				);
				$where1 ['Idinvoice = ?'] = $lastid;
				$lobjDbAdpt->update ('tbl_invoicegeneration', $data1, $where1 );
		}
		}
}*/

public function	fncheckregistereddetails($Regpin,$compflag)
{
   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	         if($compflag==1)
	         {
	        $lstrSelect = $lobjDbAdpt->select()
										  //->from(array("a" => "tbl_batchregistration"),array("key" => "a.registrationPin","value" =>"a.registrationPin"))
										 // ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										  ->from(array("d" =>"tbl_registereddetails"),array("d.*"))
										  ->join(array("e" =>"tbl_studentapplication"),'e.IDApplication=d.IDApplication',array(""))
										 // ->where("a.idCompany =?",$idcompany)
										  //->where("c.companyflag =1")
										  //->where("a.AdhocVenue = 'Blocked' or a.AdhocVenue ='others'")										  
										 // ->where("a.paymentStatus = 2")
										  ->where("d.Approved !=0")
										  ->where("d.RegistrationPin =?",$Regpin);
										  //->where("c.ModeofPayment =181")	
										  //->group("a.registrationPin");										
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			}
			if($compflag==2)
			{
				$lstrSelect = $lobjDbAdpt->select()
										  //->from(array("a" => "tbl_batchregistration"),array("key" => "a.registrationPin","value" =>"a.registrationPin"))
										 // ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())										 			  
										  ->from(array("d" =>"tbl_registereddetails"),array("d.*"))
										  ->join(array("e" =>"tbl_studentapplication"),'e.IDApplication=d.IDApplication',array(""))
										 // ->where("a.idCompany =?",$idcompany)
										 // ->where("a.AdhocVenue = 'Blocked' or a.AdhocVenue ='others'")
										 // ->where("c.companyflag =2")										
										 // ->where("a.paymentStatus = 2")
										  ->where("d.Approved !=0")
										  ->where("d.RegistrationPin =?",$Regpin);
										  //->where("c.ModeofPayment =181")
										 // ->group("a.registrationPin");										
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			}		
		    return $larrResult;	
}
public function  fncheckpindetails($Regpin)
	{
	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
		                          ->from(array("a" => "tbl_batchregistration"),array("a.totalNoofCandidates"))								   
								  ->join(array("b"=>"tbl_invoicedetails"),"a.registrationPin=b.Regpin",array(""))								  
								  ->join(array("c"=>"tbl_invoicegeneration"),"c.Idinvoice=b.Idinvoicemaster",array("c.Invoiceuniqueid"))
								  ->where("a.AdhocVenue ='Generated'")								 
								  ->where("b.Regpin = ?",$Regpin);
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
	}
public function fnupdatebulkinvoicegenerated($larrformData,$Uniqueid,$upduser,$lintidcompany,$companyflag)
{
     // echo "<pre>";
	 // print_r($larrformData);die();
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$Regpins ='';
		$TotalAmount =0;
		$Totalcandidates =0;
		$flg =1;
        for($i=0;$i<count($larrformData['Pins']);$i++)		
        {
           $pin = $larrformData['Pins'][$i];
		   $Checkforpin = self::fncheckpindetails($pin);
		    
		    if(empty($Checkforpin))
		    {
		      $Regpins = $Regpins.','.$pin;
			  $TotalAmount = ($TotalAmount+$larrformData['Amount'][$pin]);
			  $Totalcandidates = ($Totalcandidates+$larrformData['Candidates'][$pin]);			 
		    }
			else
			{
			  $flg =0;
			}
		}			
		if($flg ==1)
		{
		         $table1 = "tbl_invoicegeneration";
							$postData1 = array ('Invoiceuniqueid'=>$Uniqueid,
						    'Regpin' =>$Regpins,
						    'Amount'=>$TotalAmount,
							'GST'=>$larrformData['TotalGST'],
						    'Totalcandidates'=>$Totalcandidates,
		    			    'Upddate' => date('Y-m-d H:i:s'), 
		    			    'Upduser' => $upduser,
                            'Idcompany'=>$lintidcompany,
							'Flag'=>$companyflag,
							'GenerationMethod'=>2);						 					  
		         $lobjDbAdpt->insert ($table1,$postData1);				
				 $lastid = $lobjDbAdpt->lastInsertId('tbl_invoicegeneration','Idinvoice');				
				  for($i=0;$i<count($larrformData['Pins']);$i++)		
                  {
				             $Batchid = $larrformData['Pins'][$i];
				             $tableinvoicedetails = "tbl_invoicedetails";
							 $postinvoicedetails = array ('Idinvoicemaster'=>$lastid,
						     'Regpin' =>$larrformData['Pins'][$i],
						     'Amount'=>$larrformData['Amount'][$Batchid],
						     'Totalcandidates'=>$larrformData['Candidates'][$Batchid]);							 
		    			     $lobjDbAdpt->insert ($tableinvoicedetails,$postinvoicedetails);							 
							 $data = array (
								             'AdhocVenue'=>'Generated'
								           );
							 $where ['registrationPin = ?'] = $Batchid;
							 $lobjDbAdpt->update ('tbl_batchregistration', $data, $where );
				 }
         }
		      if(strlen($lastid) ==1)
				{
				   $lastidnew = "000".$lastid;
				}
				else if(strlen($lastid) ==2)
				{
				    $lastidnew = "00".$lastid;
				}
				else if(strlen($lastid) ==3)
				{
				    $lastidnew = "0".$lastid;
				}
				else if(strlen($lastid) ==4)
				{
				   $lastidnew = $lastid;
				}
				$ids=$Uniqueid.$lastidnew;
				
				$data1 = array (
				'Invoiceuniqueid'=>$ids
				);
				$where1 ['Idinvoice = ?'] = $lastid;
				$lobjDbAdpt->update ('tbl_invoicegeneration', $data1, $where1 );
    }
public function getinvoicenumber($larrformData)
{
	    $pins=0;
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
        for($i=0;$i<count($larrformData['Pins']);$i++)		
        {
           $pin = $larrformData['Pins'][$i];
		   $pins = $pins.','."'$pin'";
		   
		}  
		$lstrSelect = $lobjDbAdpt->select()
							  ->from(array("a" => "tbl_batchregistration"),array(""))
							  ->join(array("b"=>"tbl_invoicedetails"),"a.registrationPin=b.Regpin",array(""))
							  
							  ->join(array("c"=>"tbl_invoicegeneration"),"c.Idinvoice=b.Idinvoicemaster",array("c.Invoiceuniqueid"))
							  
							  ->where("a.AdhocVenue ='Generated'")								 
							  ->where("a.registrationPin in ($pins)");
	    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	    return $larrResult;
}
public function fngetreceivermail($idflag)
{
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			if($idflag ==1)
			{
			    $lstrSelect = $lobjDbAdpt->select()
                                ->from(array("a"=>"tbl_config"),array("a.ClosingBatchCompany as Duration","a.Schedulerpushemail"));
       		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			}
			else if($idflag ==2)
			{
			    $lstrSelect = $lobjDbAdpt->select()
    	   			->from(array("a"=>"tbl_config"),array("a.ClosingBatchCompany as Duration","a.Schedulerpushemail"));
       		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			}
       		return $larrResult;
}
}
