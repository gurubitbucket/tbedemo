<?php
class Registrations_Model_DbTable_Paymentdistribution extends Zend_Db_Table {	
	public function fnGetOperatorNames($opType,$namestring){
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($opType==1){
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("tbl_companies"),array("CompanyName as name"))
			->where('CompanyName like ? "%"',$namestring)
			;
		}else if($opType==2){
			$lstrSelect = $lobjDbAdpt->select()
			->from(array("tbl_takafuloperator"),array("TakafulName as name"))
			->where('TakafulName like ? "%"',$namestring);
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
     	
     }
	
 	public function fnSearchpaymentdistribution($opType,$opname){//Function for searching the Company/Takaful Payment Details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($opType==1){//Search for Company 	 
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_invoicegeneration"),array("a.Idinvoice","a.Invoiceuniqueid As invoiceno","a.Flag","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate"))
				-> join(array("d"=>"tbl_invoicedetails"),"d.Idinvoicemaster = a.Idinvoice",array("d.Idinvoicedetails","d.Amount","d.Totalcandidates","d.Regpin"))
				-> join(array("b"=>"tbl_batchregistration"),"b.registrationPin = d.Regpin",array(""))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.Upduser",array("u.fName"))
				-> join(array("c"=>"tbl_companies"),"c.IdCompany = b.idCompany",array("c.CompanyName","c.IdCompany As idoperator"))
				-> joinleft(array("e"=>"tbl_receiptdetails"),'e.BatchId = d.Regpin',array("(e.Amount) As batchamount"))
				-> joinleft(array("f"=>"tbl_receiptmaster"),"f.Idreceipt = e.Idreceipt",array(""))
				-> where('a.Flag = 1')
				->where('c.CompanyName =?',$opname)
				-> order('a.Flag')
				-> order('a.UpdDate');
			$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		}else if($opType==2){//Search for Takaful 	  
			$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_invoicegeneration"),array("a.Idinvoice","a.Invoiceuniqueid As invoiceno","a.Flag","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate"))
				-> join(array("d"=>"tbl_invoicedetails"),"d.Idinvoicemaster = a.Idinvoice",array("d.Idinvoicedetails","d.Amount","d.Totalcandidates","d.Regpin"))
				-> join(array("b"=>"tbl_batchregistration"),"b.registrationPin = d.Regpin",array(""))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.Upduser",array("u.fName"))
				-> join(array("c"=>"tbl_takafuloperator"),"c.idtakafuloperator = b.idCompany",array("c.TakafulName","c.idtakafuloperator As idoperator"))
				-> joinleft(array("e"=>"tbl_receiptdetails"),"e.BatchId = d.Regpin",array("(e.Amount) As batchamount","e.Active"))
				-> joinleft(array("f"=>"tbl_receiptmaster"),"f.Idreceipt = e.Idreceipt",array(""))
				-> where('a.Flag = 2')
				->where('c.TakafulName =?',$opname)
				-> order('a.Flag')
				-> order('a.UpdDate');
				
				//echo $lstrselect;die();
				/*$lstrselect="select a.Idinvoice,a.Invoiceuniqueid As invoiceno,a.Flag,DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate,d.Idinvoicedetails,d.Amount,d.Totalcandidates,
				d.Regpin,u.fName,c.TakafulName,c.idtakafuloperator As idoperator,e.Amount As batchamount,e.Active
				from tbl_invoicegeneration as a join tbl_invoicedetails as d on d.Idinvoicemaster = a.Idinvoice
				join tbl_batchregistration as b  on b.registrationPin = d.Regpin
				join tbl_user as u on u.iduser = a.Upduser
				join tbl_takafuloperator as c on c.idtakafuloperator = b.idCompany
				left join tbl_receiptdetails as e on e.BatchId = d.Regpin 
				left join tbl_receiptmaster as f on f.Idreceipt = e.Idreceipt
				where a.Flag =2 AND c.TakafulName='".$opname."' ORDER BY a.UpdDate";*/
				//echo $lstrselect;die();
				
				
				
				
			$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		}
		return $larrResult; 
	}
	
	
	public function fninsertreceiptdetails($larrformData,$Uniqueid,$companyflag,$idcompany,$upduser){
			//echo "<pre/>";print_r($larrformData);die();
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
                        $tablemaster = "tbl_receiptmaster";
                        $consdata['operator']=$larrformData['operator'];
			$consdata['operatorname']=$larrformData['operatorname'];
			$consdata['Branchname']=$larrformData['Branchname'];
			$consdata['ChequeDate']=$larrformData['ChequeDate'];
			$consdata['ChequeNumber']=$larrformData['ChequeNumber'];
			$consdata['ReceiptDate']=$larrformData['ReceiptDate'];
			$consdata['PaymentMode']=$larrformData['PaymentMode'];
			$consdata['Amount']=$larrformData['Amount'];
			$consdata['TotalAmountReceived']=$larrformData['TotalAmountReceived'];
			$consdata['totalpayableamount']=$larrformData['totalpayableamount'];
			$consdata['Totaloutstandingamount']=$larrformData['Totaloutstandingamount'];
			unset ( $larrformData ['refresh'] );
			unset ( $larrformData ['operator'] );
			unset ( $larrformData ['operatorname'] );
			unset ( $larrformData ['Branchname'] );
			unset ( $larrformData ['ChequeDate'] );
			unset ( $larrformData ['ChequeNumber'] );
			unset ( $larrformData ['ReceiptDate'] );
			unset ( $larrformData ['PaymentMode'] );
			unset ( $larrformData ['Amount'] );
			unset ( $larrformData ['TotalAmountReceived'] );
			unset ( $larrformData ['totalpayableamount'] );
			unset ( $larrformData ['Totaloutstandingamount'] );
			unset ( $larrformData ['totalrows'] );
			unset ( $larrformData ['totalpaid'] );
			unset ( $larrformData ['chk'] );
			$postData = array(				    
                            'ReceiptNum' =>$Uniqueid,	
                            'ReceiptDate' =>$consdata['ReceiptDate'],	
                            'Companyflag' =>$companyflag, 
                            'Idcompany' =>$idcompany,	
                            'Bankbranch' =>$consdata['Branchname'],	
                            'ChkNum' =>$consdata['ChequeNumber'], 
                            'ReceiptType' =>$consdata['PaymentMode'],
                            'ChkDate' =>$consdata['ChequeDate'],	
                            'Amount' =>$consdata['TotalAmountReceived'], 
                            'Active'=>'1',
                            'Printstatus'=>'1',
                            'UpdDate' =>date("Y-m-d"),
                            'UpdUser'=>$upduser,		    				
			);
			//echo "<pre/>";print_r($larrformData);die();
				//echo "<pre/>";print_r($larrformData);die();
				
				$table = "tbl_receiptdetails";
				$onemaster=1;
				foreach($larrformData as $larrformData1){
                                    if($larrformData1['chk']==1){
                                        if($larrformData1['paidamount']>0){
                                            //insert into tbl_receiptmaster
                                            if($onemaster==1){
                                                $lobjDbAdpt->insert($tablemaster,$postData);
                                                $onemaster++;
                                                //update receipt number format
                                                $lastid = $lobjDbAdpt->lastInsertId('tbl_receiptmaster','Idreceipt');
                                                if(strlen($lastid) ==1){$lastidnew = "000".$lastid;}
                                                else if(strlen($lastid) ==2){$lastidnew = "00".$lastid;}
                                                else if(strlen($lastid) ==3){$lastidnew = "0".$lastid;}
                                                else if(strlen($lastid) ==4){$lastidnew = $lastid;}
                                                $ids=$Uniqueid.$lastidnew;
                                                $data1 = array ('ReceiptNum'=>$ids);
                                                $where1 ['Idreceipt = ?'] = $lastid;
                                                $lobjDbAdpt->update ('tbl_receiptmaster', $data1, $where1 );
                                            }
                                            //insert into tbl_receiptdetails
                                            if($larrformData1['paidamount']==$larrformData1['outstandingamount']){
                                                $active=1;
                                            }else{
                                                $active=2;
                                            }
                                            $postData = array(				    
                                                'Idreceipt' =>$lastid,	
                                                'Idinvoicedetails' =>$larrformData1['Idinvoicedetails'],	
                                                'BatchId' =>$larrformData1['regpin'], 
                                                'InvoiceNum' =>$larrformData1['invoiceno'],	
                                                'Amount' =>$larrformData1['paidamount'],	
                                                'Active' =>$active, 	
                                                'UpdDate' =>date("Y-m-d"),
                                                'UpdUser'=>$upduser,
                                            );
                                            $lobjDbAdpt->insert($table,$postData);
                                        }
                                    }
				}
		return $lastid;
	}
	
	/*function fngetamount($regpin){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptdetails"),array("sum(a.Amount) as sum"))
				-> where('a.BatchId ='.$regpin);
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
		
	}*/
        function fngetamount($regpin,$idoperator){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrselect = $lobjDbAdpt->select()
                                -> from(array("a"=>"tbl_receiptmaster"),array(""))
                                -> join(array("b"=>"tbl_receiptdetails"),"b.Idreceipt = a.Idreceipt",array("sum(b.Amount) as sum"))
				//-> from(array("a"=>"tbl_receiptdetails"),array("sum(a.Amount) as sum"))
				-> where('b.BatchId ='.$regpin)
                                -> where('a.Idcompany ='.$idoperator);
                //echo $lstrselect;
		$larrResult = $lobjDbAdpt->fetchAll($lstrselect);
		return $larrResult;
		
	}
	

	public function fngetreceiptdetails($receiptnumpk,$compflag){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($compflag == 1)
		{
			$lstrSelect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptmaster"),array("a.Idreceipt","a.ReceiptNum","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate","DATE_FORMAT(a.ReceiptDate,'%d-%m-%Y') As ReceiptDate","a.Bankbranch","a.ChkNum","a.Amount As totalamount","a.Idcompany","a.ReceiptType As PaymentMode"))
				-> join(array("b"=>"tbl_receiptdetails"),"b.Idreceipt = a.Idreceipt",array("b.BatchId","b.InvoiceNum","b.Amount"))
				-> join(array("i"=>"tbl_invoicedetails"),"i.Regpin = b.BatchId",array("i.Amount as batchamount"))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.UpdUser",array("u.fName As Generatedby"))
				-> join(array("c"=>"tbl_companies"),"c.IdCompany = a.Idcompany",array("c.CompanyName As operatorname","c.Email","c.ContactPerson As ContactName","c.Address","c.Postcode","c.Phone1 As workphone","c.Fax"))
				->join(array("e"=>"tbl_businesstype"),"e.idbusinesstype= c.Businesstype",array("e.idbusinesstype","e.BusinessType"))
				->join(array("f"=>"tbl_state"),"f.idState= c.IdState",array("f.StateName"))
             	->join(array("d"=>"tbl_countries"),"d.idCountry= c.IdCountry",array("d.CountryName"))
				-> where("a.Idreceipt =?",$receiptnumpk);
		}
		else if($compflag == 2)
		{
			 $lstrSelect = $lobjDbAdpt->select()
				-> from(array("a"=>"tbl_receiptmaster"),array("a.Idreceipt","a.ReceiptNum","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') As UpdDate","DATE_FORMAT(a.ReceiptDate,'%d-%m-%Y') As ReceiptDate","a.Bankbranch","a.ChkNum","a.Amount As totalamount","a.Idcompany","a.ReceiptType As PaymentMode"))
				-> join(array("b"=>"tbl_receiptdetails"),"b.Idreceipt = a.Idreceipt",array("b.BatchId","b.InvoiceNum","b.Amount"))
				-> join(array("i"=>"tbl_invoicedetails"),"i.Regpin = b.BatchId",array("i.Amount as batchamount"))
				-> join(array("u"=>"tbl_user"),"u.iduser = a.UpdUser",array("u.fName As Generatedby"))
				-> join(array("c"=>"tbl_takafuloperator"),"c.idtakafuloperator = a.Idcompany",array("c.TakafulName As operatorname","c.ContactEmail As Email","c.ContactName","c.ContactCell","c.paddr1 As Address","c.zipCode as Postcode","c.workphone"))
				->join(array("e"=>"tbl_businesstype"),"e.idbusinesstype= c.Businesstype",array("e.idbusinesstype","e.BusinessType"))
				->join(array("f"=>"tbl_state"),"f.idState= c.state",array("f.StateName"))
             	->join(array("d"=>"tbl_countries"),"d.idCountry= c.country",array("d.CountryName"))
				-> where("a.Idreceipt =?",$receiptnumpk);								    ;
		}
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
public function fngetreceivermail($idflag)
{
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			if($idflag ==1)
			{
			    $lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_config"),array("a.ClosingBatchCompany as Duration","a.Schedulerpushemail"));
       		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			}
			else if($idflag ==2)
			{
			    $lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_config"),array("a.ClosingBatchTakaful as Duration","a.Schedulerpushemail"));
       		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			}
       		return $larrResult;
}
	

}