<?php
class Registrations_Model_DbTable_Studentapproval extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	
	public function fnGetStudentDetails()
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											 ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
											  ->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
											  ->where("a.Approved=0")
											  ->where("a.Registrationpin=0000000")
 ->group("a.IDApplication");
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
public function fnGetStudent($lstrType)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											  ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
											  ->join(array("f" =>"tbl_programmaster"),'b.Program=f.IdProgrammaster')
											  ->join(array("c" =>"tbl_state"),'b.ExamState=c.idState',array("c.*"))
											  ->join(array("d" =>"tbl_city"),'b.ExamCity=d.idCity',array("d.*"))
											  ->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
											  ->join(array("r" =>"tbl_newscheduler"),'r.idnewscheduler=b.Year',array("r.Year as Years"))
											  ->where("a.Approved=0")
											  ->where("a.Registrationpin=0000000")
											  ->where("b.IDApplication = ?",$lstrType)
											  ->group("b.IDApplication");
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
 
	public function fnStudentApproved($larrformdata)
	{
		/*echo "<pre/>";
		print_r($larrformdata);
		die();*/
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$tableName = "tbl_registereddetails";

	    for($i = 0; $i<count($larrformdata['IDApplication']); $i++ )
			{		
				$idBatchRegistration = $larrformdata['IDApplication'][$i];
				
				$postData = array('Approved' => 1);			
                $where = "IDApplication=$idBatchRegistration";
				$lobjDbAdpt->update($tableName,$postData,$where);
				//self::fnGetStudentapproved($idBatchRegistration);
			}
			
	}
	
      public function fngetStudentSearch($lobjgetarr){
		//echo "harsha";die();
		$db 	= Zend_Db_Table::getDefaultAdapter();	
		$select = 	$db->select()          
             	 ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
				->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
				->join(array("c" =>"tbl_programmaster"),'b.Program=c.IdProgrammaster')
				->where("a.Registrationpin=0000000")							  
            	-> where('b.FName like  ? "%"',$lobjgetarr['field3'])
            	->where('a.RegistrationPin like  ? "%"',$lobjgetarr['field4'])            	
				->where("a.Approved=0");	           	       
		$result = $db->fetchAll($select);	
		return $result;		
	}

}
