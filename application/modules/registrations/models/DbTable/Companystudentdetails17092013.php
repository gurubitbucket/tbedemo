<?php
class Registrations_Model_DbTable_Companystudentdetails extends Zend_Db_Table { //Model Class for Company Student Details
	   protected $_name = 'tbl_companies';

	  /* public function fnGenerateQueries($intQueryType,$varAllOne,$varOthers = NULL)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		switch ($intQueryType)
	   		{ 
	   			// To get the company details
	   			case 1:$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
					 				 ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
					 				 ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 ->where("tbl_city.Active = 1") 
									 ->order("tbl_companies.CompanyName");
								break;
								
				//To get the details of a particular				
	   			case 2:	$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
					 				 ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
					 				 ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 ->where("tbl_city.Active = 1") 
					 				 ->where('tbl_companies.IdCompany = ?',$varOthers)
									 ->order("tbl_companies.CompanyName");
								break;			
								
				// To get Details of the companies on click of the hyper link				
	   			case 3: $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
					 				 ->join(array("tbl_batchregistration"=>"tbl_batchregistration"),"tbl_companies.IdCompany= tbl_batchregistration.idCompany",array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","tbl_batchregistration.paymentStatus","tbl_batchregistration.UpdDate"))
					 				 ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_batchregistration.idBatchRegistration= tbl_studentpaymentoption.IDApplication",array("tbl_studentpaymentoption.ModeofPayment"))
					 				 ->where('tbl_companies.IdCompany = ?',$varOthers)
					 				 ->where('tbl_studentpaymentoption.companyflag=1')
					 				 ->order('tbl_batchregistration.UpdDate DESC');	
					 				 break;

				// To get Details of the student Details based on the registration pin				
	   			case 4: $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_registereddetails"=>"tbl_registereddetails"),array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.Regid"))
					 				 ->join(array("tbl_studentapplication"=>"tbl_studentapplication"),"tbl_registereddetails.IDApplication = tbl_studentapplication.IDApplication",array("CONCAT_WS(' ',IFNULL(tbl_studentapplication.FName,''),IFNULL(tbl_studentapplication.MName,''),IFNULL(tbl_studentapplication.LName,'')) as Candidatename","DATE_FORMAT(tbl_studentapplication.DateOfBirth, '%d-%m-%Y') as DOB","DATE_FORMAT(tbl_studentapplication.DateTime, '%d-%m-%Y') as DateTime","tbl_studentapplication.ICNO","tbl_studentapplication.EmailAddress","tbl_studentapplication.pass"))
					 				 ->join(array("tbl_programmaster"=>"tbl_programmaster"),"tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster",array("tbl_programmaster.ProgramName"))
					 				 ->join(array("tbl_managesession"=>"tbl_managesession"),"tbl_studentapplication.Examsession = tbl_managesession.idmangesession",array("tbl_managesession.managesessionname","tbl_managesession.starttime"))
					 				 ->join(array("tbl_center"=>"tbl_center"),"tbl_studentapplication.Examvenue= tbl_center.idcenter",array("tbl_center.centername"))
					 				 ->where('tbl_registereddetails.RegistrationPin = ?',$varOthers)
					 				 ->where('tbl_registereddetails.Approved=1')
					 				 ->order('tbl_studentapplication.FName')
					 				 ->group('tbl_registereddetails.IDApplication');	
					 				 break;
					 				 
				// To get Count of Candidates				
	   			case 5: $strQuery = $lobjDbAdpt->select()->from(array("tbl_batchregistration" =>"tbl_batchregistration"),array('tbl_batchregistration.totalNoofCandidates','tbl_batchregistration.idBatchRegistration'))
										->joinLeft(array("tbl_registereddetails" =>"tbl_registereddetails"),'tbl_batchregistration.registrationPin = tbl_registereddetails.RegistrationPin',array('count(distinct tbl_registereddetails.idregistereddetails) as totalregistered'))
										
										->where('tbl_batchregistration.registrationPin =?',$varOthers);	
					 				 break;
					 				 
				//To get Details of a particular pin in takaful
				case 6:	$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator as IdCompany","tbl_takafuloperator.TakafulName as CompanyName","tbl_takafuloperator.TakafulShortName as ShortName","tbl_takafuloperator.paddr1 as Address","tbl_takafuloperator.workphone as Phone1","tbl_takafuloperator.email as Email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.businesstype = tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
					 				 ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state = tbl_state.idState",array("tbl_state.StateName"))
					 				 ->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city = tbl_city.idCity",array("tbl_city.CityName"))
					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 ->where("tbl_city.Active = 1") 
					 				 ->where('tbl_takafuloperator.idtakafuloperator = ?',$varOthers)
									 ->order("tbl_takafuloperator.TakafulName");
								break;	
								
				// To get Details of the takaful on click of the hyper link				
	   			case 7: $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator as IdCompany","tbl_takafuloperator.TakafulName as CompanyName","tbl_takafuloperator.TakafulShortName as ShortName","tbl_takafuloperator.paddr1 as Address","tbl_takafuloperator.workphone as Phone1","tbl_takafuloperator.email as Email"))
					 				 ->join(array("tbl_batchregistration"=>"tbl_batchregistration"),"tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany",array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","tbl_batchregistration.paymentStatus","tbl_batchregistration.UpdDate"))
					 				 ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_batchregistration.idBatchRegistration = tbl_studentpaymentoption.IDApplication",array("tbl_studentpaymentoption.ModeofPayment"))
					 				 ->where('tbl_takafuloperator.idtakafuloperator = ?',$varOthers)
					 				 ->where('tbl_studentpaymentoption.companyflag=2')
					 				 ->order('tbl_batchregistration.UpdDate DESC');	
					 				 break;
					 				 
			// To get takaful details	 				 
			  case 8:$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.TakafulName","tbl_takafuloperator.TakafulShortName","tbl_takafuloperator.paddr1","tbl_takafuloperator.workphone","tbl_takafuloperator.email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.Businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   						 ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state= tbl_state.idState",array("tbl_state.StateName"))
			  						 ->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city= tbl_city.idCity",array("tbl_city.CityName"))					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 ->where("tbl_city.Active = 1") 
									 ->order("tbl_takafuloperator.TakafulName");
								break;
					 				 
	   		}
	   		
	   		//echo $strQuery;
	   		
	   		if($varAllOne == '0') 
	   		{
	   			$larrResult = $lobjDbAdpt->fetchRow($strQuery);
	   		}
	   		elseif($varAllOne == '1')
	   		{
		   		$larrResult = $lobjDbAdpt->fetchAll($strQuery);
	   		}
			return $larrResult;
	   }
	   */

public function fnSearchCompaniesnew($larrformData) 
	   {
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
			   ->joinleft(array("tbl_companypaymenttype"=>"tbl_companypaymenttype"),"tbl_companies.IdCompany=tbl_companypaymenttype.idcompany",array("tbl_companypaymenttype.*"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_companies.CompanyName like "%" ? "%"',$larrformData['field3'])
			   ->where('tbl_companies.ShortName like  "%" ? "%"',$larrformData['field2'])
			   ->where('tbl_companies.Address like "%" ? "%"',$larrformData['field6'])
			   ->where('tbl_companies.Phone1 like "%" ? "%"',$larrformData['field12'])
			   ->where('tbl_companies.Email like "%" ? "%"',$larrformData['field13']);
			

			if(isset($larrformData['field5']) && !empty($larrformData['field5']) ){
				$select = $select->where("tbl_companies.businesstype = ?",$larrformData['field5']);
			}
			
			if(isset($larrformData['field1']) && !empty($larrformData['field1']) ){
				$select = $select->where("tbl_companies.IdState = ?",$larrformData['field1']);
			}
			
			if(isset($larrformData['field8']) && !empty($larrformData['field8']) ){
				$select = $select->where("tbl_companies.City = ?",$larrformData['field8']);
			}
		
			$select ->order("tbl_companies.CompanyName");
			$result = $this->fetchAll($select);
			return $result;
	   }


	   public function fnSearchCompanies($post = array()) 
	   {
	   	 	
	   	 //Function for searching the Company details and Takaful Details
    	 $db = Zend_Db_Table::getDefaultAdapter();
    
    if($post['field19']==1){
    	 //Search for Company   	 
		 $select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
			   ->joinleft(array("tbl_companypaymenttype"=>"tbl_companypaymenttype"),"tbl_companies.IdCompany=tbl_companypaymenttype.idcompany",array("tbl_companypaymenttype.*"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_companies.CompanyName like "%" ? "%"',$post['field3'])
			   ->where('tbl_companies.ShortName like  "%" ? "%"',$post['field2']);
			   //->where('tbl_companies.Address like "%" ? "%"',$post['field6'])
			  // ->where('tbl_companies.Phone1 like "%" ? "%"',$post['field12'])
			   //->where('tbl_companies.Email like "%" ? "%"',$post['field13'])
			

		/*	if(isset($post['field5']) && !empty($post['field5']) ){
				$select = $select->where("tbl_companies.businesstype = ?",$post['field5']);
			}
			
			if(isset($post['field1']) && !empty($post['field1']) ){
				$select = $select->where("tbl_companies.IdState = ?",$post['field1']);
			}
			
			if(isset($post['field8']) && !empty($post['field8']) ){
				$select = $select->where("tbl_companies.City = ?",$post['field8']);
			}*/
		
			$select ->order("tbl_companies.CompanyName");
		
     }else if($post['field19']==2){
    	 	//Search for takaful 12-07-2012    	 	
     		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator as IdCompany","tbl_takafuloperator.TakafulName as CompanyName","tbl_takafuloperator.TakafulShortName as ShortName","tbl_takafuloperator.paddr1 as Address","tbl_takafuloperator.workphone as Phone1","tbl_takafuloperator.email as Email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.Businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state = tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city = tbl_city.idCity",array("tbl_city.CityName"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_takafuloperator.TakafulName like "%" ? "%"',$post['field3'])
			   ->where('tbl_takafuloperator.TakafulShortName like  "%" ? "%"',$post['field2']);
			   
			   //->where('tbl_takafuloperator.paddr1 like "%" ? "%"',$post['field6'])
			   //->where('tbl_takafuloperator.workphone like "%" ? "%"',$post['field12'])
			 //  ->where('tbl_takafuloperator.email like "%" ? "%"',$post['field13']);

			/*if(isset($post['field5']) && !empty($post['field5']) ){
				$select = $select->where("tbl_takafuloperator.Businesstype = ?",$post['field5']);
			}
			
			if(isset($post['field1']) && !empty($post['field1']) ){
				$select = $select->where("tbl_takafuloperator.state = ?",$post['field1']);
			}
			
			if(isset($post['field8']) && !empty($post['field8']) ){
				$select = $select->where("tbl_takafuloperator.city = ?",$post['field8']);
			}*/
			   
			
		$select ->order("tbl_takafuloperator.TakafulName");
    	 
     }
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	
	public function fnSearchTakaful($post = array()){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.TakafulName","tbl_takafuloperator.TakafulShortName","tbl_takafuloperator.paddr1","tbl_takafuloperator.workphone","tbl_takafuloperator.email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.Businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state= tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city= tbl_city.idCity",array("tbl_city.CityName"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_takafuloperator.TakafulName like "%" ? "%"',$post['field3'])
			   ->where('tbl_takafuloperator.TakafulShortName like  "%" ? "%"',$post['field2'])
			   ->where('tbl_takafuloperator.paddr1 like "%" ? "%"',$post['field6'])
			   ->where('tbl_takafuloperator.workphone like "%" ? "%"',$post['field12'])
			   ->where('tbl_takafuloperator.email like "%" ? "%"',$post['field13']);

			if(isset($post['field5']) && !empty($post['field5']) ){
				$select = $select->where("tbl_takafuloperator.Businesstype = ?",$post['field5']);
			}
			
			if(isset($post['field1']) && !empty($post['field1']) ){
				$select = $select->where("tbl_takafuloperator.state = ?",$post['field1']);
			}
			
			if(isset($post['field8']) && !empty($post['field8']) ){
				$select = $select->where("tbl_takafuloperator.city = ?",$post['field8']);
			}
			
		$select ->order("tbl_takafuloperator.TakafulName");
		$result = $this->fetchAll($select);
		return $result->toArray();
		
	}
	

     public function fnGetOperatorNames($opType,$namestring){
     	
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     			 if($opType==1){     			 
				 		$lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_companies"),array("CompanyName as name"))							 				
							 				     ->where('CompanyName like ? "%"',$namestring)
							 				   ;
     			 }else if($opType==2){
     			 	    $lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_takafuloperator"),array("TakafulName as name"))							 				
							 				     ->where('TakafulName like ? "%"',$namestring);
     			 }
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }

     public function fngetnoofstudentsfromexcel($ids)
     { // Get remaining number of students in a batch
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_tempexcelcandidates"),array("a.*"))							 				
							 				 ->where("a.RegistrationPin = ?",$ids)
							 				// ->where("a.ICNO not in (select ICNO from tbl_studentapplication)")
											;



				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }
     
     public function fngetexcelappliedcandidates($ids)
     { // Get excel applied candidates details     	
     	 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_studentapplication"),array("a.FName","a.ICNO","a.DateTime","a.EmailAddress"))	
							 				 ->join(array("e"=>"tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid")) 
							 				 ->join(array("b"=>"tbl_programmaster"),'a.Program = b.IdProgrammaster',array("b.ProgramName")) 
							 				 ->join(array("c"=>"tbl_managesession"),'a.Examsession = c.idmangesession',array("c.managesessionname")) 	
							 				 ->join(array("d"=>"tbl_center"),'a.Examvenue = d.idcenter',array("d.centername")) 						 							
							 				  ->where("e.RegistrationPin = ?",$ids)
											 ->order("a.FName");				 				 
							 				// ->where("a.ICNO in (select ICNO from tbl_tempexcelcandidates where RegistrationPin = '$ids' )");
							 				// echo $lstrSelect;exit;				
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;   	
     	
     }
     
	public function fnGetModeofpay($idbatchreg){
		
		     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_studentpaymentoption"),array("ModeofPayment"))
							 				 ->where("IDApplication = ?",$idbatchreg)
							 				 ->where("companyflag = 1");
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
		
	}
	
public function fnInsertIntoStd($larrformData,$countloop,$batchid,$regpin,$companyaddress,$idcompany,$iduser,$flag)
	{
	$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //TAkafulmodels model object
		for($i=0;$i<$countloop;$i++)
		{
$randarray=$this->lobjTakafulcandidatesmodel->fngeneraterandompassword();
		  $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_studentapplication";
          
          if(strlen($larrformData['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformData['setmonth'];
          } else {
          	$monthsss = $larrformData['setmonth'];
          }
          
          
		  if(strlen($larrformData['setdate']) <= 1) {
          	$dayssss = '0'.$larrformData['setdate'];
          } else {
          	$dayssss = $larrformData['setdate'];
          }
          
          
          $datetime = $larrformData['Year'].'-'.$monthsss.'-'.$dayssss;
		  $postData = array('StudentId' =>'00',	
           					'FName' =>$larrformData['candidatename'][$i],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['candidatedateofbirth'][$i],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateemail'][$i],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>$iduser,	
		                    'username' =>$larrformData['candidateicno'][$i],	
                            'password' =>$randarray,
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateicno'][$i],	
           					'Payment' => 1, 
							'DateTime' =>$datetime,	
           					'PermAddressDetails' =>$companyaddress,	
           					'Takafuloperator' =>$idcompany, 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' =>$flag, 
							'Gender' =>$larrformData['candidategender'][$i],	
           					'Race' =>$larrformData['candidaterace'][$i],	
		                    'Religion' =>$larrformData['candidatereligion'][$i],	
           					'Qualification' =>$larrformData['candidateeducation'][$i], 
							'State' =>$larrformData['candidatestate'][$i],	
           					'CorrAddress' =>$larrformData['candidateaddress'][$i],	
           					'PostalCode' =>$larrformData['postalcode'][$i], 
		 					'ContactNo' =>$larrformData['candidatenum'][$i],	
           					'MobileNo' =>$larrformData['candidatemobnum'][$i], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  						'Year'=>$larrformData['scheduler'],
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  					'Examvenue'=>$larrformData['NewVenue'],	
		  					'Examsession'=>$larrformData['idsession'],
		                    'pass'=>3	
						);			
						
	        $db->insert($table,$postData);	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');	
	        
	        
	       $larrdatainsert=array(
	        					'StudentName' =>$larrformData['candidatename'][$i],	
           						'ICNO' =>$larrformData['candidateicno'][$i],	
           						'Race' =>$larrformData['candidaterace'][$i], 								
           						'email' =>$larrformData['candidateemail'][$i], 
           						'education' =>$larrformData['candidateeducation'][$i],	
           						'Gender' =>$larrformData['candidategender'][$i], 
		                  		'DOB'=>$larrformData['candidatedateofbirth'][$i],		    											
	        					'Address' =>$larrformData['candidateaddress'][$i],		    					
		    					'CorrespAddress'=>$larrformData['candidateaddress'][$i],
		    					'PostalCode'=>$larrformData['postalcode'][$i],
		    					'IdCountry'=>0,
		    					'IdState'=>$larrformData['candidatestate'][$i],	
		    					'ContactNo'=>$larrformData['candidatenum'][$i],
		    					'MobileNo'=>$larrformData['candidatemobnum'][$i],
	        );
	        
	        
	        
			//inserting into temp registration excel table    18-06-2012
	        self::fninsertintotemp($larrdatainsert,0,$regpin);
	        
	        
	        
	        
	        $this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //TAkafulmodels model object	        	        
	        $larrresult = $this->lobjTakafulcandidatesmodel->fnviewstudentdetails($lastid);        
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($idcompany,$lastid);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['setactive'],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>1,
            				'Cetreapproval'=>0,
            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);
	       
	     
	     $this->lobjTakafulcandidatesmodel->sendmails($larrresult,$regid);
	       
		}

		$where = 'idBatchRegistration = '.$batchid;
		$postData = array(		
			   	 'paymentStatus' => 1,
				 'Approved' => 1															
				);
		$table = "tbl_batchregistration";
	    $db->update($table,$postData,$where);
	    
	    //updating table tbl_venuedateschedule
	    $this->lobjTakafulcandidatesmodel->fnupdatevenuedateschedule($larrformData,2);
	    
	}
	
	public function fninsertintotemp($larrdatainsert,$idtakaful,$idregpin) 
	{ 

		// Function to enter into tbl_tempexcelcandidates		
			$sessionID = Zend_Session::getId();
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_tempexcelcandidates";
		    $postData = array(				    
							'StudentName' =>$larrdatainsert['StudentName'],	
           					'ICNO' =>$larrdatainsert['ICNO'],	
           					'Race' =>$larrdatainsert['Race'], 
							'Takafuloperator' =>$idtakaful,	
           					'idprogram' =>'0',	
           					'email' =>$larrdatainsert['email'], 
           					'education' =>$larrdatainsert['education'],	
           					'Gender' =>$larrdatainsert['Gender'], 
		                    'DOB'=>$larrdatainsert['DOB'],
		    				'session'=>$sessionID,
							'Address' =>$larrdatainsert['Address'],
		    				'RegistrationPin'=>$idregpin,
		    				'CorrespAddress'=>$larrdatainsert['CorrespAddress'],
		    				'PostalCode'=>$larrdatainsert['PostalCode'],
		    				'IdCountry'=>$larrdatainsert['IdCountry'],
		    				'IdState'=>$larrdatainsert['IdState'],	
		    				'ContactNo'=>$larrdatainsert['ContactNo'],
		    				'MobileNo'=>$larrdatainsert['MobileNo'],		    				
		    );
		      $db->insert($table,$postData);
		    
	}
	
	
	
	public function fnInsertintostudapplicationexcel($larrformData,$lintidbatch,$linttotnumofapplicant,$regpin,$idCompany,$iduser,$flag){
		//$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates ();
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //TAkafulmodels model object
		$avalus = array_values($larrformData['studenttakful']);		
		for($insertfrmexcel=0;$insertfrmexcel<count($avalus);$insertfrmexcel++)
		{
 $randarray=$this->lobjTakafulcandidatesmodel->fngeneraterandompassword();		
		  $dob=date("Y-m-d",strtotime($larrformData['candidateDOBtakaful'][$larrformData['studenttakful'][$insertfrmexcel]]));
		  $db = Zend_Db_Table::getDefaultAdapter();
		  
		  
		 if(strlen($larrformData['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformData['setmonth'];
          } else {
          	$monthsss = $larrformData['setmonth'];
          }
          
          
		  if(strlen($larrformData['setdate']) <= 1) {
          	$dayssss = '0'.$larrformData['setdate'];
          } else {
         	$dayssss = $larrformData['setdate'];
          }
          
          
          $datetime = $larrformData['Year'].'-'.$monthsss.'-'.$dayssss;
		  
		  
          $table = "tbl_studentapplication";
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['candidatenametakaful'][$avalus[$insertfrmexcel]],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$dob,	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateEMAILtakaful'][$avalus[$insertfrmexcel]],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>$iduser,	
		                    'username' =>$larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],	
                            'password' =>$randarray,
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],	
           					'Payment' => 1, 
							'DateTime' =>$datetime,	
           					'PermAddressDetails' =>$larrformData['Address'][$avalus[$insertfrmexcel]],	
           					'Takafuloperator' =>$idCompany, 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => $flag, 
							'Gender' =>$larrformData['Gender'][$avalus[$insertfrmexcel]],	
           					'Race' =>$larrformData['Race'][$avalus[$insertfrmexcel]],	
		                    'Religion' =>144,	
           					'Qualification' =>$larrformData['education'][$avalus[$insertfrmexcel]], 
							'State' =>$larrformData['IdState'][$avalus[$insertfrmexcel]],	
           					'CorrAddress' =>$larrformData['CorrespAddress'][$avalus[$insertfrmexcel]],	
           					'PostalCode' =>$larrformData['PostalCode'][$avalus[$insertfrmexcel]], 
		 					'ContactNo' =>$larrformData['ContactNo'][$avalus[$insertfrmexcel]],	
           					'MobileNo' =>$larrformData['MobileNo'][$avalus[$insertfrmexcel]], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  						'Year'=>$larrformData['scheduler'],
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  					'Examvenue'=>$larrformData['Examvenue'],	
		  					'Examsession'=>$larrformData['idsession'],
		                    'pass'=>3		  
						);	
							
	        $db->insert($table,$postData);
	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');	        
	         $this->lobjTakafulcandidatesmodel->fnupdatestudentcount($larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],$lastid);
	        $larrresult = $this->lobjTakafulcandidatesmodel->fnviewstudentdetails($lastid);	        
	       
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($idCompany,$lastid);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['setactive'],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>1,
            				'Cetreapproval'=>0,
            				'RegistrationPin'=>$larrformData['idbatch']
            				);	
            						
	       $db->insert($tables,$postDatas);       
	       $this->lobjTakafulcandidatesmodel->sendmails($larrresult,$regid);	

	       $this->lobjTakafulcandidatesmodel->fndeletefromtempexcelcandidates($avalus[$insertfrmexcel]);	  //new function to delete redundant data from tempescelcandidates added on 1-3-2013
		}		
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_registereddetails',array('count(idregistereddetails) as totapplied'))
								->where('RegistrationPin =?',$regpin);
			$resulttc = $db->fetchRow($sql);
			
		//echo $resulttc['totapplied'];exit;
		if($resulttc['totapplied'] < $linttotnumofapplicant){
		     $paymentstatus=1;
		}else{
			 $paymentstatus=2;
		}
		
			     $where = 'idBatchRegistration = '.$lintidbatch;
			     $postData = array(		
						   	 'paymentStatus' =>$paymentstatus,
			    			 'Approved' => 1															
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	            
	            
	            //updating table tbl_venuedateschedule
	            $this->lobjTakafulcandidatesmodel->fnupdatevenuedateschedule($larrformData,1);
	            
		
	}
	
	
	//updated data from the local server(15-03-2013)
	public function fnGenerateQueries($intQueryType,$varAllOne,$varOthers = NULL)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		switch ($intQueryType)
	   		{ 
	   			// To get the company details
	   			case 1:$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
					 				 ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
					 				 ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 ->where("tbl_city.Active = 1") 
									 ->order("tbl_companies.CompanyName");
								break;
								
				//To get the details of a particular				
	   			case 2:	$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
					 				 ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
					 				 //->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 //->where("tbl_city.Active = 1") 
					 				 ->where('tbl_companies.IdCompany = ?',$varOthers)
									 ->order("tbl_companies.CompanyName");
								break;			
								
				// To get Details of the companies on click of the hyper link				
	   			case 3: $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
					 				 ->join(array("tbl_batchregistration"=>"tbl_batchregistration"),"tbl_companies.IdCompany= tbl_batchregistration.idCompany",array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","tbl_batchregistration.paymentStatus","tbl_batchregistration.UpdDate","tbl_batchregistration.AdhocVenue"))
					 				 ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_batchregistration.idBatchRegistration= tbl_studentpaymentoption.IDApplication",array("tbl_studentpaymentoption.ModeofPayment","tbl_studentpaymentoption.*"))
					 				 ->joinLeft(array("m"=>"tbl_registereddetails"),"tbl_batchregistration.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pin","m.Approved as approve"))
									->where('tbl_companies.IdCompany = ?',$varOthers)
					 				 ->where('tbl_studentpaymentoption.companyflag=1')
									  ->group("tbl_batchregistration.idBatchRegistration")
					 				 ->order('tbl_batchregistration.UpdDate DESC');	
					 				 break;

				// To get Details of the student Details based on the registration pin				
	   			case 4: $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_registereddetails"=>"tbl_registereddetails"),array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.Regid"))
					 				 ->join(array("tbl_studentapplication"=>"tbl_studentapplication"),"tbl_registereddetails.IDApplication = tbl_studentapplication.IDApplication",array("CONCAT_WS(' ',IFNULL(tbl_studentapplication.FName,''),IFNULL(tbl_studentapplication.MName,''),IFNULL(tbl_studentapplication.LName,'')) as Candidatename","DATE_FORMAT(tbl_studentapplication.DateOfBirth, '%d-%m-%Y') as DOB","DATE_FORMAT(tbl_studentapplication.DateTime, '%d-%m-%Y') as DateTime","tbl_studentapplication.ICNO","tbl_studentapplication.EmailAddress","tbl_studentapplication.pass"))
					 				 ->join(array("tbl_programmaster"=>"tbl_programmaster"),"tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster",array("tbl_programmaster.ProgramName"))
					 				 ->join(array("tbl_managesession"=>"tbl_managesession"),"tbl_studentapplication.Examsession = tbl_managesession.idmangesession",array("tbl_managesession.managesessionname","tbl_managesession.starttime"))
					 				 ->join(array("tbl_center"=>"tbl_center"),"tbl_studentapplication.Examvenue= tbl_center.idcenter",array("tbl_center.centername"))
					 				 ->where('tbl_registereddetails.RegistrationPin = ?',$varOthers)
					 				 ->where('tbl_registereddetails.Approved=1')
					 				 ->order('tbl_studentapplication.FName')
					 				 ->group('tbl_registereddetails.IDApplication');
//echo 	$strQuery;
//die();
					 				 break;
					 				 
				// To get Count of Candidates				
	   			case 5: $strQuery = $lobjDbAdpt->select()->from(array("tbl_batchregistration" =>"tbl_batchregistration"),array('tbl_batchregistration.totalNoofCandidates','tbl_batchregistration.idBatchRegistration',"tbl_batchregistration.AdhocVenue"))
										->joinLeft(array("tbl_registereddetails" =>"tbl_registereddetails"),'tbl_batchregistration.registrationPin = tbl_registereddetails.RegistrationPin and tbl_registereddetails.Approved=1',array('count(distinct tbl_registereddetails.idregistereddetails) as totalregistered'))
										//->joinLeft(array("tbl_tempexcelcandidates" =>"tbl_tempexcelcandidates"),'tbl_batchregistration.registrationPin = tbl_tempexcelcandidates.RegistrationPin and tbl_tempexcelcandidates.ICNO not in (select ICNO from tbl_studentapplication where batchpayment = "1" and Examvenue !="000")',array('count(distinct tbl_tempexcelcandidates.idcandidates) as totalcount'))
										->where('tbl_batchregistration.registrationPin =?',$varOthers);
	
					 				 break;
					 				 
				//To get Details of a particular pin in takaful
				case 6:	$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator as IdCompany","tbl_takafuloperator.TakafulName as CompanyName","tbl_takafuloperator.TakafulShortName as ShortName","tbl_takafuloperator.paddr1 as Address","tbl_takafuloperator.workphone as Phone1","tbl_takafuloperator.email as Email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.businesstype = tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
					 				 ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state = tbl_state.idState",array("tbl_state.StateName"))
					 				 //->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city = tbl_city.idCity",array("tbl_city.CityName"))
					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 //->where("tbl_city.Active = 1") 
					 				 ->where('tbl_takafuloperator.idtakafuloperator = ?',$varOthers)
									 ->order("tbl_takafuloperator.TakafulName");
								break;	
								
				// To get Details of the takaful on click of the hyper link				
	   			case 7: $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator as IdCompany","tbl_takafuloperator.TakafulName as CompanyName","tbl_takafuloperator.TakafulShortName as ShortName","tbl_takafuloperator.paddr1 as Address","tbl_takafuloperator.workphone as Phone1","tbl_takafuloperator.email as Email"))
					 				 ->join(array("tbl_batchregistration"=>"tbl_batchregistration"),"tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany",array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","tbl_batchregistration.paymentStatus","tbl_batchregistration.UpdDate","tbl_batchregistration.AdhocVenue"))
					 				 ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_batchregistration.idBatchRegistration = tbl_studentpaymentoption.IDApplication",array("tbl_studentpaymentoption.ModeofPayment","tbl_studentpaymentoption.*"))
					 				 ->joinLeft(array("m"=>"tbl_registereddetails"),"tbl_batchregistration.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pin","m.Approved as approve"))
									 ->where('tbl_takafuloperator.idtakafuloperator = ?',$varOthers)
					 				 ->where('tbl_studentpaymentoption.companyflag=2')
									  ->group("tbl_batchregistration.idBatchRegistration")
					 				   ->order('tbl_batchregistration.UpdDate DESC')
  ->order('m.Approved');	
					 				 break;
					 				 
			// To get takaful details	 				 
			  case 8:$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.TakafulName","tbl_takafuloperator.TakafulShortName","tbl_takafuloperator.paddr1","tbl_takafuloperator.workphone","tbl_takafuloperator.email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.Businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   						 ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state= tbl_state.idState",array("tbl_state.StateName"))
			  						 ->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city= tbl_city.idCity",array("tbl_city.CityName"))					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 ->where("tbl_city.Active = 1") 
									 ->order("tbl_takafuloperator.TakafulName");
								break;
					 				 
	   		}
	   		
	   		//echo $strQuery;
	   		if($varAllOne == '0') 
	   		{
			    
	   			$larrResult = $lobjDbAdpt->fetchRow($strQuery);
				
	   		}
	   		elseif($varAllOne == '1')
	   		{
		   		$larrResult = $lobjDbAdpt->fetchAll($strQuery);
	   		}
			return $larrResult;
	   }
	    public function fngetpaymentdetails($idBatchRegistration,$companyflag)//Function to get program names 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_companies"=>"tbl_companies"),"tbl_companies.IdCompany=tbl_batchregistration.idCompany",array("tbl_companies.CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										 // ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =1")
										 // ->where("tbl_batchregistration.Approved=0")
										   ->where("idBatchRegistration = ?",$idBatchRegistration);
		}else{
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_takafuloperator"=>"tbl_takafuloperator"),"tbl_takafuloperator.idtakafuloperator=tbl_batchregistration.idCompany",array("tbl_takafuloperator.TakafulName AS CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  //->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =2")
										  //->where("tbl_batchregistration.Approved=0")
										  ->where("idBatchRegistration = ?",$idBatchRegistration);
		}				
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
 public function fnGettempregpins($idcomp,$idopt)
	 {
	   //echo $idopt;die();
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     			 if($idopt==1){     			 
				 		$lstrSelect = $lobjDbAdpt->select()
							 				      ->from(array("a"=>"tbl_companies"),array(""))
                                                  ->join(array("b" =>"tbl_batchregistration"),"a.IdCompany=b.idCompany",array("b.*","DATE_FORMAT(b.UpdDate,'%d-%m-%Y') as Date"))
									              ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                                  ->join(array("m"=>"tbl_tempexcelcandidates"),"b.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pins"))	
												  ->where("a.IdCompany=?",$idcomp)
												  ->where("d.companyflag=?",$idopt)
												  ->group("m.RegistrationPin");
								 //echo $lstrSelect;die();				 
							 				   
     			 }else if($idopt==2){
     			 	     $lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("a"=>"tbl_takafuloperator"),array(""))
                                                  ->join(array("b" =>"tbl_batchregistration"),"b.idCompany=a.idtakafuloperator",array("b.*","DATE_FORMAT(b.UpdDate,'%d-%m-%Y') as Date"))
									              ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                                 ->join(array("m"=>"tbl_tempexcelcandidates"),"b.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pins"))	
												  ->where("a.idtakafuloperator=?",$idcomp)
												  ->where("d.companyflag=?",$idopt)
												  ->group("m.RegistrationPin");
											// echo $lstrSelect;die();	  
							 				 
     			 }
     			
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }	
	 public function checkforcredittoibfim($idcompany)
	{
	    $db = Zend_Db_Table::getDefaultAdapter();
		$sql = $db->select()->from('tbl_companycredittoibfim',array('tbl_companycredittoibfim.*'))
								->where('tbl_companycredittoibfim.Idcompany =?',$idcompany)
								->where('tbl_companycredittoibfim.Status =1');
		$resulttc = $db->fetchRow($sql);
	     return $resulttc;
	}
	public function fngetpindetails($idbatchregistration,$companyflag)
    {
	
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount","tbl_batchregistration.UpdUser","tbl_batchregistration.UpdDate"))
										  ->join(array("tbl_companies"=>"tbl_companies"),"tbl_companies.IdCompany=tbl_batchregistration.idCompany",array("tbl_companies.CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										 // ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =1")
										 // ->where("tbl_batchregistration.Approved=0")
										   ->where("tbl_batchregistration.idBatchRegistration = ?",$idbatchregistration);
		}else{
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount","tbl_batchregistration.UpdUser","tbl_batchregistration.UpdDate"))
										  ->join(array("tbl_takafuloperator"=>"tbl_takafuloperator"),"tbl_takafuloperator.idtakafuloperator=tbl_batchregistration.idCompany",array("tbl_takafuloperator.TakafulName AS CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  //->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =2")
										  //->where("tbl_batchregistration.Approved=0")
										  ->where("tbl_batchregistration.idBatchRegistration = ?",$idbatchregistration);
		}	
	
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;	
    }
	public function fnpreviouspayment($larresultdetails,$oldpaymentmode,$oldupddate,$oldupduser,$idbatch,$idcompany,$regpin,$amount,$userid)
{
	/*echo "<pre>";
	print_R($larresultdetails);
	die();*/
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_batchpreviouspaymentdetail";
		$postData = array(		
							'idbatchregistration' =>$idbatch,	
           					'OldModeofPayment' =>$oldpaymentmode,	
           					'OldUpdDate' => $oldupddate, 
							'OldUpdUser' =>$oldupduser,	
           					'NewUpdUser' =>$userid,	
           					'NewUpdDate' =>date('Y-m-d H:i:s'),
							'NewModeofPayment'=>$larresultdetails['paymentdetails'],
							'Reference'=>$larresultdetails['refnumber']
			);
			//print_r($postData);die();
						  $lobjDbAdpt->insert($table,$postData);
						  
						   $where = 'IDApplication = '.$idbatch;
			     $postData1 = array(		
						   	 'ModeofPayment' =>$larresultdetails['paymentdetails']
			    			 															
						);
				$table1 = "tbl_studentpaymentoption";
	            $lobjDbAdpt->update($table1,$postData1,$where);
				
				
				$table2 = "tbl_credittobank";
                $postData2 = array('IdBatch' => $idbatch,	
           					'Regpin' =>$regpin,							
           					'ChkNumber' => $larresultdetails['refnumber'],
         					'ChkAmount'=>$amount,
							'Chkdate' => date('Y-m-d H:i:s'),
                            'Idcompany' => $idcompany,	
           					'Upduser' =>$userid,
							'Upddate' =>date('Y-m-d H:i:s')
         					);	
              			
	            $lobjDbAdpt->insert($table2,$postData2);					
				$table3 = "tbl_creditapprovaldetails";
                $postData3 = array('Idcompany' =>$idcompany,	
           					'Ibbatch' =>$idbatch,	
           					'Upduser' =>$userid,
         					'Upddate'=>date('Y-m-d H:i:s')
         					);					
	            $lobjDbAdpt->insert($table3,$postData3);
	            
	            $resultdetails = self::fngetbatchdetails($idbatch);
	            if($resultdetails['registrationPin']==0)
	            {
	            
				$regpin = self::fngeneraterandom();
	            }
	            else 
	            {
	            	$regpin = $resultdetails['registrationPin'];
	            }
				 $where = 'idBatchRegistration  = '.$idbatch;
						 $postData4 = array(	
												'registrationPin'=>$regpin,
												'Approved' => 1,
                                                'paymentStatus'=>1												
											);
						 $table4 = "tbl_batchregistration";
						 
				$lobjDbAdpt->update($table4,$postData4,$where);			
				
}
public function fngetbatchdetails($idbatch)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_batchregistration"),array("a.*"))
							 				 ->where("idBatchRegistration = ?",$idbatch);
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
}
public function fngeneraterandom()
	{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $length = 10;
    $characters = '0123456789';
    $string = "";    

      for ($p = 0; $p < $length; $p++) {
        $string.= $characters[mt_rand(0, strlen($characters))];
        }

         $lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_registereddetails"),array("Regid"));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
			for($i=0;$i<count($larrResult);$i++)
				 	{
				 		
				 		if($string == $larrResult[$i]['Regid'])
				 		{
				 	           self::fngeneraterandom();
				 	    } 
				 	    
				 	
				 	}
    return $string;
    
	}
	
	
	
	//**********************updated***************
	   public function fnCancelPayment($larrformData,$idcompany,$flag)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		//echo "<pre>";
		//print_r($larrformData);die();
		$table = "tbl_cancelpayment";
		$postData = array ('idBatchRegistration' => $larrformData ['idBatchRegistration'],
		                   'idcompany'=>$idcompany,
						   'Remarks' => $larrformData ['remarks'],
						   'Details' => $larrformData ['chequedetails'],
						   'Regpin' => $larrformData ['pin'],
		    			   'UpdDate' => $larrformData ['UpdDate'], 
		    			   'UpdUser' => $larrformData ['UpdUser'],
						   'Companyflag'=>$flag
		    			  );		
		$lobjDbAdpt->insert ( $table, $postData );
				
		$data = array ('paymentStatus' => 3 );
		$where ['idBatchRegistration = ?'] = $larrformData ['idBatchRegistration'];
		$lobjDbAdpt->update ( 'tbl_batchregistration', $data, $where );
	}
	public function fngetpindetailsformovecandidates($idbatchregistration,$companyflag,$idcomp)
    {
	
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("a.idBatchRegistration","a.registrationPin","a.idBatchRegistration","a.idCompany","a.totalNoofCandidates","a.totalAmount","a.UpdUser","a.UpdDate"))
										  ->join(array("b"=>"tbl_companies"),"b.IdCompany=a.idCompany",array("b.CompanyName"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array("c.companyflag","c.ModeofPayment"))
										 // ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("a.IdCompany =?",$idcomp)
										  ->where("c.companyflag =1")
										  ->where("a.paymentStatus=2")
										   ->where("a.idBatchRegistration != ?",$idbatchregistration);
		}else{
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("a.idBatchRegistration","a.registrationPin","a.idBatchRegistration","a.idCompany","a.totalNoofCandidates","a.totalAmount","a.UpdUser","a.UpdDate"))
										  ->join(array("b"=>"tbl_takafuloperator"),"b.idtakafuloperator=a.idCompany",array("b.TakafulName AS CompanyName"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array("c.companyflag","c.ModeofPayment"))
										  //->where("tbl_batchregistration.paymentStatus=1")
										   ->where("a.IdCompany =?",$idcomp)
										  ->where("c.companyflag =2")
										  ->where("a.paymentStatus=2")
										  ->where("a.idBatchRegistration != ?",$idbatchregistration);
		}	
	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
    }
	public function fngettotalcandidatesforbatch($idbatchregistration,$companyflag)
	{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("a.idBatchRegistration","a.registrationPin","a.idBatchRegistration","a.idCompany","a.totalNoofCandidates","a.totalAmount","a.UpdUser","a.UpdDate"))
										  ->join(array("b"=>"tbl_companies"),"b.IdCompany=a.idCompany",array("b.CompanyName"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array("c.companyflag","c.ModeofPayment"))
										// ->where("b.IdCompany=?",$idcompany)
										  ->where("c.companyflag =1")
										  ->where("a.paymentStatus=2")
										   ->where("a.idBatchRegistration = ?",$idbatchregistration);
		}else{
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("a.idBatchRegistration","a.registrationPin","a.idBatchRegistration","a.idCompany","a.totalNoofCandidates","a.totalAmount","a.UpdUser","a.UpdDate"))
										  ->join(array("b"=>"tbl_takafuloperator"),"b.idtakafuloperator=a.idCompany",array("b.TakafulName AS CompanyName"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array("c.companyflag","c.ModeofPayment"))
										//  ->where("b.idtakafuloperator=?",$idcompany)
										  ->where("c.companyflag =2")
										  ->where("a.paymentStatus=2")
										  ->where("a.idBatchRegistration = ?",$idbatchregistration);
		}	
	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	
	}
	public function fngetpinformovecandidates($idbatchregistration,$companyflag,$idcompany)
    {
	
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("key" => "a.idBatchRegistration","value" =>"a.registrationPin"))
										  ->join(array("b"=>"tbl_companies"),"b.IdCompany=a.idCompany",array())
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										 // ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("c.companyflag =1")
										   ->where("a.idCompany=?",$idcompany)
										  ->where("a.paymentStatus=2")
										   ->where("a.idBatchRegistration != ?",$idbatchregistration);
		}else{
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("key" => "a.idBatchRegistration","value" =>"a.registrationPin"))
										  ->join(array("b"=>"tbl_takafuloperator"),"b.idtakafuloperator=a.idCompany",array())
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										  //->where("tbl_batchregistration.paymentStatus=1")
										   ->where("a.idCompany=?",$idcompany)
										  ->where("c.companyflag =2")
										  ->where("a.paymentStatus=2")
										  ->where("a.idBatchRegistration != ?",$idbatchregistration);
		}	
	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
    }
public function fninsertmovedetails($larresultdetails)
{
		$total = self::gettotalcandidates($larresultdetails['NewRegpin']);
		$totalcandidates   = $total['totalNoofCandidates'];
		$newtotalcandidates = $totalcandidates+$larresultdetails['totalcandidates'];

		$Totalamount = $total['Totalamount'];
		$Newtotalamount = $Totalamount+$larresultdetails['amount'];
		//echo $Newtotalamount;die();
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$table = "tbl_movebatchdetails";
		$postData = array(		
							
						'Idcompany' =>$larresultdetails['idcompany'],	
						'Idbatch' =>$larresultdetails['idbatch'], 
						'Oldregpin' =>$larresultdetails['oldregpin'],	
						'Newregpin' =>$larresultdetails['NewRegpin'],
						'Totalcandidates'=>$larresultdetails['totalcandidates'],						
						'UpdDate' =>date('Y-m-d H:i:s'),
						'UpdUser'=>$larresultdetails['UpdUser']
						
		);
//print_r($postData);die();
	$lobjDbAdpt->insert($table,$postData);
			  
	 $where1 = 'idBatchRegistration = '.$larresultdetails['idbatch'];
	 $postData1 = array('totalNoofCandidates' =>'0',
	 'paymentStatus'=>4,
	 'AdhocVenue'=>'Moved',
	 'totalAmount'=>0.00
			);
	$table1 = "tbl_batchregistration";				
	$lobjDbAdpt->update($table1,$postData1,$where1);
	
	
	 $where3 = 'registrationPin = '.$larresultdetails['NewRegpin'];
	 $postData3 = array('totalNoofCandidates' =>$newtotalcandidates,
	'totalAmount'=>$Newtotalamount
			);
	$table3 = "tbl_batchregistration";				
	$lobjDbAdpt->update($table3,$postData3,$where3);
	
	 $where4 = 'RegistrationPin = '.$larresultdetails['oldregpin'];
	 $postData4 = array('RegistrationPin' =>$larresultdetails['NewRegpin']	
			);
	$table4 = "tbl_registereddetails";				
	$lobjDbAdpt->update($table4,$postData4,$where4);
	}
	public function gettotalcandidates($regpin)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_batchregistration"),array("a.totalNoofCandidates","a.totalAmount as Totalamount"))
					  ->join(array("b"=>"tbl_batchregistrationdetails"),"b.idBatchRegistration=a.idBatchRegistration",array(""))
					   ->where("a.registrationPin =?",$regpin);
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	  
	  return $larrResult;	
	}
	public function gettotalamount($idbatch)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_batchregistration"),array("a.totalNoofCandidates","a.totalAmount as Totalamount"))
					  ->join(array("b"=>"tbl_batchregistrationdetails"),"b.idBatchRegistration=a.idBatchRegistration",array(""))
					   ->where("a.idBatchRegistration =?",$idbatch);
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	  
	  return $larrResult;	
   }
				
	public function fnupdatetotalcandidates($idbatch,$total,$upduser,$lintidcompany,$totalregister)
	 {
	 
	    $programdetails = self::fngetprogramforbatch($idbatch);
	    $idprogram      = $programdetails['idProgram'];
		$amountdetails = self::fnGetProgramFee($idprogram);
		$newamount = $amountdetails['sum(abc.amount)']*$total;

	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
		$data = array ('totalNoofCandidates' =>$total,
		'paymentStatus'=>2,
		'AdhocVenue'=>'Blocked',
		'totalAmount'=>$newamount);
		$where ['idBatchRegistration = ?'] = $idbatch;
		$lobjDbAdpt->update ( 'tbl_batchregistration', $data, $where );
		
		$table1 = "tbl_closedbatchregistration";
		$postData1 = array ('Idbatch' => $idbatch,
		                   'Idcompany'=>$lintidcompany,
						   'Totalcandidates' =>$total,
						   
		    			   'Upddate' => date('Y-m-d H:i:s'), 
		    			   'Upduser' => $upduser,
						   'Totalregistered' =>$totalregister
						  
		    			  );		
		$lobjDbAdpt->insert ($table1,$postData1);
	 
	 }
	 public function fngetprogramforbatch($idbatch)
	{
				    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				   $lstrSelect = $lobjDbAdpt->select()
		                         ->from(array("a" => "tbl_batchregistration"),array("a.totalNoofCandidates"))
								  ->join(array("b"=>"tbl_batchregistrationdetails"),"b.idBatchRegistration=a.idBatchRegistration",array("b.idProgram"))
								   ->where("a.idBatchRegistration =?",$idbatch);
		           $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				  
		          return $larrResult;	
	}
	
	public function fnGetProgramFee($idprog)
	{		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT sum(abc.amount) from 
				(select Rate as amount from tbl_programrate where `EffectiveDate` in 
				(SELECT max(a.EffectiveDate)  from tbl_programrate a,tbl_accountmaster b
				where a.idProgram= $idprog and 
				 b.idAccount= a.IdAccountmaster and  
				 b.Active=1 and a.Active=1 and b.idAccount not in (select t.idAccount from tbl_accountmaster t where t.duringRegistration=1 ) group by a.IdAccountmaster)and idProgram=$idprog and Active=1   and IdAccountmaster not in (select r.idAccount from tbl_accountmaster r where r.duringRegistration=1 ))as abc  ";
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	 public function fnupdateforalreadycompleted($idbatch,$total,$upduser,$lintidcompany,$totalregister)
	 {
	 
	    $programdetails = self::fngetprogramforbatch($idbatch);
	    $idprogram      = $programdetails['idProgram'];
		$amountdetails = self::fnGetProgramFee($idprogram);
		$newamount = $amountdetails['sum(abc.amount)']*$total;

	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
		$data = array ('totalNoofCandidates' =>$total,
		'paymentStatus'=>2,		
		'totalAmount'=>$newamount);
		$where ['idBatchRegistration = ?'] = $idbatch;
		$lobjDbAdpt->update ( 'tbl_batchregistration', $data, $where );
		
		$table1 = "tbl_closedbatchregistration";
		$postData1 = array ('Idbatch' => $idbatch,
		                   'Idcompany'=>$lintidcompany,
						   'Totalcandidates' =>$total,
						   
		    			   'Upddate' => date('Y-m-d H:i:s'), 
		    			   'Upduser' => $upduser,
						   'Totalregistered' =>$totalregister
						  
		    			  );		
		$lobjDbAdpt->insert ($table1,$postData1);
	 
	 }
        public function fndeletebatchid($idbatch,$idRegPin,$idcompany,$companyflag,$upduser)
	{
	 
	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	
        	
				 //$varid = $imagarr['imageid'][$linti];
				
				// $db->query($lstrSelect); 
				 $table = "tbl_deletedbatchid";
				 $postData = array ('Idbatch' => $idbatch,
				           'Regpin'=>$idRegPin,
		                   'Idcompany'=>$idcompany,
						   'Batchflag'=>$companyflag,
		    			   'Upddate' => date('Y-m-d H:i:s'), 
		    			   'Upduser' =>$upduser						  
		    			  );		
		        $lobjDbAdpt->insert ($table,$postData);
				$lstrSelect = "Delete from tbl_batchregistration where idBatchRegistration = $idbatch and registrationPin=$idRegPin"; 
				//echo $lstrSelect;die();
		        $lobjDbAdpt->query($lstrSelect); 
 $res = $lobjDbAdpt->query($lstrSelect); 
				 return $res;
	}


public function fngetpinfordummimonths($idbatch,$companyflag,$idcompany)
{
	
$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("key"=>"a.DateTime","value" =>"DATE_FORMAT(a.DateTime,'%M %Y')"))
										  ->join(array("b"=>"tbl_registereddetails"),"a.IDApplication=b.IDApplication",array())
                                          ->join(array("c"=>"tbl_batchregistration"),"c.registrationPin=b.RegistrationPin",array())
										  //->where("tbl_batchregistration.paymentStatus=1")
										  ->where("a.pass=4")
										  ->where("c.idBatchRegistration =?",$idbatch)
										  ->group("year(a.DateTime)")
										    ->group("month(a.DateTime)");
										 //   echo $lstrSelect;die();
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;		
	
}

public function fngetabsentcandidates($idbatch,$companyflag,$idcompany,$dat)
{
	
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	
	
	if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array(""))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										  ->join(array("d"=>"tbl_registereddetails"),"a.registrationPin=d.RegistrationPin",array("d.Regid"))
										  ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication=d.IDApplication",array("e.*","DATE_FORMAT(e.DateTime, '%d-%m-%Y' ) AS examdate"))
										  ->join(array("m"=>"tbl_programmaster"),"e.Program = m.IdProgrammaster",array("m.ProgramName"))
					 				 	  ->join(array("n"=>"tbl_managesession"),"e.Examsession = n.idmangesession",array("n.managesessionname","n.starttime"))
					 				      ->join(array("p"=>"tbl_center"),"e.Examvenue= p.idcenter",array("p.centername"))
										 // ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("c.companyflag =1")
										  ->where("e.pass=4")
										  ->where("month(e.DateTime)=month('$dat')")
										  	  ->where("year(e.DateTime)=year('$dat')")
										   ->where("a.idCompany=?",$idcompany)
										  ->where("a.paymentStatus=2")
										   ->where("a.idBatchRegistration = ?",$idbatch);
		}else{
			 $lstrSelect = $lobjDbAdpt->select()
										   ->from(array("a" => "tbl_batchregistration"),array(""))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										  ->join(array("d"=>"tbl_registereddetails"),"a.registrationPin=d.RegistrationPin",array("d.Regid"))
										  ->join(array("e"=>"tbl_studentapplication"),"e.IDApplication=d.IDApplication",array("e.*","DATE_FORMAT(e.DateTime, '%d-%m-%Y' ) AS examdate"))
										  ->join(array("m"=>"tbl_programmaster"),"e.Program = m.IdProgrammaster",array("m.ProgramName"))
					 				 	  ->join(array("n"=>"tbl_managesession"),"e.Examsession = n.idmangesession",array("n.managesessionname","n.starttime"))
					 				      ->join(array("p"=>"tbl_center"),"e.Examvenue= p.idcenter",array("p.centername"))
										  //->where("tbl_batchregistration.paymentStatus=1")
										  ->where("a.idCompany=?",$idcompany)
										    ->where("e.pass=4")
										      ->where("month(e.DateTime)=month('$dat')")
										  	  ->where("year(e.DateTime)=year('$dat')")
										  ->where("c.companyflag =2")
										  ->where("a.paymentStatus=2")
										  ->where("a.idBatchRegistration = ?",$idbatch);
		}	
	//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
	
	
}

public function fngetdummionepin($dat)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistration"),array("a.registrationPin"))
										  ->join(array("c"=>"tbl_studentpaymentoption"),"c.IDApplication=a.idBatchRegistration",array())
										  //->where("tbl_batchregistration.paymentStatus=1")
										  ->where("a.idCompany=34")
										  ->where("c.companyflag =2")
										  ->where("a.paymentStatus in (1,2)")
										  ->where("month(a.UpdDate)=month('$dat')")
										   ->where("year(a.UpdDate)=year('$dat')")
										   ->order("a.UpdDate");
										//   echo $lstrSelect;die();
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;	
	
	
}

	public function gettotalamountss($idbatch)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_batchregistration"),array("a.totalNoofCandidates","a.totalAmount"))
					   ->where("a.idBatchRegistration =?",$idbatch);
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	  
	  return $larrResult;	
   }


public function fninsertabsentbatchdetails($post,$companyflag)
{
		//$total = self::gettotalcandidates($larresultdetails['NewRegpin']);
		
	$regpin=$post['newdbpin'];
	
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
	   $lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_batchregistration"),array("a.totalNoofCandidates","a.totalAmount","a.idBatchRegistration"))
					   ->where("a.registrationPin ='$regpin'");
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	   
	   
		$totalcandidates   = $post['totalcandidates'];
		 $totalamount=$post['amount'];
		 if(!empty($totalcandidates))
		 {
		 	if($totalcandidates<=0)
		 	{
		 		$totalcandidates=1;
		 	}
           	$unitamount=$totalamount/$totalcandidates;
		 }
		 else 
		 {
		 	$unitamount=1;
		 }
           	
           	$sturemoved = count($post['stuapp']);
           	
           	$sturemvovedamount=($sturemoved)*($unitamount);
           	
          
           	$frmamount=$totalamount - $sturemvovedamount;
           	$fromcandidate=($totalcandidates) - ($sturemoved);
           	
          
           	//if($sturemoved<)
           //	$tocandidate=$larrResult['totalNoofCandidates']+$sturemoved;
           	
           	
          
           	
           	if($fromcandidate <0)
           	{
           		$fromcandidate=0;
           	}
           	  	if($frmamount <0)
           	{
           		$frmamount=0;
           	}
           	
           	
           	
           		
	   $lstrSelect2 = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_studentapplication"),array("a.*"))
					 ->join(array("b"=>"tbl_registereddetails"),"b.IDApplication=a.IDApplication",array(""))
					  ->where("b.Approved =1")
					    ->where("a.examsession!=000")
					 ->where("b.RegistrationPin ='$regpin'");
	   $larrResult2 = $lobjDbAdpt->fetchAll($lstrSelect2);
	   
	if(empty($larrResult2))
	{
		$tocandidatess=$sturemoved;
		$toamountss=$sturemvovedamount;
	}
	else 
	{
	 	$toamountss=$larrResult['totalAmount']+$sturemvovedamount;
	 	$tocandidatess=$larrResult['totalNoofCandidates']+$sturemoved;
	}
	
	 /*	echo "<pre/>";
           	print_r($post);
           	echo $totalcandidates."<br/>";
           	 	echo $totalamount."<br/>";
           	 	 	echo $sturemoved."<br/>";
           	 	 	 	echo $unitamount."<br/>";
           	 	 	 	echo $frmamount."<br/>";
           	 	 	 	echo $fromcandidate."<br/>";
           	 	 	 	echo $toamountss."<br/>";
           	 	 	 	echo $tocandidatess."<br/>";
           	 	 	 	 	die();*/

	
		//echo $tocandidatess."<br/>";die();
	if(!empty($post['newdbpin']))
	{
	 $where3 = 'registrationPin = '.$post['newdbpin'];
	 $postData3 = array('totalNoofCandidates' =>$tocandidatess,
	'totalAmount'=>$toamountss
			);
	$table3 = "tbl_batchregistration";				
	$lobjDbAdpt->update($table3,$postData3,$where3);
	
           	
	}
	
		$table = "tbl_batchremovedmaster";
		$postData = array(		
							
						'Frmcompanyflag' =>$companyflag,	
						'Frmcomapanyid' =>$post['idcompany'], 
						'Frmregistrationpin' =>$post['oldregpin'],	
						'Tocompanyflag' =>'2',
						'Tocomapanyid'=>'34',
						'Toregistrationpin'=>$post['newdbpin'],
						'Active' =>'1',					
						'Upddate' =>date('Y-m-d H:i:s'),
						'Upduser'=>$post['UpdUser']
						
		);
//print_r($postData);die();
	$lobjDbAdpt->insert($table,$postData);
	  $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_batchremovedmaster','Idbatchremovemaster');			  
	
	$table2 = "tbl_batchremoveddetails";
	
	$table4 = "tbl_registereddetails";	
	  for($stu=0;$stu<count($post['stuapp']);$stu++)
	  {
	  	
		$postData2 = array(		
							
						'Idbatchremovemaster' =>$lastid,	
						'IDApplication' =>$post['stuapp'][$stu], 
						
		);
		$lobjDbAdpt->insert($table2,$postData2);
		
		if(!empty($post['stuapp'][$stu]))
		{
			
		
		 $where4 = 'IDApplication = '.$post['stuapp'][$stu];
	 $postData4 = array('RegistrationPin' =>$post['newdbpin']	
			);
				
	$lobjDbAdpt->update($table4,$postData4,$where4);
		}
		
	  }
//print_r($postData);die();
	
	  
	// 'AdhocVenue'=>'Absent Removed',
	 $where1 = 'idBatchRegistration = '.$post['idbatch'];
	 $postData1 = array('totalNoofCandidates' =>$fromcandidate,
	 'totalAmount'=>$frmamount
			);
	$table1 = "tbl_batchregistration";				
	$lobjDbAdpt->update($table1,$postData1,$where1);
	
	
	
	
	
	
	
	
	
	}
public function fnupdateinvoicegenerated($formdata,$Regpin,$Uniqueid,$iduser,$Idcompany,$Companyflag)
	{
	    $batchid = $Regpin;
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
		$Checkforpin = self::fncheckpindetails($batchid);
		if(empty($Checkforpin))
		{
							$table1 = "tbl_invoicegeneration";
							$postData1 = array ('Invoiceuniqueid'=>$Uniqueid,
						    'Regpin' =>$batchid,
						    'Amount'=>$formdata['Totalamount'],
						    'Totalcandidates'=>$formdata['totalNoofCandidates'],
		    			    'Upddate' => date('Y-m-d H:i:s'), 
		    			    'Upduser' => $iduser,
                            'Idcompany'=>$Idcompany,
							'Flag'=>$Companyflag
						   
						  
		    			  );
		$lobjDbAdpt->insert ($table1,$postData1);
		$lastid = $lobjDbAdpt->lastInsertId('tbl_invoicegeneration','Idinvoice');

                if(strlen($lastid) ==1)
				{
				   $lastidnew = "000".$lastid;
				}
				else if(strlen($lastid) ==2)
				{
				    $lastidnew = "00".$lastid;
				}
				else if(strlen($lastid) ==3)
				{
				    $lastidnew = "0".$lastid;
				}
				else if(strlen($lastid) ==4)
				{
				   $lastidnew = $lastid;
				}	
				$data = array (
				'AdhocVenue'=>'Generated'
				);
				$where ['registrationPin = ?'] = $batchid;
				$lobjDbAdpt->update ('tbl_batchregistration', $data, $where );
				
				$data1 = array (
				'Invoiceuniqueid'=>$Uniqueid.$lastidnew
				);
				$where1 ['Idinvoice = ?'] = $lastid;
				$lobjDbAdpt->update ('tbl_invoicegeneration', $data1, $where1 );
				
				$tableinvoicedetails = "tbl_invoicedetails";
							$invoicedetails = array ('Idinvoicemaster'=>$lastid,
						    'Regpin' =>$batchid,						    
						    'TotalCandidates'=>$formdata['totalNoofCandidates'],
		    			    'Amount'=>$formdata['Totalamount']
						   
						  
		    			  );
		       $lobjDbAdpt->insert ($tableinvoicedetails,$invoicedetails);
		       //$lastid = $lobjDbAdpt->lastInsertId('tbl_invoicegeneration','Idinvoice');
		}
	}
	public function  fncheckpindetails($Regpin)
	{
	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
		                          ->from(array("a" => "tbl_batchregistration"),array("a.totalNoofCandidates"))
								  ->join(array("b"=>"tbl_invoicegeneration"),"a.registrationPin=b.Regpin",array(""))
								  ->where("a.AdhocVenue ='Generated'")								 
								  ->where("a.registrationPin = ?",$Regpin);
		           $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		          return $larrResult;
	}
	//this function already there in main server just give a.*
	
	public function getinvoicedetails($regpin)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_batchregistration"),array("a.totalNoofCandidates","a.totalAmount as Totalamount","a.*"))
					  //->join(array("b"=>"tbl_batchregistrationdetails"),"b.idBatchRegistration=a.idBatchRegistration",array(""))
					  ->join(array("c"=>"tbl_invoicegeneration"),"c.Regpin=a.registrationPin",array("c.*"))
					   ->where("a.registrationPin =?",$regpin);
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	  
	  return $larrResult;	
	}

	public function Fngetactualcandidates($regpin)
	{
	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_registereddetails"=>"tbl_registereddetails"),array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.Regid"))
					 				 ->join(array("tbl_studentapplication"=>"tbl_studentapplication"),"tbl_registereddetails.IDApplication = tbl_studentapplication.IDApplication",array("CONCAT_WS(' ',IFNULL(tbl_studentapplication.FName,''),IFNULL(tbl_studentapplication.MName,''),IFNULL(tbl_studentapplication.LName,'')) as Candidatename","DATE_FORMAT(tbl_studentapplication.DateOfBirth, '%d-%m-%Y') as DOB","DATE_FORMAT(tbl_studentapplication.DateTime, '%d-%m-%Y') as DateTime","tbl_studentapplication.ICNO","tbl_studentapplication.EmailAddress","tbl_studentapplication.pass"))
					 				// ->join(array("tbl_programmaster"=>"tbl_programmaster"),"tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster",array("tbl_programmaster.ProgramName"))
					 				// ->join(array("tbl_managesession"=>"tbl_managesession"),"tbl_studentapplication.Examsession = tbl_managesession.idmangesession",array("tbl_managesession.managesessionname","tbl_managesession.starttime"))
					 				// ->join(array("tbl_center"=>"tbl_center"),"tbl_studentapplication.Examvenue= tbl_center.idcenter",array("tbl_center.centername"))
					 				 ->where('tbl_registereddetails.RegistrationPin = ?',$regpin)
					 				 ->where('tbl_registereddetails.Approved=1')
					 				 ->order('tbl_studentapplication.FName')
					 				 ->group('tbl_registereddetails.IDApplication');
//echo $strQuery;die();
		$larrResult = $lobjDbAdpt->fetchAll($strQuery);	
return $larrResult;	
					 				
	}
public function fngettotalcount($regpin)
	{
	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	      $strQuery = $lobjDbAdpt->select()->from(array("a" =>"tbl_batchregistration"),array("a.totalNoofCandidates"))
										   ->join(array("b" =>"tbl_registereddetails"),'a.registrationPin = b.RegistrationPin',array('count(distinct b.idregistereddetails) as totalregistered'))
										   ->join(array("c" =>"tbl_studentapplication"),'b.IDApplication = c.IDApplication' ,array('count(distinct c.IDApplication) as Totalappln'))
										   ->where("c.batchpayment in (1,2)")
										   ->where("c.Examvenue!=000")									  
										   ->where('a.registrationPin =?',$regpin);
	      $larrResult = $lobjDbAdpt->fetchRow($strQuery);	
          return $larrResult;			  
    }	
 public function fngetreceivermail($idflag)
	{
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			if($idflag ==1)
			{
			    $lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_config"),array("a.ClosingBatchCompany as Duration"));
       		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			}
			else if($idflag ==2)
			{
			    $lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_config"),array("a.ClosingBatchTakaful as Duration"));
       		    $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			}
       		return $larrResult;
	}
	 public function fngetbatchaddress($idcomp,$compflag)
	 {
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	    if($compflag == 1)
		{
			$lstrSelect = $lobjDbAdpt->select()
						             ->from(array("a" => "tbl_companies"),array("a.Postcode","a.ContactPerson as Contact","a.Address","a.Phone1 as Phone","a.Fax"))
									->join(array("b"=>"tbl_businesstype"),"a.Businesstype= b.idbusinesstype",array("b.idbusinesstype","b.BusinessType"))
					 				 ->join(array("c"=>"tbl_state"),"c.idState= a.IdState",array("c.StateName"))
                                      ->join(array("d"=>"tbl_countries"),"d.idCountry= a.IdCountry",array("d.CountryName"))									 
									->where("a.idCompany =?",$idcomp);
		}
		else
		{
			 $lstrSelect = $lobjDbAdpt->select()
							          ->from(array("a" => "tbl_takafuloperator"),array("a.paddr1 as Address","a.ContactName as Contact","a.zipCode as Postcode","a.workphone as Phone"))
									 ->join(array("b"=>"tbl_businesstype"),"a.businesstype= b.idbusinesstype",array("b.idbusinesstype","b.BusinessType"))
					 				 ->join(array("c"=>"tbl_state"),"c.idState= a.state",array("c.StateName"))
									  ->join(array("d"=>"tbl_countries"),"d.IdCountry = a.country",array("d.CountryName"))
									 ->where("a.idtakafuloperator=?",$idcomp);										    ;
		}	
	
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;	
	
	    
	 }
	public function fnconvertnumbertowords($number) {
   
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'Negative ';
    $decimal     = ' Point ';
    $dictionary  = array(
        0                   => 'Zero',
        1                   => 'One',
        2                   => 'Two',
        3                   => 'Three',
        4                   => 'Four',
        5                   => 'Five',
        6                   => 'Six',
        7                   => 'Seven',
        8                   => 'Eight',
        9                   => 'Nine',
        10                  => 'Ten',
        11                  => 'Eleven',
        12                  => 'Twelve',
        13                  => 'Thirteen',
        14                  => 'Fourteen',
        15                  => 'Fifteen',
        16                  => 'Sixteen',
        17                  => 'Seventeen',
        18                  => 'Eighteen',
        19                  => 'Nineteen',
        20                  => 'Twenty',
        30                  => 'Thirty',
        40                  => 'Fourty',
        50                  => 'Fifty',
        60                  => 'Sixty',
        70                  => 'Seventy',
        80                  => 'Eighty',
        90                  => 'Ninety',
        100                 => 'Hundred',
        1000                => 'Thousand',
        1000000             => 'Million',
        1000000000          => 'Billion',
        1000000000000       => 'Trillion',
        1000000000000000    => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );
   
    if (!is_numeric($number)) {
        return false;
    }
   
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
   
    $string = $fraction = null;
   
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
   
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction .self::fnconvertnumbertowords($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = self::fnconvertnumbertowords($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= self::fnconvertnumbertowords($remainder);
            }
            break;
    }
   
    /*if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }*/
   
    return $string;
}
public function fngetbatchinvoicedetails($Regpin)
{
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	      $strQuery = $lobjDbAdpt->select()->from(array("a" =>"tbl_registereddetails"),array(""))
										  // ->join(array("b" =>"tbl_registereddetails"),'a.registrationPin = b.RegistrationPin',array('count(distinct b.idregistereddetails) as totalregistered'))
										   ->join(array("c" =>"tbl_studentapplication"),'a.IDApplication = c.IDApplication' ,array('count(c.IDApplication) as Totalappln','c.Program'))
										  // ->where("c.batchpayment in (1,2)")
										   ->where("a.Approved =1")
										   ->where("c.Examvenue!=000")									  
										   ->where("a.registrationPin ='$Regpin'")
										   ->group("c.Program");
										  // echo $strQuery;die();jkjhhjk
	      $larrResult = $lobjDbAdpt->fetchAll($strQuery);	
          return $larrResult;	
}

public function fngetprogramdetails($idprograms)
{
  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
$lstrSelect = $lobjDbAdpt->select()
						             ->from(array("a" =>"tbl_programmaster"),array("a.*"))
									->join(array("b"=>"tbl_programrate"),"a.IdProgrammaster= b.idProgram",array("b.Rate"))								 
									->where("a.IdProgrammaster in ($idprograms)")
									->group("a.IdProgrammaster");
									 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);	
          return $larrResult;
}

//updated for invoice(13/09/2013)
public function getinvoicenumber($regpin)
{
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $lstrSelect = $lobjDbAdpt->select()
						             ->from(array("a" =>"tbl_invoicegeneration"),array("a.Invoiceuniqueid"))
									->join(array("b"=>"tbl_batchregistration"),"a.Regpin= b.registrationPin",array(""))								 
									->where("a.Regpin =?",$regpin);
									
									 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);	
          return $larrResult;
}	
}
