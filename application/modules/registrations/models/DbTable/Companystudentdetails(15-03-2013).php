<?php
class Registrations_Model_DbTable_Companystudentdetails extends Zend_Db_Table { //Model Class for Company Student Details
	   protected $_name = 'tbl_companies';

	   public function fnGenerateQueries($intQueryType,$varAllOne,$varOthers = NULL)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		switch ($intQueryType)
	   		{ 
	   			// To get the company details
	   			case 1:$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
					 				 ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
					 				 ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 ->where("tbl_city.Active = 1") 
									 ->order("tbl_companies.CompanyName");
								break;
								
				//To get the details of a particular				
	   			case 2:	$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
					 				 ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
					 				 //->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 //->where("tbl_city.Active = 1") 
					 				 ->where('tbl_companies.IdCompany = ?',$varOthers)
									 ->order("tbl_companies.CompanyName");
								break;			
								
				// To get Details of the companies on click of the hyper link				
	   			case 3: $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
					 				 ->join(array("tbl_batchregistration"=>"tbl_batchregistration"),"tbl_companies.IdCompany= tbl_batchregistration.idCompany",array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","tbl_batchregistration.paymentStatus","tbl_batchregistration.UpdDate"))
					 				 ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_batchregistration.idBatchRegistration= tbl_studentpaymentoption.IDApplication",array("tbl_studentpaymentoption.ModeofPayment","tbl_studentpaymentoption.*"))
					 				 ->joinLeft(array("m"=>"tbl_registereddetails"),"tbl_batchregistration.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pin","m.Approved as approve"))
									->where('tbl_companies.IdCompany = ?',$varOthers)
					 				 ->where('tbl_studentpaymentoption.companyflag=1')
									  ->group("tbl_batchregistration.idBatchRegistration")
					 				 ->order('tbl_batchregistration.UpdDate DESC');	
					 				 break;

				// To get Details of the student Details based on the registration pin				
	   			case 4: $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_registereddetails"=>"tbl_registereddetails"),array("tbl_registereddetails.idregistereddetails","tbl_registereddetails.Regid"))
					 				 ->join(array("tbl_studentapplication"=>"tbl_studentapplication"),"tbl_registereddetails.IDApplication = tbl_studentapplication.IDApplication",array("CONCAT_WS(' ',IFNULL(tbl_studentapplication.FName,''),IFNULL(tbl_studentapplication.MName,''),IFNULL(tbl_studentapplication.LName,'')) as Candidatename","DATE_FORMAT(tbl_studentapplication.DateOfBirth, '%d-%m-%Y') as DOB","DATE_FORMAT(tbl_studentapplication.DateTime, '%d-%m-%Y') as DateTime","tbl_studentapplication.ICNO","tbl_studentapplication.EmailAddress","tbl_studentapplication.pass"))
					 				 ->join(array("tbl_programmaster"=>"tbl_programmaster"),"tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster",array("tbl_programmaster.ProgramName"))
					 				 ->join(array("tbl_managesession"=>"tbl_managesession"),"tbl_studentapplication.Examsession = tbl_managesession.idmangesession",array("tbl_managesession.managesessionname","tbl_managesession.starttime"))
					 				 ->join(array("tbl_center"=>"tbl_center"),"tbl_studentapplication.Examvenue= tbl_center.idcenter",array("tbl_center.centername"))
					 				 ->where('tbl_registereddetails.RegistrationPin = ?',$varOthers)
					 				 ->where('tbl_registereddetails.Approved=1')
					 				 ->order('tbl_studentapplication.FName')
					 				 ->group('tbl_registereddetails.IDApplication');	
					 				 break;
					 				 
				// To get Count of Candidates				
	   			case 5: $strQuery = $lobjDbAdpt->select()->from(array("tbl_batchregistration" =>"tbl_batchregistration"),array('tbl_batchregistration.totalNoofCandidates','tbl_batchregistration.idBatchRegistration'))
										->joinLeft(array("tbl_registereddetails" =>"tbl_registereddetails"),'tbl_batchregistration.registrationPin = tbl_registereddetails.RegistrationPin',array('count(distinct tbl_registereddetails.idregistereddetails) as totalregistered'))
										//->joinLeft(array("tbl_tempexcelcandidates" =>"tbl_tempexcelcandidates"),'tbl_batchregistration.registrationPin = tbl_tempexcelcandidates.RegistrationPin and tbl_tempexcelcandidates.ICNO not in (select ICNO from tbl_studentapplication where batchpayment = "1" and Examvenue !="000")',array('count(distinct tbl_tempexcelcandidates.idcandidates) as totalcount'))
										->where('tbl_batchregistration.registrationPin =?',$varOthers);	
					 				 break;
					 				 
				//To get Details of a particular pin in takaful
				case 6:	$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator as IdCompany","tbl_takafuloperator.TakafulName as CompanyName","tbl_takafuloperator.TakafulShortName as ShortName","tbl_takafuloperator.paddr1 as Address","tbl_takafuloperator.workphone as Phone1","tbl_takafuloperator.email as Email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.businesstype = tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
					 				 ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state = tbl_state.idState",array("tbl_state.StateName"))
					 				 //->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city = tbl_city.idCity",array("tbl_city.CityName"))
					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 //->where("tbl_city.Active = 1") 
					 				 ->where('tbl_takafuloperator.idtakafuloperator = ?',$varOthers)
									 ->order("tbl_takafuloperator.TakafulName");
								break;	
								
				// To get Details of the takaful on click of the hyper link				
	   			case 7: $strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator as IdCompany","tbl_takafuloperator.TakafulName as CompanyName","tbl_takafuloperator.TakafulShortName as ShortName","tbl_takafuloperator.paddr1 as Address","tbl_takafuloperator.workphone as Phone1","tbl_takafuloperator.email as Email"))
					 				 ->join(array("tbl_batchregistration"=>"tbl_batchregistration"),"tbl_takafuloperator.idtakafuloperator = tbl_batchregistration.idCompany",array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.registrationPin","tbl_batchregistration.paymentStatus","tbl_batchregistration.UpdDate"))
					 				 ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_batchregistration.idBatchRegistration = tbl_studentpaymentoption.IDApplication",array("tbl_studentpaymentoption.ModeofPayment","tbl_studentpaymentoption.*"))
					 				 ->joinLeft(array("m"=>"tbl_registereddetails"),"tbl_batchregistration.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pin","m.Approved as approve"))
									 ->where('tbl_takafuloperator.idtakafuloperator = ?',$varOthers)
					 				 ->where('tbl_studentpaymentoption.companyflag=2')
									  ->group("tbl_batchregistration.idBatchRegistration")
					 				   ->order('tbl_batchregistration.UpdDate DESC');	
					 				 break;
					 				 
			// To get takaful details	 				 
			  case 8:$strQuery = $lobjDbAdpt->select()
					 				 ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.TakafulName","tbl_takafuloperator.TakafulShortName","tbl_takafuloperator.paddr1","tbl_takafuloperator.workphone","tbl_takafuloperator.email"))
					 				 ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.Businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   						 ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state= tbl_state.idState",array("tbl_state.StateName"))
			  						 ->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city= tbl_city.idCity",array("tbl_city.CityName"))					 				 ->where("tbl_businesstype.Active = 1")
					 				 ->where("tbl_state.Active = 1")
					 				 ->where("tbl_city.Active = 1") 
									 ->order("tbl_takafuloperator.TakafulName");
								break;
					 				 
	   		}
	   		
	   		//echo $strQuery;
	   		if($varAllOne == '0') 
	   		{
			    
	   			$larrResult = $lobjDbAdpt->fetchRow($strQuery);
				
	   		}
	   		elseif($varAllOne == '1')
	   		{
		   		$larrResult = $lobjDbAdpt->fetchAll($strQuery);
	   		}
			return $larrResult;
	   }
	   public function fngetpaymentdetails($idBatchRegistration,$companyflag)//Function to get program names 
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_companies"=>"tbl_companies"),"tbl_companies.IdCompany=tbl_batchregistration.idCompany",array("tbl_companies.CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										 // ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =1")
										 // ->where("tbl_batchregistration.Approved=0")
										   ->where("idBatchRegistration = ?",$idBatchRegistration);
		}else{
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount"))
										  ->join(array("tbl_takafuloperator"=>"tbl_takafuloperator"),"tbl_takafuloperator.idtakafuloperator=tbl_batchregistration.idCompany",array("tbl_takafuloperator.TakafulName AS CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  //->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =2")
										  //->where("tbl_batchregistration.Approved=0")
										  ->where("idBatchRegistration = ?",$idBatchRegistration);
		}				
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	 public function fnupdatetotalcandidates($idbatch,$total,$upduser,$lintidcompany)
	 {
	 
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
		$data = array ('totalNoofCandidates' =>$total,
		'paymentStatus'=>2);
		$where ['idBatchRegistration = ?'] = $idbatch;
		$lobjDbAdpt->update ( 'tbl_batchregistration', $data, $where );
		
		$table1 = "tbl_closedbatchregistration";
		$postData1 = array ('Idbatch' => $idbatch,
		                   'Idcompany'=>$lintidcompany,
						   'Totalcandidates' =>$total,
						   
		    			   'Upddate' => date('Y-m-d H:i:s'), 
		    			   'Upduser' => $upduser
						  
		    			  );		
		$lobjDbAdpt->insert ($table1,$postData1);
	 
	 }
	   public function fnCancelPayment($larrformData,$idcompany,$flag)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		//echo "<pre>";
		//print_r($larrformData);die();
		$table = "tbl_cancelpayment";
		$postData = array ('idBatchRegistration' => $larrformData ['idBatchRegistration'],
		                   'idcompany'=>$idcompany,
						   'Remarks' => $larrformData ['remarks'],
						   'Details' => $larrformData ['chequedetails'],
		    			   'UpdDate' => $larrformData ['UpdDate'], 
		    			   'UpdUser' => $larrformData ['UpdUser'],
						   'Companyflag'=>$flag
		    			  );		
		$lobjDbAdpt->insert ( $table, $postData );
				
		$data = array ('paymentStatus' => 3 );
		$where ['idBatchRegistration = ?'] = $larrformData ['idBatchRegistration'];
		$lobjDbAdpt->update ( 'tbl_batchregistration', $data, $where );
	}	
	  
	    public function fnGettempregpins($idcomp,$idopt)
	 {
	   
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     			 if($idopt==1){     			 
				 		$lstrSelect = $lobjDbAdpt->select()
							 				      ->from(array("a"=>"tbl_companies"),array(""))
                                                  ->join(array("b" =>"tbl_batchregistration"),"a.IdCompany=b.idCompany",array("b.*","DATE_FORMAT(b.UpdDate,'%d-%m-%Y') as Date"))
									              ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                                  ->join(array("m"=>"tbl_tempexcelcandidates"),"b.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pins"))	
												  ->where("a.IdCompany=?",$idcomp)
												  ->where("d.companyflag=?",$idopt)
												  ->group("m.RegistrationPin");
												 
							 				   
     			 }else if($idopt==2){
     			 	     $lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("a"=>"tbl_takafuloperator"),array(""))
                                                  ->join(array("b" =>"tbl_batchregistration"),"b.idCompany=a.idtakafuloperator",array("b.*","DATE_FORMAT(b.UpdDate,'%d-%m-%Y') as Date"))
									              ->join(array("d"=>"tbl_studentpaymentoption"),"d.IDApplication=b.idBatchRegistration",array("d.*"))
                                                 ->join(array("m"=>"tbl_tempexcelcandidates"),"b.registrationPin=m.RegistrationPin",array("m.RegistrationPin as pins"))	
												  ->where("a.idtakafuloperator=?",$idcomp)
												  ->where("d.companyflag=?",$idopt)
												  ->group("m.RegistrationPin");
												  
							 				 
     			 }
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }
		
	   public function fnSearchCompaniesnew($larrformData) 
	   {
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
			   ->joinleft(array("tbl_companypaymenttype"=>"tbl_companypaymenttype"),"tbl_companies.IdCompany=tbl_companypaymenttype.idcompany",array("tbl_companypaymenttype.*"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_companies.CompanyName like "%" ? "%"',$larrformData['field3'])
			   ->where('tbl_companies.ShortName like  "%" ? "%"',$larrformData['field2'])
			   ->where('tbl_companies.Address like "%" ? "%"',$larrformData['field6'])
			   ->where('tbl_companies.Phone1 like "%" ? "%"',$larrformData['field12'])
			   ->where('tbl_companies.Email like "%" ? "%"',$larrformData['field13']);
			

			if(isset($larrformData['field5']) && !empty($larrformData['field5']) ){
				$select = $select->where("tbl_companies.businesstype = ?",$larrformData['field5']);
			}
			
			if(isset($larrformData['field1']) && !empty($larrformData['field1']) ){
				$select = $select->where("tbl_companies.IdState = ?",$larrformData['field1']);
			}
			
			if(isset($larrformData['field8']) && !empty($larrformData['field8']) ){
				$select = $select->where("tbl_companies.City = ?",$larrformData['field8']);
			}
		
			$select ->order("tbl_companies.CompanyName");
			$result = $this->fetchAll($select);
			return $result;
	   }
	   public function fnSearchCompanies($post = array()) 
	   {
	    	
	   	 //Function for searching the Company details and Takaful Details
    	 $db = Zend_Db_Table::getDefaultAdapter();
    
         if($post['field19']==1){
    	 //Search for Company   	 
		 $select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_companies"=>"tbl_companies"),array("tbl_companies.IdCompany","tbl_companies.CompanyName","tbl_companies.ShortName","tbl_companies.RegistrationNo","tbl_companies.Address","tbl_companies.Phone1","tbl_companies.Email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_companies.businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_companies.IdState= tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_companies.City= tbl_city.idCity",array("tbl_city.CityName"))
			   ->joinleft(array("tbl_companypaymenttype"=>"tbl_companypaymenttype"),"tbl_companies.IdCompany=tbl_companypaymenttype.idcompany",array("tbl_companypaymenttype.*"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_companies.CompanyName like "%" ? "%"',$post['field3'])
			   ->where('tbl_companies.ShortName like  "%" ? "%"',$post['field2']);
			   //->where('tbl_companies.Address like "%" ? "%"',$post['field6'])
			  // ->where('tbl_companies.Phone1 like "%" ? "%"',$post['field12'])
			   //->where('tbl_companies.Email like "%" ? "%"',$post['field13'])
			

		/*	if(isset($post['field5']) && !empty($post['field5']) ){
				$select = $select->where("tbl_companies.businesstype = ?",$post['field5']);
			}
			
			if(isset($post['field1']) && !empty($post['field1']) ){
				$select = $select->where("tbl_companies.IdState = ?",$post['field1']);
			}
			
			if(isset($post['field8']) && !empty($post['field8']) ){
				$select = $select->where("tbl_companies.City = ?",$post['field8']);
			}*/
		
			$select ->order("tbl_companies.CompanyName");
			
		
     }else if($post['field19']==2){
    	 	//Search for takaful 12-07-2012    	 	
     		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator as IdCompany","tbl_takafuloperator.TakafulName as CompanyName","tbl_takafuloperator.TakafulShortName as ShortName","tbl_takafuloperator.paddr1 as Address","tbl_takafuloperator.workphone as Phone1","tbl_takafuloperator.email as Email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.Businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state = tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city = tbl_city.idCity",array("tbl_city.CityName"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_takafuloperator.TakafulName like "%" ? "%"',$post['field3'])
			   ->where('tbl_takafuloperator.TakafulShortName like  "%" ? "%"',$post['field2']);
			   
			   //->where('tbl_takafuloperator.paddr1 like "%" ? "%"',$post['field6'])
			   //->where('tbl_takafuloperator.workphone like "%" ? "%"',$post['field12'])
			 //  ->where('tbl_takafuloperator.email like "%" ? "%"',$post['field13']);

			/*if(isset($post['field5']) && !empty($post['field5']) ){
				$select = $select->where("tbl_takafuloperator.Businesstype = ?",$post['field5']);
			}
			
			if(isset($post['field1']) && !empty($post['field1']) ){
				$select = $select->where("tbl_takafuloperator.state = ?",$post['field1']);
			}
			
			if(isset($post['field8']) && !empty($post['field8']) ){
				$select = $select->where("tbl_takafuloperator.city = ?",$post['field8']);
			}*/
			   
			
		$select ->order("tbl_takafuloperator.TakafulName");
    	 
     }
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	
	public function fnSearchTakaful($post = array()){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		 $select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->from(array("tbl_takafuloperator"=>"tbl_takafuloperator"),array("tbl_takafuloperator.idtakafuloperator","tbl_takafuloperator.TakafulName","tbl_takafuloperator.TakafulShortName","tbl_takafuloperator.paddr1","tbl_takafuloperator.workphone","tbl_takafuloperator.email"))
			   ->join(array("tbl_businesstype"=>"tbl_businesstype"),"tbl_takafuloperator.Businesstype= tbl_businesstype.idbusinesstype",array("tbl_businesstype.idbusinesstype","tbl_businesstype.BusinessType"))
			   ->join(array("tbl_state"=>"tbl_state"),"tbl_takafuloperator.state = tbl_state.idState",array("tbl_state.StateName"))
			   ->join(array("tbl_city"=>"tbl_city"),"tbl_takafuloperator.city = tbl_city.idCity",array("tbl_city.CityName"))
			   ->where("tbl_businesstype.Active = 1")
			   ->where("tbl_state.Active = 1")
			   ->where("tbl_city.Active = 1") 
			   ->where('tbl_takafuloperator.TakafulName like "%" ? "%"',$post['field3'])
			   ->where('tbl_takafuloperator.TakafulShortName like  "%" ? "%"',$post['field2'])
			   ->where('tbl_takafuloperator.paddr1 like "%" ? "%"',$post['field6'])
			   ->where('tbl_takafuloperator.workphone like "%" ? "%"',$post['field12'])
			   ->where('tbl_takafuloperator.email like "%" ? "%"',$post['field13']);

			if(isset($post['field5']) && !empty($post['field5']) ){
				$select = $select->where("tbl_takafuloperator.Businesstype = ?",$post['field5']);
			}
			
			if(isset($post['field1']) && !empty($post['field1']) ){
				$select = $select->where("tbl_takafuloperator.state = ?",$post['field1']);
			}
			
			if(isset($post['field8']) && !empty($post['field8']) ){
				$select = $select->where("tbl_takafuloperator.city = ?",$post['field8']);
			}
			
		$select ->order("tbl_takafuloperator.TakafulName");
		$result = $this->fetchAll($select);
		return $result->toArray();
		
	}
	
     public function fngetnoofstudentsfromexcel($ids)
     { // Get remaining number of students in a batch
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_tempexcelcandidates"),array("a.*"))							 				
							 				 ->where("a.RegistrationPin = ?",$ids)
							 				 ->where("a.ICNO not in (select ICNO from tbl_studentapplication)");
							 				// echo $lstrSelect;
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     }
     
     
     public function fnGetOperatorNames($opType,$namestring){
     	
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
     			 if($opType==1){     			 
				 		$lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_companies"),array("CompanyName as name"))							 				
							 				     ->where('CompanyName like ? "%"',$namestring)
							 				   ;
     			 }else if($opType==2){
     			 	    $lstrSelect = $lobjDbAdpt->select()
							 				     ->from(array("tbl_takafuloperator"),array("TakafulName as name"))							 				
							 				     ->where('TakafulName like ? "%"',$namestring);
     			 }
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }
     public function fngetexcelappliedcandidates($ids)
     { // Get excel applied candidates details     	
     	 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_studentapplication"),array("a.FName","a.ICNO","a.DateTime","a.EmailAddress"))	
							 				 ->join(array("e"=>"tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid")) 
							 				 ->join(array("b"=>"tbl_programmaster"),'a.Program = b.IdProgrammaster',array("b.ProgramName")) 
							 				 ->join(array("c"=>"tbl_managesession"),'a.Examsession = c.idmangesession',array("c.managesessionname")) 	
							 				 ->join(array("d"=>"tbl_center"),'a.Examvenue = d.idcenter',array("d.centername")) 						 				
							 				 //->where("a.batchpayment = 1")					
							 				 ->where("e.RegistrationPin = ?",$ids)
							 				 ->order("a.FName");	
							 				 //echo $lstrSelect;	 				 
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;   	
     }
     
	public function fnGetModeofpay($idbatchreg){
		
		     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_studentpaymentoption"),array("ModeofPayment"))
							 				 ->where("IDApplication = ?",$idbatchreg)
							 				 ->where("companyflag = 1");
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
		
	}
	
	public function fnInsertIntoStd($larrformData,$countloop,$batchid,$regpin,$companyaddress,$idcompany,$iduser,$flag)
	{
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //TAkafulmodels model object
		for($i=0;$i<$countloop;$i++)
		{			
		  $randarray=$this->lobjTakafulcandidatesmodel->fngeneraterandompassword();
		  $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_studentapplication";
          
          if(strlen($larrformData['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformData['setmonth'];
          } else {
          	$monthsss = $larrformData['setmonth'];
          }          
		  if(strlen($larrformData['setdate']) <= 1) {
          	$dayssss = '0'.$larrformData['setdate'];
          } else {
          	$dayssss = $larrformData['setdate'];
          }
          
          
          $datetime = $larrformData['Year'].'-'.$monthsss.'-'.$dayssss;
		  $postData = array('StudentId' =>'00',	
           					'FName' =>$larrformData['candidatename'][$i],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['candidatedateofbirth'][$i],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateemail'][$i],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>$iduser,	
		                    'username' =>$larrformData['candidateemail'][$i],	
                            'password' =>$randarray,
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateicno'][$i],	
           					'Payment' => 1, 
							'DateTime' =>$datetime,	
           					'PermAddressDetails' =>$companyaddress,	
           					'Takafuloperator' =>$idcompany, 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' =>$flag, 
							'Gender' =>$larrformData['candidategender'][$i],	
           					'Race' =>$larrformData['candidaterace'][$i],	
		                    'Religion' =>$larrformData['candidatereligion'][$i],	
           					'Qualification' =>$larrformData['candidateeducation'][$i], 
							'State' =>$larrformData['candidatestate'][$i],	
           					'CorrAddress' =>$larrformData['candidateaddress'][$i],	
           					'PostalCode' =>$larrformData['postalcode'][$i], 
		 					'ContactNo' =>$larrformData['candidatenum'][$i],	
           					'MobileNo' =>$larrformData['candidatemobnum'][$i], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  					'Year'=>$larrformData['scheduler'],
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  					'Examvenue'=>$larrformData['NewVenue'],	
		  					'Examsession'=>$larrformData['idsession'],
		                    'pass'=>3	
						);			
						
						
	        $db->insert($table,$postData);	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');	
	        
	        
	       $larrdatainsert=array(
	        					'StudentName' =>$larrformData['candidatename'][$i],	
           						'ICNO' =>$larrformData['candidateicno'][$i],	
           						'Race' =>$larrformData['candidaterace'][$i], 								
           						'email' =>$larrformData['candidateemail'][$i], 
           						'education' =>$larrformData['candidateeducation'][$i],	
           						'Gender' =>$larrformData['candidategender'][$i], 
		                  		'DOB'=>$larrformData['candidatedateofbirth'][$i],		    											
	        					'Address' =>$larrformData['candidateaddress'][$i],		    					
		    					'CorrespAddress'=>$larrformData['candidateaddress'][$i],
		    					'PostalCode'=>$larrformData['postalcode'][$i],
		    					'IdCountry'=>0,
	       						'idprogram'=>$larrformData['Program'],
		    					'IdState'=>$larrformData['candidatestate'][$i],	
		    					'ContactNo'=>$larrformData['candidatenum'][$i],
		    					'MobileNo'=>$larrformData['candidatemobnum'][$i],
	        );
	        
			//inserting into temp registration excel table    18-06-2012
	        self::fninsertintotemp($larrdatainsert,0,$regpin);
	     	        
	        $larrresult = $this->lobjTakafulcandidatesmodel->fnviewstudentdetails($lastid);        
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($idcompany,$lastid);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['setactive'],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>1,
            				'Cetreapproval'=>0,
            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);
	       
	     
	   //  $this->lobjTakafulcandidatesmodel->sendmails($larrresult,$regid);
	       
		}
		
		$larrresultbatchcount=$this->lobjTakafulcandidatesmodel->fngettotalcountinbatchregistration($regpin);		
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_registereddetails',array('count(idregistereddetails) as totapplied'))
								->where('RegistrationPin =?',$regpin);
			$resulttc = $db->fetchRow($sql);
			
			
		//echo $resulttc['totapplied'];exit;
		if($resulttc['totapplied'] < $larrresultbatchcount['totalNoofCandidates']){
		     $paymentstatus=1;
		}else{
			 $paymentstatus=2;
			 $this->lobjTakafulcandidatesmodel->fndeletefromtempexcelcandidates($regpin);	  //new function to delete redundant data from tempescelcandidates
		} 

		$where = 'idBatchRegistration = '.$batchid;
		$postData = array(		
			   	 'paymentStatus' =>$paymentstatus,
				 'Approved' => 1															
				);
		$table = "tbl_batchregistration";
	    $db->update($table,$postData,$where);
	    
	    //updating table tbl_venuedateschedule
	    $this->lobjTakafulcandidatesmodel->fnupdatevenuedateschedule($larrformData,2);	    
	}
	
	public function fninsertintotemp($larrdatainsert,$idtakaful,$idregpin) 
	{ 
		// Function to enter into tbl_tempexcelcandidates		
		
			$sessionID = Zend_Session::getId();
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_tempexcelcandidates";
		    $postData = array(				    
							'StudentName' =>$larrdatainsert['StudentName'],	
           					'ICNO' =>$larrdatainsert['ICNO'],	
           					'Race' =>$larrdatainsert['Race'], 
							'Takafuloperator' =>$idtakaful,	
           					'idprogram' =>'0',	
           					'email' =>$larrdatainsert['email'], 
           					'education' =>$larrdatainsert['education'],	
           					'Gender' =>$larrdatainsert['Gender'], 
		                    'DOB'=>$larrdatainsert['DOB'],
		    				'session'=>$sessionID,
							'Address' =>$larrdatainsert['Address'],
		    				'RegistrationPin'=>$idregpin,
		    				'CorrespAddress'=>$larrdatainsert['CorrespAddress'],
		    				'PostalCode'=>$larrdatainsert['PostalCode'],
		    				'IdCountry'=>$larrdatainsert['IdCountry'],
		    				'IdState'=>$larrdatainsert['IdState'],	
		    				'ContactNo'=>$larrdatainsert['ContactNo'],
		    				'MobileNo'=>$larrdatainsert['MobileNo'],		    				
		    );
		      $db->insert($table,$postData);
		    
	}
	
	
	
	public function fnInsertintostudapplicationexcel($larrformData,$lintidbatch,$linttotnumofapplicant,$regpin,$idCompany,$iduser,$flag){
	
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //TAkafulmodels model object
		
		/*echo "<pre>";		
		print_r($avalus);		
		exit;*/
		
			$avalus = array_values($larrformData['studenttakful']);
		for($insertfrmexcel=0;$insertfrmexcel<count($avalus);$insertfrmexcel++)
		{		
		  $randarray=$this->lobjTakafulcandidatesmodel->fngeneraterandompassword();
		  $dob=date("Y-m-d",strtotime($larrformData['candidateDOBtakaful'][$larrformData['studenttakful'][$insertfrmexcel]]));
		  $db = Zend_Db_Table::getDefaultAdapter();		  
		  
		 if(strlen($larrformData['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformData['setmonth'];
          } else {
          	$monthsss = $larrformData['setmonth'];
          }         
          
		  if(strlen($larrformData['setdate']) <= 1) {
          	$dayssss = '0'.$larrformData['setdate'];
          } else {
         	$dayssss = $larrformData['setdate'];
          }
          
          $datetime = $larrformData['Year'].'-'.$monthsss.'-'.$dayssss;		  		  
          $table = "tbl_studentapplication";
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['candidatenametakaful'][$avalus[$insertfrmexcel]],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$dob,	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateEMAILtakaful'][$avalus[$insertfrmexcel]],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>$iduser,	
		                    'username' =>$larrformData['candidateEMAILtakaful'][$avalus[$insertfrmexcel]],	
                            'password' =>$randarray,
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],	
           					'Payment' => 1, 
							'DateTime' =>$datetime,	
           					'PermAddressDetails' =>$larrformData['Address'][$avalus[$insertfrmexcel]],	
           					'Takafuloperator' =>$idCompany, 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' =>$flag, 
							'Gender' =>$larrformData['Gender'][$avalus[$insertfrmexcel]],	
           					'Race' =>$larrformData['Race'][$avalus[$insertfrmexcel]],	
		                    'Religion' =>144,	
           					'Qualification' =>$larrformData['education'][$avalus[$insertfrmexcel]], 
							'State' =>$larrformData['IdState'][$avalus[$insertfrmexcel]],	
           					'CorrAddress' =>$larrformData['CorrespAddress'][$avalus[$insertfrmexcel]],	
           					'PostalCode' =>$larrformData['PostalCode'][$avalus[$insertfrmexcel]], 
		 					'ContactNo' =>$larrformData['ContactNo'][$avalus[$insertfrmexcel]],	
           					'MobileNo' =>$larrformData['MobileNo'][$avalus[$insertfrmexcel]], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  					'Year'=>$larrformData['scheduler'],
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  					'Examvenue'=>$larrformData['Examvenue'],	
		  					'Examsession'=>$larrformData['idsession'],
		                    'pass'=>3		  
						);	
							
	        $db->insert($table,$postData);
	        	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');	        
	        	
	        $larrresult = $this->lobjTakafulcandidatesmodel->fnviewstudentdetails($lastid);	        
	       
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($idCompany,$lastid);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['setactive'],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>1,
            				'Cetreapproval'=>0,
            				'RegistrationPin'=>$larrformData['idbatch']
            				);	
            						
	       $db->insert($tables,$postDatas);       
	      // $this->lobjTakafulcandidatesmodel->sendmails($larrresult,$regid);	  
		  $this->lobjTakafulcandidatesmodel->fndeletefromtempexcelcandidates($avalus[$insertfrmexcel]);	  //new function to delete redundant data from tempescelcandidates added on 1-3-2013
	       
		}		
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_registereddetails',array('count(idregistereddetails) as totapplied'))
								->where('RegistrationPin =?',$regpin);
			$resulttc = $db->fetchRow($sql);
			
		//echo $resulttc['totapplied'];exit;
		if($resulttc['totapplied'] < $linttotnumofapplicant){
		     $paymentstatus=1;
		}else{
			 $paymentstatus=2;
			  
		}
		
			     $where = 'idBatchRegistration = '.$lintidbatch;
			     $postData = array(		
						   	 'paymentStatus' =>$paymentstatus,
			    			 'Approved' => 1															
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	            
	    
	            //updating table tbl_venuedateschedule
	            $this->lobjTakafulcandidatesmodel->fnupdatevenuedateschedule($larrformData,1);
	            
		
	}
	public function checkforcredittoibfim($idcompany)
	{
	    $db = Zend_Db_Table::getDefaultAdapter();
		$sql = $db->select()->from('tbl_companycredittoibfim',array('tbl_companycredittoibfim.*'))
								->where('tbl_companycredittoibfim.Idcompany =?',$idcompany)
								->where('tbl_companycredittoibfim.Status =1');
		$resulttc = $db->fetchRow($sql);
	     return $resulttc;
	}
	public function fngetpindetails($idbatchregistration,$companyflag)
    {
	
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	if($companyflag == 1){
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount","tbl_batchregistration.UpdUser","tbl_batchregistration.UpdDate"))
										  ->join(array("tbl_companies"=>"tbl_companies"),"tbl_companies.IdCompany=tbl_batchregistration.idCompany",array("tbl_companies.CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										 // ->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =1")
										 // ->where("tbl_batchregistration.Approved=0")
										   ->where("tbl_batchregistration.idBatchRegistration = ?",$idbatchregistration);
		}else{
			 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration" => "tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration","tbl_batchregistration.registrationPin","tbl_batchregistration.idBatchRegistration","tbl_batchregistration.idCompany","tbl_batchregistration.totalNoofCandidates","tbl_batchregistration.totalAmount","tbl_batchregistration.UpdUser","tbl_batchregistration.UpdDate"))
										  ->join(array("tbl_takafuloperator"=>"tbl_takafuloperator"),"tbl_takafuloperator.idtakafuloperator=tbl_batchregistration.idCompany",array("tbl_takafuloperator.TakafulName AS CompanyName"))
										  ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),"tbl_studentpaymentoption.IDApplication=tbl_batchregistration.idBatchRegistration",array("tbl_studentpaymentoption.companyflag","tbl_studentpaymentoption.ModeofPayment"))
										  //->where("tbl_batchregistration.paymentStatus=1")
										  ->where("tbl_studentpaymentoption.companyflag =2")
										  //->where("tbl_batchregistration.Approved=0")
										  ->where("tbl_batchregistration.idBatchRegistration = ?",$idbatchregistration);
		}	
	
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;	
    }
	public function fnpreviouspayment($larresultdetails,$oldpaymentmode,$oldupddate,$oldupduser,$idbatch,$idcompany,$regpin,$amount,$userid)
{
	/*echo "<pre>";
	print_R($larresultdetails);
	die();*/
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_batchpreviouspaymentdetail";
		$postData = array(		
							'idbatchregistration' =>$idbatch,	
           					'OldModeofPayment' =>$oldpaymentmode,	
           					'OldUpdDate' => $oldupddate, 
							'OldUpdUser' =>$oldupduser,	
           					'NewUpdUser' =>$userid,	
           					'NewUpdDate' =>date('Y-m-d H:i:s'),
							'NewModeofPayment'=>$larresultdetails['paymentdetails'],
							'Reference'=>$larresultdetails['refnumber']
			);
			//print_r($postData);die();
						  $lobjDbAdpt->insert($table,$postData);
						  
						   $where = 'IDApplication = '.$idbatch;
			     $postData1 = array(		
						   	 'ModeofPayment' =>$larresultdetails['paymentdetails']
			    			 															
						);
				$table1 = "tbl_studentpaymentoption";
	            $lobjDbAdpt->update($table1,$postData1,$where);
				
				
				$table2 = "tbl_credittobank";
                $postData2 = array('IdBatch' => $idbatch,	
           					'Regpin' =>$regpin,							
           					'ChkNumber' => $larresultdetails['refnumber'],
         					'ChkAmount'=>$amount,
							'Chkdate' => date('Y-m-d H:i:s'),
                            'Idcompany' => $idcompany,	
           					'Upduser' =>$userid,
							'Upddate' =>date('Y-m-d H:i:s')
         					);	
              			
	            $lobjDbAdpt->insert($table2,$postData2);					
				$table3 = "tbl_creditapprovaldetails";
                $postData3 = array('Idcompany' =>$idcompany,	
           					'Ibbatch' =>$idbatch,	
           					'Upduser' =>$userid,
         					'Upddate'=>date('Y-m-d H:i:s')
         					);					
	            $lobjDbAdpt->insert($table3,$postData3);
				
				 $where = 'idBatchRegistration  = '.$idbatch;
						 $postData4 = array(		
												'Approved' => 1,
                                                'paymentStatus'=>1												
											);
						 $table4 = "tbl_batchregistration";
						 
				$lobjDbAdpt->update($table4,$postData4,$where);
				
				
				
				
}
}