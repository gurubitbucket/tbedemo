<?php
	class Registrations_Form_Paymentdistribution extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
	    $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$yesterdaydate1= date('Y-m-d', mktime(0,0,0,$month,($day+14),$year));
		$dateofbirth1 = "{min:'$yesterdaydate1',datePattern:'dd-MM-yyyy'}"; 
		
		$operator = new Zend_Dojo_Form_Element_FilteringSelect('operator');        
        $operator ->removeDecorator("DtDdWrapper");
        $operator ->addMultiOption('','Select');
        $operator->setAttrib('required',"true");
        $operator ->removeDecorator("Label");        
        $operator ->removeDecorator('HtmlTag')
        		 ->setAttrib('class', 'txt_put') ;
        $operator ->setRegisterInArrayValidator(false);
		$operator ->setAttrib('dojoType',"dijit.form.FilteringSelect");
		 
		$operatorname = new Zend_Form_Element_Text('operatorname');
        $operatorname ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $operatorname->setAttrib('required',"true");
      	$operatorname->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        		//->setAttrib('onblur', 'fnSearchdistrubution(this.value);')
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
		
		$ChequeDate = new Zend_Dojo_Form_Element_DateTextBox('ChequeDate');
		$ChequeDate->setAttrib('required', 'true');
		$ChequeDate->setAttrib('dojoType',"dijit.form.DateTextBox")
			->setAttrib('title',"dd-mm-yyyy")
			->removeDecorator("Label")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag');
		$ChequeDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
			
			
		$ReceiptDate = new Zend_Dojo_Form_Element_DateTextBox('ReceiptDate');
		$ReceiptDate->setAttrib('dojoType',"dijit.form.DateTextBox")
			->setAttrib('title',"dd-mm-yyyy")
			->setvalue(date("Y-m-d"))
			->removeDecorator("Label")
			->setAttrib('required',"true")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag');
		$ReceiptDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
						
		

		$submit = new Zend_Form_Element_Button('Distribute');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Distribute The Amount");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->setAttrib('onclick', 'fndistribute();');
		$submit->removeDecorator('HtmlTag');
			//->class = "NormalBtn";
			
		$Save = new Zend_Form_Element_Submit('Save');
		$Save->dojotype="dijit.form.Button";
		$Save->label = $gstrtranslate->_("Save");
		$Save->removeDecorator("DtDdWrapper");
		$Save->removeDecorator("Label");
		$Save->removeDecorator('HtmlTag')
			->class = "NormalBtn";
			
		$Branchname = new Zend_Form_Element_Text('Branchname');
		$Branchname ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Branchname->setAttrib('required', 'true');
        $Branchname->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $ChequeNumber = new Zend_Form_Element_Text('ChequeNumber');
		$ChequeNumber ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$ChequeNumber->setAttrib('required', 'true');
        $ChequeNumber->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
		$PaymentMode = new Zend_Dojo_Form_Element_FilteringSelect('PaymentMode');        
        $PaymentMode ->removeDecorator("DtDdWrapper");
        //$PaymentMode ->addMultiOption('','Select');
        //$PaymentMode->setAttrib ( 'placeholder', 'Select' );
        $PaymentMode->addMultiOptions(array('1' => 'Cheque','2' =>"Cash"));
        $PaymentMode ->removeDecorator("Label");        
        $PaymentMode ->removeDecorator('HtmlTag')
        		 ->setAttrib('class', 'txt_put') ;
        $PaymentMode ->setRegisterInArrayValidator(false);
        $PaymentMode->setAttrib('onchange', 'fnhideboxes();');
		$PaymentMode ->setAttrib('dojoType',"dijit.form.FilteringSelect");

		$Amount = new Zend_Form_Element_Text('Amount');
		$Amount ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Amount->setAttrib('readonly', 'true');  
        $Amount->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $TotalAmountReceived = new Zend_Form_Element_Text('TotalAmountReceived');
		$TotalAmountReceived ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $TotalAmountReceived->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        		->setAttrib('onkeyup', 'fnfillamountbox();')  
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $TotalPayableAmount = new Zend_Form_Element_Text('TotalPayableAmount');
		$TotalPayableAmount ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $TotalPayableAmount->setAttrib('class', 'txt_put');
        $TotalPayableAmount->setAttrib('readonly', 'true')          
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $chk  = new Zend_Form_Element_Checkbox('chk');
        $chk->setAttrib('dojoType',"dijit.form.CheckBox");
        $chk->setvalue('1');
        $chk->removeDecorator("DtDdWrapper");
        $chk->removeDecorator("Label");
        $chk->removeDecorator('HtmlTag');
				
	  $this->addElements(array($submit,$operatorname,$operator,$ChequeNumber,$Branchname,$Save,
	  			$ChequeDate,$ReceiptDate,$PaymentMode,$Amount,$TotalAmountReceived,$TotalPayableAmount,
	  			$chk
        				)
        			);
		}
}
