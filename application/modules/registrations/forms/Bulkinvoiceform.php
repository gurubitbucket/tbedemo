<?php
class Registrations_Form_Bulkinvoiceform extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ('Zend_Translate');
		
		
		
		$Pin = new Zend_Dojo_Form_Element_FilteringSelect ('Pin');
		$Pin->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Pin->removeDecorator("DtDdWrapper");
		$Pin->removeDecorator("Label");
		$Pin->removeDecorator('HtmlTag');
		$Pin->setAttrib('required',"true");
		
		$Fromdate = new Zend_Dojo_Form_Element_DateTextBox ('Fromdate');
		$Fromdate->setAttrib ('dojoType', "dijit.form.DateTextBox");
		$Fromdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	    $Fromdate->setAttrib('title',"dd-mm-yyyy");
		$Fromdate->setAttrib('onChange', "examtoDateSetting();");
	    $Fromdate->removeDecorator("Label");
		$Fromdate->removeDecorator("DtDdWrapper");
		$Fromdate->removeDecorator('HtmlTag');
		      
		$Todate = new Zend_Dojo_Form_Element_DateTextBox ('Todate');
		$Todate->setAttrib ('dojoType', "dijit.form.DateTextBox");
		$Todate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Todate->setAttrib ('title', "dd-mm-yyyy");
		$Todate->setAttrib ('required', "false");
		$Todate->removeDecorator ("Label");
		$Todate->removeDecorator ("DtDdWrapper");
		$Todate->removeDecorator ('HtmlTag');
		
		$Venue = new Zend_Dojo_Form_Element_FilteringSelect ('Venue');
		$Venue->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$Venue->setAttrib('required',"false");
		$Venue->removeDecorator ("DtDdWrapper");
		$Venue->removeDecorator ("Label" );
		$Venue->removeDecorator ('HtmlTag');
		
		$Course = new Zend_Dojo_Form_Element_FilteringSelect ( 'Course' );
		$Course->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Course->setAttrib ( 'required', "true" );
		$Course->removeDecorator ( "DtDdWrapper" );
		$Course->removeDecorator ( "Label" );
		$Course->removeDecorator ( 'HtmlTag' );
		
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
         	   
        $excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
		
		//$Takcompnames->setAttrib ('onChange',"fngetpin();");
		
		$BatchIds = new Zend_Dojo_Form_Element_FilteringSelect ('BatchIds');
		$BatchIds->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		//$BatchIds->setAttrib ('onChange',"fngetpin();");
		$BatchIds->removeDecorator ("DtDdWrapper");
		$BatchIds->removeDecorator("Label");
		$BatchIds->removeDecorator('HtmlTag');
		$BatchIds->setAttrib ('required',"true");
		
		$Clear = new Zend_Form_Element_Button('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Search = new Zend_Form_Element_Submit('Search');
        $Search->dojotype="dijit.form.Button";
        $Search->label = $gstrtranslate->_("Search");
        $Search->removeDecorator("DtDdWrapper");
        $Search->removeDecorator("Label");
        $Search->removeDecorator('HtmlTag')
         		->class = "NormalBtn";	

        $Takcomp = new Zend_Dojo_Form_Element_FilteringSelect ('Takcomp');
		$Takcomp->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Takcomp->removeDecorator ("DtDdWrapper");
		$Takcomp->removeDecorator("Label");
               $Takcomp ->addMultiOption('','Select');
		$Takcomp->removeDecorator('HtmlTag');
		$Takcomp->setAttrib('required',"true");
		$Takcomp->setAttrib ('onChange',"fntakcompnames();");
		
		$Takcompnames = new Zend_Dojo_Form_Element_FilteringSelect ('Takcompnames');
		$Takcompnames->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Takcompnames->setAttrib ('onChange',"fngetpin();");
		$Takcompnames->setAttrib('style','width:200px;');
		//$Takcompnames->setAttrib ('onChange',"fngetpin();");
		$Takcompnames->removeDecorator ("DtDdWrapper");
		$Takcompnames->removeDecorator("Label");
		$Takcompnames->removeDecorator('HtmlTag');
		$Takcompnames->setAttrib ('required',"true");

		$Add = new Zend_Form_Element_Button('Add');
		$Add->dojotype="dijit.form.Button";
		$Add->label = "Add";	
		$Add->setAttrib('OnClick', 'addcompanyDetails()')
					 ->setAttrib('class', 'NormalBtn')
					 ->removeDecorator("Label")
					 ->removeDecorator("DtDdWrapper")
					 ->removeDecorator('HtmlTag');
					 
		$Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";

        $Next = new Zend_Form_Element_Submit('Next');
        $Next->dojotype="dijit.form.Button";
        $Next->label = $gstrtranslate->_("Next");
        $Next->removeDecorator("DtDdWrapper");
        $Next->removeDecorator("Label");
        $Next->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";			 			   
		
		$this->addElements ( array ($Takcomp,
		                            $Takcompnames,
		                            $Pin,
		                            $Fromdate, 
		                            $Todate,
		                            $excel, 
		                            $submit,
		                            $Venue,
									$Add,
		                            $Course,$BatchIds,$Search,$Clear,$Save,$Next)
		                   );
	}
}
