<?php
class Registrations_Form_Studentupdate extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ('Zend_Translate');
				
		$Name = new Zend_Form_Element_Text('Name');
		$Name ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Name ->setAttrib('class','txt_put');  
	    $Name ->removeDecorator("DtDdWrapper");
	    $Name ->removeDecorator("Label");
	    $Name ->removeDecorator('HtmlTag');	
		
		$idapp  = new Zend_Form_Element_Hidden('idapp');
        $idapp ->removeDecorator("DtDdWrapper");
        $idapp ->removeDecorator("Label");
        $idapp ->removeDecorator('HtmlTag');
		
        $Dob = new Zend_Dojo_Form_Element_DateTextBox('DateOfBirth');
	    $Dob->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');		
		
		$FName = new Zend_Form_Element_Text('FName');
		$FName ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $FName ->setAttrib('class','txt_put');  
	    $FName ->removeDecorator("DtDdWrapper");
	    $FName ->removeDecorator("Label");
	    $FName ->removeDecorator('HtmlTag');
		
		$ProgramName = new Zend_Form_Element_Text('ProgramName');
		$ProgramName ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $ProgramName ->setAttrib('class','txt_put');  
	    $ProgramName ->removeDecorator("DtDdWrapper");
	    $ProgramName ->removeDecorator("Label");
	    $ProgramName ->removeDecorator('HtmlTag');
		
		$centername = new Zend_Form_Element_Text('centername');
		$centername ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $centername ->setAttrib('class','txt_put');  
	    $centername ->removeDecorator("DtDdWrapper");
	    $centername ->removeDecorator("Label");
	    $centername ->removeDecorator('HtmlTag');
		
		$managesessionname = new Zend_Form_Element_Text('managesessionname');
		$managesessionname ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $managesessionname ->setAttrib('class','txt_put');  
	    $managesessionname ->removeDecorator("DtDdWrapper");
	    $managesessionname ->removeDecorator("Label");
	    $managesessionname ->removeDecorator('HtmlTag');
		
		
		$Icno = new Zend_Form_Element_Text('ICNO1');
		$Icno ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Icno ->setAttrib('class','txt_put');  
	    $Icno ->removeDecorator("DtDdWrapper");
		//$Icno ->setAttrib('required',"true");  
	    $Icno ->removeDecorator("Label");
	    $Icno ->removeDecorator('HtmlTag');
		
		
		$Icno1 = new Zend_Form_Element_Text('ICNO',array('regExp'=>"[0-9]{2}(0{1}[1-9]{1}|1[012]{1})(0{1}[1-9]{1}|1[0-9]{1}|2[0-9]{1}|3[01]{1})[0-9]{6}$",'invalidMessage'=>"Please Enter First 6 Digits only in format as 'YYMMDD'"));
		$Icno1 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	    $Icno1 ->setAttrib('class','txt_put');  
	    $Icno1 ->removeDecorator("DtDdWrapper");
		//$Icno ->setAttrib('required',"true");  
	    $Icno1 ->removeDecorator("Label");
	    $Icno1 ->removeDecorator('HtmlTag');
		
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
			   
		$Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label = $gstrtranslate->_("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
			   
		$Back = new Zend_Form_Element_Submit('Back');
        $Back->dojotype="dijit.form.Button";
        $Back->label = $gstrtranslate->_("Back");
        $Back->removeDecorator("DtDdWrapper");
        $Back->removeDecorator("Label");
        $Back->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";	   
			   
		$Clear = new Zend_Form_Element_Submit('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
        $Clear->removeDecorator("DtDdWrapper");
        $Clear->removeDecorator("Label");
        $Clear->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";	   
		
		$this->addElements ( array ($Name,
		                            $Icno,$ProgramName,$centername,$managesessionname,$FName,$Dob,$idapp,$Save,$Back,
		                            $submit,$Clear,$Icno1
		                            )
		                   );
	}
}
