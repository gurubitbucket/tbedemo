<?php
class Registrations_Form_Batchreport extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ('Zend_Translate');
		
		$Takcomp = new Zend_Dojo_Form_Element_FilteringSelect ('Takcomp');
		$Takcomp->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Takcomp->removeDecorator ("DtDdWrapper");
		$Takcomp->removeDecorator("Label");
		$Takcomp->removeDecorator('HtmlTag');
		$Takcomp->setAttrib('required',"true");
		$Takcomp->setAttrib ('onChange',"fntakcompnames();");
		
		$Takcompnames = new Zend_Dojo_Form_Element_FilteringSelect ('Takcompnames');
		$Takcompnames->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Takcompnames->setAttrib ('onChange',"fngetpin();");
		$Takcompnames->removeDecorator ("DtDdWrapper");
		$Takcompnames->removeDecorator("Label");
		$Takcompnames->removeDecorator('HtmlTag');
		$Takcompnames->setAttrib ('required',"true");
		
		$Pin = new Zend_Dojo_Form_Element_FilteringSelect ('Pin');
		$Pin->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Pin->removeDecorator("DtDdWrapper");
		$Pin->removeDecorator("Label");
		$Pin->removeDecorator('HtmlTag');
		$Pin->setAttrib('required',"true");
		
		$Fromdate = new Zend_Dojo_Form_Element_DateTextBox ('Fromdate');
		$Fromdate->setAttrib ('dojoType', "dijit.form.DateTextBox");
		$Fromdate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	    $Fromdate->setAttrib('title',"dd-mm-yyyy");
		$Fromdate->setAttrib('onChange', "examtoDateSetting();");
	    $Fromdate->removeDecorator("Label");
		$Fromdate->removeDecorator("DtDdWrapper");
		$Fromdate->removeDecorator('HtmlTag');
		      
		$Todate = new Zend_Dojo_Form_Element_DateTextBox ('Todate');
		$Todate->setAttrib ('dojoType', "dijit.form.DateTextBox");
		$Todate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$Todate->setAttrib ('title', "dd-mm-yyyy");
		$Todate->setAttrib ('required', "false");
		$Todate->removeDecorator ("Label");
		$Todate->removeDecorator ("DtDdWrapper");
		$Todate->removeDecorator ('HtmlTag');
		
		$Venue = new Zend_Dojo_Form_Element_FilteringSelect ('Venue');
		$Venue->setAttrib('dojoType', "dijit.form.FilteringSelect");
		$Venue->setAttrib('required',"false");
		$Venue->removeDecorator ("DtDdWrapper");
		$Venue->removeDecorator ("Label" );
		$Venue->removeDecorator ('HtmlTag');
		
		$Course = new Zend_Dojo_Form_Element_FilteringSelect ( 'Course' );
		$Course->setAttrib ( 'dojoType', "dijit.form.FilteringSelect" );
		$Course->setAttrib ( 'required', "true" );
		$Course->removeDecorator ( "DtDdWrapper" );
		$Course->removeDecorator ( "Label" );
		$Course->removeDecorator ( 'HtmlTag' );
		
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
         	   
        $excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
		
		$this->addElements ( array ($Takcomp,
		                            $Takcompnames,
		                            $Pin,
		                            $Fromdate, 
		                            $Todate,
		                            $excel, 
		                            $submit,
		                            $Venue,
		                            $Course)
		                   );
	}
}
