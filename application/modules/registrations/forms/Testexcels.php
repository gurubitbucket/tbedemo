<?php
class Registrations_Form_Testexcels extends Zend_Dojo_Form {
	public function init(){
		$gstrtranslate =Zend_Registry::get('Zend_Translate');
		 
		$Upload = new Zend_Form_Element_Submit('Upload');
		$Upload->dojotype="dijit.form.Button";
		$Upload->removeDecorator("DtDdWrapper");
		$Upload->removeDecorator("Label");
		$Upload->removeDecorator('HtmlTag');
		$Upload->class = "NormalBtn";
		$Upload->label = $gstrtranslate->_("Test");
		
		$Clear = new Zend_Form_Element_Button('Clear');
		$Clear->dojotype="dijit.form.Button";
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator('HtmlTag');
		$Clear->setAttrib('onclick','fnclear()');
		$Clear->class = "NormalBtn";
		$Clear->label = $gstrtranslate->_("Clear");
		
		$FileName = new Zend_Form_Element_File('FileName');
		$FileName->setAttrib('dojoType',"dijit.form.TextBox")
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
					->removeDecorator('HtmlTag');  
        
		$Companytakaful = new Zend_Dojo_Form_Element_FilteringSelect('Companytakaful');
		$Companytakaful->setAttrib('dojoType',"dijit.form.FilteringSelect");
		$Companytakaful->addMultiOptions(array(''=>'Select','1'=>'Company','2'=>'Takaful')); 
		$Companytakaful->setAttrib('onChange','fngetctlist(this.value);');
        $Companytakaful ->setAttrib('required',"true");		
		$Companytakaful->removeDecorator("DtDdWrapper");
		$Companytakaful->removeDecorator("Label");
	    $Companytakaful->removeDecorator('HtmlTag');
		
		$Ctlist = new Zend_Dojo_Form_Element_FilteringSelect('Ctlist');
		$Ctlist->setAttrib('dojoType',"dijit.form.FilteringSelect");
		//$Ctlist->addMultiOptions(array(''=>'Select','1'=>'Company','2'=>'Takaful')); 
		//$Ctlist->setAttrib('onChange','fngetctlist(this.value);');	           	         		       		     
		$Ctlist->removeDecorator("DtDdWrapper");
		$Ctlist->removeDecorator("Label");
	    $Ctlist->removeDecorator('HtmlTag');
		
		
		
		$Change = new Zend_Form_Element_Submit('Change');
		$Change->dojotype="dijit.form.Button";
		$Change->removeDecorator("DtDdWrapper");
		$Change->removeDecorator("Label");
		$Change->removeDecorator('HtmlTag');
		//$Change->class = "NormalBtn";
		$Change->label = $gstrtranslate->_("Apply Changes");
		
		$this->addElements(
							array(
									$Upload,$FileName,$Companytakaful,$Ctlist,$Change,
									$Clear
							     )
						  );
	}
}
