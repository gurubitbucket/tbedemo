<?php
class Registrations_Form_Paymentform extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
    	$Name = new Zend_Form_Element_Text('Sname');
	    $Name  ->setAttrib('dojoType',"dijit.form.ValidationTextBox")	
		 	     ->setAttrib('maxlength','20')
		 	     ->setAttrib('required',"true")
				 ->removeDecorator("Label")
				 ->removeDecorator("DtDdWrapper")
				 ->setAttrib('propercase','true')
				 ->removeDecorator('HtmlTag');
				      
        $paymentFee = new Zend_Form_Element_Text('paymentFee');
		$paymentFee->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $paymentFee->setAttrib('maxlength','10')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $payerId = new Zend_Form_Element_Text('payerId');
		$payerId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $payerId->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $verifySign = new Zend_Form_Element_Text('verifySign');
		$verifySign->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $verifySign->setAttrib('maxlength','255')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $transactionId = new Zend_Form_Element_Text('transactionId');
		$transactionId->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $transactionId->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');				 		

        //form elements
        $this->addElements(array($paymentFee,$payerId,$transactionId,$verifySign
        						 ));

    }
}