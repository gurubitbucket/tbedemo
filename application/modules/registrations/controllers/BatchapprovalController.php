<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Registrations_BatchapprovalController extends Base_Base 
{
    private $_gobjlogger;
    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
    }
    public function fnsetObj()
    {
		$this->lobjBatchapproval = new Registrations_Model_DbTable_Batchapproval();
		//$this->lobjStudentpaymentForm = new GeneralSetup_Form_Studentpayment ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjBatchapproval->fnGetCompanyDetails(); //get user details
		/*echo('<pre>');
		print_r($larrresult);die();*/
		//$larrresult1 = $this->lobjBatchapproval->fnGetcourse();
		/*echo('<pre>');
		print_r($larrresult1);die();*/
		$this->view->countcomp = count($larrresult);
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost() && $this->_request->getPost('Approve')) {
			$lobjFormData = $this->_request->getPost();
			
			$larrresult = $this->lobjBatchapproval->fnBatchApproved($lobjFormData);
			//$larrresult = $this->lobjBatchapproval->fnBatchApproved($lobjFormData);
			for($i=0;$i<count($larrresult);$i++)
			{
				$name[] =$larrresult[$i]['FName'].'---the regid for taking the exam is---'.$larrresult[$i]['Regid']; 
			}
			
			$this->view->message = $larrresult;
				
    			$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Approved the Students by batchapproval"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
			
			//$this->_redirect($this->baseUrl . '/registrations/batchapproval/index');
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjBatchapproval->fngetBatchSearch($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->semesterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/registrations/batchapproval/index');
			//$this->_redirect($this->view->url(array('module'=>'registrations' ,'controller'=>'batchapproval', 'action'=>'index'),'default',true));
		}
		
		
    }
    
    public function batchapprovallistAction()
    {
    	$lstrType = $this->_getParam('lvaredit');
    	$larrstudentdetails=$this->lobjBatchapproval->fnGetStudentDetails($lstrType);
 /*   	echo "<pre/>";
    	print_r($larrstudentdetails);
    	die();*/
    	$this->view->studentdetails = $larrstudentdetails;
    	//$idbatch = $larrstudentname['IdBatch'];
	

    }

}

