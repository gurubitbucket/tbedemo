<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Registrations_StudentapprovalController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjStudentapproval = new Registrations_Model_DbTable_Studentapproval();
		$this->lobjStudentpaymentForm = new GeneralSetup_Form_Studentpayment ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjStudentapproval->fnGetStudentDetails(); //get user details
		/*echo('<pre>');
		print_r($larrresult);die();*/
		$this->view->countcomp = count($larrresult);
		$lintpagecount =10000000;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost() && $this->_request->getPost('Approve')) {
			$lobjFormData = $this->_request->getPost();
			$larrresult = $this->lobjStudentapproval->fnStudentApproved($lobjFormData);
			$this->_redirect($this->baseUrl . '/registrations/studentapproval/index');
		}
		
    if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjStudentapproval->fngetStudentSearch($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->semesterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/registrations/studentapproval/index');
			//$this->_redirect($this->view->url(array('module'=>'registrations' ,'controller'=>'batchapproval', 'action'=>'index'),'default',true));
		}
		
    }
    
    public function studentapprovallistAction()
    {
    	$lstrType = $this->_getParam('lvaredit');
    	$larrstudentdetails=$this->lobjStudentapproval->fnGetStudent($lstrType);
    	//echo "<pre/>";
    	//print_r($larrstudentdetails);
    	//die();
    	$this->view->studentdetails = $larrstudentdetails;
    	//$this->view->examdate = $larrstudentdetails['Examdate'].'-'.$larrstudentdetails['Exammonth'].'-'.$larrstudentdetails['Year'];
    	
    	
    	//$idbatch = $larrstudentname['IdBatch'];
	

    }

}

