<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Registrations_BulkinvoicegenerationController extends Base_Base 
{
    private $_gobjlogger;
    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
    }
    public function fnsetObj()
    {
	    $this->lobjform = new App_Form_Search ();
		$this->lobjbulkform = new Registrations_Form_Bulkinvoiceform();
		$this->lobjbulkmodel = new Registrations_Model_DbTable_Bulkinvoicemodel();		
		$this->lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //Company student details model object
    }
    public function indexAction()
    {
       	$this->view->title="Program Setup";		
		$larrresult = $this->lobjbulkmodel->fnGetCompanyDetails(); //get user details
		$lobjbulkform = $this->view->lobjbulkform = $this->lobjbulkform; //send the lobjuserForm object to the view		
		$lobjbulkform->Takcomp->setAttrib('OnChange', 'fnGetCityList');
		$lobjbulkform->Takcompnames->setAttrib('onkeyup', 'fnGetOperatorNames');		
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Company";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Takaful";
		$this->view->lobjbulkform->Takcomp->addMultiOptions($larrtypeofcompany);		
		$larrresult = $this->lobjcompanystudentdetails->fnGenerateQueries(1,1); //get Company details
		$larrresult=0;
	
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->companystudentdetailspaginatorresult);						
		$lintpagecount = $this->gintPageCount=10;		
		$lintpage = $this->_getParam('page',1); //Paginator instance        		
		if(isset($this->gobjsessionsis->companystudentdetailspaginatorresult)) 
		{			
			$lobjbulkform->Takcomp->setValue($this->gobjsessionsis->operatortype);		
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->companystudentdetailspaginatorresult,$lintpage,$lintpagecount);
		}
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Next' ))
		{
		   
			  $larrformData = $this->_request->getPost ();
			  $this->view->regpin = $registrationpins = $larrformData['BatchIdsname'];
			
		}
    }
    
   public function getallregpinsforbatchAction()
	{
	   $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender ();
	   $compflag = $this->_getParam('compflag');
	   $idcompany = $this->_getParam('idcomp');	  
	   $regpindetails = $this->lobjbulkmodel->fngetpinforbatch($compflag,$idcompany);
      // $regpindetails[]=array('key'=>'','value'=>'All');	 
	   $larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ($regpindetails);	
	   echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );
	}
	public function getoperatornamesAction()
	{
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$newresult="";
		$opType = $this->_getParam('opType');	
		$namestring = $this->_getParam('namestring');			
		$larrresultopnames = $this->lobjbulkmodel->fnGetOperatorNames($opType,$namestring); 		
		foreach ($larrresultopnames as $larrresnewarray)
		{
			$opname=$larrresnewarray['name'];
			$newresult=$newresult."<tr><td><span id='idspan'   onclick='fnsetvalue(\"".$opname."\");'>".$larrresnewarray['name']."</span></td></tr></BR>";			
		}		
		echo $newresult;
		exit;
	}
	public function fnajaxgettakcompnamesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidname = $this->_getParam('idname');
		$lobjExamreportModel = new  Reports_Model_DbTable_BatchExamreport();	 
		$larrcenters = $lobjExamreportModel->fnajaxgetcenternames($lintidname);			
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrcenters);		
		echo Zend_Json_Encoder::encode($larrcentreDetailss);
	}
	public function invoicegenerationAction()
	{
	 if ($this->_request->isPost () && $this->_request->getPost ( 'Next' ))
	 { 
	    $larrformData = $this->_request->getPost ();       
	    if(isset($larrformData['BatchIdsname']))
		{					
					if(!$larrformData['Takcomp'])
					{
						echo '<script language="javascript">alert("Please Select Type Of Company")</script>';
						echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/bulkinvoicegeneration/index/';</script>";					
						exit;
					}
					$lintidcompany =$this->view->idcompany = $larrformData['Takcompnames'];//$this->_getParam('Takcompnames');
					$registrationpins = $larrformData['BatchIdsname'];					
					$this->view->compflag = $companyflag = $larrformData['Takcomp'];//$this->_getParam('Takcomp');					
					for($i=0;$i<count($larrformData['BatchIdsname']);$i++)
					{
				        $pinwithnoofcandidates[$i] = $this->lobjbulkmodel->fngettotalcandidates($larrformData['BatchIdsname'][$i]);
                        $pinwithtotalamount[$i] = $this->lobjbulkmodel->fngettotalamount($larrformData['BatchIdsname'][$i]);
                                        //$taxamount[$i] = $this->lobjbulkmodel->fngettaxamount($larrformData['BatchIdsname'][$i]);
				    } 	
                                    //echo "<pre/>";print_r($pinwithnoofcandidates);die();
					$this->view->regpinarray = $pinwithnoofcandidates;
                                         //echo "<pre/>";print_r($pinwithnoofcandidates);
					$this->view->amountarray  = $pinwithtotalamount;
                                        //echo "<pre/>";print_r($pinwithnoofcandidates);die();
					$this->view->regpin =  $registrationpins;
					$newarray = implode($registrationpins,',');
					$candidatesdetails = $this->lobjbulkmodel->fngettotalbatchcount($newarray);					
					$appliedcandidates = $this->lobjbulkmodel->fngettotalapplied($newarray);					
					$larrresult = $this->lobjbulkmodel->Fngetactualcandidates($newarray);
					if(empty($larrresult))
					{ 
							echo '<script language="javascript">alert("No Candidates Present Invoice Can Not Generate")</script>';
							echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/bulkinvoicegeneration/index';</script>";
							die();
					}
					if($candidatesdetails['totalNoofCandidates'] != $appliedcandidates['Totalappln'])
					{
							echo '<script language="javascript">alert("There is a mismatch can not generate invoice")</script>';
							echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/bulkinvoicegeneration/index';</script>";
							die();
					}		
					$this->view->pindetails = $Pindetails = $this->lobjbulkmodel->gettotalcandidates($newarray);
					if($companyflag ==1)
					{
						 $this->view->companydetails = $companydetails =  $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
					}
					else
					{
						 $this->view->companydetails = $companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
					}
						 $this->view->studentdetails = $studentdetails = $this->lobjbulkmodel->fngetstudentdetailsforpin($newarray); //Get company details by ID	
		}
		else
		{
			   echo '<script language="javascript">alert("Please Select Atleast One BatchId And Then Continue")</script>';
			   echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/bulkinvoicegeneration/index';</script>";
			  
		}
	  }
	}
	public function viewgenerationAction()
   {    
         $this->view->generated=0; 
         $gst = $this->lobjbulkmodel->fngettaxrate();
         $this->view->GST=$gst['GST'];
        //echo "<pre/>";print_r($gst);
         if ($this->_request->isPost () && $this->_request->getPost ('Next' ))
	     {
				$larrformData = $this->_request->getPost ();
                                //echo "<pre/>";print_r($larrformData);
				$lintidcompany = $this->view->idcompany = $larrformData['Idcomp'];
				$this->view->Regpinscandidates = $lintidregistrationpin =  $larrformData['Regpinarray'];
				$this->view->Amount =   $larrformData['Amountarray'];
                                //echo "<pre/>";print_r($larrformData);die();
                $this->view->taxAmount =   $larrformData['taxarray'];
				$this->view->Pins = $larrformData['Regpins'];                				
				$this->view->compflag = $companyflag =  $larrformData['flag'];		
				$this->view->Configdetails = $Configdetails = $this->lobjbulkmodel->fngetreceivermail($companyflag);
				$this->view->BatchAddressdetails = $BatchAddressdetails = $this->lobjbulkmodel->fngetbatchaddress($lintidcompany,$companyflag);
				if($companyflag ==1)
				{
				   $this->view->companydetails = $companydetails =  $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
				}
				else
				{
				   $this->view->companydetails = $companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
				}		
               if(empty($BatchAddressdetails['Fax']))
				{
				   $BatchAddressdetails['Fax']='';
				}
				if($companyflag == 1)		
				{
				   $this->view->comp = $companyName="".$companydetails['CompanyName'];
				}
				elseif($companyflag == 2)
				{
				   $this->view->comp = $companyName="".$companydetails['CompanyName'];
				}          		   
	 }
			 if ($this->_request->isPost () && $this->_request->getPost ( 'Submit' )) //save
			  {          
					$larrformData = $this->_request->getPost ();
                     // echo "<pre>";
                     //print_r($larrformData);die();					  
					$lintidcompany = $this->view->idcompany = $larrformData['Idcomp'];
					$this->view->Regpinscandidates = $lintidregistrationpin =  $larrformData['Candidates'];
				    $this->view->Amount =   $larrformData['Amount'];
					
					
					
                                    $this->view->taxAmount =   $larrformData['taxamountarray'];
									
									
									
				    $this->view->Pins = $larrformData['Pins'];
				    $this->view->compflag = $companyflag =  $larrformData['flag'];
				    $this->view->Configdetails = $Configdetails = $this->lobjbulkmodel->fngetreceivermail($companyflag);
				    $this->view->BatchAddressdetails = $BatchAddressdetails = $this->lobjbulkmodel->fngetbatchaddress($lintidcompany,$companyflag);
				if($companyflag ==1)
				{
				   $this->view->companydetails = $companydetails =  $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
				} 
				else
				{
				   $this->view->companydetails = $companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
				}		
                if(empty($BatchAddressdetails['Fax']))
				{
				   $BatchAddressdetails['Fax']='';
				}
				if($companyflag == 1)		
				{
				   $this->view->comp = $companyName="".$companydetails['CompanyName'];
				}
				elseif($companyflag == 2)
				{
				   $this->view->comp = $companyName="".$companydetails['CompanyName'];
				}       
								$idcompany = $larrformData['Idcomp'];
								$companyflag = $larrformData['flag'];						
								if($larrformData['flag'] == 2)
								{
									 $Typeofoperator = 'T';
								}
								if($larrformData['flag']==1)
								{
								   $Typeofoperator = 'C';
							    }
							    $Uniqueid = "INV/IBFIM/".$Typeofoperator.'/'.$idcompany.'/'.date('Y').'/';
							    $auth = Zend_Auth::getInstance(); 
							    $upduser =  $auth->getIdentity()->iduser;
							    $this->lobjbulkmodel->fnupdatebulkinvoicegenerated($larrformData,$Uniqueid,$upduser,$idcompany,$companyflag);
							    $invoicenum = $this->lobjbulkmodel->getinvoicenumber($larrformData);								
							    $this->view->invoicenum =  $invoicenum['Invoiceuniqueid'];
								$this->view->generated=1;
								//$this->_redirect( $this->baseUrl . '/registrations/bulkinvoicegeneration/index');
			  }
			  if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) //Send Invoice To Admin
			  {
                $this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();			  
			    $frmdate =date('d-m-Y');
				$day= date("d-m-Y");
				$host = $_SERVER['SERVER_NAME'];
				$imgp = "http://".$host."/tbenew/images/reportheader.jpg";				
				$time = date('h:i:s',time());
				$filename = 'Invoice_Report_'.$frmdate;
				$ReportName = $this->view->translate( "INVOICE" );	
                $lstrreportytpe ="Pdf";				
				if($lstrreportytpe=='Pdf')
				{
					$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
				}else
				{
					$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
				}
				 $larrformData = $this->_request->getPost ();
				 
				$date =date('d-m-Y');				
				$invoicenum = $this->lobjbulkmodel->getinvoicenumber($larrformData);
				$invoiceid = $invoicenum['Invoiceuniqueid'];				 
				$this->view->Configdetails = $Configdetails = $this->lobjbulkmodel->fngetreceivermail($larrformData['flag']);
				$this->view->BatchAddressdetails = $BatchAddressdetails = $this->lobjbulkmodel->fngetbatchaddress($larrformData['Idcomp'],$larrformData['flag']);
				$tabledata.= "<br/>";
                     $tabledata.="<table border='0' width='100%' ><tr ><td align='center'><b>TAX INVOICE</b></td></tr></table>";                       
	            $tabledata.="<table border='1' width='100%' align='center' >							
							<tr >								
							<th colspan=3 align='left'><b>To:</b>
								<b>&nbsp;&nbsp;&nbsp;{$larrformData['CompanyName']}
								</b><br/>
								<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								{$BatchAddressdetails['Contact']}
								</b><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									{$BatchAddressdetails['Address']}
								</b><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									{$BatchAddressdetails['Postcode']}
									</b><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									{$BatchAddressdetails['StateName']}
									</b><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									{$BatchAddressdetails['CountryName']}
									</b><br/>
									<b>Attn:</b><b>&nbsp;
									{$BatchAddressdetails['Contact']}
									</b>
									<b>Tel:</b><b>&nbsp;&nbsp;{$BatchAddressdetails['Phone']}
                                    </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Fax:</b><b>{$BatchAddressdetails['Fax']}</b><br/></th>									
								    <th align ='left' valign='top' colspan='3'>
                                    <b>GST Ref. No.:</b><b>&nbsp;&nbsp;&nbsp;<b>
									001167200256
									</b><br/><br/> 
                                    <b>Invoice:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									$invoiceid
									</b><br/><br/>																		
									<b>Date:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									$date									
									</b><br/><br/>
									<b>Term:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									{$Configdetails['Duration']} Days
									</b></th>							
							</tr></table>				
		        <table border='1' width='100%' align='center' >
						<tr>
                            <th ><b>SL NO</b></th>
							<th ><b>BatchId</b></th>							
							<th ><b>No of Candidates</b></th>
							<th ><b>TOTAL</b></th>
						</tr>";
			 $slcount =0;
				//$programs = $this->Program;
					$Amount = $larrformData['Amount'];
                                        $taxAmount=$larrformData['taxamountarray'];
                                        //$taxAmount = $this->taxAmount;
					$Pins =  $larrformData['Pins'];
					$Regpinscandidates =  $larrformData['Candidates'];
					$Total=0;
					$Totalamount = 0.00;
                                        $Totaltaxamount= 0.00;
                                        $Totaltaxamount=$Totaltaxamount+$larrformData['TotalTaxamount'];
 				 foreach ($Pins as $lobjpindetails ):
						$slcount =$slcount+1;
						$pin=$lobjpindetails; 
						$total= $Amount[$pin];			
						$totalcandidates = $Total+$Regpinscandidates[$pin];
						$Total = $totalcandidates;
						$totalamt = $Totalamount+$total;
						$Totalamount = $totalamt;
				 $tabledata.= '<tr ><td  align="center"><b>'.$slcount.'</b></td>
                                    <td  align="center"><b>'.$lobjpindetails.'</b></td>
                                    <td  align="center"><b>'.$Regpinscandidates[$pin].'</b></td> 
                                    <td  align="center" align="right" ><b>'.number_format($Amount[$pin],2).'</b></td>
			</tr>';
			  //} 
			 endforeach;
                         
                         //$subtotal=$Totalamount-$Totaltaxamount;
                         $subtotal=$Totalamount;
                         $calctax=$subtotal*($gst['GST']/100);
                         $Totalamount=$Totalamount+$calctax;
			 $tabledata.='
                                    <tr >             						   
                                        <td  colspan = "3" align="right"><b>Sub-Total:<b></td><td align="right"><b>'.number_format($subtotal,2).'</b></td>
                                    </tr>
                                    <tr >             						   
                                        <td  colspan = "3" align="right"><b>GST payable @'.$gst['GST'].'%:<b></td><td align="right"><b>'.number_format($calctax,2).'</b></td>
                                    </tr>
                                    <tr >             						   
                                        <td  colspan = "3" align="right"><b>Total Amount:<b></td><td align="right"><b>'.number_format($Totalamount,2).'</b></td>
                                        </td>
                                   </tr>
                                   <tr>
			          <td  align="center" colspan="4"><b>Ringgit Malaysia:</b><b>';
					 
                               $lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //Company student details model object
                               $Amountinwords = $lobjcompanystudentdetails->fnconvertnumbertowords($Totalamount);
                               $number = number_format($Totalamount, 2, ".", "");
                               sscanf($number, '%d.%d', $whole, $fraction);
                               if($fraction){
                                    $fractioninwords = $lobjcompanystudentdetails->fnconvertnumbertowords($fraction);
                                    $Amountinwords=$Amountinwords.' and '.$fractioninwords.' cent Only';
                               }else{
                                   $Amountinwords=$Amountinwords.' Only';
                               }
                               
                               
                               $tabledata.=$Amountinwords.' </b></td></tr></table>';
				$tabledata.='<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>';
		 $tabledata.='<table><tr></tr>
								<tr><td><b>Note:</b></td>
								</tr>
								<tr><td colspan="4"><b>1.  Please make payment within '.$Configdetails['Duration'].' Days from the date of registration.
                                </b></td>
								</tr>
								<tr><td colspan="4"><b>2.  All cheques must be  made payable to "IBFIM-TBE".</b></td>
								</tr>
								<tr><td colspan="4"><b>3.  Please send this copy of invoice together with your payment for the attention of TBE Secretariat, Learning Management				
                                                                    Talent Development Department, IBFIM.</b></td>
								</tr><br/><br/>
                                                                
                                                             <tr><td colspan="4"><b> Thank you for your prompt payment.  Please ignore this invoice if payment has already been made.</b></td></tr>
                                                              <br/><br/>
                                                              <tr><td colspan="4"><b> Issued by:</b></td></tr>
                                                              <tr><td colspan="4"><b> ACCOUNTS UNIT</b></td></tr>
                                                              <tr><td colspan="4"><b> Finance and Administration Department</b></td></tr>
                                                              <tr><td colspan="4"><b> IBFIM</b></td></tr>
                                                                <br/><br/><br/><br/><br/>
                                <tr><td align="center"><b>This is a computer generated document.  No signature is required.</b></td></tr>
								<tr>
								<td><hr/></td></tr>
								<tr>
								<td align="center"><b>IBFIM (Co. No. 763075W)
								3rd Floor, Menara Takaful Malaysia, Jalan Sultan Sulaiman,  50000 Kuala Lumpur, MALAYSIA
								(Tel) +603-2031 1010  (Fax) +603-2026 9988   (E-mail) tbe@ibfim.com
								</b></td></tr>
						</table>';
if($larrformData['mailsend'])
{         
        $frmdate =date('d-m-Y');
        $filename = 'Bulk_Invoice_Report_'.$frmdate;                   
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		require_once('Zend/Mail.php');
		require_once('Zend/Mail/Transport/Smtp.php');
		$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');	
					
    	$mpdf->SetDirectionality ( $this->gstrHTMLDir );
    	$mpdf->text_input_as_HTML = true;
    	$mpdf->useLang = true;
    	$mpdf->SetAutoFont();    	
    	$mpdf->SetDisplayMode('fullpage');
    	$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
    	$mpdf->pagenumSuffix = ' / ';
    	$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
    	$mpdf->allow_charset_conversion = true; // Set by default to TRUE
    	$mpdf->charset_in = 'utf-8';       
		$mpdf->WriteHTML($tabledata);
		$mpdf->Output('pdf/Bulkinvoice_Report'.'_'.$frmdate.'.pdf','F');	
		
		                                $auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' =>'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com',$config);
										Zend_Mail::setDefaultTransport($transport);										
										$mail = new Zend_Mail();
                                        $filename ='pdf/Bulkinvoice_Report'.'_'.$frmdate.'.pdf';		
									    $mail->setBodyHtml("Bulk Invoice Report");
										$at = new Zend_Mime_Part(file_get_contents($filename));
										//$at = $mail->createAttachment($filename);
                                        $at->type= 'application/pdf';	
                                        //$at->disposition = Zend_Mime::DISPOSITION_INLINE;
										$at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                                        $at->encoding = Zend_Mime::ENCODING_BASE64;
                                        $at->filename = 'Bulkinvoice_Report'.'_'.$frmdate.'.pdf';   
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'iTWINE';
										$receiver_email = $Configdetails['Schedulerpushemail'];
										$receiver = 'Admin';
										$mail->setFrom('itwinesgm@gmail.com','itwine')
											 ->addTo($receiver_email, Zuarudin)
									         ->setSubject("Invoice_Report"."_".$frmdate)
                                             ->addAttachment($at);     
	                                        try
										    {
							                          $mail->send($lobjTransport);
                                                     // unlink('pdf/Bulkinvoice_Report_'.$frmdate.'.pdf');
							                          echo '<script language="javascript">alert("Invoice successfully  send to admin")</script>';	
							                          echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/bulkinvoicegeneration/index';</script>";
				                	                  die();
				                	
						                    } 
											catch (Exception $e)
											{
							                    echo '<script language="javascript">alert("Unable to send mail \n check Internet Connection ")</script>';	
							                    echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/bulkinvoicegeneration/index';</script>";
				                	            die();
						                    }
}								
    }
	 if ($this->_request->isPost () && $this->_request->getPost ( 'Print' )) //print invoice
			  {			  
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender();				
				$frmdate =date('d-m-Y');
				$day= date("d-m-Y");
				$host = $_SERVER['SERVER_NAME'];
				$imgp = "http://".$host."/tbedemo/images/reportheader.jpg";				
				$time = date('h:i:s',time());
				$filename = 'Invoice_Report_'.$frmdate;
				$ReportName = $this->view->translate( "INVOICE" );				
                $lstrreportytpe="Pdf";	
                $tabledata = '<img width=100% src="'.$imgp.'" />';
				
                               
				 $larrformData = $this->_request->getPost ();
                                
				 $date =date('d-m-Y');				
				$invoicenum = $this->lobjbulkmodel->getinvoicenumber($larrformData);
				$invoiceid = $invoicenum['Invoiceuniqueid'];				 
				$this->view->Configdetails = $Configdetails = $this->lobjbulkmodel->fngetreceivermail($larrformData['flag']);
				$this->view->BatchAddressdetails = $BatchAddressdetails = $this->lobjbulkmodel->fngetbatchaddress($larrformData['Idcomp'],$larrformData['flag']);
				$tabledata.= "<br/>";
                    $tabledata.="<table border='0' width='100%' ><tr ><td align='center'><b>TAX INVOICE</b></td></tr></table>";            
	            $tabledata.="<table border='1' width='100%' align='center' >							
							<tr >								
							<th colspan=3 align='left'><b>To:</b>
								<b>&nbsp;&nbsp;&nbsp;{$larrformData['CompanyName']}
								</b><br/>
								<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								{$BatchAddressdetails['Contact']}
								</b><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									{$BatchAddressdetails['Address']}
								</b><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									{$BatchAddressdetails['Postcode']}
									</b><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									{$BatchAddressdetails['StateName']}
									</b><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									{$BatchAddressdetails['CountryName']}
									</b><br/>
									<b>Attn:</b><b>&nbsp;
									{$BatchAddressdetails['Contact']}
									</b>
									<b>Tel:</b><b>&nbsp;&nbsp;{$BatchAddressdetails['Phone']}
                                    </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Fax:</b><b>{$BatchAddressdetails['Fax']}</b><br/></th>									
								    <th align ='left' valign='top' colspan='3'>
                                    <b>GST Ref. No.:</b><b>&nbsp;&nbsp;&nbsp;<b>
									001167200256
									</b><br/><br/>                               
                                    <b>Invoice:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
									$invoiceid
									</b><br/><br/>
																		
									<b>Date:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									$date									
									</b><br/><br/>
									<b>Term:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									{$Configdetails['Duration']} Days
									</b></th>							
							</tr></table>				
		        <table border='1' width='100%' align='center' >
						<tr>
                            <th ><b>SL NO</b></th>
							<th ><b>BatchId</b></th>							
							<th ><b>No of Candidates</b></th>
							<th ><b>TOTAL</b></th>
						</tr>";
			 $slcount =0;				
					$Amount = $larrformData['Amount'];
                                        $taxAmount=$larrformData['taxamountarray'];
                                        
					$Pins =  $larrformData['Pins'];
					$Regpinscandidates =  $larrformData['Candidates'];
					$Total=0;
					$Totalamount = 0.00;                       
                                        
                                         //echo "<pre/>";print_r($larrformData);die();
 				 foreach ($Pins as $lobjpindetails ):
			$slcount =$slcount+1;				
			 $pin=$lobjpindetails; 
			 $total= $Amount[$pin];			
			$totalcandidates = $Total+$Regpinscandidates[$pin];
			$Total = $totalcandidates;
			$totalamt = $Totalamount+$total;
			$Totalamount = $totalamt;
                        $Totaltaxamount= 0.00;
                        $Totaltaxamount=$Totaltaxamount+$larrformData['TotalTaxamount'];
				 $tabledata.= '<tr ><td  align="center"><b>'.$slcount.'</b></td>
                                    <td  align="center"><b>'.$lobjpindetails.'</b></td>
                                    <td  align="center"><b>'.$Regpinscandidates[$pin].'</b></td> 
                                    <td  align="center" align="right" ><b>'.number_format($Amount[$pin],2).'</b></td> 
			</tr>';
			  //} 
			 endforeach;
                         
                         //$subtotal=$Totalamount-$Totaltaxamount;
                         $subtotal=$Totalamount;
                         $calctax=$subtotal*($gst['GST']/100);
                         $Totalamount=$Totalamount+$calctax;
			   $tabledata.='
                                        <tr >             						   
                                            <td  colspan = "3" align="right"><b>Sub-Total:<b></td><td align="right"><b>'.number_format($subtotal,2).'</b></td>
                                        </tr>
                                        <tr >             						   
                                            <td  colspan = "3" align="right"><b>GST payable @'.$gst['GST'].'%:<b></td><td align="right"><b>'.number_format($calctax,2).'</b></td>
                                        </tr>
                                        <tr >             						   
                                            <td  colspan = "3" align="right"><b>Total Amount:<b></td><td align="right"><b>'.number_format($Totalamount,2).'</b></td>
                                        </tr>
                                        <tr>
			              <td  align="center" colspan="4"><b>Ringgit Malaysia:</b><b>';
                               $lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //Company student details model object
                               $Amountinwords = $lobjcompanystudentdetails->fnconvertnumbertowords($Totalamount);
                               $number = number_format($Totalamount, 2, ".", "");
                               sscanf($number, '%d.%d', $whole, $fraction);
                               if($fraction){
                                    $fractioninwords = $lobjcompanystudentdetails->fnconvertnumbertowords($fraction);
                                    $Amountinwords=$Amountinwords.' and '.$fractioninwords.' cent Only';
                               }else{
                                   $Amountinwords=$Amountinwords.' Only';
                               }
                               
                               $tabledata.=$Amountinwords.' </b></td></tr></table>';
							   $tabledata.='<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>';
		 $tabledata.='<table><tr></tr>
								<tr><td><b>Note:</b></td>
								</tr>
								<tr><td colspan="4"><b>1.  Please make payment within '.$Configdetails['Duration'].' Days from the date of registration.
                                 </b></td>
								</tr>
								<tr><td colspan="4"><b>2.  All cheques must be  made payable to "IBFIM-TBE".</b></td>
								</tr>
								<tr><td colspan="4"><b>3.  Please send this copy of invoice together with your payment for the attention of TBE Secretariat, Learning Management				
                                                                    Talent Development Department, IBFIM.</b></td>
								</tr><br/><br/>
                                                                
                                                             <tr><td colspan="4"><b> Thank you for your prompt payment.  Please ignore this invoice if payment has already been made.</b></td></tr>
                                                              <br/><br/>
                                                              <tr><td colspan="4"><b> Issued by:</b></td></tr>
                                                              <tr><td colspan="4"><b> ACCOUNTS UNIT</b></td></tr>
                                                              <tr><td colspan="4"><b> Finance and Administration Department</b></td></tr>
                                                              <tr><td colspan="4"><b> IBFIM</b></td></tr>
                                                            <br/><br/><br/><br/><br/>
                                                                <tr><td align="center"><b>This is a computer generated document.  No signature is required.</b></td></tr>
								<tr>
								<td><hr/></td></tr>
								<tr>
								<td align="center"><b>IBFIM (Co. No. 763075W)
								3rd Floor, Menara Takaful Malaysia, Jalan Sultan Sulaiman,  50000 Kuala Lumpur, MALAYSIA
								(Tel) +603-2031 1010  (Fax) +603-2026 9988   (E-mail) tbe@ibfim.com
								</b></td></tr>
						</table>';
                 //echo "<pre/>";print_r($tabledata);die();
			 $slcount =0;				
					$Amount = $larrformData['Amount'];
					$Pins =  $larrformData['Pins'];
					$Regpinscandidates =  $larrformData['Candidates'];
					$Total=0;
					$Totalamount = 0.00;
 				
      $lstrreportytpe='Pdf';
     if($lstrreportytpe=='Pdf')
	{
					include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
					$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
					$mpdf->SetDirectionality ( $this->gstrHTMLDir );
					$mpdf->text_input_as_HTML = true;
					$mpdf->useLang = true;
					$mpdf->SetAutoFont();
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 1; // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumSuffix = ' / ';
                    //$mpdf->SetHTMLFooterForPage( $footer, 1);
					$mpdf->setFooter ('' );
					$mpdf->allow_charset_conversion = true; // Set by default to TRUE
					$mpdf->charset_in = 'utf-8';
					ini_set('max_execution_time',3600);
					$mpdf->WriteHTML($tabledata);
					$mpdf->Output("$filename.pdf",'D');
					//header("refresh: 2; url='".$this->view->baseUrl()."/registrations/bulkinvoicegeneration/index'");
			
	}
		}
	}
	
}




