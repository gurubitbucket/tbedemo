<?php
class Registrations_CompanystudentdetailsController extends Base_Base { //Controller for the User Module

	public function init() 
	{   
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	   // $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    
		$this->lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //Company student details model object
		$this->lobjcompanymaster = new App_Model_Companymaster(); //Company student details model object
		$this->lobjloadfilesForm = new Examination_Form_Uploadfiles ();
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //TAkafulmodels model object	
		$this->lobjTakafulcandidatesForm = new App_Form_Takafulcandidates (); //intialize user lobjuserForm
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates (); //Batch candidates model object	
		$this->lobjBatchcandidatesForm = new App_Form_Batchcandidates (); //intialize user lobjuserForm
		$this->lobjstudentForm = new App_Form_Studentapplication (); // form of student application
		$this->lobjsearchForm = new App_Form_Search (); //intialize Search lobjsearchForm
		$this->lobjstudentmodel = new App_Model_Studentapplication (); // Model of student application
		$this->lobjTakafulapplicationmodel = new App_Model_Takafulapplication (); //user model object
		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$auth = Zend_Auth::getInstance();
		if($auth->getIdentity()->iduser == 17)
		{
		   $this->_helper->layout()->setLayout('/web/usty');
		}
	}
	

	public function indexAction() 
	{   
		// action for search and view
		$lobjform=$this->view->lobjform = $this->lobjsearchForm; //send the lobjuserForm object to the view
		
		
		$larrrbusinesstypelist = $this->lobjcompanymaster->fnGetBusinesstypeList (); //get Business type list details
		$lobjform->field5->addMultioptions($larrrbusinesstypelist);
		$lobjform->field1->setAttrib('OnChange', 'fnGetCityList');
		$lobjform->field3->setAttrib('onkeyup', 'fnGetOperatorNames');
		$lobjform->field8 ->setRegisterInArrayValidator(false);
		
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Company";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Takaful";
		$this->view->lobjform->field19->addMultiOptions($larrtypeofcompany);
		
		$larrresult = $this->lobjcompanystudentdetails->fnGenerateQueries(1,1); //get Company details
		$larrresult=0;
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->companystudentdetailspaginatorresult);
						
		$lintpagecount = $this->gintPageCount=10;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
        		
		if(isset($this->gobjsessionsis->companystudentdetailspaginatorresult)) {
			$lobjform->field19->setValue($this->gobjsessionsis->operatortype);	
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->companystudentdetailspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {				
				if(!$larrformData['field19']){
					echo '<script language="javascript">alert("Please Select Type Of Company")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/index/';</script>";					
					exit;
				}
				$this->gobjsessionsis->operatortype = $this->view->operatortype = $larrformData['field19'];				
				$lobjform->field19->setValue($this->view->operatortype);
				$larrresult = $this->lobjcompanystudentdetails->fnSearchCompanies($lobjform->getValues ()); //searching the values for the Companies
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->companystudentdetailspaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			unset($this->gobjsessionsis->operatortype);
		 	$this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/index');
		}
	}
	public function fncancelpaymentAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$idcompanyflag = $this->_getParam('companyflag');
		$idBatchRegistration = $this->_getParam('idBatchRegistration');
		
		$lobjpaymentdetails = $this->lobjcompanystudentdetails->fngetpaymentdetails($idBatchRegistration,$idcompanyflag);
		//echo "<pre>";print_r($lobjpaymentdetails);
		
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Company Name</b></th>
					<th><b>Pin Number</b></th>
					<th><b>Amount</b></th>
					<th><b>Number Of Candidates</b></th>
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['registrationPin']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalAmount']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalNoofCandidates']."</b></td>
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 7) $tabledata.= 'Credit to IBFIM';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 10) $tabledata.= 'MIGS';
		
		$tabledata.= "</b></td>
					</tr>
					</table><br>";
	    $tabledata.="<table  class='table' border=1 align='center' width=100%>
	  					<tr>
	  						<td>Remarks</td>
	  						<td><input type='text' name='remarks' id='remarks' value='' > <input type='hidden' name='idBatchRegistration' id='idBatchRegistration' value='".$idBatchRegistration."' > </td>
	  					</tr>
	  					<tr>
	  						<td>Payment Details</td>
	  						<td><input type='text' name='chequedetails' id='chequedetails' value='' ><input type='hidden' name='companyflag' id='companyflag' value='".$idcompanyflag."' > </td>
	  					</tr>
	  					<tr>
	  						<td></td>
	  						<td><input type='submit' id='submit' name='submit'  value='Payment Cancel' > &nbsp;<input type='button' id='close' name='close'  value='Close' onClick='Closefn();'> </td>
	  					</tr>
	  				</table>";	
	  					
	  
	  echo  $tabledata;
		//die();
		
	}
	
	public function companystudentlistAction() 
	{   
	  	$this->view->idcompany = $lintidcompany = $this->_getParam('idcompany'); 	
	  	$operatortype = $this->view->operatortype = $this->gobjsessionsis->operatortype; 
		
		
		$this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Candidate Registration";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Cancel Payment";
		//$this->view->lobjform->field19->addMultiOptions($larrtypeofcompany);
		$this->view->lobjBatchcandidatesForm->option1->addMultiOptions($larrtypeofcompany);
		$larrcompany[0]['key']=1;
		$larrcompany[0]['value']="Candidate Registration";
		$larrcompany[1]['key']=2;
		$larrcompany[1]['value']="Move Candidates";
		$this->view->lobjBatchcandidatesForm->option2->addMultiOptions($larrcompany);
		
		$larr[0]['key']=1;
		$larr[0]['value']="Candidate Registration";		
		$this->view->lobjBatchcandidatesForm->option3->addMultiOptions($larr);
		
	  
	  	if($operatortype==1){ 		  
			$this->view->companydetails = $res= $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
			//echo "<pre>";print_r($res);die();
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(3,1,$lintidcompany); //Get company details with the other details by ID
	  	}else if($operatortype==2){
	  		$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(7,1,$lintidcompany); //Get company details with the other details by ID
	  	}
		 $laresulttemppin=$this->lobjcompanystudentdetails->fnGettempregpins($lintidcompany,$operatortype);
		
			  $temppins=array();
			  for($temp=0;$temp<count($laresulttemppin);$temp++)
			  {
				   $temppins[]=$laresulttemppin[$temp]['pins'];
			  }
			  $this->view->temppins = $temppins;
	  		//echo "<pre>";
	  		//print_r($this->view->companyresults);die();
			if ($this->_request->isPost () && $this->_request->getPost ( 'submit' ))
		{
			$larrformData = $this->_request->getPost ();	
            // echo "<pre>";print_r($larrformData);die();			
			$larrformData['UpdDate'] = date( 'Y-m-d H:i:s' );
			$auth = Zend_Auth::getInstance(); 
			$larrformData['UpdUser'] =  $auth->getIdentity()->iduser;	
		    $laresulttemppin=$this->lobjcompanystudentdetails->fnGettempregpins($lintidcompany,$operatortype);
		    //echo "<pre/>";		
			$temppins=array();
			for($temp=0;$temp<count($laresulttemppin);$temp++)
			{
			$temppins[]=$laresulttemppin[$temp]['pins'];
			}
			$this->view->temppins = $temppins;
			$this->lobjcompanystudentdetails->fnCancelPayment($larrformData,$lintidcompany,$operatortype);	

          if($operatortype==1){ 		  
			$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(3,1,$lintidcompany); //Get company details with the other details by ID
	  	   }else if($operatortype==2){
	  		$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(7,1,$lintidcompany); //Get company details with the other details by ID
	  	   }
           $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$lintidcompany);

			///registrations/companystudentdetails/companystudentlist/idcompany/89
			//$larrresult = $this->lobjPaymentcancelmodel->fnGetregpins($larrformData,$idcompany,$idopt); //Call a function to searching the values for the candidates
            
			//$this->view->lvartype  = $larrformData['lvartype'];
            //$this->view->lobjform->field15->setValue( $larrformData['lvartype'] );
			//$this->view->count=count($larrresult);
			//$this->view->countcomp = $larrresult;
		}
        if ($this->_request->isPost () && $this->_request->getPost ( 'block' ))
		{
		
		   //$this->view->idcompany = $lintidcompany = $this->_getParam('idcompany'); 	
	  	   //$operatortype = $this->view->operatortype = $this->gobjsessionsis->operatortype; 
		   $larrformData = $this->_request->getPost ();
           $larr =    explode(',',$larrformData['block']);
		  $total = $larr[0];
		  $idbatch = $larr[1];
		  $auth = Zend_Auth::getInstance(); 
			$upduser =  $auth->getIdentity()->iduser;	
		      //echo "<pre>";print_r($larr);die();
		   $this->view->companydetails = $this->lobjcompanystudentdetails->fnupdatetotalcandidates($idbatch,$total,$upduser,$lintidcompany); //Get company details by ID
        	
            if($operatortype==1){ 		  
			$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(3,1,$lintidcompany); //Get company details with the other details by ID
	  	   }else if($operatortype==2){
	  		$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(7,1,$lintidcompany); //Get company details with the other details by ID
	  	   }
         $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$lintidcompany);
			 
        }	
         if ($this->_request->isPost () && $this->_request->getPost ( 'paymentchangerequest' ))
		{
		   $larrformData = $this->_request->getPost (); //getting the values of bank from post 		  
		   //echo $larrformData['paymentchangerequest'];
		   $larrdetails = explode(',',$larrformData['paymentchangerequest']);
		   $auth = Zend_Auth::getInstance();
		  $userid =  $auth->getIdentity()->iduser;
		    $oldpaymentmode= $larrdetails[0];
           //echo "<br/>";		   
		   $oldupddate = $larrdetails[1];	
          // echo "<br/>";		   
		    $oldupduser = $larrdetails[2];
           // echo "<br/>";		   
		   $idbatch = $larrdetails[3];
			//echo "<br/>";
		     $idcompany = $larrdetails[4];
			//echo "<br/>";
		     $regpin = $larrdetails[5];
			//echo "<br/>";
		     $amount = $larrdetails[6];
		 
		   $this->lobjcompanystudentdetails->fnpreviouspayment($larrformData,$oldpaymentmode,$oldupddate,$oldupduser,$idbatch,$idcompany,$regpin,$amount,$userid);
		   $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$idcompany);
        }		
        		
	  	
	}
	
	public function studentsregisteredAction() 
	{   
		$lintidcompany = $this->view->idcompany = $this->_getParam('idcompany');
		$lintidregistrationpin = $this->_getParam('idRegPin');
		$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
		$this->view->studentdetails = $this->lobjcompanystudentdetails->fnGenerateQueries(4,1,$lintidregistrationpin); //Get company details by ID	
	}
	
	public function candidateregistrationAction() 
	{   
		$this->_helper->layout->disableLayout();
	 	$lintidcompany = $this->view->idcompany = $this->_getParam('idcompany');
	 	
		$lintidregistrationpin = $this->view->regpin = $this->_getParam('idRegPin');
					
		$larrsultfailedStud=$this->lobjTakafulapplicationmodel->fnGetfailedregistrered ($lintidregistrationpin);	
		$this->view->failedstudentlist=$larrsultfailedStud;		
		$larrresultStudent = $this->lobjTakafulapplicationmodel->fnGetStudregistrered ($lintidregistrationpin);			
		if (!$larrresultStudent) {
			$this->view->showshceduler = 0;
		} else {
			$this->view->showshceduler = 1;
		}
	}
	public function fnchecktempexcelAction()
	{
	   $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender ();
	    $idcompany = $this->_getParam('idcompany');
	  
	   
	    $idRegPin = $this->_getParam('idRegPin');
	   
	    $idbatch = $this->_getParam('idbatch');
	  
	   
	   $companyflag = $this->_getParam('flag');
	   
	   $larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$idRegPin);
	   $larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($idRegPin); // Newly added as on 21-01-2013
			
		$idcompanyflag = $this->_getParam('companyflag');
		$this->view->idbatchreg = $idBatchRegistration = $this->_getParam('idBatchRegistration');
					
		$lobjpaymentdetails = $this->lobjcompanystudentdetails->fngetpaymentdetails($idbatch,$companyflag);
			
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Company Name</b></th>
					<th><b>Pin Number</b></th>
					<th><b>Amount</b></th>
					<th><b>Number Of Candidates</b></th>
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['registrationPin']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalAmount']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalNoofCandidates']."</b></td>
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 7) $tabledata.= 'Credit to IBFIM';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 10) $tabledata.= 'MIGS';
		
		$tabledata.= "</b></td>
					</tr></table><br>";
		 if($larrstudentscountcount['totalNoofCandidates'] == $larrstudentscountcount['totalregistered'])
		{
			     $tabledata.="<table  class='table' border=1 align='center' width=100%>
									
									<tr>
										<td><b>All Candidates Successfully Registered...!!!</td>
										<td><input type='button' id='close' name='close'  value='Close' onClick='BlockClosefn();'> </td>
									</tr>
								</table>";	
								 //echo  $tabledata;
					//die();
					echo  $tabledata;
					die();
				 
		}
		elseif($larrstudentscountcount['totalregistered'] < $larrstudentscountcount['totalNoofCandidates'])
		{
		    if($larrstudentcounttemp['totalcount'] ==0)
			{
			     
						
					$tabledata.="<table  class='table' border=1 align='center' width=100%>
									
									<tr>
										<td><input type='hidden' id='submit1'   value='".$larrstudentscountcount['totalregistered'].",".$idbatch."'> </td>";
						$tabledata.='<td><button type="submit" class="NormalBtn" dojotype="dijit.form.Button"  id="submit" name="block" value="'.$larrstudentscountcount['totalregistered'].','.$idbatch.'">Block Registration</button> &nbsp;<input type="button" id="close" class="NormalBtn" dojotype="dijit.form.Button" label="Close
 name="close"  value="Close" onClick="BlockClosefn();"> </td>';
						$tabledata.=	"</tr>
								</table>";	
							
					echo  $tabledata;
					die();
				 
			}
			else
			{
			   $tabledata.="<table  class='table' border=1 align='center' width=100%>
									
									<tr>
										<td><b>Still Candidates there in tempexcelcandidates...First Delete And Then Block The Registration... !!!</td>
										<td><input type='button' id='close1' name='close'  value='Close' onClick='BlockClosefn();'> </td>
									</tr>
								</table>";	
								 //echo  $tabledata;
					//die();
					echo  $tabledata;
					die();
			
			}
		}
		
		
		//$availseat = $larrstudentscountcount ['totalNoofCandidates'] - $actualregistered;
		
		//$availseat = $larrstudentscountcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrstudentscountcount ['totalregistered']);
		
	   //$totalcandidates = $this->
	   
	   
	   
	   
	   
	}
	public function selectvenuecompanyAction(){
		
		$this->_helper->layout->disableLayout();
		$this->view->lobjstudentForm = $this->lobjstudentForm; 
		$day = $this->_getParam('day');
		$month = $this->_getParam('month');
		$year = $this->_getParam('year');
		$venue = $this->_getParam('city');
		if($month <10){
			$month = '0'.$month;
		}
		if($day <10){
			$day = '0'.$day;
		}
		 $selecteddate = $year.'-'.$month.'-'.$day;

		$studentapp = new App_Model_Studentapplication();
		$sessresult = $studentapp->fnGetvenuedatescheduleDetails($selecteddate,$venue);
		$this->view->sessresult = $sessresult;
		$this->view->regdate=$selecteddate;
		
		$result5 = $this->lobjstudentmodel->fngetdayStudent($selecteddate); 
		$this->view->daystu= $result5[0]['days'];
	
	}
	
	public function manualstudentsAction() 
	{   
		
		$auth = Zend_Auth::getInstance();
		$userid =  $auth->getIdentity()->iduser;
		$lintidcompany =$this->view->idcompany = $this->_getParam('idcompany');
		$lintidregistrationpin = $this->view->regpin = $this->_getParam('idRegPin');
		
		
		$auth = Zend_Auth::getInstance();
		
		$month = date ( "m" ); // Month value
		$day = date ( "d" ); //today's date
		$year = date ( "Y" ); // Year value
		$minmumage = new App_Model_Studentapplication ();
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		$larr = $minmumage->fngetminimumage ();
		$age = $larr [0] ['MinAge'];
		$eligibility = ($year) - ($age);
		
		$year = $eligibility;
		$this->view->yearss = $year;
		$this->view->minages = $age;
		
		$yeste = date ( 'Y-m-d', mktime ( 0, 0, 0, $month, ($day - 1), $year ) );
		$this->view->yesdate = $yeste;
		
		$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
		$companyaddress = $this->view->companydetails['Address'];	
		
		$larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$lintidregistrationpin);
		
		
		$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($lintidregistrationpin); // Newly added as on 21-01-2013
		
		if($larrstudentcounttemp ['totalcount']>$larrstudentscountcount ['totalregistered']){
			$actualregistered = $larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered = $larrstudentscountcount ['totalregistered'];
		}
		
		//$availseat = $larrstudentscountcount ['totalNoofCandidates'] - $actualregistered;
		
		$availseat = $larrstudentscountcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrstudentscountcount ['totalregistered']);
		
		$this->view->alreadyapppliedexcel = $availseat;
		$ids =$larrstudentscountcount['idBatchRegistration'];
		
		$laresultscandidate = $this->lobjTakafulcandidatesmodel->fngetBatchRegistration ( $ids );
		
		$noofcandidates = Array ();
		$noofexams = count ( $laresultscandidate );
		for($i = 0; $i < $noofexams; $i ++) {
			$noofcandidates ['idprgm'] [] = $laresultscandidate [$i] ['idProgram'];
			$noofcandidates ['ProgramName'] [$laresultscandidate [$i] ['idProgram']] = $laresultscandidate [$i] ['ProgramName'];
			$noofcandidates [$laresultscandidate [$i] ['idProgram']] = $laresultscandidate [$i] ['noofCandidates'];
			$noofcandidatesssss [] = $laresultscandidate [$i] ['noofCandidates'];
		}
		//echo "<pre/>";
		$larrbatchprog = $this->lobjTakafulcandidatesmodel->fnBatchProg ();
		for($g = 0; $g < count ( $noofcandidates ['idprgm'] ); $g ++) {
			$larrbatchprog123 [$noofcandidates ['idprgm'] [$g]] = $this->lobjTakafulcandidatesmodel->fnBatchProgram ( $noofcandidates ['idprgm'] [$g] );
		}
		$this->view->batchresults = $larrbatchprog123;

		$total = 0;
		for($m = 0; $m < count ( $noofcandidatesssss ); $m ++) {
			$total = $total + $noofcandidatesssss [$m];
		}
		//print_r($total);
		$this->view->total = $total;
		$this->view->noofprog = $noofcandidates ['idprgm'];
		$this->view->progname = $noofcandidates ['ProgramName'];
		$this->view->noofcandidates = $noofcandidates;
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
		$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		$larrresultprogram = $this->lobjTakafulcandidatesmodel->fnGetProgramName ();
		$this->view->programresult = $larrresultprogram;
		$larresultbatch = $this->lobjTakafulcandidatesmodel->fnGetBatchName ();
		$this->view->batchresult = $larresultbatch;
		$larrresultrace = $this->lobjCommon->fnGetRace ();
		$this->view->raceresult = $larrresultrace;
		$larrresuleducation = $this->lobjCommon->fnGetEducation ();
		$this->view->educationresult = $larrresuleducation;
		$larresultreligionoperator = $this->lobjCommon->fnGetAllActiveReligionNameList ();
		$this->view->Religion = $larresultreligionoperator;
		
		
		
		// Function to check the mode of pay and blockin if program already selected
		$larrresultBatch = $this->lobjTakafulapplicationmodel->fngetBatchDetails ( $lintidregistrationpin );		
		$larrgetpaydetails=$this->lobjcompanystudentdetails->fnGetModeofpay($larrresultBatch['idBatchRegistration']);		
		$this->view->mode0fpay=$larrgetpaydetails['ModeofPayment'];		
			
		$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($larrresultBatch['idBatchRegistration']);	
		$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
		$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
		$this->view->progid=$larrgetprogramapplied['idProgram'];
		
		$larreducationresult = $this->lobjCommon->fnGetCountryList ();
		$this->view->countryresult = $larreducationresult;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'save' )) {			
			$larrformData = $this->_request->getPost ();
			
				for($i=0;$i<count($larrformData['candidatename']);$i++){
					
					$larrdatainsert['StudentName']=$larrformData['candidatename'][$i];					
					$larrdatainsert['ICNO']=$larrformData['candidateicno'][$i];
					$larrdatainsert['Race']=$larrformData['candidaterace'][$i];
					$larrdatainsert['email']=$larrformData['candidateemail'][$i];
					$larrdatainsert['education']=$larrformData['candidateeducation'][$i];
					$larrdatainsert['Gender']=$larrformData['candidategender'][$i];
					$larrdatainsert['DOB']=$larrformData['candidatedateofbirth'][$i];
					$larrdatainsert['Address']=$larrformData['candidateaddress'][$i];
					$larrdatainsert['CorrespAddress']=$larrformData['correspondanceaddress'][$i];
					$larrdatainsert['PostalCode']=$larrformData['postalcode'][$i];
					$larrdatainsert['IdCountry']=$larrformData['candidatecountry'][$i];
					$larrdatainsert['IdState']=$larrformData['candidatestate'][$i];
					$larrdatainsert['ContactNo']=$larrformData['candidatenum'][$i];
					$larrdatainsert['MobileNo']=$larrformData['candidatemobnum'][$i];
						
					$this->lobjcompanystudentdetails->fninsertintotemp ( $larrdatainsert, $lintidcompany , $lintidregistrationpin );
				}
				
		$larrcandidateappliedcount=$this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$lintidregistrationpin);
		
		$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($lintidregistrationpin); // Newly added as on 21-01-2013

		if($larrstudentcounttemp ['totalcount']>$larrcandidateappliedcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrcandidateappliedcount ['totalregistered'];
		}
				
	     $availremainseat=$larrcandidateappliedcount['totalNoofCandidates'] - ($larrcandidateappliedcount['totalregistered']+$larrstudentcounttemp['totalcount']);
		//$availremainseat=$larrcandidateappliedcount['totalNoofCandidates'] - $actualregistered;
		
		$mainpath=$this->view->baseUrl();
				if($availremainseat!=0){
					echo '<script language="javascript">var theAnswer = confirm("Do You Wish To Insert More Candidates ?");					
					if(theAnswer){  	
						alert("Your Data is Saved.....Please Enter the Next Candidate");
						window.location.href = "'.$mainpath.'/registrations/companystudentdetails/manualstudents/idcompany/'.$lintidcompany.'/idRegPin/'.$lintidregistrationpin.'";
					}else{
						alert("Thank You for your Patience....Your Data Has been Saved. Please Schedule the Candidates Entered");						
						window.location.href = "'.$mainpath.'/registrations/companystudentdetails/coursevenue/idcompany/'.$lintidcompany.'/registrationpin/'.$lintidregistrationpin.'";
					}
					</script>';
					exit;
				}else{
					
					$this->_redirect( $this->baseUrl . 'registrations/companystudentdetails/companystudentlist/idcompany/'.$lintidcompany);
					exit;
				}
	
				
		   /*$larrgetvenuedetails = $this->lobjTakafulcandidatesmodel->fngetvenuedetailsinsert($larrformData ['NewVenue'] );
			$larrformData ['NewState'] = $larrgetvenuedetails ['state'];
			$larrformData ['NewCity'] = $larrgetvenuedetails ['city'];
			$regpin = $lintidregistrationpin;
			$countloop = count ( $larrformData ['candidatename'] );
			
			if(strlen($larrformData['setmonth']) <=1) {
				$larrformData['setmonth'] = '0'.$larrformData['setmonth'];
			}
			if(strlen($larrformData['setdate']) <=1) {
				$larrformData['setdate'] = '0'.$larrformData['setdate'];
			}	
			$availdate=$larrformData['Year']."-".$larrformData['setmonth']."-".$larrformData['setdate'];			
			$larravailseat=$this->lobjTakafulcandidatesmodel->fngetavailseatvenue($larrformData['NewVenue'],$availdate,$larrformData['idsession']);
			$larrformData['scheduler']=$larravailseat['idnewscheduler'];
			   
			if($countloop > $larravailseat['availseat']){
				echo '<script language="javascript">alert("The No of Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
				echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/companystudentlist/idcompany/".$lintidcompany."';</script>";
				exit;
			}			
			$larrinsertdata = $this->lobjcompanystudentdetails->fnInsertIntoStd($larrformData, $countloop, $ids, $regpin, $companyaddress,$lintidcompany,$userid,$this->gobjsessionsis->operatortype);
			*/
				
			
		}
		
	}
	
	public function excelstudentsAction() 
	{   
		$lintidcompany =$this->view->idcompany = $this->_getParam('idcompany');
		
		$ids = $lintidregistrationpin = $this->view->regpin = $this->_getParam('idRegPin');
		
		$auth = Zend_Auth::getInstance();
		$lobjUploadfilesForm = $this->lobjloadfilesForm; //intialize bank form
		$this->view->lobjUploadfilesForm = $lobjUploadfilesForm;
		
		$larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$lintidregistrationpin);
		$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($lintidregistrationpin); // Newly added as on 21-01-2013
		
		if($larrstudentcounttemp ['totalcount']>$larrstudentscountcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrstudentscountcount ['totalregistered'];
		}
	
		$availseat = $larrstudentscountcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrstudentscountcount ['totalregistered']);
        
       // $availseat = $larrstudentscountcount ['totalNoofCandidates'] - $actualregistered;
        
		$this->view->remspplication = $availseat;
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
			$larrformData = $this->_request->getPost (); //getting the values of bank from post 
			require_once 'Excel/excel_reader2.php';
			$lintfilecount ['Count'] = 0;
			$lstruploaddir = "/uploads/questions/";
			$larrformData ['FileLocation'] = $lstruploaddir;
			$larrformData ['UploadDate'] = date ( 'Y-m-d:H:i:s' );
			
			if ($_FILES ['FileName'] ['error'] != UPLOAD_ERR_NO_FILE) {
				$lintfilecount ['Count'] ++;
				$lstrfilename = pathinfo ( basename ( $_FILES ['FileName'] ['name'] ), PATHINFO_FILENAME );
				$lstrext = pathinfo ( basename ( $_FILES ['FileName'] ['name'] ), PATHINFO_EXTENSION );
				
				$filename = $lintfilecount ['Count'] . "." . date ( 'YmdHis' ) . "." . $lstrext;
				$filename = str_replace ( ' ', '_', $lstrfilename ) . "_" . $filename;
				$file = realpath ( '.' ) . $lstruploaddir . $filename;
				if (move_uploaded_file ( $_FILES ['FileName'] ['tmp_name'], $file )) {
					//echo "success";
					$larrformData ['FilePath'] = $filename;
					$larrData ['FileName'] = $lstrfilename;
					$larrData ['FilePath'] = $filename;
				} else {
					//echo "error";
				}
			}

			$userDoc = realpath ( APPLICATION_PATH . '/../public/uploads/questions/' . $filename );
			$data = new Spreadsheet_Excel_Reader( $userDoc );
			
			$arr = $data->sheets;
			for($i = 2; $i < 100; $i ++) 
			{
				if ($arr [0] ['cells'] [$i] [1] == '') 
				{
					break;
				} 
				else
				{ 
					$totalarray [$i] = $arr [0] ['cells'] [$i];
				}
			}
			
		$larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$lintidregistrationpin);
		$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($lintidregistrationpin); // Newly added as on 21-01-2013
		
		if($larrstudentcounttemp ['totalcount']>$larrstudentscountcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrstudentscountcount ['totalregistered'];
		}
			
			//$availseat = $larrstudentscountcount ['totalNoofCandidates'] - $actualregistered;
			
			$availseat = $larrstudentscountcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrstudentscountcount ['totalregistered']);
			
			if ($availseat < count ( $totalarray )) 
			{
				$counts = $availseat + 2;
			} 
			else 
			{
				$count = count ( $totalarray );
				$counts = $count + 2;
			}
			
			if ($arr [0] ['cells'] [1] [1] != "Student Name" || $arr [0] ['cells'] [1] [2] != "ICNO" || $arr [0] ['cells'] [1] [3] != "E-Mail" || $arr [0] ['cells'] [1] [4] != "Race" || $arr [0] ['cells'] [1] [5] != "Education" || $arr [0] ['cells'] [1] [6] != "DateofBirth" || $arr [0] ['cells'] [1] [7] != "Gender" || $arr [0] ['cells'] [1] [8] != "Mailing Address" || $arr [0] ['cells'] [1] [9] != "Correspondance Address" || $arr [0] ['cells'] [1] [10] != "Postal Code" || $arr [0] ['cells'] [1] [11] != "Country" || $arr [0] ['cells'] [1] [12] != "State" || $arr [0] ['cells'] [1] [13] != "Contact No" || $arr [0] ['cells'] [1] [14] != "Mobile No") 
			{
				echo '<script language="javascript">alert("Excel Sheet Not in correct Format")</script>';
				echo "<script>parent.location = '" . $this->view->baseUrl () . "registrations/companystudentdetails/excelstudents/idcompany/".$lintidcompany."/idRegPin/".$lintidregistrationpin."';</script>";
			}
			
				$idprogarray=$this->lobjTakafulcandidatesmodel->fnGetprogramappliedExcel ( $ids ); 
				$idprogramapplied=$idprogarray['idProgram'];
			
			
			
			for($iterexcelread = 2; $iterexcelread < $counts; $iterexcelread++) {
				
				$flag=0;
				///////////////////////NAme/////////////////////
				$larrdatainsert ['StudentName'] = $arr [0] ['cells'] [$iterexcelread] [1];				
				
				////////////////////ICNO///////////////////////////////
				$icno = $arr [0] ['cells'] [$iterexcelread] [2];
			if(is_numeric($icno)){
									$larrdatainsert ['ICNO'] = $arr [0] ['cells'] [$iterexcelread] [2];
									$icnos = "$icno";
									$larricno = $this->lobjBatchcandidatesmodel->fnGetIcno ( $icno , $idprogramapplied );  // function to validate ICNO
									$larricnoexcel = $this->lobjTakafulcandidatesmodel->fnGetIcno ( $icno,$idprogramapplied );  // function to validate ICNO	

									$dobicnum= "19".$icno[0].$icno[1]."-".$icno[2].$icno[3]."-".$icno[4].$icno[5];	
									$dobexcel = $arr [0] ['cells'] [$iterexcelread] [6];	
									$dobexcel=date('Y-m-d',strtotime($dobexcel));						
									if($icno[11]%2==0){
										if($arr [0] ['cells'] [$iterexcelread] [7] == 'Male' || $arr [0] ['cells'] [$iterexcelread] [7] == 'MALE'){
											$flag=12;
										}
									} else{
									if($arr [0] ['cells'] [$iterexcelread] [7] != 'Female' || $arr [0] ['cells'] [$iterexcelread] [7] == 'FEMALE'){
											$flag=12;
										}
									}			
									if($dobicnum!=$dobexcel){
										$flag=11;
									}			
									if(count($larricnoexcel) > 0){
										$flag=10;
									}else if (count($larricno) > 0){
										$flag=10;
									}else{
										$flag=1;
									}								
									$icnolen = strlen ( $icno );
									if ($icnolen != 12 || count($larricno)>0 || count($larricnoexcel)>0) {
										echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, $flag );
										continue;				
									}
				}else{
					
							echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
							$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 1 );
							//echo "<script>parent.location = '".$this->view->baseUrl()."/takafulcandidates/import/batchId/".$ids."';</script>";
							continue;	
				}
				
			
				$year = '19' . $icnos [0] . $icnos [1];
				$month = $icnos [2] . $icnos [3];
				$day = $icnos [4] . $icnos [5];
				$dob = $year . '-' . $month . '-' . $day;
				/////////////////ICNO ENDS////////////////////////////////
				/////////////////Email Starts////////////////////////////////
				$larrdatainsert ['email'] = $arr [0] ['cells'] [$iterexcelread] [3];
				$email = $arr [0] ['cells'] [$iterexcelread] [3];
				$larrmailexcel = $this->lobjTakafulcandidatesmodel->fnGetmailId ( $email );  // function to validate E-MAIL
				$larrmail = $this->lobjBatchcandidatesmodel->fnGetmailId ( $email );   // function to validate EMAIL
				$race = $larrmail ['EmailAddress'];
				$mailcount = strlen ( $larrmail ['EmailAddress'] );

				/*if (! $larrmail && ! $larrmailexcel) {
				
				} else {
					echo '<script language="javascript">alert("Email Already Taken")</script>';
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 2 );
					continue;								
				}*/
				
				/////////////////Email ENDS////////////////////////////////
				///////////////////////RACE/////////////////////////////////
				
				$chienesearray = $arr [0] ['cells'] [$iterexcelread] [4];
				$larrrace = $this->lobjBatchcandidatesmodel->fnGetRaceId ( $chienesearray );// function to check race
				
				if(!$larrrace){
					$others="Others";
					$larrraceothers = $this->lobjBatchcandidatesmodel->fnGetRaceId ( $others );					
					$larrdatainsert ['Race'] = $larrraceothers ['idDefinition'];
				}else{
					$larrdatainsert ['Race'] = $larrrace ['idDefinition'];
				}
								
				$racecount = strlen ( $larrdatainsert ['Race'] );			
				if (!$larrdatainsert ['Race']) {
					echo '<script language="javascript">alert("Please check the race and upload the file")</script>';
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 4 );
					continue;	
				}				
				////////////////////////RACE ENDS////////////////////////////////
				///////////////////////Education////////////////////////////////
				$educationarray = $arr [0] ['cells'] [$iterexcelread] [5];
				$larreducation = $this->lobjBatchcandidatesmodel->fnGetEducatinexcel ( $educationarray ); // function to check education
				
				if(!$larreducation){
					$others="Others";
					$larreducationothers = $this->lobjBatchcandidatesmodel->fnGetEducatinexcel ( $others );
					$larrdatainsert ['education'] = $larreducationothers ['idDefinition'];
				}else{
					$larrdatainsert ['education'] = $larreducation ['idDefinition'];				
				}
				
				if ($larrdatainsert ['education']) {
				
				} else {
					echo '<script language="javascript">alert("Please check the Education and upload the file")</script>';
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 5 );
					continue;					
				}
				
				/////////////////////////////////////////////////////////////////////
				$dob = $larrdatainsert ['DOB'] = $arr [0] ['cells'] [$iterexcelread] [6];
				$toddate = date ( 'Y-m-d' );
				$diff = abs ( strtotime ( $toddate ) - strtotime ( $dob ) );
				$years = floor ( $diff / (365 * 60 * 60 * 24) );
				if ($years < 18) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 3 );
					continue;
				}
				
				//////////////////////////////////GENDER////////////////////////

				if ($arr [0] ['cells'] [$iterexcelread] [7] == 'Male' ||$arr [0] ['cells'] [$iterexcelread] [7] == 'MALE') {
					$larrdatainsert ['Gender'] = 1;
				} else if ($arr [0] ['cells'] [$iterexcelread] [7] == 'Female' || $arr [0] ['cells'] [$iterexcelread] [7] == 'FEMALE') {
					$larrdatainsert ['Gender'] = 0;
				} else {
					echo '<script language="javascript">alert("Please check the Gender and upload the file")</script>';
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 6 );
					continue;
				}
				
				///////////////////////////////////GENDER ENDS////////////

				///////////////////ADDRESS/////////////////////
				$larrdatainsert ['Address'] = $arr [0] ['cells'] [$iterexcelread] [8];
				$larrdatainsert ['CorrespAddress'] = $arr [0] ['cells'] [$iterexcelread] [9];
				$larrdatainsert ['PostalCode'] = $arr [0] ['cells'] [$iterexcelread] [10];
				if (! $larrdatainsert ['PostalCode']) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 7 );
					continue;
				}
					
				///////////////////////Country//////////////////////////////////
				$countryarray = $arr [0] ['cells'] [$iterexcelread] [11];
				if (!$countryarray) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 8 );
					continue;
				}
				$larrcountry = $this->lobjBatchcandidatesmodel->fnGetCountryexcel ( $countryarray );	
				
				if(!$larrcountry){
					$countryarraydefault="MALAYSIA";
					$larrcountrydefault = $this->lobjBatchcandidatesmodel->fnGetCountryexcel ( $countryarraydefault );
					$larrdatainsert ['IdCountry']=$larrcountrydefault['idCountry'];
				}else{
					$larrdatainsert ['IdCountry'] = $larrcountry ['idCountry'];		
				}
											     		   
				if ($larrcountry ['idCountry'] != "") {
				
				} else {
					echo '<script language="javascript">alert("Please check the Country List and upload the file")</script>';	
					//Function to log errors				
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 8 );
					continue;					
				}
				//////////////////////Country//////////////////////////////////
				/////////////////////State////////////////////////////////////
				$statearray = $arr [0] ['cells'] [$iterexcelread] [12];
				if (!$statearray) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 9 );
					continue;
				}
				
				$larrstate = $this->lobjBatchcandidatesmodel->fnGetStateexcel ( $statearray );// Function to validate and check states	
				if(!$larrstate){
					$others="Others";
					//$larrstateothers = $this->lobjBatchcandidatesmodel->fnGetStateexcelothers ( $statearray );
					$larrstateothers = $this->lobjBatchcandidatesmodel->fnGetStateexcelothers ( $larrdatainsert['IdCountry'],$others );
					$larrdatainsert['IdState'] = $larrstateothers ['idState'];	
				}else{
					$larrdatainsert['IdState'] = $larrstate ['idState'];	
				}
							
				if ($larrdatainsert['IdState'] != "") {
				} else {
					echo '<script language="javascript">alert("Please check the State List and upload the file")</script>';
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 9 );
					continue;					
				}

				//////////////////////////////////////////////////////////////
				$larrdatainsert ['ContactNo'] = $arr [0] ['cells'] [$iterexcelread] [13];
				$larrdatainsert ['MobileNo'] = $arr [0] ['cells'] [$iterexcelread] [14];
				$larrdatainsert ['idprogram']=$idprogramapplied;	
				
				$insertarray = $this->lobjTakafulcandidatesmodel->fninsertintotemp ( $larrdatainsert, $lintidcompany , $ids );
								
				$larrdataimport ['UpdUser'] = $lintidcompany;
				$larrdataimport ['IdregistrationPin'] = $ids;
				$larrdataimport ['Typeofimport'] = 1;	
				$insertarray = $this->lobjTakafulcandidatesmodel->fninserttoimported ( $larrdataimport );
			}
			//echo $iterexcelread;
			$this->_redirect( $this->baseUrl . 'registrations/companystudentdetails/coursevenue/registrationpin/'.$lintidregistrationpin.'/idcompany/'.$lintidcompany);
		}

	}
	
	public function coursevenueAction() {  // Action for Scheduling students  added from excel
		
		$auth = Zend_Auth::getInstance();
		$userid =  $auth->getIdentity()->iduser;
		$ids = $this->_getParam ( 'registrationpin' );
		$idCompany = $this->_getParam ( 'idcompany' );
		
		
		$this->view->idcomps = $idCompany;
		$this->view->lobjTakafulcandidatesForm = $this->lobjTakafulcandidatesForm;
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		$this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;

		$this->view->idbatchss = $ids;
		$laresultscandidate = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationimport ( $ids );
	
		$noofcandidates = Array ();
		$noofexams = count ( $laresultscandidate );
		
		for($iteresultscandidate = 0; $iteresultscandidate < $noofexams; $iteresultscandidate ++) {
			$noofcandidates ['idprgm'] [] = $laresultscandidate [$iteresultscandidate] ['idProgram'];
			$noofcandidates ['ProgramName'] [$laresultscandidate [$iteresultscandidate] ['idProgram']] = $laresultscandidate [$iteresultscandidate] ['ProgramName'];
			$noofcandidates [$laresultscandidate [$iteresultscandidate] ['idProgram']] = $laresultscandidate [$iteresultscandidate] ['noofCandidates'];
			$noofcandidatesssss [] = $laresultscandidate [$iteresultscandidate] ['noofCandidates'];
			$noofcandidates ['programid'] = $laresultscandidate [$iteresultscandidate] ['idProgram'];
		}
		
		$total = 0;
		
		for($iternumcandidate = 0; $iternumcandidate < count ( $noofcandidatesssss ); $iternumcandidate ++) {
			$total = $total + $noofcandidatesssss [$iternumcandidate];
		}
		
		$this->view->total = $total;
		$this->view->noofprog = $noofcandidates ['idprgm'];
		$this->view->idprogram = $noofcandidates ['programid'];
		$this->view->progname = $noofcandidates ['ProgramName'];
		$this->view->noofcandidates = $noofcandidates;
		$this->view->operatortype=$this->gobjsessionsis->operatortype;
		$larrtempexcelcandidates = $this->lobjcompanystudentdetails->fngetnoofstudentsfromexcel ( $ids);
		
		$this->view->takcandiddetails = $larrtempexcelcandidates;
		$this->view->totalexcelstudents = count ( $larrtempexcelcandidates ); 
		$this->view->countparts = count ( $laresultscandidate );
		$this->view->programresult = $laresultscandidate;
		

		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
		$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		
	// Function to check the mode of pay and blockin if program already selected
		$larrresultBatch = $this->lobjTakafulapplicationmodel->fngetBatchDetails ( $ids );		
		$larrgetpaydetails=$this->lobjcompanystudentdetails->fnGetModeofpay($larrresultBatch['idBatchRegistration']);		
		$this->view->mode0fpay=$larrgetpaydetails['ModeofPayment'];		
			
		$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($larrresultBatch['idBatchRegistration']);	
		$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
		$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
		$this->view->progid=$larrgetprogramapplied['idProgram'];

		$larrexcelappliedcandidates = $this->lobjcompanystudentdetails->fngetexcelappliedcandidates ($ids);
		$this->view->larrappliedresult = $larrexcelappliedcandidates;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost ();
			//echo "<pre>";
			//print_r($larrformData);die();
			
			$larrformData ['Examvenue'] = $larrformData ['NewVenue'];
			$resultstate = $this->lobjstudentmodel->fngetstatecity ( $larrformData ['NewVenue'] );
			$larrformData ['ExamState'] = $resultstate ['state'];
			$larrformData ['ExamCity'] = $resultstate ['city'];
			$larrformData ['NewState'] = $resultstate ['state'];
			$larrformData ['NewCity'] = $resultstate ['city'];
			$larrformData ['hiddenscheduler'] = 1;
			

			if(strlen($larrformData['setmonth']) <= 1) {
	         	$monthsss = '0'.$larrformData['setmonth'];
	        } else {
	         	$monthsss = $larrformData['setmonth'];
	        }	            
			if(strlen($larrformData['setdate']) <= 1) {
	         	$dayssss = '0'.$larrformData['setdate'];
	        } else {
	         	$dayssss = $larrformData['setdate'];
	        }
			
			$availdate=$larrformData['Year']."-".$monthsss."-".$dayssss;		
			$larravailseat=$this->lobjTakafulcandidatesmodel->fngetavailseatvenue($larrformData['NewVenue'],$availdate,$larrformData['idsession']);
			
			$larrformData['scheduler']=$larravailseat['idnewscheduler'];
			
			if(count ( $larrformData ['studenttakful'] ) >= $larravailseat['availseat']){
				echo '<script language="javascript">alert("The Noof Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
				echo "<script>parent.location = '".$this->view->baseUrl()."/takafulcandidates/coursevenue/batchId/".$ids."';</script>";
				exit;
			}

			if (count ( $larrformData ['studenttakful'] ) > 0) {
				$larrbatchregID = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationPinforexcel ( $larrformData ['idbatch'] );
				$lintidbatch = $larrbatchregID ['idBatchRegistration'];
				$linttotnumofapplicant = $larrbatchregID ['totalNoofCandidates'];
				$larrinsertstudent = $this->lobjcompanystudentdetails->fnInsertintostudapplicationexcel ( $larrformData, $lintidbatch, $linttotnumofapplicant, $larrformData ['idbatch'],$idCompany ,$userid,$this->gobjsessionsis->operatortype);
			} else {
				?><script>alert("Check any Applicant")</script><?php
			}
			$this->_redirect( $this->baseUrl . 'registrations/companystudentdetails/coursevenue/registrationpin/'.$ids.'/idcompany/'.$idCompany);
					
		}
	
	}
	
	public function viewerrapplicationAction() {  //Action to view the error applications from excel upload
		$regpin = $this->_getParam ( 'regpin' ); 
		$idCompany = $this->_getParam ( 'idcompany' ); 
		$this->view->idcompany = $idCompany;
		$this->view->idbatchss = $regpin;
		$larrerrresult = $this->lobjTakafulcandidatesmodel->fngetErrstudentapllication ( $regpin );
		$this->view->larrappliederr = $larrerrresult;
		
	}
	
	
	public function deletefromtempAction(){ // ADDED on 28-2-2013
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$idcandidates = $this->_getParam ( 'idcandidates' );
		$result = $this->lobjTakafulcandidatesmodel->fndeletefromtemp ( $idcandidates );
		echo $result;
		exit;
		
	}
	
	public function fngetstateAction() {
		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$idcountry = $this->_getParam ( 'idcountry' );
		$larrstatelist = $this->lobjCommon->fnGetCountryStateList ( $idcountry );
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrstatelist );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetailss );
	}
	
	public function fngetstudentconfirmAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		
		//Get Country Id
		$Program = $this->_getParam ( 'Program' );
		$icno = $this->_getParam ( 'icno' );
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnstudentconfirm ( $Program, $icno );
		$pass = $larrvenuetimeresult ['pass'];
		if ($pass == 3) {
			echo '1' . '***' . 'You have already applied for the exam';
		} else if ($pass == 1) {
			echo '1' . '***' . 'You have already Passed the exam';
		} else {
			echo '0' . '***';
		}
	}
	
	public function fngetemaildetailsAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		
		//Get Country Id
		$Program = $this->_getParam ( 'Program' );
		$idyear = $this->_getParam ( 'Year' );
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fngetstudentsemail ( $Program, $idyear );
		if ($larrvenuetimeresult) {
			echo 'We observe that the email id provided already exists, please login to the portal if you have already registered. If you have not registered earlier, please provide with another email id';
		
		}
	}
	
	
	public function pdfexportAction()
	{
		
		//Exporting data to an excel sheet 
		$lstrreportytpe="";
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) {
				
			$larrformData = $this->_request->getPost ();				
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
				
		$time = date('h:i:s',time());
		$filename = 'Company_Registration_Report_'.$frmdate;
		$ReportName = $this->view->translate( "Company" ).' '.$this->view->translate( "Registration" ).' '.$this->view->translate( "Report" );
		$companyName="Name Of The Company :".$larrformData['CompanyName'];
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
			$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
		}
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 7><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 10><b> {$ReportName}</b></td>									
							</tr>
							<tr>								
								<td align=left colspan = 10><b> {$companyName}</b></td>
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b></b></th>
							<th><b>Student Name</b></th>
							<th><b>ICNO</b></th>
							<th><b>E-Mail</b></th>
							<th><b>Registration Id</b></th>
							<th><b>Program Applied</b></th>
							<th><b>Venue Name</b></th>
							<th><b>Exam Date</b></th>
							<th><b>Exam Time</b></th>
							<th><b>Exam Status</b></th>						
						</tr>';     
	 
		if (count($larrformData)){
			 $cnt = 0; 
		
			 
      	for($expdata=0;$expdata<count($larrformData['FName']);$expdata++){
      		  	
			$tabledata.= ' <tr>
				   		   <td><b>'; 
      		      $tabledata.= '</b></td>
					       <td>'.$larrformData['FName'][$expdata].'</td> 
						   <td>'.$larrformData['ICNO'][$expdata].'</td> 
						   <td>'.$larrformData['EmailAddress'][$expdata].'</td> 
						   <td>'.$larrformData['Regid'][$expdata].'</td> 
						   <td>'.$larrformData['ProgramName'][$expdata].'</td> 
						   <td>'.$larrformData['centername'][$expdata].'</td> 		  
						   <td>'.$larrformData['dateofexam'][$expdata].'</td>
						   <td>'.$larrformData['examtime'][$expdata].'</td>  
						   <td>'.$larrformData['status'][$expdata].'</td> 		   
				        </tr> ';				   	
	    	$cnt++; 
      		  }
	     
		 }		
	
		 $tabledata.= '<tr>		      
		     	<td></td>
		     	<td></td>
		     	<td></td>
		     </tr>
		     </table>';	
		
		//echo $tabledata;exit;
		if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}
			
		}
		
	}
	
	public function pdfexportregAction()
	{
		//Exporting data to an excel sheet 
		$lstrreportytpe="";
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) {
				
			$larrformData = $this->_request->getPost ();	
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
			
		$time = date('h:i:s',time());
		$filename = 'Company_Registration_Report_'.$frmdate;
		$ReportName = $this->view->translate( "Company" ).' '.$this->view->translate( "Registration" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
			$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
		}
			$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b>Time</b></td>
								<td align = 'left' colspan= 6><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 9><b> {$ReportName}</b></td>	
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b></b></th>
							<th><b>Student Name</b></th>
							<th><b>ICNO</b></th>
							<th><b>E-Mail</b></th>
							<th><b>Registration Id</b></th>
							<th><b>Program Applied</b></th>
							<th><b>Venue Name</b></th>
							<th><b>Exam Date</b></th>
							<th><b>Exam Session</b></th>						
						</tr>';     
	 
		if (count($larrformData)){
			 $cnt = 0; 
		
			 
      	for($expdata=0;$expdata<count($larrformData['FName']);$expdata++){
      		  	
			$tabledata.= ' <tr>
				   		   <td><b>'; 
      		      $tabledata.= '</b></td>
					       <td>'.$larrformData['FName'][$expdata].'</td> 
						   <td>'.$larrformData['ICNO'][$expdata].'</td> 
						   <td>'.$larrformData['Email'][$expdata].'</td> 
						   <td>'.$larrformData['Regid'][$expdata].'</td> 
						   <td>'.$larrformData['ProgramName'][$expdata].'</td> 
						   <td>'.$larrformData['centername'][$expdata].'</td> 		  
						   <td>'.$larrformData['dateofexam'][$expdata].'</td> 
						   <td>'.$larrformData['session'][$expdata].'</td> 		   
				        </tr> ';				   	
	    	$cnt++; 
      		  }
	     
		 }		
	
		 $tabledata.= '<tr>		      
		     	<td></td>
		     	<td></td>
		     	<td></td>
		     </tr>
		     </table>';	
		
		//echo $tabledata;exit;
		if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}
	}
		

				
}

	public function changecompanypaymentAction()
	{
	   $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender ();
	    $idcompany = $this->_getParam('idcompany');
		$companyflag = $this->_getParam('flag');
		$idRegPin = $this->_getParam('idRegPin');
		//echo $larrresult;
	   //print_r($larrresult);
	   //die();
	    $idbatch = $this->_getParam('idbatch');
	  $larrresult =0;
	  
	   $previousdetails = $this->lobjcompanystudentdetails->fngetpindetails($idbatch,$companyflag);
	   $paymentmode = $previousdetails['ModeofPayment'];
	   $upddatetime = $previousdetails['UpdDate'];
	   $upduser = $previousdetails['UpdUser'];
	  // echo "<pre>";
	   //print_r($previousdetails);die();
	   if($companyflag ==1)
	   {
	      $larrresult = $this->lobjcompanystudentdetails->checkforcredittoibfim($idcompany);
		}
	    
	  
	   
	    
	   $larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$idRegPin);
	  // $larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($idRegPin); // Newly added as on 21-01-2013
			
		$idcompanyflag = $this->_getParam('companyflag');
		$this->view->idbatchreg = $idBatchRegistration = $this->_getParam('idBatchRegistration');
					
		$lobjpaymentdetails = $this->lobjcompanystudentdetails->fngetpaymentdetails($idbatch,$companyflag);
			$amt=$lobjpaymentdetails[0]['totalAmount'];
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left">Present Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Company Name</b></th>
					<th><b>Amount</b></th>
					<th><b>Number Of Candidates</b></th>
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalAmount']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalNoofCandidates']."</b></td>
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 7) $tabledata.= 'Credit to IBFIM';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 10) $tabledata.= 'MIGS';
		
		$tabledata.="</tr></table>";
		$tabledata.= '<br><fieldset><legend align = "left">Change Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><td><b>Change Payment Mode To:</b></td>";
					if($companyflag == 1)
					{
							if($larrresult)
							{
								$tabledata.= '<td ><select name="paymentdetails" style="width=400px" dojoType="dijit.form.FilteringSelect" id="comp" >
														<option id="1" value="4">Money Order</option>
														<option id="2" value="7">Credit To IBFIM</option>
											</select></td>';
							}
							else
							{
									$tabledata.= '<td ><select name="paymentdetails" dojoType="dijit.form.FilteringSelect" id="comp" >
												<option id="1" value="4">Money Order</option></select></td>';
							   
							}
					}		
				else{
						   $tabledata.= '<td ><select name="paymentdetails" dojoType="dijit.form.FilteringSelect" id="comp" >
												<option id="1" value="4" >Money Order</option>
												<option id="2" value="181">Paylater</option>
									</select></td>';
					}

								
		$tabledata.='<tr><td><b>Reference number:</b></td><td><input type="text" dojoType="dijit.form.ValidationTextBox" id="refnumber" name="refnumber" required="true"/></td><tr>';
		//$tabledata.='<tr><td><b>Date:</b></td><td><input type="date" name="date1" id="date1" dojoType="dijit.form.DateTextBox"   /></td></tr>';
					
		$tabledata.='</table>';
					
					$tabledata.="<table   class='table' border=1 align='center' width=100%>
									
									<tr>";
										
						 $tabledata.='<td align="center"><button type="submit" class="NormalBtn" dojotype="dijit.form.Button"  id="paymentchangerequest"  name="paymentchangerequest" value="'.$paymentmode.','.$upddatetime.','.$upduser.','.$idbatch.','.$idcompany.','.$idRegPin.','.$amt.'">Save</button> &nbsp;<input type="button" id="closechangerequest" class="NormalBtn" dojotype="dijit.form.Button" label="Close
 name="Back"  value="Close" onClick="fnpaymentchangerequest();"> </td>';
						$tabledata.=	"</tr>
								</table>";	
							
					echo  $tabledata;
					die();
		
		
		
		
		
		
		
		
		
		
		
	   
		
	
	}
	
	
	
}