<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Registrations_StudentupdateController extends Base_Base 
{
    private $_gobjlogger;
    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->lobjCommon = new App_Model_Common();
    }
    public function fnsetObj()
    {
		$this->lobjstudentupdatemodel = new Registrations_Model_DbTable_Studentupdate();
		$this->lobjstudentupdateform = new Registrations_Form_Studentupdate();  	
	}
    public function indexAction()
    {
       	$this->view->title="Program Setup";
		$this->view->lobjstudentupdateform = $this->lobjstudentupdateform;
		$larrresult = $this->lobjstudentupdatemodel->fnGetStudentDetails(); //get user details
		$this->view->countcomp = count($larrresult);
		$lintpagecount =10;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$this->view->display =0;
		if(isset($this->gobjsessionstudent->studentupdatepaginatorresult))
		{
		    $this->view->display =1;
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->studentupdatepaginatorresult,$lintpage,$lintpagecount);
		}
		else 
		{
		    $this->view->display =1;
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
        if ($this->_request->isPost () && $this->_request->getPost ('Search')) 
		{
		     $this->view->display =1;
			$larrformData = $this->_request->getPost ();
			$this->lobjstudentupdateform->populate($larrformData);
			if ($this->lobjform->isValid ( $larrformData ))
			{
			    
				$larrresult = $this->lobjstudentupdatemodel->fngetStudentSearch($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->studentupdatepaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' ))
		{
			$this->_redirect( $this->baseUrl . '/registrations/studentupdate/index');
		}
    }
    public function updateicnoAction()
    {
    	$idappn = $this->_getParam('idapp');
		$larrresult = $this->lobjstudentupdatemodel->fnStudentdetails($idappn); 
		
    	$this->lobjstudentupdateform->ProgramName->setAttrib('readonly','true'); 	
		$this->lobjstudentupdateform->centername->setAttrib('readonly','true'); 	
		$this->lobjstudentupdateform->managesessionname->setAttrib('readonly','true'); 
		$this->lobjstudentupdateform->ICNO->setAttrib('onchange','fnchangedate(this.value)'); 
		$this->lobjstudentupdateform->DateOfBirth->setAttrib('readonly','true'); 	
		$this->lobjstudentupdateform->idapp->setValue($larrresult['IDApplication']);
		$this->lobjstudentupdateform->populate($larrresult);
		$this->view->lobjstudentupdateform = $this->lobjstudentupdateform;
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
			$larrformData = $this->_request->getPost ();
			$larrduplicate = $this->lobjstudentupdatemodel->fncheckduplicateicno($larrformData['idapp'],$larrformData['ICNO']);
			if($larrduplicate)
			{
			    echo '<script language="javascript">alert("ICNO Already Exist Please Change IC Number");</script>';   	
				echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/studentupdate/index';</script>";
				die();
			}
			$mainstr = $larrformData['ICNO'];
			$yr = substr($mainstr, 0,2);
			$month = substr($mainstr, 2,2);
			$day = substr($mainstr, 4,2);
			$getminimumyear = $this->lobjstudentupdatemodel->fngetyear();
		    $minage = $getminimumyear['age'];
		    $larrvalidate= $this->lobjstudentupdatemodel->fngetvalidate($minage); 
			if(is_numeric($larrformData['ICNO']))
			{		
			    $DateOfBirth = "19".$yr.'-'.$month.'-'.$day;
					if(strlen($larrformData['ICNO']) != 12)
					{
						  echo '<script language="javascript">alert("Informat ICNO");</script>';   	
						  echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/studentupdate/index';</script>";
						  die();
					}
					elseif($DateOfBirth <= $larrvalidate['validdate'])
					{
					  
						if($yr >= 01 && $yr <= 35)
						{
						   $dateofbirth = "20".$yr.'-'.$month.'-'.$day;
						}
						else
						{
						   $dateofbirth = "19".$yr.'-'.$month.'-'.$day;
						}
				   }
				   else
				   {
					   echo '<script language="javascript">alert("Not Eligible");</script>';   	
					   echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/studentupdate/index';</script>";
					   die();
				   }
			}
            else
            {
			    echo '<script language="javascript">alert("ICNO is not numeric");</script>';   	
			    echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/studentupdate/index';</script>";
				die();
            }
			
			if ($this->lobjform->isValid ( $larrformData ))
			{
			   
				 $larrresult = $this->lobjstudentupdatemodel->fnupdateStudentdetails($larrformData['FName'],$larrformData['ICNO'],$dateofbirth,$larrformData['idapp']); //searching the values for the user
				 echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/studentupdate/index';</script>";
				 $this->view->display =1;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Back' ))
		{
			$this->_redirect( $this->baseUrl . '/registrations/studentupdate/index');
		}
    }
	public function fncheckduplicateicnoAction()
	{
	   $this->_helper->viewRenderer->setNoRender();
	   $this->_helper->layout->disableLayout();
       $icno = $this->_getParam('icnumber');
	   $idapp = $this->_getParam('Idappn');
	   $larresult = $this->lobjstudentupdatemodel->fncheckduplicateicno($idapp,$icno);   
	   if(empty($larresult))
	   {
		 echo 1;
	   }	
	   else
	   {	 
		 echo 0;
	   } 
	
	}

}

