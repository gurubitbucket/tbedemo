<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Registrations_BatchapprovalController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjBatchapproval = new Registrations_Model_DbTable_Batchapproval();
		//$this->lobjStudentpaymentForm = new GeneralSetup_Form_Studentpayment ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjBatchapproval->fnGetCompanyDetails(); //get user details
		/*echo('<pre>');
		print_r($larrresult);die();*/
		//$larrresult1 = $this->lobjBatchapproval->fnGetcourse();
		/*echo('<pre>');
		print_r($larrresult1);die();*/
		$this->view->countcomp = count($larrresult);
		$lintpagecount =$this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost() && $this->_request->getPost('Approve')) {
			$lobjFormData = $this->_request->getPost();
			$larrresult = $this->lobjBatchapproval->fnBatchApproved($lobjFormData);
			$this->_redirect($this->baseUrl . '/registrations/batchapproval/index');
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjBatchapproval->fngetBatchSearch($larrformData); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->semesterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/registrations/batchapproval/index');
			//$this->_redirect($this->view->url(array('module'=>'registrations' ,'controller'=>'batchapproval', 'action'=>'index'),'default',true));
		}
		
		
    }
    
    public function batchapprovallistAction()
    {
    	$lstrType = $this->_getParam('lvaredit');
    	$larrstudentdetails=$this->lobjBatchapproval->fnGetStudentDetails($lstrType);
 /*   	echo "<pre/>";
    	print_r($larrstudentdetails);
    	die();*/
    	$this->view->studentdetails = $larrstudentdetails;
    	//$idbatch = $larrstudentname['IdBatch'];
	

    }

}

