<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class Registrations_PaymentdistributionController extends Base_Base
{
	public function init()
	{
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance
		Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator		
		$this->lobjdistributionForm = new  Registrations_Form_Paymentdistribution();//distribution form
		$this->lobjdistributionModel = new Registrations_Model_DbTable_Paymentdistribution();//distribution model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	
	public function indexAction()//function to set and display the result
	{
		$this->view->lobjform = $this->lobjdistributionForm;	
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Company";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Takaful";
		$this->view->lobjform->operator->addMultiOptions($larrtypeofcompany);
		$this->lobjdistributionForm->operatorname->setAttrib('onkeyup', 'fnGetOperatorNames');
	    $this->lobjdistributionForm->operator->setAttrib('onChange', "fnemptyoperatornames");
	    
		if ($this->_request->isPost () && $this->_request->getPost( 'Save' ))	{
			$larrformData = $this->_request->getPost ();
		 	if ($this->lobjdistributionForm->isValid($larrformData)){
		 		$this->view->generated=1;
			 	unset ( $larrformData ['Save'] );
			 	$idcompany = $larrformData['idoperator'];
				$companyflag = $larrformData['Flag'];
				if($larrformData['Flag'] == 2){$Typeofoperator = 'T';}
				if($larrformData['Flag']==1){$Typeofoperator = 'C';}
				unset ( $larrformData ['idoperator'] );
				unset ( $larrformData ['Flag'] );
			 	$Uniqueid = "RCP/IBFIM/".$Typeofoperator.'/'.$idcompany.'/'.date('Y').'/';
			 	$auth = Zend_Auth::getInstance();
			 	$upduser =  $auth->getIdentity()->iduser;
			 	$receiptlink = $this->lobjdistributionModel->fninsertreceiptdetails($larrformData,$Uniqueid,$companyflag,$idcompany,$upduser); //insert receipt details
				
			 	//receipt report purpose//////
			 	$receiptnum=$this->view->receiptnum =  $receiptlink['ReceiptNum'];
			 	$this->view->generated=1;
			 	$this->view->companyflag=$companyflag;
				$receiptdetails = $this->lobjdistributionModel->fngetreceiptdetails($receiptnum,$companyflag); //get receipt details
				//echo "<pre/>";print_r($receiptdetails);die();
				
				
				$this->view->PaymentMode =$larrformData['PaymentMode'];
				$this->view->receiptdetails =$receiptdetails;
			 	//$this->_redirect( $this->baseUrl . '/registrations/paymentdistribution/index');
			}
		}
			
		if ($this->_request->isPost () && $this->_request->getPost ( 'Print' )){			  
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$this->lobjdistributionModel = new Registrations_Model_DbTable_Paymentdistribution();//distribution model
			$lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //amount in words
										
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";				
			$time = date('h:i:s',time());
			$filename = 'Receipt_Report_'.$frmdate;
			$ReportName = $this->view->translate( "RECEIPT" );				
			$lstrreportytpe="Pdf";	
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
			$larrformData = $this->_request->getPost ();	
			//print_r($larrformData);die();			 
			$date =date('d-m-Y');				
			$rcn = $larrformData['rcn'];
			$flag = $larrformData['flag'];
			$receiptdetails = $this->lobjdistributionModel->fngetreceiptdetails($rcn,$flag); //get receipt details
			$totalamt=$receiptdetails['0']['totalamount'];
			$Amountinwords = $lobjcompanystudentdetails->fnconvertnumbertowords($totalamt);
			if($receiptdetails['0']['PaymentMode']==1){
				$mode="Cheque";
				$condition="*This Receipt is valid subject to realisation of Cheque";
			}else if($receiptdetails['0']['PaymentMode']==2){
				$mode="Cash";
				$condition="";
			}
			if(isset($receiptdetails['0']['Fax'])){
				$fax=$receiptdetails['0']['Fax'];
			}else {
				$fax="-";
			}
			
			$tabledata.= "<br/>";
			
			$tabledata.="<div style='border:0.5mm solid;font-family:arial;'>";
			$tabledata.="<table border='0' width='100%' align='center' >
							 	<tr>
								 	<td align='left' width='40%'><b>No. {$rcn}<br/></b></td>
								  	<td align='left' style='font-size:130%;'><b><u><i>RECEIPT</i></u></b><br/></td>
								 	<td align='left'><b>Date: {$date}<br/></b></td>
								</tr>
								<tr >
								  	<td align='left' colspan='3'><i>Received With Thanks from:<b>{$larrformData['CompanyName']}</b></i><br/></td>
								</tr>
								<tr>
									<td align='left' colspan='3'><i>Address : {$larrformData['Address']}</i></td>
								</tr>
								<tr>
									<td align='left' colspan='3' style='padding-left:7%'>
										<i>{$larrformData['StateName']}<br/>
				 							{$larrformData['CountryName']}<br/>
				 							PostCode:{$larrformData['Postcode']}<br/>
				 							Phone:{$larrformData['Phone']}<br/>
				 							Email:{$larrformData['Email']}<br/>
				 							Fax:{$fax}<br/>
				 							Attn:{$larrformData['Contact']}
				 						 </i>
									</td>
									
								</tr>
								<tr>
									<td align='left' colspan='3'><i>a Sum of Ringgit Malaysia: {$Amountinwords} Only.</i><br/></td>
								</tr>
								<tr >
									<td align='left' colspan='3'><i>By: {$mode}.</i><br/></td>
								</tr>
								<tr>
									<td align='left' colspan='3'><i>towards Examination Fees as detail bellow.</i><br/></td>
								</tr>
						</table>";
			
			$tabledata.="<br/><table border='1' width='100%' align='center' style='margin:1em 1cm;'>
							<tr>
								<th ><b>SL NO</b></th>
								<th ><b>BatchId</b></th>							
								<th ><b>Invoice Number</b></th>
								<th ><b>Batch Amount</b></th>
								<th ><b>Outstanding Amount</b></th>							
								<th ><b>Received Amount</b></th>
								<th ><b>Balance</b></th>
							</tr>";
						$count=1;
						foreach($receiptdetails as $receiptdetails){
							$sum=0;$outstandingamountafterinsert=0;$outstandingamount=0;$balance=0;
							$sum=$this->lobjdistributionModel->fngetamount($receiptdetails['BatchId']);
							$outstandingamountafterinsert=($receiptdetails['batchamount']-$sum['0']['sum']);
							$outstandingamount=$outstandingamountafterinsert+$receiptdetails['Amount'];
							$balance=$outstandingamount-$receiptdetails['Amount'];
							 
                              
						
				$tabledata.="<tr>
								<td align='center'>{$count}</td>
						      	<td >{$receiptdetails['BatchId']}</td>	    		
								<td >".$receiptdetails['InvoiceNum']."</td>	    		       
							  	<td align='right'>".number_format($receiptdetails['batchamount'],2)."</td>
							  	<td align='right'>".number_format($outstandingamount,2)."</td>	    		
								<td align='right'>".number_format($receiptdetails['Amount'],2)."</td>	    		       
							 	<td align='right'>".number_format($balance,2)."</td>
							</tr>";
							 $count++;
						}
						
						
			$tabledata.="</table><table border='0' width='100%' align='center'>
							<tr >             						   
								<td align='center'><b>Total Amount Received : ".number_format($totalamt,2)."(Including 6% Service Tax)<b></td>
							</tr>
							<tr></tr>
						</table>
						<table border='0' width='100%' align='center'>
							<tr><td></td></tr>
							<tr >             						   
								<td  style='padding-left:85%' ><b>Signature<b></td>
								
							</tr>
							<tr>
							<td>$condition</td>
							</tr>
						</table>
				</div>";
			
 	
     		if($lstrreportytpe=='Pdf')
				{
					include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
					$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
					$mpdf->SetDirectionality ( $this->gstrHTMLDir );
					$mpdf->text_input_as_HTML = true;
					$mpdf->useLang = true;
					$mpdf->SetAutoFont();
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 1; // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumSuffix = ' / ';
                    $mpdf->setFooter ('Copyright &copy; 2013, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
					//$mpdf->setFooter ('' );
					$mpdf->allow_charset_conversion = true; // Set by default to TRUE
					$mpdf->charset_in = 'utf-8';
					ini_set('max_execution_time',3600);
					$mpdf->WriteHTML($tabledata);
					$mpdf->Output($filename.pdf,'D');
					//header("refresh: 2; url='".$this->view->baseUrl()."/registrations/paymentdistribution/index'");
				}
		}
		
	  if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) 
		{
            $this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$this->lobjdistributionModel = new Registrations_Model_DbTable_Paymentdistribution();//distribution model
			$lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //amount in words				
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";				
			$time = date('h:i:s',time());
			$filename = 'Receipt_Report_'.$frmdate;
			$ReportName = $this->view->translate( "RECEIPT" );				
			$lstrreportytpe="Pdf";			
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
			$larrformData = $this->_request->getPost ();	
			//print_r($larrformData);die();			 
			$date =date('d-m-Y');				
			$rcn = $larrformData['rcn'];
			$flag = $larrformData['flag'];
			//print_r($Configdetails);die();
			$receiptdetails = $this->lobjdistributionModel->fngetreceiptdetails($rcn,$flag); //get receipt details
			$receivermail = $receiptdetails['0']['Email'];
			$totalamt=$receiptdetails['0']['totalamount'];
			$Amountinwords = $lobjcompanystudentdetails->fnconvertnumbertowords($totalamt);
			if($receiptdetails['0']['PaymentMode']==1){
				$mode="Cheque";
				$condition="*This Receipt is valid subject to realisation of Cheque";
			}else if($receiptdetails['0']['PaymentMode']==2){
				$mode="Cash";
				$condition="";
			}
			if(isset($receiptdetails['0']['Fax'])){
				$fax=$receiptdetails['0']['Fax'];
			}else {
				$fax="-";
			}
			$tabledata.= "<br/>";
			
			$tabledata.="<div style='border:0.5mm solid;font-family:arial;'>";
			$tabledata.="<table border='0' width='100%' align='center' >
								
							 	<tr>
								 	<td align='left' width='40%'><b>No. {$rcn}<br/></b></td>
								  	<td align='left' style='font-size:130%;'><b><u><i>RECEIPT</i></u></b><br/></td>
								 	<td align='left'><b>Date: {$date}<br/></b></td>
								</tr>
								<tr >
								  	<td align='left' colspan='3'><i>Received With Thanks from:<b>{$larrformData['CompanyName']}</b></i><br/></td>
								</tr>
								<tr>
									<td align='left' colspan='3'><i>Address : {$larrformData['Address']}</i></td>
								</tr>
								<tr>
									<td align='left' colspan='3' style='padding-left:7%'>
										<i>
								    		{$larrformData['StateName']}<br/>
				 							{$larrformData['CountryName']}<br/>
				 							PostCode:{$larrformData['Postcode']}<br/>
				 							Phone:{$larrformData['Phone']}<br/>
				 							Email:{$larrformData['Email']}<br/>
				 							Fax:{$fax}<br/>
				 							Attn:{$larrformData['Contact']}
								         </i>
									</td>
									
								</tr>
								<tr>
									<td align='left' colspan='3'><i>a Sum of Ringgit Malaysia: {$Amountinwords} Only.</i><br/></td>
								</tr>
								<tr >
									<td align='left' colspan='3'><i>By: {$mode}.</i><br/></td>
								</tr>
								<tr>
									<td align='left' colspan='3'><i>towards Examination Fees as detail bellow.</i><br/></td>
								</tr>
						</table>";
			
			$tabledata.="<br/><table border='1' width='100%' align='center' style='margin:1em 1cm;'>
							<tr>
								<th ><b>SL NO</b></th>
								<th ><b>BatchId</b></th>							
								<th ><b>Invoice Number</b></th>
								<th ><b>Batch Amount</b></th>
								<th ><b>Outstanding Amount</b></th>							
								<th ><b>Received Amount</b></th>
								<th ><b>Balance</b></th>
							</tr>";
						$count=1;
						foreach($receiptdetails as $receiptdetails){
							$sum=0;$outstandingamountafterinsert=0;$outstandingamount=0;$balance=0;
							$sum=$this->lobjdistributionModel->fngetamount($receiptdetails['BatchId']);
							$outstandingamountafterinsert=($receiptdetails['batchamount']-$sum['0']['sum']);
							$outstandingamount=$outstandingamountafterinsert+$receiptdetails['Amount'];
							$balance=$outstandingamount-$receiptdetails['Amount'];
							 
                              
						
				$tabledata.="<tr>
								<td align='center'>{$count}</td>
						      	<td >{$receiptdetails['BatchId']}</td>	    		
								<td >".$receiptdetails['InvoiceNum']."</td>	    		       
							  	<td align='right'>".number_format($receiptdetails['batchamount'],2)."</td>
							  	<td align='right'>".number_format($outstandingamount,2)."</td>	    		
								<td align='right'>".number_format($receiptdetails['Amount'],2)."</td>	    		       
							 	<td align='right'>".number_format($balance,2)."</td>
							</tr>";
							 $count++;
						}
						
						
			$tabledata.="</table><table border='0' width='100%' align='center'>
							<tr >             						   
								<td align='center' border='1'><b>Total Amount Received : ".number_format($totalamt,2)."(Including 6% Service Tax)<b></td>
							</tr>
							<tr></tr>
						</table>
						<table border='0' width='100%' align='center'>
							<tr><td></td></tr>
							<tr >             						   
								<td  style='padding-left:85%' ><b>Signature<b></td>
								
							</tr>
							<tr>
							<td>$condition</td>
							</tr>
						</table>
				</div>";
		 
				if($larrformData['mailsend'])
				{         
				        $frmdate =date('d-m-Y');
				        $filename = 'Receipt_Report_'.$frmdate;                   
						include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
						require_once('Zend/Mail.php');
						require_once('Zend/Mail/Transport/Smtp.php');
						$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');	
									
				    	$mpdf->SetDirectionality ( $this->gstrHTMLDir );
				    	$mpdf->text_input_as_HTML = true;
				    	$mpdf->useLang = true;
				    	$mpdf->SetAutoFont();    	
				    	$mpdf->SetDisplayMode('fullpage');
				    	$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
				    	$mpdf->pagenumSuffix = ' / ';
				    	$mpdf->setFooter ('Copyright &copy; 2013, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
				    	$mpdf->allow_charset_conversion = true; // Set by default to TRUE
				    	$mpdf->charset_in = 'utf-8';       
						$mpdf->WriteHTML($tabledata);
						$mpdf->Output('pdf/Receipt_Report'.'_'.$frmdate.'.pdf','F');	
		
		                                $auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' =>'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com',$config);
										Zend_Mail::setDefaultTransport($transport);										
										$mail = new Zend_Mail();
                                        $filename ='pdf/Receipt_Report'.'_'.$frmdate.'.pdf';		
									    $mail->setBodyHtml("Receipt Report");
										$at = new Zend_Mime_Part(file_get_contents($filename));
										//$at = $mail->createAttachment($filename);
                                        $at->type= 'application/pdf';	
                                        //$at->disposition = Zend_Mime::DISPOSITION_INLINE;
										$at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                                        $at->encoding = Zend_Mime::ENCODING_BASE64;
                                        $at->filename = 'Receipt_Report'.'_'.$frmdate.'.pdf';   
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'iTWINE';
										$receiver_email = $receivermail;
										$receiver = 'Admin';
										$mail->setFrom('itwinesgm@gmail.com','itwine')
											 ->addTo($receiver_email, Zuarudin)
									         ->setSubject("Receipt_Report"."_".$frmdate)
                                             ->addAttachment($at);     
	                                        try
										    {
							                          $mail->send($lobjTransport);
                                                     // unlink('pdf/Bulkinvoice_Report_'.$frmdate.'.pdf');
							                          echo '<script language="javascript">alert("Receipt successfully  sent to admin")</script>';	
							                          echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/paymentdistribution/index';</script>";
				                	                  die();
				                	
						                    } 
											catch (Exception $e)
											{
							                    echo '<script language="javascript">alert("Unable to send mail \n check Internet Connection ")</script>';	
							                    echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/paymentdistribution/index';</script>";
				                	            die();
						                    }
			}								
    }
}
	
	public function getoperatornamesAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$newresult="";
		$opType = $this->_getParam('opType');	
		$namestring = $this->_getParam('namestring');	
		$larrresultopnames = $this->lobjdistributionModel->fnGetOperatorNames($opType,$namestring); 
		foreach ($larrresultopnames as $larrresnewarray){
			$opname=$larrresnewarray['name'];
			$newresult=$newresult."<tr><td><span id='idspan'   onclick='fnsetvalue(\"".$opname."\");'>".$larrresnewarray['name']."</span></td></tr></BR>";			
		}
		echo $newresult;
		exit;
	}
	
	public function fnsearchpaymentdistributionAction(){
		$this->_helper->layout->disableLayout();
		$this->view->lobjform = $this->lobjdistributionForm;
		//$this->_helper->viewRenderer->setNoRender();
		$opType = $this->_getParam('oprtype');	
		$opname = $this->_getParam('opname');
		$larrresult = $this->lobjdistributionModel->fnSearchpaymentdistribution($opType,$opname);
		//echo "<pre/>";print_r($larrresult);
		$this->view->distributionres=$larrresult;
		
	}
	
 				
}
