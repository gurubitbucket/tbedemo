<?php
class Registrations_CompanystudentdetailsController extends Base_Base { //Controller for the User Module

	public function init() 
	{   
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	   // $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    
		$this->lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //Company student details model object
		$this->lobjcompanymaster = new App_Model_Companymaster(); //Company student details model object
		$this->lobjloadfilesForm = new Examination_Form_Uploadfiles ();
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //TAkafulmodels model object	
		$this->lobjTakafulcandidatesForm = new App_Form_Takafulcandidates (); //intialize user lobjuserForm
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates (); //Batch candidates model object	
		$this->lobjBatchcandidatesForm = new App_Form_Batchcandidates (); //intialize user lobjuserForm
		$this->lobjstudentForm = new App_Form_Studentapplication (); // form of student application
		$this->lobjsearchForm = new App_Form_Search (); //intialize Search lobjsearchForm
		$this->lobjstudentmodel = new App_Model_Studentapplication (); // Model of student application
		$this->lobjCommon = new App_Model_Common ();
		$this->lobjTakafulapplicationmodel = new App_Model_Takafulapplication (); //user model object
		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$auth = Zend_Auth::getInstance();
		if($auth->getIdentity()->iduser == 17)
		{
		   $this->_helper->layout()->setLayout('/web/usty');
		}
	}
	

	public function indexAction() 
	{   
		// action for search and view
		$lobjform=$this->view->lobjform = $this->lobjsearchForm; //send the lobjuserForm object to the view
		$larrrbusinesstypelist = $this->lobjcompanymaster->fnGetBusinesstypeList (); //get Business type list details
		$lobjform->field5->addMultioptions($larrrbusinesstypelist);
		$lobjform->field1->setAttrib('OnChange', 'fnGetCityList');
		$lobjform->field3->setAttrib('onkeyup', 'fnGetOperatorNames');
		$lobjform->field8 ->setRegisterInArrayValidator(false);
		
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Company";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Takaful";
		$this->view->lobjform->field19->addMultiOptions($larrtypeofcompany);
		
		$larrresult = $this->lobjcompanystudentdetails->fnGenerateQueries(1,1); //get Company details
		$larrresult=0;
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->companystudentdetailspaginatorresult);
						
		$lintpagecount = $this->gintPageCount;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
        		
		if(isset($this->gobjsessionsis->companystudentdetailspaginatorresult)) {
			$lobjform->field19->setValue($this->gobjsessionsis->operatortype);	
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->companystudentdetailspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {				
				if(!$larrformData['field19']){
					echo '<script language="javascript">alert("Please Select Type Of Company")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/index/';</script>";					
					exit;
				}
				$this->gobjsessionsis->operatortype = $this->view->operatortype = $larrformData['field19'];				
				$lobjform->field19->setValue($this->view->operatortype);
				$larrresult = $this->lobjcompanystudentdetails->fnSearchCompanies($lobjform->getValues ()); //searching the values for the Companies
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->companystudentdetailspaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			unset($this->gobjsessionsis->operatortype);
		 	$this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/index');
		}
	}
	
	
	
	public function studentsregisteredAction() 
	{   
		$lintidcompany =$this->view->idcompany = $this->_getParam('idcompany');
		$lintidregistrationpin = $this->_getParam('idRegPin');
		$companyflag = $this->_getParam('companyflag');
		if($companyflag ==1)
		{
		   $this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
				}
		else
		{
		   $this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
		}
		$this->view->studentdetails = $this->lobjcompanystudentdetails->fnGenerateQueries(4,1,$lintidregistrationpin); //Get company details by ID	
			
	}
	
	public function candidateregistrationAction() 
	{   
		$this->_helper->layout->disableLayout();
	 	$lintidcompany =$this->view->idcompany = $this->_getParam('idcompany');
		$lintidregistrationpin = $this->view->regpin = $this->_getParam('idRegPin');			
		$larrsultfailedStud=$this->lobjTakafulapplicationmodel->fnGetfailedregistrered ($lintidregistrationpin);	
		$this->view->failedstudentlist=$larrsultfailedStud;		
		$larrresultStudent = $this->lobjTakafulapplicationmodel->fnGetStudregistrered ($lintidregistrationpin);			
		if (!$larrresultStudent) {
			$this->view->showshceduler = 0;
		} else {
			$this->view->showshceduler = 1;
		}
	}
	
	public function selectvenuecompanyAction(){
		
		$this->_helper->layout->disableLayout();
		$this->view->lobjstudentForm = $this->lobjstudentForm; 
		$day = $this->_getParam('day');
		$month = $this->_getParam('month');
		$year = $this->_getParam('year');
		$venue = $this->_getParam('city');
		if($month <10){
			$month = '0'.$month;
		}
		if($day <10){
			$day = '0'.$day;
		}
		 $selecteddate = $year.'-'.$month.'-'.$day;

		$studentapp = new App_Model_Studentapplication();
		$sessresult = $studentapp->fnGetvenuedatescheduleDetails($selecteddate,$venue);
		$this->view->sessresult = $sessresult;
		$this->view->regdate=$selecteddate;
		
		$result5 = $this->lobjstudentmodel->fngetdayStudent($selecteddate); 
		$this->view->daystu= $result5[0]['days'];
	
	}
	
	public function manualstudentsAction() 
	{   
		
		$auth = Zend_Auth::getInstance();
		$userid =  $auth->getIdentity()->iduser;
		$lintidcompany =$this->view->idcompany = $this->_getParam('idcompany');
		$lintidregistrationpin = $this->view->regpin = $this->_getParam('idRegPin');
		
		$auth = Zend_Auth::getInstance();
		
		$month = date ( "m" ); // Month value
		$day = date ( "d" ); //today's date
		$year = date ( "Y" ); // Year value
		$minmumage = new App_Model_Studentapplication ();
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		$larr = $minmumage->fngetminimumage ();
		$age = $larr [0] ['MinAge'];
		$eligibility = ($year) - ($age);
		
		$year = $eligibility;
		$this->view->yearss = $year;
		$this->view->minages = $age;
		
		$yeste = date ( 'Y-m-d', mktime ( 0, 0, 0, $month, ($day - 1), $year ) );
		$this->view->yesdate = $yeste;
		

		$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
		$companyaddress = $this->view->companydetails['Address'];	
		
		$larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$lintidregistrationpin);
		echo "<pre>";

		print_r($larrstudentscountcount);

		$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($lintidregistrationpin); // Newly added as on 21-01-2013

                print_r($larrstudentcounttemp);

		if($larrstudentcounttemp ['totalcount']>$larrstudentscountcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrstudentscountcount ['totalregistered'];
		}
		//$availseat = $larrstudentscountcount ['totalNoofCandidates'] - $actualregistered;
		
		$availseat = $larrstudentscountcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrstudentscountcount ['totalregistered']);

		
		$this->view->alreadyapppliedexcel = $availseat;
		$ids =$larrstudentscountcount['idBatchRegistration'];
		
		$laresultscandidate = $this->lobjTakafulcandidatesmodel->fngetBatchRegistration ( $ids );

		$noofcandidates = Array ();
		$noofexams = count ( $laresultscandidate );
		for($i = 0; $i < $noofexams; $i ++) {
			$noofcandidates ['idprgm'] [] = $laresultscandidate [$i] ['idProgram'];
			$noofcandidates ['ProgramName'] [$laresultscandidate [$i] ['idProgram']] = $laresultscandidate [$i] ['ProgramName'];
			$noofcandidates [$laresultscandidate [$i] ['idProgram']] = $laresultscandidate [$i] ['noofCandidates'];
			$noofcandidatesssss [] = $laresultscandidate [$i] ['noofCandidates'];
		}
		//echo "<pre/>";
		$larrbatchprog = $this->lobjTakafulcandidatesmodel->fnBatchProg ();
		for($g = 0; $g < count ( $noofcandidates ['idprgm'] ); $g ++) {
			$larrbatchprog123 [$noofcandidates ['idprgm'] [$g]] = $this->lobjTakafulcandidatesmodel->fnBatchProgram ( $noofcandidates ['idprgm'] [$g] );
		}
		$this->view->batchresults = $larrbatchprog123;
		$total = 0;
		for($m = 0; $m < count ( $noofcandidatesssss ); $m ++) {
			$total = $total + $noofcandidatesssss [$m];
		}
		//print_r($total);
		$this->view->total = $total;
		$this->view->noofprog = $noofcandidates ['idprgm'];
		$this->view->progname = $noofcandidates ['ProgramName'];
		$this->view->noofcandidates = $noofcandidates;
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
		$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		$larrresultprogram = $this->lobjTakafulcandidatesmodel->fnGetProgramName ();
		$this->view->programresult = $larrresultprogram;
		$larresultbatch = $this->lobjTakafulcandidatesmodel->fnGetBatchName ();
		$this->view->batchresult = $larresultbatch;
		$larrresultrace = $this->lobjTakafulcandidatesmodel->fnGetRace ();
		$this->view->raceresult = $larrresultrace;
		$larrresuleducation = $this->lobjTakafulcandidatesmodel->fnGetEducation ();
		$this->view->educationresult = $larrresuleducation;
		$larresultreligionoperator = $this->lobjTakafulcandidatesmodel->fnGetAllActiveReligionNameList ();
		$this->view->Religion = $larresultreligionoperator;
		
		$larreducationresult = $this->lobjTakafulcandidatesmodel->fnGetCountryList ();
		$this->view->countryresult = $larreducationresult;
		
		// Function to check the mode of pay and blockin if program already selected
		$larrresultBatch = $this->lobjTakafulapplicationmodel->fngetBatchDetails ( $lintidregistrationpin );		
		$larrgetpaydetails=$this->lobjcompanystudentdetails->fnGetModeofpay($larrresultBatch['idBatchRegistration']);		
		$this->view->mode0fpay=$larrgetpaydetails['ModeofPayment'];		
			
		$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($larrresultBatch['idBatchRegistration']);	
		$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
		$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
		$this->view->progid=$larrgetprogramapplied['idProgram'];
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'save' )) {
			
			$larrformData = $this->_request->getPost ();
			for($i=0;$i<count($larrformData['candidatename']);$i++){
					
					$larrdatainsert['StudentName']=$larrformData['candidatename'][$i];					
					$larrdatainsert['ICNO']=$larrformData['candidateicno'][$i];
					$larrdatainsert['Race']=$larrformData['candidaterace'][$i];
					$larrdatainsert['email']=$larrformData['candidateemail'][$i];
					$larrdatainsert['education']=$larrformData['candidateeducation'][$i];
					$larrdatainsert['Gender']=$larrformData['candidategender'][$i];
					$larrdatainsert['DOB']=$larrformData['candidatedateofbirth'][$i];
					$larrdatainsert['Address']=$larrformData['candidateaddress'][$i];
					$larrdatainsert['CorrespAddress']=$larrformData['correspondanceaddress'][$i];
					$larrdatainsert['PostalCode']=$larrformData['postalcode'][$i];
					$larrdatainsert['IdCountry']=$larrformData['candidatecountry'][$i];
					$larrdatainsert['IdState']=$larrformData['candidatestate'][$i];
					$larrdatainsert['ContactNo']=$larrformData['candidatenum'][$i];
					$larrdatainsert['MobileNo']=$larrformData['candidatemobnum'][$i];
						
					$this->lobjcompanystudentdetails->fninsertintotemp ( $larrdatainsert, $lintidcompany , $lintidregistrationpin );
				}
				
				$larrcandidateappliedcount=$this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$lintidregistrationpin);
				$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($lintidregistrationpin); // Newly added as on 21-01-2013
		if($larrstudentcounttemp ['totalcount']>$larrcandidateappliedcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrcandidateappliedcount ['totalregistered'];
		}
			$availremainseat=$larrcandidateappliedcount['totalNoofCandidates'] - ($larrcandidateappliedcount['totalregistered']+$larrstudentcounttemp['totalcount']);	
				//$availremainseat=$larrcandidateappliedcount['totalNoofCandidates'] - ($larrcandidateappliedcount['totalregistered']+$larrcandidateappliedcount['totalcount']);
		//$availremainseat=$larrcandidateappliedcount['totalNoofCandidates'] - $actualregistered;
				$mainpath=$this->view->baseUrl();
				if($availremainseat!=0){
					echo '<script language="javascript">var theAnswer = confirm("Do You Wish To Insert More Candidates ?");					
					if(theAnswer){  	
						alert("Your Data is Saved.....Please Enter the Next Candidate");
						window.location.href = "'.$mainpath.'/registrations/companystudentdetails/manualstudents/idcompany/'.$lintidcompany.'/idRegPin/'.$lintidregistrationpin.'";
					}else{
						alert("Thank You for your Patience....Your Data Has been Saved. Please Schedule the Candidates Entered");						
						window.location.href = "'.$mainpath.'/registrations/companystudentdetails/coursevenue/idcompany/'.$lintidcompany.'/registrationpin/'.$lintidregistrationpin.'";
					}
					</script>';
					exit;
				}else{
					
$this->_redirect ( $this->baseUrl . "/registrations/companystudentdetails/companystudentlist/idcompany/$lintidcompany" );
					exit;
				}
	
		}
		
	}
	
	
	public function excelstudentsAction() 
	{   
		$lintidcompany =$this->view->idcompany = $this->_getParam('idcompany');
		$ids = $lintidregistrationpin = $this->view->regpin = $this->_getParam('idRegPin');
		$auth = Zend_Auth::getInstance();
		$lobjUploadfilesForm = $this->lobjloadfilesForm; //intialize bank form
		$this->view->lobjUploadfilesForm = $lobjUploadfilesForm;
		
		$larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$lintidregistrationpin);
		$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($lintidregistrationpin); // Newly added as on 21-01-2013
		if($larrstudentcounttemp ['totalcount']>$larrstudentscountcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrstudentscountcount ['totalregistered'];
		}
	
	   $availseat = $larrstudentscountcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrstudentscountcount ['totalregistered']);
        //$availseat = $larrstudentscountcount ['totalNoofCandidates'] - $actualregistered;
		$this->view->remspplication = $availseat;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		{
			$larrformData = $this->_request->getPost (); //getting the values of bank from post 
			require_once 'Excel/excel_reader2.php';
			$lintfilecount ['Count'] = 0;
			$lstruploaddir = "/uploads/questions/";
			$larrformData ['FileLocation'] = $lstruploaddir;
			$larrformData ['UploadDate'] = date ( 'Y-m-d:H:i:s' );
			
			if ($_FILES ['FileName'] ['error'] != UPLOAD_ERR_NO_FILE) {
				$lintfilecount ['Count'] ++;
				$lstrfilename = pathinfo ( basename ( $_FILES ['FileName'] ['name'] ), PATHINFO_FILENAME );
				$lstrext = pathinfo ( basename ( $_FILES ['FileName'] ['name'] ), PATHINFO_EXTENSION );
				
				$filename = $lintfilecount ['Count'] . "." . date ( 'YmdHis' ) . "." . $lstrext;
				$filename = str_replace ( ' ', '_', $lstrfilename ) . "_" . $filename;
				$file = realpath ( '.' ) . $lstruploaddir . $filename;
				if (move_uploaded_file ( $_FILES ['FileName'] ['tmp_name'], $file )) {
					//echo "success";
					$larrformData ['FilePath'] = $filename;
					$larrData ['FileName'] = $lstrfilename;
					$larrData ['FilePath'] = $filename;
				} else {
					//echo "error";
				}
			}

			$userDoc = realpath ( APPLICATION_PATH . '/../public/uploads/questions/' . $filename );
			$data = new Spreadsheet_Excel_Reader( $userDoc );
			
			$arr = $data->sheets;
			for($i = 2; $i < 100; $i ++) 
			{
				if ($arr [0] ['cells'] [$i] [1] == '') 
				{
					break;
				} 
				else
				{ 
					$totalarray [$i] = $arr [0] ['cells'] [$i];
				}
			}
			
			$larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$lintidregistrationpin);
			$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($lintidregistrationpin); // Newly added as on 21-01-2013
		if($larrstudentcounttemp ['totalcount']>$larrstudentscountcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrstudentscountcount ['totalregistered'];
		}
			
			//$availseat = $larrstudentscountcount ['totalNoofCandidates'] - $actualregistered;

			$availseat = $larrstudentscountcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrstudentscountcount ['totalregistered']);
			
			if ($availseat < count ( $totalarray )) 
			{
				$counts = $availseat + 2;
			} 
			else 
			{
				$count = count ( $totalarray );
				$counts = $count + 2;
			}
			
			if ($arr [0] ['cells'] [1] [1] != "Student Name" || $arr [0] ['cells'] [1] [2] != "ICNO" || $arr [0] ['cells'] [1] [3] != "E-Mail" || $arr [0] ['cells'] [1] [4] != "Race" || $arr [0] ['cells'] [1] [5] != "Education" || $arr [0] ['cells'] [1] [6] != "DateofBirth" || $arr [0] ['cells'] [1] [7] != "Gender" || $arr [0] ['cells'] [1] [8] != "Mailing Address" || $arr [0] ['cells'] [1] [9] != "Correspondance Address" || $arr [0] ['cells'] [1] [10] != "Postal Code" || $arr [0] ['cells'] [1] [11] != "Country" || $arr [0] ['cells'] [1] [12] != "State" || $arr [0] ['cells'] [1] [13] != "Contact No" || $arr [0] ['cells'] [1] [14] != "Mobile No") 
			{
				echo '<script language="javascript">alert("Excel Sheet Not in correct Format")</script>';
				echo "<script>parent.location = '" . $this->view->baseUrl () . "registrations/companystudentdetails/excelstudents/idcompany/".$lintidcompany."/idRegPin/".$lintidregistrationpin."';</script>";
			}
			
			
				$idprogarray=$this->lobjTakafulcandidatesmodel->fnGetprogramappliedExcel ( $ids ); 
				$idprogramapplied=$idprogarray['idProgram'];
			
			
			
			for($iterexcelread = 2; $iterexcelread < $counts; $iterexcelread++) {
				
				$flag=0;
				///////////////////////NAme/////////////////////
				$larrdatainsert ['StudentName'] = $arr [0] ['cells'] [$iterexcelread] [1];				
				
				////////////////////ICNO///////////////////////////////
				$icno = $arr [0] ['cells'] [$iterexcelread] [2];
			if(is_numeric($icno)){
									$larrdatainsert ['ICNO'] = $arr [0] ['cells'] [$iterexcelread] [2];
									$icnos = "$icno";
									$larricno = $this->lobjBatchcandidatesmodel->fnGetIcno ( $icno , $idprogramapplied );  // function to validate ICNO
									$larricnoexcel = $this->lobjTakafulcandidatesmodel->fnGetIcno ( $icno,$idprogramapplied );  // function to validate ICNO	

									$dobicnum= "19".$icno[0].$icno[1]."-".$icno[2].$icno[3]."-".$icno[4].$icno[5];	
									$dobexcel = $arr [0] ['cells'] [$iterexcelread] [6];	
									$dobexcel=date('Y-m-d',strtotime($dobexcel));	

			                        $month=$icno[2].$icno[3];
									$day = $icno[4].$icno[5];
									
								   if($month>12){
								   		echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 1 );
										continue;
								   }
								   
								   if($day>31){
								   		echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 1 );
										continue;
								   }
			
									if($icno[11]%2==0){																				
										if($arr[0]['cells'][$iterexcelread][7] == 'Female' || $arr [0]['cells'][$iterexcelread][7] == 'FEMALE'){
											
										}else{
											$flag=6;											
										    echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										    $this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, $flag );
										    continue;	
										}
									} else{										
									if($arr[0]['cells'][$iterexcelread][7] == 'Male' || $arr [0]['cells'][$iterexcelread][7] == 'MALE'){
											
										}else{
											$flag=6;
											echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										    $this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, $flag );
										    continue;
										}
									}
									
									$toddate = date ( 'Y-m-d' );
									$diff = abs ( strtotime ( $toddate ) - strtotime ( $dobicnum ) );
									$years = floor ( $diff / (365 * 60 * 60 * 24) );
									if ($years < 18) {
										//Function to log errors
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 3 );
										continue;
									}		
									if($dobicnum!=$dobexcel){
										$flag=11;
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, $flag );
										continue;
										
									}			
									if(count($larricnoexcel) > 0){
										$flag=10;
									}else if (count($larricno) > 0){
										$flag=10;
									}else{
										$flag=1;
									}								
									$icnolen = strlen ( $icno );
									if ($icnolen != 12 || count($larricno)>0 || count($larricnoexcel)>0) {
										echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, $flag );
										continue;				
									}
				}else{
					
							echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
							$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 1 );
							//echo "<script>parent.location = '".$this->view->baseUrl()."/takafulcandidates/import/batchId/".$ids."';</script>";
							continue;	
				}
				
			
				$year = '19' . $icnos [0] . $icnos [1];
				$month = $icnos [2] . $icnos [3];
				$day = $icnos [4] . $icnos [5];
				$dob = $year . '-' . $month . '-' . $day;
				/////////////////ICNO ENDS////////////////////////////////
				/////////////////Email Starts////////////////////////////////
				$larrdatainsert ['email'] = $arr [0] ['cells'] [$iterexcelread] [3];
				$email = $arr [0] ['cells'] [$iterexcelread] [3];
				$larrmailexcel = $this->lobjTakafulcandidatesmodel->fnGetmailId ( $email );  // function to validate E-MAIL
				$larrmail = $this->lobjBatchcandidatesmodel->fnGetmailId ( $email );   // function to validate EMAIL
				$race = $larrmail ['EmailAddress'];
				$mailcount = strlen ( $larrmail ['EmailAddress'] );

				/*if (! $larrmail && ! $larrmailexcel) {
				
				} else {
					echo '<script language="javascript">alert("Email Already Taken")</script>';
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 2 );
					continue;								
				}*/
				
				/////////////////Email ENDS////////////////////////////////
				///////////////////////RACE/////////////////////////////////
				
				$chienesearray = $arr [0] ['cells'] [$iterexcelread] [4];
				$larrrace = $this->lobjBatchcandidatesmodel->fnGetRaceId ( $chienesearray );// function to check race
				
				if(!$larrrace){
					$others="Others";
					$larrraceothers = $this->lobjBatchcandidatesmodel->fnGetRaceId ( $others );					
					$larrdatainsert ['Race'] = $larrraceothers ['idDefinition'];
				}else{
					$larrdatainsert ['Race'] = $larrrace ['idDefinition'];
				}
								
				$racecount = strlen ( $larrdatainsert ['Race'] );			
				if (!$larrdatainsert ['Race']) {
					echo '<script language="javascript">alert("Please check the race and upload the file")</script>';
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 4 );
					continue;	
				}				
				////////////////////////RACE ENDS////////////////////////////////
				///////////////////////Education////////////////////////////////
				$educationarray = $arr [0] ['cells'] [$iterexcelread] [5];
				$larreducation = $this->lobjBatchcandidatesmodel->fnGetEducatinexcel ( $educationarray ); // function to check education
				
				if(!$larreducation){
					$others="Others";
					$larreducationothers = $this->lobjBatchcandidatesmodel->fnGetEducatinexcel ( $others );
					$larrdatainsert ['education'] = $larreducationothers ['idDefinition'];
				}else{
					$larrdatainsert ['education'] = $larreducation ['idDefinition'];				
				}
				
				if ($larrdatainsert ['education']) {
				
				} else {
					echo '<script language="javascript">alert("Please check the Education and upload the file")</script>';
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 5 );
					continue;					
				}
				
				/////////////////////////////////////////////////////////////////////
				$dob = $larrdatainsert ['DOB'] = $arr [0] ['cells'] [$iterexcelread] [6];
				$toddate = date ( 'Y-m-d' );
				$diff = abs ( strtotime ( $toddate ) - strtotime ( $dob ) );
				$years = floor ( $diff / (365 * 60 * 60 * 24) );
				if ($years < 18) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 3 );
					continue;
				}
				
				//////////////////////////////////GENDER////////////////////////

				if ($arr [0] ['cells'] [$iterexcelread] [7] == 'Male' ||$arr [0] ['cells'] [$iterexcelread] [7] == 'MALE') {
					$larrdatainsert ['Gender'] = 1;
				} else if ($arr [0] ['cells'] [$iterexcelread] [7] == 'Female' || $arr [0] ['cells'] [$iterexcelread] [7] == 'FEMALE') {
					$larrdatainsert ['Gender'] = 0;
				} else {
					echo '<script language="javascript">alert("Please check the Gender and upload the file")</script>';
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 6 );
					continue;
				}
				
				///////////////////////////////////GENDER ENDS////////////

				///////////////////ADDRESS/////////////////////
				$larrdatainsert ['Address'] = $arr [0] ['cells'] [$iterexcelread] [8];
				$larrdatainsert ['CorrespAddress'] = $arr [0] ['cells'] [$iterexcelread] [9];
				$larrdatainsert ['PostalCode'] = $arr [0] ['cells'] [$iterexcelread] [10];
				if (! $larrdatainsert ['PostalCode']) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 7 );
					continue;
				}
					
				///////////////////////Country//////////////////////////////////
				$countryarray = $arr [0] ['cells'] [$iterexcelread] [11];
				if (!$countryarray) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 8 );
					continue;
				}
				$larrcountry = $this->lobjBatchcandidatesmodel->fnGetCountryexcel ( $countryarray );	
				
				if(!$larrcountry){
					$countryarraydefault="MALAYSIA";
					$larrcountrydefault = $this->lobjBatchcandidatesmodel->fnGetCountryexcel ( $countryarraydefault);
					$larrdatainsert ['IdCountry']=$larrcountrydefault['idCountry'];
				}else{
					$larrdatainsert ['IdCountry'] = $larrcountry ['idCountry'];		
				}
											     		   
				if ($larrdatainsert ['IdCountry'] != "") {
				
				} else {
					echo '<script language="javascript">alert("Please check the Country List and upload the file")</script>';	
					//Function to log errors				
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 8 );
					continue;					
				}
				
			
				//////////////////////Country//////////////////////////////////
				/////////////////////State////////////////////////////////////
				$statearray = $arr [0] ['cells'] [$iterexcelread] [12];
				if (!$statearray) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 9 );
					continue;
				}
				
				$larrstate = $this->lobjBatchcandidatesmodel->fnGetStateexcel ( $statearray );// Function to validate and check states	
				if(!$larrstate){
					$others="Others";
					$larrstateothers = $this->lobjBatchcandidatesmodel->fnGetStateexcelothers ($larrdatainsert['IdCountry'],$others );
					$larrdatainsert ['IdState'] = $larrstateothers ['idState'];	
				}else{
					$larrdatainsert ['IdState'] = $larrstate ['idState'];	
				}
							
				if ($larrdatainsert ['IdState'] != "") {
				} else {
					echo '<script language="javascript">alert("Please check the State List and upload the file")</script>';
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 9 );
					continue;					
				}

				//////////////////////////////////////////////////////////////
				$larrdatainsert ['ContactNo'] = $arr [0] ['cells'] [$iterexcelread] [13];
				$larrdatainsert ['MobileNo'] = $arr [0] ['cells'] [$iterexcelread] [14];		
				$larrdatainsert ['idprogram']=$idprogramapplied;
				$insertarray = $this->lobjTakafulcandidatesmodel->fninsertintotemp ( $larrdatainsert, $lintidcompany , $ids );				
				$larrdataimport ['UpdUser'] = $lintidcompany;
				$larrdataimport ['IdregistrationPin'] = $ids;
				$larrdataimport ['Typeofimport'] = 1;	

				$insertarray = $this->lobjTakafulcandidatesmodel->fninserttoimported ( $larrdataimport );
			
			}
			//echo $iterexcelread;
			$this->_redirect( $this->baseUrl . 'registrations/companystudentdetails/coursevenue/registrationpin/'.$lintidregistrationpin.'/idcompany/'.$lintidcompany);
		}

	}
	
	public function coursevenueAction() {  // Action for Scheduling students  added from excel
		
		$auth = Zend_Auth::getInstance();
		$userid =  $auth->getIdentity()->iduser;
		$ids = $this->_getParam ( 'registrationpin' );
		$idCompany = $this->_getParam ( 'idcompany' );
		$this->view->idcomps = $idCompany;
		$this->view->lobjTakafulcandidatesForm = $this->lobjTakafulcandidatesForm;
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		$this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;

		$this->view->idbatchss = $ids;
		$laresultscandidate = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationimport ( $ids );
	
		$noofcandidates = Array ();
		$noofexams = count ( $laresultscandidate );
		
		for($iteresultscandidate = 0; $iteresultscandidate < $noofexams; $iteresultscandidate ++) {
			$noofcandidates ['idprgm'] [] = $laresultscandidate [$iteresultscandidate] ['idProgram'];
			$noofcandidates ['ProgramName'] [$laresultscandidate [$iteresultscandidate] ['idProgram']] = $laresultscandidate [$iteresultscandidate] ['ProgramName'];
			$noofcandidates [$laresultscandidate [$iteresultscandidate] ['idProgram']] = $laresultscandidate [$iteresultscandidate] ['noofCandidates'];
			$noofcandidatesssss [] = $laresultscandidate [$iteresultscandidate] ['noofCandidates'];
			$noofcandidates ['programid'] = $laresultscandidate [$iteresultscandidate] ['idProgram'];
		}
		
		$total = 0;
		
		for($iternumcandidate = 0; $iternumcandidate < count ( $noofcandidatesssss ); $iternumcandidate ++) {
			$total = $total + $noofcandidatesssss [$iternumcandidate];
		}
		
		$this->view->total = $total;
		$this->view->noofprog = $noofcandidates ['idprgm'];
		$this->view->idprogram = $noofcandidates ['programid'];
		$this->view->progname = $noofcandidates ['ProgramName'];
		$this->view->noofcandidates = $noofcandidates;
		$this->view->operatortype=$this->gobjsessionsis->operatortype;
	
		$larrtempexcelcandidates = $this->lobjcompanystudentdetails->fngetnoofstudentsfromexcel ( $ids);
		
		$this->view->takcandiddetails = $larrtempexcelcandidates;
		$this->view->totalexcelstudents = count ( $larrtempexcelcandidates ); 
		$this->view->countparts = count ( $laresultscandidate );
		$this->view->programresult = $laresultscandidate;
		

		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
		$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		
	// Function to check the mode of pay and blockin if program already selected
		$larrresultBatch = $this->lobjTakafulapplicationmodel->fngetBatchDetails ( $ids );		
		$larrgetpaydetails=$this->lobjcompanystudentdetails->fnGetModeofpay($larrresultBatch['idBatchRegistration']);		
		$this->view->mode0fpay=$larrgetpaydetails['ModeofPayment'];		
			
		$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($larrresultBatch['idBatchRegistration']);	
		$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
		$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
		$this->view->progid=$larrgetprogramapplied['idProgram'];

		$larrexcelappliedcandidates = $this->lobjcompanystudentdetails->fngetexcelappliedcandidates ($ids);
		$this->view->larrappliedresult = $larrexcelappliedcandidates;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost ();
	
			$larrformData ['Examvenue'] = $larrformData ['NewVenue'];
			$resultstate = $this->lobjstudentmodel->fngetstatecity ( $larrformData ['NewVenue'] );
			$larrformData ['ExamState'] = $resultstate ['state'];
			$larrformData ['ExamCity'] = $resultstate ['city'];
			$larrformData ['NewState'] = $resultstate ['state'];
			$larrformData ['NewCity'] = $resultstate ['city'];
			$larrformData ['hiddenscheduler'] = 1;
			

			if(strlen($larrformData['setmonth']) <= 1) {
	         	$monthsss = '0'.$larrformData['setmonth'];
	        } else {
	         	$monthsss = $larrformData['setmonth'];
	        }
	            
			if(strlen($larrformData['setdate']) <= 1) {
	         	$dayssss = '0'.$larrformData['setdate'];
	        } else {
	         	$dayssss = $larrformData['setdate'];
	        }
			
			$availdate=$larrformData['Year']."-".$monthsss."-".$dayssss;		
			$larravailseat=$this->lobjTakafulcandidatesmodel->fngetavailseatvenue($larrformData['NewVenue'],$availdate,$larrformData['idsession']);
	$larrformData['scheduler']=$larravailseat['idnewscheduler'];

			if(count ( $larrformData ['studenttakful'] ) >= $larravailseat['availseat']){
				echo '<script language="javascript">alert("The Noof Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
				echo "<script>parent.location = '".$this->view->baseUrl()."/takafulcandidates/coursevenue/batchId/".$ids."';</script>";
				exit;
			}

			if (count ( $larrformData ['studenttakful'] ) > 0) {
				$larrbatchregID = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationPinforexcel ( $larrformData ['idbatch'] );
				$lintidbatch = $larrbatchregID ['idBatchRegistration'];
				$linttotnumofapplicant = $larrbatchregID ['totalNoofCandidates'];
				$larrinsertstudent = $this->lobjcompanystudentdetails->fnInsertintostudapplicationexcel ( $larrformData, $lintidbatch, $linttotnumofapplicant, $larrformData ['idbatch'],$idCompany ,$userid,$this->gobjsessionsis->operatortype);
			} else {
				?><script>alert("Check any Applicant")</script>";<?php
			}
			$this->_redirect( $this->baseUrl . 'registrations/companystudentdetails/coursevenue/registrationpin/'.$ids.'/idcompany/'.$idCompany);
					
		}
	
	}
	
	public function viewerrapplicationAction() {  //Action to view the error applications from excel upload
		$regpin = $this->_getParam ( 'regpin' ); 
		$idCompany = $this->_getParam ( 'idcompany' ); 
		$this->view->idcompany = $idCompany;
		$this->view->idbatchss = $regpin;
		$larrerrresult = $this->lobjTakafulcandidatesmodel->fngetErrstudentapllication ( $regpin );
		$this->view->larrappliederr = $larrerrresult;
	}
	
	public function deletefromtempAction(){ // ADDED on 28-2-2013
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$idcandidates = $this->_getParam ( 'idcandidates' );
		$result = $this->lobjTakafulcandidatesmodel->fndeletefromtemp ( $idcandidates );
		echo $result;
		exit;
		
	}
	
	
	
	public function fngetstateAction() {
		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$idcountry = $this->_getParam ( 'idcountry' );
		$larrstatelist = $this->lobjCommon->fnGetCountryStateList ( $idcountry );
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrstatelist );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetailss );
	}
	
	public function fngetstudentconfirmAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		
		//Get Country Id
		$Program = $this->_getParam ( 'Program' );
		$icno = $this->_getParam ( 'icno' );
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnstudentconfirm ( $Program, $icno );
		$pass = $larrvenuetimeresult ['pass'];
		if ($pass == 3) {
			echo '1' . '***' . 'You have already applied for the exam';
		} else if ($pass == 1) {
			echo '1' . '***' . 'You have already Passed the exam';
		} else {
			echo '0' . '***';
		}
	}
	
	public function fngetemaildetailsAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		
		//Get Country Id
		$Program = $this->_getParam ( 'Program' );
		$idyear = $this->_getParam ( 'Year' );
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fngetstudentsemail ( $Program, $idyear );
		if ($larrvenuetimeresult) {
			echo 'We observe that the email id provided already exists, please login to the portal if you have already registered. If you have not registered earlier, please provide with another email id';
		
		}
	}
	
	
	public function pdfexportAction()
	{
		
		//Exporting data to an excel sheet 
		$lstrreportytpe="";
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) {
				
			$larrformData = $this->_request->getPost ();				
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbe/images/reportheader.jpg";
				
		$time = date('h:i:s',time());
		$filename = 'Company_Registration_Report_'.$frmdate;
		$ReportName = $this->view->translate( "Company" ).' '.$this->view->translate( "Registration" ).' '.$this->view->translate( "Report" );
		$companyName="Name Of The Company :".$larrformData['CompanyName'];
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
			$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
		}
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 6><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 9><b> {$ReportName}</b></td>									
							</tr>
							<tr>								
								<td align=left colspan = 9><b> {$companyName}</b></td>
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b></b></th>
							<th><b>Student Name</b></th>
							<th><b>ICNO</b></th>							
							<th><b>Registration Id</b></th>
							<th><b>Program Applied</b></th>
							<th><b>Venue Name</b></th>
							<th><b>Exam Date</b></th>
							<th><b>Exam Time</b></th>
							<th><b>Exam Status</b></th>						
						</tr>';     
	 
		if (count($larrformData)){
			 $cnt = 0; 
		
			 
      	for($expdata=0;$expdata<count($larrformData['FName']);$expdata++){
      		  	
			$tabledata.= ' <tr>
				   		   <td><b>'; 
      		      $tabledata.= '</b></td>
					       <td>'.$larrformData['FName'][$expdata].'</td> 
						   <td>'.$larrformData['ICNO'][$expdata].'</td> 						   
						   <td>'.$larrformData['Regid'][$expdata].'</td> 
						   <td>'.$larrformData['ProgramName'][$expdata].'</td> 
						   <td>'.$larrformData['centername'][$expdata].'</td> 		  
						   <td>'.$larrformData['dateofexam'][$expdata].'</td> 
 						<td>'.$larrformData['examtime'][$expdata].'</td> 
						   <td>'.$larrformData['status'][$expdata].'</td> 		   
				        </tr> ';				   	
	    	$cnt++; 
      		  }
	     
		 }		
	
		 $tabledata.= '<tr>		      
		     	<td></td>
		     	<td></td>
		     	<td></td>
		     </tr>
		     </table>';	
		
		//echo $tabledata;exit;
		if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}
			
		}
		
	}
	
	public function pdfexportregAction()
	{
		//Exporting data to an excel sheet 
		$lstrreportytpe="";
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) {
				
			$larrformData = $this->_request->getPost ();	
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
			
		$time = date('h:i:s',time());
		$filename = 'Company_Registration_Report_'.$frmdate;
		$ReportName = $this->view->translate( "Company" ).' '.$this->view->translate( "Registration" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
			$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
		}
			$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b>Time</b></td>
								<td align = 'left' colspan= 6><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 9><b> {$ReportName}</b></td>	
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b></b></th>
							<th><b>Student Name</b></th>
							<th><b>ICNO</b></th>
							<th><b>E-Mail</b></th>
							<th><b>Registration Id</b></th>
							<th><b>Program Applied</b></th>
							<th><b>Venue Name</b></th>
							<th><b>Exam Date</b></th>
							<th><b>Exam Session</b></th>						
						</tr>';     
	 
		if (count($larrformData)){
			 $cnt = 0; 
		
			 
      	for($expdata=0;$expdata<count($larrformData['FName']);$expdata++){
      		  	
			$tabledata.= ' <tr>
				   		   <td><b>'; 
      		      $tabledata.= '</b></td>
					       <td>'.$larrformData['FName'][$expdata].'</td> 
						   <td>'.$larrformData['ICNO'][$expdata].'</td> 
						   <td>'.$larrformData['Email'][$expdata].'</td> 
						   <td>'.$larrformData['Regid'][$expdata].'</td> 
						   <td>'.$larrformData['ProgramName'][$expdata].'</td> 
						   <td>'.$larrformData['centername'][$expdata].'</td> 		  
						   <td>'.$larrformData['dateofexam'][$expdata].'</td> 
						   <td>'.$larrformData['session'][$expdata].'</td> 		   
				        </tr> ';				   	
	    	$cnt++; 
      		  }
	     
		 }		
	
		 $tabledata.= '<tr>		      
		     	<td></td>
		     	<td></td>
		     	<td></td>
		     </tr>
		     </table>';	
		
		//echo $tabledata;exit;
		if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}
	}
		

				
}

//updated data from the local server(15-03-2013)
public function fncancelpaymentAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		echo $idcompanyflag = $this->_getParam('companyflag');
		echo $idBatchRegistration = $this->_getParam('idBatchRegistration');
		
		$lobjpaymentdetails = $this->lobjcompanystudentdetails->fngetpaymentdetails($idBatchRegistration,$idcompanyflag);
		//echo "<pre>";print_r($lobjpaymentdetails);die();
		
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Company Name</b></th>
					<th><b>Pin Number</b></th>
					<th><b>Amount</b></th>
					<th><b>Number Of Candidates</b></th>
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['registrationPin']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalAmount']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalNoofCandidates']."</b></td>
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 7) $tabledata.= 'Credit to IBFIM';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 10) $tabledata.= 'MIGS';
		$regpin = $lobjpaymentdetails[0]['registrationPin'];
		//echo $regpin;die();
		$tabledata.= "</b></td>
					</tr>
					</table><br>";
	    $tabledata.="<table  class='table' border=1 align='center' width=100%>
	  					<tr>
	  						<td>Remarks</td>
	  						<td><input type='text' name='remarks' id='remarks' value='' > <input type='hidden' name='idBatchRegistration' id='idBatchRegistration' value='".$idBatchRegistration."' >  <input type='hidden' name='pin' id='pin' value='".$regpin."' ></td>
	  					</tr>
	  					<tr>
	  						<td>Payment Details</td>
	  						<td><input type='text' name='chequedetails' id='chequedetails' value='' ><input type='hidden' name='companyflag' id='companyflag' value='".$idcompanyflag."' > </td>
	  					</tr>
	  					<tr>
	  						<td></td>
	  						<td><input type='submit' id='submit' name='submit'  value='Payment Cancel' > &nbsp;<input type='button' id='close' name='close'  value='Close' onClick='Closefn();'> <input type='hidden' name='pin' id='pin' value='".$regpin."' ></td>
	  					</tr>
	  				</table>";	
	  					
	  
	  echo  $tabledata;
		//die();
		
	}
	public function companystudentlistAction() 
	{   
	  	$this->view->idcompany = $lintidcompany = $this->_getParam('idcompany'); 	
	  	$operatortype = $this->view->operatortype = $this->gobjsessionsis->operatortype;
		//echo $operatortype;die();
		//echo $this->gobjsessionsis->operatortype;die();
		$this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Candidate Registration";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Cancel Payment";	
		$this->view->lobjBatchcandidatesForm->option1->addMultiOptions($larrtypeofcompany);
		$larrcompany[0]['key']=1;
		$larrcompany[0]['value']="Candidate Registration";
		$larrcompany[1]['key']=2;
		$larrcompany[1]['value']="Move Candidates";
		$this->view->lobjBatchcandidatesForm->option2->addMultiOptions($larrcompany);		
		$larr[0]['key']=1;
		$larr[0]['value']="Candidate Registration";		
		$this->view->lobjBatchcandidatesForm->option3->addMultiOptions($larr);	  
	  	if($operatortype==1)
		{ 		  
			$this->view->companydetails = $res= $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(3,1,$lintidcompany); //Get company details with the other details by ID
	  	}
		else if($operatortype==2)
		{
	  		$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(7,1,$lintidcompany); //Get company details with the other details by ID
	  	}
		$laresulttemppin=$this->lobjcompanystudentdetails->fnGettempregpins($lintidcompany,$operatortype);
		$temppins=array();
		for($temp=0;$temp<count($laresulttemppin);$temp++)
		{
				   $temppins[]=$laresulttemppin[$temp]['pins'];
		}
		$this->view->temppins = $temppins;	  		
		if ($this->_request->isPost () && $this->_request->getPost ( 'submit' ))
		{
			$larrformData = $this->_request->getPost ();	
           // echo "<pre>";print_r($larrformData);die();			
			$larrformData['UpdDate'] = date( 'Y-m-d H:i:s' );
			$auth = Zend_Auth::getInstance(); 
			$larrformData['UpdUser'] =  $auth->getIdentity()->iduser;	
		    $laresulttemppin=$this->lobjcompanystudentdetails->fnGettempregpins($lintidcompany,$operatortype);		   	
			$temppins=array();
			for($temp=0;$temp<count($laresulttemppin);$temp++)
			{
			       $temppins[]=$laresulttemppin[$temp]['pins'];
			}
			$this->view->temppins = $temppins;
			$this->lobjcompanystudentdetails->fnCancelPayment($larrformData,$lintidcompany,$operatortype);
          if($operatortype==1)
		  { 		  
			$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(3,1,$lintidcompany); //Get company details with the other details by ID
	  	   }else if($operatortype==2)
		   {
	  		$this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
			$this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(7,1,$lintidcompany); //Get company details with the other details by ID
	  	   }
           $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$lintidcompany);

		}
        if ($this->_request->isPost () && $this->_request->getPost ('block'))
		{
		   
		   $larrformData = $this->_request->getPost ();
           $larr =    explode(',',$larrformData['block']);
		   $total = $larr[0];
		   $idbatch = $larr[1];
		   $totalregister = $larr[2];
		   //echo "<pre>";
		   //print_r($larr);die();
		   $auth = Zend_Auth::getInstance(); 
		   $upduser =  $auth->getIdentity()->iduser;		    
		   $this->view->companydetails = $this->lobjcompanystudentdetails->fnupdatetotalcandidates($idbatch,$total,$upduser,$lintidcompany,$totalregister); //Get company details by ID
           if($operatortype==1)
		   { 		  
			 $this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
			 $this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(3,1,$lintidcompany); //Get company details with the other details by ID
	  	   }else if($operatortype==2)
		   {
	  		  $this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
			  $this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(7,1,$lintidcompany); //Get company details with the other details by ID
	  	   }
         $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$lintidcompany);
	}	
	if ($this->_request->isPost () && $this->_request->getPost ('blockforalreadycompleted'))
		{
            $larrformData = $this->_request->getPost ();
           $larr =    explode(',',$larrformData['blockforalreadycompleted']);
		   $total = $larr[0];
		   $idbatch = $larr[1];
		   $totalregister = $larr[2];
		   //echo "<pre>";
		   //print_r($larr);die();
		   $auth = Zend_Auth::getInstance(); 
		   $upduser =  $auth->getIdentity()->iduser;		    
		   $this->view->companydetails = $this->lobjcompanystudentdetails->fnupdateforalreadycompleted($idbatch,$total,$upduser,$lintidcompany,$totalregister); //Get company details by ID
           if($operatortype==1)
		   { 		  
			 $this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
			 $this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(3,1,$lintidcompany); //Get company details with the other details by ID
	  	   }else if($operatortype==2)
		   {
	  		  $this->view->companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
			  $this->view->companyresults = $this->lobjcompanystudentdetails->fnGenerateQueries(7,1,$lintidcompany); //Get company details with the other details by ID
	  	   }
         $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$lintidcompany);


		
		}
	
         if ($this->_request->isPost () && $this->_request->getPost ( 'paymentchangerequest' ))
		{
		   $larrformData = $this->_request->getPost (); //getting the values of bank from post 		  
		   //echo $larrformData['paymentchangerequest'];
		   $larrdetails = explode(',',$larrformData['paymentchangerequest']);
		   $auth = Zend_Auth::getInstance();
		   $userid =  $auth->getIdentity()->iduser;
		   $oldpaymentmode= $larrdetails[0];
           //echo "<br/>";		   
		   $oldupddate = $larrdetails[1];      	   
		   $oldupduser = $larrdetails[2];         	   
		   $idbatch = $larrdetails[3];			
		   $idcompany = $larrdetails[4];			
		   $regpin = $larrdetails[5];			
		   $amount = $larrdetails[6];		 
		   $this->lobjcompanystudentdetails->fnpreviouspayment($larrformData,$oldpaymentmode,$oldupddate,$oldupduser,$idbatch,$idcompany,$regpin,$amount,$userid);
		   $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$idcompany);
        }	
	}
	public function fnchecktempexcelAction()
	{
	   $this->_helper->layout->disableLayout();
	   $this->_helper->viewRenderer->setNoRender ();
	    $idcompany = $this->_getParam('idcompany');	   
	    $idRegPin = $this->_getParam('idRegPin');	   
	    $idbatch = $this->_getParam('idbatch');	   
	   $companyflag = $this->_getParam('flag');	   
	   $larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$idRegPin);
	   $larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($idRegPin); // Newly added as on 21-01-2013
		$idcompanyflag = $this->_getParam('companyflag');
		$this->view->idbatchreg = $idBatchRegistration = $this->_getParam('idBatchRegistration');					
		$lobjpaymentdetails = $this->lobjcompanystudentdetails->fngetpaymentdetails($idbatch,$companyflag);			
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Company Name</b></th>
					<th><b>Pin Number</b></th>
					<th><b>Amount</b></th>
					<th><b>Number Of Candidates</b></th>
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['registrationPin']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalAmount']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalNoofCandidates']."</b></td>
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 7) $tabledata.= 'Credit to IBFIM';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 10) $tabledata.= 'MIGS';		
		$tabledata.= "</b></td>
					</tr></table><br>";
		 if($larrstudentscountcount['totalNoofCandidates'] == $larrstudentscountcount['totalregistered'])
		{
			      $tabledata.="<table  class='table' border=1 align='center' width=100%>
									<tr>
										<td><b>All Candidates Successfully Registered...!!!</td>
										<td><input type='hidden' id='submit1'   value='".$larrstudentscountcount['totalregistered'].",".$idbatch."'> </td>";
						$tabledata.='<td><button type="submit" class="NormalBtn" dojotype="dijit.form.Button"  id="blockforalreadycompleted" name="blockforalreadycompleted" value="'.$larrstudentscountcount['totalregistered'].','.$idbatch.','.$larrstudentscountcount['totalNoofCandidates'].'">Block Registration</button> &nbsp;<input type="button" id="close" class="NormalBtn" dojotype="dijit.form.Button" label="Exit"
                                       name="Exit"  value="Exit" onClick="BlockClosefn();"> </td></tr>
								</table>';
																
					echo  $tabledata;
					die();
				 
		}
		elseif($larrstudentscountcount['totalregistered'] < $larrstudentscountcount['totalNoofCandidates'])
		{
		    if($larrstudentcounttemp['totalcount'] ==0)
			{	
					$tabledata.="<table  class='table' border=1 align='center' width=100%>
									<tr>
										<td><input type='hidden' id='submit1'   value='".$larrstudentscountcount['totalregistered'].",".$idbatch."'> </td>";
						$tabledata.='<td><button type="submit" class="NormalBtn" dojotype="dijit.form.Button"  id="submit" name="block" value="'.$larrstudentscountcount['totalregistered'].','.$idbatch.','.$larrstudentscountcount['totalNoofCandidates'].'">Block Registration</button> &nbsp;<input type="button" id="close" class="NormalBtn" dojotype="dijit.form.Button" label="Close
                                       name="close"  value="Close" onClick="BlockClosefn();"> </td>';
						$tabledata.=	"</tr>
								</table>";	
					echo  $tabledata;
					die();
			}
			else
			{
			   $tabledata.="<table  class='table' border=1 align='center' width=100%>
									<tr>
										<td><b>Still Candidates there in tempexcelcandidates...First Delete And Then Block The Registration... !!!</td>
										<td><input type='button' id='close1' name='close'  value='Close' onClick='BlockClosefn();'> </td>
									</tr>
								</table>";	
					echo  $tabledata;
					die();
			
			}
		}
		
	}
	public function changecompanypaymentAction()
	{
	    $this->_helper->layout->disableLayout();
	    $this->_helper->viewRenderer->setNoRender ();
	    $idcompany = $this->_getParam('idcompany');
		$companyflag = $this->_getParam('flag');
		$idRegPin = $this->_getParam('idRegPin');		
	    $idbatch = $this->_getParam('idbatch');
	   $larrresult =0;	  
	   $previousdetails = $this->lobjcompanystudentdetails->fngetpindetails($idbatch,$companyflag);
	   $paymentmode = $previousdetails['ModeofPayment'];
	   $upddatetime = $previousdetails['UpdDate'];
	   $upduser = $previousdetails['UpdUser'];
	   // echo "<pre>";
	   //print_r($previousdetails);die();
	   if($companyflag ==1)
	   {
	      $larrresult = $this->lobjcompanystudentdetails->checkforcredittoibfim($idcompany);
		}
	   $larrstudentscountcount = $this->lobjcompanystudentdetails->fnGenerateQueries(5,0,$idRegPin);
	    // $larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel($idRegPin); // Newly added as on 21-01-2013
		$idcompanyflag = $this->_getParam('companyflag');
		$this->view->idbatchreg = $idBatchRegistration = $this->_getParam('idBatchRegistration');					
		$lobjpaymentdetails = $this->lobjcompanystudentdetails->fngetpaymentdetails($idbatch,$companyflag);
		$amt=$lobjpaymentdetails[0]['totalAmount'];
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left">Present Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><th><b>Company Name</b></th>
					<th><b>Amount</b></th>
					<th><b>Number Of Candidates</b></th>
					<th><b>Payment Mode</b></th>
					</tr>
					<tr>
					<td><b>".$lobjpaymentdetails[0]['CompanyName']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalAmount']."</b></td>
					<td><b>".$lobjpaymentdetails[0]['totalNoofCandidates']."</b></td>
					<td><b>";if($lobjpaymentdetails[0]['ModeofPayment'] == 1) $tabledata.= 'FPX'; 
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 2) $tabledata.= 'Credit Card';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 4) $tabledata.= 'Cheque';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 7) $tabledata.= 'Credit to IBFIM';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 181) $tabledata.= 'Paylater';
		else if($lobjpaymentdetails[0]['ModeofPayment'] == 10) $tabledata.= 'MIGS';
		
		$tabledata.="</tr></table>";
		$tabledata.= '<br><fieldset><legend align = "left">Change Payment Details </legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%>
					<tr><td><b>Change Payment Mode To:</b></td>";
					if($companyflag == 1)
					{
							if($larrresult)
							{
								$tabledata.= '<td ><select name="paymentdetails" style="width: 140px"  id="comp" >
														<option id="1"  value="4">Cheque</option>
														<option id="2" value="7">Credit To IBFIM</option>
											</select></td>';
							}
							else
							{
									$tabledata.= '<td ><select name="paymentdetails" style="width: 140px"  id="comp" >
												<option id="1"   value="4">Cheque</option></select></td>';							   
							}
					}		
				else{
						   $tabledata.= '<td ><select name="paymentdetails" style="width: 140px"  id="comp" >
												<option id="1" value="4" >Cheque</option>
												<option id="2" value="181">Paylater</option>
									</select></td>';
					}		
		$tabledata.='<tr><td><b>Reference number:</b></td><td><input type="text" dojoType="dijit.form.ValidationTextBox" id="refnumber" name="refnumber" required="true"/></td><tr>';
		//$tabledata.='<tr><td><b>Date:</b></td><td><input type="date" name="date1" id="date1" dojoType="dijit.form.DateTextBox"   /></td></tr>';
		$tabledata.='</table>';					
		$tabledata.="<table   class='table' border=1 align='center' width=100%>
									<tr>";
		$tabledata.='<td align="center"><button type="submit" class="NormalBtn" dojotype="dijit.form.Button"  id="paymentchangerequest"  name="paymentchangerequest" value="'.$paymentmode.','.$upddatetime.','.$upduser.','.$idbatch.','.$idcompany.','.$idRegPin.','.$amt.'">Save</button> &nbsp;<input type="button" id="closechangerequest" class="NormalBtn" dojotype="dijit.form.Button" label="Close
                                       name="Back"  value="Close" onClick="fnpaymentchangerequest();"> </td>';
		$tabledata.=	"</tr>
								</table>";	
		echo  $tabledata;
		die();
	}
	public function regpinsmovecandidatesAction()
	{
	   $this->view->lobjform = $this->lobjsearchForm; //send the lobjuserForm object to the view
	   $this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;
	   $this->view->idcompany = $idcompany = $this->_getParam('idcompany');
	   $this->view->flag = $companyflag = $this->_getParam('companyflag');
	   $this->view->regpin = $idRegPin = $this->_getParam('idRegPin');
	   $this->view->batch = $idbatch = $this->_getParam('batch');
	   $larrresult = $this->lobjcompanystudentdetails->fngettotalcandidatesforbatch($idbatch,$companyflag);
	   $larramount =   $this->lobjcompanystudentdetails->gettotalamount($idbatch);	 
       $this->view->amount	 =  $larramount['Totalamount'];
$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Same company";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="To Dummy";
		$this->view->lobjBatchcandidatesForm->NewType->addMultiOptions($larrtypeofcompany);
	   

	 // 
	   $this->view->totalcandidates = $larrresult[0]['totalNoofCandidates'];
	    $this->view->paymentmode  = $larrresult[0]['ModeofPayment'];
	  // $larrresult = $this->lobjcompanystudentdetails->fnGetProgramFee($idprogram); 
		//echo $larrresult['sum(abc.amount)'];
       $regpindetails = $this->lobjcompanystudentdetails->fngetpindetailsformovecandidates($idbatch,$companyflag,$idcompany);
	  
	   $this->view->companyname =  $regpindetails[0]['CompanyName'];
	    if ($this->_request->isPost () && $this->_request->getPost ('Save'))
		{
		  		//echo $companyflag ;
			
		   $larrformData = $this->_request->getPost (); //getting the values of bank from post 	
//print_r($larrformData['NewRegpin']);
		   
		   if(empty($larrformData['NewRegpin']))
		   {
		   		echo '<script language="javascript">alert("Please select regpin")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/regpinsmovecandidates/idRegPin/$idRegPin/idcompany/$idcompany/companyflag/$companyflag/batch/$idbatch';</script>";
 	die();
		   	
		   }
		   
		     if($larrformData['NewType']==2)
           {	
		 if(empty($larrformData['stuapp']))
		   {
		   		echo '<script language="javascript">alert("Please select atleast One candidate")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/regpinsmovecandidates/idRegPin/$idRegPin/idcompany/$idcompany/companyflag/$companyflag/batch/$idbatch';</script>";
 		die();
		   	
		   }
           }
		//   echo "<pre/>";print_r($larrformData);die();
		   //echo $larrformData['paymentchangerequest'];		  
		   
		   $auth = Zend_Auth::getInstance();
		   $userid =  $auth->getIdentity()->iduser;	
           $larrformData['UpdUser'] =  $userid; 
		   $larrformData['UpdDate'] = date('Y-m-d:H-i-s');
		   if(!empty($larrformData))
		   {
           if($larrformData['NewType']==2)
           {			

          /* 	echo "<pre/>";
           	print_r($larrformData);
           	die();
		 
           	for($idapp=0;$idapp<count($larrformData['stuapp']);$idapp++)
           	{
           		if($idapp==0)
           		{
           		$iadpps=$larrformData['stuapp'][$idapp];
           		}
           		else 
           		{
           		$iadpps.=','.$larrformData['stuapp'][$idapp];	
           		}
           		}
           		if(empty($iadpps))
           		{
           			$iadpps=0;
           		}
           		//echo $iadpps;die();
           		$larrgetmonths= $this->lobjcompanystudentdetails->fngetmonthsbyid($iadpps);
           		if(!empty($larrgetmonths))
           		{
           			
           		$larrpin=$this->lobjcompanystudentdetails->fngetregisterpinsbymonth($larrgetmonths);
           		if(count($larrgetmonths)==count($larrpin))
           		{
           			$totalcandidates   = $larrformData['totalcandidates'];
		 $totalamount=$larrformData['amount'];
		 if(!empty($totalcandidates))
		 {
		 	if($totalcandidates<=0)
		 	{
		 		$totalcandidates=1;
		 	}
           	$unitamount=$totalamount/$totalcandidates;
		 }
           			
           			for($pin=0;$pin<count($larrpin);$pin++)
           			{
           				$newpin=$larrpin[$pin];
           				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
	   $lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a"=>"tbl_studentapplication"),array("a.totalNoofCandidates","a.totalAmount","a.idBatchRegistration"))
					   ->where("a.registrationPin ='$regpin'");
	   $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
           				
           				
           				
           				
           				
           			}
           		}
           		else 
           		{
           		}
           		die();
           
           		}*/
           //	echo "<pre/>";
           //	print_r($larrformData);
           	//die();
           	$fngetnewpins=$this->lobjcompanystudentdetails->fngetdummionepin($larrformData['NewRegpin']);
           	$larramountss =   $this->lobjcompanystudentdetails->gettotalamountss($larrformData['idbatch']);	 
           	
           	if(empty($fngetnewpins))
           	{
           			echo '<script language="javascript">alert("Plese contact IBFIM admin")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/regpinsmovecandidates/idRegPin/$idRegPin/idcompany/$idcompany/companyflag/$companyflag/batch/$idbatch';</script>";
 		die();
           	}
           	$larrformData['amount']=$larramountss['totalAmount'];
           	$larrformData['newdbpin']=$fngetnewpins['registrationPin'];
           //	echo   	$larrformData['newdbpin'];
          // 	die();
           		$larrmoveabsents= $this->lobjcompanystudentdetails->fninsertabsentbatchdetails($larrformData,$companyflag);
           }
           else 
           {
           	 $larrofmoveresult = $this->lobjcompanystudentdetails->fninsertmovedetails($larrformData);
           }
		   }
		   $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$idcompany);
        }
	}
	public function getallregpinsAction()
	{
	  $this->_helper->layout->disableLayout();
	  $this->_helper->viewRenderer->setNoRender ();
	  $idbatch = $this->_getParam('batch');
	  $companyflag = $this->_getParam('companyflag');
	  $idcompany = $this->_getParam('idcompany');
	  $regpindetails = $this->lobjcompanystudentdetails->fngetpinformovecandidates($idbatch,$companyflag,$idcompany);
	  //echo "<pre>";
	  // print_r($regpindetails);die();
	  $larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ($regpindetails);
	 // echo "<pre>";
	 // print_r($larrgetregpinDetailss);die();
	  echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );		
	  
	}
public function deletebatchidAction()
	{
	    $this->_helper->layout->disableLayout();
	    $this->_helper->viewRenderer->setNoRender ();	   
	    $idcompany = $this->_getParam('idcompany');
		//echo "<br/>";
		 $companyflag = $this->_getParam('flag');
		//echo "<br/>";
		 $idRegPin = $this->_getParam('idRegPin');
       // echo "<br/>";		
	    $idbatch = $this->_getParam('idbatch');
		//echo "<br/>";die();
		 $auth = Zend_Auth::getInstance(); 
		   $upduser =  $auth->getIdentity()->iduser;	
		$regpindetails = $this->lobjcompanystudentdetails->fndeletebatchid($idbatch,$idRegPin,$idcompany,$companyflag,$upduser);
	
	}

		public function companydummibatchlistAction()
	{
		$this->_helper->layout->disableLayout();
	  $this->_helper->viewRenderer->setNoRender ();
	  $idbatch = $this->_getParam('batch');
	  $companyflag = $this->_getParam('companyflag');
	  $idcompany = $this->_getParam('idcompany');
	  
	   $regpindetailsss = $this->lobjcompanystudentdetails->fngetpinfordummimonths($idbatch,$companyflag,$idcompany);
	   
	   if(!empty($regpindetailsss))
	   {
	
	  //echo "<pre>";
	  // print_r($regpindetails);die();
	  $larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ($regpindetailsss);
	 // echo "<pre>";
	 // print_r($larrgetregpinDetailss);die();
	   }
	   else {
	   $larrgetregpinDetailss="";
	   }
	   
	   //print_r($larrgetregpinDetailss);
	   //die();
	  echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );
	}

	public function getallabsentstudentsAction()
	{
		 $this->_helper->layout->disableLayout();
	  $this->_helper->viewRenderer->setNoRender ();
	  $idbatch = $this->_getParam('batch');
	  $companyflag = $this->_getParam('companyflag');
	  $idcompany = $this->_getParam('idcompany');
	    $dates = $this->_getParam('datess');
	  $absentdetails = $this->lobjcompanystudentdetails->fngetabsentcandidates($idbatch,$companyflag,$idcompany,$dates);
	  //echo "<pre>";
	  // print_r($absentdetails);die();
	   $tabledata = '';
		$tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><input type='checkbox' name='allstu' id='allstu' value='".count($absentdetails)."' onclick='fngetcheckall(this.value);'/></th><th><b>Student Name</b></th><th><b>ICNO</b></th><th><b>Registration ID</b></th><th><b>Program</b></th><th><b>Exam Date(DD-MM-YYYY)</b></th><th><b>Exam Venue</b></th><th><b>Exam Session</b></th><th><b>Status</b></th></tr><tr>";
		$m=0;
		foreach($absentdetails as $lobjCountry)
		{
			$idapp=$lobjCountry['IDApplication'];
		$tabledata.="<td><input type='checkbox' id='stuapp".$m."'  name='stuapp[]' value='".$idapp."' /></td><td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left'>".$lobjCountry['Regid']."</td><td align = 'left'>".$lobjCountry['ProgramName']."</td><td align = 'left'>".$lobjCountry['examdate']."</td><td align = 'left'>".$lobjCountry['centername']."</td><td align = 'left'>".$lobjCountry['managesessionname']."</td><td align = 'left'>";
		if($lobjCountry['pass']==1)
		{
		$tabledata.="Pass";
		}
		if($lobjCountry['pass']==2)
		{
		$tabledata.="Fail";
		}
		if($lobjCountry['pass']==3)
		{
		$tabledata.="Applied";
		}
		if($lobjCountry['pass']==4)
		{
		$tabledata.="Absent";
		}
		$m++;
			$tabledata.="</td></tr>";
		}
		$tabledata.="</table><br>";
		if(empty($absentdetails))
		{
			$tabledata = '';
		}
		echo  $tabledata;
	   
	   
	  //$larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ($regpindetails);
	 // echo "<pre>";
	 // print_r($larrgetregpinDetailss);die();
	 // echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );	
		
	}
	public function invoicegenerationAction()
	{
	    $lintidcompany =$this->view->idcompany = $this->_getParam('idcompany');
		$this->view->regpin = $lintidregistrationpin = $this->_getParam('idRegPin');
		$this->view->compflag = $companyflag = $this->_getParam('companyflag');
$this->view->Idbatch =  $this->_getParam('batch');

//$larrresult = $this->lobjcompanystudentdetails->Fngetactualcandidates($lintidregistrationpin);
$candidatesdetails = $this->lobjcompanystudentdetails->fngettotalcount($lintidregistrationpin);
				//echo "<pre>";
				//print_r($candidatesdetails);die();
 $larrresult = $this->lobjcompanystudentdetails->Fngetactualcandidates($lintidregistrationpin);
  if(empty($larrresult))
				{ 
					 echo '<script language="javascript">alert("No Candidates Present Invoice Can Not Generate")</script>';
					 echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/companystudentlist/idRegPin/$lintidregistrationpin/idcompany/$lintidcompany';</script>";
					 die();
				}
		        if($candidatesdetails['totalNoofCandidates'] != $candidatesdetails['Totalappln'])
		        {
					 echo '<script language="javascript">alert("There is a mismatch can not generate invoice")</script>';
//echo "fine";die();
					 echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/companystudentlist/idRegPin/$lintidregistrationpin/idcompany/$lintidcompany';</script>";
					 die();
		   
		        }
		
		$this->view->pindetails = $Pindetails = $this->lobjcompanystudentdetails->gettotalcandidates($lintidregistrationpin);
		if($companyflag ==1)
		{
		   $this->view->companydetails = $companydetails =  $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
		}
		else
		{
		   $this->view->companydetails = $companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
		}
		 $this->view->studentdetails = $studentdetails = $this->lobjcompanystudentdetails->fnGenerateQueries(4,1,$lintidregistrationpin); //Get company details by ID	
		    if($companyflag == 2)
			{
			  $Typeofoperator = 'T';
			}
			if($companyflag==1)
			{
			  $Typeofoperator = 'C';
			}
		   if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		   {
		        $larrformData = $this->_request->getPost ();
				
                                 if(empty($larrresult))
				{ 
					 echo '<script language="javascript">alert("No Candidates Present Invoice Can Not Generate")</script>';
					 echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/companystudentlist/idRegPin/                                         $lintidregistrationpin/idcompany/$lintidcompany';</script>";
					 die();
				}
		        if($candidatesdetails['totalNoofCandidates'] != $candidatesdetails['Totalappln'])
		        {
					 echo '<script language="javascript">alert("There is a mismatch can not generate invoice")</script>';
					 echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/companystudentlist/idRegPin/                                         $lintidregistrationpin/idcompany/$lintidcompany';</script>";
					 die();
		   
		        }
		       
				
				$Uniqueid = "IBFIM/".$Typeofoperator.'/'.$lintidcompany.'/'.date('Y').'/';
				$auth = Zend_Auth::getInstance(); 
				$upduser =  $auth->getIdentity()->iduser;	
				$this->lobjcompanystudentdetails->fnupdateinvoicegenerated($Pindetails,$lintidregistrationpin,$Uniqueid,$upduser,$lintidcompany,$companyflag);
                                  $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$lintidcompany);		
                   }		  		
		   if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) 
		   {	
		         //Exporting data to an excel sheet 
				   $lstrreportytpe="Pdf";
				   $this->_helper->layout->disableLayout();
				   $this->_helper->viewRenderer->setNoRender();
			    $larrformData = $this->_request->getPost ();
                            $Pin=       $larrformData['Regpin'];
				//echo "<pre>";
				//print_r($larrformData);die();
				if($larrformData['flag']==1)		
				{
				   $companyName="Name Of The Company:".$larrformData['CompanyName'];
				}
				elseif($larrformData['flag']==2)
				{
				   $companyName="Name Of The Takaful:".$larrformData['CompanyName'];
				}
				$frmdate =date('d-m-Y');
				$day= date("d-m-Y");
				$host = $_SERVER['SERVER_NAME'];
				$imgp = "http://".$host."/tbenew/images/reportheader.jpg";				
				$time = date('h:i:s',time());
				$filename = 'Invoice_Report_'.$frmdate;
				$ReportName = $this->view->translate( "INVOICE" ).' '.$this->view->translate( "REPORT" );			
				if($lstrreportytpe=='Pdf'){
					$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
				}else{
					$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
				}
				$tabledata.= "<br>";
							
		        $tabledata.= '<br><table border="1" align = "center" >
							<tr >
								
                                                              <td align = "center" colspan = "8"><b> '.$ReportName.'</b> </td>									
							</tr>

							<tr>								
								<td colspan = "2"><b>'.$companyName.'</b></td>
							        <td colspan = "2"><b>BatchId:'.$Pin.'</b></td>
								<td colspan = "2"><b>Date: '.$day.'</b></td>
								<td colspan = "2"><b> Time:'.$time.'</b></td>
							</tr>
					   </table><br>';
                        
		        $tabledata.= '<table border= "1" align=center >
						<tr>
							<th><b>Student Name</b></th>
							<th><b>ICNO</b></th>							
							<th><b>Registration Id</b></th>
							<th><b>Exam Date</b></th>
							<th><b>Venue Name</b></th>
							<th><b>Session Name</b></th>
							<th><b>Program Applied</b></th>
							<th><b>Result</b></th>						
						</tr>';  
		if (count($larrformData))
		{
			 $cnt = 0;
				for($expdata=0;$expdata<count($larrformData['FName']);$expdata++)
				{
					  $tabledata.= ' <tr>';
							   
					  $tabledata.= '<td>'.$larrformData['FName'][$expdata].'</td> 
							   <td>'.$larrformData['ICNO'][$expdata].'</td> 						   
							   <td>'.$larrformData['Regid'][$expdata].'</td> 
							   <td>'.$larrformData['dateofexam'][$expdata].'</td> 
								<td>'.$larrformData['centername'][$expdata].'</td>
							<td>'.$larrformData['sessionname'][$expdata].'</td> 
							<td>'.$larrformData['ProgramName'][$expdata].'</td> 
							   <td>'.$larrformData['result'][$expdata].'</td> 		   
							</tr> ';				   	
					  $cnt++; 
				}
		 }	
		 $tabledata.= '<tr></tr><tr><td colspan="4"></td>';
         $tabledata.= '<td><b>Total Candidates:</b></td> 
							   <td>'.$larrformData['TotalCandidates'].'</td> 						   
							   <td><b>Total Amount:</b></td> 
							   <td>'.$larrformData['Amount'].'</td>
                                </tr></table>';							   
				if($lstrreportytpe=='Pdf')
				{
					include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
					$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
					$mpdf->SetDirectionality ( $this->gstrHTMLDir );
					$mpdf->text_input_as_HTML = true;
					$mpdf->useLang = true;
					$mpdf->SetAutoFont();
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumSuffix = ' / ';
					$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
					$mpdf->allow_charset_conversion = true; // Set by default to TRUE
					$mpdf->charset_in = 'utf-8';
					ini_set('max_execution_time',3600);
					$mpdf->WriteHTML($tabledata);
					$mpdf->Output($filename.pdf,'D');
				}else
				{
					$ourFileName = realpath('.')."/data";
					$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
					ini_set('max_execution_time', 3600);
					fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
					fclose($ourFileHandle);
					header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
					header("Content-Disposition: attachment; filename=$filename.xls");
					header("Pragma: no-cache");
					header("Expires: 0");
					readfile($ourFileName);
					unlink($ourFileName);
				}
		}
	}
public function viewgenerationAction()
{
 $lintidcompany =$this->view->idcompany = $this->_getParam('idcompany');
		$this->view->regpin = $lintidregistrationpin = $this->_getParam('idRegPin');
		$this->view->compflag = $companyflag = $this->_getParam('companyflag');
		$larrresult = $this->lobjcompanystudentdetails->Fngetactualcandidates($lintidregistrationpin);
		$candidatesdetails = $this->lobjcompanystudentdetails->fngettotalcount($lintidregistrationpin);
		//echo "<pre>";
		//print_r($candidatesdetails);die();
$larrresult = $this->lobjcompanystudentdetails->Fngetactualcandidates($lintidregistrationpin);
		if(empty($larrresult))
		{ 
		     echo '<script language="javascript">alert("No Candidates Present Invoice Not Viewed")</script>';
		     echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/companystudentlist/idRegPin/$lintidregistrationpin/idcompany/$lintidcompany';</script>";
 		     die();
		}
		if($candidatesdetails['totalNoofCandidates'] != $candidatesdetails['Totalappln'])
		{
		     echo '<script language="javascript">alert("There is a mismatch can not generate invoice")</script>';
		     echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/companystudentlist/idRegPin/$lintidregistrationpin/idcompany/$lintidcompany';</script>";
 		     die();
		   
		}
		$this->view->Configdetails = $Configdetails = $this->lobjcompanystudentdetails->fngetreceivermail($companyflag);
				$this->view->BatchAddressdetails =$BatchAddressdetails = $this->lobjcompanystudentdetails->fngetbatchaddress($lintidcompany,$companyflag);
		
		//$this->view->pindetails = $Pindetails = $this->lobjcompanystudentdetails->getinvoicedetails($lintidregistrationpin);
                $this->view->pindetails = $Pindetails = $this->lobjcompanystudentdetails->gettotalcandidates($lintidregistrationpin);
               // echo "<pre>";
                //print_r($Pindetails);die();
		if($companyflag ==1)
		{
		   $this->view->companydetails = $companydetails =  $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
		}
		else
		{
		   $this->view->companydetails = $companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
		}
		 $this->view->studentdetails = $studentdetails = $this->lobjcompanystudentdetails->fnGenerateQueries(4,1,$lintidregistrationpin); //Get company details by ID	
		 // echo "<pre>";
//print_r($studentdetails);
if(empty($BatchAddressdetails['Fax']))
				{
				   $BatchAddressdetails['Fax']='';
				}
				if($companyflag == 1)		
				{
				   $this->view->comp = $companyName="".$companydetails['CompanyName'];
				}
				elseif($companyflag == 2)
				{
				   $this->view->comp = $companyName="".$companydetails['CompanyName'];
				}
    
 if($companyflag == 2)
			{
			  $Typeofoperator = 'T';
			}
			if($companyflag==1)
			{
			  $Typeofoperator = 'C';
			}
			 
			$Uniqueid = "IBFIM/".$Typeofoperator.'/'.$lintidcompany.'/'.date('Y').'/';
			$auth = Zend_Auth::getInstance(); 
		    $upduser =  $auth->getIdentity()->iduser;
		
		   if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
		   {
                        $larrformData = $this->_request->getPost ();
			$this->lobjcompanystudentdetails->fnupdateinvoicegenerated($Pindetails,$lintidregistrationpin,$Uniqueid,$upduser,$lintidcompany,$companyflag);
                          $this->_redirect( $this->baseUrl . '/registrations/companystudentdetails/companystudentlist/idcompany/'.$lintidcompany);
                    }		
		   $larrprogram=$this->lobjcompanystudentdetails->fngetbatchinvoicedetails($lintidregistrationpin);
			$this->view->studentprogramdetails = $larrprogram;
			//echo "<pre/>";
			//print_r($larrprogram);
			$idprogram='';
			for($pr=0;$pr<count($larrprogram);$pr++)
			{
			if($pr==0)
			{
			$idprogram=$larrprogram[$pr]['Program'];
			}
			else
			{
			$idprogram.=$idprogram.','.$larrprogram[$pr]['Program'];
			}
			
			}
			if(empty($idprogram))
			{
			$idprogram=0;
			}
			$larrprogramdetails=$this->lobjcompanystudentdetails->fngetprogramdetails($idprogram);
			
			//print_r($larrprogramdetails);
			
			$prgramname=array();
			$programrate=array();
			
			for($prg=0;$prg<count($larrprogramdetails);$prg++)
			{
			    $idprg=$larrprogramdetails[$prg]['IdProgrammaster'];
			    $prgramname[$idprg]=$larrprogramdetails[$prg]['ProgramName'];
			    $programrate[$idprg]=$larrprogramdetails[$prg]['Rate'];
			}
			$this->view->Program = $prgramname;
			$this->view->Programrate = $programrate;
	
}
	public function viewinvoiceAction()
	{
	    $lintidcompany =$this->view->idcompany = $this->_getParam('idcompany');
		$this->view->regpin = $lintidregistrationpin = $this->_getParam('idRegPin');
		$this->view->compflag = $companyflag = $this->_getParam('companyflag');
		$larrresult = $this->lobjcompanystudentdetails->Fngetactualcandidates($lintidregistrationpin);
		$candidatesdetails = $this->lobjcompanystudentdetails->fngettotalcount($lintidregistrationpin);
		//echo "<pre>";
		//print_r($candidatesdetails);die();
$larrresult = $this->lobjcompanystudentdetails->Fngetactualcandidates($lintidregistrationpin);
		if(empty($larrresult))
		{ 
		     echo '<script language="javascript">alert("No Candidates Present Invoice Not Viewed")</script>';
		     echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/companystudentlist/idRegPin/$lintidregistrationpin/idcompany/$lintidcompany';</script>";
 		     die();
		}
		if($candidatesdetails['totalNoofCandidates'] != $candidatesdetails['Totalappln'])
		{
		     echo '<script language="javascript">alert("There is a mismatch can not generate invoice")</script>';
		     echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companystudentdetails/companystudentlist/idRegPin/$lintidregistrationpin/idcompany/$lintidcompany';</script>";
 		     die();
		   
		}
		
		
		$this->view->pindetails = $Pindetails = $this->lobjcompanystudentdetails->getinvoicedetails($lintidregistrationpin);
                //echo "<pre>";
               // print_r($Pindetails);die();
		if($companyflag ==1)
		{
		   $this->view->companydetails = $companydetails =  $this->lobjcompanystudentdetails->fnGenerateQueries(2,0,$lintidcompany); //Get company details by ID
		}
		else
		{
		   $this->view->companydetails = $companydetails = $this->lobjcompanystudentdetails->fnGenerateQueries(6,0,$lintidcompany); //Get company details by ID
		}
		 $this->view->studentdetails = $studentdetails = $this->lobjcompanystudentdetails->fnGenerateQueries(4,1,$lintidregistrationpin); //Get company details by ID	
		 // echo "<pre>";
//print_r($studentdetails);
    
 if($companyflag == 2)
			{
			  $Typeofoperator = 'T';
			}
			if($companyflag==1)
			{
			  $Typeofoperator = 'C';
			}
			 
			$Uniqueid = "IBFIM/".$Typeofoperator.'/'.$lintidcompany.'/'.date('Y').'/';
			$auth = Zend_Auth::getInstance(); 
		    $upduser =  $auth->getIdentity()->iduser;	
			$this->lobjcompanystudentdetails->fnupdateinvoicegenerated($Pindetails,$lintidregistrationpin,$Uniqueid,$upduser,$lintidcompany,$companyflag);		
		   $larrprogram=$this->lobjcompanystudentdetails->fngetbatchinvoicedetails($lintidregistrationpin);
			$this->view->studentprogramdetails = $larrprogram;
			//echo "<pre/>";
			//print_r($larrprogram);
			$idprogram='';
			for($pr=0;$pr<count($larrprogram);$pr++)
			{
			if($pr==0)
			{
			$idprogram=$larrprogram[$pr]['Program'];
			}
			else
			{
			$idprogram.=$idprogram.','.$larrprogram[$pr]['Program'];
			}
			
			}
			if(empty($idprogram))
			{
			$idprogram=0;
			}
			$larrprogramdetails=$this->lobjcompanystudentdetails->fngetprogramdetails($idprogram);
			
			//print_r($larrprogramdetails);
			
			$prgramname=array();
			$programrate=array();
			
			for($prg=0;$prg<count($larrprogramdetails);$prg++)
			{
			    $idprg=$larrprogramdetails[$prg]['IdProgrammaster'];
			    $prgramname[$idprg]=$larrprogramdetails[$prg]['ProgramName'];
			    $programrate[$idprg]=$larrprogramdetails[$prg]['Rate'];
			}
			$this->view->Program = $prgramname;
			$this->view->Programrate = $programrate;
			
		   if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) 
		   {	
		         //Exporting data to an excel sheet 
				   $lstrreportytpe="Pdf";
				   $this->_helper->layout->disableLayout();
				   $this->_helper->viewRenderer->setNoRender();
			    $larrformData = $this->_request->getPost ();
				$invoiceid = $larrformData['invoiceid'];
				//echo "<pre>";
				//print_r($larrformData);die();
				$this->view->Configdetails = $Configdetails = $this->lobjcompanystudentdetails->fngetreceivermail($larrformData['flag']);
				$this->view->BatchAddressdetails =$BatchAddressdetails = $this->lobjcompanystudentdetails->fngetbatchaddress($lintidcompany,$larrformData['flag']);
				if(empty($BatchAddressdetails['Fax']))
				{
				   $BatchAddressdetails['Fax']='';
				}
				if($larrformData['flag']==1)		
				{
				   $this->view->comp = $companyName="".$larrformData['CompanyName'];
				}
				elseif($larrformData['flag']==2)
				{
				   $this->view->comp =$companyName="".$larrformData['CompanyName'];
				}
				$frmdate =date('d-m-Y');
				$day= date("d-m-Y");
				$host = $_SERVER['SERVER_NAME'];
				$imgp = "http://".$host."/tbenew/images/reportheader.jpg";				
				$time = date('h:i:s',time());
				$filename = 'Invoice_Report_'.$frmdate;
				$ReportName = $this->view->translate( "INVOICE" );			
				if($lstrreportytpe=='Pdf'){
					$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
				}else{
					$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
				}
				$tabledata.= "<br/>";
							
		        $tabledata.= '<table border="1" width="100%" align="center" >
							<tr>
								<th  colspan = "6"><b>'.$ReportName.'</b></th>
								
							</tr>
							<tr >';								
							$tabledata.= "<th colspan='3' align='left'><b>To:
</b><b>&nbsp;&nbsp;&nbsp;$companyName</b><br/>
<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$BatchAddressdetails['Contact']}</b><br/>";
									$tabledata.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$BatchAddressdetails['Address']}</b><br/>";
									$tabledata.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$BatchAddressdetails['Postcode']}</b><br/>";
									$tabledata.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$BatchAddressdetails['StateName']}</b><br/>";
									$tabledata.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$BatchAddressdetails['CountryName']}</b><br/>";
									$tabledata.="<b>Attn:</b><b>&nbsp;{$BatchAddressdetails['Contact']}</b>";
$tabledata.="<br/>";
									$tabledata.="<b>Tel:</b><b>&nbsp;&nbsp;{$BatchAddressdetails['Phone']}
                                                                                                                        </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Fax:</b><b>{$BatchAddressdetails['Fax']}</b><br/></th>
								        <th align ='left' valign='top' colspan='3'>
									<b>Invoice:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									{$invoiceid}</b><br/><br/>
									<b>Date:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									$day</b><br/><br/>
									<b>Term:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									{$Configdetails['Duration']} Days</b></th>
								
							</tr>
						</table>
					<br>";
		        $tabledata.= "<table width='100%' border=1 align='center' >
						<tr>
                                                        <th ><b>SL NO</b></th>
							<th ><b>BatchId</b></th>
							<th ><b>Program Name</b></th>
							<th ><b>No Of Candidates</b></th>							
							<th ><b>Rate</b></th>
							<th ><b>TOTAL</b></th>
						</tr>";
		if (count($larrformData))
		{
			 $cnt = 0;
 $slcount=0;
				for($expdata=0;$expdata<count($larrformData['ProgramName']);$expdata++)
				{
                                 $slcount =$slcount+1;
					  $tabledata.= ' <tr>';
							   
					  $tabledata.= '<td  align="center"><b>'.$slcount.'</b></td>
                                                        <td  align="center"><b>'.$larrformData['Regpin'].'</b></td>
                                                         <td  align="center"><b>'.$larrformData['ProgramName'][$expdata].'</b></td> 
							   <td  align="center" ><b>'.$larrformData['TotalCandidates'][$expdata].'</b></td> 						   
							   <td  align="center" ><b>'.$larrformData['Programrate'][$expdata].'</b></td> 
							   <td  align="center" ><b>'.$larrformData['Amount'][$expdata].'</b></td> 
							</tr> ';				   	
					  $cnt++; 
				}
		 }
		$Amountinwords = $this->lobjcompanystudentdetails->fnconvertnumbertowords($larrformData['Totalamount']);		 
		 $tabledata.= '<tr>';
         $tabledata.= '<th colspan="5" >Ringgit Malaysia:<b>'.$Amountinwords.' Only</b></th> 						   
							   <th ><b>Total Amount:</b>'.$larrformData['Totalamount'].'</th>
                                </tr></table>';
$tabledata.="<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
						 $tabledata.='<table><tr></tr>
								<tr><td><b>Note:</b></td>
								</tr>
								<tr><td colspan=6><b>1.  Please make payment within '.$Configdetails['Duration'].' Days from the date of registration.
                                 </b></td>
								</tr>
								<tr><td colspan=6><b>2.  All cheques must be  made payable to "IBFIM-TBE".</b></td>
								</tr>
								<tr><td colspan=6><b>3.  Please send this copy of invoice together with your payment for the attention of TBE Secretariat, Learning Management				
	                                      Talent Development Department, IBFIM.</b></td>
								</tr><br/><br/><br/><br/><br/>
<tr><td align="center"><b>This is a computer generated document.  No signature is required.</b></td></tr>
<tr>
<td><hr/></td></tr>
<tr>
<td align="center"><b>IBFIM (Co. No. 763075W)
3rd Floor, Menara Takaful Malaysia, Jalan Sultan Sulaiman,  50000 Kuala Lumpur, MALAYSIA
(Tel) +603-2031 1010  (Fax) +603-2026 9988   (E-mail) tbe@ibfim.com
</b></td></tr>
								</table>';								   
				if($lstrreportytpe=='Pdf')
				{
					include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
					$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
					$mpdf->SetDirectionality ( $this->gstrHTMLDir );
					$mpdf->text_input_as_HTML = true;
					$mpdf->useLang = true;
					$mpdf->SetAutoFont();
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 1; // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumSuffix = ' / ';
//$mpdf->setFooter ('This is a computer generated document.  No signature is required.  IBFIM (Co. No. 763075W)
//3rd Floor, Menara Takaful Malaysia, Jalan Sultan Sulaiman,  50000 Kuala Lumpur, MALAYSIA
//(Tel) +603-2031 1010  (Fax) +603-2026 9988   (E-mail) tbe@ibfim.com
//');
 //$mpdf->SetHTMLFooterForPage( $footer, 1);
					$mpdf->setFooter ('' );
					$mpdf->allow_charset_conversion = true; // Set by default to TRUE
					$mpdf->charset_in = 'utf-8';
					ini_set('max_execution_time',3600);
					$mpdf->WriteHTML($tabledata);
					$mpdf->Output($filename.pdf,'D');
				}else
				{
					$ourFileName = realpath('.')."/data";
					$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
					ini_set('max_execution_time', 3600);
					fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
					fclose($ourFileHandle);
					header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
					header("Content-Disposition: attachment; filename=$filename.xls");
					header("Pragma: no-cache");
					header("Expires: 0");
					readfile($ourFileName);
					unlink($ourFileName);
				}
		}
		
	}
	
	
}
