<?php
class Registrations_CompanyapplicationController extends Base_Base 
{  
	public $gidcompany;
	
	public function init()
	{   //initialization function	

		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    
   	    $this->lobjcompanymaster = new App_Model_Companymaster(); //Company student details model object		
		$this->lobjTakafulmodel = new App_Model_Takafulapplication ();
		$this->lobjTakafulForm = new App_Form_Takafulapplication (); //intialize user lobjuserForm
		$this->lobjstudentmodel = new App_Model_Companyapplication(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object		
		$this->lobjstudentForm = new App_Form_Companyapplication(); //intialize user lobjuserForm
		$this->lobjAdhocApplicationForm = new App_Form_Adhocapplication();
		$this->lobjform = new App_Form_Search ();
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates(); //user model object	
		$this->lobjCompanypayment = new Finance_Model_DbTable_Approvecreditstudenttakaful ();
		$this->lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //Company student details model object
   		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates ();

		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$auth = Zend_Auth::getInstance();

	}
	
	public function indexAction() {

		// action for search and view
		$lobjform=$this->view->lobjform = $this->lobjform; //send the lobjuserForm object to the view
		/*test data */
		//$larrtestresult = $this->lobjcompanystudentdetails->fnGetubsurddatetime();
		
		
		
		/***********************************************/
		//$larrrbusinesstypelist = $this->lobjcompanymaster->fnGetBusinesstypeList (); //get Business type list details		
		//$lobjform->field5->addMultioptions($larrrbusinesstypelist);
		
		$lobjform->field1->setAttrib('OnChange', 'fnGetCityList');
		$lobjform->field3->setAttrib('onkeyup', 'fnGetOperatorNames');
		//$lobjform->field8 ->setRegisterInArrayValidator(false);
		 
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Company";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Takaful";
		$this->view->lobjform->field19->addMultiOptions($larrtypeofcompany);	
		
		$larrresult = $this->lobjcompanystudentdetails->fnGenerateQueries(1,1); //get Company details
		$larrresult=0;
	
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->companystudentdetailspaginatorresult);
						
		$lintpagecount = $this->gintPageCount=10;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
        		
		if(isset($this->gobjsessionsis->companystudentdetailspaginatorresult)) {			
			$lobjform->field19->setValue($this->gobjsessionsis->operatortype);		
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->companystudentdetailspaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();			
			if ($lobjform->isValid ( $larrformData )) {				
			  if(!$larrformData['field19']){
					echo '<script language="javascript">alert("Please Select Type Of Company")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/companyapplication/index/';</script>";					
					exit;
				}
				$this->gobjsessionsis->operatortype = $this->view->operatortype = $larrformData['field19'];				
				$lobjform->field19->setValue($this->view->operatortype);								
				$larrresult = $this->lobjcompanystudentdetails->fnSearchCompanies($lobjform->getValues ()); //searching the values for the Companies
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->companystudentdetailspaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			unset($this->gobjsessionsis->operatortype);
		 	$this->_redirect( $this->baseUrl . '/registrations/companyapplication/index');
		}
	}
	
   public function ibfimAction()
	{
	    $this->view->operatortype = $operator = $this->_getParam('operatortype');
		$this->view->insertedId =$insertedId = $this->_getParam('insertedId');
	    $this->view->idcompany = $idcompany = $this->_getParam('idcompany');	
	   $larresultbatchdetails = $this->lobjstudentmodel->fngetbatchregistrationdetails($insertedId);
	  
		$regpin = $larresultbatchdetails['registrationPin'];
		if($regpin==0)
		{
			$randomnumber1 = rand(100000,999999);
		    $randomnumber2 = rand(100000,999999);
		    $regpin  = $randomnumber1.''.$randomnumber2;
		}
		
		$larresult = $this->lobjstudentmodel->fnupdatebatchregdetailsforibfimpayment($insertedId,$regpin);		
		$this->view->lobjstudentForm = $this->lobjstudentForm;		
		$idcompany=$this->_getParam('idcompany');
		if($this->gobjsessionsis->operatortype==1){
				$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idcompany);	
		}
		else
		{
				$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ($idcompany);
		}	
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($insertedId);	
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $insertedId;	
	}
	
	
   public function displayAction()
	{
		
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		$lintinsertedId = $this->_getParam('insertedid');	
		//	$this->gobjsessionsis->operatortype
		$idcompany=$this->_getParam('idcompany');
		if($this->gobjsessionsis->operatortype==1){
				$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idcompany);	
		}else{
				$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $idcompany );
		}	
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);	
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $lintinsertedId;
	}
	  
	
	public function showpopupregistrationAction() {
		
		$this->_helper->layout->disableLayout ();
		$idregpin = $this->_getParam ( 'idbatch' );		
				
		$larrsultfailedStud=$this->lobjTakafulmodel->fnGetfailedregistrered ($idregpin);		
		$this->view->failedstudentlist=$larrsultfailedStud;
		
		$larrresultStudent = $this->lobjTakafulmodel->fnGetStudregistrered ($idregpin);
		$this->view->regpin = $idregpin;
		if (!$larrresultStudent) {
			$this->view->showshceduler = 0;
		} else {
			$this->view->showshceduler = 1;
		}	
	}
	
	
	public function venueseatavailabilityAction() {
		
		$this->_helper->layout->disableLayout ();
		$idvenue = $this->_getParam ( 'idvenue' );
		$fromdate = $this->_getParam ( 'fromdate' );
		$todate = $this->_getParam ( 'todate' );		
		$larrresultcenter = $this->lobjTakafulmodel->fnGetexamcenterfull ( $idvenue, $fromdate, $todate );		
		$this->view->examcenterfull = $larrresultcenter;
	
	}
	
	public function newapplicationAction() { // action for search and view
		
		$this->view->lobjform = $this->lobjform;
		$this->view->lobjTakafulForm = $this->lobjTakafulForm;
		$month = date ( "m" ); // Month value
		$day = date ( "d" ); //today's date
		$year = date ( "Y" ); // Year value
		$yesterdaydate = date ( 'Y-m-d', mktime ( 0, 0, 0, $month, ($day - 1), $year ) );
		
		$datsearch = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}";
		$this->view->lobjform->FromDate->setAttrib ( 'constraints', $datsearch );
		$this->view->lobjform->FromDate->setAttrib ( 'required', "true" );
		$this->view->lobjform->ToDate->setAttrib ( 'required', "true" );
		
		$gidcompany = $this->view->idcompany = $idcompany = $this->_getParam ( 'idcompany' ); 
	    $operatortype = $this->view->operatortype = $this->gobjsessionsis->operatortype; 
				
		$lobjform=$this->view->lobsearchform = new App_Form_Search ();//send the lobjuserForm object to the view
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idcompany); //get user details
		
 		$this->view->lobjstudentForm = $this->lobjstudentForm;
  		$this->view->companyDetails =  $larrresult;
  		
  		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramNameList();
		$this->view->lobsearchform->field1->addMultiOptions($larrbatchresult);
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Print' )) {
			$larrformData = $this->_request->getPost ();		
		    $larrresultcenter = $this->lobjTakafulmodel->fnGetexamcenter ( $larrformData );
			$this->view->lobjform->FromDate->setValue ( $larrformData ['FromDate'] );
			$this->view->lobjform->ToDate->setValue ( $larrformData ['ToDate'] );
			$this->view->examcenter = $larrresultcenter;
			
		}
  		
  		$larrresultdiscount = $this->lobjstudentmodel->fngetintialdiscount();
  		
  		/*$larrresultcenter = $this->lobjstudentmodel->fnGetexamcenter();
  		$this->view->examcenter=$larrresultcenter;*/
  		
  		$larrresultcourse= $this->lobjstudentmodel->fnGetcoursename();
  		$this->view->coursename=$larrresultcourse;  		
  		$this->view->discount=$larrresultdiscount['Discount'];  		
  		$larrcaptionresult = $this->lobjstudentmodel->fnGetCaptionName();
  		if($larrcaptionresult['CourseAliasName'])
  		{
  			$this->view->caption=$larrcaptionresult['CourseAliasName'];
  		}
  		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();		
		//$this->lobjstudentForm->idPrograms->addMultiOption('0','Select');
		$this->lobjstudentForm->idPrograms->addMultiOptions($larrbatchresult);
		$this->lobjstudentForm->idCompany->setValue($idcompany);

		/// to get Credit to IBFIM account //
		
		$larrresult = $this->lobjstudentmodel->fntogetpaylater($idcompany);
		if($larrresult)
		{
		   $this->lobjstudentForm->ModeofPayment->addMultiOption('7','Credit to IBFIM account');
		}
		
		// end of Credit to IBFIM account //
		
		
		
		///////////////////////////for pay later/////////////////////////
		if($operatortype==1){		
				$larrpaylater = $this->lobjstudentmodel->fngetpaylater ( $idcompany );		
		}else{
				$larrpaylater = $this->lobjTakafulmodel->fngetpaylater ( $idcompany );	
		}
		$cntpaylater = count ( $larrpaylater );
		if ($cntpaylater > 1) {
			$this->lobjstudentForm->ModeofPayment->addMultiOption ( $larrpaylater ['idDefinition'], $larrpaylater ['DefinitionDesc'] );
		}		
		$this->lobjstudentForm->ModeofPayment->setValue ( 181 );
		//////////////////////////end for pay later/////////////////////		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save'))
		{   
			
			$larrformData = $this->_request->getPost ();
$larrformData['Servicetax'] = number_format($larrformData['Servicetax'], 2);			
			if ($this->lobjstudentForm->isValid($larrformData)) 
			{					 
				$larrformData['UpdUser']= 1;
				$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );					
				$lastInsId = $this->lobjstudentmodel->fnInsertPaymentdetails($larrformData);
						
			if ($larrformData ['ModeofPayment'] == 181) {
					$larrformDataapp ['IDApplication'] = $lastInsId;
					$larrformDataapp ['companyflag'] = $operatortype;
					$larrformDataapp ['Amount'] = $larrformData ['grossAmt'];
					$larrformDataapp ['UpdUser'] = 1;
					$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
					$regid = $this->lobjCompanypayment->fngeneraterandom ();	
					$flag=0;// IF ordinary registration				
					$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);
				}
			/*if($larrformData['ModeofPayment']==10)
			{   				
				
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/migspayment/operatortype/$operatortype/insertedId/".$lastInsId."/idcompany/".$larrformData['idCompany']);
			}	*/			
				
			$db = Zend_Db_Table::getDefaultAdapter();				
			$lastpaymentid=$this->lobjstudentmodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$lastInsId,$operatortype);
			$auth = Zend_Auth::getInstance();// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Data is Saved"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
			$lastpayid=$this->lobjstudentmodel->fngetmodeofpayment($lastpaymentid);
			
			if($lastpayid['ModeofPayment']==4)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/display/insertedid/".$lastInsId."/idcompany/".$larrformData['idCompany']); 
			}
			if($lastpayid['ModeofPayment']==1)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/fpxpageone/insertedId/".$lastInsId."/idcompany/".$larrformData['idCompany']); 
			}
			if($lastpayid['ModeofPayment']==10)
			{
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/migspayment/operatortype/$operatortype/insertedId/".$lastInsId."/idcompany/".$larrformData['idCompany']);
			}
			if($lastpayid['ModeofPayment']==7)
			{
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/ibfim/operatortype/$operatortype/insertedId/".$lastInsId."/idcompany/".$larrformData['idCompany']);
			}
			
			else if($lastpayid['ModeofPayment']!=2)
			{   				
				$this->_redirect( $this->baseUrl . "/registrations/companystudentdetails/companystudentlist/idcompany/".$larrformData['idCompany']);
			}
			
			else 
			{
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/confirmpayment/insertedId/".$lastInsId."/idcompany/".$larrformData['idCompany']);
			    exit;
			}
		}		
	  }
	}

	
	public function confirmpaymentAction()
	{
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		$lintinsertedId = $this->_getParam('insertedId');
		$idcompany=$this->_getParam('idcompany');
		
	//	$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idcompany);	

		if($this->gobjsessionsis->operatortype==1){
				$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idcompany);	
		}else{
				$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $idcompany );
		}
		
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);
			
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $lintinsertedId;
		//Get SMTP Mailing Server Setting Details
		$postArray = $this->_request->getPost ();
		$this->view->pageStatus = 1;
		if($postArray){	
				if($postArray['payment_status'] = 'Completed'){
					$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					$postArray['Regid']  = substr($postArray['txn_id'], 1, 6).rand(1000, 9999).substr($postArray['txn_id'], 5, 9);
					$this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$idcompany,$lintinsertedId);	
					$this->view->Regid= $postArray['Regid'];	
					$this->view->mess = "Payment Completed Sucessfully";					
					$this->view->pageStatus = 2;					
						$StudModel = new App_Model_Studentapplication();
						$larrSMTPDetails  = $StudModel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $StudModel->fnGetEmailTemplateDescription("Batch Registration");
					
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
													
							$larrEmailIds[0] = $larrStudentMailingDetails["Email"];
							$larrNames[0] 	 = $larrStudentMailingDetails['CompanyName'];
							$lstrStudentName = $larrStudentMailingDetails['CompanyName'];
							
							try{
								$lobjProtocol->connect();
						   		$lobjProtocol->helo($lstrSMTPUsername);
								$lobjTransport->setConnection($lobjProtocol);
						 	
								//Intialize Zend Mailing Object
								$lobjMail = new Zend_Mail();
						
								$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
								$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
								$lobjMail->addHeader('MIME-Version', '1.0');
								$lobjMail->setSubject($lstrEmailTemplateSubject);
						
								for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
									if($larrEmailIds[$lintI] != ""){
										$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);	
																
										//replace tags with values
										//$Link = "<a href='".$this->Url."/registration'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Company]",$lstrStudentName,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$postArray['mc_gross'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$postArray['Regid'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										$lobjMail->setBodyHtml($lstrEmailTemplateBody);
								
										try {
											$lobjMail->send($lobjTransport);
										} catch (Exception $e) {
											$lstrMsg = "error";      				
										}	
										$lobjMail->clearRecipients();
										$this->view->mess .= ". Login Details have been sent to user Email";
										unset($larrEmailIds[$lintI]);
									}
								}
							}catch(Exception $e){
								$lstrMsg = "error";
							}
						}else{
							$lstrMsg = "No Template Found";
						}
				}
				else {
					$this->view->mess = "Payment Failed";
				}				
			}		
	}

	public function getprogramtotalAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idprogram = (int)$this->_getParam('idprogram');		
		$larrresult = $this->lobjstudentmodel->fnGetProgramFee($idprogram); 
		echo json_encode($larrresult);die();
		exit;
	}
	
	
	//New auto search option in index page Ation 27-9-2012
	
	public function getoperatornamesAction(){
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$newresult="";
		$opType = $this->_getParam('opType');	
		$namestring = $this->_getParam('namestring');	
			
		$larrresultopnames = $this->lobjcompanystudentdetails->fnGetOperatorNames($opType,$namestring); 
		//echo Zend_Json_Encoder::encode ( $larrresultopnames );
		//exit;
		
		foreach ($larrresultopnames as $larrresnewarray){
						$opname=$larrresnewarray['name'];
			$newresult=$newresult."<tr><td><span id='idspan'   onclick='fnsetvalue(\"".$opname."\");'>".$larrresnewarray['name']."</span></td></tr></BR>";			
		}
		
		echo $newresult;
		exit;
		
		
	}
	
			
	public function getdiscountAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$nocandidates = (int)$this->_getParam('nocandidates');		
		$idprogram = (int)$this->_getParam('idprogram');

		$larrresultprog = $this->lobjstudentmodel->fngetprogramdiscount($idprogram,$nocandidates);
		if(!$larrresultprog){
					echo 0;
				}else{
					echo $larrresultprog[0]['Amount'];
				}
		die();
	}
	
	
    public function printreportAction() 
    {			
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();		
		$IdApplication = (int) $this->_getParam('insertedId');					
	    $larrresult = $this->lobjstudentmodel->fnGetExamDetails($IdApplication);  
	    $totamt= (int)$larrresult ['totalAmount'];		    
		$AmountInWords = $this->lobjstudentmodel->fnGetAmountInWords($totamt);
/*		print_r($AmountInWords);
		die();*/
		    						
		//object to initialize ini file
		  $lobjAppconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini','development');										
		    try 
		    {	
	            //java class
	            $lobjdbdriverclass = new Java("java.lang.Class");
	            
	            //set db driver
	            $lobjdbdriverclass->forName("com.mysql.jdbc.Driver");
	
	            //driver manager object
	            $lobjdrivermanager = new Java("java.sql.DriverManager");
	            
	            //get the db connection
				$lstrConnection  =  "jdbc:mysql://".
										$lobjAppconfig->resources->db->params->host."/".
										$lobjAppconfig->resources->db->params->dbname."?user=".
										$lobjAppconfig->resources->db->params->username."&password=".
										$lobjAppconfig->resources->db->params->password;
														
				$lobjconnection = $lobjdrivermanager->getConnection($lstrConnection);
	           // print_r($lobjconnection);
	          //  die();
	            //Jasper Compile manager object
	            $lobjcompileManager = new Java(
	            					"net.sf.jasperreports.engine.JasperCompileManager");	            
	            echo "CompileManager object created</br>";
	            $lstrreportdir = realpath(".") . "/report/";
	            $lstrimagepath = realpath(".") . "/images/";
	            	
	             //compiled report path
	              $lobjreport = $lobjcompileManager->compileReport(realpath($lstrreportdir."companyapplicationreport.jrxml"));	            	            
	            //Jasper Fill Manager object
	           /* print_r($lobjreport);
	            die();*/
	            $lobjfillManager = new Java(
	            					"net.sf.jasperreports.engine.JasperFillManager");
	            $int1 = new Java("java.lang.Integer");
	            //Hashmap object
	           //print_r($lstrreportdir);die();
	            $lobjparams = new Java("java.util.HashMap");	            
	          	$lobjparams->put("IDAPPLICATION",$IdApplication);
	          	$lobjparams->put ( "IMAGEPATH", $lstrimagepath . "ibfim.jpg" );
	          	$lobjparams->put("AMOUNTINWORDS",$AmountInWords['Amount']);
	          	
	           
	           echo "Fill Manager</br>";
	            					
	            //Jasper Print Object
	            $lobjjasperPrint = $lobjfillManager->fillReport(
	            					$lobjreport, $lobjparams, $lobjconnection);
	            					
	            echo "Jasper Printed</br>";
	            
	            //Jasper Export Manager object
	            $lobjexportManager = new Java(
	            					"net.sf.jasperreports.engine.JasperExportManager");
	            
	            //output file path
	            $lstrhtmloutputPath = realpath(".") . "/" . "output.html";
	            echo "Before Export</br>";
	            $session = Zend_Session::getId();
	            $lstrpdfoutputPath = realpath(".") . "/" . "$session.pdf";
	            $objStream = new Java("java.io.ByteArrayOutputStream");
	            $lobjexportManager->exportReportToPdfFile($lobjjasperPrint,$lstrpdfoutputPath);
	            
	            //Export report to HTML	            
	            echo 'HTML Exported</br>';
	
				header("Content-type: application/pdf;charset=utf-8;encoding=utf-8");
				header('Content-Disposition: attachment; filename="Company_details.pdf"');
				
	            readfile($lstrpdfoutputPath);
	            unlink($lstrpdfoutputPath);
				echo "finished";	
		 		 		            
		    } 
		    catch (JavaException $lobjexception) 
		    {
		    	echo 'Exception caught: ', $lobjexception->getMessage() . "\n";
		    }		    		   
			 //$this->_redirect( $this->baseUrl . "/companyapplication/index/editid/1");
	}

	
	//////
	
	
public function changepasswordAction() { // action for search and view
		$lobjform=$this->view->lobsearchform = new App_Form_Search ();//send the lobjuserForm object to the view
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany); //get user details
 		$this->view->lobjstudentForm = $this->lobjstudentForm;
 		$this->view->lobjAdhocApplicationForm = $this->lobjAdhocApplicationForm;
  		$this->view->companyDetails =  $larrresult;
		$idcomp=$this->gsessionbatch->idCompany;
		
		$pass=$larrresult['Password'];
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save')){
			$larrformData = $this->_request->getPost ();
			if ($this->lobjAdhocApplicationForm->isValid($larrformData)) {	
			
			$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );
			$larrformData['oldpassword']=md5($larrformData['oldpassword']);
			//print_r($larrformData);die();
		if($pass==$larrformData['oldpassword'])
		{
			$larrformData['newpassword']=md5($larrformData['newpassword']);
           //echo $larrformData['newpassword'];die();
			$lastInsId = $this->lobjstudentmodel->updatecompanypass($larrformData['newpassword'],$idcomp);
			    $auth = Zend_Auth::getInstance();// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Password Changed"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);	
		  	$this->_redirect( $this->baseUrl . "/companyapplication/index");
		
		}
		else 
		{
			echo '<script language="javascript">alert("The Old Password is Not Correct")</script>';
				//$this->_redirect( $this->baseUrl . "/companyapplication/changepassword");
  			//die();
			
		}	
			
		}
		}
	}
	
	
	public function fpxpageoneAction(){
		
		$lintinsertedId = $this->_getParam('insertedId');	
		
		$this->view->lintIdCompany = $idcompany =$this->_getParam('idcompany');
		
		//$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idcompany);	
		
		if($this->gobjsessionsis->operatortype==1){
			
				$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idcompany);	
		}else{
				$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $idcompany );
		}
		
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);	
	
		
		$this->view->data = $larrPaymentDetails;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->CmpnyIds = $lintinsertedId;	
			
		unset($_SESSION["pageName"]);
		unset($_SESSION["StudsId"]);
		unset($_SESSION["idCompany"]);
		
		$_SESSION["pageName"] = "companyapplication";	
		$_SESSION["CmpnyId"]  = $lintinsertedId;
		$_SESSION["idCompany"]  = $idcompany;	
		
		
	}	
	public function fpxpagetwoAction(){			
		
		$this->view->intidstudent=$lintinsertedId = $this->_getParam('insertedId');  
		$idCompany = $this->_getParam('companyId');		
			
		//$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idCompany);		
		
		if($this->gobjsessionsis->operatortype==1){
				$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idcompany);	
		}else{
				$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $idcompany );
		}
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);		
		
		  //print_r($larrresult);exit;	
		  
		$this->view->data = $larrPaymentDetails;	
		
		error_reporting(E_ALL);
		$address = "127.0.0.1";
		$service_port = 6000;
		// Create a TCP/IP socket. 
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($socket < 0){ 
			//echo "socket_create() failed: reason: " . socket_strerror($socket) . "\n"; 
		}
		else{ 
				//echo "Socket creation successfull."; 
		}				
		// Establish socket connection. 
		$result = socket_connect($socket, $address, $service_port);
		if (!$result){ 
			//echo "Socket connection failed.<br>";
			die();
		}
		else{ 
			//echo "Socket connection successfull.<br>"; 
		}				
		// Generating String to send to plugin. 
		$messageOrderNo = $_POST['TxnOrderNo'];
		$messageTXNTime = date('YmdHis');
		$sellerOrderNo = $_POST['TxnOrderNo'];
		$this->view->TxnAmount = $messageAmount = $_POST['TxnAmount'];
		$sellerID = $_POST['sellerID'];			
		
		$in = "message:request|message.type:AR|message.token:01|message.orderno:$messageOrderNo|message.ordercount:1|message.txntime:$messageTXNTime|message.serialno:1|message.currency:MYR|message.amount:$messageAmount|charge.type:AA|seller.orderno:$sellerOrderNo|seller.id:$sellerID|seller.bank:01|\n";
		$out = '';			
		
		socket_write($socket, $in);		
		while ($out = socket_read($socket,6001)){
			$fpxValue = $out;	
		}
		$sendFpxValue = str_replace("\n", "", $fpxValue);
		$this->view->sendFpxValue = $sendFpxValue;			
		socket_close($socket);
	}	
	
	
	public function migspaymentAction()
{
	
	$this->view->operatortype = $operator = $this->_getParam('operatortype');
		$this->view->insertedId =$insertedId = $this->_getParam('insertedId');
	$this->view->idcompany = $idcompany = $this->_getParam('idcompany');
	if($operator==1)
	{
		$larrcompanydetails = $this->lobjstudentmodel->fngetbatchcompdetails($insertedId);
		$this->view->name = $larrcompanydetails['CompanyName'];
		$this->view->amount = $larrcompanydetails['totalAmount'];
	}
	else 
	{
		$larrcompanydetails = $this->lobjstudentmodel->fngetbatchtakafuldetails($insertedId);
		$this->view->name = $larrcompanydetails['TakafulName'];
		$this->view->amount = $larrcompanydetails['totalAmount'];
	}

	
}

public function mipgtwoAction()
{
		$larrformData = $this->_request->getPost ();
	$this->view->formdata = $larrformData;	
	$this->view->operatortype = $larrformData['operatortype'];
}

public function mipgrequestingAction()
{
	$operator = $this->_getParam('operatortype');
	$migsarray = $_GET;
	$larresult = $this->lobjstudentmodel->fninsertmigspayment($_GET,$operator);
	$responsecode = $_GET['vpc_TxnResponseCode'];
	$idbatchregistration =$_GET['vpc_MerchTxnRef'];
	$larresultbatchdetails = $this->lobjstudentmodel->fngetbatchregistrationdetails($idbatchregistration);
	$this->view->idcompany = $larresultbatchdetails['idCompany'];
	if($responsecode=='0')
	{
		$regpin = $larresultbatchdetails['registrationPin'];
		if($regpin==0)
		{
			$randomnumber1 = rand(100000,999999);
		$randomnumber2 = rand(100000,999999);
		$regpin  = $randomnumber1.''.$randomnumber2;
		}
		
		$larresult = $this->lobjstudentmodel->fnupdatebatchregdetails($idbatchregistration,$regpin);
		$this->view->status = 1;
	}
	else 
	{
		
	}
	
	
}

}

	