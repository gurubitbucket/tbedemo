<?php
class Registrations_RegistrationfailedcompanyController extends Base_Base { //Controller for the User Module
	
	public function init() { //initialization function		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object		
   	    
		$this->lobjTakafulmodel = new App_Model_Takafulapplication();
		$this->lobjCommon = new App_Model_Common ();		
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //user model object			
		$this->lobjTakafulForm = new App_Form_Takafulapplication (); //intialize user lobjuserForm			
		$this->lobjstudentForm = new App_Form_Studentapplication ();
		$this->lobjstudentmodel = new App_Model_Studentapplication ();
		$this->lobjcompanymodel = new App_Model_Companyapplication();
		$this->lobjCompanypayment = new Finance_Model_DbTable_Approvecreditstudenttakaful ();			
		$this->lobjsearchForm = new App_Form_Search (); //intialize Search lobjsearchForm
		$this->lobjcompanymaster = new App_Model_Companymaster(); //Company student details model object
		$this->lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //Company student details model object
	
		$this->registry = Zend_Registry::getInstance ();
		$this->locale = $this->registry->get ( 'Zend_Locale' );
		$auth = Zend_Auth::getInstance();
		/*if($auth->getIdentity()->iduser == 17)
		{
		   $this->_helper->layout()->setLayout('/web/usty');
		}*/
	}
		
	
	public function indexAction(){
		
		$lobjform=$this->view->lobjform = $this->lobjsearchForm; //send the lobjuserForm object to the view		
		$larrrbusinesstypelist = $this->lobjcompanymaster->fnGetBusinesstypeList (); //get Business type list details
		$lobjform->field5->addMultioptions($larrrbusinesstypelist);
		$lobjform->field1->setAttrib('OnChange', 'fnGetCityList');
		$lobjform->field3->setAttrib('onkeyup', 'fnGetOperatorNames');
		$lobjform->field8->setRegisterInArrayValidator(false);
		
		$larrtypeofcompany[0]['key']=1;
		$larrtypeofcompany[0]['value']="Company";
		$larrtypeofcompany[1]['key']=2;
		$larrtypeofcompany[1]['value']="Takaful";
		$this->view->lobjform->field19->addMultiOptions($larrtypeofcompany);
		
		$larrresult = $this->lobjcompanystudentdetails->fnGenerateQueries(1,1); //get Company details		
		$larrresult=0;	
		if(!$this->_getParam('search')){		   	
			unset($this->gobjsessionsis->companystudentreregpaginatorresult);
		}
		
		$lintpagecount = $this->gintPageCount;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
        		
		if(isset($this->gobjsessionsis->companystudentreregpaginatorresult)) {		
			$lobjform->field19->setValue($this->gobjsessionsis->operatortype);	
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->companystudentreregpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {			
			$larrformData = $this->_request->getPost ();				
			if ($lobjform->isValid ( $larrformData )) {		
				
			    if(!$larrformData['field19']){
					echo '<script language="javascript">alert("Please Select Type Of Company")</script>';
					echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/registrationfailedcompany/index/';</script>";					
					exit;
				}
				$this->gobjsessionsis->operatortype = $this->view->operatortype = $larrformData['field19'];				
				$lobjform->field19->setValue($this->view->operatortype);				
				$larrresult = $this->lobjcompanystudentdetails->fnSearchCompanies($lobjform->getValues ()); //searching the values for the Companies
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->companystudentreregpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			unset($this->gobjsessionsis->operatortype);
		 	$this->_redirect( $this->baseUrl . '/registrations/registrationfailedcompany/index');
		}
	
	}
	public function ibfimAction()
	{
	    $this->view->operatortype = $operator = $this->_getParam('operatortype');
		$this->view->insertedId =$insertedId = $this->_getParam('insertedId');
	    $this->view->idcompany = $idcompany = $this->_getParam('idcompany');	
	   $larresultbatchdetails = $this->lobjcompanymodel->fngetbatchregistrationdetails($insertedId);
	  
		$regpin = $larresultbatchdetails['registrationPin'];
		if($regpin==0)
		{
			$randomnumber1 = rand(100000,999999);
		    $randomnumber2 = rand(100000,999999);
		    $regpin  = $randomnumber1.''.$randomnumber2;
		}
		
		$larresult = $this->lobjcompanymodel->fnupdatebatchregdetailsforcredittoibfimpayment($insertedId,$regpin);
		
		$this->view->lobjstudentForm = $this->lobjstudentForm;		
		//$idcompany=$this->_getParam('idcompany');
		if($this->gobjsessionsis->operatortype==1){
				$larrresult = $this->lobjcompanymodel->fngetCompanyDetails($idcompany);	
		}
		else
		{
				$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ($idcompany);
		}	
		$larrPaymentDetails = $this->lobjcompanymodel->fngetPaymentDetails($insertedId);	
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $insertedId;	
	}
	
	public function addreregistrationAction() {// Action for Search
			
		$this->view->lobjstudentForm = $this->lobjstudentForm;		
		$this->view->lobjTakafulForm = $this->lobjTakafulForm; //intialize user lobjuserForm
			
		$larrbatchresult = $this->lobjTakafulmodel->fnGetProgramName ();
		$this->view->lobjTakafulForm->idPrograms->addMultiOptions ( $larrbatchresult );		
		$this->view->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		
		$this->view->lobjstudentForm->Program->setAttrib('style','width:165px;');  		
		$lintidcompany = $this->_getParam('idcompany');
		$this->view->idcompany=$lintidcompany; 
	
			/*
			 * if(!$this->_getParam('search')) 
				unset($this->gobjsessionsis->companypaginator);			
				$lintpagecount = $this->gintPageCount;		
				$lintpage = $this->_getParam('page',1); //Paginator instance
			*/
        		
		$larrresultdiscount = $this->lobjTakafulmodel->fngetintialdiscount ();
		$this->view->discount = $larrresultdiscount['discount'];		
		
		/// to get Credit to IBFIM account //
		
		$larrresult = $this->lobjcompanymodel->fntogetpaylater($lintidcompany);
		//
		if($larrresult)
		{
		   // echo "<pre>";
		    //print_r($larrresult);die();
		   $this->view->lobjTakafulForm->ModeofPayment->addMultiOption('7','Credit to IBFIM account');
		}
		
		// end of Credit to IBFIM account //
		
		$larrpaylater = $this->lobjTakafulmodel->fngetpaylater ( $lintidcompany );		
		$cntpaylater = count ( $larrpaylater );				
		if ($cntpaylater > 1) {
		       
				$this->view->lobjTakafulForm->ModeofPayment->addMultiOption ( $larrpaylater ['idDefinition'], $larrpaylater ['DefinitionDesc'] );
		}
				$this->view->lobjTakafulForm->ModeofPayment->setValue ( 181 );
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost ();
            $larrformData['Servicetax'] = number_format($larrformData['Servicetax'], 2);			
			if ($this->view->lobjTakafulForm->isValid ( $larrformData )) {				
									
			$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );			
			$lastInsId = $this->lobjcompanymodel->fnInsertPaymentdetails($larrformData);
			
			$flag=1;// If function called from re-registration module
			if ($larrformData ['ModeofPayment'] == 181) {
				$flag=0;
			}
					$larrformDataapp ['IDApplication'] = $lastInsId;
					$larrformDataapp ['companyflag'] = $this->gobjsessionsis->operatortype;
					$larrformDataapp ['Amount'] = $larrformData ['grossAmt'];
					$larrformDataapp ['UpdUser'] = 1;
					$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
				//	echo $flag;					exit;
					$regid = $this->lobjCompanypayment->fngeneraterandom ();	
					$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);
				
			$db = Zend_Db_Table::getDefaultAdapter();				
			
			$lastpaymentid=$this->lobjcompanymodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$lastInsId,$this->gobjsessionsis->operatortype);
			
				$auth = Zend_Auth::getInstance();// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Data is Saved"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
				$lastpayid=$this->lobjcompanymodel->fngetmodeofpayment($lastpaymentid);
				
				$this->lobjTakafulmodel->fninsertintostudentapplication($larrformData,$lastInsId,$regid,$lintidcompany,$this->gobjsessionsis->operatortype);				
			
			if($lastpayid['ModeofPayment']==4)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/display/insertedid/".$lastInsId."/idcompany/".$lintidcompany); 
			}
			if($lastpayid['ModeofPayment']==1)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/fpxpageone/insertedId/".$lastInsId."/idcompany/".$lintidcompany); 
			}
			if($lastpayid['ModeofPayment']==10)
			{
				$operator = $this->gobjsessionsis->operatortype;
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/migspayment/operatortype/$operator/insertedId/$lastInsId/idcompany/$lintidcompany");
			}
			if($lastpayid['ModeofPayment']==7)
			{
			    $operator = $this->gobjsessionsis->operatortype;
				$this->_redirect( $this->baseUrl . "/registrations/registrationfailedcompany/ibfim/operatortype/$operator/insertedId/".$lastInsId."/idcompany/".$lintidcompany);
			}
			else if($lastpayid['ModeofPayment']!=2)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/companystudentdetails/companystudentlist/idcompany/".$lintidcompany);
			}
			
			else
			{
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/confirmpayment/insertedId/".$lastInsId."/idcompany/".$lintidcompany);
				exit;
			}
				$this->_redirect( $this->baseUrl . "/registrations/companystudentdetails/companystudentlist/idcompany/".$lintidcompany);
				exit;
			}	
		}
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'proceed' )) {
			
			$larrformData = $this->_request->getPost ();
			
			//print_r($larrformData);die();
			$lintidcompany=$larrformData['companyid'];
			if($larrformData){
			
				$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );			
			$lastInsId = $this->lobjcompanymodel->fnReregistrationPaymentdetails($larrformData);
			
			$flag=1;// If function called from re-registration module
			if ($larrformData ['ModeofPayment'] == 181) {
				$flag=0;
			}
					$larrformDataapp ['IDApplication'] = $lastInsId;
					$larrformDataapp ['companyflag'] = $this->gobjsessionsis->operatortype;
					$larrformDataapp ['Amount'] = $larrformData ['totalamount'];
					$larrformDataapp ['UpdUser'] = 1;
					$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
				//	echo $flag;					exit;
					$regid = $this->lobjCompanypayment->fngeneraterandompins ();	
					$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);
				
			$db = Zend_Db_Table::getDefaultAdapter();				
			
			$lastpaymentid=$this->lobjcompanymodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$lastInsId,$this->gobjsessionsis->operatortype);
			
				$lastpayid=$this->lobjcompanymodel->fngetmodeofpayment($lastpaymentid);
				
				$this->lobjTakafulmodel->fninsertintostudentapplicationonpopup($larrformData,$lastInsId,$regid,$lintidcompany,$this->gobjsessionsis->operatortype);				
			
			if($lastpayid['ModeofPayment']==4)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/display/insertedid/".$lastInsId."/idcompany/".$lintidcompany); 
			}
			if($lastpayid['ModeofPayment']==1)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/fpxpageone/insertedId/".$lastInsId."/idcompany/".$lintidcompany); 
			}
			if($lastpayid['ModeofPayment']==10)
			{
				$operator = $this->gobjsessionsis->operatortype;
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/migspayment/operatortype/$operator/insertedId/$lastInsId/idcompany/$lintidcompany");
			}
			if($lastpayid['ModeofPayment']==7)
			{
			    $operator = $this->gobjsessionsis->operatortype;
				$this->_redirect( $this->baseUrl . "/registrations/registrationfailedcompany/ibfim/operatortype/$operator/insertedId/".$lastInsId."/idcompany/".$lintidcompany);
			}
			else if($lastpayid['ModeofPayment']!=2)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/companystudentdetails/companystudentlist/idcompany/".$lintidcompany);
			}
			
			else
			{
				$this->_redirect( $this->baseUrl . "/registrations/companyapplication/confirmpayment/insertedId/".$lastInsId."/idcompany/".$lintidcompany);
				exit;
			}
				$this->_redirect( $this->baseUrl . "/registrations/companystudentdetails/companystudentlist/idcompany/".$lintidcompany);
				exit;
			
			
		}
			
		}

		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {		
		   $this->_redirect( $this->baseUrl . '/registrations/registrationfailedcompany/addreregistration/idcompany/'.$lintidcompany);
		}
	
	}
	

	public function schedulestudentsAction(){	
			
		$this->view->lobjstudentForm = $this->lobjstudentForm;			
		if ($this->_getParam ('regpinnum')) {	
					
			$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
			$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );			
			$Idregpinnum = $this->_getParam ( 'regpinnum' );
			
			$idcompany = $this->view->idcompany = $this->_getParam ( 'idcompany' );
			$this->view->operatortype=$this->gobjsessionsis->operatortype;
			
			$larrstudentresult=$this->lobjTakafulmodel->fngetfailedregstered($Idregpinnum);	
						
			$this->view->larrresultdata=$larrstudentresult;
			
			// Function to check the mode of pay and blockin if program already selected
			$larrresultBatch = $this->lobjTakafulmodel->fngetBatchDetails ( $Idregpinnum );			
			$larrgetpaydetails=$this->lobjTakafulcandidatesmodel->fnGetModeofpay($larrresultBatch['idBatchRegistration']);		
			$this->view->mode0fpay = $larrgetpaydetails['ModeofPayment'];		
						
			if($larrgetpaydetails['ModeofPayment']==2){
						$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($larrresultBatch['idBatchRegistration']);			
						$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
						$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
						$this->view->progid=$larrgetprogramapplied['idProgram'];
			}else{
						$this->view->mode0fpay=2;
						$this->lobjstudentForm->Program->setValue($larrstudentresult[0]['idProgram']);
						$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
						$this->view->progid=$larrstudentresult[0]['idProgram'];
			}
			
		}
		
		$larrreappliedcandidates=$this->lobjTakafulmodel->fngetreappliedcandidates($Idregpinnum);
		$this->view->larrappliedresult = $larrreappliedcandidates;
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost ();
			
			$idcompany=$larrformData ['idcompany'];
			$larrformData ['Examvenue'] = $larrformData ['NewVenue'];
			$resultstate = $this->lobjstudentmodel->fngetstatecity ( $larrformData ['NewVenue'] );
			$larrformData ['ExamState'] = $resultstate ['state'];
			$larrformData ['ExamCity'] = $resultstate ['city'];
			$larrformData ['NewState'] = $resultstate ['state'];
			$larrformData ['NewCity'] = $resultstate ['city'];
			$larrformData ['hiddenscheduler'] = 1;
			
			if(strlen($larrformData['setmonth']) <=1) {
			$monthsss = '0'.$larrformData['setmonth'];
			}else{
				$monthsss = $larrformData['setmonth'];
			}
			if(strlen($larrformData['setdate']) <=1) {
				$dates = '0'.$larrformData['setdate'];
			}else{
				$dates = $larrformData['setdate'];
			}
			
			 $availdate=$larrformData['Year']."-".$monthsss."-".$dates;	
					
			$larravailseat=$this->lobjTakafulcandidatesmodel->fngetavailseatvenue($larrformData['NewVenue'],$availdate,$larrformData['idsession']);
			
			$larrformData['scheduler']=$larravailseat['idnewscheduler'];
			
			if(count($larrformData['studentId']) > $larravailseat['availseat']){
				echo '<script language="javascript">alert("The Noof Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
				echo "<script>parent.location = '".$this->view->baseUrl()."/registrations/registrationfailedcompany/schedulestudents/regpinnum/".$Idregpinnum."/idcompany/".$idcompany."';</script>";
				exit;
			}else{
			
			if (count ( $larrformData ['studentId'] ) > 0) {					
				$larrbatchregID = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationPinforexcel ( $Idregpinnum );
				$lintidbatch = $larrbatchregID ['idBatchRegistration'];
				$linttotnumofapplicant = $larrbatchregID ['totalNoofCandidates'];	
		 		 
				$larrinsertstudent = $this->lobjTakafulcandidatesmodel->fnUpdatestudapplication ( $larrformData, $lintidbatch, $linttotnumofapplicant,$Idregpinnum);
			
			} else {
				?>				
			<script>alert("Check any Applicant")</script>
		<?php
			}
			}
                         $this->gsessionbatch= new Zend_Session_Namespace('sis'); //Added on 04-02-2015
			$this->gsessionbatch->visistsschedulestudents = 0;	//Added on 04-02-2015
			//$this->_redirect ( $this->baseUrl . "/registrationfailedcompany/schedulestudents/regpinnum/$Idregpinnum" );
			$this->_redirect( $this->baseUrl . "/registrations/companystudentdetails/companystudentlist/idcompany/".$idcompany);
		}
	}
	
	public function getcandidatesAction(){
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		
		$regpin = $this->_getParam ( 'regpin' );  
		$status = $this->_getParam ( 'status' );
		$idprog = $this->_getParam ( 'idprog' );
		$idcompany = $this->_getParam ( 'idcompany' );
		
		$flag=$this->gobjsessionsis->operatortype;
				
		$larrgetallfailedstudentsearch=$this->lobjTakafulcandidatesmodel->fnSearchFailedStudentsCompany($regpin,$status,$idcompany,$flag);
		$this->view->numcand=count($larrgetallfailedstudentsearch);		
		echo Zend_Json_Encoder::encode($larrgetallfailedstudentsearch);
	}
	
	public function getnewprogramddAction(){
				
			$this->_helper->viewRenderer->setNoRender(true);
			$this->_helper->layout->disableLayout();
			$status = $this->_getParam ( 'status' );
			$idprog = $this->_getParam ( 'idprog' );	
			
			if($status==1){
				$larrbatchresultnew = $this->lobjTakafulcandidatesmodel->fnGetProgramNameNewreregistration ($idprog);	
			}else{
				$larrbatchresultnew = $this->lobjTakafulmodel->fnGetProgramName ();
			}		
			$larrgetprogramnewlist = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrbatchresultnew );
			echo Zend_Json_Encoder::encode ( $larrgetprogramnewlist );	
	}
	
	
		
	public function fngetregpinAction(){	

		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$idprog = $this->_getParam ( 'idprog' );
		$idcompany = $this->_getParam ( 'idcompany' );
		$flag=$this->gobjsessionsis->operatortype;
		$larrgetregpin=$this->lobjTakafulcandidatesmodel->fnGetregpin($idprog,$idcompany,$flag);
		
	    $arrcount=count($larrgetregpin);	    
		$larrgetregpin[$arrcount]['key']=0;
		$larrgetregpin[$arrcount]['value']="Others";
				
		$larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrgetregpin );
		echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );		
	}
	
	
	public function getprogramtotalAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idprogram = (int)$this->_getParam('idprogram');		
		$larrresult = $this->lobjcompanymodel->fnGetProgramFee($idprogram); 
		echo json_encode($larrresult);die();
		exit;
	}
	
	public function lookupbatchidAction(){		
		// Added on 05-09-2012
		$this->_helper->layout->disableLayout();
	}	
	
	public function refinebatchidAction(){
		// Added on 05-09-2012
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$strcandidatename = $this->_getParam('candidatename');
		$strcandidateicno = $this->_getParam('candidateicno');
		$lintprogramid = $this->_getParam('programid');
		$lintcompanyid = $this->_getParam('companyid');
		$lintstatus = $this->_getParam('status');
		
		$flag=$this->gobjsessionsis->operatortype;
		$larrgetregpin=$this->lobjTakafulcandidatesmodel->fnGetregpinrefined($lintprogramid,$lintcompanyid,$strcandidatename,$strcandidateicno,$lintstatus,$flag);
		echo Zend_Json_Encoder::encode($larrgetregpin);
		
	}
	
	public function getcandidatelookupAction(){		
		//Added on 07-09-2012
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$lintidapplication = $this->_getParam('idapplication');
		
		$newlookuparrays['key'][0]=0;
		$newlookuparray[0]['key']=0;
		$newlookuparray[0]['value']=0;
		
		$larrgetcandidatelookup=$this->lobjTakafulcandidatesmodel->fnGetcandidatelookup($lintidapplication);		
		$count=count($larrgetcandidatelookup);		
		for($lokiter=0;$lokiter<$count;$lokiter++){			
			if(!in_array($larrgetcandidatelookup[$lokiter]['key'],$newlookuparrays['key'])){
				
				$newlookuparrays['key'][$lokiter]=$larrgetcandidatelookup[$lokiter]['key'];
				$newlookuparray[$lokiter]['key']=$larrgetcandidatelookup[$lokiter]['key'];
				$newlookuparray[$lokiter]['value']=$larrgetcandidatelookup[$lokiter]['value'];
				
			}
		}	
		$larrgetcandidatelookup = $this->lobjCommon->fnResetArrayFromValuesToNames ( $newlookuparray );
		echo Zend_Json_Encoder::encode($larrgetcandidatelookup);		
	}
	
	public function getdiscountAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$nocandidates = (int)$this->_getParam('nocandidates');		
		$idprogram = (int)$this->_getParam('idprogram');
		$larrresultprog = $this->lobjcompanymodel->fngetprogramdiscount($idprogram,$nocandidates);
				if(!$larrresultprog){
					echo 0;
				}else{
					echo $larrresultprog[0]['Amount'];
				}
		die();
	}


public function fngetprerequesitionforregAction(){

			$this->view->lobjTakafulForm = $this->lobjTakafulForm;
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
$tabledate='';
		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		$idapplications=$this->_getParam('idapps');
		$lintidcompany=$this->_getParam('idcompany');
		
			$larrresult = $this->lobjcompanymodel->fntogetpaylater($lintidcompany);
		//
		if($larrresult)
		{
		   // echo "<pre>";
		    //print_r($larrresult);die();
		   $this->view->lobjTakafulForm->ModeofPayment->addMultiOption('7','Credit to IBFIM account');
		}
		
		// end of Credit to IBFIM account //
		
		$larrpaylater = $this->lobjTakafulmodel->fngetpaylater ( $lintidcompany );		
		$cntpaylater = count ( $larrpaylater );				
		if ($cntpaylater > 1) {
		       
				$this->view->lobjTakafulForm->ModeofPayment->addMultiOption ( $larrpaylater ['idDefinition'], $larrpaylater ['DefinitionDesc'] );
		}
				$this->view->lobjTakafulForm->ModeofPayment->setValue ( 181 );
		
		
		$larrprogid = $this->lobjstudentmodel->fnGetPreRequesition($lintidprog);
		$prerequest = $larrprogid['PreRequesition'];
		if($larrprogid['PreRequesition'] == 0)
		{
			die();
		}
		else 
		{
		  $larrprog = $this->lobjstudentmodel->fnGetPreRequesitionProgDetails($prerequest);
		  
		  
		  $larrgeticnos= $this->lobjTakafulcandidatesmodel->fngeticnosbyid($idapplications);
		  
		//  print_r($larrgeticnos);die();
		  
		  $arricnos=array();
		  for($mn=0;$mn<count($larrgeticnos);$mn++)
		  {
		  	if($mn==0)
		  	{
		  	$icno=$larrgeticnos[$mn]['ICNO'];
		  	
		  	$icno="'$icno'";
		  	
		  	}
		  	else 
		  	{
		  		$icnns=$larrgeticnos[$mn]['ICNO'];
		  		$icnns="'$icnns'";
		  		$icno.=','.$icnns;
		  	}
		  	$arricnos[]=$larrgeticnos[$mn]['ICNO'];
		  }
		  
		
		 $llaresutsicno=$this->lobjTakafulcandidatesmodel->fngetvalidateprgbyicno($prerequest,$icno);
		 
		 //print_r($llaresutsicno);
		 if(!empty($llaresutsicno))
		 {
		 $noticno='0';
		 	$larrresult = $this->lobjcompanymodel->fnGetProgramFee($lintidprog); 
		
		 $passicno=array();
		  for($mnc=0;$mnc<count($llaresutsicno);$mnc++)
		  {
		  	/*if(in_array($llaresutsicno[$mnc]['ICNO'],$arricnos))
		  	{
		  	$icnnq=$llaresutsicno[$mnc]['ICNO'];
		  		$icnnq="'$icnnq'";
		  		$noticno.=','.$icnnq;
		  	
		  	}
		  	*/
		  	$passicno[]=$llaresutsicno[$mnc]['ICNO'];
		  	
		  $key = array_search($llaresutsicno[$mnc]['ICNO'],$arricnos);
if($key!==false){
    unset($arricnos[$key]);
}		  

		  }
		  
		
		 $arricnoss =array_values($arricnos);
		 
		 
		   
		  for($mn=0;$mn<count($arricnoss);$mn++)
		  {
		  	if($mn==0)
		  	{
		  	$icno=$arricnoss[$mn];
		  	
		  	$noticno="'$icno'";
		  	
		  	}
		  	else 
		  	{
		  		$icnns=$arricnoss[$mn];
		  		$icnns="'$icnns'";
		  		$noticno.=','.$icnns;
		  	}
		  	
		  }
		   if(empty($arricnoss))
		   {
		   	 echo '1****Please bring along a copy of TBE Examination certificate for '.$larrprog['ProgramName'].' which must be certified as a true copy by the designated authorized person as agreed by IBFIM and MTA';
		     die();
		   }
		   else 
		   {
		   	
		   	  $tabledata='3****';
		   	   $tabledata.='<table class="table" width="100%"><tr><th><b>Candidates Name</b></th><th><b>ICNO</b></th><th><b>'.$larrprogid['ProgramName'].'</b></th>';
		   	$m=0;   
		   foreach($larrgeticnos as $lobjCountry)
		{
			
			    $tabledata.="<tr>";
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['Name'];
                $tabledata.="</td>";
				
			   	
			    $tabledata.="<td>";
				$tabledata.=$lobjCountry['ICNO'];
				$tabledata.="</td>";
				
				$tabledata.="<td>";
				if(in_array($lobjCountry['ICNO'],$passicno))
				{
				$tabledata.="<input type='checkbox' id='stuapp".$m."'  checked onclick='return false'; onkeydown='return false;' name='stuapp[]' value='".$lobjCountry['IDApplication']."' />";
				}
				else {
				$tabledata.="";
			
				}
				$tabledata.="</td>";
				
				
			$tabledata.="</tr>";
			$m++;
		}
		
		//$tabledata.='<tr><td>'.$noticno.'</td></tr>';
		// $tabledata.='<tr><td>'.$noticno.'</td></tr>';
		$eachamount=$larrresult['sum(abc.amount)'];
		$totalamount= $larrresult['sum(abc.amount)']*count($llaresutsicno);
		$tabledata.='<tr><td><b>Total No of Candidats : '.count($llaresutsicno).'<input type="hidden" id="nocandidate" value='.count($llaresutsicno).' name="nocandidate"  /><input type="hidden" id="eachamount" value='.$eachamount.' name="eachamount"  /><input type="hidden" id="companyid" value='.$lintidcompany.' name="companyid"  /><input type="hidden" id="idprg" value='.$lintidprog.' name="idprg"  /></b></td><td><b>Total Amount  :'.$totalamount.'<input type="hidden" id="totalamount" value='.$totalamount.' name="totalamount"  /><b></td></tr>';
		$tabledata.='<tr><td colspan="3"><b>Mode of Payment :'.$this->lobjTakafulForm->ModeofPayment.' </b></td></tr>';
		$tabledata.='<tr><td  align="center"><input type="submit" id="proceed" value="proceed" name="proceed" />&nbsp&nbsp&nbsp&nbsp<input type="button" id="button1" value="Cancel" lebel="Cancel" align="center"  onclick="closebox()" /></td><tr></tr></table>';
		  
		   	 echo $tabledata;
		   	 die();
		   }
		  
		 
		 
		 
		  // echo $noticno;
		 }
		 //print_r($arricnoss);
		 //echo "abc";
		
		 //die();
		 
		//  $larrrappliedpaid=$this->lobjstudentmodel->fncheckstatuspaid($program,$icno);
		  
		  if(empty($llaresutsicno))
		  {
		  	
		  	echo '2****You can only register for '.$larrprogid['ProgramName'].' once all the above candidates  have to  passed '.$larrprog['ProgramName'].'';

		  	//echo $tabledate;
		  	die();
		  	// echo '1****Please bring along a copy of TBE Examination certificate for '.$larrprog['ProgramName'].' which must be certified as a true copy by the designated authorized person as agreed by IBFIM and MTA';
		  //die();
		  	
		  }
		 
		  
		 
		  
		 // die();
		}
		//die();
	}
	
	

}