<?php
class Registrations_RegistrationfailedtakafulController extends Zend_Controller_Action { //Controller for the User Module
	   
	public function init() { //initialization function		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    //$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object		
   	    
		$this->lobjTakafulmodel = new App_Model_Takafulapplication();
		$this->lobjCommon = new App_Model_Common ();		
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //user model object			
		$this->lobjTakafulForm = new App_Form_Takafulapplication (); //intialize user lobjuserForm			
		$this->lobjstudentForm = new App_Form_Studentapplication ();
		$this->lobjstudentmodel = new App_Model_Studentapplication ();
		$this->lobjcompanymodel = new App_Model_Companyapplication();
		$this->lobjCompanypayment = new Finance_Model_DbTable_Approvecreditstudenttakaful ();			
		$this->lobjsearchForm = new App_Form_Search (); //intialize Search lobjsearchForm
		$this->lobjcompanymaster = new App_Model_Companymaster(); //Company student details model object
		$this->lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //Company student details model object
	//	$this->lobjstudentmodel = new App_Model_Companyapplication();
	 
		$this->registry = Zend_Registry::getInstance ();
		$this->locale = $this->registry->get ( 'Zend_Locale' );
		$auth = Zend_Auth::getInstance();
		if($auth->getIdentity()->iduser == 17)
		{
		   $this->_helper->layout()->setLayout('/web/usty');
		}
	}
		
	
	public function indexAction(){
		
		$lobjform=$this->view->lobjform = $this->lobjsearchForm; //send the lobjuserForm object to the view
		$larrrbusinesstypelist = $this->lobjcompanymaster->fnGetBusinesstypeList (); //get Business type list details
		$lobjform->field5->addMultioptions($larrrbusinesstypelist);
		$lobjform->field1->setAttrib('OnChange', 'fnGetCityList');
		$lobjform->field8 ->setRegisterInArrayValidator(false);
		$larrresult = $this->lobjcompanystudentdetails->fnGenerateQueries(8,1); //get Company details
	
		if(!$this->_getParam('search')) {
			$larrresult=array();
			unset($this->gobjsessionsis->companystudentdetailspaginatorresultTA);
		}
						
		$lintpagecount = $this->gintPageCount=10;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
        		
		if(isset($this->gobjsessionsis->companystudentdetailspaginatorresultTA)) {
			
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->companystudentdetailspaginatorresultTA,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjcompanystudentdetails->fnSearchTakaful($lobjform->getValues ()); //searching the values for the Companies
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->companystudentdetailspaginatorresultTA = $larrresult;				
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
		 	$this->_redirect( $this->baseUrl . '/registrations/registrationfailedtakaful/index');
		}
	
	}
	
	
	public function addreregistrationAction() {// Action for Search
			
		$this->view->lobjstudentForm = $this->lobjstudentForm;		
		$this->view->lobjTakafulForm = $this->lobjTakafulForm; //intialize user lobjuserForm
			
		$larrbatchresult = $this->lobjTakafulmodel->fnGetProgramName ();
		$this->view->lobjTakafulForm->idPrograms->addMultiOptions ( $larrbatchresult );		
		$this->view->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		
		$this->view->lobjstudentForm->Program->setAttrib('style','width:165px;');  
		
		$lintidtakaful = $this->_getParam('idtakaful');
		
		$this->view->idtakaful=$lintidtakaful; 
	
			/*if(!$this->_getParam('search')) 
				unset($this->gobjsessionsis->companypaginator);			
				$lintpagecount = $this->gintPageCount;		
				$lintpage = $this->_getParam('page',1); //Paginator instance
			*/
        		
		$larrresultdiscount = $this->lobjTakafulmodel->fngetintialdiscount ();
		$this->view->discount = $larrresultdiscount['discount'];		
		
		$larrpaylater = $this->lobjTakafulmodel->fngetpaylater ( $lintidtakaful );		
		$cntpaylater = count ( $larrpaylater );				
		if ($cntpaylater > 1) {
				$this->view->lobjTakafulForm->ModeofPayment->addMultiOption ( $larrpaylater ['idDefinition'], $larrpaylater ['DefinitionDesc'] );
		}
				$this->view->lobjTakafulForm->ModeofPayment->setValue ( 181 );
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost ();				
			if ($this->view->lobjTakafulForm->isValid ( $larrformData )) {				
				
			
							$larrformData ['UpdUser'] = 1;
							$larrformData ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
							
							$lastInsId = $this->lobjTakafulmodel->fnInsertPaymentdetails ( $larrformData ); //insert into batch reg and getting that idbatchregistration
							$lastpaymentid = $this->lobjTakafulmodel->fnInsertStudentPaymentdetails ( $larrformData ['ModeofPayment'], $lastInsId );
							
							
							$lastpayid = $this->lobjTakafulmodel->fngetmodeofpayment ( $lastpaymentid );
							
			$flag=1;// If function called from re-registration module
			if ($larrformData ['ModeofPayment'] == 181) {
				$flag=0;
			}							
								$larrformDataapp ['IDApplication'] = $lastInsId;
								$larrformDataapp ['companyflag'] = 1;
								$larrformDataapp ['Amount'] = $larrformData ['grossAmt'];
								$larrformDataapp ['UpdUser'] = 1;
								$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
								
							$regid = $this->lobjCompanypayment->fngeneraterandom ();								
							$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);	
													
							$this->lobjTakafulmodel->fninsertintostudentapplication($larrformData,$lastInsId,$regid,$lintidtakaful,2);				
			
			if($lastpayid['ModeofPayment']==4)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/takafulapplication/display/insertedid/".$lastInsId."/idtakaful/".$lintidtakaful); 
			}
			if($lastpayid['ModeofPayment']==1)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/takafulapplication/fpxpageone/insertedId/".$lastInsId."/idtakaful/".$lintidtakaful); 
			}
			else if($lastpayid['ModeofPayment']!=2)
			{   
				$this->_redirect( $this->baseUrl . "/registrations/takafulstudentdetails/takafulstudentlist/idtakaful/".$lintidtakaful);
			}
			else
			{
				$this->_redirect( $this->baseUrl . "/registrations/takafulapplication/confirmpayment/insertedId/".$lastInsId."/idtakaful/".$lintidtakaful);
				exit;
			}
				$this->_redirect( $this->baseUrl . "/registrations/takafulstudentdetails/takafulstudentlist/idtakaful/".$lintidtakaful);
				exit;
			}	
		}		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {		
			
			$this->_redirect( $this->baseUrl . '/registrations/registrationfailedtakaful/addreregistration/idtakaful/'.$lintidtakaful);
		}
	
	}
	
	

	public function schedulestudentsAction(){
		
		$this->view->lobjstudentForm = $this->lobjstudentForm;
			
		if ($this->_getParam ( 'regpinnum' )) {	
					
			$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
			$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );			
			$Idregpinnum = $this->_getParam ( 'regpinnum' );			
			$idcompany = $this->view->idcompany = $this->_getParam ( 'idcompany' );							
			$larrstudentresult=$this->lobjTakafulmodel->fngetfailedregstered($Idregpinnum);			
			$this->view->larrresultdata=$larrstudentresult;
			// Function to check the mode of pay and blockin if program already selected
			$larrresultBatch = $this->lobjTakafulmodel->fngetBatchDetails ( $Idregpinnum );			
			$larrgetpaydetails=$this->lobjTakafulcandidatesmodel->fnGetModeofpay($larrresultBatch['idBatchRegistration']);		
			$this->view->mode0fpay=$larrgetpaydetails['ModeofPayment'];		
						
			if($larrgetpaydetails['ModeofPayment']==2){
						$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($larrresultBatch['idBatchRegistration']);			
						$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
						$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
						$this->view->progid=$larrgetprogramapplied['idProgram'];
			}else{
						$this->view->mode0fpay=2;
						$this->lobjstudentForm->Program->setValue($larrstudentresult[0]['idProgram']);
						$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
						$this->view->progid=$larrstudentresult[0]['idProgram'];
			}
		}
		
		$larrreappliedcandidates=$this->lobjTakafulmodel->fngetreappliedcandidates($Idregpinnum);
		$this->view->larrappliedresult = $larrreappliedcandidates;
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost ();
			$idcompany=$larrformData ['idcompany'];
			$larrformData ['Examvenue'] = $larrformData ['NewVenue'];
			$resultstate = $this->lobjstudentmodel->fngetstatecity ( $larrformData ['NewVenue'] );
			$larrformData ['ExamState'] = $resultstate ['state'];
			$larrformData ['ExamCity'] = $resultstate ['city'];
			$larrformData ['NewState'] = $resultstate ['state'];
			$larrformData ['NewCity'] = $resultstate ['city'];
			$larrformData ['hiddenscheduler'] = 1;
			
			
			if(strlen($larrformData['setmonth']) <=1) {
				$monthsss = '0'.$larrformData['setmonth'];
			}else{
				$monthsss = $larrformData['setmonth'];
			}
			if(strlen($larrformData['setdate']) <=1) {
				$dates = '0'.$larrformData['setdate'];
			}else{
				$dates = $larrformData['setdate'];
			}
		
			
			$availdate=$larrformData['Year']."-".$monthsss ."-".$dates;			
			$larravailseat=$this->lobjTakafulcandidatesmodel->fngetavailseatvenue($larrformData['NewVenue'],$availdate,$larrformData['idsession']);
			$larrformData['scheduler']=$larravailseat['idnewscheduler'];
			
			if(count($larrformData['studentId']) > $larravailseat['availseat']){
				echo '<script language="javascript">alert("The Noof Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
				echo "<script>parent.location = '".$this->view->baseUrl()."/takafulcandidates/coursevenue/batchId/".$ids."';</script>";
				exit;
			}else{
			
			if (count ( $larrformData ['studentId'] ) > 0) {					
				$larrbatchregID = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationPinforexcel ( $Idregpinnum );
				$lintidbatch = $larrbatchregID ['idBatchRegistration'];
				$linttotnumofapplicant = $larrbatchregID ['totalNoofCandidates'];					
				$larrinsertstudent = $this->lobjTakafulcandidatesmodel->fnUpdatestudapplication ( $larrformData, $lintidbatch, $linttotnumofapplicant,$Idregpinnum);
			
			} else {
				?>				
			<script>alert("Check any Applicant")</script>
		<?php
			}
			}
			//$this->_redirect ( $this->baseUrl . "/registrationfailedcompany/schedulestudents/regpinnum/$Idregpinnum" );
			$this->_redirect( $this->baseUrl . "/registrations/takafulstudentdetails/takafulstudentlist/idtakaful/".$idcompany);
		}
	}
	
	public function getcandidatesAction(){		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();		
		$regpin = $this->_getParam ( 'regpin' );  
		$status = $this->_getParam ( 'status' );
		$idprog = $this->_getParam ( 'idprog' );
		$idcompany=$this->_getParam ( 'idcompany' );			
		$larrgetallfailedstudentsearch=$this->lobjTakafulcandidatesmodel->fnSearchFailedStudentsCompany($regpin,$status,$idcompany,2);
		$this->view->numcand=count($larrgetallfailedstudentsearch);		
		echo Zend_Json_Encoder::encode($larrgetallfailedstudentsearch);
	}
	
	public function getnewprogramddAction(){		
		
			$this->_helper->viewRenderer->setNoRender(true);
			$this->_helper->layout->disableLayout();
			$status = $this->_getParam ( 'status' );
			$idprog = $this->_getParam ( 'idprog' );	
			
			if($status==1){
				$larrbatchresultnew = $this->lobjTakafulcandidatesmodel->fnGetProgramNameNewreregistration ($idprog);	
			}else{
				$larrbatchresultnew = $this->lobjTakafulmodel->fnGetProgramName ();
			}		
			$larrgetprogramnewlist = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrbatchresultnew );
			echo Zend_Json_Encoder::encode ( $larrgetprogramnewlist );	
	}
	
	
		
	public function fngetregpinAction(){	

		 $this->_helper->viewRenderer->setNoRender(true);
		 $this->_helper->layout->disableLayout();
		 $idprog = $this->_getParam ( 'idprog' );
	 	 $idcompany = $this->_getParam ( 'idcompany' );	
	 	 
		$flag=2;		
		$larrgetregpin=$this->lobjTakafulcandidatesmodel->fnGetregpin($idprog,$idcompany,$flag);
		
		$arrcount=count($larrgetregpin);	    
		$larrgetregpin[$arrcount]['key']=0;
		$larrgetregpin[$arrcount]['value']="Others";
		
		$larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrgetregpin );
		echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );		
	}
	
	
	public function getprogramtotalAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idprogram = (int)$this->_getParam('idprogram');		
		$larrresult = $this->lobjcompanymodel->fnGetProgramFee($idprogram); 
		echo $larrresult['sum(abc.amount)'];
		exit;
	}
	
	
	public function lookupbatchidAction(){		
		// Added on 05-09-2012
		$this->_helper->layout->disableLayout();
	}	
	
	public function refinebatchidAction(){
		// Added on 05-09-2012
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$strcandidatename = $this->_getParam('candidatename');
		$strcandidateicno = $this->_getParam('candidateicno');
		$lintprogramid = $this->_getParam('programid');
		$lintcompanyid = $this->_getParam('companyid');
		$lintstatus = $this->_getParam('status');
		
		$larrgetregpin=$this->lobjTakafulcandidatesmodel->fnGetregpinrefined($lintprogramid,$lintcompanyid,$strcandidatename,$strcandidateicno,$lintstatus,2);
		echo Zend_Json_Encoder::encode($larrgetregpin);
	}
	
	public function getcandidatelookupAction(){		
		//Added on 07-09-2012
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$lintidapplication = $this->_getParam('idapplication');
		
		$newlookuparrays['key'][0]=0;
		$newlookuparray[0]['key']=0;
		$newlookuparray[0]['value']=0;
		
		$larrgetcandidatelookup=$this->lobjTakafulcandidatesmodel->fnGetcandidatelookup($lintidapplication);
		$count=count($larrgetcandidatelookup);
		
		for($lokiter=0;$lokiter<$count;$lokiter++){			
			if(!in_array($larrgetcandidatelookup[$lokiter]['key'],$newlookuparrays['key'])){
				$newlookuparrays['key'][$lokiter]=$larrgetcandidatelookup[$lokiter]['key'];
				$newlookuparray[$lokiter]['key']=$larrgetcandidatelookup[$lokiter]['key'];
				$newlookuparray[$lokiter]['value']=$larrgetcandidatelookup[$lokiter]['value'];
			}
		}
	
		$larrgetcandidatelookup = $this->lobjCommon->fnResetArrayFromValuesToNames ( $newlookuparray );
		echo Zend_Json_Encoder::encode($larrgetcandidatelookup);		
	}
	
	
	public function getdiscountAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$nocandidates = (int)$this->_getParam('nocandidates');		
		$idprogram = (int)$this->_getParam('idprogram');
		$larrresultprog = $this->lobjcompanymodel->fngetprogramdiscount($idprogram,$nocandidates);
				if(!$larrresultprog){
					echo 0;
				}else{
					echo $larrresultprog[0]['Amount'];
				}
		die();
	}
	

	
}