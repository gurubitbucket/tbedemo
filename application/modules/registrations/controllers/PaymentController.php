<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
class Registrations_PaymentController extends Base_Base 
{

    public function init()
    {
        $this->fnsetObj();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
    }
    public function fnsetObj()
    {
		$this->lobjPaymentmodel = new Registrations_Model_DbTable_Paymentmodel();
		$this->lobjpaymentForm = new Registrations_Form_Paymentform ();  	
	}
    public function indexAction()
    {
       	$this->view->title="Pyament Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$this->view->lobjpaymentForm = $this->lobjpaymentForm;
		$larrresult = $this->lobjPaymentmodel->fnGetStudentpaymentDetails(); //get user details
		/*echo('<pre>');
		print_r($larrresult);die();*/	
		$this->view->countcomp = count($larrresult);	
		$lintpagecount =10000000;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		if(isset($this->gobjsessionstudent->semesterpaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionstudent->semesterpaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		
		
    if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjPaymentmodel->fngetpaymentSearch($larrformData); //searching the values for the user
				/*echo('<pre>');
		         print_r($larrresult);die();*/
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->semesterpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/registrations/payment/index');			
		}
		
    }
    
   

}

