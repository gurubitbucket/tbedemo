<?php
class Scheduler_NewschedulerController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	private $_gobjlogger;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
	
	public function fnsetObj()
	{
		$this->lobjschedulermodel = new Scheduler_Model_DbTable_Newscheduler();
		$this->lobjschedulermodelForm = new Scheduler_Form_Newscheduler(); 
	  	//$this->lobjcoursemaster = new Scheduler_Model_DbTable_Coursemaster();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	public function indexAction() {
    	$this->view->title="Manage Scheduler";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjschedulermodel->fnGetScheduler(); 
	//	print_r($larrresult);die();	
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->newschedulerresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->newschedulerresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->newschedulerresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				
				$larrresult = $this->lobjschedulermodel->fnSearchSession( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->newschedulerresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/scheduler/newscheduler/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function addschedulerAction() { //title
		$this->view->lobjprogramForm = $this->lobjschedulermodelForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjprogramForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjprogramForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		$larrresult = $this->lobjschedulermodel->fngetCourse();
		$this->view->courses=$larrresult;
		
		//$year= date("Y"); 
		//echo $year;die();
		//$larrresult2 = $this->lobjschedulermodel->fngetSateDetails();
		//$this->lobjschedulermodelForm->idstate->addMultiOptions($larrresult2);
		
		//$larrresult22 = $this->lobjschedulermodel->fngetActiveset();
		//print_r($larrresult22);die();
		//if($larrresult22)
		//$this->view->activeset=$larrresult22;
		
		$larrresultdays = $this->lobjschedulermodel->fngetDaysDetails();
		$this->view->days=$larrresultdays;
		
		
		$larrresults = $this->lobjschedulermodel->fngetSessionDetails();
		$this->view->mansession=$larrresults;
		
		$larrresult2 = $this->lobjschedulermodel->fnfetchvenue();
		$this->view->centerarray=$larrresult2;
		//print_r($larrresult2);die();
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
		/*	echo "<pre>";
			print_r($formData);
			die();*/
				
				$larrresult = $this->lobjschedulermodel->fnAddScheduler($formData);
				$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Scheduler Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/scheduler/newscheduler/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'program', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
    
public function editschedulerAction() { //title

		$lintidscheduler= $this->_getParam('id');
		$this->view->lobjprogramForm = $this->lobjschedulermodelForm;		
		/////////////function to fetch all the details/////////////////
		$larrresultdays = $this->lobjschedulermodel->fngetDaysDetails();
		$this->view->days=$larrresultdays;
		
         $larrresult = $this->lobjschedulermodel->fngetCourse();
		 $this->view->courses=$larrresult;
		 
		 $larrresult22 = $this->lobjschedulermodel->fnGetAllMonthList();
		$this->view->lobjprogramForm->From->addMultiOptions($larrresult22);
		$this->view->lobjprogramForm->To->addMultiOptions($larrresult22);
		
		//$this->view->activeset=$larrresult22;
		
		 $larrresults = $this->lobjschedulermodel->fngetSessionDetails();
		 $this->view->mansession=$larrresults;
		
		 $larrresult2 = $this->lobjschedulermodel->fnfetchvenue();
		 $this->view->centerarray=$larrresult2;
		/////////////////////////////end of fetch all details//////////////////
		
		$larrresultscheduler = $this->lobjschedulermodel->fnGetSchedulerDetails($lintidscheduler); 
		
			$this->view->lobjprogramForm->From->setValue($larrresultscheduler['From']);
			$this->view->lobjprogramForm->To->setValue($larrresultscheduler['To']);
				$this->view->lobjprogramForm->From->setAttrib('readonly','readonly');
				$this->view->lobjprogramForm->To->setAttrib('readonly','readonly'); 
				$this->view->lobjprogramForm->Year->setAttrib('readonly','readonly'); 
				$this->view->lobjprogramForm->description->setValue($larrresultscheduler['Description']); 
				//$this->view->lobjprogramForm->description->setAttrib('readonly','readonly'); 
				$this->view->lobjprogramForm->Active->setValue($larrresultscheduler['Active']); 
	//print_r($larrresultscheduler);die();
		//$this->view->lobjprogramForm->From->addMultiOption($this->lobjschedulermodel->fnGetfromList($larrresultscheduler['From']));
		//$this->view->lobjprogramForm->To->addMultiOption($this->lobjschedulermodel->fnGettoList($larrresultscheduler['To']));
		$this->view->lobjprogramForm->Year->setValue($larrresultscheduler['Year']);
		
		/////////////////FUNCTION TO FETCH FOR THE SCHEDULER DAYS/////////////
		$larrresultschedulerdays = $this->lobjschedulermodel->fnGetSchedulerDays($lintidscheduler); 
		/*print_r($larrresultschedulerdays);
		die();*/
		 $arrschedulerdays = array();
		foreach ($larrresultschedulerdays as $arrschedulerdays)
		{
			$arrIDschedulerdays[] = $arrschedulerdays['Days'];
		}
		/*print_r($arrIDschedulerdays);
		die();*/
		$this->view->schedulerdays = $arrIDschedulerdays;
		//////////////////////////////////////////////////////////////////
	
		////////////function tofetch the scheduler session and view////////////////
		$larrresultschedulersession = $this->lobjschedulermodel->fnGetSchedulerSession($lintidscheduler);
	    $arrschedulersession = array();
		foreach ($larrresultschedulersession as $arrschedulersession)
		{
			$arrIdarrschedulersession[] = $arrschedulersession['idmanagesession'];
		}
		$this->view->schedulersession = $arrIdarrschedulersession;
		//////////////////end of scheduler session///////////////////////////////
		

		////////////function tofetch the scheduler session and view////////////////
		$larrresultschedulercourse = $this->lobjschedulermodel->fnGetSchedulerCourse($lintidscheduler);
	    $arrschedulercourse = array();
		foreach ($larrresultschedulercourse as $arrschedulercourse)
		{
			$larrresultschedulercourse[] = $arrschedulercourse['IdProgramMaster'];
		}
		$this->view->schedulercourse = $larrresultschedulercourse;
		//////////////////end of scheduler session///////////////////////////////
		
		
		////////////function tofetch the scheduler venue and view////////////////
		$larrresultschedulervenue = $this->lobjschedulermodel->fnGetSchedulerVenue($lintidscheduler);
	    $arrschedulervenue = array();
		foreach ($larrresultschedulervenue as $arrschedulervenue)
		{
			$larrresultschedulervenue[] = $arrschedulervenue['idvenue'];
		}
		$this->view->schedulervenue = $larrresultschedulervenue;
		//////////////////end of scheduler session///////////////////////////////
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
		/*	echo "<pre>";
			echo $lintidscheduler;
			print_r($formData);
			die();*/
			
				$checkscheduler=$this->lobjschedulermodel->fncheckidsechduler($lintidscheduler);
				if($checkscheduler)
				{
					
				}
				else 
				{
				$larrresult = $this->lobjschedulermodel->fninsertintoSchedulerdatevenue($formData,$lintidscheduler);
				}
				
				$larrresult = $this->lobjschedulermodel->fnupdateScheduler($lintidscheduler,$formData);
				$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			//$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the New Scheduler Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the New Scheduler with Id = ".$lintidscheduler."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/scheduler/newscheduler/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'program', 'action'=>'index'),'default',true));	//redirect	
          
    }
		}
		
	public function getmonthlistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idyear');//Get Country Id
		$year= date("Y"); 
		if($lintIdCountry==$year)
		{
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjschedulermodel->fnGetMonthList());
		}
		else 
		{
			$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjschedulermodel->fnGetAllMonthList());
		}
		
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}	
	public function gettomonthlistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idmonth');//Get Country Id
			$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjschedulermodel->fnGetToMonthList($lintIdCountry));
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}	
		
}