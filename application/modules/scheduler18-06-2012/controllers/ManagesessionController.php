<?php
class Scheduler_ManagesessionController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	private $_gobjlogger;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
	
	public function fnsetObj(){
		$this->lobjprogram = new Scheduler_Model_DbTable_Managesession();
		$this->lobjprogramForm = new Scheduler_Form_Managesession(); 
	  	//$this->lobjcoursemaster = new Scheduler_Model_DbTable_Coursemaster();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
		
	}
	
	public function indexAction() {
    	$this->view->title="Program Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjprogram->fngetSessionDetails(); 	
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->managesessionpaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->managesessionpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->managesessionpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjprogram->fnSearchSession( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->managesessionpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/scheduler/managesession/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function newsessionAction() { //title
		$this->view->lobjprogramForm = $this->lobjprogramForm;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjprogramForm->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjprogramForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
				unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$fromtimearray = $formData['starttime'];
			 	$pieces = explode("T", $fromtimearray);
			 	$fromtime = $pieces[1];
			 	$fromtimearray = $formData['endtime'];
			 	$pieces1 = explode("T", $fromtimearray);
			 	$totime = $pieces1[1];
				//print_r($formData);die();
				$larrresult = $this->lobjprogram->fnaddSession($formData,$fromtime,$totime);
				$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Session Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/scheduler/managesession/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'program', 'action'=>'index'),'default',true));	//redirect	
        }     
    }
    
	public function editsessionAction(){
		$this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjprogramForm->UpdDate->setValue ( $ldtsystemDate );		
    	$IdProgram = $this->_getParam('id');
    	//echo $IdProgram;die();
    	$this->view->idmangesession=$IdProgram;
    	$result=$this->lobjprogram->fnGetsessionList($IdProgram);
    	//print_r($result);die();
    	$this->view->starttime= "Sat Nov 12 2011".' '.$result['starttime'];
    	$this->view->endtime= "Sat Nov 12 2011".' '.$result['endtime'];
 
    	$this->lobjprogramForm->populate($result);	
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjprogramForm->isValid($formData)) {
	    		unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				$fromtimearray = $formData['starttime'];
			 	$pieces = explode("T", $fromtimearray);
			 	$fromtime = $pieces[1];
			 	$fromtimearray = $formData['endtime'];
			 	$pieces1 = explode("T", $fromtimearray);
			 	$totime = $pieces1[1];
	   			$lintIdProgram = $formData ['idmangesession'];
	   		/*	echo "<pre />";
	   			echo $fromtime;
	   			echo $totime;
	   			echo $lintIdProgram;
	   			print_r($formData);die();*/
	   			
				$larrresults = $this->lobjprogram->fnupdateSession($formData,$fromtime,$totime);//update university
				$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			//$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the New Session Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the New Session with Id = ".$IdProgram."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/scheduler/managesession/index');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'program', 'action'=>'index'),'default',true));
			}
    	}
    }
}