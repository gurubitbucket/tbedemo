<?php
class Scheduler_SessionconstraintsController extends Base_Base {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjcoursemaster;
	private $lobjdeftype;
	private $_gobjlogger;
	
	public function init() {
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
	
	public function fnsetObj(){
		$this->lobjprogram = new Scheduler_Model_DbTable_Sessionconstraints();
		$this->lobjprogramForm = new Scheduler_Form_Sessionconstraints(); 
	  	
	}
	
	public function indexAction() {
    	$this->view->title="Session Constraints";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		$larrresult = $this->lobjprogram->fngetSessionDetails(); 	
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->sessionconpaginatorresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->sessionconpaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->sessionconpaginatorresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				$larrresult = $this->lobjprogram->fnSearchSession( $this->lobjform->getValues () ); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->sessionconpaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	}
	
public function newsessionconstraintsAction() {
		$this->view->lobjprogramForm = $this->lobjprogramForm;
		$larrVenueresult = $this->lobjprogram->fngetVenueDetails();
		$this->view->larrVenueresult=$larrVenueresult;
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			unset ( $formData ['Save'] );
			unset ( $formData ['Back'] );
			$larrresult = $this->lobjprogram->fnaddSessionconstrains($formData);
			$auth = Zend_Auth::getInstance();
			// Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the New Sessionconstraints Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
			$this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
		}
	}
    
	public function editsessionconstraintsAction(){
		$this->view->lobjprogramForm = $this->lobjprogramForm; //send the lobjuniversityForm object to the view
			
    	$IdProgram = $this->_getParam('id');
    	$this->view->lobjprogramForm->idschedulerconstraints->setValue($IdProgram);
    	$larrresults = $this->lobjprogram->fngetDaysDetails();
		
		$larrresult2 = $this->lobjprogram->fngetSateDetails();
		$this->lobjprogramForm->idstate->addMultiOptions($larrresult2);
		
		
		
       	
    	$result=$this->lobjprogram->fnGetsessionList($IdProgram);
    	//print_r($result);die();
    	//$this->lobjprogramForm->idcity->addMultiOptions(array('0'=>'others'));
    	$this->lobjprogramForm->idcity->addMultiOptions($this->lobjprogram->fnGetcityList($result['state']));
        $this->lobjprogramForm->idvenue->addMultiOptions($this->lobjprogram->fnGetVenueList($result['city']));
        
    	
    	$this->view->lobjprogramForm->constraintname->setValue($result['constraintname']);
    	$this->view->lobjprogramForm->idstate->setValue($result['state']);
    	$this->view->lobjprogramForm->idcity->setValue($result['city']);
    	$this->view->lobjprogramForm->idvenue->setValue($result['idstate']);
    	$this->view->lobjprogramForm->Date->setValue($result['Date']);
    	$this->view->lobjprogramForm->Active->setValue($result['Active']);
    	if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
	    	if ($this->lobjprogramForm->isValid($formData)) {
	    		unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				
	   			$lintIdProgram = $formData['idschedulerconstraints'];
	   			/*print_r($formData);die();*/
				$larrresults = $this->lobjprogram->fnupdateSession($formData);//update university
				$auth = Zend_Auth::getInstance();
    	    // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			//$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the New Sessionconstraints Details"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Updated the New Sessionconstraints with Id = ".$IdProgram."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
				 $this->_redirect( $this->baseUrl . '/scheduler/sessionconstraints/index');
			}
    	}
    }
    public function getvenuelistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		//get all the city that correspond to the selected state
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjprogram->fnGetVenueList($lintIdCountry));
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}			
}