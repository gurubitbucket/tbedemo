<?php 
class Scheduler_Model_DbTable_Managesession extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_program';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetSessionDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_managesession"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
    
    public function fnaddSession($formData,$fromtime,$totime) { //Function for adding the University details to the table
		$db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_managesession";
            $postData = array(		
							'managesessionname' => $formData['managesessionname'],	
           					'starttime' =>$fromtime,	
           					'endtime' => $totime,           				                           
                            'UpdDate' => $formData['UpdDate'],	
            				'UpdUser' =>$formData['UpdUser'],
            				'active' =>$formData['active'],			
						);	
					
	     $db->insert($table,$postData);
			
			//self::updateactive();
	}
    
    public function fnupdateSession($formData,$fromtime,$totime) { //Function for updating the university
    		 $db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$data = array('managesessionname' => $formData['managesessionname'],
		              'starttime'=>$fromtime,
		              'endtime'=> $totime,
		              'UpdDate'=>$formData['UpdDate'],
		              'UpdUser' =>$formData['UpdUser'],
            		'active' =>$formData['active'],	
		 );
		$where['idmangesession = ? ']= $formData['idmangesession'];		
		return $db->update('tbl_managesession', $data, $where);
    }
    
	public function fnSearchSession($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_managesession'),array('a.*'))
			   ->where('a.managesessionname  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	public function fnGetsessionList($IdProgram){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_managesession"),array("a.*"))
				 				 ->where("a.idmangesession = ?",$IdProgram);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	

}
?>