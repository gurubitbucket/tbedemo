<?php 
class Scheduler_Model_DbTable_Newscheduler extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_program';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
	public function fnSearchSession($post = array())
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$field7 = "a.Active= ".$post["field7"];
		$select = $db->select() 	
			   ->from(array('a' => 'tbl_newscheduler'),array('a.*'))
			    ->join(array("b"=>"tbl_newmonths"),"a.From=b.idmonth",array("b.MonthName as From"))
										  ->join(array("c"=>"tbl_newmonths"),"a.To=c.idmonth",array("c.MonthName as To"))
			   ->where('a.Year like "%" ? "%"',$post['field3'])			  
			   ->where($field7);
		$result = $db->fetchAll($select);
		return $result;
	}
     public function fngetCourse() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programmaster"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     public function fngetActiveset()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_tosmaster"),array())
								 ->join(array("b"=>"tbl_batchmaster"),'b.IdBatch=a.IdBatch',array('b.BatchName'))
								 ->where("a.Active=1");							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     	
     }
     public function fngetSessionDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_managesession"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
	public function fnfetchvenue()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("a.*"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
	}
   public function fnAddScheduler($larrformdata)
   {
  /* 	echo "<pre/>";
   	print_r($larrformdata);
   	die()*/
	      $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_newscheduler";
          $postData = array(	
                             'From'=>$larrformdata['From'],
          					 'To'=>$larrformdata['To'],
          					 'Year'=>$larrformdata['Year'],
            				 'UpdUser' =>$larrformdata['UpdUser'],		
		            		 'UpdDate' =>$larrformdata['UpdDate'],
          					 'Active'=>1												
						);			
	       $db->insert($table,$postData);
	       $lastid  = $db->lastInsertId("tbl_newscheduler","idnewscheduler");	
	       
	       /////////////////for weekdays////////////////////////////
	        for($i=0;$i<count($larrformdata['iddays']);$i++)
	        {
	          	 $tables = "tbl_newschedulerdays";
	       	     $arryresult['idnewscheduler']= $lastid;
	       	     $arryresult['Days'] = $larrformdata['iddays'][$i];
	       	     $db->insert($tables,$arryresult);
	        }
	       ///////////////////end for weekdays///////////////////
	       
	         /////////////////for course////////////////////////////
	        for($i=0;$i<count($larrformdata['idprogram']);$i++)
	        {
	          	 $tablescourse = "tbl_newschedulercourse";
	       	     $arryresultcourse['idnewscheduler']= $lastid;
	       	     $arryresultcourse['IdProgramMaster'] = $larrformdata['idprogram'][$i];
	       	     $db->insert($tablescourse,$arryresultcourse);
	        }
	       ///////////////////end for course///////////////////
	       
	         /////////////////for venue session////////////////////////////
	        for($i=0;$i<count($larrformdata['idsession']);$i++)
	        {
	          	 $tablessession = "tbl_newschedulersession";
	       	     $arryresultsession['idnewscheduler']= $lastid;
	       	     $arryresultsession['idmanagesession'] = $larrformdata['idsession'][$i];
	       	     $db->insert($tablessession,$arryresultsession);
	        }
	       ///////////////////end for venue session///////////////////
	       
	         /////////////////for venue////////////////////////////
	        for($i=0;$i<count($larrformdata['idcenter']);$i++)
	        {
	          	 $tablescenter = "tbl_newschedulervenue";
	       	     $arryresultcenter['idnewscheduler']= $lastid;
	       	     $arryresultcenter['idvenue'] = $larrformdata['idcenter'][$i];
	       	     $db->insert($tablescenter,$arryresultcenter);
	        }
	       ///////////////////end for venue///////////////////	       
   }
   
   
   public function fnGetScheduler()
   {
   
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
										  ->join(array("b"=>"tbl_newmonths"),"a.From=b.idmonth",array("b.MonthName as From"))
										  ->join(array("c"=>"tbl_newmonths"),"a.To=c.idmonth",array("c.MonthName as To"));
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;	
	
   }
   
   /*
    * functionto get the scheduler details
    */
   public function fnGetSchedulerDetails($idscheduler)
   {  
   	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;
   }
   
    /*
    * function to get the scheduler days
    */
    public function fnGetSchedulerDays($idscheduler)
    {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newschedulerdays"),array("a.*"))
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     public function fngetDaysDetails()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_days"),array("a.*"));						 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
    /*
    * function to get the scheduler sessions
    */
    public function fnGetSchedulerSession($idscheduler)
    {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newschedulersession"),array("a.*"))								 
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
    /*
    * function to get the scheduler sessions
    */
    public function fnGetSchedulerCourse($idscheduler)
    {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newschedulercourse"),array("a.*"))								 
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
    /*   
     * function to get the scheduler sessions
    */
    public function fnGetSchedulerVenue($idscheduler)
    {  
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newschedulervenue"),array("a.*"))								 
								 ->where("a.idnewscheduler =?",$idscheduler);							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     public function fnGetMonthList()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"))								 
								 ->where("a.idmonth >=(SELECT month(curdate( )))");						 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     	
     }
  public function fnGetAllMonthList()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"));			 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     	
     }
     
 public function fnGetToMonthList($idmonth)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"))
								  ->where("a.idmonth >= ?",$idmonth);			 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     	
     }
     
     
public function fnGetfromList($idmonth)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName as From"))
								  ->where("a.idmonth = ?",$idmonth);			 						
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;
     	
     }
     
public function fnGettoList($idmonth)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName as To"))
								  ->where("a.idmonth = ?",$idmonth);			 						
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;
     	
     }
     
     
}
?>