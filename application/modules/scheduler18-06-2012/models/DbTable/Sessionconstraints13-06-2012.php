<?php 
class Scheduler_Model_DbTable_Sessionconstraints extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_program';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}
    
     public function fngetSessionDetails() { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_schedulerexception"),array("a.*"));							 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     
     public function fngetDaysDetails()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	   	             ->from(array("a"=>"tbl_days"),array("key"=>"a.iddays","value"=>"a.Days"));			 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
public function fngetSateDetails()
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	   	             ->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
	   	             ->where("a.idCountry=121")
	   	             ->order("a.StateName"); 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     
     
public function fnGetVenueList($lintIdCountry)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
	   	             ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"a.centername"))
	   	             ->where("a.country=121")
	   	             ->where("a.city=?",$lintIdCountry)
	   	             ->where("a.Active=1")
	   	             ->order("a.centername"); 						
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 		
		return $larrResult;
     }
     
     

    public function fnaddSessionconstrains($formData) { //Function for adding the University details to the table
		//	print_r($formData);die();
    	$db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_schedulerexception";
            $postData = array(		
							'Date' => $formData['Date'],	
           					'idstate' => $formData['idstate'],           				                           
                            'idcity' => $formData['idcity'],	
            				'idvenue' =>$formData['idvenue'],
                            'constraintname'=>$formData['constraintname'],
            				'Active' =>$formData['Active'],			
						);	
					
	     $db->insert($table,$postData);

	      $data = array('Active' =>0 			
						);
	     $where['date = ? ']= $formData['Date'];
	     $where['idvenue = ? ']= $formData['idvenue'];
		return $db->update('tbl_venuedateschedule', $data, $where);
	     
	}
    
public function fnSearchSession($post = array()) { //Function for searching the university details
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_schedulerexception'),array('a.*'))
			   ->where('a.constraintname  like "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
public function  fnGetcityList($idstate)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
         	$select = $db->select()
			   ->from(array('a' => 'tbl_city'),array("key"=>"a.idCity","value"=>"CityName"))
			   ->where('a.idState=?',$idstate);
		$result = $db->fetchAll($select);
		return $result;
	}
	
	public function fnGetsessionList($IdProgram){
		$lstrSelect = $this->lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_schedulerexception"),array("a.constraintname","a.Date","a.idstate as state","a.idcity as city","a.Active"))
				 				   ->join(array("c"=>"tbl_state"),'a.idstate=c.idState',array("c.StateName as idstate"))
				 				 ->where("a.idschedulerconstraints = ?",$IdProgram);
		$larrResult = $this->lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
	
	
	
    public function fnupdateSession($formData) { //Function for updating the university
    		 $db 	= 	Zend_Db_Table::getDefaultAdapter();	
    		/* print_r($formData);
    		 die();*/
		     $data =  array('Date' => $formData['Date'],	
           					'idstate' => $formData['idstate'],           				                           
                            'idcity' => $formData['idcity'],	
            				'idvenue' =>$formData['idvenue'],
                            'constraintname'=>$formData['constraintname'],
            				'Active' =>$formData['Active'],			
						);	
						/*print_r($formData['idschedulerconstraints']);
						die();*/
		$where['idschedulerconstraints = ? ']= $formData['idschedulerconstraints'];
		 $db->update('tbl_schedulerexception', $data, $where);
		
		///////////////////venue scheduler days////////////////////////////
		$data = array('Active' =>0);
	     $where1['date = ? ']= $formData['Date'];
	     $where1['idvenue = ? ']= $formData['idvenue'];
		return $db->update('tbl_venuedateschedule', $data, $where1);
    }
    
	
	
	
}
?>