<?php
class GeneralSetup_Model_DbTable_Maintenance extends Zend_Db_Table {
	
	protected $_name = 'tbl_definationtypems';

	//Function to Get Maintenance Details
	public function fnGetMaintenanceDetails() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect = $lobjDbAdpt->select()
							->from('tbl_definationtypems')
							->order('defTypeDesc');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		
		//$larrResult = $this->fetchAll();
		return $larrResult;
	}
	
	//Maintenance Search Function
	public function fnSearchMaintenace($searchStr) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array('a'=>'tbl_definationtypems'),array('a.idDefType','a.defTypeDesc'))
								 ->where('a.defTypeDesc like "%" ? "%"',$searchStr);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	//Function To Save Maintenance
	public function fnAddMaintenance($post) {
		$lstrMsg = $this->insert($post);
		return $lstrMsg;
	}
	
    //Function To View Maintenace Type Ms
	public function fnViewMaintenance($idDefinition) {
		$lstrSelect = $this	->select()
						->setIntegrityCheck(false)  
						->join(array('a' => 'tbl_definationms'),array('idDefinition'))
						->where('idDefType = ?',$idDefinition)
						->order('DefinitionDesc');                   
		$larrResult = $this->fetchAll($lstrSelect);
		return $larrResult->toArray();
	}
    
	
	//Function To View Maintenace Ms
	public function fnViewMaintenanceMs($idDefinition,$idDefinitionType) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt	->select()
						->join(array('a' => 'tbl_definationms'),array('idDefinition'))
						->where('idDefinition = ?',$idDefinition);    
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
	//Function To Get Maintenance Ms Details
	public function fnGetMaintenanceMsDetails($idDefType){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
							->from('tbl_definationms',array('idDefinition','idDefType','DefinitionCode','DefinitionDesc'))
							->where("Status='1' and idDefType=?",$idDefType)
							->order('definitionDesc');
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
		
	}
	
	//Function To Check Maintenance Detail
	public function fnCheckMaintenanceDetails($definitionDesc,$idDefinition){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				  ->from('tbl_definationms','count(*) as num')
				  ->where("DefinitionDesc=?",$definitionDesc)
				  ->where("idDefType =?",$idDefinition);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
		
	}
	
	//Function To Update Maintenance Ms details
	public function fnUpdateMaintenanceMs($data,$idDefinition){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrTable='tbl_definationms';
		$lstrWhere="idDefinition = '".$idDefinition . " ' ";
		$lstrMsg=$lobjDbAdpt->update($lstrTable,$data,$lstrWhere);
		
		return $lstrMsg;
	}

}
