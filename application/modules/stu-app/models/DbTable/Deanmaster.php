<?php 
class GeneralSetup_Model_DbTable_Deanmaster extends Zend_Db_Table_Abstract
{
    protected $_name = 'tbl_deanlist';
    private $lobjDbAdpt;
    
	public function init()
	{
		$this->lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	}    
	
	public function fnaddDean($result,$resultstaff,$larrformData) { //Function for adding the University details to the table
		$data = array('IdCollege'=>$result,
					  'IdStaff'=>$resultstaff,
		  			  'FromDate'=>$larrformData['FromDate'],
		 			  'ToDate'=>$larrformData['ToDate'],
					  'Active'=>$larrformData['Active'],
					  'UpdDate'=>$larrformData['UpdDate'],
					  'UpdUser'=>$larrformData['UpdUser']);
		$this->insert($data);
	}
	
	public function fnupdateDeanList($formData,$lintIdCollege)
	{

		$lstrselectsql = $this->lobjDbAdpt->select()
									->from(array('dean'=>'tbl_deanlist'),array('IdDeanList '=>'MAX(dean.IdDeanList )'))
									->where("dean.IdCollege = $lintIdCollege" );
		$larrresultset = $this->lobjDbAdpt->fetchRow($lstrselectsql);	
		if(!empty($larrresultset['IdDeanList']))
		{
	    	$larrdeanlist['ToDate'] = $formData['FromDate'];
	    	$lstrwhere = "IdDeanList = ".$larrresultset['IdDeanList'];
			$this->lobjDbAdpt->update('tbl_deanlist',$larrdeanlist,$lstrwhere);
		}
	}
	
	public function fninsertDeanList($formData,$lintIdCollege)
	{
    	$larrdeanlist['IdCollege'] = $lintIdCollege;
    	$larrdeanlist['IdStaff'] = $formData['IdStaff'];
    	$larrdeanlist['FromDate'] = $formData['FromDate'];
    	$larrdeanlist['ToDate'] = $formData['ToDate'];
    	$larrdeanlist['Active'] = $formData['Active'];
    	$larrdeanlist['UpdDate'] = $formData['UpdDate'];
    	$larrdeanlist['UpdUser'] = $formData['UpdUser'];
    	
		$this->lobjDbAdpt->insert('tbl_deanlist',$larrdeanlist);
		
	

	}
}
?>