<?php
class GeneralSetup_Model_DbTable_Departmentmaster extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_departmentmaster';
	
	
 	public function fngetDepartmentDetails2() { //Function to get the user details
        $result = $this->fetchAll();
        return $result;
     }
     
     public function fngetDepartmentDetails() {
     	$select = $this->select()
                ->setIntegrityCheck(false)  
                ->join(array('a' => 'tbl_departmentmaster'),array('IdDepartment'))
                ->join(array('b'=>'tbl_collegemaster'),'a.IdCollege  = b.IdCollege');
       $result = $this->fetchAll($select);
       return $result->toArray();
     }
     
	public function fnSearchDepartment($post = array()) { //Function for searching the user details
    	$db = Zend_Db_Table::getDefaultAdapter();
		$field7 = "Active = ".$post["field7"];
		$select = $this->select()
			   ->setIntegrityCheck(false)  	
			   ->join(array('a' => 'tbl_departmentmaster'),array('IdDepartment'))
			   ->where('a.DepartmentName like "%" ? "%"',$post['field2'])
			   ->where('a.DeptCode like  "%" ? "%"',$post['field3'])
			   ->where($field7);
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	
	
	public function fnaddDepartment($larrformData) { //Function for adding the user details to the table
		
		if($larrformData['DepartmentType'] == '0') {
			$larrformData['IdCollege'] = $larrformData['IdCollege'];
		} else if($larrformData['DepartmentType'] == '1') {
			$larrformData['IdCollege'] = $larrformData['IdBranch'];
		}
		unset($larrformData['IdBranch']);
		$this->insert($larrformData);
	}
	
	public function fnGetCollegeList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		 				 ->where("a.CollegeType = 0")
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetBranchList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		 				 ->where("a.CollegeType = 1")
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetBranchListofCollege($lintidCollege){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		 				 ->from(array("a"=>"tbl_collegemaster"),array("key"=>"a.IdCollege","value"=>"a.CollegeName"))
		 				 ->where("a.CollegeType = 1")
		 				 ->where("a.Idhead= ?",$lintidCollege)
		 				 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
		
    public function fnviewDepartment($lintidepartment) { //Function for the view user 
    	//echo $lintidepartment;die();
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select 	= $lobjDbAdpt->select()
						->from(array("a" => "tbl_departmentmaster"),array("a.*"))				
		            	->where("a.IdDepartment= ?",$lintidepartment);	
		return $result = $lobjDbAdpt->fetchRow($select);
    }
    
    public function fnupdateDepartment($lintIdDepartment,$larrformData) { //Function for updating the user
	if($larrformData['DepartmentType'] == '0') {
		$larrformData['IdCollege'] = $larrformData['IdCollege'];
	} else if($larrformData['DepartmentType'] == '1') {
		$larrformData['IdCollege'] = $larrformData['IdBranch'];
	}
	unset($larrformData['IdBranch']);
	$where = 'IdDepartment = '.$lintIdDepartment;
	$this->update($larrformData,$where);
    }
    
	public function fnGetCollege($lstridbranch){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_collegemaster"),array("a.Idhead"))
       								->where('a.IdCollege = ?',$lstridbranch);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	} 
	
}