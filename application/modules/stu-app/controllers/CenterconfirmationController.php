<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class StuApp_CenterconfirmationController extends Base_Base { //Controller for the User Module

	private $locale;
	private $registry;
	private $lobjuser;
	private $lobjuserForm;
	
	public function init() { //initialization function
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjstudentmodel = new StuApp_Model_Centerconfirmation(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
			//$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
		$this->lobjstudentForm = new StuApp_Form_Studentapplicationapp (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

public function indexAction() { // action for search and view
		$lobjform=$this->view->lobsearchform = $this->lobjform; //send the lobjuserForm object to the view
		
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		//$larrresult = $this->lobjstudentmodel->fngetStudentDetails(); //get user details

		$this->lobjintialConfigmodel = new GeneralSetup_Model_DbTable_Initialconfiguration();		
		$InitialConfigDetails = $this->lobjintialConfigmodel->fnGetInitialConfigDetails(1);	
		$larrBatch = $this->lobjstudentmodel->fnGetBatchName($InitialConfigDetails['ClosingBatch']); //searching the values for the user
		$this->view->lobsearchform->field1->addMultioption('0','Select');
		$this->view->lobsearchform->field1->addMultioptions($larrBatch);
		$this->view->lobsearchform->field1->setAttrib('onChange','fngetschedulerdetails(this.value);');
		$this->view->lobsearchform->field1->setAttrib('required',"true");
		$this->view->lobsearchform->field1->setRegisterInArrayValidator(false);
		$this->view->lobsearchform->field5->setAttrib('onChange','fngetvenuedetails(this.value);') ;
		$this->view->lobsearchform->field5->setAttrib('required',"true");
		$this->view->lobsearchform->field5->setRegisterInArrayValidator(false);
		$this->view->lobsearchform->field8->setAttrib('onChange','fngettimefordatevalidation(this.value)');
		$this->view->lobsearchform->field8->setAttrib('required',"true");
		$this->view->lobsearchform->field8->setRegisterInArrayValidator(false);
		$lintpagecount = $this->gintPageCount;
		$lintpage = $this->_getParam('page',1); // Paginator instance

		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($lobjform->isValid ( $larrformData )) {
				$larrShedule = $this->lobjstudentmodel->fnGetscheduler($larrformData['field1']);				
				$this->view->lobsearchform->field5->addMultioptions($larrShedule);
				$larrVenue = $this->lobjstudentmodel->fnGetVenueName($larrformData['field5']);				
				$this->view->lobsearchform->field8->addMultioptions($larrVenue);
				$larrresult = $this->lobjstudentmodel->fnSearchStudent($larrformData,$InitialConfigDetails['NoofStd']); //searching the values for the user
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionstudent->userpaginatorresult = $larrresult;
			}
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/stu-app/centerconfirmation/index');
			//$this->_redirect($this->view->url(array('module'=>'stu-app' ,'controller'=>'centerconfirmation', 'action'=>'index'),'default',true));
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'SendMail' )) {
			$larrformData = $this->_request->getPost ();
			
			$lobjstudentmodel = new App_Model_Studentapplication(); 
			//print_r($larrformData);		
			$larrShedule = $this->lobjstudentmodel->AdddatatoTable($larrformData);
			$larrSMTPDetails  = $lobjstudentmodel->fnGetSMTPSettings();
			$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
			$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
			$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
			$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
			$lstrSSL          = $larrSMTPDetails['SSL'];
			$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
			
			$lobjTransport = new Zend_Mail_Transport_Smtp();
			$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
			
			//Get Email Template Description
			$larrEmailTemplateDesc =  $lobjstudentmodel->fnGetEmailTemplateDescription("Venue Change");	
			
			
			
			if($larrEmailTemplateDesc['TemplateFrom']!=""){
				$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
				$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
				$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
				$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
				$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
			
			
			//Get Student's Mailing Details
					for($i=0;$i<count($larrformData['IdStudents']);$i++){
						$larrstud = $this->lobjstudentmodel->fnGetStudentDetails($larrformData['IdStudents'][$i]);				
						$larrEmailIds[$i] = $larrstud["EmailAddress"];
						$larrNames[$i] 	 = $larrstud['Name'];
						$lstrStudentName = $larrstud['Name'];
					}				
					try{
						$lobjProtocol->connect();
				   		$lobjProtocol->helo($lstrSMTPUsername);
						$lobjTransport->setConnection($lobjProtocol);
				 	
						//Intialize Zend Mailing Object
						$lobjMail = new Zend_Mail();
				
						$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
						$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
						$lobjMail->addHeader('MIME-Version', '1.0');
						$lobjMail->setSubject($lstrEmailTemplateSubject);
				
						for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
							if($larrEmailIds[$lintI] != ""){
								$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);	
														
								$lstrEmailTemplateBody = str_replace("[Candidate]",$larrNames[$lintI],$lstrEmailTemplateBody);
								$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
								
								$lobjMail->setBodyHtml($lstrEmailTemplateBody);
						
								try {
									$lobjMail->send($lobjTransport);
								} catch (Exception $e) {
									$lstrMsg = "error";      				
								}	
								$lobjMail->clearRecipients();
								//$this->view->mess .= ". Login Details have been sent to user Email";
								unset($larrEmailIds[$lintI]);
							}
						}
					}catch(Exception $e){
						$lstrMsg = "error";
					}			
			}
			else{
				$lstrMsg = "No Template Found";
			}			
			$this->_redirect( $this->baseUrl . '/stu-app/centerconfirmation/index');
		}		
	}

}