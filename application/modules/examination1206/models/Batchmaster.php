<?php
class Examination_Model_Batchmaster extends Zend_Db_Table {
	//protected $_name = 'tbl_BatchMaster'; // table name
	public  function fnGetparts()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_questions"),array("key"=>"a.QuestionGroup","value"=>"a.QuestionGroup") )
					 				->group("a.QuestionGroup");					  	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		
	}
	public  function fnGetPrograms()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_programmaster"),array("key"=>"IdProgrammaster","value"=>"ProgramName") )
       								->join(array("b"=>"tbl_program"),'a.idprog=b.IdProgram',array())
       								->where("b.Active = 1")
					 				->where("a.Active = 1");
					 									  	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		
	}
	public function fnAddBatch($post)
	{
		Unset($post['IdPart']);
		Unset($post['Save']);
			$db = Zend_Db_Table::getDefaultAdapter();
	    	$table = "tbl_batchmaster";
			$msg = $db->insert($table,$post);
			return $msg;
	}
	public function fnAddBatchdetails($post,$lintIdBatchMaster)
	{
	     
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_batchdetail";
           

     	foreach ($post['IdPart'] as $larrdata):
     	    $post['IdBatch']  = $lintIdBatchMaster;
     	    $post['IdPart']  = $larrdata;
     	    $post['BatchDtlStatus']=0;     	   
     	    /*$post['BatchDtlStatus']  = 0;
     	    $post['UpdDate']  = $post['UpdDate'];
     	    $larrdata['UpdUser']  = $post['UpdUser'];*/
	        $db->insert($table,$post);
	    endforeach;
	}
    	
public function fnGetBatchDetails($date)
	{
		//echo $date;die();
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
		    ->from(array('a'=>'tbl_batchmaster'),array('a.*'))
			->joinLeft(array('b' => 'tbl_tosmaster'),'a.IdBatch=b.IdBatch',array('b.IdBatch as Tos'))			
			->where("a.BatchStatus = 0")
			->where("a.BatchTo>=?",$date);
			//->group("b.IdBatch");
		$result = $db->fetchAll($select);
		
		return $result;   
	}
public function fnSearchbatch($post = array(),$date) {
		    $db = Zend_Db_Table::getDefaultAdapter();
		    $select = $db->select()
			->from(array('a'=>'tbl_batchmaster'),array('a.*'))
			->joinLeft(array('b' => 'tbl_tosmaster'),'a.IdBatch=b.IdBatch',array('b.IdBatch as Tos'));
			if($post['field7']==1)
			{
				$select->where('a.BatchStatus=?',0);
			}
			else {
				$select->where('a.BatchStatus=?',1);
			}
			if($post['field3']) $select -> where("a.BatchName like '".$post['field3']."%'");
		    if($post['field10']) $select -> where("a.BatchFrom >= ?",$post['field10']);
		     if($post['field11']) $select -> where("a.BatchTo <= ?",$post['field11']);
		    if($post['field5'])
		      {
		      	$select->join(array('c'=>'tbl_batchdetail'),'a.IdBatch=c.IdBatch',array('c.IdPart'))
		              -> where("c.IdPart= ?",$post['field5'])
		              ->where("c.BatchDtlStatus=0");
		              
		      }
		      if($post['field3']==""&&$post['field10']==""&&$post['field11']=="")
		      {
		       $select->where("a.BatchTo>=?",$date);;
		      }
   			/*if(isset($post['field10']) && !empty($post['field10']) && isset($post['field11']) && !empty($post['field11'])){
				$lstrFromDate = $post['field10'];
				$lstrToDate = $post['field11'];
				$select = $select->where("a.BatchFrom>=?",$lstrFromDate)
								->where("a.BatchTo<=?",$lstrToDate);
			}*/
		$result = $db->fetchAll($select);
		
		return $result;
	}
		
    public function fnViewBatch($IdBatch) {
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('a' => 'tbl_batchmaster'),array('a.*'))
                        ->where('IdBatch = ?',$IdBatch);
	 $result = $db->fetchRow($select);	
		return $result;
    }
    public function fnViewPart($IdBatch)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('a' => 'tbl_batchdetail'),array('a.IdPart','a.IdBatchDetail'))
                        ->where('IdBatch = ?',$IdBatch)
                        ->Where('BatchDtlStatus=0');
	 $result = $db->fetchAll($select);	
		return $result;
    	
    }
    
 public function fnUpdateBatch($larrformData,$lintidbank)
	 {
	 	 $db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$data = array('BatchName' => $larrformData['BatchName'],
		              'BatchStatus'=>$larrformData['BatchStatus'],
		              'BatchFrom'=> $larrformData['BatchFrom'],
		              'BatchTo'=>$larrformData['BatchTo'] );
		$where['IdBatch = ? ']= $lintidbank;		
		return $db->update('tbl_batchmaster', $data, $where);	
			    
	 }
	 public function fnInactiveparts($lintidbank){
	    $db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$data = array('BatchDtlStatus' => '1');
		$where['IdBatch = ? ']= $lintidbank;		
		return $db->update('tbl_batchdetail', $data, $where);			
	 }
	 public function fnActiveParts($lintidbank,$part)
	 { 
	 	$db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$data = array('BatchDtlStatus' => '0');
		//$where= array('IdBatch'=> $lintidbank,'IdPart'=>$part);	
		$where = "IdBatch = '".$lintidbank."'  AND IdPart ='".$part."'"; 	
		return $db->update('tbl_batchdetail', $data, $where);	
	 }
	 public function fnGetInactive($lintidbank)
	 {
	
	 	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('a' => 'tbl_batchdetail'),array('a.IdPart','a.IdBatchDetail'))
                        ->where('IdBatch = ?',$lintidbank)
                        ->Where('BatchDtlStatus=?',1);
	 $result = $db->fetchAll($select);	
		return $result;
	 }
	 
	 public function fnInsertnewpart($larrformData,$part)
	 {
	 	$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_batchdetail";
            $post['IdBatch']  = $larrformData['IdBatch'];
     	    $post['IdPart']  = $part;
     	    $post['BatchDtlStatus']=0;
     	    $post['NoOfQuestions']=0;
     	    $post['UpdDate']=$larrformData['UpdDate'];
     	    $post['UpdUser']=$larrformData['UpdUser'];
	        $db->insert($table,$post);
	 }

}
