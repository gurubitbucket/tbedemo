<?php
     class Examination_Model_Createquestionpaper extends Zend_Db_Table_Abstract 
      {    
      	    //Model Class for Users Details	
	        protected $_name = 'tbl_questiongroup';
	
        
        public function fngetquestionsetlist(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array("a.*"))
										  ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster=a.Program',array("c.*"));
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	  
            
            
      public function fnGetProgramName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										  ->join(array("c" => "tbl_program"),'c.IdProgram=a.idprog')
										  ->join(array("b"=>"tbl_batchmaster"),'b.IdProgrammaster=a.IdProgrammaster',array())
										  ->join(array("d"=>"tbl_tosmaster"),'b.IdBatch=d.IdBatch',array())
  										  ->join(array("e"=>"tbl_programrate"),'a.IdProgrammaster=e.idProgram',array())
										  ->where("e.Active=1")
										  ->where("d.Active=1")
										  ->where("c.Active =1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
            //Function for searching the author details	      
      public function fninserquestioncodes($larrformData,$idIdBatch,$iduser,$ldtsystemDate)
   {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   
		 	 $larrcreditnotedetails['Program'] = $larrformData['field1'];
		 	 $larrcreditnotedetails['Activeset'] = $idIdBatch;
		 	 $larrcreditnotedetails['PaperCode'] = $larrformData['field28'];
		 	 $larrcreditnotedetails['UpdDate'] = $ldtsystemDate;
		 	 $larrcreditnotedetails['UpdUser'] = $iduser;
		 	 $result = $lobjDbAdpt->insert('tbl_questiongroup',$larrcreditnotedetails);
		 	 $lastid  = $lobjDbAdpt->lastInsertId("tbl_questiongroup","idquestiongroup");	
   	 return $lastid;
   }
   
  public function fninserquestionsforabovecode($larrresult,$larrquestionid)
   {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  
   	  for($linti=0;$linti<count($larrquestionid);$linti++)
		 {
		 	 $larrcreditnotedetails['idquestiongroup'] = $larrresult;
		 	 $larrcreditnotedetails['idquestions'] = $larrquestionid[$linti];
		 	 $result = $lobjDbAdpt->insert('tbl_questiongroupdetails',$larrcreditnotedetails);
		 	 
		 }
   	 return $result;
   }
   
   
        public function fndisplayquestionsforabovecode($RegID){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroupdetails"),array("")) 
										  ->join(array("c" => "tbl_questions"),'c.idquestions=a.idquestions',array('c.idquestions','c.Question'))
										  ->where("a.idquestiongroup=?",$RegID);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
      public function fndisplayanswersforquestions($ID){
      	      
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_answers"),array("a.*")) 
										  ->join(array("c" => "tbl_questions"),'c.idquestions=a.idquestion',array("c.*"))
										  ->where("a.idquestion=?",$ID)
										  ->order("a.idanswers");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
      public function fngetprgname($ID){
      	      
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array("a.PaperCode","DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as creationdate")) 
										  ->join(array("c" => "tbl_programmaster"),'c.IdProgrammaster = a.Program',array('c.ProgramName'))
										  ->where("a.idquestiongroup = ?",$ID);
										  //->order("a.idanswers");	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	   }
	
      public function fngettosdesc($ID){
      	        
				 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_questiongroup"),array("a.*")) 
										  ->join(array("d" => "tbl_tosmaster"),'d.IdBatch = a.Activeset',array("d.*"))
										  ->where("a.idquestiongroup = ?",$ID)
										  ->where("d.Active = 1");
										   
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
      public function fnGetsearchdetails($larrformData)
	{
		if($larrformData['field28']) $setcode = $larrformData['field28'];
		if($larrformData['field1']) $programname = $larrformData['field1'];
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		              ->from(array("a" => "tbl_questiongroup"),array('a.*'))
		              ->join(array("b" => "tbl_programmaster"),'b.IdProgrammaster = a.Program',array('b.*'));
		
		if($larrformData['field28']) $lstrSelect .= " AND a.PaperCode like '%$setcode%'"; 
	    if($larrformData['field1']) $lstrSelect .= " AND a.Program = $programname";
					
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	public function fngetpaper($code){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		              ->from(array("a" => "tbl_questiongroup")) 
					  ->where("a.PaperCode  = ?",$code);
					  //echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
		
	}
	
}    
		  	 
	
