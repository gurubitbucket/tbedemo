<?php
	class Examination_Form_Newscreen extends Zend_Dojo_Form {
		public function init() {
		$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
		
		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day+1),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
	    
				$Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
			    $Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Coursename->addMultiOption('','Select'); 	           	         		       		     
	            $Coursename->removeDecorator("DtDdWrapper");
	            $Coursename->removeDecorator("Label");
	            $Coursename->removeDecorator('HtmlTag');   
	            
	            $Coursename1 = new Zend_Dojo_Form_Element_FilteringSelect('Coursename1');
			    $Coursename1->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            //$Coursename1->addMultiOption('','Select'); 	
	            $Coursename1->setAttrib('onChange','fngetstatedetails(this.value);');	           	         		       		     
	            $Coursename1->removeDecorator("DtDdWrapper");
	            $Coursename1->removeDecorator("Label");
	            $Coursename1->removeDecorator('HtmlTag');
	            
	            $Companyname = new Zend_Dojo_Form_Element_FilteringSelect('Companyname');
			    $Companyname->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Companyname->addMultiOption('','Select'); 	           	         		       		     
	            $Companyname->removeDecorator("DtDdWrapper");
	            $Companyname->removeDecorator("Label");
	            $Companyname->removeDecorator('HtmlTag');  

	            $Takafulname = new Zend_Dojo_Form_Element_FilteringSelect('Takafulname');
			    $Takafulname->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Takafulname->addMultiOption('','Select'); 	           	         		       		     
	            $Takafulname->removeDecorator("DtDdWrapper");
	            $Takafulname->removeDecorator("Label");
	            $Takafulname->removeDecorator('HtmlTag');   
	            
	            $Result = new Zend_Dojo_Form_Element_FilteringSelect('Result');
			    $Result->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Result->addMultiOptions(array(''=>'Select','3'=>'Applied','1'=>'Pass','2'=>'Fail')); 	           	         		       		     
	            $Result->removeDecorator("DtDdWrapper");
	            $Result->removeDecorator("Label");
	            $Result->removeDecorator('HtmlTag');  
	            
	            $ICNO = new Zend_Form_Element_Text('ICNO');
			    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $ICNO->setAttrib('class','txt_put');  
	            $ICNO->removeDecorator("DtDdWrapper");
	            $ICNO->removeDecorator("Label");
	            $ICNO->removeDecorator('HtmlTag');
	            
	              
	            $Examdate = new Zend_Form_Element_Text('Examdate');
			    $Examdate->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $Examdate->setAttrib('class','txt_put');  
	            $Examdate->removeDecorator("DtDdWrapper");
	            $Examdate->removeDecorator("Label");
	            $Examdate->removeDecorator('HtmlTag');
	            
	            $Studentname = new Zend_Dojo_Form_Element_FilteringSelect('Studentname');
			    $Studentname->setAttrib('dojoType',"dijit.form.FilteringSelect");
			    $Studentname->addMultiOption('','Select'); 
	            $Studentname->setAttrib('class','txt_put');  
	            $Studentname->removeDecorator("DtDdWrapper");
	            $Studentname->removeDecorator("Label");
	            $Studentname->removeDecorator('HtmlTag');
	            
	            
	            $newexamdates = new Zend_Form_Element_Text('newexamdates');
			    $newexamdates->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $newexamdates->setAttrib('class','txt_put')
	               -> setAttrib('required',"true"); 
	            $newexamdates->removeDecorator("DtDdWrapper");
	            $newexamdates->removeDecorator("Label");
	            $newexamdates->removeDecorator('HtmlTag');
	            
	          /*  $newexamdates = new Zend_Dojo_Form_Element_DateTextBox('newexamdates');
	        	$newexamdates->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');*/
	            
				$Dates = new Zend_Dojo_Form_Element_DateTextBox('Dates');
	        	$Dates->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$Dates2 = new Zend_Dojo_Form_Element_DateTextBox('Dates2');
	        	$Dates2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
									
		       $UpDate1 = new Zend_Dojo_Form_Element_DateTextBox('UpDate1');
	           $UpDate1->setAttrib('dojoType',"dijit.form.DateTextBox")
	               ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('UpDate2').constraints.min = arguments[0];")		
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
	           $UpDate2 = new Zend_Dojo_Form_Element_DateTextBox('UpDate2');
	           $UpDate2->setAttrib('dojoType',"dijit.form.DateTextBox")
	                   ->setAttrib('title',"dd-mm-yyyy")
			           ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
			           ->setAttrib('onChange', "dijit.byId('UpDate1').constraints.max = arguments[0];")
			  		   ->removeDecorator("Label")
			  		   ->removeDecorator("DtDdWrapper")
			  		   ->removeDecorator('HtmlTag');
			  		   
			  	$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Date2').constraints.min = arguments[0];")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	        	$Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->setAttrib('onChange', "dijit.byId('Date').constraints.max = arguments[0];")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');
								   
			  /*$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	        	$Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');*/
				/*$UpDate1 = new Zend_Dojo_Form_Element_DateTextBox('UpDate1');
	        	$UpDate1->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 	
	        	$UpDate2 = new Zend_Dojo_Form_Element_DateTextBox('UpDate2');
	        	$UpDate2->setAttrib('dojoType',"dijit.form.DateTextBox")
	        				->setAttrib('title',"dd-mm-yyyy")
							->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
							->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');*/
					
				$Date4 = new Zend_Dojo_Form_Element_DateTextBox('Date4');
	        	$Date4->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
							
							
							
		        $Date3 = new Zend_Dojo_Form_Element_DateTextBox('Date3');
	        	$Date3->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	        	
	            $Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
			    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Venues->addMultiOption('','Select');  
	            $Venues->removeDecorator("DtDdWrapper");
	            $Venues->removeDecorator("Label");
	            $Venues->removeDecorator('HtmlTag');
	            
	           
	            
	            
	            	
	            $examstate = new Zend_Dojo_Form_Element_FilteringSelect('examstate');
			    $examstate->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $examstate->addMultiOption('','Select');  
	            $examstate->removeDecorator("DtDdWrapper");
	            $examstate->removeDecorator("Label");
	            $examstate->removeDecorator('HtmlTag');
	            
	            
	            	
	            $examcity = new Zend_Dojo_Form_Element_FilteringSelect('examcity');
			    $examcity->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $examcity->addMultiOption('','Select');  
	            $examcity->removeDecorator("DtDdWrapper");
	            $examcity->removeDecorator("Label");
	            $examcity->removeDecorator('HtmlTag');
	            
	             $examsession = new Zend_Dojo_Form_Element_FilteringSelect('examsession');
			    $examsession->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $examsession->addMultiOption('','Select');  
	            $examsession->removeDecorator("DtDdWrapper");
	            $examsession->removeDecorator("Label");
	            $examsession->removeDecorator('HtmlTag');
			
	            
	            $newexamstate = new Zend_Dojo_Form_Element_FilteringSelect('newexamstate');
			    $newexamstate->setAttrib('dojoType',"dijit.form.FilteringSelect")
			     ->setAttrib('OnChange','fnGetCitylist(this.value)')
			        -> setAttrib('required',"true");
	            //$newexamstate->addMultiOption('','Select');  
	            $newexamstate->removeDecorator("DtDdWrapper");
	            $newexamstate->removeDecorator("Label");
	            $newexamstate->removeDecorator('HtmlTag');
	            
	            
	            	
	            $newexamcity = new Zend_Dojo_Form_Element_FilteringSelect('newexamcity');
			    $newexamcity->setAttrib('dojoType',"dijit.form.FilteringSelect")
			    ->setAttrib('OnChange','fndisplaycalender(this.value)');
	            $newexamcity-> setAttrib('required',"true");
	            $newexamcity->removeDecorator("DtDdWrapper");
	            $newexamcity->removeDecorator("Label");
	            $newexamcity->removeDecorator('HtmlTag');
	            
	            
	             $Venues1 = new Zend_Dojo_Form_Element_FilteringSelect('Newexamvenue');
			    $Venues1->setAttrib('dojoType',"dijit.form.FilteringSelect")
			    ->setAttrib('OnChange','fnGetVenuesessionname(this.value)');
	            $Venues1  -> setAttrib('required',"true");  
	            $Venues1->removeDecorator("DtDdWrapper");
	            $Venues1->removeDecorator("Label");
	            $Venues1->removeDecorator('HtmlTag');
			
	            
	            $newexamsession = new Zend_Dojo_Form_Element_FilteringSelect('newexamsession');
			    $newexamsession->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $newexamsession -> setAttrib('required',"true");  
	            $newexamsession->removeDecorator("DtDdWrapper");
	            $newexamsession->removeDecorator("Label");
	            $newexamsession->removeDecorator('HtmlTag');
	            
	            
	            $newexammonth = new Zend_Dojo_Form_Element_FilteringSelect('newexammonth');
			    $newexammonth->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $newexammonth->setAttrib('OnChange','fnShowCalendarnew(this.value)')
	            -> setAttrib('required',"true");  
	            $newexammonth->removeDecorator("DtDdWrapper");
	            $newexammonth->removeDecorator("Label");
	            $newexammonth->removeDecorator('HtmlTag');
	            
				$FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
	        	$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	       	 	$FromDate->removeDecorator("DtDdWrapper")
						 ->setAttrib('title',"dd-mm-yyyy")
	       				 ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	        	$FromDate->removeDecorator("Label");
	        	$FromDate->removeDecorator('HtmlTag'); 
        				
			  
				$ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
	        	$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	       	 	$ToDate->removeDecorator("DtDdWrapper")
						->setAttrib('title',"dd-mm-yyyy")
	       				 ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");;
	        	$ToDate->removeDecorator("Label");
	        	$ToDate->removeDecorator('HtmlTag'); 
	        	
	        	
	        	$Approvaltype = new Zend_Dojo_Form_Element_FilteringSelect('Approvaltype');
			    $Approvaltype->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Approvaltype->addMultiOptions(array(
									'1' => 'All',
									'2' => 'Approved',
	                                '3' => 'Not Approved' ));           	         		       		     
	            $Approvaltype->removeDecorator("DtDdWrapper");
	            $Approvaltype->removeDecorator("Label");
	            $Approvaltype->removeDecorator('HtmlTag');
	            
	            $Applicationtype = new Zend_Dojo_Form_Element_FilteringSelect('Applicationtype');
			    $Applicationtype->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Applicationtype->addMultiOptions(array(
	                                '1' => 'All',
									'2' => 'Individual',
	                                '3' => 'Company',
	                                '4' => 'Takaful'));      	         		       		     
	            $Applicationtype->removeDecorator("DtDdWrapper");
	            $Applicationtype->removeDecorator("Label");
	            $Applicationtype->removeDecorator('HtmlTag');
	            
	            $Payment = new Zend_Dojo_Form_Element_FilteringSelect('Payment');
			    $Payment->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Payment->addMultiOption('','Select');
	            $Payment->addMultiOptions(array(
									'1' => 'Paid',
									'0' => 'Pending'));
	            $Payment->removeDecorator("DtDdWrapper");
	            $Payment->removeDecorator("Label");
	            $Payment->removeDecorator('HtmlTag');
	            
	            $paymentmode = new Zend_Dojo_Form_Element_FilteringSelect('paymentmode');
			    $paymentmode->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $paymentmode->addMultiOption('','Select'); 
	            $paymentmode->addMultiOptions(array(
									'1' => 'FPX',
									'2' => 'Credit Card',
	            					'5' => 'Money Order',
	            					'6' => 'Postal Order',
	            					'7' => 'Credit/Bank to IBFIM account')); 	           	         		       		     
	            $paymentmode->removeDecorator("DtDdWrapper");
	            $paymentmode->removeDecorator("Label");
	            $paymentmode->removeDecorator('HtmlTag');
	            
	            $approval = new Zend_Dojo_Form_Element_FilteringSelect('approval');
			    $approval->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $approval->addMultiOption('','Select'); 
	           	           	         		       		     
	            $approval->removeDecorator("DtDdWrapper");
	            $approval->removeDecorator("Label");
	            $approval->removeDecorator('HtmlTag');
         	 
    	   	
				$schedulerename = new Zend_Dojo_Form_Element_FilteringSelect('schedulerename');
			    $schedulerename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $schedulerename->removeDecorator("DtDdWrapper");
	            $schedulerename->removeDecorator("Label");
	            $schedulerename->removeDecorator('HtmlTag');   
			
	            
	            $schedulerename1 = new Zend_Dojo_Form_Element_FilteringSelect('schedulerename1');
			    $schedulerename1->setAttrib('dojoType',"dijit.form.FilteringSelect")
			    ->setAttrib('onChange','newcitylistss(this.value);');
	            $schedulerename1->removeDecorator("DtDdWrapper");
	            $schedulerename1->removeDecorator("Label");
	            $schedulerename1->removeDecorator('HtmlTag'); 
	            
	            $Clear = new Zend_Form_Element_Submit('Clear');
        		$Clear->dojotype="dijit.form.Button";
        		$Clear->label = $gstrtranslate->_("Clear");
				$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
				$search = new Zend_Form_Element_Submit('Search');
        		$search->dojotype="dijit.form.Button";
        		$search->label = $gstrtranslate->_("Search");
        		$search->removeDecorator("DtDdWrapper");
       			$search->removeDecorator("Label");
        		$search->removeDecorator('HtmlTag')
         		       ->class = "NormalBtn";
         		       
         		$Newprgdate = new Zend_Dojo_Form_Element_DateTextBox('Newprgdate');
	        	$Newprgdate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        	->setAttrib('constraints', "$dateofbirth")
	        		    ->setAttrib('title',"dd-mm-yyyy")
	        		    ->setAttrib('onChange','newvenuelists(this.value);')
						//->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
						
				$newprgexamsession = new Zend_Dojo_Form_Element_FilteringSelect('newprgexamsession');
			    $newprgexamsession->setAttrib('dojoType',"dijit.form.FilteringSelect")
			      ->setAttrib('onChange','newgetremlists(this.value);');
	            $newprgexamsession -> setAttrib('required',"true");  
	            $newprgexamsession->removeDecorator("DtDdWrapper");
	            $newprgexamsession->removeDecorator("Label");
	            $newprgexamsession->removeDecorator('HtmlTag');
	            
	            
	            $newprgexamvenue = new Zend_Dojo_Form_Element_FilteringSelect('newprgexamvenue');
			    $newprgexamvenue->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $newprgexamvenue -> setAttrib('required',"true")
	               ->setAttrib('onChange','newdatexption(this.value);');
	            $newprgexamvenue->removeDecorator("DtDdWrapper");
	            $newprgexamvenue->removeDecorator("Label");
	            $newprgexamvenue->removeDecorator('HtmlTag');
						
						
         		
	            
	            $newremainingseats = new Zend_Form_Element_Text('newremainingseats',array('regExp'=>"[1-9]+[0-9]*",'invalidMessage'=>"all seats are full"));
			    $newremainingseats->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $newremainingseats->setAttrib('class','txt_put')
	               -> setAttrib('required',"true"); 
	            $newremainingseats->removeDecorator("DtDdWrapper");
	            $newremainingseats->removeDecorator("Label");
	            $newremainingseats->removeDecorator('HtmlTag');
	            
	            
	              		     		
        $passwd = new Zend_Form_Element_Password('passwd');	
        $passwd->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $passwd->setAttrib('required',"true") ;
        $passwd->removeDecorator("DtDdWrapper");
        $passwd->removeDecorator("Label");
        $passwd->removeDecorator('HtmlTag');
	            
         		 $Save = new Zend_Form_Element_Submit('Save');
        		$Save->dojotype="dijit.form.Button";
        		$Save->removeDecorator("DtDdWrapper");
        		$Save->removeDecorator("Label");
        		$Save->removeDecorator('HtmlTag');
        		$Save->class = "NormalBtn";
        		$Save->label = $gstrtranslate->_("Save"); 
         		       
   /*$Save = new Zend_Form_Element_Submit('Yes');
        $Save->dojotype="dijit.form.Button";
        $Save->label =("Yes");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";	*/
         		
			$this->addElements(
        					array($Coursename,
        					      $Coursename1,
        					      $Newprgdate,
        					      $newprgexamsession,
        					      $newprgexamvenue,
        					      $newremainingseats,
        					      $schedulerename,
        					      $schedulerename1,
        					      $Companyname,
        					      $newexamdates,
        						  $ICNO,
        					      $Studentname,
        					      $Venues,
        					      $Venues1,
        					      $newexamsession,
        					      $examsession,
        					      $Date,
        					      $Date2,
        					      $Takafulname,
        					      $examstate,
        					      $newexammonth,
        					      $examcity,
        					      $Examdate,
        					      $newexamstate,
        					      $newexamcity,
        					      $Result,
        					      $FromDate,
        					      $ToDate,
        						  $Date3,$Date4,
        						  $UpDate1,
        						  $UpDate2,
        						  $Dates,
        						  $Dates2,
        						  $Approvaltype,
        						  $Applicationtype,
        						  $Payment,
        						  $paymentmode,
        						  $approval,
        						  $passwd,
        						  $Clear,
        						  $search,
        						  $Save
        						
        						)
        			);
		}
}
