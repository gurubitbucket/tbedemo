<?php
class Examination_Form_Createquestionpaper extends Zend_Dojo_Form 
{    
	
    public function init() 
    {    
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    		
        $GroupName = new Zend_Form_Element_Text('groupname');	
		$GroupName->setAttrib('dojoType',"dijit.form.ValidationTextBox");		
        $GroupName->setAttrib('required',"true");       			        		  
        $GroupName->removeDecorator("DtDdWrapper");
        $GroupName->removeDecorator("Label");
        $GroupName->removeDecorator('HtmlTag');
        		
       	$Description = new Zend_Form_Element_Text('groupdescription');
		$Description->setAttrib('dojoType',"dijit.form.ValidationTextBox");    			         				      
        $Description->removeDecorator("DtDdWrapper");
        $Description->removeDecorator("Label");
        $Description->removeDecorator('HtmlTag');
        
        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
         $Save = new Zend_Form_Element_Submit('Save');
         $Save->dojotype="dijit.form.Button";
         $Save->label = $gstrtranslate->_("Save");
         $Save->setAttrib('class', 'NormalBtn');
         $Save->setAttrib('id', 'submitbutton');
         $Save->removeDecorator("DtDdWrapper");
         $Save->removeDecorator("Label");
         $Save->removeDecorator('HtmlTag');
           		         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn');
		$Back->removeDecorator("Label");
		$Back->removeDecorator("DtDdWrapper");
		$Back->removeDecorator('HtmlTag');
		
		
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $field1 = new Zend_Dojo_Form_Element_FilteringSelect('field1');
        $field1->setAttrib('dojoType',"dijit.form.FilteringSelect"); 
        $field1->removeDecorator("DtDdWrapper");        
        $field1->removeDecorator("Label");
        $field1->removeDecorator('HtmlTag');
        $field1->setAttrib('required',"false");
        //$field1->setAttrib('OnChange', 'fnGetDetails');
        $field1->setRegisterInArrayValidator(false);        
		
         		
        $field28 = new Zend_Form_Element_Text('field28');
		$field28 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field28 ->setAttrib('class', 'txt_put')   
        		 ->removeDecorator("DtDdWrapper")
        	     ->removeDecorator("Label")
        		 ->removeDecorator('HtmlTag');
        		 
        $field29 = new Zend_Form_Element_Text('field29',array('invalidMessage'=>''));
		$field29 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field29 ->setAttrib('class', 'txt_put') 
       			 //->setAttrib('validator', 'validateUsername')		 
        		 ->setAttrib('required',"true")     
        		 ->removeDecorator("DtDdWrapper")
        	     ->removeDecorator("Label")
        		 ->removeDecorator('HtmlTag');
         		
        $Generate = new Zend_Form_Element_Submit('Generate');
		$Generate->dojotype="dijit.form.Button";
        $Generate->setAttrib('class', 'NormalBtn')
       //->setAttrib('onclick', 'fnCloseLyteBox()')
        		  ->removeDecorator("DtDdWrapper")
	        	  ->removeDecorator("Label")
    	    	  ->removeDecorator('HtmlTag');
		$Generate->label = $gstrtranslate->_("Generate");
		
		$Active1  = new Zend_Form_Element_Checkbox('Active1');
        $Active1->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active1->setvalue('1');
        $Active1->removeDecorator("DtDdWrapper");
        $Active1->removeDecorator("Label");
        $Active1->removeDecorator('HtmlTag');

        $DescActive  = new Zend_Form_Element_Checkbox('DescActive');
        $DescActive->setAttrib('dojoType',"dijit.form.CheckBox");
        $DescActive->setvalue('1');
        $DescActive->removeDecorator("DtDdWrapper");
        $DescActive->removeDecorator("Label");
        $DescActive->removeDecorator('HtmlTag');
        
        //form elements
        $this->addElements(array($GroupName,
        						 $Description,
        						 $Active,
        						 $Active1,
        						 $DescActive,       						
                                 $Save,
                                 $Back,
                                 $submit,
                                 $field28,
                                 $Generate,
                                 $field1,
                                 $field29
                                )
                          );

    }
}