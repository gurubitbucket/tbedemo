<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class Examination_ExamtimeController extends Base_Base {
	
	public function init() 
	{		
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj(); //call fnsetObj
	}
    
	private function fnsetObj()
	{	
		$this->lobjexamtimemodel = new Examination_Model_Examtime(); //intialize newscreen db object
		$this->lobjexamtimeForm = new Examination_Form_Examtime(); 
		$this->lobjform = new App_Form_Search(); 
	}
	
	public function indexAction() 
	{
		
		$this->view->checkEmpty = 1;
		$this->view->Active = 1;	 
		$this->view->lobjform = $this->lobjexamtimeForm; //send the lobjForm object to the view
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Submit' )) 
		{
		$larrformData = $this->_request->getPost ();
		if ($this->lobjform->isValid($larrformData)) 
		{
		$lstrpassword = $larrformData ['passwd'];
		$larrformData ['passwd'] = md5 ( $lstrpassword );
		$resultpassword = $this->lobjexamtimemodel->fncheckSuperUserPwd($larrformData['passwd']);
		if(!empty($resultpassword['passwd']) && $larrformData['passwd'] == $resultpassword['passwd'])
		{
		$this->view->resultpassword =  $resultpassword['passwd'];
		$this->view->checkEmpty = 2;
		}
		else
		{
		echo "<script> alert('Please Enter Valid Password of Super Admin')</script>";
		}
		}		
		}
		$larrresult=0;
		if(!$this->_getParam('search')) 
		unset($this->gobjsessionsis->examtimepaginatorresult);
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		$larrVenuesresult = $this->lobjexamtimemodel->fnGetVenueNames();	
		$this->lobjexamtimeForm->Venues->addMultiOptions($larrVenuesresult);
		if(isset($this->gobjsessionsis->examtimepaginatorresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->examtimepaginatorresult,$lintpage,$lintpagecount);
			$this->view->checkEmpty = 2;
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) 
		{
		$larrformData = $this->_request->getPost ();
		if ($this->lobjform->isValid ( $larrformData )) 
		{
		$this->view->paramsearch =  $this->_getParam('search');
		//print_r($larrformData);die();
		$this->view->lobjform->Dates->setValue($larrformData['Dates']);
			$this->view->lobjform->Venues->setValue($larrformData['Venues']);
	    $larrresult = $this->lobjexamtimemodel->fnSearchCentergracetime($larrformData); //searching the values for the user
	    if(!empty($larrresult) )
	    {
		$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		$this->gobjsessionsis->examtimepaginatorresult = $larrresult;
		$this->view->checkEmpty = 2;
		$this->lobjform->populate($larrformData);
		}
		else
		{
		$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		$this->gobjsessionsis->examtimepaginatorresult = $larrresult;
		$this->view->checkEmpty = 2;
		}	
		}
		}
	if($this->_getParam('search'))
	{
	$larrresult = $this->lobjexamtimemodel->fnSearchCentergracetime($larrformData); //searching the values for the user
	$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
	$this->gobjsessionsis->examtimepaginatorresult = $larrresult;
	$this->view->checkEmpty = 2;
	}			
	/*	if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/general-setup/examtime/index');
		
		}*/
	}
	
	public function newexamtimeAction() 
	{	
	
	$this->view->lobjnewscreenForm = $this->lobjexamtimeForm;
	$studentexamgracetime=$this->lobjexamtimemodel->fngetintialgracetimeinfo();		
	$this->view->gracetime=$studentexamgracetime['examgracetime'];
	$this->view->lobjnewscreenForm->ExamDatetime->setValue(date('Y-m-d'));
		
	if ($this->_request->isPost() && $this->_request->getPost('Save')) 
	{
	$larrformData = $this->_request->getPost();
	
	$larrcheckdata=$this->lobjexamtimemodel->fngetalreadythere($larrformData['newexamcity'],$larrformData['examsession'],$larrformData['ExamDatetime']);
	if($larrcheckdata)
	{
		echo '<script language="javascript">alert("This Data is already there")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/examination/examtime/index';</script>";
 		die();
		
	}
	else 
	{
		//print_r($larrformData);die();
	$auth = Zend_Auth::getInstance();
	$iduser=$auth->getIdentity()->iduser;
	$larrformData['UpdDate']=date('Y-m-d H:i:s');
	$studenteditiinserlarr=$this->lobjexamtimemodel->fngetgracetimeinsertinfo($larrformData,$iduser);
	$this->_redirect( $this->baseUrl . '/examination/examtime/index/search/1');		
	}
	}			
	}
	
	
	

public function schedulerexceptionAction()
{
    $this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$lintday = $this->_getParam('day');//city
	$lintcity = $this->_getParam('city');
	$lintmonth = $this->_getParam('month');
	$year = $this->_getParam('year');
	$days = $year.'-'.$lintmonth.'-'.$lintday;
	$larrresultcity=$this->lobjexamtimemodel->newfnGetcitydetailsgetsecid($lintcity);
	$resultsss = $this->lobjexamtimemodel->fngetschedulerexception($days,$larrresultcity['city']);
	$counts = count($resultsss);
	if($counts>1)
	{
		echo "No exams are offerred on the selected date. It can be a public holiday, please select a different date.";
		die();
		
	}
	else 
	{
		
	}	
	
}	
	
	public function newfngetyearAction()
 {
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
        $year=date('Y');
        $larrvenuetimeresults = $this->lobjexamtimemodel->newfncurrentgetyear($year);
        $larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
 }
		
public function newvenuelistsnamesAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$larrdates=explode('-',$iddate);
		$year= $larrdates[0];
		$month=$larrdates[1];
		$date=$larrdates[2];
		$larrschedulerresults = $this->lobjexamtimemodel->newgetscheduleryear($year);
		$values=0;
		for($idsech=0;$idsech<count($larrschedulerresults);$idsech++)
		{
		if(( $month >= $larrschedulerresults[$idsech]['From']) && ($month <= $larrschedulerresults[$idsech]['To'] ))
		{
		$value=$larrschedulerresults[$idsech]['idnewscheduler'];
		$values=$values.','.$value;
		}
		}
	   	$larrweekday = $this->lobjexamtimemodel->fngetdayofdate($iddate);
	    if($larrweekday['days']==1)
		{
		$larrweekday['days']= 7;
		}
		else 
		{
		$larrweekday['days']=$larrweekday['days']-1;	
		}
	   	$larrschedulerdays=$this->lobjexamtimemodel->fngetschedulerofdatevenuestudent($values,$larrweekday['days'],$iddate);
	   	
	   	if($larrschedulerdays)
		{
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdays);
		}
		else 
		{
		$larrschedulerdays="";
		}
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}


public function newvenuelistsemptynamesAction()
{
	 	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$iddate = $this->_getParam('iddate');
		$larrCountryStatesDetailss[]=array('key'=>'0','name'=>'Select');
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}




public function newsessionemptynamesAction()
{
	 	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
        $iddate = $this->_getParam('iddate');
		$idvenue = $this->_getParam('idvenues');
		$idsession= $this->_getParam('idsessions');
	
		$larrschedulerdays=$this->lobjexamtimemodel->fngetschedulerofcenterstart($idvenue,$idsession,$iddate);
		$message="";
		if($larrschedulerdays)
		{
		$message="This exam venue is already started";
		}
		else 
		{
		$today=date('Y-m-d');
		if($today==$iddate)
		{
		$time=date('H:i:s');	
		$larrdates=explode('-',$iddate);
		$year= $larrdates[0];
		$month=$larrdates[1];
		$date=$larrdates[2];
		$larrschedulerresults = $this->lobjexamtimemodel->newgetscheduleryear($year);
		
		$values=0;
		for($idsech=0;$idsech<count($larrschedulerresults);$idsech++)
		{
		if(( $month >= $larrschedulerresults[$idsech]['From']) && ($month <= $larrschedulerresults[$idsech]['To'] ))
		{
		$value=$larrschedulerresults[$idsech]['idnewscheduler'];
		$values=$values.','.$value;
		}
		}	
	   	$larrweekday = $this->lobjexamtimemodel->fngetdayofdate($iddate);
	    if($larrweekday['days']==1)
		{
		$larrweekday['days']= 7;
		}
		else 
		{
		$larrweekday['days']=$larrweekday['days']-1;	
		}
			
	   	$larrschedulerdayssession=$this->lobjexamtimemodel->fngetschedulerofdatesessionstudentforcurrentdate($values,$larrweekday['days'],$iddate,$idvenue,$time);
		if($larrschedulerdayssession=='')
		{
		$message="This exam venue session  time is elapsed";
		}
			
		}		
		}

		$larrschedulerresultthere=$this->lobjexamtimemodel->fngetalreadythere($idvenue,$idsession,$iddate);
		if($larrschedulerresultthere)
		{
		$message="This exam venue session data is already present";
		
		}
		echo  $message;

		 
}

public function newsessionlistsnamesAction()
{
	   	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		$iddate = $this->_getParam('iddate');
		$venueid = $this->_getParam('venueid');
		
		$larrdates=explode('-',$iddate);
	
		$year= $larrdates[0];
		$month=$larrdates[1];
		$date=$larrdates[2];
		
		$larrschedulerresults = $this->lobjexamtimemodel->newgetscheduleryear($year);
		$values=0;
		for($idsech=0;$idsech<count($larrschedulerresults);$idsech++)
		{
		if(( $month >= $larrschedulerresults[$idsech]['From']) && ($month <= $larrschedulerresults[$idsech]['To'] ))
		{
		$value=$larrschedulerresults[$idsech]['idnewscheduler'];
		$values=$values.','.$value;
		}
		}
			
		$larrweekday = $this->lobjexamtimemodel->fngetdayofdate($iddate);
	    if($larrweekday['days']==1)
		{
		$larrweekday['days']= 7;
		}
		else 
		{
		$larrweekday['days']=$larrweekday['days']-1;	
		}
		$today=date('Y-m-d');
		if($today==$iddate)
		{
		$time=date('H:i:s');
	   	$larrschedulerdayssession=$this->lobjexamtimemodel->fngetschedulerofdatesessionstudentforcurrentdate($values,$larrweekday['days'],$iddate,$venueid,$time);
		}
		else 
		{
		$larrschedulerdayssession=$this->lobjexamtimemodel->fngetschedulerofdatesessionstudent($values,$larrweekday['days'],$iddate,$venueid);
		}
		if($larrschedulerdayssession)
		{
		$larrCountrysessionDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrschedulerdayssession);
		}
		else 
		{
		$larrCountrysessionDetailss='';	
		}
		echo Zend_Json_Encoder::encode($larrCountrysessionDetailss);
}
	
}