<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');

class Examination_StartexamondateController extends Base_Base 
{
	//private $lobjanswer; //db variable
	//private $lobjform;//Form variable
	private $_gobjlogger;
	
	public function init() 
	{		
		$this->view->translate =Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->fnsetObj();
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object 
	}
	
	//Function to set the objects	
	public function fnsetObj()
	{			
		$this->lobjCommon = new App_Model_Common();
		$this->lobjCenterloginmodel = new App_Model_Centerloginnew(); //user model object
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates(); //user model object		
		$this->lobjBatchcandidatesForm = new App_Form_Batchcandidates (); //intialize user lobjuserForm
		$this->lobjAdhocApplicationForm=new App_Form_Adhocapplication();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	//function to set and display the result
	public function indexAction() 
	{    

		$presntdate=date("Y-m-d");
		
		$strSql = "SELECT DISTINCT (cnt.idcenter), cnt.centername, cnt.contactperson,std.DateTime
					FROM tbl_studentapplication std LEFT OUTER JOIN tbl_center cnt ON std.Examvenue = cnt.idcenter
					WHERE std.DateTime = '".$presntdate."' AND
					std.payment=1 order by cnt.centername"; 
		$this->view->examcenterdetailsondate = $larrcenterdetailsondate = $this->lobjCenterloginmodel->fnGetValues($strSql);

		$this->view->hur = date('H');
		$this->view->mun = date('i');
		$this->view->sur = date('s');
		$this->view->currenttime = date("H:i:s");
	}
	
	public function insertintotempAction()
	{
		
	 	$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$issession= $this->_getParam('issession');
		$idcenter = $this->_getParam('idcenter');
		$endtime = $this->_getParam('endtime');
		
		$sessionID = Zend_Session::getId();
	    $date =date('d');
		if($date<10)
		{
			$date = $date[1];
		}
		
		
		$hour = date('H');
		$minutes = date('i');
		$hours = $hour*60;
		$startedmin = $minutes+$hours;
		$presntdate = date('Y-m-d');
		$larrsessionresult = $this->lobjCenterloginmodel->fngetprogramDetails($idcenter,$issession,$presntdate);
		$todaytime = date('H:i:s');
		
		if($endtime <= $todaytime) {
			echo "0";
		} else {
			foreach($larrsessionresult as $larrsessionresults) {
				$totaltime = $this->lobjCenterloginmodel->fngetactivetotaltime($larrsessionresults['IdProgrammaster']);
				$totaltimerequired = $totaltime[0]['TimeLimit'];
				$larrresult = $this->lobjCenterloginmodel->fninsertintotemp($idcenter,$sessionID,$larrsessionresults['IdProgrammaster'],$startedmin,$totaltimerequired,$date,$issession);
			}
			echo "1";
		}
        $auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully inserted into the temp table"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$issession."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	}

 }