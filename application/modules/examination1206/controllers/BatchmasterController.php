<?php
class Examination_BatchmasterController extends Base_Base {
		
	/*private $gobjroles;//class roles global variable
	private $gobjlog;//class logwriter object global variable
	private $gobjsessionsfs; //class session global variable	
	private $gstrtranslate;//Global translation variable
	private $gstrHTMLDir;//Global String for HTML Direction
	private $gstrHTMLLang;//Global String for HTML Langauage
	private $gstrDefaultCountry;//Global String for Default Country
	private $gintPageCount;//Global integer for Pagination Count	
	private $gintDefaultCountry;//Global Integer For Default Country
	*/
	private $lobjbatchmasterModel;
	private $lobjbatchmasterForm;
	private $_gobjlogger;
   	
	/*
	 * initialization function	
	 */
	public function init() {
		
  	 	$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);  
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	    
	}
	public function fnsetObj(){
		$this->lobjbatchmasterModel = new Examination_Model_Batchmaster();
		$this->lobjbatchmasterForm = new Examination_Form_Batchmaster(); //intialize user lobjuniversityForm
	}
	/*
	 *  search form & grid display  
	 */
	public function indexAction() {
			
 		$lobjsearchform = new App_Form_Search();  //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjuserForm object to the view
				
					
		$lobjBatchmodel =  $this->lobjbatchmasterModel; //bank model object
		$date=date('Y-m-d');
		$larrresult = $lobjBatchmodel->fnGetBatchDetails($date); // get bank details
	    /* print_r($larrresult);die();*/
		$larrDropdownValues = $lobjBatchmodel->fnGetparts();  // get all 'Room Type' Definitions from DMS Table 
        $this->view->form->field5->addMultiOption('','Select');
		$this->view->form->field5->addMultiOptions($larrDropdownValues);
		
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->batchespaginatorresult);
			
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1);
		if(isset($this->gobjsessionsis->batchespaginatorresult)) {
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->batchespaginatorresult,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->view->form->isValid ( $larrformData )) {
				$date=date('Y-m-d');
				$larrresult = $lobjBatchmodel->fnSearchbatch($lobjsearchform->getValues(),$date); //searching the values for the bank
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->batchespaginatorresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect( $this->baseUrl . '/examination/batchmaster/index');
			//$this->_redirect($this->view->url(array('module'=>'examination' ,'controller'=>'batchmaster', 'action'=>'index'),'default',true));
		}
	}
        	
	/*
	 * create new bank
	 */
  	public function newbatchAction() {  				
		
        $lobjbatchForm = $this->lobjbatchmasterForm;//intialize bank form
		$this->view->form = $lobjbatchForm; 
  		$this->view->form->IdPart->addMultiOption('','Select');
  		$lobjBatchmasterModel = $this->lobjbatchmasterModel;	// model object	
		$larrDropdownValues = $lobjBatchmasterModel->fnGetparts();  // get all 'Room Type' Definitions from DMS Table 
		$this->view->form->IdPart->addMultiOptions($larrDropdownValues);
		$larrDropdownValues = $lobjBatchmasterModel->fnGetPrograms();  // get all 'Room Type' Definitions from DMS Table 
		$this->view->form->IdProgrammaster->addMultiOptions($larrDropdownValues);
        $this->view->form->UpdUser->setValue(1);
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) {			
				$larrformData = $this->_request->getPost();
				/*print_r($larrformData);die();*/
				if ($lobjbatchForm->isValid($larrformData)) {					
                	if(!array_key_exists('IdParts',$larrformData)) {
                	echo '<script language="javascript">alert("Please Add Atleast one part ");</script>';                		
                	}
                	else
                	{
					$vardsfs	= $larrformData['IdParts'];
										unset($larrformData['IdParts']);
		                                unset($larrformData['save']);		
		                                 if($larrformData['BatchStatus']==1)
		                                 {
		                                 	$larrformData['BatchStatus']=0;
		                                 }
		                                 else 
		                                 {
		                                 	$larrformData['BatchStatus']=1;
		                                 }
					$lobjbatchmodel = $this->lobjbatchmasterModel; //bank model object				   
			        $lintresult = $lobjbatchmodel->fnAddBatch($larrformData); //instance for adding the lobjuserForm values to DB
			     $lintIdBatchMaster = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_BatchMaster','IdBatch');
			     $larrformData['IdPart'] =  $vardsfs;
			       unset($larrformData['IdProgrammaster']);
			       unset($larrformData['BatchName']);
			       unset($larrformData['BatchFrom']);
			       unset($larrformData['BatchTo']);
			       unset($larrformData['BatchStatus']);
			       unset($larrformData['Save']);	
				$result = $lobjbatchmodel->fnAddBatchdetails($larrformData,$lintIdBatchMaster);
				
				$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Added the Batchmaster"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$lintIdBatchMaster."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
			
			        /*$lobjLogs = new Logs_log();*/ // object for log & insertion to log table
					//$lobjLogs->fnLogs('BANK','ADD','New Bank Add',$this->gobjsessionsfs->primaryuserid,$this->getRequest()->getServer('REMOTE_ADDR'));
					$this->_redirect( $this->baseUrl . '/examination/createexam/createexams/id/'.$lintIdBatchMaster);
					//$this->_redirect($this->view->url(array('module'=>'examination' ,'controller'=>'batchmaster', 'action'=>'index'),'default',true));
					
				}
		}
	}
  	}
	/*
	 * function for ajax 
	 */
	public function batchlistAction()
	{
		//$this->_helper->layout->disableLayout();
		$IdBatch = $this->_getParam('id');
			$lobjbatchForm = $this->lobjbatchmasterForm;//intialize bank form
		$this->view->form = $lobjbatchForm; //send the lobjuserForm object to the view
		$this->view->form->IdBatch->setValue($IdBatch);
		//$lobjInitialConfigModel = new App_Model_Initialconfig();
		//$larrresult = $lobjInitialConfigModel->fnGetInitConfigDetails($this->gobjsessionsfs->collegeId);
		$larrresult['BatchStatus'] = 0;
         $lobjbatchmodel = $this->lobjbatchmasterModel;
		$larrDropdownValues = $lobjbatchmodel->fnGetparts();  // get all 'Room Type' Definitions from DMS Table 
		$this->view->form->IdPart->addMultiOptions($larrDropdownValues);
		$larrDropdownValues = $lobjbatchmodel->fnGetPrograms();  // get all 'Room Type' Definitions from DMS Table 
		$this->view->form->IdProgrammaster->addMultiOptions($larrDropdownValues);
			if($IdBatch)
	               	{             		               	
	        			$larrresult = $lobjbatchmodel->fnViewBatch($IdBatch);   
	        			$lobjbatchForm->populate($larrresult);
	        			 if($larrresult['BatchStatus']==0)
	        			  {
	        			  	$this->view->form->BatchStatus->setChecked(1);
	        			  } 
	        			  else 
	        			  {
	        			  	$this->view->form->BatchStatus->setChecked(0);
	        			  }	
	        			$larrresultpart=$lobjbatchmodel->fnViewPart($IdBatch);
	        			//print_r($larrresultpart);die();
	        			$this->view->result=$larrresultpart;
	               	}	   
		  
        // save operation
        if ($this->_request->isPost() && $this->_request->getPost('Save')) {	
        	if ($this->_request->isPost()) {
                $larrformData = $this->_request->getPost();
                //print_r($larrformData);die();
                if ($lobjbatchForm->isValid($larrformData)) {
                	
                	//$partsids='IdParts';
                	if(!array_key_exists('IdParts',$larrformData)) {
                	echo '<script language="javascript">alert("Please Add Atleast one part ")</script>';
                		
                	}
                	else
                	{
                	if($larrformData['BatchStatus']==1)
		                                 {
		                                 	$larrformData['BatchStatus']=0;
		                                 }
		                                 else 
		                                 {
		                                 	$larrformData['BatchStatus']=1;
		                                 }
                	$lintidbank = $larrformData['IdBatch'];
                	//$where = 'IdBatch = '.$IdBatch;
                	$vardsfs	= $larrformData['IdParts'];
                	//$vardba=$larrformData['IdBatchDetails'];
                	/*if(!$vardsfs)
                	{
                		echo "<script type=javascript> alert('please add atleast one part')</script>";
                		
                	}*/
						unset($larrformData['IdParts']);
						unset($larrformData['IdBatchDetails']);
		                unset($larrformData['Save']);			   			       
                    $larrupdata=$lobjbatchmodel->fnUpdateBatch($larrformData,$lintidbank);                    
                    $larrformData['IdPart'] =  $vardsfs;
                    //print_r($vardsfs[1]);die();
                    //$larrformData['IdBatchDetails'] =  $vardba;
			      unset($larrformData['BatchName']);
			      unset($larrformData['BatchFrom']);
			      unset($larrformData['BatchTo']);
			      unset($larrformData['BatchStatus']);
			       
			        $lobjbatchmodel = $this->lobjbatchmasterModel;
			      $larrupdate= $lobjbatchmodel->fnInactiveparts($lintidbank);
			    
			       $larrInactivepart=$lobjbatchmodel->fnGetInactive($lintidbank);
			    
			       
			       for($m=0;$m<count($larrInactivepart);$m++){
			       	$nArray[$m]	= $larrInactivepart[$m]['IdPart'];
			       
			       }
			       foreach($larrformData['IdPart'] as $partsid)
			       {			       
			       		if(!in_array($partsid,$nArray)) {
			       			$result = $lobjbatchmodel->fnInsertnewpart($larrformData,$partsid);			       			
			       		}
			       		else{
			       			$result = $lobjbatchmodel->fnActiveParts($lintidbank,$partsid);
			       		}		       		
			       	}
			      
			       	
			       	$auth = Zend_Auth::getInstance();
    	    	// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully updated the batchmaster"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$lintidbank."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
			       	
			       	
                   // $lobjLogs = new Logs_log(); // object for log & insertion to log table
					//$lobjLogs->fnLogs('BANK','SAVE','Bank Edit Id='.$lintidbank,$this->gobjsessionsfs->primaryuserid,$this->getRequest()->getServer('REMOTE_ADDR'));
					$this->_redirect( $this->baseUrl . '/examination/batchmaster/index');
                  // 	$this->_redirect($this->view->url(array('module'=>'examination' ,'controller'=>'batchmaster', 'action'=>'index'),'default',true));	
				}
			}
        }
		
	}
	}
    	
	

  	
}