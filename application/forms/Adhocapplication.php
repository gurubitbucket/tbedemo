<?php
class App_Form_Adhocapplication extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
    	//$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    
		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day+1),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
	    
		    $Update = new Zend_Form_Element_Hidden('UpdDate');
        	$Update	->removeDecorator("DtDdWrapper")
        			//->setvalue($strSystemDate)
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');
        		 	 
			$UpdUser = new Zend_Form_Element_Hidden('UpdUser');
			$UpdUser->setAttrib('id','UpdUser')
					->removeDecorator("Label")
					->removeDecorator("DtDdWrapper")
				 	->removeDecorator('HtmlTag');

			$idCompany = new Zend_Form_Element_Hidden('idCompany');
			$idCompany	->removeDecorator("Label")
							->removeDecorator("DtDdWrapper")
							->removeDecorator('HtmlTag');

												
		$idPayment= new Zend_Dojo_Form_Element_FilteringSelect('ModeofPayment');
        $idPayment	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('1'=>'Fbx','2'=>'Paypal','4'=>'Check'))
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
							
		$idPrograms = new Zend_Dojo_Form_Element_FilteringSelect('idPrograms');
        $idPrograms	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->setAttrib('onChange','fngetfeedetails(this.value);') 				
							->removeDecorator("DtDdWrapper")
							->setRegisterInArrayValidator(false)
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');				
							
		
							
							
		    $eachAmounts = new Zend_Form_Element_Text('eachAmounts');
			$eachAmounts	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setAttrib('readonly','true')
						->setAttrib('class','txt_put')
						->setAttrib('style','width:93px')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 
						->setValue('0')						
						->removeDecorator('HtmlTag');				
							
							
			$noOfCand = new Zend_Form_Element_Text('noOfCand',array('regExp'=>"[0-9]+",'invalidMessage'=>"Digits Only"));
			$noOfCand	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setAttrib('class','txt_put')
						->setAttrib('style','width:93px')
						//->setAttrib('onKeyup','fncalculateSum(this);') 
						->setAttrib('onBlur','fngetDiscount(this.value)') 
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 
						->setValue('0')						
						->removeDecorator('HtmlTag');			
							
			$totalAmt = new Zend_Form_Element_Text('totalAmt');
			$totalAmt	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setAttrib('class','txt_put')
						->setAttrib('readonly','true')
						->setAttrib('style','width:93px')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 
						->setValue('0')						
						->removeDecorator('HtmlTag');		
							
			$AdhocVenue = new Zend_Form_Element_Text('AdhocVenue');
			$AdhocVenue	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','200')
						->setAttrib('required',"true")  
						->setAttrib('class','txt_put')
						->setAttrib('style','width:300px')
						->setAttrib('style','hight:800px')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 
						//->setValue('0')						
						->removeDecorator('HtmlTag');	
						
						
		$AdhocDate = new Zend_Dojo_Form_Element_DateTextBox('AdhocDate');
        $AdhocDate->setAttrib('dojoType',"dijit.form.DateTextBox");
        $AdhocDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        $AdhocDate->setAttrib('constraints', "$dateofbirth");
    	$AdhocDate->setAttrib('required',"true");
        $AdhocDate->removeDecorator("DtDdWrapper")
        	     ->setAttrib('title',"dd-mm-yyyy");
        $AdhocDate->removeDecorator("Label");
        $AdhocDate->removeDecorator('HtmlTag');
							
			$totalNoofCand = new Zend_Form_Element_Text('totalNoofCand',array('regExp'=>"[0-9]+",'invalidMessage'=>"Digits Only"));
			$totalNoofCand	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setAttrib('class','txt_put')
						->setAttrib('style','width:93px')						
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 	
						->setValue('0')			
						->removeDecorator('HtmlTag');			
							
			$grossAmt = new Zend_Form_Element_Text('grossAmt',array('regExp'=>"[0-9]+",'invalidMessage'=>"Digits Only"));
			$grossAmt	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')
						->setValue('0')
						->setAttrib('class','txt_put')
						->setAttrib('style','width:93px')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 				
						->removeDecorator('HtmlTag');		
											

		$IdRegister = new Zend_Form_Element_Text('IdRegister');
		$IdRegister	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','20')					
						->setAttrib('class','txt_put')
						->setAttrib('style','width:170px')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 											
						->removeDecorator('HtmlTag');	

		   $Newpassword = new Zend_Form_Element_Password('newpassword');
			$Newpassword	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','200')
						->setAttrib('required',"true")  
						->setAttrib('class','txt_put')
						->setAttrib('style','width:300px')
						->setAttrib('style','hight:800px')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 
						//->setValue('0')						
						->removeDecorator('HtmlTag');	
						
			$Oldpassword = new Zend_Form_Element_Password('oldpassword');
			$Oldpassword	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','200')
						->setAttrib('required',"true")  
						->setAttrib('class','txt_put')
						->setAttrib('style','width:300px')
						->setAttrib('style','hight:800px')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 
						//->setValue('0')						
						->removeDecorator('HtmlTag');	
						
						$conpassword = new Zend_Form_Element_Password('conpassword');
			$conpassword	->setAttrib('dojoType',"dijit.form.ValidationTextBox")
						->setAttrib('maxlength','200')
						->setAttrib('required',"true")  
						->setAttrib('class','txt_put')
						->setAttrib('style','width:300px')
						->setAttrib('style','hight:800px')
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label") 
						//->setValue('0')						
						->removeDecorator('HtmlTag');	
						
			
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->dojotype="dijit.form.Button";
        $Save->label =("Save");
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";	
        $Next = new Zend_Form_Element_Submit('Next');
        $Next->dojotype="dijit.form.Button";
        $Next->label =("Next");
        $Next->removeDecorator("DtDdWrapper");
        $Next->removeDecorator("Label");
        $Next->removeDecorator('HtmlTag')
         		->class = "NormalBtn";							
        //form elements
        $this->addElements(array($idCompany,$IdRegister,$idPrograms,$idPayment,$eachAmounts,$noOfCand,$totalAmt,$UpdUser,$totalNoofCand,$grossAmt,$Update,$Save,$Next,$AdhocVenue,$AdhocDate,
        $Newpassword,$Oldpassword,$conpassword,
        						
        						
                                 ));

    }
}