<?php
class App_Form_Bank extends Zend_Dojo_Form {
    public function init() {

    	$lobjCountrylist = new App_Model_Country(); // country mobel object
	    $larrCountrylist = $lobjCountrylist->fnGetCountryDetails(); // get all country
	    
	    $lobjState = new App_Model_State(); // state model object
	    $larrState = $lobjState->fnGetStateDetails(); // get all states
	    	
        $IdBank = new Zend_Form_Element_Hidden('IdBank');
        $IdBank->removeDecorator("DtDdWrapper");
        $IdBank->removeDecorator("Label");
        $IdBank->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
        $BankName = new Zend_Form_Element_Text('BankName');
        $BankName->setAttrib('class', 'txt_put');
        $BankName ->setRequired(true)
        		  ->setAttrib('maxlength','50')            
                  ->addValidator('Alpha', true, array('allowWhiteSpace' => true));                       
        $BankName->removeDecorator("DtDdWrapper");
        $BankName->removeDecorator("Label");
        $BankName->removeDecorator('HtmlTag');
        
        $BankAdd1 = new Zend_Form_Element_Text('BankAdd1');
        $BankAdd1->setAttrib('class', 'txt_put');
        $BankAdd1 ->setRequired(true)
        		  ->setAttrib('maxlength','50')
                  ->addValidators(array(array('NotEmpty')));
        $BankAdd1 ->removeDecorator("DtDdWrapper");
        $BankAdd1 ->removeDecorator("Label");
        $BankAdd1 ->removeDecorator('HtmlTag');
       
        $BankAdd2 = new Zend_Form_Element_Text('BankAdd2');	
        $BankAdd2->setAttrib('class', 'txt_put');
        $BankAdd2->setAttrib('maxlength','50');
        $BankAdd2->removeDecorator("DtDdWrapper");
        $BankAdd2->removeDecorator("Label");
        $BankAdd2->removeDecorator('HtmlTag');
                  
        $City = new Zend_Form_Element_Text('City');	
        $City->setAttrib('class', 'txt_put');
        $City->setAttrib('maxlength','20');
        $City->removeDecorator("DtDdWrapper");
        $City->removeDecorator("Label");
        $City->removeDecorator('HtmlTag');
        $City->addValidator('Alpha', true, array('allowWhiteSpace' => true));        
                  
        
                    
        $Country = new Zend_Form_Element_Select('Country');	
        $Country->setAttrib('class', 'txt_put MakeEditable');
        $Country->addMultiOptions($larrCountrylist);
		$Country->setAttrib('maxlength','20');
		$Country-> setRegisterInArrayValidator(false);
        $Country->removeDecorator("DtDdWrapper");
        $Country->removeDecorator("Label");
        $Country->removeDecorator('HtmlTag')
        		->setAttrib('ComboBoxOnChange', 'fnGetPermCountryStateList')
        		->addMultiOptions(array('' => 'Select')) 
			->setAttrib('style', 'width:150px'); 
			
		$State = new Zend_Form_Element_Select('State');	
        $State->setAttrib('class', 'txt_put MakeEditable');  
        $State->setAttrib('maxlength','50');      
        $State->removeDecorator("DtDdWrapper");
        $State->removeDecorator("Label");
        $State->removeDecorator('HtmlTag')
        		->addMultiOptions(array('' => 'Select'))
        		->setRegisterInArrayValidator(false)
				->setAttrib('style', 'width:150px'); 				
                                            
        $Phone = new Zend_Form_Element_Text('Phone');	
        $Phone->setAttrib('class', 'txt_put')
        		//->setAttrib('onkeypress','return ValidateNumericInput(this, event)')
        		->setAttrib('onKeyPress','return fnContactNoInputOnly(this.value,event)')
         		//->addValidators(array(array('Digits')))
        		->setAttrib('maxlength','20');
        $Phone->removeDecorator("DtDdWrapper");
        $Phone->removeDecorator("Label");
        $Phone->removeDecorator('HtmlTag');
                  
        $Fax = new Zend_Form_Element_Text('Fax');	
        $Fax->setAttrib('class', 'txt_put')
        	//->setAttrib('onkeypress','return ValidateNumericInput(this, event)')
        	->setAttrib('onKeyPress','return fnContactNoInputOnly(this.value,event)')
        	//->addValidators(array(array('Digits')))
        	->setAttrib('maxlength','20');       
        $Fax->removeDecorator("DtDdWrapper");
        $Fax->removeDecorator("Label");
        $Fax->removeDecorator('HtmlTag');
        
        $Email = new Zend_Form_Element_Text('Email');
        $Email->setAttrib('class', 'txt_put')
        	  ->setAttrib('maxlength','50');  
        $Email->setRequired(true)
                       ->addValidators(array(array('EmailAddress')));
        $Email->removeDecorator("DtDdWrapper");
        $Email->removeDecorator("Label");
        $Email->removeDecorator('HtmlTag');
        
        $URL = new Zend_Form_Element_Text('URL');	
        $URL->setAttrib('class', 'txt_put')
        	->setAttrib('maxlength','20');        
        $URL->removeDecorator("DtDdWrapper");
        $URL->removeDecorator("Label");
        $URL->removeDecorator('HtmlTag'); 

        
        $ContactPerson = new Zend_Form_Element_Text('ContactPerson');	
        $ContactPerson->setAttrib('class', 'txt_put')
        			->setAttrib('maxlength','20');        
        $ContactPerson->removeDecorator("DtDdWrapper");
        $ContactPerson->removeDecorator("Label");
        $ContactPerson->removeDecorator('HtmlTag');
        $ContactPerson->addValidator('Alpha', true, array('allowWhiteSpace' => true));
              
              
        $Desgination= new Zend_Form_Element_Text('Desgination');	
        $Desgination->setAttrib('class', 'txt_put') 
        			->setAttrib('maxlength','20');       
        $Desgination->removeDecorator("DtDdWrapper");
        $Desgination->removeDecorator("Label");
        $Desgination->removeDecorator('HtmlTag');
        $Desgination->addValidator('Alpha', true, array('allowWhiteSpace' => true));
              
              
        $ContactPhone = new Zend_Form_Element_Text('ContactPhone');
        $ContactPhone->setAttrib('class', 'txt_put')
        			// ->setAttrib('onkeypress','return ValidateNumericInput(this, event)')
        			->setAttrib('onKeyPress','return fnContactNoInputOnly(this.value,event)')
        			// ->addValidators(array(array('Digits')))
        			 ->setAttrib('maxlength','20');       
        $ContactPhone->removeDecorator("DtDdWrapper");
        $ContactPhone->removeDecorator("Label");
        $ContactPhone->removeDecorator('HtmlTag');
                          
        $ContactCell = new Zend_Form_Element_Text('ContactCell');
        $ContactCell->setAttrib('class', 'txt_put')
        			//->setAttrib('onkeypress','return ValidateNumericInput(this, event)')
        			->setAttrib('onKeyPress','return fnContactNoInputOnly(this.value,event)')
        			//->addValidators(array(array('Digits')))
        			->setAttrib('maxlength','20'); 
        $ContactCell->removeDecorator("DtDdWrapper");
        $ContactCell->removeDecorator("Label");
        $ContactCell->removeDecorator('HtmlTag');
        
        $Active = new Zend_Form_Element_Checkbox('Active');       
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        $Active->setValue('1');
                          
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator("Label");
        $Save->removeDecorator('HtmlTag');
        $Save->class ="buttonStyle";
        
        $Close = new Zend_Form_Element_Submit('Close');
		$Close->setAttrib('class', 'buttonStyle')
			->setAttrib('onclick', 'fnCloseLyteBox()')
			->removeDecorator("Label")
			->removeDecorator("DtDdWrapper")
			->removeDecorator('HtmlTag');
       
        $micr = new Zend_Form_Element_Text('MICR');
        $micr->setAttrib('class', 'txt_put')        			 
        			 ->setAttrib('maxlength','20');       
        $micr->removeDecorator("DtDdWrapper");
        $micr->removeDecorator("Label");
        $micr->removeDecorator('HtmlTag');
        
        $bankcode1 = new Zend_Form_Element_Text('BKCode1');
        $bankcode1->setAttrib('class', 'txt_put')        			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $bankcode2 = new Zend_Form_Element_Text('BKCode2');
        $bankcode2->setAttrib('class', 'txt_put')        			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
         

        $this->addElements(array($IdBank,$BankName,$BankAdd1,
                                 $BankAdd2,$City,
                                 $State,$Country,
                                 $Phone,$Fax,$Email,
                                 $URL,$ContactPerson,$Desgination,
                                 $ContactPhone,$ContactCell,$Active,$Save,$Close,$UpdDate,$UpdUser,
                                 $micr, $bankcode1, $bankcode2));
    }
}