<?php
class App_Form_Companymaster extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	//$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$IdCompany= new Zend_Form_Element_Hidden('IdCompany');
        $IdCompany->removeDecorator("DtDdWrapper");
        $IdCompany->removeDecorator("Label");
        $IdCompany->removeDecorator('HtmlTag');
        
        $TakafulName = new Zend_Form_Element_Text('CompanyName');
        $TakafulName->setAttrib('style','width:255px;');
		$TakafulName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$TakafulName->setAttrib('required',"true")  
        	     ->setAttrib('propercase','true')
                ->setAttrib('onBlur','duplicate(this.value)')     			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
        $TakafulShortName = new Zend_Form_Element_Text('ShortName');	
        $TakafulShortName->setAttrib('style','width:230px;');
		$TakafulShortName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $TakafulShortName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');   

 		$paddr1 = new Zend_Form_Element_Text('RegistrationNo');
 		$paddr1->setAttrib('style','width:230px;');
		$paddr1->setAttrib('dojoType',"dijit.form.ValidationTextBox")
                 ->setvalue('XXX-XXX-XXX');       			 
        $paddr1	->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
       $paddr2 = new Zend_Form_Element_Text('Address');	
       $paddr2	->setAttrib('maxlength','100')
        				->setAttrib('dojoType',"dijit.form.Textarea")
        				->setAttrib('style','width:auto;')
        				->setRequired(true)
        				->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag');	

        $businesstype = new Zend_Dojo_Form_Element_FilteringSelect('businesstype');
        $businesstype->setAttrib('style','width:230px;');
        $businesstype->removeDecorator("DtDdWrapper");
        $businesstype->setAttrib('required',"true") ;
        $businesstype->removeDecorator("Label");
        $businesstype->removeDecorator('HtmlTag');
		$businesstype->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		
        $question = new Zend_Dojo_Form_Element_FilteringSelect('question');
        $question->removeDecorator("DtDdWrapper");
        $question->setAttrib('required',"true") ;
        $question->removeDecorator("Label");
        $question->removeDecorator('HtmlTag');
		$question->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		
        $answer = new Zend_Form_Element_Text('answer');	
		$answer->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $answer->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','20')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');   
		
        		
        		
      /*  $city = new Zend_Form_Element_Text('City');
		$city->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $city->setAttrib('maxlength','20')     
               ->setAttrib('required',"true") 		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');*/
        		
        
        		
       
        
        //$country = new Zend_Dojo_Form_Element_FilteringSelect('IdCountry');
        $country = new Zend_Form_Element_Text('IdCountry');
        $country->setAttrib('style','width:230px;');
        $country->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $country->removeDecorator("DtDdWrapper");
        $country->setAttrib('required',"true") ;
        $country->removeDecorator("Label");
        $country->removeDecorator('HtmlTag');
        //$country->setAttrib('OnChange', 'fnGetCountryStateList');
       // $country->setRegisterInArrayValidator(false);
		//$country->setAttrib('dojoType',"dijit.form.FilteringSelect");
        
       
        $state = new Zend_Dojo_Form_Element_FilteringSelect('IdState');
        $state->setAttrib('style','width:230px;');
        $state->removeDecorator("DtDdWrapper");
        $state->setAttrib('required',"true") ;
        $state->removeDecorator("Label");
        $state->removeDecorator('HtmlTag');
        $state->setRegisterInArrayValidator(false);
		$state->setAttrib('dojoType',"dijit.form.FilteringSelect");
		 $state->setAttrib('OnChange', 'fnGetCityList');  
              
        
		 
		 		
        $city = new Zend_Dojo_Form_Element_FilteringSelect('City');
        $city->setAttrib('style','width:230px;');
		$city->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $city->setAttrib('maxlength','20')     
               ->setAttrib('required',"true") 		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        $city->setRegisterInArrayValidator(false);
        $city->setAttrib('dojoType',"dijit.form.FilteringSelect"); 
        
        
        $zipCode = new Zend_Form_Element_Text('Postcode',array('regExp'=>"[0-9]*",'invalidMessage'=>"5 Numerical Digits Only"));
        $zipCode->setAttrib('style','width:230px;');
		$zipCode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $zipCode->setAttrib('maxlength','5');   
        $zipCode->removeDecorator("DtDdWrapper");
        $zipCode->removeDecorator("Label");
        $zipCode->removeDecorator('HtmlTag');
        
       /* $workcountrycode = new Zend_Form_Element_Text('workcountrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$workcountrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workcountrycode->setAttrib('maxlength','3')
                         ->setAttrib('required',"true");
        $workcountrycode->setAttrib('style','width:30px');  
        $workcountrycode->removeDecorator("DtDdWrapper");
        $workcountrycode->removeDecorator("Label");
        $workcountrycode->removeDecorator('HtmlTag');
        
        $workstatecode = new Zend_Form_Element_Text('workstatecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$workstatecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workstatecode->setAttrib('maxlength','5')
                      ->setAttrib('required',"true");  
        $workstatecode->setAttrib('style','width:30px');  
        $workstatecode->removeDecorator("DtDdWrapper");
        $workstatecode->removeDecorator("Label");
        $workstatecode->removeDecorator('HtmlTag');
        */
        
        
 		$workPhone = new Zend_Form_Element_Text('Phone1',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Work Phone No."));
 		$workPhone->setAttrib('style','width:230px;');
		$workPhone->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $workPhone->setAttrib('maxlength','15');   
        $workPhone->setAttrib('required',"true");   
        $workPhone->removeDecorator("DtDdWrapper");
        $workPhone->removeDecorator("Label");
        $workPhone->removeDecorator('HtmlTag');

      /*  
        $countrycode = new Zend_Form_Element_Text('countrycode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$countrycode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $countrycode->setAttrib('maxlength','3');  
        $countrycode->setAttrib('style','width:30px');
                   //  ->setAttrib('required',"true");   
        $countrycode->removeDecorator("DtDdWrapper");
        $countrycode->removeDecorator("Label");
        $countrycode->removeDecorator('HtmlTag');
        
        $statecode = new Zend_Form_Element_Text('statecode',array('regExp'=>"[0-9]+",'invalidMessage'=>"Only digits"));
		$statecode->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $statecode->setAttrib('maxlength','5');  
        $statecode->setAttrib('style','width:30px');
                 // ->setAttrib('required',"true");  
        $statecode->removeDecorator("DtDdWrapper");
        $statecode->removeDecorator("Label");
        $statecode->removeDecorator('HtmlTag');*/
        
        
        $Phone2 = new Zend_Form_Element_Text('Phone2',array('regExp'=>"[0-9()+-]+",'invalidMessage'=>"Not a valid Work Phone No."));
        $Phone2->setAttrib('style','width:230px;');
		$Phone2->setAttrib('dojoType',"dijit.form.ValidationTextBox");	
        $Phone2->setAttrib('maxlength','20');    
        $Phone2->removeDecorator("DtDdWrapper");
        $Phone2->removeDecorator("Label");
        $Phone2->removeDecorator('HtmlTag'); 

        $email = new Zend_Form_Element_Text('Email',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
        $email->setAttrib('style','width:230px;');
		//$email->addValidator(new Zend_Validate_Db_NoRecordExists('tbl_companies', 'Email'));
        $email->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $email->setAttrib('required',"true")  	
        //->setAttrib('onBlur','duplicateemail(this.value)') 		 
        		->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');  

        		
        $altemail = new Zend_Form_Element_Text('AltEmail',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
        $altemail->setAttrib('style','width:230px;');
		$altemail->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $altemail->setAttrib('maxlength','50')         		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');        
        		
        		
        		
       	$Fax = new Zend_Form_Element_Text('Fax');
       	$Fax->setAttrib('style','width:230px;');
		$Fax->setAttrib('dojoType',"dijit.form.ValidationTextBox")  
		                ->setAttrib('required',"true")  			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');
        				
        				
        				
        $Contactperson = new Zend_Form_Element_Text('ContactPerson');	
        $Contactperson->setAttrib('style','width:230px;');
		$Contactperson->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Contactperson->setAttrib('required',"true")  	 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        				
       	$Login = new Zend_Form_Element_Text('Login');
       	$Login->setAttrib('style','width:230px;');
		$Login->setAttrib('dojoType',"dijit.form.ValidationTextBox")   
		               ->setAttrib('onBlur','duplicateusername(this.value)')  		
		               ->setAttrib('required',"true")  		
		             //  ->setAttrib ( 'readonly',"true") 	 
        				->setAttrib('maxlength','50')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$Password = new Zend_Form_Element_Password('Password');
		$Password->setAttrib('dojoType',"dijit.form.ValidationTextBox") 
		                ->setAttrib('required',"true")    			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

       	$ConPassword = new Zend_Form_Element_Password('ConPassword');
		$ConPassword->setAttrib('dojoType',"dijit.form.ValidationTextBox")   
		               ->setAttrib('required',"true")  			 
        				->setAttrib('maxlength','20')       
        				->removeDecorator("DtDdWrapper")
        	    		->removeDecorator("Label")
        				->removeDecorator('HtmlTag');

        				
        
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label ="Save";
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
         		
        $Back = new Zend_Form_Element_Button('Back');
        //$Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');         		
    
        $this->addElements(array($IdCompany,$TakafulName,$TakafulShortName,$answer,
        							$paddr1,$paddr2,$city,$altemail,$country,$state,$zipCode,
        							$workPhone,$Phone2,$businesstype,			
        	                    $Fax,$Login,$Password,$ConPassword,$email,$question,
        						$Contactperson,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $Back));

    }
}