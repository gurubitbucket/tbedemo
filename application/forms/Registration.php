<?php

class App_Form_Registration extends Zend_Dojo_Form 
{		
    public function init()
    {             
    	$strSystemDate = date('Y-m-d');  
         $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
         $UpdDate->removeDecorator("DtDdWrapper");
         $UpdDate->removeDecorator("Label");
         $UpdDate->removeDecorator('HtmlTag');
     
         $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
         $UpdUser->removeDecorator("DtDdWrapper");
         $UpdUser->removeDecorator("Label");
         $UpdUser->removeDecorator('HtmlTag');
        
        

                   
        $id = new Zend_Form_Element_Text('id');
		$id->setAttrib('dojoType',"dijit.form.ValidationTextBox");
                $id->setAttrib('class', 'txt_put') ;
                $id->setAttrib('required',"true")      
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        $Date = new Zend_Form_Element_Hidden('Date');
		$Date->setAttrib('dojoType',"dijit.form.TextBox");
                $Date->setAttrib('class', 'txt_put') ;
                $Date->setValue($strSystemDate)     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                  
      
	         
      $Login = $this->createElement('submit','Login');
          $Login->dojotype="dijit.form.Button";
          $Login->label = "Login";	
          $Login->removeDecorator("DtDdWrapper");
          $Login->class = "NormalBtn";
         
          $Accept = new Zend_Form_Element_Submit('Start');
         $Accept->setAttrib('class', 'buttonStyle');
         //$Accept->setAttrib('onClick','questiondisplay()');
         $Accept->removeDecorator("DtDdWrapper");
         $Accept->removeDecorator("Label");
         $Accept->removeDecorator('HtmlTag');

         
      $Back = $this->createElement('button','Back');
          $Back->dojotype="dijit.form.Button";
          $Back->label = "Back";	
          $Back->removeDecorator("DtDdWrapper");
          $Back->setAttrib('onClick','goback()');
          $Back->class = "NormalBtn";
         
          $Reset = $this->createElement('Reset','Reset');
          $Reset->dojotype="dijit.form.Button";
          $Reset->label = "Reset";
          $Reset->removeDecorator("DtDdWrapper");
          $Reset->class = "NormalBtn";
        
         $this->addElements(array($UpdDate,$UpdUser,$Date,
         						 $id,$Login,$Reset,$Back,$Accept));
    }
}
        
        