<?php
class App_Form_Textfilesaveform extends Zend_Dojo_Form { //Formclass for the user module
    public function init() {
			$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    
		    $EmailAddress = new Zend_Form_Element_Text('EmailAddress',array('regExp'=>"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$",'invalidMessage'=>"Not a valid email"));
			$EmailAddress->setAttrib('dojoType',"dijit.form.ValidationTextBox");
            $EmailAddress->setAttrib('class', 'txt_put')     			 
						->setAttrib('maxlength','50')  
						->setAttrib('required',"true") 	      		     
						->removeDecorator("DtDdWrapper")
						->removeDecorator("Label")
						->removeDecorator('HtmlTag');				
					
							
							
			$Save = new Zend_Form_Element_Submit('Save');
			$Save->dojotype="dijit.form.Button";
			$Save->label =("Save");
			$Save->removeDecorator("DtDdWrapper");
			$Save->removeDecorator("Label");
			$Save->removeDecorator('HtmlTag')
				 ->class = "NormalBtn";	
        
			$Clear = new Zend_Form_Element_Submit('Clear');
			$Clear->dojotype="dijit.form.Button";
			$Clear->label =("Clear");
			$Clear->removeDecorator("DtDdWrapper");
			$Clear->removeDecorator("Label");
			$Clear->removeDecorator('HtmlTag')        	
         		  ->class = "NormalBtn";	
         		
			$this->addElements(array(
									  $EmailAddress,
									  $Save,
									  $Clear
                                 ));
    }
}
