<?php
	class App_Form_Reports extends Zend_Dojo_Form {
		public function init() {
	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
				$Coursename = new Zend_Dojo_Form_Element_FilteringSelect('Coursename');
			    $Coursename->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Coursename->addMultiOption('','Select'); 	           	         		       		     
	            $Coursename->removeDecorator("DtDdWrapper");
	            $Coursename->removeDecorator("Label");
	            $Coursename->removeDecorator('HtmlTag');   
	            
	            $Companyname = new Zend_Dojo_Form_Element_FilteringSelect('Companyname');
			    $Companyname->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Companyname->addMultiOption('','Select'); 	           	         		       		     
	            $Companyname->removeDecorator("DtDdWrapper");
	            $Companyname->removeDecorator("Label");
	            $Companyname->removeDecorator('HtmlTag');   

	            $Takafulname = new Zend_Dojo_Form_Element_FilteringSelect('Takafulname');
			    $Takafulname->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Takafulname->addMultiOption('','Select'); 	           	         		       		     
	            $Takafulname->removeDecorator("DtDdWrapper");
	            $Takafulname->removeDecorator("Label");
	            $Takafulname->removeDecorator('HtmlTag');   
	            
	            $Result = new Zend_Dojo_Form_Element_FilteringSelect('Result');
			    $Result->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Result->addMultiOptions(array(''=>'Select','3'=>'Applied','1'=>'Pass','2'=>'Fail')); 	           	         		       		     
	            $Result->removeDecorator("DtDdWrapper");
	            $Result->removeDecorator("Label");
	            $Result->removeDecorator('HtmlTag');  
	            
	            $ICNO = new Zend_Form_Element_Text('ICNO');
			    $ICNO->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $ICNO->setAttrib('class','txt_put');  
	            $ICNO->removeDecorator("DtDdWrapper");
	            $ICNO->removeDecorator("Label");
	            $ICNO->removeDecorator('HtmlTag');
	            
	            $Studentname = new Zend_Form_Element_Text('Studentname');
			    $Studentname->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $Studentname->setAttrib('class','txt_put');  
	            $Studentname->removeDecorator("DtDdWrapper");
	            $Studentname->removeDecorator("Label");
	            $Studentname->removeDecorator('HtmlTag');
	            
	            $Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	        	$Date->setAttrib('dojoType',"dijit.form.DateTextBox");
	       	 	$Date->removeDecorator("DtDdWrapper");
				$Date->setAttrib('title',"dd-mm-yyyy");
	       	    $Date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	        	$Date->removeDecorator("Label");
	        	$Date->removeDecorator('HtmlTag'); 
	        	//$Date->setAttrib('required',"true");
	            
	            $Venue = new Zend_Form_Element_Text('Venue');
			    $Venue->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $Venue->setAttrib('class','txt_put');  
	            $Venue->removeDecorator("DtDdWrapper");
	            $Venue->removeDecorator("Label");
	            $Venue->removeDecorator('HtmlTag');
	            
	            $Venues = new Zend_Dojo_Form_Element_FilteringSelect('Venues');
			    $Venues->setAttrib('dojoType',"dijit.form.FilteringSelect");
	            $Venues->addMultiOption('','Select');  
	            $Venues->removeDecorator("DtDdWrapper");
	            $Venues->removeDecorator("Label");
	            $Venues->removeDecorator('HtmlTag');
	           // $Venues->setAttrib('required',"true");
	            
	            $State = new Zend_Form_Element_Text('State');
			    $State->setAttrib('dojoType',"dijit.form.ValidationTextBox");
	            $State->setAttrib('class','txt_put');  
	            $State->removeDecorator("DtDdWrapper");
	            $State->removeDecorator("Label");
	            $State->removeDecorator('HtmlTag');
			
				$FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
	        	$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	       	 	$FromDate->removeDecorator("DtDdWrapper")
						 ->setAttrib('title',"dd-mm-yyyy")
	       				 ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
	        	$FromDate->removeDecorator("Label");
	        	$FromDate->removeDecorator('HtmlTag'); 
        				
			  
				$ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
	        	$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox");
	       	 	$ToDate->removeDecorator("DtDdWrapper")
						->setAttrib('title',"dd-mm-yyyy")
	       				 ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");;
	        	$ToDate->removeDecorator("Label");
	        	$ToDate->removeDecorator('HtmlTag'); 
         	 
			/*-----------COMMON FIELDS FOR REPORT & NOT TO BE CHANGED-----------*/	
			//Common Form Elements For Report Generation
			
			
    	$Generate = new Zend_Form_Element_Submit('Generate');
		$Generate->dojotype="dijit.form.Button";
        $Generate->setAttrib('class', 'NormalBtn')
        		  ->removeDecorator("DtDdWrapper")
	        	  ->removeDecorator("Label")
    	    	   ->removeDecorator('HtmlTag');
		$Generate->label = $gstrtranslate->_("Generate");
    	    			
    	    			
		$Print = new Zend_Form_Element_Button('Print');
		$Print->dojotype="dijit.form.Button";
        $Print->label = "Print";
        $Print->setAttrib('id', 'Print')
        		->setAttrib('class', 'NormalBtn')
        		->setAttrib('onclick', 'GenerateReport()')
        		->removeDecorator("DtDdWrapper")
	        	->removeDecorator("Label")
    	    	->removeDecorator('HtmlTag');
        
        $ColumnFilter = new Zend_Form_Element_MultiCheckbox('ColumnFilter');
	    $ColumnFilter->setAttrib('onclick','ToggleTableHeaders(this.value)')
    	    		  ->setAttrib('id','ColumnFilter')
    	    		  ->removeDecorator("DtDdWrapper")
        			  ->removeDecorator("Label")
        			  ->removeDecorator('HtmlTag');
        					
        $ColumnOdrerBy = new Zend_Form_Element_Select('ColumnOdrerBy');
	    $ColumnOdrerBy->setAttrib('onChange','ToggleTableHeaders(this.value)')
    	    			->setAttrib('id','ColumnOdrerBy')
						->removeDecorator("DtDdWrapper")
        				->removeDecorator("Label")
        				->removeDecorator('HtmlTag')
        				->addMultiOptions(array(''=>''));
        					
        	
        $ReportType = new Zend_Dojo_Form_Element_FilteringSelect('ReportType');	
        $ReportType->addMultiOptions(array('pdf' => 'PDF',
									   'excel' => 'Excel'))
        		   ->setAttrib('id','ColumnOdrerBy')
	        	   ->setAttrib('class', 'MakeEditable');
        $ReportType->removeDecorator("DtDdWrapper");
        $ReportType->removeDecorator("Label");
        $ReportType->removeDecorator('HtmlTag');
		$ReportType->setAttrib('dojoType',"dijit.form.FilteringSelect");
 			
        				
        $Export = new Zend_Form_Element_Submit('Export');
        $Export->dojotype="dijit.form.Button";
        $Export->label = $gstrtranslate->_("Export");
        $Export	->setAttrib('id', 'Export')
        		->setAttrib('class', 'NormalBtn')
        		->setAttrib('onclick', 'ExportReport()')
        		->removeDecorator("DtDdWrapper")
	        	->removeDecorator("Label")
    	    	->removeDecorator('HtmlTag');
		    			
			
			$this->addElements(
        					array($Coursename,
        					      $Companyname,
        						  $ICNO,
        					      $Studentname,
        					      $Venue,
        					      $Venues,
        					      $Date,
        					      $State,
        					      $Takafulname,
        					      $Result,
        					      $FromDate,
        					      $ToDate,
        					      $Generate,
        					      $Print,
        						  $ColumnFilter,	
        						  $ColumnOdrerBy,
        						  $ReportType,
        						  $Export
        						)
        			);
		}
}
