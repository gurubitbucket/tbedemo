<?php
class App_Form_Takafulcandidates extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	//$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$IdCompany= new Zend_Form_Element_Hidden('IdCompany');
        $IdCompany->removeDecorator("DtDdWrapper");
        $IdCompany->removeDecorator("Label");
        $IdCompany->removeDecorator('HtmlTag');
        
        $CandidateName = new Zend_Form_Element_Text('CandidateName');	
		$CandidateName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $CandidateName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
  $Save = new Zend_Form_Element_Submit('Save');
        $Save->label ="Save";
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";		
    
        $this->addElements(array($IdCompany,$CandidateName,$Save));

    }
}