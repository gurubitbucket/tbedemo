<?php
class App_Form_Search extends Zend_Dojo_Form {
	
    public function init() {
	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	
	  $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
	    
		$month3= 03; // Month value
		$day3=  21; //today's date
		$year3=2012; // Year value
		
		
		$yesterdaydate3= date('Y-m-d', mktime(0,0,0,$month3,($day3),$year3));
		$dateofbirth3 = "{min:'$yesterdaydate3',datePattern:'dd-MM-yyyy'}"; 
		
    	
        /*$field1 = new Zend_Form_Element_Select('field1');
        $field1->addMultiOption('','Select');
        $field1->setAttrib('class', 'txt_put MakeEditable');
        $field1->setAttrib('style','width:150px;');
        $field1->removeDecorator("DtDdWrapper");
        $field1->removeDecorator("Label");
        $field1->removeDecorator('HtmlTag');
        */
		
		
		$HiddenField1 = new Zend_Form_Element_Hidden('HiddenField1');
        $HiddenField1->removeDecorator("DtDdWrapper");
        $HiddenField1->removeDecorator("Label");
        $HiddenField1->removeDecorator('HtmlTag');
        
        $field1 = new Zend_Dojo_Form_Element_FilteringSelect('field1');
        $field1->setAttrib('dojoType',"dijit.form.FilteringSelect"); 
        $field1->removeDecorator("DtDdWrapper");        
        $field1->removeDecorator("Label");
        $field1->removeDecorator('HtmlTag');
        $field1->setAttrib('required',"false");
        //$field1->setAttrib('OnChange', 'fnGetDetails');
        $field1->setRegisterInArrayValidator(false);        
		

       
        $field2 = new Zend_Form_Element_Text('field2');
		$field2 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field2->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        $field3 = new Zend_Form_Element_Text('field3');
        $field3 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
       // $field3->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
              $field3->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                
        $field4 = new Zend_Form_Element_Text('field4');
		$field4->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field4->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');

        $field5 = new Zend_Dojo_Form_Element_FilteringSelect('field5');        
        $field5->removeDecorator("DtDdWrapper");
        $field5->setAttrib('required',"false");
        //$field5->addMultiOption('','Select');
        $field5->removeDecorator("Label");
        $field5->removeDecorator('HtmlTag');
        //$field5->setAttrib('OnChange', 'fnGetDetails')
        //->setAttrib('class', 'txt_put') ;
        //$field5->setRegisterInArrayValidator(false);        
		$field5->setAttrib('dojoType',"dijit.form.FilteringSelect");			
        		
        		
        /*		
        $field5 = new Zend_Form_Element_Select('field5');
        $field5->setAttrib('style','width:150px;');
        $field5->addMultiOption('','Select');
        $field5->setAttrib('class', 'txt_put MakeEditable');
        $field5->removeDecorator("DtDdWrapper");
        $field5->removeDecorator("Label");
        $field5->removeDecorator('HtmlTag');
        */
        
        
           
        $field6 = new Zend_Form_Element_Text('field6');
		$field6->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field6->setAttrib('class', 'txt_put')        
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
                
        $field7  = new Zend_Form_Element_Checkbox('field7');
        $field7->setAttrib('dojoType',"dijit.form.CheckBox");
        $field7->setvalue('1');
        $field7->removeDecorator("DtDdWrapper");
        $field7->removeDecorator("Label");
        $field7->removeDecorator('HtmlTag');
        
        
        $field8 = new Zend_Dojo_Form_Element_FilteringSelect('field8');        
       // $field8 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field8 ->removeDecorator("DtDdWrapper");
        //$field8 ->addMultiOption('','Select');
        $field8 ->removeDecorator("Label");        
        $field8 ->removeDecorator('HtmlTag');
       // ->setAttrib('class', 'txt_put') ;
       // $field8 ->setAttrib('OnChange', 'fnGetDetails');
        //$field8 ->setRegisterInArrayValidator(false);
		$field8 ->setAttrib('dojoType',"dijit.form.FilteringSelect");	
        
     /*   $field8 = new Zend_Form_Element_Select('field8');
        $field8->addMultiOption('','Select');
        $field8->setAttrib('class', 'txt_put');
        $field8->setAttrib('style','width:150px;');
        $field8->removeDecorator("DtDdWrapper");
        $field8->removeDecorator("Label");
        $field8->removeDecorator('HtmlTag'); */

        $field10 = new Zend_Dojo_Form_Element_DateTextBox('field10');
        $field10->setAttrib('dojoType',"dijit.form.DateTextBox");
        $field10->setAttrib('class', 'txt_put');
        $field10->removeDecorator("DtDdWrapper")
        	    ->setAttrib('title',"dd-mm-yyyy");
        $field10->removeDecorator("Label");
        $field10->removeDecorator('HtmlTag');
        $field10->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        
        
        $field11 = new Zend_Dojo_Form_Element_DateTextBox('field11');
         $field11->  setAttrib('dojoType',"dijit.form.DateTextBox");
        $field11->removeDecorator("DtDdWrapper");
        $field11->setAttrib('class', 'txt_put')
        	->setAttrib('title',"dd-mm-yyyy");
        $field11->removeDecorator("Label");
        $field11->removeDecorator('HtmlTag');
        $field11->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        
        $field12 = new Zend_Form_Element_Text('field12');
		$field12 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field12->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag'); 
        		
       	$field13 = new Zend_Form_Element_Text('field13');
		$field13 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field13->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');          
        					
        $field14 = new Zend_Form_Element_Radio('field14');       
        $field14->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			->setAttrib('dojoType',"dijit.form.RadioButton")
        			->setValue('1')				
					->setSeparator('');
				
					
		$field15 =new Zend_Form_Element_Select('field15');        
        $field15->removeDecorator("DtDdWrapper");
        $field15->addMultiOption('','Select');
        $field15->removeDecorator("Label");
        $field15->removeDecorator('HtmlTag');
        $field15->setAttrib('OnChange', 'fnGetDetails');
        $field15->	setAttrib('required',"true"); 
        $field15->setRegisterInArrayValidator(false);
		$field15->setAttrib('dojoType',"dijit.form.FilteringSelect");			
					
		/*$field15 = new Zend_Form_Element_Select('field15');
        $field15->addMultiOption('','Select');
        $field15->setAttrib('class', 'txt_put');
        $field15->setAttrib('style','width:150px;');
        $field15->removeDecorator("DtDdWrapper");
        $field15->removeDecorator("Label");
        $field15->removeDecorator('HtmlTag');*/
				
				
	/*	$field16 = new Zend_Form_Element_Radio('field16');       
        $field16->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag')
        			->addMultiOptions(array('1' => 'Name','0' => 'Id'))
					->setValue('1')				
					->setSeparator('')
					->setAttrib('onclick', 'fnToggleStudentDetails(this.value)');*/
					
		$field16  = new Zend_Form_Element_Radio('field16');
		$field16->setAttrib('dojoType',"dijit.form.RadioButton");
        $field16->addMultiOptions(array('1' => 'Name','0' => 'Id'))
        			->setvalue('1')
        			->setSeparator('&nbsp;')
        			->setAttrib('onClick','fnToggleStudentDetails(this.value)')
        			->removeDecorator("DtDdWrapper")
        			->removeDecorator("Label")
        			->removeDecorator('HtmlTag');
        						
					
					
        				
      /*  $field17 = new Zend_Form_Element_Select('field17');
        $field17->addMultiOption('','Select');
        $field17->setAttrib('class', 'txt_put MakeEditable');
        $field17->setAttrib('ComboBoxOnChange','fnGetDetails');
        $field17->setAttrib('style','width:150px;');
        $field17->removeDecorator("DtDdWrapper");
        $field17->removeDecorator("Label");
        $field17->removeDecorator('HtmlTag');*/
        
        
        $field17 = new Zend_Dojo_Form_Element_FilteringSelect('field17');        
        $field17->removeDecorator("DtDdWrapper");
        $field17->addMultiOption('','Select');
        $field17->removeDecorator("Label");
        $field17->removeDecorator('HtmlTag');
        $field17->setAttrib('OnChange', 'fnGetDetails')
        		->setAttrib('class', 'txt_put');
        $field17->setRegisterInArrayValidator(false);
		$field17->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
        
       /* $field18 = new Zend_Form_Element_Text('field18');
        $field18->setAttrib('class', 'txt_put');
        $field18->setAttrib('style','width:150px;');
        $field18->setAttrib('readonly',true);
        $field18->removeDecorator("DtDdWrapper");
        $field18->removeDecorator("Label");
        $field18->removeDecorator('HtmlTag');	*/	

        $field18 = new Zend_Form_Element_Text('field18');
		$field18->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $field18->setAttrib('class', 'txt_put')   			         		       		     
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
  	
       	 $field19 = new Zend_Dojo_Form_Element_FilteringSelect('field19');        
         $field19 ->removeDecorator("DtDdWrapper");
         $field19 ->addMultiOption('','Select');
         $field19 ->removeDecorator("Label");        
         $field19 ->removeDecorator('HtmlTag')
        		  ->setAttrib('class', 'txt_put') ;
         $field19 ->setRegisterInArrayValidator(false);
		 $field19 ->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		
		$field20 = new Zend_Dojo_Form_Element_FilteringSelect('field20'); 
        $field20->removeDecorator("DtDdWrapper");        
        $field20->removeDecorator("Label");
        $field20->removeDecorator('HtmlTag');
        $field20->setRegisterInArrayValidator(false);        
		$field20->setAttrib('dojoType',"dijit.form.FilteringSelect");
		
		$field21 = new Zend_Dojo_Form_Element_DateTextBox('field21');
        $field21->setAttrib('dojoType',"dijit.form.DateTextBox");
        $field21->setAttrib('class', 'txt_put');
        $field21->removeDecorator("DtDdWrapper")
        	    ->setAttrib('title',"dd-mm-yyyy");    
        $field21->removeDecorator("Label");
        $field21->removeDecorator('HtmlTag');
        $field21->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        
        	 $companyflag = new Zend_Dojo_Form_Element_FilteringSelect('companyflag');        
       // $field8 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $companyflag ->removeDecorator("DtDdWrapper");
        //$field8 ->addMultiOption('','Select');
        $companyflag ->removeDecorator("Label");        
        $companyflag ->removeDecorator('HtmlTag')
        ->addMultiOptions(array('1' => 'Company','0' => 'Takaful'));
       // ->setAttrib('class', 'txt_put') ;
       // $field8 ->setAttrib('OnChange', 'fnGetDetails');
        //$field8 ->setRegisterInArrayValidator(false);
		$companyflag ->setAttrib('dojoType',"dijit.form.FilteringSelect");	
		
					
		$FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
	    $FromDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	    				->setAttrib('title',"dd-mm-yyyy")
	        		   ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
	        		   ->setAttrib('onChange', "examtoDateSetting();")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
		$RegFromDate = new Zend_Dojo_Form_Element_DateTextBox('RegFromDate');
	    $RegFromDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
	        		    ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
	        		    //->setAttrib('onChange', "dijit.byId('RegToDate').constraints.min = arguments[0];")
	        		    //->setAttrib('onChange', "dijit.byId('RegToDate').constraints.max = arguments[0];")
	        		    ->setAttrib('onChange', "toDateSetting();")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
		$RegToDate = new Zend_Dojo_Form_Element_DateTextBox('RegToDate');
	    $RegToDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
	        		    ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
	        		    //->setAttrib('onChange', "dijit.byId('RegToDate').constraints.max = arguments[0];")
	        		    //->setAttrib('onChange', "dijit.byId('RegFromDate').value = arguments[0];")
	        		    ->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');				
										
		$ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
	    $ToDate->setAttrib('dojoType',"dijit.form.DateTextBox")
	        		    ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');				
	       	 	
        
        $Clear = new Zend_Form_Element_Submit('Clear');
        $Clear->dojotype="dijit.form.Button";
        $Clear->label = $gstrtranslate->_("Clear");
		$Clear->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Close = new Zend_Form_Element_Submit('Close');
		$Close	->setAttrib('id', 'Close')
				->setAttrib('onclick', 'fnCloseLyteBox()')
				->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$Add = new Zend_Form_Element_Submit('Add');
		$Add->dojotype="dijit.form.Button";
         $Add->label = $gstrtranslate->_("Add");
		$Add->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		        
        $Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');

        $DescActive  = new Zend_Form_Element_Checkbox('DescActive');
        $DescActive->setAttrib('dojoType',"dijit.form.CheckBox");
        $DescActive->setvalue('1');
        $DescActive->removeDecorator("DtDdWrapper");
        $DescActive->removeDecorator("Label");
        $DescActive->removeDecorator('HtmlTag');
        
        $SubmitButton = new Zend_Form_Element_Submit('Submit');
        $SubmitButton->dojotype="dijit.form.Button";
        $SubmitButton->label = $gstrtranslate->_("Submit");
        $SubmitButton->removeDecorator("DtDdWrapper");
        $SubmitButton->removeDecorator("Label");
        $SubmitButton->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
        $passwd = new Zend_Form_Element_Password('passwd');	
        $passwd->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $passwd->setAttrib('required',"true") ;
        $passwd->removeDecorator("DtDdWrapper");
        $passwd->removeDecorator("Label");
        $passwd->removeDecorator('HtmlTag');		

        
        $submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
         		     $field28 = new Zend_Form_Element_Text('field28',array('invalidMessage'=>""));
					$field28 ->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        			$field28 ->setAttrib('class', 'txt_put') 
        					  ->setAttrib('validator', 'validateUsername')
        					 ->setAttrib('required',"true")     
        					 ->removeDecorator("DtDdWrapper")
        	    			 ->removeDecorator("Label")
        					 ->removeDecorator('HtmlTag');
        $Generate = new Zend_Form_Element_Submit('Generate');
        $Generate->dojotype="dijit.form.Button";
        $Generate->label = $gstrtranslate->_("Generate");
        $Generate->removeDecorator("DtDdWrapper");
        $Generate->removeDecorator("Label");
        $Generate->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
         		
         		
		$Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
	    $Date->setAttrib('dojoType',"dijit.form.DateTextBox")
	         ->setAttrib('title',"dd-mm-yyyy")
						->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
						->setAttrib('onChange', "dijit.byId('Date2').constraints.min = arguments[0];")
								
						->removeDecorator("Label")
						->removeDecorator("DtDdWrapper")
						->removeDecorator('HtmlTag');
	       	 
	        	
	    $Date2 = new Zend_Dojo_Form_Element_DateTextBox('Date2');
	    $Date2->setAttrib('dojoType',"dijit.form.DateTextBox")
	          ->setAttrib('title',"dd-mm-yyyy")
			  ->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}")
			  ->setAttrib('onChange', "dijit.byId('Date').constraints.max = arguments[0];")
			  		
			  ->removeDecorator("Label")
			  ->removeDecorator("DtDdWrapper")
			  ->removeDecorator('HtmlTag');
			  
			  
			  
		$field35 = new Zend_Dojo_Form_Element_DateTextBox('field35');
        $field35->  setAttrib('dojoType',"dijit.form.DateTextBox");
        $field35->removeDecorator("DtDdWrapper");
        $field35->setAttrib('class', 'txt_put')
        	->setAttrib('title',"dd-mm-yyyy");
        $field35->removeDecorator("Label");
        $field35->removeDecorator('HtmlTag');
        $field35->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
        
        
          $Paymentmode= new Zend_Dojo_Form_Element_FilteringSelect('modepayment');
        $Paymentmode	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addMultiOption('','Select')
							->addmultioptions(array('5'=>'Money Order','6'=>'Postal Order','7'=>'Credit/Bank to IBFIM account'))
							->setRegisterInArrayValidator(false)
							->setAttrib('onChange','getthemodes(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
							
		$Questionstatus = new Zend_Dojo_Form_Element_FilteringSelect ('Questionstatus');
		$Questionstatus->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Questionstatus->addMultiOptions(array('3'=>'Select','1'=>'Active','0'=>'InActive','2'=>'Review'));
		$Questionstatus->removeDecorator ("DtDdWrapper");
		$Questionstatus->removeDecorator("Label");
		$Questionstatus->removeDecorator('HtmlTag');
		$Questionstatus->setAttrib ('required',"true");			

        $this->addElements(array(
        							$field1,$field2,$field3,$field4,$field5,$field6,$field7,$field8,$submit,$field10,$field11,$field12,
                                 	$field13,$field14,$field15,$field16,$field17,$field18,$field19,$field20,$field21,$FromDate,$ToDate,$RegFromDate,$RegToDate,$Paymentmode,
                                 	$Clear,$Close,$Add,$companyflag,$Generate,$field28,$Active,$DescActive,$SubmitButton,$passwd,$HiddenField1,$Date,$Date2,$field35,$Questionstatus
                            ));   
    }
}
