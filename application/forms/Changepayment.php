<?php
class App_Form_Changepayment extends Zend_Dojo_Form {
    public function init() {
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');
        
        	$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = ("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
         	   
         	   
         	   	    $ModeofPaymentmade= new Zend_Dojo_Form_Element_FilteringSelect('ModeofPaymentmade');
        $ModeofPaymentmade	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('1'=>'Direct Debit FPX','2'=>'Credit Card','4'=>'Cheque'))
							->setRegisterInArrayValidator(false)
							->setAttrib('onChange','getthemodes(this.value);')
							->setAttrib('required',"true") 	 
							->setAttrib('readonly',"true") 	
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
	    $ModeofPayment= new Zend_Dojo_Form_Element_FilteringSelect('ModeofPayment');
        $ModeofPayment	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('1'=>'Direct Debit FPX','2'=>'Credit Card','4'=>'Cheque'))
							->setRegisterInArrayValidator(false)
							->setAttrib('onChange','getthemodes(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');
         
		$Amount = new Zend_Form_Element_Text('Amount',array());			
        $Amount->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $Amount->setAttrib('class', 'txt_put') 
        	  ->setAttrib('maxlength','250')       
	        		->removeDecorator("DtDdWrapper")
	        	    ->removeDecorator("Label")
	        	    ->setAttrib('readonly',"true") 	 
	        		->removeDecorator('HtmlTag'); 
	        		
        	$Pay = new Zend_Form_Element_Submit('Pay');
        $Pay->dojotype="dijit.form.Button";
        $Pay->label = ("Pay");
        $Pay->removeDecorator("DtDdWrapper");
        $Pay->removeDecorator("Label");
        $Pay->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";	        		
	        		
        $this->addElements(array($ModeofPayment,$UpdDate,$UpdUser,$submit,$ModeofPaymentmade,$Amount,$Pay));
    }
}