<?php
class App_Form_Batchcandidates extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	//$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$IdCompany= new Zend_Form_Element_Hidden('IdCompany');
        $IdCompany->removeDecorator("DtDdWrapper");
        $IdCompany->removeDecorator("Label");
        $IdCompany->removeDecorator('HtmlTag');
        
        $CandidateName = new Zend_Form_Element_Text('CandidateName');	
		$CandidateName->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $CandidateName->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
				
		

        $NewRegpin= new Zend_Dojo_Form_Element_FilteringSelect('NewRegpin');
        $NewRegpin	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioption(array('0'=>'Select'))
							->setRegisterInArrayValidator(false)
								->setAttrib('onChange','fnlistabs(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');

					
							    $NewType= new Zend_Dojo_Form_Element_FilteringSelect('NewType');
        $NewType	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
						//	->addmultioption(array('0'=>'Select'))
							->setRegisterInArrayValidator(false)
							->setAttrib('onChange','fngetabsentlist(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');



        $Back = new Zend_Form_Element_Button('Back');
        $Back->label ="Back";
		$Back->setAttrib('onclick','fngoback();');
        $Back->dojotype="dijit.form.Button";
        $Back->removeDecorator("DtDdWrapper");
        $Back->removeDecorator('HtmlTag')
         		->class = "NormalBtn";	
							
        		
        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label ="Save";
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
              ->setAttrib('onClick','fnhidesavebutton()')
         		->class = "NormalBtn";	

        $option1= new Zend_Dojo_Form_Element_FilteringSelect('option1');
        $option1	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							//->addmultioption(array('0'=>'Select All'))
							->setRegisterInArrayValidator(false)
							//->setAttrib('onChange','fnlistabs(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');

        $option2= new Zend_Dojo_Form_Element_FilteringSelect('option2');
        $option2	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							//->addmultioption(array('0'=>'Select All'))
							->setRegisterInArrayValidator(false)
							//->setAttrib('onChange','fnlist(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');	
        $option3= new Zend_Dojo_Form_Element_FilteringSelect('option3');
        $option3	->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							//->addmultioption(array('0'=>'Select All'))
							->setRegisterInArrayValidator(false)
							//->setAttrib('onChange','fnlist(this.value);')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');								
    
        $this->addElements(array($IdCompany,$Back,$NewRegpin,$CandidateName,$Save,$option1,$NewType,$option2,$option3));

    }
}