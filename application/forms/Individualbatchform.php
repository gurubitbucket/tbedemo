<?php
class App_Form_Individualbatchform extends Zend_Dojo_Form {
	public function init() {
		$gstrtranslate = Zend_Registry::get ('Zend_Translate');
		
		
		$Takcompnames = new Zend_Form_Element_Text('Takcompnames');
		$Takcompnames->setAttrib('dojoType',"dijit.form.ValidationTextBox");
		$Takcompnames->setAttrib('readonly',"True")
		              ->setAttrib('style','width:300px;');;
		$Takcompnames->removeDecorator ("DtDdWrapper");
		$Takcompnames->removeDecorator("Label");
		$Takcompnames->removeDecorator('HtmlTag');
		
		$Pin = new Zend_Dojo_Form_Element_FilteringSelect ('Pin');
		$Pin->setAttrib ('dojoType', "dijit.form.FilteringSelect");
		$Pin->removeDecorator("DtDdWrapper");
		$Pin->removeDecorator("Label");
		$Pin->removeDecorator('HtmlTag');
		$Pin->setAttrib('required',"true");
		
		$submit = new Zend_Form_Element_Submit('Search');
        $submit->dojotype="dijit.form.Button";
        $submit->label = $gstrtranslate->_("Search");
        $submit->removeDecorator("DtDdWrapper");
        $submit->removeDecorator("Label");
        $submit->removeDecorator('HtmlTag')
         	   ->class = "NormalBtn";
         	   
        $excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
		
		$this->addElements ( array (
		                            $Takcompnames,
		                            $Pin,
		                            $excel, 
		                            $submit,
		                           )
		                   );
	}
}
