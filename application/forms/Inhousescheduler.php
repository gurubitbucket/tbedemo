<?php
class App_Form_Inhousescheduler extends Zend_Dojo_Form { //Formclass for the Programmaster	 module
    public function init() {
    	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
    	
		$idnewscheduler = new Zend_Form_Element_Hidden('idnewscheduler');
        $idnewscheduler->removeDecorator("DtDdWrapper");
        $idnewscheduler->removeDecorator("Label");
        $idnewscheduler->removeDecorator('HtmlTag');
        
        
        $Date = new Zend_Dojo_Form_Element_DateTextBox('Date');
        $Date->setAttrib('dojoType',"dijit.form.DateTextBox")
             ->setAttrib('onchange',"fngetdateinwords()")
             	->setAttrib('style','width:200px;');
		$Date->setAttrib('required',"true");
        $Date->removeDecorator("DtDdWrapper");
            
        $Date->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
              //->setAttrib('onchange',"fngetexceptiondates(this.value)");
        $Date->removeDecorator("Label");
        $Date->removeDecorator('HtmlTag');							

        $description = new Zend_Form_Element_Text('description');
		$description->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $description->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','150') 
        		 ->setAttrib('style','width:200px;')
        		->setAttrib('readonly',true)      
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
        		
      	$Active  = new Zend_Form_Element_Checkbox('Active');
        $Active->setAttrib('dojoType',"dijit.form.CheckBox");
        $Active->setvalue('1');
        $Active->removeDecorator("DtDdWrapper");
        $Active->removeDecorator("Label");
        $Active->removeDecorator('HtmlTag');
        
        $UpdDate = new Zend_Form_Element_Hidden('UpdDate');
        $UpdDate->removeDecorator("DtDdWrapper");
        $UpdDate->removeDecorator("Label");
        $UpdDate->removeDecorator('HtmlTag');
        
        $UpdUser  = new Zend_Form_Element_Hidden('UpdUser');
        $UpdUser->removeDecorator("DtDdWrapper");
        $UpdUser->removeDecorator("Label");
        $UpdUser->removeDecorator('HtmlTag');

        $Save = new Zend_Form_Element_Submit('Save');
        $Save->label = $gstrtranslate->_("Save");
        $Save->dojotype="dijit.form.Button";
        $Save->removeDecorator("DtDdWrapper");
        $Save->removeDecorator('HtmlTag')
         		->class = "NormalBtn";
    		
         		
        $Back = new Zend_Form_Element_Button('Back');
        $Back->label = $gstrtranslate->_("Back");
        $Back->dojotype="dijit.form.Button";
		$Back->setAttrib('class', 'NormalBtn')
				->removeDecorator("Label")
				->removeDecorator("DtDdWrapper")
				->removeDecorator('HtmlTag');
				
		$session = new Zend_Dojo_Form_Element_FilteringSelect('session');
	    $session->setAttrib('dojoType',"dijit.form.FilteringSelect")
	            ->setAttrib('required','false')
	             ->setAttrib('style','width:200px;');
	    $session->removeDecorator("DtDdWrapper");
	    $session->removeDecorator("Label");
	    $session->removeDecorator('HtmlTag');
	    
	    $schedulertype = new Zend_Dojo_Form_Element_FilteringSelect('schedulertype');
	    $schedulertype->setAttrib('dojoType',"dijit.form.FilteringSelect")
	            ->setAttrib('required','true')
	            	->addmultioptions(array('1'=>'Takaful','2'=>'company','3'=>'Takaful and company','4'=>'Indivisual'))
	             ->setAttrib('style','width:200px;');
	    $schedulertype->removeDecorator("DtDdWrapper");
	    $schedulertype->removeDecorator("Label");
	    $schedulertype->removeDecorator('HtmlTag');
	    
	    $venue = new Zend_Dojo_Form_Element_FilteringSelect('venue');
	    $venue->setAttrib('dojoType',"dijit.form.FilteringSelect")
	            ->setAttrib('required','false');
	    $venue->removeDecorator("DtDdWrapper");
	    $venue->removeDecorator("Label");
	    $venue->removeDecorator('HtmlTag');
        
	    $capacity = new Zend_Form_Element_Text('capacity');
		$capacity->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $capacity->setAttrib('maxlength','20') 
		         ->setAttrib('OnBlur','fnvalidatetime(this.value)')
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');
				
		$From= new Zend_Dojo_Form_Element_FilteringSelect('From');
       $From	->setAttrib('dojoType',"dijit.form.FilteringSelect")
       ->addmultioptions(array())
                            ->setAttrib('OnChange', 'fnGetToMonthList')						
							->removeDecorator("DtDdWrapper")
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');

	   $To= new Zend_Dojo_Form_Element_FilteringSelect('To');
       $To->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');	

         $Year= new Zend_Dojo_Form_Element_FilteringSelect('Year');
       $Year->setAttrib('dojoType',"dijit.form.FilteringSelect")						
							->removeDecorator("DtDdWrapper")
							->addmultioptions(array('2012'=>'2012',
													'2013'=>'2013',
													'2014'=>'2014',
													'2015'=>'2015',
													'2016'=>'2016',
													'2017'=>'2017',
													'2018'=>'2018',
													'2019'=>'2019',
													'2020'=>'2020',
													))
							->setAttrib('OnChange', 'fnGetMonthList')
							->setAttrib('required',"true") 	 
							->removeDecorator("Label") 				
							->removeDecorator('HtmlTag');

        $description = new Zend_Form_Element_Text('description');
		$description->setAttrib('dojoType',"dijit.form.ValidationTextBox");
        $description->setAttrib('required',"true")       			 
        		->setAttrib('maxlength','100')       
        		->removeDecorator("DtDdWrapper")
        	    ->removeDecorator("Label")
        		->removeDecorator('HtmlTag');							
        //form elements
        $this->addElements(array($idnewscheduler,$To,$From,$Year,$description,
        						 $schedulertype,
        						 $description,
                                 $Active,
                                 $UpdDate,
                                 $UpdUser,
                                 $Save,
                                 $Back,$Date,
                                 $session,
                                 $venue,
                                 $capacity));

    }
}