<?php
	class App_Form_Takafulreportform extends Zend_Dojo_Form {
		public function init() {
	$gstrtranslate =Zend_Registry::get('Zend_Translate'); 
	        
        $month= date("m"); // Month value
		$day=  date("d"); //today's date
		$year= date("Y"); // Year value
		
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$dateofbirth = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		$dateofbirth1 = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}"; 
		
		$Venue = new Zend_Dojo_Form_Element_FilteringSelect('Venue');
		$Venue->removeDecorator("DtDdWrapper");
		$Venue->setAttrib('required',"true") ;
		$Venue->removeDecorator("Label");
		$Venue->removeDecorator('HtmlTag');
		//$Venue->setAttrib('OnChange', 'fnGetCountryStateList');
		$Venue->setRegisterInArrayValidator(false);
		$Venue->setAttrib('dojoType',"dijit.form.FilteringSelect");	
	
				$FromDate = new Zend_Dojo_Form_Element_DateTextBox('FromDate');
		$FromDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$FromDate->setAttrib('title',"dd-mm-yyyy");
	  	$FromDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$FromDate->setAttrib('constraints',"$dateofbirth");
		//$FromDate->setAttrib('onChange',"dijit.byId('ToDate').constraints.min = arguments[0];");
		$FromDate->setAttrib('onChange', "examtoDateSetting();");
		$FromDate->setAttrib('required',"true");
		$FromDate->removeDecorator("Label");
		$FromDate->removeDecorator("DtDdWrapper");
		$FromDate->removeDecorator('HtmlTag');
		
		$ToDate = new Zend_Dojo_Form_Element_DateTextBox('ToDate');
		$ToDate->setAttrib('dojoType',"dijit.form.DateTextBox");
		$ToDate->setAttrib('title',"dd-mm-yyyy");
	  	$ToDate->setAttrib('constraints', "{datePattern:'dd-MM-yyyy'}");
		$ToDate->setAttrib('onChange', "dijit.byId('FromDate').constraints.max = arguments[0];");
		$ToDate->setAttrib('required',"true");
		$ToDate->removeDecorator("Label");
		$ToDate->removeDecorator("DtDdWrapper");
		$ToDate->removeDecorator('HtmlTag');
		
		$submit = new Zend_Form_Element_Submit('Search');
		$submit->dojotype="dijit.form.Button";
		$submit->label = $gstrtranslate->_("Search");
		$submit->removeDecorator("DtDdWrapper");
		$submit->removeDecorator("Label");
		$submit->removeDecorator('HtmlTag');
		$submit->class = "NormalBtn";
		
		$excel = new Zend_Form_Element_Submit('ExportToExcel');
		$excel->dojotype="dijit.form.Button";
		$excel->label = $gstrtranslate->_("Export To Excel");
		$excel->removeDecorator("DtDdWrapper");
		$excel->removeDecorator("Label");
		$excel->removeDecorator('HtmlTag') ;
		
		$Clear = new Zend_Form_Element_Submit('Clear');
		$Clear->setAttrib('class','NormalBtn');
		$Clear->dojotype="dijit.form.Button";
		$Clear->label = $gstrtranslate->_("Clear");
		$Clear->removeDecorator("Label");
		$Clear->removeDecorator("DtDdWrapper");
		$Clear->removeDecorator('HtmlTag');
		
		$day = new Zend_Dojo_Form_Element_FilteringSelect('day');
	    $day->setAttrib('dojoType',"dijit.form.FilteringSelect");
	    $day->addMultiOptions(array(''=>'All Days','Sunday'=>'Sunday','Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday')); 	           	         		       		     
	    $day->removeDecorator("DtDdWrapper");
	    $day->removeDecorator("Label");
	    $day->removeDecorator('HtmlTag');
	    
		
	    $this->addElements(
        				   array(
        					   $FromDate,
        					   $ToDate,
        					   $submit,
        					   $excel,$Clear,
        					   $day,$Venue
        				     )
        			      );
		}
}
