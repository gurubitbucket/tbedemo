<?php
class App_Model_Companymaster extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_companies';
	

 public function fnaddcompany($larrformdata,$password)
 {
/* 	print_r($password);
	print_r($larrformdata);die();*/
	
 	$larrformdata['Phone1'] = $larrformdata['Phone1'];
 	$larrformdata['Phone2'] = $larrformdata['Phone2'];
 	$larrformdata['IdCountry'] = 121;
 	$this->insert($larrformdata);
 	
 	  // $lastids  = $db->lastInsertId("tbl_companies","IdCompany");	
 	self::sendmails($larrformdata,$password);
 }
	
 /*
  * function to fetch all the details of the takaful operator
  */
public function fngetcompanymaster()
{
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
						 ->from(array("a"=>"tbl_companies"),array("UPPER(a.CompanyName) as CompanyName","UPPER(a.Login) as Login"));
       $result = $db->fetchAll($lstrSelect);
       return $result;						 
}

public function fngetcompaniesemail()
{
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
						 ->from(array("a"=>"tbl_companies"),array("UPPER(a.Email) as Email"));
       $result = $db->fetchAll($lstrSelect);
       return $result;						 
}
/*
 * function to fetcha ll the details based on the id
 */
public function fnGetCompanyDetails($intidtakafuloperator)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_companies"),array("a.*"))
					->where("a.IdCompany=?",$intidtakafuloperator);
	$result = $db->fetchRow($lstrSelect);
	return $result;
}


public function fngetcompanyname($companyname)
{
	//$where=$companyname;
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_companies"),array("a.*"))
					//->where("a.CompanyName=?",$where);
                    ->where("a.CompanyName= '$companyname'");
	$result = $db->fetchRow($lstrSelect);
	return $result;
	
}

public function fngetusername($username)
{
	//$where=$username;
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_companies"),array("a.*"))
					->where("a.Login= '$username'");
	$result = $db->fetchRow($lstrSelect);
	return $result;
	
}

public function fngetemail($email)
{
	$where=$email;
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_companies"),array("a.*"))
					->where("a.Email=?",$where);
	$result = $db->fetchRow($lstrSelect);
	return $result;
}

/*
 * function to update the takaful operator
 */
public function fnupdatecompanymaster($formData,$lintidtakafuloperator)
{
	$larrformdata['Phone1'] = $larrformdata['workcountrycode']."-".$larrformdata['workstatecode']."-".$larrformdata['Phone1'];
 	$larrformdata['Phone2'] = $larrformdata['countrycode']."-".$larrformdata['statecode']."-".$larrformdata['Phone2'];
 		unset($larrformdata['workcountrycode']);
		unset($larrformdata['workstatecode']);
		unset($larrformdata['countrycode']);
		unset($larrformdata['statecode']);
	$db =Zend_Db_Table::getDefaultAdapter();
	$table = "tbl_companies";
	$where['IdCompany = ? '] = $lintidtakafuloperator;
	$db->update($table,$formData,$where);
				
}


public function fnGetStateCityList($idCountry){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_city"),array("key"=>"a.idCity","value"=>"CityName"))
					 				 ->where("a.idState = ?",$idCountry)
					 				 ->where("a.Active = 1")
					 				 ->order("a.CityName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
		
public function fnGetBusinesstypeList()
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_businesstype"),array("key"=>"a.idbusinesstype","value"=>"a.BusinessType"))
					 				 ->where("a.Active = 1")
					 				 ->order("a.idbusinesstype");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;	
}

public  function fnGetCompanydetailsview($lintId)
{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_companies"),array("a.Login","a.answer"))
					 				 ->join(array("b"=>"tbl_securityquestions"),"b.idsecurityquestions=a.question",array("b.SecurityQuestions as quest"))
					 				 //->where("a.Active = 1")
					 				 ->where("a.IdCompany=?",$lintId);
					 				// ->order("a.idbusinesstype");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
}
public function  fnGetQuestiontypeList()
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_securityquestions"),array("key"=>"a.idsecurityquestions","value"=>"a.SecurityQuestions"))
					 				 ->where("a.Active = 1");
					 				// ->order("a.BusinessType");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;	
}
/*
 * function to search the takaful operator
 */
public function fnSearchTakafulOperator($post)
{


	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_companies"),array("a.*"))
					->where('a.CompanyName like  ? "%"',$post['field3']);
	$result = $db->fetchAll($lstrSelect);
	return $result;
}
function fnGenerateCode($lintIdRefundMaster,$stateid){	
	//$accCode="";
			$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $db->select()
				->  from('tbl_config')
				-> where('idConfig  = 1');				 
		$result = 	$db->fetchRow($select);		
		$sepr	=	$result['Separator'];
		$str	=	"RegField";
		for($i=1;$i<=3;$i++){
			$check = $result[$str.$i];
			//echo $check;die();
			switch ($check){
				case 'year':
				  $code	= date('Y');
				  break;
				case 'state':
					 $select =  $db->select()
					 		 -> from(array('a'=>'tbl_state'),array("left(a.StateName,3) as StateName"))
					 		 ->	where('idState = ?',$stateid);  				 
					$resultstate = $db->fetchRow($select);	
				  $code	= $resultstate['StateName'];
				  break;
				case 'company':
					 $select =  $db->select()
					 		 -> from('tbl_companies')
					 		 ->	where('IdCompany = ?',$lintIdRefundMaster);  				 
					$resultCollage = $db->fetchRow($select);		
					$code		   = $resultCollage['ShortName'];
				  break;
				default:
				  break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}
		
				$accCode	.=	$sepr.$lintIdRefundMaster;
	 	$data = array('RegistrationNo' => $accCode);
		$where['IdCompany = ? ']= $lintIdRefundMaster;		
		return $db->update('tbl_companies', $data, $where);	
}
public function sendmails($larrformdata,$password)
	{
			require_once('Zend/Mail.php');
			require_once('Zend/Mail/Transport/Smtp.php');
				 $this->lobjstudentmodel = new App_Model_Studentapplication();
					$larrSMTPDetails  = $this->lobjstudentmodel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Company Registration");
						$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
						$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
						$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
						$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
						$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
						$larrEmailIds = $larrformdata['Email'];//["EmailAddress"];
						//$lstrEmailTemplateBody = str_replace("[Candidate]",$larrformdata['ContactPerson'],$lstrEmailTemplateBody);
						$lstrEmailTemplateBody = str_replace("[Person]",$larrformdata['ContactPerson'],$lstrEmailTemplateBody);
						$lstrEmailTemplateBody = str_replace("[Company]",$larrformdata['CompanyName'],$lstrEmailTemplateBody);
						$lstrEmailTemplateBody = str_replace("[LoginId]",$larrformdata['Login'],$lstrEmailTemplateBody);
						$lstrEmailTemplateBody = str_replace("[Password]",$password,$lstrEmailTemplateBody);
					    $lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
						//$lobjMail->setBodyHtml($lstrEmailTemplateBody);

						    $auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrformdata['Email'];
										$receiver = $larrformdata['ContactPerson'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
							 try {
									$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Unable to send mail due to technical reason")</script>';
				                	
								}
				
	}

public function sendmailsold($larrformdata,$password)
	{
		/*echo "asfsafsad";
		die();*/
		 $this->lobjstudentmodel = new App_Model_Studentapplication();
		$larrSMTPDetails  = $this->lobjstudentmodel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Company Registration");
					
						//Get Student's Mailing Details
						//$larrStudentMailingDetails = $larrresult;
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
						
						
						$larrEmailIds = $larrformdata['Email'];//["EmailAddress"];
							//$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							//$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							/*echo "asfsafsdfsd";
							die();*/
							try{
								$lobjProtocol->connect();
						   		$lobjProtocol->helo($lstrSMTPUsername);
								$lobjTransport->setConnection($lobjProtocol);
						 	
								//Intialize Zend Mailing Object
								$lobjMail = new Zend_Mail();
						
								$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
								$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
								$lobjMail->addHeader('MIME-Version', '1.0');
								$lobjMail->setSubject($lstrEmailTemplateSubject);
						
								/*for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
									if($larrEmailIds[$lintI] != ""){
										$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);*/
																
										//replace tags with values
										//$Link = "<a href='".$this->Url."/registration'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrformdata['ContactPerson'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Company]",$larrformdata['CompanyName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrformdata['Login'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$password,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										$lobjMail->setBodyHtml($lstrEmailTemplateBody);
								
										try {
											$lobjMail->send($lobjTransport);
										} catch (Exception $e) {
											$lstrMsg = "error";      				
										}	
										$lobjMail->clearRecipients();
										$this->view->mess .= ". Login Details have been sent to user Email";
									//unset($larrEmailIds[$lintI]);
									//}
								//}
							}catch(Exception $e){
								$lstrMsg = "error";
							}
						}else{
							$lstrMsg = "No Template Found";
						}
	}


}