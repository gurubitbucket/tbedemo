<?php
class App_Model_Batchlogin extends Zend_Db_Table {
	//protected $_name = 'tbl_batchmaster'; // table name
public  function fngetlogincheck($username,$password)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_companies"),array("a.IdCompany","a.CompanyName","a.ShortName","a.RegistrationNo" ))
					 				->where("a.Login= ?",$username)
					 				->where("a.Password = ?",$password);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
function fnGenerateCode($idtakafuloperator,$Idunique){	
	
	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $db->select()
				->  from('tbl_studentapplication')
				->	where('IDApplication  = ?',$Idunique);				 
		$result = 	$db->fetchRow($select);		
		$batchpayment=	$result['batchpayment'];
		
		$sepr='*';
		$str	=	"SturegField";
		
		//echo $batchpayment;die();
		
		if($batchpayment==1)
		{
			for($i=1;$i<=4;$i++){
			$check = $i;
			switch ($check){
				case '1':
				   $code	= $Idunique;
				  break;
				case '2':
				  $code	= date('dMy');
				  break;
				case '3':
					$code = 'C';
				  break;
				  case '4':
					$code = $idtakafuloperator;
				  break;
				default:
				  break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}	
			
			
		}
		else if ($batchpayment==2)
		{
			
				for($i=1;$i<=4;$i++){
			$check = $i;
			switch ($check){
				case '1':
				   $code	= $Idunique;
				  break;
				case '2':
				  $code	= date('dMy');
				  break;
				case '3':
					$code = 'T';
				  break;
				  case '4':
					$code = $idtakafuloperator;
				  break;
				default:
				  break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}
			
		}
		else 
		{
		for($i=1;$i<=3;$i++){
			$check = $i;
			switch ($check){
				case '1':
				   $code	= $Idunique;
				  break;
				case '2':
				  $code	= date('dMy');
				  break;
				case '3':
					$code = 'i';
				  break;
				default:
				  break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}	
		}
		return $accCode;	
	 	/*$data = array('InvoiceNo' => $accCode);
		$where['IdInvoice = ? ']= $Idunique;		
		return $db->update('tbl_sfsinvoicemaster', $data, $where);	*/		
	} 
	
	
	//disable multiple logins and other functionality 16-12-2014
	function fnupdatelastlogindetails($iduser,$Typeofuser){
		
		$lobjDbAdapt = Zend_Db_Table::getDefaultAdapter();
		$lstrtable = "tbl_logindetails";
		$Sessionend=date("Y-m-d H:i:s");
		 $lstrSelect="UPDATE tbl_logindetails SET tbl_logindetails.Isonline=0,tbl_logindetails.Sessionend='$Sessionend' WHERE tbl_logindetails.Iduser='$iduser' AND tbl_logindetails.Typeofuser='$Typeofuser'";
		
		$data2=$lobjDbAdapt->query($lstrSelect);
		$data2->execute(); 
	}
	
	
	function fnInsertLoginDetails($iduser,$Sessionstart,$Sessionend,$Isonline,$ipaddress,$Active,$phpsession,$Typeofuser){
		$lobjDbAdapt = Zend_Db_Table::getDefaultAdapter();
		$lstrtable = "tbl_logindetails";
		$larrdata = array("Iduser"=>$iduser,
	                     "Sessionstart"=>$Sessionstart,
	                     "Sessionend"=>$Sessionend,
	                     "Isonline"=>$Isonline,
	                     "Ipaddress"=>$ipaddress,
	                     "Active"=>$Active,
	                     "Phpsessid"=>$phpsession,
	                     "Typeofuser"=>$Typeofuser);
	                     $data=$this->fnFetchLoginDetails($iduser,$Typeofuser);
	                     if($data==0)
	                       {
		                        $insertlogindetails = $lobjDbAdapt->insert($lstrtable,$larrdata);
					        }
					        else{
								$lstrSelect="UPDATE tbl_logindetails SET tbl_logindetails.Sessionstart='$Sessionstart',tbl_logindetails.Sessionend='$Sessionend',tbl_logindetails.Isonline='$Isonline',tbl_logindetails.Ipaddress='$ipaddress' WHERE tbl_logindetails.Iduser='$iduser' AND tbl_logindetails.Typeofuser='$Typeofuser'";
							    $data2=$lobjDbAdapt->query($lstrSelect);
							    $data2->execute(); 
							}
		
	   
		$lstrtable1 = "tbl_userlastactivity";
		$larrdata1 = array("Idusers"=>$iduser,
	                       "Lastactivity"=>$Sessionstart,
	                       "Typeofuser"=>$Typeofuser);
	                       //echo "asasas";die();
	                       $data=$this->fnFetchLastActivityDetails($iduser,$Typeofuser);
	                       //echo $data;exit;
	                       if($data==0)
	                       {
		                        $insertlogindetails = $lobjDbAdapt->insert($lstrtable1,$larrdata1);
					        }
					        else{
								$lstrSelect="UPDATE tbl_userlastactivity SET tbl_userlastactivity.Lastactivity='$Sessionstart' WHERE tbl_userlastactivity.Idusers='$iduser' AND tbl_userlastactivity.Typeofuser='$Typeofuser'";
							    $data1=$lobjDbAdapt->query($lstrSelect);
							    $data1->execute(); 
							}
		
	}
	function fnFetchLastActivityDetails($iduser,$Typeofuser){
		$lobjDbAdapt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect="SELECT EXISTS(SELECT 1 FROM tbl_userlastactivity WHERE tbl_userlastactivity.Idusers='$iduser' AND tbl_userlastactivity.Typeofuser='$Typeofuser' ) AS data";
		//echo $lstrSelect;die();
		$data=$lobjDbAdapt->fetchRow($lstrSelect);
		//echo "<pre>";print_r($data['data']);die();
		return $data['data'];
	}
	
	function fnFetchLoginDetails($iduser,$Typeofuser){
		$lobjDbAdapt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect="SELECT EXISTS(SELECT 1 FROM tbl_logindetails WHERE tbl_logindetails.Iduser='$iduser' AND tbl_logindetails.Typeofuser='$Typeofuser' ) AS data";
		//echo $lstrSelect;die();
		$data=$lobjDbAdapt->fetchRow($lstrSelect);
		//echo "<pre>";print_r($data['data']);die();
		return $data['data'];
		
		
	}
	
	function fnPunchInLogOut($iduser,$Typeofuser,$logouttime){
		$lobjDbAdapt = Zend_Db_Table::getDefaultAdapter();
		
		$lstrSelect="UPDATE tbl_logindetails SET tbl_logindetails.Sessionend='$logouttime',tbl_logindetails.Isonline=0 WHERE tbl_logindetails.Iduser='$iduser' AND tbl_logindetails.Typeofuser='$Typeofuser'";
							    $data1=$lobjDbAdapt->query($lstrSelect);
							    $data1->execute(); 
						    
	   $lstrSelect1="UPDATE tbl_userlastactivity SET tbl_userlastactivity.Lastactivity='$logouttime' WHERE tbl_userlastactivity.Idusers='$iduser' AND tbl_userlastactivity.Typeofuser='$Typeofuser'";
							    $data2=$lobjDbAdapt->query($lstrSelect1);
							    $data2->execute(); 	
							    			    
	}
	
	function fnGetSessionStatus($iduser,$Typeofuser){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("tbl_logindetails"=>"tbl_logindetails"),array("tbl_logindetails.Isonline"))
					 				->where("tbl_logindetails.Iduser= ?",$iduser)
					 				->where("tbl_logindetails.Typeofuser = ?",$Typeofuser);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			
			return $larrResult;
     }
     function fnUpdateLastActivity($iduser,$Typeofuser,$activitytime){
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect1="UPDATE tbl_userlastactivity SET tbl_userlastactivity.Lastactivity='$activitytime' WHERE tbl_userlastactivity.Idusers='$iduser' AND tbl_userlastactivity.Typeofuser='$Typeofuser'";
							    $data2=$lobjDbAdpt->query($lstrSelect1);
							    $data2->execute(); 			
	 }
	 
	 function fngetisonlinestatus() { 
		$lobjDbAdapt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdapt->select()
								  ->from(array("tbl_userlastactivity"=>"tbl_userlastactivity"),array("tbl_userlastactivity.Idusers","tbl_userlastactivity.Typeofuser"))
								  ->where("TIMESTAMPDIFF(MINUTE,tbl_userlastactivity.Lastactivity, NOW()) > 10");
	    $userlastactivity = $lobjDbAdapt->fetchAll($lstrSelect);
		
		return $userlastactivity;
	}
	
	function fnupdateisonlinestatus($logincredentials){
		$lobjDbAdapt =  Zend_Db_Table::getDefaultAdapter();
		//echo "<pre>";print_r($logincredentials);
		for($i=0;$i<count($logincredentials);$i++)
		{
			$logouttime=$logincredentials[$i]['logouttime'];
			$iduser=$logincredentials[$i]['iduser'];
			$Typeofuser=$logincredentials[$i]['Typeofuser'];
			
            $lstrSelect="UPDATE tbl_logindetails SET tbl_logindetails.Sessionend='$logouttime',tbl_logindetails.Isonline=0 WHERE tbl_logindetails.Iduser='$iduser' AND tbl_logindetails.Typeofuser='$Typeofuser'";
            //echo $lstrSelect;die();
		   $data1=$lobjDbAdapt->query($lstrSelect);
							    $data1->execute(); 
		}
		
	}
	
	////////////////Changes by Vineet Kulkarni 15.04.2015//////////
	function fetchcompanycandidateexpiry()
	{
		$lobjDbAdapt =  Zend_Db_Table::getDefaultAdapter();
		$lstrselect=$lobjDbAdapt->select()
		                        ->from(array("tbl_config"=>"tbl_config"),array("tbl_config.Paymentorder5"));
							
					  $resultset = $lobjDbAdapt->fetchRow($lstrselect);
					  return $resultset;
	}
	 function updatepaymentstatus($companycandidateexpirydate)
	{
		$lobjDbAdapt =  Zend_Db_Table::getDefaultAdapter();
		 /*$lstrselect  =  "SELECT tbl_batchregistration.idCompany 
		                 FROM  tbl_batchregistration  
						 INNER JOIN tbl_companies 
						 ON tbl_batchregistration.idCompany=tbl_companies.IdCompany 
						 WHERE DATEDIFF(NOW(),tbl_batchregistration.UpdDate) > 14 
						 AND tbl_batchregistration.paymentStatus=1";*/
		//$data1=$lobjDbAdapt->query($lstrselect);
		//$larrResult = $data1->execute();
		$lstrSelect = $lobjDbAdapt->select()
								  ->from(array("tbl_batchregistration"=>"tbl_batchregistration"),array("tbl_batchregistration.idBatchRegistration"))
								  ->join(array("tbl_companies"=>"tbl_companies"),"tbl_batchregistration.idCompany=tbl_companies.IdCompany ",array(""))
								  ->where("DATEDIFF(NOW(),tbl_batchregistration.UpdDate) > '$companycandidateexpirydate'")
								  ->where("tbl_batchregistration.`UpdDate` >= '2015-01-01 00:00:00'")
								  ->where("tbl_batchregistration.paymentStatus = 1");				 
		echo $lstrSelect;die();			 
		$resultset = $lobjDbAdapt->fetchAll($lstrSelect);
		//echo "<pre/>";print_r($resultset);exit;
		$list="";
		foreach($resultset as $results)
		{
			$list.= $results['idBatchRegistration'].',';
		}
		$log_list=trim($list,",");
		//echo $log_list;exit;		
		$lstrupdate="UPDATE tbl_batchregistration 
		            SET tbl_batchregistration.paymentStatus = 2,tbl_batchregistration.AdhocVenue = 'Seized'
					WHERE tbl_batchregistration.idBatchRegistration in($log_list)";
		$data1=$lobjDbAdapt->query($lstrupdate);
		$data1->execute();
	} 
	////////////////End of Changes by Vineet Kulkarni 15.04.2015///
    
}
