<?php
class App_Model_Batchcandidates extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
		 public function fngetBatchRegistration($idbatch)
		 {
		 	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_batchregistrationdetails"),array("a.*"))
							 				 ->join(array("b"=>"tbl_programmaster"),'a.idProgram= b.IdProgrammaster')
							 				 ->where("a.idBatchRegistration = ?",$idbatch);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
		 }
		 
	 public function fngetBatchDetailsforRegistrationpin($idpin)
	 {
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchregistration"),array('a.*'))
										  ->where("a.idBatchRegistration  = ?",$idpin);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	 }
		 
		 public function  fnAdhocvenue($ids)
		 {
		 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchregistration"),array('a.*'))
										  ->where("a.idBatchRegistration  = ?",$ids);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
		 }
		 
		 public function fnGetProgramName()
		 {
		 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										  
										  ->join(array("c" => "tbl_program"),'c.IdProgram=a.idprog')
										  ->join(array("b"=>"tbl_batchmaster"),'b.IdProgrammaster=a.IdProgrammaster',array())
										  ->join(array("d"=>"tbl_tosmaster"),'b.IdBatch=d.IdBatch',array())
										  ->where("d.Active=1")
										  ->where("c.Active =1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		 }
		 public function fngetregisteredstudentsdetails($regpin)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_registereddetails"),array("a.*"))
										  ->join(array("b"=>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
										  ->where("a.Registrationpin='$regpin'");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
}	
		 
		 public function fnGetBatchName()
		 {
		 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 			
 				$lstrSelect="SELECT `a`.`IdBatch` AS `key`, CONCAT(DATE_FORMAT(`a`.`BatchFrom`,'%d-%m-%Y'),'---',DATE_FORMAT(`a`.`BatchTo`,'%d-%m-%Y')) AS `value` FROM `tbl_batchmaster` AS `a`";			
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		 }
		 
 public function fnGetVenuedetailsgetsecid($idsech)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
						                  ->where("a.Active=1")
										  ->where("a.idnewscheduler =?",$idsech); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
  
public function fnGetVenuedetailsRemainingseats($year,$idsech,$city,$month,$Date)
  {
  	
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $select = "SELECT b.NumberofSeat, b.centername,b.idcenter, d.managesessionname, d.starttime, d.endtime,d.idmangesession, (
b.NumberofSeat - IFNULL( count( a.IDApplication ) , 0 )
) AS rem
FROM `tbl_studentapplication` a, tbl_center b, tbl_managesession d,tbl_newscheduler m
WHERE a.Examvenue = b.idcenter
AND a.Examdate =$Date
AND a.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND b.Active =1
and  b.city =$city
AND d.idmangesession = a.Examsession
GROUP BY a.Examvenue, a.Examsession
UNION
SELECT b.NumberofSeat, b.centername,b.idcenter, j.managesessionname, j.starttime, j.endtime, j.idmangesession, b.NumberofSeat AS rem
FROM tbl_center b,tbl_managesession e,`tbl_studentapplication` c,tbl_newschedulersession f, tbl_managesession j,tbl_newscheduler m
WHERE f.idmanagesession NOT
IN (
SELECT b.Examsession
FROM tbl_studentapplication b
WHERE b.Examvenue = c.Examvenue
AND b.ExamCity =$city
AND b.Examdate =$Date
AND b.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND b.Examvenue !=000
AND b.Examsession !=000
GROUP BY b.Examvenue, b.Examsession
)
AND c.ExamCity =$city
AND c.Examdate =$Date
AND c.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND c.Examvenue !=000
AND f.idnewscheduler =$idsech
AND f.idmanagesession=j.idmangesession
AND c.Examvenue = b.idcenter
UNION
SELECT b.NumberofSeat, b.centername,b.idcenter, e.managesessionname, e.starttime, e.endtime,e.idmangesession, b.NumberofSeat AS rem
FROM tbl_center b, tbl_newschedulervenue c, tbl_managesession e
WHERE b.idcenter NOT
IN (
SELECT Examvenue
FROM tbl_studentapplication
WHERE ExamCity =$city
AND Examdate =$Date
AND Exammonth =$month
AND Year =$idsech
)
AND b.city =$city
AND b.Active =1
AND  c.idvenue = b.idcenter
AND e.idmangesession
IN (
SELECT h.idmanagesession
FROM tbl_newschedulersession h
WHERE idnewscheduler =$idsech
)" ;
		 return	$result = $lobjDbAdpt->fetchAll($select);	
  }
		 
  /*
   * function to fetch all the scheduler based on the prog
   */
 public function fnGetscheduler($idbatch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("key"=>"a.idschedulermaster","value"=>("a.ScheduleName")))
										  ->where("a.idBatch  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
   /*
  * funtion to fetch all the venue based on the batch
  */			
  public function fnGetVenueName($lintidscheduler)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"b.idschedulervenue","value"=>"a.centername"))
										  ->join(array("b" =>"tbl_schedulervenue"),'a.idcenter = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulermaster"),'b.idschedulermaster = c.idschedulermaster',array())
										  ->where("c.idschedulermaster  = ?",$lintidscheduler)
										  ->order("a.city");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  
  public function fnGetVenueTime($idvenue)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
/* 	echo 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("key"=>"a.idschedulervenuetime","value"=>("distinct(a.Date) as Date")))
										 // ->join(array("b" =>"tbl_center"),'a.idcentre = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulervenue"),'a.idschedulervenue = c.idschedulervenue',array())
										  ->where("c.idschedulervenue  = ?",$idvenue);die();*/
 	$lstrSelect="SELECT (a.Date)as `key`,(a.Date)as value FROM `tbl_schedulervenuetime` AS `a`
 INNER JOIN `tbl_schedulervenue` AS `c` ON a.idschedulervenue = c.idschedulervenue WHERE (c.idschedulervenue  = $idvenue)group by value";
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
	/*
	 * 
	 */
	public function fnGetTimingsForDate($date,$idscheduler)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect="SELECT idschedulervenuetime as `key`,`From` as value FROM `tbl_schedulervenuetime` WHERE `idschedulervenue`=$idscheduler and Date='$date'";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	

	public function fnTakafuloperator(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnBatchProg()
	{
		       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT * FROM tbl_batchmaster where idProgrammaster in (select IdProgrammaster from tbl_programmaster) order by idProgrammaster";	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnviewstudentdetails($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b"=>"tbl_center"),'b.idcenter=a.Examvenue',array("b.*"))
										  ->join(array("c"=>"tbl_registereddetails"),'a.IDApplication=c.IDApplication',array("c.*"))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }


     public function fngetBatchcompanydetails($idcomps)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_companies"),array('a.*'))
										  ->where("a.IdCompany 	  = ?",$idcomps);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }




	
	public function fnInsertIntoStd($larrformData,$countloop,$batchid,$regpin,$companyaddress)
	{
		/*echo "<pre/>";
		print_r($larrformData);
		die();*/
		
		for($i=0;$i<$countloop;$i++)
		{
			$db = Zend_Db_Table::getDefaultAdapter();
           $table = "tbl_studentapplication";
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['candidatename'][$i],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['candidatedateofbirth'][$i],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateemail'][$i],
					'username' =>$larrformData['candidateemail'][$i],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>1,	
           					'IdBatch' =>$larrformData['activeset'][$i],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['candidateprog'][$i],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateicno'][$i],
					'password' =>$larrformData['candidateicno'][$i],	
           					'Payment' => 1, 
							'DateTime' =>0,	
           					'PermAddressDetails' =>$companyaddress,	
           					'Takafuloperator' =>$larrformData['candidatetakaful'][$i], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 0, 
							'Gender' =>$larrformData['candidategender'][$i],	
           					'Race' =>$larrformData['candidaterace'][$i],	
           					'Qualification' =>$larrformData['candidateeducation'][$i], 
							'State' =>000,	
           					'CorrAddress' =>000,	
           					'PostalCode' =>000, 
		 					'ContactNo' =>000,	
           					'MobileNo' =>000, 
		  					'ExamState'=>$larrformData['NewState'][$i],
		  					'ExamCity'=>$larrformData['NewCity'][$i],
		  					'Year'=>$larrformData['Year'][$i],
		  					'Examdate'=>$larrformData['Examdate'][$i],
		  					'Exammonth'=>$larrformData['NewMonth'][$i],
		  					'Examvenue'=>$larrformData['NewVenue'][$i],
		  					'Examsession'=>$larrformData['Newsession'][$i],		
		                    'pass'=>3	  
						);			
	       $db->insert($table,$postData);
	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');
	        
	        
	        $larrresult = self::fnviewstudentdetails($lastid);
	        
	       
	       $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($larrformData['candidatetakaful'][$i],$lastid);
	     
	        
	        
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['activeset'][$i],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>0,
            				'Cetreapproval' =>0,
            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);
	       
	       
	      self::sendmails($larrresult,$regid);
	       
		}
		

			    $where = 'idBatchRegistration = '.$batchid;
			     $postData = array(		
							'paymentStatus' => 2,
			     'Approved' => 0															
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	}
	
public function fnGetStatelistforcourse($idprog,$idyear){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler=?",$idyear)
										    ->group("e.idState");
										    
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnInsertIntoAdhocStd($larrformData,$countloop,$batchid,$regpin)
	{
		echo "<pre/>";
		/*print_r($larrformData);
		die();*/
		print_r($larrformData['activeset'][0]);die();
		for($i=0;$i<$countloop;$i++)
		{
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_studentapplication";
		    $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['candidatename'][$i],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['candidatedateofbirth'][$i],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateemail'][$i],
					'username' =>$larrformData['candidateemail'][$i],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>1,	
           					'IdBatch' =>$larrformData['activeset'][$i],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['candidateprog'][$i],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateicno'][$i],
					'password' =>$larrformData['candidateicno'][$i],		
           					'Payment' => 1, 
							'DateTime' =>0,	
           					'PermAddressDetails' =>00,	
           					'Takafuloperator' =>$larrformData['candidatehiddentakaful'][$i], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 0, 
							'Gender' =>$larrformData['candidategender'][$i],	
           					'Race' =>$larrformData['candidaterace'][$i],	
           					'Qualification' =>$larrformData['candidateeducation'][$i], 
							'State' =>000,	
           					'CorrAddress' =>000,	
           					'PostalCode' =>000, 
		 					'ContactNo' =>000,	
           					'MobileNo' =>000, 
		  					'ExamState'=>00,
		  					'ExamCity'=>0,
		  					'Year'=>0,
		  					'Examdate'=>0,
		  					'Exammonth'=>0,
		  					'Examvenue'=>0,
		                    'pass'=>3
						);			
	       $db->insert($table,$postData);
	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');
	        
	        
	        $larrresult = self::fnviewstudentdetails($lastid);
	        
	       
	       $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($larrformData['candidatehiddentakaful'][$i],$lastid);
	     
	        
	        
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['activeset'][$i],
            				'Approved' =>0,	
            				'Cetreapproval'=>0,	
		            		'IDApplication' =>$lastid,
            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);
	       
	       
	      self::sendmails($larrresult,$regid);
	       
		}
		

			    $where = 'idBatchRegistration = '.$batchid;
			     $postData = array(		
							'paymentStatus' => 2,	
			     			'Approved' => 0													
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	}
	public function fnBatchProgram($idprog)
	{
	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_batchmaster',array('key' =>'IdBatch','value' => 'BatchName'))
								->where('IdProgrammaster =?',$idprog);
			$result = $db->fetchAll($sql);
			return $result;
	}
	
	public function sendmails($larrresult,$regid)
	{

		//print_r($regid);
	require_once('Zend/Mail.php');
	require_once('Zend/Mail/Transport/Smtp.php');
				
				
	$lobjExamdetailsmodel = new App_Model_Examdetails();
			$larrresult= $lobjExamdetailsmodel->fnGetStudentDetailsPassfailforbatchcandidates($regid);	
	
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;

							$larrEmailTemplateDesc =  $lobjExamdetailsmodel ->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;					
										
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							//$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							//$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'],$lstrEmailTemplateBody);
										
										
										//$lstrEmailTemplateBody = str_replace("[Amount]",$postArray['mc_gross'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$regid,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
										/*$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;							
						
			
	}
						
	
	public function fnGetRace()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where('a.idDefType = 13');	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetEducation()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where('a.idDefType = 14');	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
public function fnGetDaysforcourse($idprog,$year){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulerdays"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.year = ?",$year)
										  ->order("b.Days");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
public function fnGetMonths($year,$Program){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->where("c.IdProgramMaster  = ?",$Program)
										  ->where("a.idnewscheduler = ?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetmonthsbetween($frommonth,$tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >='$frommonth' and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
public function fnGetmonthsbetween2($tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >=(SELECT month(curdate( ))) and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
  public function fnGetVenuedetails($year,$prog,$city)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler')
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array())
										  ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
                                                                                                                                                                                                          ->group("b.idvenue")
										  ->where("a.idnewscheduler =?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  
  public function fnGetsesssiondetails($year,$prog,$city,$venue,$idsession,$day,$month)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler')
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession',array("key"=>"g.idmangesession","value"=>"g.managesessionname"))
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter')
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array())
										  ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										   ->where("a.idnewscheduler =?",$year)
										     ->where("b.idvenue =?",$venue)
										     ->where("f.idmanagesession not in (select Examsession from  tbl_studentapplication where  Examsession in ($idsession) and ExamCity=$city and Examdate=$day and Examvenue=$venue and Year=$year)");
										  // ->group("f.idmanagesession");
										 
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
public function fnGetActiveSet($Program){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("a.*"))
										  ->join(array("c"=>"tbl_batchmaster"),'a.IdProgrammaster=c.IdProgrammaster')
										  ->join(array("d"=>"tbl_tosmaster"),'d.IdBatch=c.IdBatch')
										  ->where("a.IdProgramMaster  = ?",$Program)
										  ->where("d.Active=1");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	 public function fngetstudentsemail($Program,$idyear)
		 {
		 	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_studentapplication"),array("a.*"))
							 				  ->where("a.Program = ?",$Program)
							 				 ->where("a.EmailAddress = ?",$idyear);
					$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
					return $larrResult;
		 }
		 
  
 }