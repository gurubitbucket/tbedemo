<?php
class App_Model_Takafulapplication extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
 
     /*
      * function to fetch all the details
      */
    public function fngetTakafulOperator($IdTakaful) { //Function to get the user details
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_takafuloperator"),array("a.*"))								
								 ->where("a.idtakafuloperator=?",$IdTakaful);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
	public function fngetPaymentDetails($idPayment){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistrationdetails"),array("a.*"))
										  ->joinLeft(array("b" => "tbl_batchregistration"),'a.idBatchRegistration=b.idBatchRegistration',array("b.*"))
										  ->joinLeft(array("c" => "tbl_programmaster"),'a.idProgram=c.IdProgrammaster ',array("c.ProgramName"))
										  ->where("b.idBatchRegistration  = ?",$idPayment);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
     public function fnGetCaptionName()
     {
     	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_config"),array("a.*"))								
								 ->where("a.idConfig=1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
public function fngetintialdiscount()
{
	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_config"),array("a.*"))								 						
								 ->where("a.idConfig= 1");
								 //->where("a.paymentStatus= 1");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult; 		
}

	public function fnGetProgramName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										   ->join(array("c" => "tbl_batchmaster"),'a.IdProgrammaster=c.IdProgrammaster')
										  ->join(array("b"=>"tbl_tosmaster"),'b.IdBatch=c.IdBatch')
										  ->where("a.Active  = ?","1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
 public function fngetRegistratinpin($IdCompany) { //Function to get the user details
 
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_batchregistration"),array("key"=>"a.registrationPin","value"=>"a.registrationPin"))
								 ->where("a.idBatchRegistration in (select IDApplication from tbl_studentpaymentoption where companyflag=2)")
								 ->where("a.paymentStatus=1")
								 ->where("a.Approved=1")
								 ->where("a.AdhocDate=0000-00-00")						
								 ->where("a.IdCompany=?",$IdCompany);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
	
	  public function fnInsertPaymentdetails($larrformData){  	
  		 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_batchregistration";
            $postData = array(		
							'idCompany' => $larrformData['idTakaful'],	
           					'totalNoofCandidates' =>$larrformData['totalNoofCand'],	
           					'totalAmount' => $larrformData['grossAmt'],           				                           
                            'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
            				 'AdhocDate' => '0000-00-00',	
            				'AdhocVenue' =>'others',
            				'Approved'=>0					
						);	
					
	     $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_batchregistration","idBatchRegistration");		 
	 	  $table = "tbl_batchregistrationdetails";
		 for($i=0;$i<count($larrformData['idProgram']);$i++){		
		         $postData = array('idBatchRegistration' => $lastid,	
							         'idProgram' => $larrformData['idProgram'][$i],	
							         'eachAmount' => $larrformData['eachAmount'][$i],	
							         'noofCandidates' => $larrformData['noofCandidates'][$i],	
		           					'toatlAmount' =>$larrformData['toatlAmount'][$i],	
		           					'registrationPin' => 0,
		                           'discount'=>$larrformData['discountamount'][$i],               				                           
		              	              'UpdDate' => $larrformData['UpdDate'],	
		            				'UpdUser' =>$larrformData['UpdUser']	);					
			     $db->insert($table,$postData);
		 }
		return $lastid;
  	
  } 
  
  public function  fnInsertStudentPaymentdetails($mod,$lastInsId)
  {
  
  	 $db = Zend_Db_Table::getDefaultAdapter();
  	$table = "tbl_studentpaymentoption";
		  $postData = array(		
							'IDApplication' =>$lastInsId,	
           					'ModeofPayment' =>$mod,	
           					'companyflag' => 2,           				                           
                            /*'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
            				 'AdhocDate' => '0000-00-00',	
            				'AdhocVenue' =>'others'			*/		
						);
						  $db->insert($table,$postData);
						   $lastids  = $db->lastInsertId("tbl_studentpaymentoption","idstudentpaymentoption");	
						   return $lastids;
						 
							
  }
  
 public function fngetmodeofpayment($mod)
  {
  	 $db = Zend_Db_Table::getDefaultAdapter();
  	 $lstrSelect = $db->select()
				->from(array("a" => "tbl_studentpaymentoption"),array("a.ModeofPayment"))
				 ->where("a.idstudentpaymentoption = ?",$mod);	
				$larrResult = $db->fetchRow($lstrSelect);
						  return $larrResult;	
  }
  
  
  public function fnInsertPaymentdetailsadhoc($larrformData){  	
  		 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_batchregistration";
            $postData = array(		
							'idCompany' => $larrformData['idTakaful'],	
           					'totalNoofCandidates' =>$larrformData['totalNoofCand'],	
           					'totalAmount' => $larrformData['grossAmt'],           				                           
                            'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
            				 'AdhocDate' => $larrformData['AdhocDate'],	
            				'AdhocVenue' =>$larrformData['AdhocVenue'],
            				'Approved'=>0						
						);	
					
	     $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_batchregistration","idBatchRegistration");		 
	 	  $table = "tbl_batchregistrationdetails";
		 for($i=0;$i<count($larrformData['idProgram']);$i++){		
		         $postData = array('idBatchRegistration' => $lastid,	
							         'idProgram' => $larrformData['idProgram'][$i],	
							         'eachAmount' => $larrformData['eachAmount'][$i],	
							         'noofCandidates' => $larrformData['noofCandidates'][$i],	
		           					'toatlAmount' =>$larrformData['toatlAmount'][$i],	
		           					'registrationPin' => 0,           				                           
		              	              'UpdDate' => $larrformData['UpdDate'],	
		            				'UpdUser' =>$larrformData['UpdUser']	);					
			     $db->insert($table,$postData);
		 }
		return $lastid;
  	
  } 
  
     
public function fnGetProgramFee($idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT sum(abc.amount) from 

(SELECT max(a.EffectiveDate),a.Rate as amount  from tbl_programrate a,tbl_accountmaster b
where a.idProgram=$idprog and 
 b.idAccount= a.IdAccountmaster and  
 b.Active=1 and a.Active=1  and b.idAccount not in (select t.idAccount from tbl_accountmaster t where t.duringRegistration=1 ) group by a.IdAccountmaster) as abc ";
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
  
     /* function fnGetProgramFee($idProgram){
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	//Program Fee
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programrate"),array("a.*","max(a.EffectiveDate)"))								
								 ->where("a.idProgram=?",$idProgram)
								 ->group("a.IdAccountmaster");									 
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);		
		$sumProgram = 0;
		for($j=0;$j<count($larrResult);$j++){
			$sumProgram = $sumProgram+$larrResult[$j]['Rate'];
		}
		//Course Fee
		$sql = "SELECT sum(Rate) tot FROM `tbl_courserate`  WHERE `IdAccountmaster` IN(						
						SELECT w.IdAccountmaster  FROM(SELECT `a`.*, max(a.EffectiveDate) FROM `tbl_programrate` AS `a` WHERE (a.idProgram=$idProgram) 
						GROUP BY `a`.`IdAccountmaster`)w
						)";
		$larrsumCourse = $lobjDbAdpt->fetchRow($sql);
		//Discount
		$sql = "SELECT SUM(Amount) dis FROM tbl_accounthead WHERE idAccountHead IN (  SELECT max(ah.idAccountHead) idAcc FROM `tbl_accounthead` ah  WHERE `idAccount` IN(
								SELECT w.IdAccountmaster  FROM(SELECT `a`.*, max(a.EffectiveDate) FROM `tbl_programrate` AS `a` WHERE (a.idProgram=$idProgram) 						
								GROUP BY `a`.`IdAccountmaster`)w)
								GROUP BY ah.idAccount)";
		$larrsumDisc = $lobjDbAdpt->fetchRow($sql); 
		
		return $sumProgram+$larrsumCourse['tot']-$larrsumDisc['dis'];		
    } */
    
  /*
   * function to get the registration pin details
   */
	public function fngetBatchDetails($Idreg) { //Function to get the user details 		
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_batchregistration"),array("a.*"))								 						
								 ->where("a.registrationPin=?",$Idreg)
								 ->where("a.paymentStatus= 1");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;
     }
     
public function fngetprogramdiscount($idprog,$noofcandidates)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programmaster"),array("a.*"))	
								 ->join(array("b"=>"tbl_programrate"),'a.IdProgrammaster=b.idProgram',array("b.*"))							 						
								 ->where("a.IdProgrammaster=?",$idprog)
								 ->where("a.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
       
		for($i=0;$i<count($larrResult);$i++)
		{
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_accounthead"),array("a.*"))	
								 						 						
								 ->where("a.idAccount=?",$larrResult[$i]['IdAccountmaster'])
								 ->where("a.minnostd in (select b.minnostd from tbl_accounthead as  b where a.idAccount=b.idAccount and  b.minnostd <=$noofcandidates)")
                                 ->where("a.Active=1")
								 ->order("a.minnostd desc");
		   $larraysum = $lobjDbAdpt->fetchAll($lstrSelect);
		   $sum = $larraysum;
		  // $sum= $sum+$sum;
		}
		return $sum;
	}
	
	
public function fnGetAmountInWords($Amount)
     {//echo $Amount;die;
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = "SELECT CONCAT(ucwords(str_numtowords($Amount)),' ','Only') as Amount";		
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
				return $larrResult;
     }
     
	public function fnGetExamDetails($IdApplication)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchregistration"),array('a.*'))
										  ->where("a.idBatchRegistration  = ?",$IdApplication);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }

  public function updatecompanypass($password,$idcomp)
     {
     	 $db = Zend_Db_Table::getDefaultAdapter();
     	$data = array('Password' => $password);
		$where['idtakafuloperator = ?']= $idcomp;		
		$db->update('tbl_takafuloperator', $data, $where);	
     }
	
	
	
  }