<?php
	class App_Model_Companyreport1 extends Zend_Db_Table {
		

		public function fngetprogramnames()
	   {
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }

	   
		public function fncompanysearchdetails($larrformData,$idcompany)
		{ 
			
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['Status']) $Status = $larrformData['Status'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(d.Year,'')) as ExamDate",'a.ICNO as ICNO',"DATE_FORMAT(date(a.UpdDate),'%d-%m-%Y') as RegDate")) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_registereddetails'),'c.IDApplication= a.IDApplication',array('c.Approved as Status','c.Regid as RegId'))			
			           ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler',array())
			           ->join(array('f' => 'tbl_batchregistration'),'c.RegistrationPin = f.registrationPin',array())
			           ->join(array('e' => 'tbl_studentpaymentoption'), 'e.IDApplication= f.idBatchRegistration',array())
			           ->where('e.companyflag=?',1)
			           ->where('f.idCompany = ?',$idcompany);
			
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName = ?",$name);
			if($larrformData['Status']==5)
			 { 
				$lstrSelect->where("c.Approved = 0");
			 }
			 
		     if($larrformData['Status']==6)
			{
				$lstrSelect->where("c.Approved = 1"); 
			}
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename);
			
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
	   
	   /*public function fngetcompanynames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"CompanyName")) 				 
				 				 ->order("a.CompanyName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   
	   public function fngetcenternames()
	   {
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername")) 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   
	   public function fngettakafulnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 				 
				 				 ->order("a.TakafulName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   
	   public function fngettakafulcompanies($tid)
	   {
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	$lstrSelect = $lobjDbAdpt->select()
	   	                         ->from(array("a"=>"tbl_studentapplication"),array('a.Gender','a.IDApplication as IDApplication','a.FName as StudentName','year(curdate())-year(a.DateOfBirth) as Age','a.ICNO as ICNO',"CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(e.Year,'')) as Date"))
	   	                         ->join(array("b"=>"tbl_takafuloperator"),'b.idtakafuloperator= a.Takafuloperator',array("b.TakafulName as TakafulCompany"))
	   	                         ->join(array("c"=>"tbl_definationms"),'c.idDefinition = a.Qualification',array("c.DefinitionDesc as Qualification"))
	   	                          ->join(array("d"=>"tbl_definationms"),'d.idDefinition = a.Race',array("d.DefinitionDesc as Race"))
	   	                         ->join(array('e' => 'tbl_newscheduler'),'a.Year = e.idnewscheduler',array())
	   	                         ->join(array('g' => 'tbl_registereddetails'),'g.IDApplication = a.IDApplication',array('g.Regid as ExamNo'))
	   	                         ->join(array('f' => 'tbl_center'),'f.idcenter= a.Examvenue',array('f.centername as Venue'))
	   	                          ->where("b.idtakafuloperator= ?",$tid)
	   	                          ->where("c.idDefType=14")
	   	                           ->where("d.idDefType=13"); 
	   	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	    return $larrResult;
	   }
	   
		public function fnReportSearchDetails($larrformData)
		{ 
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Venue']) $venue = $larrformData['Venue'];
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(d.Year,'')) as Date",'a.ICNO as ICNO','a.Pass as Result')) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_center'),'c.idcenter= a.Examvenue',array('c.centername as Venue'))			
			             ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler');		
			if($larrformData['Venue']) $lstrSelect->where("c.centername= ?",$venue);
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName = ?",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO = ?",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename);
			
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
		
		public function fnReportSearchByState($larrformData)
		{ 
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['State']) $state = $larrformData['State'];
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(d.Year,'')) as Date",'a.ICNO as ICNO','a.Pass as Result')) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_state'),'c.idState = a.ExamState',array('c.StateName as State'))			
			           ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler');
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName = ?",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO = ?",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename); 
			if($larrformData['State']) $lstrSelect->where("c.StateName= ?",$state);
			
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);	
		    return $larrResult;              
		}
		
		public function fnReportSearchByTakaful($larrformData)
		{ 
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Takafulname']) $Takafulname = $larrformData['Takafulname'];
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(d.Year,'')) as Date",'a.ICNO as ICNO','a.Pass as Result')) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_takafuloperator'),'c.idtakafuloperator = a.Takafuloperator',array('c.TakafulName as TakafulName'))			
			            ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler');
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName = ?",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO = ?",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename); 
			if($larrformData['Takafulname']) $lstrSelect->where("c.idtakafuloperator= ?",$Takafulname);
			
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
		
	   public function fnReportSearchStatus($larrformData)
		{ 
			
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			if($larrformData['Result']) $Result = $larrformData['Result'];
					
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(d.Year,'')) as Date",'a.ICNO as ICNO','a.Pass as Result')) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_center'),'c.idcenter= a.Examvenue',array('c.centername as Venue'))	
			           ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler');
			           		
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName = ?",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO = ?",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename); 
			if($larrformData['Result'] == '')
			{
				
			}
			else 
			{
				$lstrSelect->where("a.Pass= ?",$Result);
			}
			
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;           
		}
		

	  public function fngetexamanddatedetails($larrformData)
		{ 
			if($larrformData['Month']) $month = $larrformData['Month'];
			if($larrformData['Year'])  $year = $larrformData['Year'];
			if($larrformData['Day']) $day = $larrformData['Day'];	
			if($larrformData['Venues']) $venue = $larrformData['Venues'];
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = "SELECT b.idcenter as idcenter,b.centername as Venuename,CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(c.Year,'')) as Date, a.Pass as Result, b.centername as Venue
                           FROM `tbl_studentapplication` AS `a`,`tbl_center` AS `b`,`tbl_newscheduler` as `c`
                           WHERE (a.Examvenue=b.idcenter) and (a.Year = c.idnewscheduler) and (c.Year=$year) and (a.Examdate = $day) and (a.Exammonth=$month) and (b.idcenter=$venue)";
		
		    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
		
		
	public function fngetstudentpaymentdetails($larrformData)
		{ 
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			$regpin="0000000";
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("FName as StudentName","CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(d.Year,'')) as Date",'a.ICNO as ICNO','a.Pass as Result','a.Amount as Amount')) 	   			   
			          ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('e' => 'tbl_registereddetails'),'a.IDApplication = e.IDApplication',array('e.Regid as Reg_ID','e.idregistereddetails'))
			          ->join(array('c' => 'tbl_center'),'c.idcenter= a.Examvenue',array('c.centername as Venue'))			
			           ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler',array())	
			           ->where("e.RegistrationPin=?",$regpin)
			           ->where("a.Payment=1")
			           ->group("e.Regid");
			           	
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName = ?",$name);
			if($larrformData['ICNO'])           $lstrSelect->where("a.ICNO = ?",$icno);
			
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			
		    return $larrResult;              
		}
		
		
	public function fngetcompanypaymentdetails($larrformData)
		{ 
			$flag=1;
			if($larrformData['Companyname']) $cname=$larrformData['Companyname'];
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_companies"),array("a.IdCompany as IdCompany","a.CompanyName as CompanyName")) 	   			   
			           ->join(array('b' => 'tbl_batchregistration'),'b.idCompany = a.IdCompany',array('b.idBatchRegistration as idBatchRegistration','b.registrationPin as RegistrationPIN','totalNoofCandidates as NoOfStudents','totalAmount as Amount'))
			           ->join(array('c' => 'tbl_studentpaymentoption'),'c.IDApplication = b.idBatchRegistration',array())	
			           ->where("c.companyflag = ?",$flag);
			if($larrformData['Companyname'])       $lstrSelect->where("a.IdCompany = ?",$cname);	
			
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
		
		
	public function fngettakafulpaymentdetails($larrformData)
		{ 
			$flag=2;
			if($larrformData['Takafulname']) $tname=$larrformData['Takafulname'];
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		    $lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_takafuloperator"),array("a.idtakafuloperator as idtakafuloperator","a.TakafulName as TakafulName")) 	   			   
			           ->join(array('b' => 'tbl_batchregistration'),'b.idCompany = a.idtakafuloperator',array('b.idBatchRegistration as idBatchRegistration','b.registrationPin as RegistrationPIN','totalNoofCandidates as NoOfStudents','totalAmount as Amount'))
			           ->join(array('c' => 'tbl_studentpaymentoption'),'c.IDApplication = b.idBatchRegistration',array())	
			           ->where("c.companyflag = ?",$flag);
			           
			if($larrformData['Takafulname']) $lstrSelect->where("a.idtakafuloperator = ?",$tname);	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}*/
		/*//Get UserNameList
		public function fnGetUserNameList(){
   			$db = Zend_Db_Table::getDefaultAdapter();
			$select =$db->select()->from('tbl_user')->order('iduser');
			$result = $db->fetchAll($select);
			return $result;
   		}*/
		
		/*//Log Table Report Search Function
		public function fnReportSearchDetails($postData){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("cm"=>"tbl_collegemaster"),array("cm.IdCollege AS IdCollege","cm.CollegeName AS BranchName"))
       								->join(array("pbl"=>"tbl_programbranchlink"),'pbl.IdCollege=cm.IdCollege',array("pbl.Quota AS Quota"))
       								->join(array("p"=>"tbl_program"),'p.IdProgram=pbl.IdProgram',array("p.ProgramName as ProgramName"))
       								->join(array("sa"=>"tbl_studentapplication"),'sa.IDCourse=p.IdProgram',array("COUNT(sa.IDCourse) as Applied","COUNT(sa.Accepted) as Accepted","COUNT(sa.Offered) as Offered"));
       								
					  				
       		if(isset($postData['BranchList']) && !empty($postData['BranchList']) ){
				$lstrSelect = $lstrSelect->where("cm.IdCollege  = ?",$postData['BranchList']);
				}	
			if(isset($postData['ProgramList']) && !empty($postData['ProgramList']) ){
				$lstrSelect = $lstrSelect->where("pbl.IdProgram  = ?",$postData['ProgramList']);
				}	
			
			$lstrSelect //->where("cm.CollegeType = 1")
						->where("cm.IdCollege = sa.idCollege")
						//->where("pbl.Active = 1")
		 				->group("sa.idCollege")
		 				->group("pbl.IdProgram")
		 				->order("BranchName");

		 				 
			//echo $lstrSelect;die();				
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
			
		}*/
 
}
