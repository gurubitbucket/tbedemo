<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class RegistrationController extends Zend_Controller_Action { //Controller for the User Module

	
public function init() { //initialization function
		$this->_helper->layout()->setLayout('/batch/usty1');	
		//$this->Url = "http://192.168.1.103/tbe";	
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
			//$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
		$this->lobjstudentForm = new App_Form_Studentapplication(); //intialize user lobjuserForm
		$this->lobjCommon=new App_Model_Common();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		  	$this->lobjdeftype = new App_Model_Definitiontype();
	}
	

public function indexAction() { // action for search and view
	
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		$count=0;
    $larrinitialresult=$this->lobjstudentmodel->fngetintialconfigdetails();
	if($larrinitialresult[FldTxt1])
	{
		$this->view->Txt1=$larrinitialresult[FldTxt1];
		$this->view->Dpd1=$larrinitialresult[FldDdwn1];
	$count=$count+$larrinitialresult[FldTxt1];
	}
	if($larrinitialresult[FldTxt2])
	{
		$this->view->Txt2=$larrinitialresult[FldTxt2];
		$this->view->Dpd2=$larrinitialresult[FldDdwn2];
	$count=$count+$larrinitialresult[FldTxt2];
	}
	if($larrinitialresult[FldTxt3])
	{
		$this->view->Txt3=$larrinitialresult[FldTxt3];
		$this->view->Dpd3=$larrinitialresult[FldDdwn3];
	$count=$count+$larrinitialresult[FldTxt3];
	}
	if($larrinitialresult[FldTxt4])
	{
		$this->view->Txt4=$larrinitialresult[FldTxt4];
		$this->view->Dpd4=$larrinitialresult[FldDdwn4];
	$count=$count+$larrinitialresult[FldTxt4];
	}
	if($larrinitialresult[FldTxt5])
	{
		$this->view->Txt5=$larrinitialresult[FldTxt5];
		$this->view->Dpd5=$larrinitialresult[FldDdwn1];
	$count=$count+$larrinitialresult[FldTxt5];	
	}
	if($larrinitialresult[FldTxt6])
	{
		$this->view->Txt6=$larrinitialresult[FldTxt6];
		$this->view->Dpd6=$larrinitialresult[FldDdwn6];
	$count=$count+$larrinitialresult[FldTxt6];
	}
	//echo $count;die;
	$this->view->counts=$count;
	}

public function newstudentAction() { //Action for creating the new user
	$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
	$auth = Zend_Auth::getInstance();
	$auth->getIdentity()->iduser = 1;	
	//////////////////////////////////////////////////////////////////////////////////////
	
	
	
	$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		
		$larr=$this->lobjstudentmodel->fngetminimumage();
				$age=$larr[0]['MinAge'];
				$eligibility = ($year)-($age);
		$year=$eligibility;	
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));
		//echo $yesterdaydate;die();
		$this->view->yesdate=$yesterdaydate;
		
  /////////////////////////////////////////////////////////////////////////////
	
	
	$count=0;
    $larrinitialresult=$this->lobjstudentmodel->fngetintialconfigdetails();
	if($larrinitialresult[FldTxt1])
	{
		$this->view->Txt1=$larrinitialresult[FldTxt1];
		$this->view->Dpd1=$larrinitialresult[FldDdwn1];
	$count=$count+$larrinitialresult[FldTxt1];
	}
	if($larrinitialresult[FldTxt2])
	{
		$this->view->Txt2=$larrinitialresult[FldTxt2];
		$this->view->Dpd2=$larrinitialresult[FldDdwn2];
	$count=$count+$larrinitialresult[FldTxt2];
	}
	if($larrinitialresult[FldTxt3])
	{
		$this->view->Txt3=$larrinitialresult[FldTxt3];
		$this->view->Dpd3=$larrinitialresult[FldDdwn3];
	$count=$count+$larrinitialresult[FldTxt3];
	}
	if($larrinitialresult[FldTxt4])
	{
		$this->view->Txt4=$larrinitialresult[FldTxt4];
		$this->view->Dpd4=$larrinitialresult[FldDdwn4];
	$count=$count+$larrinitialresult[FldTxt4];
	}
	if($larrinitialresult[FldTxt5])
	{
		$this->view->Txt5=$larrinitialresult[FldTxt5];
		$this->view->Dpd5=$larrinitialresult[FldDdwn1];
	$count=$count+$larrinitialresult[FldTxt5];	
	}
	if($larrinitialresult[FldTxt6])
	{
		$this->view->Txt6=$larrinitialresult[FldTxt6];
		$this->view->Dpd6=$larrinitialresult[FldDdwn6];
	$count=$count+$larrinitialresult[FldTxt6];
	}
	//echo $count;die;
	$this->view->counts=$count;
	
	
	//////////////////////////////////////////////////////////////////////////////////

	
	if ($this->_request->isPost ()) {
			$larrformData = $this->_request->getPost ();
			//print_r($larrformData);die();
			
		/*	if(($larrformData['ArmyNo'] =='')&&(($larrformData['ICN1']=='')||($larrformData['ICN2']=='')||($larrformData['ICN3']=='')||($larrformData['ICN4']=='')||($larrformData['ICN5']=='') || ($larrformData['ICN6']=='')))
			{
				echo '<script language="javascript">alert("Please ente IC Number or Army Number")</script>';
				 echo "<script>parent.location = '".$this->view->baseUrl()."/registration';</script>";
				die();
		
			}*/
			$this->view->ic1 = $larrformData['ICN1'];
			$this->view->ic2 = $larrformData['ICN2'];
			$this->view->ic3 = $larrformData['ICN3'];
			$this->view->ic4 = $larrformData['ICN4'];
			$this->view->ic5 = $larrformData['ICN5'];
			$this->view->ic6 = $larrformData['ICN6'];
			
			$icno=$larrformData['ICN1'].''.$larrformData['ICN2'].''.$larrformData['ICN3'].''.$larrformData['ICN4'].''.$larrformData['ICN5'].''.$larrformData['ICN6'];
			
			$this->view->icno1=$icno;
			/*print_R($icno);
			die();*/
			$ArmyNo=$larrformData['ArmyNo'];
			
			$resltss = $this->lobjstudentmodel->fnGetICorArmyNo();
			$studentdetails=0;
			$flag=0;
			if($icno)
			{
			       for($i=0;$i<count($resltss);$i++)
				 	{
				 		
				 		if($icno == $resltss[$i]['ICNO'])
				 		{
							 	$studentdetails = $icno;
								$larrresults = $this->lobjstudentmodel->fnGetStudentdetailsbasedonicno($icno);
							 	$flag=1;
				 	    } 
				 	
				 	}
			}
			if($flag==0)
			{
			
					if($ArmyNo)
					{
						for($i=0;$i<count($resltss);$i++)
							{
								 		
								 if($ArmyNo == $resltss[$i]['ArmyNo'])
								 	{
										$studentdetails = $ArmyNo;
										$larrresults = $this->lobjstudentmodel->fnGetStudentdetailsbasedonarmyno($ArmyNo);
								 	 } 
								 	
							}
					}
			}//end if for comparing flag
			
			 
      }//end if for comparing post of data
      	$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjstudentForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		$larrstate = $this->lobjstudentmodel->fnGetStateName();
		$this->lobjstudentForm->State->addMultiOptions($larrstate);
		
		$larrQualification = $this->lobjstudentmodel->fnGetEducationDetails();
		$this->lobjstudentForm->Qualification->addMultiOptions($larrQualification);
		
		
$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Race');
	 /*   print_r($larrdefmsresultset);
	    die();*/
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjstudentForm->Race->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
		
 if(count($larrresults)==0)
 {
		//$this->_helper->layout->disableLayout();
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		
	/*	$larrbatchresult = $this->lobjstudentmodel->fnGetBatchName();
		$this->lobjstudentForm->IdBatch->addMultiOptions($larrbatchresult);*/
		

 }
 
 
	
 else 
 {
 	
 	$this->view->lobjstudentForm->FName->setValue($larrresults['FName']);
	$this->view->lobjstudentForm->MName->setValue($larrresults['MName']);
	$this->view->lobjstudentForm->LName->setValue($larrresults['LName']);
	$this->view->lobjstudentForm->DateOfBirth->setValue($larrresults['DateOfBirth']);
	$this->view->lobjstudentForm->PermCity->setValue($larrresults['PermCity']);
	$this->view->lobjstudentForm->EmailAddress->setValue($larrresults['EmailAddress']);		 	
	$this->view->lobjstudentForm->Takafuloperator->setValue($larrresults['Takafuloperator']);
	$this->view->lobjstudentForm->ICNO->setValue($larrresults['ICNO']);
	$this->view->lobjstudentForm->PermAddressDetails->setValue($larrresults['PermAddressDetails']);	
	$this->view->lobjstudentForm->Gender->setValue($larrresults['Gender']);	
	$this->view->lobjstudentForm->CorrAddress->setValue($larrresults['CorrAddress']);	
	$this->view->lobjstudentForm->ArmyNo->setValue($larrresults['ArmyNo']);	
	$this->view->lobjstudentForm->State->setValue($larrresults['State']);
	$this->view->lobjstudentForm->PostalCode->setValue($larrresults['PostalCode']);
	$this->view->lobjstudentForm->Race->setValue($larrresults['Race']);
    $this->view->lobjstudentForm->ContactNo->setValue($larrresults['ContactNo']);
    $this->view->lobjstudentForm->MobileNo->setValue($larrresults['MobileNo']);
 }	
 		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();
		$this->lobjstudentForm->Program->addMultiOptions($larrbatchresult);
	    
	/*	$larrdaysarresult = $this->lobjstudentmodel->fnGetDaysforcourse(1);
	
		
		$this->view->monday =empty($larrdaysarresult[0]['Days'])? '0':'1';
		$this->view->tuesday = empty($larrdaysarresult[1]['Days'])?'0':'1';
		$this->view->wednesday = empty($larrdaysarresult[2]['Days'])?'0':'1';;
		$this->view->thursday = empty($larrdaysarresult[3]['Days'])?'0':'1';;
		$this->view->friday = empty($larrdaysarresult[4]['Days'])?'0':'1';;
		$this->view->saturday = empty($larrdaysarresult[5]['Days'])?'0':'1';;
		$this->view->sunday = empty($larrdaysarresult[6]['Days'])?'0':'1';;*/
		
		
		//$this->lobjstudentForm->Program->addMultiOptions($larrbatchresult);
	//	die();
		//$this->lobjstudentForm->NewState->fnGetYearlistforcourse();
		
	$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		$this->view->lobjstudentForm->UpdDate->setValue ( $ldtsystemDate );
		//$larrQualification=$this->lobjstudentmodel->fnGetEducation();
		//$this->view->lobjstudentForm->Qualification->setValue($larrQualification);
		 
		$this->view->lobjstudentForm->UpdUser->setValue ( 1);
		$this->view->lobjstudentForm->ICNO->setValue($icno);

		
	/*	$this->lobjstudentForm->PermCountry->addMultiOptions($lobjcountry);
		$this->lobjstudentForm->CorrsCountry->addMultiOptions($lobjcountry);*/


		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			
			$larrformData['UpdDate'] =  date ( 'Y-m-d:H-i-s' );
			$larrformData['UpdUser'] = 1;
			$larrformData['Qualification']=0;
			unset($larrformData['Save']);
			unset($larrformData['Close']);
			$larrformData['ICNO'] = $larrformData['ICN1'].''.$larrformData['ICN2'].''.$larrformData['ICN3'].''.$larrformData['ICN4'].''.$larrformData['ICN5'].''.$larrformData['ICN6'];
			unset($larrformData['ICN1']);
			unset($larrformData['ICN2']);
			unset($larrformData['ICN3']);
			unset($larrformData['ICN4']);
			unset($larrformData['ICN5']);
			unset($larrformData['ICN6']);
			/*print_r($larrformData);
			die();*/
				$result = $this->lobjstudentmodel->fnAddStudent($larrformData); //instance for adding the lobjuserForm values to DB
				
				echo $result;
				$this->_redirect( $this->baseUrl . "/registration/selectvenue/insertedId/".$result);
				/*
                $lastpaymentid = $this->lobjstudentmodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$result); 
                echo $lastpaymentid;
                 $paymentmod = $this->lobjstudentmodel->fngetmodeofpayment($lastpaymentid); 
                 //print_r($paymentmod);die();
                if($paymentmod['ModeofPayment']!=2)
                {
                	$this->_redirect( $this->baseUrl . "/registration/index");
                }
				else 
				{
				$this->_redirect( $this->baseUrl . "/registration/confirmpayment/insertedId/".$result);
				}*/
				//$this->_redirect($this->view->url(array('module'=>'reg' ,'controller'=>'index', 'action'=>'confirmpayment','insertedId'=>$result),'default',true));
		
		}

	}
	public function savedetailsAction()
	{
		$this->_helper->layout->disableLayout();
			if ($this->_request->isPost ()) {
			$larrformData = $this->_request->getPost ();
			$larrformData['UpdDate'] =  date ( 'Y-m-d:H-i-s' );
			$larrformData['UpdUser'] = 1;
			$larrformData['Qualification']=0;
			unset($larrformData['Save']);
			unset($larrformData['Close']);
			$larrformData['ICNO'] = $larrformData['ICN1'].''.$larrformData['ICN2'].''.$larrformData['ICN3'].''.$larrformData['ICN4'].''.$larrformData['ICN5'].''.$larrformData['ICN6'];
			unset($larrformData['ICN1']);
			unset($larrformData['ICN2']);
			unset($larrformData['ICN3']);
			unset($larrformData['ICN4']);
			unset($larrformData['ICN5']);
			unset($larrformData['ICN6']);
			/*print_r($larrformData);
			die();*/
				$result = $this->lobjstudentmodel->fnAddStudent($larrformData); //instance for adding the lobjuserForm values to DB
				
				echo $result;
				$this->_redirect( $this->baseUrl . "/registration/selectvenue/insertedId/".$result);
			}
	}
	
	public function selectvenueAction()
	{
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$lintidstudent = $this->_getParam('insertedId');
		$this->view->id = $lintidstudent;
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetails($lintidstudent);
		
		
		$venueselect = $this->lobjstudentmodel->fnGetVenuedetails($larrresult['Year'],$larrresult['Program'],$larrresult['ExamCity']);
		echo "<pre/>";
		//print_r($venueselect);
		//die();
		
		$larrdate=$larrresult['Year'].'-'.$larrresult['Exammonth'].'-'.$larrresult['Examdate'];
		//echo $larrdate;die();
		//$contvenueselect = $this->lobjstudentmodel->fnCountVenuedetails($larrdate);
		
		$this->view->dates = $larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['Year'];
		
		$this->view->larrvenues = $venueselect;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			
			print_r($larrformData);
			
			$idvenue = $larrformData['idvenues'][0];
			
		//	echo $idvenue;
			$arrworkphone = explode("-",$idvenue);
		      $idcenter= $arrworkphone [0];
	          $idsession= $arrworkphone[1];
			//die();
			//print_R($idvenue);
			$larrresuls = $this->lobjstudentmodel->fnupdateexamvenuess($idcenter,$idsession,$larrformData['idapplication']);
			//die();
			
		
			  if($larrformData['ModeofPayment']!=2)
                {
                	$this->_redirect( $this->baseUrl . "/registration/index");
                }
				else 
				{
				$this->_redirect(  $this->baseUrl  . "/registration/confirmpayment/insertedId/".$larrformData['idapplication']);
				}
			
		}
	}
	
public function printreportAction() 
{			
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		
			$IdApplication = ( int ) $this->_getParam ('insertedId');
			
			
			$larrresult = $this->lobjstudentmodel->fnGetExamDetails($IdApplication);
			$AmountInWords = $this->lobjstudentmodel->fnGetAmountInWords($larrresult['Amount']);
			$StudentType=1;
		    
			
			//object to initialize ini file
		$lobjAppconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini','development');
									
		    try 
		    {	
	            //java class
	            $lobjdbdriverclass = new Java("java.lang.Class");
	            
	            //set db driver
	            $lobjdbdriverclass->forName("com.mysql.jdbc.Driver");
	
	            //driver manager object
	            $lobjdrivermanager = new Java("java.sql.DriverManager");
	            
	            //get the db connection
				$lstrConnection  =  "jdbc:mysql://".
										$lobjAppconfig->resources->db->params->host."/".
										$lobjAppconfig->resources->db->params->dbname."?user=".
										$lobjAppconfig->resources->db->params->username."&password=".
										$lobjAppconfig->resources->db->params->password;
														
				$lobjconnection = $lobjdrivermanager->getConnection($lstrConnection);
	            
	            //Jasper Compile manager object
	            $lobjcompileManager = new Java(
	            					"net.sf.jasperreports.engine.JasperCompileManager");
	            
	            echo "CompileManager object created</br>";
	            $lstrreportdir = realpath(".") . "/report/";
	            $lstrimagepath = realpath(".") . "/images/";
	
	             //compiled report path
	              $lobjreport = $lobjcompileManager->compileReport(realpath($lstrreportdir."StudentInvoice.jrxml"));
	            
	            
	            //Jasper Fill Manager object
	            $lobjfillManager = new Java(
	            					"net.sf.jasperreports.engine.JasperFillManager");
	            $int1 = new Java("java.lang.Integer");
	            //Hashmap object
	            //print_r($lstrreportdir);die();
	            $lobjparams = new Java("java.util.HashMap");
	          	$lobjparams->put("IDAPPLICATION",$IdApplication);
	          	$lobjparams->put ("IMAGEPATH", $lstrimagepath . "ibfim.jpg" );
	          	$lobjparams->put("AMOUNTINWORDS",$AmountInWords['Amount']);
	          	$lobjparams->put("STUDENTTYPE",$StudentType);
	           
	           echo "Fill Manager</br>";
	            					
	            //Jasper Print Object
	            $lobjjasperPrint = $lobjfillManager->fillReport(
	            					$lobjreport, $lobjparams, $lobjconnection);
	            					
	            echo "Jasper Printed</br>";
	            
	            //Jasper Export Manager object
	            $lobjexportManager = new Java(
	            					"net.sf.jasperreports.engine.JasperExportManager");
	            
	            //output file path
	            $lstrhtmloutputPath = realpath(".") . "/" . "output.html";
	            echo "Before Export</br>";
	            $session = Zend_Session::getId();
	            $lstrpdfoutputPath = realpath(".") . "/" . "$session.pdf";
	            $objStream = new Java("java.io.ByteArrayOutputStream");
	            $lobjexportManager->exportReportToPdfFile($lobjjasperPrint,$lstrpdfoutputPath);
	            
	            //Export report to HTML	            
	            echo 'HTML Exported</br>';
	
				header("Content-type: application/pdf;charset=utf-8;encoding=utf-8");
				header('Content-Disposition: attachment; filename="Student_Invoice.pdf"');
				
	            readfile($lstrpdfoutputPath);
	            unlink($lstrpdfoutputPath);
				echo "finished";	
		 		 		            
		    } 
		    catch (JavaException $lobjexception) 
		    {
		    	echo 'Exception caught: ', $lobjexception->getMessage() . "\n";
		    }		    		   
			
	}
						
	
	/*public function printreportAction() {

		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		
			$IDInvoice = ( int ) $this->_getParam ('insertedId');
			print_r($IDInvoice);
			die();
	}*/
public function confirmpaymentAction(){
		$lintidstudent = $this->_getParam('insertedId');
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($lintidstudent);	
		/*print_r($larrresult);
		die();*/		
		$this->view->data = $larrresult;
		$this->view->idstudent = $lintidstudent;		
		//$this->Url = "http://".$_SERVER['SERVER_NAME']."/tbe";
		//Get SMTP Mailing Server Setting Details
		$postArray = $this->_request->getPost ();
		
		if($postArray){						
				if($postArray['payment_status'] = 'Completed'){
					//print_r($postArray);
					//die();
					$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					//$postArray['Regid']  = substr($postArray['txn_id'], 1, 6).rand(1000, 9999).substr($postArray['txn_id'], 5, 9);
					$postArray['Regid']  = $this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$lintidstudent);	

					//print_r($postArray);
					if($postArray['Regid']=='')
					{
						//print_r("duplication");
						//die();
					}
					else
					{			
					$this->view->mess = "Payment Completed Sucessfully";
					
					$larrregid = $this->lobjstudentmodel->fngetRegid($lintidstudent);
					
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
						/*print_r($larrresult);
						die();	*/			
								
						require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');		
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['Year'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'],$lstrEmailTemplateBody);
										
										
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										/*
										
										$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								//die();							
					}	
				}
				else {
					$this->view->mess = "Payment Failed";
				}				
			}
			
	}

	//Action To Get List Of States From Country Id
	public function fngetvenueAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidscheduler = $this->_getParam('idscheduler');
		
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetVenueName($lintidscheduler);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrbatchresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	//Action To Get List Of States From Country Id
	public function fngetvenuetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidvenue = $this->_getParam('idvenue');
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetVenueTime($lintidvenue);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}
	
public function fngetbatchAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		
		$idunit = 1;
		$larrinitconfigdetilas = $this->lobjstudentmodel->fnGetInitialConfigDetails($idunit);
		$days = $larrinitconfigdetilas['ClosingBatch'];
		$datetocmp = date('Y-m-d', strtotime($days.'days'));

		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetbatch($lintidprog,$datetocmp);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}
	
	
	
public function fngetschedulerdetailsAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidbatch = $this->_getParam('idbatch');
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetscheduler($lintidbatch);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	
	
	
	public function fngetdatetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('date');
		$lintidvenue = $this->_getParam('idvenue');
		
	
		
		//echo $totalnoofstudents;
		
		/*if($noofseats == $totalnoofstudents)
		{
			echo "<script>alert('Select Other Date')</script>";
			$larrCountryStatesDetails="safsffsfsfsd";
			echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
			die();
		}*/
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetTimingsForDate($lintdate,$lintidvenue);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	/*
	 * functino to check the data and time 
	 */
	public function fngetdatetimevalueAction()
	{
			$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidvenuetime = $this->_getParam('idvenuetime');
		
		$larrresultnoofseats = $this->lobjstudentmodel->fnGetNoOfSeats($lintidvenuetime);
		$noofseats = $larrresultnoofseats['NoofSeats'];
	  // echo $noofseats;
		
		$larrnoofstudents = $this->lobjstudentmodel->fnGetNoofStudents($lintidvenuetime);
		$totalnoofstudents = $larrnoofstudents['count(IDApplication)'];
		// $totalnoofstudents;
		
		if($totalnoofstudents == $noofseats)
		{
			$value = '1';
			$alert =  'This Schedule has been completely filled please select other venue or time';
			$alert =$alert.'*****'.$value;
			echo $alert;
			
		}
		else 
		{
			$value = '0';
			$alert =  'This';
			$alert =$alert.'*****'.$value;
			echo $alert;
		}
		
		
	}
	
	/*
	 * function to find the takaful operator count
	 */
	public function fngettakafulidAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintididtakaful = $this->_getParam('idtakaful');
		$larrresultnoofseats = $this->lobjstudentmodel->fnGetNoOfTakafulOperator($lintididtakaful);
		$takafulseat = $larrresultnoofseats['NumberofSeat'];
		
		$larrresultnoofseatsinstudent = $this->lobjstudentmodel->fnGetNoOfTakafulOperatorForStudent($lintididtakaful);
		$studentoccupied = $larrresultnoofseatsinstudent['count(IDApplication)'];
		
	   if($takafulseat == $studentoccupied)
		{
			$value = '1';
			$alert =  'This Takaful Operator has been completed select other Takaful Operator';
			$alert =$alert.'*****'.$value;
			echo $alert;
			
		}
		else 
		{
			$value = '0';
			$alert =  'This';
			$alert =$alert.'*****'.$value;
			echo $alert;
		}
		
		
		
	}
	/*
	 * funcion for fees structure
	 */
	public function studentapplicationfeesAction()
	{
		$lintidstudent = (int)$this->_getParam('id');
		$larrresult = $this->lobjstudentmodel->fnGetStudentName($lintidstudent); 
		$this->view->studentname = $larrresult[''];
		$this->lobjstudentForm->populate($larrresult);
	}
	
	
	public function fngetprogramamountAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		$larrprogamountresult = $this->lobjstudentmodel->fnGetProgAmount($lintidprog);
		echo $larrprogamountresult['sum(abc.amount)'];	
	}
	
	public function fngetprerequesitionAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		$larrprogid = $this->lobjstudentmodel->fnGetPreRequesition($lintidprog);
		$prerequest = $larrprogid['PreRequesition'];
		if($larrprogid['PreRequesition'] == 0)
		{
			die();
		}
		else 
		{
		  $larrprog = $this->lobjstudentmodel->fnGetPreRequesitionProgDetails($prerequest);
		  
		  echo 'Please bring along a copy of TBE Examination certificate for '.$larrprog['ProgramName'].' which must be certified as a true copy by the designated authorized person as agreed by IBFIM and MTA';
		  die();
		  
		 // die();
		}
		//die();
	}

public function fngetsameexamAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		$lintidicno = $this->_getParam('icno');
		
		$larrprogid = $this->lobjstudentmodel->fnGetsameexam($lintidprog,$lintidicno);
		if($larrprogid)
		{
			 echo 'You already taken this course';
		      die();
		  
		}
		
	}
public function fngetstatenameAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
			$year= $this->_getParam('year');

		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetStatelistforcourse($Program,$year);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}

public function fngetcitynamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('idstate');
		$Program = $this->_getParam('Program');

		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetCitylistforcourse($lintdate,$Program);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}

public function fngetactivesetAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetActiveSet($Program);
		//print_r($larrvenuetimeresults);die();
		echo $larrvenuetimeresults[0]['IdBatch'];
		//$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}


public function fngetyearAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetYearlistforcourse($Program);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}
public function tempdaysAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day = $this->_getParam('day');
		$year = $this->_getParam('year');
		$month = $this->_getParam('month');
		$dateid = $day.''.$month;
		
		
       $larrresultinserted = $this->lobjstudentmodel->fngetdetails();
       
		$id = $larrresultinserted[0]['dateid'];
		
		
		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetTempDays($day,$year,$month,$dateid);
		$lintidstudenttempday = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studenttempday','idstudenttempday');
		
		$larrstudenttempdetials = $this->lobjstudentmodel->fngetstudenttempdays($lintidstudenttempday);
		/* print_R($larrstudenttempdetials);
		 die();*/
		
		
		 $larrdeleteddata= $this->lobjstudentmodel->fnDeleteTempDetails($larrstudenttempdetials[0]['dateid']);
		/*//print_r($larrvenuetimeresults);die(); fnDeleteTempDetails($id)
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);*/
		echo $id;
}










public function caleshowAction()
{
	
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$Program = $this->_getParam('Program');
		$year = $this->_getParam('year');
		$city = $this->_getParam('city');
		
		$larrresultdisable = $this->lobjstudentmodel->fnGetDisabledate($city);
		/*echo "<pre/>";
		print_R($larrresultdisable);
		die();*/
		
		$larrdaysarresult = $this->lobjstudentmodel->fnGetDaysforcourse($Program,$year);
		
		for($i=0;$i<count($larrdaysarresult);$i++)
		{
			$days[$i]=$larrdaysarresult[$i]['Days'];
		}
		//print_R($days);
		//die();
	
		$larrmonthresult = $this->lobjstudentmodel->fnGetMonths($year,$Program);		
		$frommonth = $larrmonthresult[0]['From'];
		$tomonth = $larrmonthresult[0]['To'];
		$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<7;$j++)
		{
			if($days[$j]==1)
			  $monday=1;
			 if($days[$j]==2)
			  $tuesday=1;
			 if($days[$j]==3)
			  $wednesday=1;
			  if($days[$j]==4)
			  $thursday=1;
			  if($days[$j]==5)
			  $friday=1;
			  if($days[$j]==6)
			  $saturday=1;
			  if($days[$j]==7)
			  $sunday=1;
		}
		/*$monday =empty($larrdaysarresult[0]['Days'])? '0':'1';
		$tuesday = empty($larrdaysarresult[1]['Days'])?'0':'1';
		$wednesday = empty($larrdaysarresult[2]['Days'])?'0':'1';
		$thursday = empty($larrdaysarresult[3]['Days'])?'0':'1';
		$friday = empty($larrdaysarresult[4]['Days'])?'0':'1';
		$saturday = empty($larrdaysarresult[5]['Days'])?'0':'1';
		$sunday = empty($larrdaysarresult[6]['Days'])?'0':'1';*/
		
/*		
		
                                  $monday = $this->monday;
                                  $tuesday = $this->tuesday;
                                  $wednesday = $this->wednesday;
                                  $thursday = $this->thursday;
                                  $friday = $this->friday;
                                  $saturday = $this->saturday;
                                   $sunday = $this->sunday;
                                   
                    */        
$monat=date('n');
$jahr=$year;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0 width=700>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{
	$cnt++;
	if($frommonth<=$cnt && $tomonth>=$cnt)
	{
		
	
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" valign=top>';
				echo '<table border=1 align=center style="font-size:8pt; font-family:Verdana">';
				echo '<th colspan=7 align=center style="font-size:12pt; font-family:Arial; color:#666699;">'.$monate[$this_month-1].'</th>';
				echo '<tr><td style="color:#666666"><b>Mo</b></td>';
				echo '<td style="color:#666666"><b>Tu</b></td>';
				echo '<td style="color:#666666"><b>We</b></td>';
				echo '<td style="color:#666666"><b>Th</b></td>';
				echo '<td style="color:#666666"><b>Fr</b></td>';
				echo '<td style="color:#666666"><b>Sa</b></td>';
				echo '<td style="color:#666666"><b>Su</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:8pt; font-family:Verdana; " align=center>';}
				else{echo '<td style="font-size:10pt; font-family:Verdana" align=center>';}
				if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				
				else if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'<span id="'.$i.''.$this_month.'" style="color:#00FF00" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#00FF00" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo '<span id="'.$i.''.$this_month.'" style="color:#00FF00" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo '<span id="'.$i.''.$this_month.'" style="color:#00FF00" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#00FF00" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#00FF00" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#00FF00" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
      ///////
     }
///////
//////else
else 
{
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" valign=top>';
				echo '<table border=1 align=center style="font-size:8pt; font-family:Verdana">';
				echo '<th colspan=7 align=center style="font-size:12pt; font-family:Arial; color:#666699;">'.$monate[$this_month-1].'</th>';
				echo '<tr><td style="color:#666666"><b>Mo</b></td>';
				echo '<td style="color:#666666"><b>Tu</b></td>';
				echo '<td style="color:#666666"><b>We</b></td>';
				echo '<td style="color:#666666"><b>Th</b></td>';
				echo '<td style="color:#666666"><b>Fr</b></td>';
				echo '<td style="color:#666666"><b>Sa</b></td>';
				echo '<td style="color:#666666"><b>So</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:8pt; font-family:Verdana; " align=center>';}
				else{echo '<td style="font-size:10pt; font-family:Verdana" align=center>';}
				if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				
				else if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
				/////end of else
}
}
echo '</tr>';
}
echo '</table>';
}
}