<?php
class App_Model_Examdetails extends Zend_Db_Table {
	
	/*
	 * functin to fetch all easy questions
	 */
	public function fnGetEasyQuestions($idbranch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()				  
						->from(array('a' => 'tbl_tossubdetail'),array('a.NoofQuestions','a.IdDiffcultLevel'))
						->join(array('b' =>'tbl_tosdetail'),'a.IdTOSDetail = b.IdTOSDetail')
						->join(array('c'=>'tbl_tosmaster'),'b.IdTOS = c.IdTOS')
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$idbranch);                                
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
	}

	/*
	 * function to fetch the time limit
	 */
	public function fnGetTime($idIdBatch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_tosmaster"),array("a.TimeLimit","a.AlertTime"))
					 				 ->where("a.IdBatch = ?",$idIdBatch);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	}
	
	public function fnGetSectionsss($idbatch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $select ="SELECT distinct(`b`.`IdSection`) FROM `tbl_tosdetail` AS `b` INNER JOIN `tbl_tosmaster` AS `c` ON b.IdTOS = c.IdTOS INNER JOIN `tbl_batchmaster` AS `d` ON c.IdBatch = d.IdBatch WHERE (d.IdBatch =$idbatch)" ;                         
			return	$result = $lobjDbAdpt->fetchAll($select);	
				
	}
	public function fnGetSections($idbatch)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()				  
						->from(array('d'=>'tbl_batchmaster'),array("d.*"))
						->join(array('c'=>'tbl_batchdetail'),'c.IdBatch = d.IdBatch',array("c.*"))
                        ->where('d.IdBatch = '.$idbatch);                                
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
				
	}
	/*
	 * function for editing using ajax
	 */
	public function fnGetNoofQuestion($IdBatch,$Questionlevel)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()				  
						->from(array('a' => 'tbl_tossubdetail'),array('a.NoofQuestions','a.IdDiffcultLevel'))
						->join(array('b' =>'tbl_tosdetail'),'a.IdTOSDetail = b.IdTOSDetail')
						->join(array('c'=>'tbl_tosmaster'),'b.IdTOS = c.IdTOS')
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$IdBatch)
                        ->where('b.IdSection = "'.$Questionlevel.'"');                           
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
	}
	
	/*
	 * function for getting the random easy questions
	 */
	public function fnGetRandomEasyQuestions($Questionnumber,$easyquestion,$Mediumquestion,$difficultquestion)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 
		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a` WHERE (a.QuestionLevel = 1) AND (a.QuestionNumber = '$Questionnumber') ORDER BY RAND() LIMIT 0,$easyquestion ";
		$result1 = $lobjDbAdpt->fetchAll($select);

		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a` WHERE (a.QuestionLevel = 2) AND (a.QuestionNumber = '$Questionnumber') ORDER BY RAND() LIMIT 0,$Mediumquestion ";
		$result2 = $lobjDbAdpt->fetchAll($select);
		
		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a` WHERE (a.QuestionLevel = 3) AND (a.QuestionNumber = '$Questionnumber') ORDER BY RAND() LIMIT 0,$difficultquestion ";
		$result3 = $lobjDbAdpt->fetchAll($select);
//print_r($result1);die();
		$result = array_merge($result1,$result2,$result3);
		//print_r($result);die();
		//print_r($result['idquestions']);
		return $result;
	}
public function fnGetRandomEasyQuestionss($Questionnumber,$easyquestion,$Mediumquestion,$difficultquestion)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 
		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a`,tbl_questionset as b WHERE (a.QuestionLevel = 1) AND (a.QuestionNumber = '$Questionnumber')AND a.idquestions=b.idquestion  ORDER BY RAND() LIMIT 0,$easyquestion ";
		$result1 = $lobjDbAdpt->fetchAll($select);

		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a`,tbl_questionset as b WHERE (a.QuestionLevel = 2) AND (a.QuestionNumber = '$Questionnumber') AND a.idquestions=b.idquestion  ORDER BY RAND() LIMIT 0,$Mediumquestion ";
		$result2 = $lobjDbAdpt->fetchAll($select);
		
		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a`,tbl_questionset as b WHERE (a.QuestionLevel = 3) AND (a.QuestionNumber = '$Questionnumber') AND a.idquestions=b.idquestion  ORDER BY RAND() LIMIT 0,$difficultquestion ";
		$result3 = $lobjDbAdpt->fetchAll($select);
//print_r($result1);die();
		$result = array_merge($result1,$result2,$result3);
		//print_r($result);
		//print_r($result['idquestions']);
		return $result;
	}
	
	/*
	 * function to fetch all the answer based on the questions
	 */
   public function fnGetAnswers($arrayqstn)
   {
   	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $select = "SELECT answers,idquestion,idanswers,Malay FROM tbl_answers WHERE idquestion IN ($arrayqstn)";
   	 $result = $lobjDbAdpt->fetchAll($select);
   	 return $result;
   }
   public function fngetidbatchdetails($regid)
   
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$select= "SELECT b.IdBatch
   	 			FROM tbl_studentapplication b,tbl_registereddetails a
   	 			WHERE a.IDApplication=b.IDApplication and a.Regid='$regid'";
   	$result = $lobjDbAdpt->fetchAll($select);
   	 return $result;
   	
   }
   /*
    * function to fetch the section name
    */
   public function fnGetSectionName($lintidquestion)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	 $select = "SELECT * FROM tbl_questions WHERE idquestions=$lintidquestion";
   	 $result = $lobjDbAdpt->fetchRow($select);
   	 return $result;
   }
   
   /*
    * inserting into temp table
    */
	public function fnInsertTempDetails($IdBatch,$RegId,$sessionID,$lintidquestion,$lintidanswers,$questionlevel)
	{
		
		 $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_tempexamdetails";
           $postData = array(		
							'Regid' => $RegId,		
            				'IdBatch' =>$IdBatch,		
		            		'Section' =>$questionlevel,	
		            		'Session'	=>$sessionID,
		            		'QuestionNo'=>$lintidquestion,
		            		'Answer'=>$lintidanswers															
						);			
	        $db->insert($table,$postData);
	}
	
	/*$where = "idtemp = '".$idtemp."' AND idsession ='".$sessionID."'"; 	    
	 * fetching details from the table
	 */
 public function fnGetAllDetailsFromTemp($IdBatch,$RegId,$sessionID)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$select = "SELECT QuestionNo 
   	 			FROM tbl_tempexamdetails 
   	 			WHERE Session='$sessionID' AND IdBatch=$IdBatch AND Regid='$RegId'";
   	 $result = $lobjDbAdpt->fetchAll($select);
   	 return $result;
   }
   
   /*
    * function for updating the temp details
    */
   public function fnUpdateTempDetails($IdBatch,$RegId,$sessionID,$lintidquestion,$lintidanswers)
   {
   	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			   	$where = "RegId = '".$RegId."' AND Session ='".$sessionID."' AND IdBatch ='".$IdBatch."'  AND QuestionNo ='".$lintidquestion."'"; 	 
			     $postData = array(		
							'Answer' => $lintidanswers														
						);
				$table = "tbl_tempexamdetails";
	            $lobjDbAdpt->update($table,$postData,$where);
   }
   
   /*
    * function to take no of question from ajax
    */
   public function fnGetNoofQuestioninajax($IdBatch,$questionlevel)
   {
   		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()				  
						//->from(array('a' => 'tbl_tossubdetail'),array('a.NoofQuestions','a.IdDiffcultLevel'))
						->from(array('b' =>'tbl_tosdetail'),array('b.*'))
						->join(array('c'=>'tbl_tosmaster'),'b.IdTOS = c.IdTOS')
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$IdBatch)
                        ->where('b.IdSection = "'.$questionlevel.'"');                           
				$result = $lobjDbAdpt->fetchRow($select);	
				return $result;
   }
   
   /*
    * 
    */
   public function fnGetNoofQuestioninajaxupdated($IdBatch,$questionlevel)
   {
   		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()				  
		                ->from(array('c'=>'tbl_batchdetail'),array("c.*"))
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$IdBatch)
                         ->where('c.IdPart = "'.$questionlevel.'"');                          
				$result = $lobjDbAdpt->fetchRow($select);	
				return $result;
   }
   
   /*
    * function to take from the temporaty table
    */
   public function fnGetQstnfromtemptable($IdBatch,$RegId,$sessionID,$questionlevel)
   {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$select = "SELECT count(QuestionNo) 
   	 			FROM tbl_tempexamdetails 
   	 			WHERE Session='$sessionID' AND IdBatch=$IdBatch AND Regid='$RegId' AND Section='$questionlevel'";
   	 $result = $lobjDbAdpt->fetchRow($select);
   	 return $result;
   }
   
   /*
    * fetchfrom temp table
    */
   public function fnGetFromTempDetail($IdBatch,$RegId,$sessionID)
   {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$select = "SELECT * 
   	 			FROM tbl_tempexamdetails 
   	 			WHERE Session='$sessionID' AND IdBatch=$IdBatch AND Regid='$RegId'";
   	 $resulttempdetails = $lobjDbAdpt->fetchAll($select);
   	  for($linti=0;$linti<count($resulttempdetails);$linti++)
		 {
		 	 $larrcreditnotedetails['Regid'] = $resulttempdetails[$linti]['Regid'];
		 	 $larrcreditnotedetails['QuestionNo'] = $resulttempdetails[$linti]['QuestionNo'];
		 	 $larrcreditnotedetails['Answer'] = $resulttempdetails[$linti]['Answer'];
		 	  $result = $lobjDbAdpt->insert('tbl_answerdetails',$larrcreditnotedetails);
		 	 
		 }
   	 return $result;
   }
   
	/*
 	 * function to delete datas from temp table on close
 	 */
 	public function fnDeleteTempDetails($sessionID)
 	{
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$where = "Session = '".$sessionID."'";
 		$db->delete('tbl_tempexamdetails',$where);
 		
 	}
 	
	/*
	 * function to fetch all the answer based on the questions
	 */
   public function fnGetQtnfromidbatch($IdBatch)
   {
   	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()				  
						//->from(array('a' => 'tbl_tossubdetail'),array('a.NoofQuestions','a.IdDiffcultLevel'))
						->from(array('b' =>'tbl_tosdetail'),array('b.*'))
						->join(array('c'=>'tbl_tosmaster'),'b.IdTOS = c.IdTOS')
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$IdBatch);                     
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
   }
   
   /*
    * function to fetch the name of the studen tname
    */
		public function fnGetStudentName($idstudent) {
			$db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT CONCAT(FName,' ',IFNULL(MName,' '),' ',IFNULL(LName,' ')) AS Name,StudentId,EmailAddress,Examdate  FROM tbl_studentapplication WHERE IDApplication = $idstudent";
    		$result = $db->fetchRow($sql);    
			return $result;
    	}
    	
  public function fnGetStudentdetails($idstudent)
  {
  	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_studentapplication'),array("a.*"))
		            //->joinleft(array('b'=>'tbl_registereddetails'),'a.IdBatch = b.IdBatch')
		            ->where("a.IDApplication = ?",$idstudent);
		    $result = $db->fetchRow($select);
		    return $result;
  }
    	
  public function fnGetIdBatch($IdBatch)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('a' => 'tbl_registereddetails'),array('a.*'))
				->where("a.Approved = 1")
                        ->where('a.Regid = ?',$IdBatch);
	 $result = $db->fetchRow($select);	
		return $result;
    	
    }
    
    public function fnCheckforApproval($examvenue,$idprog,$date)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_centerstartexam'),array("a.*"))
	                ->where("a.Program=?",$idprog)
	                ->where("a.Date=?",$date)
		            ->where("a.idcenter = ?",$examvenue)
		            ->order("a.idcenterstartexam desc");
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    /*
     * function for date check
     */
    public function fnCheckDate($Regid){
    		$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_batchmaster'),array("a.BatchTo"))
		            ->joinleft(array('b'=>'tbl_registereddetails'),'a.IdBatch = b.IdBatch')
		            ->where("b.IdBatch = ?",$Regid);
		    $result = $db->fetchRow($select);
		    return $result;
    	}
    
    	/*
    	 * function for getting register ids
    	 */
    public function fnGetRegisterid()
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_answerdetails'),array('distinct(a.Regid)'));
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
    /*
     * 
     */
    public function fnGetBatchName($Regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		 $select = $db->select()
		            ->from(array('a'=>'tbl_batchmaster'),array('a.*'))
		            ->join(array('b'=>'tbl_batchdetail'),'a.IdBatch = b.IdBatch',array("b.*"))
		            ->join(array('c'=>'tbl_tosmaster'),'a.IdBatch = c.IdBatch')
 		           ->join(array('d'=>'tbl_programmaster'),'d.IdProgrammaster=a.IdProgrammaster')
		            ->where("b.BatchDtlStatus =0")
		             ->where("a.IdBatch = ?",$Regid);
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
    /*SELECT count(`QuestionNo`) FROM `tbl_tempexamdetails` WHERE `Regid`=555
     * 
     * 
     */
    public function fnGetNoofQtnsFromTempTable($Regid)
    {
    	    $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT count(`QuestionNo`) FROM `tbl_tempexamdetails` WHERE `Regid`='$Regid'";
    		$result = $db->fetchRow($sql);    
			return $result;
    }
    
    public function fnDeleteIdCenterDeatails($idcenter)
    {
    	$db 	= Zend_Db_Table::getDefaultAdapter();
 		$where = "idcenter = '".$idcenter."'";
 		$db->delete('tbl_centerstartexam',$where);
    	
    }
    
    public function fnExampercentage($idbatch)
    {
    	   $db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->join(array('c'=>'tbl_tosmaster'),array("c.*"))
		             ->where("c.IdBatch = ?",$idbatch);
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fnAttended($Regid)
    {
    	
    	    $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT * FROM tbl_answerdetails  where Regid = '$Regid'  and answer in(SELECT idanswers FROM tbl_answers where CorrectAnswer =1)";
    		$result = $db->fetchAll($sql);    
			return $result;
    }
    
    public function fnGetEachSection($idbatch)
    { 
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->join(array('c'=>'tbl_tosdetail'),array("c.*"))
		             ->where("c.idTOS = ?",$idbatch);
		    $result = $db->fetchAll($select);
		    return $result;
    	
    }
    
    public function fnGetSectionWise($idsection)
    {
    	 $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT * FROM tbl_answerdetails  where answer in(SELECT idanswers from tbl_answers where idquestion in (Select idquestions from tbl_questions where QuestionNumber='$idsection') and CorrectAnswer=1)";
    		$result = $db->fetchAll($sql);    
			return $result;
    }
    
    public function fngetInitconfigDetails($iduniversity)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->join(array('a'=>'tbl_config'),array("a.*"))
		             ->where("a.idUniversity =1");
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fnGetStudentDetailsPassfail($regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_registereddetails'),array("a.*"))
		            ->join(array('b'=>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array("b.*"))
		              ->join(array("c" =>"tbl_newscheduler"),'b.Year=c.idnewscheduler',array('c.Year as years'))
		            ->where("a.Regid = ?",$regid);
		    $result = $db->fetchRow($select);
		    return $result;
    }
    public function totalattendedquestions($regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_answerdetails'),array("a.*"))
		            //->join(array('b'=>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array("b.*"))
		            ->where("a.Regid  ='$regid'");
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
    public function fnnoofqtns($idbatch)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_tosmaster'),array("a.*"))
		            //->join(array('b'=>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array("b.*"))
		            ->where("a.IdBatch  ='$idbatch'");
		    $result = $db->fetchAll($select);
		    return $result;
    	
    }
    
    public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}

    public function fnGetStudentDetailsPassfailforbatchcandidates($regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_registereddetails'),array("a.*"))
		            ->join(array('b'=>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array("b.*"))
		            //->join(array)
		            ->join(array("c"=>'tbl_center'),'b.Examvenue=c.idcenter',array("c.*"))
		            ->join(array("e" =>"tbl_programmaster"),'b.Program=e.IdProgrammaster',array('e.*'))
					->join(array("f" =>"tbl_newscheduler"),'b.Year=f.idnewscheduler',array('f.Year as years'))
		            ->where("a.Regid = ?",$regid);
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
   public function fnupdatepass($idstudent,$result)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Pass'] = $result;	
		 $where = "IDApplication = '".$idstudent."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
    }
    
    public function fngetstudentid($Regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_registereddetails'),array("a.*"))
		            ->where("a.Regid = '$Regid'");
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fnGetQuestions($idbatch,$group)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            
		            ->from(array('b'=>'tbl_questions'),array("b.*"))
		            ->join(array('a'=>'tbl_questionset'),'a.idquestion=b.idquestions',array("a.*"))
		           // ->join(array("c"=>'tbl_answers'),'c.idquestion=b.idquestions',array("c.*"))
		            ->where("a.idbatch = ?",$idbatch)
		            ->where("a.editbatch=1")
		            ->where("b.QuestionGroup=?",$group)
				 ->order('RAND()');
		    $result = $db->fetchAll($select);
		    return $result;
    }
 public function fnGetQuestionsmultiple($idbatch,$group)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	 $select = $db->select()
		               ->from(array('b'=>'tbl_questions'),array("b.*"))
		             //  ->join(array('a'=>'tbl_questionset'),'b.idquestions=a.idquestion',array("a.*"))
		           // ->join(array("c"=>'tbl_answers'),'c.idquestion=b.idquestions',array("c.*"))
		            ->where("b.idquestions in (select idquestion from tbl_questionset where  idbatch = $idbatch  and editbatch=1)")
		            ->where("b.QuestionGroup=?",$group);
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
}
