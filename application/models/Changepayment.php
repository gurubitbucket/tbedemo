<?php
class App_Model_Changepayment extends Zend_Db_Table {
	//protected $_name = 'tbl_batchmaster'; // table name
public  function fngettakafulname($idtakaful)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_takafuloperator"),array("a.*" ))
					 				->where("a.idtakafuloperator= ?",$idtakaful);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
	
	
public function fngetuppaidregpins($idtakaful,$modeofpayment)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_batchregistration"),array("a.*"))
									->join(array("b"=>"tbl_studentpaymentoption"),'a.idBatchRegistration = b.IDApplication',array("b.*"))
									->where("b.companyflag = 2")
									->where("a.paymentStatus =0")
									->where("a.idCompany = $idtakaful")
									->where("b.ModeofPayment =$modeofpayment");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;	
}


public function fngetpindetails($idbatchregistration)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
									->from(array("a"=>"tbl_batchregistration"),array("a.*"))
									->join(array("b"=>"tbl_studentpaymentoption"),'a.idBatchRegistration = b.IDApplication',array("b.*"))
									->join(array("c"=>"tbl_takafuloperator"),'c.idtakafuloperator = a.idCompany',array("c.TakafulName"))
									->where("a.idBatchRegistration =$idbatchregistration")
									->where("b.companyflag=2");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;	
}

public function fnupdatechequepayment($idbatchregistration,$modeofpayment)
{
	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['ModeofPayment'] = $modeofpayment;	
		 $where = "idstudentpaymentoption = '".$idbatchregistration."'"; 	
		 $db->update('tbl_studentpaymentoption',$larrformData1,$where);
	 
}

public function fnpreviouspayment($larresultdetails,$newpayment,$idtakaful)
{
	/*echo "<pre>";
	print_R($larresultdetails);
	die();*/
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_takafulpreviouspaymentdetail";
		$postData = array(		
							'idbatchregistration' =>$larresultdetails['idBatchRegistration'],	
           					'OldModeofPayment' =>$larresultdetails['ModeofPayment'],	
           					'OldUpdDate' => $larresultdetails['UpdDate'], 
							'OldUpdUser' =>$larresultdetails['UpdUser'],	
           					'NewUpdUser' =>$idtakaful,	
           					'NewUpdDate' =>date('Y-m-d H:i:s'),
							'NewModeofPayment'=>$newpayment
			);
						  $lobjDbAdpt->insert($table,$postData);
}

public function fngetpaylater($idpaylater)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_takafulpaymenttype"),array("a.*" ))
       								->where("a.paymenttype=181")
					 				->where("a.idtakafuloperator = ?",$idpaylater);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
}

public function fnupdatepaylater($idbatchregistration,$modeofpayment,$randomnumber)
{
	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['registrationPin']=$randomnumber;
    	$larrformData1['paymentStatus']=1;
    	$larrformData1['Approved']=1;			
		 $where = "idBatchRegistration = '".$idbatchregistration."'"; 	
		 $db->update('tbl_batchregistration',$larrformData1,$where);
}

public function fngetAllanswers()
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_answers"),array("a.*" ));					  	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
}
}
