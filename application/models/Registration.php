<?php
class App_Model_Registration extends Zend_Db_Table {
	//protected $_name = 'tbl_batchmaster'; // table name
	public  function fnGetparts()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_questions"),array("key"=>"a.QuestionGroup","value"=>"a.QuestionGroup") )
					 				->group("a.QuestionGroup");					  	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		
	}
	public function fnAddBatch($post)
	{
		Unset($post['IdPart']);
		Unset($post['Save']);
			$db = Zend_Db_Table::getDefaultAdapter();
	    	$table = "tbl_batchmaster";
			$msg = $db->insert($table,$post);
			return $msg;
	}
	public function fnAddBatchdetails($post,$lintIdBatchMaster)
	{
	     
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_batchdetail";
           

     	foreach ($post['IdPart'] as $larrdata):
     	    $post['IdBatch']  = $lintIdBatchMaster;
     	    $post['IdPart']  = $larrdata;
     	    $post['BatchDtlStatus']=0;
     	    /*$post['BatchDtlStatus']  = 0;
     	    $post['UpdDate']  = $post['UpdDate'];
     	    $larrdata['UpdUser']  = $post['UpdUser'];*/
	        $db->insert($table,$post);
	    endforeach;
	}
    	
	public function fnGetBatchDetails()
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
			->from(array('a'=>'tbl_batchdetail'),array('a.IdPart'))	
			->join(array('b' => 'tbl_batchmaster'),'a.IdBatch=b.IdBatch',array('b.*'))			
			->where("b.BatchStatus = 0");
		$result = $db->fetchAll($select);
		
		return $result;   
	}
public function fnSearchbatch($post = array()) {
		    $db = Zend_Db_Table::getDefaultAdapter();
		    $select = $db->select()
			->from(array('a'=>'tbl_batchdetail'),array('a.IdPart'))	
			->join(array('b' => 'tbl_batchmaster'),'a.IdBatch=b.IdBatch',array('b.*'))	
			->where('b.BatchName like  ? "%"',$post['field3']);
		$result = $db->fetchAll($select);
		
		return $result;
	}
		
    public function fnViewBatch($IdBatch) {
    	
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('a' => 'tbl_batchmaster'),array('a.*'))
                        ->where('IdBatch = ?',$IdBatch);
	 $result = $db->fetchRow($select);	
		return $result;
    }
    public function fnGetIdBatch($IdBatch)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('a' => 'tbl_registereddetails'),array('IdBatch'))
                        ->where('Regid = ?',$IdBatch);
	 $result = $db->fetchRow($select);	
		return $result;
    	
    }
    
 public function fnUpdateBatch($larrformData,$lintidbank)
	 {
	 	 $db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$data = array('BatchName' => $larrformData['BatchName'],
		              'BatchStatus'=>$larrformData['BatchStatus'],
		              'BatchFrom'=> $larrformData['BatchFrom'],
		              'BatchTo'=>$larrformData['BatchTo'] );
		$where['IdBatch = ? ']= $lintidbank;		
		return $db->update('tbl_batchmaster', $data, $where);	
			    
	 }
	 public function fnDeleteparts($lintidbank){
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			
    		$where = $lobjDbAdpt->quoteInto('IdBatch = ?',$lintidbank);
    		$Table = "tbl_batchdetail";
			$lobjDbAdpt->delete($Table, $where);
	 }
	 

}
