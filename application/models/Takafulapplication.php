<?php
class App_Model_Takafulapplication extends Zend_Db_Table { //Model Class for Users Details
	

	protected $_name = 'tbl_studentapplication';
	
	 /*
      * function to fetch all the details
      */
	
	public function fngetpaylater($idcomp) {
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_takafulpaymenttype" ), array ("a.idtakafulpaymenttype" ) )
											->join ( array ("b" => "tbl_definationms" ), 'a.paymenttype=b.idDefinition', array ("b.idDefinition", "b.DefinitionDesc" ) )
											->where ( "a.idtakafuloperator=?", $idcomp )
											->where ( "a.paymenttype= 181" );
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
	}
	
	
	public function fnGetexamcenter($larrformData) {
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( "tbl_venuedateschedule", array ("sum(Totalcapacity-Allotedseats) as availseat", "idvenue", "date" ) )
											->join ( array ("tbl_center" ), 'idvenue = idcenter', array ("centername" ) )
											->where("tbl_venuedateschedule.Active = 1")
											->where("tbl_venuedateschedule.centeractive=1")
										 	->where("tbl_venuedateschedule.Reserveflag=1");
		
		if (isset ( $larrformData ['FromDate'] ) && ! empty ( $larrformData ['FromDate'] ) && isset ( $larrformData ['ToDate'] ) && ! empty ( $larrformData ['ToDate'] )) {
			$lstrFromyear = $larrformData ['FromDate'];
			$lstrToYear = $larrformData ['ToDate'];
			$lstrSelect = $lstrSelect->where ( "date BETWEEN '$lstrFromyear' and '$lstrToYear'" );
		}
		
		$lstrSelect = $lstrSelect->group ( "idvenue" )
								 ->order ( "centername" );
		//echo $lstrSelect;
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
	}
	
	public function fnGetexamcenterfull($idvenue, $fromdate, $todate) {
		$fromdate = date ( 'Y-m-d', strtotime ( $fromdate ) );
		$todate = date ( 'Y-m-d', strtotime ( $todate ) );
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( "tbl_venuedateschedule", array ("(Totalcapacity-Allotedseats) as availseat", "idvenue", "date", "idsession" ) )->join ( array ("tbl_center" ), 'idvenue = idcenter', array ("centername" ) )->join ( array ("tbl_managesession" ), 'idsession = idmangesession', array ("managesessionname" ) )->where ( "idvenue=?", $idvenue )->where ( "date BETWEEN '$fromdate' and '$todate'" )->group ( "date" )->group ( "idsession" );
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
	
	}
	
	public function fngetbatchpin($idtakaful){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ("tbl_batchregistration", array ("key" => "idBatchRegistration", "value" => "registrationPin") )
								 			->join("tbl_studentpaymentoption",'idBatchRegistration=IDApplication AND companyflag=2')
								 			->where("idCompany =?",$idtakaful);
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
		
	}
	
	public function fngetbatchpinCompany($idCompany){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ("tbl_batchregistration", array ("key" => "idBatchRegistration", "value" => "registrationPin") )
								 			->join("tbl_studentpaymentoption",'idBatchRegistration=IDApplication AND companyflag=1')
								 			->where("idCompany =?",$idCompany);
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
		
	}
	
	public function fninsertintostudentapplicationonpopup($larrinsertdata,$idbatchreg,$regpin,$operatorId,$batchpayment){
		
		$db = Zend_Db_Table::getDefaultAdapter ();			
		$studcount=count($larrinsertdata['stuapp']);
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //TAkafulmodels model object				
		for($countstud=0;$countstud<$studcount;$countstud++){			
		$lstrSelect = $db->select ()->from ( array ("tbl_studentapplication" ), array ("StudentId","username","password","FName","MName","LName","DateOfBirth","PermCity","EmailAddress","Program","ICNO","PermAddressDetails","Takafuloperator","ArmyNo","Gender","Race","Qualification","State","CorrAddress","PostalCode","ContactNo","MobileNo","Religion","DateTime","batchpayment") )
									    ->where ( "IDApplication = ?", $larrinsertdata['stuapp'][$countstud]);
		$larrResult = $db->fetchRow ( $lstrSelect );
					
		$randarray=$this->lobjTakafulcandidatesmodel->fngeneraterandompassword();	
		
		$table = "tbl_studentapplication";;
		$postData = array ('StudentId' => $larrResult['StudentId'], 
						   'username' => $larrResult['username'], 
						   'password' => $randarray,       				                           
                           'UpdDate' =>date('Y-m-d H:i:s'),	
            			   'UpdUser' =>$larrResult['Takafuloperator'],		
						   'FName' => $larrResult['FName'], 
						   'MName' => $larrResult['MName'],           				                           
                           'LName' =>$larrResult['LName'],	
            			   'DateOfBirth' =>$larrResult['DateOfBirth'],
						   'PermCity' => 0, 
						   'EmailAddress' => $larrResult['EmailAddress'],           				                           
                           'Program' =>0,	
            			   'ICNO' =>$larrResult['ICNO'],
            			   'PermAddressDetails' => $larrResult['PermAddressDetails'], 
						   'Takafuloperator' => $larrResult['Takafuloperator'],           				                           
                           'ArmyNo' =>$larrResult['ArmyNo'],	
            			   'Gender' =>$larrResult['Gender'],
            			   'Race' => $larrResult['Race'], 
						   'Qualification' => $larrResult['Qualification'],           				                           
                           'State' =>$larrResult['State'],	
            			   'CorrAddress' =>$larrResult['CorrAddress'],
            			   'PostalCode' =>$larrResult['PostalCode'], 
						   'ContactNo' =>$larrResult['ContactNo'],           				                           
                           'MobileNo' =>$larrResult['MobileNo'],	
            			   'Religion' =>$larrResult['Religion'],	
						   'DateTime'=>0,						   
						   'IdBatch' =>0,
						   'Venue' =>0,
						   'VenueTime' =>0,
						   'Program' =>0,
						   'idschedulermaster' =>0, 
						   'Amount' =>0,
						   'Payment' =>1,
						   'VenueChange' =>0,
						   'batchpayment'=>$batchpayment,
						   'ExamState'=>0,
						   'Year'=>0,
						   'Examdate'=>0,
						   'Exammonth'=>0,
						   'ExamCity'=>0,
						   'Examvenue'=>000,
						   'Examsession'=>0,
						   'pass'=>5
						);
			$db->insert ( $table, $postData );
			$lastids = $db->lastInsertId ( "tbl_studentapplication", "IDApplication" );
		
		    //$larrresult = self::fnviewstudentdetails($lastids);
	        //$this->lobjTakafulcandidatesmodel->fnupdatestudentcount($larrResult['ICNO'],$lastids); removed on 3-5-2013
	        
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($operatorId,$lastids);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
								'Regid' =>$regid,		
	            				'IdBatch' =>0,
	            				'Approved' =>0,
	            				'Cetreapproval'=>0,		
			            		'IDApplication' =>$lastids,
	            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);	      
	     /*
	       $takafulmodel = new App_Model_Takafulcandidates();
	       $takafulmodel->sendmails($larrresult,$regid);
	       */
		}
			
	}
	
	public function fnGetStudregistrered($idbatch) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( "tbl_tempexcelcandidates", array ("idcandidates" ) )->where ( "RegistrationPin=?", $idbatch );
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
	}
	
	public function fnGetfailedregistrered($idregpin){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ("tbl_registereddetails", array ("idregistereddetails","IDApplication") )
								 			->where("RegistrationPin=?", $idregpin )
								 			->where("IdBatch = 0")
								 			;
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
		
	}
	
	public function fngetTakafulOperator($IdTakaful) { //Function to get the user details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a"=>"tbl_takafuloperator" ), array ("a.*","a.TakafulName as CompanyName" ) )->where ( "idtakafuloperator=?", $IdTakaful );
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
	}
	
	public function fngetPaymentDetails($idPayment) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_batchregistrationdetails" ), array ("a.*" ) )->joinLeft ( array ("b" => "tbl_batchregistration" ), 'a.idBatchRegistration=b.idBatchRegistration', array ("b.*" ) )->joinLeft ( array ("c" => "tbl_programmaster" ), 'a.idProgram=c.IdProgrammaster ', array ("c.ProgramName" ) )->where ( "b.idBatchRegistration  = ?", $idPayment );
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
	}
	
	public function fnGetCaptionName() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("tbl_config" ), array ("CourseAliasName" ) )->where ( "idConfig=1" );
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		
		return $larrResult;
	}
	
	public function fngetintialdiscount() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("tbl_config" ), array ("discount" ) )->where ( "idConfig= 1" );
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
	}
	
	public function fnGetProgramNamess() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_programmaster" ), array ("key" => "a.IdProgrammaster", "value" => "a.Description" ) )
											->join ( array ("c" => "tbl_batchmaster" ), 'a.IdProgrammaster=c.IdProgrammaster' )
											->join ( array ("b" => "tbl_tosmaster" ), 'b.IdBatch=c.IdBatch' )
	 ->where("a.PreRequesition=0")
											->where ( "a.Active  = ?", "1" );
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
	}
	

	public function fnGetProgramName() {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_programmaster" ), array ("key" => "a.IdProgrammaster", "value" => "a.Description" ) )
											->join ( array ("c" => "tbl_batchmaster" ), 'a.IdProgrammaster=c.IdProgrammaster' )
											->join ( array ("b" => "tbl_tosmaster" ), 'b.IdBatch=c.IdBatch' )
	 
											->where ( "a.Active  = ?", "1" );
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
	}
	
	
	
	public function fnGetProgramNamereentry($idprogram){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_programmaster" ), array ("key" => "a.IdProgrammaster", "value" => "a.ProgramName" ) )
											->join ( array ("c" => "tbl_batchmaster" ), 'a.IdProgrammaster=c.IdProgrammaster' )
											->join ( array ("b" => "tbl_tosmaster" ), 'b.IdBatch=c.IdBatch' )
											->where ( "a.Active  = ?", "1" )
											->where("a.IdProgrammaster !=?",$idprogram);
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
		
	}
	
	public function fngetRegistratinpin($IdCompany) { //Function to get the user details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_batchregistration" ), array ("key" => "a.registrationPin", "value" => "a.registrationPin" ) )
											->where ( "a.idBatchRegistration in (select IDApplication from tbl_studentpaymentoption where companyflag=2)" )
											->where ( "a.paymentStatus=1" )
											->where ( "a.Approved=1" )
											->where ( "a.AdhocDate=0000-00-00" )
											->where ( "a.IdCompany=?", $IdCompany );
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
	}
	
	public function fnInsertPaymentdetails($larrformData) {
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$table = "tbl_batchregistration";
                $amount=$larrformData ['noOfCand']*$larrformData['eachAmounts'];
		$postData = array ('idCompany' => $larrformData ['idTakaful'],
						   'totalNoofCandidates' => $larrformData ['totalNoofCand'],
						   'totalAmount' => $amount,
		    			   'UpdDate' => $larrformData ['UpdDate'], 
		    			   'UpdUser' => $larrformData ['UpdUser'], 
		    			   'AdhocDate' => '0000-00-00', 
		    			   'AdhocVenue' => 'others', 
		    			   'Approved' => 0,
							'ServiceTax'=>$larrformData['Servicetax']
							);
		
		$db->insert ( $table, $postData );
		$lastid = $db->lastInsertId ( "tbl_batchregistration", "idBatchRegistration" );
		$table = "tbl_batchregistrationdetails";
		for($i = 0; $i < count ( $larrformData ['idProgram'] ); $i ++) {
			$postData = array ('idBatchRegistration' => $lastid, 
							   'idProgram' => $larrformData ['idProgram'] [$i],
							   'eachAmount' => $larrformData ['eachAmount'] [$i],
							   'noofCandidates' => $larrformData ['noofCandidates'] [$i],
							   'toatlAmount' => $larrformData ['toatlAmount'] [$i], 
							   'registrationPin' => 0, 
							   'discount' => $larrformData ['discountamount'] [$i], 
							   'UpdDate' => $larrformData ['UpdDate'], 
							   'UpdUser' => $larrformData ['UpdUser'] );
			
			$db->insert ( $table, $postData );
		}
		return $lastid;
	
	}
	
	public function fnInsertStudentPaymentdetails($mod, $lastInsId) {
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$table = "tbl_studentpaymentoption";
		$postData = array ('IDApplication' => $lastInsId, 
						   'ModeofPayment' => $mod, 
						   'companyflag' => 2,          
						   );
		$db->insert ( $table, $postData );
		$lastids = $db->lastInsertId ( "tbl_studentpaymentoption", "idstudentpaymentoption" );
		return $lastids;
	}
	
	public function fngetmodeofpayment($mod) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $db->select ()->from ( array ("a" => "tbl_studentpaymentoption" ), array ("a.ModeofPayment" ) )->where ( "a.idstudentpaymentoption = ?", $mod );
		$larrResult = $db->fetchRow ( $lstrSelect );
		return $larrResult;
	}
	
	public function fnInsertPaymentdetailsadhoc($larrformData) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$table = "tbl_batchregistration";
		$postData = array ('idCompany' => $larrformData ['idTakaful'], 'totalNoofCandidates' => $larrformData ['totalNoofCand'], 'totalAmount' => $larrformData ['grossAmt'], 'UpdDate' => $larrformData ['UpdDate'], 'UpdUser' => $larrformData ['UpdUser'], 'AdhocDate' => $larrformData ['AdhocDate'], 'AdhocVenue' => $larrformData ['AdhocVenue'], 'Approved' => 0 );
		
		$db->insert ( $table, $postData );
		$lastid = $db->lastInsertId ( "tbl_batchregistration", "idBatchRegistration" );
		$table = "tbl_batchregistrationdetails";
		for($i = 0; $i < count ( $larrformData ['idProgram'] ); $i ++) {
			$postData = array ('idBatchRegistration' => $lastid, 'idProgram' => $larrformData ['idProgram'] [$i], 'eachAmount' => $larrformData ['eachAmount'] [$i], 'noofCandidates' => $larrformData ['noofCandidates'] [$i], 'toatlAmount' => $larrformData ['toatlAmount'] [$i], 'registrationPin' => 0, 'UpdDate' => $larrformData ['UpdDate'], 'UpdUser' => $larrformData ['UpdUser'] );
			$db->insert ( $table, $postData );
		}
		return $lastid;
		
	
	}
	
	
	public function fninsertintostudentapplication($larrinsertdata,$idbatchreg,$regpin,$operatorId,$batchpayment){
		
		$db = Zend_Db_Table::getDefaultAdapter ();			
		$studcount=count($larrinsertdata['studentid']);
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //TAkafulmodels model object				
		for($countstud=0;$countstud<$studcount;$countstud++){			
		$lstrSelect = $db->select ()->from ( array ("tbl_studentapplication" ), array ("StudentId","username","password","FName","MName","LName","DateOfBirth","PermCity","EmailAddress","Program","ICNO","PermAddressDetails","Takafuloperator","ArmyNo","Gender","Race","Qualification","State","CorrAddress","PostalCode","ContactNo","MobileNo","Religion","DateTime","batchpayment") )
									    ->where ( "IDApplication = ?", $larrinsertdata['studentid'][$countstud]);
		$larrResult = $db->fetchRow ( $lstrSelect );
					
		$randarray=$this->lobjTakafulcandidatesmodel->fngeneraterandompassword();	
		
		$table = "tbl_studentapplication";;
		$postData = array ('StudentId' => $larrResult['StudentId'], 
						   'username' => $larrResult['username'], 
						   'password' => $randarray,       				                           
                           'UpdDate' =>date('Y-m-d H:i:s'),	
            			   'UpdUser' =>$larrResult['Takafuloperator'],		
						   'FName' => $larrResult['FName'], 
						   'MName' => $larrResult['MName'],           				                           
                           'LName' =>$larrResult['LName'],	
            			   'DateOfBirth' =>$larrResult['DateOfBirth'],
						   'PermCity' => 0, 
						   'EmailAddress' => $larrResult['EmailAddress'],           				                           
                           'Program' =>0,	
            			   'ICNO' =>$larrResult['ICNO'],
            			   'PermAddressDetails' => $larrResult['PermAddressDetails'], 
						   'Takafuloperator' => $larrResult['Takafuloperator'],           				                           
                           'ArmyNo' =>$larrResult['ArmyNo'],	
            			   'Gender' =>$larrResult['Gender'],
            			   'Race' => $larrResult['Race'], 
						   'Qualification' => $larrResult['Qualification'],           				                           
                           'State' =>$larrResult['State'],	
            			   'CorrAddress' =>$larrResult['CorrAddress'],
            			   'PostalCode' =>$larrResult['PostalCode'], 
						   'ContactNo' =>$larrResult['ContactNo'],           				                           
                           'MobileNo' =>$larrResult['MobileNo'],	
            			   'Religion' =>$larrResult['Religion'],	
						   'DateTime'=>0,						   
						   'IdBatch' =>0,
						   'Venue' =>0,
						   'VenueTime' =>0,
						   'Program' =>0,
						   'idschedulermaster' =>0, 
						   'Amount' =>0,
						   'Payment' =>1,
						   'VenueChange' =>0,
						   'batchpayment'=>$batchpayment,
						   'ExamState'=>0,
						   'Year'=>0,
						   'Examdate'=>0,
						   'Exammonth'=>0,
						   'ExamCity'=>0,
						   'Examvenue'=>000,
						   'Examsession'=>0,
						   'pass'=>5
						);
			$db->insert ( $table, $postData );
			$lastids = $db->lastInsertId ( "tbl_studentapplication", "IDApplication" );
		
		    //$larrresult = self::fnviewstudentdetails($lastids);
	        //$this->lobjTakafulcandidatesmodel->fnupdatestudentcount($larrResult['ICNO'],$lastids); removed as on 3-5-2013
	        
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($operatorId,$lastids);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
								'Regid' =>$regid,		
	            				'IdBatch' =>0,
	            				'Approved' =>0,
	            				'Cetreapproval'=>0,		
			            		'IDApplication' =>$lastids,
	            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);	      

	     /*
	       $takafulmodel = new App_Model_Takafulcandidates();
	       $takafulmodel->sendmails($larrresult,$regid);
	       */
		}
			
	}
	
	
	public function fnviewstudentdetails($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
									 ->from(array("tbl_studentapplication"),array('FName','MName','LName','EmailAddress','ICNO','PermAddressDetails','CorrAddress','Examdate','Exammonth','Year'))
									 ->join(array("tbl_programmaster"),'Program=IdProgrammaster',array('ProgramName'))
									 ->join(array("tbl_center"),'Examvenue=idcenter',array('centername'))
									 ->join(array("tbl_managesession"),'Examsession=idmangesession',array('managesessionname'))										  
									 ->where("IDApplication  = ?",$lintidstudent);	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
     }
	
	public function fnGetProgramFee($idprog) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		/*$lstrSelect = "SELECT sum(abc.amount) from 

(SELECT max(a.EffectiveDate),a.Rate as amount  from tbl_programrate a,tbl_accountmaster b
where a.idProgram=$idprog and 
 b.idAccount= a.IdAccountmaster and  
 b.Active=1 and a.Active=1  and b.idAccount not in (select t.idAccount from tbl_accountmaster t where t.duringRegistration=1 ) group by a.IdAccountmaster) as abc ";
	
		*/
		/*$lstrSelect =  "SELECT sum(abc.amount) from 
					   (select Rate as amount from tbl_programrate where `EffectiveDate` in 
					   (SELECT max(a.EffectiveDate)  from tbl_programrate a,tbl_accountmaster b
						where a.idProgram= $idprog and b.idAccount= a.IdAccountmaster and  b.Active=1 and a.Active=1 and b.idAccount 
						not in (select t.idAccount from tbl_accountmaster t where t.duringRegistration=1 ) 
						group by a.IdAccountmaster)and idProgram=$idprog and Active=1   and IdAccountmaster 
						not in (select r.idAccount from tbl_accountmaster r where r.duringRegistration=1 ))as abc  ";
		
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;*/
		$lstrSelect = "select a.Rate,a.ServiceTax from tbl_programrate as a
			               join tbl_accountmaster as b on b.idAccount= a.IdAccountmaster
						   where a.idProgram=$idprog and a.Active=1 and b.idAccount =1";
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	/* function fnGetProgramFee($idProgram){
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	//Program Fee
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programrate"),array("a.*","max(a.EffectiveDate)"))								
								 ->where("a.idProgram=?",$idProgram)
								 ->group("a.IdAccountmaster");									 
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);		
		$sumProgram = 0;
		for($j=0;$j<count($larrResult);$j++){
			$sumProgram = $sumProgram+$larrResult[$j]['Rate'];
		}
		//Course Fee
		$sql = "SELECT sum(Rate) tot FROM `tbl_courserate`  WHERE `IdAccountmaster` IN(						
						SELECT w.IdAccountmaster  FROM(SELECT `a`.*, max(a.EffectiveDate) FROM `tbl_programrate` AS `a` WHERE (a.idProgram=$idProgram) 
						GROUP BY `a`.`IdAccountmaster`)w
						)";
		$larrsumCourse = $lobjDbAdpt->fetchRow($sql);
		//Discount
		$sql = "SELECT SUM(Amount) dis FROM tbl_accounthead WHERE idAccountHead IN (  SELECT max(ah.idAccountHead) idAcc FROM `tbl_accounthead` ah  WHERE `idAccount` IN(
								SELECT w.IdAccountmaster  FROM(SELECT `a`.*, max(a.EffectiveDate) FROM `tbl_programrate` AS `a` WHERE (a.idProgram=$idProgram) 						
								GROUP BY `a`.`IdAccountmaster`)w)
								GROUP BY ah.idAccount)";
		$larrsumDisc = $lobjDbAdpt->fetchRow($sql); 
		
		return $sumProgram+$larrsumCourse['tot']-$larrsumDisc['dis'];		
    } */
	
	/*
   * function to get the registration pin details
   */
	public function fngetBatchDetails($Idreg) { //Function to get the user details 		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_batchregistration" ), array ("a.*" ) )
											->where ( "a.registrationPin=?", $Idreg );
											//->where ( "a.paymentStatus= 1" );
									
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
	}
	
	public function fngetfailedregstered($idregpin){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from(array("tbl_registereddetails"=>"tbl_registereddetails" ), array("tbl_registereddetails.Regid"))
											->join(array("tbl_studentapplication"=>"tbl_studentapplication"),'tbl_registereddetails.IDApplication=tbl_studentapplication.IDApplication',array("tbl_studentapplication.IDApplication","tbl_studentapplication.FName","tbl_studentapplication.ICNO","tbl_studentapplication.EmailAddress","tbl_studentapplication.ContactNo","tbl_studentapplication.MobileNo"))
											->join(array("tbl_batchregistration"=>"tbl_batchregistration"),'tbl_registereddetails.RegistrationPin=tbl_batchregistration.registrationPin',array())
											->join(array("tbl_batchregistrationdetails"=>"tbl_batchregistrationdetails"),'tbl_batchregistration.idBatchRegistration=tbl_batchregistrationdetails.idBatchRegistration',array("tbl_batchregistrationdetails.idProgram"))
											->where( "tbl_registereddetails.RegistrationPin=?", $idregpin )
											//->where("tbl_studentapplication.pass = 3 or tbl_studentapplication.pass = 5")
											->where("tbl_studentapplication.pass = 5")
											;
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
		
	}
	
	
	public function fngetreappliedcandidates($idregpin){
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from(array ("tbl_registereddetails"=>"tbl_registereddetails" ), array("tbl_registereddetails.Regid"))
											->join(array("tbl_studentapplication"=>"tbl_studentapplication"),'tbl_registereddetails.IDApplication=tbl_studentapplication.IDApplication',array("tbl_studentapplication.IDApplication","tbl_studentapplication.FName","tbl_studentapplication.ICNO","tbl_studentapplication.EmailAddress","tbl_studentapplication.ContactNo","tbl_studentapplication.MobileNo","tbl_studentapplication.DateTime"))
											->join(array("tbl_programmaster"=>"tbl_programmaster"),'tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster',array("tbl_programmaster.ProgramName")) 
							 				->join(array("tbl_managesession"=>"tbl_managesession"),'tbl_studentapplication.Examsession = tbl_managesession.idmangesession',array("tbl_managesession.managesessionname")) 	
							 				->join(array("tbl_center"=>"tbl_center"),'tbl_studentapplication.Examvenue = tbl_center.idcenter',array("tbl_center.centername")) 	
							 				->where("tbl_registereddetails.RegistrationPin=?", $idregpin )
											->where("tbl_studentapplication.pass != 3 or tbl_studentapplication.pass != 5")
											->order("tbl_studentapplication.FName");	
										
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;		
	}
	
	
	public function fngetprogramdiscount($idprog, $noofcandidates) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_programmaster" ), array ("a.*" ) )->join ( array ("b" => "tbl_programrate" ), 'a.IdProgrammaster=b.idProgram', array ("b.*" ) )->where ( "a.IdProgrammaster=?", $idprog )->where ( "a.Active = 1" );
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		
		for($i = 0; $i < count ( $larrResult ); $i ++) {
			$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_accounthead" ), array ("a.*" ) )->where ( "a.idAccount=?", $larrResult [$i] ['IdAccountmaster'] )->where ( "a.minnostd in (select b.minnostd from tbl_accounthead as  b where a.idAccount=b.idAccount and  b.minnostd <=$noofcandidates)" )->where ( "a.Active=1" )->order ( "a.minnostd desc" );
			$larraysum = $lobjDbAdpt->fetchAll ( $lstrSelect );
			$sum = $larraysum;
		
		// $sum= $sum+$sum;
		}
		return $sum;
	}
	
	public function fnGetAmountInWords($Amount) { //echo $Amount;die;
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = "SELECT CONCAT(ucwords(str_numtowords($Amount)),' ','Only') as Amount";
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
	}
	
	public function fnGetExamDetails($IdApplication) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_batchregistration" ), array ('a.*' ) )->where ( "a.idBatchRegistration  = ?", $IdApplication );
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
	}
	
	public function updatecompanypass($password, $idcomp) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$data = array ('Password' => $password );
		$where ['idtakafuloperator = ?'] = $idcomp;
		$db->update ( 'tbl_takafuloperator', $data, $where );
	}
	
	public function fnInsertPaypaldetails($larrformData, $studentId, $idPayment) {
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$table = "tbl_batchpaypal";
		$postData = array ('IDCompany' => $idPayment, 
							'paymentFee' => $larrformData ['mc_gross'], 
							'grossAmount' => $larrformData ['mc_gross'], 
							'payerId' => $larrformData ['payer_email'], 
							'transactionId' => $larrformData ['txn_id'], 
							'verifySign' => $larrformData ['verify_sign'], 
							'entryFrom' => 1,  
							'UpdDate' => $larrformData ['UpdDate'], 
							'UpdUser' => $larrformData ['UpdUser'], 
							'paymentStatus' => 1 );
		
		$db->insert ( $table, $postData );
		$lastid = $db->lastInsertId ( "tbl_batchpaypal", "idpaypalDetails" );
		
		//Function to check if regpin already generated or not .
		$lstrSelect = $db->select()
		                          ->from(array("tbl_batchregistration"),array("idBatchRegistration"))
		                          ->where("idBatchRegistration = ?",$idPayment)
		                          ->where("registrationPin=0");
		 $larrResult = $db->fetchRow($lstrSelect); 
		 
		 if($larrResult){		
				$larrformData1 ['registrationPin'] = $larrformData ['Regid'];
				$larrformData1 ['paymentStatus'] = 1;
				$larrformData1 ['Approved'] = 1;
		 }else{		 		
				$larrformData1 ['paymentStatus'] = 1;
				$larrformData1 ['Approved'] = 1;
		 }
		 
		$where = "idBatchRegistration = '" . $idPayment . "'";
		$db->update ( 'tbl_batchregistration', $larrformData1, $where );
	}
	
	///new addition
	

	public function listBatchApplication($idCompany) {
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_batchregistration" ), array ("a.*" ) )
											->join ( array ("c" => "tbl_studentpaymentoption" ), 'a.idBatchRegistration = c.IDApplication AND c.companyflag=2',array("c.ModeofPayment") )
											->where ( "a.idCompany   = ?", $idCompany );
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
	}
	
	
		public function fngetbatchtakafuldetails($idbatchreg)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()				
						                 ->from(array("a" => "tbl_batchregistration"),array("a.*"))
						                 ->join(array("b"=>"tbl_takafuloperator"),'a.idCompany=b.idtakafuloperator',array("b.*"))
						                 ->where("a.idBatchRegistration  = ?",$idbatchreg);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	public function fninsertmigspayment($larrformData,$operator)
{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_batchmigspayment";
		$postData = array(		
           					'vpc_MerchTxnRef' =>$larrformData['vpc_MerchTxnRef'],		
           					'vpc_Card' =>$larrformData['vpc_Card'],	
           					'vpc_Merchant' =>$larrformData['vpc_Merchant'],
					        'vpc_TransactionNo' =>$larrformData['vpc_TransactionNo'],	
           					'vpc_TxnResponseCode' => $larrformData['vpc_TxnResponseCode'], 
							'vpc_SecureHash' =>$larrformData['vpc_SecureHash'],	
           					'vpc_VerStatus' =>$larrformData['vpc_VerStatus'],	
           					'vpc_BatchNo' =>$larrformData['vpc_BatchNo'],	
							'vpc_AuthorizeId' =>$larrformData['vpc_AuthorizeId'],	
							'vpc_ReceiptNo' =>$larrformData['vpc_ReceiptNo'],
							'batchflag' =>$operator,
           					'UpdDate' =>date('Y-m-d H:i:s'));
						  $lobjDbAdpt->insert($table,$postData);
						  
}	
public function fngetbatchregistrationdetails($idbatch)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()				
						                 ->from(array("a" => "tbl_batchregistration"),array("a.*"))
						                // ->join(array("b"=>"tbl_takafuloperator"),'a.idCompany=b.idtakafuloperator',array("b.*"))
						                 ->where("a.idBatchRegistration  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
}
public function fnupdatebatchregdetails($idbatchreg,$randomnumber)
{
	$db 	= 	Zend_Db_Table::getDefaultAdapter();
	
		  $postData = array(		
							'registrationPin' => $randomnumber,	
			     			'paymentStatus' => 1,
		    				'Approved'=>1													
						);	
		$where = "idBatchRegistration = '".$idbatchreg."'"; 	
		return $db->update('tbl_batchregistration',$postData,$where);
}
	
}