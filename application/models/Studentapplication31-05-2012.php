<?php
class App_Model_Studentapplication extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
/*
 * function to fetch all the batch details
 */
	public function fnGetDisabledate($city)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_schedulerexception"),array("a.*"))
										  ->where("a.idcity  = ?",$city);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	public function fnGetBatchName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchmaster"),array("key"=>"a.IdBatch","value"=>"a.BatchName"))
										  ->where("a.BatchStatus  = ?","0");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}	

	public function fnGetStateName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
										   ->where("a.idCountry  = ?","121")
										   ->order("a.StateName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			
	public function fnGetemailprogramdetails($Emails,$idprog)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	
	$select = "SELECT a.*
FROM `tbl_studentapplication` a
WHERE  a.EmailAddress='$Emails'
AND a.Program=$idprog
AND a.Examvenue!=000";
	//echo $select;die();
return	$result = $lobjDbAdpt->fetchAll($select);
  	
  		
} 		
			
			
public function fngetdayStudent($dates){	
	//echo $dates;die();
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
				 $select = "select DAYNAME('$dates') as days"; 
			
				$result = $lobjDbAdpt->fetchAll($select);
				return $result;
			}
			
			public function fngetminimumage()
			{
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_config"),array("a.MinAge"));										  
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
 /*
  * funtion to fetch all the venue based on the batch
  */			
  public function fnGetVenueName($lintidscheduler)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"b.idschedulervenue","value"=>"a.centername"))
										  ->join(array("b" =>"tbl_schedulervenue"),'a.idcenter = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulermaster"),'b.idschedulermaster = c.idschedulermaster',array())
										  ->where("c.idschedulermaster  = ?",$lintidscheduler)
										  ->order("a.city");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
public function fnupdatedstudentapplication($lintidstudent)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										  //->join(array("b" =>"tbl_programmaster"),'a.Program = b.IdProgrammaster',array("b.ProgramName"))
										  ->where("a.IDApplication  = ?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
public function fnviewscoursedtudentdetails($lintidstudent)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array())
										  ->join(array("b" =>"tbl_programmaster"),'a.Program = b.IdProgrammaster',array("b.ProgramName"))
										  ->where("IDApplication  = ?",$lintidstudent);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  /*
   * function to fetch the timings based on the venue("CONCAT(TRIM(a.FirstNm),'_',IFNULL(TRIM(a.MiidleNM),''),'',IFNULL(TRIM(a.LastNM),'')) AS EmployeeName")
   */
public function fnGetVenueTime($idvenue)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
/* 	echo 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("key"=>"a.idschedulervenuetime","value"=>("distinct(a.Date) as Date")))
										 // ->join(array("b" =>"tbl_center"),'a.idcentre = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulervenue"),'a.idschedulervenue = c.idschedulervenue',array())
										  ->where("c.idschedulervenue  = ?",$idvenue);die();*/
 	$lstrSelect="SELECT (a.Date)as `key`, DATE_FORMAT(a.Date,'%d-%m-%Y') as value FROM `tbl_schedulervenuetime` AS `a`
 INNER JOIN `tbl_schedulervenue` AS `c` ON a.idschedulervenue = c.idschedulervenue WHERE (c.idschedulervenue  = $idvenue)group by value";
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }

	public function fnGetInitialConfigDetails($iduniversity) {
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()
					->from(array("a" => "tbl_config"),array("a.*"))				
		            ->where("a.idUniversity = ?",$iduniversity);	
		 return $result = $lobjDbAdpt->fetchRow($select);
	}
	
  public function fnGetbatch($idvenue,$datetocmp)
  {
  			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
/*			echo $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchmaster"),array("key"=>"a.IdBatch","value"=>("a.BatchName")))
										  ->join(array("b" =>"tbl_programmaster"),'a.IdProgrammaster = b.IdProgrammaster',array())
										  ->where("b.IdProgrammaster  = ?",$idvenue);die();*/
  			
	 $lstrSelect="SELECT `a`.`IdBatch` AS `key`, CONCAT(DATE_FORMAT(`a`.`BatchFrom`,'%d-%m-%Y'),'---',DATE_FORMAT(`a`.`BatchTo`,'%d-%m-%Y')) AS `value` FROM `tbl_batchmaster` AS `a` INNER JOIN `tbl_programmaster` AS `b` ON a.IdProgrammaster = b.IdProgrammaster WHERE (b.IdProgrammaster =$idvenue) and  a.BatchFrom > '$datetocmp'";			
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  /*
   * function to fetch all the scheduler based on the prog
   */
 public function fnGetscheduler($idbatch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("key"=>"a.idschedulermaster","value"=>("a.ScheduleName")))
										  ->where("a.idBatch  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  
 public function fnGetDefinations($idbatch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("key"=>"a.idschedulermaster","value"=>("a.ScheduleName")))
										  ->where("a.idBatch  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  /*
   * function to get the time
   */
  public function fnGetTimings($date,$venue)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("key"=>"a.idschedulervenuetime","value"=>("a.From")))
										  ->where("a.Date = '$date'")
										  ->where("a.idschedulervenue=?",$venue);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  /*
   * fun;ction to insert into the table
   */
   public function fnAddStudent($larrformData,$scheduleryear) { //Function to get the user details
   	
   /*	echo "<pre/>";
   	print_r($larrformData);
   	die();*/
	if($larrformData['setmonth']<10)
   	{
   		$months = '0'.$larrformData['setmonth'];
   		//$larrformData['setmonth']=
   	}
   	else 
   	{
   		$months = $larrformData['setmonth'];
   	}
   	
    if($larrformData['setdate']<10)
   	{
   		$date = '0'.$larrformData['setdate'];
   		//$larrformData['setmonth']=
   	}
   	else {
   		 		$date = $larrformData['setdate'];
   	}
   	
   	if($larrformData['Takafuloperator'] == '')
   	{
   	 $larrformData['Takafuloperator']=0;
   	}
   	if($larrformData['Religion'] == '')
   	{
   	 $larrformData['Religion']=0;
   	}
   	
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_studentapplication";
		$postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['FName'],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['DateOfBirth'],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['EmailAddress'],
					'username' =>$larrformData['EmailAddress'],	
           					'UpdDate' => $larrformData['UpdDate'], 
							'UpdUser' =>$larrformData['UpdUser'],	
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>$larrformData['Amount'],	
           					'ICNO' =>$larrformData['ICNO'],	
					'password' =>$larrformData['ICNO'],	
           					'Payment' => 0, 
								'DateTime' =>$scheduleryear.'-'.$months.'-'.$date,	
           					'PermAddressDetails' =>$larrformData['PermAddressDetails'],	
           					'Takafuloperator' =>$larrformData['Takafuloperator'], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 0, 
							'Gender' =>$larrformData['Gender'],	
           					'Race' =>$larrformData['Race'],	
           					'Qualification' =>$larrformData['Qualification'], 
							'State' =>$larrformData['State'],	
           					'CorrAddress' =>$larrformData['CorrAddress'],	
           					'PostalCode' =>$larrformData['PostalCode'], 
		 					'ContactNo' =>$larrformData['ContactNo'],	
           					'MobileNo' =>$larrformData['MobileNo'], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  					'Year'=>$larrformData['Year'],
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  					'Examvenue'=>'000',	
		  					'Examsession'=>'000',
		  					'Pass'=>'3',
							 'Religion'=>$larrformData['Religion']			  
						);
						  $lobjDbAdpt->insert($table,$postData);
						   $lastid  = $lobjDbAdpt->lastInsertId("tbl_studentapplication","IDApplication");	
						   return $lastid;
     }
     
    public function fnupdateexamvenue($idvenue,$idapplication)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Examvenue'] = $idvenue;	
		 $where = "IDApplication = '".$idapplication."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
    }
    
    public function fnupdateexamvenuess($idvenue,$idsession,$idapplication)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	  $postData = array(		
							'Examvenue' => $idvenue,	
			     			'Examsession' => $idsession													
						);
		 $where = "IDApplication = '".$idapplication."'"; 	
		 $db->update('tbl_studentapplication',$postData,$where);
    }
     
 public function  fnInsertStudentPaymentdetails($mod,$lastInsId)
  {
  
  	 $db = Zend_Db_Table::getDefaultAdapter();
  	$table = "tbl_studentpaymentoption";
		  $postData = array(		
							'IDApplication' =>$lastInsId,	
           					'ModeofPayment' =>$mod,	
           					'companyflag' => 0,           				                           
                            /*'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
            				 'AdhocDate' => '0000-00-00',	
            				'AdhocVenue' =>'others'			*/		
						);
						  $db->insert($table,$postData);
						//   $lastids  = $db->lastInsertId("tbl_studentpaymentoption","idstudentpaymentoption");	
						 //  return $lastids;
						 
							
  }

public function fngetPaymentDetails($idPayment){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.*"))
										  ->join(array("c" => "tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.ProgramName"))
										  ->where("a.IDApplication = ?",$idPayment);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
  
  public function fnGetVenuedetails($year,$prog,$city)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler',array('f.idmanagesession as managesession'))
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("d.*"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array("e.*"))
										  ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										  ->where("d.Active=1")
										  //->group("f.idmanagesession")
										  ->where("a.idnewscheduler =?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  
 public function fnCountVenuedetails($larrdate)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler',array('f.idmanagesession as managesession'))
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("d.*"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array("e.*"))
										  ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										  ->group("f.idmanagesession")
										  ->where("a.Year =?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  public function fngetmodeofpayment($mod)
  {
  	 $db = Zend_Db_Table::getDefaultAdapter();
  	 $lstrSelect = $db->select()
				->from(array("a" => "tbl_studentpaymentoption"),array("a.ModeofPayment"))
				 ->where("a.idstudentpaymentoption = ?",$mod);	
				$larrResult = $db->fetchRow($lstrSelect);
						  return $larrResult;	
  }
     public function fnGetRulesRegulation($id)
     {
     	 $db = Zend_Db_Table::getDefaultAdapter();
  	           $lstrSelect = $db->select()
				->from(array("a" => "tbl_termsandcondition"),array("a.Terms"))
				 ->where("a.studenttype = ?",$id);	
				$larrResult = $db->fetchAll($lstrSelect);
						  return $larrResult;	
     }
     /*
      * function to fetch all the details
      */
  public function fngetStudentDetails() { //Function to get the user details
        $result = $this->fetchAll();
        return $result->toArray();
     }
     
     /*
      * function to fetch all the details
      */
     public function fnviewstudentdetails($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										   ->join(array("b" =>"tbl_newscheduler"),'a.Year=b.idnewscheduler',array('b.Year as years'))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
     
  public function fnGetVenuedetailsgetsecid($idsech)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
						                  ->where("a.Active=1")
										  ->where("a.idnewscheduler =?",$idsech); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  
public function fnGetVenuedetailsRemainingseats($year,$idsech,$city,$month,$Date)
  {
  	
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $select = "SELECT b.NumberofSeat, b.centername,b.idcenter, d.managesessionname, d.starttime, d.endtime,d.idmangesession, (
b.NumberofSeat - IFNULL( count( a.IDApplication ) , 0 )
) AS rem
FROM `tbl_studentapplication` a, tbl_center b, tbl_managesession d,tbl_newscheduler m
WHERE a.Examvenue = b.idcenter
AND a.Examdate =$Date
AND a.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND b.Active =1
and  b.city =$city
AND d.idmangesession = a.Examsession
GROUP BY a.Examvenue, a.Examsession
UNION
SELECT b.NumberofSeat, b.centername,b.idcenter, j.managesessionname, j.starttime, j.endtime, j.idmangesession, b.NumberofSeat AS rem
FROM tbl_center b,tbl_managesession e,`tbl_studentapplication` c,tbl_newschedulersession f, tbl_managesession j,tbl_newscheduler m
WHERE f.idmanagesession NOT
IN (
SELECT b.Examsession
FROM tbl_studentapplication b
WHERE b.Examvenue = c.Examvenue
AND b.ExamCity =$city
AND b.Examdate =$Date
AND b.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND b.Examvenue !=000
AND b.Examsession !=000
GROUP BY b.Examvenue, b.Examsession
)
AND c.ExamCity =$city
AND c.Examdate =$Date
AND c.Exammonth =$month
AND m.idnewscheduler =$idsech
AND m.Year =$year
AND c.Examvenue !=000
AND f.idnewscheduler =$idsech
AND f.idmanagesession=j.idmangesession
AND c.Examvenue = b.idcenter
UNION
SELECT b.NumberofSeat, b.centername,b.idcenter, e.managesessionname, e.starttime, e.endtime,e.idmangesession, b.NumberofSeat AS rem
FROM tbl_center b, tbl_newschedulervenue c, tbl_managesession e
WHERE b.idcenter NOT
IN (
SELECT Examvenue
FROM tbl_studentapplication
WHERE ExamCity =$city
AND Examdate =$Date
AND Exammonth =$month
AND Year =$idsech
)
AND b.city =$city
AND b.Active =1
AND  c.idvenue = b.idcenter
AND e.idmangesession
IN (
SELECT h.idmanagesession
FROM tbl_newschedulersession h
WHERE idnewscheduler =$idsech
)" ;
		 return	$result = $lobjDbAdpt->fetchAll($select);	
  }
  
 public function fnviewstudentdetailssss($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										   ->join(array("d" =>"tbl_newscheduler"),'a.Year=d.idnewscheduler',array('d.Year as years'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										   ->join(array("e"=>"tbl_managesession"),'a.Examsession=e.idmangesession',array('e.*'))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
     /*
      * function to update the student details
      */
     public function updatestudent($lintstudentid,$larrformData)
     {
     	/*$larrformData['HomePhone'] = $larrformData['homecountrycode']."-".$larrformData['homestatecode']."-".$larrformData['HomePhone'];
		$larrformData['CellPhone'] = $larrformData['countrycode']."-".$larrformData['statecode']."-".$larrformData['CellPhone'];
		unset($larrformData['countrycode']);
		unset($larrformData['statecode']);
		unset($larrformData['homecountrycode']);
		unset($larrformData['homestatecode']);*/
     	
     	$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$where = "IDApplication = '".$lintstudentid."'"; 	
		return $db->update('tbl_studentapplication',$larrformData,$where);
     }
     
     /*
      * functin to search the student
      */
	public function fnSearchStudent($post = array()) { //Function for searching the user details
    	$db = Zend_Db_Table::getDefaultAdapter();
		//$field7 = "Active = ".$post["field7"];
		$select = $db->select() 	
			   ->join(array('a' => 'tbl_studentapplication'),array('a.*'))
				->where('a.LName like "%" ? "%"',$post['field3'])
			   ->where('a.FName like  "%" ? "%"',$post['field2'])
			   ->where('a.MName like "%" ? "%"',$post['field4']);
			   //
			  // ->where($field7);exit;
		$result = $db->fetchAll($select);
		return $result;
	}
	/*
	 * function for fetching details
	 * 
	 */
         public function fnGetStudentName($idstudent) {
			$db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT CONCAT(FName,' ',IFNULL(MName,' '),' ',IFNULL(LName,' ')) AS Name  FROM tbl_studentapplication WHERE IDApplication = $idstudent";
    		$result = $db->fetchRow($sql);    
			return $result;
    	}
    	/*
    	 * Paypal Entries
    	 * */
  public function fnInsertPaypaldetails($larrformData,$studentId){
 //print_r($larrformData);//exit;
  		 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_paypaldetails";
          
           $lstrselecttransactionid = $db->select()
						  ->from(array("a" =>"tbl_paypaldetails"),array('a.*'));	
		     $larrResult = $db->fetchAll($lstrselecttransactionid);
		/*     echo "<pre/>";
		     print_r($larrResult);
		     die();*/
		     $flag=0;
		     for($i=0;$i<count($larrResult);$i++)
		     {
		     	if($larrformData['txn_id'] ==$larrResult[$i]['transactionId'])
		     	{
		     		$flag=1;
		     		break;
		     	}
		     }
		$lstrSelect2 = $db -> select()
				  -> from(array("a" =>"tbl_registereddetails"),array('a.*'))
				  -> where("a.IDApplication  = ?",$studentId);	
			 $larrResult2 = $db->fetchAll($lstrSelect2);	
			 if(count($larrResult2) >0){
			 	$flag = 1;	     		
			 }
       ///  die();
		     if($flag==0)
		     {
            $postData = array(		
							'IDApplication' => $studentId,	
           					'paymentFee' =>$larrformData['mc_gross'],	
           					'grossAmount' => $larrformData['mc_gross'],	
            				'payerId' =>$larrformData['payer_email'],		
		            		'transactionId' =>$larrformData['txn_id'],  
            				'verifySign' =>$larrformData['verify_sign'],                            
                            'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
                            'paymentStatus'=> 1											
						);			
	     $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_paypaldetails","idpaypalDetails");
		 
		
		 $larrformData1['Payment'] = 1;	
		 $where = "IDApplication = '".$studentId."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
		 
		 
		 
		 $lstrSelect = $db->select()
						  ->from(array("a" =>"tbl_studentapplication"),array('a.IdBatch','a.Takafuloperator'))
						  ->where("a.IDApplication  = ?",$studentId);	
		 $larrResult = $db->fetchRow($lstrSelect);	
		 	 
		 $ModelBatchlogin = new App_Model_Batchlogin();
		 $Regid = $ModelBatchlogin->fnGenerateCode($larrResult['Takafuloperator'],$studentId);	
		 	 
		 $table = "tbl_registereddetails";
         $postData = array('Regid' =>   $Regid,	
           					'IdBatch' =>$larrResult['IdBatch'],	
         					'Approved' =>1,	
         					'RegistrationPin'=>'0000000',
         					'Cetreapproval'=>'0',
           					'IDApplication' => $studentId);					
	     $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_registereddetails","idregistereddetails");
		 return $Regid;
		     }
		     else if($flag==1){
		     	
		     }
  } 	
/*
 * function to fetch all the Prog Name
 */
public function fnGetProgramName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										  
										  ->join(array("c" => "tbl_program"),'c.IdProgram=a.idprog')
										  ->join(array("b"=>"tbl_batchmaster"),'b.IdProgrammaster=a.IdProgrammaster',array())
										  ->join(array("d"=>"tbl_tosmaster"),'b.IdBatch=d.IdBatch',array())
  										->join(array("e"=>"tbl_programrate"),'a.IdProgrammaster=e.idProgram',array())
										  ->where("e.Active=1")
										  ->where("d.Active=1")
										  ->where("c.Active =1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
/*
 * function to fetch all the Prog Name
 */
	public function fnTakafuloperator(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
		
	/*
	 * function to fetch all the amount for that course
	 */
	
	public function fnGetProgAmount($idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT sum(abc.amount) from 

(SELECT max(a.EffectiveDate),a.Rate as amount  from tbl_programrate a,tbl_accountmaster b
where a.idProgram=$idprog and 
 b.idAccount= a.IdAccountmaster and  
 b.Active=1 and a.Active=1 and b.idAccount not in (select t.idAccount from tbl_accountmaster t where t.duringRegistration=1 ) group by a.IdAccountmaster) as abc ";
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	
	
	
	/*
	 *get course amount for fail  
	 */
	
	public function fnGetProgAmountfail($idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT sum(abc.amount) from 

(SELECT max(a.EffectiveDate),a.Rate as amount  from tbl_programrate a,tbl_accountmaster b
where a.idProgram=$idprog and 
 b.idAccount= a.IdAccountmaster and  
 b.Active=1 and a.Active=1 and b.duringRegistration=1 group by a.IdAccountmaster) as abc ";
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetTimingsForDate($date,$idscheduler)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect="SELECT idschedulervenuetime as `key`,`From` as value FROM `tbl_schedulervenuetime` WHERE `idschedulervenue`=$idscheduler and Date='$date'";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	/*
	 * functin to fetch theno of seats
	 */
	public function fnGetNoOfSeats($lintidvenuetime)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("a.NoofSeats"))
										  //->from(array("b" =>"tbl_schedulervenuetime"),'a.idschedulervenue = b.idschedulervenue',array())
										  //->where("b.Date  = ?",$lintdate)
										    //->where("b.idcentre  = ?",$lintidvenue)
										    ->where("a.idschedulervenuetime =?",$lintidvenuetime);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	/*
	 * function to get theno of students aplied for the same exam
	 */
  public function fnGetNoofStudents($lintidvenuetime)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$select ="select count(IDApplication) from tbl_studentapplication where DateTime=$lintidvenuetime";
  	$larrResult = $lobjDbAdpt->fetchRow($select);
				return $larrResult;
  }
  
  public function fnGetNoOfTakafulOperatorForStudent($idtakaful)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$select ="select count(IDApplication) from tbl_studentapplication where Takafuloperator=$idtakaful";
  	$larrResult = $lobjDbAdpt->fetchRow($select);
				return $larrResult;
  }
  
  public function fnGetNoOfTakafulOperator($idtakaful)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$select ="select NumberofSeat from tbl_takafuloperator where idtakafuloperator=$idtakaful";
  	$larrResult = $lobjDbAdpt->fetchRow($select);
				return $larrResult;
  }
	public function fnGetSMTPSettings(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_config"),array("a.SMTPServer","a.SMTPUsername","a.SMTPPassword","a.SMTPPort","a.SSL","a.DefaultEmail") );
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}	
		
		
public function fnGetICorArmyNo()
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.ICNO","a.ArmyNo"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
 public function fnGetStudentdetailsbasedonicno($icno)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										    ->where("a.ICNO =?",$icno)
										    ->order("a.IDApplication desc");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 
 
 public function fnGetStudentdetailsbasedonarmyno($armyno)
{
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										    ->where("a.ArmyNo =?",$armyno);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 public function fngetintialconfigdetails()
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_config"),array("a.*"));
										    //->where("a.ArmyNo =?",$armyno);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 public function fnGetPreRequesition($idprog)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_programmaster"),array("a.*"))
										    ->where("a.IdProgrammaster =?",$idprog);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 
 public function fnGetPreRequesitionProgDetails($idprog)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_programmaster"),array("a.*"))
										    ->where("a.IdProgrammaster =?",$idprog);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
public function fnGetEducation()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where('a.idDefType = 14')
										  ->where('a.Active= 1');	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
 public function fnGetsameexam($lintidprog,$lintidicno)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										    ->where("a.ICNO =?",$lintidicno)
										       ->where("a.Payment =1")
										     ->where("a.Program =?",$lintidprog);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 ////////////////////////////////fetch all states for course/////////////////////////////////
 
 
/*
 * function to fetch all the Prog Name
 */
public function fnGetStatelistforcourse($idprog,$idyear){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->group("e.idState")
										  ->where("a.idnewscheduler=?",$idyear);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
public function fnbetweenmonths($id)
{
$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
										  ->where("a.idnewscheduler=?",$id);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	
}
public function fnGetMonthlistofcourse($from,$to){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("e"=>"tbl_newmonths"),array("key"=>"e.idmonth","value"=>"e.MonthName"))
										  ->where("e.idmonth  >= ?",$from)
										   ->where("e.idmonth  <= ?",$to);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
 ///////////////////////////////////////////////////////////////////////////////////////////
 
public function fnGetCitylistforcourse($idstate,$idprog,$idseched){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array("key"=>"e.idCity","value"=>"e.CityName"))
										  ->where("e.idState  = ?",$idstate)
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler  = ?",$idseched)
										  ->group("e.CityName");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
public function fnGetYearlistforcourse($idprog){		
			/*	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Year"))
										  //->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())


										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult; */



/*$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"CONCAT(IFNULL(a.Year,'0') , '(', IFNULL(d.MonthName, '0' ),' To ',IFNULL(e.MonthName, '0' ),')')"))
										  //->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										    ->join(array("d"=>"tbl_newmonths"),"a.From=d.idmonth",array())
										  ->join(array("e"=>"tbl_newmonths"),"a.To=e.idmonth",array())
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())


										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;*/


$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.idnewscheduler","value"=>"a.Description"))
										  //->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										    ->join(array("d"=>"tbl_newmonths"),"a.From=d.idmonth",array())
										  ->join(array("e"=>"tbl_newmonths"),"a.To=e.idmonth",array())
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())

                                          ->where("a.Active   = 1")
										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;





	}
		
public function fnGetDaysforcourse($idprog,$year){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.Year"))
										  ->join(array("b"=>"tbl_newschedulerdays"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.idnewscheduler = ?",$year)
										  ->order("b.Days");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
public function fnGetMonths($year,$Program){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->where("c.IdProgramMaster  = ?",$Program)
										  ->where("a.idnewscheduler = ?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}


public function fnGetActiveSet($Program){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("a.*"))
										  ->join(array("c"=>"tbl_batchmaster"),'a.IdProgrammaster=c.IdProgrammaster')
										  ->join(array("d"=>"tbl_tosmaster"),'d.IdBatch=c.IdBatch')
										  ->where("a.IdProgramMaster  = ?",$Program)
										  ->where("d.Active=1");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
		
public function fnGetTempDays($day,$year,$month,$dateid)
{
	$sessionID = Zend_Session::getId();
	
	 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_studenttempday";
         $postData = array('day' =>$day,	
           					'year' =>$year,	
            				'month' =>$month,		
		            		'dateid' =>$dateid,
                            'sessionid'=>$sessionID			
						);	
	    $result =  $db->insert($table,$postData);
	     return $result;
}

 public function fnDeleteTempDetails($id)
 	{
 		$sessionID = Zend_Session::getId();
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$select = "Delete FROM tbl_studenttempday  where dateid !=$id and sessionid='$sessionID'";
 		$db->query($select);
 		
 	}
 	
 public function fngetdetails()
 {
 	$sessionID = Zend_Session::getId();
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studenttempday"))
										  ->where("a.sessionid  = ?",$sessionID);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }
 
 public function fngetstudenttempdays($larrstudenttempdetails)
 {
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studenttempday"))
										  ->where("a.idstudenttempday = ?",$larrstudenttempdetails);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }	
 
 public function fnGetAmountInWords($Amount)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT CONCAT(ucwords(str_numtowords($Amount)),' ','Only') as Amount";		
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
	public function fnGetExamDetails($IdApplication)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->where("a.IDApplication  = ?",$IdApplication);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
 
public function fnGetEducationDetails()
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where("a.idDefType = 14");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
 
     public function fngetRegid($lintidstudent)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_registereddetails"),array('a.*'))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
  public function fnstudentconfirm($idprog,$idicno)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->where("a.ICNO = ?",$idicno)
										//->where("a.Payment = 1")
										->where("a.Examvenue!=000")
										   ->where("a.Program = ?",$idprog)
										   ->order("a.IDApplication desc");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
  
  public function initconfig()
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_config"),array('a.*'))
										  ->where("a.idConfig = 1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }
 
  public function fngetidpassdetails($RegID)
  { 
  	$regid=$RegID;
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$lstrSelect = $lobjDbAdpt->select()
	                         ->from(array("a" =>"tbl_registereddetails"),array())
	                         ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication = b.IDApplication',array('b.IDApplication as IDApplication','b.pass as pass'))
							 ->where('a.Regid = ?',$regid);
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
  }
  
  public function fngetresultdetails($IDAPPLICATION)
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 $lstrSelect="SELECT CONCAT_WS( ' ', IFNULL( a.FName, '' ) , IFNULL( a.MName, '' ) , IFNULL( a.LName, '' ) ) AS 'Studentname',
				        a.ICNO as 'PersonalID',
                        CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(b.Year,'')) as ExamDate
                 FROM tbl_studentapplication a, tbl_newscheduler b
                 WHERE a.Year = b.idnewscheduler and a.IDApplication = $IDAPPLICATION"; 
  	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
  	return $larrResult;
  }
 
   public function fngetstudeappdetails($RegID)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$lstrSelect="SELECT CONCAT_WS( ' ', IFNULL( a.FName, '' ) , IFNULL( a.MName, '' ) , IFNULL( a.LName, '' ) ) AS 'Studentname',
                 		b.ProgramName as ProgramName,
                		a.Amount as 'ExaminationFee',
                 		CONCAT_WS( ' ', IFNULL( a.Examdate, '' ) ,'-', IFNULL( a.Exammonth, '' ),'-', IFNULL( e.Year, '' )) as ExamDate,
                 		c.centername as Venue,
                 		a.PermAddressDetails as Address1,
                 		a.CorrAddress as Address2,
                 		
                 		CONCAT_WS( ' ', IFNULL( d.managesessionname, '' ) , IFNULL( d.starttime, '' ),'to', IFNULL( d.endtime, '' )) as Session

                 FROM tbl_studentapplication a, tbl_programmaster b, tbl_center c, tbl_managesession d,tbl_newscheduler e

                 WHERE a.Program = b.IdProgrammaster and a.Examvenue=c.idcenter and a.Examsession=d.idmangesession and a.Year = e.idnewscheduler
                 AND a.IDApplication = $RegID";
   	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
  	return $larrResult;
   }


 public function fnupdateStudentdetailsonvenueno($idapplication)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['pass'] = 0;	
		 $where = "IDApplication = '".$idapplication."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
    }
    
    
 public function fnGetStudentdetailsonvenueno($insertedId)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										    ->where("a.IDApplication =?",$insertedId);
										    //->order("a.IDApplication desc");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
   
 
  public function fnUpdateStudent($result,$larrformData,$scheduleryear)
   {
                if($larrformData['setmonth']<10)
   	{
   		$months = '0'.$larrformData['setmonth'];
   		//$larrformData['setmonth']=
   	}
   	else 
   	{
   		$months = $larrformData['setmonth'];
   	}
   	
    if($larrformData['setdate']<10)
   	{
   		$date = '0'.$larrformData['setdate'];
   		//$larrformData['setmonth']=
   	}
   	else {
   		 		$date = $larrformData['setdate'];
   	}
   	if($larrformData['Takafuloperator'] == '')
   	{
   	 $larrformData['Takafuloperator']=0;
   	}
   	if($larrformData['Religion'] == '')
   	{
   	 $larrformData['Religion']=0;
   	}
   	
   $db 	= 	Zend_Db_Table::getDefaultAdapter();	
		$postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['FName'],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['DateOfBirth'],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['EmailAddress'],	
           					'UpdDate' => $larrformData['UpdDate'], 
							'UpdUser' =>$larrformData['UpdUser'],	
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>$larrformData['Amount'],	
           					'ICNO' =>$larrformData['ICNO'],	
							'DateTime' =>$scheduleryear.'-'.$months.'-'.$date,	
           					'PermAddressDetails' =>$larrformData['PermAddressDetails'],	
           					'Takafuloperator' =>$larrformData['Takafuloperator'], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 0, 
							'Gender' =>$larrformData['Gender'],	
           					'Race' =>$larrformData['Race'],	
           					'Qualification' =>$larrformData['Qualification'], 
							'State' =>$larrformData['State'],	
           					'CorrAddress' =>$larrformData['CorrAddress'],	
           					'PostalCode' =>$larrformData['PostalCode'], 
		 					'ContactNo' =>$larrformData['ContactNo'],	
           					'MobileNo' =>$larrformData['MobileNo'], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  					'Year'=>$larrformData['Year'],
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  	  'username' =>$larrformData['EmailAddress'],
                                                       'password' =>$larrformData['ICNO'],
                                                        'Religion'=>$larrformData['Religion'],		
		  					'Pass'=>'3'			  
						);
		$where['IDApplication = ? ']= $result;		
		return $db->update('tbl_studentapplication', $postData, $where);	
   }

   public function fngetschedulerexception($days,$lintcity)
   {
   	  $db 	= 	Zend_Db_Table::getDefaultAdapter();	
   		$lstrSelect = $db->select()
	                         ->from(array("a" =>"tbl_schedulerexception"))
	                         ->where('a.Date=?',$days)
	                         ->where('a.idcity=?',$lintcity);
	    $larrResult = $db->fetchRow($lstrSelect);
	    return $larrResult;
   	   
   }

public function fnstudentconfirm88($idprog,$Emails,$day,$month,$year)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	
	$select = "SELECT a.*
FROM `tbl_studentapplication` a,tbl_newscheduler m
WHERE a.Year=m.idnewscheduler
AND a.Examdate >=$day
AND a.Exammonth >=$month
AND m.Year >=$year
AND a.EmailAddress='$Emails'
AND a.Program=$idprog
AND a.Examvenue!=000
order by a.IDApplication desc";
return	$result = $lobjDbAdpt->fetchRow($select);
  	
  		
}

public function fnstudentconfirmvaliddate($idprog,$idicno,$days)
  {
 
$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 $select =
"SELECT a.*
FROM tbl_studentapplication a
WHERE a.IDApplication =(select max(IDApplication) from tbl_studentapplication where pass=1 
AND ICNO =$idicno
AND Program=$idprog)
UNION
SELECT a.*
FROM tbl_studentapplication a
WHERE a.IDApplication =(select max(IDApplication) from tbl_studentapplication where Payment=1 
AND ICNO =$idicno
AND Program=$idprog
and pass =3)
and a.DateTime >= $days
UNION
SELECT a.*
FROM `tbl_studentapplication` a
WHERE a.IDApplication = (select max(IDApplication) from tbl_studentapplication
where ICNO =$idicno
AND Program=$idprog
AND Examvenue!=000)
and a.DateTime >= $days";

return	$result = $lobjDbAdpt->fetchRow($select);
  }
  
   public function fnstudentdetailspayment($iduniqueapp)
   {
   	  $db 	= 	Zend_Db_Table::getDefaultAdapter();	
   		$lstrSelect = $db->select()
	                         ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
	                         ->where('a.IDApplication=?',$iduniqueapp)
	                         ->where('a.Payment=0');
	    $larrResult = $db->fetchRow($lstrSelect);
	    return $larrResult;
   	   
   }
   
  public function  fndeleteoldstudentdetails($iduniqueapp)
  {
  	$db 	= Zend_Db_Table::getDefaultAdapter();
 		$where = "IDApplication = '".$iduniqueapp."'";
 		$db->delete('tbl_studentapplication',$where);
 		
 		//$where = "IDApplication = '".$iduniqueapp."' AND companyflag=0";
 		//$db->delete('tbl_studentpaymentoption',$where);
  }
  
 public function fncheckStudentPaymentdetails($insertedId)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentpaymentoption"),array("a.*"))
										    ->where("a.IDApplication=?",$insertedId);
										    //->order("a.IDApplication desc");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
public function fngetpreviousdays()
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	
	$select = "SELECT  DATE_SUB(curdate(), INTERVAL 1 DAY) as previousday";
return	$result = $lobjDbAdpt->fetchRow($select);

/*$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();	
				 $select = "select DAYNAME('$dates') as days"; 
			
				$result = $lobjDbAdpt->fetchAll($select);
				return $result;*/
  	
  		
}
  
   
 /*public function fngetidpassdetails($RegID)
  { 
  	$regid=$RegID;
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$lstrSelect = $lobjDbAdpt->select()
	                         ->from(array("a" =>"tbl_registereddetails"),array())
	                         ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication = b.IDApplication',array('b.IDApplication as IDApplication','b.pass as pass'))
							 ->where('a.Regid = ?',$regid);
	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
	return $larrResult;
  }

 public function fngetresultdetails($IDAPPLICATION)
  {
  	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 $lstrSelect="SELECT CONCAT_WS( ' ', IFNULL( a.FName, '' ) , IFNULL( a.MName, '' ) , IFNULL( a.LName, '' ) ) AS 'Studentname',
				        a.ICNO as 'PersonalID',
                        CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(b.Year,'')) as ExamDate,
                        c.ProgramName as CourseName
                 FROM tbl_studentapplication a, tbl_newscheduler b,tbl_programmaster c
                 WHERE a.Year = b.idnewscheduler and a.Program=c.IdProgrammaster and a.IDApplication = $IDAPPLICATION"; 
  	$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
  	return $larrResult;
  }*/

    public function fnGetemailnameicno($lintemailaddress)
 {
 
$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  $select = "SELECT a.*
FROM `tbl_studentapplication` a
WHERE  a.EmailAddress='$lintemailaddress'
AND a.Examvenue!=000
order by a.IDApplication desc";
return	$result = $lobjDbAdpt->fetchRow($select);
 }


 public function fngetyearforthescheduler($id)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_newscheduler"),array("a.*"))
										  ->where("a.idnewscheduler=?",$id);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
  }

public function fnstudentinsertstudentdetails($larrpaymentststus) { //Function to get the user details
   	
   /*	echo "<pre/>";
   	print_r($larrformData);
   	die();*/
   
   	
   	
   	
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_tempstudentapplication";
		$postData = array(		
		                    'IDApplication'=>$larrpaymentststus['IDApplication'],
							'StudentId' =>'00',	
           					'FName' =>$larrpaymentststus['FName'],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrpaymentststus['DateOfBirth'],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrpaymentststus['EmailAddress'],
					        'username' =>$larrpaymentststus['username'],	
           					'UpdDate' => $larrpaymentststus['UpdDate'], 
							'UpdUser' =>$larrpaymentststus['UpdUser'],	
           					'IdBatch' =>$larrpaymentststus['IdBatch'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrpaymentststus['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>$larrpaymentststus['Amount'],	
           					'ICNO' =>$larrpaymentststus['ICNO'],	
					        'password' =>$larrpaymentststus['password'],	
           					'Payment' => $larrpaymentststus['Payment'], 
							'DateTime' =>$larrpaymentststus['DateTime'],	
           					'PermAddressDetails' =>$larrpaymentststus['PermAddressDetails'],	
           					'Takafuloperator' =>$larrpaymentststus['Takafuloperator'], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => $larrpaymentststus['batchpayment'], 
							'Gender' =>$larrpaymentststus['Gender'],	
           					'Race' =>$larrpaymentststus['Race'],	
           					'Qualification' =>$larrpaymentststus['Qualification'], 
							'State' =>$larrpaymentststus['State'],	
           					'CorrAddress' =>$larrpaymentststus['CorrAddress'],	
           					'PostalCode' =>$larrpaymentststus['PostalCode'], 
		 					'ContactNo' =>$larrpaymentststus['ContactNo'],	
           					'MobileNo' =>$larrpaymentststus['MobileNo'], 
		  					'ExamState'=>$larrpaymentststus['ExamState'],
		  					'ExamCity'=>$larrpaymentststus['ExamCity'],
		  					'Year'=>$larrpaymentststus['Year'],
		  					'Examdate'=>$larrpaymentststus['Examdate'],
		  					'Exammonth'=>$larrpaymentststus['Exammonth'],
		  					'Examvenue'=>$larrpaymentststus['Examvenue'],	
		  					'Examsession'=>$larrpaymentststus['Examsession'],
		  					'pass'=>$larrpaymentststus['pass'],
							'Religion'=>$larrpaymentststus['Religion']			  
						);
						  $result=$lobjDbAdpt->insert($table,$postData);
						  
						   return $result;
     }





     
public function newfngetyear($prog)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect="select `year`as `key` , `year` as `value` from tbl_newscheduler where idnewscheduler in (Select idnewscheduler from tbl_newschedulercourse where idprogrammaster=$prog) and active=1 group by `year`";
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
}

public function newfnGetCitylistforcourse($idprog,$idyear,$curmonth){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array())
										   ->join(array("k"=>"tbl_venuedateschedule"),'a.idnewscheduler=k.idnewscheduler')
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("CAST(a.Year AS SIGNED)  = ?",$idyear)
										  ->where("CAST(a.To AS SIGNED)>=?",$curmonth)
										  ->where("a.Active  = 1")
										  ->group("d.idcenter");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	
public function fnnewmonthcaleshowlatest($prog,$idvenue,$year)
	{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
		 $lstrSelect = "SELECT a.*,b.*
	FROM tbl_newscheduler as a,tbl_newschedulercourse  as b,tbl_newschedulercourse as c
	where a.idnewscheduler=b.idnewscheduler
	and a.idnewscheduler=c.idnewscheduler
	and a.Active=1
	and b.IdProgramMaster=$prog
	and a.idnewscheduler in (SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
	and a.Year=$year";
		  	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	  	return $larrResult;
	}
	
	
	
	
public function fnnewmonths($values)
{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
 		$lstrSelect = "SELECT min(`From`)as minimum,max(`To`) as maximum FROM tbl_newscheduler where idnewscheduler in ($values)";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}


public function fnnewmonthsrange($frommonth,$tomonth)
{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
  $lstrSelect = "Select a.idmonth as `key`,a.MonthName as `value`
from tbl_newmonths  as a
where a.idmonth>=$frommonth and a.idmonth<=$tomonth";	
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}


	public	function fnvalidateseats($venue,$selecteddate)
	{
	  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  	$select = "SELECT Allotedseats,Totalcapacity from  tbl_venuedateschedule where idvenue =$venue and date ='$selecteddate'";
		return	$result = $lobjDbAdpt->fetchAll($select);
	}
	
	
	public function  fnGetvenuedatescheduleDetails($regdate,$city) 
	{
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				   $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("a.date","a.Allotedseats","a.idsession","a.Totalcapacity"))
										  ->join(array("b"=>"tbl_managesession"),'a.idsession=b.idmangesession',array("b.managesessionname","b.starttime","b.endtime"))
                                          ->where("a.Active = 1")
                                          ->where("b.Active = 1")
                                          ->where("a.idvenue=?",$city)
                                          ->where("a.date =  ?",$regdate);                    
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
				
       
       }
public function fngetstatecity($venue)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a"=>"tbl_center"),array("a.*"))
										  ->where("a.idcenter=?",$venue);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;				
}
       

  
}