<?php
class App_Model_Takafullogin extends Zend_Db_Table {
	//protected $_name = 'tbl_batchmaster'; // table name
public  function fngetlogincheck($username,$password)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_takafuloperator"),array("a.*" ))
					 				->where("a.LoginId= ?",$username)
					 				->where("a.Password = ?",$password);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
function fnGenerateCode($idtakafuloperator,$Idunique){	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $db->select()
				->  from('tbl_config')
				->	where('idUniversity  = ?',1);				 
		$result = 	$db->fetchRow($select);		
		$sepr	=	$result['Sturegtext'];
		$str	=	"SturegField";
		for($i=1;$i<=3;$i++){
			$check = $result[$str.$i];
			switch ($check){
				case 'year':
				  $code	= date('dMy');
				  break;
				case 'uniqueid':
				  $code	= $Idunique;
				  break;
				case 'takaful':
					 $select =  $db->select()
					 		 -> from('tbl_takafuloperator')
					 		 ->	where('idtakafuloperator  = ?',$idtakafuloperator);  				 
					$resultCollage = $db->fetchRow($select);		
					$code		   = $resultCollage['TakafulShortName'];
				  break;
				default:
				  break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}	
		return $accCode;	
	 	/*$data = array('InvoiceNo' => $accCode);
		$where['IdInvoice = ? ']= $Idunique;		
		return $db->update('tbl_sfsinvoicemaster', $data, $where);	*/		
	} 

}
