<?php
class App_Model_Autosubmitbycenter extends Zend_Db_Table {
	
	
public function fnGetStudentdetails($idcenter,$examsession,$curdate)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.*"))
										   ->join(array("b" => "tbl_managesession"),'a.Examsession=b.idmangesession',array("b.*"))
										    ->join(array("c" => "tbl_registereddetails"),'a.IDApplication=c.IDapplication',array("c.*"))
										    ->where("a.payment=1")
										    ->where("a.Examdate=?",$curdate)
										   ->where("a.pass  = 3")
										  	->where("c.Cetreapproval = 1")
										  ->where("a.Examsession=?",$examsession)
										  ->where("a.Examvenue  = ?",$idcenter);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
public function fnchecksubmitted($idapplication)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentmarks"),array("a.*"))
										  ->where("a.IDApplication  = ?",$idapplication);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	
}

  public function fnUpdatecenterstart($idcenter,$idsessions,$currdate,$autosubmission)
     {
     	$db = Zend_Db_Table::getDefaultAdapter();
     	if($autosubmission == 1) {
     		$data = array('AutoSubmitCloseTime' => date('H:i:s'));
     	} elseif($autosubmission == 0) {
     		$data = array('CloseTime' => date('H:i:s'));
     	}
     		
		$where['idcenter = ?']= $idcenter;	
		$where['idSession = ?']= $idsessions;	
		$where['ExamDate = ?']= $currdate;		
		$db->update('tbl_centerstartexam', $data, $where);	
     }
     
public function fnupdatestudentexamdetails($studentid,$endtime,$submittedby)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Endtime'] = $endtime;
    	$larrformData1['Submittedby'] = $submittedby;	
		 $where = "IDApplication = '".$studentid."'"; 	
		 $db->update('tbl_studentstartexamdetails',$larrformData1,$where);
    }     

public function updatestudentapplication($idapplication,$result)
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$data['Pass'] = $result;
		$where['IDApplication = ?']= $idapplication;	
		$lobjDbAdpt->update('tbl_studentapplication', $data, $where);	
	
}
    
}
