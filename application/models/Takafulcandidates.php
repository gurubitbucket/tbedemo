<?php
class App_Model_Takafulcandidates extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
	


	public function fngeticnosbyid($idapplications)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.ICNO","a.IDApplication","a.Fname as Name"))
										    ->where("a.IDApplication  in ($idapplications)")
										     ->group("a.icno");
										     
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				//echo $lstrSelect;die();
				return $larrResult;
		
	}

	public function fngetvalidateprgbyicno($prerequest,$icno)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.ICNO"))
										    ->where("a.ICNO in ($icno)")
										     ->where("a.Program =?",$prerequest)
										     ->where("a.Pass =1");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
//echo $lstrSelect ;die();
				//echo $lstrSelect;die();
				return $larrResult;
		
		
	}





	public function fnupdatestudentcount($ICNO,$lastid){

		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_studentapplication"),array('max(PermCity) as maxcount'))
							 				 ->where("ICNO = ?",$ICNO);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			
		if($larrResult['maxcount']==""){
				$larrResult['maxcount']=0;
			}
			
				 $parmcity= $larrResult['maxcount'] + 1;
				 $where = 'IDApplication = '.$lastid;
			     $postData = array(		
						   	 'PermCity' =>$parmcity,			    																
						);
				$table = "tbl_studentapplication";
	            $lobjDbAdpt->update($table,$postData,$where);			
	
	}
	
	
	
	 public function fngetstudentcounttempexcel($regpin){ // newly added as on 21-02-2013
	 	
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_tempexcelcandidates"),array('count(distinct idcandidates) as totalcount'))
							 				 ->where("RegistrationPin = ?",$regpin);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	 	
	 }
	
	 public function fngetBatchRegistration($idbatch)
		 {
		 	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_batchregistrationdetails"),array("a.idBatchRegistrationDetails","a.idBatchRegistration","a.idProgram","a.eachAmount","a.noofCandidates","a.toatlAmount","a.registrationPin","a.discount","a.UpdUser","a.UpdDate"))
							 				 ->join(array("b"=>"tbl_programmaster"),'a.idProgram= b.IdProgrammaster')
							 				 ->where("a.idBatchRegistration = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		 }
		 
	 public function fngetnumberappliedexcel($idpinnum){
	 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
							 		 ->from(array("tbl_tempexcelcandidates"),array("idcandidates"))							 				 
							 		 ->where("RegistrationPin = ?",$idpinnum);
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }
	
	 
	 public function fngetTakafulDetails($IdCompany) { //Function to get the user details	 	 
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
								 	 ->from(array("a"=>"tbl_takafuloperator"),array("a.idtakafuloperator"))								
								     ->where("a.idtakafuloperator=?",$IdCompany);								 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
     
     public function fngetnoofstudentsfromexcel($ids,$idtakaful)
     { // Get remaining number of students in a batch
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_tempexcelcandidates"),array("a.idcandidates","a.StudentName","a.ICNO","a.DOB","a.Race","a.Takafuloperator","a.idprogram","a.email","a.education","a.Gender","a.Address","a.session","a.ContactNo","a.MobileNo"))							 				
							 				 ->where("a.RegistrationPin = ?",$ids)
							 				 ->where("a.ICNO not in (select ICNO from tbl_studentapplication where Takafuloperator = $idtakaful)");
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }
     
     public function fngetexcelappliedcandidates($ids,$idtakaful)
     { // Get excel applied candidates details     	
     	 			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_studentapplication"),array("a.FName","a.ICNO","a.DateTime"))	
							 				  ->join(array("e"=>"tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid")) 
							 				 ->join(array("b"=>"tbl_programmaster"),'a.Program = b.IdProgrammaster',array("b.ProgramName")) 
							 				 ->join(array("c"=>"tbl_managesession"),'a.Examsession = c.idmangesession',array("c.managesessionname")) 	
							 				 ->join(array("d"=>"tbl_center"),'a.Examvenue = d.idcenter',array("d.centername")) 						 				
							 				 ->where("a.Takafuloperator = ?",$idtakaful)
							 				 ->where("e.RegistrationPin = ?",$ids);							 				 
							 				 //->where("a.ICNO in (select ICNO from tbl_tempexcelcandidates where RegistrationPin = '$ids' )");
							 				 //echo $lstrSelect;				
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;       	
     	
     }
     
	public function fngetBatchRegistrationimport($idbatch)
	{   // Getting all batchreg details
		 		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			     $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_batchregistration"),array("a.idBatchRegistration","a.idCompany","a.totalNoofCandidates","a.totalAmount","a.registrationPin","a.paymentStatus","a.UpdUser","a.UpdDate","a.AdhocDate","a.AdhocVenue","a.Approved"))
							 				 ->join(array("p"=>"tbl_batchregistrationdetails"),'a.idBatchRegistration = p.idBatchRegistration')
							 				 ->join(array("b"=>"tbl_programmaster"),'p.idProgram= b.IdProgrammaster')
							 				 ->where("a.registrationPin = ?",$idbatch);
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
	}
		 
	public function fninsertintotemp($larrdatainsert,$idtakaful,$idregpin) 
	{  // Function to enter into tbl_tempexcelcandidates
		
			$sessionID = Zend_Session::getId();
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_tempexcelcandidates";
		    $postData = array(		
							'StudentName' =>$larrdatainsert['StudentName'],	
           					'ICNO' =>$larrdatainsert['ICNO'],	
           					'Race' =>$larrdatainsert['Race'], 
							'Takafuloperator' =>$idtakaful,	
           					'idprogram' =>$larrdatainsert['idprogram'],	
           					'email' =>$larrdatainsert['email'], 
           					'education' =>$larrdatainsert['education'],	
           					'Gender' =>$larrdatainsert['Gender'], 
		                    'DOB'=>$larrdatainsert['DOB'],
		    				'session'=>$sessionID,
							'Address' =>$larrdatainsert['Address'],
		    				'RegistrationPin'=>$idregpin,
		    				'CorrespAddress'=>$larrdatainsert['CorrespAddress'],
		    				'PostalCode'=>$larrdatainsert['PostalCode'],
		    				'IdCountry'=>$larrdatainsert['IdCountry'],
		    				'IdState'=>$larrdatainsert['IdState'],	
		    				'ContactNo'=>$larrdatainsert['ContactNo'],
		    				'MobileNo'=>$larrdatainsert['MobileNo'],		    				
		    );
		      $db->insert($table,$postData);
		    
	}
		 
	 public function fninserttoimported($postData){// Inserting into tbl_imported
	 		$db = Zend_Db_Table::getDefaultAdapter();
	 		$postData ['Upddate'] = date ( "Y-m-d H:i:s" );
            $table = "tbl_imported";
	 		$db->insert($table,$postData);	 	
	 }
	 
	 public function fngetBatchDetailsforRegistrationpin($idpin)
	 {
	 	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration"),array('idCompany','registrationPin'))
										  ->where("idBatchRegistration  = ?",$idpin);	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	 }
		 
		 public function  fnAdhocvenue($ids)
		 {
		 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchregistration"),array("a.idBatchRegistration","a.idCompany","a.totalNoofCandidates","a.totalAmount","a.registrationPin","a.paymentStatus","a.UpdUser","a.UpdDate","a.AdhocDate","a.AdhocVenue","a.Approved"))
										  ->where("a.idBatchRegistration  = ?",$ids);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
		 }
		 
		 public function fnGetProgramName()
		 {
		 	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_programmaster',array('key' =>'IdProgrammaster','value' => 'ProgramName'));
			$result = $db->fetchAll($sql);
			return $result;
		 }
		 
		 
		 public function fnGetBatchName()
		 {
		 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 			
 				$lstrSelect="SELECT `a`.`IdBatch` AS `key`, CONCAT(DATE_FORMAT(`a`.`BatchFrom`,'%d-%m-%Y'),'---',DATE_FORMAT(`a`.`BatchTo`,'%d-%m-%Y')) AS `value` FROM `tbl_batchmaster` AS `a`";			
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		 }
		 
  /*
   * function to fetch all the scheduler based on the prog
   */
 public function fnGetscheduler($idbatch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("key"=>"a.idschedulermaster","value"=>("a.ScheduleName")))
										  ->where("a.idBatch  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
   /*
  * funtion to fetch all the venue based on the batch
  */			
  public function fnGetVenueName($lintidscheduler)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"b.idschedulervenue","value"=>"a.centername"))
										  ->join(array("b" =>"tbl_schedulervenue"),'a.idcenter = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulermaster"),'b.idschedulermaster = c.idschedulermaster',array())
										  ->where("c.idschedulermaster  = ?",$lintidscheduler)
										  ->order("a.city");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  
  public function fnGetVenueTime($idvenue)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
/* 	echo 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("key"=>"a.idschedulervenuetime","value"=>("distinct(a.Date) as Date")))
										 // ->join(array("b" =>"tbl_center"),'a.idcentre = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulervenue"),'a.idschedulervenue = c.idschedulervenue',array())
										  ->where("c.idschedulervenue  = ?",$idvenue);die();*/
 	$lstrSelect="SELECT (a.Date)as `key`,(a.Date)as value FROM `tbl_schedulervenuetime` AS `a`
    INNER JOIN `tbl_schedulervenue` AS `c` ON a.idschedulervenue = c.idschedulervenue WHERE (c.idschedulervenue  = $idvenue)group by value";
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
	/*
	 * 
	 */
	public function fnGetTimingsForDate($date,$idscheduler)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	$lstrSelect="SELECT idschedulervenuetime as `key`,`From` as value FROM `tbl_schedulervenuetime` WHERE `idschedulervenue`=$idscheduler and Date='$date'";
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	

	public function fnTakafuloperator($idcomps){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"))
										  ->where("a.idtakafuloperator=?",$idcomps);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnBatchProg()
	{
		        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT IdBatch,BatchName,BatchFrom,BatchTo,BatchStatus,UpdDate,UpdUser,IdProgrammaster FROM tbl_batchmaster where idProgrammaster in (select IdProgrammaster from tbl_programmaster) order by idProgrammaster";	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	public function fngettakafuladdressdetails($idcomps)
     {  // get takaful address
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_takafuloperator"),array('paddr1'))
										  ->where("idtakafuloperator  = ?",$idcomps);	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
     }
	
	public function fnInsertIntoStd($larrformData,$countloop,$batchid,$regpin,$companyaddress)
	{ 
		 // Insert into tbl_studentapplication manual entry  #$
		 
		 if(strlen($larrformData['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformData['setmonth'];
          } else {
          	$monthsss = $larrformData['setmonth'];
          }          
		  if(strlen($larrformData['setdate']) <= 1) {
          	$dayssss = '0'.$larrformData['setdate'];
          } else {
         	$dayssss = $larrformData['setdate'];
          }
		
		for($insertstudapp=0;$insertstudapp<$countloop;$insertstudapp++)
		{
          $randarray=$this->fngeneraterandompassword();
		  $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_studentapplication";
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['candidatename'][$insertstudapp],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['candidatedateofbirth'][$insertstudapp],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateemail'][$insertstudapp],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>1,	
		                    'username' =>$larrformData['candidateicno'][$insertstudapp],	
                            'password' =>$randarray,
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateicno'][$insertstudapp],	
           					'Payment' => 1, 
							'DateTime' =>$larrformData['Year']."-".$monthsss."-".$dayssss,	
           					'PermAddressDetails' =>$companyaddress,	
           					'Takafuloperator' =>$larrformData['candidatetakaful'], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' =>2, 
							'Gender' =>$larrformData['candidategender'][$insertstudapp],	
           					'Race' =>$larrformData['candidaterace'][$insertstudapp],	
		                    'Religion' =>$larrformData['candidatereligion'][$insertstudapp],	
           					'Qualification' =>$larrformData['candidateeducation'][$insertstudapp], 
							'State' =>$larrformData['candidatestate'][$insertstudapp],	
           					'CorrAddress' =>$larrformData['candidateaddress'][$insertstudapp],	
           					'PostalCode' =>$larrformData['postalcode'][$insertstudapp], 
		 					'ContactNo' =>$larrformData['candidatenum'][$insertstudapp],	
           					'MobileNo' =>$larrformData['candidatemobnum'][$insertstudapp], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  					'Year'=>$larrformData['scheduler'], 
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  					'Examvenue'=>$larrformData['NewVenue'],	
		  					'Examsession'=>$larrformData['idsession'],
		                    'pass'=>3	
						);			
						
	        $db->insert($table,$postData);	       
	       $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');	        

		 $larrdatainsert=array(
	        					'StudentName' =>$larrformData['candidatename'][$insertstudapp],	
           						'ICNO' =>$larrformData['candidateicno'][$insertstudapp],	
           						'Race' =>$larrformData['candidaterace'][$insertstudapp], 								
           						'email' =>$larrformData['candidateemail'][$insertstudapp], 
           						'education' =>$larrformData['candidateeducation'][$insertstudapp],	
           						'Gender' =>$larrformData['candidategender'][$insertstudapp], 
 								'idprogram'=>$larrformData['Program'],
		                  		'DOB'=>$larrformData['candidatedateofbirth'][$insertstudapp],		    											
	        					'Address' =>$larrformData['candidateaddress'][$insertstudapp],		    					
		    					'CorrespAddress'=>$larrformData['candidateaddress'][$insertstudapp],
		    					'PostalCode'=>$larrformData['postalcode'][$insertstudapp],
		    					'IdCountry'=>0,
		    					'IdState'=>$larrformData['candidatestate'][$insertstudapp],	
		    					'ContactNo'=>$larrformData['candidatenum'][$insertstudapp],
		    					'MobileNo'=>$larrformData['candidatemobnum'][$insertstudapp],
	        
	        );
	        
	        
	        
			//inserting into temp registration excel table    18-06-2012
	        self::fninsertintotemp($larrdatainsert,$larrformData['candidatetakaful'],$regpin);
	        self::fnupdatestudentcount($larrformData['candidateicno'][$insertstudapp],$lastid);
	        $larrresult = self::fnviewstudentdetails($lastid);        
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($larrformData['candidatetakaful'],$lastid);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['setactive'],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>1,
            				'Cetreapproval'=>0,
            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);
	       	       
	      self::sendmails($larrresult,$regid);
	       
		}
		
	 $larrresultbatchcount=self::fngettotalcountinbatchregistration($regpin);
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_registereddetails',array('count(idregistereddetails) as totapplied'))
								->where('RegistrationPin =?',$regpin);
			$resulttc = $db->fetchRow($sql);
			
		if($resulttc['totapplied'] < $larrresultbatchcount['totalNoofCandidates']){
		     $paymentstatus=1;
		}else{
			 $paymentstatus=2;
			 self::fndeletefromtempexcelcandidates($regpin);	  //new function to delete redundant data from tempescelcandidates
		}
		
			     $where = 'idBatchRegistration = '.$batchid;
			     $postData = array(		
						   	 'paymentStatus' =>$paymentstatus,
			    			 'Approved' => 0															
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	          
	             //updating table tbl_venuedateschedule
	             self::fnupdatevenuedateschedule($larrformData,2);
	}
	
	
	
	///////////company Insertion
	
	
	public function fnInsertIntoStdCompany($larrformData,$countloop,$batchid,$regpin,$companyaddress)
	{ 
		//exit;
		 // Insert into tbl_studentapplication manual entry  #$
		 if(strlen($larrformData['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformData['setmonth'];
          } else {
          	$monthsss = $larrformData['setmonth'];
          }
          
          
		  if(strlen($larrformData['setdate']) <= 1) {
          	$dayssss = '0'.$larrformData['setdate'];
          } else {
         	$dayssss = $larrformData['setdate'];
          }
		
		
		
		for($insertstudapp=0;$insertstudapp<$countloop;$insertstudapp++)
		{

 		  $randarray=$this->fngeneraterandompassword();
		  $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_studentapplication";
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['candidatename'][$insertstudapp],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['candidatedateofbirth'][$insertstudapp],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateemail'][$insertstudapp],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>1,	
		                    'username' =>$larrformData['candidateicno'][$insertstudapp],	
                            'password' =>$randarray,
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateicno'][$insertstudapp],	
           					'Payment' => 1, 
							'DateTime' =>$larrformData['Year']."-".$monthsss."-".$dayssss,	
           					'PermAddressDetails' =>$companyaddress,	
           					'Takafuloperator' =>$larrformData['candidatetakaful'], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' =>1, 
							'Gender' =>$larrformData['candidategender'][$insertstudapp],	
           					'Race' =>$larrformData['candidaterace'][$insertstudapp],	
		                    'Religion' =>$larrformData['candidatereligion'][$insertstudapp],	
           					'Qualification' =>$larrformData['candidateeducation'][$insertstudapp], 
							'State' =>$larrformData['candidatestate'][$insertstudapp],	
           					'CorrAddress' =>$larrformData['candidateaddress'][$insertstudapp],	
           					'PostalCode' =>$larrformData['postalcode'][$insertstudapp], 
		 					'ContactNo' =>$larrformData['candidatenum'][$insertstudapp],	
           					'MobileNo' =>$larrformData['candidatemobnum'][$insertstudapp], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  					'Year'=>$larrformData['scheduler'],
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  					'Examvenue'=>$larrformData['NewVenue'],	
		  					'Examsession'=>$larrformData['idsession'],
		                    'pass'=>3	
						);			
						
	        $db->insert($table,$postData);	       
	       $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');	   

  			$larrdatainsert=array(
	        					'StudentName' =>$larrformData['candidatename'][$insertstudapp],	
           						'ICNO' =>$larrformData['candidateicno'][$insertstudapp],	
           						'Race' =>$larrformData['candidaterace'][$insertstudapp], 								
           						'email' =>$larrformData['candidateemail'][$insertstudapp], 
           						'education' =>$larrformData['candidateeducation'][$insertstudapp],	
  								'idprogram'=>$larrformData['Program'],
           						'Gender' =>$larrformData['candidategender'][$insertstudapp], 
		                  		'DOB'=>$larrformData['candidatedateofbirth'][$insertstudapp],		    											
	        					'Address' =>$larrformData['candidateaddress'][$insertstudapp],		    					
		    					'CorrespAddress'=>$larrformData['candidateaddress'][$insertstudapp],
		    					'PostalCode'=>$larrformData['postalcode'][$insertstudapp],
		    					'IdCountry'=>0,
		    					'IdState'=>$larrformData['candidatestate'][$insertstudapp],	
		    					'ContactNo'=>$larrformData['candidatenum'][$insertstudapp],
		    					'MobileNo'=>$larrformData['candidatemobnum'][$insertstudapp],
	        
	        );
	        
	        
	        
			//inserting into temp registration excel table    18-06-2012
	        self::fninsertintotemp($larrdatainsert,0,$regpin);
     	    self::fnupdatestudentcount($larrformData['candidateicno'][$insertstudapp],$lastid);
	        $larrresult = self::fnviewstudentdetails($lastid);        
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($larrformData['candidatetakaful'],$lastid);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['setactive'],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>1,
            				'Cetreapproval'=>0,
            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);
	       	       
	     // self::sendmails($larrresult,$regid);
	       
		}
		
		    $larrresultbatchcount=self::fngettotalcountinbatchregistration($regpin);		
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_registereddetails',array('count(idregistereddetails) as totapplied'))
								->where('RegistrationPin =?',$regpin);
			$resulttc = $db->fetchRow($sql);
			
			
		//echo $resulttc['totapplied'];exit;
		if($resulttc['totapplied'] < $larrresultbatchcount['totalNoofCandidates']){
		     $paymentstatus=1;
		}else{
			 $paymentstatus=2;
			  self::fndeletefromtempexcelcandidates($regpin);	  //new function to delete redundant data from tempescelcandidates
		} 
			     $where = 'idBatchRegistration = '.$batchid;			     
				 $table = "tbl_batchregistration";
				 $postData = array(		
						   	 'paymentStatus' =>$paymentstatus,
			    			 'Approved' => 0															
						);
				
	             $db->update($table,$postData,$where);
	             //updating table tbl_venuedateschedule
	             self::fnupdatevenuedateschedule($larrformData,2);
	}
	
	public function fngettotalcountinbatchregistration($regpin){
		
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration"),array("totalNoofCandidates"))
										  ->where("registrationPin = ?",$regpin);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
	
	
	public function fngetavailseatvenue($idvenue,$availdate,$idsession){
		
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_venuedateschedule"),array("(Totalcapacity-Allotedseats) as availseat","idnewscheduler"))
										  ->where("idvenue = ?",$idvenue)
										  ->where("date = ?",$availdate)
										  ->where("idsession=?",$idsession)
										->where("Active=1");	
										  
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
	public function fnUpdatestudapplication($larrformData, $lintidbatch, $linttotnumofapplicant ,$Idregpinnum){// Updation process for reregistration of failed candidates
				
 	if(strlen($larrformData['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformData['setmonth'];
          } else {
          	$monthsss = $larrformData['setmonth'];
          }
          
          
		  if(strlen($larrformData['setdate']) <= 1) {
          	$dayssss = '0'.$larrformData['setdate'];
          } else {
         	$dayssss = $larrformData['setdate'];
          }
					$avalus = array_values($larrformData['studentId']);	
					$avaluesICNO = array_values($larrformData['ICNO']);
				echo "<pre>";
//print_R($larrformData);exit;

				
				 $db = Zend_Db_Table::getDefaultAdapter();
				 for($updstuapp=0;$updstuapp<count($avalus);$updstuapp++){
				 		 $where = 'IDApplication = '.$avalus[$updstuapp];
					     $postData = array(		
					     			 'IdBatch' =>$larrformData['setactive'],	
					     			 'DateTime' =>$larrformData['Year']."-".$monthsss."-".$dayssss,	
								   	 'Program' => $larrformData['Program'],
					     			 'ExamState'=>$larrformData['NewState'],
		  							 'ExamCity'=>$larrformData['NewCity'],
		  					  		 'Year'=>$larrformData['scheduler'],
		  					  		 'Examdate'=>$larrformData['setdate'],
		  							 'Exammonth'=>$larrformData['setmonth'],
		  							 'Examvenue'=>$larrformData['NewVenue'],	
		  							 'Examsession'=>$larrformData['idsession'],
					     			 'pass'=>3				    																
								);
						 $table = "tbl_studentapplication";
						 $db->update($table,$postData,$where);
						 
						 //Update tbl_registereddetails						 
						$tbl_registereddetails = "tbl_registereddetails";
            			$postDatasreg = array(	
            				'IdBatch' =>$larrformData['setactive'],	
            				'Approved' =>1,
            				'Cetreapproval'=>0,
            				);			
            			 $wherereg = 'IDApplication = '.$avalus[$updstuapp];
	       				 $db->update($tbl_registereddetails,$postDatasreg,$wherereg);
			            //////////////////
			             $larrresult = self::fnviewstudentdetails($avalus[$updstuapp]);    
			             self::sendmails($larrresult,$larrformData['regId'][$updstuapp]);	
			             
			             self::fnupdatestudentcount($avaluesICNO[$updstuapp],$avalus[$updstuapp]);

			             
				 }
				 
		        $larrresultcount=self::fnGetreregisteredcount($Idregpinnum);		 	
				
				if($larrresultcount['totalreregisterd']==0){
					$payststus=2;
					// self::fndeletefromtempexcelcandidates($regpin);	  //new function to delete redundant data from tempescelcandidates
				}else{
					$payststus=1;
				}
				
				 $wherebatch = 'idBatchRegistration = '.$lintidbatch;
			     $postDatabatch = array(		
						   	 'paymentStatus' => $payststus,
			    			 'Approved' => 1															
						);
				$tablebatch = "tbl_batchregistration";
	            $db->update($tablebatch,$postDatabatch,$wherebatch);
	            
	             //updating table tbl_venuedateschedule
	             self::fnupdatevenuedateschedule($larrformData,3);
				 
	}
	
	public function fnGetreregisteredcount($Idregpinnum){
		
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_registereddetails"),array("count(idregistereddetails) as totalreregisterd"))
										  ->where("RegistrationPin = ?",$Idregpinnum)
										  ->where("IdBatch = 0");	
										  
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	}
	
	public function fnGetmailId($mailID)
	{
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_tempexcelcandidates"),array("a.email"))
										  ->where('a.email = ?',$mailID);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	
	public function fnGetIcno($icno,$idprog){
		
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_tempexcelcandidates"),array("a.ICNO"))
										->where('a.idprogram=?',$idprog)
										  ->where('a.ICNO = ?',$icno);	
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		
	}
	
	public function fnInsertintostudapplicationexcelCompany($larrformData,$lintidbatch,$linttotnumofapplicant,$regpin){
      	// Scheduling from company login#$
		
		 if(strlen($larrformData['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformData['setmonth'];
          } else {
          	$monthsss = $larrformData['setmonth'];
          }
          
          
		  if(strlen($larrformData['setdate']) <= 1) {
          	$dayssss = '0'.$larrformData['setdate'];
          } else {
         	$dayssss = $larrformData['setdate'];
          }
       
          
      	$avalus = array_values($larrformData['studenttakful']);		
      	
		for($insertfrmexcel=0;$insertfrmexcel<count($avalus);$insertfrmexcel++)
		{		
			
			/*
			 * print_r($larrformData['Takafuloperator'][$avalus[$insertfrmexcel]]);
			exit;*/
			
 		  $randarray=$this->fngeneraterandompassword();
		  $dob=date("Y-m-d",strtotime($larrformData['candidateDOBtakaful'][$larrformData['studenttakful'][$insertfrmexcel]]));
		  $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_studentapplication";
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['candidatenametakaful'][$avalus[$insertfrmexcel]],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$dob,	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateEMAILtakaful'][$avalus[$insertfrmexcel]],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>1,	
		                    'username' =>$larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],	
                            'password' =>$randarray,
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],	
           					'Payment' => 1, 
							'DateTime' =>$larrformData['Year']."-".$monthsss."-".$dayssss,
           					'PermAddressDetails' =>$larrformData['Address'][$avalus[$insertfrmexcel]],	
           					'Takafuloperator' =>$larrformData['Takafuloperator'][$avalus[$insertfrmexcel]], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 1, 
							'Gender' =>$larrformData['Gender'][$avalus[$insertfrmexcel]],	
           					'Race' =>$larrformData['Race'][$avalus[$insertfrmexcel]],	
		                    'Religion' =>144,	
           					'Qualification' =>$larrformData['education'][$avalus[$insertfrmexcel]], 
							'State' =>$larrformData['IdState'][$avalus[$insertfrmexcel]],	
           					'CorrAddress' =>$larrformData['CorrespAddress'][$avalus[$insertfrmexcel]],	
           					'PostalCode' =>$larrformData['PostalCode'][$avalus[$insertfrmexcel]], 
		 					'ContactNo' =>$larrformData['ContactNo'][$avalus[$insertfrmexcel]],	
           					'MobileNo' =>$larrformData['MobileNo'][$avalus[$insertfrmexcel]], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  					'Year'=>$larrformData['scheduler'], 
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  					'Examvenue'=>$larrformData['Examvenue'],	
		  					'Examsession'=>$larrformData['idsession'],
		                    'pass'=>3		  
						);	
							
	        $db->insert($table,$postData);
	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');	        
	        
	        $larrresult = self::fnviewstudentdetails($lastid);	        
	        self::fnupdatestudentcount($larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],$lastid);
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($larrformData['Takafuloperator'][$avalus[$insertfrmexcel]],$lastid);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['setactive'],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>1,
            				'Cetreapproval'=>0,
            				'RegistrationPin'=>$larrformData['idbatch']
            				);	
            						
	       $db->insert($tables,$postDatas);       
	       self::sendmails($larrresult,$regid);	       
	       
	       self::fndeletefromtempexcelcandidates($avalus[$insertfrmexcel]);	  //new function to delete redundant data from tempescelcandidates added on 1-3-2013
		}		
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_registereddetails',array('count(idregistereddetails) as totapplied'))
								->where('RegistrationPin =?',$regpin);
			$resulttc = $db->fetchRow($sql);
			
		//echo $resulttc['totapplied'];exit;
		if($resulttc['totapplied'] < $linttotnumofapplicant){
		     $paymentstatus=1;
		}else{
			 $paymentstatus=2;
			//  self::fndeletefromtempexcelcandidates($regpin);	  //new function to delete redundant data from tempescelcandidates
		}
		
			     $where = 'idBatchRegistration = '.$lintidbatch;
			     $postData = array(		
						   	 'paymentStatus' =>$paymentstatus,
			    			 'Approved' => 0															
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	            //updating table tbl_venuedateschedule
	             self::fnupdatevenuedateschedule($larrformData,1);
      	
      }
	
	
	
	public function fnInsertintostudapplicationexcel($larrformData,$lintidbatch,$linttotnumofapplicant,$regpin){
		// Scheduling from takaful login#$
		  if(strlen($larrformData['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformData['setmonth'];
          } else {
          	$monthsss = $larrformData['setmonth'];
          }
          
		  if(strlen($larrformData['setdate']) <= 1) {
          	$dayssss = '0'.$larrformData['setdate'];
          } else {
         	$dayssss = $larrformData['setdate'];
          }
          
		$avalus = array_values($larrformData['studenttakful']);		
		for($insertfrmexcel=0;$insertfrmexcel<count($avalus);$insertfrmexcel++)
		{		
  			 $randarray=$this->fngeneraterandompassword();
		  $dob=date("Y-m-d",strtotime($larrformData['candidateDOBtakaful'][$larrformData['studenttakful'][$insertfrmexcel]]));
		  $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_studentapplication";
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['candidatenametakaful'][$avalus[$insertfrmexcel]],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$dob,	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateEMAILtakaful'][$avalus[$insertfrmexcel]],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>1,	
		                    'username' =>$larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],	
                            'password' => $randarray,
           					'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],	
           					'Payment' => 1, 
							'DateTime' =>$larrformData['Year']."-".$monthsss."-".$dayssss,	
           					'PermAddressDetails' =>$larrformData['Address'][$avalus[$insertfrmexcel]],	
           					'Takafuloperator' =>$larrformData['Takafuloperator'][$avalus[$insertfrmexcel]], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 2, 
							'Gender' =>$larrformData['Gender'][$avalus[$insertfrmexcel]],	
           					'Race' =>$larrformData['Race'][$avalus[$insertfrmexcel]],	
		                    'Religion' =>144,	
           					'Qualification' =>$larrformData['education'][$avalus[$insertfrmexcel]], 
							'State' =>$larrformData['IdState'][$avalus[$insertfrmexcel]],	
           					'CorrAddress' =>$larrformData['CorrespAddress'][$avalus[$insertfrmexcel]],	
           					'PostalCode' =>$larrformData['PostalCode'][$avalus[$insertfrmexcel]], 
		 					'ContactNo' =>$larrformData['ContactNo'][$avalus[$insertfrmexcel]],	
           					'MobileNo' =>$larrformData['MobileNo'][$avalus[$insertfrmexcel]], 
		  					'ExamState'=>$larrformData['NewState'],
		  					'ExamCity'=>$larrformData['NewCity'],
		  					'Year'=>$larrformData['scheduler'], 
		  					'Examdate'=>$larrformData['setdate'],
		  					'Exammonth'=>$larrformData['setmonth'],
		  					'Examvenue'=>$larrformData['Examvenue'],	
		  					'Examsession'=>$larrformData['idsession'],
		                    'pass'=>3		  
						);	
							
	        $db->insert($table,$postData);
	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');	        
	        
	        $larrresult = self::fnviewstudentdetails($lastid);	        
	        self::fnupdatestudentcount($larrformData['candidateICNOtakaful'][$avalus[$insertfrmexcel]],$lastid); 
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($larrformData['Takafuloperator'][$avalus[$insertfrmexcel]],$lastid);
	        
            $tables = "tbl_registereddetails";
            
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['setactive'],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>1,
            				'Cetreapproval'=>0,
            				'RegistrationPin'=>$larrformData['idbatch']
            				);	
            						
	       $db->insert($tables,$postDatas);       
	       self::sendmails($larrresult,$regid);	   
	       self::fndeletefromtempexcelcandidates($avalus[$insertfrmexcel]);	  //new function to delete redundant data from tempescelcandidates added on 1-3-2013    
		}		
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_registereddetails',array('count(idregistereddetails) as totapplied'))
								->where('RegistrationPin =?',$regpin);
			$resulttc = $db->fetchRow($sql);
			
		//echo $resulttc['totapplied'];exit;
		if($resulttc['totapplied'] < $linttotnumofapplicant){
		     $paymentstatus=1;
		}else{
			 $paymentstatus=2;
			  //self::fndeletefromtempexcelcandidates($regpin);	  //new function to delete redundant data from tempescelcandidates
		}
		
			     $where = 'idBatchRegistration = '.$lintidbatch;
			     $postData = array(		
						   	 'paymentStatus' =>$paymentstatus,
			    			 'Approved' => 1															
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	            
	            
	            //updating table tbl_venuedateschedule
	             self::fnupdatevenuedateschedule($larrformData,1);
	            
		
	}
	
	public function fnupdatevenuedateschedule($larrformdata,$flag){		
	
		
		if($flag==1){		
				$studcount=count($larrformdata['studenttakful']);
				$idvenue=$larrformdata['NewVenue'];
		}else if($flag==2){
				$studcount=count($larrformdata['candidatename']);
				$idvenue=$larrformdata['NewVenue'];
		}else if($flag==3){
				$studcount=count($larrformdata['studentId']);
				$idvenue=$larrformdata['NewVenue'];
		}
				
		//echo $studcount;exit;
		 if(strlen($larrformdata['setmonth']) <= 1) {
          	$monthsss = '0'.$larrformdata['setmonth'];
          } else {
          	$monthsss = $larrformdata['setmonth'];
          }
          
          
		  if(strlen($larrformdata['setdate']) <= 1) {
          	$dayssss = '0'.$larrformdata['setdate'];
          } else {
         	$dayssss = $larrformdata['setdate'];
          }
          

				$availdate=$larrformdata['Year']."-".$monthsss."-".$dayssss;
				$examsession=$larrformdata['idsession'];		
				 $db = Zend_Db_Table::getDefaultAdapter();
				echo $where = 'idvenue = '.$idvenue.' and date="'.$availdate.'" and idsession='.$examsession.' ';

			     $postDatave = array(		
						   	 'Allotedseats' =>new Zend_Db_Expr('Allotedseats +'.$studcount)			    															
						);
					
				$table = "tbl_venuedateschedule";			
	      	$db->update($table,$postDatave,$where); 
	
	}
	
	
  public function fnGetStatelistforcourse($idprog,$idyear){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.year=?",$idyear);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnInsertIntoAdhocStd($larrformData,$countloop,$batchid,$regpin)
	{
		 
		for($inseradhoc=0;$inseradhoc<$countloop;$inseradhoc++)
		{
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_studentapplication";
		    $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['candidatename'][$inseradhoc],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['candidatedateofbirth'][$inseradhoc],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['candidateemail'][$inseradhoc],	
           					'UpdDate' =>date('Y-m-d:H-i-s'), 
							'UpdUser' =>1,	
           					'IdBatch' =>$larrformData['activeset'][$inseradhoc],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					'Program' =>$larrformData['candidateprog'][$inseradhoc],	
           					'idschedulermaster' => 0, 
							'Amount' =>000,	
           					'ICNO' =>$larrformData['candidateicno'][$inseradhoc],	
           					'Payment' => 0, 
							'DateTime' =>0,	
           					'PermAddressDetails' =>00,	
           					'Takafuloperator' =>$larrformData['candidatetakaful'][$inseradhoc], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 0, 
							'Gender' =>$larrformData['candidategender'][$inseradhoc],	
           					'Race' =>$larrformData['candidaterace'][$inseradhoc],	
           					'Qualification' =>$larrformData['candidateeducation'][$inseradhoc], 
							'State' =>000,	
           					'CorrAddress' =>000,	
           					'PostalCode' =>000, 
		 					'ContactNo' =>000,	
           					'MobileNo' =>000, 
		  					'ExamState'=>00,
		  					'ExamCity'=>0,
		  					'Year'=>0,
		  					'Examdate'=>0,
		  					'Exammonth'=>0,
		  					'Examvenue'=>0,
		                     'pass'=>3
						);			
	       $db->insert($table,$postData);
	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');
	        
	        
	        $larrresult = self::fnviewstudentdetails($lastid);
	        
	       
	        $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($larrformData['candidatetakaful'][$inseradhoc],$lastid);
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['activeset'][$inseradhoc],
            				'Approved' =>1,
            				'Cetreapproval'=>0,		
		            		'IDApplication' =>$lastid,
            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);	       
	       self::sendmails($larrresult,$regid);
	       
		}
			    $where = 'idBatchRegistration = '.$batchid;
			    $postData = array(		
							'paymentStatus' => 2,	
			     			'Approved' => 0													
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	}
	
	public function fnBatchProgram($idprog)
	{
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_batchmaster',array('key' =>'IdBatch','value' => 'BatchName'))
								->where('IdProgrammaster =?',$idprog);
			$result = $db->fetchAll($sql);
			return $result;
	}
	
	public function fngetErrstudentapllication($regpin){ // Show error log
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_errexcelupload',array('StudentName','ICNO','MailId','RegistrationPin','DOB','Flag','UpdDate'))
								->where('RegistrationPin =?',$regpin);
			$result = $db->fetchAll($sql);
			return $result;
	}
	public function fngetBatchRegistrationPinforexcel($idregpin){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_batchregistration',array('idBatchRegistration','totalNoofCandidates'))
								->where('registrationPin =?',$idregpin);
			$result = $db->fetchRow($sql);
			return $result;		
	}
	public function fngetvenuedetailsinsert($idcenter){
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_center',array('city','state'))
								->where('idcenter =?',$idcenter);
			$result = $db->fetchRow($sql);
			return $result;
	}
	
	public function fnerrorreporter($emailid,$icno,$name,$dob,$regpin,$flag){// to get error data from excel
		
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_errexcelupload";
            $postdata=array('StudentName'=>$name,
            				'ICNO'=>$icno,
            				'MailId'=>$emailid,
            				'RegistrationPin'=>$regpin,
            				'DOB'=>$dob,
            				'Flag'=>$flag
            );
		$db->insert($table,$postdata);
	}
	
	public function fnGetapplicantcount($regpin){// to get total remaining application
				
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from(array("a" =>"tbl_batchregistration"),array('a.totalNoofCandidates'))
								->joinLeft(array("c" =>"tbl_registereddetails"),'a.registrationPin = c.RegistrationPin',array('count(distinct c.idregistereddetails) as totalregistered'))
								//->joinLeft(array("b" =>"tbl_tempexcelcandidates"),'a.registrationPin = b.RegistrationPin and b.ICNO not in (select ICNO from tbl_studentapplication where Takafuloperator = a.idCompany)',array('count(distinct b.idcandidates) as totalcount'))
								->where('a.registrationPin =?',$regpin);
							
			$result = $db->fetchRow($sql);
			return $result;
	}

	public function fnGetapplicantcountCompany($regpin){
		
			$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from(array("a" =>"tbl_batchregistration"),array('a.totalNoofCandidates'))
								->joinLeft(array("c" =>"tbl_registereddetails"),'a.registrationPin = c.RegistrationPin',array('count(distinct c.idregistereddetails) as totalregistered'))
								//->joinLeft(array("b" =>"tbl_tempexcelcandidates"),'a.registrationPin = b.RegistrationPin and b.ICNO not in (select ICNO from tbl_studentapplication where batchpayment = 1 and Examvenue !=000 )',array('count(distinct b.idcandidates) as totalcount'))
								->where('a.registrationPin =?',$regpin);							
			$result = $db->fetchRow($sql);
			return $result;
	}


	public function fnviewstudentdetails($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_studentapplication"),array('FName','MName','LName','EmailAddress','ICNO','PermAddressDetails','CorrAddress','Examdate','Exammonth','Year','password','username',"year(DateTime) as yearss"))
										  ->join(array("tbl_programmaster"),'Program=IdProgrammaster',array('ProgramName'))
										  ->join(array("tbl_center"),'Examvenue=idcenter',array('centername','addr1','addr2'))
										  ->join(array("tbl_managesession"),'Examsession=idmangesession',array('managesessionname','ampmstart','ampmend'))	
										 //->join(array("tbl_center"),'Examvenue=idcenter',array('centername'))
										 //->join(array("tbl_managesession"),'Examsession=idmangesession',array('managesessionname'))										  
										  ->where("IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
	public function sendmails($larrresult,$regid)
	{
			$larrresult['Year']=$larrresult['yearss'];
			require_once('Zend/Mail.php');
			require_once('Zend/Mail/Transport/Smtp.php');
	        $lobjExamdetailsmodel = new App_Model_Examdetails();
			//$larrresult= $lobjExamdetailsmodel->fnGetStudentDetailsPassfailforbatchcandidates($regid);	
	
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;
						$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;				
										
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							//$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							//$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'].' '.$larrresult['addr2'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['Year'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
										//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'].'('.$larrresult['ampmstart'].'--'.$larrresult['ampmend'].')',$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Amount]",$postArray['mc_gross'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$regid,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["username"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['password'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
								/*      $to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);
										*/
										
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
							try {
									$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
										//$result = $mail->send($transport);
										
				 $this->view->mess .= $lstrEmailTemplateBody;							
			
	}		
	
	public function fnGetRace()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where('a.idDefType = 13');	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetEducation()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where('a.idDefType = 14');	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	public function fnGetDaysforcourse($idprog,$year){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulerdays"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  //->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.year = ?",$year)
										  ->order("b.Days");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetMonths($year,$Program){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_newscheduler"),array("a.From","a.To","a.Year"))
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->where("c.IdProgramMaster  = ?",$Program)
										  ->where("a.year = ?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetmonthsbetween($frommonth,$tomonth)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lselect = "select idmonth as `key`,MonthName as `value` from tbl_newmonths where idmonth >='$frommonth' and idmonth <='$tomonth'";
		$larrResult = $lobjDbAdpt->fetchAll($lselect);
				return $larrResult;
	}
	
  public function fnGetVenuedetails($year,$prog,$city)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
 		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" => "tbl_newscheduler"),array())
								 ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
								 ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler')
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("key"=>"d.idcenter","value"=>"d.centername"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array())
										  ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										  ->group("b.idvenue")
										  ->where("a.Year =?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  
  public function fnGetsesssiondetails($year,$prog,$city)
  {
  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  		$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler')
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession',array("key"=>"g.idmangesession","value"=>"g.managesessionname"))
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter')
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array())
										  ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										  ->where("a.Year =?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
	public function fnGetActiveSet($Program){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"))
										  ->join(array("c"=>"tbl_batchmaster"),'a.IdProgrammaster=c.IdProgrammaster',array(c.IdBatch))
										  ->join(array("d"=>"tbl_tosmaster"),'d.IdBatch=c.IdBatch')
										  ->where("a.IdProgramMaster  = ?",$Program)
										  ->where("d.Active=1");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fngetstudentsemail($Program,$idyear)
		 {
		 	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication"))
							 				  ->where("a.Program = ?",$Program)
							 				 ->where("a.EmailAddress = ?",$idyear);
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
		 }
		 
	public function fnGetModeofpay($idbatchreg){
		
		     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_studentpaymentoption"),array("ModeofPayment"))
							 				 ->where("IDApplication = ?",$idbatchreg)
							 				 ->where("companyflag = 2");
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
		
	}
	
	public function fnGetModeofpayCompany($idbatchreg){
		
		     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_studentpaymentoption"),array("ModeofPayment"))
							 				 ->where("IDApplication = ?",$idbatchreg)
							 				 ->where("companyflag = 1");
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
		
	}
	
	
	public function fnGetprogramapplied($idbatchreg){
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_batchregistrationdetails"),array("idProgram"))
							 				 ->where("idBatchRegistration = ?",$idbatchreg);
			 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			 return $larrResult;
	}
	
	public function fngetfailedstudents(){
		
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("tbl_studentapplication.IDApplication","tbl_studentapplication.Examdate","tbl_studentapplication.Exammonth","tbl_studentapplication.Year","tbl_studentapplication.FName","tbl_studentapplication.Program"))								 				 
							 				 ->join(array("tbl_programmaster"=>"tbl_programmaster"),'tbl_studentapplication.Program=tbl_programmaster.IdProgrammaster',array('tbl_programmaster.ProgramName'))
							 				 ->join(array("tbl_studentmarks"),'tbl_studentapplication.IDApplication=tbl_studentmarks.IDApplication',array("tbl_studentmarks.Correct","tbl_studentmarks.Grade"))						 				 
							 				 ->where("tbl_studentapplication.pass = 2 or tbl_studentapplication.pass = 4");							 				
		 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		 return $larrResult;
		
	}
	
	public function fnSearchFailedStudents($larrformData){		
			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("tbl_studentapplication.IDApplication","tbl_studentapplication.Examdate","tbl_studentapplication.Exammonth","tbl_studentapplication.Year","tbl_studentapplication.FName","tbl_studentapplication.Program"))								 				 
											 ->join(array("tbl_registereddetails"=>"tbl_registereddetails"),'tbl_studentapplication.IDApplication = tbl_registereddetails.IDApplication',array())
											 ->join(array("tbl_batchregistration"=>"tbl_batchregistration"),'tbl_registereddetails.RegistrationPin = tbl_batchregistration.registrationPin',array())
											 ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),'tbl_batchregistration.idBatchRegistration = tbl_studentpaymentoption.IDApplication AND tbl_studentpaymentoption.companyflag=2',array())
							 				 ->join(array("tbl_programmaster"=>"tbl_programmaster"),'tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster',array('tbl_programmaster.ProgramName'))
							 				->join(array("tbl_studentmarks"),'tbl_studentapplication.IDApplication=tbl_studentmarks.IDApplication',array("tbl_studentmarks.Correct","tbl_studentmarks.Grade"))						 				 
											 ->where("tbl_batchregistration.idBatchRegistration=?",$larrformData['Batchpin'])
							 				 ->where("tbl_studentapplication.FName LIKE '%".$larrformData['studentname']."%'")
							 				 ->where("tbl_studentapplication.pass = ?",$larrformData['Studentstatus'])
							 				 ->where("tbl_studentapplication.Program = ?",$larrformData['ProgramSearch'])
							 				 ;	
							 			//echo $lstrSelect;				 				
			 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			 return $larrResult;
	}

	//newadded 27-06-2012
	public function fnGetprogramappliedExcel($idregpin){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();			
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("tbl_batchregistration"=>"tbl_batchregistration"),array())
										  ->join(array("tbl_batchregistrationdetails"=>"tbl_batchregistrationdetails"),'tbl_batchregistration.idBatchRegistration = tbl_batchregistrationdetails.idBatchRegistration',array("tbl_batchregistrationdetails.idProgram"))
										  ->where('tbl_batchregistration.registrationPin = ?',$idregpin);	
										  
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	}
	
	
	public function fnSearchFailedStudentsCompany($larrregpin,$status,$idcompany,$flag)
	{
		    //$status=3;
			//$flag=1 means company $flag=2 means takaful		
			if($status==1)
			{
				$status="1,2,3,4";
			}
			
		 	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	 if($larrregpin!=0){
		 	 	//Fetching normal registered candidates		 	 	
				 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array("tbl_studentapplication.IDApplication","tbl_studentapplication.Examdate","tbl_studentapplication.ICNO","tbl_studentapplication.Exammonth","tbl_studentapplication.Year","tbl_studentapplication.FName","tbl_studentapplication.Program","tbl_studentapplication.DateTime","tbl_studentapplication.pass"))								 				 
											 ->join(array("tbl_registereddetails"=>"tbl_registereddetails"),'tbl_studentapplication.IDApplication = tbl_registereddetails.IDApplication',array("tbl_registereddetails.Regid"))
											 ->join(array("tbl_batchregistration"=>"tbl_batchregistration"),'tbl_registereddetails.RegistrationPin = tbl_batchregistration.registrationPin',array())
											 ->join(array("tbl_studentpaymentoption"=>"tbl_studentpaymentoption"),'tbl_batchregistration.idBatchRegistration = tbl_studentpaymentoption.IDApplication',array())
							 				 ->join(array("tbl_programmaster"=>"tbl_programmaster"),'tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster',array('tbl_programmaster.ProgramName','tbl_programmaster.IdProgrammaster'))
							 				//->join(array("tbl_studentmarks"),'tbl_studentapplication.IDApplication=tbl_studentmarks.IDApplication',array("tbl_studentmarks.Correct","tbl_studentmarks.Grade"))						 				 
											 ->where("tbl_batchregistration.idBatchRegistration=?",$larrregpin)							 				 
							 				 ->where("tbl_studentapplication.pass in ($status)")
							 				 ->where("tbl_studentapplication.IDApplication NOT IN (select IDApplication from tbl_studentchangeddetails where companytype = ".$flag.")")
							 				 ->where("tbl_studentpaymentoption.companyflag=?",$flag)							 				 
							 				 ->where("tbl_batchregistration.idCompany=?",$idcompany)							 				
							 				 ->order("tbl_studentapplication.FName");	
		 	 }else{
		 	 	//Fetching Migrated candidates		 	 	
		 	 	 $lstrSelect = $lobjDbAdpt->select()
		 	 	 							 ->from(array("tbl_studentchangeddetails"=>"tbl_studentchangeddetails"),array(""))
							 				 ->join(array("tbl_studentapplication"=>"tbl_studentapplication"),'tbl_studentchangeddetails.IDApplication = tbl_studentapplication.IDApplication',array("tbl_studentapplication.IDApplication","tbl_studentapplication.Examdate","tbl_studentapplication.ICNO","tbl_studentapplication.Exammonth","tbl_studentapplication.Year","tbl_studentapplication.FName","tbl_studentapplication.Program","tbl_studentapplication.DateTime","tbl_studentapplication.pass"))								 				 
											 ->join(array("tbl_registereddetails"=>"tbl_registereddetails"),'tbl_studentapplication.IDApplication = tbl_registereddetails.IDApplication',array("tbl_registereddetails.Regid"))
											 //->join(array("tbl_batchregistration"=>"tbl_batchregistration"),'tbl_registereddetails.RegistrationPin = tbl_batchregistration.registrationPin',array())
							 				 ->join(array("tbl_programmaster"=>"tbl_programmaster"),'tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster',array('tbl_programmaster.ProgramName','tbl_programmaster.IdProgrammaster'))
							 				 ->where("tbl_studentchangeddetails.pass = ?",$status)
							 				 ->where("tbl_studentchangeddetails.companytype = ?",$flag)
							 				 ->where("tbl_studentchangeddetails.Idcompany = ?",$idcompany)
							 				 ->where("tbl_studentchangeddetails.Active = 1")						 											 				
							 				 ->order("tbl_studentapplication.FName");	
		 	 }
		 	//echo $lstrSelect;
		 	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	
	
	public function fnGetcandidatelookup($lintidapplication){
		//Added on 07-09-2012
		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
							 	    ->from(array("tbl_studentapplication"=>"tbl_studentapplication"),array(""))								 				 
								    ->join(array("tbl_registereddetails"=>"tbl_registereddetails"),'tbl_studentapplication.IDApplication = tbl_registereddetails.IDApplication',array(""))
								    ->join(array("tbl_batchregistration"=>"tbl_batchregistration"),'tbl_registereddetails.RegistrationPin = tbl_batchregistration.registrationPin',array ("key" =>"tbl_batchregistration.idBatchRegistration","value" =>"tbl_batchregistration.registrationPin"))
							 	    ->where("tbl_studentapplication.IDApplication in (".$lintidapplication.")")							 											 				
							 	    ->order("tbl_studentapplication.FName");	
					 				
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	
	
	function fnGetregpin($programid,$idCompany,$flag)
    {   //$flag=1 means company;$flag=2 means takaful

  		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from (array("tbl_batchregistration" => "tbl_batchregistration"), array ("key" => "tbl_batchregistration.idBatchRegistration","value" =>"tbl_batchregistration.registrationPin") )
											->join(array("tbl_batchregistrationdetails" => "tbl_batchregistrationdetails"),'tbl_batchregistration.idBatchRegistration = tbl_batchregistrationdetails.idBatchRegistration',array())
								 			->join(array("tbl_studentpaymentoption" => "tbl_studentpaymentoption"),'tbl_batchregistration.idBatchRegistration = tbl_studentpaymentoption.IDApplication',array())
								 			->where("tbl_batchregistration.idCompany =?",$idCompany)
								 			->where("tbl_studentpaymentoption.companyflag =?",$flag)
								 			->where("tbl_batchregistrationdetails.idProgram=?",$programid)
								 			->where("tbl_batchregistration.registrationPin != 0")
								 			->where("tbl_batchregistration.registrationPin IN (select RegistrationPin from tbl_registereddetails)");
								 		
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );		
		return $larrResult;
    }
	
    public function fnGetregpinrefined($programid,$idCompany,$strcandidatename,$strcandidateicno,$lintstatus,$flag){
    	//added on 05-09-2012
    	//$flag=1;
    	//$idCompany=1;  	
    if($lintstatus==1)
			{
				$lintstatus="1,2,3,4";
			}
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()
								 ->from(array("tbl_studentapplication" => "tbl_studentapplication"),array("tbl_studentapplication.IDApplication","tbl_studentapplication.Examdate","tbl_studentapplication.ICNO","tbl_studentapplication.Exammonth","tbl_studentapplication.Year","tbl_studentapplication.FName","tbl_studentapplication.Program","tbl_studentapplication.DateTime","tbl_studentapplication.pass"))
								 ->join(array("tbl_registereddetails"=>"tbl_registereddetails"),'tbl_studentapplication.IDApplication = tbl_registereddetails.IDApplication', array("tbl_registereddetails.Regid","tbl_registereddetails.RegistrationPin"))
								 ->join(array("tbl_batchregistration" => "tbl_batchregistration"),'tbl_registereddetails.RegistrationPin = tbl_batchregistration.registrationPin',array())
								 ->join(array("tbl_programmaster"=>"tbl_programmaster"),'tbl_studentapplication.Program = tbl_programmaster.IdProgrammaster',array('tbl_programmaster.ProgramName','tbl_programmaster.IdProgrammaster'))
								 ->where("tbl_studentapplication.FName like '%".$strcandidatename."%'")	
								 ->where("tbl_studentapplication.ICNO like '%".$strcandidateicno."%'")	
								 ->where("tbl_studentapplication.Program = ?",$programid)
								 ->where("tbl_studentapplication.pass in ($lintstatus)")
								 ->where("tbl_studentapplication.batchpayment = ?",$flag)
								 ->where("tbl_batchregistration.idCompany =?",$idCompany)							
								 ->where("tbl_batchregistration.registrationPin != 0");
			//echo $lstrSelect;exit;
		$larrResult = $lobjDbAdpt->fetchAll ( $lstrSelect );
		return $larrResult;
    }
	
	public function  fnGetAllActiveReligionNameList()
	{ 	
		$db = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $db->select()
								  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
								  ->where("a.idDefType  = 8")
								  ->where("a.Status  = ?","1");	
		$larrResult = $db->fetchAll($lstrSelect);
		return $larrResult;
	}

		public function fnGetCountryList(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_countries"),array("key"=>"a.idCountry","value"=>"CountryName"))
					 				 ->where("a.Active = 1")
					 				 ->order("a.CountryName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}

	public function fngeneraterandompassword()
	{
		/*$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	      $length = 12;
	      $characters = '0123456789';
	      $string = "";    	
	      for ($p = 0; $p < $length; $p++) {
	        	$string.= $characters[mt_rand(0, strlen($characters))];
	        }           
    	  return $string;*/
		
   		return $newrand=rand(999999,100000).rand(777777,999999);
	}

	
	public function fndeletefromtempexcelcandidates($idcandidates){ 		//ADDED on 28-02-2013
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 		
		$table ="tbl_tempexcelcandidates"; 		
		$where ="idcandidates =".$idcandidates;	   
		//echo $where; exit;
		$lobjDbAdpt->delete($table,$where); 	
	 }
	 
	 public function fndeletefromtemp($idcandidates){  //ADDED on 28-02-2013
	 	//$idcandidates=26;
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 		
		$table ="tbl_tempexcelcandidates"; 		
		$where ="idcandidates =".$idcandidates;	   
		return $lobjDbAdpt->delete($table,$where); 	
	 }
	
	
	public	function fnvalidateseats($venue,$selecteddate)
	{
	  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	  	$select = "SELECT Allotedseats,Totalcapacity,(Totalcapacity-Allotedseats) as rem from  tbl_venuedateschedule where idvenue =$venue and Active=1 AND Reserveflag=1  AND centeractive=1 AND date ='$selecteddate'";
		return	$result = $lobjDbAdpt->fetchAll($select);
	}
  
 }
