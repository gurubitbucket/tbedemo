<?php
class App_Model_Examdetails extends Zend_Db_Table {
	
	/*
	 * functin to fetch all easy questions
	 */
	public function fnGetEasyQuestions($idbranch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()				  
						->from(array('a' => 'tbl_tossubdetail'),array('a.NoofQuestions','a.IdDiffcultLevel'))
						->join(array('b' =>'tbl_tosdetail'),'a.IdTOSDetail = b.IdTOSDetail')
						->join(array('c'=>'tbl_tosmaster'),'b.IdTOS = c.IdTOS')
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$idbranch);                                
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
	}
	
public function fntotalanswered($Regid)
	{
		$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		                 ->from(array('a'=>'tbl_answerdetails'),array('a.*'))
		                 ->where("a.Regid='$Regid'");
		    $result = $db->fetchAll($select);
		    return $result;
	}
	
	/*
	 * function to insert into the student marks table
	 */
	public function fninsertstudentmarks($studentid,$noofquestions,$totalanswered,$attended,$grade)
	{
		$db =  Zend_Db_Table::getDefaultAdapter();  
		 $table = "tbl_studentmarks";
           $postData = array(		
							'IDApplication' => $studentid,		
            				'NoofQtns' =>$noofquestions,		
		            		'Attended' =>$totalanswered,	
		            		'Correct'	=>$attended,
		            		'Grade'=>$grade															
						);			
	        $db->insert($table,$postData);
	}
	

	/*
	 * function to fetch the time limit
	 */
	public function fnGetTime($idIdBatch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_tosmaster"),array("a.TimeLimit","a.AlertTime"))
					 				 ->where("a.IdBatch = ?",$idIdBatch);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
	}
	
	public function fnGetSectionsss($idbatch)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $select ="SELECT distinct(`b`.`IdSection`) FROM `tbl_tosdetail` AS `b` INNER JOIN `tbl_tosmaster` AS `c` ON b.IdTOS = c.IdTOS INNER JOIN `tbl_batchmaster` AS `d` ON c.IdBatch = d.IdBatch WHERE (d.IdBatch =$idbatch)" ;                         
			return	$result = $lobjDbAdpt->fetchAll($select);	
				
	}
	public function fnGetSections($idbatch)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()				  
						->from(array('d'=>'tbl_batchmaster'),array("d.*"))
						->join(array('c'=>'tbl_batchdetail'),'c.IdBatch = d.IdBatch',array("c.*"))
                        ->where('d.IdBatch = '.$idbatch);                                
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
				
	}
	/*
	 * function for editing using ajax
	 */
	public function fnGetNoofQuestion($IdBatch,$Questionlevel)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()				  
						->from(array('a' => 'tbl_tossubdetail'),array('a.NoofQuestions','a.IdDiffcultLevel'))
						->join(array('b' =>'tbl_tosdetail'),'a.IdTOSDetail = b.IdTOSDetail')
						->join(array('c'=>'tbl_tosmaster'),'b.IdTOS = c.IdTOS')
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$IdBatch)
                        ->where('b.IdSection = "'.$Questionlevel.'"');                           
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
	}
	
	/*
	 * function for getting the random easy questions
	 */
	public function fnGetRandomEasyQuestions($Questionnumber,$easyquestion,$Mediumquestion,$difficultquestion)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 
		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a` WHERE (a.QuestionLevel = 1) AND (a.QuestionNumber = '$Questionnumber')  AND a.Active=1 ORDER BY RAND() LIMIT 0,$easyquestion ";
		$result1 = $lobjDbAdpt->fetchAll($select);

		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a` WHERE (a.QuestionLevel = 2) AND (a.QuestionNumber = '$Questionnumber')  AND a.Active=1 ORDER BY RAND() LIMIT 0,$Mediumquestion ";
		$result2 = $lobjDbAdpt->fetchAll($select);
		
		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a` WHERE (a.QuestionLevel = 3) AND (a.QuestionNumber = '$Questionnumber')  AND a.Active=1 ORDER BY RAND() LIMIT 0,$difficultquestion ";
		$result3 = $lobjDbAdpt->fetchAll($select);
//print_r($result1);die();
		$result = array_merge($result1,$result2,$result3);
		//print_r($result);die();
		//print_r($result['idquestions']);
		return $result;
	}
public function fnGetRandomEasyQuestionss($Questionnumber,$easyquestion,$Mediumquestion,$difficultquestion)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 
		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a`,tbl_questionset as b WHERE (a.QuestionLevel = 1) AND (a.QuestionNumber = '$Questionnumber')AND a.idquestions=b.idquestion  ORDER BY RAND() LIMIT 0,$easyquestion ";
		$result1 = $lobjDbAdpt->fetchAll($select);

		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a`,tbl_questionset as b WHERE (a.QuestionLevel = 2) AND (a.QuestionNumber = '$Questionnumber') AND a.idquestions=b.idquestion  ORDER BY RAND() LIMIT 0,$Mediumquestion ";
		$result2 = $lobjDbAdpt->fetchAll($select);
		
		 $select ="SELECT `a`.`Question`,`a`.`idquestions`,`a`.`Arabic`,`a`.`Malay` FROM `tbl_questions` AS `a`,tbl_questionset as b WHERE (a.QuestionLevel = 3) AND (a.QuestionNumber = '$Questionnumber') AND a.idquestions=b.idquestion  ORDER BY RAND() LIMIT 0,$difficultquestion ";
		$result3 = $lobjDbAdpt->fetchAll($select);
//print_r($result1);die();
		$result = array_merge($result1,$result2,$result3);
		//print_r($result);
		//print_r($result['idquestions']);
		return $result;
	}
	
	/*
	 * function to fetch all the answer based on the questions
	 */
   public function fnGetAnswers($arrayqstn)
   {
   	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    $select = "SELECT answers,idquestion,idanswers,Malay,Tamil,Arabic FROM tbl_answers WHERE idquestion IN ($arrayqstn)";
   	 $result = $lobjDbAdpt->fetchAll($select);
   	 return $result;
   }
   public function fngetidbatchdetails($regid)
   
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$select= "SELECT b.IdBatch
   	 			FROM tbl_studentapplication b,tbl_registereddetails a
   	 			WHERE a.IDApplication=b.IDApplication and a.Regid='$regid'";
   	$result = $lobjDbAdpt->fetchAll($select);
   	 return $result;
   	
   }
   /*
    * function to fetch the section name
    */
   public function fnGetSectionName($lintidquestion)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	 $select = "SELECT * FROM tbl_questions WHERE idquestions=$lintidquestion";
   	 $result = $lobjDbAdpt->fetchRow($select);
   	 return $result;
   }
   
   /*
    * inserting into temp table
    */
	public function fnInsertTempDetails($IdBatch,$RegId,$sessionID,$lintidquestion,$lintidanswers,$questionlevel)
	{
		
		 $db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_tempexamdetails";
           $postData = array(		
							'Regid' => $RegId,		
            				'IdBatch' =>$IdBatch,		
		            		'Section' =>$questionlevel,	
		            		'Session'	=>$sessionID,
		            		'QuestionNo'=>$lintidquestion,
		            		'Answer'=>$lintidanswers															
						);			
	        $db->insert($table,$postData);
	}
	
	/*$where = "idtemp = '".$idtemp."' AND idsession ='".$sessionID."'"; 	    
	 * fetching details from the table
	 */
 public function fnGetAllDetailsFromTemp($IdBatch,$RegId,$sessionID)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$select = "SELECT QuestionNo 
   	 			FROM tbl_tempexamdetails 
   	 			WHERE  IdBatch=$IdBatch AND Regid='$RegId'";
   	 $result = $lobjDbAdpt->fetchAll($select);
   	 return $result;
   }
   
   /*
    * function for updating the temp details
    */
   public function fnUpdateTempDetails($IdBatch,$RegId,$sessionID,$lintidquestion,$lintidanswers)
   {
   	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			   	$where = "RegId = '".$RegId."'  AND IdBatch ='".$IdBatch."'  AND QuestionNo ='".$lintidquestion."'"; 	 
			     $postData = array(		
							'Answer' => $lintidanswers														
						);
				$table = "tbl_tempexamdetails";
	            $lobjDbAdpt->update($table,$postData,$where);
   }
   
   /*
    * function to take no of question from ajax
    */
   public function fnGetNoofQuestioninajax($IdBatch,$questionlevel)
   {
   		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()				  
						//->from(array('a' => 'tbl_tossubdetail'),array('a.NoofQuestions','a.IdDiffcultLevel'))
						->from(array('b' =>'tbl_tosdetail'),array('b.*'))
						->join(array('c'=>'tbl_tosmaster'),'b.IdTOS = c.IdTOS')
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$IdBatch)
                        ->where('b.IdSection = "'.$questionlevel.'"');                           
				$result = $lobjDbAdpt->fetchRow($select);	
				return $result;
   }
   
   /*
    * 
    */
   public function fnGetNoofQuestioninajaxupdated($IdBatch,$questionlevel)
   {
   		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()				  
		                ->from(array('c'=>'tbl_batchdetail'),array("c.*"))
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$IdBatch)
                         ->where('c.IdPart = "'.$questionlevel.'"');                          
				$result = $lobjDbAdpt->fetchRow($select);	
				return $result;
   }
   
   /*
    * function to take from the temporaty table
    */
   public function fnGetQstnfromtemptable($IdBatch,$RegId,$sessionID,$questionlevel)
   {
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$select = "SELECT count(QuestionNo) 
   	 			FROM tbl_tempexamdetails 
   	 			WHERE  IdBatch=$IdBatch AND Regid='$RegId' AND Section='$questionlevel'";
   	 $result = $lobjDbAdpt->fetchRow($select);
   	 return $result;
   }
   
   /*
    * fetchfrom temp table
    */
   public function fnGetFromTempDetail($IdBatch,$RegId,$sessionID)
   {
    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
   	$select = "SELECT * 
   	 			FROM tbl_tempexamdetails 
   	 			WHERE IdBatch=$IdBatch AND Regid='$RegId'";
   	 $resulttempdetails = $lobjDbAdpt->fetchAll($select);
   	  for($linti=0;$linti<count($resulttempdetails);$linti++)
		 {
		 	 $larrcreditnotedetails['Regid'] = $resulttempdetails[$linti]['Regid'];
		 	 $larrcreditnotedetails['QuestionNo'] = $resulttempdetails[$linti]['QuestionNo'];
		 	 $larrcreditnotedetails['Answer'] = $resulttempdetails[$linti]['Answer'];
		 	  $result = $lobjDbAdpt->insert('tbl_answerdetails',$larrcreditnotedetails);
		 	 
		 }
   	// return $result;
   }
   
	/*
 	 * function to delete datas from temp table on close
 	 */
 	public function fnDeleteTempDetails($sessionID)
 	{
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$where = "Regid = '".$sessionID."'";
 		$db->delete('tbl_tempexamdetails',$where);
 		
 	}
 	
	/*
	 * function to fetch all the answer based on the questions
	 */
   public function fnGetQtnfromidbatch($IdBatch)
   {
   	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$select = $lobjDbAdpt->select()				  
						//->from(array('a' => 'tbl_tossubdetail'),array('a.NoofQuestions','a.IdDiffcultLevel'))
						->from(array('b' =>'tbl_tosdetail'),array('b.*'))
						->join(array('c'=>'tbl_tosmaster'),'b.IdTOS = c.IdTOS')
						->join(array('d'=>'tbl_batchmaster'),'c.IdBatch = d.IdBatch')
                        ->where('d.IdBatch = '.$IdBatch);                     
				$result = $lobjDbAdpt->fetchAll($select);	
				return $result;
   }
   
   /*
    * function to fetch the name of the studen tname
    */
		public function fnGetStudentName($idstudent) {
			$db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT CONCAT(FName,' ',IFNULL(MName,' '),' ',IFNULL(LName,' ')) AS Name,StudentId,EmailAddress,Examdate  FROM tbl_studentapplication WHERE IDApplication = $idstudent";
    		$result = $db->fetchRow($sql);    
			return $result;
    	}
    	
  public function fnGetStudentdetails($idstudent)
  {
  	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_studentapplication'),array("a.*"))
		            //->joinleft(array('b'=>'tbl_registereddetails'),'a.IdBatch = b.IdBatch')
		            ->where("a.IDApplication = ?",$idstudent);
		    $result = $db->fetchRow($select);
		    return $result;
  }
    	
  public function fnGetIdBatch($IdBatch)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
				->from(array('a' => 'tbl_registereddetails'),array('a.*'))
				->where("a.Approved = 1")
				->where("a.Cetreapproval = 1")
                        ->where('a.Regid = ?',$IdBatch);
	 $result = $db->fetchRow($select);	
		return $result;
    	
    }
    
    public function fnCheckforApproval($examvenue,$idprog,$date)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_centerstartexam'),array("a.*"))
	                ->where("a.Program=?",$idprog)
	                ->where("a.Date=?",$date)
		            ->where("a.idcenter = ?",$examvenue)
		            ->order("a.idcenterstartexam desc");
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    /*
     * function for date check
     */
    public function fnCheckDate($Regid){
    		$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_batchmaster'),array("a.BatchTo"))
		            ->joinleft(array('b'=>'tbl_registereddetails'),'a.IdBatch = b.IdBatch')
		            ->where("b.IdBatch = ?",$Regid);
		    $result = $db->fetchRow($select);
		    return $result;
    	}
    	
    	/*
    	 * function for getting register ids
    	 */
    public function fnGettempRegisterid($regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_tempexamdetails'),array('a.*'))
		            ->where("a.Regid='$regid'");
		    $result = $db->fetchAll($select);
		    return $result;
    }   

   /* public function fngettempanswerssystemshutdown()
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_tempanswerdetails'),array('a.*'));
		    $result = $db->fetchAll($select);
		    return $result;
    } */
    
    	/*
    	 * function for getting register ids
    	 */
    public function fnGetRegisterid()
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_answerdetails'),array('distinct(a.Regid)'));
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
    /*
     * 
     */
    public function fnGetBatchName($Regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		 $select = $db->select()
		            ->from(array('a'=>'tbl_batchmaster'),array('a.*'))
		            ->join(array('b'=>'tbl_batchdetail'),'a.IdBatch = b.IdBatch',array("b.*"))
		            ->join(array('c'=>'tbl_tosmaster'),'a.IdBatch = c.IdBatch')
 		           ->join(array('d'=>'tbl_programmaster'),'d.IdProgrammaster=a.IdProgrammaster')
		            ->where("b.BatchDtlStatus =0")
		             ->where("a.IdBatch = ?",$Regid);
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
    /*SELECT count(`QuestionNo`) FROM `tbl_tempexamdetails` WHERE `Regid`=555
     * 
     * 
     */
    public function fnGetNoofQtnsFromTempTable($Regid)
    {
    	    $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT count(`QuestionNo`) FROM `tbl_tempexamdetails` WHERE `Regid`='$Regid'";
    		$result = $db->fetchRow($sql);    
			return $result;
    }
    
    public function fnDeleteIdCenterDeatails($idcenter)
    {
    	$db 	= Zend_Db_Table::getDefaultAdapter();
 		$where = "idcenter = '".$idcenter."'";
 		$db->delete('tbl_centerstartexam',$where);
    	
    }
    
    public function fnExampercentage($idbatch)
    {
    	   $db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->join(array('c'=>'tbl_tosmaster'),array("c.*"))
		             ->where("c.IdBatch = ?",$idbatch);
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fnAttended($Regid)
    {
    	
    	    $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT * FROM tbl_answerdetails  where Regid = '$Regid'  and answer in(SELECT idanswers FROM tbl_answers where CorrectAnswer =1) group by QuestionNo";
    		$result = $db->fetchAll($sql);    
			return $result;
    }
    
    public function fnGetEachSection($idbatch)
    { 
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->join(array('c'=>'tbl_tosdetail'),array("c.*"))
		             ->where("c.idTOS = ?",$idbatch);
		    $result = $db->fetchAll($select);
		    return $result;
    	
    }
    
    public function fnGetSectionWise($idsection)
    {
    	 $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT * FROM tbl_answerdetails  where answer in(SELECT idanswers from tbl_answers where idquestion in (Select idquestions from tbl_questions where QuestionNumber='$idsection') and CorrectAnswer=1)";
    		$result = $db->fetchAll($sql);    
			return $result;
    }
    
    public function fngetInitconfigDetails($iduniversity)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->join(array('a'=>'tbl_config'),array("a.*"))
		             ->where("a.idUniversity =1");
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fnGetStudentDetailsPassfail($regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_registereddetails'),array("a.*"))
		            ->join(array('b'=>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array("b.*"))
		              ->join(array('f'=>'tbl_programmaster'),'f.IdProgrammaster=b.Program',array("f.ProgramName"))
		              ->join(array("c" =>"tbl_newscheduler"),'b.Year=c.idnewscheduler',array('c.Year as years'))
		            ->where("a.Regid = ?",$regid);
		    $result = $db->fetchRow($select);
		    return $result;
    }
    public function totalattendedquestions($regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_answerdetails'),array("a.*"))
		            //->join(array('b'=>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array("b.*"))
		            ->where("a.Regid  ='$regid'");
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
    public function fnnoofqtns($idbatch)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_tosmaster'),array("a.*"))
		            //->join(array('b'=>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array("b.*"))
		            ->where("a.IdBatch  ='$idbatch'");
		    $result = $db->fetchAll($select);
		    return $result;
    	
    }
    
    public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}

    public function fnGetStudentDetailsPassfailforbatchcandidates($regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_registereddetails'),array("a.*"))
		            ->join(array('b'=>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array("b.*"))
		            //->join(array)
		            ->join(array("c"=>'tbl_center'),'b.Examvenue=c.idcenter',array("c.*"))
		            ->join(array("e" =>"tbl_programmaster"),'b.Program=e.IdProgrammaster',array('e.*'))
	                 ->join(array("f" =>"tbl_newscheduler"),'b.Year=f.idnewscheduler',array('f.Year as years'))
		              ->join(array("g"=>"tbl_managesession"),'b.Examsession=g.idmangesession',array('g.*'))
		            ->where("a.Regid = ?",$regid);
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
   public function fnupdatepass($idstudent,$result)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Pass'] = $result;	
		 $where = "IDApplication = '".$idstudent."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
    }
    
    public function fngetstudentid($Regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_registereddetails'),array("a.*"))
		            ->where("a.Regid = '$Regid'");
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fnGetQuestions($idbatch,$group)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            
		            ->from(array('b'=>'tbl_questions'),array("b.*"))
		            ->join(array('a'=>'tbl_questionset'),'a.idquestion=b.idquestions',array("a.*"))
		           // ->join(array("c"=>'tbl_answers'),'c.idquestion=b.idquestions',array("c.*"))
		            ->where("a.idbatch = ?",$idbatch)
		            ->where("a.editbatch=1")
		            ->where("b.QuestionGroup=?",$group)
		            ->order('RAND()');
		    $result = $db->fetchAll($select);
		    return $result;
    }
 public function fnGetQuestionsmultiple($idbatch,$group)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	 $select = $db->select()
		               ->from(array('b'=>'tbl_questions'),array("b.*"))
		             //  ->join(array('a'=>'tbl_questionset'),'b.idquestions=a.idquestion',array("a.*"))
		           // ->join(array("c"=>'tbl_answers'),'c.idquestion=b.idquestions',array("c.*"))
		            ->where("b.idquestions in (select idquestion from tbl_questionset where  idbatch = $idbatch  and editbatch=1)")
		            ->where("b.QuestionGroup=?",$group);
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
    public function fngetquestionpool($values,$group)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        	$select = $db->select()
		            
		         ->from(array('b'=>'tbl_questions'),array("b.*"))
		            ->where("b.idquestions in ($values)")
		             ->where("b.QuestionGroup=?",$group);
		          
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
 public function fnsinglequestions($id,$group)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
       	$select = $db->select()
		         ->from(array('b'=>'tbl_questions'),array("b.*"))
		            ->where("b.idquestions =?",$id)
		              ->where("b.QuestionGroup=?",$group);
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fngetactivebatch($idprog)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        	$select = $db->select()
		            		->from(array('b'=>'tbl_batchmaster'),array("b.*"))
		            		->join(array('c'=>'tbl_tosmaster'),'b.IdBatch=c.Idbatch',array("c.*"))
		            		->where("c.Active=1")
		           		    ->where("b.Idprogrammaster=?",$idprog);
		          
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
     public function fnGetStudentobtainedquestions($idstudent,$program,$idIdBatch,$larrquestionids)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        $table = "tbl_questionsetforstudents";
           $postData = array(		
							'IDApplication' => $idstudent,		
            				'Program' =>$program,		
		            		'IdBatch' =>$idIdBatch,	
		            		'idquestions'=>$larrquestionids
						);			
	        $db->insert($table,$postData);
		   // return $result;
    }
    
    public function fngetquestionsobtained($idapplication)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        $select = $db->select()
		            		->from(array('b'=>'tbl_questionsetforstudents'),array("b.idquestions"))
		           		    ->where("b.IDApplication=?",$idapplication)
		           		    ->order("b.idquestionsetforstudents Desc");
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    
       public function fnupdatebatch($result,$IdBatch)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['IdBatch'] = $IdBatch;	
		 $where = "Regid = '".$result."'"; 	
		 $db->update('tbl_registereddetails',$larrformData1,$where);
    }
    
    public function fninsertintostudentexamdetails($idApplication,$starttime,$endtime,$submittedby,$StudentIpAddress)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        $table = "tbl_studentstartexamdetails";
           $postData = array(		
							'IDApplication' => $idApplication,		
            				'Starttime' =>$starttime,		
		            		'Endtime' =>$endtime,	
		            		'Submittedby'=>$submittedby,
                            'UpdDate'=>date("Y-m-d H:i:s"),
           					'StudentIpAddress'=>$StudentIpAddress
						);			
	        $db->insert($table,$postData);
    }
    
    /* 
     * function to find the start time  
     */
    public function fnupdatestudentexamdetails($studentid,$endtime,$submittedby)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Endtime'] = $endtime;
    	$larrformData1['Submittedby'] = $submittedby;	
		 $where = "IDApplication = '".$studentid."'"; 	
		 $db->update('tbl_studentstartexamdetails',$larrformData1,$where);
    }
    /*
     * check approval updated
     */
    public function fnCheckforApprovalupdated($examvenue,$idprog)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_centerstartexam'),array("a.*"))
	                ->where("a.Program=?",$idprog)
	                ->where("a.ExamDate=curdate()")
		            ->where("a.idcenter = ?",$examvenue);
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    /*
     * function to insert in to the partwise marks
     */
    
  public function fngettheidparts($idbatchresult)
   {
   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			 $lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_batchdetail"),array("GROUP_CONCAT(IdPart SEPARATOR ',')as partsids")) 	
				 				 ->where("a.IdBatch=?",$idbatchresult);
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;	
   }
   
	public function partwiseattended($Regid,$part)
       {
    	
    	     $db =  Zend_Db_Table::getDefaultAdapter();    	
    		 $sql = "Select count(idquestions) from tbl_questions where idquestions  in (
					SELECT  QuestionNo FROM tbl_answerdetails  where Regid = '$Regid'  and answer in(SELECT idanswers FROM tbl_answers where CorrectAnswer =1))
					and QuestionGroup = '$part'";
    		 $result = $db->fetchRow($sql);    
			 return $result;
       }
       
    public function updatepartwisemarksfora($RegID,$partbmarks)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['A'] = $partbmarks;	
		 $where = "Regid = '".$RegID."'"; 	
		 $db->update('tbl_studentdetailspartwisemarks',$larrformData1,$where);
    }
    
    public function updatepartwisemarksforb($RegID,$partbmarks)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['B'] = $partbmarks;	
		 $where = "Regid = '".$RegID."'"; 	
		 $db->update('tbl_studentdetailspartwisemarks',$larrformData1,$where);
    }
    
    public function updatepartwisemarksforc($RegID,$partbmarks)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['C'] = $partbmarks;	
		 $where = "Regid = '".$RegID."'"; 	
		 $db->update('tbl_studentdetailspartwisemarks',$larrformData1,$where);
    }
    
    public function fninsertstudentpartwise($studentid,$RegID)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        $table = "tbl_studentdetailspartwisemarks";
           $postData = array(		
							'IDApplication' => $studentid,		
            				'Regid' =>$RegID,		
		            		'A' =>0,	
		            		'B'=>0,
           					'C'=>0,
                            'UpdDate'=>date("Y-m-d H:i:s")
						);			
	        $db->insert($table,$postData);
    }
    
    /*
     * finding for the already appeared
     */
    public function fncheckavailability($Regid)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_answerdetails'),array("a.*"))
		            ->where("a.Regid = '$Regid'");
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fncheckstudentexists($idapplication)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_studentmarks'),array("a.*"))
		            ->where("a.IDApplication = '$idapplication'");
		    $result = $db->fetchRow($select);
		    return $result;
    		
    }
    
    public function fngetextratime($examvenue,$date,$examsession)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_gracetime'),array("a.*"))
		            ->where("a.Idvenue = ?",$examvenue)
		            ->where("a.Idsession = ?",$examsession)
		            ->where("a.ExamDate = ?",$date);
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fngetquestionsetforstudents($idapplication)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    		$select = $db->select()
		            ->from(array('a'=>'tbl_questionsetforstudents'),array('a.*'))
		            ->where("a.IDApplication='$idapplication'");
		    $result = $db->fetchAll($select);
		    return $result;
    }
    
    public function fngetstudentlocaldetails($idapplication)
    {
    	$db =  Zend_Db_Table::getDefaultAdapter();  
    	$select = $db->select()
		            ->from(array('a'=>'tbl_studentapplication'),array("a.VenueTime"))
		            ->where("a.IDApplication = '$idapplication'");
		    $result = $db->fetchRow($select);
		    return $result;
    }
    
    public function fninsertstudentmails($studentid,$larrdata)
    {
    	
    	$db =  Zend_Db_Table::getDefaultAdapter();  
        $table = "tbl_resultmails";
           $postData = array(		
							'Idapplication' => $studentid,		
            				'Examdate' =>$larrdata['Examdate'],		
		            		'Venue' =>$larrdata['Examvenue'],	
		            		'Session'=>$larrdata['Examsession'],
           					'Status'=>0,
                            'Upddate'=>date("Y-m-d H:i:s")
						);			
	        $db->insert($table,$postData);
    	
    	
    }
public function updatesession($IDApp)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData['StudentId'] = 1;	
		 $where = "IDApplication = '".$IDApp."'"; 	
		 $db->update('tbl_studentapplication',$larrformData,$where);
    }
	public function checksessionexist($IDApp)
	{
	      $db =  Zend_Db_Table::getDefaultAdapter();  
    	  $select = $db->select()
		            ->from(array('a'=>'tbl_studentapplication'),array("a.*"))
		            ->where("a.IDApplication = '$IDApp'")
					->where("a.StudentId =1");
		    $result = $db->fetchRow($select);
		    return $result;
	}
    
}
