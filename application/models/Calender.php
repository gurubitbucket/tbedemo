<?php
class App_Model_Calender extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_venuedateschedule';

	public function fnGetCountSession($idvenue,$date){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT count(ast.dayk) as count,ast.dayk ,ast.idsession    FROM ( SELECT DAYNAME(date) as dayk ,idsession   FROM `tbl_venuedateschedule`  GROUP BY DAYNAME(date),idsession  )ast GROUP BY ast.dayk";
										   

 
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
	public function fnGetCountSessiondays($idvenue,$date){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_venuedateschedule"),array("DAYNAME(a.date) as Days","a.idsession","a.idsession"))
										   ->where("a.idvenue  = ?",$idvenue)
										   ->where("Year(a.date) = ?",$date);	

				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}		

}