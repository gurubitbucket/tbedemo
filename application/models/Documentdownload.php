<?php
class App_Model_Documentdownload extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_documentsuploads';
	
	public function fnGetUploadfiles() {					
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("tbl_documentsuploads"=>"tbl_documentsuploads"),array("tbl_documentsuploads.*"));
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	}
	public function fnGetUploadfilesss($idapplication)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("tbl_documentsuploads"=>"tbl_documentsuploads"),array("tbl_documentsuploads.*"))
				 				 ->where("tbl_documentsuploads.idapplication = ?",$idapplication);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
}
