<?php
class App_Model_Centerloginnew extends Zend_Db_Table {
	//protected $_name = 'tbl_batchmaster'; // table name
    
	public function fnGetValuesRow($strQry)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();				  	
			$larrResult = $lobjDbAdpt->fetchRow("$sqlQry");
			return $larrResult;
	}
	
	public function fnGetValues($strQry)
	{
		 $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$result = $db->fetchAll("$strQry");   
			return $result;
	}
	
    public function fnInsert($strSql)
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
	     $db->$strSql;
    }
	
	
	
	public function fngetactivetotaltime($crsid)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()  
			         ->from(array("a"=>"tbl_batchmaster"),array("a.*"))
                     ->join(array("b"=>"tbl_tosmaster"),'a.IdBatch=b.IdBatch',array("b.*"))
                     ->where("b.Active=1")
                     ->where("a.IdProgramMaster=?",$crsid);                
	$result = $db->fetchAll($select);
	
	return $result;
    }
 public function fngetCenterDetails($idcenter) { //Function for the view user 
      $db = Zend_Db_Table::getDefaultAdapter();
	  $select = $db->select()  
			->from(array("a"=>"tbl_center"),array("a.*"))
           ->where("a.idcenter =?",$idcenter);
	 $result = $db->fetchRow($select);
	 return $result;
    }
  public function updatecenterpass($password,$idcomp)
     {
     	 $db = Zend_Db_Table::getDefaultAdapter();
     	$data = array('centerpassword' => $password);
		$where['idcenter = ?']= $idcomp;		
		$db->update('tbl_center', $data, $where);	
     }
   
    public function fngetprogramDetails($idcenter,$idmanagesession,$presntdate)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_studentapplication"),array("a.*"))
                     ->join(array("b"=>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array("b.*"))
                     ->where("a.Examvenue =?",$idcenter)
                     ->where("a.Payment =1")
                     ->where("a.Examsession =?",$idmanagesession)
                     ->where("a.DateTime =?",$presntdate)
                     ->group('a.Program'); 
	$result = $db->fetchAll($select);
	return $result;
    }

    public function fngetcountofsessions($NewCity,$year)
    {
       $db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_newschedulervenue"),array("a.idvenue"))
			         ->join(array("b"=>"tbl_newscheduler"),'a.idnewscheduler=b.idnewscheduler',array(""))
			         ->join(array("c"=>"tbl_newschedulersession"),'b.idnewscheduler=c.idnewscheduler',array("COUNT(c.idmanagesession) as countidmanagesession","c.idnewscheduler"))
			          ->join(array("d"=>"tbl_newschedulerdays"),'a.idnewscheduler=d.idnewscheduler',array("d.*"))
                     ->where("a.idvenue =?",$NewCity)
                     ->where("b.Active =1")
                     ->where("b.Year =?",$year)
                     ->group("d.days"); 
	$result = $db->fetchAll($select);
	return $result;
    } 
    
    
    
    public function fninsertintotemp($idcenter,$session,$crsid,$startedmin,$totaltimerequired,$date,$idSession)
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_centerstartexam";
          $postData = array(		
							'idcenter' => $idcenter,	
           					'sessionid' =>$session,
                            'Program'=>$crsid,
            				'Totaltime'=>$totaltimerequired,
                            'Startedtime'=>$startedmin,
          					'idSession'=>$idSession,
          					'ExamDate'=>date('Y-m-d'),
          					'ExamStartTime'=>date('H:i:s'),
          					'Date'=>$date
            																
						);
	       $db->insert($table,$postData);
    }
 /////////////////////////////////////////////////////
    public function fngetsessionDetailsBasedOnCenter($idcenter,$days,$presentyear)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()  
				         ->from(array("a"=>"tbl_newschedulervenue"),array(""))
				         ->join(array("k"=>"tbl_newscheduler"),'a.idnewscheduler=k.idnewscheduler',array(""))
	                     ->join(array("b"=>"tbl_newschedulerdays"),'a.idnewscheduler=b.idnewscheduler',array("b.idnewscheduler"))
	                     ->join(array("c"=>"tbl_newschedulersession"),'b.idnewscheduler=c.idnewscheduler',array(""))
	                          ->join(array("d"=>"tbl_managesession"),'c.idmanagesession=d.idmangesession',array("d.*"))
	                          ->where("k.Active =1")
	                          ->where("a.idvenue =?",$idcenter)
	                           ->where("k.Year =?",$presentyear)
	                          ->where("b.Days =?",$days); 
		$result = $db->fetchAll($select);
		return $result;
    }
    
    public function fngetsessionDetailsBasedOnCenterIds($presentyear,$idcenter)
    {
 
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					  ->from(array("a" => "tbl_venuedateschedule"),array("a.date","a.Allotedseats","a.idsession","a.idnewscheduler"))
					  ->join(array("b"=>"tbl_managesession"),'a.idsession=b.idmangesession',array("b.*"))
                      ->where("a.idvenue=?",$idcenter)
                      ->where("a.date =  ?",$presentyear)
                             ->where("a.Active =  1");
                        //->group("b.idmangesession");    

        
        //$lstrSelect = "SELECT a.date,a.idsession,a.idnewscheduler,b.* FROM tbl_venuedateschedule as a,tbl_managesession b  WHERE a.idsession=b.idmangesession and  a.idvenue=$idcenter and a.date ='$presentyear' and a.Active =  1
 	     //   union  SELECT a.date,a.idsession,a.idnewscheduler,b.* FROM tbl_venuedateschedule as a,tbl_managesession b,tbl_studentapplication as c  WHERE a.idsession=b.idmangesession and a.idnewscheduler=c.Year and  a.idvenue=$idcenter and a.date ='$presentyear' and a.Allotedseats>0 and c.Examvenue!=000 and c.Examsession!=000";
	//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
    }
  public function fngetsessionDetailsBasedOnCenterIdsInactive($presentyear,$idcenter)
    {
 
        $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	
        
        $lstrSelect = "SELECT a.date,a.idsession,a.idnewscheduler,b.* FROM tbl_venuedateschedule as a,tbl_managesession b,tbl_studentapplication as c  WHERE a.idsession=b.idmangesession and a.idnewscheduler=c.Year and  a.idvenue=$idcenter and a.date ='$presentyear' and a.Allotedseats>0 and c.Examvenue!=000 and c.Examsession!=000 and a.Active=0 group by a.idsession";
	//echo $lstrSelect;die();
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
    }
    
        public function fncheckvalidate($idsession,$idcenter)
    {
       $db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_centerstartexam"),array("a.*"))
                     ->where("a.idcenter =?",$idcenter)
                      ->where("a.idSession =?",$idsession)
                     ->where("a.ExamDate =?",date('Y-m-d')); 
                    // echo $select;die();
	$result = $db->fetchAll($select);
	return $result;
    } 
    
    public function fngetprogramCount($idcenter,$idmanagesession,$idprogram,$presntdate)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_studentapplication"),array("a.*"))
                     ->where("a.Examvenue =?",$idcenter)
                     ->where("a.Payment =1")
                     ->where("a.Examsession =?",$idmanagesession)
                     ->where("a.Program =?",$idprogram)
                     ->where("a.DateTime =?",$presntdate); 
	$result = $db->fetchAll($select);
	return $result;
    }
    
    public function fngetdays($idvenue,$month,$year)
{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
	 $lstrSelect = "Select a.*,b.*
from tbl_newscheduler as a,tbl_newschedulerdays as b
where a.idnewscheduler=b.idnewscheduler and a.idnewscheduler in (
SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
and  a.Active=1 and a.From=$month and a.Year=$year";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}
public function fngetdaysto($idvenue,$month,$year)
{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
 $lstrSelect = "Select a.*,b.*
from tbl_newscheduler as a,tbl_newschedulerdays as b
where a.idnewscheduler=b.idnewscheduler and a.idnewscheduler in (
SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
and  a.Active=1 and a.To=$month and a.Year=$year";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}

public function fngetdaysbetween($idvenue,$month,$year)
{
		 $lobjDbAdpt 	= 	Zend_Db_Table::getDefaultAdapter();	
  $lstrSelect = "Select a.*,b.*
from tbl_newscheduler as a,tbl_newschedulerdays as b,tbl_newmonths as c
where a.idnewscheduler=b.idnewscheduler
and a.idnewscheduler in (
SELECT idnewscheduler FROM tbl_newschedulervenue where idvenue=$idvenue)
and  a.Active=1 and a.From<=$month and a.To>=$month and a.Year=$year";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
  	return $larrResult;
}

    public function fngetprogramDetailsbyschedular($idschedular)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_newschedulercourse"),array("a.*"))
                     ->join(array("b"=>"tbl_programmaster"),'a.IdProgramMaster=b.IdProgrammaster',array("b.*"))
                     ->where("a.idnewscheduler=?",$idschedular); 
	$result = $db->fetchAll($select);
	return $result;
    }
    
    public function fngetsessionDetails($idcenter,$presntdate)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_studentapplication"),array("a.*"))
                     ->join(array("b"=>"tbl_managesession"),'a.Examsession=b.idmangesession',array("b.*"))
                     ->where("a.Examvenue =?",$idcenter)
                     ->where("a.Payment =1")
                     ->where("a.DateTime =?",$presntdate)
                     ->order('b.starttime')
                     ->group('a.Examsession'); 
	$result = $db->fetchAll($select);
	return $result;
    }

 public function fnALreadyExamStarted($idcenter,$idprogram,$issession,$examdate) { //Function for the view user 
      $db = Zend_Db_Table::getDefaultAdapter();
	  $select = $db->select()  
			->from(array("a"=>"tbl_centerstartexam"),array("a.idcenterstartexam"))
           ->where("a.idcenter =?",$idcenter)
           ->where("a.Program =?",$idprogram)
           ->where("a.idSession =?",$issession)
           ->where("a.ExamDate =?",$examdate);
	 $result = $db->fetchRow($select);
	 return $result;
    }
    
    
	public function fnGetSessionforthesechedulers($Year,$Newvenue)
	{
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				   $lstrSelect = $lobjDbAdpt->select()
							 			  ->from(array("a" => "tbl_venuedateschedule"),array("count(a.idsession) as total"))
										  //->join(array("b"=>"tbl_studentapplication"),'a.date=b.DateTime',array(""))
										  //->where("a.Allotedseats>0")
                                          ->where("a.idvenue=?",$Newvenue)
                                          ->where("year(a.date) =  ?",$Year)
                                           ->where("a.Active=1")
                                       	  ->where("a.date >= curdate()")
                                       	  // ->where("a.date in (select date from tbl_venuedateschedule where Allotedseats>0 )")
                                       	 // ->where("b.Examvenue!=000")
                                          ->group("a.date");          
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		
		
	}
}