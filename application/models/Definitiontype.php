<?php
class App_Model_Definitiontype extends Zend_Db_Table {
	
	protected $_name = 'tbl_definationtypems';
   
	public function fnPagination($larrresult,$page,$lintpagecount) { // Function for pagination
		$paginator = Zend_Paginator::factory($larrresult); //instance of the pagination
		$paginator->setItemCountPerPage($lintpagecount);
		$paginator->setCurrentPageNumber($page);
		return $paginator;
	}
	
	public function fnGetDefinationMs($defms) { 
		$select = $this->select()
							->setIntegrityCheck(false) 
							->join(array('dtms' => 'tbl_definationtypems'),array())
                       		->join(array('dms' => 'tbl_definationms'),'dms.idDefType = dtms.idDefType',array('key' => 'dms.idDefinition','value' => 'dms.DefinitionDesc'))
                       		->where('dtms.defTypeDesc = ?', $defms)
                       		->order('dms.idDefinition');               
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
		
	public function fnGetDefinations($defms) { 
		$select = $this->select()
							->setIntegrityCheck(false) 
							->join(array('dtms' => 'tbl_definationtypems'),array())
                       		->join(array('dms' => 'tbl_definationms'),'dms.idDefType = dtms.idDefType')
                       		->where('dtms.defTypeDesc = ?', $defms)
                       		->order('dms.idDefinition');               
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	
	
	public function fnGetDefinationMs2($defms) { 
		$select = $this->select()
							->setIntegrityCheck(false) 
							->join(array('dtms' => 'tbl_definationtypems'),array())
                       		->join(array('dms' => 'tbl_definationms'),'dms.idDefType = dtms.idDefType',
                      		 array('key' => 'dms.DefinitionCode','value' => 'dms.DefinitionDesc'))
                       		->where('dtms.defTypeDesc = ?', $defms)
                       		->order('dms.idDefinition');               
		$result = $this->fetchAll($select);
		return $result->toArray();
	}
	
	
	public function fnGetDefinationTypeString($idDefType) { 
  	$db = Zend_Db_Table::getDefaultAdapter();
		$select = $this->select()
							->setIntegrityCheck(false) 
							->from('tbl_definationtypems')
                       		->where('idDefType = ?', $idDefType);             
		$result = $db->fetchRow($select);
		return $result;
	}
	
    public function getLanguageDetailms() {
	$select = $this->select()
			->setIntegrityCheck(false)  
			->join(array('a' => 'tbl_definationms'),array('idDefType'))
            ->join(array('b'=>'tbl_definationtypems'),'a.idDefType = b.idDefType')
           ->where ('b.defTypeDesc = ?',"Languages");
	$result = $this->fetchAll($select);
	return $result->toArray();
    }

               
}
