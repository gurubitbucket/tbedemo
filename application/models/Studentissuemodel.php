<?php
class App_Model_Studentissuemodel  extends Zend_Db_Table { //Model Class for Users Details
	

	protected $_name = 'tbl_studentapplication';
	
	 /*
      * function to fetch all the details
      */
	
	public function fngetpaylater($idcomp) {
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_takafulpaymenttype" ), array ("a.idtakafulpaymenttype" ) )
											->join ( array ("b" => "tbl_definationms" ), 'a.paymenttype=b.idDefinition', array ("b.idDefinition", "b.DefinitionDesc" ) )
											->where ( "a.idtakafuloperator=?", $idcomp )
											->where ( "a.paymenttype= 181" );
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
	}
	public function fninsertissues($formdata,$upddate) {
	      $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_issuelist";
          $postData = array(	
                             'Issue'=>$formdata['Issue'],
          					 'Active'=>$formdata['Active'],
          					 'UpdUser'=>1,
                             'UpdDate'=>$upddate							 
						);			
	       $db->insert($table,$postData);
	}
	public function fngetdetails($formdata) {
	      $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_issuelist" ), array ("a.*","DATE_FORMAT(a.UpdDate,'%d-%m-%Y') as Date") )
											//->join ( array ("b" => "tbl_definationms" ), 'a.paymenttype=b.idDefinition', array ("b.idDefinition", "b.DefinitionDesc" ) )
											 ->where('a.Issue  like "%" ? "%"',$formdata['Issue'])
											 ->where( "a.Active=?", $formdata['Active']);
											
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect );
		return $larrResult;
	}
	public function fnGetissueList($Idissue){
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_issuelist"),array("a.*"))
				 				 ->where("a.Idissue = ?",$Idissue);
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	public function fnupdateissue($formdata,$idissue)
	{
	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		
		$data = array(
		              'Issue'=>$formdata['Issue'],
		              'UpdDate'=>date ( 'Y-m-d H:i:s' ),
		              'UpdUser' =>$formdata['UpdUser'],
            		  'Active' =>$formdata['Active']	
		              
		);
		$where['Idissue = ? ']= $idissue;
		return $lobjDbAdpt->update('tbl_issuelist', $data, $where);
	}
	
}