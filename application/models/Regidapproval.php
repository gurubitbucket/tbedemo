<?php
class App_Model_Regidapproval extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	
	public function fnGetCompanyDetails()
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
											  ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany')
											  ->join(array("c" =>"tbl_studentpaymentoption"),'c.IDApplication=a.idBatchRegistration')
											  ->where("a.paymentStatus =2")
											  ->where("a.Approved=0")
											  ->where("c.companyflag=1")
											  ->where("c.Modeofpayment=4");
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
    
	
	
	public function fnGetStudent($lstrType)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											 ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
											  ->join(array("f" =>"tbl_programmaster"),'b.Program=f.IdProgrammaster')
											  ->join(array("c" =>"tbl_state"),'b.ExamState=c.idState',array("c.*"))
											  ->join(array("d" =>"tbl_city"),'b.ExamCity=d.idCity',array("d.*"))
											   ->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
											  ->where("a.Approved=1")
											    ->where("a.Cetreapproval=0")
											  //->where("a.Registrationpin=0000000")
											  ->where("b.IDApplication = ?",$lstrType);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	 
	public function fnGetProgramList($date)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											 ->from(array("a" =>"tbl_studentapplication"))
											 ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array("key"=>"b.IdProgrammaster","value"=>"b.ProgramName"))
											 ->where("a.DateTime = ?",$date)	
											 ->group('a.Program');
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	 }

	/*public function fnSearchstudent($name,$icno,$idcenter)
	{
		//////////////todays/////////////////////
		$date =  date('d');
  		
  		$month = date('m');
  		
  		if($month<10)
  		{
  			$month = $month[1];

  		}
  		
	  if($date<10)
  		{
  			$date = $date[1];
  		}
  		$year = date('Y');
  		/////////////todays///////////////////////////
		 //$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_studentapplication"))
											  ->join(array("b"=>"tbl_registereddetails"),'a.IDApplication=b.IDApplication')
											    ->join(array("c"=>"tbl_newscheduler"),'c.idnewscheduler=a.Year')
											     ->join(array("e" =>"tbl_programmaster"),'a.Program=e.IdProgrammaster',array('e.*'))
											    ->where("b.Approved=1")
											   ->where("a.Examdate =?",$date)
											  ->where("c.Year=?",$year)
											  ->where("a.Exammonth=?",$month)
											  ->where("b.Cetreapproval=0")
											  ->where("a.Examvenue =?",$idcenter)
											   ->where('a.ICNO like "%" ? "%"',$icno)
											   ->where('a.FName like "%" ? "%"',$name);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}*/
    
    public function fnGetprogram()
    {
  			    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 			
	            $lstrSelect="SELECT `a`.`IdProgrammaster` AS `key`,`a`.`ProgramName` AS `value` FROM `tbl_programmaster` as a";			
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
    }
	
	public function fnSearchstudent($larrformData,$idcenter)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_studentapplication"))
											  ->join(array("b"=>"tbl_registereddetails"),'a.IDApplication=b.IDApplication')
											  ->join(array("c"=>"tbl_newscheduler"),'c.idnewscheduler=a.Year')
											  ->join(array("e" =>"tbl_programmaster"),'a.Program=e.IdProgrammaster',array('e.*'))
											   ->where("b.Approved=1")
											  ->where("b.Cetreapproval=1")
											   ->where("a.StudentId=1")
											  ->where("a.Examvenue =?",$idcenter);
							if($larrformData['field4']) $lstrSelect->where('a.ICNO like "%" ? "%"',$larrformData['field4']);
							if($larrformData['field3']) $lstrSelect->where('a.FName like "%" ? "%"',$larrformData['field3']);
							if($larrformData['field21'])$lstrSelect->where("a.DateTime =?",$larrformData['field21']);
							if($larrformData['field5'])$lstrSelect->where("a.Program =?",$larrformData['field5']);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	public function fnStudentApproved($larrformdata)
	{
 	    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$tableName = "tbl_studentapplication";
	    for($i = 0; $i<count($larrformdata['IDApp']); $i++ )
			{		
				$idappln = $larrformdata['IDApp'][$i];
				$postData = array('StudentId' => 00);			
                $where = "IDApplication=$idappln";
				$lobjDbAdpt->update($tableName,$postData,$where);
				//self::fnGetStudentapproved($idBatchRegistration);
			}
	 }
	 public function fnGetStudentdetails($date,$month,$year,$center)	
    {
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											 ->from(array("a" =>"tbl_studentapplication"),array('a.*','date(a.UpdDate) as dares'))
											  ->join(array("b"=>"tbl_registereddetails"),'a.IDApplication=b.IDApplication')
											   ->join(array("e" =>"tbl_programmaster"),'a.Program=e.IdProgrammaster',array('e.*'))
											  ->join(array("c"=>"tbl_newscheduler"),'c.idnewscheduler=a.Year')
											  ->where("b.Approved=1")
											  ->where("a.StudentId=1")
											  //->where("a.Examdate =?",$date)
											  //->where("c.Year=?",$year)
											  //->where("a.Exammonth=?",$month)
											  ->where("b.Cetreapproval=1")
											  ->where("a.Examvenue =?",$center);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	 }
	
}
