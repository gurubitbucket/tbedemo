<?php
class App_Model_Studentedit extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
/*
 * function to fetch all the batch details
 */
	public function fnGetRegid($regid)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_registereddetails"),array("a.*"))
										  ->join(array("b"=>"tbl_studentapplication"),'a.IDApplication=b.IDApplication',array("b.*"))
										  ->join(array("c"=>"tbl_programmaster"),'c.IdProgrammaster=b.Program',array("c.*"))
										  ->join(array("d"=>"tbl_state"),'b.ExamState=d.idState',array("d.*"))
										  ->join(array("e"=>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
										  ->where("a.Regid  = '$regid'"); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
				return $larrResult;
	}
	
	public function fnGetDisabledate($city)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_schedulerexception"),array("a.*"))
										  ->where("a.idcity  = ?",$city);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	public function fnGetBatchName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchmaster"),array("key"=>"a.IdBatch","value"=>"a.BatchName"))
										  ->where("a.BatchStatus  = ?","0");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}	

	public function fnGetStateName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_state"),array("key"=>"a.idState","value"=>"a.StateName"))
										   ->where("a.idCountry  = ?","121")
										   ->order("a.StateName");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
			
			public function fngetminimumage()
			{
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_config"),array("a.MinAge"));
										  //->where("a.idCountry  = ?","121");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}
 /*
  * funtion to fetch all the venue based on the batch
  */			
  public function fnGetVenueName($lintidscheduler)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"b.idschedulervenue","value"=>"a.centername"))
										  ->join(array("b" =>"tbl_schedulervenue"),'a.idcenter = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulermaster"),'b.idschedulermaster = c.idschedulermaster',array())
										  ->where("c.idschedulermaster  = ?",$lintidscheduler)
										  ->order("a.city");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  /*
   * function to fetch the timings based on the venue("CONCAT(TRIM(a.FirstNm),'_',IFNULL(TRIM(a.MiidleNM),''),'',IFNULL(TRIM(a.LastNM),'')) AS EmployeeName")
   */
public function fnGetVenueTime($idvenue)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
/* 	echo 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("key"=>"a.idschedulervenuetime","value"=>("distinct(a.Date) as Date")))
										 // ->join(array("b" =>"tbl_center"),'a.idcentre = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulervenue"),'a.idschedulervenue = c.idschedulervenue',array())
										  ->where("c.idschedulervenue  = ?",$idvenue);die();*/
 	$lstrSelect="SELECT (a.Date)as `key`, DATE_FORMAT(a.Date,'%d-%m-%Y') as value FROM `tbl_schedulervenuetime` AS `a`
 INNER JOIN `tbl_schedulervenue` AS `c` ON a.idschedulervenue = c.idschedulervenue WHERE (c.idschedulervenue  = $idvenue)group by value";
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }

	public function fnGetInitialConfigDetails($iduniversity) {
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()
					->from(array("a" => "tbl_config"),array("a.*"))				
		            ->where("a.idUniversity = ?",$iduniversity);	
		 return $result = $lobjDbAdpt->fetchRow($select);
	}
	
  public function fnGetbatch($idvenue,$datetocmp)
  {
  			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
/*			echo $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchmaster"),array("key"=>"a.IdBatch","value"=>("a.BatchName")))
										  ->join(array("b" =>"tbl_programmaster"),'a.IdProgrammaster = b.IdProgrammaster',array())
										  ->where("b.IdProgrammaster  = ?",$idvenue);die();*/
  			
	 $lstrSelect="SELECT `a`.`IdBatch` AS `key`, CONCAT(DATE_FORMAT(`a`.`BatchFrom`,'%d-%m-%Y'),'---',DATE_FORMAT(`a`.`BatchTo`,'%d-%m-%Y')) AS `value` FROM `tbl_batchmaster` AS `a` INNER JOIN `tbl_programmaster` AS `b` ON a.IdProgrammaster = b.IdProgrammaster WHERE (b.IdProgrammaster =$idvenue) and  a.BatchFrom > '$datetocmp'";			
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  /*
   * function to fetch all the scheduler based on the prog
   */
 public function fnGetscheduler($idbatch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("key"=>"a.idschedulermaster","value"=>("a.ScheduleName")))
										  ->where("a.idBatch  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  
 public function fnGetDefinations($idbatch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("key"=>"a.idschedulermaster","value"=>("a.ScheduleName")))
										  ->where("a.idBatch  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  /*
   * function to get the time
   */
  public function fnGetTimings($date,$venue)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("key"=>"a.idschedulervenuetime","value"=>("a.From")))
										  ->where("a.Date = '$date'")
										  ->where("a.idschedulervenue=?",$venue);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  /*
   * fun;ction to insert into the table
   */
   public function fnAddStudent($larrformData,$idapplication) { //Function to get the user details
   	
   /*	echo "<pre/>";
   	print_r($larrformData);
   	die();*/
   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	$table = "tbl_studentapplication";
    	 $where = "IDApplication = '".$idapplication."'"; 	
		  $postData = array(		
							'StudentId' =>'00',	
           					'FName' =>$larrformData['FName'],	
           					'MName' => '', 
							'LName' =>'',	
           					'DateOfBirth' =>$larrformData['DateOfBirth'],	
           					'PermCity' => '0', 
           					'EmailAddress' =>$larrformData['EmailAddress'],	
           					'UpdDate' => $larrformData['UpdDate'], 
							'UpdUser' =>$larrformData['UpdUser'],	
           					//'IdBatch' =>$larrformData['setactive'],	
           					'Venue' => 0, 
							'VenueTime' =>0,	
           					//'Program' =>$larrformData['Program'],	
           					'idschedulermaster' => 0, 
							//'Amount' =>$larrformData['Amount'],	
           					//'ICNO' =>$larrformData['ICNO'],	
           					'Payment' => 0, 
							'DateTime' =>0,	
           					'PermAddressDetails' =>$larrformData['PermAddressDetails'],	
           					'Takafuloperator' =>$larrformData['Takafuloperator'], 
							'VenueChange' =>0,	
           					'ArmyNo' =>0,	
           					'batchpayment' => 0, 
							'Gender' =>$larrformData['Gender'],	
           					'Race' =>$larrformData['Race'],	
           					'Qualification' =>$larrformData['Qualification'], 
							'State' =>$larrformData['State'],	
           					'CorrAddress' =>$larrformData['CorrAddress'],	
           					'PostalCode' =>$larrformData['PostalCode'], 
		 					'ContactNo' =>$larrformData['ContactNo'],	
           					'MobileNo' =>$larrformData['MobileNo'], 
		  					//'ExamState'=>$larrformData['NewState'],
		  					//'ExamCity'=>$larrformData['NewCity'],
		  					//'Year'=>$larrformData['Year'],
		  					//'Examdate'=>$larrformData['setdate'],
		  					//'Exammonth'=>$larrformData['setmonth'],
		  					//'Examvenue'=>'000'			  
						);
						 $lobjDbAdpt->update('tbl_studentapplication',$postData,$where);
     }
     
    public function fnupdateexamvenue($idvenue,$idapplication)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$larrformData1['Examvenue'] = $idvenue;	
		 $where = "IDApplication = '".$idapplication."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
    }
     
 public function  fnInsertStudentPaymentdetails($mod,$lastInsId)
  {
  
  	 $db = Zend_Db_Table::getDefaultAdapter();
  	$table = "tbl_studentpaymentoption";
		  $postData = array(		
							'IDApplication' =>$lastInsId,	
           					'ModeofPayment' =>$mod,	
           					'companyflag' => 0,           				                           
                            /*'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
            				 'AdhocDate' => '0000-00-00',	
            				'AdhocVenue' =>'others'			*/		
						);
						  $db->insert($table,$postData);
						   $lastids  = $db->lastInsertId("tbl_studentpaymentoption","idstudentpaymentoption");	
						   return $lastids;
						 
							
  }
  
  public function fnGetVenuedetails($year,$prog,$city)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("f"=>"tbl_newschedulersession"),'a.idnewscheduler=f.idnewscheduler')
										  ->join(array("g"=>"tbl_managesession"),'f.idmanagesession=g.idmangesession')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array("d.*"))
										  ->join(array("e"=>"tbl_city"),'d.city=e.idcity',array("e.*"))
										  ->where("c.IdProgramMaster  = ?",$prog)
										  ->where("e.idCity =?",$city)
										  ->group("f.idmanagesession")
										  ->where("a.Year =?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  public function fngetmodeofpayment($mod)
  {
  	 $db = Zend_Db_Table::getDefaultAdapter();
  	 $lstrSelect = $db->select()
				->from(array("a" => "tbl_studentpaymentoption"),array("a.ModeofPayment"))
				 ->where("a.idstudentpaymentoption = ?",$mod);	
				$larrResult = $db->fetchRow($lstrSelect);
						  return $larrResult;	
  }
     public function fnGetRulesRegulation($id)
     {
     	 $db = Zend_Db_Table::getDefaultAdapter();
  	           $lstrSelect = $db->select()
				->from(array("a" => "tbl_termsandcondition"),array("a.Terms"))
				 ->where("a.studenttype = ?",$id);	
				$larrResult = $db->fetchAll($lstrSelect);
						  return $larrResult;	
     }
     /*
      * function to fetch all the details
      */
  public function fngetStudentDetails() { //Function to get the user details
        $result = $this->fetchAll();
        return $result->toArray();
     }
     
     /*
      * function to fetch all the details
      */
     public function fnviewstudentdetails($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
 public function fnviewstudentdetailssss($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.*'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.*'))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
     /*
      * function to update the student details
      */
     public function updatestudent($lintstudentid,$larrformData)
     {
     	/*$larrformData['HomePhone'] = $larrformData['homecountrycode']."-".$larrformData['homestatecode']."-".$larrformData['HomePhone'];
		$larrformData['CellPhone'] = $larrformData['countrycode']."-".$larrformData['statecode']."-".$larrformData['CellPhone'];
		unset($larrformData['countrycode']);
		unset($larrformData['statecode']);
		unset($larrformData['homecountrycode']);
		unset($larrformData['homestatecode']);*/
     	
     	$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$where = "IDApplication = '".$lintstudentid."'"; 	
		return $db->update('tbl_studentapplication',$larrformData,$where);
     }
     
     /*
      * functin to search the student
      */
	public function fnSearchStudent($post = array()) { //Function for searching the user details
    	$db = Zend_Db_Table::getDefaultAdapter();
		//$field7 = "Active = ".$post["field7"];
		$select = $db->select() 	
			   ->join(array('a' => 'tbl_studentapplication'),array('a.*'))
				->where('a.LName like "%" ? "%"',$post['field3'])
			   ->where('a.FName like  "%" ? "%"',$post['field2'])
			   ->where('a.MName like "%" ? "%"',$post['field4']);
			   //
			  // ->where($field7);exit;
		$result = $db->fetchAll($select);
		return $result;
	}
	/*
	 * function for fetching details
	 * 
	 */
         public function fnGetStudentName($idstudent) {
			$db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT CONCAT(FName,' ',IFNULL(MName,' '),' ',IFNULL(LName,' ')) AS Name  FROM tbl_studentapplication WHERE IDApplication = $idstudent";
    		$result = $db->fetchRow($sql);    
			return $result;
    	}
    	/*
    	 * Paypal Entries
    	 * */
  public function fnInsertPaypaldetails($larrformData,$studentId){
  	//print_r($larrformData);exit;
  		 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_paypaldetails";
            $postData = array(		
							'IDApplication' => $studentId,	
           					'paymentFee' =>$larrformData['mc_gross'],	
           					'grossAmount' => $larrformData['mc_gross'],	
            				'payerId' =>$larrformData['payer_email'],		
		            		'transactionId' =>$larrformData['txn_id'],  
            				'verifySign' =>$larrformData['verify_sign'],                            
                            'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
                            'paymentStatus'=> 1											
						);	
					
	     $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_paypaldetails","idpaypalDetails");
		 
		
		 $larrformData1['Payment'] = 1;	
		 $where = "IDApplication = '".$studentId."'"; 	
		 $db->update('tbl_studentapplication',$larrformData1,$where);
		 
		 
		 
		 $lstrSelect = $db->select()
						  ->from(array("a" =>"tbl_studentapplication"),array('a.IdBatch','a.Takafuloperator'))
						  ->where("a.IDApplication  = ?",$studentId);	
		 $larrResult = $db->fetchRow($lstrSelect);	
		 	 
		 $ModelBatchlogin = new App_Model_Batchlogin();
		 $Regid = $ModelBatchlogin->fnGenerateCode($larrResult['Takafuloperator'],$studentId);	
		 	 
		 $table = "tbl_registereddetails";
         $postData = array('Regid' =>   $Regid,	
           					'IdBatch' =>$larrResult['IdBatch'],	
         					'Approved' =>0,	
         					'RegistrationPin'=>'0000000',
         					'Cetreapproval'=>'0',
           					'IDApplication' => $studentId);					
	     $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_registereddetails","idregistereddetails");
		 return $Regid;
  	
  } 	
/*
 * function to fetch all the Prog Name
 */
public function fnGetProgramName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										  
										  ->join(array("c" => "tbl_program"),'c.IdProgram=a.idprog')
										  ->join(array("b"=>"tbl_batchmaster"),'b.IdProgrammaster=a.IdProgrammaster',array())
										  ->join(array("d"=>"tbl_tosmaster"),'b.IdBatch=d.IdBatch',array())
										  ->where("d.Active=1")
										  ->where("c.Active =1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
/*
 * function to fetch all the Prog Name
 */
	public function fnTakafuloperator(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
		
	/*
	 * function to fetch all the amount for that course
	 */
	
	public function fnGetProgAmount($idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT sum(abc.amount) from 

(SELECT max(a.EffectiveDate),a.Rate as amount  from tbl_programrate a,tbl_accountmaster b
where a.idProgram=$idprog and 
 b.idAccount= a.IdAccountmaster and  
 b.Active=1 group by a.IdAccountmaster) as abc ";
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	/*
	 * 
	 */
	public function fnGetTimingsForDate($date,$idscheduler)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect="SELECT idschedulervenuetime as `key`,`From` as value FROM `tbl_schedulervenuetime` WHERE `idschedulervenue`=$idscheduler and Date='$date'";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	/*
	 * functin to fetch theno of seats
	 */
	public function fnGetNoOfSeats($lintidvenuetime)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("a.NoofSeats"))
										  //->from(array("b" =>"tbl_schedulervenuetime"),'a.idschedulervenue = b.idschedulervenue',array())
										  //->where("b.Date  = ?",$lintdate)
										    //->where("b.idcentre  = ?",$lintidvenue)
										    ->where("a.idschedulervenuetime =?",$lintidvenuetime);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	/*
	 * function to get theno of students aplied for the same exam
	 */
  public function fnGetNoofStudents($lintidvenuetime)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$select ="select count(IDApplication) from tbl_studentapplication where DateTime=$lintidvenuetime";
  	$larrResult = $lobjDbAdpt->fetchRow($select);
				return $larrResult;
  }
  
  public function fnGetNoOfTakafulOperatorForStudent($idtakaful)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$select ="select count(IDApplication) from tbl_studentapplication where Takafuloperator=$idtakaful";
  	$larrResult = $lobjDbAdpt->fetchRow($select);
				return $larrResult;
  }
  
  public function fnGetNoOfTakafulOperator($idtakaful)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$select ="select NumberofSeat from tbl_takafuloperator where idtakafuloperator=$idtakaful";
  	$larrResult = $lobjDbAdpt->fetchRow($select);
				return $larrResult;
  }
	public function fnGetSMTPSettings(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_config"),array("a.SMTPServer","a.SMTPUsername","a.SMTPPassword","a.SMTPPort","a.SSL","a.DefaultEmail") );
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}	
		
		
public function fnGetICorArmyNo()
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.ICNO","a.ArmyNo"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
 public function fnGetStudentdetailsbasedonicno($icno)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										    ->where("a.ICNO =?",$icno);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 public function fnGetStudentdetailsbasedonarmyno($armyno)
{
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										    ->where("a.ArmyNo =?",$armyno);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 public function fngetintialconfigdetails()
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_config"),array("a.*"));
										    //->where("a.ArmyNo =?",$armyno);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 public function fnGetPreRequesition($idprog)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_programmaster"),array("a.*"))
										    ->where("a.IdProgrammaster =?",$idprog);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 
 public function fnGetPreRequesitionProgDetails($idprog)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_programmaster"),array("a.*"))
										    ->where("a.IdProgrammaster =?",$idprog);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
public function fnGetEducation()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where('a.idDefType = 14')
										  ->where('a.Active= 1');	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
 public function fnGetsameexam($lintidprog,$lintidicno)
 {
 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array("a.*"))
										    ->where("a.ICNO =?",$lintidicno)
										     ->where("a.Program =?",$lintidprog);;
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
 }
 
 ////////////////////////////////fetch all states for course/////////////////////////////////
 
 
/*
 * function to fetch all the Prog Name
 */
public function fnGetStatelistforcourse($idprog,$idyear){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->group("e.idState")
										  ->where("a.year=?",$idyear);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
 ///////////////////////////////////////////////////////////////////////////////////////////
 
public function fnGetCitylistforcourse($idstate,$idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										  ->join(array("e"=>"tbl_city"),'d.city=e.idCity',array("key"=>"e.idCity","value"=>"e.CityName"))
										  ->where("e.idState  = ?",$idstate)
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->group("e.CityName");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
public function fnGetYearlistforcourse($idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("key"=>"a.Year","value"=>"a.Year"))
										  //->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())


										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										 ->group("a.Year");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
		
public function fnGetDaysforcourse($idprog,$year){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array())
										  ->join(array("b"=>"tbl_newschedulerdays"),'a.idnewscheduler=b.idnewscheduler')
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  //->join(array("d"=>"tbl_center"),'b.idvenue=d.idcenter',array())
										 // ->join(array("e"=>"tbl_state"),'d.state=e.idState',array("key"=>"e.idState","value"=>"e.StateName"))
										  ->where("c.IdProgramMaster  = ?",$idprog)
										  ->where("a.year = ?",$year)
										  ->order("b.Days");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
public function fnGetMonths($year,$Program){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
										  ->join(array("c"=>"tbl_newschedulercourse"),'a.idnewscheduler=c.idnewscheduler')
										  ->where("c.IdProgramMaster  = ?",$Program)
										  ->where("a.year = ?",$year);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}


public function fnGetActiveSet($Program){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("a.*"))
										  ->join(array("c"=>"tbl_batchmaster"),'a.IdProgrammaster=c.IdProgrammaster')
										  ->join(array("d"=>"tbl_tosmaster"),'d.IdBatch=c.IdBatch')
										  ->where("a.IdProgramMaster  = ?",$Program)
										  ->where("d.Active=1");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
		
public function fnGetTempDays($day,$year,$month,$dateid)
{
	$sessionID = Zend_Session::getId();
	
	 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_studenttempday";
         $postData = array('day' =>$day,	
           					'year' =>$year,	
            				'month' =>$month,		
		            		'dateid' =>$dateid,
                            'sessionid'=>$sessionID			
						);	
	    $result =  $db->insert($table,$postData);
	     return $result;
}

 public function fnDeleteTempDetails($id)
 	{
 		$sessionID = Zend_Session::getId();
 		$db 	= Zend_Db_Table::getDefaultAdapter();
 		$select = "Delete FROM tbl_studenttempday  where dateid !=$id and sessionid='$sessionID'";
 		$db->query($select);
 		
 	}
 	
 public function fngetdetails()
 {
 	$sessionID = Zend_Session::getId();
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studenttempday"))
										  ->where("a.sessionid  = ?",$sessionID);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }
 
 public function fngetstudenttempdays($larrstudenttempdetails)
 {
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studenttempday"))
										  ->where("a.idstudenttempday = ?",$larrstudenttempdetails);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
 }	
 
 public function fnGetAmountInWords($Amount)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT CONCAT(ucwords(str_numtowords($Amount)),' ','Only') as Amount";		
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
	public function fnGetExamDetails($IdApplication)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->where("a.IDApplication  = ?",$IdApplication);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
 
public function fnGetEducationDetails()
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where("a.idDefType = 14");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
 
 
 
 
}