<?php
class App_Model_Introduction extends Zend_Db_Table {
	//protected $_name = 'tbl_batchmaster'; // table name
 public function fnGetIntroductionDetails($iddefinationms)
 {
 	//echo ($iddefinationms);die();
 	$db = Zend_Db_Table::getDefaultAdapter();
	$select = $db->select()
							->from('tbl_details')
                       		->where('iddefinition = ?', $iddefinationms);       
		$result = $db->fetchRow($select);
		return $result;
 }
 
 public function fnGetWebPageDetails()
 {
 	//echo ($iddefinationms);die();
 	$db = Zend_Db_Table::getDefaultAdapter();
	$select = $db->select()
							->from(array("a" => 'tbl_webpagetemplate'),array('a.*'))
                       		  ->where("a.Active  = ?","1")
							  ->order("a.Orderid");	      
		$result = $db->fetchAll($select);
		return $result;
 }
 
public function reportDetails()
 {
 	//echo ($iddefinationms);die();
 	$db = Zend_Db_Table::getDefaultAdapter();
	$select = "SELECT a.UpdDate, a.IDApplication, a.FName, a.ICNO, a.EmailAddress, b.Year, a.Exammonth, a.Examdate, f.managesessionname, f.starttime, f.endtime, d.centername, e.ProgramName, a.Payment
FROM `tbl_studentapplication` a
JOIN tbl_newscheduler b ON b.idnewscheduler = a.Year
JOIN tbl_newschedulervenue c ON c.idnewschedulervenue = a.Examvenue
JOIN tbl_center d ON d.idcenter = a.Examvenue
JOIN tbl_programmaster e ON e.IdProgrammaster = a.Program
JOIN tbl_managesession f ON f.idmangesession = a.Examsession
WHERE a.UpdDate
BETWEEN '2012-03-30 00:00:00'
AND '2012-04-02 00:00:00'
ORDER BY `a`.`UpdDate` ASC, d.centername asc";	      
		$result = $db->fetchAll($select);
		return $result;
 }
 
public function getcenter()
 {
 	//echo ($iddefinationms);die();
 	$db = Zend_Db_Table::getDefaultAdapter();
	$select = "SELECT * from tbl_center";	      
		$result = $db->fetchAll($select);
		return $result;
 }
         
}
