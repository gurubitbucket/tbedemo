<?php
class App_Model_Studentviewdetails extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';

	
	public function fngetstudentinfo($icno)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.ICNO"))
										  ->where("a.ICNO = '$icno'")
										  //->where("a.pass=3")
										   ->where("a.Examvenue!=000"); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
				return $larrResult;
	}
	
	public function fngetstudentallapliedinfo($icno)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.*"))
										  	->join(array('b' =>'tbl_programmaster'),'a.Program = b.IdProgrammaster',array("b.ProgramName"))
										  ->where("a.ICNO = '$icno'")
										 // ->where("a.pass=3")
										   ->where("a.Examvenue!=000")
										   ->order("a.Program")
										   ->order("a.pass"); 
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
				return $larrResult;
	}
	
	
 public function fngetstudentparticurinfo($idapp)
 {
 	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_studentapplication"),array("a.*"))
										  ->join(array("b" =>"tbl_programmaster"),'a.Program=b.IdProgrammaster',array('b.ProgramName'))
										   ->join(array("d" =>"tbl_newscheduler"),'a.Year=d.idnewscheduler',array('d.Year as years'))
										  ->join(array("c"=>"tbl_center"),'a.Examvenue=c.idcenter',array('c.centername','c.addr1'))
										   ->join(array("e"=>"tbl_managesession"),'a.Examsession=e.idmangesession',array('e.*'))
										  ->where("a.IDApplication =?",$idapp);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
				return $larrResult;
 	
 }
 
 public  function fngetstudentparticurregid($idapp)
 {
 	
 	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_registereddetails"),array("a.Regid"))
										  ->join(array('b' =>'tbl_studentapplication'),'a.IDApplication=b.IDApplication',array(""))
										  ->where("b.Payment=1")
										  ->where("a.IDApplication =?",$idapp); 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
				return $larrResult;
 }
 
public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}
		
		
public function fnGetValues($strQry)
	{
		 $db =  Zend_Db_Table::getDefaultAdapter();    	
    	 $result = $db->fetchAll("$strQry");   
		 return $result;
	}
	
public function fnGetValuesRow($strQry)
	{
		 $db =  Zend_Db_Table::getDefaultAdapter();    	
    	 $result = $db->fetchrow("$strQry");   
		 return $result;
	}
 
 
}