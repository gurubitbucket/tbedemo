<?php
class App_Model_Takafulcandidates extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
		 public function fngetBatchRegistration($idbatch)
		 {
		 	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_batchregistrationdetails"),array("a.*"))
							 				 ->join(array("b"=>"tbl_programmaster"),'a.idProgram= b.IdProgrammaster')
							 				 ->where("a.idBatchRegistration = ?",$idbatch);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
		 }
		 
	 public function fngetBatchDetailsforRegistrationpin($idpin)
	 {
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchregistration"),array('a.*'))
										  ->where("a.idBatchRegistration  = ?",$idpin);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	 }
		 
		 public function  fnAdhocvenue($ids)
		 {
		 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchregistration"),array('a.*'))
										  ->where("a.idBatchRegistration  = ?",$ids);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
		 }
		 
		 public function fnGetProgramName()
		 {
		 	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_programmaster',array('key' =>'IdProgrammaster','value' => 'ProgramName'));
			$result = $db->fetchAll($sql);
			return $result;
		 }
		 
		 
		 public function fnGetBatchName()
		 {
		 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter(); 			
 				$lstrSelect="SELECT `a`.`IdBatch` AS `key`, CONCAT(DATE_FORMAT(`a`.`BatchFrom`,'%d-%m-%Y'),'---',DATE_FORMAT(`a`.`BatchTo`,'%d-%m-%Y')) AS `value` FROM `tbl_batchmaster` AS `a`";			
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
		 }
		 
  /*
   * function to fetch all the scheduler based on the prog
   */
 public function fnGetscheduler($idbatch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("key"=>"a.idschedulermaster","value"=>("a.ScheduleName")))
										  ->where("a.idBatch  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
   /*
  * funtion to fetch all the venue based on the batch
  */			
  public function fnGetVenueName($lintidscheduler)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"b.idschedulervenue","value"=>"a.centername"))
										  ->join(array("b" =>"tbl_schedulervenue"),'a.idcenter = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulermaster"),'b.idschedulermaster = c.idschedulermaster',array())
										  ->where("c.idschedulermaster  = ?",$lintidscheduler)
										  ->order("a.city");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  
  public function fnGetVenueTime($idvenue)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
/* 	echo 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("key"=>"a.idschedulervenuetime","value"=>("distinct(a.Date) as Date")))
										 // ->join(array("b" =>"tbl_center"),'a.idcentre = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulervenue"),'a.idschedulervenue = c.idschedulervenue',array())
										  ->where("c.idschedulervenue  = ?",$idvenue);die();*/
 	$lstrSelect="SELECT (a.Date)as `key`,(a.Date)as value FROM `tbl_schedulervenuetime` AS `a`
 INNER JOIN `tbl_schedulervenue` AS `c` ON a.idschedulervenue = c.idschedulervenue WHERE (c.idschedulervenue  = $idvenue)group by value";
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
	/*
	 * 
	 */
	public function fnGetTimingsForDate($date,$idscheduler)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect="SELECT idschedulervenuetime as `key`,`From` as value FROM `tbl_schedulervenuetime` WHERE `idschedulervenue`=$idscheduler and Date='$date'";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	

	public function fnTakafuloperator(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnBatchProg()
	{
		       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT * FROM tbl_batchmaster where idProgrammaster in (select IdProgrammaster from tbl_programmaster) order by idProgrammaster";	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnviewstudentdetails($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
	
	public function fnInsertIntoStd($larrformData,$countloop,$batchid,$regpin)
	{
		echo "<pre/>";
		/*print_r($larrformData);
		die();*/
		
		for($i=0;$i<$countloop;$i++)
		{
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_studentapplication";
            $postData = array(		
							'FName' => $larrformData['candidatename'][$i],		
            				'EmailAddress' =>$larrformData['candidateemail'][$i],		
		            		'UpdDate' =>date('Y-m-d:H-i-s'),	
		            		'UpdUser'=>1,
            				'IdBatch'=>$larrformData['candidatebatch'][$i],
            				'Venue' => $larrformData['candidatevenue'][$i],		
            				'VenueTime' =>$larrformData['candidatedate'][$i],		
		            		'Program' =>$larrformData['candidatehiddenprog'][$i],	
		            		'idschedulermaster'=>$larrformData['candidatescheduler'][$i],
            				'DateOfBirth'=>$larrformData['candidatedateofbirth'][$i],
            				'Amount'=>0,
            				'ICNO' => $larrformData['candidateicno'][$i],		
            				'Payment' =>1,		
		            		'DateTime' =>$larrformData['candidatetime'][$i],	
            				'Takafuloperator'=>$larrformData['candidatehiddentakaful'][$i],
            				'VenueChange' =>0,		
            				'ArmyNo' =>0,		
		            		'batchpayment' =>1,
            				'Gender'=>$larrformData['candidategender'][$i],
            				'Race'=>$larrformData['candidaterace'][$i]
						);			
	       $db->insert($table,$postData);
	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');
	        
	        
	        $larrresult = self::fnviewstudentdetails($lastid);
	        
	       
	       $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($larrformData['candidatehiddentakaful'][$i],$lastid);
	     
	        
	        
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['candidatebatch'][$i],		
		            		'IDApplication' =>$lastid,
            				'Approved' =>0,
            				'RegistrationPin'=>$regpin
            				);			
	       $db->insert($tables,$postDatas);
	       
	       
	      self::sendmails($larrresult,$regid);
	       
		}
		

			    $where = 'idBatchRegistration = '.$batchid;
			     $postData = array(		
							'paymentStatus' => 2														
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	}
	
	
	
	public function fnInsertIntoAdhocStd($larrformData,$countloop,$batchid)
	{
		echo "<pre/>";
		/*print_r($larrformData);
		die();*/
		
		for($i=0;$i<$countloop;$i++)
		{
			$db = Zend_Db_Table::getDefaultAdapter();
            $table = "tbl_studentapplication";
            $postData = array(		
							'FName' => $larrformData['candidatename'][$i],		
            				'EmailAddress' =>$larrformData['candidateemail'][$i],		
		            		'UpdDate' =>date('Y-m-d:H-i-s'),	
		            		'UpdUser'=>1,
            				'IdBatch'=>$larrformData['candidatebatch'][$i],
            				'Venue' => 0,		
            				'VenueTime' =>0,		
		            		'Program' =>$larrformData['candidatehiddenprog'][$i],	
		            		'idschedulermaster'=>0,
            				'DateOfBirth'=>$larrformData['candidatedateofbirth'][$i],
            				'Amount'=>0,
            				'ICNO' => $larrformData['candidateicno'][$i],		
            				'Payment' =>1,		
		            		'DateTime' =>0,	
            				'Takafuloperator'=>$larrformData['candidatehiddentakaful'][$i],
            				'VenueChange' =>0,		
            				'ArmyNo' =>0,		
		            		'batchpayment' =>1
						);			
	       $db->insert($table,$postData);
	       
	        $lastid = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studentapplication','IDApplication');
	        
	        
	        $larrresult = self::fnviewstudentdetails($lastid);
	        
	       
	       $batchmodel = new App_Model_Batchlogin();
	        $regid = $batchmodel->fnGenerateCode($larrformData['candidatehiddentakaful'][$i],$lastid);
	     
	        
	        
	        
            $tables = "tbl_registereddetails";
            $postDatas = array(		
							'Regid' =>$regid,		
            				'IdBatch' =>$larrformData['candidatebatch'][$i],		
		            		'IDApplication' =>$lastid
            				);			
	       $db->insert($tables,$postDatas);
	       
	       
	      self::sendmails($larrresult,$regid);
	       
		}
		

			    $where = 'idBatchRegistration = '.$batchid;
			     $postData = array(		
							'paymentStatus' => 2														
						);
				$table = "tbl_batchregistration";
	            $db->update($table,$postData,$where);
	}
	public function fnBatchProgram($idprog)
	{
	$db = Zend_Db_Table::getDefaultAdapter();
			$sql = $db->select()->from('tbl_batchmaster',array('key' =>'IdBatch','value' => 'BatchName'))
								->where('IdProgrammaster =?',$idprog);
			$result = $db->fetchAll($sql);
			return $result;
	}
	
	public function sendmails($larrresult,$regid)
	{
		/*echo "asfsafsad";
		die();*/
		 $this->lobjstudentmodel = new App_Model_Studentapplication();
	$larrSMTPDetails  = $this->lobjstudentmodel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
					
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
						
						
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							/*echo "asfsafsdfsd";
							die();*/
							try{
								$lobjProtocol->connect();
						   		$lobjProtocol->helo($lstrSMTPUsername);
								$lobjTransport->setConnection($lobjProtocol);
						 	
								//Intialize Zend Mailing Object
								$lobjMail = new Zend_Mail();
						
								$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
								$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
								$lobjMail->addHeader('MIME-Version', '1.0');
								$lobjMail->setSubject($lstrEmailTemplateSubject);
						
								for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
									if($larrEmailIds[$lintI] != ""){
										$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);	
																
										//replace tags with values
										//$Link = "<a href='".$this->Url."/registration'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Candidate]",$lstrStudentName,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",0,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[TransactionId]",'NA',$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$regid,$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										$lobjMail->setBodyHtml($lstrEmailTemplateBody);
								
										try {
											$lobjMail->send($lobjTransport);
										} catch (Exception $e) {
											$lstrMsg = "error";      				
										}	
										$lobjMail->clearRecipients();
										$this->view->mess .= ". Login Details have been sent to user Email";
										unset($larrEmailIds[$lintI]);
									}
								}
							}catch(Exception $e){
								$lstrMsg = "error";
							}
						}else{
							$lstrMsg = "No Template Found";
						}
	}
	
	
	public function fnGetRace()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where('a.idDefType = 13');	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetEducation()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionDesc"))
										  ->where('a.idDefType = 14');	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
 }