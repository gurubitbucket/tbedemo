<?php
class App_Model_Centerlogin extends Zend_Db_Table {
	//protected $_name = 'tbl_batchmaster'; // table name
	
	public function fnGetValuesRow($strQry)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();				  	
			$larrResult = $lobjDbAdpt->fetchRow("$sqlQry");
			return $larrResult;
	}
	
	public function fnGetValues($strQry)
	{

		 $db =  Zend_Db_Table::getDefaultAdapter();    	
    		$result = $db->fetchAll("$strQry");   
			return $result;
	}	
	
public  function fngetlogincheck($username,$password)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_center"),array("a.idcenter" ))
					 				->where("a.centerlogin= ?",$username)
					 				->where("a.centerpassword = ?",$password);					  	
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		
	}
function fnGenerateCode($idtakafuloperator,$Idunique){	
		$db 	= 	Zend_Db_Table::getDefaultAdapter();			
		$select =   $db->select()
				->  from('tbl_config')
				->	where('idUniversity  = ?',1);				 
		$result = 	$db->fetchRow($select);		
		$sepr	=	$result['Sturegtext'];
		$str	=	"SturegField";
		for($i=1;$i<=3;$i++){
			$check = $result[$str.$i];
			switch ($check){
				case 'year':
				  $code	= date('dMy');
				  break;
				case 'uniqueid':
				  $code	= $Idunique;
				  break;
				case 'takaful':
					 $select =  $db->select()
					 		 -> from('tbl_takafuloperator')
					 		 ->	where('idtakafuloperator  = ?',$idtakafuloperator);  				 
					$resultCollage = $db->fetchRow($select);		
					$code		   = $resultCollage['TakafulShortName'];
				  break;
				default:
				  break;
			}
			if($i == 1) $accCode 	 =  $code;
			else 		$accCode	.=	$sepr.$code;
		}	
		return $accCode;	
	 	/*$data = array('InvoiceNo' => $accCode);
		$where['IdInvoice = ? ']= $Idunique;		
		return $db->update('tbl_sfsinvoicemaster', $data, $where);	*/		
	} 

 public function fnviewCenter($idcenter) { //Function for the view user 

    	$db = Zend_Db_Table::getDefaultAdapter();
	  $select = $db->select()  
			->from(array("a"=>"tbl_center"),array("a.*"))
           ->where("a.idcenter =?",$idcenter);
	$result = $db->fetchRow($select);
	return $result;
    }
    
    public function fninsertintotemp($id,$session,$crsid,$startedmin,$totaltimerequired,$date)
    {
    	 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_centerstartexam";
          $postData = array(		
							'idcenter' => $id,	
           					'sessionid' =>$session,
                            'Program'=>$crsid,
            				'Totaltime'=>$totaltimerequired,
                            'Startedtime'=>$startedmin,
          					'Date'=>$date
            																
						);
	       $db->insert($table,$postData);
    }
    
    public function fnGetStudents($idcenter,$date,$month,$year)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
	   $select = $db->select()  
			         ->from(array("a"=>"tbl_center"),array("a.*"))
                     ->join(array("b"=>"tbl_studentapplication"),'a.idcenter=b.Examvenue',array("b.*"))
                     ->join(array("c"=>"tbl_registereddetails"),'b.IDApplication=c.IDApplication',array("c.*"))
                     ->join(array("d"=>"tbl_newscheduler"),'d.idnewscheduler=b.Year',array(""))
                     //->where("c.Regid not in (select Regid from tbl_answerdetails)") 
                     ->where("b.Examvenue =?",$idcenter)
                     ->where("b.Examdate=?",$date)
                     ->where("b.Exammonth=?",$month)
                     ->where("d.Year =?",$year); 
                     //->where("b.Examdate=curdate()");
                     
	$result = $db->fetchAll($select);
	return $result;
    }
    
    public function fnGetcourses($year,$month)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()  
			        ->from(array("a"=>"tbl_newscheduler"),array())
                    ->join(array("b"=>"tbl_newschedulercourse"),'a.idnewscheduler=b.idnewscheduler',array())
                     //->join(array("c"=>"tbl_newschedulervenue"),'a.idnewscheduler=c.idnewscheduler',array())
                      ->join(array("d" => "tbl_programmaster"),'b.IdProgramMaster=d.IdProgrammaster',array("key"=>"d.IdProgrammaster","value"=>"d.ProgramName"))
                   //  ->where("a.From>=?",$month)
                     //->where("a.Year=?",$year)
                     ->group("b.IdProgramMaster");
                     //->where("b.Examdate=curdate()");
                     
	$result = $db->fetchAll($select);
	return $result;
    }
    
    public function fngetactivetotaltime($crsid)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
	    $select = $db->select()  
			         ->from(array("a"=>"tbl_batchmaster"),array("a.*"))
                     ->join(array("b"=>"tbl_tosmaster"),'a.IdBatch=b.IdBatch',array("b.*"))
                     ->where("b.Active=1")
                     ->where("a.IdProgramMaster=?",$crsid);                
	$result = $db->fetchAll($select);
	return $result;
    }
    
    public function fndeletecenterdetails($idcenter)
    {
    	
    }
    
 public function fngetCenterDetails($idcenter) { //Function for the view user 

    	$db = Zend_Db_Table::getDefaultAdapter();
	  $select = $db->select()  
			->from(array("a"=>"tbl_center"),array("a.*"))
           ->where("a.idcenter =?",$idcenter);
	$result = $db->fetchRow($select);
	return $result;
    }
  public function updatecenterpass($password,$idcomp)
     {
     	 $db = Zend_Db_Table::getDefaultAdapter();
     	$data = array('centerpassword' => $password);
		$where['idcenter = ?']= $idcomp;		
		$db->update('tbl_center', $data, $where);	
     }
     
     
 public function getDateExam($idvenue)
     {
     	  $db = Zend_Db_Table::getDefaultAdapter();
     		$lstrSelect = $db->select()
			  ->from(array("a" => "tbl_newscheduler"),array("a.*"))
			  ->join(array("b"=>"tbl_newschedulervenue"),'a.idnewscheduler=b.idnewscheduler',array("b.*"))
			  ->join(array("c"=>"tbl_newschedulerdays"),'c.idnewscheduler = a.idnewscheduler',array("c.*"))
			  ->where("b.idvenue  = ?",$idvenue);
			 
			$larrResult = $db->fetchAll($lstrSelect);	
			return $larrResult;
     }
}