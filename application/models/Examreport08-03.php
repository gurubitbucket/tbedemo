<?php
	class App_Model_Examreport extends Zend_Db_Table {
		

		public function fngetprogramnames()
	   {
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"ProgramName")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   
	   public function fngetcompanynames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_companies"),array("key"=>"a.IdCompany","value"=>"CompanyName")) 				 
				 				 ->order("a.CompanyName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   
	   public function fngetcenternames()
	   {
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername")) 
				 				 ->order("a.idcenter");				 
				 				 //->order("a.centername"); 
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   
	   public function fngettakafulnames()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 				 
				 				 ->order("a.TakafulName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	   
	   public function fngettakafulcompanies($tid)
	   {
	   	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	 	$lstrSelect = $lobjDbAdpt->select()
	   	                         ->from(array("a"=>"tbl_studentapplication"),array('a.Gender','a.IDApplication as IDApplication','a.FName as StudentName','year(curdate())-year(a.DateOfBirth) as Age','a.ICNO as ICNO',"CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(e.Year,'')) as Date"))
	   	                         ->join(array("b"=>"tbl_takafuloperator"),'b.idtakafuloperator= a.Takafuloperator',array("b.TakafulName as TakafulCompany"))
	   	                         ->join(array("c"=>"tbl_definationms"),'c.idDefinition = a.Qualification',array("c.DefinitionDesc as Qualification"))
	   	                          ->join(array("d"=>"tbl_definationms"),'d.idDefinition = a.Race',array("d.DefinitionDesc as Race"))
	   	                         ->join(array('e' => 'tbl_newscheduler'),'a.Year = e.idnewscheduler',array())
	   	                         ->join(array('g' => 'tbl_registereddetails'),'g.IDApplication = a.IDApplication',array('g.Regid as ExamNo'))
	   	                         ->join(array('f' => 'tbl_center'),'f.idcenter= a.Examvenue',array('f.centername as Venue'))
	   	                          ->where("b.idtakafuloperator= ?",$tid)
	   	                          ->where("c.idDefType=14")
	   	                           ->where("d.idDefType=13"); 
	   	                         /*->from(array("a"=>"tbl_studentpaymentoption"),array())
	   	                         ->join(array("b"=>"tbl_batchregistration"),'b.idBatchRegistration= a.IDApplication',array("b.registrationPin"))
	   	                         ->where("b.idCompany= ?",$tid)
	   	                         ->where("a.companyflag= ?",2);*/
	   	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	    return $larrResult;
	   }
	   
		public function fnReportSearchDetails($larrformData,$idvenue)
		{ 
			
			if($larrformData['Studentname']) $name = $larrformData['Studentname'];
			if($larrformData['ICNO']) $icno = $larrformData['ICNO'];
			if($larrformData['Coursename']) $Coursename = $larrformData['Coursename'];
			//if($larrformData['Venues']) $venue = $idvenue;
			
		    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
		               ->from(array("a"=>"tbl_studentapplication"),array("a.IDApplication as IDApplication","FName as StudentName","CONCAT(IFNULL(a.Examdate,''),'-',IFNULL(a.Exammonth,''),'-',IFNULL(d.Year,'')) as Date",'a.ICNO as ICNO','a.Pass as Result')) 	   			   
			           ->join(array('b' => 'tbl_programmaster'),'b.IdProgrammaster = a.Program',array('b.ProgramName as Coursename'))
			           ->join(array('c' => 'tbl_center'),'c.idcenter= a.Examvenue',array('c.centername as Venue'))			
			           ->join(array('d' => 'tbl_newscheduler'),'a.Year = d.idnewscheduler')
			           ->where("a.Examvenue=$idvenue");
			//if($larrformData['Venues']) $lstrSelect->where("c.idcenter= ?",$idvenue);
			if($larrformData['Studentname'])	$lstrSelect->where("a.FName = ?",$name);
			if($larrformData['ICNO']) $lstrSelect->where("a.ICNO = ?",$icno);
			if($larrformData['Coursename']) $lstrSelect->where("b.IdProgrammaster = ?",$Coursename);
			
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		    return $larrResult;              
		}
		
		
}
