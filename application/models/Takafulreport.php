<?php
class App_Model_Takafulreport extends Zend_Db_Table { //Model Class for Users Details
	

	protected $_name = 'tbl_studentapplication';
	
	 /*
      * function to fetch all the details
      */
	
	public function fngetpaylater($idcomp) {
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_takafulpaymenttype" ), array ("a.idtakafulpaymenttype" ) )
											->join ( array ("b" => "tbl_definationms" ), 'a.paymenttype=b.idDefinition', array ("b.idDefinition", "b.DefinitionDesc" ) )
											->where ( "a.idtakafuloperator=?", $idcomp )
											->where ( "a.paymenttype= 181" );
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
	}
	
	public function fnGetMonthList()
	{
		$month= date("m"); 
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"))
								->where("a.idmonth<='$month'");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fnGetAllMonthList()
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_newmonths"),array("key"=>"a.idmonth","value"=>"a.MonthName"));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetallstudents($larrformData,$idtakafuloperator)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("c"=>"tbl_batchregistration"),array("c.*"))
								->join(array("d"=>"tbl_studentpaymentoption"),'c.idBatchRegistration=d.IDApplication',array("d.*"))
								->where("c.idCompany='$idtakafuloperator'")
								->where("d.companyflag=2");

		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetmarks($idapplication)
	{
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_studentmarks"),array("a.*"))
								->where("a.idapplication='$idapplication'");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetallstudentsdetails($regids,$larrformData)
	{
		$month = $larrformData['Month'];
		$year = $larrformData['Year'];
		
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_studentapplication"),array("a.*"))
								->join(array("b"=>"tbl_registereddetails"),'a.idapplication=b.idapplication',array("b.*"))
								->join(array("c"=>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
									->join(array("d"=>"tbl_center"),'a.examvenue=d.idcenter',array("d.*"))
								->where("b.RegistrationPin in ($regids)")
								->where("month(a.DateTime)=$month")
								->where("year(a.DateTime)=$year")
								->group("a.idapplication")
								->order("a.DateTime")
								->order("c.IdProgrammaster")
								->order("b.RegistrationPin")
								->order("a.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetallstudentsdetailsindividuals($idtakafuloperator,$larrformData)
	{
		$month = $larrformData['Month'];
		$year = $larrformData['Year'];
		
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_studentapplication"),array("a.*"))
								->join(array("c"=>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
								->join(array("d"=>"tbl_center"),'a.examvenue=d.idcenter',array("d.*"))
								->where("month(a.DateTime)=$month")
								->where("year(a.DateTime)=$year")
								->where("a.Takafuloperator='$idtakafuloperator'")
								->where("a.batchpayment=0")
									->where("a.Payment=1")
								->group("a.idapplication")
								->order("a.DateTime")
								->order("c.IdProgrammaster")
								->order("a.FName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngettakafulname($idtakaful)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_takafuloperator"),array("a.*"))
								->where("a.idtakafuloperator='$idtakaful'");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
	
	public function fngetallprogram()
	{
	
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_programmaster"),array("a.*"));
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
	public function fngettotaltakafulregistered($regpins,$month,$year,$idprogram)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_studentapplication"),array("a.*"))
								->join(array("b"=>"tbl_registereddetails"),'a.idapplication=b.idapplication',array("b.*"))
								->join(array("c"=>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
								->join(array("d"=>"tbl_center"),'a.examvenue=d.idcenter',array("d.*"))
								->where("b.RegistrationPin in ($regpins)")
								->where("month(a.DateTime)=$month")
								->where("year(a.DateTime)=$year")
								->where("a.Program='$idprogram'")
								->group("a.idapplication");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	public function fngetindividualregistered($idtakaful,$month,$year,$idprogram)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								->from(array("a" =>"tbl_studentapplication"),array("a.*"))
								->join(array("c"=>"tbl_programmaster"),'a.Program=c.IdProgrammaster',array("c.*"))
								->join(array("d"=>"tbl_center"),'a.examvenue=d.idcenter',array("d.*"))
								->where("month(a.DateTime)=$month")
								->where("year(a.DateTime)=$year")
								->where("a.Takafuloperator=$idtakaful")
								->where("a.Program='$idprogram'")
								->where("a.batchpayment=0")
								->where("a.Payment=1")
								->group("a.idapplication");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	///function to find the pass
	
}