<?php
class App_Model_Trainingcenter extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_center';
	
	//Get Country List
	public function fnGetCountryList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_countries"),array("key"=>"a.idCountry","value"=>"CountryName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.CountryName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
		
	//Get State List
	public function fnGetStateList(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_state"),array("key"=>"a.idState","value"=>"StateName"))
				 				 ->where("a.Active = 1")
				 				 ->order("a.StateName");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
public function  fnGetcityList($idstate)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
         	$select = $db->select()
			   ->from(array('a' => 'tbl_city'),array("key"=>"a.idCity","value"=>"CityName"))
			   ->where('a.idState=?',$idstate);
		$result = $db->fetchAll($select);
		return $result;
	}
	
public function fnGetStateCityList($idCountry){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   		$lstrSelect = $lobjDbAdpt->select()
					 				 ->from(array("a"=>"tbl_city"),array("key"=>"a.idCity","value"=>"CityName"))
					 				 ->where("a.idState = ?",$idCountry)
					 				 ->where("a.Active = 1")
					 				 ->order("a.CityName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
		}
	/*//Function to Get Initial Config Data
	public function fnGetInitialConfigData(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_config"),array("a.Language"));
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}*/
	
	//Function To Get Pagination Count from Initial Config
	public function fnGetPaginationCountFromInitialConfig(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lintPageCount = "";
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a"=>"tbl_config"),array("noofrowsingrid") );
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		if($larrResult['noofrowsingrid'] == "" || $larrResult['noofrowsingrid'] == "0"){
			$lintPageCount = "5";
		}else{
			$lintPageCount = $larrResult['noofrowsingrid'];
		}
		
		return $lintPageCount;
	}
	
	//Function to Get Initial Config Data
	public function fnGetInitialConfigData(){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a"=>"tbl_definationms"),array("LCASE(SUBSTRING(a.DefinitionCode,1,2)) AS Language") )
					 			 ->join(array("b"=>"tbl_definationtypems"),'a.idDefType = b.idDefType',array())
				 				 ->join(array("c"=>"tbl_config"),'c.Language = a.idDefinition',array("c.HtmlDir","c.DefaultCountry"));
		//echo $lstrSelect;exit();
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult;
	}
     /*public function fngetCenterDetails() { //Function to get the user details
       $db = Zend_Db_Table::getDefaultAdapter();
		//$field7 = "a.Active= ".$post["field7"];
		$select = $db->select() 	
			   ->from(array('a' => 'tbl_center'),array('a.*'))
			   ->joinLeft(array('b'=>'tbl_city'),'a.city=b.idCity',array('b.CityName as city'));
			   //->where('a.centername like  ?"%"',$post['field3'])
			  // ->where('a.city like  ?"%"',$post['field2'])
			  // ->where('a.mName like "%" ? "%"',$post['field4'])
			   //->where('a.lName like "%" ? "%"',$post['field6'])
			//   ->where($field7);
		$result = $db->fetchAll($select);
		return $result;
     }*/
        
public function fngetCenterDetails() { //Function to get the user details
       $db = Zend_Db_Table::getDefaultAdapter();
		//$field7 = "a.Active= ".$post["field7"];
		$select = $db->select() 	
			   ->from(array('a' => 'tbl_center'),array('a.*'))
			   ->joinLeft(array('b'=>'tbl_city'),'a.city=b.idCity',array('b.CityName as city'))
			   ->order('a.centername');
			   //->where('a.centername like  ?"%"',$post['field3'])
			  // ->where('a.city like  ?"%"',$post['field2'])
			  // ->where('a.mName like "%" ? "%"',$post['field4'])
			   //->where('a.lName like "%" ? "%"',$post['field6'])
			//   ->where($field7);
		$result = $db->fetchAll($select);
		return $result;
     }
    public function fnuserinfo($lstrusername) { //Function for getting the user information based on the username
        $result = $this->fetchAll( "loginName = '$lstrusername'") ;
        return $result;
    }
        
	/*public function fnSearchCenter($post = array()) 
	{		
    	$db = Zend_Db_Table::getDefaultAdapter();
		$field7 = "a.Active= ".$post["field7"];
		$select = $db->select() 	
			   ->from(array('a' => 'tbl_center'),array('a.*'))
			   ->joinLeft(array('b'=>'tbl_city'),'a.city=b.idCity',array('b.CityName as city'))
			   ->where('a.centername like "%" ? "%"',$post['field3'])			  
			   ->where($field7);
		$result = $db->fetchAll($select);
		return $result;
	}*/
    

	
	
	
    public function fnviewCenter($idcenter) { //Function for the view user 
    	$db = Zend_Db_Table::getDefaultAdapter();
	  $select = $db->select()  
			->from(array("a"=>"tbl_center"),array("a.*"))
           ->where("a.idcenter =?",$idcenter);
	$result = $db->fetchRow($select);
	return $result;
    }
    
    
 public function fngetcenterfromstu($lintidcenter) { //Function for the view user 
    	$db = Zend_Db_Table::getDefaultAdapter();
	  $select = $db->select()  
			->from(array("a"=>"tbl_studentapplication"),array("a.*"))
           ->where("a.Examvenue =?",$lintidcenter);
	$result = $db->fetchRow($select);
	return $result;
    }
    
    
 public function fnviewCenterfloor($idcenter) { //Function for the view user 
    	$db = Zend_Db_Table::getDefaultAdapter();
	  $select = $db->select()  
			->from(array("b" =>"tbl_centrefloor"),array("b.FloorName","b.NoofRoom","b.idcentrefloor"))
           ->where("b.idcenter =?",$idcenter);
	$result = $db->fetchAll($select);
	return $result;
    }
    
public function fnviewCenterrooms($idcenter) { //Function for the view user 
    	$db = Zend_Db_Table::getDefaultAdapter();
	  $select = $db->select()  
			->from(array("c"=>"tbl_centerroom"),array("c.RoomName","c.Capacity","c.idcentrefloor"))
           ->where("c.idcentrefloor in ($idcenter)");
	$result = $db->fetchAll($select);
	return $result;
    }
    
    
  
    
	public function fnviewUserSpecialRole($iduser) { //Function for the view user  special roles
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()
					->from(array("a" => "tbl_specialroles"),array("a.*"))
					->join(array("b" => "tbl_user"),"a.idUser=b.iduser",array("b.lName"))
					->join(array("c" => "tbl_definationms"),"a.idRole=c.idDefinition",array("c.DefinitionCode"))				
		            ->where("a.iduser = ?",$iduser);		   
		 return $result = $lobjDbAdpt->fetchAll($select);	
	}
	public function fnAddUserSpecialRoles($larrformData) { //Function for adding the user details to the table
		 $db = Zend_Db_Table::getDefaultAdapter();
		 return $db->insert('tbl_specialroles',$larrformData);		
	}
	public function fnGetUserSpecialRolesEdit($iduserSpecial) { //Function for the view user  special roles
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()
					->from(array("a" => "tbl_specialroles"),array("a.*"))
					->join(array("b"=>"tbl_definationms"),"a.idRole	=b.idDefinition",array("b.DefinitionCode"))
					->where("a.idSpecialRole = ?",$iduserSpecial);		   
		 return $result = $lobjDbAdpt->fetchRow($select);	
	}
	 public function fnUpdateUserSpecialRoles($larrformData) { //Function for updating the user
	 	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$where = 'idSpecialRole = '.$larrformData['edit'];
		unset($larrformData ['edit']);
		return $lobjDbAdpt->update('tbl_specialroles',$larrformData,$where);
    }
	public function fnGetRolesDetails($idRoles){
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_definationms"),array("key"=>"a.idDefinition","value"=>"a.DefinitionCode"))
				 				 ->join(array("b"=>"tbl_definationtypems"),"a.idDefType=b.idDefType AND b.defTypeDesc='Role'",array()) 
				 				 ->where("a.idDefinition  NOT IN".$idRoles);
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		return $larrResult;
	}
	
	
	public function getstaffdetails($idstaff) { //Function for the view user  special roles
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()
					->from(array("a" => "tbl_staffmaster"),array("a.FirstName","a.SecondName","a.ThirdName","a.ArabicName"))
					->where("a.IdStaff = ?",$idstaff);		   
		 return $result = $lobjDbAdpt->fetchRow($select);	
	}
	
	
	
	public function fninsertCenterfloor($lintidcenter,$larrformData) { //Function for adding the user details to the table
	//print_r($larrformData);print_r($larrformData);die();
		 $db = Zend_Db_Table::getDefaultAdapter();
		 $sum=0;
	 for($i=0;$i<count($larrformData['floorname']);$i++)
	       {
	          	 $tables = "tbl_centrefloor";
	       	     $arryresult['idcenter']= $lintidcenter;
	       	     $arryresult['FloorName'] = $larrformData['floorname'][$i];
	       	     $var = $larrformData['NoOfRooms'][$i];
	       	     $arryresult['NoofRoom']= $larrformData['NoOfRooms'][$i];
	       	     $arryresult['UpdDate']=$larrformData['UpdDate'];
	       	     $arryresult['UpdUser']=$larrformData['UpdUser'];
	       	     $db->insert($tables,$arryresult);
	       	     $lastidfloor  = $db->lastInsertId("tbl_centrefloor","idcentrefloor");
	       	      
	       	      	
	       	      	for($k=0;$k<$var;$k++)
	       	      	{
	       	      		$sum=$sum+$larrformData['Capacity'][$i][$k];
	       	      		 //$time = $tempdetails[$k]['Fromtime'].'---'.$tempdetails[$k]['Totime'];
	       	      		 $tabless = "tbl_centerroom";
			       	     $arrytime['idcentrefloor']= $lastidfloor;
			       	     //$arrytime['From'] = $time;
			       	     $arrytime['RoomName'] = $larrformData['Roomname'][$i][$k];
			       	     $arrytime['Capacity']=$larrformData['Capacity'][$i][$k];
			       	     $arrytime['UpdDate']=$larrformData['UpdDate'];
			       	     $arrytime['UpdUser']=$larrformData['UpdUser'];
			       	     $db->insert($tabless,$arrytime);
	       	      	}
	       	      	
	       	      
	       	     
	       }
	      // print_r($sum);die();
	       	$data = array('NumberofSeat' => $sum);
		$where['idcenter = ?']= $lintidcenter;		
		$db->update('tbl_center', $data, $where);	
	       
	       
	       
	}
	
/////////

public function fngetexistingcenter($cname)
{  	
	$where=$cname;
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_center"),array("a.*"))
					->where("a.centerlogin='$cname'");
					
					//echo $lstrSelect;die();
	$result = $db->fetchRow($lstrSelect);
	return $result;
}
//////////

public function fnCheckIpaddressCenter($ipaddress)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	 $lstrSelect = $db->select()
					->from(array("a"=>"tbl_center"),array("a.ipaddress"))
					->where("a.ipaddress=?",$ipaddress);
	$result = $db->fetchRow($lstrSelect);
	return $result;
	
}

public function fngetcenters($idtakaful){
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("key"=>"a.idcenter","value"=>"centername")) 				 
				 				 ->order("a.centername")
				 				 ->where ("a.centercode=?",$idtakaful );
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
}

public function fngetexistingcentername($cname)
{  	
	$where=$cname;
	$db = Zend_Db_Table::getDefaultAdapter();
	$lstrSelect = $db->select()
					->from(array("a"=>"tbl_center"),array("a.*"))
					->where("a.centername='$cname'");
					
					//echo $lstrSelect;die();
	$result = $db->fetchRow($lstrSelect);
	return $result;
}
//17/022014(In-houseconcept)

//17022014(In-houseconcept)
public function fnaddCenter($larrformData) { //Function for adding the user details to the table
		$larrformData['workPhone'] = $larrformData['workcountrycode']."-".$larrformData['workstatecode']."-".$larrformData['workPhone'];
		$larrformData['cellPhone'] = $larrformData['countrycode']."-".$larrformData['statecode']."-".$larrformData['cellPhone'];
		 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_center";
            $postData = array(		
							'centername' => $larrformData['centername'],	
           					'centercode' => $larrformData['Operators'],	
            				'addr1' =>$larrformData['addr1'],		
		            		'addr2' =>$larrformData['addr2'],
                            'city' => $larrformData['city'],	
           				    'country' => $larrformData['country'],	
            				'state' =>$larrformData['state'],	
                            'zipCode' => $larrformData['zipCode'],	
           					 'workphone' => $larrformData['workPhone'],	
            				'fax' =>$larrformData['fax'],
                             'email' => $larrformData['email'],	
            				'Active' =>'0',	
                            'contactperson' => $larrformData['contactperson'],	
           					'cellphone' => $larrformData['cellPhone'],	
            				'paddr1' =>$larrformData['paddr1'],		
                            'paddr2' => $larrformData['paddr2'],	
            				'NumberofSeat' =>'0',	
                            'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
                            'Nooffloors'=>'0',	
                            'centerlogin'=>$larrformData['centerlogin'],
                            'centerpassword'=>$larrformData['centerpassword'],
                            'ipaddress'=>$larrformData['ipaddress'],
                            'ipaddress'=>$larrformData['hostname']
                             								
						);	
						//print_r($postData);die();		
	       $db->insert($table,$postData);
		/* $lastid  = $db->lastInsertId("tbl_center","idcenter");
		 $sum=0;
	 for($i=0;$i<count($larrformData['floorname']);$i++)
	       {
	          	 $tables = "tbl_centrefloor";
	       	     $arryresult['idcenter']= $lastid;
	       	     $arryresult['FloorName'] = $larrformData['floorname'][$i];
	       	     $var = $larrformData['NoOfRooms'][$i];
	       	     $arryresult['NoofRoom']= $larrformData['NoOfRooms'][$i];
	       	     $arryresult['UpdDate']=$larrformData['UpdDate'];
	       	     $arryresult['UpdUser']=$larrformData['UpdUser'];
	       	     $db->insert($tables,$arryresult);
	       	     $lastidfloor  = $db->lastInsertId("tbl_centrefloor","idcentrefloor");
	       	      
	       	      	
	       	      	for($k=0;$k<$var;$k++)
	       	      	{
	       	      		$sum=$sum+$larrformData['Capacity'][$i][$k];
	       	      		 //$time = $tempdetails[$k]['Fromtime'].'---'.$tempdetails[$k]['Totime'];
	       	      		 $tabless = "tbl_centerroom";
			       	     $arrytime['idcentrefloor']= $lastidfloor;
			       	     //$arrytime['From'] = $time;
			       	     $arrytime['RoomName'] = $larrformData['Roomname'][$i][$k];
			       	     $arrytime['Capacity']=$larrformData['Capacity'][$i][$k];
			       	     $arrytime['UpdDate']=$larrformData['UpdDate'];
			       	     $arrytime['UpdUser']=$larrformData['UpdUser'];
			       	     $db->insert($tabless,$arrytime);
	       	      	}
	       	      	
	       	      
	       	     
	       }
	      // print_r($sum);die();
	       	$data = array('NumberofSeat' => $sum);
		$where['idcenter = ? ']= $lastid;		
		$db->update('tbl_center', $data, $where);	*/
	       
	       
	       
	}
	public function fnSearchCenter($post = array(),$idtakaful) 
	{		
    	$db = Zend_Db_Table::getDefaultAdapter();
		$Active = "a.Nooffloors= ".$post["Active"];
		$select = $db->select() 	
			   ->from(array('a' => 'tbl_center'),array('a.*'))
			   ->joinLeft(array('b'=>'tbl_city'),'a.city=b.idCity',array('b.CityName as city'))
			   ->join(array('c'=>'tbl_takafuloperator'),'c.idtakafuloperator=a.centercode',array('c.TakafulName'))
			  ->where ("c.idtakafuloperator=?",$idtakaful );
		if($post['Venuenew'])  $select ->where("a.idcenter =?",$post['Venuenew']);			  
	                           $select ->where($Active)
							          // ->where("a.Nooffloors !=0")
			                           ->order('a.centername');
	    //echo $select;
		$result = $db->fetchAll($select);
		return $result;
	}
    public function fngettakafuloperators()
	{
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
	  $lstrSelect = $lobjDbAdpt->select()
				 			 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 
							 ->join(array('b'=>'tbl_center'),'a.idtakafuloperator = b.centercode',array('')) 
				 			 ->order("a.TakafulName");	
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	   return $larrResult;	
	}
	 public function fngettakafuloperatorsleft($operatoralready)
	{
	  //print_r($operatoralready);die();
	 
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
	    $lstrSelect = $lobjDbAdpt->select()
				 			 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 
							 //->join(array('b'=>'tbl_center'),'a.idtakafuloperator = b.centercode',array('')) 
							 ->where("a.idtakafuloperator not in ($operatoralready)")
				 			 ->order("a.TakafulName");		
	   $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	   //echo "<pre>";
	   //print_r($larrResult);die();
	   return $larrResult;	
	}
	
	public function fnGetoperator($operator)
	{
	     $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
	     $lstrSelect = $lobjDbAdpt->select()
				 			 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 
							 ->where("a.idtakafuloperator =?",$operator)
				 			 ->order("a.TakafulName");		
	     $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	     return $larrResult;
	}
	 public function fnupdateCenter($lintidcenter,$larrformData,$floorids) { //Function for updating the user
   	$larrformData['workPhone'] = $larrformData['workcountrycode']."-".$larrformData['workstatecode']."-".$larrformData['workPhone'];
		$larrformData['cellPhone'] = $larrformData['countrycode']."-".$larrformData['statecode']."-".$larrformData['cellPhone'];
		 $postData = array(		
							'centername' => $larrformData['centername'],	
           					'centercode' => $larrformData['Operators'],	
            				'addr1' =>$larrformData['addr1'],		
		            		'addr2' =>$larrformData['addr2'],
                            'city' => $larrformData['city'],	
           				    'country' => $larrformData['country'],	
            				'state' =>$larrformData['state'],	
                            'zipCode' => $larrformData['zipCode'],	
           					 'workphone' => $larrformData['workPhone'],	
            				'fax' =>$larrformData['fax'],
                             'email' => $larrformData['email'],	
            				'Active' =>'2',	
                            'contactperson' => $larrformData['contactperson'],	
           					'cellphone' => $larrformData['cellPhone'],	
            				'paddr1' =>$larrformData['paddr1'],		
                            'paddr2' => $larrformData['paddr2'],	
            				'NumberofSeat' =>'0',	
                            'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
		                    'Nooffloors'=>'1',
		                    'centerlogin'=>$larrformData['centerlogin'],
		                    'centerpassword'=>$larrformData['centerpassword'],
		                     'ipaddress'=>$larrformData['ipaddress'],														
		 					'hostname'=>$larrformData['hostname']
						);	
   	 	$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$where = "idcenter = '".$lintidcenter."'"; 	
		$db->update('tbl_center', $postData, $where);
		
		
 		//$wheres1 = "idcenter = '".$lintidcenter."'";
 		//$db->delete('tbl_centrefloor',$wheres1);
 		
 		 //$wheres = "idcentrefloor in ($floorids)";
 		//$db->delete('tbl_centerroom',$wheres);
    }
    
    ///////////////////takaful login////////////////

	public function fnGetoperatorList()
{
	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();		
	$lstrSelect = $lobjDbAdpt->select()
				 			 ->from(array("a"=>"tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"TakafulName")) 
                             ->where("a.TakafulField6=1")								 
				 			 ->order("a.TakafulName");		
	$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
	return $larrResult;	
}
public function fngetTakafulname($IdTakaful) { //Function to get the user details
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a"=>"tbl_takafuloperator" ), array ("key" => "idtakafuloperator", "value" => "TakafulName") )
					->where ( "a.idtakafuloperator=?", $IdTakaful )
					->where("a.TakafulField6=1");
		$larrResult = $lobjDbAdpt->fetchAll( $lstrSelect );
		return $larrResult;
	}
}