<?php
class App_Model_Companyapplication extends Zend_Db_Table { //Model Class for Users Details
	protected $_name = 'tbl_studentapplication';
	
	
 	public function fngetBatchDetails($Idreg) { //Function to get the user details 		
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_batchregistration"),array("a.*"))								 						
								 ->where("a.registrationPin=?",$Idreg)
								 ->where("a.paymentStatus= 1");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 		
		return $larrResult;
     }
     
     
	public function fngetCompanyOperator($IdCOmpany) { //Function to get the user details
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("tbl_companies" ), array ("*" ) )->where ( "IdCompany=?", $IdCOmpany );
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
		
	}
     
	public function fngetpaylater($idcomp) {
		
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter ();
		$lstrSelect = $lobjDbAdpt->select ()->from ( array ("a" => "tbl_companypaymenttype" ), array ("a.idcompanypaymenttype" ) )
											->join ( array ("b" => "tbl_definationms" ), 'a.paymenttype=b.idDefinition', array ("b.idDefinition", "b.DefinitionDesc" ) )
											->where ( "a.idcompany=?", $idcomp )
											->where ( "a.paymenttype= 181" );
		$larrResult = $lobjDbAdpt->fetchRow ( $lstrSelect );
		return $larrResult;
	}
     
	public function fnGetProgramFee($idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				/*$lstrSelect = "SELECT sum(abc.amount) from 

(SELECT max(a.EffectiveDate),a.Rate as amount  from tbl_programrate a,tbl_accountmaster b
where a.idProgram=$idprog and 
 b.idAccount= a.IdAccountmaster and  
 b.Active=1 and a.Active=1   and b.idAccount not in (select t.idAccount from tbl_accountmaster t where t.duringRegistration=1 )  group by a.IdAccountmaster) as abc ";
			*/
$lstrSelect = "SELECT sum(abc.amount) from 

(select Rate as amount from tbl_programrate where `EffectiveDate` in 
(SELECT max(a.EffectiveDate)  from tbl_programrate a,tbl_accountmaster b
where a.idProgram= $idprog and 
 b.idAccount= a.IdAccountmaster and  
 b.Active=1 and a.Active=1 and b.idAccount not in (select t.idAccount from tbl_accountmaster t where t.duringRegistration=1 ) group by a.IdAccountmaster)and idProgram=$idprog and Active=1   and IdAccountmaster not in (select r.idAccount from tbl_accountmaster r where r.duringRegistration=1 ))as abc  ";
				
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	public function fnGetDiscountFee($nocandidates,$idprogram) { //Function to get the user details 		
   /*    $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programrate"),array())
								 ->join(array("b"=>"tbl_accountmaster"),'b.idAccount=a.IdAccountmaster',array(""))	
								 ->join(array("c"=>""))					 						
								 ->where("a.registrationPin=?",$Idreg)
								 ->where("a.paymentStatus= 1");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 	*/
            
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$sql = "SELECT SUM(Amount) dis FROM tbl_accounthead WHERE idAccountHead IN (  SELECT max(ah.idAccountHead) idAcc FROM `tbl_accounthead` ah  WHERE `idAccount` IN(
								SELECT w.IdAccountmaster  FROM(SELECT `a`.*, max(a.EffectiveDate) FROM `tbl_programrate` AS `a` WHERE (a.idProgram=$idprogram) 						
								GROUP BY `a`.`IdAccountmaster`)w)
								GROUP BY ah.idAccount) AND Active=1";
		$larrsumDisc = $lobjDbAdpt->fetchRow($sql); 
		return $larrsumDisc;
     }
     
public function fngetintialdiscount()
{
	  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
	   $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_config"),array("a.*"))								 						
								 ->where("a.idConfig= 1");
								 //->where("a.paymentStatus= 1");
		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
		return $larrResult; 		
}
	
    /*function fnGetProgramFee($idProgram){
    	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
    	//Program Fee
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programrate"),array("a.*","max(a.EffectiveDate)"))								
								 ->join(array("b"=>"tbl_accountmaster"),'a.IdAccountmaster=b.idAccount')							
								 ->where("a.idProgram=?",$idProgram)
								 ->where("b.Active=1")
								 ->group("a.IdAccountmaster");									 
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);		
		$sumProgram = 0;
		for($j=0;$j<count($larrResult);$j++){
			$sumProgram = $sumProgram+$larrResult[$j]['Rate'];
		}
		//Course Fee
		$sql = "SELECT sum(Rate) tot FROM `tbl_courserate`  WHERE `IdAccountmaster` IN(						
						SELECT w.IdAccountmaster  FROM(SELECT `a`.*, max(a.EffectiveDate) FROM `tbl_programrate` AS `a` WHERE (a.idProgram=$idProgram) 
						GROUP BY `a`.`IdAccountmaster`)w
						)";
		$larrsumCourse = $lobjDbAdpt->fetchRow($sql);
		//Discount
		$sql = "SELECT SUM(Amount) dis FROM tbl_accounthead WHERE idAccountHead IN (  SELECT max(ah.idAccountHead) idAcc FROM `tbl_accounthead` ah  WHERE `idAccount` IN(
								SELECT w.IdAccountmaster  FROM(SELECT `a`.*, max(a.EffectiveDate) FROM `tbl_programrate` AS `a` WHERE (a.idProgram=$idProgram) 						
								GROUP BY `a`.`IdAccountmaster`)w)
								GROUP BY ah.idAccount) AND Active=1";
		$larrsumDisc = $lobjDbAdpt->fetchRow($sql); 
		
		return $sumProgram+$larrsumCourse['tot']-$larrsumDisc['dis'];		
    } */
     /*
      * function to fetch all the details
      */
  public function fngetCompanyDetails($IdCompany) { //Function to get the user details
 
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_companies"),array("a.*"))								
								 ->where("a.IdCompany=?",$IdCompany);
								 
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
     
 public function fngetRegistratinpin($IdCompany) { //Function to get the user details
 
       $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_batchregistration"),array("key"=>"a.registrationPin","value"=>"a.registrationPin"))
								 ->where("a.idBatchRegistration in (select IDApplication from tbl_studentpaymentoption where companyflag=1)")
								 ->where("a.paymentStatus=1")
								 ->where("a.Approved=1")
								 ->where("a.AdhocDate=0000-00-00")						
								 ->where("a.IdCompany=?",$IdCompany);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     public function fngetAdhocRegistratinpin($IdCompany)
     {
     	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_batchregistration"),array("key"=>"a.registrationPin","value"=>"a.registrationPin"))
								 ->where("a.idBatchRegistration in (select IDApplication from tbl_studentpaymentoption where companyflag=1)")
								 ->where("a.paymentStatus=1")
								 ->where("a.Approved=1")
								 ->where("a.AdhocDate !=0000-00-00")						
								 ->where("a.IdCompany=?",$IdCompany);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
     }
     public function fnGetCaptionName()
     {
     	 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_config"),array("a.*"))								
								 ->where("a.idConfig=1");
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
  public function fnInsertPaymentdetails($larrformData){  

  	/*print_r($larrformData);
  	die();*/
  		 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_batchregistration";
            $postData = array(		
							'idCompany' => $larrformData['idCompany'],	
           					'totalNoofCandidates' =>$larrformData['totalNoofCand'],	
           					'totalAmount' => $larrformData['grossAmt'],           				                           
                            'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
            				'AdhocDate' => '0000-00-00',	
            				'AdhocVenue' =>'others',
            				'Approved'=>0					
						);	
					
	     $db->insert($table,$postData);
	     
		 $lastid  = $db->lastInsertId("tbl_batchregistration","idBatchRegistration");		 
	 	 $table = "tbl_batchregistrationdetails";
		 for($i=0;$i<count($larrformData['idProgram']);$i++){		
		         $postData = array('idBatchRegistration' => $lastid,	
							        'idProgram' => $larrformData['idProgram'][$i],	
							        'eachAmount' => $larrformData['eachAmount'][$i],	
							        'noofCandidates' => $larrformData['noofCandidates'][$i],	
		           					'toatlAmount' =>$larrformData['toatlAmount'][$i],	
		           					'registrationPin' => 0,  
		                            'discount'=>$larrformData['discountamount'][$i],         				                           
		              	            'UpdDate' => $larrformData['UpdDate'],	
		            				'UpdUser' =>$larrformData['UpdUser']	);					
			     $db->insert($table,$postData);
		 }
		return $lastid;
  	
  } 
  
  public function  fnInsertStudentPaymentdetails($mod,$lastInsId)
  {
  
  		 $db = Zend_Db_Table::getDefaultAdapter();
  		 $table = "tbl_studentpaymentoption";
		  $postData = array(		
							'IDApplication' =>$lastInsId,	
           					'ModeofPayment' =>$mod,	
           					'companyflag' => 1,           				                           
                            /*
                             * 'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
            				 'AdhocDate' => '0000-00-00',	
            				'AdhocVenue' =>'others'			*/		
						);
						   $db->insert($table,$postData);
						   $lastids  = $db->lastInsertId("tbl_studentpaymentoption","idstudentpaymentoption");	
						   return $lastids;
						 
							
  }
  
  public function fngetmodeofpayment($mod)
  {
  	 $db = Zend_Db_Table::getDefaultAdapter();
  	 
  	 $lstrSelect = $db->select()
				->from(array("a"=>"tbl_studentpaymentoption"),array("a.ModeofPayment"))
				->where("a.idstudentpaymentoption = ?",$mod);	
				
	 $larrResult = $db->fetchRow($lstrSelect);
	return $larrResult;	
  }
  
    public function fnInsertPaymentdetailsadhoc($larrformData){  	
  		 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_batchregistration";
            $postData = array(		
							'idCompany' => $larrformData['idCompany'],	
           					'totalNoofCandidates' =>$larrformData['totalNoofCand'],	
           					'totalAmount' => $larrformData['grossAmt'],           				                           
                            'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
            				 'AdhocDate' => $larrformData['AdhocDate'],	
            				'AdhocVenue' =>$larrformData['AdhocVenue'],
            				'Approved'=>0						
						);	
					
	     $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_batchregistration","idBatchRegistration");		 
	 	  $table = "tbl_batchregistrationdetails";
		 for($i=0;$i<count($larrformData['idProgram']);$i++){		
		         $postData = array('idBatchRegistration' => $lastid,	
							         'idProgram' => $larrformData['idProgram'][$i],	
							         'eachAmount' => $larrformData['eachAmount'][$i],	
							         'noofCandidates' => $larrformData['noofCandidates'][$i],	
		           					'toatlAmount' =>$larrformData['toatlAmount'][$i],	
		           					'registrationPin' => 0,           				                           
		              	              'UpdDate' => $larrformData['UpdDate'],	
		            				'UpdUser' =>$larrformData['UpdUser']	);					
			     $db->insert($table,$postData);
		 }
		return $lastid;
  	
  } 
/*
 * function to fetch all the Prog Name
 */
	public function fnGetProgramName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										   ->join(array("c" => "tbl_batchmaster"),'a.IdProgrammaster=c.IdProgrammaster')
										  ->join(array("b"=>"tbl_tosmaster"),'b.IdBatch=c.IdBatch')
										 ->join(array("e"=>"tbl_programrate"),'a.IdProgrammaster=e.idProgram',array())
										     ->join(array("t"=>"tbl_accountmaster"),'e.IdAccountmaster=t.idAccount',array())
										 ->where("t.duringRegistration=0")
										        ->where("b.Active=1")
										  ->where("e.Active=1")
										  ->where("a.Active  = ?","1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}	
	public function fnGetProgramNameList(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_programmaster"),array("key"=>"a.IdProgrammaster","value"=>"a.ProgramName"))
										   ->join(array("c" => "tbl_batchmaster"),'a.IdProgrammaster=c.IdProgrammaster')
										  ->join(array("b"=>"tbl_tosmaster"),'b.IdBatch=c.IdBatch')
										  ->where("a.Active  = ?","1");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	public function fngetPaymentDetails($idPayment){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_batchregistrationdetails"),array("a.*"))
										  ->joinLeft(array("b" => "tbl_batchregistration"),'a.idBatchRegistration=b.idBatchRegistration',array("b.*"))
										  ->joinLeft(array("c" => "tbl_programmaster"),'a.idProgram=c.IdProgrammaster ',array("c.ProgramName"))
										  ->where("b.idBatchRegistration  = ?",$idPayment);	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}	
	
	public function fnInsertPaypaldetails($larrformData,$studentId,$idPayment){
  	
  		 $db = Zend_Db_Table::getDefaultAdapter();
          $table = "tbl_batchpaypal";
            $postData = array(		
							'IDCompany' => $idPayment,	
           					'paymentFee' =>$larrformData['mc_gross'],	
           					'grossAmount' => $larrformData['mc_gross'],		
            				'payerId' =>$larrformData['payer_email'],		
		            		'transactionId' =>$larrformData['txn_id'],  
            				'verifySign' =>$larrformData['verify_sign'],  
            				'entryFrom' => 0,                          
                            'UpdDate' => $larrformData['UpdDate'],	
            				'UpdUser' =>$larrformData['UpdUser'],
                            'paymentStatus'=> 1											
						);	
					
	     $db->insert($table,$postData);
		 $lastid  = $db->lastInsertId("tbl_batchpaypal","idpaypalDetails");		

		//Function to check if regpin already generated or not.
		
		$lstrSelect = $lobjDbAdpt->select()
		                          ->from(array("tbl_batchregistration"),array("idBatchRegistration"))
		                          ->where("idBatchRegistration = ?",$idPayment)
		                          ->where("registrationPin=0");
		 $larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
		 if($larrResult){		
				$larrformData1 ['registrationPin'] = $larrformData ['Regid'];
				$larrformData1 ['paymentStatus'] = 1;
				$larrformData1 ['Approved'] = 1;
		 }else{		 		
				$larrformData1 ['paymentStatus'] = 1;
				$larrformData1 ['Approved'] = 1;
		 }
		 

		/* $larrformData1['registrationPin'] = $larrformData['Regid'];
		 $larrformData1['paymentStatus'] = 1;	
		 $larrformData1['Approved'] = 1;*/
		 
		 
		 $where = "idBatchRegistration = '".$idPayment."'"; 	
		 $db->update('tbl_batchregistration',$larrformData1,$where); 
		 
  	
  } 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/*
 * function to fetch all the batch details
 */
	public function fnGetBatchName(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_BatchMaster"),array("key"=>"a.IdBatch","value"=>"a.BatchName"))
										  ->where("a.BatchStatus  = ?","0");	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
			}	
			
 /*
  * funtion to fetch all the venue based on the batch
  */			
  public function fnGetVenueName($lintidscheduler)
  {
  		  $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_center"),array("key"=>"b.idschedulervenue","value"=>"a.centername"))
										  ->join(array("b" =>"tbl_schedulervenue"),'a.idcenter = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulermaster"),'b.idschedulermaster = c.idschedulermaster',array())
										  ->where("c.idschedulermaster  = ?",$lintidscheduler)
										  ->order("a.city");
		  $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
		  return $larrResult;
		  
  }
  /*
   * function to fetch the timings based on the venue("CONCAT(TRIM(a.FirstNm),'_',IFNULL(TRIM(a.MiidleNM),''),'',IFNULL(TRIM(a.LastNM),'')) AS EmployeeName")
   */
  public function fnGetVenueTime($idvenue)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		/* 	echo 	$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("key"=>"a.idschedulervenuetime","value"=>("distinct(a.Date) as Date")))
										 // ->join(array("b" =>"tbl_center"),'a.idcentre = b.idcenter',array())
										  ->join(array("c" =>"tbl_schedulervenue"),'a.idschedulervenue = c.idschedulervenue',array())
										  ->where("c.idschedulervenue  = ?",$idvenue);die();*/
 	$lstrSelect="SELECT (a.Date)as `key`,(a.Date)as value FROM `tbl_schedulervenuetime` AS `a`
 				 INNER JOIN `tbl_schedulervenue` AS `c` ON a.idschedulervenue = c.idschedulervenue WHERE (c.idschedulervenue  = $idvenue)group by value";
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }

	public function fnGetInitialConfigDetails($iduniversity) {
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $select = $lobjDbAdpt->select()
					->from(array("a" => "tbl_config"),array("a.*"))				
		            ->where("a.idUniversity = ?",$iduniversity);	
		 return $result = $lobjDbAdpt->fetchRow($select);
	}
	
  public function fnGetbatch($idvenue,$datetocmp)
  {
  			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
/*			echo $lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_BatchMaster"),array("key"=>"a.IdBatch","value"=>("a.BatchName")))
										  ->join(array("b" =>"tbl_programmaster"),'a.IdProgrammaster = b.IdProgrammaster',array())
										  ->where("b.IdProgrammaster  = ?",$idvenue);die();*/
  			
 $lstrSelect="SELECT `a`.`IdBatch` AS `key`, CONCAT(DATE_FORMAT(`a`.`BatchFrom`,'%d-%m-%Y'),'---',DATE_FORMAT(`a`.`BatchTo`,'%d-%m-%Y')) AS `value` FROM `tbl_BatchMaster` AS `a` INNER JOIN `tbl_programmaster` AS `b` ON a.IdProgrammaster = b.IdProgrammaster WHERE (b.IdProgrammaster =1) and  a.BatchFrom > '$datetocmp'";			
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  /*
   * function to fetch all the scheduler based on the prog
   */
 public function fnGetscheduler($idbatch)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulermaster"),array("key"=>"a.idschedulermaster","value"=>("a.ScheduleName")))
										  ->where("a.idBatch  = ?",$idbatch);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  
  /*
   * function to get the time
   */
  public function fnGetTimings($date,$venue)
  {
  	
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("key"=>"a.idschedulervenuetime","value"=>("a.From")))
										  ->where("a.Date = '$date'")
										  ->where("a.idschedulervenue=?",$venue);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
  }
  /*
   * fun;ction to insert into the table
   */
   public function fnAddStudent($larrformData) { //Function to get the user details
   	//$larrformData['workPhone'] = $larrformData['workcountrycode']."-".$larrformData['workstatecode']."-".$larrformData['workPhone'];
    	//$larrformData['HomePhone'] = $larrformData['homecountrycode']."-".$larrformData['homestatecode']."-".$larrformData['HomePhone'];
		//$larrformData['CellPhone'] = $larrformData['countrycode']."-".$larrformData['statecode']."-".$larrformData['CellPhone'];
		//unset($larrformData['countrycode']);
	//unset($larrformData['statecode']);
		//unset($larrformData['workcountrycode']);
		//unset($larrformData['workstatecode']);
		//unset($larrformData['homecountrycode']);
		//unset($larrformData['homestatecode']);
        $result = $this->insert($larrformData);
        return $result;
     }
     

     
     /*
      * function to fetch all the details
      */
     public function fnviewstudentdetails($lintidstudent)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_studentapplication"),array('a.*'))
										  ->where("a.IDApplication  = ?",$lintidstudent);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
     
     /*
      * function to update the student details
      */
     public function updatestudent($lintstudentid,$larrformData)
     {
     	/*$larrformData['HomePhone'] = $larrformData['homecountrycode']."-".$larrformData['homestatecode']."-".$larrformData['HomePhone'];
		$larrformData['CellPhone'] = $larrformData['countrycode']."-".$larrformData['statecode']."-".$larrformData['CellPhone'];
		unset($larrformData['countrycode']);
		unset($larrformData['statecode']);
		unset($larrformData['homecountrycode']);
		unset($larrformData['homestatecode']);*/
     	
     	$db 	= 	Zend_Db_Table::getDefaultAdapter();		
		$where = "IDApplication = '".$lintstudentid."'"; 	
		return $db->update('tbl_studentapplication',$larrformData,$where);
     }
     
     /*
      * functin to search the student
      */
	public function fnSearchStudent($post = array()) { //Function for searching the user details
    	$db = Zend_Db_Table::getDefaultAdapter();
		$field7 = "Active = ".$post["field7"];
		$select = $db->select() 	
			   ->join(array('a' => 'tbl_studentapplication'),array('a.*'))
				->where('a.LName like "%" ? "%"',$post['field3'])
			   ->where('a.FName like  "%" ? "%"',$post['field2'])
			   ->where('a.MName like "%" ? "%"',$post['field4'])
			   //
			   ->where($field7);
		$result = $db->fetchAll($select);
		return $result;
	}
	/*
	 * function for fetching details
	 * 
	 */
         public function fnGetStudentName($idstudent) {
			$db =  Zend_Db_Table::getDefaultAdapter();    	
    		$sql = "SELECT CONCAT(FName,' ',IFNULL(MName,' '),' ',IFNULL(LName,' ')) AS Name  FROM tbl_studentapplication WHERE IDApplication = $idstudent";
    		$result = $db->fetchRow($sql);    
			return $result;
    	}
    	/*
    	 * Paypal Entries
    	 * */
  	

	
/*
 * function to fetch all the Prog Name
 */
	public function fnTakafuloperator(){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" => "tbl_takafuloperator"),array("key"=>"a.idtakafuloperator","value"=>"a.TakafulName"));	
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
		
	/*
	 * function to fetch all the amount for that course
	 */
	
	public function fnGetProgAmount($idprog){		
				$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = "SELECT sum(abc.amount) from 

(SELECT max(EffectiveDate),Rate as amount  from tbl_programrate where idProgram=$idprog group by `IdAccountmaster`) as abc ";
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	/*public function fnGetexamcenter()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_center"),array("a.*")) 				 
				 				 ->order("a.centername");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	*/
public function fnGetexamcenter($larrformData)
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("ns"=>"tbl_newscheduler"))
				 				 ->join(array("nsv"=>"tbl_newschedulervenue"),'ns.idnewscheduler = nsv.idnewscheduler')
				 				 ->join(array("c"=>"tbl_center"),'nsv.idvenue = c.idcenter')
				 				 ->join(array("nsc"=>"tbl_newschedulercourse"),'ns.idnewscheduler = nsc.idnewscheduler')
				 				 ;	
				 				 
	 		if(isset($larrformData['field1']) && !empty($larrformData['field1']) ){
				$lstrSelect = $lstrSelect->where("nsc.IdProgramMaster = ?",$larrformData['field1']);
			}
			
			if(isset($larrformData['Date1']) && !empty($larrformData['Date1']) && isset($larrformData['Date2']) && !empty($larrformData['Date2'])){
				
				//$lstrFromDate = date("Y-m-d",strtotime($larrformData['Date1']));
				//$lstrToDate = date("Y-m-d",strtotime($larrformData['Date2']));
				$lstrFrommonth = date("m",strtotime($larrformData['Date1']));
				$lstrTomonth = date("m",strtotime($larrformData['Date2']));
				$lstrFromyear = date("Y",strtotime($larrformData['Date1']));
				$lstrToYear = date("Y",strtotime($larrformData['Date2']));				
				
				$lstrSelect = $lstrSelect->where("ns.From <= $lstrFrommonth")	
										 ->where("ns.To >= $lstrTomonth")
										 ->where("ns.Year BETWEEN '$lstrFromyear' and '$lstrToYear'");			
			}	 						 	
						 
 			$lstrSelect=$lstrSelect->group("c.centername");
 			
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
		
public function fnGetcoursename()
	   {
	   		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
				 				 ->from(array("a"=>"tbl_programmaster"),array("a.*")) 				 
				 				 ->order("a.ProgramName");
			$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
			return $larrResult;
	   }	
	
	/*
	 * 
	 */
	public function fnGetTimingsForDate($date,$idscheduler)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect="SELECT idschedulervenuetime as `key`,`From` as value FROM `tbl_schedulervenuetime` WHERE `idschedulervenue`=$idscheduler and Date='$date'";
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	
	/*
	 * functin to fetch theno of seats
	 */
	public function fnGetNoOfSeats($lintidvenuetime)
	{
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_schedulervenuetime"),array("a.NoofSeats"))
										  //->from(array("b" =>"tbl_schedulervenuetime"),'a.idschedulervenue = b.idschedulervenue',array())
										  //->where("b.Date  = ?",$lintdate)
										    //->where("b.idcentre  = ?",$lintidvenue)
										    ->where("a.idschedulervenuetime =?",$lintidvenuetime);
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
	}
	
	/*
	 * function to get theno of students aplied for the same exam
	 */
  public function fnGetNoofStudents($lintidvenuetime)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$select ="select count(IDApplication) from tbl_studentapplication where DateTime=$lintidvenuetime";
  	$larrResult = $lobjDbAdpt->fetchRow($select);
				return $larrResult;
  }
  
  public function fnGetNoOfTakafulOperatorForStudent($idtakaful)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$select ="select count(IDApplication) from tbl_studentapplication where Takafuloperator=$idtakaful";
  	$larrResult = $lobjDbAdpt->fetchRow($select);
				return $larrResult;
  }
  
  public function fnGetNoOfTakafulOperator($idtakaful)
  {
  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
  	$select ="select NumberofSeat from tbl_takafuloperator where idtakafuloperator=$idtakaful";
  	$larrResult = $lobjDbAdpt->fetchRow($select);
				return $larrResult;
  }
	public function fnGetSMTPSettings(){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
       								->from(array("a"=>"tbl_config"),array("a.SMTPServer","a.SMTPUsername","a.SMTPPassword","a.SMTPPort","a.SSL","a.DefaultEmail") );
			$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
			return $larrResult;
		}
public function fnGetEmailTemplateDescription($TemplateName){
			$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
    	   							->from(array("a"=>"tbl_emailtemplate"))
       								->join(array("b" => "tbl_definationms"),"a.idDefinition = b.idDefinition",array(""))
       								->where("b.DefinitionDesc LIKE '".$TemplateName."%'");
       		$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
       		return $larrResult;
		}

		
	public function fngetprogramdiscount($idprog,$noofcandidates)
	{
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		 $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_programmaster"),array("a.*"))	
								 ->join(array("b"=>"tbl_programrate"),'a.IdProgrammaster=b.idProgram',array("b.*"))	
								 ->join(array("c"=>"tbl_accountmaster"),'b.IdAccountmaster=c.idAccount',array(""))							 						
								 ->where("a.IdProgrammaster=?",$idprog)
								 ->where("a.Active = 1")
								  ->where("c.Active = 1")
								  ->where("c.duringRegistration=0")
								  ->where("b.Active = 1");
		$larrResult = $lobjDbAdpt->fetchAll($lstrSelect); 
       
		for($i=0;$i<count($larrResult);$i++)
		{
			 $lstrSelect = $lobjDbAdpt->select()
								 ->from(array("a" =>"tbl_accounthead"),array("a.*"))
								 ->where("a.idAccount=?",$larrResult[$i]['IdAccountmaster'])
								 ->where("a.minnostd in (select b.minnostd from tbl_accounthead as  b where a.idAccount=b.idAccount and  b.minnostd <= $noofcandidates)")
                                 ->where("a.Active=1")
								 ->order("a.minnostd desc");								 
		    $larraysum = $lobjDbAdpt->fetchAll($lstrSelect);
		    $sum = $larraysum;
		  // $sum= $sum+$sum;
		}
		return $sum;
	}
	
	
	public function fnGetAmountInWords($Amount)
     {//echo $Amount;die;
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = "SELECT CONCAT(ucwords(str_numtowords($Amount)),' ','Only') as Amount";		
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect); 
				return $larrResult;
     }
     
	public function fnGetExamDetails($IdApplication)
     {
     		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
										  ->from(array("a" =>"tbl_batchregistration"),array('a.*'))
										  ->where("a.idBatchRegistration  = ?",$IdApplication);	
				$larrResult = $lobjDbAdpt->fetchRow($lstrSelect);
				return $larrResult;
     }
	
	
 	 public function updatecompanypass($password,$idcomp)
     {
     	 $db = Zend_Db_Table::getDefaultAdapter();
     	$data = array('Password' => $password);
		$where['IdCompany = ?']= $idcomp;		
		$db->update('tbl_companies', $data, $where);	
     }
     
	function listBatchApplication($idCompany){		
	  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()				
						  ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
						  ->join(array("b"=>"tbl_studentpaymentoption"),'a.idBatchRegistration=b.IDApplication',array("b.ModeofPayment"))
						  ->where("a.idCompany   = ?",$idCompany)
						  ->where('b.companyflag = 1');
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	  }

	  
	  
 	 public function fngetnoofstudentsfromexcel($ids,$idtakaful)
     { // Get remaining number of students in a batch
     			 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				 $lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_tempexcelcandidates"),array("a.idcandidates","a.StudentName","a.ICNO","a.DOB","a.Race","a.Takafuloperator","a.idprogram","a.email","a.education","a.Gender","a.Address","a.session","a.ContactNo","a.MobileNo"))							 				
							 				 ->where("a.RegistrationPin = ?",$ids)
							 				 ->where("a.ICNO not in (select ICNO from tbl_studentapplication where batchpayment = 1)");
				 $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				 return $larrResult;
     	
     }

     public function fngetexcelappliedcandidates($ids,$idtakaful)
     { // Get excel applied candidates details     	
     	 		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
							 				 ->from(array("a"=>"tbl_studentapplication"),array("a.FName","a.ICNO","a.DateTime"))	
							 				 ->join(array("e"=>"tbl_registereddetails"),'a.IDApplication = e.IDApplication',array("e.Regid")) 
							 				 ->join(array("b"=>"tbl_programmaster"),'a.Program = b.IdProgrammaster',array("b.ProgramName")) 
							 				 ->join(array("c"=>"tbl_managesession"),'a.Examsession = c.idmangesession',array("c.managesessionname")) 	
							 				 ->join(array("d"=>"tbl_center"),'a.Examvenue = d.idcenter',array("d.centername")) 						 				
							 				 ->where("a.batchpayment = 1")					
							 				  ->where("e.RegistrationPin = ?",$ids);				 				 
							 				// ->where("a.ICNO in (select ICNO from tbl_tempexcelcandidates where RegistrationPin = '$ids' )");
							 				// echo $lstrSelect;exit;				
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;     	
     	
     }
	  
	  
	/*public function listBatchApplication($idCompany){		
	  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()				
						  ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
						  ->join(array("c" =>"tbl_studentpaymentoption"),'a.idBatchRegistration = c.IDApplication AND C.companyflag=2')
						  ->where("a.idCompany   = ?",$idCompany);
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	  }*/
	function listBatchApplicationDetails($idApp){
	
	  	$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
				$lstrSelect = $lobjDbAdpt->select()
				
						  ->from(array("a" => "tbl_batchregistrationdetails"),array("a.*"))
						  ->where("a.idBatchRegistration  = ?",$idApp)
						  ->order("a.idProgram asc");
				$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				
				return $larrResult;
	  }
	
}
