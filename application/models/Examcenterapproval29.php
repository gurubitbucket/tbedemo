<?php
class App_Model_Examcenterapproval extends  Zend_Db_Table  {
	protected $_name = 'tbl_studentpaymentoption';			
	
	
	public function fnGetCompanyDetails()
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
			$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_batchregistration"),array("a.*"))
											  ->join(array("b" =>"tbl_companies"),'a.idCompany=b.IdCompany')
											  ->join(array("c" =>"tbl_studentpaymentoption"),'c.IDApplication=a.idBatchRegistration')
											  ->where("a.paymentStatus =2")
											  ->where("a.Approved=0")
											  ->where("c.companyflag=1")
											  ->where("c.Modeofpayment=4");
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
  public function fnGetStudentdetails($date,$month,$year,$center)	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_studentapplication"))
											  ->join(array("b"=>"tbl_registereddetails"),'a.IDApplication=b.IDApplication')
											    ->join(array("c"=>"tbl_newscheduler"),'c.idnewscheduler=a.Year')
											    ->where("b.Approved=1")
											   ->where("a.Examdate =?",$date)
											  ->where("c.Year=?",$year)
											  ->where("a.Exammonth=?",$month)
											  ->where("b.Cetreapproval=0")
											  ->where("a.Examvenue =?",$center);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
	
	public function fnStudentApproved($larrformdata)
	{
		/*echo "<pre/>";
		print_r($larrformdata);
		die();*/
	   $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$tableName = "tbl_registereddetails";

	    for($i = 0; $i<count($larrformdata['idregistereddetails']); $i++ )
			{		
				$idregistereddetails = $larrformdata['idregistereddetails'][$i];
				$postData = array('Cetreapproval' => 1);			
                $where = "idregistereddetails=$idregistereddetails";
				$lobjDbAdpt->update($tableName,$postData,$where);
				//self::fnGetStudentapproved($idBatchRegistration);
			}
			
	}
	
	
	public function fnGetStudent($lstrType)
	{
		 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
					$lstrSelect = $lobjDbAdpt->select()
											  ->from(array("a" =>"tbl_registereddetails"),array("a.*"))
											 ->join(array("b" =>"tbl_studentapplication"),'a.IDApplication=b.IDApplication')
											  ->join(array("f" =>"tbl_programmaster"),'b.Program=f.IdProgrammaster')
											  ->join(array("c" =>"tbl_state"),'b.ExamState=c.idState',array("c.*"))
											  ->join(array("d" =>"tbl_city"),'b.ExamCity=d.idCity',array("d.*"))
											   ->join(array("e" =>"tbl_center"),'b.Examvenue=e.idcenter',array("e.*"))
											  ->where("a.Approved=1")
											    ->where("a.Cetreapproval=0")
											  //->where("a.Registrationpin=0000000")
											  ->where("b.IDApplication = ?",$lstrType);
					$larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
					return $larrResult;
	}
	
 
}
