<?php
class SpecialtkloginController extends Zend_Controller_Action {
	
 	public $gsessionbatch;//Global Session Name
 	private $_gobjlogger;
	public function init() { //instantiate log object
	$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	}
	
    public function indexAction() {

	}

    function loginAction() {
    	$this->_helper->layout->disableLayout (); //
		$this->gsessionbatch= new Zend_Session_Namespace('sis');  
        $lobjform = new App_Form_Login(); //intialize login form
                
        $this->view->lobjform = $lobjform; //send the form object to the view
        
        if ($this->_request->isPost()) {
        	
	        Zend_Loader::loadClass('Zend_Filter_StripTags');
	        $filter = new Zend_Filter_StripTags();	       	        
	        if ($this->_request->getPost( 'ses_username')){    
	        	$_SESSION['pagename'] = 'introduction';   		
	       	 	$username = $filter->filter($this->_request->getPost('ses_username'));
	        	$password = $filter->filter($this->_request->getPost('ses_userpass'));
	        }
	        else{
	        	$_SESSION['pagename'] = 'batchlogin';
	        	$username = $filter->filter($this->_request->getPost('username'));
	            $password = $filter->filter($this->_request->getPost('password'));
	        }
	        
	        $ModelSpecialtklogin = new App_Model_Specialtklogin();
	        $resultCheck = $ModelSpecialtklogin->fngetlogincheck($username,md5($password));
	       
	        if($resultCheck){
	        	
	        	$this->gsessionbatch->__set('idtk',$resultCheck['idtakafuloperator']);
	        	$auth = Zend_Auth::getInstance();
				    	
	        	echo "<script>parent.location = '".$this->view->baseUrl()."/specialstatusreport/index';</script>";	
	        	exit;
	        }
	        else {
	        	$this->view->alertError = 'Login failed. Either username or password is incorrect';
            	
            	//echo "<script>parent.location = '".$this->view->baseUrl()."/batchlogin/login";
            	if($_SESSION['pagename'] ==  'introduction'){
            		echo "<script>alert('Login failed. Either username or password is incorrect');</script>";
            		echo "<script>parent.location = '".$this->view->baseUrl()."/introduction/index/page/company';</script>";
            		exit;
            	}
            	else{
            		echo "<script>parent.location = '".$this->view->baseUrl()."/specialtklogin/login';</script>";
            	}
            }  
	        
            
            
            
           /* 
            
	        
			$dbAdapter = Zend_Db_Table::getDefaultAdapter();
			$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
				
			$authAdapter->setTableName('tbl_companies')
			    		->setIdentityColumn('Login')
			    		->setCredentialColumn('Password');
			    		
            $authAdapter->setIdentity($username);
            $authAdapter->setCredential(md5($password));
            
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);
            echo "<pre>";
            print_r($result);
            exit;
            if ($result->isValid()) {
				$data = $authAdapter->getResultRowObject(null, 'passwd');
				$auth->getStorage()->write($data);
				$auth->getIdentity()->iduser;
				
				
                $larrCommonModel = new App_Model_Common();
                $Rolename = $larrCommonModel->fnGetRoleName($auth->getIdentity()->IdRole);
                
				if($Rolename['DefinitionDesc']== "Admin") {
					$this->gstrsessionSIS->__set('idUniversity',1);
					$this->gstrsessionSIS->__set('idCollege',0);
					// user type 0:college  1: branch
					$this->gstrsessionSIS->__set('userType',0);
					$this->gstrsessionSIS->__set('rolename',$Rolename['DefinitionDesc']);
				} else {
					$staffdetails = $larrCommonModel->fnGetStaff($auth->getIdentity()->IdStaff);
					$Universitydetails = $larrCommonModel->fnGetUniversity($staffdetails['IdCollege']);

					$this->gstrsessionSIS->__set('idUniversity',$Universitydetails['AffiliatedTo']);
					$this->gstrsessionSIS->__set('idCollege',$staffdetails['IdCollege']);
					$this->gstrsessionSIS->__set('userType',$staffdetails['StaffType']);  // user type 0:college  1: branch
					$this->gstrsessionSIS->__set('rolename',$Rolename['DefinitionDesc']);
				}
				echo "<script>parent.location = '".$this->view->baseUrl()."/batch-reg/companyapplication/index';</script>";	
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'user', 'action'=>'index'),'default',true));
            } else {
            	$this->view->alertError = 'Login failed. Either username or password is incorrect';
            }  */   
        }
    	else{
        	$_SESSION['pagename'] =  'batchlogin';
        }
		$this->render(); //render the view
    }


    public function logoutAction() {
    	
    	$this->_helper->layout->disableLayout (); 
    	$this->_helper->viewRenderer->setNoRender();
    	
    	$auth = Zend_Auth::getInstance();
    	 // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged Out"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
			
    	 	Zend_Session:: namespaceUnset('sis');
			echo "<script>parent.location = '".$this->view->baseUrl()."/index/login';</script>";exit;
   		/* if($_SESSION['pagename'] ==  'introduction'){
            		echo "<script>parent.location = '".$this->view->baseUrl()."/introduction/index/page/company';</script>";exit;
            	}
            	else{
            		echo "<script>parent.location = '".$this->view->baseUrl()."/batchlogin/login';</script>";
            	}*/
    }
}