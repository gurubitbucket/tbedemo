<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class ChronejobsController extends Zend_Controller_Action
{
	public function init() 
	{

   	    $this->lobjautomailmodel = new Examination_Model_Automailmodel(); //intialize newscreen db object

   	    $this->lobjAutovenuemodel = new Examination_Model_Autovenue();
		$this->ModelBatchlogin = new App_Model_Batchlogin();
		


	}
	public function indexAction() 
	{
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		
		////////////////Force LogOut/////////////////
		$userlastactivity = $this->ModelBatchlogin->fngetisonlinestatus();
		
		
		if(!empty($userlastactivity)){
			$logincredentials=array();
			for($p=0;$p<count($userlastactivity);$p++)
			{
				$logincredentials[$p]['iduser']=$userlastactivity[$p]['Idusers'];
				$logincredentials[$p]['Typeofuser']=$userlastactivity[$p]['Typeofuser'];
				$logincredentials[$p]['logouttime']=date("Y-m-d H:i:s");
			}
			//echo "<pre>";print_r($logincredentials);die();
			$this->ModelBatchlogin->fnupdateisonlinestatus($logincredentials);
		}
		////////////////end of force logout////////////////////////////
	    /*************************************************************/
		
		
		
		////////////////Changes by Vineet Kulkarni 15.04.2015//////////
		$companycandidateexpirydate=$this->ModelBatchlogin->fetchcompanycandidateexpiry();		
		$checkpaymentstatus=$this->ModelBatchlogin->updatepaymentstatus($companycandidateexpirydate['Paymentorder5']);		
		//echo "<pre/>";print_r($checkpaymentstatus);die();		
		////////////////End of Changes by Vineet Kulkarni 15.04.2015///
		
		
	                   //echo    $Currentdate = date('Y-m-d H:i');
		$larr = explode(' ',$Currentdate);
		$shcdate = $larr[0];
		$shcdtime = $larr[1] = $larr[1].':00';
		$larrresult = $this->lobjautomailmodel->fngetscheduledetails($shcdate,$shcdtime);
		
		if($larrresult)
		{
			$larremailtempdetails = $this->lobjautomailmodel->fngetemailtempdetails();
			for($linti = 0;$linti<count($larrresult);$linti++)
			{
				$larrstudresult = $this->lobjautomailmodel->fngetiddetails($larrresult[$linti]['idautomail']);
				for($lintj = 0;$lintj<count($larrstudresult);$lintj++)
			    {
			    //$updstatus = $this->lobjautomailmodel->fnupddetails($larrstudresult[$lintj]['idapplication']);
			    $from = $larremailtempdetails[0]['TemplateFrom'];
				$subject = $larremailtempdetails[0]['TemplateSubject'];
				$larremailresultsetdata = $this->lobjautomailmodel->fngetsendingdetails($larrstudresult[$lintj]['idapplication']);
				$stremailTemplateBody =  $larremailtempdetails[0]['TemplateBody'];
				$stremailTemplateBody = str_replace("[Student]",$larremailresultsetdata['FName'],$stremailTemplateBody);
				$stremailTemplateBody = str_replace("[LoginID]",$larremailresultsetdata['username'],$stremailTemplateBody); 
				$stremailTemplateBody = str_replace("[Passwd]",$larremailresultsetdata['password'],$stremailTemplateBody);
				$auth = 'ssl';
				$port = '465';
				$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
				$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
				$mail = new Zend_Mail();
				$mail->setBodyHtml($stremailTemplateBody);
				$sender_email = 'itwinesgm@gmail.com';
				$sender = 'Paramesh122';
				$receiver_email = 'parameshvec@gmail.com'; //kashinath62@gmail.com;
				$receiver = 'Paramesh';//$larremailresultsetdata['Name'];
				$mail->setFrom($sender_email, $sender)
					 ->addTo($receiver_email, $receiver)
					 ->setSubject($subject);
				try {
					 $resultemails = $mail->send($transport);
					} 
				catch (Exception $e)
					{
						echo '<script language="javascript">alert("Unable to send mail at this time because of some technical problems")</script>'; 
					    die();
				    }
					
			    }
			    //function to update the scheduler mail status
			}
		}
		
		$lobjchroneclientmodel = new Examination_Model_Chroneclient();
		
		$larrresultidautomails = $this->lobjAutovenuemodel->fngetscheduledetails($shcdate,$shcdtime);
		$totalcountofidautomails = count($larrresultidautomails);
	
		if($totalcountofidautomails>0)
		{
			$idautomail = 0;
			for($i=0;$i<count($larrresultidautomails);$i++)
			{
				$idautomail = $idautomail.','.$larrresultidautomails[$i]['idautomail'];
			}
		
			////for questions updating functions/////////////////////////
			$larresultquestionsid = $this->lobjAutovenuemodel->fngetallvenuesforidautomail($idautomail);
			
			$venuequestioncount = count($larresultquestionsid);
			//print_R($venuequestioncount);			
			if($venuequestioncount>0)
			{
				
				$larrmainquestions = $this->lobjAutovenuemodel->fngetquestions();
				$larrmainanswers = $this->lobjAutovenuemodel->fngetanswers();
				   for($j=0;$j<$venuequestioncount;$j++)
				   {
				   	
				   	$larresultidautovenueslist = $this->lobjAutovenuemodel->fngetvenuesforidautomail($larresultquestionsid[$j]['idautomail']);
				  	$larrchronevenuedetails = $lobjchroneclientmodel->fninsertclientquestions($larresultidautovenueslist,$larrmainquestions,$larrmainanswers);
				   }
			}
			
			/////////////////end of questions functioning/////////////////////
			
			
			////////////////start of the scheduler functioning/////////////////
			$larrresultidschedulers = $this->lobjAutovenuemodel->fngetallschedulervenuesforidautomail($idautomail);
			$venueschedulercount = count($larrresultidschedulers);
			if($venueschedulercount>0)
			{
				 for($s=0;$s<$venueschedulercount;$s++)
				   {
				   
					$larresultschedulervenuelist = $this->lobjAutovenuemodel->fngetscheduleridautomail($larrresultidschedulers[$s]['idautomail']);
				
					$larrchronevenuescheduler = $lobjchroneclientmodel->fninsertclientscheduler($larresultschedulervenuelist);
					
				   }
			}
			/////////////end of the scheduler scheduling//////////////////////////
			
			$larrresultidcandidatesscheduler = $this->lobjAutovenuemodel->fngetallcandidatesvenuesforidautomail($idautomail);
			$venueschedulercandidatescount = count($larrresultidcandidatesscheduler);
			if($venueschedulercandidatescount>0)
			{
				for($c=0;$c<$venueschedulercandidatescount;$c++)
				   {
				     	$larresultschedulerstudentslist = $this->lobjAutovenuemodel->fngetschedulerstudentsvenuesidautomail($larrresultidcandidatesscheduler[$c]['idautomail']);
				     	$larrchronevenuedetails = $lobjchroneclientmodel->fninsertclientmodel($larresultschedulerstudentslist);
				   }
			}
		}
		
		
			//////Chrone pull//////////////////////////////////////////////////////////
		$this->lobjChronepullmodel = new Examination_Model_Chronepull(); 
		$Currentdate = date('Y-m-d H:i'); 
		$lobjchronepullclientmodel = new Examination_Model_Chronepullclient();
		$larr = explode(' ',$Currentdate);
		$shcdate = $larr[0];
		
		$shcdtime =$larr[1] = $larr[1].':00';
		$larrresultidautomails = $this->lobjChronepullmodel->fngetscheduledetails($shcdate,$shcdtime);
		$totalcountofidautomails = count($larrresultidautomails);
	
		if($totalcountofidautomails>0)
		{
			$idautomail = 0;
			for($i=0;$i<count($larrresultidautomails);$i++)
			{
				$idautomail = $idautomail.','.$larrresultidautomails[$i]['idautochronepull'];
			}

			$larresultvenuesid = $this->lobjChronepullmodel->fngetallvenuesforidautomail($idautomail);
			
			$venuecount = count($larresultvenuesid);
					
			if($venuecount>0)
			{
				 for($j=0;$j<$venuecount;$j++)
				   {
					   	
					   	$Venue = $larresultvenuesid[$j]['Venue'];
					   	$Session = $larresultvenuesid[$j]['Session'];
					   	$Examdate = $larresultvenuesid[$j]['Examdate'];
					   	$idchronepull = $larresultvenuesid[$j]['idchronepull'];
					   	$larresultidautovenueslist = $lobjchronepullclientmodel->fngetstatusforthevenue($Venue,$idchronepull);
					   	if($larresultidautovenueslist==1)
					   	{
					   		
					   		    $larrclosetimefinding = $lobjchronepullclientmodel->fnfindthemaxtimeofprogram($Venue,$Session,$Examdate);
					   		    $maxprogramtime = $larrclosetimefinding['maximumtime'];
					   		    
					   		    $larresultsmaxtime = $lobjchronepullclientmodel->fnfindthemaxstarttimeofvenue($Venue,$Session,$Examdate);
								$maxstartedtime = $larresultsmaxtime['maximumstartedtime'];
								
								$totaltime=$maxprogramtime+$maxstartedtime+2; 
		
								$currenttime = date('H');
								$min = date('i');
								
								//echo $selecteddate;
								$currenttimeinmin = $currenttime*60+$min;
								$currentdate =  date('Y-m-d');
								$activatetheclosetime=0;
								
								/*echo $currenttimeinmin;
								echo $totaltime;
								die();*/
					   			if($currentdate>$Examdate)
								{
									$activatetheclosetime=1;
								}
								
								if($activatetheclosetime==0)
								{
						   			if($currenttimeinmin>$totaltime)
									{
										$activatetheclosetime=1;
									}
									else
									{
							 			$activatetheclosetime=0;
									}
								}
							/*	print_r($activatetheclosetime);
								die();*/
								if($activatetheclosetime==1)
								{
							   		    $larrcandidatepresent = $lobjchronepullclientmodel->closetimeimplementation($Venue,$Session,$Examdate);
							   		
							   			$larrexamstudents = $lobjchronepullclientmodel->fngetstudentsexamdetails($Venue,$Session,$Examdate);
							   			//function to update the student pass,fail, and inserting the student marks,details marks etc on the server
			    						$larupdate = $this->lobjChronepullmodel->fnupdatestudent($larrexamstudents);
			    						$IDApplication = 0;$Regid=0;
							    		for($linti=0;$linti<count($larrexamstudents);$linti++)
							    		{
							    			$value=$larrexamstudents[$linti]['IDApplication'];
							    			$valueregids = $larrexamstudents[$linti]['Regid'];
							    			$Regid = $Regid.','."'$valueregids'";
							    			$IDApplication = $IDApplication.','."'$value'";
							    		}
			    					  $larranswerdetails = $lobjchronepullclientmodel->fngetanswerdetails($Venue,$Regid);
							   		  if(count($larranswerdetails)>0)
							            {
							    				$larranswerdetails = $this->lobjChronepullmodel->fninsertanswerdetailsinserver($larranswerdetails);
							            }
							            ////absentese student/////////////////////////////////////////////
							    		$larrexamstudentabsent = $lobjchronepullclientmodel->fngetstudentsabsent($Venue,$Session,$Examdate);
							    		$larupdatess = $this->lobjChronepullmodel->fnupdatestudentabsent($larrexamstudentabsent);
							    		
							    		
							    		$larresultstarttimeincenter = $this->lobjChronepullmodel->fngetcenterstartfromserver($Venue,$Session,$Examdate);
										$larrclosetimeupdate = $lobjchronepullclientmodel->fngetclosetimeserver($Venue,$Session,$Examdate);
										if(count($larresultstarttimeincenter)>0)
										{
											$larrresultupdateonserver = $this->lobjChronepullmodel->fnupdateclosetimeserver($larrclosetimeupdate);
										}
										else 
										{
											$larrresultupdateonserver = $this->lobjChronepullmodel->fninsertexamcentermainserver($larrclosetimeupdate);
										}
										
									$larrresultupdateonserver = $this->lobjChronepullmodel->fnupdatedchronedstatussucess($idchronepull);	
									$larrexamstudentabsent = $lobjchronepullclientmodel->fngetupdatepushstatus($Venue,$Session,$Examdate);
////////////////////////////////////////////////////to send mails////////////////////////////////////////////////////////////


$larrgetresulmails=$lobjchronepullclientmodel->fngetreultmails($Venue,$Session,$Examdate);
										if(!empty($larrgetresulmails))
										{
											
											$larrinsertmainserver= $this->lobjChronepullmodel->fninsertresultmailsmainserver($larrgetresulmails);
										}


/////////////////////////////////////////////////////////////////////end of send mails/////////////////////////////////////////

						
								}
								
								if($activatetheclosetime==0)
								{
									 	$larresultidautovenueslist = $lobjchronepullclientmodel->fnupdatedchronedstatusfailed($idchronepull);
								}
					   	}
					   								
				   }
			}
		}
		
		




 ///auto chrone pull issues from local server
		$this->lobjChronepullissuemodel = new Examination_Model_Chronepullissuemodel(); 
		 $Currentdate = date('Y-m-d H:i'); 
		//$lobjchronepullclientmodel = new Examination_Model_Chronepullclient();
		$larr = explode(' ',$Currentdate);
		 $shcdate = $larr[0];
		
		 $shcdtime =$larr[1] = $larr[1].':00';
		$larrresultidautomails = $this->lobjChronepullissuemodel->fngetscheduledetails($shcdate,$shcdtime);
		
		$totalcountofidautomails = count($larrresultidautomails);
		
		if($totalcountofidautomails>0)
		{
			$idautomail = 0;
			for($i=0;$i<count($larrresultidautomails);$i++)
			{
				$idautomail = $idautomail.','.$larrresultidautomails[$i]['Idautoissue'];
			}
//echo $idautomail;die();
		    $larresultvenuesid = $this->lobjChronepullissuemodel->fngetallvenuesforidautoissuemail($idautomail);
			
			$venuecount = count($larresultvenuesid);
					
			if($venuecount>0)
			{
				 for($j=0;$j<$venuecount;$j++)
				   {
				     $Venue = $larresultvenuesid[$j]['Examcenter'];
					 $Examdate = $larresultvenuesid[$j]['Examdate'];
					   $idchronepull = $larresultvenuesid[$j]['Idautoissue'];
				       $larresultidautovenueslist = $this->lobjChronepullissuemodel->fngetstatusforthevenue($Venue,$idchronepull);
		           
 if($larresultidautovenueslist == 1)
					   	{
					   		 
					   		    $larrclosetimefinding = $this->lobjChronepullissuemodel->fnfindthemaxtimeofprogram($Venue,$Examdate);
					   		     $maxprogramtime = $larrclosetimefinding['maximumtime'];
					   		    
					   		    
					   		   
					   		    $larresultsmaxtime = $this->lobjChronepullissuemodel->fnfindthemaxstarttimeofvenue($Venue,$Examdate);
								$maxstartedtime = $larresultsmaxtime['maximumstartedtime'];
								
								$totaltime=$maxprogramtime+$maxstartedtime+2; 
		
								$currenttime = date('H');
								$min = date('i');
								
								//echo $selecteddate;
								$currenttimeinmin = $currenttime*60+$min;
								$currentdate =  date('Y-m-d');
								$activatetheclosetime=0;								
					   			if($currentdate>$Examdate)
								{
									$activatetheclosetime=1;
								}
								
								if($activatetheclosetime==0)
								{
						   			if($currenttimeinmin>$totaltime)
									{
										$activatetheclosetime=1;
									}
									else
									{
							 			$activatetheclosetime=0;
									}
								}
// echo $activatetheclosetime ;die(); 							
								if($activatetheclosetime==1)
								{
							   			$larrexamstudents = $this->lobjChronepullissuemodel->fngetstudentsexamissuedetails($Venue,$Examdate);
										//echo "<pre>";
										//print_r($larrexamstudents);die();
							   			if($larrexamstudents)
										{

										   
			    						     $larupdate = $this->lobjChronepullissuemodel->fnupdatestudent($larrexamstudents);			    						
										     $larrresultupdateonserver = $this->lobjChronepullissuemodel->fnupdatedchronedsucess($idchronepull);
										     $larrexamstudentabsent = $this->lobjChronepullissuemodel->fngetupdatepushstatus($Venue,$Examdate);
										   
										}
										else
										{

										   $larresultidautovenueslist = $this->lobjChronepullissuemodel->fnupdatedchronedstatusfailed($idchronepull);
										}
											
											
								}
					   	}
						if($larresultidautovenueslist==0)
						{

									 	$larresultidautovenueslist = $this->lobjChronepullissuemodel->fnupdatedserverdownchronedstatus($idchronepull);
						}
		            }
			}
		}
		//end of auto chronepull issues
			
		
	//////////////////////////duplicate rows in register table//////////////////////
		$current_time = (int) date('Hi');
		echo $current_time;
		if($current_time==2355){
			$this->lobjAutochronepushmodel = new Examination_Model_Autochronepush();
			$larrduplicateregid = $this->lobjAutochronepushmodel->fngetduplicateregid();
			if(count($larrduplicateregid)>0){
				//send mail
					$auth = 'ssl';
					$port = '465';
					$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
					$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
					$mail = new Zend_Mail();
					$imgsrc = $this->view->serverUrl().$this->view->baseUrl().'/images/reportheader.jpg';
					$tabledata = "<html><body><img src='$imgsrc' width='100%' height='100px' alt='reportheader.jpg'>";
					$tabledata.="<br><legend><p style='color:red;'>Duplicate rows inserted Details in tbl_registereddetails </p></legend>			
								<table class='table' border=1 width=100%>
									<tr>
										<th><b>SI.No</b></th>
										<th><b>Register Number</b></th>
										<th><b>Number of times Repeated</b></th>
										
									</tr>";
						$i=0;
						foreach($larrduplicateregid as $lobjCountry){
							
							$tabledata.= "<tr>
											<td>".++$i."</td>
											<td>".$lobjCountry['Regid']."</td>
											<td>".$lobjCountry['totalrepetion']."</td>
											
										</tr>";
						}
					$tabledata.="</table></body></html><br>";
					//echo $tabledata;die();
					$mail->setBodyHtml($tabledata);
					$subject = "Duplicate values inserted";
					$sender_email = 'itwinesgm@gmail.com';
					$sender = "Paramesh reddy 122";
					$receiver_email = 'parameshvec@gmail.com'; //kashinath62@gmail.com;
					$receiver = 'Paramesh';//$larremailresultsetdata['Name'];
					$mail->setFrom($sender_email, $sender)
						 ->addTo($receiver_email, $receiver)
						 ->setSubject($subject);
					try {
						 $resultemails = $mail->send($transport);
						 $mailsent  = 1;
						 //echo '<script language="javascript">alert("Mail sent")</script>';
						} 
					catch (Exception $e)
						{
							//echo '<script language="javascript">alert("Unable to send mail at this time because of some technical problems")</script>';
							$mailsent  = 0; 
						}
			}
		}//////////////////////////end of duplicate rows in register table//////////////////////
		
		
	}
}	
			
