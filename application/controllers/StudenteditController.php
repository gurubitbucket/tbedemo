<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class StudenteditController extends Zend_Controller_Action { //Controller for the User Module

	
public function init() { //initialization function
		$this->_helper->layout()->setLayout('/batch/usty1');	
		//$this->Url = "http://192.168.1.103/tbe";	
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjstudentmodel = new App_Model_Studentedit(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
			//$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
		$this->lobjstudentForm = new App_Form_Studentapplication(); //intialize user lobjuserForm
		$this->lobjCommon=new App_Model_Common();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		  	$this->lobjdeftype = new App_Model_Definitiontype();
	}
	

public function indexAction() { // action for search and view
	
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		$count=0;
    $larrinitialresult=$this->lobjstudentmodel->fngetintialconfigdetails();
	if($larrinitialresult[FldTxt1])
	{
		$this->view->Txt1=$larrinitialresult[FldTxt1];
		$this->view->Dpd1=$larrinitialresult[FldDdwn1];
	$count=$count+$larrinitialresult[FldTxt1];
	}
	if($larrinitialresult[FldTxt2])
	{
		$this->view->Txt2=$larrinitialresult[FldTxt2];
		$this->view->Dpd2=$larrinitialresult[FldDdwn2];
	$count=$count+$larrinitialresult[FldTxt2];
	}
	if($larrinitialresult[FldTxt3])
	{
		$this->view->Txt3=$larrinitialresult[FldTxt3];
		$this->view->Dpd3=$larrinitialresult[FldDdwn3];
	$count=$count+$larrinitialresult[FldTxt3];
	}
	if($larrinitialresult[FldTxt4])
	{
		$this->view->Txt4=$larrinitialresult[FldTxt4];
		$this->view->Dpd4=$larrinitialresult[FldDdwn4];
	$count=$count+$larrinitialresult[FldTxt4];
	}
	if($larrinitialresult[FldTxt5])
	{
		$this->view->Txt5=$larrinitialresult[FldTxt5];
		$this->view->Dpd5=$larrinitialresult[FldDdwn1];
	$count=$count+$larrinitialresult[FldTxt5];	
	}
	if($larrinitialresult[FldTxt6])
	{
		$this->view->Txt6=$larrinitialresult[FldTxt6];
		$this->view->Dpd6=$larrinitialresult[FldDdwn6];
	$count=$count+$larrinitialresult[FldTxt6];
	}
	//echo $count;die;
	$this->view->counts=$count;
	}

public function newstudentAction() { //Action for creating the new user
	$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
	$auth = Zend_Auth::getInstance();
	$auth->getIdentity()->iduser = 1;	
	//////////////////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////////////

	
	if ($this->_request->isPost ()) {
			$larrformData = $this->_request->getPost ();
			//print_r($larrformData);die();
			$id = $larrformData['ArmyNo'];
		
			$larrresults =  $this->lobjstudentmodel->fnGetRegid($id);
			/*echo"<pre/>";
			print_r($larrresults);
			die();*/
			
			
			 
      }//end if for comparing post of data
      if($larrresults['idregistereddetails']=='')
      {
      	echo "Not a valid Registered id";
      	$this->_redirect( $this->baseUrl . "/studentedit/index");
      	die();
      }
      	$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjstudentForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		$larrstate = $this->lobjstudentmodel->fnGetStateName();
		$this->lobjstudentForm->State->addMultiOptions($larrstate);
		
		$larrQualification = $this->lobjstudentmodel->fnGetEducationDetails();
		$this->lobjstudentForm->Qualification->addMultiOptions($larrQualification);
		
		
$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Race');
	 /*   print_r($larrdefmsresultset);
	    die();*/
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjstudentForm->Race->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
	
 	$this->view->lobjstudentForm->FName->setValue($larrresults['FName']);
	$this->view->lobjstudentForm->MName->setValue($larrresults['MName']);
	$this->view->lobjstudentForm->LName->setValue($larrresults['LName']);
	$this->view->lobjstudentForm->DateOfBirth->setValue($larrresults['DateOfBirth']);
	$this->view->lobjstudentForm->PermCity->setValue($larrresults['PermCity']);
	$this->view->lobjstudentForm->EmailAddress->setValue($larrresults['EmailAddress']);		 	
	$this->view->lobjstudentForm->Takafuloperator->setValue($larrresults['Takafuloperator']);
	$this->view->lobjstudentForm->ICNO->setValue($larrresults['ICNO']);
	$this->view->lobjstudentForm->PermAddressDetails->setValue($larrresults['PermAddressDetails']);	
	$this->view->lobjstudentForm->Gender->setValue($larrresults['Gender']);	
	$this->view->lobjstudentForm->CorrAddress->setValue($larrresults['CorrAddress']);	
	$this->view->lobjstudentForm->ArmyNo->setValue($larrresults['ArmyNo']);	
	$this->view->lobjstudentForm->State->setValue($larrresults['State']);
	$this->view->lobjstudentForm->PostalCode->setValue($larrresults['PostalCode']);
	$this->view->lobjstudentForm->Race->setValue($larrresults['Race']);
    $this->view->lobjstudentForm->ContactNo->setValue($larrresults['ContactNo']);
    $this->view->lobjstudentForm->MobileNo->setValue($larrresults['MobileNo']);
 	
 	$this->view->programs = $larrresults['ProgramName'];
 	$this->view->StateNames = $larrresults['StateName'];
 	$this->view->centernames = $larrresults['centername'];
 	$this->view->idapplication = $larrresults['IDApplication'];
	    
	/*	$larrdaysarresult = $this->lobjstudentmodel->fnGetDaysforcourse(1);
	
		
		$this->view->monday =empty($larrdaysarresult[0]['Days'])? '0':'1';
		$this->view->tuesday = empty($larrdaysarresult[1]['Days'])?'0':'1';
		$this->view->wednesday = empty($larrdaysarresult[2]['Days'])?'0':'1';;
		$this->view->thursday = empty($larrdaysarresult[3]['Days'])?'0':'1';;
		$this->view->friday = empty($larrdaysarresult[4]['Days'])?'0':'1';;
		$this->view->saturday = empty($larrdaysarresult[5]['Days'])?'0':'1';;
		$this->view->sunday = empty($larrdaysarresult[6]['Days'])?'0':'1';;*/
		
		
		//$this->lobjstudentForm->Program->addMultiOptions($larrbatchresult);
	//	die();
		//$this->lobjstudentForm->NewState->fnGetYearlistforcourse();
		
	$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		$this->view->lobjstudentForm->UpdDate->setValue ( $ldtsystemDate );
		//$larrQualification=$this->lobjstudentmodel->fnGetEducation();
		//$this->view->lobjstudentForm->Qualification->setValue($larrQualification);
		 
		$this->view->lobjstudentForm->UpdUser->setValue ( 1);
		$this->view->lobjstudentForm->ICNO->setValue($icno);
		}
	
}	
	public function selectvenueAction()
	{
		if ($this->_request->isPost ()) {
			$larrformData = $this->_request->getPost ();
			//print_r($larrformData);die();
			
			$idapplication = $larrformData['idapplication'];
			
			$larresults = $this->lobjstudentmodel->fnAddStudent($larrformData,$idapplication);
			$this->_redirect( $this->baseUrl . "/studentedit/index");
			
		}
		
		
	}

}