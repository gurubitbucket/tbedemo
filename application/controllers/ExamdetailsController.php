<?php
error_reporting (E_ALL & ~E_NOTICE & ~E_DEPRECATED);
class ExamdetailsController extends Zend_Controller_Action {
			public $gsessionregistration;
			private $_gobjlogger;
			public $gsessionemail;
	public function init() { //initialization function

		$this->_helper->layout()->setLayout('/reg/usty1');
		$this->gsessionregistration = Zend_Registry::get('sis'); 	
		
		if(empty($this->gsessionregistration->IdBatch)){ 
			$this->_redirect( $this->baseUrl . '/index');					
		}
			//$this->gsessionemail = Zend_Registry::get('sis'); 
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
			//$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
		//$this->lobjstudentForm = new StuApp_Form_Studentapplication (); //intialize user lobjuserForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}

	/*
	 *  search form & grid display  
	 */
	public function indexAction() {
		//Set Transaltion 			
		
		$this->gsessionregistration->takenexam=1;
		$this->gsessionregistration->mails=0;
  		$this->_helper->layout->disableLayout();  // dis
		 $lobjsearchform = new App_Form_Registration();  //intialize search lobjuserForm
		$this->view->lobjform = $lobjsearchform; 
		$lobjExamdetailsmodel = new App_Model_Examdetails();
			$idApplication = $this->gsessionregistration->IDApplication;
		  $studentdetailsarray= $lobjExamdetailsmodel->fnGetStudentdetails($idApplication);
  	 /*  print_R($studentdetailsarray);
  	   die();*/
  	   $program = $studentdetailsarray['Program'];
  	   
  	   $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
  	  // print_R($larractiveidbatch);
  	   $idIdBatch = $larractiveidbatch['IdBatch'];
		
		//print_r($this->gsessionregistration->Regid);
		//die();
	/*if(empty($idIdBatch))
  	    {
  	    	echo "<script>parent.location = '".$this->view->baseUrl()."/registration';</script>";	
  	    	die();
  	    }*/

		$regid = $this->gsessionregistration->Regid;
		$lobjExamdetailsmodel = new App_Model_Examdetails();
		$larrresult = $lobjExamdetailsmodel->fnGetBatchName($idIdBatch);
		/*echo"<pre/>";
		print_r($larrresult);
		die();*/
		$this->view->BatchName= $larrresult;
		
		$lobjExamdetailsmodel = new App_Model_Examdetails();
  		/*$sessionID = Zend_Session::getId();
		$larrresulttemp = $lobjExamdetailsmodel->fnDeleteTempDetails($sessionID);
  		$this->view->idbatch = $idIdBatch;
  		$this->view->RegId = $RegId;*/
  	    
	}
        	
  
	public function getqtnnumberforlevelAction()
	{
		 //Set Transaltion 			
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$QuestionNo = $this->_getParam('QuestionNosection');
	}
	
	/*
	 * function to fetch no of qtns from the temp table
	 */
	public function noofqtnsAction()
	{
			//$this->view->translate = $this->gstrtranslate; //Set Transaltion 			
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$RegId = $this->_getParam('RegId');		
		$sessionID = Zend_Session::getId();
		$lobjExamdetailsmodel = new App_Model_Examdetails();
		$larrresulttemp = $lobjExamdetailsmodel->fnGetNoofQtnsFromTempTable($RegId);
		echo ($larrresulttemp['count(`QuestionNo`)']);
		//die();
	}
	
	  public function getqtnnumberAction()
	  {
	  //Set Transaltion 			
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$QuestionNo = $this->_getParam('QuestionNo');
		$lobjExamdetailsmodel = new App_Model_Examdetails();
		$larrsection = $lobjExamdetailsmodel->fnGetSectionName($QuestionNo);
		$questionlevel = $larrsection['QuestionNumber'];
		echo $questionlevel;
	  }
	 
	 public function passfailAction()
	 {
	 	$passresult=1;
	 	$failresult = 2;
	 	$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$IdBatch = $this->_getParam('IdBatch');
		$RegId = $this->_getParam('RegId');	
		/*print_r($RegId);
		die();*/

		//$sessionID = Zend_Session::getId();
		
		$lobjExamdetailsmodel = new App_Model_Examdetails();
		$larrstudentid = $lobjExamdetailsmodel->fngetstudentid($RegId);	
		$studentid = $larrstudentid['IDApplication'];
		
		
		$larrpercentageset = $lobjExamdetailsmodel->fnExampercentage($IdBatch);
		/*print_R($larrpercentageset);
		die();*/
		if(empty($larrpercentageset['Pass']))
		{
			$larreachsection = $lobjExamdetailsmodel->fnGetEachSection($larrpercentageset['IdTOS']);
			/*print_r($larreachsection);
			die();*/
			for($i=0;$i<count($larreachsection);$i++)
			{
				$pass = $lobjExamdetailsmodel->fnGetSectionWise($larreachsection[$i]['IdSection']);
				$sectionpercentage = $larreachsection[$i]['sectionpercentage'];
				$noofquestions = $larreachsection[$i]['NosOfQuestion'];
				$attended = count($pass);
				$percentage = ($attended/$noofquestions)*100;
				/*print_r($percentage);
				die();*/
				if($percentage>=$sectionpercentage)
				{
					$flag= 1;
				}
				else
				{
					$flag=0;
				}
			}
			/*print_r($flag);
			die();*/
			$iduniversity=1;
			if($flag ==0)
			{
				$larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
				//print_r($larrinitconfig);
					$larrpass = $lobjExamdetailsmodel->fnupdatepass($studentid,$failresult);
				echo '1'.'****'.$larrinitconfig['StudentFails'];
				die();
				
			}
			if($flag ==1)
			{
				$larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
				//print_r($larrinitconfig);
				$larrpass = $lobjExamdetailsmodel->fnupdatepass($studentid,$passresult);
				echo '0'.'****'.$larrinitconfig['StudentPass'];
				die();
			}
			
		}
		else
	    {
	    	//$larreachsection = $lobjExamdetailsmodel->fnGetEachSection($larrpercentageset['IdTOS']);
	    	$iduniversity=1;
	   	  $larrresultpercentage = $lobjExamdetailsmodel->fnAttended($RegId);
	   	  $pass = $larrpercentageset['Pass'];
		  $noofquestions = $larrpercentageset['NosOfQues'];
		  $attended = count($larrresultpercentage);
		  if($attended == 0)
		  {
		  	 $larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
				//print_r($larrinitconfig);
				$larrpass = $lobjExamdetailsmodel->fnupdatepass($studentid,$failresult);
				echo '1'.'****'.$larrinitconfig['StudentFails'];
				die();
		  }
		  $percentage = ($attended/$noofquestions)*100;
		  if($percentage>=$pass)
		  {
		  	   $larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
		  	   $larrpass = $lobjExamdetailsmodel->fnupdatepass($studentid,$passresult);
				//print_r($larrinitconfig);
				echo '0'.'****'.$larrinitconfig['StudentPass'];
				die();
		  }
		  else 
		  {
		  	$larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
				//print_r($larrinitconfig);
				$larrpass = $lobjExamdetailsmodel->fnupdatepass($studentid,$failresult);
				echo '1'.'****'.$larrinitconfig['StudentFails'];
				die();
		  }
	
		}
	
	 }
	public function getanswertempAction()
	{
		//Transaltion 			
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$IdBatch = $this->_getParam('IdBatch');
		$QuestionNo = $this->_getParam('QuestionNo');
		$RegId = $this->_getParam('RegId');		
		
			$idcenter = $this->_getParam('idcenter');	
		$sessionID = Zend_Session::getId();
		$lobjExamdetailsmodel = new App_Model_Examdetails();
		
		$larrresult = $lobjExamdetailsmodel->fnGetFromTempDetail($IdBatch,$RegId,$sessionID);
		
		$larrresulttemp = $lobjExamdetailsmodel->fnDeleteTempDetails($sessionID);
		
		//$larrresulttemp = $lobjExamdetailsmodel->fnDeleteIdCenterDeatails($idcenter);
		echo"inserted";
		//die();
	}
	/*
	 * function for inserting into the temp table
	 */
	public function getanswerAction()
	{
				
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$IdBatch = $this->_getParam('IdBatch');
		$QuestionNo = $this->_getParam('QuestionNo');
		$RegId = $this->_getParam('RegId');
        $pieces = explode("--", $QuestionNo);
        $lobjExamdetailsmodel = new App_Model_Examdetails();
       
		$lintidquestion = $pieces[0];
		$lintidanswers = $pieces[1];
		$sessionID = Zend_Session::getId();
		//echo $sessionID;die();
		$larrsection = $lobjExamdetailsmodel->fnGetSectionName($lintidquestion);
		
		$questionlevel = $larrsection['QuestionNumber'];
		
		$larrresultofnoofquestion = $lobjExamdetailsmodel->fnGetNoofQuestioninajax($IdBatch,$questionlevel);
		$noofquestioninsection = $larrresultofnoofquestion['NosOfQuestion'];
		
		$larrresultfindquestion =$lobjExamdetailsmodel->fnGetAllDetailsFromTemp($IdBatch,$RegId,$sessionID);
	//print_r(count($larrresultfindquestion));die();
	//echo "<pre/>";
		//print_r($IdBatch);
		///echo "<br/>";
		
		
		//print_r($RegId);
		//echo "<br/>";
		////print_r($sessionID);
		//echo "<br/>";
		//print_r($lintidquestion);
		//echo "<br/>";
		//print_r($lintidanswers);
	//die();
		
		
		
		
		$flag=0;
		for($i=0;$i<count($larrresultfindquestion);$i++)
		{
			if($lintidquestion == $larrresultfindquestion[$i]['QuestionNo'])
			{
				$larrupdatedresult = $lobjExamdetailsmodel->fnUpdateTempDetails($IdBatch,$RegId,$sessionID,$lintidquestion,$lintidanswers);
				$flag=1;
			}
		}
		if($flag==0)
		   $larrinsert = $lobjExamdetailsmodel->fnInsertTempDetails($IdBatch,$RegId,$sessionID,$lintidquestion,$lintidanswers,$questionlevel);
		   
		  // print_r($larrinsert);
		   //die();
		   $larrcountfromtemp = $lobjExamdetailsmodel->fnGetQstnfromtemptable($IdBatch,$RegId,$sessionID,$questionlevel);
		  // print_r($larrcountfromtemp);
		   $noofqtnfromtemp = $larrcountfromtemp['count(QuestionNo)'];
		   $percentage = (($noofqtnfromtemp)/($noofquestioninsection))*100;
           if($percentage <25)
           {
           	 $colorcode="#E2E2E2";
           }
           elseif($percentage <50)
		   {
           	 $colorcode="#C0C0C0";
           }
	      elseif($percentage <75)
		   {
           	 $colorcode="4FD5D6";
           }
           elseif($percentage<99.99)
           {
           	$colorcode="#D4E8C1";
           }
           else 
           $colorcode="#8DB87C";
             
		
		echo $colorcode.'--'.$questionlevel;
		
	}
	/*
	 * function for ajax 
	 */
	public function getsectionAction()
	{
		$this->view->rtl = $this->gstrHTMLDir;  //Set HTML Tag Parameters 
		$this->view->lang = $this->gstrHTMLLang;		
		$this->view->translate = $this->gstrtranslate; //Set Transaltion 			
		//$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$IdBatch = $this->_getParam('IdBatch');
		$Questionlevel = $this->_getParam('Questionlevel');
		$Questionnumber = $Questionlevel;
		//echo $IdBatch;
		//echo $Questionlevel;
		$difficultquestion=0;
		$easyquestion=0;
		$Mediumquestion=0;
		$lobjExamdetailsmodel = new App_Model_Examdetails();
		$larrresult = $lobjExamdetailsmodel->fnGetNoofQuestion($IdBatch,$Questionlevel);
		//print_r($larrresult);die();
		 for($k=0;$k<count($larrresult);$k++)
            {
            	$questionArr  = $larrresult[$k]['IdDiffcultLevel'];
            	$noofquestion = $larrresult[$k]['NoofQuestions'];
            	$idsection = $larrresult[$k]['IdSection'];
				switch ($questionArr) 
				{
				    case 1:
				        $easyquestion = $noofquestion;
				        break;
				    case 2:
				        $Mediumquestion = $noofquestion;
				        break;
				    case 3:
				        $difficultquestion = $noofquestion;
				        break;
				}
            }	
            $lobjbankForm = new App_Form_Bank();//intialize bank form
		$this->view->form = $lobjbankForm; //send the lobjuserForm object to the view	
		//echo"<pre/>";
		//print_r($easyquestion);
		$larreasyquestion = $lobjExamdetailsmodel->fnGetRandomEasyQuestions($Questionnumber,$easyquestion,$Mediumquestion,$difficultquestion);
		//print_r($larreasyquestion);
		$this->view->questions = $larreasyquestion;
		for($k=0;$k<count($larreasyquestion);$k++)
		{
			$arrayqstn[$k] = $larreasyquestion[$k]['idquestions'];
		}
		for($j=0;$j<count($arrayqstn);$j++)
		{
			$value = $arrayqstn[$j];
			if($j==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		//print_r($values);
		
		$larranswers = $lobjExamdetailsmodel->fnGetAnswers($values);
		
		$this->view->answers = $larranswers;
		//print_r($larranswers);
		//print_r($larreasyquestion);
  		//print_r($easyquestion);
  		//print_r($Mediumquestion);
  		//print_r($difficultquestion);
		//die();
    	
	}
	
	public function newdefinedquestionAction() {  	

  		$idIdBatch = $this->gsessionregistration->IdBatch;
  		$RegId = $this->gsessionregistration->Regid;
  		$idApplication = $this->gsessionregistration->IDApplication;
  		//echo $idApplication;
  		
  		$lobjExamdetailsmodel = new App_Model_Examdetails();
  		$sessionID = Zend_Session::getId();
		$larrresulttemp = $lobjExamdetailsmodel->fnDeleteTempDetails($sessionID);
  		$this->view->idbatch = $idIdBatch;
  		$this->view->RegId = $RegId;
  	    if(empty($idIdBatch))
  	    {
  	    	
  	    	echo "<script>parent.location = '".$this->view->baseUrl()."/registration';</script>";	
  	    	die();
  	    }
  		
  		$timelimit = $lobjExamdetailsmodel->fnGetTime($idIdBatch);
  		$studentname = $lobjExamdetailsmodel->fnGetStudentName($idApplication);
  		//print_r($studentname);
  		//print_r($timelimit);die();
  		$this->view->studentname = $studentname['Name'];
  		$this->view->timelimit = $timelimit['TimeLimit']-1;
  		$this->view->AlertTime = $timelimit['AlertTime']-1;
  		//die();
  		
  		
  		////////////////////based on the batch fetch no of qtns//////////
  		$larrresultaa = $lobjExamdetailsmodel->fnGetBatchName($idIdBatch);
	/*	echo"<pre/>";
		print_r($larrresult[0]['NosOfQues']);die();*/
		$this->view->noofqtns = $larrresultaa[0]['NosOfQues'];
		//$this->view->BatchName= $larrresult;
  		//////////////////////////////////////////////
  		
  		//for defining the section////////////////
  		$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
  		
  		$larridqtnfrombatch = $lobjExamdetailsmodel->fnGetQtnfromidbatch($idIdBatch);
  	
  		
  		//print_r($larridqtnfrombatch);
  		for ($linta=0;$linta<count($larridqtnfrombatch);$linta++)
	  	{
	  		$larrsection[$linta]=$larrmultisections[$linta]['IdSection'];
	  		//print_r($larrsection);die();
	  		$larrqtnfrombatch[$larrsection[$linta]]=$larridqtnfrombatch[$linta]['NosOfQuestion'];
	  	}
  			//print_r($larrqtnfrombatch);
  		//die();
  		
  		$this->view->sectionqtns = $larrqtnfrombatch;
  	for ($sec=0;$sec<count($larrmultisections);$sec++)
  	{
  		$larrsection[$sec]=$larrmultisections[$sec]['IdSection'];
  	}
  	
  	//print_r($larrsection);die();
  	for($s=0;$s<count($larrsection);$s++)
		{
			$value = $larrsection[$s];
			if($s==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		//print_r(count($larrsection));
		$this->view->countsections = count($larrsection);
		$this->view->listsections = $values;
 // die();
  	
  	$this->view->sectionsarray = $larrsection;
  		$this->view->sections = $larrmultisections;
  		//die();
  		///////////////////////////////////
  		
  		//$larrQtnsToDisp=array();
  		
  		for($i=0;$i<count($larrsection);$i++){
  		
  		$larrresulteasy = $lobjExamdetailsmodel->fnGetNoofQuestion($idIdBatch,$larrsection[$i]);
  		//print_r($larrresulteasy);die();
  		
  	     for($k=0;$k<count($larrresulteasy);$k++)
            {
            	$questionArr  = $larrresulteasy[$k]['IdDiffcultLevel'];
            	$noofquestion = $larrresulteasy[$k]['NoofQuestions'];
            	$idsection = $larrresulteasy[$k]['IdSection'];
				switch ($questionArr) 
				{
				    case 1:
				        $easyquestion = $noofquestion;
				        break;
				    case 2:
				        $Mediumquestion = $noofquestion;
				        break;
				    case 3:
				        $difficultquestion = $noofquestion;
				        break;
				}
            }
            	
            $larreasyquestion[$larrsection[$i]] = $lobjExamdetailsmodel->fnGetRandomEasyQuestionss($larrsection[$i],$easyquestion,$Mediumquestion,$difficultquestion);
         // $arrayqstn[$l] = $larreasyquestion[$larrsection[$i]]['idquestions'];
          
  		}
  		$this->view->arrQuestions=$larreasyquestion;
  		//echo"<pre/>";
  		//print_r($larreasyquestion);die();
  		
  		for($i=0;$i<count($larrsection);$i++)
		{
			$qunnoforsection  = array();
			$arranssection[] =$larrsection[$i] ;
			$noofqtnssection[$larrsection[$i]] =count($larreasyquestion[$larrsection[$i]]);
			for($l=0;$l<count($larreasyquestion[$larrsection[$i]]);$l++)
			{
				
				$qunno[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
				$qunnoforsection[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
		
				//$noofquestionofsec = 
			}
					
				$arraysofqtnnowithsection[$larrsection[$i]]=$qunnoforsection;
			
			//$arrayqstn[$l] = $larreasyquestion[$l]['idquestions'];
		}
		//print_r($arraysofqtnnowithsection);
		//die();
		$this->view->questionno = $arraysofqtnnowithsection;
		//die();
			
		for($j=0;$j<count($qunno);$j++)
		{
			$value = $qunno[$j];
			if($j==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		$arrayqtns = $values;
		$this->view->arrayqtns = $values;
		
		/*print_r($values);
		die();*/
		$larranswers = $lobjExamdetailsmodel->fnGetAnswers($values);
  		$this->view->answers = $larranswers;
  		 //print_r($larranswers);die();
         
  		$lobjbankForm = new App_Form_Bank();//intialize bank form
		$this->view->form = $lobjbankForm; //send the lobjuserForm object to the view	
  		//print_r($easyquestion);die();
  		//print_r($Mediumquestion);
  		//print_r($difficultquestion);
  		//$larrresultquestions = 
  		//
  		$sessionID = Zend_Session::getId();
  		$larrresulttemp = $lobjExamdetailsmodel->fnDeleteTempDetails($sessionID);
        // save opearation
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost(); //getting the values of bank from post 
			if ($this->_request->isPost()) {
				$larrformData = $this->_request->getPost();
				if ($lobjbankForm->isValid($larrformData)) {
										
					$lobjbankmodel = new App_Model_Bank(); //bank model object				   
			        $lintresult = $lobjbankmodel->fnAddBank($lobjbankForm->getValues()); //instance for adding the lobjuserForm values to DB
			     
			        $lobjLogs = new Logs_log(); // object for log & insertion to log table
					$lobjLogs->fnLogs('BANK','ADD','New Bank Add',$this->gobjsessionsfs->primaryuserid,$this->getRequest()->getServer('REMOTE_ADDR'));
					echo "<script>parent.location = '".$this->view->baseUrl()."/bank/index';</script>";	
				}
			}
		}
	}
	

	public function examsheetAction() {  	

		//$this->gsessionregistration->mails=0;
		$this->gsessionregistration->mails=0;
		//print_r
		 
  		$idIdBatch = $this->gsessionregistration->IdBatch;
  		/*print_R($idIdBatch);
  		die();*/
  		$RegId = $this->gsessionregistration->Regid;
  		$idApplication = $this->gsessionregistration->IDApplication;
  	
  		//echo $idApplication;
  		//$this->gstrsessionSIS->__set('mailflag',0);
  		$lobjExamdetailsmodel = new App_Model_Examdetails();
  		$sessionID = Zend_Session::getId();
		$larrresulttemp = $lobjExamdetailsmodel->fnDeleteTempDetails($sessionID);
  		$this->view->idbatch = $idIdBatch;
  		$this->view->RegId = $RegId;
  	    if(empty($idIdBatch))
  	    {
  	    	
  	    	echo "<script>parent.location = '".$this->view->baseUrl()."/registration';</script>";	
  	    	die();
  	    }
  		
  	     $studentdetailsarray= $lobjExamdetailsmodel->fnGetStudentdetails($idApplication);
  	  /* print_R($studentdetailsarray);
  	   die();*/
  	
	    $date =date('d');
		if($date<10)
		{
			$date = $date[1];
		}
		
		
  		$examvenue = $studentdetailsarray['Examvenue'];
  		$this->view->examvenue = $examvenue;
  		$lselectvalidation = $lobjExamdetailsmodel->fnCheckforApproval($examvenue,$studentdetailsarray['Program'],$date);
  	/*	print_r($lselectvalidation);
  		die();*/
		if($date != $lselectvalidation['Date'])
		{
			echo '<script language="javascript">alert("Still Exam has not been started wait for further instruction")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();			
		}
		
  		
  		if(empty($lselectvalidation))
  		{
  			echo '<script language="javascript">alert("Still Exam has not been started wait for further instruction")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();
  		}
  		/*print_r($lselectvalidation);
  		die();*/
  		
  		$timelimit = $lobjExamdetailsmodel->fnGetTime($idIdBatch);
  		$studentname = $lobjExamdetailsmodel->fnGetStudentName($idApplication);
  		
  		$hour = date('H');
		
		
		$minutes = date('i');
		
		
		$hours = $hour*60;
		$currenttime = $minutes+$hours;
		
	/*	echo "<pre/>";
	
		print_r($currenttime);
		die();*/
		$examtotaltime = $lselectvalidation['Startedtime'] + $lselectvalidation['Totaltime'];
		//die();
		$totaltimeforstudent = $examtotaltime-$currenttime;
		//print_r($totaltimeforstudent);
		
		if($totaltimeforstudent<0)
		{
			echo '<script language="javascript">alert("The exam time has been completed")</script>';
			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();
		}
		
  		//print_r($totaltimeforstudent);
  		$this->view->studentname = $studentname['Name'];
  		$this->view->timelimit = $totaltimeforstudent;
  		$this->view->AlertTime = $timelimit['AlertTime']-1;
  		//die();

  		$larrresultaa = $lobjExamdetailsmodel->fnGetBatchName($idIdBatch);
/*	echo"<pre/>";
		print_r($larrresultaa[0]['NosOfQues']);die();*/
		$this->view->noofqtns = $larrresultaa[0]['NosOfQues'];
		
  		//for defining the section////////////////
  		$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
  		print_R($larrmultisections);
  		die();
  		$flagg = 1;
  		if(count($larrmultisections)>1)
  		{
  			$flagg = 0;
  		}
  		
		    for ($linta=0;$linta<count($larrmultisections);$linta++)
		    {
		    	
		    	$larrparts[$linta]=$larrmultisections[$linta]['IdPart'];
		    	//die();
		    	
		  		  	$larrquestions[$linta] = $lobjExamdetailsmodel->fnGetQuestions($idIdBatch,$larrmultisections[$linta]['IdPart']);  
		  		    for($totalqtn=0;$totalqtn<count($larrquestions[$linta]);$totalqtn++)
		  		    {
		  		    	 $larrquestionnumber[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestion'];
		  		    	
		  		    }
		  		    $larrquestionid = array();
		  		   for($totalqtn=0;$totalqtn<count($larrquestionnumber);$totalqtn++)
		  		    {	  		    	
		  		     	 $array2  = $larrquestionnumber[$totalqtn];		  		 
			  		     $larrquestionid  = array_merge($larrquestionid,$array2);
		  		    }
		  	 }
  		/*}
  		else 
  		{
  			 $larrquestions[$linta] = $lobjExamdetailsmodel->fnGetQuestions($idIdBatch,$larrmultisections[$linta]['IdPart']);  
  		}*/
     /* print_R($larrquestionid);
      die();*/
     
     	for($j=0;$j<count($larrquestionid);$j++)
		{
			$value = $larrquestionid[$j];
		
			if($j==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		$arrayqtns = $values;
		$this->view->arrayqtns = $values;
	//	echo "<pre/>";

		print_r($larrquestions);die();
		$larranswers = $lobjExamdetailsmodel->fnGetAnswers($values);
  		$this->view->answers = $larranswers;
  /*		echo "<pre/>";
  		print_r($larranswers);
		die();*/
  	$this->view->totalnumberofquestions = count($larrquestionid);
       $this->view->questionsdisplay = $larrquestions;
       
  		
  		if ($this->_request->isPost() && $this->_request->getPost('Save')) {
			$larrformData = $this->_request->getPost(); //getting the values of bank from post 
			if ($this->_request->isPost()) {
				$larrformData = $this->_request->getPost();
				if ($lobjbankForm->isValid($larrformData)) {
										
					$lobjbankmodel = new App_Model_Bank(); //bank model object				   
			        $lintresult = $lobjbankmodel->fnAddBank($lobjbankForm->getValues()); //instance for adding the lobjuserForm values to DB
			     
			        $lobjLogs = new Logs_log(); // object for log & insertion to log table
					$lobjLogs->fnLogs('BANK','ADD','New Bank Add',$this->gobjsessionsfs->primaryuserid,$this->getRequest()->getServer('REMOTE_ADDR'));
					echo "<script>parent.location = '".$this->view->baseUrl()."/bank/index';</script>";	
				}
			}
		}
	}
	
public function examdetailsAction()
	{
		$msg='';
	    $passresult=1;
	 	$failresult = 2;
	 	$submittedby=1;
		$RegId = $this->_getParam('RegId');
		$IdBatch =$this->_getParam('IdBatch');
		$submittedby =$this->_getParam('autosumbmit');
	
		
		if($submittedby =='')
		{
					$submittedby=1;
		}
		
		$this->view->regid = $RegId;
	    $lobjExamdetailsmodel = new App_Model_Examdetails();	
	    $sessionID = Zend_Session::getId();
	    

	    
		////function for moving from temp details to main table//////
		$larrresult = $lobjExamdetailsmodel->fnGetFromTempDetail($IdBatch,$RegId,$sessionID);
	
		$larrresulttemp = $lobjExamdetailsmodel->fnDeleteTempDetails($RegId);
		////////End of the funciton//////////////////////////////
		
		$iduniversity=1;
		/////////function for calculating the percentage///////////
		  $larrstudentid = $lobjExamdetailsmodel->fngetstudentid($RegId);	
		  $studentid = $larrstudentid['IDApplication'];
		  
		   $larrstudentpresent = $lobjExamdetailsmodel->fncheckstudentexists($studentid);
		  
		 
		  	    /////////function to update the end time of the student///////////
	    ////////////////////////////////////////student exam timingssss///////////////
		//$starttime =date('H:i:s');
		
  		$endtime=date('H:i:s');
  		$larrresultsstudentime = $lobjExamdetailsmodel->fnupdatestudentexamdetails($studentid,$endtime,$submittedby);
  		//////////////////////////////////////////end of student exam timings///////////////
  
	    ////////end of the function//////////////////////////////////////
	    
		  
		  //function for finding the pass percentage and the noof questions
		  $larrpercentageset = $lobjExamdetailsmodel->fnExampercentage($IdBatch);
		  $pass = $larrpercentageset['Pass'];
		  $noofquestions = $larrpercentageset['NosOfQues'];
		  
		  $totoalnoofqtnsattended = $lobjExamdetailsmodel->fntotalanswered($RegId);
		  $totalanswered = count($totoalnoofqtnsattended);
		 /* print_R($totalanswered);
		  die();*/
	   	  $larrresultpercentage = $lobjExamdetailsmodel->fnAttended($RegId);
		  $attended = count($larrresultpercentage);
		  $unanswered = $noofquestions-$totalanswered;
		  
		  	
		  //if fails
		  if($attended == 0)
		  {
		  	 $larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
				//print_r($larrinitconfig);
				$larrpass = $lobjExamdetailsmodel->fnupdatepass($studentid,$failresult);
				//$msg = $larrinitconfig['StudentFails'];
				$msg = 'Sorry, You are unsuccessful on your attempt.\n Please re-register and try again.\n Good Luck';
				$grade = 'F';
				$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Fail");	
				//die();
		  }
		  //calculate percentage 
		  $percentage = ($attended/$noofquestions)*100;
		  if($percentage>=$pass)
		  {
		  	   $larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
		  	   $larrpass = $lobjExamdetailsmodel->fnupdatepass($studentid,$passresult);
				
				    $correct = count($lobjExamdetailsmodel->fnAttended($RegId));
				    $msg = 'Congratulations! You have passed the exam';
				    
				    $passpercent= (int)(($correct/$noofquestions)*100);
				   
					  if($passpercent>=85)
					  {
					  	$grade = 'A';
					  }
					  if($passpercent >= 70 && $passpercent <= 84)
					  {
					  	$grade = 'B';
					  }
					  if($passpercent >= $pass && $passpercent <=69)
					  {
					  	$grade = 'C';
					  }
				
				$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Pass");
		  }
		  else 
		  {
		  	$larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
				//print_r($larrinitconfig);
				$larrpass = $lobjExamdetailsmodel->fnupdatepass($studentid,$failresult);
				//$msg = $larrinitconfig['StudentFails'];
				$msg = 'Sorry, You are unsuccessful on your attempt.\n Please re-register and try again.\n Good Luck';
				$grade = 'F';
				$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Fail");	
				//die();
		  }
		
		  $larrstudentpresent = $lobjExamdetailsmodel->fncheckstudentexists($studentid);
		  $larrstudentdetails = $lobjExamdetailsmodel->fnGetStudentDetailsPassfail($RegId);
		  if(count($larrstudentpresent)>1)
		  {
		  	 $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							//$serverName =	$_SERVER['SERVER_NAME'];					
										//replace tags with values
										//$Link = "<a href='".$serverName.$this->baseUrl."/tbe/takaful/login'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[NAME]",$larrstudentdetails['FName'],$lstrEmailTemplateBody);
											$lstrEmailTemplateBody = str_replace("[Course]",$larrstudentdetails['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address1]",$larrstudentdetails['PermAddressDetails'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[NRIC]",$larrstudentdetails['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[EXAMDATE]",$larrstudentdetails['Examdate'].'-'.$larrstudentdetails['Exammonth'].'-'.$larrstudentdetails['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[GRADE]",$grade,$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										 $this->view->message = $lstrEmailTemplateBody;
		  }
		  else 
		  {
		$larrresultinsertedmarks = $lobjExamdetailsmodel->fninsertstudentmarks($studentid,$noofquestions,$totalanswered,$attended,$grade);
		  echo '<script language="javascript">alert("'.$msg.'")</script>';
		  $larrstudentdetails = $lobjExamdetailsmodel->fnGetStudentDetailsPassfail($RegId);
	   
			require_once('Zend/Mail.php');
				require_once('Zend/Mail/Transport/Smtp.php');
			
			  	
			                $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							//$serverName =	$_SERVER['SERVER_NAME'];					
										//replace tags with values
										//$Link = "<a href='".$serverName.$this->baseUrl."/tbe/takaful/login'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[NAME]",$larrstudentdetails['FName'],$lstrEmailTemplateBody);
											$lstrEmailTemplateBody = str_replace("[Course]",$larrstudentdetails['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address1]",$larrstudentdetails['PermAddressDetails'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[NRIC]",$larrstudentdetails['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[EXAMDATE]",$larrstudentdetails['Examdate'].'-'.$larrstudentdetails['Exammonth'].'-'.$larrstudentdetails['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[GRADE]",$grade,$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										$strsmtpServer = 'mail.itwinetech.com';
							    		$strusername = 'prasad.itwinetech.com';
							    		$strpassword = '';
							        	$strsslValue = '0';
									    $strfromEmail = 'sachin_gururaj@itwinetech.com';
							        	$strsmtpPort = '25';
							        	$strEmailAddress = 'askiran123@gmail.com';
							        	$lstrUserName='Kiran';
				                        if(!isset($strsmtpServer)) 
				                        	{
	  			 		               			echo '<script language="javascript">alert("Unable to send mail \n Check  STMP Settings")</script>';
			       							}
			       						else 
			       							{
			       	 	$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($strsmtpServer);
		  				try{
							$lobjProtocol->connect();
		   					$lobjProtocol->helo($strusername);
							$lobjTransport->setConnection($lobjProtocol);
			 
							//Intialize Zend Mailing Object
							/*$lobjMail = new Zend_Mail();
							$lobjMail->setFrom($strfromEmail,$lstrEmailTemplateFromDesc);
							$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
							$lobjMail->addHeader('MIME-Version', '1.0');
							$lobjMail->setSubject($lstrEmailTemplateSubject);
			       	 		$lobjMail->addTo($strEmailAddress,$lstrUserName);
			       	 		$lobjMail->setBodyHtml($lstrEmailTemplateBody);*/
			       	 		
			       	 		
			       	 		$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										//$mail = new Zend_Mail();
										
										$mail = new Zend_Mail('UTF-8');

										// Reset to Base64 Encoding.
										$mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
										
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'iTWINE';
										$receiver_email = $larrstudentdetails['EmailAddress'];
										$receiver = $larrstudentdetails['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
			       	 		
			       	 		
			       	 		try {
								//$lobjMail->send($lobjTransport);
								$resultemails = $mail->send($transport);
							
							} catch (Exception $e) {
								 $lobjExamdetailsmodel->fninsertstudentmails($studentid,$larrstudentdetails);
								echo '<script language="javascript">alert("Unable to send mail \n check Internet Connection ")</script>';	
							}
			    	  }catch(Exception $e){
			    	  	echo '<script language="javascript">alert("Unable to send mail \n check Internet Connection ")</script>';
			    	  }
					}
										/*
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrstudentdetails['EmailAddress'];
										$receiver = $larrstudentdetails['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
									        //$resultemails = $mail->send($transport);*/
							/*		 try {
									//$resultemails = $mail->send($transport);
										
									} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
								}*/
										//$this->gsessionregistration->mails=1;
		
			
		 $this->view->message = $lstrEmailTemplateBody;
		 
		 /////////////////////function for inserting into the partwise marks////////////////////////////
		 $larrresultinsertedmarks = $lobjExamdetailsmodel->fninsertstudentpartwise($studentid,$RegId);
		             $idbatchresult=$IdBatch;
					$larresultidparts= $lobjExamdetailsmodel->fngettheidparts($idbatchresult);
					
					$idpartsresultsarray=$larresultidparts['partsids'];
					$resarr= explode(',',$idpartsresultsarray);

					for($i=0;$i<3;$i++)
					{
						switch($resarr[$i])
						{
							Case 'A':$part= 'A';
								  	
								  	 $subdetails = $lobjExamdetailsmodel->partwiseattended($RegId,$part);
								  	 $partamarks = $subdetails['count(idquestions)'];
								  	  	 $resultupdatequery = $lobjExamdetailsmodel->updatepartwisemarksfora($RegId,$partamarks);
								    Break;
								    
						    Case 'B':$part= 'B';
								 
								  	 $subdetails = $lobjExamdetailsmodel->partwiseattended($RegId,$part);
								  	 $partbmarks = $subdetails['count(idquestions)'];
								  	 $resultupdatequery = $lobjExamdetailsmodel->updatepartwisemarksforb($RegId,$partbmarks);
								    Break;
								    
						    Case 'C':$part= 'C';
								  	
								  	 $subdetails = $lobjExamdetailsmodel->partwiseattended($RegId,$part);
								  	 $partcmarks = $subdetails['count(idquestions)'];
								  	  	 $resultupdatequery = $lobjExamdetailsmodel->updatepartwisemarksforc($RegId,$partcmarks);
								    Break;							    
						}
					}
		    }
		 /////////////////////end of the function////////////////////////////////////////////////////////

	}
	public function oldexamdetailsAction()
	{
		
		  $this->gsessionregistration->takenexam=2;
			$RegId = $this->_getParam('RegId');
			$this->view->regid = $RegId;
	     	$pass = $this->_getParam('pass');
	     	$lobjExamdetailsmodel = new App_Model_Examdetails();	
	     	$iduniversity=1;
			$larrstudentdetails = $lobjExamdetailsmodel->fnGetStudentDetailsPassfail($RegId);
			///////////////////************/////////////////////////
			$larrresult = $lobjExamdetailsmodel->fngetidbatchdetails($RegId);
			/*print_R($larrresult);
			die();*/
			$IdBatch = $larrresult[0]['IdBatch'];
			
			$idApplication = $this->gsessionregistration->IDApplication;
			$studentdetailsarray= $lobjExamdetailsmodel->fnGetStudentdetails($idApplication);
  	  		 $program = $studentdetailsarray['Program'];
  	   
  	  	 $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
  	  // print_R($larractiveidbatch);
  	  	 $IdBatch = $larractiveidbatch['IdBatch'];
  	  	 $this->view->idbatch = $IdBatch;
			
			
			$larrpercentageset = $lobjExamdetailsmodel->fnExampercentage($IdBatch);
			$noquestions = $larrpercentageset['NosOfQues'];
			/*print_r($noquestions);
			die();*/
			
				$larrstudetails= $this->lobjstudentmodel->fngetidpassdetails($RegId);
				    $IDAPPLICATION=(int) $larrstudetails['IDApplication'];
				    $passno=$larrstudetails['pass'];
				    $correct = count($lobjExamdetailsmodel->fnAttended($RegId));
				    $larrstudentresult=$this->lobjstudentmodel->fngetresultdetails($IDAPPLICATION);
				    
				    $passpercent= (int)(($correct/$noquestions)*100);
				    
					//$grade =array();
					/*$CheckedValuesList[] =$larrstudentresult['Studentname'];
					$CheckedValuesList[] =$larrstudentresult['PersonalID'];
					$CheckedValuesList[] =$larrstudentresult['CourseName'];
					$CheckedValuesList[] =$larrstudentresult['ExamDate'];*/
					if($passno==1)
					{
					  //$CheckedValuesList[] ='Pass';	
					  if($passpercent>=85)
					  {
					  	$grade = 'A';
					  }
					  if($passpercent >= 70 && $passpercent <= 84)
					  {
					  	$grade = 'B';
					  }
					  if($passpercent >= 55 && $passpercent <=69)
					  {
					  	$grade = 'C';
					  }
					}
					else{
					  	$grade = 'F';
					}
			$this->view->grade = $grade;
			//////////////////////*****///////////////////////////////////
			$larrtotalattended = $lobjExamdetailsmodel->totalattendedquestions($RegId);
			$answered = count($larrtotalattended);	
			$this->view->attended = $answered;		
			$idApplication = $this->gsessionregistration->IDApplication;
			 $studentdetailsarray= $lobjExamdetailsmodel->fnGetStudentdetails($idApplication);
  	 /*  print_R($studentdetailsarray);
  	   die();*/
  	   $program = $studentdetailsarray['Program'];
  	   
  	   $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
  
			$noofquestion = $lobjExamdetailsmodel->fnnoofqtns($this->gsessionregistration->IdBatch);
			$this->view->noofquestion = $larractiveidbatch['NosOfQues'];
			$larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
			
			if($pass == 0)
			{
				$this->view->result = $larrinitconfig['StudentPass'];
				$this->view->studentpass = 'Pass';
				$this->view->kashipass = '1';
			}
			else 
			{
				$this->view->result = $larrinitconfig['StudentFails'];
				$this->view->studentpass = 'Fail';
				$this->view->kashipass = '2';
			}
			if($pass == 0)
			{
				//echo "asdf";
				
			$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Pass");
			}
			//print_r($larrEmailTemplateDesc
			else if($pass == 1)
			{
				$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Fail");	
			}
			
			//print_r($larrEmailTemplateDesc);
			//die();
			require_once('Zend/Mail.php');
				require_once('Zend/Mail/Transport/Smtp.php');
			  if($this->gsessionregistration->mails == 0)
			  {
			  	
			                $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							//$serverName =	$_SERVER['SERVER_NAME'];					
										//replace tags with values
										//$Link = "<a href='".$serverName.$this->baseUrl."/tbe/takaful/login'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[NAME]",$larrstudentdetails['FName'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Date]",$address,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address1]",$larrstudentdetails['PermAddressDetails'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[NRIC]",$larrstudentdetails['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[EXAMDATE]",$larrstudentdetails['Examdate'].'-'.$larrstudentdetails['Exammonth'].'-'.$larrstudentdetails['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[GRADE]",$grade,$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
										
										
									/*	$to 	 = $larrstudentdetails['EmailAddress'];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrstudentdetails['EmailAddress'];
										$receiver = $larrstudentdetails['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										//$result = $mail->send($transport);
										//$this->gsessionregistration->mails=1;
									 try {
									$result = $mail->send($transport);
										
									} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
				                	// echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	// die();
								}
										$this->gsessionregistration->mails=1;
			  }
			
		 $this->view->message = $lstrEmailTemplateBody;
	}
	
public function inserttempanswerAction()
{
	   $this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		$IdBatch = $this->_getParam('IdBatch');
		$QuestionNo = $this->_getParam('QuestionNo');
		$RegId = $this->_getParam('RegId');
        $pieces = explode("--", $QuestionNo);
        
        $lintidquestion = $pieces[0];
		$lintidanswers = $pieces[1];
		$sessionID = Zend_Session::getId();
		//echo $sessionID;die();
		$lobjExamdetailsmodel = new App_Model_Examdetails();
		$larrsection = $lobjExamdetailsmodel->fnGetSectionName($lintidquestion);
		$questionlevel = $larrsection['QuestionGroup'];
		
		/////function to get number of question in particular part//////////////////////////
		$larrresultofnoofquestion = $lobjExamdetailsmodel->fnGetNoofQuestioninajaxupdated($IdBatch,$questionlevel);
		$noofquestioninsection = $larrresultofnoofquestion['NoOfQuestions'];
		/////////////end of function/////////////////////////////
		
		$larrresultfindquestion =$lobjExamdetailsmodel->fnGetAllDetailsFromTemp($IdBatch,$RegId,$sessionID);

		$flag=0;
		for($i=0;$i<count($larrresultfindquestion);$i++)
		{
			if($lintidquestion == $larrresultfindquestion[$i]['QuestionNo'])
			{
				$larrupdatedresult = $lobjExamdetailsmodel->fnUpdateTempDetails($IdBatch,$RegId,$sessionID,$lintidquestion,$lintidanswers);
				$flag=1;
			}
		}
		if($flag==0)
		   $larrinsert = $lobjExamdetailsmodel->fnInsertTempDetails($IdBatch,$RegId,$sessionID,$lintidquestion,$lintidanswers,$questionlevel);
		   
		  // print_r($larrinsert);
		   //die();
		   $larrcountfromtemp = $lobjExamdetailsmodel->fnGetQstnfromtemptable($IdBatch,$RegId,$sessionID,$questionlevel);
		    //print_r($larrcountfromtemp);die();
		   $noofqtnfromtemp = $larrcountfromtemp['count(QuestionNo)'];
		   $percentage = (($noofqtnfromtemp)/($noofquestioninsection))*100;
		   echo '1';
        /*   if($percentage <25)
           {
           	 $colorcode="#E2E2E2";
           }
           elseif($percentage <50)
		   {
           	 $colorcode="#C0C0C0";
           }
	      elseif($percentage <75)
		   {
           	 $colorcode="4FD5D6";
           }
           elseif($percentage<99.99)
           {
           	$colorcode="#D4E8C1";
           }
           else 
           $colorcode="#8DB87C";
             
		ech*
		echo $colorcode.'--'.$lintidquestion;*/
}

///////////////////////////

public function printreportAction() 
{			
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		
	    $RegID = $this->_getParam('regId');
	    $larrstudetails= $this->lobjstudentmodel->fngetidpassdetails($RegID);
	    $pass=$larrstudetails['pass'];
	    $IDAPPLICATION=(int) $larrstudetails['IDApplication'];
	   
		//object to initialize ini file
		$lobjAppconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini','development');
									
		    try 
		    {	
	            //java class
	            $lobjdbdriverclass = new Java("java.lang.Class");
	            
	            //set db driver
	            $lobjdbdriverclass->forName("com.mysql.jdbc.Driver");
	
	            //driver manager object
	            $lobjdrivermanager = new Java("java.sql.DriverManager");
	            
	            //get the db connection
				$lstrConnection  =  "jdbc:mysql://".
										$lobjAppconfig->resources->db->params->host."/".
										$lobjAppconfig->resources->db->params->dbname."?user=".
										$lobjAppconfig->resources->db->params->username."&password=".
										$lobjAppconfig->resources->db->params->password;
														
				$lobjconnection = $lobjdrivermanager->getConnection($lstrConnection);
	            
	            //Jasper Compile manager object
	            $lobjcompileManager = new Java(
	            					"net.sf.jasperreports.engine.JasperCompileManager");
	            
	            echo "CompileManager object created</br>";
	            $lstrreportdir = realpath(".") . "/report/";
	            $lstrimagepath = realpath(".") . "/images/";
	             
	            if($pass==1)
	            {
	             //compiled report path
	              $lobjreport = $lobjcompileManager->compileReport(realpath($lstrreportdir."StudentResultPass.jrxml"));
	            }
	            else 
	            {
	            	$lobjreport = $lobjcompileManager->compileReport(realpath($lstrreportdir."StudentResultFail.jrxml"));
	            }
	            
	            //Jasper Fill Manager object
	            $lobjfillManager = new Java(
	            					"net.sf.jasperreports.engine.JasperFillManager");
	            $int1 = new Java("java.lang.Integer");
	            //Hashmap object
	            //print_r($lstrreportdir);die();
	            $lobjparams = new Java("java.util.HashMap");
	          	$lobjparams->put("IDAPPLICATION",$IDAPPLICATION);
	          	$lobjparams->put ("IMAGEPATH", $lstrimagepath . "reportheader.jpg" );
	           
	           echo "Fill Manager</br>";
	            					
	            //Jasper Print Object
	            $lobjjasperPrint = $lobjfillManager->fillReport(
	            					$lobjreport, $lobjparams, $lobjconnection);
	            					
	            echo "Jasper Printed</br>";
	            
	            //Jasper Export Manager object
	            $lobjexportManager = new Java(
	            					"net.sf.jasperreports.engine.JasperExportManager");
	            
	            //output file path
	            $lstrhtmloutputPath = realpath(".") . "/" . "output.html";
	            echo "Before Export</br>";
	            $session = Zend_Session::getId();
	            $lstrpdfoutputPath = realpath(".") . "/" . "$session.pdf";
	            $objStream = new Java("java.io.ByteArrayOutputStream");
	            $lobjexportManager->exportReportToPdfFile($lobjjasperPrint,$lstrpdfoutputPath);
	            
	            //Export report to HTML	            
	            echo 'HTML Exported</br>';
	
				header("Content-type: application/pdf;charset=utf-8;encoding=utf-8");
				header('Content-Disposition: attachment; filename="Student_Result.pdf"');
				
	            readfile($lstrpdfoutputPath);
	            unlink($lstrpdfoutputPath);
				echo "finished";	
		 		 		            
		    } 
		    catch (JavaException $lobjexception) 
		    {
		    	echo 'Exception caught: ', $lobjexception->getMessage() . "\n";
		    }		    		   
			
	}
    
 public function pdfexportAction()
	{
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$RegID = $this->_getParam('regId');
		$lobjExamdetailsmodel = new App_Model_Examdetails(); // get the model object
		
		$larrresult = $lobjExamdetailsmodel->fngetidbatchdetails($RegID);
		$IdBatch = $larrresult[0]['IdBatch'];// get idbatch number
	
		$larrpercentageset = $lobjExamdetailsmodel->fnExampercentage($IdBatch); 
		$noquestions = $larrpercentageset['NosOfQues'];//get the count of questions in the program
		
		$larrtotalattended = count($lobjExamdetailsmodel->totalattendedquestions($RegID));
		$correct = count($lobjExamdetailsmodel->fnAttended($RegID)); // get the count of correctly answered questions 
	   
		$larrstudetails= $this->lobjstudentmodel->fngetidpassdetails($RegID); //get the result and idapplication
	    $IDAPPLICATION=(int) $larrstudetails['IDApplication'];
	    $pass=$larrstudetails['pass'];
	    
	    $passpercent= (int)(($correct/$noquestions)*100);//calculate the percentage
	    $larrstudentresult=$this->lobjstudentmodel->fngetresultdetails($IDAPPLICATION);// get the 
	    	
	   
	    
		$CheckedValuesList =array();
		$CheckedValuesList[] =$larrstudentresult['Studentname'];
		$CheckedValuesList[] =$larrstudentresult['PersonalID'];
		$CheckedValuesList[] =$larrstudentresult['CourseName'];
		$CheckedValuesList[] =$larrstudentresult['ExamDate'];
		if($pass==1)
		{
		  $CheckedValuesList[] ='Pass';	
		  if($passpercent>=85)
		  {
		  	$CheckedValuesList[] = 'A';
		  }
		  if($passpercent >= 70 && $passpercent <= 84)
		  {
		  	$CheckedValuesList[] = 'B';
		  }
		  if($passpercent >= 55 && $passpercent <=69)
		  {
		  	$CheckedValuesList[] = 'C';
		  }
		}
		else{
			$CheckedValuesList[] ='Fail';
		    if($passpercent < 55 )
		   {
		  	$CheckedValuesList[] = 'F';
		   }
		}
		
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
	
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
			
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                              '.'{PAGENO}{nbpg}');
		
				// LOAD a stylesheet
		//$stylesheet = file_get_contents('../public/css/default.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Student" ).' '.$this->view->translate( "Result" ).' '.$this->view->translate( "Sheet" );
		$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		
		
		$tabledata="<br><br><br>";
		$tabledata= "<br><br>Dear".' '.$CheckedValuesList[0].','; 
  if($pass==1)
		{
        $tabledata.="<br><br><b><u>TAKAFUL BASIC EXAMINATION RESULT</u></b><br><br>
                     Congratulations! You have completed the Takaful Basic Examination.
                     Below is the details of your examination result:<br><br>";
		}
		else 
		{
		$tabledata.="<br><br><b><u>TAKAFUL BASIC EXAMINATION RESULT</u></b><br><br>
                     We regret to inform you that your attempt in the Takaful Basic Examination has been unsuccessful. 
                     Below is the detail of your examination result:<br><br>";
		}
		$tabledata.= "<table border='1' align=left width='100%'>
								<tr>
					 				<td><b>Student Name </b></td>
									<td>$CheckedValuesList[0]</td>
								</tr>
								<tr>
					 				<td><b>NRIC</b></td>
									<td>$CheckedValuesList[1]</td>
								</tr>
								
								<tr>
					 				<td><b>ExamDate</b></td>
									<td>$CheckedValuesList[3]</td>
								</tr>
								<tr>
					 				<td><b>Result</b></td>
									<td>$CheckedValuesList[4]</td>
								</tr>
								<tr>
					 				<td><b>Grade</b></td>
									<td>$CheckedValuesList[5]</td>
								</tr>
        					</table><br>";
        					
		$tabledata.= "For further enquiries, kindly contact our TBE helpdesk at  03-2031 1010 ext. 582 or email
                      tbe@ibfim.com<br><br>
                      Again, we wish you all the best in your future undertakings.<br><br>
                      Thank you,<br><br>
                      Yours faithfully,<br><br>
                      for IBFIM.";
		$mpdf->WriteHTML($tabledata);   
		$mpdf->Output('Result_Report.pdf','D');
		//echo "fgd";die();
		}
		
	public function systemshutdownAction()
	{
		//$RegId = $this->gsessionregistration->Regid;
	  if($this->gsessionregistration->takenexam==2)
	    {
	    	echo '<script language="javascript">alert("You have already taken the exam")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/index';</script>";
  			//die();
	    }
		$RegId = $this->gsessionregistration->Regid;
  		$idApplication = $this->gsessionregistration->IDApplication;
  	
  		//echo $idApplication;
  		//$this->gstrsessionSIS->__set('mailflag',0);
  		$lobjExamdetailsmodel = new App_Model_Examdetails();
  		$sessionID = Zend_Session::getId();
		//$larrresulttemp = $lobjExamdetailsmodel->fnDeleteTempDetails($sessionID);
  		 $studentdetailsarray= $lobjExamdetailsmodel->fnGetStudentdetails($idApplication);
  	 /*  print_R($studentdetailsarray);
  	   die();*/
  	   $program = $studentdetailsarray['Program'];
  	   
  	   $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
  	  // print_R($larractiveidbatch);
  	   $idIdBatch = $larractiveidbatch['IdBatch'];
  	   $this->view->idbatch = $idIdBatch;
  		$this->view->RegId = $RegId;
  	    if(empty($idIdBatch))
  	    {
  	    	
  	    	echo "<script>parent.location = '".$this->view->baseUrl()."/registration';</script>";	
  	    	die();
  	    }
  		
  	    
  	   //die();
  		
  	   $curentdte = date('Y-m-d');
	    $date =date('d');
		if($date<10)
		{
			$date = $date[1];
		}
		
		
		//$examsession=2;
  		 $examsession = $studentdetailsarray['Examsession'];
  		$examvenue = $studentdetailsarray['Examvenue'];
  		$this->view->examvenue = $examvenue;
  		//$lselectvalidation = $lobjExamdetailsmodel->fnCheckforApproval($examvenue,$studentdetailsarray['Program'],$date);
  		$lselectvalidation = $lobjExamdetailsmodel->fnCheckforApprovalupdated($examvenue,$studentdetailsarray['Program']);
  		/*print_r($lselectvalidation);
  		die();*/
  		
  		/*
  		$this->view->totaltimeforexam = $lselectvalidation['Totaltime'];
		if($curentdte != $studentdetailsarray['DateTime'])
		{
			echo '<script language="javascript">alert("Still Exam has not been started wait for further instruction")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();			
		}
		
  		
  		if(empty($lselectvalidation))
  		{
  			echo '<script language="javascript">alert("Still Exam has not been started wait for further instruction")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();
  		}*/
  		/*print_r($lselectvalidation);
  		die();*/
  		
  		$timelimit = $lobjExamdetailsmodel->fnGetTime($idIdBatch);
  		$studentname = $lobjExamdetailsmodel->fnGetStudentName($idApplication);
  		
  		$hour = date('H');
		
		
		$minutes = date('i');
		
		
		$hours = $hour*60;
		$currenttime = $minutes+$hours;
		
	/*	echo "<pre/>";
	
		print_r($currenttime);
		die();*/
		$examtotaltime = $lselectvalidation['Startedtime'] + $lselectvalidation['Totaltime'];
		//die();
		$totaltimeforstudent = $examtotaltime-$currenttime;
		//print_r($totaltimeforstudent);
		
	/*	if($totaltimeforstudent<0)
		{
			echo '<script language="javascript">alert("The exam time has been completed")</script>';
			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();
		}
		*/
	    $extratime=0;
  		
  		$larrstudentextratime = $lobjExamdetailsmodel->fngetextratime($examvenue,$studentdetailsarray['DateTime'],$examsession);
  /*		print_R($larrstudentextratime);
  		die();*/
  		if(count($larrstudentextratime>1))
  		{
  			$extratime=$larrstudentextratime['Gracetime'];
  		}
  		//print_r($totaltimeforstudent);
  		$this->view->studentname = $studentname['Name'];
  		$this->view->timelimit = $totaltimeforstudent+$extratime;
  		$this->view->AlertTime = $timelimit['AlertTime']-1;
  		//die();
  		$larrresultaa = $lobjExamdetailsmodel->fnGetBatchName($idIdBatch);
	/*echo"<pre/>";
		print_r($larrresultaa[0]['NosOfQues']);die();*/
		$this->view->noofqtns = $larrresultaa[0]['NosOfQues'];
		$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
		$larrvalues = $lobjExamdetailsmodel->fngetquestionsobtained($idApplication);
		$values = $larrvalues['idquestions'];
		$this->view->arrayqtns = $values;
		$pieces = explode(",", $values);
		
		for ($linta=0;$linta<count($larrmultisections);$linta++)
		    {
		    	
		    	$larrparts[$linta]=$larrmultisections[$linta]['IdPart'];
		    	//die();
		    	
		  		  	$larrquestions[$linta] = $lobjExamdetailsmodel->fngetquestionpool($values,$larrmultisections[$linta]['IdPart']); 
		  		  	for($i=0;$i<count($pieces);$i++)
							{
								$resay = $lobjExamdetailsmodel->fnsinglequestions($pieces[$i],$larrmultisections[$linta]['IdPart']);
								if(count($resay['idquestions'])>0)$larrquestionssssssss[$linta][] = $resay;
							
							}  
		  		    for($totalqtn=0;$totalqtn<count($larrquestions[$linta]);$totalqtn++)
		  		    {
		  		    	 $larrquestionnumber[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestions'];
		  		    }
		  		    $larrquestionid = array();
		  		   for($totalqtn=0;$totalqtn<count($larrquestionnumber);$totalqtn++)
		  		    {	  		    	
		  		     	 $array2  = $larrquestionnumber[$totalqtn];		  		 
			  		     $larrquestionid  = array_merge($larrquestionid,$array2);
		  		    }
		  	 }
       
		 for($i=0;$i<count($larrquestionid);$i++)
		 {
		 	if($i==0)
			{
				$larrquestionids=$larrquestionid[$i];
			}
			else
			$larrquestionids.=','.$larrquestionid[$i];
		 }
		 /////////////////////////////inserting obrtained questions for the student//////////
		$idstudent = $this->gsessionregistration->IDApplication;
		
		//////////////////////////////////end of funciton/////////////////////////////
        $this->view->totalnumberofquestions = count($larrquestionid);
        $this->view->questionsdisplay = $larrquestionssssssss;
       
        $larranswers = $lobjExamdetailsmodel->fnGetAnswers($values);
  		$this->view->answers = $larranswers;
  		
  		/////////////////////////////////////////function to fetch from the temp answers//////////////////////
  		  $RegId = $this->gsessionregistration->Regid;
 		  $lobjExamdetailsmodel = new App_Model_Examdetails();
 		  $larrtempanswered = $lobjExamdetailsmodel->fnGettempRegisterid($RegId);
 		
 		 
 		  for($ssss = 0;$ssss<count($larrtempanswered);$ssss++)	 
 		  { 
 		  $larrtempanarrp[$ssss]  = $larrtempanswered[$ssss]['Answer'];
 		  $larrtempquestionarray[$ssss]  = $larrtempanswered[$ssss]['QuestionNo'];
 		  }
 		 
 		 
 		  $this->view->attended =  $larrtempanarrp;
 		  $this->view->qtnqttended =  $larrtempquestionarray;
  		/////////////////////////////////////////////////////////////////////////////////////////////////////
       
       
		
		
	}
	public function newbankAction()
	{ 
		
		  $lobjRegistrationModel= new App_Model_Examdetails();
		  $RegId = $this->gsessionregistration->Regid;
		 
		  $larrformData = $this->_request->getPost ();		 
		  $StudentIpAddress =  $larrformData['ipaddresss'];
		  unset($larrformData);

		
	         $larcheckresults = $lobjRegistrationModel->fncheckavailability($RegId);
 		 	if(count($larcheckresults)>1)
 		 	{
 		 		echo '<script language="javascript">alert("You have already taken the exam")</script>';
 		 		 	echo "<script>parent.location = '".$this->view->baseUrl()."/online/index';</script>";
              		die();
 		 	}
 		 	////////////////
 		 	
 		///////////////////////////system shut down///////////////////////////////////////////////////
 		  $RegId = $this->gsessionregistration->Regid;
 		  	$lobjExamdetailsmodel = new App_Model_Examdetails();
 		  $larrtempanswered = $lobjExamdetailsmodel->fnGettempRegisterid($RegId);
 		  $tempcount = count($larrtempanswered);
 		  
 		  if($tempcount>0)
 		  {
 		  	echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails/systemshutdown';</script>";
 		  	die();
 		  }
	   
 		  
 		 	
 		 /////////////////////////////////////////////////////////////
 		 	
 		 
 		 	
	    if($this->gsessionregistration->takenexam==2)
	    {
	    	echo '<script language="javascript">alert("You have already taken the exam")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/index';</script>";
  			//die();
	    }
	    else 
	    {
			  
		$this->gsessionregistration->mails=0;
  		$idIdBatch = $this->gsessionregistration->IdBatch;
  		$RegId = $this->gsessionregistration->Regid;
  		$idApplication = $this->gsessionregistration->IDApplication;
  	
  		//echo $idApplication;
  		//$this->gstrsessionSIS->__set('mailflag',0);
  		$lobjExamdetailsmodel = new App_Model_Examdetails();
  		$sessionID = Zend_Session::getId();
		$larrresulttemp = $lobjExamdetailsmodel->fnDeleteTempDetails($sessionID);
  		
  		$this->view->RegId = $RegId;
  	    if(empty($idIdBatch))
  	    {
  	    	
  	    	echo "<script>parent.location = '".$this->view->baseUrl()."/registration';</script>";	
  	    	die();
  	    }
  		
  	     $studentdetailsarray= $lobjExamdetailsmodel->fnGetStudentdetails($idApplication);
  	     $examsession = $studentdetailsarray['Examsession'];
  	    $program = $studentdetailsarray['Program'];
  	   
  	   $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
  	  // print_R($larractiveidbatch);
  	   $idIdBatch = $larractiveidbatch['IdBatch'];
  	   $this->view->idbatch = $idIdBatch;
  	   //die();
  
  	   $curentdte = date('Y-m-d');
	    $date =date('d');
		if($date<10)
		{
			$date = $date[1];
		}
		
		
  		$examvenue = $studentdetailsarray['Examvenue'];
  		$this->view->examvenue = $examvenue;
  		$lselectvalidation = $lobjExamdetailsmodel->fnCheckforApproval($examvenue,$studentdetailsarray['Program'],$date);
  		$lselectvalidation = $lobjExamdetailsmodel->fnCheckforApprovalupdated($examvenue,$studentdetailsarray['Program']);
  		/*print_r($lselectvalidation);
  		die();*/
  		if(count($lselectvalidation)<1)
  		{
  			echo '<script language="javascript">alert("Still Exam has not been started")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();
  		}
  			
  		$this->view->totaltimeforexam = $lselectvalidation['Totaltime'];
		if($curentdte != $studentdetailsarray['DateTime'])
		{
			echo '<script language="javascript">alert("Still Exam has not been started wait for further instruction")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();			
		}
		
  		
  		if(empty($lselectvalidation))
  		{
  			echo '<script language="javascript">alert("Still Exam has not been started wait for further instruction")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();
  		}
  		/*print_r($lselectvalidation);
  		die();*/
  		
  		$timelimit = $lobjExamdetailsmodel->fnGetTime($idIdBatch);
  		$studentname = $lobjExamdetailsmodel->fnGetStudentName($idApplication);
  		
  		$hour = date('H');
		
		
		$minutes = date('i');
		
		
		$hours = $hour*60;
		$currenttime = $minutes+$hours;
		
	/*	echo "<pre/>";
	
		print_r($currenttime);
		die();*/
		$examtotaltime = $lselectvalidation['Startedtime'] + $lselectvalidation['Totaltime'];
		//die();
		$totaltimeforstudent = $examtotaltime-$currenttime;
		
		//print_r($totaltimeforstudent);
		
	/*	if($totaltimeforstudent<0)
		{
			echo '<script language="javascript">alert("The exam time has been completed")</script>';
			echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails';</script>";
  			die();
		}*/
		
  		//print_r($totaltimeforstudent);
  		////////////////////////////////////////student exam timingssss///////////////
  		
		$starttime =date('H:i:s');
  		$endtime=0;
  		$submittedby=0;
  		
  		//////////////////////////////////////////end of student exam timings///////////////
  		$extratime=0;
  	
  		$larrstudentextratime = $lobjExamdetailsmodel->fngetextratime($examvenue,$studentdetailsarray['DateTime'],$examsession);
  		if(count($larrstudentextratime>1))
  		{
  			$extratime=$larrstudentextratime['Gracetime'];
  		}
  		
  		$this->view->studentname = $studentname['Name'];
  		$this->view->timelimit = $totaltimeforstudent+$extratime;
  		$this->view->AlertTime = $timelimit['AlertTime']-1;
  		//die();
  		$larrresultaa = $lobjExamdetailsmodel->fnGetBatchName($idIdBatch);
	/*echo"<pre/>";
		print_r($larrresultaa[0]['NosOfQues']);die();*/
		$this->view->noofqtns = $larrresultaa[0]['NosOfQues'];

  		
  		$larrmultisections = $lobjExamdetailsmodel->fnGetSectionsss($idIdBatch);
  		  	
  		$larridqtnfrombatch = $lobjExamdetailsmodel->fnGetQtnfromidbatch($idIdBatch);
  	$larrquestionsetforstudents = $lobjExamdetailsmodel->fngetquestionsetforstudents($idApplication);
		$alreadypresent = count($larrquestionsetforstudents);
		if($alreadypresent==1)
		{
			
				
			$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
		$larrvalues = $lobjExamdetailsmodel->fngetquestionsobtained($idApplication);
		$values = $larrvalues['idquestions'];
		$this->view->arrayqtns = $values;
		$pieces = explode(",", $values);
		
		
		for ($linta=0;$linta<count($larrmultisections);$linta++)
		    {
		    		
		    	$larrparts[$linta]=$larrmultisections[$linta]['IdPart'];
		    	//die();
		    	
		  		  	$larrquestions[$linta] = $lobjExamdetailsmodel->fngetquestionpool($values,$larrmultisections[$linta]['IdPart']); 
		  		  	for($i=0;$i<count($pieces);$i++)
							{
								$resay = $lobjExamdetailsmodel->fnsinglequestions($pieces[$i],$larrmultisections[$linta]['IdPart']);
								if(count($resay['idquestions'])>0)
								$larrquestionssssssss[$linta][] = $resay;
							
							}  
		  		    for($totalqtn=0;$totalqtn<count($larrquestions[$linta]);$totalqtn++)
		  		    {
		  		    	 $larrquestionnumber[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestions'];
		  		    }
		  		    $larrquestionid = array();
		  		   for($totalqtn=0;$totalqtn<count($larrquestionnumber);$totalqtn++)
		  		    {	  		    	
		  		     	 $array2  = $larrquestionnumber[$totalqtn];		  		 
			  		     $larrquestionid  = array_merge($larrquestionid,$array2);
		  		    }
		  	 }
		  	
      
		 for($i=0;$i<count($larrquestionid);$i++)
		 {
		 	if($i==0)
			{
				$larrquestionids=$larrquestionid[$i];
			}
			else
			$larrquestionids.=','.$larrquestionid[$i];
		 }
		 /////////////////////////////inserting obrtained questions for the student//////////
		$idstudent = $this->gsessionregistration->IDApplication;
	
		//////////////////////////////////end of funciton/////////////////////////////
        $this->view->totalnumberofquestions = count($larrquestionid);
		
        $this->view->questionsdisplay = $larrquestionssssssss;
       
        $larranswers = $lobjExamdetailsmodel->fnGetAnswers($values);
		
  		$this->view->answers = $larranswers;
  			
  		/////////////////////////////////////////function to fetch from the temp answers//////////////////////
  		  $RegId = $this->gsessionregistration->Regid;
 		  $lobjExamdetailsmodel = new App_Model_Examdetails();
 		  $larrtempanswered = $lobjExamdetailsmodel->fnGettempRegisterid($RegId);
 		
 		  
 		  for($ssss = 0;$ssss<count($larrtempanswered);$ssss++)	 
 		  { 
 		  $larrtempanarrp[$ssss]  = $larrtempanswered[$ssss]['Answer'];
 		  $larrtempquestionarray[$ssss]  = $larrtempanswered[$ssss]['QuestionNo'];
 		  }
 		 
 	
 		  $this->view->attended =  $larrtempanarrp;
 		  $this->view->qtnqttended =  $larrtempquestionarray;
				
		}
  	else 
  	{
		//echo "abssssssssss";die();	
  		$larrresultsstudentime = $lobjExamdetailsmodel->fninsertintostudentexamdetails($idApplication,$starttime,$endtime,$submittedby,$StudentIpAddress);
  		for ($linta=0;$linta<count($larridqtnfrombatch);$linta++)
	  	{
	  		$larrsection[$linta]=$larrmultisections[$linta]['IdSection'];
	  		
	  		$larrqtnfrombatch[$larrsection[$linta]]=$larridqtnfrombatch[$linta]['NosOfQuestion'];
	  	}
  			
  		
  		$this->view->sectionqtns = $larrqtnfrombatch;
  	for ($sec=0;$sec<count($larrmultisections);$sec++)
  	{
  		$larrsection[$sec]=$larrmultisections[$sec]['IdSection'];
  	}
  	
  	//print_r($larrmultisections);die();
  	for($s=0;$s<count($larrsection);$s++)
		{
			$value = $larrsection[$s];
			if($s==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
			
		}
		
		$this->view->countsections = count($larrsection);
		$this->view->listsections = $values;
 
  	
  	$this->view->sectionsarray = $larrsection;
  		$this->view->sections = $larrmultisections;
  		
  		 $g=0;
  		for($i=0;$i<count($larrsection);$i++){
  		
  		$larrresulteasy = $lobjExamdetailsmodel->fnGetNoofQuestion($idIdBatch,$larrsection[$i]);
  		
  	 
  		if($larrresulteasy[0]['NoofQuestions']!=0)
  		{   
  		$larrdef[$g]=$larrresulteasy[0]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[1]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[1]['IdDiffcultLevel'];
  		$g++;
  		}
  		if($larrresulteasy[2]['NoofQuestions']!=0)
  		{
  		$larrdef[$g]=$larrresulteasy[2]['IdDiffcultLevel'];
  		$g++;
  		}
  		
  		
  	     for($k=0;$k<count($larrresulteasy);$k++)
            {
            	$questionArr  = $larrresulteasy[$k]['IdDiffcultLevel'];
            	$noofquestion = $larrresulteasy[$k]['NoofQuestions'];
            	if($noofquestion<0)
            	 $noofquestion = 0;
            	$idsection = $larrresulteasy[$k]['IdSection'];
				switch ($questionArr) 
				{
				    case 1:
				        $easyquestion = $noofquestion;
				        break;
				    case 2:
				        $Mediumquestion = $noofquestion;
				        break;
				    case 3:
				        $difficultquestion = $noofquestion;
				        break;
				}
			
				
            }
            /*echo "<pre />";
        echo $easyquestion."<br>";
           echo  $Mediumquestion."<br>";
           echo $difficultquestion."<br>";
            print_r($larrsection[$i]);*/
            $larreasyquestion[$larrsection[$i]] = $lobjExamdetailsmodel->fnGetRandomEasyQuestions($larrsection[$i],$easyquestion,$Mediumquestion,$difficultquestion);
       
          
  		}
		
  		
  $this->view->difficultylevel=$larrdef;
 //echo "<pre/>";
 // print_r(count($larreasyquestion));
 // print_r($larreasyquestion);
for($i=0;$i<count($larrsection);$i++)
		{
			$qunnoforsection  = array();
			$arranssection[] =$larrsection[$i] ;
			$noofqtnssection[$larrsection[$i]] =count($larreasyquestion[$larrsection[$i]]);
			for($l=0;$l<count($larreasyquestion[$larrsection[$i]]);$l++)
			{
				
				$qunno[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
				$qunnoforsection[] = ($larreasyquestion[$larrsection[$i]][$l]['idquestions']);
		
				//$noofquestionofsec = 
			}
					
				$arraysofqtnnowithsection[$larrsection[$i]]=$qunnoforsection;
			
			//$arrayqstn[$l] = $larreasyquestion[$l]['idquestions'];
		}
		//print_r($arraysofqtnnowithsection);
		//die();
		$this->view->questionno = $arraysofqtnnowithsection;
		//die();
			
		for($j=0;$j<count($qunno);$j++)
		{
			$value = $qunno[$j];
			if($j==0)
			{
				$values=$value;
			}
			else
			$values.=','.$value;
		}
		$arrayqtns = $values;
		$this->view->arrayqtns = $values;
		
		
		
		//echo "<pre/>";

	//print_r($values);
		//$larrquestions  = $lobjExamdetailsmodel->fngetquestionpool($values);
		
		
		$larranswers = $lobjExamdetailsmodel->fnGetAnswers($values);
  		$this->view->answers = $larranswers;
  		/*print_r($larrquestions);
		die();*/
  //	$this->view->totalnumberofquestions = count($larrquestionid);
   //    $this->view->questionsdisplay = $larrquestions;
     ////////////////////////////////////////////////////////////////////////////////////////  
       
       
       
	$larrmultisections = $lobjExamdetailsmodel->fnGetSections($idIdBatch);
  		
  		$flagg = 1;
  		if(count($larrmultisections)>1)
  		{
  			$flagg = 0;
  		}
  		
		    for ($linta=0;$linta<count($larrmultisections);$linta++)
		    {
		    	
		    	$larrparts[$linta]=$larrmultisections[$linta]['IdPart'];
		    	//die();
		    	
		  		  $larrquestions[$linta] = $lobjExamdetailsmodel->fngetquestionpool($values,$larrmultisections[$linta]['IdPart']);  
					shuffle ($larrquestions[$linta]);
		  		    for($totalqtn=0;$totalqtn<count($larrquestions[$linta]);$totalqtn++)
		  		    {
		  		    	 $larrquestionnumber[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestions'];
		  		    	
		  		    	 
		  		    	/*$larrquestionnumbers[$linta][$totalqtn] = $larrquestions[$linta][$totalqtn]['idquestions'];
		  		    	$concatquestions = $larrquestionnumbers[$linta][$totalqtn];
		  		    	$concatquestions.= ','.$concatquestions;*/
		  		    	
		  		    }
		  		    $larrquestionid = array();
		  		   for($totalqtn=0;$totalqtn<count($larrquestionnumber);$totalqtn++)
		  		    {	  		    	
		  		     	 $array2  = $larrquestionnumber[$totalqtn];		  		 
			  		     $larrquestionid  = array_merge($larrquestionid,$array2);
		  		    }
		  	 }
       
      // echo "<br/>";
  
		//echo "<pre/>";
//print_r($larrquestionid);die();
		 for($i=0;$i<count($larrquestionid);$i++)
		 {
		 	if($i==0)
			{
				$larrquestionids=$larrquestionid[$i];
			}
			else
			$larrquestionids.=','.$larrquestionid[$i];
		 }
		 /////////////////////////////inserting obrtained questions for the student//////////
		$idstudent = $this->gsessionregistration->IDApplication;
		//  echo "<br/>";
		// echo  $program;
		 // echo "<br/>";
		// echo  $idIdBatch;
		 // echo "<br/>";
		  $larrstudentobtainedquestions = $lobjExamdetailsmodel->fnGetStudentobtainedquestions($idstudent,$program,$idIdBatch,$larrquestionids);
		//print_r($larrquestionids);die();
		
		//////////////////////////////////end of funciton/////////////////////////////
       $this->view->totalnumberofquestions = count($larrquestionid);
       $this->view->questionsdisplay = $larrquestions;  
       
       		 $updatedidbatch = $lobjExamdetailsmodel->fnupdatebatch($RegId,$idIdBatch);
	   } 
	   
	    
	    }
	   if ($this->_request->isPost() && $this->_request->getPost('Submit')) {
			$larrformData = $this->_request->getPost(); //getting the values of bank from post 
				if ($lobjbankForm->isValid($larrformData)) {
					//print_r($larrformData);
					//die();
										
					$lobjbankmodel = new App_Model_Bank(); //bank model object				   
			        $lintresult = $lobjbankmodel->fnAddBank($lobjbankForm->getValues()); //instance for adding the lobjuserForm values to DB
			     
			        $lobjLogs = new Logs_log(); // object for log & insertion to log table
					$lobjLogs->fnLogs('BANK','ADD','New Bank Add',$this->gobjsessionsfs->primaryuserid,$this->getRequest()->getServer('REMOTE_ADDR'));
					echo "<script>parent.location = '".$this->view->baseUrl()."/bank/index';</script>";	
				}
		}
	}
}
