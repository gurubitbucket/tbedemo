<?php
class IndexController extends Zend_Controller_Action {
	
 	private $gstrsessionSIS;//Global Session Name
 	private $_gobjlogger;

	public function init() { //instantiate log object
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object

   	}
	
    public function indexAction() {
	       $this->_redirect('/index/login');
    	//echo "<script>parent.location = '".$this->view->baseUrl()."/index/login';</script>";
	}

    public function loginAction() {    
    	$this->_helper->layout->disableLayout (); //
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
        $lobjform = new App_Form_Login(); //intialize login form
                
        $this->view->lobjform = $lobjform; //send the form object to the view
        
        if ($this->_request->isPost()){
			
			$ipaddress=substr($this->view->serverUrl(),7);  /////code change 17-12-2014 
		    $phpsession=$_SERVER['HTTP_COOKIE'];/////code change 17-12-2014
	        Zend_Loader::loadClass('Zend_Filter_StripTags');
	        $filter = new Zend_Filter_StripTags();
	        $username = $filter->filter($this->_request->getPost('username'));
	        $password = $filter->filter($this->_request->getPost('password'));       			      
	        $_SESSION['pagename'] = 'batchlogin';
        	$ModelBatchlogin = new App_Model_Batchlogin();
	        $resultCheck = $ModelBatchlogin->fngetlogincheck($username,md5($password));        
	        if($resultCheck){
	        	$this->gsessionbatch= new Zend_Session_Namespace('sis');  
	        	$this->gsessionbatch->__set('idCompany',$resultCheck['IdCompany']);
	        	$auth = Zend_Auth::getInstance();
	            $priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	        	/*$this->gstrsessionSIS->__set('idCompany',$resultCheck['IdCompany']);
	        	$this->gsessionbatch->idCompany = $resultCheck['IdCompany'];	*/        	
	        	echo "<script>parent.location = '".$this->view->baseUrl()."/companyapplication/index';</script>";	
	        	exit;
	        }
	        
	        
	     /*   public function fngetsearchdetails($larrformData)
	{
		//$fromdate=$larrformData['Date7'];
		//$todate=$larrformData['Date8'];
		$lstrFromDate = date("Y-m-d",strtotime($larrformData['Date7']));
		$lstrToDate = date("Y-m-d",strtotime($larrformData['Date8']));
		$venue=$larrformData['Venue'];
		$active=$larrformData['Active'];
		$lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		$lstrSelect = $lobjDbAdpt->select()
					 ->from(array("a" => "tbl_venuedateschedule"),array("a.*,DATE_FORMAT(date(a.date),'%d-%m-%Y') as formatdate"))
					 ->join(array("b" => "tbl_center"),'a.idvenue = b.idcenter',array("b.centername"))
					 ->join(array("c" =>"tbl_managesession"),'a.idsession=c.idmangesession',array("c.managesessionname"))
					 ->where ("a.Active = ?" , $active)
					 ->where("DATE_FORMAT(a.date,'%Y-%m-%d') BETWEEN '$lstrFromDate' and '$lstrToDate'");
		if($larrformData['Venue']) $lstrSelect .= " AND a.idvenue = $venue";					  
		$lstrSelect .= " order by a.date,b.centername";		
			    $larrResult = $lobjDbAdpt->fetchAll($lstrSelect);
				return $larrResult;
	}
	        
	        */
	        
	        
	        
	        
	        
	        
	        
	        
            $Takafulmodellogin = new App_Model_Takafullogin();
	        $resultCheck = $Takafulmodellogin->fngetlogincheck($username,md5($password));
	        if($resultCheck){
	        	$this->gsessionidtakafuloperator= new Zend_Session_Namespace('sis'); 
	        	$this->gsessionidtakafuloperator->__set('idtakafuloperator',$resultCheck['idtakafuloperator']);
	            $auth = Zend_Auth::getInstance();
	            $priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$username."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	        	/*$this->gstrsessionSIS->__set('idCompany',$resultCheck['IdCompany']);
	        	$this->gsessionbatch->idCompany = $resultCheck['IdCompany'];*/        	
	        	echo "<script>parent.location = '".$this->view->baseUrl()."/takafulapplication/index';</script>";	
	        	exit;
	        }
          $Centermodellogin = new App_Model_Centerlogin();
	      $resultCheck = $Centermodellogin->fngetlogincheck($username,md5($password));
	       
	        if($resultCheck){
	        	$this->gsessionidCenter= new Zend_Session_Namespace('sis');
	        	$this->gsessionidCenter->__set('idcenter',$resultCheck['idcenter']);
	        	$auth = Zend_Auth::getInstance();
	            $priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	          
	        	/*$this->gstrsessionSIS->__set('idCompany',$resultCheck['IdCompany']);
	        	$this->gsessionbatch->idCompany = $resultCheck['IdCompany'];	*/        

				
	        	echo "<script>parent.location = '".$this->view->baseUrl()."/centerauthnew/index';</script>";	
	        	exit;
	        }
	        
	        $dbAdapter = Zend_Db_Table::getDefaultAdapter();

			$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
				
			$authAdapter->setTableName('tbl_user')
			    		->setIdentityColumn('loginName')
			    		->setCredentialColumn('passwd');
			    		
            $authAdapter->setIdentity($username);
            $authAdapter->setCredential(md5($password));
            
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);
           /* echo "<pre>";
            print_r($result);
            exit;*/
            if ($result->isValid()) {
				$data = $authAdapter->getResultRowObject(null, 'passwd');
				$auth->getStorage()->write($data);
				$auth->getIdentity()->iduser;
				$role = $auth->getIdentity()->IdRole;
			
	           if($auth->getIdentity()->iduser==26)
				{
					//$this->gsessionbatch= new Zend_Session_Namespace('sis');  
					//	$this->gsessionbatch->__set('idtk',$auth->getIdentity()->iduser);
				echo "<script>parent.location = '".$this->view->baseUrl()."/specialstatusreport/index';</script>";	
				}
				else 
				{
	
                $larrCommonModel = new App_Model_Common();
                $Rolename = $larrCommonModel->fnGetRoleName($auth->getIdentity()->IdRole);
                $this->gstrsessionSIS->__set('iduser',$auth->getIdentity()->iduser);
                
               // $userdetails = $larrCommonModel->fnGetUserDetails($auth->getIdentity()->iduser);
				if($Rolename['DefinitionDesc']== "Admin") {
					$this->gstrsessionSIS->__set('idUniversity',1);
					$this->gstrsessionSIS->__set('idCollege',0);
					// user type 0:college  1: branch
					$this->gstrsessionSIS->__set('userType',0);
					$this->gstrsessionSIS->__set('rolename',$Rolename['DefinitionDesc']);
				} else {
					$staffdetails = $larrCommonModel->fnGetStaff($auth->getIdentity()->IdStaff);
					$Universitydetails = $larrCommonModel->fnGetUniversity($staffdetails['IdCollege']);

					$this->gstrsessionSIS->__set('idUniversity',$Universitydetails['AffiliatedTo']);
					$this->gstrsessionSIS->__set('idCollege',$staffdetails['IdCollege']);
					$this->gstrsessionSIS->__set('userType',$staffdetails['StaffType']);  // user type 0:college  1: branch
					$this->gstrsessionSIS->__set('rolename',$Rolename['DefinitionDesc']);
				}
				
				
				
				//multiple login disable 16-12-2014
				$Typeofuser=1;
				$iduser=$auth->getIdentity()->iduser;
				$sessionstatus=$ModelBatchlogin->fnGetSessionStatus($iduser,$Typeofuser);
				$this->gstrsessionSIS->loggedinflag=0;
				if($sessionstatus['Isonline']==1){
					$this->gstrsessionSIS->loggedinflag = 1;
					//echo "<script>alert('Your session is still alive,Please try again after some time');</script>";
					$this->_redirect('/index/login');				
					//echo "<script>parent.location = '".$this->view->baseUrl()."/index/login';</script>";
					exit;
				}
				//end of multiple login disable
				//multiple login disable 16-12-2014
				
				$Sessionstart=date("Y-m-d H:i:s");
				
				$Sessionend="0000-00-00 00:00:00";
				$Isonline=1;
				$Active=1;
				$Typeofuser=1;
				$ModelBatchlogin->fnInsertLoginDetails($iduser,$Sessionstart,$Sessionend,$Isonline,$ipaddress,$Active,$phpsession,$Typeofuser); 
				//end of multiple login disable
				
				
					// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
				
				if($role == 169)
				{
				     $this->_redirect('/website/webpagetemplate/index');	
					//echo "<script>parent.location = '".$this->view->baseUrl()."/website/webpagetemplate/index';</script>";
				}
                                else if($auth->getIdentity()->iduser == 21)
	
			         {
					 $this->_redirect('/general-setup/initialconfiguration/index');	
				   // echo "<script>parent.location = '".$this->view->baseUrl()."/general-setup/initialconfiguration/initialconfiguration';</script>";				   
				
                                 }
				else 
				{
				$this->_redirect('/general-setup/user/index');	
				//echo "<script>parent.location = '".$this->view->baseUrl()."/general-setup/user/index';</script>";
				}	
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'user', 'action'=>'index'),'default',true));
            }
} else {
            	$this->view->alertError = 'Login failed. Either username or password is incorrect';
            }     
        }
		$this->render(); //render the view
    }


    public function logoutAction() {    
 
    	 $auth = Zend_Auth::getInstance();
		 
		  ///punch in log out modified on 16/12/2014
    	$this->gsessionbatch= new Zend_Session_Namespace('sis');  
		$Typeofuser=1;
		$logouttime=date("Y-m-d H:i:s");
		$iduser=$this->gsessionbatch->__get('iduser');
	    $ModelBatchlogin = new App_Model_Batchlogin();
	    $ModelBatchlogin->fnPunchInLogOut($iduser,$Typeofuser,$logouttime);
    	//end of punch in log out modified on 16/12/2014 
		 
		 
    	 // Write Logs
		$priority=Zend_Log::INFO;
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged Out"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
		$this->_gobjlogger->log($message,5);
    	
    	Zend_Session:: namespaceUnset('sis');
    	$storage = new Zend_Auth_Storage_Session();          
        $storage->clear(); 
        $this->_redirect('/index/login');            
        //print_r(Zend_Registry::get('sis'));         
    	//echo "<script>parent.location = '".$this->view->baseUrl()."/index/login';</script>";
    	//exit;
		//$this->_redirect($this->view->url(array('controller'=>'index', 'action'=>'login'),'default',true));
		//echo "<script>parent.location = '".$this->view->baseUrl()."/index/login';</script>";
    }
}
