<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class DocumentuploadsController extends Zend_Controller_Action
{
	public function init() 
	{ 
		
		$this->_helper->layout()->setLayout('/batch3/usty1');	
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	Private function fnsetObj() 
	{
		$this->lobjdocumentuploadsform=new App_Form_Documentuploads();
		$this->lobjDocumentuploadsmodel=new App_Model_Documentuploads();//object of common model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
		
	public function indexAction() 
	{
		
		if ($this->_request->isPost('Save'))
		{  	
			$larrformData = $this->_request->getPost();
			$lintfilecount['Count'] =0;
			$lstruploaddir = "/uploads/documentsuploads/";
			for($k=1;$k<=3;$k++)
			{
				
				if($_FILES['uploadeddocuments'.$k]['error'] != UPLOAD_ERR_NO_FILE)
				{
						$lintfilecount['Count']++;
						$lstrfilename = pathinfo(basename($_FILES['uploadeddocuments'.$k]['name']), PATHINFO_FILENAME);
						$lstrext = pathinfo(basename($_FILES['uploadeddocuments'.$k]['name']), PATHINFO_EXTENSION);
						$filename = $lintfilecount['Count'].".".date('YmdHis').".".$lstrext;
						$filename = str_replace(' ','_',$lstrfilename)."_".$filename;	
						
						$file = realpath('.').$lstruploaddir . $filename;
						if (move_uploaded_file($_FILES['uploadeddocuments'.$k]['tmp_name'],$file)) 
						{
							if($k==1)
							{
								$documentnames['ImageFiles'] = $filename;									//for images to upload for passport
								$result1=$documentnames['ImageFiles'];	
							}						
							if($k==2)
							{
								$documentnames['DocForPassport'] = $filename;									//for date of birth details
								$result2=$documentnames['DocForPassport'];
							}
							if($k==3)
							{
								$documentnames['DocForAddress'] = $filename;								//for document address
								$result3=$documentnames['DocForAddress'];
							}							
							
						}
						else 
						{
							echo "error1";
						}
						
					}
				}
			$larrformData['Photo']=$result1;
			$larrformData['passport']=$result2;
			$larrformData['Address']=$result3;
			$result=$this->lobjDocumentuploadsmodel->fnAddUploadfiles($larrformData);
			//$largarrayresult=$this->lobjDocumentuploadsmodel->fnGetUploadfiles();
			//$this->view->lararrdata=$largarrayresult;
		}	
	}
}
