<?php
class IntroductionController extends Zend_Controller_Action { //Controller for the User Module

	public $gsessionbatch;//Global Session Name
	public $gsessionidtakafuloperator;//Global Session Name
	private $_gobjlogger;
public function init() { //initialization function

		$this->_helper->layout()->setLayout('/details/usty2');	
		//$this->Url = "http://192.168.1.103/tbe";	
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
	}
	
	public function fnsetObj() {
		$this->lobjIntroductionmodel = new App_Model_Introduction(); //user model object
		$this->lobjIntroductionForm = new App_Form_Introduction();
		$this->lobjCommon=new App_Model_Common();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	

   public function indexAction() { // action for search and view
   //	$this->_helper->layout->disableLayout();
   
     	$this->view->lobjIntroductionForm = $this->lobjIntroductionForm;
		$larrDropdownValues = $this->lobjIntroductionmodel->fnGetWebPageDetails();
/*		echo "<pre/>";	
		print_r($larrDropdownValues);
		die();*/
		$this->view->larrsideview = $larrDropdownValues;
        $this->view->counarr = count($larrDropdownValues);
       
        $_SESSION['pagename'] = 'introduction';
        $this->view->pagename = $this->_getParam('page');
        if( $this->_getParam('lang')) $_SESSION['lange'] = $this->_getParam('lang');
        else if($_SESSION['lange']) $_SESSION['lange'] = $_SESSION['lange'];
        else $_SESSION['lange'] = "en";
       
        $this->view->lange = $_SESSION['lange'];
        $u= $this->_getParam('u');
        $this->view->errorUsername = $u;
       
        $p= $this->_getParam('p');
        $this->view->errorPassword = $p;
        if ($this->_request->isPost () && $this->_request->getPost( 'login' ))	{ 	
        	 
        	
			$larrformData = $this->_request->getPost ();			
       		Zend_Loader::loadClass('Zend_Filter_StripTags');
	        $filter = new Zend_Filter_StripTags();	       	        
	        if ($this->_request->getPost( 'ses_username')){ 	        	 		
	       	 	$username = $filter->filter($this->_request->getPost('ses_username'));
	        	$password = $filter->filter($this->_request->getPost('ses_userpass'));
	        }
	        else{
	        	
	        	$username = $filter->filter($this->_request->getPost('username'));
	            $password = $filter->filter($this->_request->getPost('password'));
	        }
	        
	        $_SESSION['pagename'] = 'introduction';
	        
       		$ModelBatchlogin = new App_Model_Batchlogin();
	        $resultCheck = $ModelBatchlogin->fngetlogincheck($username,md5($password));        
	        if($resultCheck){
	        	$this->gsessionbatch= new Zend_Session_Namespace('sis');  
	        	$this->gsessionbatch->__set('idCompany',$resultCheck['IdCompany']);
	        	$auth = Zend_Auth::getInstance();
	            $priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	        	/*$this->gstrsessionSIS->__set('idCompany',$resultCheck['IdCompany']);
	        	$this->gsessionbatch->idCompany = $resultCheck['IdCompany'];	*/        	
	        	echo "<script>parent.location = '".$this->view->baseUrl()."/companyapplication/index';</script>";	
	        	exit;
	        }
	        
       		$Takafulmodellogin = new App_Model_Takafullogin();
	        $resultCheck = $Takafulmodellogin->fngetlogincheck($username,md5($password));
	        if($resultCheck){
	        	$this->gsessionidtakafuloperator= new Zend_Session_Namespace('sis');    	
	        	$this->gsessionidtakafuloperator->__set('idtakafuloperator',$resultCheck['idtakafuloperator']);
	            $auth = Zend_Auth::getInstance();
	            $priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$username."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	        	/*$this->gstrsessionSIS->__set('idCompany',$resultCheck['IdCompany']);
	        	$this->gsessionbatch->idCompany = $resultCheck['IdCompany'];	*/        	
	        	echo "<script>parent.location = '".$this->view->baseUrl()."/takafulapplication/index';</script>";	
	        	exit;
	        }
	        
	      $Centermodellogin = new App_Model_Centerlogin();
	      $resultCheck = $Centermodellogin->fngetlogincheck($username,md5($password));

	       
	        if($resultCheck){
	        	$this->gsessionidCenter= new Zend_Session_Namespace('sis');
	        	$this->gsessionidCenter->__set('idcenter',$resultCheck['idcenter']);
	        	$auth = Zend_Auth::getInstance();
	            $priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	          
	        	/*$this->gstrsessionSIS->__set('idCompany',$resultCheck['IdCompany']);
	        	$this->gsessionbatch->idCompany = $resultCheck['IdCompany'];	*/        	
	        	echo "<script>parent.location = '".$this->view->baseUrl()."/centerauthnew/index';</script>";	
	        	exit;
	        }
	        
	        $dbAdapter = Zend_Db_Table::getDefaultAdapter();

			$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
				
			$authAdapter->setTableName('tbl_user')
			    		->setIdentityColumn('loginName')
			    		->setCredentialColumn('passwd');
			    		
            $authAdapter->setIdentity($username);
            $authAdapter->setCredential(md5($password));
            
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);
            
            if ($result->isValid()) {
				$data = $authAdapter->getResultRowObject(null, 'passwd');
				$auth->getStorage()->write($data);
				$auth->getIdentity()->iduser;
				$role = $auth->getIdentity()->IdRole;
				
                $larrCommonModel = new App_Model_Common();
                $Rolename = $larrCommonModel->fnGetRoleName($auth->getIdentity()->IdRole);
                $this->gstrsessionSIS = new Zend_Session_Namespace('sis');
                $this->gstrsessionSIS->__set('iduser',$auth->getIdentity()->iduser);
                
               // $userdetails = $larrCommonModel->fnGetUserDetails($auth->getIdentity()->iduser);
				if($Rolename['DefinitionDesc']== "Admin") {
					$this->gstrsessionSIS->__set('idUniversity',1);
					$this->gstrsessionSIS->__set('idCollege',0);
					// user type 0:college  1: branch
					$this->gstrsessionSIS->__set('userType',0);
					$this->gstrsessionSIS->__set('rolename',$Rolename['DefinitionDesc']);
				} else {
					$staffdetails = $larrCommonModel->fnGetStaff($auth->getIdentity()->IdStaff);
					$Universitydetails = $larrCommonModel->fnGetUniversity($staffdetails['IdCollege']);

					$this->gstrsessionSIS->__set('idUniversity',$Universitydetails['AffiliatedTo']);
					$this->gstrsessionSIS->__set('idCollege',$staffdetails['IdCollege']);
					$this->gstrsessionSIS->__set('userType',$staffdetails['StaffType']);  // user type 0:college  1: branch
					$this->gstrsessionSIS->__set('rolename',$Rolename['DefinitionDesc']);
				}
				
					// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".$auth->getIdentity()->loginName."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				
				
				if($role == 169)
				{
					echo "<script>parent.location = '".$this->view->baseUrl()."/website/webpagetemplate/index';</script>";
				}
				else 
				{
				echo "<script>parent.location = '".$this->view->baseUrl()."/general-setup/user/index';</script>";
				}	
	        
	        
	        
            }
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	        
	       // print_r($resultCheck);
	        //exit;
	        /*else {        
            		echo "<script>alert('Login failed. Either username or password is incorrect');</script>";
            		echo "<script>parent.location = '".$this->view->baseUrl()."/introduction/index';</script>";
            		exit;                 	
            } */
			
			//$_SESSION['pagename'] = 'batchlogin';
			$_SESSION['pagename'] = 'introduction';  
        }
   	}

	public function fngetdetailsAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$languages = 0;
		$idDefinition = $this->_getParam('idDefinition');
		$languages = $this->_getParam('languages');
			
		if($languages == 0)
		{
			$larrbatchresult = $this->lobjIntroductionmodel->fnGetIntroductionDetails($idDefinition);
			echo $contents = $larrbatchresult['content'];exit;
		}
		if($languages == 1)
		{
			$larrbatchresult = $this->lobjIntroductionmodel->fnGetIntroductionDetails($idDefinition);
			echo $contents = $larrbatchresult['banhasa'];exit;
		}
	  if($languages == 2)
		{
			$larrbatchresult = $this->lobjIntroductionmodel->fnGetIntroductionDetails($idDefinition);
			echo $contents = $larrbatchresult['others'];exit;
		}
	
		//$this->view->editdetails = $contents;
		//$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrbatchresult);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	public function fngetarabicAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$languages = $this->_getParam('languages');
		
		$larrbatchresult = $this->lobjIntroductionmodel->fnGetIntroductionDetails($idDefinition);
		echo $contents = $larrbatchresult['content'];exit;
		
		
		//$this->view->editdetails = $contents;
		//$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrbatchresult);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	public function testAction(){
		$this->_helper->layout->disableLayout();
		$result = $this->lobjIntroductionmodel->reportDetails();
		$this->view->data = $result;
		
		
		$resultcenter = $this->lobjIntroductionmodel->getcenter();
		$this->view->center = $resultcenter;
		
//		echo "<pre>";
//		print_r($result);
//		echo "</pre>";
		
		//$this->view->editdetails = $contents;
		//$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrbatchresult);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
}
