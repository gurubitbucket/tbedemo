<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class VenueresultxlcreationController extends Zend_Controller_Action {
	private $_gobjlogger;
	public function init() { //initialization function			
		$this->gsessionidCenter = Zend_Registry::get('sis'); 	
		if(empty($this->gsessionidCenter->idcenter)){ 
			$this->_redirect( $this->baseUrl . '/batchlogin/logout');					
		}
		$this->_helper->layout()->setLayout('/examcenter/usty1');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		//$this->fnsetObj();
	}
    
	public function indexAction(){
		$this->view->checkEmpty = 0;
		$lobjReportsForm =  new Examination_Form_Newscreen();
		$lobjExamreportModel = new App_Model_Examreport();
		$this->view->lobjform = $lobjReportsForm;	
		$this->lobjvenuexlcreationmodel = new Examination_Model_Venuexlcreation;	
		$larrcourses=$lobjExamreportModel->fngetprogramnames();		
		$lobjReportsForm->Coursename->addMultiOption('','Select'); 	
		$lobjReportsForm->Coursename->addmultioptions($larrcourses);
		$larrvenues = $lobjExamreportModel->fngetcenternames();	
		
		$larrSchedulerSessionDetails = $this->lobjvenuexlcreationmodel->fnGetSchedulerSessionDetails();	
		$lobjReportsForm->examsession->addMultiOption('','Select'); 
		$lobjReportsForm->examsession->addmultioptions($larrSchedulerSessionDetails);
		
		$lobjReportsForm->Date->setAttrib('required',"true");
 		
		if ($this->_request->isPost () && $this->_request->getPost ('Search')) {
 			$larrformData = $this->_request->getPost();
 			//echo "<pre>";
 			///print_r($larrformData);
 		
 			if ($lobjReportsForm->isValid ( $larrformData )) {
 				$this->view->venufield = $venuefield 	=  $this->gsessionidCenter->idcenter;
 				$this->view->datefield = $examrvenuedate =  $larrformData['Date'];
 				$larrresult = $this->lobjvenuexlcreationmodel->fnReportSearchDetails($larrformData,$this->gsessionidCenter->idcenter);			
 			
 				$this->view->paginar = $larrresult;
				$this->gsessionidCenter->examreportpaginatorresult1 = $larrresult;
				$this->gsessionidCenter->examvenue=$venuefield;
				$this->gsessionidCenter->examrvenuedate = $examrvenuedate;			
			}	
	  }
	  if ($this->_request->getParam ('examdate')) {
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$larrformDate = $this->_request->getParam ('examdate');
			$larrresult = $this->lobjvenuexlcreationmodel->fnReportDataDetails($larrformDate);			
			$lvarcentername = $this->gsessionidCenter->idcenter;
			if (count($larrresult)){				
				$day= $larrformDate;
				$host = $_SERVER['SERVER_NAME'];
				$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
				$time = date('h:i:s',time());
			 	$filename = "Venuewise_".$day."_".$lvarcentername;		
	
				$tabledata.= '<table border=1 align=center width=100%>
							<tr>
								<th><b>IDApplication</b></th>
								<th><b>idstudentmarks</b></th>
								<th><b>NoofQtns</b></th>
								<th><b>Attended</b></th>
								<th><b>Correct</b></th>
								<th><b>Grade</b></th>
								<th><b>idstudentdetailspartwisemarks</b></th>
								<th><b>Regid</b></th>
								
								<th><b>A</b></th>
								<th><b>B</b></th>
								<th><b>C</b></th>
								<th><b>UpdDate</b></th>
								<th><b>idstudentstartexamdetails</b></th>
								<th><b>Starttime</b></th>
								<th><b>Endtime</b></th>
								<th><b>Submittedby</b></th>
								<th><b>StudentIpAddress</b></th>								
							</tr>';
						
	      		  foreach ($larrresult as $lobjStudent ){
	      		  	   $tabledata.= ' <tr>
					   		   <td>'.$lobjStudent['IDApplication'].'</td>
						       <td>'.$lobjStudent['idstudentmarks'].'</td> 
							   <td>'.$lobjStudent['NoofQtns'].'</td> 
							   <td>'.$lobjStudent['Attended'].'</td> 
							   <td>'.$lobjStudent['Correct'].'</td> 
							   <td>'.$lobjStudent['Grade'].'</td> 		  
							   <td>'.$lobjStudent['idstudentdetailspartwisemarks'].'</td> 
							   <td>'.$lobjStudent['Regid'].'</td> 
	
							   <td>'.$lobjStudent['A'].'</td>
						       <td>'.$lobjStudent['B'].'</td> 
							   <td>'.$lobjStudent['C'].'</td> 
							   <td>'.$lobjStudent['UpdDate'].'</td> 
							   <td>'.$lobjStudent['idstudentstartexamdetails'].'</td> 
							   <td>'.$lobjStudent['Starttime'].'</td> 		  
							   <td>'.$lobjStudent['Endtime'].'</td> 
							   <td>'.$lobjStudent['Submittedby'].'</td>
							   <td>'.$lobjStudent['StudentIpAddress'].'</td>							   
					        </tr> ';				   			
	      		  }	 
	      		 $tabledata.= '</table>';
		
				$ourFileName = realpath('.')."/data";
				$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
				ini_set('max_execution_time', 3600);
				fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
				fclose($ourFileHandle);
				header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
				header("Content-Disposition: attachment; filename=$filename.xls");
				header("Pragma: no-cache");
				header("Expires: 0");
				readfile($ourFileName);
				chmod($ourFileName, 0777);
				unlink($ourFileName);     
			 }	
			 exit;	
	  }	  
	}	
}