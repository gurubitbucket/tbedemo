<?php
error_reporting (E_ALL & ~E_NOTICE & ~E_DEPRECATED);
class TestcompanyexcelController extends Base_Base 
{
	public function init() 
	{
		$this->view->translate = Zend_Registry::get('Zend_Translate'); //get translator instance 
   	    Zend_Form::setDefaultTranslator($this->view->translate); //initialize translator
   	    $this->registry = Zend_Registry::getInstance();
   	    $this->locale = $this->registry->get('Zend_Locale');
   	    $this->lobjTestexcelmodel = new Examination_Model_Testexcels(); //intialize newscreen db object
		$this->lobjTestexcelForm = new Examination_Form_Testexcels(); 
		$this->_helper->layout()->setLayout('/adhoc/usty1');
	}
	public function indexAction() 
	{
	    $auth = Zend_Auth::getInstance();
		$this->view->lobjTestexcelForm = $this->lobjTestexcelForm;		
		if ($this->_request->isPost() && $this->_request->getPost('Upload'))
		{
			$larrformData = $this->_request->getPost();
			$lintfilecount['Count'] =0;
			$lstruploaddir = "/uploads/testingdata/";
			$larrformData['FileLocation'] = $lstruploaddir;			    
			 require_once 'Excel/excel_reader2.php';			
			 if ($this->lobjTestexcelForm->isValid($larrformData))
			 {
			    if($_FILES['FileName']['error'] != UPLOAD_ERR_NO_FILE)
				{
				  $lintfilecount['Count']++; 				
				  $lstrfilename = pathinfo(basename($_FILES['FileName']['name']),PATHINFO_FILENAME);					
				  $lstrext = pathinfo(basename($_FILES['FileName']['name']),PATHINFO_EXTENSION);				 
				  $filename = $lintfilecount['Count'].".".date('YmdHis').".".$lstrext;
				  $filename = str_replace(' ','_',$lstrfilename)."_".$filename;
				  $file = realpath('.').$lstruploaddir . $filename;	
				  if (move_uploaded_file($_FILES['FileName']['tmp_name'],$file)) 
				 {
					
				 }
				  $data = new Spreadsheet_Excel_Reader(APPLICATION_PATH . '/../public/uploads/testingdata/'.$filename);			 			    
     		      $arr = $data->sheets;				  
				  $min = (array_keys($arr[0]['cells']));				  
				  $firstrowvalue = $min[1];				  
				  $totalcols = max(array_keys($arr[0]['cells'][1]));
				  $cnt = max(array_keys($arr[0]['cells']));
				  $maxfirstvalue = max(array_keys($arr[0]['cells'][1]));				  
                  $genderfield = strtoupper($arr[0]['cells'][$i][7]);					
				  $this->lobjTestexcelmodel->fnDeleteTempDetails();
				//print_r($min);
				//echo $firstrowvalue;
				//echo $cnt;
				//echo "<br/>";
				//echo $totalcols;die();
		if($cnt > 1)
		{
				for($i=$firstrowvalue;$i<=$cnt;$i++)
                {				
                    $remarks = '';
					
                    $idapp = '';					
					$genderfield = strtoupper($arr[0]['cells'][$i][7]);  
                    //checking for invalid excel sheet					
				    if ($arr [0] ['cells'] [1] [1] != "Student Name")
					{ 
					    echo '<script language="javascript">alert("Incorrect excel format Please change First field to Student Name")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
					if($arr [0] ['cells'] [1] [2] != "ICNO")
					{
					    echo '<script language="javascript">alert("Incorrect excel format  Please change Second field  change to ICNO")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
					if($arr [0] ['cells'] [1] [3] != "E-Mail")
					{
					    echo '<script language="javascript">alert("Incorrect excel  format Please change Third field  change to E-Mail")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
					if($arr [0] ['cells'] [1] [4] != "Race")
					{
					    echo '<script language="javascript">alert("Incorrect excel format Please change Fourth field  change to Race")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
					if($arr [0] ['cells'] [1] [5] != "Education")
					{
					    echo '<script language="javascript">alert("Incorrect excel format Please change Fifth field  change to Education")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
					if($arr [0] ['cells'] [1] [6] != "DateofBirth")
					{
					    echo '<script language="javascript">alert("Incorrect excel format Please change Sixth field  change to DateofBirth")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
                    if($arr [0] ['cells'] [1] [7] != "Gender")
					{
					    echo '<script language="javascript">alert("Incorrect excel format Please change Seventh field change to Gender")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
					if($arr [0] ['cells'] [1] [8] != "Mailing Address")
					{
					    echo '<script language="javascript">alert("Incorrect excel format Please change Eigth field  change to Mailing Address")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
					if($arr [0] ['cells'] [1] [9] != "Correspondance Address")
					{
					    echo '<script language="javascript">alert("Incorrect excel format Please change Ninth field  change to Correspondance Address")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
                    if($arr [0] ['cells'] [1] [10] != "Postal Code")
					{ 
					    echo '<script language="javascript">alert("Incorrect excel format Please change Tenth field  change to Postal Code")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
                    if($arr [0] ['cells'] [1] [11] != "Country")
					{
					    echo '<script language="javascript">alert("Incorrect excel format Please change 11th field change to Country")</script>';
					    echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
   					if($arr [0] ['cells'] [1] [12] != "State")
					{
					   echo '<script language="javascript">alert("Incorrect excel format Please change 12th field  change to State")</script>';
					   echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
					if($arr [0] ['cells'] [1] [13] != "Contact No")
					{
					   echo '<script language="javascript">alert("Incorrect excel format Please change 13th field  change to Contact No")</script>';
					   echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
					}
					if($arr [0] ['cells'] [1] [14] != "Mobile No") 
			        {
				       echo '<script language="javascript">alert("Incorrect excel format Please change 14th field  change to Mobile No")</script>';
					 /*$filepath = "/uploads/Sample.xls";
			         $file = $filepath; header("Content-type: application/force-download");
			         header("Content-Transfer-Encoding: Binary");
			         header("Content-length: ".filesize($file)); 
			         header("Content-disposition: attachment; filename=\"".basename($file)."\""); readfile($file);
			         //Delete it
			         //unlink($ip);
					 */
					   echo "<script>parent.location = '".$this->view->baseUrl()."/testcompanyexcel/index';</script>";
			        }
					
					$studentnamefield = $arr[0]['cells'][$i][1];
					//echo $studentnamefield;die();
				    $icnumfield =$arr[0]['cells'][$i][2];
				    $emailfield = $arr[0]['cells'][$i][3];
				    $racefield = $arr[0]['cells'][$i][4];
				    $edufield = $arr[0]['cells'][$i][5];
					$DOB = $arr[0]['cells'][$i][6];
					$Address = $arr[0]['cells'][$i][8];
					$CorrAddress = $arr[0]['cells'][$i][9];
					$Postal = $arr[0]['cells'][$i][10];
                    $Country = $arr[0]['cells'][$i][11];
					$State = $arr[0]['cells'][$i][12];
					$Contactno =$arr[0]['cells'][$i][13];
					$Mobile =$arr[0]['cells'][$i][14];
					
					if($icnumfield !='')
					{
						$larricnum = $this->lobjTestexcelmodel->fncheckicnum($icnumfield);
						$informaticnum = $icnumfield;                    				
						$larricnumfromtemp = $this->lobjTestexcelmodel->fncheckicnumfromtemp($icnumfield);						
						if(!empty($larricnum))
						{
						   $informaticnum ="DUPLICATE IN SYSTEM";
						   $larrregisterdetails = $this->lobjTestexcelmodel->fngetregisterdetails($larricnum['IDApplication']);	
						   if($larrregisterdetails['pass']==1)
						   {
						     $result ='Pass';
						   }
						   elseif($larrregisterdetails['pass']==2)
						   {
						     $result ='Fail';
						   }
						   elseif($larrregisterdetails['pass']==3)
						   {
						     $result ='Applied';
						   }
						   else
						   {
						     $result ='Absent';
						   }	
						   $larrcompanydetails = $this->lobjTestexcelmodel->getcompanydetails($larrregisterdetails['batchpayment'],$larrregisterdetails['RegistrationPin']);
						   //echo $larrcompanydetails['Company'];
						   if(!empty($larrcompanydetails))
						   {
						     // echo "<pre>";
							 //print_r($larrcompanydetails);die();
							 if($larrcompanydetails['Company'])
								 {
								   $remarks = 'Regid:'.$larrregisterdetails['Regid'].'  '.'RegPin:'.$larrregisterdetails['RegistrationPin'].'  '.'CompanyName:'.$larrcompanydetails['Company'].'  '.'Result:'.$result;
                                   $idapp = $larrregisterdetails['IDApplication'];								
								}
								 else
								 {
								   $remarks = 'Regid:'.$larrregisterdetails['Regid'].'  '.'RegPin:'.$larrregisterdetails['RegistrationPin'].'  '.'TakafulName:'.$larrcompanydetails['Takaful'].'  '.'Result:'.$result;
								   $idapp = $larrregisterdetails['IDApplication'];	
								 }
						   }
						   else
						   {
						     $remarks = 'Regid:'.$larrregisterdetails['Regid'].'  '.'RegPin:'.$larrregisterdetails['RegistrationPin'].'  '.'Registration:Individual'.'  '.'Result:'.$result;
                             $idapp = $larrregisterdetails['IDApplication'];							  
						  }
						}
						if(!empty($larricnumfromtemp))
						{
						    $informaticnum ="DUPLICATE IN EXCEL";
						}					
						if(is_numeric($icnumfield))
						{
							   $mainstr = $icnumfield;
							   $yr = substr($mainstr, 0,2);
							   $month = substr($mainstr, 2,2);
							   $day = substr($mainstr, 4,2);
							   $currentdate = date("Y-m-d");
							   $dob = "19".$yr.'-'.$month.'-'.$day;
							   $getminimumyear = $this->lobjTestexcelmodel->fngetyear();
							   $minage = $getminimumyear['age'];
							   $larrvalidate= $this->lobjTestexcelmodel->fngetvalidate($minage);                              							   
							   if(strlen($icnumfield) != 12)
							   {
								  $informaticnum ="INFORMAT ICNO";								  
								  $dateofbirth ='';
							   }
							   elseif($dob <= $larrvalidate['validdate'])
							   {
								    if($yr >= 01 && $yr <= 35)
									{
									 $dateofbirth = "20".$yr.'-'.$month.'-'.$day;
									}
									else
									{
									 $dateofbirth = "19".$yr.'-'.$month.'-'.$day;
									}
							   }
							   else
							   {
								   $remarks = 'Not Eligible,';
								   $dateofbirth = "19".$yr.'-'.$month.'-'.$day;
								   //$informaticnum = $icnumfield;
							   }							 
							    $dobfromexcel = strtotime($DOB); 
							    $dobfromicno = strtotime($dateofbirth);
							   $difference = $dobfromexcel - $dobfromicno;
							   // echo $difference;
							   // die();
                               if($DOB != '' && $difference != 0 && $informaticnum !='DUPLICATE IN SYSTEM' && $dateofbirth !='')
                               {
							      $remarks = $remarks.'    '.'ICNO & DOB not matching in excel';
                               }					   
						}					
						else
						{
							$informaticnum = 'NOT NUMBERS';
							$dateofbirth = '';
						}
					}
					else
					{
					  $informaticnum ='EMPTY';
					  $dateofbirth = '';
					}
					
					if($studentnamefield =='')
					{
					  $Student ="EMPTY";
					}
					else
					{
					  $Student = $studentnamefield;
					}	
                    if($genderfield =='')
                    {
					  $Gender ='EMPTY';
                    }					
					elseif($genderfield == "MALE")
					{
					   $Gender ='MALE';
					}
					else if($genderfield == "FEMALE")
					{
					  $Gender = 'FEMALE';
					}
					else
					{
					   $Gender = "INCORRECT";
					}
					$emailresult = preg_match("/^[_a-z0-9-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $emailfield);				
					if($emailresult ==1)
					{
					   $email = $emailfield;
					   
					}
					elseif($emailfield =='')
					{
					  $email ="EMPTY";
					}
					else
					{
					  $email = 'INVALID EMAIL';
					}
					if($racefield =='')
					{
					   $race ="EMPTY";
					}
					else
					{
					   $race = $racefield;
					}
					if($edufield =='')
					{
					   $education ="EMPTY";
					}
					else
					{
					   $education = $edufield;
					}
					if($Address =='')
					{ 
					  $mailingaddrs = "EMPTY";
					}
					else
					{
					 $mailingaddrs = $Address;
					}
					if($Postal =='')
					{ 
					  $Postaladdr = "EMPTY";
					}
					else
					{
					 $Postaladdr = $Postal;
					}					
					$larrinfo = $this->lobjTestexcelmodel->fninsertintotemp($informaticnum,$Student,$Gender,$email,
					$race,$education,$dateofbirth,$mailingaddrs,$CorrAddress,$Postaladdr,$Country,$State,$Contactno,$Mobile,$mailingaddrs,$remarks,$idapp);
				}
		}
	  }
    }			
	$larrapplications = $this->lobjTestexcelmodel->fngetalldetails();			
    if($larrapplications)	
    {
	   $this->view->info =  $larrapplications;
	   $this->view->status =1;
    }	
    else
    {
		       $this->view->status =0;
            }
     for($i=0;$i<count($larrapplications);$i++)	
			{
			  if($larrapplications[$i]['IDApplication'] != 0)
			  {
			    $this->view->displaycheck =1;
				break;
			  }
			}				
		}
		 if ($this->_request->isPost() && $this->_request->getPost('Change'))
		{
		    $iduser=$auth->getIdentity()->iduser;
			
			$larrformData = $this->_request->getPost();
			$CT = $larrformData['Companytakaful'];
			$CTid = $larrformData['Ctlist'];
			//echo "<pre>";
			  //  print_r($larrformData);
			for($i=0;$i<count($larrformData['idapp']);$i++)
			{
			    $icno = $this->lobjTestexcelmodel->fngeticnumber($larrformData['idapp'][$i]);
				/*echo $CT;
				echo "<br>";
			    echo $CTid;
				echo "<br>";
				 echo  $larrformData['idapp'][$i];
				 echo "<br>";
				 echo  $icno['ICNO'];
				 echo "<br>";
				 echo  $icno['pass'];
				 echo "<br>";
				 echo  $iduser;
				 echo "<br>";
			    print_r($icno);die();*/
			    $this->lobjTestexcelmodel->fninsertintostudentchangedetails($CT,$CTid,$larrformData['idapp'][$i],$icno['ICNO'],$icno['pass'],$iduser);
			}
	}
	}
	public function getctlistAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$id = $this->_getParam('idct');
		$larrcompanydetails = $this->lobjTestexcelmodel->fnResetArrayFromValuesToNames($this->lobjTestexcelmodel->fnGetcompanyortakafulList($id));		
		Zend_Json_Encoder::encode($larrcompanydetails);
		echo Zend_Json_Encoder::encode($larrcompanydetails);
	}
}