<?php
//error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class BatchcandidatesController extends Zend_Controller_Action { //Controller for the User Module
public $gsessionbatch;//Global Session Name
private $_gobjlogger;
	public function init() { //initialization function		
		$this->gsessionbatch = Zend_Registry::get('sis'); 		
		if(empty($this->gsessionbatch->idCompany)){ 
			$this->_redirect( $this->baseUrl . '/batchlogin/logout');					
		}
		$this->_helper->layout()->setLayout('/reg/usty1');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		//echo "abc";die();
		$this->lobjCommon = new App_Model_Common();
		$this->lobjCompanyapplicationmodel = new App_Model_Companyapplication(); //user model object
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates(); //user model object		
		$this->lobjBatchcandidatesForm = new App_Form_Batchcandidates (); //intialize user lobjuserForm
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates(); //user model object		
		$this->lobjTakafulcandidatesForm = new App_Form_Takafulcandidates(); //intialize user lobjuserForm
		$this->lobjloadfilesForm = new Examination_Form_Uploadfiles(); 
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	public function indexAction() 
	{					
		$lobjUploadfilesForm = $this->lobjloadfilesForm;//intialize bank form
		$this->view->lobjUploadfilesForm = $lobjUploadfilesForm;
		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$minmumage=new App_Model_Studentapplication();		
		$larr=$minmumage->fngetminimumage();		
	    $age=$larr[0]['MinAge'];
	    $this->view->minages = $age;
		$eligibility = ($year)-($age);		
	    //echo $eligibility;die();
			$year=$eligibility;	
			$this->view->yearss=$year;
			
			$yeste= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));
		//echo $yesterdaydate;die();
		$this->view->yesdate=$yeste;
		
		$larrresult = $this->lobjCompanyapplicationmodel->fngetCompanyDetails($this->gsessionbatch->idCompany);
 		$this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;
  		$this->view->companyDetails =  $larrresult;
  		$ids=$this->_getParam('batchId');
  		$this->view->idbatchss = $ids;
  		
  		$laresultsregpin=$this->lobjBatchcandidatesmodel->fngetBatchDetailsforRegistrationpin($ids);
  		
            $idcomps= $this->gsessionbatch->idCompany;	
  			$companydetails=$this->lobjBatchcandidatesmodel->fngetBatchcompanydetails($idcomps);	
  			$companyaddress=$companydetails['Address'];

  		
  	/*	print_r($laresultsregpin);
  		die();*/
  		$regpin = $laresultsregpin['registrationPin'];
  		$laresultscandidate=$this->lobjBatchcandidatesmodel->fngetBatchRegistration($ids);
  		
  		/*print_r($laresultscandidate);
  		die();*/
  		$noofcandidates =  Array();
  		$noofexams = count($laresultscandidate);
  		for($i=0;$i<$noofexams;$i++)
  		{
  			$noofcandidates['idprgm'][] = $laresultscandidate[$i]['idProgram'];
  			$noofcandidates['ProgramName'][$laresultscandidate[$i]['idProgram']] = $laresultscandidate[$i]['ProgramName'];
  			$noofcandidates[$laresultscandidate[$i]['idProgram']] = $laresultscandidate[$i]['noofCandidates'];
  			$noofcandidatesssss[] = $laresultscandidate[$i]['noofCandidates'];
  			
  		}
  		//echo "<pre/>";
  		$larrbatchprog = $this->lobjBatchcandidatesmodel->fnBatchProg();
  		for($g=0;$g<count($noofcandidates['idprgm']);$g++){
  			$larrbatchprog123[$noofcandidates['idprgm'][$g]] = $this->lobjBatchcandidatesmodel->fnBatchProgram($noofcandidates['idprgm'][$g]);
  			
  		}
  		$this->view->batchresults  = $larrbatchprog123;
  		//print_r($larrbatchprog123);die();
  		
  		
  		
  		
  		
  		//-------------------------
  		$total=0;
  		for($m=0;$m<count($noofcandidatesssss);$m++)
  		{
  			$total = $total + $noofcandidatesssss[$m];
  		}
  		//print_r($total);
  		$this->view->total = $total;
  		$this->view->noofprog = $noofcandidates['idprgm'];
  		$this->view->progname = $noofcandidates['ProgramName'];
  		$this->view->noofcandidates = $noofcandidates;
  		
  		$larrresultprogram = $this->lobjBatchcandidatesmodel->fnGetProgramName();
  		$this->view->programresult = $larrresultprogram;
  		//print_r($larrresultprogram);
  		//die();
  		
  		
  		/*print_r($larresultbatch);
  		die();*/
  		$this->view->batchresult = $larresultbatch;
  		
  		$larrresultrace = $this->lobjBatchcandidatesmodel->fnGetRace();
  		/*print_r($larrresultrace);
  		die();*/
  		$this->view->raceresult = $larrresultrace;

  		$larrresuleducation = $this->lobjBatchcandidatesmodel->fnGetEducation();
  		$this->view->educationresult = $larrresuleducation;

                $larresultreligionoperator = $this->lobjBatchcandidatesmodel->fnGetReligion();
		$this->view->Religion = $larresultreligionoperator;  
  		
  		$larresultTakafuloperator = $this->lobjBatchcandidatesmodel->fnTakafuloperator();
		$this->view->takafuloperator = $larresultTakafuloperator;  	

		
		$larrbatchprog = $this->lobjBatchcandidatesmodel->fnBatchProg();
		
		
		for($k=0;$k<count($larrbatchprog);$k++)
		{
			$programarrrya[$larrbatchprog[$k]['IdProgrammaster']][] = $larrbatchprog[$k]['IdBatch'];
		}

  		if ($this->_request->isPost () && $this->_request->getPost ('Save')){  			
  			//die();
  			
  				$larrformData = $this->_request->getPost ();
  				/*echo "<pre/>";
  				print_r($larrformData);
  				die();*/
  			  $ids = $larrformData['idbatch'];
  			
  			$countloop = count($larrformData['candidatename']);
  			$larrinsertdata = $this->lobjBatchcandidatesmodel->fnInsertIntoStd($larrformData,$countloop,$ids,$regpin,$companyaddress);
  			$auth = Zend_Auth::getInstance();// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Completed the Batchcandidates Registration"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
    			  //print_r($this->gsessionregistration->mails);
  		    //$this->view->message = $larrinsertdata;
  			//print_r($larrinsertdata);
  			$this->_redirect( $this->baseUrl . "/batchcandidates/display/results/$regpin");
  		
  		}
	}
public function reportAction(){
		
		$ids=$this->_getParam('batchId');
  		$this->view->idbatchss = $ids;
  		
  		$resultBatchRegistration = $this->lobjBatchcandidatesmodel->fnAdhocvenue($ids);  		
		$regpin = $resultBatchRegistration['registrationPin'];				
		$larrresult = $this->lobjBatchcandidatesmodel->fngetregisteredstudentsdetails($regpin);		
		
		$this->view->candidate=$larrresult;
		
	}
	
	public function importdisplayAction()
	{
		
			$filename = $this->_getParam('filename');
		     require_once 'Excel/excel_reader2.php';
		
		                       $userDoc = realpath(APPLICATION_PATH . '/../public/uploads/questions/'.$filename);
							    $data = new Spreadsheet_Excel_Reader($userDoc);
							 
				     		    $arr = $data->sheets;
				     		    echo "<pre/>";
				     		    print_r($arr[0]['cells'][2][1]);
				     		    for($i=2;$i<100;$i++)
				     		    {
				     		    	if($arr[0]['cells'][$i][1] == '')
				     		    	{
				     		    		echo "asdf";
				     		    		break;
				     		    	}
						     		    	
				     		    }
				     		    die();
	}
	
	
	public function displayAction()
	{
		$regid= $this->_getParam('results');
	   	$larrresutls =  $this->lobjBatchcandidatesmodel->fngetregisteredstudentsdetails($regid);
		$this->view->message = $larrresutls;
	}
	
	public function adhocAction()
	{
		
		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$minmumage=new App_Model_Studentapplication();
		
				$larr=$minmumage->fngetminimumage();
		
				$age=$larr[0]['MinAge'];
				$eligibility = ($year)-($age);		
				//echo $eligibility;die();
			$year=$eligibility;	
			$this->view->yearss=$year;
			
			$yeste= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));
		//echo $yesterdaydate;die();
		$this->view->yesdate=$yeste;
			
			
		$larrresult = $this->lobjCompanyapplicationmodel->fngetCompanyDetails($this->gsessionbatch->idCompany);
 		$this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;
  		$this->view->companyDetails =  $larrresult;
  		$ids=$this->_getParam('batchId');
  		$this->view->idbatchss = $ids;
  		
  		$laresultsregpin=$this->lobjBatchcandidatesmodel->fngetBatchDetailsforRegistrationpin($ids);
  		/*	print_r($laresultsregpin);
  		die();*/
  		
  		$regpin = $laresultsregpin['registrationPin'];
  		$this->view->regpin = $regpin;
  		$laresultscandidate=$this->lobjBatchcandidatesmodel->fngetBatchRegistration($ids);
  		//print_r($laresultscandidate);
  		//die();
  		$noofcandidates =  Array();
  		$noofexams = count($laresultscandidate);
  		for($i=0;$i<$noofexams;$i++)
  		{
  			$noofcandidates['idprgm'][] = $laresultscandidate[$i]['idProgram'];
  			$noofcandidates['ProgramName'][$laresultscandidate[$i]['idProgram']] = $laresultscandidate[$i]['ProgramName'];
  			$noofcandidates[$laresultscandidate[$i]['idProgram']] = $laresultscandidate[$i]['noofCandidates'];
  			$noofcandidatesssss[] = $laresultscandidate[$i]['noofCandidates'];
  		}
  		//echo "<pre/>";
  		$larrbatchprog = $this->lobjBatchcandidatesmodel->fnBatchProg();
  		for($g=0;$g<count($noofcandidates['idprgm']);$g++){
  			$larrbatchprog123[$noofcandidates['idprgm'][$g]] = $this->lobjBatchcandidatesmodel->fnBatchProgram($noofcandidates['idprgm'][$g]);
  		}
  		$this->view->batchresults  = $larrbatchprog123;
  		//print_r($larrbatchprog123);die();

  		//-------------------------
  		$total=0;
  		for($m=0;$m<count($noofcandidatesssss);$m++)
  		{
  			$total = $total + $noofcandidatesssss[$m];
  		}
  		//print_r($total);
  		$this->view->total = $total;
  		$this->view->noofprog = $noofcandidates['idprgm'];
  		$this->view->progname = $noofcandidates['ProgramName'];
  		$this->view->noofcandidates = $noofcandidates;
  		
  		$larrresultprogram = $this->lobjBatchcandidatesmodel->fnGetProgramName();
  		$this->view->programresult = $larrresultprogram; 		
  		
  		$larresultbatch = $this->lobjBatchcandidatesmodel->fnGetBatchName();
  		/*print_r($larresultbatch);
  		die();*/
  		$this->view->batchresult = $larresultbatch;
  		$larrresultrace = $this->lobjBatchcandidatesmodel->fnGetRace();
  		/*print_r($larrresultrace);
  		die();*/
  		$this->view->raceresult = $larrresultrace;

  		$larrresuleducation = $this->lobjBatchcandidatesmodel->fnGetEducation();
  		$this->view->educationresult = $larrresuleducation;
  		
  		$larresultTakafuloperator = $this->lobjBatchcandidatesmodel->fnTakafuloperator();
		$this->view->takafuloperator = $larresultTakafuloperator;  	
        $resultvenue=$this->lobjBatchcandidatesmodel->fnAdhocvenue($ids);
		$this->view->venues=$resultvenue['AdhocVenue'];
		$this->view->adhocdate=$resultvenue['AdhocDate'];
	
		
		$larrbatchprog = $this->lobjBatchcandidatesmodel->fnBatchProg();
		
		
		for($k=0;$k<count($larrbatchprog);$k++)
		{
			$programarrrya[$larrbatchprog[$k]['IdProgrammaster']][] = $larrbatchprog[$k]['IdBatch'];
		}
		//print_r($programarrrya);
		//die();

  		if ($this->_request->isPost () && $this->_request->getPost ('Save')){  			
  			
  				$larrformData = $this->_request->getPost ();
  				//echo '<pre>';
  				//print_r($larrformData);
  				//die();
  			    $ids = $larrformData['idbatch'];
  			  
  			$regpin =$larrformData['regpin'];
  			$countloop = count($larrformData['candidatename']);
  			$larrinsertdata = $this->lobjBatchcandidatesmodel->fnInsertIntoAdhocStd($larrformData,$countloop,$ids,$regpin);
  				$this->_redirect( $this->baseUrl . "/companyapplication/index/");
  		
  		}
		
	}
		
	public function importAction(){

		$sessionID = Zend_Session::getId();
//			$resulttemp = $this->lobjBatchcandidatesmodel->fnDeletetempdetails($sessionID);
		$ids=$this->_getParam('batchId');		
  		$this->view->idbatchss = $ids;
		$lobjUploadfilesForm = $this->lobjloadfilesForm;//intialize bank form
		$this->view->lobjUploadfilesForm = $lobjUploadfilesForm;
		
		$larrtakafulcount = $this->lobjTakafulcandidatesmodel->fnGetapplicantcount($ids);
				     			
				     			$availseat=$larrtakafulcount['totalNoofCandidates']-$larrtakafulcount['totalcount'];
				     			$this->view->remspplication=$availseat;
			if ($this->_request->isPost () && $this->_request->getPost ('Save')){  			
  			
				$larrformData = $this->_request->getPost(); //getting the values of bank from post 
			//print_r($larrformData);die();
			
			     require_once 'Excel/excel_reader2.php';
	
  					$lintfilecount['Count'] =0;
					$lstruploaddir = "/uploads/questions/";
					$larrformData['FileLocation'] = $lstruploaddir;
					$larrformData['UploadDate'] = date('Y-m-d:H:i:s');
					
						if($_FILES['FileName']['error'] != UPLOAD_ERR_NO_FILE)
							{
								$lintfilecount['Count']++; 
								//$larrext = explode(".", basename($_FILES['uploadedfiles']['name'][$linti]));
								$lstrfilename = pathinfo(basename($_FILES['FileName']['name']), PATHINFO_FILENAME);					
								$lstrext = pathinfo(basename($_FILES['FileName']['name']), PATHINFO_EXTENSION);
								
								$filename = $lintfilecount['Count'].".".date('YmdHis').".".$lstrext;
							    $filename = str_replace(' ','_',$lstrfilename)."_".$filename;				 
								$file = realpath('.').$lstruploaddir . $filename;					
								if (move_uploaded_file($_FILES['FileName']['tmp_name'],$file)) 
								{
									//echo "success";
										$larrformData['FilePath'] = $filename;
										$larrData['FileName'] =  $lstrfilename;
										$larrData['FilePath'] = $filename;
										//$larrData['UpdDate'] =  $larrformData['UpdDate'];
										//$larrData['UpdUser'] =  $larrformData['UpdUser'];
										//$larrData['Active'] = $larrformData['Active'];
									    //$filesData .=	"<tr><td align='center'>".($linti+1)."</td><td>".$filename."</td><td>".$filenames[$linti]['size']."</td></tr>";
								} 
								else 
								{
						    		//echo "error";
								}
								//$larrformData['Name']=$lstrfilename;
								//$section = $larrformData['Section'];
								//$lobjRead =  $this->lobjuploadfilesModel; 
								// $larrresultdetails = $lobjRead->fnDeleteTempDetails();
								/* echo "<pre/>";
								 print_r($larrData);
								 die();
								 */
								//$lintresult = $lobjUploadfilesmodel->fnAddUploadfiles($larrData);						
				     		   /* print_r($totalarray);
				     		    print_r(count($totalarray));
				     		    die();*/
							}
					          require_once 'Excel/excel_reader2.php';
		
		                       $userDoc = realpath(APPLICATION_PATH . '/../public/uploads/questions/'.$filename);
							   $data = new Spreadsheet_Excel_Reader($userDoc);
							 
				     		    $arr = $data->sheets;
				     		    //echo "<pre/>";
				     		    //print_r($arr[0]['cells'][2][1]);
				     		    for($i=2;$i<100;$i++)
				     		    {
				     		    	if($arr[0]['cells'][$i][1] == '')
				     		    	{
				     		    		//echo "asdf";
				     		    		break;
				     		    	}
				     		    	else 
				     		    		       $totalarray[$i] = $arr[0]['cells'][$i];				     		    		
				     		    }
				     		    
				     		    $larrtakafulcount = $this->lobjTakafulcandidatesmodel->fnGetapplicantcount($ids);
				     			
				     			$availseat=$larrtakafulcount['totalNoofCandidates']-$larrtakafulcount['totalcount'];
				     			
				     		    if($availseat < count($totalarray)){
				     		    	$counts=$availseat+2;
				     		    }
				     		    else{
				     		      	 $count = count($totalarray);				     		   
				     		  		 $counts = $count+2;
				     		    }
				     		   
				     		   for($i=2;$i<$counts;$i++)
				     		   {
				     		     ///////////////////////NAme/////////////////////
				     		   	  $name = $arr[0]['cells'][$i][1];				     		   	 
				     		   	 ////////////////////ICNO///////////////////////////////
				     		      $icno = $arr[0]['cells'][$i][2];
				     		      $icnos = "$icno";
				     		     // $icno=new string($icnos);
				     		     // print_r($icno);
				     		    
				     		      $icnolen = strlen($icno);
				     		      if($icnolen !=8)
				     		      {
				     		        	echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
				     		      	    echo "<script>parent.location = '".$this->view->baseUrl()."/batchcandidates/import/batchId/".$ids."';</script>";
                					    die();				     		      
				     		      }
				     		      
				     		      $year = '19'.$icnos[0].$icnos[1];
				     		      $month = $icnos[2].$icnos[3];
				     		      $day = $icnos[4].$icnos[5];
				     		      $dob = $year.'-'.$month.'-'.$day;
				     		      /////////////////ICNO ENDS////////////////////////////////
				     		      /////////////////Email Starts////////////////////////////////
				     		      $email = $arr[0]['cells'][$i][3];
				     		 
				     		   	  $email = $arr[0]['cells'][$i][3];			
								  $larrmailexcel = $this->lobjTakafulcandidatesmodel->fnGetmailId($email);	
				     		   
				     		      $larrmail = $this->lobjBatchcandidatesmodel->fnGetmailId($email);	
				     		      			     		      
				     		      $race = $larrmail['EmailAddress'];
				     		      $mailcount = strlen($larrmail['EmailAddress']);
				     		      if(!$larrmail && !$larrmailexcel)
				     		      {
				     		      	
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Email Already Taken")</script>';
				     		      	echo "<script>parent.location = '".$this->view->baseUrl()."/batchcandidates/import/batchId/".$ids."';</script>";
                					 die();
				     		      }
				     		      /////////////////Email ENDS////////////////////////////////
				     		      ///////////////////////RACE/////////////////////////////////
				     		      $chienesearray = $arr[0]['cells'][$i][4];				     		     
				     		      $larrrace = $this->lobjBatchcandidatesmodel->fnGetRaceId($chienesearray);				     		      
				     		      $race = $larrrace['idDefinition'];
				     		      $racecount = strlen($larrrace['idDefinition']);
				     		      if($racecount>=1)
				     		      {
				     		      	
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Please check the race and upload the file")</script>';
				     		      	echo "<script>parent.location = '".$this->view->baseUrl()."/batchcandidates/import/batchId/".$ids."';</script>";
                					 die();
				     		      }
				     		      ////////////////////////RACE ENDS////////////////////////////////
				     		      ///////////////////////EDUCATION////////////////////////////////
				     		      $chienesearray = $arr[0]['cells'][$i][5];
				     		      $larrrace = $this->lobjBatchcandidatesmodel->fnGetRaceId($chienesearray);
				     		      $education = $larrrace['idDefinition'];
				     		      $educationcount = strlen($larrrace['idDefinition']);
				     		      if($educationcount>=1)
				     		      {
				     		      	
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Please check the Education and upload the file")</script>';
				     		      	echo "<script>parent.location = '".$this->view->baseUrl()."/batchcandidates/import/batchId/".$ids."';</script>";
                					 die();
				     		      }
				     		      ///////////////////////EDU ENDS//////////////////////////////////
				     		      
				     		      //////////////////////////////////GENDER////////////////////////
				     		
				     		      if($arr[0]['cells'][$i][7]== 'Male')
				     		      {
				     		      	$gender =1;
				     		      }
				     		      else if($arr[0]['cells'][$i][7] =='Female')
				     		      {
				     		      	$gender =0;
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Please check the Gender and upload the file")</script>';
				     		      	echo "<script>parent.location = '".$this->view->baseUrl()."/batchcandidates/import/batchId/".$ids."';</script>";
                					 die();
				     		      }
				     		      ///////////////////////////////////GENDER ENDS////////////////
				     		      
				     		      ///////////////////ADDRESS/////////////////////
				     		       $dob = $arr[0]['cells'][$i][6];
				     		       
				     		     /* 
				     		       $pieces = explode("/", $dob);
				     		       //echo $pieces[0];
				     		        
				     		       $Month = substr($pieces[0],0,3);
				     		       $Day = substr($pieces[1],0,3);
				     		       $Year = substr($pieces[2],0,4);
				     		       
				     		     
				     		       $date= substr($pieces[1],0,3).'-'.substr($pieces[0],0,3).'-'.substr($pieces[2],0,4);
				     		       
				     		     
				     		       $xyz = date('Y-m-d',strtotime($date));
				     		       echo $xyz;
				     		       die();
				     		       $MonthNum = date('m',strtotime($Month));
				     		       
				     		       $DayNum = date('d',strtotime($Day));
				     		       
				     		       $dob = date('Y-m-d',strtotime($dob));
				     		       
				     		        echo $DayNum;
				     		       die();
				     		        
				     		      $ConvertedDate= date('Y-m-d', mktime(0,0,0,$MonthNum,$DayNum,$Year));
				     		      echo $ConvertedDate;
				     		       die();
				     		  
				     		      //$dob = date('Y-m-d',strtotime($dob));
				     		     */

									
				     		       $education= $arr[0]['cells'][$i][5];
				     		       $address= $arr[0]['cells'][$i][8];
				     		       //$dob = 
				     		
				     		      $insertarray = $this->lobjTakafulcandidatesmodel->fninsertintotemp($name,$icno,$email,$race,$education,$gender,$address,$dob,$this->gsessionbatch->idCompany,$ids);
				     		      
				     		   }
				     		    
			$this->_redirect( $this->baseUrl . "/batchcandidates/coursevenue/batchId/$ids");
			}
		if ($this->_request->isPost () && $this->_request->getPost ('Schedule')){ 			
			
			$this->_redirect( $this->baseUrl . "/batchcandidates/coursevenue/batchId/$ids");
		}	
		
	}	
	
	
	public function import1Action(){

		$sessionID = Zend_Session::getId();
//			$resulttemp = $this->lobjBatchcandidatesmodel->fnDeletetempdetails($sessionID);
		$ids=$this->_getParam('batchId');
  		$this->view->idbatchss = $ids;
			$lobjUploadfilesForm = $this->lobjloadfilesForm;//intialize bank form
		$this->view->lobjUploadfilesForm = $lobjUploadfilesForm;
			if ($this->_request->isPost () && $this->_request->getPost ('Save')){  			
  			
				$larrformData = $this->_request->getPost(); //getting the values of bank from post 
			//print_r($larrformData);die();
			
			     require_once 'Excel/excel_reader2.php';
	
  					$lintfilecount['Count'] =0;
					$lstruploaddir = "/uploads/questions/";
					$larrformData['FileLocation'] = $lstruploaddir;
					$larrformData['UploadDate'] = date('Y-m-d:H:i:s');
					
						if($_FILES['FileName']['error'] != UPLOAD_ERR_NO_FILE)
							{
								$lintfilecount['Count']++; 
								//$larrext = explode(".", basename($_FILES['uploadedfiles']['name'][$linti]));
								$lstrfilename = pathinfo(basename($_FILES['FileName']['name']), PATHINFO_FILENAME);					
								$lstrext = pathinfo(basename($_FILES['FileName']['name']), PATHINFO_EXTENSION);
								
								$filename = $lintfilecount['Count'].".".date('YmdHis').".".$lstrext;
							    $filename = str_replace(' ','_',$lstrfilename)."_".$filename;				 
								$file = realpath('.').$lstruploaddir . $filename;					
								if (move_uploaded_file($_FILES['FileName']['tmp_name'],$file)) 
								{
									//echo "success";
									
										$larrformData['FilePath'] = $filename;
										$larrData['FileName'] =  $lstrfilename;
										$larrData['FilePath'] = $filename;
										//$larrData['UpdDate'] =  $larrformData['UpdDate'];
										//$larrData['UpdUser'] =  $larrformData['UpdUser'];
										//$larrData['Active'] = $larrformData['Active'];
									//$filesData .=	"<tr><td align='center'>".($linti+1)."</td><td>".$filename."</td><td>".$filenames[$linti]['size']."</td></tr>";
								} 
								else 
								{
						    		//echo "error";
								}
								//$larrformData['Name']=$lstrfilename;
								//$section = $larrformData['Section'];
								//$lobjRead =  $this->lobjuploadfilesModel; 
								// $larrresultdetails = $lobjRead->fnDeleteTempDetails();
								/* echo "<pre/>";
								 print_r($larrData);
								 die();
								 */
								//$lintresult = $lobjUploadfilesmodel->fnAddUploadfiles($larrData);
									
									
							
				     		   /* print_r($totalarray);
				     		    print_r(count($totalarray));
				     		    die();*/
							}
					          require_once 'Excel/excel_reader2.php';
		
		                       $userDoc = realpath(APPLICATION_PATH . '/../public/uploads/questions/'.$filename);
							    $data = new Spreadsheet_Excel_Reader($userDoc);
							 
				     		    $arr = $data->sheets;
				     		   // echo "<pre/>";
				     		    //print_r($arr[0]['cells'][2][1]);
				     		    for($i=2;$i<100;$i++)
				     		    {
				     		    	if($arr[0]['cells'][$i][1] == '')
				     		    	{
				     		    		//echo "asdf";
				     		    		break;
				     		    	}
				     		    	else 
				     		    		       $totalarray[$i] = $arr[0]['cells'][$i];
				     		    		
				     		    }
				     		 // print_r($totalarray);
				     		   $count = count($totalarray);
				     		   $counts = $count+2;
				     		   for($i=2;$i<$counts;$i++)
				     		   {
				     		   	///////////////////////NAme/////////////////////
				     		   	 $name = $arr[0]['cells'][$i][1];
				     		   	 
				     		   	 ////////////////////ICNO///////////////////////////////
				     		      $icno = $arr[0]['cells'][$i][2];
				     		      $icnos = "$icno";
				     		     // $icno=new string($icnos);
				     		     // print_r($icno);
				     		    
				     		      $icnolen = strlen($icno);
				     		      if($icnolen !=12)
				     		      {
				     		      	echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
				     		      	echo "<script>parent.location = '".$this->view->baseUrl()."/companyapplication/index';</script>";
                					 die();
				     		      
				     		      }
				     		      
				     		      $year = '19'.$icnos[0].$icnos[1];
				     		      $month = $icnos[2].$icnos[3];
				     		      $day = $icnos[4].$icnos[5];
				     		      $dob = $year.'-'.$month.'-'.$day;
				     		      /////////////////ICNO ENDS////////////////////////////////
				     		      
				     		      $email = $arr[0]['cells'][$i][3];
				     		      
				     		      ///////////////////////RACE/////////////////////////////////
				     		      $chienesearray = $arr[0]['cells'][$i][4];
				     		      $larrrace = $this->lobjBatchcandidatesmodel->fnGetRaceId($chienesearray);
				     		      $race = $larrrace['idDefinition'];
				     		      $racecount = strlen($larrrace['idDefinition']);
				     		      if($racecount>=1)
				     		      {
				     		      	
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Please check the race and upload the file")</script>';
				     		      	echo "<script>parent.location = '".$this->view->baseUrl()."/companyapplication/index';</script>";
                					 die();
				     		      }
				     		      ////////////////////////RACE ENDS////////////////////////////////
				     		      ///////////////////////EDUCATION////////////////////////////////
				     		      $chienesearray = $arr[0]['cells'][$i][5];
				     		      $larrrace = $this->lobjBatchcandidatesmodel->fnGetRaceId($chienesearray);
				     		      $education = $larrrace['idDefinition'];
				     		      $educationcount = strlen($larrrace['idDefinition']);
				     		      if($educationcount>=1)
				     		      {
				     		      	
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Please check the Education and upload the file")</script>';
				     		      	echo "<script>parent.location = '".$this->view->baseUrl()."/companyapplication/index';</script>";
                					 die();
				     		      }
				     		      ///////////////////////EDU ENDS////////////////////////////////
				     		      
				     		      //////////////////////////////////GENDER////////////////////////
				     		      if($arr[0]['cells'][$i][6]== 'Male')
				     		      {
				     		      	$gender =1;
				     		      }
				     		      else if($arr[0]['cells'][$i][6] =='Female')
				     		      {
				     		      	$gender =0;
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Please check the Gender and upload the file")</script>';
				     		      	echo "<script>parent.location = '".$this->view->baseUrl()."/companyapplication/index';</script>";
                					 die();
				     		      }
				     		      ///////////////////////////////////GENDER ENDS////////////////
				     		      
				     		      ///////////////////ADDRESS/////////////////////
				     		       $address = $arr[0]['cells'][$i][7];
				     		       
				     		       //$dob = 
				     		       
				     		      $insertarray = $this->lobjBatchcandidatesmodel->fninsertintotemp($name,$icno,$email,$race,$education,$gender,$address,$dob);
				     		      
				     		      
				     		   }
				     		    
				$this->_redirect( $this->baseUrl . "/batchcandidates/coursevenue/batchId/$ids");
			}
			
				
		
	}	
	
	public function fngetschedulerdetailsAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidbatch = $this->_getParam('idbatch');
		
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnGetscheduler($lintidbatch);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	
public function fngetvenueAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidscheduler = $this->_getParam('idscheduler');
		
		
		$larrbatchresult = $this->lobjBatchcandidatesmodel->fnGetVenueName($lintidscheduler);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrbatchresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
public function fngetvenuetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidvenue = $this->_getParam('idvenue');
		
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnGetVenueTime($lintidvenue);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	public function fngetdatetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('date');
		$lintidvenue = $this->_getParam('idvenue');

		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnGetTimingsForDate($lintdate,$lintidvenue);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
public function fngetstatenameAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
		$idyear = $this->_getParam('idyear');

		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnGetStatelistforcourse($Program,$idyear);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}
public function fngetemaildetailsAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
		$idyear = $this->_getParam('Year');
		
		$linticno = $this->_getParam('ICNO');
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fngetstudentsemail($Program,$idyear);
		
	if(count($larrvenuetimeresult)>0)
		{ 
			foreach($larrvenuetimeresult as $batches)
			{
			
				
			if($linticno != $batches[ICNO])
			{
			
			    echo 'We observe that the email id provided already exists, please login to the portal if you have already registered. If you have not registered earlier, please provide with another email id';
			    break;
		 
			}	
			
			}
			
		
		}
		
}





public function fngetstudentconfirmAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
		$icno = $this->_getParam('icno');
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnstudentconfirm($Program,$icno);
        $pass = $larrvenuetimeresult['pass'];
		if($pass==3)
		{
			echo '1'.'***'.'You have already applied for the exam';
		}
		else if($pass==1)
		{
			echo '1'.'***'.'You have already Passed the exam';
		}
		else 
		{
			echo '0'.'***';
		}
}
public function fngetcitynamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('idstate');
		$Program = $this->_getParam('Program');

		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetCitylistforcourse($lintdate,$Program);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}

public function fngetmonthslistAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$idprog = $this->_getParam('Program');
		$year = $this->_getParam('idyear');
$curyear=date('Y');
		$larrvenuetimeresults = $this->lobjBatchcandidatesmodel->fnGetMonths($year,$idprog);
		$frommonth = $larrvenuetimeresults[0]['From'];
		$tomonth = $larrvenuetimeresults[0]['To'];
		$years=$larrvenuetimeresults[0]['Year'];
		if($curyear==$years)
		{
			$curmonth=date('m');
			
			if($frommonth<=$curmonth)
			{
				//echo $frommonth;die();
			$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween2($tomonth);
			}
			else 
			{
				$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween($frommonth,$tomonth);
			}
			}
		else 
		{
		$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween($frommonth,$tomonth);
		}
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}
	public function fngetdateAction()
	{
		     $this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
	
			//Get Country Id
			$idprog = $this->_getParam('Program');
			$year = $this->_getParam('idyear');
	
			$larrvenuetimeresults = $this->lobjBatchcandidatesmodel->fnGetMonths($year,$idprog);
				$frommonth = $larrvenuetimeresults[0]['From'];
			$tomonth = $larrvenuetimeresults[0]['To'];
			
			$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween($frommonth,$tomonth);
			
			$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresults);
			echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
	}
	
public function fngetvenuedetailsAction()
	{
		$this->lobjstudentmodel = new App_Model_Studentapplication(); 
		     $this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
	
			//Get Country Id
			$Year= $this->_getParam('Year');
			$Program = $this->_getParam('Program');
			$idcity = $this->_getParam('idcity');
	
			$venueselect = $this->lobjBatchcandidatesmodel->fnGetVenuedetails($Year,$Program,$idcity);
			
			$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($venueselect);
			echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
	}
	
	
	public function coursevenueAction()
	{
		
	   	$sessionID = Zend_Session::getId();
	   	$this->view->lobjTakafulcandidatesForm = $this->lobjTakafulcandidatesForm;
	   		
			//$resulttemp = $this->lobjBatchcandidatesmodel->fnDeletetempbatchcandidates($sessionID);
			/*print_r($resulttemp);
			die();*/
			
		$larrresult = $this->lobjTakafulcandidatesmodel->fngetTakafulDetails($this->gsessionbatch->idCompany);
		
		
 		$this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;
  		$this->view->companyDetails =  $larrresult;
  		//batchId/
  		$ids=$this->_getParam('batchId');
  	   
  	
  		$this->view->idbatchss = $ids;
  		$laresultscandidate=$this->lobjTakafulcandidatesmodel->fngetBatchRegistrationimport($ids);
  		 		
  		$noofcandidates =  Array();
  		$noofexams = count($laresultscandidate);
  		
  		for($i=0;$i<$noofexams;$i++)  		
  		{
  			$noofcandidates['idprgm'][] = $laresultscandidate[$i]['idProgram'];
  			$noofcandidates['ProgramName'][$laresultscandidate[$i]['idProgram']] = $laresultscandidate[$i]['ProgramName'];
  			$noofcandidates[$laresultscandidate[$i]['idProgram']] = $laresultscandidate[$i]['noofCandidates'];
  			$noofcandidatesssss[] = $laresultscandidate[$i]['noofCandidates'];
  			  			
  		}
  		
  		/*$larrbatchprog = $this->lobjTakafulcandidatesmodel->fnBatchProg();
  		for($g=0;$g<count($noofcandidates['idprgm']);$g++){
  			$larrbatchprog123[$noofcandidates['idprgm'][$g]] = $this->lobjTakafulcandidatesmodel->fnBatchProgram($noofcandidates['idprgm'][$g]);
  		}
  		echo "<pre/>";
  		print_r($larrbatchprog);
  		exit;
  		
  		$this->view->batchresults  = $larrbatchprog123;*/
  		
  		
  		$total=0;
  		for($m=0;$m<count($noofcandidatesssss);$m++)
  		{
  			$total = $total + $noofcandidatesssss[$m];
  		}
  		//print_r($total);
  		$this->view->total = $total;
  		$this->view->noofprog = $noofcandidates['idprgm'];
  		
  		$this->view->progname = $noofcandidates['ProgramName'];
  		$this->view->noofcandidates = $noofcandidates;
  		//$larrtotalcandidats = $this->lobjBatchcandidatesmodel->fngetnoofstudents($ids);
  		
  		$larrtempexcelcandidates = $this->lobjTakafulcandidatesmodel->fngetnoofstudentsfromexcel($ids,$this->gsessionbatch->idCompany);
  		$this->view->takcandiddetails = $larrtempexcelcandidates;  		
  		$this->view->totalstudents = $laresultscandidate['totalNoofCandidates'];  		
  		$this->view->totalexcelstudents = count($larrtempexcelcandidates);  		
  		//die();
  		$this->view->countparts = count($laresultscandidate);  		
  		$this->view->programresult = $laresultscandidate;
  	
  		
  		$larrresultprogram = $this->lobjTakafulcandidatesmodel->fnGetProgramName();
  		$this->view->programresult = $larrresultprogram;
  		
  		
  		$larrexcelappliedcandidates = $this->lobjTakafulcandidatesmodel->fngetexcelappliedcandidates($ids,$this->gsessionbatch->idCompany);
  		$this->view->larrappliedresult = $larrexcelappliedcandidates;
  		
  		
			//////////////////////////////////
				if ($this->_request->isPost () && $this->_request->getPost ('Save')){  	
						$larrformData = $this->_request->getPost ();
					 				
  				    // $larrresutls =  $this->lobjBatchcandidatesmodel->fninserttempdetails($larrformData);
  				  	//$larrbatchdetails = $this->lobjBatchcandidatesmodel->fngetBatchRegistrationdetails($larrformData['idbatch']);
  				  	//$larrbatchregpin = $this->lobjBatchcandidatesmodel->fngetBatchRegistrationPinforexcel($larrformData['idbatch']);
  				  	
  				  	$larrbatchregID = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationPinforexcel($larrformData['idbatch']);
  			 	
  				  	$lintidbatch  = $larrbatchregID['idBatchRegistration'];
  				  	$linttotnumofapplicant = $larrbatchregID['totalNoofCandidates'];
  				  	//die();
  				   /*
  				  	for($j=0;$j<count($larrbatchdetails);$j++)
  				  	{
  				  		$updatestudentresults = $this->lobjBatchcandidatesmodel->fnupdateexcelstudents($larrbatchdetails[$j]['noofCandidates'],$larrbatchdetails[$j]['idProgram']);
  				  	}
  				  	$sessionID = Zend_Session::getId();
  				  	$larresultcandidates = $this->lobjBatchcandidatesmodel->fnfetchallcandidates($sessionID);
  				  	//echo "<pre/>";*/
  				   /// print_r($larresultcandidates);
  				  	
  				  		$larrinsertstudent = $this->lobjTakafulcandidatesmodel->fnInsertintostudapplicationexcel($larrformData,$lintidbatch,$linttotnumofapplicant);
  				  		
						$this->_redirect( $this->baseUrl . "/batchcandidates/coursevenue/batchId/$ids");
				}
		
							 
		
	}
	
	public function coursevenue1Action()
	   {
		
	   		$sessionID = Zend_Session::getId();
			$resulttemp = $this->lobjBatchcandidatesmodel->fnDeletetempbatchcandidates($sessionID);
			/*print_r($resulttemp);
			die();*/
			$filename = $this->_getParam('filename');
		   /* ////////////////////////////////////////
  			require_once 'Excel/excel_reader2.php';
		
		                       $userDoc = realpath(APPLICATION_PATH . '/../public/uploads/questions/'.$filename);
							    $data = new Spreadsheet_Excel_Reader($userDoc);
							 
				     		    $arr = $data->sheets;
				     		    echo "<pre/>";
				     		    //print_r($arr[0]['cells'][2][1]);
				     		    for($i=2;$i<100;$i++)
				     		    {
				     		    	if($arr[0]['cells'][$i][1] == '')
				     		    	{
				     		    		//echo "asdf";
				     		    		break;
				     		    	}
				     		    	else 
				     		    		       $totalarray[$i] = $arr[0]['cells'][$i];
				     		    		
				     		    }
				     		 // print_r($totalarray);
				     		   $count = count($totalarray);
				     		   $counts = $count+2;
				     		   for($i=2;$i<$counts;$i++)
				     		   {
				     		   	///////////////////////NAme/////////////////////
				     		   	 $name = $arr[0]['cells'][$i][1];
				     		   	 
				     		   	 ////////////////////ICNO///////////////////////////////
				     		      $icno = $arr[0]['cells'][$i][2];
				     		      $icnolen = strlen($icno);
				     		      if($icnolen !=12)
				     		      {
				     		      	echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
				     		      	exit;
				     		      }
				     		      /////////////////ICNO ENDS////////////////////////////////
				     		      
				     		      $email = $arr[0]['cells'][$i][3];
				     		      
				     		      ///////////////////////RACE/////////////////////////////////
				     		      $chienesearray = $arr[0]['cells'][$i][4];
				     		      $larrrace = $this->lobjBatchcandidatesmodel->fnGetRaceId($chienesearray);
				     		      $race = $larrrace['idDefinition'];
				     		      $racecount = strlen($larrrace['idDefinition']);
				     		      if($racecount>=1)
				     		      {
				     		      	
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Please check the race and upload the file")</script>';
				     		      	exit;
				     		      }
				     		      ////////////////////////RACE ENDS////////////////////////////////
				     		      ///////////////////////EDUCATION////////////////////////////////
				     		      $chienesearray = $arr[0]['cells'][$i][5];
				     		      $larrrace = $this->lobjBatchcandidatesmodel->fnGetRaceId($chienesearray);
				     		      $education = $larrrace['idDefinition'];
				     		      $educationcount = strlen($larrrace['idDefinition']);
				     		      if($educationcount>=1)
				     		      {
				     		      	
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Please check the Education and upload the file")</script>';
				     		      	exit;
				     		      }
				     		      ///////////////////////EDU ENDS////////////////////////////////
				     		      
				     		      //////////////////////////////////GENDER////////////////////////
				     		      if($arr[0]['cells'][$i][6]== 'Male')
				     		      {
				     		      	$gender =1;
				     		      }
				     		      else if($arr[0]['cells'][$i][6] =='Female')
				     		      {
				     		      	$gender =0;
				     		      }
				     		      else 
				     		      {
				     		      	echo '<script language="javascript">alert("Please check the Gender and upload the file")</script>';
				     		      	exit;
				     		      }
				     		      ///////////////////////////////////GENDER ENDS////////////////
				     		      
				     		      ///////////////////ADDRESS/////////////////////
				     		       $address = $arr[0]['cells'][$i][7];
				     		       
				     		       //$dob = 
				     		       
				     		    //  $insertarray = $this->lobjBatchcandidatesmodel->fninsertintotemp($name,$icno,$email,$race,$education,$gender,$address);
				     		      
				     		      
				     		   }
				     		    
				     		   
			$this->view->studentarray = $totalarray;
			
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		*/
		$larrresult = $this->lobjCompanyapplicationmodel->fngetCompanyDetails($this->gsessionbatch->idCompany);
 		$this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;
  		$this->view->companyDetails =  $larrresult;
  		//batchId/
  		$ids=$this->_getParam('batchId');
  		
  		//$this->view->idbatchss = $ids;
  		
  		$this->view->idbatchss = $ids;
  		$laresultscandidate=$this->lobjBatchcandidatesmodel->fngetBatchRegistrationimport($ids);
  		$larrtotalcandidats = $this->lobjBatchcandidatesmodel->fngetnoofstudents($ids);
  		$larrtempexcelcandidates = $this->lobjBatchcandidatesmodel->fngetnoofstudentsfromexcel($sessionID);
  		//echo "<pre/>";
  		//print_r($larrtempexcelcandidates);
  		$this->view->totalstudents = $larrtotalcandidats['totalNoofCandidates'];
  		$this->view->totalexcelstudents = count($larrtempexcelcandidates);
  		//die();
  		$this->view->countparts = count($laresultscandidate);
  		
  		$this->view->programresult = $laresultscandidate;
  	
  	
  		
			//////////////////////////////////
				if ($this->_request->isPost () && $this->_request->getPost ('Save')){  	
						$larrformData = $this->_request->getPost ();
  				/*echo "<pre/>";
  				print_R($larrformData);
  				die();*/
  				  	$larrresutls =  $this->lobjBatchcandidatesmodel->fninserttempdetails($larrformData);
  				  	
  				  	$larrbatchdetails = $this->lobjBatchcandidatesmodel->fngetBatchRegistrationdetails($larrformData['idbatch']);
  				  	$larrbatchregpin = $this->lobjBatchcandidatesmodel->fngetBatchRegistrationPinforexcel($larrformData['idbatch']);
  				  	$regpin  = $larrbatchregpin['registrationPin'];
  				  	//die();
  				 
  				  	for($j=0;$j<count($larrbatchdetails);$j++)
  				  	{
  				  		$updatestudentresults = $this->lobjBatchcandidatesmodel->fnupdateexcelstudents($larrbatchdetails[$j]['noofCandidates'],$larrbatchdetails[$j]['idProgram']);
  				  	}
  				  	$sessionID = Zend_Session::getId();
  				  	$larresultcandidates = $this->lobjBatchcandidatesmodel->fnfetchallcandidates($sessionID);
  				  	//echo "<pre/>";
  				  ///	print_r($larresultcandidates);
  				  	
  				  	$larrinsertstudent = $this->lobjBatchcandidatesmodel->fninsertstudent($larresultcandidates,$regpin);
  				  	
  				  		
						$this->_redirect( $this->baseUrl . "/batchlogin/login");
				}
		
							 
		
	}
	
	
public function fngetvenuesessiondetailsAction()
	{
		$this->lobjstudentmodel = new App_Model_Studentapplication(); 
		     $this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
	
			//Get Country Id
			$Year= $this->_getParam('Year');
			$Program = $this->_getParam('Program');
			$idcity = $this->_getParam('idcity');
			$venue = $this->_getParam('venue');
			$day = $this->_getParam('day');
			$month = $this->_getParam('month');
	
		$idsechduler=$this->lobjBatchcandidatesmodel->fnGetVenuedetailsgetsecid($Year);
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjBatchcandidatesmodel->fnGetVenuedetailsRemainingseats($idsechduler['Year'],$idsechduler['idnewscheduler'],$idcity,$month,$day);
		$flag=0;
		$idsession=0;
		$venueid=0;
		//$idsession="";
		foreach($venueselect as $ven)
		{
			if($ven['rem']>0)
			{
				$flag=1;
			}
			if($ven['rem']==0 && ($ven['idcenter']==$venue))
			{
				//$idsession=$ven['idmangesession'];
				 $idsession=$idsession.','.$ven['idmangesession'];
				// $idsessions=$ven['idmangesession'];
			}

			if($ven['rem']<0 && ($ven['idcenter']==$venue))
			{	
				$idsession=$idsession.','.$ven['idmangesession'];
			} 
			
			
		}
	//	echo $idsession;die();
			$venueselect = $this->lobjBatchcandidatesmodel->fnGetsesssiondetails($Year,$Program,$idcity,$venue,$idsession,$day,$month);
			
			$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($venueselect);
			echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
	}
	
public function fngetactivesetAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjBatchcandidatesmodel->fnGetActiveSet($Program);
		//print_r($larrvenuetimeresults);die();
		echo $larrvenuetimeresults[0]['IdBatch'];
		//$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}
	

public function caleshowAction()
{
		$this->lobjstudentmodel = new App_Model_Studentapplication(); 
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$idmonth = $this->_getParam('idmonth');
		$no = $this->_getParam('no');
		$year = $this->_getParam('Year');
		$Program = $this->_getParam('Program');
		
		$larrdaysarresult = $this->lobjstudentmodel->fnGetDaysforcourse($Program,$year);
		//echo "<pre/>";
		//print_R($larrdaysarresult);
		for($i=0;$i<count($larrdaysarresult);$i++)
		{
			$days[$i]=$larrdaysarresult[$i]['Days'];
		}
		//print_R($days);
		//die();
	
		$larrmonthresult = $this->lobjstudentmodel->fnGetMonths($year,$Program);		
		$frommonth = $larrmonthresult[0]['From'];
		$tomonth = $larrmonthresult[0]['To'];
		$yearss = $larrmonthresult[0]['Year'];
		//print_r($frommonth);
		//print_r($tomonth);
		//die();
		$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<7;$j++)
		{
			if($days[$j]==1)
			  $monday=1;
			 if($days[$j]==2)
			  $tuesday=1;
			 if($days[$j]==3)
			  $wednesday=1;
			  if($days[$j]==4)
			  $thursday=1;
			  if($days[$j]==5)
			  $friday=1;
			  if($days[$j]==6)
			  $saturday=1;
			  if($days[$j]==7)
			  $sunday=1;
		}
		/*$monday =empty($larrdaysarresult[0]['Days'])? '0':'1';
		$tuesday = empty($larrdaysarresult[1]['Days'])?'0':'1';
		$wednesday = empty($larrdaysarresult[2]['Days'])?'0':'1';
		$thursday = empty($larrdaysarresult[3]['Days'])?'0':'1';
		$friday = empty($larrdaysarresult[4]['Days'])?'0':'1';
		$saturday = empty($larrdaysarresult[5]['Days'])?'0':'1';
		$sunday = empty($larrdaysarresult[6]['Days'])?'0':'1';*/
		
/*		
		
                                  $monday = $this->monday;
                                  $tuesday = $this->tuesday;
                                  $wednesday = $this->wednesday;
                                  $thursday = $this->thursday;
                                  $friday = $this->friday;
                                  $saturday = $this->saturday;
                                   $sunday = $this->sunday;
                                   
                    */   
	$curmonth = date('m');     
	$monat=date('n');
	$jahr=$yearss;
	$heute=date('d');
	$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
	echo '<table border=0  width=30% align=center>';
	echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
	$cnt=0;
	for($reihe=1;$reihe<=3;$reihe++)
	{
	echo '<tr>';
	for ($spalte=1;$spalte<=4;$spalte++)
	{
		$cnt++;
		//print_r($cnt);  
	if($idmonth==$cnt)
	{
		
	
		if($idmonth==$curmonth)
		{
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				
				$curdate = date('d');
				if($i<$curdate)
				{
					if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
				//if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				else {
				
				 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
		else
		 {
			$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
		         $rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				if($cnt==4)
				{
						//$curdate = date('d')+14;
						$i=1;
						while($i<=$insgesamt)
						{
						$rest=($i+$erster-1)%7;
						if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
						else{echo '<td  align=center ';}
						//$curdate = date('d')+14;
						if($i<12)
						{
							 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{}
							echo "</td>\n";
						}
								
						else 
						
						{
						if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
						else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else{echo $i;}
						echo "</td>\n";
						}
						if($rest==0){echo "</tr>\n<tr>\n";}
						$i++;
						}
						
				}
				else 
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.','.$no.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
      ///////
     }
    
	}
	echo '</tr>';
	}
	echo '</table>';
	}
	
	public function tempdaysAction()
	{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day = $this->_getParam('day');
		$year = $this->_getParam('year');
		$month = $this->_getParam('month');
		$city = $this->_getParam('city');
		$dateid = $day.''.$month;
		
		
		$idsechduler=$this->lobjBatchcandidatesmodel->fnGetVenuedetailsgetsecid($year);
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjBatchcandidatesmodel->fnGetVenuedetailsRemainingseats($idsechduler['Year'],$idsechduler['idnewscheduler'],$city,$month,$day);
		$flag=0;
		foreach($venueselect as $ven)
		{
			if($ven['rem']>0)
			{
				$flag=1;
			}
			
		}
		echo $flag;die();
		//print_r($venueselect);die();
     /*  $larrresultinserted = $this->lobjstudentmodel->fngetdetails();
       
		$id = $larrresultinserted[0]['dateid'];
		
		
		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetTempDays($day,$year,$month,$dateid);
		$lintidstudenttempday = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studenttempday','idstudenttempday');
		
		$larrstudenttempdetials = $this->lobjstudentmodel->fngetstudenttempdays($lintidstudenttempday);
		
		
		
		 $larrdeleteddata= $this->lobjstudentmodel->fnDeleteTempDetails($larrstudenttempdetials[0]['dateid']);
		
		echo $id;*/
}

public function fngetremainingseatsAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day = $this->_getParam('day');
		$year = $this->_getParam('Year');
		$month = $this->_getParam('month');
		$city = $this->_getParam('idcity');
		$venue = $this->_getParam('venue');
		$sessionsids = $this->_getParam('sessionsids');
		
		$idsechduler=$this->lobjBatchcandidatesmodel->fnGetVenuedetailsgetsecid($year);
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjBatchcandidatesmodel->fnGetVenuedetailsRemainingseats($idsechduler['Year'],$idsechduler['idnewscheduler'],$city,$month,$day);
		$rem="";
		foreach($venueselect as $ven)
		{
			if ($ven['rem']>0)
			{
			if($ven['idcenter']==$venue && $ven['idmangesession']==$sessionsids )
			{
			$rem=$ven['rem'];	
			}
			}
		}
		echo $rem;die();
		//print_r($venueselect);die();
     /*  $larrresultinserted = $this->lobjstudentmodel->fngetdetails();
       
		$id = $larrresultinserted[0]['dateid'];
		
		
		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetTempDays($day,$year,$month,$dateid);
		$lintidstudenttempday = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_studenttempday','idstudenttempday');
		
		$larrstudenttempdetials = $this->lobjstudentmodel->fngetstudenttempdays($lintidstudenttempday);
		
		
		
		 $larrdeleteddata= $this->lobjstudentmodel->fnDeleteTempDetails($larrstudenttempdetials[0]['dateid']);
		
		echo $id;*/
}

public function caleshowimportAction()
{
		$this->lobjstudentmodel = new App_Model_Studentapplication(); 
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$idmonth = $this->_getParam('idmonth');
		$no = $this->_getParam('no');
		$year = $this->_getParam('Year');
		$Program = $this->_getParam('Program');
		
		$larrdaysarresult = $this->lobjstudentmodel->fnGetDaysforcourse($Program,$year);
		//echo "<pre/>";
		//print_R($larrdaysarresult);
		for($i=0;$i<count($larrdaysarresult);$i++)
		{
			$days[$i]=$larrdaysarresult[$i]['Days'];
		}
		//print_R($days);
		//die();
	
		$larrmonthresult = $this->lobjstudentmodel->fnGetMonths($year,$Program);		
		$frommonth = $larrmonthresult[0]['From'];
		$tomonth = $larrmonthresult[0]['To'];
		//print_r($larrmonthresult);
		//print_r($tomonth);
		//die();
		$yearss = $larrmonthresult[0]['Year'];
		$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<7;$j++)
		{
			if($days[$j]==1)
			  $monday=1;
			 if($days[$j]==2)
			  $tuesday=1;
			 if($days[$j]==3)
			  $wednesday=1;
			  if($days[$j]==4)
			  $thursday=1;
			  if($days[$j]==5)
			  $friday=1;
			  if($days[$j]==6)
			  $saturday=1;
			  if($days[$j]==7)
			  $sunday=1;
		}
		/*$monday =empty($larrdaysarresult[0]['Days'])? '0':'1';
		$tuesday = empty($larrdaysarresult[1]['Days'])?'0':'1';
		$wednesday = empty($larrdaysarresult[2]['Days'])?'0':'1';
		$thursday = empty($larrdaysarresult[3]['Days'])?'0':'1';
		$friday = empty($larrdaysarresult[4]['Days'])?'0':'1';
		$saturday = empty($larrdaysarresult[5]['Days'])?'0':'1';
		$sunday = empty($larrdaysarresult[6]['Days'])?'0':'1';*/
		
/*		
		
                                  $monday = $this->monday;
                                  $tuesday = $this->tuesday;
                                  $wednesday = $this->wednesday;
                                  $thursday = $this->thursday;
                                  $friday = $this->friday;
                                  $saturday = $this->saturday;
                                   $sunday = $this->sunday;
                                   
                    */        
$monat=date('n');
$jahr=$yearss;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0 width=20>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{
	$cnt++;
	if($idmonth==$cnt)
	{
		
	
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="50px" valign=top>';
				echo '<table border=1 align=center style="font-size:8pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#ffffff;font-size:14px;background-color: #808080"><div align="center"> '.$monate[$this_month-1].' '.$yearss.'</div></th>';
				echo '<tr><td style="COLOR:#ffffff;font-size:11px;background-color: #808080"><b>Mo</b></td>';
				echo '<td style="COLOR:#ffffff;font-size:11px;background-color: #808080"><b>Tu</b></td>';
				echo '<td style="COLOR:#ffffff;font-size:11px;background-color: #808080"><b>We</b></td>';
				echo '<td style="COLOR:#ffffff;font-size:11px;background-color: #808080"><b>Th</b></td>';
				echo '<td style="COLOR:#ffffff;font-size:11px;background-color: #808080"><b>Fr</b></td>';
				echo '<td style="COLOR:#ffffff;font-size:11px;background-color: #808080"><b>Sa</b></td>';
				echo '<td style="COLOR:#ffffff;font-size:11px;background-color: #808080"><b>Su</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:8pt; font-family:Verdana; " align=center>';}
				else{echo '<td style="font-size:10pt; font-family:Verdana" align=center>';}
				
				$curdate = date('d');
				if($i<$curdate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
				//if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				else {
				
				 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'<span id="'.$i.''.$this_month.'" style=" border:1px solid #008E00;background: #FEFFBF" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style=" border:1px solid #008E00;background: #FEFFBF" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo '<span id="'.$i.''.$this_month.'" style=" border:1px solid #008E00;background: #FEFFBF" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo '<span id="'.$i.''.$this_month.'" style=" border:1px solid #008E00;background: #FEFFBF" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style=" border:1px solid #008E00;background: #FEFFBF" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style=" border:1px solid #008E00;background: #FEFFBF" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style=" border:1px solid #008E00;background: #FEFFBF" onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
      ///////
     }

	}
	echo '</tr>';
	}
	echo '</table>';
}

public function downloadfileAction(){
		//disable layout
		$this->_helper->layout->disableLayout(); 
  		
		//disable view
		$this->_helper->viewRenderer->setNoRender(true);
		
		//Get File Name To Download
		$lstrfilename = $this->_getParam('lvaredit');
		
		
		//get the filename into an array
		$larrfilename = explode('.', $lstrfilename);
		/*print_r($larrfilename);
		die();*/
		
		$lstrfilepath = realpath('.')."/uploads/book/abcbatchuploads";
		
		header("Content-type: application/octet-stream;charset=utf-8;encoding=utf-8");//MIME type
		//0,1 filename,2 extension
		header("Content-Disposition: attachment; filename=abcbatchuploads.xls");
		readfile($lstrfilepath);
  	}

  	
  	
	public function pdfexportAction()
	{
		//Exporting data to an excel sheet 

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) {
				
			$larrformData = $this->_request->getPost ();	
			
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
				
		$time = date('h:i:s',time());
		$filename = 'Company_Registration_Report_'.$frmdate;
		$ReportName = $this->view->translate( "Company" ).' '.$this->view->translate( "Registration" ).' '.$this->view->translate( "Report" );
		//$companyname= 
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
		}
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 6><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 9><b> {$ReportName}</b></td>	
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b></b></th>
							<th><b>Student Name</b></th>
							<th><b>ICNO</b></th>
							<th><b>E-Mail</b></th>
							<th><b>Registration Id</b></th>
							<th><b>Program Applied</b></th>
							<th><b>Venue Name</b></th>
							<th><b>Exam Date</b></th>
							<th><b>Exam Status</b></th>						
						</tr>';     
	 
		if (count($larrformData)){
			 $cnt = 0; 
		
			 
      	for($expdata=0;$expdata<count($larrformData['FName']);$expdata++){
      		  	
			$tabledata.= ' <tr>
				   		   <td><b>'; 
      		      $tabledata.= '</b></td>
					       <td>'.$larrformData['FName'][$expdata].'</td> 
						   <td>'.$larrformData['ICNO'][$expdata].'</td> 
						   <td>'.$larrformData['EmailAddress'][$expdata].'</td> 
						   <td>'.$larrformData['Regid'][$expdata].'</td> 
						   <td>'.$larrformData['ProgramName'][$expdata].'</td> 
						   <td>'.$larrformData['centername'][$expdata].'</td> 		  
						   <td>'.$larrformData['dateofexam'][$expdata].'</td> 
						   <td>'.$larrformData['status'][$expdata].'</td> 		   
				        </tr> ';				   	
	    	$cnt++; 
      		  }
	     
		 }		
	
		 $tabledata.= '<tr>		      
		     	<td></td>
		     	<td></td>
		     	<td></td>
		     </tr>
		     </table>';	
		
		//echo $tabledata;exit;
		if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}			
				
		}
		

				
	}


}