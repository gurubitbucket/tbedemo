<?php
 error_reporting (E_ALL ^ E_WARNING);
error_reporting ( E_ALL ^ E_NOTICE );
class RegistrationfailedController extends Zend_Controller_Action { //Controller for the User Module
	
	//private $_gobjlogger;
	
	public function init() { //initialization function				
		$this->gsessionidtakafuloperator = Zend_Registry::get ( 'sis' );
		
		if (empty ( $this->gsessionidtakafuloperator->idtakafuloperator )) {
			$this->_redirect ( $this->baseUrl . '/takafullogin/logout' );
		}
		
		$this->_helper->layout ()->setLayout ( '/takaful/usty1' );
		$this->view->translate = Zend_Registry::get ( 'Zend_Translate' );
		Zend_Form::setDefaultTranslator ( $this->view->translate );
		//$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		
		$this->lobjTakafulmodel = new App_Model_Takafulapplication();
		$this->lobjCommon = new App_Model_Common ();		
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //user model object			
		$this->lobjTakafulForm = new App_Form_Takafulapplication (); //intialize user lobjuserForm			
		$this->lobjstudentForm = new App_Form_Studentapplication ();
		$this->lobjstudentmodel = new App_Model_Studentapplication ();
		$this->lobjCompanypayment = new Finance_Model_DbTable_Approvecreditstudenttakaful ();			
		$this->registry = Zend_Registry::getInstance ();
		$this->locale = $this->registry->get ( 'Zend_Locale' );
	
	}
		
	
	public function indexAction() {// Action for Search
	
		$this->view->lobjstudentForm = $this->lobjstudentForm;		
		$this->view->lobjTakafulForm = $this->lobjTakafulForm; //intialize user lobjuserForm
			
		$larrbatchresult = $this->lobjTakafulmodel->fnGetProgramName ();
		$this->view->lobjTakafulForm->idPrograms->addMultiOptions ( $larrbatchresult );
		$this->view->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		$this->view->lobjstudentForm->Program->setAttrib('style','width:165px;');  
		
		$this->view->idTakaful=$this->gsessionidtakafuloperator->idtakafuloperator;
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->takafulpaginator);			
			$lintpagecount = $this->gintPageCount;		
			$lintpage = $this->_getParam('page',1); //Paginator instance
        		
		$larrresultdiscount = $this->lobjTakafulmodel->fngetintialdiscount ();
		$this->view->discount = $larrresultdiscount ['Discount'];		
		///////////////////////////for pay later/////////////////////////
				$larrpaylater = $this->lobjTakafulmodel->fngetpaylater ( $this->gsessionidtakafuloperator->idtakafuloperator );		
				$cntpaylater = count ( $larrpaylater );
				if ($cntpaylater > 1) {
					$this->view->lobjTakafulForm->ModeofPayment->addMultiOption ( $larrpaylater ['idDefinition'], $larrpaylater ['DefinitionDesc'] );
				}
				$this->view->lobjTakafulForm->ModeofPayment->setValue ( 181 );
		
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost ();
	       $larrformData['Servicetax'] = number_format($larrformData['Servicetax'], 2);
			if ($this->view->lobjTakafulForm->isValid ( $larrformData )) {	
				
							$larrformData ['UpdUser'] = $this->gsessionidtakafuloperator->idtakafuloperator;
							$larrformData ['UpdDate'] = date ( 'Y-m-d:H-i-s' );							
							$lastInsId = $this->lobjTakafulmodel->fnInsertPaymentdetails ( $larrformData ); //insert into batch reg and getting that idbatchregistration
							$lastpaymentid = $this->lobjTakafulmodel->fnInsertStudentPaymentdetails ( $larrformData ['ModeofPayment'], $lastInsId );
							$auth = Zend_Auth::getInstance (); // Write Logs
							
							$lastpayid = $this->lobjTakafulmodel->fngetmodeofpayment ( $lastpaymentid );
			$flag=1;// If function called from re-registration module
			if ($larrformData ['ModeofPayment'] == 181) {
				$flag=0;
			}
								$larrformDataapp ['IDApplication'] = $lastInsId;
								$larrformDataapp ['companyflag'] = 1;
								$larrformDataapp ['Amount'] = $larrformData ['grossAmt'];
								$larrformDataapp ['UpdUser'] = 1;
								$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
								$regid = $this->lobjCompanypayment->fngeneraterandom ();
								$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);
					
							
							$this->lobjTakafulmodel->fninsertintostudentapplication($larrformData,$lastInsId,$regid,$this->gsessionidtakafuloperator->idtakafuloperator,2);
							
							if ($lastpayid ['ModeofPayment'] == 4) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
							}
			if ($lastpayid ['ModeofPayment'] == 10) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/migspayment/insertedid/" . $lastInsId );
							}
							if ($lastpayid ['ModeofPayment'] > 100) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
							}
							
							if ($lastpayid ['ModeofPayment'] == 1) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/fpxpageone/insertedId/" . $lastInsId );
							} else if ($lastpayid ['ModeofPayment'] != 2) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/index/editid/" . $lastInsId );
							} else {					
								$this->_redirect ( $this->baseUrl . "/takafulapplication/confirmpayment/insertedId/$lastInsId" );
								exit ();
							}				
							    $this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
							    exit ();
			}
		}
		

	if ($this->_request->isPost () && $this->_request->getPost ( 'proceed' )) {
			
			$larrformData = $this->_request->getPost ();
			
			//print_r($larrformData);die();
			$lintidcompany=$this->gsessionidtakafuloperator->idtakafuloperator;
			if($larrformData){
			
					$larrformData ['UpdUser'] = $this->gsessionidtakafuloperator->idtakafuloperator;
							$larrformData ['UpdDate'] = date ( 'Y-m-d:H-i-s' );							
							$lastInsId = $this->lobjTakafulmodel->fnReregistrationPaymentdetails ( $larrformData ); //insert into batch reg and getting that idbatchregistration
							$lastpaymentid = $this->lobjTakafulmodel->fnInsertStudentPaymentdetails ( $larrformData ['ModeofPayment'], $lastInsId );
							$auth = Zend_Auth::getInstance (); // Write Logs
							
							$lastpayid = $this->lobjTakafulmodel->fngetmodeofpayment ( $lastpaymentid );
			$flag=1;// If function called from re-registration module
			if ($larrformData ['ModeofPayment'] == 181) {
				$flag=0;
			}
								$larrformDataapp ['IDApplication'] = $lastInsId;
								$larrformDataapp ['companyflag'] = 2;
								$larrformDataapp ['Amount'] = $larrformData ['totalamount'];
								$larrformDataapp ['UpdUser'] = 1;
								$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
								$regid = $this->lobjCompanypayment->fngeneraterandompins ();
								$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);
					
							
							$this->lobjTakafulmodel->fninsertintostudentapplicationonpopup($larrformData,$lastInsId,$regid,$this->gsessionidtakafuloperator->idtakafuloperator,2);
							
							if ($lastpayid ['ModeofPayment'] == 4) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
							}
			if ($lastpayid ['ModeofPayment'] == 10) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/migspayment/insertedid/" . $lastInsId );
							}
							if ($lastpayid ['ModeofPayment'] > 100) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
							}
							
							if ($lastpayid ['ModeofPayment'] == 1) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/fpxpageone/insertedId/" . $lastInsId );
							} else if ($lastpayid ['ModeofPayment'] != 2) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/index/editid/" . $lastInsId );
							} else {					
								$this->_redirect ( $this->baseUrl . "/takafulapplication/confirmpayment/insertedId/$lastInsId" );
								exit ();
							}				
							    $this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
							    exit ();
			
		}
			
		}
	if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {		
		 $this->_redirect( $this->baseUrl . '/registrationfailed/index');
		}
	
	}
	
	public function paymentAction(){
		
				$this->view->lobjTakafulForm = $this->lobjTakafulForm;
				///////////////////////////for pay later/////////////////////////
				$larrpaylater = $this->lobjTakafulmodel->fngetpaylater ( $this->gsessionidtakafuloperator->idtakafuloperator );		
				$cntpaylater = count ( $larrpaylater );
				if ($cntpaylater > 1) {
					$this->view->lobjTakafulForm->ModeofPayment->addMultiOption ( $larrpaylater ['idDefinition'], $larrpaylater ['DefinitionDesc'] );
				}
				$this->view->lobjTakafulForm->ModeofPayment->setValue ( 181 );
		
				if ($this->_request->isPost () && $this->_request->getPost ( 'Makepay' )) {
		
				$larrformData = $this->_request->getPost ();
				
				$larrbatchresult=$this->lobjTakafulmodel->fnGetProgramName ();
				$this->view->lobjTakafulForm->idPrograms->addMultiOptions($larrbatchresult);
				
				$this->view->studentlist=$larrformData;
				
				$this->view->lobjTakafulForm->idPrograms->setValue($larrformData['studprogram'][0]);
				$this->view->lobjTakafulForm->idPrograms->setAttrib('readOnly','true');
				$this->view->programid=$larrformData['studprogram'][0];
				
				$larrresultdiscount = $this->lobjTakafulmodel->fngetintialdiscount ();
				$this->view->discount = $larrresultdiscount ['Discount'];
				
				$larrcaptionresult = $this->lobjTakafulmodel->fnGetCaptionName();		
				if ($larrcaptionresult ['CourseAliasName']) {
					$this->view->caption = $larrcaptionresult ['CourseAliasName'];
				}		
				//$larrbatchresult = $this->lobjTakafulmodel->fnGetProgramNamereentry ($larrformData['studprogram'][0]);	
				
				
				$this->view->lobjTakafulForm->idTakaful->setValue ( $this->gsessionidtakafuloperator->idtakafuloperator );
				
				$this->view->lobjTakafulForm->noOfCand->setValue(count($larrformData['student']));
				$this->view->lobjTakafulForm->noOfCand->setAttrib('readOnly','true');					
	
				}
				
				if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
					
						$larrformData = $this->_request->getPost ();						
						if ($this->view->lobjTakafulForm->isValid ( $larrformData )) {							
							
							$larrformData ['UpdUser'] = 1;
							$larrformData ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
							
							$lastInsId = $this->lobjTakafulmodel->fnInsertPaymentdetails ( $larrformData ); //insert into batch reg and getting that idbatchregistration
							$lastpaymentid = $this->lobjTakafulmodel->fnInsertStudentPaymentdetails ( $larrformData ['ModeofPayment'], $lastInsId );
							
							$auth = Zend_Auth::getInstance (); // Write Logs
							
							
							$lastpayid = $this->lobjTakafulmodel->fngetmodeofpayment ( $lastpaymentid );
							
							if ($larrformData ['ModeofPayment'] == 181) {
								$larrformDataapp ['IDApplication'] = $lastInsId;
								$larrformDataapp ['companyflag'] = 1;
								$larrformDataapp ['Amount'] = $larrformData ['grossAmt'];
								$larrformDataapp ['UpdUser'] = 1;
								$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
								$regid = $this->lobjCompanypayment->fngeneraterandom ();
								$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid );
							}
							
							$this->lobjTakafulmodel->fninsertintostudentapplication($larrformData,$lastInsId,$regid);
							
							
							if ($lastpayid ['ModeofPayment'] == 4) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
							}
							if ($lastpayid ['ModeofPayment'] > 100) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
							
							}
							if ($lastpayid ['ModeofPayment'] == 1) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/fpxpageone/insertedId/" . $lastInsId );
							} else if ($lastpayid ['ModeofPayment'] != 2) {
								$this->_redirect ( $this->baseUrl . "/takafulapplication/index/editid/" . $lastInsId );
							} else {					
								$this->_redirect ( $this->baseUrl . "/takafulapplication/confirmpayment/insertedId/$lastInsId" );
								exit ();
							}				
							    $this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
							    exit ();
						}
					}
	}

	public function schedulestudentsAction(){
		
			$this->view->lobjstudentForm = $this->lobjstudentForm;
                        $this->view->operator = $this->gsessionidtakafuloperator->idtakafuloperator;
			
		if ($this->_getParam ( 'regpinnum' )) {	
					
			$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
			$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
			
			
			$Idregpinnum = $this->_getParam ( 'regpinnum' );				
			$larrstudentresult=$this->lobjTakafulmodel->fngetfailedregstered($Idregpinnum);			
			$this->view->larrresultdata=$larrstudentresult;
		
		
			// Function to check the mode of pay and blockin if program already selected
			$larrresultBatch = $this->lobjTakafulmodel->fngetBatchDetails ( $Idregpinnum );			
			$larrgetpaydetails=$this->lobjTakafulcandidatesmodel->fnGetModeofpay($larrresultBatch['idBatchRegistration']);		
			$this->view->mode0fpay=$larrgetpaydetails['ModeofPayment'];
			
			if($larrgetpaydetails['ModeofPayment']==2){
						$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($larrresultBatch['idBatchRegistration']);			
						$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
						$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
						$this->view->progid=$larrgetprogramapplied['idProgram'];
			}else{							
						$this->view->mode0fpay=2;
						$this->lobjstudentForm->Program->setValue($larrstudentresult[0]['idProgram']);
						$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
						$this->view->progid=$larrstudentresult[0]['idProgram'];
			}
			
		}
		
		$larrreappliedcandidates=$this->lobjTakafulmodel->fngetreappliedcandidates($Idregpinnum);
		$this->view->larrappliedresult = $larrreappliedcandidates;
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost ();
			
			//print_r($larrformData);exit;
			
			$larrformData ['Examvenue'] = $larrformData ['NewVenue'];
			$resultstate = $this->lobjstudentmodel->fngetstatecity ( $larrformData ['NewVenue'] );
			$larrformData ['ExamState'] = $resultstate ['state'];
			$larrformData ['ExamCity'] = $resultstate ['city'];
			$larrformData ['NewState'] = $resultstate ['state'];
			$larrformData ['NewCity'] = $resultstate ['city'];
			$larrformData ['hiddenscheduler'] = 1;
			
			$availdate=$larrformData['Year']."-".$larrformData['setmonth']."-".$larrformData['setdate'];			
			$larravailseat=$this->lobjTakafulcandidatesmodel->fngetavailseatvenue($larrformData['NewVenue'],$availdate,$larrformData['idsession']);
			$larrformData['scheduler']=$larravailseat['idnewscheduler'];
			
			if(count($larrformData['studentId']) >= $larravailseat['availseat']){
				echo '<script language="javascript">alert("The Noof Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
				echo "<script>parent.location = '".$this->view->baseUrl()."/takafulcandidates/coursevenue/batchId/".$ids."';</script>";
				exit;
			}else{
			
			if (count ( $larrformData ['studentId'] ) > 0) {					
				$larrbatchregID = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationPinforexcel ( $Idregpinnum );
				$lintidbatch = $larrbatchregID ['idBatchRegistration'];
				$linttotnumofapplicant = $larrbatchregID ['totalNoofCandidates'];		
				
				$larrinsertstudent = $this->lobjTakafulcandidatesmodel->fnUpdatestudapplication ( $larrformData, $lintidbatch, $linttotnumofapplicant,$Idregpinnum);
			
			} else {
				?>				
			<script>alert("Check any Applicant")</script>
		<?php
			}
			}
			
			$this->gsessionbatch= new Zend_Session_Namespace('sis');  //Added on 04-02-2015 
			$this->gsessionbatch->visistsschedulestudents = 0;		  //Added on 04-02-2015 
			$this->_redirect ( $this->baseUrl . "/registrationfailed/schedulestudents/regpinnum/$Idregpinnum" );		
		}
	}
	
	public function getcandidatesAction(){
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		
		$regpin = $this->_getParam ( 'regpin' );  
		$status = $this->_getParam ( 'status' );
		$idprog = $this->_getParam ( 'idprog' );
		
		$larrgetallfailedstudentsearch=$this->lobjTakafulcandidatesmodel->fnSearchFailedStudentsCompany($regpin,$status,$this->gsessionidtakafuloperator->idtakafuloperator,2);
		$this->view->numcand=count($larrgetallfailedstudentsearch);
		echo Zend_Json_Encoder::encode($larrgetallfailedstudentsearch);
	}
		
	public function getnewprogramddAction(){		
			$this->_helper->viewRenderer->setNoRender(true);
			$this->_helper->layout->disableLayout();
			$status = $this->_getParam ( 'status' );
			$idprog = $this->_getParam ( 'idprog' );	
			if($status==1){
				$larrbatchresultnew = $this->lobjTakafulcandidatesmodel->fnGetProgramNameNewreregistration ($idprog);	
			}else{
				$larrbatchresultnew = $this->lobjTakafulmodel->fnGetProgramName ();
			}		
			$larrgetprogramnewlist = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrbatchresultnew );
			echo Zend_Json_Encoder::encode ( $larrgetprogramnewlist );	
		
	}
	
	public function fngetregpinAction(){		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$idprog = $this->_getParam ( 'idprog' );
		$flag=2;
		//echo $flag;exit;
		$larrgetregpin=$this->lobjTakafulcandidatesmodel->fnGetregpin($idprog,$this->gsessionidtakafuloperator->idtakafuloperator,$flag);
		$arrcount=count($larrgetregpin);	    
		
		$larrgetregpin[$arrcount]['key']=0;
		$larrgetregpin[$arrcount]['value']="Others";
		
		$larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrgetregpin );
		echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );		
	}
	
	public function getcandidatelookupAction(){		
		//Added on 07-09-2012
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$lintidapplication = $this->_getParam('idapplication');
		
		$newlookuparrays['key'][0]=0;
		$newlookuparray[0]['key']=0;
		$newlookuparray[0]['value']=0;
		
		$larrgetcandidatelookup=$this->lobjTakafulcandidatesmodel->fnGetcandidatelookup($lintidapplication);
		$count=count($larrgetcandidatelookup);
		
		for($lokiter=0;$lokiter<$count;$lokiter++){			
			if(!in_array($larrgetcandidatelookup[$lokiter]['key'],$newlookuparrays['key'])){
				$newlookuparrays['key'][$lokiter]=$larrgetcandidatelookup[$lokiter]['key'];
				$newlookuparray[$lokiter]['key']=$larrgetcandidatelookup[$lokiter]['key'];
				$newlookuparray[$lokiter]['value']=$larrgetcandidatelookup[$lokiter]['value'];
			}
		}
	
		$larrgetcandidatelookup = $this->lobjCommon->fnResetArrayFromValuesToNames ( $newlookuparray );
		echo Zend_Json_Encoder::encode($larrgetcandidatelookup);		
	}
	
	public function lookupbatchidAction(){		
		// Added on 05-09-2012
		$this->_helper->layout->disableLayout();		
	}	
	
	public function refinebatchidAction(){
		// Added on 05-09-2012
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$strcandidatename = $this->_getParam('candidatename');
		$strcandidateicno = $this->_getParam('candidateicno');
		$lintprogramid = $this->_getParam('programid');
		$lintcompanyid = $this->gsessionidtakafuloperator->idtakafuloperator;
		$lintstatus = $this->_getParam('status');
		
		$larrgetregpin=$this->lobjTakafulcandidatesmodel->fnGetregpinrefined($lintprogramid,$lintcompanyid,$strcandidatename,$strcandidateicno,$lintstatus,2);
		echo Zend_Json_Encoder::encode($larrgetregpin);
	}
	

}