<?php
class AutosubmitbycenterController extends Zend_Controller_Action {
public $gsessionidCenter;//Global Session Name
private $_gobjlogger;
	public function init() { //initialization function		
		$this->gsessionidCenter = Zend_Registry::get('sis'); 
		$idcenter = $this->_getParam('idadmincenter');
		if((empty($this->gsessionidCenter->idcenter)) && (empty($idcenter))) { 
			$this->_redirect( $this->baseUrl . '/centerlogin/logout');					
		}
		$this->_helper->layout()->setLayout('/examcenter/usty1');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		
	}
	
	/*
	 *  search form & grid display  
	 */
	public function indexAction() {
		 $lobjExamdetailsmodel = new App_Model_Examdetails();
		$this->lobjStudentexamModel = new GeneralSetup_Model_DbTable_Studentexam();
		$idcenter = $this->gsessionidCenter->idcenter;
		
		$lobjAutosubmitbycentermodel = new App_Model_Autosubmitbycenter();
		//$curtime = date('H:i:s');
		$cutdate = date('d');
		$idsessions =  $this->_getParam('idsession');
		$autosub =  $this->_getParam('autosub');
		$currdate = date('Y-m-d');
		if($autosub == 1) {
			$autosubmission = 1;
		} else {
			$autosubmission = 0;
		}
		if($idcenter=="" || $idcenter==NULL) {
			$idcenter = $this->_getParam('idadmincenter');
		}
		$updateofexamcenter= $lobjAutosubmitbycentermodel->fnUpdatecenterstart($idcenter,$idsessions,$currdate,$autosubmission);
			
		
		$studentdetailsarrayssss= $lobjAutosubmitbycentermodel->fnGetStudentdetails($idcenter,$idsessions,$cutdate);
		for($i=0;$i<count($studentdetailsarrayssss);$i++)
		{
			$idApplication = $studentdetailsarrayssss[$i]['IDApplication'];
			$RegId =$studentdetailsarrayssss[$i]['Regid'];
		
			
		   $larrresultupdate = $this->lobjStudentexamModel->fnGetFromTempDetail($RegId);
		  
		  if(count($larrresultupdate)>1)
		  {
	        $larrresult = $lobjExamdetailsmodel->fngetidbatchdetails($RegId);
			$studentdetailsarray= $lobjExamdetailsmodel->fnGetStudentdetails($idApplication);
/*			print_R($studentdetailsarray);
			die();*/
  	  	    $program = $studentdetailsarray['Program'];
  	  	    $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
  	  	    $IdBatch = $larractiveidbatch['IdBatch'];
  	  	    $this->view->idbatch = $IdBatch;
			
			//$lardeleteresult = $this->lobjStudentexamModel->fnDeleteTempDetails($RegId);
			$larrpercentageset = $lobjExamdetailsmodel->fnExampercentage($IdBatch);
			$noquestions = $larrpercentageset['NosOfQues'];
			
			$totoalnoofqtnsattended = $lobjExamdetailsmodel->fntotalanswered($RegId);
		    $totalanswered = count($totoalnoofqtnsattended);
		    
	   	    $larrresultpercentage = $lobjExamdetailsmodel->fnAttended($RegId);
	   	    
		    $attendedcorrect = count($larrresultpercentage);
				    $passpercent= (int)(($attendedcorrect/$noquestions)*100);
					if($passpercent>=55)
					{
					  //$CheckedValuesList[] ='Pass';	
					  if($passpercent>=85)
					  {
					  	$grade = 'A';
					  }
					  if($passpercent >= 70 && $passpercent <= 84)
					  {
					  	$grade = 'B';
					  }
					  if($passpercent >= 55 && $passpercent <=69)
					  {
					  	$grade = 'C';
					  }
					  $larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Pass");
					}
					else{
					  	$grade = 'F';
					  	$larrEmailTemplateDesc =  $lobjExamdetailsmodel->fnGetEmailTemplateDescription("Student Fail");	
					}
			$this->view->grade = $grade;
			//////////////////////*****///////////////////////////////////
			$larrtotalattended = $lobjExamdetailsmodel->totalattendedquestions($RegId);
			$answered = count($larrtotalattended);	
			$this->view->attended = $answered;		
			//$idApplication = $this->gsessionregistration->IDApplication;
		  $studentdetailsarray= $this->lobjStudentexamModel->fnGetStudentdetails($idApplication);
		  /*print_r($studentdetailsarray);
		  die();*/
		  $larridapplicationsubmitted = $lobjAutosubmitbycentermodel->fnchecksubmitted($idApplication);
		 /* print_r($larridapplicationsubmitted);
		  die();*/
		  if(count($larridapplicationsubmitted)>1)
		  {
		  	
		  }
		  else {
		  	
		 $larrresultinsertedmarks = $lobjExamdetailsmodel->fninsertstudentmarks($idApplication,$noquestions,$totalanswered,$attendedcorrect,$grade);
		      }

  	    $larractiveidbatch = $lobjExamdetailsmodel->fngetactivebatch($program);
  
			$noofquestion = $lobjExamdetailsmodel->fnnoofqtns($IdBatch);
			$this->view->noofquestion = $larractiveidbatch['NosOfQues'];
			$iduniversity=1;
			$larrinitconfig = $lobjExamdetailsmodel->fngetInitconfigDetails($iduniversity);
			
/*			
		
			
			print_r($studentdetailsarray);
			die();*/
			require_once('Zend/Mail.php');
				require_once('Zend/Mail/Transport/Smtp.php');
			
			                $lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$lstrEmailTemplateBody = str_replace("[NAME]",$studentdetailsarray['FName'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Address1]",$studentdetailsarray['PermAddressDetails'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Course]",$studentdetailsarray['ProgramName'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[NRIC]",$studentdetailsarray['ICNO'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[EXAMDATE]",$studentdetailsarray['Examdate'].'-'.$studentdetailsarray['Exammonth'].'-'.$studentdetailsarray['years'],$lstrEmailTemplateBody);
						    $lstrEmailTemplateBody = str_replace("[GRADE]",$grade,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
							
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = 'askiran123@gmail.com';
										$receiver = $studentdetailsarray['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										
									 try {
											//	$mail->send($transport);	
									} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Unable to send mail at this time because of some technical reason")</script>';
				       
								}
			
		  }
		  else 
		  {
		  
		  	
		  }
		}
		 $auth = Zend_Auth::getInstance(); // Write Logs
		   $priority=Zend_Log::INFO;
		   $controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		   $message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Submited the Exam sheet"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
		   $this->_gobjlogger->log($message,5);
    	   //print_r($this->gsessionregistration->mails);
   	   
	}
}