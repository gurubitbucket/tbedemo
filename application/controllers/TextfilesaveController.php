<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class TextfilesaveController extends Zend_Controller_Action 
{
	private $_gobjlogger;
	public function init() 
	{
		$this->gstrsessiontextfile= Zend_Registry::get('sis');
		$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->lobjTextfilesaveform = new  App_Form_Textfilesaveform();	
	}
	
	public function indexAction() 
	{
		$this->view->lobjTextfilesaveform=$this->lobjTextfilesaveform;
		 if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
        {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			unset ( $larrformData ['Save'] );
			$this->view->flag=1;
			$file = APPLICATION_PATH.'/logs/emailaddress.txt';
			$filename=explode('/',$file);
			if(file_exists($file))
			{	
				$contents = file_get_contents($file);
				$searcharray=array();
				$inducontents=explode("\n",$contents);
				for($inti=0;$inti<(count($inducontents)-1);$inti++)
				{
					$flag=strcmp($inducontents[$inti] , $larrformData ['EmailAddress']);
					if($flag == '0')
					break;
				}
				if($flag == '0')
				{
					$this->view->flag=0;
					//echo '<script language="javascript">alert("This Emailid is Already Exist.")</script>';
					//echo "<script>parent.location = '".$this->view->baseUrl()."/textfilesave/index';</script>";
					//die();
				}
				else
				{
					if($inducontents[0]=='')
					{
						$ourFileName = "emailaddress.txt";
						$ourFileHandle = fopen('../application/logs/'.$ourFileName, 'w') or die("can't open file");
						if ($larrformData["EmailAddress"] <> "")
						{
							fwrite($ourFileHandle, $larrformData ['EmailAddress']."\n");
						}
						fclose($ourFileHandle);
					}
					else
					{
						$ourFileName = "emailaddress.txt";
						$ourFileHandle = fopen('../application/logs/'.$ourFileName, 'a+') or die("can't open file");
						if ($larrformData["EmailAddress"] <> "")
						{
							fwrite($ourFileHandle, $larrformData ['EmailAddress']."\n");
						}
						fclose($ourFileHandle);
					}
					
				}
			}
			else
			{
				$ourFileName = "emailaddress.txt";
				$ourFileHandle = fopen('../application/logs/'.$ourFileName, 'w') or die("can't open file");
				if ($larrformData["EmailAddress"] <> "")
				{
					fwrite($ourFileHandle, $larrformData ['EmailAddress']."\n");
				}
				fclose($ourFileHandle);
			}
			
			
		}
	}
	
}
