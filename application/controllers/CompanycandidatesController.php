<?php
//error_reporting (E_ALL ^ E_WARNING);
error_reporting ( E_ALL ^ E_NOTICE );
class CompanycandidatesController extends Zend_Controller_Action { //Controller for the User Module
	//public $gsessionidtakafuloperator; //Global Session Name
	//private $_gobjlogger;
	
	public function init() { //initialization function		
		$this->gsessionbatch = Zend_Registry::get('sis'); 	
		if (empty ( $this->gsessionbatch->idCompany )) {
			$this->_redirect ( $this->baseUrl . '/batchlogin/logout' );
		}
		$this->_helper->layout ()->setLayout ( '/reg/usty1' );
		$this->view->translate = Zend_Registry::get ( 'Zend_Translate' );
		Zend_Form::setDefaultTranslator ( $this->view->translate );
		//$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		//$this->fnsetObj ();		
		$this->lobjCommon = new App_Model_Common ();			
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates (); //user model object		
		$this->lobjBatchcandidatesForm = new App_Form_Batchcandidates (); //intialize user lobjuserForm
		$this->lobjloadfilesForm = new Examination_Form_Uploadfiles ();
		$this->lobjTakafulapplicationmodel = new App_Model_Takafulapplication (); //user model object
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //user model object		
		$this->lobjTakafulcandidatesForm = new App_Form_Takafulcandidates (); //intialize user lobjuserForm
		$this->lobjstudentForm = new App_Form_Studentapplication ();
		$this->lobjstudentmodel = new App_Model_Studentapplication ();
		$this->registry = Zend_Registry::getInstance ();
		$this->locale = $this->registry->get ( 'Zend_Locale' );
		$this->lobjdeftype = new App_Model_Definitiontype ();
		$this->lobjcommonmodel= new App_Model_Common();
		$this->lobjCompanyapplication=new App_Model_Companyapplication();
	}
		
	
	public function indexAction() {// Action for manual application

		$month = date ( "m" ); // Month value
		$day = date ( "d" ); //today's date
		$year = date ( "Y" ); // Year value		
		
		$minmumage = new App_Model_Studentapplication ();		
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		$larr = $minmumage->fngetminimumage ();
		$age = $larr [0] ['MinAge'];
		$eligibility = ($year) - ($age);
		
		$year = $eligibility;
		$this->view->yearss = $year;
		$this->view->minages = $age;

		$yeste = date ( 'Y-m-d', mktime ( 0, 0, 0, $month, ($day - 1), $year ) );
		$this->view->yesdate = $yeste;
		$larrresult = $this->lobjCompanyapplication->fngetCompanyOperator ( $this->gsessionbatch->idCompany );
		$this->view->lobjTakafulcandidatesForm = $this->lobjTakafulcandidatesForm;
		$this->view->takafulDetails = $larrresult;		
		$ids = $this->_getParam ( 'batchId' );
		$this->view->idbatchss = $ids;
		$this->view->takafulname = $larrresult ['TakafulName'];
		$this->view->takafulid = $larrresult ['idtakafuloperator'];
		$idcomps = $this->gsessionidtakafuloperator->idtakafuloperator;
		
	/*	$companydetails = $this->lobjTakafulcandidatesmodel->fngettakafuladdressdetails ( $idcomps );
		$companyaddress = $companydetails ['paddr1'];
		*/
		
		$laresultsregpin = $this->lobjTakafulcandidatesmodel->fngetBatchDetailsforRegistrationpin ( $ids );
		$this->view->candidatetakaful = $laresultsregpin ['idCompany'];
		$regpin = $laresultsregpin ['registrationPin'];
		$this->view->regpin = $regpin;

		$laresultscandidate = $this->lobjTakafulcandidatesmodel->fngetBatchRegistration ( $ids );		
		$larrtakafulcount = $this->lobjTakafulcandidatesmodel->fnGetapplicantcountCompany ( $regpin );
		
		$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel( $regpin ); // Added on 21-02-2012
			
		if($larrstudentcounttemp ['totalcount']>$larrtakafulcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrtakafulcount ['totalregistered'];
		}		
		
		
		
		$availseat = $larrtakafulcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrtakafulcount ['totalregistered']);
	   // $availseat = $larrtakafulcount ['totalNoofCandidates'] - $actualregistered;
		$this->view->alreadyapppliedexcel = $availseat;

		$noofcandidates = Array ();
		$noofexams = count ( $laresultscandidate );
		for($lrescandidate = 0; $lrescandidate < $noofexams; $lrescandidate ++) {
			$noofcandidates ['idprgm'] [] = $laresultscandidate [$lrescandidate] ['idProgram'];
			$noofcandidates ['ProgramName'] [$laresultscandidate [$lrescandidate] ['idProgram']] = $laresultscandidate [$lrescandidate] ['ProgramName'];
			$noofcandidates [$laresultscandidate [$lrescandidate] ['idProgram']] = $laresultscandidate [$lrescandidate] ['noofCandidates'];
			$noofcandidatesssss [] = $laresultscandidate [$lrescandidate] ['noofCandidates'];
		}

		$larrbatchprog = $this->lobjTakafulcandidatesmodel->fnBatchProg ();
		for($numcandidate = 0; $numcandidate < count ( $noofcandidates ['idprgm'] ); $numcandidate ++) {
			$larrbatchprog123 [$noofcandidates ['idprgm'] [$numcandidate]] = $this->lobjTakafulcandidatesmodel->fnBatchProgram ( $noofcandidates ['idprgm'] [$numcandidate] );
		}
		$this->view->batchresults = $larrbatchprog123;
		$total = 0;
		for($numcandidatess = 0; $numcandidatess < count ( $noofcandidatesssss ); $numcandidatess ++) {
			$total = $total + $noofcandidatesssss [$numcandidatess];
		}
		//print_r($total);
		$this->view->total = $total;
		$this->view->noofprog = $noofcandidates ['idprgm'];
		$this->view->progname = $noofcandidates ['ProgramName'];
		$this->view->noofcandidates = $noofcandidates;

		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
		$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		
		//validation for payment being done and blocking
		$larrgetpaydetails=$this->lobjTakafulcandidatesmodel->fnGetModeofpayCompany($ids);
	
		$this->view->mode0fpay=$larrgetpaydetails['ModeofPayment'];		
		
			$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($ids);			
			$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
			$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
			$this->view->progid=$larrgetprogramapplied['idProgram'];
		
             		//$larrgetprogramappliedyear=$this->lobjTakafulcandidatesmodel->fnGetyearofprg($larrgetprogramapplied['idProgram']);	
             		//$this->lobjstudentForm->Year->addMultiOptions ( $larrgetprogramappliedyear );
				    //$this->lobjstudentForm->Year->setValue($larrgetprogramappliedyear[0]['Year']); 
						
		$larrresultrace = $this->lobjcommonmodel->fnGetRace ();
		$this->view->raceresult = $larrresultrace;
		$larrresuleducation = $this->lobjcommonmodel->fnGetEducation ();
		$this->view->educationresult = $larrresuleducation;
		
		$larresultreligionoperator = $this->lobjcommonmodel->fnGetAllActiveReligionNameList();
		$this->view->Religion = $larresultreligionoperator;		
		$larreducationresult = $this->lobjcommonmodel->fnGetCountryList();
		$this->view->countryresult = $larreducationresult;
		
		$larrbatchprog = $this->lobjTakafulcandidatesmodel->fnBatchProg ();
		
		for($iterbatch = 0; $iterbatch < count ( $larrbatchprog ); $iterbatch ++) {
			$programarrrya [$larrbatchprog [$iterbatch] ['IdProgrammaster']] [] = $larrbatchprog [$iterbatch] ['IdBatch'];
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'save' )) {		
			
			$larrformData = $this->_request->getPost ();	

		
			for($i=0;$i<count($larrformData['candidatename']);$i++){
					
					$larrdatainsert['StudentName']=$larrformData['candidatename'][$i];					
					$larrdatainsert['ICNO']=$larrformData['candidateicno'][$i];
					$larrdatainsert['Race']=$larrformData['candidaterace'][$i];
					$larrdatainsert['email']=$larrformData['candidateemail'][$i];
					$larrdatainsert['education']=$larrformData['candidateeducation'][$i];
					$larrdatainsert['Gender']=$larrformData['candidategender'][$i];
					$larrdatainsert['DOB']=$larrformData['candidatedateofbirth'][$i];
					$larrdatainsert['Address']=$larrformData['candidateaddress'][$i];
					$larrdatainsert['CorrespAddress']=$larrformData['correspondanceaddress'][$i];
					$larrdatainsert['PostalCode']=$larrformData['postalcode'][$i];
					$larrdatainsert['IdCountry']=$larrformData['candidatecountry'][$i];
					$larrdatainsert['IdState']=$larrformData['candidatestate'][$i];
					$larrdatainsert['ContactNo']=$larrformData['candidatenum'][$i];
					$larrdatainsert['MobileNo']=$larrformData['candidatemobnum'][$i];
					$larrdatainsert['idprogram']=0;
					
					
					$this->lobjTakafulcandidatesmodel->fninsertintotemp ( $larrdatainsert, $this->gsessionbatch->idCompany , $regpin );
				}
				
				
				$larrcandidateappliedcount = $this->lobjTakafulcandidatesmodel->fnGetapplicantcountCompany ( $regpin );	
				
				$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel( $regpin ); // Added on 21-02-2012
			
		if($larrstudentcounttemp ['totalcount']>$larrcandidateappliedcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrcandidateappliedcount ['totalregistered'];
		}		
			
			$availremainseat=$larrcandidateappliedcount['totalNoofCandidates'] - ($larrcandidateappliedcount['totalregistered']+$larrstudentcounttemp['totalcount']);
			//$availremainseat=$larrcandidateappliedcount['totalNoofCandidates'] - $actualregistered;
		$mainpath=$this->view->baseUrl();
				if($availremainseat!=0){
					echo '<script language="javascript">var theAnswer = confirm("Do You Wish To Insert More Candidates ?");					
					if(theAnswer){  	
						alert("Your Previous Candidate Data is Saved.....Please Enter the Next Candidate");
						window.location.href = "'.$mainpath.'/companycandidates/index/batchId/'.$ids.'";
					}else{
						alert("Thank You for your Patience....Your Data Has been Saved. Please Schedule the Candidates Entered");
						window.location.href = "'.$mainpath.'/companycandidates/coursevenue/batchId/'.$regpin.'";
					}
					</script>';
					exit;
				}else{					
					$this->_redirect( $this->baseUrl . '/companyapplication/index/');
					exit;
				}
			
			
			
			/*$ids = $larrformData ['idbatch'];		
			$larrgetvenuedetails = $this->lobjTakafulcandidatesmodel->fngetvenuedetailsinsert ( $larrformData ['NewVenue'] );			
			$larrformData ['NewState'] = $larrgetvenuedetails ['state'];
			$larrformData ['NewCity'] = $larrgetvenuedetails ['city'];			
			$countloop = count ( $larrformData ['candidatename'] );
			
			if(strlen($larrformData['setmonth']) <=1) {
				$larrformData['setmonth'] = '0'.$larrformData['setmonth'];
			}
			if(strlen($larrformData['setdate']) <=1) {
				$larrformData['setdate'] = '0'.$larrformData['setdate'];
			}			
			$availdate=$larrformData['Year']."-".$larrformData['setmonth']."-".$larrformData['setdate'];		
				
			$larravailseat=$this->lobjTakafulcandidatesmodel->fngetavailseatvenue($larrformData['NewVenue'],$availdate,$larrformData['idsession']);
			
			$larrformData['scheduler']=$larravailseat['idnewscheduler'];
			
			if($countloop > $larravailseat['availseat']){
				echo '<script language="javascript">alert("The No of Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
				echo "<script>parent.location = '".$this->view->baseUrl()."/companycandidates/index/batchId/".$ids."';</script>";
				exit;
			}else{				
				$larrinsertdata = $this->lobjTakafulcandidatesmodel->fnInsertIntoStdCompany ( $larrformData, $countloop, $ids, $regpin, $companyaddress );			
			}*/
		$this->_redirect ( $this->baseUrl . "/companycandidates/display/results/$regpin" );
		}
	}
	
	public function importAction() {  // Action for adding students through excerl
		
		$sessionID = Zend_Session::getId ();
		//$resulttemp = $this->lobjBatchcandidatesmodel->fnDeletetempdetails($sessionID);
		$ids = $this->_getParam ( 'batchId' );
	
		$this->view->idbatchss = $ids;
		$lobjUploadfilesForm = $this->lobjloadfilesForm; //intialize upload form
		$this->view->lobjUploadfilesForm = $lobjUploadfilesForm;		
		$larrtakafulcount = $this->lobjTakafulcandidatesmodel->fnGetapplicantcountCompany ( $ids ); //function to get applicant count	
				
		$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel( $ids ); // Added on 21-02-2012			
		
		if($larrstudentcounttemp ['totalcount'] > $larrtakafulcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrtakafulcount ['totalregistered'];
		}		
		
		$availseat = $larrtakafulcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrtakafulcount ['totalregistered']);
		//$availseat = $larrtakafulcount ['totalNoofCandidates'] - $actualregistered;
		
		$this->view->remspplication = $availseat;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost (); //getting the values of  from post 
			require_once 'Excel/excel_reader2.php';
			$lintfilecount ['Count'] = 0;
			$lstruploaddir = "/uploads/questions/";
			$larrformData ['FileLocation'] = $lstruploaddir;
			$larrformData ['UploadDate'] = date ( 'Y-m-d:H:i:s' );
			
			if ($_FILES ['FileName'] ['error'] != UPLOAD_ERR_NO_FILE) {
				$lintfilecount ['Count'] ++;				
				$lstrfilename = pathinfo ( basename ( $_FILES ['FileName'] ['name'] ), PATHINFO_FILENAME );
				$lstrext = pathinfo ( basename ( $_FILES ['FileName'] ['name'] ), PATHINFO_EXTENSION );
				
				$filename = $lintfilecount ['Count'] . "." . date ( 'YmdHis' ) . "." . $lstrext;
				$filename = str_replace ( ' ', '_', $lstrfilename ) . "_" . $filename;
				$file = realpath ( '.' ) . $lstruploaddir . $filename;
				if (move_uploaded_file ( $_FILES ['FileName'] ['tmp_name'], $file )) {
					//echo "success";
					$larrformData ['FilePath'] = $filename;
					$larrData ['FileName'] = $lstrfilename;
					$larrData ['FilePath'] = $filename;		
				} else {
					//echo "error";
				}
		
			}
			require_once 'Excel/excel_reader2.php';			
			
			$userDoc = realpath ( APPLICATION_PATH . '/../public/uploads/questions/' . $filename );
			
			$data = new Spreadsheet_Excel_Reader ( $userDoc );
			
			$arr = $data->sheets;			
			for($iterexcel = 2; $iterexcel < 100; $iterexcel ++) {
				if ($arr [0] ['cells'] [$iterexcel] [1] == '') {					
					break;
				} else
					$totalarray [$iterexcel] = $arr [0] ['cells'] [$iterexcel];
			}
			
			$larrtakafulcount = $this->lobjTakafulcandidatesmodel->fnGetapplicantcountCompany ( $ids ); 
			$larrstudentcounttemp = $this->lobjTakafulcandidatesmodel->fngetstudentcounttempexcel( $ids );
			
		if($larrstudentcounttemp ['totalcount']>$larrtakafulcount ['totalregistered']){
			$actualregistered=$larrstudentcounttemp ['totalcount'];
		}else{
			$actualregistered=$larrtakafulcount ['totalregistered'];
		}
			
		$availseat = $larrtakafulcount ['totalNoofCandidates'] - ($larrstudentcounttemp ['totalcount'] + $larrtakafulcount ['totalregistered']);
			
		//$availseat = $larrtakafulcount ['totalNoofCandidates'] - $actualregistered;
			
			if ($availseat < count ( $totalarray )) {
				$counts = $availseat + 2;
			} else {
				$count = count ( $totalarray );
				$counts = $count + 2;
			}
			
			if ($arr [0] ['cells'] [1] [1] != "Student Name" || $arr [0] ['cells'] [1] [2] != "ICNO" || $arr [0] ['cells'] [1] [3] != "E-Mail" || $arr [0] ['cells'] [1] [4] != "Race" || $arr [0] ['cells'] [1] [5] != "Education" || $arr [0] ['cells'] [1] [6] != "DateofBirth" || $arr [0] ['cells'] [1] [7] != "Gender" || $arr [0] ['cells'] [1] [8] != "Mailing Address" || $arr [0] ['cells'] [1] [9] != "Correspondance Address" || $arr [0] ['cells'] [1] [10] != "Postal Code" || $arr [0] ['cells'] [1] [11] != "Country" || $arr [0] ['cells'] [1] [12] != "State" || $arr [0] ['cells'] [1] [13] != "Contact No" || $arr [0] ['cells'] [1] [14] != "Mobile No") {
				echo '<script language="javascript">alert("Excel Sheet Not in correct Format")</script>';
				echo "<script>parent.location = '" . $this->view->baseUrl () . "/companycandidates/import/batchId/" . $ids . "';</script>";	
				exit;		
			}
				// newadded 27-06-2012
				
				$idprogarray=$this->lobjTakafulcandidatesmodel->fnGetprogramappliedExcel ( $ids ); 
				$idprogramapplied=$idprogarray['idProgram'];
				
			for($iterexcelread = 2; $iterexcelread < $counts; $iterexcelread++) {
				$flag=0;
				///////////////////////NAme/////////////////////
				$larrdatainsert ['StudentName'] = $arr [0] ['cells'] [$iterexcelread] [1];				
				////////////////////ICNO///////////////////////////////
				$icno = $arr [0] ['cells'] [$iterexcelread] [2];
							
				if(is_numeric($icno)){
									$larrdatainsert ['ICNO'] = $arr [0] ['cells'] [$iterexcelread] [2];
									$icnos = "$icno";
									$larricno = $this->lobjBatchcandidatesmodel->fnGetIcno ( $icno , $idprogramapplied );  // function to validate ICNO
									$larricnoexcel = $this->lobjTakafulcandidatesmodel->fnGetIcno ( $icno ,$idprogramapplied);  // function to validate ICNO	

									$dobicnum= "19".$icno[0].$icno[1]."-".$icno[2].$icno[3]."-".$icno[4].$icno[5];	
									$dobexcel = $arr [0] ['cells'] [$iterexcelread] [6];	
									$dobexcel=date('Y-m-d',strtotime($dobexcel));	

									$month=$icno[2].$icno[3];
									$day = $icno[4].$icno[5];
									
								   if($month>12){
								   		echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 1 );
										continue;
								   }
								   
								   if($day>31){
								   		echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 1 );
										continue;
								   }
				
									if($icno[11]%2==0){																				
										if($arr[0]['cells'][$iterexcelread][7] == 'Female' || $arr [0]['cells'][$iterexcelread][7] == 'FEMALE'){
											
										}else{
											$flag=6;											
										    echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										    $this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, $flag );
										    continue;	
										}
									} else{										
									if($arr[0]['cells'][$iterexcelread][7] == 'Male' || $arr [0]['cells'][$iterexcelread][7] == 'MALE'){
											
										}else{
											$flag=6;
											echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										    $this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, $flag );
										    continue;
										}
									}
									
									$toddate = date ( 'Y-m-d' );
									$diff = abs ( strtotime ( $toddate ) - strtotime ( $dobicnum ) );
									$years = floor ( $diff / (365 * 60 * 60 * 24) );
									if ($years < 18) {
										//Function to log errors
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 3 );
										continue;
									}
											
									if($dobicnum!=$dobexcel){
										$flag=11;//Function to log errors
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, $flag );
										continue;
									}			
									if(count($larricnoexcel) > 0){
										$flag=10;
									}else if (count($larricno) > 0){
										$flag=10;
									}else{
										$flag=1;
									}								
									$icnolen = strlen ( $icno );
									if ($icnolen != 12 || count($larricno)>0 || count($larricnoexcel)>0) {
										echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
										$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, $flag );
										continue;				
									}
				}else{
					
							echo '<script language="javascript">alert("ICNO are not entered properly")</script>';
							$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 1 );
							//echo "<script>parent.location = '".$this->view->baseUrl()."/takafulcandidates/import/batchId/".$ids."';</script>";
							continue;	
				}
									
									$year = '19' . $icnos [0] . $icnos [1];
									$month = $icnos [2] . $icnos [3];
									$day = $icnos [4] . $icnos [5];
									$dob = $year . '-' . $month . '-' . $day;
				/////////////////ICNO ENDS////////////////////////////////
				/////////////////Email Starts////////////////////////////////
				$larrdatainsert ['email'] = $arr [0] ['cells'] [$iterexcelread] [3];
				$email = $arr [0] ['cells'] [$iterexcelread] [3];
				$larrmailexcel = $this->lobjTakafulcandidatesmodel->fnGetmailId ( $email );  // function to validate E-MAIL
				$larrmail = $this->lobjBatchcandidatesmodel->fnGetmailId ( $email );   // function to validate EMAIL
				$race = $larrmail ['EmailAddress'];
				$mailcount = strlen ( $larrmail ['EmailAddress'] );

				/*if (! $larrmail && ! $larrmailexcel) {
				
				} else {
					echo '<script language="javascript">alert("Email Already Taken")</script>';
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 2 );
					continue;								
				}*/
				
				/////////////////Email ENDS////////////////////////////////
				///////////////////////RACE/////////////////////////////////
				
				$chienesearray = $arr [0] ['cells'] [$iterexcelread] [4];
				$larrrace = $this->lobjBatchcandidatesmodel->fnGetRaceId ( $chienesearray );// function to check race
				
				if(!$larrrace){
					$others="Others";
					$larrraceothers = $this->lobjBatchcandidatesmodel->fnGetRaceId ( $others );					
					$larrdatainsert ['Race'] = $larrraceothers ['idDefinition'];
				}else{
					$larrdatainsert ['Race'] = $larrrace ['idDefinition'];
				}
								
				$racecount = strlen ( $larrdatainsert ['Race'] );			
				if (!$larrdatainsert ['Race']) {
					echo '<script language="javascript">alert("Please check the race and upload the file")</script>';
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 4 );
					continue;	
				}				
				////////////////////////RACE ENDS////////////////////////////////
				///////////////////////Education////////////////////////////////
				$educationarray = $arr [0] ['cells'] [$iterexcelread] [5];
				$larreducation = $this->lobjBatchcandidatesmodel->fnGetEducatinexcel ( $educationarray ); // function to check education
				
				if(!$larreducation){
					$others="Others";
					$larreducationothers = $this->lobjBatchcandidatesmodel->fnGetEducatinexcel ( $others );
					$larrdatainsert ['education'] = $larreducationothers ['idDefinition'];
				}else{
					$larrdatainsert ['education'] = $larreducation ['idDefinition'];				
				}
				
				if ($larrdatainsert ['education']) {
				
				} else {
					echo '<script language="javascript">alert("Please check the Education and upload the file")</script>';
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 5 );
					continue;					
				}
				
				/////////////////////////////////////////////////////////////////////
				$dob = $larrdatainsert ['DOB'] = $arr [0] ['cells'] [$iterexcelread] [6];
				$toddate = date ( 'Y-m-d' );
				$diff = abs ( strtotime ( $toddate ) - strtotime ( $dob ) );
				$years = floor ( $diff / (365 * 60 * 60 * 24) );
				
				$minmumage = new App_Model_Studentapplication ();		
				$this->view->lobjstudentForm = $this->lobjstudentForm;
				$larr = $minmumage->fngetminimumage ();
				$age = $larr [0] ['MinAge'];
				
				if ($years < $age) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 3 );
					continue;
				}
				
				//////////////////////////////////GENDER////////////////////////

				if ($arr [0] ['cells'] [$iterexcelread] [7] == 'Male' ||$arr [0] ['cells'] [$iterexcelread] [7] == 'MALE') {
					$larrdatainsert ['Gender'] = 1;
				} else if ($arr [0] ['cells'] [$iterexcelread] [7] == 'Female' || $arr [0] ['cells'] [$iterexcelread] [7] == 'FEMALE') {
					$larrdatainsert ['Gender'] = 0;
				} else {
					echo '<script language="javascript">alert("Please check the Gender and upload the file")</script>';
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 6 );
					continue;
				}
				
				///////////////////////////////////GENDER ENDS////////////

				///////////////////ADDRESS/////////////////////
				$larrdatainsert ['Address'] = $arr [0] ['cells'] [$iterexcelread] [8];
				$larrdatainsert ['CorrespAddress'] = $arr [0] ['cells'] [$iterexcelread] [9];
				$larrdatainsert ['PostalCode'] = $arr [0] ['cells'] [$iterexcelread] [10];
				if (! $larrdatainsert ['PostalCode']) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 7 );
					continue;
				}
					
				///////////////////////Country//////////////////////////////////
				$countryarray = $arr [0] ['cells'] [$iterexcelread] [11];
				if (!$countryarray) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 8 );
					continue;
				}
				$larrcountry = $this->lobjBatchcandidatesmodel->fnGetCountryexcel ( $countryarray );	
				
				if(!$larrcountry){
					$countryarraydefault="MALAYSIA";
					$larrcountrydefault = $this->lobjBatchcandidatesmodel->fnGetCountryexcel ( $countryarraydefault );
					$larrdatainsert ['IdCountry']=$larrcountrydefault['idCountry'];
				}else{
					$larrdatainsert ['IdCountry'] = $larrcountry ['idCountry'];		
				}
											     		   
				if ($larrcountry ['idCountry'] != "") {
				
				} else {
					echo '<script language="javascript">alert("Please check the Country List and upload the file")</script>';	
					//Function to log errors				
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 8 );
					continue;					
				}
				
			
				//////////////////////Country//////////////////////////////////
				/////////////////////State////////////////////////////////////
				$statearray = $arr [0] ['cells'] [$iterexcelread] [12];
				if (!$statearray) {
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 9 );
					continue;
				}
				
				$larrstate = $this->lobjBatchcandidatesmodel->fnGetStateexcel ( $statearray );// Function to validate and check states	
				if(!$larrstate){
					$others="Others";
					//$larrstateothers = $this->lobjBatchcandidatesmodel->fnGetStateexcelothers ( $statearray );
					$larrstateothers = $this->lobjBatchcandidatesmodel->fnGetStateexcelothers ( $larrdatainsert['IdCountry'],$others );
					$larrdatainsert ['IdState'] = $larrstateothers ['idState'];	
				}else{
					$larrdatainsert ['IdState'] = $larrstate ['idState'];	
				}
							
				if ($larrstate ['idState'] != "") {
				} else {
					echo '<script language="javascript">alert("Please check the State List and upload the file")</script>';
					//Function to log errors
					$this->lobjTakafulcandidatesmodel->fnerrorreporter ( $arr [0] ['cells'] [$iterexcelread] [3], $arr [0] ['cells'] [$iterexcelread] [2], $arr [0] ['cells'] [$iterexcelread] [1], $arr [0] ['cells'] [$iterexcelread] [6], $ids, 9 );
					continue;					
				}

				//////////////////////////////////////////////////////////////
				$larrdatainsert ['ContactNo'] = $arr [0] ['cells'] [$iterexcelread] [13];
				$larrdatainsert ['MobileNo'] = $arr [0] ['cells'] [$iterexcelread] [14];		
				$larrdatainsert ['idprogram']=$idprogramapplied;
				$insertarray = $this->lobjTakafulcandidatesmodel->fninsertintotemp ( $larrdatainsert, $this->gsessionbatch->idCompany , $ids );				
				$larrdataimport ['UpdUser'] = $this->gsessionbatch->idCompany;
				$larrdataimport ['IdregistrationPin'] = $ids;
				$larrdataimport ['Typeofimport'] = 1;	
					

				$insertarray = $this->lobjTakafulcandidatesmodel->fninserttoimported ( $larrdataimport );
			
			}
			//echo $iterexcelread;
			$this->_redirect ( $this->baseUrl . "/companycandidates/coursevenue/batchId/$ids" );
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Schedule' )) {			
			$this->_redirect ( $this->baseUrl . "/companycandidates/coursevenue/batchId/$ids" );
		}
	
	}
	
	public function coursevenueAction() {  // Action for Scheduling students  added from excel
		
		$sessionID = Zend_Session::getId ();
		$this->view->lobjTakafulcandidatesForm = $this->lobjTakafulcandidatesForm;
		$this->view->lobjstudentForm = $this->lobjstudentForm;	
		$this->view->lobjBatchcandidatesForm = $this->lobjBatchcandidatesForm;
		
		$ids = $this->_getParam ( 'batchId' );
	
		$this->view->idbatchss = $ids;
		$laresultscandidate = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationimport ( $ids );
	
		
		$noofcandidates = Array ();
		$noofexams = count ( $laresultscandidate );
		
		for($iteresultscandidate = 0; $iteresultscandidate < $noofexams; $iteresultscandidate ++) {
			$noofcandidates ['idprgm'] [] = $laresultscandidate [$iteresultscandidate] ['idProgram'];
			$noofcandidates ['ProgramName'] [$laresultscandidate [$iteresultscandidate] ['idProgram']] = $laresultscandidate [$iteresultscandidate] ['ProgramName'];
			$noofcandidates [$laresultscandidate [$iteresultscandidate] ['idProgram']] = $laresultscandidate [$iteresultscandidate] ['noofCandidates'];
			$noofcandidatesssss [] = $laresultscandidate [$iteresultscandidate] ['noofCandidates'];
			$noofcandidates ['programid'] = $laresultscandidate [$iteresultscandidate] ['idProgram'];
		}
		
		$total = 0;
		
		for($iternumcandidate = 0; $iternumcandidate < count ( $noofcandidatesssss ); $iternumcandidate ++) {
			$total = $total + $noofcandidatesssss [$iternumcandidate];
		}
		
		$this->view->total = $total;
		$this->view->noofprog = $noofcandidates ['idprgm'];
		$this->view->idprogram = $noofcandidates['programid'];
		$this->view->progname = $noofcandidates ['ProgramName'];
		$this->view->noofcandidates = $noofcandidates;
		
		$larrtempexcelcandidates = $this->lobjCompanyapplication->fngetnoofstudentsfromexcel ( $ids, $this->gsessionbatch->idCompany );
		
		
		$this->view->takcandiddetails = $larrtempexcelcandidates;
		$this->view->totalstudents = $laresultscandidate ['totalNoofCandidates'];
		$this->view->totalexcelstudents = count ( $larrtempexcelcandidates ); 
		$this->view->countparts = count ( $laresultscandidate );
		$this->view->programresult = $laresultscandidate;
		

		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
		$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		
	// Function to check the mode of pay and blockin if program already selected
	
			$larrresultBatch = $this->lobjTakafulapplicationmodel->fngetBatchDetails ( $ids );		
		
			$larrgetpaydetails=$this->lobjTakafulcandidatesmodel->fnGetModeofpayCompany($larrresultBatch['idBatchRegistration']);
	
				
			$this->view->mode0fpay=$larrgetpaydetails['ModeofPayment'];		
						
						$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($larrresultBatch['idBatchRegistration']);	
						$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
						$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
						$this->view->progid=$larrgetprogramapplied['idProgram'];
						
						//$larrgetprogramappliedyear=$this->lobjTakafulcandidatesmodel->fnGetyearofprg($larrgetprogramapplied['idProgram']);	
						//$this->lobjstudentForm->Year->setValue($larrgetprogramappliedyear['Year']);
	
		
		$larrexcelappliedcandidates = $this->lobjCompanyapplication->fngetexcelappliedcandidates ( $ids, $this->gsessionbatch->idCompany );
		$this->view->larrappliedresult = $larrexcelappliedcandidates;
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost ();
		
			$larrformData ['Examvenue'] = $larrformData ['NewVenue'];
			$resultstate = $this->lobjstudentmodel->fngetstatecity ( $larrformData ['NewVenue'] );
			$larrformData ['ExamState'] = $resultstate ['state'];
			$larrformData ['ExamCity'] = $resultstate ['city'];
			$larrformData ['NewState'] = $resultstate ['state'];
			$larrformData ['NewCity'] = $resultstate ['city'];
			$larrformData ['hiddenscheduler'] = 1;
			
			$availdate=$larrformData['Year']."-".$larrformData['setmonth']."-".$larrformData['setdate'];			
			$larravailseat=$this->lobjTakafulcandidatesmodel->fngetavailseatvenue($larrformData['NewVenue'],$availdate,$larrformData['idsession']);
		    $larrformData['scheduler']=$larravailseat['idnewscheduler'];
			if(count ( $larrformData ['studenttakful'] ) > $larravailseat['availseat']){
				echo '<script language="javascript">alert("The Noof Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
				echo "<script>parent.location = '".$this->view->baseUrl()."/companycandidates/coursevenue/batchId/".$ids."';</script>";
				exit;
			}else{
			
			if (count ( $larrformData ['studenttakful'] ) >= 0) {
				$larrbatchregID = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationPinforexcel ( $larrformData ['idbatch'] );
				$lintidbatch = $larrbatchregID ['idBatchRegistration'];
				$linttotnumofapplicant = $larrbatchregID ['totalNoofCandidates'];
				$larrinsertstudent = $this->lobjTakafulcandidatesmodel->fnInsertintostudapplicationexcelCompany ( $larrformData, $lintidbatch, $linttotnumofapplicant, $larrformData ['idbatch'] );
			} else {
				echo '<script language="javascript">alert("Please Select Any OF the Candidates For Scheduling")</script>';
			}
			}
			
			$this->gsessionbatch= new Zend_Session_Namespace('sis');  //Added on 04-02-2015 
			$this->gsessionbatch->visitcoursevenue = 0;		  //Added on 04-02-2015
			$this->_redirect ( $this->baseUrl . "/companycandidates/coursevenue/batchId/$ids" );
		
		}
	
	}
	
	public function viewerrapplicationAction() {  //Action to view the error applications from excel upload
		$regpin = $this->_getParam ( 'regpin' );
		$this->view->idbatchss = $regpin;
		$larrerrresult = $this->lobjTakafulcandidatesmodel->fngetErrstudentapllication ( $regpin );
		$this->view->larrappliederr = $larrerrresult;		
	}
	
	
	public function fngetremainingseatsAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$day = $this->_getParam ( 'day' );
		$year = $this->_getParam ( 'Year' );
		$month = $this->_getParam ( 'month' );
		$city = $this->_getParam ( 'idcity' );
		$venue = $this->_getParam ( 'venue' );
		$sessionsids = $this->_getParam ( 'sessionsids' );		
		$idsechduler = $this->lobjBatchcandidatesmodel->fnGetVenuedetailsgetsecid ( $year );	
		$venueselect = $this->lobjBatchcandidatesmodel->fnGetVenuedetailsRemainingseats ( $idsechduler ['Year'], $idsechduler ['idnewscheduler'], $city, $month, $day );
		$rem = "";
		foreach ( $venueselect as $ven ) {
			if ($ven ['rem'] > 0) {
				if ($ven ['idcenter'] == $venue && $ven ['idmangesession'] == $sessionsids) {
					$rem = $ven ['rem'];
				}
			}
		}
		echo $rem;
		die ();
	
	}
	
	public function displayAction() {  //Action to display all students registered		
		$regid = $this->_getParam ( 'results' );
		$larrresutls = $this->lobjBatchcandidatesmodel->fngetregisteredstudentsdetails ( $regid );
		$this->view->message = $larrresutls;
	}
	
	public function pdfexportAction()
	{
		//Exporting data to an excel sheet 

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) {
				
			$larrformData = $this->_request->getPost ();	
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
			
		$time = date('h:i:s',time());
		$filename = 'Company_Registration_Report_'.$frmdate;
		$ReportName = $this->view->translate( "Company" ).' '.$this->view->translate( "Registration" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
			$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
		}
			$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b>Time</b></td>
								<td align = 'left' colspan= 5><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 8><b> {$ReportName}</b></td>	
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b></b></th>
							<th><b>Student Name</b></th>
							<th><b>ICNO</b></th>
							<th><b>Registration Id</b></th>
							<th><b>Program Applied</b></th>
							<th><b>Venue Name</b></th>
							<th><b>Exam Date</b></th>
							<th><b>Exam Session</b></th>						
						</tr>';     
	 
		if (count($larrformData)){
			 $cnt = 0; 
		
			 
      	for($expdata=0;$expdata<count($larrformData['FName']);$expdata++){
      		  	
			$tabledata.= ' <tr>
				   		   <td><b>'; 
      		      $tabledata.= '</b></td>
					       <td>'.$larrformData['FName'][$expdata].'</td> 
						   <td>'.$larrformData['ICNO'][$expdata].'</td> 
						   <td>'.$larrformData['Regid'][$expdata].'</td> 
						   <td>'.$larrformData['ProgramName'][$expdata].'</td> 
						   <td>'.$larrformData['centername'][$expdata].'</td> 		  
						   <td>'.$larrformData['dateofexam'][$expdata].'</td> 
						   <td>'.$larrformData['session'][$expdata].'</td> 		   
				        </tr> ';				   	
	    	$cnt++; 
      		  }
	     
		 }		
	
		 $tabledata.= '<tr>		      
		     	<td></td>
		     	<td></td>
		     	<td></td>
		     </tr>
		     </table>';	
		
		//echo $tabledata;exit;
		if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}
	}
				
}
	
	public function adhocAction() {		
		$larrresult = $this->lobjTakafulapplicationmodel->fngetTakafulOperator ( $this->gsessionidtakafuloperator->idtakafuloperator );
		$this->view->lobjTakafulcandidatesForm = $this->lobjTakafulcandidatesForm;
		$this->view->takafulDetails = $larrresult;
		
		$ids = $this->_getParam ( 'batchId' );
		$this->view->idbatchss = $ids;
		
		$this->view->takafulname = $larrresult ['TakafulName'];
		$this->view->takafulid = $larrresult ['idtakafuloperator'];
		
		$laresultsregpin = $this->lobjTakafulcandidatesmodel->fngetBatchDetailsforRegistrationpin ( $ids );		
		$regpin = $laresultsregpin ['registrationPin'];
		$this->view->regpin = $regpin;
		$laresultscandidate = $this->lobjTakafulcandidatesmodel->fngetBatchRegistration ( $ids );
		
		$noofcandidates = Array ();
		$noofexams = count ( $laresultscandidate );
		for($iternumcandidate = 0; $iternumcandidate < $noofexams; $iternumcandidate ++) {
			$noofcandidates ['idprgm'] [] = $laresultscandidate [$iternumcandidate] ['idProgram'];
			$noofcandidates ['ProgramName'] [$laresultscandidate [$iternumcandidate] ['idProgram']] = $laresultscandidate [$iternumcandidate] ['ProgramName'];
			$noofcandidates [$laresultscandidate [$iternumcandidate] ['idProgram']] = $laresultscandidate [$iternumcandidate] ['noofCandidates'];
			$noofcandidatesssss [] = $laresultscandidate [$iternumcandidate] ['noofCandidates'];		
		}
		$larrbatchprog = $this->lobjTakafulcandidatesmodel->fnBatchProg ();
		for($iterbatch = 0; $iterbatch < count ( $noofcandidates ['idprgm'] ); $iterbatch ++) {
			$larrbatchprog123 [$noofcandidates ['idprgm'] [$iterbatch]] = $this->lobjTakafulcandidatesmodel->fnBatchProgram ( $noofcandidates ['idprgm'] [$iterbatch] );
		}
		$this->view->batchresults = $larrbatchprog123;		
		$total = 0;
		for($numcandidatess = 0; $numcandidatess < count ( $noofcandidatesssss ); $numcandidatess ++) {
			$total = $total + $noofcandidatesssss [$numcandidatess];
		}
		print_r ( $total );
		$this->view->total = $total;
		$this->view->noofprog = $noofcandidates ['idprgm'];
		$this->view->progname = $noofcandidates ['ProgramName'];
		$this->view->noofcandidates = $noofcandidates;
		
		$larrresultprogram = $this->lobjTakafulcandidatesmodel->fnGetProgramName ();
		$this->view->programresult = $larrresultprogram;
		
		$larresultbatch = $this->lobjTakafulcandidatesmodel->fnGetBatchName ();
		/*print_r($larresultbatch);
  		die();*/
		$this->view->batchresult = $larresultbatch;
		
		$larrresultrace = $this->lobjcommonmodel->fnGetRace ();
		/*print_r($larrresultrace);
  		die();*/
		$this->view->raceresult = $larrresultrace;
		
		$larrresuleducation = $this->lobjcommonmodel->fnGetEducation ();
		$this->view->educationresult = $larrresuleducation;
		
		$larresultTakafuloperator = $this->lobjTakafulcandidatesmodel->fnTakafuloperator ();
		$this->view->takafuloperator = $larresultTakafuloperator;
		
		$larrbatchprog = $this->lobjTakafulcandidatesmodel->fnBatchProg ();
		for($iterbatchad = 0; $iterbatchad < count ( $larrbatchprog ); $iterbatchad ++) {
			$programarrrya [$larrbatchprog [$iterbatchad] ['IdProgrammaster']] [] = $larrbatchprog [$iterbatchad] ['IdBatch'];
		}
		
		$resultvenue = $this->lobjTakafulcandidatesmodel->fnAdhocvenue ( $ids );
		$this->view->venues = $resultvenue ['AdhocVenue'];
		$this->view->adhocdate = $resultvenue ['AdhocDate'];
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost ();
			$ids = $larrformData ['idbatch'];		
			$regpin = $larrformData ['regpin'];
			$countloop = count ( $larrformData ['candidatename'] );
			$larrinsertdata = $this->lobjTakafulcandidatesmodel->fnInsertIntoAdhocStd ( $larrformData, $countloop, $ids, $regpin );
			$this->_redirect ( $this->baseUrl . "/takafulapplication/index/" );
		
		}
	
	}
	
	public function fngetschedulerdetailsAction() {		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();				
		$lintidbatch = $this->_getParam ( 'idbatch' );		
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnGetscheduler ( $lintidbatch );
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrvenuetimeresult );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetails );
	}
	
	public function fngetvenueAction() {		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();		
		$lintidscheduler = $this->_getParam ( 'idscheduler' );
		$larrbatchresult = $this->lobjBatchcandidatesmodel->fnGetVenueName ( $lintidscheduler );
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrbatchresult );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetails );
	}
	
	public function fngetvenuetimeAction() {
		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();		
		//Get Country Id
		$lintidvenue = $this->_getParam ( 'idvenue' );		
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnGetVenueTime ( $lintidvenue );
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrvenuetimeresult );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetails );
	}
	
	public function fngetdatetimeAction() {
		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		//Get Country Id
		$lintdate = $this->_getParam ( 'date' );
		$lintidvenue = $this->_getParam ( 'idvenue' );
		
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnGetTimingsForDate ( $lintdate, $lintidvenue );
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrvenuetimeresult );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetails );
	}
	
	public function deletefromtempAction(){ // ADDED on 28-2-2013
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$idcandidates = $this->_getParam ( 'idcandidates' );
		$result = $this->lobjTakafulcandidatesmodel->fndeletefromtemp ( $idcandidates );
		echo $result;
		exit;
		
	}
	
	public function fngetstatenameAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();			
		$Program = $this->_getParam ( 'Program' );
		$idyear = $this->_getParam ( 'idyear' );		
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnGetStatelistforcourse ( $Program, $idyear );		
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrvenuetimeresult );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetails );
	}
	
	public function fngetcitynamesAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();		
		//Get Country Id
		$lintdate = $this->_getParam ( 'idstate' );
		$Program = $this->_getParam ( 'Program' );		
		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetCitylistforcourse ( $lintdate, $Program );		
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrvenuetimeresults );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetailss );
	}
	
	public function fngetmonthslistAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		//Get Country Id
		$idprog = $this->_getParam ( 'Program' );
		$year = $this->_getParam ( 'idyear' );
		$curyear = date ( 'Y' );
		
		$larrvenuetimeresults = $this->lobjBatchcandidatesmodel->fnGetMonths ( $year, $idprog );
		$frommonth = $larrvenuetimeresults [0] ['From'];
		$tomonth = $larrvenuetimeresults [0] ['To'];
		$years = $larrvenuetimeresults [0] ['Year'];
		if ($curyear == $years) {
			$curmonth = date ( 'm' );
			
			if ($frommonth <= $curmonth) {				
				$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween2 ( $tomonth );
			} else {
				$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween ( $frommonth, $tomonth );
			}
			
		} else {
			$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween ( $frommonth, $tomonth );
		}
		
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrresults );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetailss );
	}
	
	public function fngetdateAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();		
		//Get Country Id
		$idprog = $this->_getParam ( 'Program' );
		$year = $this->_getParam ( 'idyear' );
		$larrvenuetimeresults = $this->lobjBatchcandidatesmodel->fnGetMonths ( $year, $idprog );
		$frommonth = $larrvenuetimeresults [0] ['From'];
		$tomonth = $larrvenuetimeresults [0] ['To'];
		
		$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween ( $frommonth, $tomonth );
		
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrresults );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetailss );
	}
	
	public function fngetvenuedetailsAction() {
		$this->lobjstudentmodel = new App_Model_Studentapplication ();
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();		
		//Get Country Id
		$Year = $this->_getParam ( 'Year' );
		$Program = $this->_getParam ( 'Program' );
		$idcity = $this->_getParam ( 'idcity' );		
		$venueselect = $this->lobjBatchcandidatesmodel->fnGetVenuedetails ( $Year, $Program, $idcity );		
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $venueselect );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetailss );
	}
	
public function fngetemaildetailsAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
		$idyear = $this->_getParam('Year');
			$linticno = $this->_getParam('ICNO');
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fngetstudentsemail($Program,$idyear);
if(count($larrvenuetimeresult)>0)
		{ 
			foreach($larrvenuetimeresult as $batches)
			{
			
				
			if($linticno != $batches[ICNO])
			{
			
			    echo 'We observe that the email id provided already exists, please login to the portal if you have already registered. If you have not registered earlier, please provide with another email id';
		 
			}	
			
			}
			
		
		}
}
	public function fngetvenuesessiondetailsAction() {
		$this->lobjstudentmodel = new App_Model_Studentapplication ();
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		
		//Get Country Id
		$Year = $this->_getParam ( 'Year' );
		$Program = $this->_getParam ( 'Program' );
		$idcity = $this->_getParam ( 'idcity' );
		$venue = $this->_getParam ( 'venue' );
		$day = $this->_getParam ( 'day' );
		$month = $this->_getParam ( 'month' );		
		$idsechduler = $this->lobjBatchcandidatesmodel->fnGetVenuedetailsgetsecid ( $Year );
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjBatchcandidatesmodel->fnGetVenuedetailsRemainingseats ( $idsechduler ['Year'], $idsechduler ['idnewscheduler'], $idcity, $month, $day );
		$flag = 0;
		$idsession = 0;
		$venueid = 0;
		//$idsession="";
		foreach ( $venueselect as $ven ) {
			if ($ven ['rem'] > 0) {
				$flag = 1;
			}
			if ($ven ['rem'] == 0 && ($ven ['idcenter'] == $venue)) {
				//$idsession=$ven['idmangesession'];
				$idsession = $idsession . ',' . $ven ['idmangesession'];
			    // $idsessions=$ven['idmangesession'];
			}
			
			if ($ven ['rem'] < 0 && ($ven ['idcenter'] == $venue)) {
				$idsession = $idsession . ',' . $ven ['idmangesession'];
			}
		
		}
		      //	echo $idsession;die();
		$venueselect = $this->lobjBatchcandidatesmodel->fnGetsesssiondetails ( $Year, $Program, $idcity, $venue, $idsession, $day, $month );
		
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $venueselect );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetailss );
	}
	
	public function fngetactivesetAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$Program = $this->_getParam ( 'idprog' );
		$larrvenuetimeresults = $this->lobjBatchcandidatesmodel->fnGetActiveSet ( $Program );
		//print_r($larrvenuetimeresults);die();
		echo $larrvenuetimeresults [0] ['IdBatch'];
	
	}
	
	public function tempdaysAction() {
		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$day = $this->_getParam ( 'day' );
		$year = $this->_getParam ( 'year' );
		$month = $this->_getParam ( 'month' );
		$city = $this->_getParam ( 'city' );
		$dateid = $day . '' . $month;
		
		$idsechduler = $this->lobjBatchcandidatesmodel->fnGetVenuedetailsgetsecid ( $year );
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjBatchcandidatesmodel->fnGetVenuedetailsRemainingseats ( $idsechduler ['Year'], $idsechduler ['idnewscheduler'], $city, $month, $day );
		$flag = 0;
		foreach ( $venueselect as $ven ) {
			if ($ven ['rem'] > 0) {
				$flag = 1;
			}
		
		}
		echo $flag;
		die ();
	
	}
	
	
	
	public function fngetstudentconfirmAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		
		//Get Country Id
		$Program = $this->_getParam ( 'Program' );
		$icno = $this->_getParam ( 'icno' );
		$larrvenuetimeresult = $this->lobjBatchcandidatesmodel->fnstudentconfirm ( $Program, $icno );
		$pass = $larrvenuetimeresult ['pass'];
		if ($pass == 3) {
			echo '1' . '***' . 'You have already applied for the exam';
		} else if ($pass == 1) {
			echo '1' . '***' . 'You have already Passed the exam';
		} else {
			echo '0' . '***';
		}
	}
	
	public function fngetstateAction() {
		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$idcountry = $this->_getParam ( 'idcountry' );
		$larrstatelist = $this->lobjCommon->fnGetCountryStateList ( $idcountry );
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrstatelist );
		echo Zend_Json_Encoder::encode ( $larrCountryStatesDetailss );
	}
	
	public function newtempdaystakafulAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day = $this->_getParam('day');
		$year = $this->_getParam('year');
		$month = $this->_getParam('month');
		$venue = $this->_getParam('city');
		$totalcanditeno = $this->_getParam('totalcanditeno');
		//echo $totalcanditeno;die();
		if($month <10){
			$month = '0'.$month;
		}
		if($day <10){
			$day = '0'.$day;
		}
		$selecteddate = $year.'-'.$month.'-'.$day;
		$noofseats = $this->lobjTakafulcandidatesmodel->fnvalidateseats($venue,$selecteddate);
		
		$flag=0;
		
	   //print_r($noofseats);die();
		
	   /*foreach($noofseats as $vens){
		if($totalcanditeno<=$vens['rem']) {
			$flag=1;
		}
		}*/
		
		
		
		foreach($noofseats as $ven){
		if($ven['Allotedseats']<$ven['Totalcapacity']) {
			$flag=1;
		}
		}
		if($flag==1){
			$flag=1;
		}
		echo $flag;
	}
	
	

}