﻿<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class RegistrationController extends Zend_Controller_Action{ //Controller for the User Module
private $_gobjlogger;
	public function init() { //initialization function
		$this->_helper->layout()->setLayout('/batch/usty1');	
		//$this->Url = "http://192.168.1.103/tbe";	
		$this->view->translate = Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
			//$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
				$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates();
				$this->lobjnewscreenmodel = new GeneralSetup_Model_DbTable_Newscreen(); 
		$this->lobjstudentForm = new App_Form_Studentapplication(); //intialize user lobjuserForm
		$this->lobjCommon=new App_Model_Common();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		  	$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	public function indexAction(){ // action for search and view
		
//		$this->_redirect('http://www.takafuleexam.com' );
//		exit;
	
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		$count=0;
    	$larrinitialresult=$this->lobjstudentmodel->fngetintialconfigdetails();
		if($larrinitialresult[FldTxt1])
		{
			$this->view->Txt1=$larrinitialresult[FldTxt1];
			$this->view->Dpd1=$larrinitialresult[FldDdwn1];
			$count=$count+$larrinitialresult[FldTxt1];
		}
		if($larrinitialresult[FldTxt2])
		{
			$this->view->Txt2=$larrinitialresult[FldTxt2];
			$this->view->Dpd2=$larrinitialresult[FldDdwn2];
			$count=$count+$larrinitialresult[FldTxt2];
		}
		if($larrinitialresult[FldTxt3])
		{
			$this->view->Txt3=$larrinitialresult[FldTxt3];
			$this->view->Dpd3=$larrinitialresult[FldDdwn3];
			$count=$count+$larrinitialresult[FldTxt3];
		}
		if($larrinitialresult[FldTxt4])
		{
			$this->view->Txt4=$larrinitialresult[FldTxt4];
			$this->view->Dpd4=$larrinitialresult[FldDdwn4];
			$count=$count+$larrinitialresult[FldTxt4];
		}
		if($larrinitialresult[FldTxt5])
		{
			$this->view->Txt5=$larrinitialresult[FldTxt5];
			$this->view->Dpd5=$larrinitialresult[FldDdwn1];
			$count=$count+$larrinitialresult[FldTxt5];	
		}
		if($larrinitialresult[FldTxt6])
		{
			$this->view->Txt6=$larrinitialresult[FldTxt6];
			$this->view->Dpd6=$larrinitialresult[FldDdwn6];
			$count=$count+$larrinitialresult[FldTxt6];
		}
		$this->view->counts=$count;
			$_SESSION['idApp'] = 0;
			
		$this->view->fromweb = $this->_getParam('fromweb');
		$_SESSION['NewVenue']= 0;
	}

	public function newstudentAction(){ 	
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the Form object to the view
		$auth = Zend_Auth::getInstance();
		$auth->getIdentity()->iduser = 1;	
	//////////////////////////////////////////////////////////////////////////////////////
		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		
		$this->view->idapp= 0;	
		$insertedId = $this->_getParam('insertedId');
		if($insertedId)$this->view->idapp=$insertedId;
		//echo $insertedId;die();
		$this->view->lobjstudentForm->IDApplication->setValue ( $insertedId );
		
		$larr=$this->lobjstudentmodel->fngetminimumage();
		$age=$larr[0]['MinAge'];
		$eligibility = ($year)-($age);
		$year=$eligibility;	
		$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));
		$this->view->yesdate=$yesterdaydate;
		$this->view->ages=$age;
		////////////////////////////////////////////////////
		
		
		//////////////////////////////////////////////////
		
  /////////////////////////////////////////////////////////////////////////////

	$count=0;
    $larrinitialresult=$this->lobjstudentmodel->fngetintialconfigdetails();
	if($larrinitialresult[FldTxt1])
	{
		$this->view->Txt1=$larrinitialresult[FldTxt1];
		$this->view->Dpd1=$larrinitialresult[FldDdwn1];
	$count=$count+$larrinitialresult[FldTxt1];
	}
	if($larrinitialresult[FldTxt2])
	{
		$this->view->Txt2=$larrinitialresult[FldTxt2];
		$this->view->Dpd2=$larrinitialresult[FldDdwn2];
	$count=$count+$larrinitialresult[FldTxt2];
	}
	if($larrinitialresult[FldTxt3])
	{
		$this->view->Txt3=$larrinitialresult[FldTxt3];
		$this->view->Dpd3=$larrinitialresult[FldDdwn3];
		$count=$count+$larrinitialresult[FldTxt3];
	}
	if($larrinitialresult[FldTxt4])
	{
		$this->view->Txt4=$larrinitialresult[FldTxt4];
		$this->view->Dpd4=$larrinitialresult[FldDdwn4];
		$count=$count+$larrinitialresult[FldTxt4];
	}
	if($larrinitialresult[FldTxt5])
	{
		$this->view->Txt5=$larrinitialresult[FldTxt5];
		$this->view->Dpd5=$larrinitialresult[FldDdwn1];
		$count=$count+$larrinitialresult[FldTxt5];	
	}
	if($larrinitialresult[FldTxt6])
	{
		$this->view->Txt6=$larrinitialresult[FldTxt6];
		$this->view->Dpd6=$larrinitialresult[FldDdwn6];
		$count=$count+$larrinitialresult[FldTxt6];
	}
	$this->view->counts=$count;
	$this->larrvenues = array();
	if ($this->_request->isPost () && $this->_request->getPost ( 'setdate' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
			//echo "<pre />";print_r($larrformData);die();
			$larrformData['UpdDate'] =  date ( 'Y-m-d:H-i-s' );
			$larrformData['UpdUser'] = 1;
			////$larrformData['Qualification']=0;
			unset($larrformData['Save']);
			unset($larrformData['Close']);
			$larrformData['ICNO'] = $larrformData['ICN1'].''.$larrformData['ICN2'].''.$larrformData['ICN3'].''.$larrformData['ICN4'].''.$larrformData['ICN5'].''.$larrformData['ICN6'];
			unset($larrformData['ICN1']);
			unset($larrformData['ICN2']);
			unset($larrformData['ICN3']);
			unset($larrformData['ICN4']);
			unset($larrformData['ICN5']);
			unset($larrformData['ICN6']);
			
			$this->view->datesss=$larrformData['setdate'];
		$this->view->monthss=$larrformData['setmonth'];
			
				//$result = $this->lobjstudentmodel->fnAddStudent($larrformData); //instance for adding the lobjuserForm values to DB
				
		   if($_SESSION['idApp'] != 0)
			{
				$result=$_SESSION['idApp'] ;
				  $resultstate = $this->lobjstudentmodel->fngetstatecity($larrformData['NewVenue']);
				 // 'Examvenue'=>$larrformData['Examvenue'],
				  	$larrformData['ExamState']= $resultstate['state'];
		$larrformData['ExamCity']=$resultstate['city'];
		$larrformData['Examvenue']=$larrformData['NewVenue'];
	    $larrformData['NewState']= $resultstate['state'];
		$larrformData['NewCity']=$resultstate['city'];
				/*print_r($larrformData);
				die();*/
				// $scheduleryear = $this->lobjstudentmodel->fngetyearforthescheduler($larrformData['Year']);
				$this->view->idapp=$result;
				
				$this->lobjstudentmodel->fnUpdateStudent($result,$larrformData);
				
				
					$resultss = $this->lobjstudentmodel->fnupdatedstudentapplication($result);
					//print_r($resultss);die();
					$icnodob= $resultss['ICNO'];
 	$icno ="$icnodob";
 	$dobs=substr($icno,0,6);
 	$icno2=$larrinitialresult[FldTxt2];
 	$icno22=substr($icno,6,$icno2);
 	$icno3=$larrinitialresult[FldTxt3];
 	$icno33=$larrinitialresult[FldTxt2]+6;
 	$icno333=substr($icno,$icno33,$icno3);
 	
 		//$larrbatchresult3 = $this->lobjstudentmodel->fnGetYearlistforcourse($resultss['Program']);
		//$this->lobjstudentForm->Year->addMultiOptions($larrbatchresult3);
		
		
		//$resultstate = $this->lobjstudentmodel->fngetstatecity($larrformData['NewCity']);
		
		//$resultss['ExamState']= $resultstate['state'];
		
	/*	$larrvenuetimeresult3 = $this->lobjstudentmodel->fnGetStatelistforcourse($resultss['Program'],$resultss['Year']);
		$this->lobjstudentForm->NewState->addMultiOptions($larrvenuetimeresult3);*/
		
 		$larrresultcourses = $this->lobjstudentmodel->fngetappearedcourses($icnodob);
			$this->view->apperedcourses = $larrresultcourses;
			$larrvenuetimeresults3 = $this->lobjstudentmodel->fnGetCitylistforcourse($resultss['ExamState'],$resultss['Program'],$resultss['Year']);
			$this->lobjstudentForm->NewCity->addMultiOptions($larrvenuetimeresults3);
			
			$curmonth=date('m');
			//if($curmonth<10)
			//{
			//	 $arrcellphone = explode("0",$curmonth);
			/*$this->view->lobjuserForm->countrycode->setValue ();
			$this->view->lobjuserForm->statecode->setValue ($arrcellphone[1]);
			$this->view->lobjuserForm->cellPhone->setValue ($arrcellphone[2]);*/
				//$curmonth = $arrcellphone[1];
				
			//}
			
			
				$larrvenuetimeresults3 = $this->lobjstudentmodel->newupdatefnGetCitylistforcourse($resultss['Program'],$larrformData['Year']);
			    $this->lobjstudentForm->NewVenue->addMultiOptions($larrvenuetimeresults3);
				$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();
		        $this->lobjstudentForm->Program->addMultiOptions($larrbatchresult);
			
				$layear = $this->lobjstudentmodel->newfngetyear($resultss['Program']);
			     $this->lobjstudentForm->Year->addMultiOptions($layear);
 	                $this->view->ic1 = $dobs;
			$this->view->ic2 = $icno22;
			$this->view->ic3 = $icno333;
 		$this->view->idproge = $resultss['Program'];
 		$this->view->years3 = $resultss['Year'];
 		$this->view->city3 = $resultss['Examvenue'];
 		$this->view->yearss = $resultss['yearss'];
					$this->view->lobjstudentForm->FName->setValue($resultss['FName']);
					$this->view->lobjstudentForm->MName->setValue($resultss['MName']);
					$this->view->lobjstudentForm->LName->setValue($resultss['LName']);
					$this->view->lobjstudentForm->DateOfBirth->setValue($resultss['DateOfBirth']);
					$this->view->lobjstudentForm->PermCity->setValue($resultss['PermCity']);
					$this->view->lobjstudentForm->EmailAddress->setValue($resultss['EmailAddress']);		 	
					$this->view->lobjstudentForm->Takafuloperator->setValue($resultss['Takafuloperator']);
					$this->view->lobjstudentForm->ICNO->setValue($resultss['ICNO']);
					$this->view->lobjstudentForm->PermAddressDetails->setValue($resultss['PermAddressDetails']);	
					$this->view->lobjstudentForm->Gender->setValue($resultss['Gender']);	
					$this->view->lobjstudentForm->CorrAddress->setValue($resultss['CorrAddress']);	
					$this->view->lobjstudentForm->ArmyNo->setValue($resultss['ArmyNo']);	
					$this->view->lobjstudentForm->State->setValue($resultss['State']);
					$this->view->lobjstudentForm->PostalCode->setValue($resultss['PostalCode']);
					$this->view->lobjstudentForm->Race->setValue($resultss['Race']);
						$this->view->lobjstudentForm->Religion->setValue($resultss['Religion']);
				    $this->view->lobjstudentForm->ContactNo->setValue($resultss['ContactNo']);
				    $this->view->lobjstudentForm->MobileNo->setValue($resultss['MobileNo']);
				      $this->view->lobjstudentForm->Qualification->setValue($resultss['Qualification']);
				    $this->view->lobjstudentForm->Program->setValue($resultss['Program']);
				  /// $this->view->lobjstudentForm->Year->setValue($resultss['Year']);
				  $this->view->lobjstudentForm->login->setValue($resultss['EmailAddress']);
                   $this->view->lobjstudentForm->password->setValue($resultss['ICNO']);	
				    $this->view->lobjstudentForm->NewState->setValue($resultss['ExamState']);
				      $this->view->lobjstudentForm->NewVenue->setValue($resultss['Examvenue']);
				     $this->view->lobjstudentForm->NewCity->setValue($resultss['ExamCity']);
				     $this->view->lobjstudentForm->Year->setValue($resultss['yearss']);
				       $this->view->studentic=$resultss['ICNO'];
					
					
				       
		$this->view->id = $result;
		$lintidstudent = $result;
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetails($lintidstudent);
		
		
		$larrresult7 = $this->lobjstudentmodel->fnviewscoursedtudentdetails($lintidstudent);
		$this->view->programname =$larrresult7['ProgramName'];
		//echo "<pre/>";
	//print_r($larrresult);die();
		$idsechduler=$this->lobjstudentmodel->newfnGetVenuedetailsgetsecid($larrformData['Year']);
		//print_r($idsechduler['idnewscheduler']);
		$venueselect = $this->lobjstudentmodel->fnGetVenuedetailsRemainingseats($larrresult['yearss'],$larrresult['Year'],$larrresult['ExamCity'],$larrresult['Exammonth'],$larrresult['Examdate'],$larrresult['Examvenue']);
	//print_r($venueselect);die();
		$larrdate=$larrresult['Year'].'-'.$larrresult['Exammonth'].'-'.$larrresult['Examdate'];
		//echo $larrdate;die();
		//$contvenueselect = $this->lobjstudentmodel->fnCountVenuedetails($larrdate);
		
		$this->view->dates = $larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['yearss'];
		$dates=$larrresult['yearss'].'-'.$larrresult['Exammonth'].'-'.$larrresult['Examdate'];
		
		//$datesel=".$dates.";
	//	$day=day($dates);
		$result5 = $this->lobjstudentmodel->fngetdayStudent($dates); 
		//echo $result5[0]['days'];die();
		$this->view->daystu= $result5[0]['days'];
		$this->view->larrvenues = $venueselect;     
	            //instance for adding the lobjuserForm values to DB
	          // echo $result;exit;
				//echo '<script>window.open("'.$this->view->baseUrl().'/registration/selectvenue/insertedId/'.$result.'",\'Payment\',\'width=950, height=1000,left=80,top=50,scrollbars=1,resizable=1\');</script>';
				
				//$this->_redirect( $this->baseUrl . "/registration/newstudent/insertedId/".$result);
			}

		}
		
	//////////////////////////////////////////////////////////////////////////////////

	else if ($this->_request->isPost ()) {

			$larrformData = $this->_request->getPost ();

			
		/*	if(($larrformData['ArmyNo'] =='')&&(($larrformData['ICN1']=='')||($larrformData['ICN2']=='')||($larrformData['ICN3']=='')||($larrformData['ICN4']=='')||($larrformData['ICN5']=='') || ($larrformData['ICN6']=='')))
			{
				echo '<script language="javascript">alert("Please ente IC Number or Army Number")</script>';
				 echo "<script>parent.location = '".$this->view->baseUrl()."/registration';</script>";
				die();
		
			}*/
			$this->view->ic1 = $larrformData['ICN1'];
			$this->view->ic2 = $larrformData['ICN2'];
			$this->view->ic3 = $larrformData['ICN3'];
			$this->view->ic4 = $larrformData['ICN4'];
			$this->view->ic5 = $larrformData['ICN5'];
			$this->view->ic6 = $larrformData['ICN6'];
			
			$icno=$larrformData['ICN1'].''.$larrformData['ICN2'].''.$larrformData['ICN3'].''.$larrformData['ICN4'].''.$larrformData['ICN5'].''.$larrformData['ICN6'];
			
			$this->view->icno1=$icno;
			////////////////function to find the oter courses which he has obtained////////
			$larrresultcourses = $this->lobjstudentmodel->fngetappearedcourses($icno);
			$this->view->apperedcourses = $larrresultcourses;
			
			////////////////////end of the function////////////////////////////////////////
			$ArmyNo=$larrformData['ArmyNo'];
			
			$resltss = $this->lobjstudentmodel->fnGetICorArmyNo();
			$studentdetails=0;
			$flag=0;
			if($icno)
			{
			       for($i=0;$i<count($resltss);$i++)
				 	{
				 		
				 		if($icno == $resltss[$i]['ICNO'])
				 		{
							 	$studentdetails = $icno;
								$larrresults = $this->lobjstudentmodel->fnGetStudentdetailsbasedonicno($icno);
							 	$flag=1;
				 	    } 
				 	
				 	}
			}
			if($flag==0)
			{
			
					if($ArmyNo)
					{
						for($i=0;$i<count($resltss);$i++)
							{
								 		
								 if($ArmyNo == $resltss[$i]['ArmyNo'])
								 	{
										$studentdetails = $ArmyNo;
										$larrresults = $this->lobjstudentmodel->fnGetStudentdetailsbasedonarmyno($ArmyNo);
								 	 } 
								 	
							}
					}
			}//end if for comparing flag	
			
			 
      }//end if for comparing post of data
      $icno=$larrformData['ICN1'].''.$larrformData['ICN2'].''.$larrformData['ICN3'].''.$larrformData['ICN4'].''.$larrformData['ICN5'].''.$larrformData['ICN6'];
      $larresultofprogram = $this->lobjstudentmodel->fnGetProgramappeared($icno);
$values=0;
	foreach($larresultofprogram as $programms)
		{
			
			$value=$programms['Program'];
			$values=$values.','.$value;
		
		}
     
      		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramNameforunapplied($values);
		    $this->lobjstudentForm->Program->addMultiOptions($larrbatchresult);
		
      	$larrTakafuloperator = $this->lobjstudentmodel->fnTakafuloperator();
		$this->lobjstudentForm->Takafuloperator->addMultiOptions($larrTakafuloperator);
		
		$larrstate = $this->lobjstudentmodel->fnGetStateName();
		$this->lobjstudentForm->State->addMultiOptions($larrstate);
		
		$larrQualification = $this->lobjstudentmodel->fnGetEducationDetails();
		$this->lobjstudentForm->Qualification->addMultiOptions($larrQualification);
		
		
$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Race');
	 /*   print_r($larrdefmsresultset);
	    die();*/
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjstudentForm->Race->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
		
	$larrdefmsresultset = $this->lobjdeftype->fnGetDefinations('Religion');
	 /*   print_r($larrdefmsresultset);
	    die();*/
		foreach($larrdefmsresultset as $larrdefmsresult) {
			$this->lobjstudentForm->Religion->addMultiOption($larrdefmsresult['idDefinition'],$larrdefmsresult['DefinitionDesc']);
		}
		
 if(count($larrresults)==0)
 {
		//$this->_helper->layout->disableLayout();
		$this->view->lobjstudentForm = $this->lobjstudentForm; //send the lobjuserForm object to the view
		$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		
	/*	$larrbatchresult = $this->lobjstudentmodel->fnGetBatchName();
		$this->lobjstudentForm->IdBatch->addMultiOptions($larrbatchresult);*/
		

 }
 
 
	
 else 
 {
 	
 	$this->view->lobjstudentForm->FName->setValue($larrresults['FName']);
	$this->view->lobjstudentForm->MName->setValue($larrresults['MName']);
	$this->view->lobjstudentForm->LName->setValue($larrresults['LName']);
	$this->view->lobjstudentForm->DateOfBirth->setValue($larrresults['DateOfBirth']);
	$this->view->lobjstudentForm->PermCity->setValue($larrresults['PermCity']);
	$this->view->lobjstudentForm->EmailAddress->setValue($larrresults['EmailAddress']);		 	
	$this->view->lobjstudentForm->Takafuloperator->setValue($larrresults['Takafuloperator']);
	$this->view->lobjstudentForm->ICNO->setValue($larrresults['ICNO']);
	$this->view->lobjstudentForm->PermAddressDetails->setValue($larrresults['PermAddressDetails']);	
	$this->view->lobjstudentForm->Gender->setValue($larrresults['Gender']);	
	$this->view->lobjstudentForm->CorrAddress->setValue($larrresults['CorrAddress']);	
	$this->view->lobjstudentForm->ArmyNo->setValue($larrresults['ArmyNo']);	
	$this->view->lobjstudentForm->State->setValue($larrresults['State']);
	$this->view->lobjstudentForm->PostalCode->setValue($larrresults['PostalCode']);
	$this->view->lobjstudentForm->Race->setValue($larrresults['Race']);
	$this->view->lobjstudentForm->Religion->setValue($larrresults['Religion']);
    $this->view->lobjstudentForm->ContactNo->setValue($larrresults['ContactNo']);
    $this->view->lobjstudentForm->MobileNo->setValue($larrresults['MobileNo']);
    $this->view->lobjstudentForm->login->setValue($larrresults['EmailAddress']);
    $this->view->lobjstudentForm->password->setValue($larrresults['ICNO']);	
    $this->view->lobjstudentForm->Qualification->setValue($larrresults['Qualification']);
    $this->view->studentic=$larrresults['ICNO'];
 }	

 	
	    $this->view->studentic=$larrresults['ICNO'];

		
	$ldtsystemDate = date ( 'Y-m-d:H-i-s' );
		$this->view->lobjstudentForm->UpdDate->setValue ( $ldtsystemDate );
		//$larrQualification=$this->lobjstudentmodel->fnGetEducation();
		//$this->view->lobjstudentForm->Qualification->setValue($larrQualification);
		 
		$this->view->lobjstudentForm->UpdUser->setValue ( 1);
		$this->view->lobjstudentForm->ICNO->setValue($icno);
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post
		
		
		$larrformData['Examvenue']=$larrformData['NewVenue'];
		$resultstate = $this->lobjstudentmodel->fngetstatecity($larrformData['NewVenue']);
		$larrformData['ExamState']= $resultstate['state'];
		$larrformData['ExamCity']=$resultstate['city'];
	    $larrformData['NewState']= $resultstate['state'];
		$larrformData['NewCity']=$resultstate['city'];
		$larrformData['ICNO'] = $larrformData['ICN1'].''.$larrformData['ICN2'].''.$larrformData['ICN3'].''.$larrformData['ICN4'].''.$larrformData['ICN5'].''.$larrformData['ICN6'];
			
				
				if($larrformData['setmonth']<10)
				{
				$larrformData['setmonth']='0'.$larrformData['setmonth'];	
				}
				
		if($larrformData['setdate']<10)
				{
				$larrformData['setdate']='0'.$larrformData['setdate'];	
				}
				$selecteddate=$larrformData['Year'].'-'.$larrformData['setmonth'].'-'.$larrformData['setdate'];
				//echo $selecteddate;
				$incrementseats=$this->lobjstudentmodel->fnUpdateAllotedseats($selecteddate,$larrformData['NewVenue'],$larrformData['idsession']);
				$resultschedulerid = $this->lobjstudentmodel->fngetStudentscheduler($larrformData['NewVenue'],$larrformData['idsession'],$selecteddate);
				//echo "<pre/>";print_r($resultschedulerid['idnewscheduler']);die();
					$larrformData['hiddenscheduler']=$resultschedulerid['idnewscheduler'];
				$resultid = $this->lobjstudentmodel->fnAddStudent($larrformData); //instance for adding the lobjuserForm values to DB
				
				//echo $resultid;die();
		    $larrpaymentmode=$this->lobjstudentmodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$resultid);		
			if($larrformData['ModeofPayment']==1) {
            	$this->_redirect( $this->baseUrl . "/registration/fpxpageone/insertedId/".$resultid);                	
            }else if($larrformData['ModeofPayment']==2)	{
					$this->_redirect(  $this->baseUrl  . "/registration/confirmpayment/insertedId/".$resultid);
			} else {
				$this->_redirect(  $this->baseUrl  . "/registration/display/insertedId/".$resultid);	
			}
		
		
		}

		
	/*	$this->lobjstudentForm->PermCountry->addMultiOptions($lobjcountry);
		$this->lobjstudentForm->CorrsCountry->addMultiOptions($lobjcountry);*/
		
	}
	public function savedetailsAction()
	{
			$this->_helper->layout->disableLayout();
			if ($this->_request->isPost ()) {
			$larrformData = $this->_request->getPost ();
			$larrformData['UpdDate'] =  date ( 'Y-m-d:H-i-s' );
			$larrformData['UpdUser'] = 1;
			$larrformData['Qualification']=0;
			unset($larrformData['Save']);
			unset($larrformData['Close']);
			$larrformData['ICNO'] = $larrformData['ICN1'].''.$larrformData['ICN2'].''.$larrformData['ICN3'].''.$larrformData['ICN4'].''.$larrformData['ICN5'].''.$larrformData['ICN6'];
			unset($larrformData['ICN1']);
			unset($larrformData['ICN2']);
			unset($larrformData['ICN3']);
			unset($larrformData['ICN4']);
			unset($larrformData['ICN5']);
			unset($larrformData['ICN6']);
			/*print_r($larrformData);
			die();*/
			if($larrformData['IDApplication']!='')
			{
				
				$result=$larrformData['IDApplication'];
				//echo $result;
				//die();
	           $this->lobjstudentmodel->fnUpdateStudent($result,$larrformData); //instance for adding the lobjuserForm values to DB
				
				$this->_redirect( $this->baseUrl . "/registration/selectvenue/insertedId/".$result);
			}
			else 
			{
			
				$result = $this->lobjstudentmodel->fnAddStudent($larrformData); //instance for adding the lobjuserForm values to DB
				
				echo $result;
				$this->_redirect( $this->baseUrl . "/registration/selectvenue/insertedId/".$result);
			}
			}
	}
	
	public function selectvenueAction(){
		
			$this->_helper->layout->disableLayout();
			$this->view->lobjstudentForm = $this->lobjstudentForm; 
		$day = $this->_getParam('day');
		$month = $this->_getParam('month');
		$year = $this->_getParam('year');
		$venue = $this->_getParam('city');
		if($month <10){
			$month = '0'.$month;
		}
		if($day <10){
			$day = '0'.$day;
		}
		 $selecteddate = $year.'-'.$month.'-'.$day;

		$studentapp = new App_Model_Studentapplication();
		$sessresult = $studentapp->fnGetvenuedatescheduleDetails($selecteddate,$venue);
		
		//print_r($sessresult);die();
		$this->view->sessresult = $sessresult;
		$this->view->regdate=$selecteddate;
		
			$result5 = $this->lobjstudentmodel->fngetdayStudent($selecteddate); 
		//echo $result5[0]['days'];die();
		$this->view->daystu= $result5[0]['days'];
		//print_r($sessresult) ;die();

/*		if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post		
			$idvenue = $larrformData['idvenues'];		
			$arrworkphone = explode("-",$idvenue);
		    $idcenter= $arrworkphone [0];
	        $idsession= $arrworkphone[1];			
			$larrresuls = $this->lobjstudentmodel->fnupdateexamvenuess($idcenter,$idsession,$larrformData['idapplication']);
            $larrpaymentmode=$this->lobjstudentmodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$larrformData['idapplication']);		
			if($larrformData['ModeofPayment']==1) {
            	$this->_redirect( $this->baseUrl . "/registration/fpxpageone/insertedId/".$larrformData['idapplication']);                	
            }else if($larrformData['ModeofPayment']==2)	{
					$this->_redirect(  $this->baseUrl  . "/registration/newpaypalentry/insertedId/".$larrformData['idapplication']);
			} else {
				$this->_redirect(  $this->baseUrl  . "/registration/display/insertedId/".$larrformData['idapplication']);	
			}
                         
			
		}*/

		
	}
	
	//***************
	public function selectvenuetakafulAction(){
		
		$this->_helper->layout->disableLayout();
		$this->view->lobjstudentForm = $this->lobjstudentForm; 
		$day = $this->_getParam('day');
		$month = $this->_getParam('month');
		$year = $this->_getParam('year');
		$venue = $this->_getParam('city');
		if($month <10){
			$month = '0'.$month;
		}
		if($day <10){
			$day = '0'.$day;
		}
		 $selecteddate = $year.'-'.$month.'-'.$day;

		$studentapp = new App_Model_Studentapplication();
		$sessresult = $studentapp->fnGetvenuedatescheduleDetails($selecteddate,$venue);
		
		//print_r($sessresult);die();
		$this->view->sessresult = $sessresult;
		$this->view->regdate=$selecteddate;
		
			$result5 = $this->lobjstudentmodel->fngetdayStudent($selecteddate); 
		//echo $result5[0]['days'];die();
		$this->view->daystu= $result5[0]['days'];
		//print_r($sessresult) ;die();

/*		if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost (); //getting the values of lobjuserFormdata from post		
			$idvenue = $larrformData['idvenues'];		
			$arrworkphone = explode("-",$idvenue);
		    $idcenter= $arrworkphone [0];
	        $idsession= $arrworkphone[1];			
			$larrresuls = $this->lobjstudentmodel->fnupdateexamvenuess($idcenter,$idsession,$larrformData['idapplication']);
            $larrpaymentmode=$this->lobjstudentmodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$larrformData['idapplication']);		
			if($larrformData['ModeofPayment']==1) {
            	$this->_redirect( $this->baseUrl . "/registration/fpxpageone/insertedId/".$larrformData['idapplication']);                	
            }else if($larrformData['ModeofPayment']==2)	{
					$this->_redirect(  $this->baseUrl  . "/registration/newpaypalentry/insertedId/".$larrformData['idapplication']);
			} else {
				$this->_redirect(  $this->baseUrl  . "/registration/display/insertedId/".$larrformData['idapplication']);	
			}
                         
			
		}*/

		
	}
	
	
	
	///////////////////
	
	public function pdfexportAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$RegID = $this->_getParam('insertedId');
	    $larrstudetails= $this->lobjstudentmodel->fngetstudeappdetails($RegID); //get the report data
	    
		$CheckedValuesList = array();// declare an array 
		$CheckedValuesList[] = $larrstudetails['Studentname'];
		$CheckedValuesList[] = $larrstudetails['Venue'];
		$CheckedValuesList[] = $larrstudetails['ProgramName'];
		$CheckedValuesList[] = $larrstudetails['ExaminationFee'];
		$CheckedValuesList[] = $larrstudetails['ExamDate'];
		$CheckedValuesList[] = $larrstudetails['Session'];
		$CheckedValuesList[] = $larrstudetails['Address1'];
		$CheckedValuesList[] = $larrstudetails['Address2'];
		
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A4','','',20,15,10,16,9,9,'L');
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'                              '.'{PAGENO}{nbpg}' );
		// LOAD a stylesheet
		//$stylesheet = file_get_contents('../public/css/default.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Student" ).' '.$this->view->translate( "Application" );
		$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		
		$tabledata="<br>$CheckedValuesList[0]".','.'<br>';
		$tabledata.="$CheckedValuesList[6]<br>$CheckedValuesList[7]";
		$tabledata.= "<br><br>Dear".' '.$CheckedValuesList[0].','; 
        $tabledata.="<br><br><b><u>APPLICATION FOR TAKAFUL BASIC EXAMINATION (TBE).</u></b><br><br>
                     Thank you for applying for Takaful Basic Examination (TBE).<br>
                     You have selected :<br><br>";
		$tabledata.= "<table border='1' align=center width='80%'>
								<tr>
					 				<td><b>EXAMINATION CENTRE </b></td>
									<td>$CheckedValuesList[1]</td>
								</tr>
								<tr>
					 				<td><b>EXAMINATION TYPE </b></td>
									<td>$CheckedValuesList[2]</td>
								</tr>
								<tr>
					 				<td><b>EXAMINATION DATE </b></td>
									<td>$CheckedValuesList[4]</td>
								</tr>
								<tr>
					 				<td><b>EXAMINATION SESSION </b></td>
									<td>$CheckedValuesList[5]</td>
								</tr>
								<tr>
					 				<td><b>EXAMINATION FEE (RM) </b></td>
									<td>$CheckedValuesList[3]</td>
								</tr>
        					</table><br><br>";
        					
		$tabledata.= "IBFIM will issue the Examination Attendance Slip to enable you to sit for the examination.<br><br>
					  Please bring your duly certified certificate, Examinaton Attendance Slip, Identity card and<br>
                      calculator for the examination on the examination day itself. Verification process can be done<br>
                      before the examination date, or 1 hour before the examination begins.<br><br>
                      Please note that MTA and IBFIM have the rights to make changes to the date, venue and<br>
                      time of the examination and you will be notified accordingly.<br><br><br>
                      Thank You,<br><br>
                      Yours faithfully,<br><br>
                      From IBFIM<br><br><br>";
		
		$tabledata.="<br><br><br><br><br><br><p style='font-size:15px;color:red;'>*This is a computer generated mail. No signature is required.</p>";
		
		$mpdf->WriteHTML($tabledata);   
		$mpdf->Output('Application_Report.pdf','D');
		}
  	
	 public function printreportAction() 
     {			
			$this->_helper->viewRenderer->setNoRender(true);
			$this->_helper->layout->disableLayout();
		
			$IdApplication = ( int ) $this->_getParam ('insertedId');
		    
			//object to initialize ini file
			$lobjAppconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini','development');
									
		    try 
		    {	
	            //java class
	            $lobjdbdriverclass = new Java("java.lang.Class");
	            
	            //set db driver
	            $lobjdbdriverclass->forName("com.mysql.jdbc.Driver");
	
	            //driver manager object
	            $lobjdrivermanager = new Java("java.sql.DriverManager");
	            
	            //get the db connection
				$lstrConnection  =  "jdbc:mysql://".
										$lobjAppconfig->resources->db->params->host."/".
										$lobjAppconfig->resources->db->params->dbname."?user=".
										$lobjAppconfig->resources->db->params->username."&password=".
										$lobjAppconfig->resources->db->params->password;
														
				$lobjconnection = $lobjdrivermanager->getConnection($lstrConnection);
	            
	            //Jasper Compile manager object
	            $lobjcompileManager = new Java(
	            					"net.sf.jasperreports.engine.JasperCompileManager");
	            
	            echo "CompileManager object created</br>";
	            $lstrreportdir = realpath(".") . "/report/";
	            $lstrimagepath = realpath(".") . "/images/";
	
	             //compiled report path
	              $lobjreport = $lobjcompileManager->compileReport(realpath($lstrreportdir."ApplicationLetter.jrxml"));
	            
	            //Jasper Fill Manager object
	            $lobjfillManager = new Java(
	            					"net.sf.jasperreports.engine.JasperFillManager");
	            $int1 = new Java("java.lang.Integer");
	            //Hashmap object
	            //print_r($lstrreportdir);die();
	            $lobjparams = new Java("java.util.HashMap");
	          	$lobjparams->put("IDAPPLICATION",$IdApplication);
	          	$lobjparams->put ("IMAGEPATH", $lstrimagepath . "reportheader.jpg" );
	          	//$lobjparams->put("AMOUNTINWORDS",$AmountInWords['Amount']);
	          	//$lobjparams->put("STUDENTTYPE",$StudentType);
	           
	            echo "Fill Manager</br>";
	            					
	            //Jasper Print Object
	            $lobjjasperPrint = $lobjfillManager->fillReport(
	            					$lobjreport, $lobjparams, $lobjconnection);
	            					
	            echo "Jasper Printed</br>";
	            
	            //Jasper Export Manager object
	            $lobjexportManager = new Java(
	            					"net.sf.jasperreports.engine.JasperExportManager");
	            
	            //output file path
	            $lstrhtmloutputPath = realpath(".") . "/" . "output.html";
	            echo "Before Export</br>";
	            $session = Zend_Session::getId();
	            $lstrpdfoutputPath = realpath(".") . "/" . "$session.pdf";
	            $objStream = new Java("java.io.ByteArrayOutputStream");
	            $lobjexportManager->exportReportToPdfFile($lobjjasperPrint,$lstrpdfoutputPath);
	            
	            //Export report to HTML	            
	            echo 'HTML Exported</br>';
	
				header("Content-type: application/pdf;charset=utf-8;encoding=utf-8");
				header('Content-Disposition: attachment; filename="Application_Report.pdf"');
				
	            readfile($lstrpdfoutputPath);
	            unlink($lstrpdfoutputPath);
				echo "finished";	
		 		 		            
		    } 
		    catch (JavaException $lobjexception) 
		    {
		    	echo 'Exception caught: ', $lobjexception->getMessage() . "\n";
		    }		    		   
	}
	
	
public function newpaypalentryAction()
{
	    $lintidstudent = $this->_getParam('insertedId');
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($lintidstudent);	
		$this->view->data = $larrresult;
		$this->view->idstudent = $lintidstudent;		
	
		
		
		
		
		$postArray = $this->_request->getPost ();
		$this->view->paystatus =  0;
		if($postArray){
			echo"<pre>";
			print_r($postArray);die();			
				$paymentType = 'Sale';
				$amount = '1';	
				$firstName = $postArray['fname'];
				$lastName= $postArray['lname'];
				$address1   = $postArray['address1'];
				$address2   = $postArray['address2'];
				$city = $postArray['city'];
				$state= $postArray['state'];
				$zip = $postArray['zip'];
				$country = $postArray['cc_country'];		
				$phone      = $postArray['phone'];		
				$creditCardType = $postArray['cc_type'];
				$creditCardNumber  = $postArray['cc_number'];
				$cvv2Number = $postArray['cv2'];
				$expDateMonth  = $postArray['exp_month'];
				$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
				$expDateYear   = $postArray['exp_year'];			
                $currencyID = 'USD';
                
                $nvpStr =	"&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
			"&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
			"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
            /*    
                print_R($nvpStr);
                die();*/
               //////////////////////////function ////////////////////////////////////////////////////////
$environment = 'sandbox';
$methodName_='DoDirectPayment';
    /* $API_UserName = urlencode('my_api_username');
	$API_Password = urlencode('my_api_password');
	$API_Signature = urlencode('my_api_signature');*/

 /*$API_UserName = 'paypal_api1.oum.edu.my';
				$API_Password = 'PML4NER868SWDDB9';
				$API_Signature = 'gTIMjYuY9sRAkEl2Sz2ApRGAoOOfXlI4CoWujLUPk4Lw7gwHGS4';*/

				/*$API_UserName = 'ITSELL_1253107231_biz_api1.itwinetech.com';				
				$API_Password = '1253107242';				
				$API_Signature = 'ACgsMoS4ga9aN2thVap2nrnGYDURAutU.egjD2fdUhIGNTJXqQ5X5uwd';*/

				$API_UserName = 'ibb_api1.perfectvista.net';				
				$API_Password = 'RMMP25ATEC3PZJX8';				
				$API_Signature = 'AiPC9BjkCyDFQXbSkoZcgqH3hpacA4EAG6uE6TDmFNlGFx6LWKnsoGLG';
				
	$API_Endpoint = "https://api-3t.paypal.com/nvp";
	if("sandbox" === $environment || "beta-sandbox" === $environment) {
		$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
	}
	$version = urlencode('51.0');
 
	// Set the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
 
	// Turn off the server and peer verification (TrustManager Concept).
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
 
	// Set the API operation, version, and API signature in the request.
	$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr";
 print_R($nvpreq);
 die();
	// Set the request as a POST FIELD for curl.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
 
	// Get response from the server.
	$httpResponse = curl_exec($ch);
	
	
 
	if(!$httpResponse) {
		exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
	}
 
	// Extract the response details.
	$httpResponseAr = explode("&", $httpResponse);
 
	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}
 print_R($httpParsedResponseAr);
	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}
 
	
//////////////////////////////////////////////////////////////////////////////////

		}
}
public function paypalentryAction(){
	$lintidstudent = $this->_getParam('insertedId');
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($lintidstudent);	
		$this->view->data = $larrresult;
		$this->view->idstudent = $lintidstudent;		
	
		
		
		
		
		$postArray = $this->_request->getPost ();
		$this->view->paystatus =  0;
		if($postArray){	
			/*echo"<pre>";
			print_r($postArray);die();*/			
				require_once('paypal_base.php');
				
				$paymentAction = 'Sale';				
				$currencyId = 'MYR';				
				$this->view->salutation = $salutation = $postArray['salutation'];
				$this->view->fname = $fname      = $postArray['fname'];
				$this->view->lname = $lname      = $postArray['lname'];
				$this->view->address1 = $address1   = $postArray['address1'];
				$this->view->address2 = $address2   = $postArray['address2'];
				$this->view->city = $city       = $postArray['city'];
				$this->view->state = $state      = $postArray['state'];
				$this->view->zip = $zip        = $postArray['zip'];
				$this->view->cc_country = $cc_country = $postArray['cc_country'];		
				$this->view->phone = $phone      = $postArray['phone'];		
				$this->view->cc_type = $cc_type    = $postArray['cc_type'];
				$this->view->cc_number = $cc_number  = $postArray['cc_number'];
				$this->view->cv2 = $cv2        = $postArray['cv2'];
				$this->view->exp_month = $exp_month  = $postArray['exp_month'];
				$this->view->exp_year = $exp_year   = $postArray['exp_year'];			
				$this->view->email = $email       = $postArray['email'];//'paypal@example.net';				
				$item_desc   = 'Registration Fee For TBE';				
				$order_desc  = 'Registration Fee For TBE';
				$custom      = 'Registration Fee For TBE';				
				$invoice     =  date('U');				
				$ip  = $_SERVER['REMOTE_ADDR'];						
				$unique_session_id = session_id();				
				$item1 = $larrresult['Amount'];				
				$item_total  = $item1 ;							
				$order_total = $item_total;
				
				
				// Setting up the Authentication information
				// Such as Username, Password, Signature and Subject
		
				$this->lobjintialConfigmodel = new GeneralSetup_Model_DbTable_Initialconfiguration();		
				$InitialConfigDetails = $this->lobjintialConfigmodel->fnGetInitialConfigDetails(1);
				
				$InitialConfigDetails['PaypalCurrency'];
				
				
				$InitialConfigDetails['PayPalBusinessUserid'];
				$InitialConfigDetails['PayPalBusinessPassword'];
				$InitialConfigDetails['PayPalSignature'];
				
				//$API_USERNAME = 'ITSELL_1253107231_biz_api1.itwinetech.com';				
				//$API_PASSWORD = '1253107242';				
				//$API_SIGNATURE = 'ACgsMoS4ga9aN2thVap2nrnGYDURAutU.egjD2fdUhIGNTJXqQ5X5uwd';	
				
				$API_USERNAME = 'paypal_api1.oum.edu.my';
				$API_PASSWORD = 'PML4NER868SWDDB9';
				$API_SIGNATURE = 'gTIMjYuY9sRAkEl2Sz2ApRGAoOOfXlI4CoWujLUPk4Lw7gwHGS4';
				
				
				
				unset($postArray['Pay']);
				$postArray['REMOTE_ADDR'] = $ip;
				$postArray['currencyId'] = $currencyId;
				$postArray['UpdDate'] = date('Y-m-d h:i:s');
				$postArray['Amount'] = $larrresult['Amount'];
				$postArray['API_USERNAME'] = $API_USERNAME;
				$postArray['API_PASSWORD'] = $API_PASSWORD;
				$postArray['API_SIGNATURE'] = $API_SIGNATURE;
				$postArray['unique_session_id'] = $unique_session_id;
				
				$db = Zend_Db_Table::getDefaultAdapter();		
	    		$db->insert("tbl_paypaldetailslog",$postArray);	
				

				$API = new WebsitePaymentsPro();				
			/*	$API_USERNAME = 'ibb_api1.perfectvista.net';
				
				$API_PASSWORD = 'RMMP25ATEC3PZJX8';
				
				$API_SIGNATURE = 'AiPC9BjkCyDFQXbSkoZcgqH3hpacA4EAG6uE6TDmFNlGFx6LWKnsoGLG';*/
				
						
				$API->prepare($API_USERNAME, $API_PASSWORD, $API_SIGNATURE);
				
				
				// DoDirectPayment
				//==========================================================================================================
				
				$Paypal = $API->selectOperation('DoDirectPayment');
				
				$Address = PayPalTypes::AddressType($fname . ''. $lname, $address1, $address2, $city, $state, $zip, $cc_country, $phone);
				
				$PersonName = PayPalTypes::PersonNameType($salutation, $fname, '', $lname);
				
				$PayerInfo = PayPalTypes::PayerInfoType($email, 'israelekpo', 'verified', $PersonName, $cc_country, '', $Address);
				
				$CreditCardDetails = PayPalTypes::CreditCardDetailsType($cc_type, $cc_number, $exp_month, $exp_year, $PayerInfo, $cv2);
				
				$PaymentDetails = PayPalTypes::PaymentDetailsType($order_total, $item_total, $shipping, $handling, $tax, $order_desc, $custom, $invoice, '', 'http://phppaypalpro.sourceforge.net/ipn_notify.php', $Address);
				
				$Paypal->setParams($paymentAction, $PaymentDetails, $CreditCardDetails, $ip, $unique_session_id);
				
				$Paypal->addPaymentItem('Registration Charges', 'Registration Amount', 1, $tax1, $item1, $currencyId);
				
				$Paypal->execute();
				
				if ($Paypal->success())
				{
					$response = $Paypal->getAPIResponse();
				}
				else 
				{
					$response = $Paypal->getAPIException();
				}
				//PRINT_R($response);
				//DIE();
				
				
						
				
				
				if($response->Ack == "Failure"){
					$Errors = $response->Errors;					
					if($Errors->ShortMessage){
						$this->view->errorsmes =  $Errors->ShortMessage."<br>";
						$this->view->errorsmes .=  $Errors->LongMessage;
					}	
					else{
						$this->view->errorsmes =  $Errors[0]->ShortMessage."<br>";
						$this->view->errorsmes .=  $Errors[0]->LongMessage;
					}
					
					$this->view->paystatus =  2;				
					$postData = array(		
							'IDApplication' => $lintidstudent,	
           					'paymentFee' => $order_total,	
           					'grossAmount' => $order_total,	
            				'payerId' => $email,		
		            		'transactionId' =>"FAILED",  
            				'verifySign' =>"FAILED",                            
                            'UpdDate' => date('Y-m-d h:i:s'),	
            				'UpdUser' =>1,
                            'paymentStatus'=> 0											
						);	
					 $db = Zend_Db_Table::getDefaultAdapter();		
	    			 $db->insert("tbl_paypaldetails",$postData);
					
					
				}
		        else {
					$Errors = $response->Errors;
					$this->view->errorsmes =  "Payment Finished Sucessfully";
					$this->view->paystatus =  1;	

					
					
		        	$postArray['payment_status'] = $response->Ack;
					$postArray['payer_email'] = $email;
					$postArray['mc_gross'] = $order_total;
					$postArray['verify_sign'] = $postArray['payment_status'];
					$postArray['txn_id'] = $response->TransactionID;
					
					
					$myFile = "/var/www/tbe/Testfile.txt";
					$fh = fopen($myFile, 'a') or die("can't open file");
			
					$stringData = "\n\n Paypal Response \n".
					" Payment Status:".$postArray['payment_status']."\n".
					" Payer Email:".$postArray['payer_email']."\n".
					" Payment Amount:".$postArray['mc_gross']."\n".	
					" verify Sign:".$postArray['verify_sign']."\n".
					" Transaction Id:".$postArray['txn_id']."\n".						
					" Date".date("h:i:s d-M-Y")."\n\n\n\n---------------------------------------------------------------------";			
					fwrite($fh, $stringData);		
					fclose($fh);

				
				
					
					
					//-------------------------------
					$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					//$postArray['Regid']  = substr($postArray['txn_id'], 1, 6).rand(1000, 9999).substr($postArray['txn_id'], 5, 9);
					$postArray['Regid']  = $this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$lintidstudent);
					
					
					
					
					
					//$this->view->mess = "Payment Completed Sucessfully <br/> Please check your mail box If you have not received a confirmation mail in next 30minutes<br/>Please check your spam folder Add ibfiminfo@gmail.com to the address book to ensure future communications doesn�t go to the spam folder";
					
					$larrregid = $this->lobjstudentmodel->fngetRegid($lintidstudent);
					
					//Get Email Template Description
					$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
					//Get Student's Mailing Details
					$larrStudentMailingDetails = $larrresult;	
									
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
							
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										//$result = $mail->send($transport);
										$this->view->mess .= $lstrEmailTemplateBody;
								
					 				   try {
										$result = $mail->send($transport);
										
										} catch (Exception $e) {								
									
										}
				}			
				
		}	
		else{
			//echo "<pre>";
			//print_r($larrresult);
				$this->view->salutation = $salutation = $postArray['salutation'];
				$this->view->fname = $fname      = $postArray['fname'];
				$this->view->lname = $lname      = $postArray['lname'];
				$this->view->address1 = $address1   = $postArray['address1'];
				$this->view->address2 = $address2   = $postArray['address2'];
				$this->view->city = $city       = $postArray['city'];
				$this->view->state = $state      = $postArray['state'];
				$this->view->zip = $zip        = $postArray['zip'];
				$this->view->cc_country = $cc_country = $postArray['cc_country'];		
				$this->view->phone = $phone      = $postArray['phone'];		
				$this->view->cc_type = $cc_type    = $postArray['cc_type'];
				$this->view->cc_number = $cc_number  = $postArray['cc_number'];
				$this->view->cv2 = $cv2        = $postArray['cv2'];
				$this->view->exp_month = $exp_month  = $postArray['exp_month'];
				$this->view->exp_year = $exp_year   = $postArray['exp_year'];			
				$this->view->email = $email       = $postArray['email'];//'paypal@example.net';		
		}
}

public function confirmpaymentAction()
	{
		$lintidstudent = $this->_getParam('insertedId');
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($lintidstudent);	
		$this->view->data = $larrresult;
		$this->view->idstudent = $lintidstudent;		
		//$this->Url = "http://".$_SERVER['SERVER_NAME']."/tbe";
		//Get SMTP Mailing Server Setting Details
		$postArray = $this->_request->getPost ();
		

		//-------------------------------------------------------------------------------------------------
		

		
		//print_r($postArray);
		if($postArray){								
				if($postArray['payment_status'] = 'Completed'){
					
								
					$this->view->beforepayment = 3;					
					
					//print_r($postArray);
					//die();
					$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					//$postArray['Regid']  = substr($postArray['txn_id'], 1, 6).rand(1000, 9999).substr($postArray['txn_id'], 5, 9);
					$postArray['Regid']  = $this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$lintidstudent);	

					//print_r($postArray);
					if($postArray['Regid']=='')
					{
						//print_r("duplication");
						//die();
					}
					else
					{			
					//$this->view->mess = "Payment Completed Sucessfully";
					$this->view->mess = "Payment Completed Sucessfully <br/> Please check your mail box If you have not received a confirmation mail in next 30minutes<br/>Please check your spam folder Add ibfiminfo@gmail.com to the address book to ensure future communications doesn�t go to the spam folder";
					
					$larrregid = $this->lobjstudentmodel->fngetRegid($lintidstudent);
					
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
									
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'].'-'.$larrresult['CorrAddress'],$lstrEmailTemplateBody);
										
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
									/*	$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										//$result = $mail->send($transport);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
									$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									
								}
								
													
					}	
				}
				else {		
					$this->view->beforepayment = 2;				
					$this->view->mess = "Payment Failed";
				}	
				
			}
			else {			 	
			 	$this->view->beforepayment = 1;				
			}
			
			
	}


	
	public function confirmpaymentoldAction()
	{
		$lintidstudent = $this->_getParam('insertedId');
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($lintidstudent);	
		$this->view->data = $larrresult;
		$this->view->idstudent = $lintidstudent;		
		//$this->Url = "http://".$_SERVER['SERVER_NAME']."/tbe";
		//Get SMTP Mailing Server Setting Details
		$postArray = $this->_request->getPost ();
		
		if($postArray){						
				if($postArray['payment_status'] = 'Completed'){
					//print_r($postArray);
					//die();
					$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					//$postArray['Regid']  = substr($postArray['txn_id'], 1, 6).rand(1000, 9999).substr($postArray['txn_id'], 5, 9);
					$postArray['Regid']  = $this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$lintidstudent);	

					//print_r($postArray);
					if($postArray['Regid']=='')
					{
						//print_r("duplication");
						//die();
					}
					else
					{			
					$this->view->mess = "Payment Completed Sucessfully";
					
					$larrregid = $this->lobjstudentmodel->fngetRegid($lintidstudent);
					
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
									
							require_once('Zend/Mail.php');
							require_once('Zend/Mail/Transport/Smtp.php');			
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							
						
										$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'],$lstrEmailTemplateBody);
										
										$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['ICNO'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										
										
									/*	$to 	 = $larrresult["EmailAddress"];
										$subject = $lstrEmailTemplateSubject;
										$message = $lstrEmailTemplateBody;
										
										$from 	 = $lstrEmailTemplateFrom;
										$headers  = "From:" . $lstrEmailTemplateFrom;		
								  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										mail($to,$subject,$message,$headers);*/
										$auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
										$mail = new Zend_Mail();
										$mail->setBodyHtml($lstrEmailTemplateBody);
										$sender_email = 'ibfiminfo@gmail.com';
										$sender = 'ibfim';
										$receiver_email = $larrresult["EmailAddress"];
										$receiver = $larrresult['FName'];
										$mail->setFrom($sender_email, $sender)
											 ->addTo($receiver_email, $receiver)
									         ->setSubject($lstrEmailTemplateSubject);
										//$result = $mail->send($transport);
										unset($_SESSION['idApp']);
								$this->view->mess .= $lstrEmailTemplateBody;
								
					 				 try {
									$result = $mail->send($transport);
										
								} catch (Exception $e) {
									
									echo '<script language="javascript">alert("Because of server problem mails cannot be send this time")</script>';
				                	 echo "<script>parent.location = '".$this->view->baseUrl()."/registration/index';</script>";
				                	 die();
								}
								
								 $this->_redirect( $this->baseUrl . "/registration/index");
								if(mess){
									
								}
								//die();							
					}	
				}
				else {
					$this->view->mess = "Payment Failed";
				}
	
				 $this->_redirect( $this->baseUrl . "/registration/index");
				
			}
			
	}
	

	//Action To Get List Of States From Country Id
	public function fngetvenueAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidscheduler = $this->_getParam('idscheduler');
		
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetVenueName($lintidscheduler);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrbatchresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}
	
	//Action To Get List Of States From Country Id
	public function fngetvenuetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidvenue = $this->_getParam('idvenue');
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetVenueTime($lintidvenue);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}
	
public function fngetbatchAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		
		$idunit = 1;
		$larrinitconfigdetilas = $this->lobjstudentmodel->fnGetInitialConfigDetails($idunit);
		$days = $larrinitconfigdetilas['ClosingBatch'];
		$datetocmp = date('Y-m-d', strtotime($days.'days'));

		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetbatch($lintidprog,$datetocmp);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}
	
	
	
public function fngetschedulerdetailsAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidbatch = $this->_getParam('idbatch');
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetscheduler($lintidbatch);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	
	
	
	public function fngetdatetimeAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('date');
		$lintidvenue = $this->_getParam('idvenue');
		
	
		
		//echo $totalnoofstudents;
		
		/*if($noofseats == $totalnoofstudents)
		{
			echo "<script>alert('Select Other Date')</script>";
			$larrCountryStatesDetails="safsffsfsfsd";
			echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
			die();
		}*/
		
		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetTimingsForDate($lintdate,$lintidvenue);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);	
	}	
	
	/*
	 * functino to check the data and time 
	 */
	public function fngetdatetimevalueAction()
	{
			$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidvenuetime = $this->_getParam('idvenuetime');
		
		$larrresultnoofseats = $this->lobjstudentmodel->fnGetNoOfSeats($lintidvenuetime);
		$noofseats = $larrresultnoofseats['NoofSeats'];
	  // echo $noofseats;
		
		$larrnoofstudents = $this->lobjstudentmodel->fnGetNoofStudents($lintidvenuetime);
		$totalnoofstudents = $larrnoofstudents['count(IDApplication)'];
		// $totalnoofstudents;
		
		if($totalnoofstudents == $noofseats)
		{
			$value = '1';
			$alert =  'This Schedule has been completely filled please select other venue or time';
			$alert =$alert.'*****'.$value;
			echo $alert;
			
		}
		else 
		{
			$value = '0';
			$alert =  'This';
			$alert =$alert.'*****'.$value;
			echo $alert;
		}
		
		
	}
	
	/*
	 * function to find the takaful operator count
	 */
	public function fngettakafulidAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintididtakaful = $this->_getParam('idtakaful');
		$larrresultnoofseats = $this->lobjstudentmodel->fnGetNoOfTakafulOperator($lintididtakaful);
		$takafulseat = $larrresultnoofseats['NumberofSeat'];
		
		$larrresultnoofseatsinstudent = $this->lobjstudentmodel->fnGetNoOfTakafulOperatorForStudent($lintididtakaful);
		$studentoccupied = $larrresultnoofseatsinstudent['count(IDApplication)'];
		
	   if($takafulseat == $studentoccupied)
		{
			$value = '1';
			$alert =  'This Takaful Operator has been completed select other Takaful Operator';
			$alert =$alert.'*****'.$value;
			echo $alert;
			
		}
		else 
		{
			$value = '0';
			$alert =  'This';
			$alert =$alert.'*****'.$value;
			echo $alert;
		}
		
		
		
	}
	/*
	 * funcion for fees structure
	 */
	public function studentapplicationfeesAction()
	{
		$lintidstudent = (int)$this->_getParam('id');
		$larrresult = $this->lobjstudentmodel->fnGetStudentName($lintidstudent); 
		$this->view->studentname = $larrresult[''];
		$this->lobjstudentForm->populate($larrresult);
	}
	
	
	public function fngetprogramamountAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		//$larrprogamountresult = $this->lobjstudentmodel->fnGetProgAmount($lintidprog);
		$icno = $this->_getParam('icno');
		$larrstudentpassdetails = $this->lobjstudentmodel->fnstudentconfirm($lintidprog,$icno);
		//print_r($larrstudentpassdetails);
	//	die();
		
		 if($larrstudentpassdetails['pass']==2)
		 {
		 	$larrprogamountresult = $this->lobjstudentmodel->fnGetProgAmountfail($lintidprog);
		 }
		else 
		{
		$larrprogamountresult = $this->lobjstudentmodel->fnGetProgAmount($lintidprog);
		}
		echo $larrprogamountresult['sum(abc.amount)'];	
	}
	
	public function fngetprerequesitionAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		
		$larrprogid = $this->lobjstudentmodel->fnGetPreRequesition($lintidprog);
		$prerequest = $larrprogid['PreRequesition'];
		if($larrprogid['PreRequesition'] == 0)
		{
			die();
		}
		else 
		{
		  $larrprog = $this->lobjstudentmodel->fnGetPreRequesitionProgDetails($prerequest);
		  
		  echo 'Please bring along a copy of TBE Examination certificate for '.$larrprog['ProgramName'].' which must be certified as a true copy by the designated authorized person as agreed by IBFIM and MTA';
		  die();
		  
		 // die();
		}
		//die();
	}

public function fngetsameexamAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintidprog = $this->_getParam('idprog');
		$lintidicno = $this->_getParam('icno');
		
		$larrprogid = $this->lobjstudentmodel->fnGetsameexam($lintidprog,$lintidicno);
		if($larrprogid)
		{
			 echo 'You already taken this course';
		      die();
		  
		}
		
	}
public function fngetstatenameAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
			$year= $this->_getParam('year');

		$larrvenuetimeresult = $this->lobjstudentmodel->fnGetStatelistforcourse($Program,$year);
		//print_r($larrvenuetimeresult);
		//die();
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresult);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}

public function newfngetcitynamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
		$year = $this->_getParam('year');
		//$idseched = $this->_getParam('idsecheduler');
   $curmonth=date('m');
      
		if($curmonth<10)
		{
			$curmonth = $curmonth[1];
		}
		$larrvenuetimeresults = $this->lobjstudentmodel->newfnGetCitylistforcourse($Program,$year,$curmonth);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}
public function fngetcitynamesAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintdate = $this->_getParam('idstate');
		$Program = $this->_getParam('Program');
		$idseched = $this->_getParam('idsecheduler');

		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetCitylistforcourse($lintdate,$Program,$idseched);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}

public function fngetactivesetAction()
{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetActiveSet($Program);
		//print_r($larrvenuetimeresults);die();
		echo $larrvenuetimeresults[0]['IdBatch'];
		//$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}

public function fnstudentconfirmAction()
{
	 $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');
		$icno = $this->_getParam('icno');
		$email= $this->_getParam('Emails');
		
		
		$flag=1;
		
		
			//$day1=date('d');
		//$month1=date('m');
		//$year1=date('Y');
		/*echo $day."<br>";
		echo $month."<br>";
		echo $year;die();*/
		$larrpreviousdayresult = $this->lobjstudentmodel->fngetpreviousdays();
		$pevousday=$larrpreviousdayresult['previousday'];
		$pevousday="'$pevousday'";
		//echo $pevousday;die();
		//print_r();die();
				//$arrcellphone = explode("-",$larrpreviousdayresult['previousday']);
		//$day=$arrcellphone[2];
		//$month=$arrcellphone[1];
		//$year=$arrcellphone[0];
		
		$larrstudentpassdetails = $this->lobjstudentmodel->fnstudentconfirmvaliddate($Program,$icno,$pevousday);
		//print_r($larrstudentpassdetails);die();
		if($larrstudentpassdetails)
		{
			$flag=0;
		}
		//if($flag==1)
		//{
		//$larrstudentpassdetails = $this->lobjstudentmodel->fnstudentconfirm88($Program,$email,$day,$month,$year);
		//}
		//$larrstudentpassdetails = $this->lobjstudentmodel->fnstudentconfirm($Program,$icno);
		
		$pass = $larrstudentpassdetails['pass'];
		if($pass==3)
		{
			if($larrstudentpassdetails['Payment']==0)
			{
			$_SESSION['uniqueidapp']=$larrstudentpassdetails['IDApplication'];
			echo '2'.'***'.'This action will update/remove any of your previous attempt to register for the same course,are sure to continue?'.$_SESSION['uniqueidapp'];
			}
			else 
			{
				$_SESSION['uniqueidapp']=0;
				echo '1'.'***'.'You have already applied for the exam'.$_SESSION['uniqueidapp'];
			}
			//echo $_SESSION['idApp'];die();
			
			
		}
		else if($pass==1)
		{
			$_SESSION['uniqueidapp']=0;
			echo '1'.'***'.'You have already Passed the exam'.$_SESSION['uniqueidapp'];
		}
		else 
		{
			$_SESSION['uniqueidapp']=0;
			echo '0'.'***'.$_SESSION['uniqueidapp'];
		}
		
		
}
 public function fngetyearAction()
 {
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjstudentmodel->fnGetYearlistforcourse($Program);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
 }
 
 
 public function newfngetyearAction()
 {
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$Program = $this->_getParam('idprog');

		$larrvenuetimeresults = $this->lobjstudentmodel->newfngetyear($Program);
		//print_r($larrvenuetimeresults);die();
		$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
 }
 
	public function tempdaysAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day = $this->_getParam('day');
		$year = $this->_getParam('year');
		$month = $this->_getParam('month');
		$venue = $this->_getParam('city');
		if($month <10){
			$month = '0'.$month;
		}
		if($day <10){
			$day = '0'.$day;
		}
		$selecteddate = $year.'-'.$month.'-'.$day;
		$noofseats = $this->lobjstudentmodel->fnvalidateseats($venue,$selecteddate);
		
		

		$flag=0;
		foreach($noofseats as $ven){
		if($ven['Allotedseats']<$ven['Totalcapacity']) {
			$flag=1;
		}
		}
		if($flag==1){
			$flag=1;
		}
		echo $flag;
	}


public function caleshowoldAction()
{
	
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$Program = $this->_getParam('Program');
		$year = $this->_getParam('year');
		$city = $this->_getParam('city');
		
		$larrresultdisable = $this->lobjstudentmodel->fnGetDisabledate($city);
		$larrinitconfigdays = $this->lobjstudentmodel->initconfig();
		
		$closingdays = $larrinitconfigdays['ClosingBatch'];
		
		$larrdaysarresult = $this->lobjstudentmodel->fnGetDaysforcourse($Program,$year);
		//$larrdaysarresult = $this->lobjstudentmodel->fnGetDaysforcourse($Program,$year);
		$presentyear = $larrdaysarresult[0]['Year'];
		
		for($i=0;$i<count($larrdaysarresult);$i++)
		{
			$days[$i]=$larrdaysarresult[$i]['Days'];
		}
		//print_R($days);
		//die();
	
		$larrmonthresult = $this->lobjstudentmodel->fnGetMonths($year,$Program);		
		$frommonth = $larrmonthresult[0]['From'];
		//print_r($frommonth);
		//die();
		$tomonth = $larrmonthresult[0]['To'];
		$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<7;$j++)
		{
			if($days[$j]==1)
			  $monday=1;
			 if($days[$j]==2)
			  $tuesday=1;
			 if($days[$j]==3)
			  $wednesday=1;
			  if($days[$j]==4)
			  $thursday=1;
			  if($days[$j]==5)
			  $friday=1;
			  if($days[$j]==6)
			  $saturday=1;
			  if($days[$j]==7)
			  $sunday=1;
		}
		/*$monday =empty($larrdaysarresult[0]['Days'])? '0':'1';
		$tuesday = empty($larrdaysarresult[1]['Days'])?'0':'1';
		$wednesday = empty($larrdaysarresult[2]['Days'])?'0':'1';
		$thursday = empty($larrdaysarresult[3]['Days'])?'0':'1';
		$friday = empty($larrdaysarresult[4]['Days'])?'0':'1';
		$saturday = empty($larrdaysarresult[5]['Days'])?'0':'1';
		$sunday = empty($larrdaysarresult[6]['Days'])?'0':'1';*/
		
/*		
		
                                  $monday = $this->monday;
                                  $tuesday = $this->tuesday;
                                  $wednesday = $this->wednesday;
                                  $thursday = $this->thursday;
                                  $friday = $this->friday;
                                  $saturday = $this->saturday;
                                   $sunday = $this->sunday;
                                   
                    */      
		      $curmonth = date('m');  
$monat=date('n');
$jahr=$presentyear;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0  width=100% align=center>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{
	$cnt++;
	
	if($frommonth<=$cnt && $tomonth>=$cnt)
	{
		   if($cnt==$curmonth)
		   {
		   			
		   }
		   else if($cnt<$curmonth)
		   {
		
		   		$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:9pt; font-family:Verdana; " align=center>';}
				else{echo '<td style="font-size:10pt; font-family:Verdana" align=center>';}
				if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				
				else if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		   }
		   else 
		   {
		   	   // echo "123456";
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green;" align=center ';}
				else{echo '<td align=center ';}
				//if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
					if($cnt==4)
				{
						//$curdate = date('d')+14;
						
						while($i<=$insgesamt)
						{
						$rest=($i+$erster-1)%7;
						if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
						else{echo '<td  align=center ';}
						//$curdate = date('d')+14;
						if($i<12)
						{
							 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
						}
								
						else 
						
						{
						if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
						else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else{echo $i;}
						echo "</td>\n";
						}
						if($rest==0){echo "</tr>\n<tr>\n";}
						$i++;
						}
						
				}
                                else
				
				 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span  onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		   }
     }
///////
//////else
else 
{
	            $curmonth = date('m');
	           if($cnt!=$curmonth)
	           {
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:9pt; font-family:Verdana; " align=center>';}
				else{echo '<td style="font-size:10pt; font-family:Verdana" align=center>';}
				if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				
				else if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
	           }/////end of else
}

 $curmonth = date('m');
if($curmonth==$cnt)
{
				
				if($curmonth>=$frommonth)
				{//echo "aaaaaaaaaa";
		        $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = date('d')+14;
				if($i<$curdate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
						
				else 
				
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
				}
			else 
			{
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:9pt; font-family:Verdana; " align=center>';}
				else{echo '<td style="font-size:10pt; font-family:Verdana" align=center>';}
				if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				
				else if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '<span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
			}
}
}
echo '</tr>';
}
echo '</table>';
}

public function fngetmonthnameAction()
{
	 $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$Program = $this->_getParam('Program');
			$year= $this->_getParam('year');

		$larrvenuetimeresult = $this->lobjstudentmodel->fnbetweenmonths($year);
		//print_r($larrvenuetimeresult);
		//die();
		
		$frommonth = $larrvenuetimeresult['From'];
		$tomonth = $larrvenuetimeresult['To'];
		$year = $larrvenuetimeresult['Year'];
		$curyear=date('Y');
		if($curyear==$year)
		{
			$curmonth=date('m');
			
			if($frommonth<=$curmonth)
			{
				//echo $frommonth;die();
			$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween2($tomonth);
			}
			else 
			{
				$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween($frommonth,$tomonth);
			}
			}
		else 
		{
		$larrresults = $this->lobjBatchcandidatesmodel->fnGetmonthsbetween($frommonth,$tomonth);
		}
		//$larrmonthslist = $this->lobjstudentmodel->fnGetMonthlistofcourse($from,$to);
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresults);
		$larrCountryStatesDetails[]=array('key'=>'0','name'=>'Entire Calender');
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}


public function caleshowmontholdAction()
{
	    $this->lobjstudentmodel = new App_Model_Studentapplication(); 
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$idmonth = $this->_getParam('idmonth');
		$no = $this->_getParam('no');
		$year = $this->_getParam('Year');
		$Program = $this->_getParam('Program');
		
		$larrdaysarresult = $this->lobjstudentmodel->fnGetDaysforcourse($Program,$year);
		//echo "<pre/>";
		//print_R($larrdaysarresult);
		for($i=0;$i<count($larrdaysarresult);$i++)
		{
			$days[$i]=$larrdaysarresult[$i]['Days'];
		}
		//print_R($days);
		//die();
	
		$larrmonthresult = $this->lobjstudentmodel->fnGetMonths($year,$Program);		
		$frommonth = $larrmonthresult[0]['From'];
		$tomonth = $larrmonthresult[0]['To'];
		$yearss = $larrmonthresult[0]['Year'];
		//print_r($frommonth);
		//print_r($tomonth);
		//die();
		$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<7;$j++)
		{
			if($days[$j]==1)
			  $monday=1;
			 if($days[$j]==2)
			  $tuesday=1;
			 if($days[$j]==3)
			  $wednesday=1;
			  if($days[$j]==4)
			  $thursday=1;
			  if($days[$j]==5)
			  $friday=1;
			  if($days[$j]==6)
			  $saturday=1;
			  if($days[$j]==7)
			  $sunday=1;
		}
		/*$monday =empty($larrdaysarresult[0]['Days'])? '0':'1';
		$tuesday = empty($larrdaysarresult[1]['Days'])?'0':'1';
		$wednesday = empty($larrdaysarresult[2]['Days'])?'0':'1';
		$thursday = empty($larrdaysarresult[3]['Days'])?'0':'1';
		$friday = empty($larrdaysarresult[4]['Days'])?'0':'1';
		$saturday = empty($larrdaysarresult[5]['Days'])?'0':'1';
		$sunday = empty($larrdaysarresult[6]['Days'])?'0':'1';*/
		
/*		
		
                                  $monday = $this->monday;
                                  $tuesday = $this->tuesday;
                                  $wednesday = $this->wednesday;
                                  $thursday = $this->thursday;
                                  $friday = $this->friday;
                                  $saturday = $this->saturday;
                                   $sunday = $this->sunday;
                                   
                    */  
		 $curmonth = date('m');      
$monat=date('n');
$jahr=$yearss;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0  width=25% align=center>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{
	$cnt++;
		//print_r($cnt);  
	if($idmonth==$cnt)
	{
		
	
		if($idmonth==$curmonth)
		{
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				
				$curdate = date('d')+14;
				if($i<$curdate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
				//if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				else {
				
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
		else
			 {
			$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				if($cnt==4)
				{
						//$curdate = date('d')+14;
						$i=1;
						while($i<=$insgesamt)
						{
						$rest=($i+$erster-1)%7;
						if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
						else{echo '<td  align=center ';}
						//$curdate = date('d')+14;
						if($i<12)
						{
							 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{}
							echo "</td>\n";
						}
								
						else 
						
						{
						if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
						else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else{echo $i;}
						echo "</td>\n";
						}
						if($rest==0){echo "</tr>\n<tr>\n";}
						$i++;
						}
						
				}
				else 
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
				else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="#FEFFBF" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				else{echo $i;}
				echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
      ///////
     }
    
}
echo '</tr>';
}
echo '</table>';
}
	
	
public function fpxpageoneAction(){
		$this->_helper->layout()->setLayout('plain');		
		$lintidstudent = $this->_getParam('insertedId');
		$this->view->intidstudent  = $lintidstudent;
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($lintidstudent);	
		$this->view->data = $larrresult;
		unset($_SESSION["pageName"]);
		unset($_SESSION["StudsId"]);
		$_SESSION["pageName"] = "registration";	
		$_SESSION["StudsId"]  = $lintidstudent;	
	}	
public function fpxpagetwoAction(){	
		$this->_helper->layout()->setLayout('plain');			
		$lintidstudent = $this->_getParam('insertedId');
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($lintidstudent);	
		//print_r($larrresult);	
		$this->view->data = $larrresult;
		
	
		error_reporting(E_ALL);
		$address = "127.0.0.1";
		$service_port = 6000;
		// Create a TCP/IP socket. 
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($socket < 0){ 
			//echo "socket_create() failed: reason: " . socket_strerror($socket) . "\n"; 
		}
		else{ 
				//echo "Socket creation successfull."; 
		}				
		// Establish socket connection. 
		$result = socket_connect($socket, $address, $service_port);
		if (!$result){ 
			//echo "Socket connection failed.<br>";
			die();
		}
		else{ 
			//echo "Socket connection successfull.<br>"; 
		}				
		// Generating String to send to plugin. 
		$messageOrderNo = $_POST['TxnOrderNo'];
		$messageTXNTime = date('YmdHis');
		$sellerOrderNo = $_POST['TxnOrderNo'];
		$this->view->TxnAmount = $messageAmount = $_POST['TxnAmount'];
		$sellerID = $_POST['sellerID'];			
		
		$in = "message:request|message.type:AR|message.token:01|message.orderno:$messageOrderNo|message.ordercount:1|message.txntime:$messageTXNTime|message.serialno:1|message.currency:MYR|message.amount:$messageAmount|charge.type:AA|seller.orderno:$sellerOrderNo|seller.id:$sellerID|seller.bank:01|\n";
		$out = '';			
		
		socket_write($socket, $in);		
		while ($out = socket_read($socket,6001)){
			$fpxValue = $out;	
		}
		$sendFpxValue = str_replace("\n", "", $fpxValue);
		$this->view->sendFpxValue = $sendFpxValue;			
		socket_close($socket);
	}	
public function fpxreturnAction(){
		$this->_helper->layout()->setLayout('plain');	
		if(!$_SESSION["pageName"]) {
			$this->_redirect( $this->baseUrl);
		}
		$this->view->pageName = $_SESSION["pageName"];
		//error_reporting(E_ALL);
		/* Set the below to your plugin setting *///
		$service_port = 6000;
		$address = "127.0.0.1";
		/* Ensure Mesg is recv from FPX */
		if(!isset($_POST['mesgFromFpx'])){
			die("mesgFromFpx was not detected\n");
		}
		$mesgFromFpxs= stripslashes($_POST['mesgFromFpx']);
		$in = "message:response|response.string:".$mesgFromFpxs;
		$out = '';
		$fpxValue = '';	
		
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);		
		$result = socket_connect($socket, $address, $service_port);	
		socket_write($socket, $in."\n");	
		
		while ($out = socket_read($socket, 6001)){
			$fpxValue .= $out;
		}
		$this->view->fpxValue = $fpxValue;	
		socket_close($socket);			
		
		foreach(explode("|", $fpxValue) as $vars){
			list($key, $val) = explode(":",$vars);
			$msg[$key] = $val;
		}	
				
		if($msg['debit.authcode'] == '00' && $msg['credit.authcode'] == '00'){
			$dataArray['paymentStatus'] =1;				
		}
		elseif ($msg['debit.authcode'] == '99'){
			$dataArray['paymentStatus'] =0;
		}
		elseif ($msg['debit.authcode'] != '00' || $msg['debit.authcode'] != '' || $msg['debit.authcode'] != '99' ){
			$dataArray['paymentStatus'] =0;
		} 
		
		$dataArray['payerMailId'] =	$msg['buyer.id'];
		$dataArray['grossAmount'] =$msg['message.amount'];
		$dataArray['orderNumber'] =$msg['message.orderno'];
		$dataArray['TxnDate'] = date('Y-m-d:H-i-s');
		$dataArray['fpxTxnId'] =$msg['message.fpxTransactionId'];
		$dataArray['bankCode'] =$msg['buyer.bank'];
		$dataArray['bankBranch'] =$msg['buyer.bankbranch'];
		$dataArray['debitAuthCode'] =$msg['debit.authcode'];
		$dataArray['debitAuthNo'] =$msg['debit.authno'];
		$dataArray['creditAuthCode'] =$msg['credit.authcode'];
		$dataArray['creditAuthNo'] =$msg['credit.authno'];				
		$dataArray['UpdUser'] = 1;
		$dataArray['UpdDate'] = date('Y-m-d:H-i-s');

		
		if( $_SESSION["pageName"] == "registration"){
			  	 $lintidstudent =	$_SESSION["StudsId"];
			  	 $larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($lintidstudent);	
			  	
			  	 $this->view->redirectPage = $this->baseUrl;
			  	 $this->view->StdId = $_SESSION["StudsId"];
			   	 $dataArray['IDApplication'] = $_SESSION["StudsId"];		
				 $dataArray['entryFrom'] = 1;
			   	 $db = Zend_Db_Table::getDefaultAdapter();
		         $table = "tbl_registrationfpx";
				 $db->insert($table,$dataArray);	
				if($dataArray['paymentStatus'] == 1){
					   	 $ModelBatchlogin = new App_Model_Batchlogin();
						 $Regid = $ModelBatchlogin->fnGenerateCode($larrresult['Takafuloperator'],$lintidstudent);							
						 $table = "tbl_registereddetails";
				         $postData = array('Regid' =>   $Regid,	
				           					'IdBatch' =>$larrresult['IdBatch'],	
				         					'Approved' =>0,	
				         					'RegistrationPin'=>'0000000',
				         					'Cetreapproval'=>'0',
				           					'IDApplication' => $lintidstudent);					
					     $db->insert($table,$postData);
						 $lastid  = $db->lastInsertId("tbl_registereddetails","idregistereddetails");

						 $larrformDatanew['Payment'] = 1;	
						 $where = "IDApplication = '".$lintidstudent."'"; 	
						 $db->update('tbl_studentapplication',$larrformDatanew,$where);
				 
						$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
						$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
						//$postArray['Regid']  = substr($postArray['txn_id'], 1, 6).rand(1000, 9999).substr($postArray['txn_id'], 5, 9);
									
						$this->view->mess = "<b><font color='green'>Payment Completed Sucessfully</font></b>";
						$this->view->alertmess = "Payment Completed Sucessfully";
							
						$larrregid  = $this->lobjstudentmodel->fngetRegid($lintidstudent);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
								//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;	
											
						require_once('Zend/Mail.php');
						require_once('Zend/Mail/Transport/Smtp.php');	
						if($larrEmailTemplateDesc['TemplateFrom']!=""){		
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];							
									
							$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'],$lstrEmailTemplateBody);				
							
							$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[LoginId]",$larrregid['Regid'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['ICNO'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;										
	
							
							$this->view->receiver_email =  $larrStudentMailingDetails["EmailAddress"];
					    	$this->view->receiver =  $lstrStudentName;
					    	$this->view->EmailTemplateSubject =  $lstrEmailTemplateSubject;
					    	$_SESSION["EmailTemplateBody"]  = $lstrEmailTemplateBody;
					    	
					    	$this->view->mess .= $lstrEmailTemplateBody;
							/*	$to 	 = $larrresult["EmailAddress"];
							$subject = $lstrEmailTemplateSubject;
							$message = $lstrEmailTemplateBody;
							
							$from 	 = $lstrEmailTemplateFrom;
							$headers  = "From:" . $lstrEmailTemplateFrom;		
					  		$headers .= "\r\n".'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
							mail($to,$subject,$message,$headers);*/
	
						    /*	
						    $auth = 'ssl';
							$port = '465';
							$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
							$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
							$mail = new Zend_Mail();
							$mail->setBodyHtml($lstrEmailTemplateBody);
							$sender_email = 'ibfiminfo@gmail.com';
							$sender = 'ibfim';
							$receiver_email = $larrresult["EmailAddress"];
							$receiver = $larrresult['FName'];
							$mail->setFrom($sender_email, $sender)
								 ->addTo($receiver_email, $receiver)
						         ->setSubject($lstrEmailTemplateSubject);
							$result = $mail->send($transport);
							$this->view->mess .= $lstrEmailTemplateBody; */					   	
						}
						unset($_SESSION["pageName"]);
						unset($_SESSION["StudsId"]);
						unset($_SESSION['idApp']);
				}
				else{
					 $this->view->alertmess = "Payment Failed Please Try Again";
					 $this->view->mess = "<b><font color='red'>Payment Failed Please Try Again</font></b>";
				}					
			 echo "<div align='left'>".$this->view->mess."</div>";							
	   }	
	   
	   
	   if( $_SESSION["pageName"] == "companyapplication"){
	   	 //echo $_SESSION["CmpnyId"];
	   	 $lintinsertedId =  $_SESSION["CmpnyId"];	   	 
	   	 $idCompany      =  $_SESSION["idCompany"];
	   		
	   	 $this->view->redirectPage = $this->baseUrl."/tbe/companyapplication";
	   	 $this->view->StdId = $_SESSION["CmpnyId"];
	   	 $dataArray['IDApplication'] = $lintinsertedId;
	   	 $dataArray['entryFrom'] = 2;	
	   	 $db = Zend_Db_Table::getDefaultAdapter();
		 $table = "tbl_registrationfpx";
		 $db->insert($table,$dataArray);
	   	 if($dataArray['paymentStatus'] == 1){		
	   	 			$Companyapplicatmodel = new App_Model_Companyapplication();
	   	 			$larrresult = $Companyapplicatmodel->fngetCompanyDetails($idCompany);		
					//$larrPaymentDetails = $Companyapplicatmodel->fngetPaymentDetails($lintinsertedId);
				
	   				$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					$postArray['Regid']  = substr($dataArray['fpxTxnId'], 1, 6).rand(1000, 9999).substr($dataArray['fpxTxnId'], 5, 9);
					
					$db = Zend_Db_Table::getDefaultAdapter();
					$larrformData1['registrationPin'] = $postArray['Regid'];
					$larrformData1['paymentStatus'] = 1;	
					$larrformData1['Approved'] = 1;
					$where = "idBatchRegistration = '".$lintinsertedId."'"; 	
					$db->update('tbl_batchregistration',$larrformData1,$where); 
		 			
		 
					//$this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$this->gsessionbatch->idCompany,$lintinsertedId);	
					$this->view->Regid= $postArray['Regid'];	
					$this->view->mess = "<b><font color='green'>Payment Completed Sucessfully</font></b>";
					$this->view->alertmess = "Payment Completed Sucessfully";					
					$this->view->pageStatus = 2;	

					
						$StudModel = new App_Model_Studentapplication();
						$larrSMTPDetails  = $StudModel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $StudModel->fnGetEmailTemplateDescription("Batch Registration");
					
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];													
							
							$lstrCompanyName = $larrStudentMailingDetails['CompanyName'];
							
							$lstrEmailTemplateBody = str_replace("[Company]",$lstrCompanyName,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Amount]",$dataArray['grossAmount'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[TransactionId]",$dataArray['fpxTxnId'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[LoginId]",$postArray['Regid'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
					    	$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;									
							
					    	$this->view->receiver_email =  $larrStudentMailingDetails["Email"];
					    	$this->view->receiver =  $larrStudentMailingDetails['CompanyName'];
					    	$this->view->EmailTemplateSubject = $lstrEmailTemplateSubject;
					    	
					    	
					    	$this->view->mess = $lstrEmailTemplateBody;
					    	$_SESSION["EmailTemplateBody"]  = $lstrEmailTemplateBody;
							
						/*	try{
								$lobjProtocol->connect();
						   		$lobjProtocol->helo($lstrSMTPUsername);
								$lobjTransport->setConnection($lobjProtocol);
						 	
								//Intialize Zend Mailing Object
								$lobjMail = new Zend_Mail();
						
								$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
								$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
								$lobjMail->addHeader('MIME-Version', '1.0');
								$lobjMail->setSubject($lstrEmailTemplateSubject);
						
								for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
									if($larrEmailIds[$lintI] != ""){
										$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);	
																
										//replace tags with values
										//$Link = "<a href='".$this->Url."/registration'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Company]",$lstrStudentName,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$dataArray['grossAmount'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[TransactionId]",$dataArray['fpxTxnId'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$postArray['Regid'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										echo $lstrEmailTemplateBody;
										$lobjMail->setBodyHtml($lstrEmailTemplateBody);
								
										try {
											$lobjMail->send($lobjTransport);
										} catch (Exception $e) {
											$lstrMsg = "error";      				
										}	
										$lobjMail->clearRecipients();
										$this->view->mess .= $lstrEmailTemplateBody;
										$this->view->mess .= ". Login Details have been sent to user Email";
										unset($larrEmailIds[$lintI]);
									}
								}
							}catch(Exception $e){
								$lstrMsg = "error";
							}*/
					    	
					    	
					    

					    		/*$auth = 'ssl';
								$port = '465';
								$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
								$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
								$mail = new Zend_Mail();
								$mail->setBodyHtml($lstrEmailTemplateBody);
								$sender_email = 'ibfiminfo@gmail.com';
								$sender = 'ibfim';
								$receiver_email = $larrStudentMailingDetails["Email"];
								$receiver = $larrStudentMailingDetails['CompanyName'];
							
								$mail->setFrom($sender_email, $sender)
									 ->addTo($receiver_email, $receiver)
							         ->setSubject($lstrEmailTemplateSubject);
								$result = $mail->send($transport);
								$this->view->mess = $lstrEmailTemplateBody;*/
					    		unset($_SESSION["pageName"]);
							unset($_SESSION["CmpnyId"]);
							unset($_SESSION["idCompany"]);							
						}   	 
	   			 }
	   			 else{
					 $this->view->alertmess = "Payment Failed Please Try Again";
					 $this->view->mess = "<b><font color='red'>Payment Failed Please Try Again</font></b>";
				}	
	   	 echo "<div align='left'>".$this->view->mess."</div>";
	   }
		if( $_SESSION["pageName"] == "takafulapplication"){ 
			  	 $lintinsertedId =  $_SESSION["idTakaful"];	   	 
			   	 $idCompany      =  $_SESSION["InsertedId"];
			   	 $this->view->mess = "";	
			   	 $this->view->redirectPage = $this->baseUrl."/tbe/takafulapplication";
			   	 $this->view->StdId = $_SESSION["idTakaful"];

			   	 $dataArray['IDApplication'] = $lintinsertedId;
			   	 $dataArray['entryFrom'] = 3;	
			   	 $db = Zend_Db_Table::getDefaultAdapter();
				 $table = "tbl_registrationfpx";
				 $db->insert($table,$dataArray);
			   	 if($dataArray['paymentStatus'] == 1){		
	   	 			$Companyapplicatmodel = new App_Model_Takafulapplication();
	   	 			$larrresult = $Companyapplicatmodel->fngetTakafulOperator($lintinsertedId);
	
					//$larrPaymentDetails = $Companyapplicatmodel->fngetPaymentDetails($lintinsertedId);
				
	   				$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					$postArray['Regid']  = substr($dataArray['fpxTxnId'], 1, 6).rand(1000, 9999).substr($dataArray['fpxTxnId'], 5, 9);
					
					$db = Zend_Db_Table::getDefaultAdapter();
					$larrformData1['registrationPin'] = $postArray['Regid'];
					$larrformData1['paymentStatus'] = 1;	
					$larrformData1['Approved'] = 1;
					$where = "idBatchRegistration = '".$lintinsertedId."'"; 	
					$db->update('tbl_batchregistration',$larrformData1,$where); 
		 			
		 
					//$this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$idCompany,$lintinsertedId);	
					$this->view->Regid= $postArray['Regid'];	
					$this->view->mess = "<b><font color='green'>Payment Completed Sucessfully</font></b>";
					$this->view->alertmess = "Payment Completed Sucessfully";					
					$this->view->pageStatus = 2;	

					
						$StudModel = new App_Model_Studentapplication();
						$larrSMTPDetails  = $StudModel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $StudModel->fnGetEmailTemplateDescription("Batch Registration");
					
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];													
							
							$lstrCompanyName = $larrStudentMailingDetails['TakafulName'];
							
							$lstrEmailTemplateBody = str_replace("[Company]",$lstrCompanyName,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Amount]",$dataArray['grossAmount'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[TransactionId]",$dataArray['fpxTxnId'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[LoginId]",$postArray['Regid'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
					    	$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;									
							
					    	$this->view->receiver_email =  $larrStudentMailingDetails["email"];
					    	$this->view->receiver =  $larrStudentMailingDetails['TakafulName'];
					    	$this->view->EmailTemplateSubject = $lstrEmailTemplateSubject;					    	
					    	
					    	$this->view->mess = $lstrEmailTemplateBody;
					    	$_SESSION["EmailTemplateBody"]  = $lstrEmailTemplateBody;						
						
					    		unset($_SESSION["pageName"]);
							unset($_SESSION["idCompany"]);
							unset($_SESSION["idTakaful"]);							
						}   	 
	   			 }
	   			 else{
					 $this->view->alertmess = "Payment Failed Please Try Again";
					 $this->view->mess = "<b><font color='red'>Payment Failed Please Try Again</font></b>";
				}	
	   	 echo "<div align='left'>".$this->view->mess."</div>";				
	   }				
	}
public function fpxdirectreturnAction(){	
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		error_reporting(E_ALL);			
		//Step 1: Set the below to your plugin setting
		//$service_port = 9999;
		$service_port = 6000;
		$address = "127.0.0.1";
		$in = '';
		$out = '';
		$fpxValue = '';
		$outputstr = '';
		
		//Step 2: Setup the file to log since this page will run in the background.
		$fp = fopen("fpx_directmesg.log" ,'at');
		$outputstr = date("d-m-Y H:i:s ");  
		
		//Step 3: Check if the mesgFromFPX is provided.
		if ( !isset($_POST['mesgFromFPX'])){
		
			$outputstr = $outputstr.">>mesgFromFPX was not detected\r\n";
			fwrite ($fp, $outputstr );
			fclose($fp);
			die("mesgFromFpx was not detected\n");
		}
		$mesgFromFpxs= stripslashes($_POST['mesgFromFpx']);
		//Step 4: mesgFromFpx detected, Save to file.
		$in = "message:response|response.string:".$mesgFromFpxs;
		$outputstr  = $outputstr.">>".$in."\r\n";
		fwrite ($fp, $outputstr );
		
		//Step5: Create a TCP/IP socket to plugin
		$outputstr = date("d-m-Y H:i:s ");  
		$outputstr = $outputstr.">>Creating TCP/IP socket: ";
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($socket < 0)
		{ 
			$outputstr = $outputstr."socket_create() failed: Reason: ".socket_strerror($socket); 
			fwrite ($fp, $outputstr);
			die("Socket Create Error\n");
		}
		else
		{ 
			$outputstr = $outputstr."Socket creation ok: Result: ".socket_strerror(socket_last_error()); 
			fwrite ($fp, $outputstr);
		}
		
		
		//Step 6: Establish socket connection. 
		$outputstr = date("d-m-Y H:i:s ");  
		$outputstr = $outputstr.">>Attempting to connect to '$address' on port '$service_port': "; 
		$result = socket_connect($socket, $address, $service_port);
		if (!$result)
		{ 
			$outputstr = $outputstr."socket_connect() failed: Reason: ".socket_strerror(socket_last_error()); 
			fwrite ($fp, $outputstr );
			die("Socket Connect Error\n");
		
		}
		else
		{ 	
			$outputstr = $outputstr."Socket connection ok: Result: ". socket_strerror(socket_last_error()); 
			fwrite ($fp, $outputstr);
		}
		
		// Step 7: Sending FPX Message String to plugin. 
		$outputstr = date("d-m-Y H:i:s ");  
		$outputstr = $outputstr.">>Sending FPX Message String to plugin: "; 
		socket_write($socket, $in."\n");
		$outputstr = $outputstr."Sending string msg result." . socket_strerror(socket_last_error());
		fwrite ($fp, $outputstr );
		
		// Step 8: Sending String to plugin. 
		$outputstr = date("d-m-Y H:i:s ");  
		$outputstr = $outputstr.">>Reading plugin response: \r\n"; 
		fwrite ($fp, $outputstr );
		while ($out = socket_read($socket, 200))
		{
			$fpxValue .= $out;
		}
		$outputstr = date("d-m-Y H:i:s ");  
		$outputstr = $outputstr.">>".$fpxValue."\r\n"; 
		fwrite ($fp, $outputstr );
		
		//$outputstr = $outputstr.">>".phpinfo()."\r\n"; 
		//fwrite ($fp, $outputstr );
		
		// Step 9: Clean up
		socket_close($socket);
		fclose($fp);
		// No error detected, reponse OK to FPX.
		echo "OK";
	}
public function sendmailAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$lstrEmailTemplateBody 		= $_SESSION["EmailTemplateBody"];//html_entity_decode ($this->_getParam('EmailTemplateBody'));
		$lstrEmailTemplateSubject	= $this->_getParam('EmailTemplateSubject');
		$receiver_email 			= $this->_getParam('receiveremail');
		$receiver					= $this->_getParam('receiver');
		unset($_SESSION["EmailTemplateBody"]);
		$this->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$receiver_email,$receiver);		
	}
public function sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$receiver_email,$receiver){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$auth = 'ssl';
		$port = '465';
		$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
		$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
		$mail = new Zend_Mail();
		$mail->setBodyHtml($lstrEmailTemplateBody);
		$sender_email = 'ibfiminfo@gmail.com';
		$sender = 'ibfim';
		$mail->setFrom($sender_email, $sender)
			 ->addTo($receiver_email, $receiver)
	         ->setSubject($lstrEmailTemplateSubject);
		$mail->send($transport);			
	}


public function displayAction()
	{
		//$this->view->lobjstudentForm = $this->lobjstudentForm;
		$lintinsertedId = $this->_getParam('insertedId');	
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);
		$this->view->PaymentDetails = $larrPaymentDetails;
	}

public function showpopupsettingsAction(){
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();	
}

/*
 * scheduler exception
 */
public function schedulerexceptionAction()
{
	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintday = $this->_getParam('day');//city
		$lintcity = $this->_getParam('city');
		$lintmonth = $this->_getParam('month');
		
		$days = '2012-'.$lintmonth.'-'.$lintday;
		$resultsss = $this->lobjstudentmodel->fngetschedulerexception($days,$lintcity);
	$counts = count($resultsss);
	if($counts>1)
	{
		echo "No exams are offerred on the selected date. It can be a public holiday, please select a different date.";
		die();
		
	}
	else 
	{
		
	}	
}

public function newfnnewcaleshowoldAction()
{
	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		
		//Get Country Id
		$Program = $this->_getParam('Program');//cityyear
		$NewCity = $this->_getParam('NewCity');
			$year = $this->_getParam('year');
		$resultsss = $this->lobjstudentmodel->fnnewmonthcaleshow($Program,$NewCity,$year);
		
/*		$this->lobjCenterloginmodel = new App_Model_Centerlogin();
		$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($NewCity,date('Y'));
		foreach($resultcount as $resultcounts) {
			$maxcolourcodes[] = $resultcounts['countidmanagesession'];
		}
		$this->view->maxcolourcodes = max($maxcolourcodes);*/
		
		
		
		$curdate = date('Y-m-d');
		$larrlastregdate = $this->lobjstudentmodel->fngetnoofdaysforregistration();
		$lastregdate =$larrlastregdate['ClosingBatch'];
		$larrresultofdates = $this->lobjstudentmodel->fngetdatediffereceforscheduler($lastregdate+1);
		$iddates = explode("-",$larrresultofdates['nextdate']);
		
		$futuremonth = $iddates['1'];
		$futuredate = $iddates['2'];
		//////////////
		$this->lobjCenterloginmodel = new App_Model_Centerlogin(); 
	    $resultcount = $this->lobjCenterloginmodel->fnGetCountSession($NewCity,$year);

		$mondays="#FFFFF";
		$tuesdays="#FFFFF";
		$wednesdays="#FFFFF";
		$thursdays="#FFFFF";
		$fridays="#FFFFF";
		$saturdays="#FFFFF";
		$sundays="#FFFFF";
		for($k=0;$k<count($resultcount);$k++)
				{
			
					switch($resultcount[$k]['countidmanagesession'])
					{

						 Case 1:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="green";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="green";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="green";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="green";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="green";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="green";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="green";
							 	   	  break;
						 		}
						 		break;   

						 Case 2:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="skyblue";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="skyblue";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="skyblue";
							 	   	  break;
						 		}
						 		break; 
						 		
						 Case 3:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="violet";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="violet";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="violet";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="violet";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="violet";
							 	   	  break;
						 		}
						 		break; 
						
						 Case 4:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="pink";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="pink";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="pink";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="pink";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="pink";
							 	   	  break;
						 		}
						 		break; 
						}
				}
		//////////
		
		
		
		
		
		
		
		//print_r($resultsss);die();
	$values=0;
			for($i=0;$i<count($resultsss);$i++)
			{
				$value=$resultsss[$i]['idnewscheduler'];
				$values=$values.','.$value;
			}
			
			
		$larresultofmonths = $this->lobjstudentmodel->fnnewmonths($values);
		
	/*for($i=0;$i<count($resultsss);$i++)
	{
		$minfrommonth = $resultsss[$i]['From'];
		if($i!=0)
		{
			if($minfrommonth>$resultsss[$i]['From'])
				{
					$minfrommonth=$resultsss[$i]['From'];
				}
				
		      if($maxtomonth<$resultsss[$i]['To'])
				{
					$maxtomonth=$resultsss[$i]['To'];
				}
		}
		
	}*/
	$frommonth = $larresultofmonths[0]['minimum'];

	
			$tomonth = $larresultofmonths[0]['maximum'];

			
			//$resultsss22 = $this->lobjstudentmodel->fnnewmonthsrange($frommonth,$tomonth);
			//print_r($resultsss22);die();
     
		    $curmonth = date('m');  
$monat=date('n');
$jahr=$year;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0  width=100% align=center>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{

	$cnt++;
	
	
	$larrdays = $this->lobjstudentmodel->fngetdays($NewCity,$cnt,$year);
	if(count($larrdays)<1)
	{
	$larrdays = $this->lobjstudentmodel->fngetdaysto($NewCity,$cnt,$year);
	}
 	if(count($larrdays)<1)
	{
	$larrdays = $this->lobjstudentmodel->fngetdaysbetween($NewCity,$cnt,$year);
	}
/*	print_R($larrdays);
	die();*/
$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<count($larrdays);$j++)
		{
			/*print_r($larrdays);
			die();*/
			if($larrdays[$j]['Days']==1)
			  $monday=1;
			 if($larrdays[$j]['Days']==2)
			  $tuesday=1;
			 if($larrdays[$j]['Days']==3)
			  $wednesday=1;
			  if($larrdays[$j]['Days']==4)
			  $thursday=1;
			  if($larrdays[$j]['Days']==5)
			  $friday=1;
			  if($larrdays[$j]['Days']==6)
			  $saturday=1;
			  if($larrdays[$j]['Days']==7)
			  $sunday=1;
		}


 $curmonth = date('m');
if($curmonth==$cnt)
{
			
		        $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td  width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = date('d');
				if($futuremonth==$curmonth)
				{
					if($i<$futuredate)
					{
						 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					
						else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else{echo $i;}
						echo "</td>\n";
					}
					else 
					{
							if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
					}
				}		
				else 
				
				{
				 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					
						else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						else{echo $i;}
						echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
				//}
			
}
else
 {
 	
 	       if($cnt==$futuremonth)
 	       {
 	       	 $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				//$curdate = 0;
				
				if($i<$futuredate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
						
				else 
				
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
 	       }
 	       else if($cnt>$futuremonth)
 	       {
                $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td  width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = 0;
				
				if($i<$curdate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
						
				else 
				
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
 	       }
 	       
 	       else 
 	       {
 	       	 $this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				$curdate = 0;
			
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
			
				
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
 	       }
		}
  
}
echo '</tr>';
}
echo '</table>';
	
}


public function newfnnewmonthslistAction()
{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	$Program = $this->_getParam('Program');//cityyear
		$NewCity = $this->_getParam('NewCity');
			$year = $this->_getParam('year');
		$resultsss = $this->lobjstudentmodel->fnnewmonthcaleshowlatest($Program,$NewCity,$year);
		//print_r($resultsss);die();
	$values=0;
			for($i=0;$i<count($resultsss);$i++)
			{
				$value=$resultsss[$i]['idnewscheduler'];
				$values=$values.','.$value;
			}
			
			
	$larresultofmonths = $this->lobjstudentmodel->fnnewmonths($values);
	$frommonth = $larresultofmonths[0]['minimum'];

	
			$tomonth = $larresultofmonths[0]['maximum'];
    $curmonth = date('m');
			if($frommonth<$curmonth)
			{
				$frommonth = $curmonth;
			}
			
			$larrresults = $this->lobjstudentmodel->fnnewmonthsrange($frommonth,$tomonth);
			$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresults);
		$larrCountryStatesDetails[]=array('key'=>'0','name'=>'Entire Calender');
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
}


public function caleshowmonthsimplyAction()
{
	$this->lobjstudentmodel = new App_Model_Studentapplication(); 
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$NewCity = $this->_getParam('NewCity');
		$idmonth = $this->_getParam('idmonth');
		//$no = $this->_getParam('no');
		$year = $this->_getParam('Year');
		$Program = $this->_getParam('Program');
		
		$larrresult = $this->lobjstudentmodel->fngetmonthcalendar($idmonth,$year,$Program,$NewCity);
		print_r($larrresult);
		die();
}
public function caleshowmonthAction()
{
	$this->lobjstudentmodel = new App_Model_Studentapplication(); 
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$NewCity = $this->_getParam('NewCity');
		$idmonth = $this->_getParam('idmonth');
		//$no = $this->_getParam('no');
		$year = $this->_getParam('Year');
		$Program = $this->_getParam('Program');
		

		$nextmonth = $presentmonth+1;
		$this->lobjCenterloginmodel = new App_Model_Centerlogin(); 
		$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($NewCity,$year);
		
		

		$mondays="#FFFFF";
		$tuesdays="#FFFFF";
		$wednesdays="#FFFFF";
		$thursdays="#FFFFF";
		$fridays="#FFFFF";
		$saturdays="#FFFFF";
		$sundays="#FFFFF";
		
		for($k=0;$k<count($resultcount);$k++)
				{
			
					switch($resultcount[$k]['countidmanagesession'])
					{

						 Case 1:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="green";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="green";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="green";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="green";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="green";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="green";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="green";
							 	   	  break;
						 		}
						 		break;   

						 Case 2:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="skyblue";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="skyblue";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="skyblue";
							 	   	  break;
						 		}
						 		break; 
						 		
						 Case 3:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="violet";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="violet";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="violet";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="violet";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="violet";
							 	   	  break;
						 		}
						 		break; 
						
						 Case 4:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="pink";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="pink";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="pink";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="pink";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="pink";
							 	   	  break;
						 		}
						 		break; 
						}
				}
		//////////
		
		
		$larrresult = $this->lobjstudentmodel->fngetmonthcalendar($idmonth,$year,$Program,$NewCity);
		//print_r($larrresult);
	
		$monday=0;
		$tuesday=0;
		$wednesday=0;
		$thursday=0;
		$friday=0;
		$saturday=0;
		$sunday=0;
		for($j=0;$j<count($larrresult);$j++)
		{
			/*print_r($larrdays);
			die();*/
			if($larrresult[$j]['Days']==1)
			  $monday=1;
			 if($larrresult[$j]['Days']==2)
			  $tuesday=1;
			 if($larrresult[$j]['Days']==3)
			  $wednesday=1;
			  if($larrresult[$j]['Days']==4)
			  $thursday=1;
			  if($larrresult[$j]['Days']==5)
			  $friday=1;
			  if($larrresult[$j]['Days']==6)
			  $saturday=1;
			  if($larrresult[$j]['Days']==7)
			  $sunday=1;
		}
		

		 $curmonth = date('m');      
$monat=date('n');
$jahr=$year;
$heute=date('d');
$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
echo '<table border=0  width=25% align=center>';
echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
$cnt=0;
for($reihe=1;$reihe<=3;$reihe++)
{
echo '<tr>';
for ($spalte=1;$spalte<=4;$spalte++)
{
	$cnt++;
		//print_r($cnt);  
	if($idmonth==$cnt)
	{
		
	
		if($idmonth==$curmonth)
		{
				$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				
				$curdate = date('d');
				if($i<$curdate)
				{
					 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
				
					else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
					else{echo $i;}
					echo "</td>\n";
				}
				//if ($i==$heute && $this_month==$monat){echo '<span style="color:#000000;" >'.$i.'</span>';}
				
				else {
				
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
		else
			 {
			$this_month=($reihe-1)*4+$spalte;
				$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
				$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
				if($erster==0){$erster=7;}
				echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
				echo '<table width=80% border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
				echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
				echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
				echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
				echo '<tr>';
				$i=1;
				while($i<$erster){echo '<td> </td>'; $i++;}
				$i=1;
				while($i<=$insgesamt)
				{
				$rest=($i+$erster-1)%7;
				if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
				else{echo '<td  align=center ';}
				if($cnt==4)
				{
						//$curdate = date('d')+14;
						$i=1;
						while($i<=$insgesamt)
						{
						$rest=($i+$erster-1)%7;
						if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
						else{echo '<td  align=center ';}
						//$curdate = date('d')+14;
						if($i<12)
						{
							 if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
						
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{}
							echo "</td>\n";
						}
								
						else 
						
						{
							if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
						}
						if($rest==0){echo "</tr>\n<tr>\n";}
						$i++;
						}
						
				}
				else 
				{
				if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
				}
				if($rest==0){echo "</tr>\n<tr>\n";}
				$i++;
				}
				echo '</tr>';
				echo '</table>';
				echo '</td>';
		}
      ///////
     }
    
}
echo '</tr>';
}
echo '</table>';
}

public function newfnnewcaleshowAction()
{
	    $this->_helper->layout->disableLayout();
	    $Program = $this->_getParam('Program');
	    $year = $this->_getParam('year');
	    $NewVenue = $this->_getParam('NewCity');
	    
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');	
    	$db_link  = mysql_connect($config->resources->db->params->host, $config->resources->db->params->username, $config->resources->db->params->password) or die('Cannot connect to the DB');
		mysql_select_db($config->resources->db->params->dbname,$db_link) or die('Cannot select the DB');
			
		//$monthquery = "SELECT DATE_FORMAT( date,  '%m' ) AS monthk FROM  `tbl_venuedateschedule` where idvenue = '$NewVenue' AND year(date) = '$year' GROUP BY monthk";
       $monthquery = "SELECT DATE_FORMAT( date,  '%m' ) AS monthk FROM  `tbl_venuedateschedule` as a,tbl_newschedulercourse as b,tbl_newscheduler c where a.idvenue = '$NewVenue' AND year(a.date) = '$year' AND b.idnewscheduler=c.idnewscheduler and b.idnewscheduler=a.idnewscheduler and c.Active=1 and b.IdProgramMaster='$Program' GROUP BY monthk";
		//echo $monthquery;die();
		
		$resultmonth = mysql_query($monthquery,$db_link) or die('cannot get results!');
    	while($rowsk = mysql_fetch_assoc($resultmonth)) {
		  		$monthk['monthk'][]=  $rowsk;
			}
			
		foreach($monthk as $monthk)

		$monthcount = count($monthk); 

		echo "<table ><tr>";
		for($month=0;$month<$monthcount;$month++){
		   	$monthp =  $monthk[$month]['monthk'];
		   	//print_r($monthp);
		 	if($monthp==01)
		   	{
		   		 $monthname='January';
		   	}
		if($monthp==02)
		   	{
		   		 $monthname='Feburary';
		   	}
		if($monthp==03)
		   	{
		   		 $monthname='March';
		   	}
		   	if($monthp==04)
		   	{
		   		 $monthname='April';
		   	}
		if($monthp==05)
		   	{
		   		 $monthname='May';
		   	}
		if($monthp==06)
		   	{
		   		 $monthname='June';
		   	}
		if($monthp==07)
		   	{
		   		 $monthname='July';
		   	}
		if($monthp==08)
		   	{
		   		 $monthname='August';
		   	}
		if($monthp==09)
		   	{
		   		 $monthname='September';
		   	}
		if($monthp==10)
		   	{
		   		 $monthname='October';
		   	}
		if($monthp==11)
		   	{
		   		 $monthname='November';
		   	}
		if($monthp==12)
		   	{
		   		 $monthname='December';
		   	}
		   	
		   	
		$events = array();
		//$query = "SELECT idvenuedateschedule,idsession AS title, DATE_FORMAT(date,'%Y-%m-%d') AS event_date FROM tbl_venuedateschedule WHERE date LIKE '$year-$monthp%' AND Active='1' AND idvenue = '$NewVenue' ";
 	$query = "SELECT idvenuedateschedule,idsession AS title, DATE_FORMAT(date,'%Y-%m-%d') AS event_date FROM tbl_venuedateschedule as a,tbl_newschedulercourse as b,tbl_newscheduler c  WHERE a.date LIKE '$year-$monthp%' AND a.Active='1' AND a.idvenue = '$NewVenue' and b.idnewscheduler=c.idnewscheduler and b.idnewscheduler=a.idnewscheduler and c.Active=1 and b.IdProgramMaster='$Program'  ";
	
		$result = mysql_query($query,$db_link) or die('cannot get results!');
		while($row = mysql_fetch_assoc($result)) {
		  	$events[$row['event_date']][]=  $row;
		}
			
		if($month%3 == 1  && $month !=1)echo "<tr>";
			echo "<td  style='border : 1px solid black;' cellpadding='10px' align = 'center' valign = 'top'>";
			//echo date('F', mktime(0,0,0,$monthp)).$year;
			//$month_name = date( 'F', mktime(0, 0, 0, $monthp) );
			echo $monthname.$year;
			echo self::draw_calendar($monthp,$year,$events,$Program,$NewVenue);
			echo "</td>";
			if($month%3 == 0 && $month !=0 ) echo "</tr>";
		}
		//die();
		//echo $query;die();
		echo "</table>";
	}

	function draw_calendar($month,$year,$events = array(),$Program,$NewVenue){


		/* draw table */
		$calendar = '<table width="80%" border="1" align="center" style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
		/* table headings */
		$headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
		$calendar.= '<tr><td class="calendar-day-head" >'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';
			
		 /* days and weeks vars now ... */
		$running_day = date('w',mktime(0,0,0,$month,1,$year));
		$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
		$days_in_this_week = 1;
		$day_counter = 0; 
		$dates_array = array();
		/* row for week one */
		$calendar.= '<tr class="calendar-row">';
		/* print "blank" days until the first of the current week */
		//echo "<pre>";
		for($x = 0; $x < $running_day; $x++):
		$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
		$days_in_this_week++;
		endfor;
			  
		//$event_day = $year.'-'.$month.’-’.$list_day;
		/* keep going with days.... */
		for($list_day = 1; $list_day <= $days_in_month; $list_day++):
			$calendar.= '<td class="calendar-day">';
			/* add in the day number */
			if($list_day < 10) {
				$list_day = str_pad($list_day, 2, '0', STR_PAD_LEFT);
			}
			//$calendar.= $list_day;
			$event_day = $year.'-'.$month.'-'.$list_day;

	
 			if(isset($events[$event_day])) {
						
				foreach($events[$event_day] as $event) {
					
			    	$event1[$event['event_date']]  = $event['title'];
			    	
			     }
			  // print_r($countday); 
	
			     $mnc = "#FFFFFF";
			      if(count($events[$event_day]) == 1){
			       	$mnc = "green";
			      }
			      elseif(count($events[$event_day]) == 2){
			      	$mnc = "skyblue";
			      }
			      if(count($events[$event_day]) == 3){
			      	$mnc = "violet";
			      }elseif(count($events[$event_day]) == 4) {
			      	$mnc = "pink";
			      }
			      $calendar.= '<a href="#" ><div style = "background-color:'.$mnc.'; font-color:'.$mnc.'; width :30px; cursor:pointer; text-align:center; font-size:10pt;  border : 1px solid black;" " onclick="functp('.$list_day.','.$month.');">'.$list_day.'</div></a> ';
			   } else {
			     	$mnc = "#f6f6f6";
			      	 $calendar.= '<div style = "background-color:'.$mnc.';font-size:10pt;" >'.$list_day.'</div>';
			        //$calendar.= str_repeat('<p>&nbsp;</p>',2);
			   }
			   $calendar.= '</td>';
			   if($running_day == 6):
			      $calendar.= '</tr>';
			      if(($day_counter+1) != $days_in_month):
			      		$calendar.= '<tr class="calendar-row">';
			     endif;
			     $running_day = -1;
			     $days_in_this_week = 0;
			    endif;
			    $days_in_this_week++; $running_day++; $day_counter++;
			  	endfor;
			
			  /* finish the rest of the days in the week */
			  if($days_in_this_week < 8):
			    for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			      if($days_in_this_week != 1) $calendar.= '<td class="calendar-day-np">&nbsp;</td>';
			    endfor;
			  endif;
			
			  /* final row */
			  $calendar.= '</tr>';
			  /* end the table */
			  $calendar.= '</table><br>';
			
			  /** DEBUG **/
			  //$calendar = str_replace('</td>','</td>'."\n",$calendar);
			 // $calendar = str_replace('</tr>','</tr>'."\n",$calendar);
			  
			  /* all done, return result */
			  return $calendar;
}

	function random_number() {
		srand(time());
		return (rand() % 7);
	}
	
	function showsessionAction() {
		$this->_helper->layout->disableLayout();
		$this->view->lobjstudentForm = new App_Form_Studentapplication(); //intialize user lobjuserForm
		 $this->view->regdate = $regdate = $this->_getParam('regdate');
		 $program = $this->_getParam('Program');
		 $city = $this->_getParam('Venue');

		$studentapp = new App_Model_Studentapplication();
		$this->view->sessresult = $studentapp->fnGetvenuedatescheduleDetails($regdate,$city);
		if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost (); 
			print_r($larrformData);
		}
	}


	public function fngetemaildetailsAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintemailaddress = $this->_getParam('email');
		
		
		$larrbatchresult = $this->lobjstudentmodel->fnGetemailnameicno($lintemailaddress);
		$flag=0;
		if(count($larrbatchresult)>0)
		{
			$flag=$larrbatchresult['ICNO'];
		}
		
		//$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrbatchresult);
		echo $flag;die();
	}
	
	
	
  public function fngetmonthofsessionAction()
  {
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$idmonth = $this->_getParam('idmonth');
		$Program = $this->_getParam('Program');
		$Year = $this->_getParam('Year');

      $Newvenue = $this->_getParam('NewCity');
      
		$larrsessiontimeresults = $this->lobjnewscreenmodel->fnGetSessionforthemonth($Program,$Year,$Newvenue);
		//print_r($larrsessiontimeresults);die();
		$values=0;
		foreach($larrsessiontimeresults as $months)
		{
		if(( $idmonth >= $months['From']) && ($idmonth<= $months['To'] ))
		{
			
			$value=$months['idnewscheduler'];
				$values=$values.','.$value;
				
			
		}
		}
		//echo ($values);die();
			$larrexectdaysresults = $this->lobjnewscreenmodel->fnGetExectSessiondays($values);
		//print_r($larrexectdaysresults);die();
		//$i=0;
			foreach($larrexectdaysresults as $days)
			{
			$larrexectsessiontimeresults = $this->lobjnewscreenmodel->fnGetExectSession($values,$days['iddays']);
			//print_r($larrexectsessiontimeresults);die();
			$sessioncount[]=count($larrexectsessiontimeresults);	
			//echo count($larrexectsessiontimeresults);die();
			//$i++;
			}
		
			$maximumsesiion=max($sessioncount);
		//print_r($maximumsesiion);die();
		echo $maximumsesiion;die();
		
		//$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}


public function fngetyearofsessionAction()
{
	   $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();


		$Program = $this->_getParam('Program');
		$Year = $this->_getParam('Year');

      $Newvenue = $this->_getParam('NewCity');
      
		$larrsessiontimeresults = $this->lobjnewscreenmodel->fnGetSessionforthemonth($Program,$Year,$Newvenue);
		//print_r($larrsessiontimeresults);die();
		
		$values=0;
foreach($larrsessiontimeresults as $months)
		{
		
			
			$value=$months['idnewscheduler'];
				$values=$values.','.$value;
				
			
		
		}
		//echo ($values);die();
			$larrexectdaysresults = $this->lobjnewscreenmodel->fnGetExectSessiondays($values);
		//print_r($larrexectdaysresults);die();
		//$i=0;
			foreach($larrexectdaysresults as $days)
			{
			$larrexectsessiontimeresults = $this->lobjnewscreenmodel->fnGetExectSession($values,$days['iddays']);
			//print_r($larrexectsessiontimeresults);die();
			$sessioncount[]=count($larrexectsessiontimeresults);	
			//echo count($larrexectsessiontimeresults);die();
			//$i++;
			}
		
			$maximumsesiion=max($sessioncount);
		//print_r($maximumsesiion);die();
		echo $maximumsesiion;die();
		
		//$larrCountryStatesDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrvenuetimeresults);
		//echo Zend_Json_Encoder::encode($larrCountryStatesDetailss);
}

	
	public function fngetemaildetailprogramAction(){

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintemailaddress = $this->_getParam('Emails');
		$lintidprog = $this->_getParam('idprog');
			$linticno = $this->_getParam('icno');
			
		//	echo "<pre />";
		//	echo $lintemailaddress."<br/>";
		//	echo $lintidprog."<br/>";
				//echo $linticno."<br/>";
			
			//$larrpreviousdayresult = $this->lobjstudentmodel->fngetpreviousdays();
		$larrbatchresult = $this->lobjstudentmodel->fnGetemailprogramdetails($lintemailaddress,$lintidprog);
		//echo "<pre />";
		
		$flag=0;
		
		// echo $larrbatchresult['0'][`IDApplication`]; exit;
		if(count($larrbatchresult)>0)
		{ 
			foreach($larrbatchresult as $batches)
			{
				//echo $batches[ICNO]."<br/>";
				//echo "b";
				//echo $linticno."<br/>";
				
			if($linticno != $batches[ICNO])
			{
				//echo "a";
			    $flag=1;	
			}	
			
			}
				//echo $flag;
				//die();
		
		}
	
		//$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($larrbatchresult);
		echo $flag;die();
	}
	
}
