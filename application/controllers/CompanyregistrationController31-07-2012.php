<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class CompanyregistrationController  extends Zend_Controller_Action {
	private $lobjprogram;
	private $lobjprogramForm;
	private $lobjinitialconfigmodel;
	private $lobjdeftype;
	private $lobjform;
	private $lobjCommon;
	private $lobver;
	public $gsessionbatch;//Global Session Name
	private $_gobjlogger;
	public function init() {
		$this->_helper->layout()->setLayout('/single/usty1');
		$this->fnsetObj();
		$this->gsessionbatch = Zend_Registry::get('sis'); 
		 $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		//$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	   // Zend_Form::setDefaultTranslator($this->view->translate);
	}
	
	public function fnsetObj(){
		
		$this->lobjcenterModel = new GeneralSetup_Model_DbTable_Center(); //user model object
		$this->lobjTakafuloperatorModel = new App_Model_Companymaster();
		$this->lobjTakafuloperatorForm = new App_Form_Companymaster(); 
	  	$this->lobjinitialconfigmodel = new GeneralSetup_Model_DbTable_Initialconfiguration();
	  	$this->lobjdeftype = new App_Model_Definitiontype();
	  	$this->lobjform=new App_Form_Search ();
	  	$this->lobjCommon = new App_Model_Common(); 
		
	}
	
	public function indexAction() {
		
//		$this->_redirect('http://www.takafuleexam.com' );
//		exit;
    	$this->view->title="Add New Profram";
		$this->view->lobjTakafuloperatorForm = $this->lobjTakafuloperatorForm;
		$ldtsystemDate = date('Y-m-d H:i:s');
	
		$this->view->lobjTakafuloperatorForm->UpdDate->setValue($ldtsystemDate);
		//$auth = Zend_Auth::getInstance();
		$this->view->lobjTakafuloperatorForm->UpdUser->setValue(1);
		
		$this->view->fromweb = $this->_getParam('fromweb');

		$id=121;
		$this->lobjstate = new GeneralSetup_Model_DbTable_Statemaster();
		$lobjcountryname = $this->lobjstate ->fnCountryName($id);
		$this->view->lobjTakafuloperatorForm->IdCountry->setValue($lobjcountryname['CountryName']);
		$this->view->lobjTakafuloperatorForm->IdCountry->setAttrib('readonly',true); 
		
		$lobjstatesname = $this->lobjstate->fnGetStateslist($id);		
		$this->view->lobjTakafuloperatorForm->IdState->addMultiOptions($lobjstatesname);
		
		
		//$lobjcountry = $this->lobjcenterModel->fnGetCountryList();		
		//$this->lobjTakafuloperatorForm->IdCountry->addMultiOptions($lobjcountry);
		//$this->lobjTakafuloperatorForm->IdCountry->setValue($lobjcountry[121]);
		
		$lobjbusiness = $this->lobjTakafuloperatorModel->fnGetBusinesstypeList();
		$this->lobjTakafuloperatorForm->businesstype->addMultiOptions($lobjbusiness);
		
		$lobjquestion = $this->lobjTakafuloperatorModel->fnGetQuestiontypeList();
		$this->lobjTakafuloperatorForm->question->addMultiOptions($lobjquestion);
		
		$lobjcompanydetails=$this->lobjTakafuloperatorModel->fngetcompanymaster();
		$this->view->companydetails=$lobjcompanydetails;
		
		$lobjcompanyemails=$this->lobjTakafuloperatorModel->fngetcompaniesemail();
		$this->view->companiesmails=$lobjcompanyemails;
		//print_r($lobjcompanydetails);die();
		$idconfig=1;
		$larrconresults=$this->lobjinitialconfigmodel->fnGetInitialConfigDetails($idconfig);
		if($larrconresults['RegcodeType']==0)
		{
			$this->view->lobjTakafuloperatorForm->RegistrationNo->setAttrib('required',true);
		}
		else {
			$this->view->lobjTakafuloperatorForm->RegistrationNo->setAttrib ( 'readonly', true );
		}
		
		if ($this->getRequest()->isPost() && $this->_request->getPost ( 'Save' ) ) {
			$larrformData = $this->getRequest()->getPost();
				unset($larrformData['Save']);
					unset($larrformData['Back']);	
					$this->gsessionbatch->pass=$larrformData['Password'];
					//print_r($larrformData);die();
			if($larrformData['Password'] != $larrformData['ConPassword'])
			{
				
				echo '<script language="javascript">alert("New password & retype password is not matching")</script>';
			}
			else
			{    //print_r($larrformData);die();
				unset($larrformData['ConPassword']);
				
				$password = $larrformData['Password'];
				$larrformData['hint'] = $password;
				$larrformData['Password'] = md5($larrformData['Password']); 
				if($larrconresults['RegcodeType']!=0){
				$larrformData['RegistrationNo'] = 0;
				$stateid=$larrformData['IdState'];
				}
				else 
				{
				$larrresults = $this->lobjTakafuloperatorModel->fngetcompanymaster();
				foreach($larrresults as $res)	
				{
					if($larrformData['RegistrationNo']==$res['RegistrationNo'])
					{
						echo "<script>alert('the Register No  already there plz change')</script>";
						die();
						break;
					}
				}
				}
				$larrresult=$this->lobjTakafuloperatorModel->fnaddcompany($larrformData,$password);
				$auth = Zend_Auth::getInstance();// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Company Registration"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
				$lintIdRefundMaster = Zend_Db_Table::getDefaultAdapter()->lastInsertId('tbl_companies','IdCompany');
				//print_r($lintIdRefundMaster);die();
				if($larrconresults['RegcodeType']!=0){
					$this->lobjTakafuloperatorModel->fnGenerateCode($lintIdRefundMaster,$stateid);
				}
				
				$this->_redirect( $this->baseUrl . "/companyregistration/completion/id/$lintIdRefundMaster");
				//$this->_redirect( $this->baseUrl . '/batchlogin/login');
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'companymaster', 'action'=>'index'),'default',true));	//redirect	
         }    
		
		} 
	}
	public function  completionAction()
	{
	$lintId = $this->_getParam('id');
	$larrresultcom=$this->lobjTakafuloperatorModel->fnGetCompanydetailsview($lintId);
	if($this->gsessionbatch->pass) 
	$larrresultcom['Password'] =$this->gsessionbatch->pass;
	//print_r($larrresultcom);die();
	$this->view->pass= $larrresultcom['Password'];
	$this->view->login=$larrresultcom;
	//$this->view->question=$larrresultcom['quest'];
	//$this->view->answer=$larrresultcom['answer'];
	//$this->view->pass= $larrresultcom['Password'];
	unset($this->gsessionbatch->pass);
	}
	/*public function getcountrystateslistAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$lintIdCountry = $this->_getParam('idCountry');
        //$lintIdCountry = 121;
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjCommon->fnGetCountryStateList($lintIdCountry));
		
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);
	}*/
		
	public function getcitylistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdCountry = $this->_getParam('idCountry');//Get Country Id
		//get all the city that correspond to the selected state
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjTakafuloperatorModel->fnGetStateCityList($lintIdCountry));
		//$larrCountryStatesDetails[]=array('key'=>'0','name'=>'Others');//if the key is 0 set city as others 
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}	
	
	public function fngetcompanydetailsAction()
	{   
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view	
		$companyname=$this->_getParam('cname');
		$larrcompanynames = $this->lobjTakafuloperatorModel->fngetcompanyname($companyname);
	    if($larrcompanynames)
	    {
	    	 echo 'The company name provided already exists please provide with another company name';die();
	    }
	 else
	    {
	    	echo 2;die();
	    }
	}
	
	public function fngetemaildetailsAction()
	{   
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view	
		$email=$this->_getParam('ename');
		$larremail = $this->lobjTakafuloperatorModel->fngetemail($email);
	    if($larremail)
	    {
	    	 echo 'The Email ID provided already exists, please provide with another Email ID';
	    }
	 
	}
	
	public function fngetlogindetailsAction()
	{   
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view	
		$username=$this->_getParam('uname');
		$larrusernames = $this->lobjTakafuloperatorModel->fngetusername($username);
	    if($larrusernames)
	    {
	    	 echo 'The user name provided already exists, please provide with another user name';die();
	    }
	else
	    {
	    	echo 2;die();
	    }
	}
	
}