<?php
error_reporting ( E_ALL ^ E_WARNING );
error_reporting ( E_ALL ^ E_NOTICE );
class TakafulreportController extends Zend_Controller_Action { //Controller for the User Module
	
	public function init() { //initialization function
		$this->gsessionidtakafuloperator = Zend_Registry::get ( 'sis' );
		if (empty ( $this->gsessionidtakafuloperator->idtakafuloperator )) {
			$this->_redirect ( $this->baseUrl . '/takafullogin/logout' );
		}

		$this->_helper->layout ()->setLayout ( '/takaful/usty1' );
		$this->view->translate = Zend_Registry::get ( 'Zend_Translate' );
		Zend_Form::setDefaultTranslator ( $this->view->translate );
		$this->fnsetObj ();
	}
	

	public function fnsetObj() {
		$this->lobjTakafulreportmodel = new App_Model_Takafulreport();
		$this->lobjTakafulreportForm = new App_Form_Takafulreport();
		$this->lobjCommon = new App_Model_Common;
		$this->lobjform = new App_Form_Search ();
		$this->registry = Zend_Registry::getInstance ();
		$this->locale = $this->registry->get ( 'Zend_Locale' );
	}
	
	public function indexAction() {
		
		//echo date('Y-m-d');
		$this->view->id = 1;
		$this->view->lobjTakafulreportForm = $this->lobjTakafulreportForm;
		$year = date('Y');
		$month = date('m');
		//$this->lobjTakafulreportForm->Year->setValue($year);
		//$this->lobjTakafulreportForm->Month->setValue($month);
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			$this->view->id = 2;
			$idtakafuloperator = 16;//$this->gsessionidtakafuloperator->idtakafuloperator;
			$larrmonths = $this->lobjTakafulreportmodel->fnGetAllMonthList();
		    $this->lobjTakafulreportForm->Month->addMultiOptions($larrmonths);
			$larresult = $this->lobjTakafulreportmodel->fngetallstudents($larrformData,$idtakafuloperator);
			$totalcount = count($larresult);
			$var=0;
			$regids="'".$var;
			for($i=0;$i<$totalcount;$i++)
			{
				$regids =$regids."'".','."'".$larresult[$i]['registrationPin'];
			}
			$totalregids = $regids."'";
			$this->lobjTakafulreportForm->Year->setvalue($larrformData['Year']);
		    $this->lobjTakafulreportForm->Month->setvalue($larrformData['Month']);
		    
		    
		    
			$larrstudentdetails = $this->lobjTakafulreportmodel->fngetallstudentsdetails($totalregids,$larrformData);
			$this->view->result = $larrstudentdetails;
			
			
			//for individuals
			$larrstudentdetailsindividuals = $this->lobjTakafulreportmodel->fngetallstudentsdetailsindividuals($idtakafuloperator,$larrformData);
			$this->view->resultindividuals = $larrstudentdetailsindividuals;
			
			$totalprogdetails = $this->lobjTakafulreportmodel->fngetallprogram();
			$this->view->programslist = $totalprogdetails;
			
			$this->view->month = $larrformData['Month'];
			$this->view->year = $larrformData['Year'];
			$this->view->regpins = $totalregids;
			$this->view->idtakaful = $idtakafuloperator;
		}
	}
		
	public function gettomonthlistAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdyear = $this->_getParam('idyear');//Get Country Id
		$year= date("Y"); 
		if($lintIdyear==$year)
		{
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjTakafulreportmodel->fnGetMonthList());
		}
		else if($lintIdyear<$year) 
		{
			$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjTakafulreportmodel->fnGetAllMonthList());
		}
		
		
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}
	
	public function getonloadmonthlistAction()
	{
	$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$lintIdyear = $this->_getParam('idyear');//Get Country Id
		$year= date("Y"); 
		
		$larrCountryStatesDetails = $this->lobjCommon->fnResetArrayFromValuesToNames($this->lobjTakafulreportmodel->fnGetMonthList());
		
		echo Zend_Json_Encoder::encode($larrCountryStatesDetails);//send to view
	}
	
	public function printexcelAction()
	{
		$takafulmodel = new App_Model_Takafulreport();
			$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$year = $this->_getParam('year');
		$month = $this->_getParam('month');
		$idtakafuloperator = 16;//$this->gsessionidtakafuloperator->idtakafuloperator;
		
		$takafuloperatorresult = $this->lobjTakafulreportmodel->fngettakafulname($idtakafuloperator);
		$takafuloperatorname = $takafuloperatorresult['TakafulName'];
	
		
			$larresult = $this->lobjTakafulreportmodel->fngetallstudents($larrformData,$idtakafuloperator);
			$totalcount = count($larresult);
			$var=0;
			$regids="'".$var;
			for($i=0;$i<$totalcount;$i++)
			{
				$regids =$regids."'".','."'".$larresult[$i]['registrationPin'];
			}
			$totalregids = $regids."'";
			$larrformData['Month']=$month;
				$larrformData['Year']=$year;
			
			$larrstudentdetails = $this->lobjTakafulreportmodel->fngetallstudentsdetails($totalregids,$larrformData);
			$totalcounttakaful = count($larrstudentdetails);
			//for individuals
				$host = $_SERVER['SERVER_NAME'];
	$imgp = "http://".$host."/tbenew/images/reportheader.jpg";  
	$tabledata ='<html><body><table border=1 align=center width=100%><tr><td><img width=100% src= "'.$imgp.'" /></td></tr></table><br><br><br><br>';
			$tabledata.='<table>
								<th>Exam Date</th>
								<th>Program</th>
								<th>Registration Pin</th>
								<th>Student Name</th>
								<th>Exam Venue</th>
								<th>Grade</th>
								<th>Result</th>
								';
			 for($i=0;$i<($totalcounttakaful);$i++)
			 {
			 	  $examdate = date('d-m-Y',strtotime($larrstudentdetails[$i]['DateTime']));
			 	  $programname = $larrstudentdetails[$i]['ProgramName'];
			 	  $RegistrationPin = $larrstudentdetails[$i]['RegistrationPin'];
			 	  $FName = $larrstudentdetails[$i]['FName'];
			 	    $centername = $larrstudentdetails[$i]['centername'];
			 	  if($larrstudentdetails[$i]['pass']==1)
			 	  {
			 	  	$result = 'Pass';
			 	  }else if($larrstudentdetails[$i]['pass']==2)
			 	  {
			 	  	$result = 'Fail';
			 	  }
			 	  else if($larrstudentdetails[$i]['pass']==3)
			 	  {
			 	  	$result = 'Pending Exam';
			 	  }else if($larrstudentdetails[$i]['pass']==4)
			 	  {
			 	  	$result='Absent';
			 	  }
			 	  $larresultss = $takafulmodel->fngetmarks($larrstudentdetails[$i]['IDApplication']);
			 	  if(count($larresultss)>0)
			 	  {
			 	  	$marks = $larresultss[0]['Grade'];
			 	  }
			 	  else
			 	  {
			 	  	$marks = 'NA';
			 	  }
			 	$tabledata.='<tr><td>'.$examdate.'</td>
			 					<td>'.$programname.'</td>
			 					<td>'.$RegistrationPin.'</td>
			 					<td>'.$FName.'</td>
			 					<td>'.$centername.'</td>
			 					<td>'.$marks.'</td>
			 					<td>'.$result.'</td>
			 					</tr>';
			 }
			
			 
			$larrstudentdetailsindividuals = $this->lobjTakafulreportmodel->fngetallstudentsdetailsindividuals($idtakafuloperator,$larrformData);
			$totalcountindividuals = count($larrstudentdetailsindividuals);
			 for($j=0;$j<($totalcountindividuals);$j++)
			 {
			 	  $examdate = date('d-m-Y',strtotime($larrstudentdetailsindividuals[$j]['DateTime']));
			 	  $programname = $larrstudentdetailsindividuals[$j]['ProgramName'];
			 	  $RegistrationPin = 'NA';//$larrstudentdetailsindividuals[$j]['RegistrationPin'];
			 	  $FName = $larrstudentdetailsindividuals[$j]['FName'];
			 	    $centername = $larrstudentdetailsindividuals[$j]['centername'];
			 	  if($larrstudentdetailsindividuals[$j]['pass']==1)
			 	  {
			 	  	$result = 'Pass';
			 	  }else if($larrstudentdetailsindividuals[$j]['pass']==2)
			 	  {
			 	  	$result = 'Fail';
			 	  }
			 	  else if($larrstudentdetailsindividuals[$j]['pass']==3)
			 	  {
			 	  	$result = 'Pending Exam';
			 	  }else if($larrstudentdetailsindividuals[$j]['pass']==4)
			 	  {
			 	  	$result='Absent';
			 	  }
			 	  $larresultss = $takafulmodel->fngetmarks($larrstudentdetailsindividuals[$j]['IDApplication']);
			 	  if(count($larresultss)>0)
			 	  {
			 	  	$marks = $larresultss[0]['Grade'];
			 	  }
			 	  else
			 	  {
			 	  	$marks = 'NA';
			 	  }
			 	$tabledata.='<tr><td>'.$examdate.'</td>
			 					<td>'.$programname.'</td>
			 					<td>'.$RegistrationPin.'</td>
			 					<td>'.$FName.'</td>
			 					<td>'.$centername.'</td>
			 					<td>'.$marks.'</td>
			 					<td>'.$result.'</td>
			 					</tr>';
			 }
			
			 $tabledata.='</table>';
			 $filename=$takafuloperatorname.' Candidate Details';
			 	$ourFileName = realpath('.')."/data";
	$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
	ini_set('max_execution_time', 3600);
	fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
	fclose($ourFileHandle);
	header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
	header("Content-Disposition: attachment; filename=$filename.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	readfile($ourFileName);
	unlink($ourFileName);
			
			
	}
	
}