<?php
//error_reporting (E_ALL ^ E_WARNING);
//error_reporting (E_ALL ^ E_NOTICE);
class FpxcompletionController extends Zend_Controller_Action {
	 //Controller for the Mail sending module
	public function init(){   //initialization function	
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}	
	public function fnsetObj() {		
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjcommonmodel = new App_Model_Common(); //user model object
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
	}	
	public function indexAction() {		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		exit;
	
		$modelSendmail = new App_Model_Sendmailtostudents();
		$larrrfpxesult = $modelSendmail->fnGetFpxresults();	
		if(count($larrrfpxesult) > 20) $lvarcnt = 20;
		else $lvarcnt = count($larrrfpxesult);
		for($lvarj=0;$lvarj<$lvarcnt;$lvarj++){			
			$fpxesult =$larrrfpxesult[$lvarj]['mesgFromFpx'];
			$fpxesultArray = json_decode(json_encode((array) simplexml_load_string($fpxesult)),1);			
			
			if($fpxesultArray['ORDER_LIST']['DEBIT_AUTH_CODE'] == '00' && $fpxesultArray['ORDER_LIST']['CREDIT_AUTH_CODE'] == '00'){
				$dataArray['paymentStatus'] =1;				
			}
			elseif ($fpxesultArray['ORDER_LIST']['DEBIT_AUTH_CODE'] == '99'){
				$dataArray['paymentStatus'] =0;
			}
			elseif ($fpxesultArray['ORDER_LIST']['DEBIT_AUTH_CODE'] != '00' || $fpxesultArray['ORDER_LIST']['DEBIT_AUTH_CODE'] != '' || $fpxesultArray['ORDER_LIST']['DEBIT_AUTH_CODE'] != '99' ){
				$dataArray['paymentStatus'] =0;
			}  
			$detailsArray = $modelSendmail->fnGetAppIdFpxOrderNumber($fpxesultArray['ORDER_NO']);
			
			if(count($fpxesultArray['ORDER_LIST']['BUYER']['BUYER_ID'])>0)$dataArray['payerMailId'] =	$fpxesultArray['ORDER_LIST']['BUYER']['BUYER_ID'][0];
			$dataArray['grossAmount'] = $fpxesultArray['ORDER_LIST']['TXN_AMT'];
			$dataArray['orderNumber'] =$fpxesultArray['ORDER_NO'];
			$dataArray['TxnDate'] = date('Y-m-d:H-i-s');
			$dataArray['fpxTxnId'] =$fpxesultArray['FPX_TXN_ID'];
			$dataArray['bankCode'] =$fpxesultArray['ORDER_LIST']['BUYER']['BUYER_BANK'];
			$dataArray['bankBranch'] =$fpxesultArray['ORDER_LIST']['BUYER']['BUYER_BANK_BRANCH'];
			$dataArray['debitAuthCode'] =$fpxesultArray['ORDER_LIST']['DEBIT_AUTH_CODE'];
			$dataArray['debitAuthNo'] = $fpxesultArray['ORDER_LIST']['DEBIT_AUTH_NO'];
			$dataArray['creditAuthCode'] = $fpxesultArray['ORDER_LIST']['CREDIT_AUTH_CODE'];
			$dataArray['creditAuthNo'] = $fpxesultArray['ORDER_LIST']['CREDIT_AUTH_NO'];		
			$dataArray['UpdUser'] = 1;
			$dataArray['UpdDate'] = date('Y-m-d:H-i-s');
			if($detailsArray['entryFrom']==1){  //Registration
				 $dataArray['IDApplication'] = $detailsArray['IDApplication'];		
				 $dataArray['entryFrom'] = 1;
			   	 $db = Zend_Db_Table::getDefaultAdapter();
		         $table = "tbl_registrationfpx";
				 //$db->insert($table,$dataArray);			 
				 $lintidstudent =	$detailsArray['IDApplication'];	
			  	 $larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($lintidstudent);	
			     $larrStudentMailingDetails = $larrresult;
			     
			  	 $this->view->StdId = $detailsArray['IDApplication'];			   	 	
				 if($dataArray['paymentStatus'] == 1 && $larrresult['Payment'] == 0){
					   $ModelBatchlogin = new App_Model_Batchlogin();
					   $Regid = $ModelBatchlogin->fnGenerateCode($larrresult['Takafuloperator'],$lintidstudent);	
					  
					   $table = "tbl_registereddetails";
				       $postData = array('Regid' =>   $Regid,	
				           					'IdBatch' =>$larrresult['IdBatch'],	
				         					'Approved' =>1,	
				         					'RegistrationPin'=>'0000000',
				         					'Cetreapproval'=>'0',
				           					'IDApplication' => $lintidstudent);					
					  // $db->insert($table,$postData);
					  // $lastid  = $db->lastInsertId("tbl_registereddetails","idregistereddetails");

					   $larrformDatanew['Payment'] = 1;	
					   $where = "IDApplication = '".$lintidstudent."'"; 	
					   //$db->update('tbl_studentapplication',$larrformDatanew,$where);
				 
					   $postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					   $postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
						//$postArray['Regid']  = substr($postArray['txn_id'], 1, 6).rand(1000, 9999).substr($postArray['txn_id'], 5, 9);
									
					   $this->view->mess = "<b><font color='green'>Payment Completed Sucessfully</font></b>";
					   $this->view->alertmess = "Payment Completed Sucessfully";
							
					   $larrregid  = $this->lobjstudentmodel->fngetRegid($lintidstudent);						
						//Get Email Template Description
					   $larrEmailTemplateDesc =  $this->lobjstudentmodel->fnGetEmailTemplateDescription("Student Application");
						//Get Student's Mailing Details
							
											
						require_once('Zend/Mail.php');
						require_once('Zend/Mail/Transport/Smtp.php');	
						if($larrEmailTemplateDesc['TemplateFrom']!=""){		
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
							$larrEmailIds[0] = $larrStudentMailingDetails["EmailAddress"];
							$larrNames[0] 	 = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];
							$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];							
									
							$lstrEmailTemplateBody = str_replace("[Candidate]",$larrresult['FName'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[ICNO]",$larrresult['ICNO'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Program]",$larrresult['ProgramName'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[venue]",$larrresult['centername'].' '.$larrresult['addr1'].' '.$larrresult['addr2'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Date]",$larrresult['Examdate'].'-'.$larrresult['Exammonth'].'-'.$larrresult['years'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Address]",$larrresult['PermAddressDetails'],$lstrEmailTemplateBody);				
							
							$lstrEmailTemplateBody = str_replace("[Amount]",$larrresult['Amount'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[LoginId]",$Regid,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[username]",$larrresult["EmailAddress"],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['ICNO'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['password'],$lstrEmailTemplateBody);
							
							$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname']." (".$larrresult['ampmstart']."-".$larrresult['ampmend'].")",$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;							
							$this->view->receiver_email =  $larrStudentMailingDetails["EmailAddress"];
					    	$this->view->receiver =  $lstrStudentName;
					    	$this->view->EmailTemplateSubject =  $lstrEmailTemplateSubject;
					    	$_SESSION["EmailTemplateBody"]  = $lstrEmailTemplateBody;
					    	
					    	$this->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$larrStudentMailingDetails["EmailAddress"],$lstrStudentName);
					    	$larrformDatanew2['mailSent'] = 1;	
							$where = "idFpxXml = '".$larrrfpxesult[$lvarj]['idFpxXml']."'"; 	
							//$db->update('tbl_fpxxml',$larrformDatanew2,$where);				    				   	
						}						
				}
				else if($larrresult['Payment'] == 1){
					
				}
				else{
						$lstrStudentName = $larrStudentMailingDetails['FName'].' '.$larrStudentMailingDetails['MName'].' '.$larrStudentMailingDetails['LName'];							
						
					    $lstrEmailTemplateSubject = "Payment Failed.";
					    $lstrEmailTemplateBody    = "<br><br> Payment of Student ".$lstrStudentName." for Amount RM".$dataArray['grossAmount']." Faild.<br> The Order number is: ".$dataArray['orderNumber'];
					    $lstrEmailTemplateBody    .= "<br> Debit Authentication Code is : ".$dataArray['debitAuthCode']."<br> Credit Authentication Code is : ".$dataArray['creditAuthCode'];
					    $lstrEmailTemplateBody    .= "<br> Payment Bank Code is : ".$dataArray['bankCode']."<br> Branch Code is : ".$dataArray['bankBranch'].".<br> Transaction id is: ".$dataArray['fpxTxnId'];
						$lstrEmailTemplateBody    .= "<br> Thank you";
					    $this->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$larrStudentMailingDetails["EmailAddress"],$lstrStudentName);
					    
					    $larrformDatanew2['mailSent'] = 2;	
						$where = "idFpxXml = '".$larrrfpxesult[$lvarj]['idFpxXml']."'"; 	
						//$db->update('tbl_fpxxml',$larrformDatanew2,$where);						    	
				}				
			}
			else if($detailsArray['entryFrom']==2){  //Company Application

				 $lintinsertedId = $detailsArray['IDApplication']; 	   	 
	   	 		 $idCompany      =  $detailsArray['IDApplication'];   		
			   	 $this->view->StdId =  $detailsArray['IDApplication']; 
			   	 $dataArray['IDApplication'] = $lintinsertedId;
			   	 $dataArray['entryFrom'] = 2;	
			   	 $db = Zend_Db_Table::getDefaultAdapter();
				 $table = "tbl_registrationfpx";
				 //$db->insert($table,$dataArray);
				 $Companyapplicatmodel = new App_Model_Companyapplication();
				 $larrresult = $Companyapplicatmodel->fngetCompanyDetails($idCompany);
				 //Get Student's Mailing Details
				$larrStudentMailingDetails = $larrresult;
	   			 if($dataArray['paymentStatus'] == 1){		   	 			
	   	 					
					//$larrPaymentDetails = $Companyapplicatmodel->fngetPaymentDetails($lintinsertedId);
				
	   				$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					$postArray['Regid']  = substr($dataArray['fpxTxnId'], 1, 6).rand(1000, 9999).substr($dataArray['fpxTxnId'], 5, 9);
					
					$db = Zend_Db_Table::getDefaultAdapter();
					$larrformData1['registrationPin'] = $postArray['Regid'];
					$larrformData1['paymentStatus'] = 1;	
					$larrformData1['Approved'] = 1;
					$where = "idBatchRegistration = '".$lintinsertedId."'"; 	
					//$db->update('tbl_batchregistration',$larrformData1,$where); 
		 			
		 
					//$this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$this->gsessionbatch->idCompany,$lintinsertedId);	
					$this->view->Regid= $postArray['Regid'];	
					$this->view->mess = "<b><font color='green'>Payment Completed Sucessfully</font></b>";
					$this->view->alertmess = "Payment Completed Sucessfully";					
					$this->view->pageStatus = 2;	

					
						$StudModel = new App_Model_Studentapplication();
						$larrSMTPDetails  = $StudModel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $StudModel->fnGetEmailTemplateDescription("Batch Registration");
					
						
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];													
							
							$lstrCompanyName = $larrStudentMailingDetails['CompanyName'];
							
							$lstrEmailTemplateBody = str_replace("[Company]",$lstrCompanyName,$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Amount]",$dataArray['grossAmount'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[TransactionId]",$dataArray['fpxTxnId'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[LoginId]",$postArray['Regid'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Session]",$larrresult['managesessionname'],$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
					    	$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;									
							
					    	$this->view->receiver_email =  $larrStudentMailingDetails["Email"];
					    	$this->view->receiver =  $larrStudentMailingDetails['CompanyName'];
					    	$this->view->EmailTemplateSubject = $lstrEmailTemplateSubject;
					    	
					    	
					    	$this->view->mess = $lstrEmailTemplateBody;
					    	//$_SESSION["EmailTemplateBody"]  = $lstrEmailTemplateBody;
					    	$this->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$larrStudentMailingDetails["Email"],$larrStudentMailingDetails['CompanyName']);
					    	
				 	
				 	
				 			$larrformDatanew2['mailSent'] = 1;	
					  		$where = "idFpxXml = '".$larrrfpxesult[$lvarj]['idFpxXml']."'"; 	
					   		//$db->update('tbl_fpxxml',$larrformDatanew2,$where);				 	
				 	   	
						}						
					}
					else{
							$lstrStudentName = $larrStudentMailingDetails['CompanyName'];							
							
						    $lstrEmailTemplateSubject = "Payment Failed.";
						    $lstrEmailTemplateBody    = "<br><br> Payment of Company ".$lstrStudentName." for Amount RM".$dataArray['grossAmount']." Faild.<br> The Order number is: ".$dataArray['orderNumber'];
						    $lstrEmailTemplateBody    .= "<br> Debit Authentication Code is : ".$dataArray['debitAuthCode']."<br> Credit Authentication Code is : ".$dataArray['creditAuthCode'];
						    $lstrEmailTemplateBody    .= "<br> Payment Bank Code is : ".$dataArray['bankCode']."<br> Branch Code is : ".$dataArray['bankBranch'].".<br> Transaction id is: ".$dataArray['fpxTxnId'];
							$lstrEmailTemplateBody    .= "<br> Thank you";
						    $this->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$larrStudentMailingDetails["Email"],$lstrStudentName);

						    $larrformDatanew2['mailSent'] = 2;	
					  		$where = "idFpxXml = '".$larrrfpxesult[$lvarj]['idFpxXml']."'"; 	
					   		//$db->update('tbl_fpxxml',$larrformDatanew2,$where);	
					}		
				}

		else if($detailsArray['entryFrom']==3){  //Takaful

				 $lintinsertedId =  $detailsArray['IDApplication']; 
			   	 $dataArray['IDApplication'] = $lintinsertedId;
			   	 $dataArray['entryFrom'] = 3;	
			   	 $db = Zend_Db_Table::getDefaultAdapter();
				 $table = "tbl_registrationfpx";
				 //$db->insert($table,$dataArray);
				 $Companyapplicatmodel = new App_Model_Takafulapplication();
	   	 		 $larrresult = $Companyapplicatmodel->fngetTakafulOperator($lintinsertedId);
	   	 		 //Get Student's Mailing Details
				 $larrStudentMailingDetails = $larrresult;
			   	 if($dataArray['paymentStatus'] == 1){		
	   	 			
	
					//$larrPaymentDetails = $Companyapplicatmodel->fngetPaymentDetails($lintinsertedId);
				
	   				$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					$postArray['Regid']  = substr($dataArray['fpxTxnId'], 1, 6).rand(1000, 9999).substr($dataArray['fpxTxnId'], 5, 9);
					
					$db = Zend_Db_Table::getDefaultAdapter();
					$larrformData1['registrationPin'] = $postArray['Regid'];
					$larrformData1['paymentStatus'] = 1;	
					$larrformData1['Approved'] = 1;
					$where = "idBatchRegistration = '".$lintinsertedId."'"; 	
					//$db->update('tbl_batchregistration',$larrformData1,$where); 
		 			
		 
					$this->view->Regid= $postArray['Regid'];	
					$this->view->mess = "<b><font color='green'>Payment Completed Sucessfully</font></b>";
					$this->view->alertmess = "Payment Completed Sucessfully";					
					$this->view->pageStatus = 2;	

					
						$StudModel = new App_Model_Studentapplication();
						$larrSMTPDetails  = $StudModel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $StudModel->fnGetEmailTemplateDescription("Takaful Payment");
					
						
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];													
							
							$lstrCompanyName = $larrStudentMailingDetails['TakafulName'];
							
							$lstrEmailTemplateBody = str_replace("[Person]",$lstrCompanyName,$lstrEmailTemplateBody);
							//$lstrEmailTemplateBody = str_replace("[Amount]",$dataArray['grossAmount'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Username]",$larrStudentMailingDetails["email"],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[RegPin]",$postArray['Regid'],$lstrEmailTemplateBody);
							$lstrEmailTemplateBody = str_replace("[Password]", substr($larrresult['hint'], 5),$lstrEmailTemplateBody);//substr($string, 5)
							$lstrEmailTemplateBody = str_replace("[Link]","http://www.takafuleexam.com/tbe/takafullogin/login",$lstrEmailTemplateBody);
					    	$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;									
							
					    	$this->view->receiver_email =  $larrStudentMailingDetails["email"];
					    	$this->view->receiver =  $larrStudentMailingDetails['TakafulName'];
					    	$this->view->EmailTemplateSubject = $lstrEmailTemplateSubject;					    	
					    	
					    	$this->view->mess = $lstrEmailTemplateBody;
					    	//$_SESSION["EmailTemplateBody"]  = $lstrEmailTemplateBody;						
						
					    	$this->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$larrStudentMailingDetails["email"],$larrStudentMailingDetails['TakafulName']);
					    	
					    	$larrformDatanew2['mailSent'] = 1;	
					  		$where = "idFpxXml = '".$larrrfpxesult[$lvarj]['idFpxXml']."'"; 	
					   		//$db->update('tbl_fpxxml',$larrformDatanew2,$where);								
							}   	 
	   				 }
	   			 
	   						
					
					else{
							$lstrStudentName = $larrStudentMailingDetails['TakafulName'];							
							
						    $lstrEmailTemplateSubject = "Payment Failed.";
						    $lstrEmailTemplateBody    = "<br><br> Payment of Takaful ".$lstrStudentName." for Amount RM".$dataArray['grossAmount']." Faild.<br> The Order number is: ".$dataArray['orderNumber'];
						    $lstrEmailTemplateBody    .= "<br> Debit Authentication Code is : ".$dataArray['debitAuthCode']."<br> Credit Authentication Code is : ".$dataArray['creditAuthCode'];
						    $lstrEmailTemplateBody    .= "<br> Payment Bank Code is : ".$dataArray['bankCode']."<br> Branch Code is : ".$dataArray['bankBranch'].".<br> Transaction id is: ".$dataArray['fpxTxnId'];
							$lstrEmailTemplateBody    .= "<br> Thank you";
						    $this->sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$larrStudentMailingDetails["email"],$lstrStudentName);
						    
						    $larrformDatanew2['mailSent'] = 2;	
					  		$where = "idFpxXml = '".$larrrfpxesult[$lvarj]['idFpxXml']."'"; 	
					   		//$db->update('tbl_fpxxml',$larrformDatanew2,$where);
						    	
					}		
				}		
			
		}
	
	}
public function sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$receiver_email,$receiver){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$auth = 'ssl';
		$port = '465';
		$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
		$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
		$mail = new Zend_Mail();
		$mail->setBodyHtml($lstrEmailTemplateBody);
		$sender_email = 'ibfiminfo@gmail.com';
		$sender = 'ibfim';
		$mail->setFrom($sender_email, $sender)
			 ->addTo("tony.uce@gmail.com", $receiver)
			// ->addBcc("h.rajkumarreddy@gmail.com",$receiver)
			 //->addBcc("sachingururaj@gmail.com",$receiver)
	         ->setSubject($lstrEmailTemplateSubject);
		//$mail->send($transport);			
	}
	
		
}