<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class QuestionencryptionController extends Base_Base {


	public function init() {	
		
		$this->fnsetObj();
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);  
	}
	public function fnsetObj(){
		$this->lobjquestionnoutilityModel = new Examination_Model_Questionencryption();

	}
	/*
	 *  search form & grid display  
	 */
	public function indexAction() {
 		$lobjsearchform = new App_Form_Search();  //intialize search lobjuserForm
		$this->view->form = $lobjsearchform; //send the lobjuserForm object to the view
	
		 $larresults = $this->lobjquestionnoutilityModel->fnfindqtntype();
		 //print_R($larresults);
		 //die();
		 $normal =  strlen($larresults['Question']);
		
		 IF($normal==167)
		 {
		 	$this->view->encrypt = 2;
		 }
		 else 
		 {
		 	$this->view->encrypt = 1;
		 }
	if ($this->_request->isPost () && $this->_request->getPost ( 'Encrypt' )) {
				
				$larresult = $this->lobjquestionnoutilityModel->fngetAllquestions();
				for($i=0;$i<count($larresult);$i++)
				{
					$engquestions = $larresult[$i]['Question'];
					$Malayquestions = $larresult[$i]['Malay'];
					$Tamilquestions = $larresult[$i]['Tamil'];
					$Arabicquestions = $larresult[$i]['Arabic'];
					$idquestions = $larresult[$i]['idquestions'];
					$key='mystring';
					$engencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($engquestions,$key);
					
				
					$malayencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Malayquestions,$key);
					
					$Tamilencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Tamilquestions,$key);
				
					$arabicencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Arabicquestions,$key);
					
		            $larrupdateresult = $this->lobjquestionnoutilityModel->fnupdateencrytptedquestions($engencryptedquestions,$malayencryptedquestions,$Tamilencryptedquestions,$arabicencryptedquestions,$idquestions);
					
				}
		
			$larresult = $this->lobjquestionnoutilityModel->fngetAllanswers();
				for($i=0;$i<count($larresult);$i++)
				{
					$engquestions = $larresult[$i]['answers'];
					$Malayquestions = $larresult[$i]['Malay'];
					$Tamilquestions = $larresult[$i]['Tamil'];
					$Arabicquestions = $larresult[$i]['Arabic'];
					$idanswers = $larresult[$i]['idanswers'];
					$key='mystring';
					$engencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($engquestions,$key);
				
					$malayencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Malayquestions,$key);
					
					$Tamilencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Tamilquestions,$key);
				
					$arabicencryptedquestions = $this->lobjquestionnoutilityModel->encrypt($Arabicquestions,$key);
					
		            $larrupdateresult = $this->lobjquestionnoutilityModel->fnupdateencrytptedanswers($engencryptedquestions,$malayencryptedquestions,$Tamilencryptedquestions,$arabicencryptedquestions,$idanswers);
					
				}
		    $this->view->encrypt = 1;
		}
		
	

		if ($this->_request->isPost () && $this->_request->getPost ( 'Decrypt' )) {
			
			$larresult = $this->lobjquestionnoutilityModel->fngetAllquestions();
			for($i=0;$i<count($larresult);$i++)
			{
				$engquestions = $larresult[$i]['Question'];
				$Malayquestions = $larresult[$i]['Malay'];
				$Tamilquestions = $larresult[$i]['Tamil'];
				$Arabicquestions = $larresult[$i]['Arabic'];
				$idquestions = $larresult[$i]['idquestions'];
				$key='mystring';
				$engdecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($engquestions,$key);
				
			
				$malaydecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Malayquestions,$key);
				
				$Tamildecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Tamilquestions,$key);
			
				$arabicdecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Arabicquestions,$key);
				
	            $larrupdateresult = $this->lobjquestionnoutilityModel->fnupdateencrytptedquestions($engdecryptedquestions,$malaydecryptedquestions,$Tamildecryptedquestions,$arabicdecryptedquestions,$idquestions);
				
			}
			
			
		$larresultanswers = $this->lobjquestionnoutilityModel->fngetAllanswers();
		for($i=0;$i<count($larresultanswers);$i++)
		{
			$engquestions = $larresultanswers[$i]['answers'];
			$Malayquestions = $larresultanswers[$i]['Malay'];
			$Tamilquestions = $larresultanswers[$i]['Tamil'];
			$Arabicquestions = $larresultanswers[$i]['Arabic'];
			$idanswers = $larresultanswers[$i]['idanswers'];
			$key='mystring';
			$engdecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($engquestions,$key);
		
			$malaydecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Malayquestions,$key);
			
			$Tamildecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Tamilquestions,$key);
		
			$arabicdecryptedquestions = $this->lobjquestionnoutilityModel->decrypt($Arabicquestions,$key);
			
            $larrupdateresult = $this->lobjquestionnoutilityModel->fnupdateencrytptedanswers($engdecryptedquestions,$malaydecryptedquestions,$Tamildecryptedquestions,$arabicdecryptedquestions,$idanswers);
			
		}
		$this->view->encrypt = 2;
		}
		
		
		
	}
	}