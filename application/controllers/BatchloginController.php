<?php
class BatchloginController extends Zend_Controller_Action {
	
	private $gstrsessionSIS;//Global Session Name 16-12-2014
 	public $gsessionbatch;//Global Session Name
 	private $_gobjlogger;
	public function init() { //instantiate log object
	$this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
   	}
	
    public function indexAction() {

	}

    function loginAction() {
    	$this->_helper->layout->disableLayout (); //
		$this->gsessionbatch= new Zend_Session_Namespace('sis');  
		$this->globalsession= new Zend_Session_Namespace('sis');  
        $lobjform = new App_Form_Login(); //intialize login form
        $this->gstrsessionSIS = new Zend_Session_Namespace('sis');   //16-12-2014        
        $this->view->lobjform = $lobjform; //send the form object to the view
        
        if ($this->_request->isPost()) {
        	
        	$ipaddress=substr($this->view->serverUrl(),7);   
		    $phpsession=$_SERVER['HTTP_COOKIE'];
	        Zend_Loader::loadClass('Zend_Filter_StripTags');
	        $filter = new Zend_Filter_StripTags();	       	        
	        if ($this->_request->getPost( 'ses_username')){    
	        	$_SESSION['pagename'] = 'introduction';   		
	       	 	$username = $filter->filter($this->_request->getPost('ses_username'));
	        	$password = $filter->filter($this->_request->getPost('ses_userpass'));
	        }
	        else{
	        	$_SESSION['pagename'] = 'batchlogin';
	        	$username = $filter->filter($this->_request->getPost('username'));
	            $password = $filter->filter($this->_request->getPost('password'));
	        }
	        
	        $ModelBatchlogin = new App_Model_Batchlogin();
	        $resultCheck = $ModelBatchlogin->fngetlogincheck($username,md5($password));
	       
	        if($resultCheck){
	        	
	        	$this->gsessionbatch->__set('idCompany',$resultCheck['IdCompany']);
				$this->globalsession->__set('typeofuser',3);
				
				//updates batchdetails 16-12-2014
	        	$Typeofuser=3;
	        	$ModelBatchlogin = new App_Model_Batchlogin();
				$sessionstatus=$ModelBatchlogin->fnGetSessionStatus($resultCheck['IdCompany'],$Typeofuser);
				$this->gstrsessionSIS->loggedinflag=0;
				//echo "<pre>";print_r($sessionstatus);die();
				if($sessionstatus['Isonline']==1){
					$this->gstrsessionSIS->loggedinflag=1;
					//$this->view->onlineflag = 1;
					//echo "<script>alert('Your session is still alive,Please try again after some time');</script>";
					$this->_redirect('/index/login');
					//echo "<script>parent.location = '".$this->view->baseUrl()."/index/login';</script>";
					exit;
				}
				$iduser=$resultCheck['IdCompany'];
				$Sessionstart=date("Y-m-d H:i:s");
				$Sessionend="0000-00-00 00:00:00";
				$Isonline=1;
				$Active=1;
				$Typeofuser=3;
				$ModelBatchlogin->fnInsertLoginDetails($iduser,$Sessionstart,$Sessionend,$Isonline,$ipaddress,$Active,$phpsession,$Typeofuser);
				//end of batchdetails 16-12-2014
				
	        	$auth = Zend_Auth::getInstance();
				
				
	        	
	           $priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
	        	/*$this->gstrsessionSIS->__set('idCompany',$resultCheck['IdCompany']);
	        	$this->gsessionbatch->idCompany = $resultCheck['IdCompany'];	*/    
				    	
						$this->_redirect('/companyapplication/index'); 	// Added on 16-12-2014
	        	//echo "<script>parent.location = '".$this->view->baseUrl()."/companyapplication/index';</script>";	
	        	exit;
	        }
	        else {
	        	$this->view->alertError = 'Login failed. Either username or password is incorrect';
            	
            	//echo "<script>parent.location = '".$this->view->baseUrl()."/batchlogin/login";
            	if($_SESSION['pagename'] ==  'introduction'){
            		echo "<script>alert('Login failed. Either username or password is incorrect');</script>";
					
					$this->_redirect('/introduction/index/page/company'); 	// Added on 16-12-2014
            		//echo "<script>parent.location = '".$this->view->baseUrl()."/introduction/index/page/company';</script>";
            		exit;
            	}
            	else{
				    $this->_redirect('/batchlogin/login'); 	// Added on 16-12-2014
            		//echo "<script>parent.location = '".$this->view->baseUrl()."/batchlogin/login';</script>";
            	}
            }  
	        
            
            
            
           /* 
            
	        
			$dbAdapter = Zend_Db_Table::getDefaultAdapter();
			$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
				
			$authAdapter->setTableName('tbl_companies')
			    		->setIdentityColumn('Login')
			    		->setCredentialColumn('Password');
			    		
            $authAdapter->setIdentity($username);
            $authAdapter->setCredential(md5($password));
            
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);
            echo "<pre>";
            print_r($result);
            exit;
            if ($result->isValid()) {
				$data = $authAdapter->getResultRowObject(null, 'passwd');
				$auth->getStorage()->write($data);
				$auth->getIdentity()->iduser;
				
				
                $larrCommonModel = new App_Model_Common();
                $Rolename = $larrCommonModel->fnGetRoleName($auth->getIdentity()->IdRole);
                
				if($Rolename['DefinitionDesc']== "Admin") {
					$this->gstrsessionSIS->__set('idUniversity',1);
					$this->gstrsessionSIS->__set('idCollege',0);
					// user type 0:college  1: branch
					$this->gstrsessionSIS->__set('userType',0);
					$this->gstrsessionSIS->__set('rolename',$Rolename['DefinitionDesc']);
				} else {
					$staffdetails = $larrCommonModel->fnGetStaff($auth->getIdentity()->IdStaff);
					$Universitydetails = $larrCommonModel->fnGetUniversity($staffdetails['IdCollege']);

					$this->gstrsessionSIS->__set('idUniversity',$Universitydetails['AffiliatedTo']);
					$this->gstrsessionSIS->__set('idCollege',$staffdetails['IdCollege']);
					$this->gstrsessionSIS->__set('userType',$staffdetails['StaffType']);  // user type 0:college  1: branch
					$this->gstrsessionSIS->__set('rolename',$Rolename['DefinitionDesc']);
				}
				echo "<script>parent.location = '".$this->view->baseUrl()."/batch-reg/companyapplication/index';</script>";	
				//$this->_redirect($this->view->url(array('module'=>'general-setup','controller'=>'user', 'action'=>'index'),'default',true));
            } else {
            	$this->view->alertError = 'Login failed. Either username or password is incorrect';
            }  */   
        }
    	else{
        	$_SESSION['pagename'] =  'batchlogin';
        }
		$this->render(); //render the view
    }


    public function logoutAction() {
    	
    	$this->_helper->layout->disableLayout (); 
    	$this->_helper->viewRenderer->setNoRender();
    	
		
		//logout updates 16-12-2014 
    	$auth = Zend_Auth::getInstance();
    	$this->gsessionbatch= new Zend_Session_Namespace('sis');  
	     $iduser=$this->gsessionbatch->__get('idCompany');
	     $Typeofuser=3;
	    // echo $iduser;
	    // echo $Typeofuser;die();
	     
	     $logouttime=date("Y-m-d H:i:s");
    	 $ModelBatchlogin = new App_Model_Batchlogin();
	     $ModelBatchlogin->fnPunchInLogOut($iduser,$Typeofuser,$logouttime);
		 // end of logout updates 16-12-2014 
		 
		 
    	$auth = Zend_Auth::getInstance();
    	 // Write Logs
			$priority=Zend_Log::INFO;
			$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
			$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged Out"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
			$this->_gobjlogger->log($message,5);
			
    	 	Zend_Session:: namespaceUnset('sis');
			$this->_redirect('/index/login'); 	// Added on 16-12-2014
			//echo "<script>parent.location = '".$this->view->baseUrl()."/index/login';</script>";exit;
   		/* if($_SESSION['pagename'] ==  'introduction'){
            		echo "<script>parent.location = '".$this->view->baseUrl()."/introduction/index/page/company';</script>";exit;
            	}
            	else{
            		echo "<script>parent.location = '".$this->view->baseUrl()."/batchlogin/login';</script>";
            	}*/
    }
}
