<?php
error_reporting (E_ALL ^ E_NOTICE);
class CenterauthnewController extends Zend_Controller_Action { //Controller for the User Module
public $gsessionidCenter;//Global Session Name
public function init() { //initialization function		
		$this->gsessionidCenter = Zend_Registry::get('sis'); 	
		if(empty($this->gsessionidCenter->idcenter)){ 
			$this->_redirect( $this->baseUrl . '/batchlogin/logout');					
		}
		$this->_helper->layout()->setLayout('/examcenter/usty1');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjCommon = new App_Model_Common();
		$this->lobjCenterloginmodel = new App_Model_Centerloginnew(); //user model object
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates(); //user model object		
		$this->lobjBatchcandidatesForm = new App_Form_Batchcandidates (); //intialize user lobjuserForm
		$this->lobjAdhocApplicationForm=new App_Form_Adhocapplication();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
 	
	public function indexAction() {

		$larrresult = $this->lobjCenterloginmodel->fngetCenterDetails($this->gsessionidCenter->idcenter);
		$this->view->centername = $larrresult['centername'];
		$this->view->contactperson = $larrresult['contactperson'];
		$this->view->seessionidcenter =$this->gsessionidCenter->idcenter;
		$presntdate=date("Y-m-d");
		$strSql = "SELECT DISTINCT (mng.idmangesession), mng.managesessionname, mng.starttime, mng.endtime
					FROM tbl_studentapplication std LEFT OUTER JOIN tbl_managesession mng ON std.Examsession = mng.idmangesession
					WHERE std.datetime = '".$presntdate."' AND
					std.examvenue =".$this->view->seessionidcenter." AND
					std.payment=1 order by mng.starttime"; 

		$this->view->examsession = $larrsessionresult = $this->lobjCenterloginmodel->fnGetValues($strSql);
		$this->view->hur = date('H');
		$this->view->mun = date('i');
		$this->view->sur = date('s');
		
		$larrsessiontimeresults = $this->lobjCenterloginmodel->fnGetSessionforthesechedulers(date('Y'),$this->gsessionidCenter->idcenter);
		foreach($larrsessiontimeresults as $days)
		{
			$sessioncount[]=$days['total'];	
		}
		
	if(empty($larrsessiontimeresults))
		{
		$sessioncount[]=0;
		}
		
		
		$maximumsesiion=max($sessioncount);
		$this->view->maxcolourcodes = $maximumsesiion;
		$this->view->currenttime = date("H:i:s");
	}
	
	public function showsessionsAction() {
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$this->view->seessionidcenter =$this->gsessionidCenter->idcenter;
		$iddays= $this->_getParam('iddays');
		$idmonth= $this->_getParam('idmonth');
		$presentyear = date("Y");

		if(strlen($iddays) <= 1)
	   	{
	   		$iddays = '0'.$iddays;
	   	}
	    if(strlen($idmonth) <= 1)
	   	{
	   		$idmonth = '0'.$idmonth;
	   	}

		$this->view->normaldate= $normaldate = $presentyear."-".$idmonth."-".$iddays;
/*		$strSql = "select DAYOFWEEK('$normaldate') as days";

		$dauyoftheweek = $this->lobjCenterloginmodel->fnGetValues($strSql);
		if($dauyoftheweek[0]['days']==1)
		{
			$dauyoftheweek['days']= 7;
		}
		else 
		{
			$dauyoftheweek['days']=$dauyoftheweek[0]['days']-1;	
		}*/
		//To be changed later to fngetvalues
		
		 $larrsessionresultbasedoncenter = $this->lobjCenterloginmodel->fngetsessionDetailsBasedOnCenterIds($normaldate,$this->gsessionidCenter->idcenter);
		// print_r($larrsessionresultbasedoncenter);
		 if( empty($larrsessionresultbasedoncenter))
		 {
		 	 $larrsessionresultbasedoncenter = $this->lobjCenterloginmodel->fngetsessionDetailsBasedOnCenterIdsInactive($normaldate,$this->gsessionidCenter->idcenter);
		 	
		 }
		$this->view->examsession =$larrsessionresultbasedoncenter;
		$this->view->normalyear = $presentyear;
		$this->view->normalmonth = $this->_getParam('idmonth');
		$this->view->normalday = $this->_getParam('iddays');
	}
	
	
	
	
	public function insertintotempAction()
	{
		//to be changed later to insert in batch instead of loop for 4 times. currently leaving as it is not very resource consuming. Sachin
 		/*$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$issession= $this->_getParam('issession');
		$idcenter = $this->_getParam('idcenter');
		$endtime = $this->_getParam('endtime');
		
		$sessionID = Zend_Session::getId();
	    $date =date('d');
		if($date<10)
		{
			$date = '0'.$date[1];
		}
		
		
		$hour = date('H');
		$minutes = date('i');
		$hours = $hour*60;
		$startedmin = $minutes+$hours;
		//$presntdate = date('Y-m-d');
		//$larrsessionresult = $this->lobjCenterloginmodel->fngetprogramDetails($idcenter,$issession,$presntdate);
		$larrsessionresult = $this->lobjCenterloginmodel->fnGetValues("select ");
		
		$strSql ="insert into tbl_centerstartexam (program,idcenter,sessionid,totaltime,startedtime,idsession,examdate,date)
		select distinct (std.program),$idcenter,$sessionID,tos.timelimit,$startedmin,$issession,day(now()),date(now()) 
		from tbl_studentapplication std left outer join tbl_batchmaster batch on std.program=batch.idprogrammaster
		left outer join tbl_tosmaster tos on batch.idbatch=tos.idbatch 
		where std.payment=1 and std.examvenue=$idcenter and std.examsession=$issession
        and std.datetime=date(now())" ;
		
		$larrsessionresult = $this->lobjCenterloginmodel->fnInsert($strSql);
		
		//-------------------Sachin End----------------------------//
		$todaytime = date('H:i:s');
		
		if($endtime <= $todaytime) {
			echo "0";
		} else {
			foreach($larrsessionresult as $larrsessionresults) {
				//$totaltime = $this->lobjCenterloginmodel->fngetactivetotaltime($larrsessionresults['IdProgrammaster']);
				//$strSql = "Select tos.timelimit from tbl_tosmaster tos left outer join tbl_batchmaster batch on tos.IdBatch= batch.IdBatch where tos.active=1 and batch.idProgrammaster =".$larrsessionresults['IdProgrammaster'];
				//$totaltime = $this->lobjCenterloginmodel-fnGetValuesRow($strSql);
				//$totaltimerequired = $totaltime['TimeLimit'];
				$larrresult = $this->lobjCenterloginmodel->fninsertintotemp($idcenter,$sessionID,$larrsessionresults['program'],$startedmin,$larrsessionresults['timelimit'],$date,$issession);
			}
			echo "1";
		}
*/
		
		
	 	$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$issession= $this->_getParam('issession');
		$idcenter = $this->_getParam('idcenter');
		$endtime = $this->_getParam('endtime');
		
		$sessionID = Zend_Session::getId();
	    $date =date('d');
		if($date<10)
		{
			$date = $date[1];
		}
		
		
		$hour = date('H');
		$minutes = date('i');
		$hours = $hour*60;
		$startedmin = $minutes+$hours;
		$presntdate = date('Y-m-d');
		$larrsessionresult = $this->lobjCenterloginmodel->fngetprogramDetails($idcenter,$issession,$presntdate);
		$todaytime = date('H:i:s');
		

		if($endtime <= $todaytime) {
			echo "0";
		} else {
			foreach($larrsessionresult as $larrsessionresults) {
				$totaltime = $this->lobjCenterloginmodel->fngetactivetotaltime($larrsessionresults['IdProgrammaster']);
				$totaltimerequired = $totaltime[0]['TimeLimit'];
				
				$exammdate = date('Y-m-d');
				$larrresultalreadystarted = $this->lobjCenterloginmodel->fnALreadyExamStarted($idcenter,$larrsessionresults['IdProgrammaster'],$issession,$exammdate);
				if(empty($larrresultalreadystarted)) {
					$larrresult = $this->lobjCenterloginmodel->fninsertintotemp($idcenter,$sessionID,$larrsessionresults['IdProgrammaster'],$startedmin,$totaltimerequired,$date,$issession);
				}	
			}
			echo "1";
		}

	}
	
	
	public function changepasswordAction() { // action for search and view
		$larrresult = $this->lobjCenterloginmodel->fngetCenterDetails($this->gsessionidCenter->idcenter); //get user details
 		$this->view->lobjAdhocApplicationForm = $this->lobjAdhocApplicationForm;
		$idcomp=$this->gsessionidCenter->idcenter;
		//print_r($larrresult);die();
		$pass=$larrresult['centerpassword'];
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save')){
			$larrformData = $this->_request->getPost ();
			if ($this->lobjAdhocApplicationForm->isValid($larrformData)) {	
			
			$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );
			$larrformData['oldpassword']=md5($larrformData['oldpassword']);
			//print_r($larrformData);die();
		if($pass==$larrformData['oldpassword'])
		{
			$larrformData['newpassword']=md5($larrformData['newpassword']);
           //echo $larrformData['newpassword'];die();
			$lastInsId = $this->lobjCenterloginmodel->updatecenterpass($larrformData['newpassword'],$idcomp);	
		  	$this->_redirect( $this->baseUrl . "/centerauthnew/index");
		
		}
		else 
		{
			echo '<script language="javascript">alert("The Old Password is Not Correct")</script>';
				//$this->_redirect( $this->baseUrl . "/companyapplication/changepassword");
  			//die();
			
		}	
			
		}
		}
	}
	
	public function newfnnewcaleshowAction() {
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$NewCity = $this->gsessionidCenter->idcenter;
		$year = date("Y"); 
		
		$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($this->gsessionidCenter->idcenter,$year);
		foreach($resultcount as $resultcounts) {
			$maxcolourcodes[] = $resultcounts['countidmanagesession'];
		}
		
		
	if(empty($resultcount))
		{
		$maxcolourcodes[]=0;
		}
		$this->view->maxcolourcodes = max($maxcolourcodes);
		
		
		
		$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($NewCity,$year);

		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');	
    	$db_link  = mysql_connect($config->resources->db->params->host, $config->resources->db->params->username, $config->resources->db->params->password) or die('Cannot connect to the DB');
		mysql_select_db($config->resources->db->params->dbname,$db_link) or die('Cannot select the DB');
		//echo $config->resources->db->params->dbname;
		
			
		//$monthquery = "SELECT DATE_FORMAT( date,  '%m' ) AS monthk FROM  `tbl_venuedateschedule` where idvenue = '$NewVenue' AND year(date) = '$year' GROUP BY monthk";
       $monthquery = "SELECT DATE_FORMAT( date,  '%m' ) AS monthk FROM  `tbl_venuedateschedule` as a,tbl_newscheduler c where a.idvenue = '$NewCity' AND year(a.date) = '$year' AND a.Active=1 and   a.idnewscheduler=c.idnewscheduler AND a.Reserveflag=1  AND a.centeractive=1   and  a.date >= curdate() GROUP BY monthk  union SELECT DATE_FORMAT( curdate(),  '%m' ) AS monthk ";
		//echo $monthquery;die();
		
		$resultmonth = mysql_query($monthquery,$db_link) or die('cannot get results!');
    	while($rowsk = mysql_fetch_assoc($resultmonth)) {
		  		$monthk['monthk'][]=  $rowsk;

			}
			
		foreach($monthk as $monthk)

		$monthcount = count($monthk); 

		echo "<table ><tr>";
		for($month=0;$month<$monthcount;$month++){
		   	$monthp =  $monthk[$month]['monthk'];

		
		if($monthp==01 || $monthp==1)
		   	{
		   		 $monthname='January';
		   	}
		if($monthp==02 || $monthp==2 )
		   	{
		   		 $monthname='Feburary';
		   	}
		if($monthp==03 || $monthp==3 )
		   	{
		   		 $monthname='March';
		   	}
		   	if($monthp==04 || $monthp==4)
		   	{
		   		 $monthname='April';
		   	}
		if($monthp==05 || $monthp==5)
		   	{
		   		 $monthname='May';
		   	}
		if($monthp==06 || $monthp==6 )
		   	{
		   		 $monthname='June';
		   	}
		if($monthp==07 || $monthp==7 )
		   	{
		   		 $monthname='July';
		   	}
		if($monthp==08 || $monthp==8)
		   	{
		   		 $monthname='August';
		   	}
		if($monthp==09 || $monthp==9 )
		   	{
		   		 $monthname='September';
		   	}
		if($monthp==10)
		   	{
		   		 $monthname='October';
		   	}
		if($monthp==11)
		   	{
		   		 $monthname='November';
		   	}
		if($monthp==12)
		   	{
		   		 $monthname='December';
		   	}
		   	
		$events = array();
		//$query = "SELECT idvenuedateschedule,idsession AS title, DATE_FORMAT(date,'%Y-%m-%d') AS event_date FROM tbl_venuedateschedule WHERE date LIKE '$year-$monthp%' AND Active='1' AND idvenue = '$NewVenue' ";
 	$query = "SELECT idsession AS title, DATE_FORMAT(date,'%Y-%m-%d') AS event_date FROM tbl_venuedateschedule as a,tbl_newscheduler c  WHERE a.date LIKE '$year-$monthp%' AND a.idvenue = '$NewCity' AND year(a.date) = '$year' AND  a.idnewscheduler=c.idnewscheduler AND a.Reserveflag=1  AND a.centeractive=1   and a.Active=1 and  a.date >= curdate() 
 	           union  select idsession AS title, DATE_FORMAT(date,'%Y-%m-%d') AS event_date FROM tbl_venuedateschedule as a  WHERE a.date in (select date from tbl_venuedateschedule where Allotedseats > 0 AND year(date) = '$year' and idvenue = '$NewCity' and  date >= curdate()  ) and  a.Allotedseats > 0 AND year(a.date) = '$year' and a.idvenue = '$NewCity' and  a.date >= curdate()";
	
	//echo $query;die();
		$result = mysql_query($query,$db_link) or die('cannot get results!');
		while($row = mysql_fetch_assoc($result)) {
		  	$events[$row['event_date']][]=  $row;
		}
			
		if($month%3 == 1  && $month !=1)echo "<tr>";
			echo "<td  style='border : 1px solid black;' cellpadding='10px' align = 'center' valign = 'top'>";
			//echo date('F', mktime(0,0,0,$monthp)).$year;
                                                          echo $monthname.$year;
			echo self::draw_calendar($monthp,$year,$events,$Program,$NewVenue);
			echo "</td>";
			if($month%3 == 0 && $month !=0 ) echo "</tr>";
		}
		//echo $query;die();
		echo "</table>";die();
	}
	
function draw_calendar($month,$year,$events = array(),$Program,$NewVenue)
{


		/* draw table */
		$calendar = '<table width="80%" border="1" align="center" style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
		/* table headings */
		$headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
		$calendar.= '<tr><td class="calendar-day-head" >'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';
			
		 /* days and weeks vars now ... */
		$running_day = date('w',mktime(0,0,0,$month,1,$year));
		$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
		$days_in_this_week = 1;
		$day_counter = 0; 
		$dates_array = array();
		/* row for week one */
		$calendar.= '<tr class="calendar-row">';
		/* print "blank" days until the first of the current week */
		//echo "<pre>";
		for($x = 0; $x < $running_day; $x++):
		$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
		$days_in_this_week++;
		endfor;
			  
		//$event_day = $year.'-'.$month.�-�.$list_day;
		/* keep going with days.... */
		for($list_day = 1; $list_day <= $days_in_month; $list_day++):
			$calendar.= '<td class="calendar-day">';
			/* add in the day number */
			if($list_day < 10) {
				$list_day = str_pad($list_day, 2, '0', STR_PAD_LEFT);
			}
			//$calendar.= $list_day;
			$event_day = $year.'-'.$month.'-'.$list_day;

	
 			if(isset($events[$event_day])) {
						
				foreach($events[$event_day] as $event) {
					
			    	$event1[$event['event_date']]  = $event['title'];
			    	
			     }

	
			     $mnc = "#FFFFFF";
			      if(count($events[$event_day]) == 1){
			       	$mnc = "green";
			      }
			      elseif(count($events[$event_day]) == 2){
			      	$mnc = "skyblue";
			      }
			      if(count($events[$event_day]) == 3){
			      	$mnc = "violet";
			      }elseif(count($events[$event_day]) == 4) {
			      	$mnc = "pink";
			      }
			      $calendar.= '<a href="#" ><div style = "background-color:'.$mnc.'; font-color:'.$mnc.'; width :30px; cursor:pointer; text-align:center; font-size:10pt;  border : 1px solid black;" " onclick="functp('.$list_day.','.$month.');">'.$list_day.'</div></a> ';
			   } else {
			     	$mnc = "#f6f6f6";
			      	 $calendar.= '<div style = "background-color:'.$mnc.';font-size:10pt;" >'.$list_day.'</div>';
			        //$calendar.= str_repeat('<p>&nbsp;</p>',2);
			   }
			   $calendar.= '</td>';
			   if($running_day == 6):
			      $calendar.= '</tr>';
			      if(($day_counter+1) != $days_in_month):
			      		$calendar.= '<tr class="calendar-row">';
			     endif;
			     $running_day = -1;
			     $days_in_this_week = 0;
			    endif;
			    $days_in_this_week++; $running_day++; $day_counter++;
			  	endfor;
			
			  /* finish the rest of the days in the week */
			  if($days_in_this_week < 8):
			    for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			      if($days_in_this_week != 1) $calendar.= '<td class="calendar-day-np">&nbsp;</td>';
			    endfor;
			  endif;
			
			  /* final row */
			  $calendar.= '</tr>';
			  /* end the table */
			  $calendar.= '</table><br>';
			
			  /** DEBUG **/
			  //$calendar = str_replace('</td>','</td>'."\n",$calendar);
			 // $calendar = str_replace('</tr>','</tr>'."\n",$calendar);
			  
			  /* all done, return result */
			  return $calendar;
}
	
}