<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class InhouseschedulerController extends Base_Base {

	
	public function init() {
		$this->gsessionidtakafuloperator = Zend_Registry::get ( 'sis' );
		if (empty ( $this->gsessionidtakafuloperator->idtakafuloperator )) {
			$this->_redirect ( $this->baseUrl . '/takafullogin/logout' );
		}
		$this->fnsetObj();
		$this->_helper->layout ()->setLayout ( '/takaful/usty1' );
	}
	
	public function fnsetObj()
	{
		$this->lobjonedayschedulermodel = new App_Model_Inhousescheduler();
		$this->lobjonedayschedulermodelForm = new App_Form_Inhousescheduler(); 

	}
	
	public function indexAction() {
    	$this->view->title="Manage Inhouse Scheduler";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
		
		$lintsess = Zend_Session::getId();
		$larrdeleteresult = $this->lobjonedayschedulermodel->fndeleteaction($lintsess);
		$larrdeletetempvenue = $this->lobjonedayschedulermodel->fndeletetempvenue($lintsess);
		$larrresult = array();//$this->lobjschedulermodel->fnGetScheduler(); 
		 if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->newschedulerresult);
		
		$lintpagecount = $this->gintPageCount;// Definitiontype model
		$lintpage = $this->_getParam('page',1); // Paginator instance
		
		if(isset($this->gobjsessionsis->newschedulerresult)) 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->newschedulerresult,$lintpage,$lintpagecount);
		} 
		else 
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid ( $larrformData )) {
				
				
				$this->view->status = $larrformData['field7'];
				$larrresult = $this->lobjonedayschedulermodel->gettoschedulernames( $this->lobjform->getValues () ,$this->gsessionidtakafuloperator->idtakafuloperator ); //searching the values for the user
				//echo "<pre>";
				//print_r($larrresult);die();
				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gobjsessionsis->newschedulerresult = $larrresult;
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			 $this->_redirect( $this->baseUrl . '/inhousescheduler/index');
			//$this->_redirect($this->view->url(array('module'=>'general-setup' ,'controller'=>'program', 'action'=>'index'),'default',true));
		}
		
	}
	
	public function addspecialschedulerAction() { //title
		$this->view->lobjonedayschedulermodelForm = $this->lobjonedayschedulermodelForm;
		$this->view->onload = 1;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjonedayschedulermodelForm->UpdDate->setValue($ldtsystemDate);
		
		$auth = Zend_Auth::getInstance();
		$this->view->lobjonedayschedulermodelForm->UpdUser->setValue( $auth->getIdentity()->iduser);
		
		$larrresult = $this->lobjonedayschedulermodel->fngetCourse();
		$this->view->courses=$larrresult;
		$this->view->editsessionarray = array();
		$larrresults = $this->lobjonedayschedulermodel->fngetSessionDetails();
		//echo "<pre>";print_r($larrresults);die();
		$this->lobjonedayschedulermodelForm->session->addMultioption('','Select');
		$this->lobjonedayschedulermodelForm->session->addMultioptions($larrresults);
		
		//$this->lobjonedayschedulermodelForm->session->addMultioption('','Select');
		//$this->lobjonedayschedulermodelForm->schedulertype->addMultioptions(array('key'=>));
		//$this->view->mansession=$larrresults;
		$this->view->centerarray = array();
		$this->view->schedulercoursearray= array();
		/*$larrresult2 = $this->lobjonedayschedulermodel->fnfetchvenue();
		$this->view->centerarray=$larrresult2;*/
		$lintsess = Zend_Session::getId();
		$larresult = $this->lobjonedayschedulermodel->fnajaxgettempsession($lintsess);
		$this->view->managesession= $larresult;
		
		$Seatcapacity = $this->lobjonedayschedulermodel->fngetmaxseatcapacity();		
		$this->view->seatcapacity = $Seatcapacity['TakafulField6'];

	if ($this->_request->isPost () && $this->_request->getPost ( 'Next' )) 
	  {
			$larrformData = $this->_request->getPost (); 
			//echo "<pre>"; print_r($larrformData);die();
			$this->view->onload = 0;
			
			$this->view->editsessionarray = $larrformData['sessionarr'];
			$this->view->schedulercoursearray = $larrformData['idprogram'];
			$this->view->lobjonedayschedulermodelForm->Date->setvalue($larrformData['Date']);
			$this->view->lobjonedayschedulermodelForm->description->setvalue($larrformData['description']);
				$this->view->lobjonedayschedulermodelForm->schedulertype->setvalue($larrformData['schedulertype']);
			$idsessions=0;
			for($i=0;$i<count($larrformData['sessionarr']);$i++)
	        {
	        	 $value=$larrformData['sessionarr'][$i];
				 $idsessions=$idsessions.','.$value;
	        }
			$larresultofpresentcenters = $this->lobjonedayschedulermodel->fngetpresentschedulers($larrformData,$idsessions);
			$centerresults = 0;
			for($center=0;$center<count($larresultofpresentcenters);$center++)
	        {
	        	 $value=$larresultofpresentcenters[$center]['idvenue'];
				 $centerresults=$centerresults.','.$value;
	        }
	        
	        $larrresultcenter = $this->lobjonedayschedulermodel->fngetcenternames($centerresults,$this->gsessionidtakafuloperator->idtakafuloperator);
	        $this->lobjonedayschedulermodelForm->venue->addMultioption('','Select');
		    $this->lobjonedayschedulermodelForm->venue->addMultioptions($larrresultcenter);
	        $this->view->centerarray=$larrresultcenter;
		}
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) 
	  {
	  	$larrformData = $this->_request->getPost (); 
	  	//echo "<pre/>";print_r($larrformData);die();
	  	$larresult = $this->lobjonedayschedulermodel->fnAddScheduler($larrformData);
	  	echo "<script>parent.location = '".$this->view->baseUrl()."/managetakafulcenters/index';</script>";
	  }
    }
    

	
	public function getexceptionaldateAction()
	{
		$this->_helper->layout->disableLayout();//disable layout
		$this->_helper->viewRenderer->setNoRender();//do not render the view		
		$iddate = $this->_getParam('iddate');
		$exceptionaldate = $this->lobjonedayschedulermodel->fngetexceptiondate($iddate);
		if(count($exceptionaldate)>2)
		{
			echo "1";
		}
		else 
		{
			echo "0";
		};
		
	}	
	
   public function fninserttempsessionAction()
    {   
   		
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidsession =  $this->_getParam('idsession');
		$lintsess = Zend_Session::getId();
		$validatesession = $this->lobjonedayschedulermodel->fngetsession($lintidsession,$lintsess);
		if($validatesession)
		{
			
		}
		else 
		{
		$result = $this->lobjonedayschedulermodel->fninserttempsession($lintidsession,$lintsess);
		}
		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempsession($lintsess);
		
        $idsessions = 0;
		for($i=0;$i<count($larrtemp);$i++)
	     {
	       $value = $larrtemp[$i]['idsession'];		  
		   $idsessions = $idsessions.','.$value;
		   
	     }
		
		 
		$larrresult = $this->lobjonedayschedulermodel->fnajaxgetsession($idsessions);
		
		$larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresult);
		echo  Zend_Json_Encoder::encode($larrcentreDetailss);
		
    }	
   public function fninserttextAction()
    {   
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintsess = Zend_Session::getId();
		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempsession($lintsess);
		$tabledata = '';
		for($linti = 0;$linti<count($larrtemp);$linti++){
		       $tabledata.= '<table><tr><td>'.$larrtemp[$linti]['managesessionname'].'('.$larrtemp[$linti]['starttime'].' - '.$larrtemp[$linti]['endtime'].')
		                      <input type ="hidden" id="session" name = "sessionarr[]" value = "'.$larrtemp[$linti]['idmangesession'].'"/><td></tr>';
		}
		$tabledata.= '</table>';
		echo  $tabledata;
		
    }
    public function fninserttempvenueAction()
    {   
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidvenue =  $this->_getParam('idvenue');
		$lintcapacity =  $this->_getParam('capacity');
		//$this->gsessionidtakafuloperator->__set('idtakafuloperator',$resultCheck['idtakafuloperator']);
		$this->gsessionidtakafuloperator = Zend_Registry::get ( 'sis' );
		$takafuloperator =  $this->gsessionidtakafuloperator->idtakafuloperator;
		$dates =  $this->_getParam('dates');
		
		$lintsess = Zend_Session::getId();
		
		
    		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempsession($lintsess);
		
        $idsessions = 0;
		for($i=0;$i<count($larrtemp);$i++)
	     {
	       $value = $larrtemp[$i]['idsession'];		  
		   $idsessions = $idsessions.','.$value;
		   
	     }
	     
		$validatevenues = $this->lobjonedayschedulermodel->fnvalidatevenue($lintidvenue,$idsessions,$dates);
		//echo "<pre>";
		//print_r($validatevenues);die();
		$flag=1;
		if($validatevenues)
		{
			$flag=2;
		}
		else 
		{
			$larrvalidatevenue=$this->lobjonedayschedulermodel->fngettempvenue($lintidvenue,$lintsess);
			if($larrvalidatevenue)
			{
				
			}
			else 
			{
		$result = $this->lobjonedayschedulermodel->fninserttempvenue($lintidvenue,$lintcapacity,$lintsess);
			}
			}
		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempvenues($lintsess);
        $idvens = 0;
		for($i=0;$i<count($larrtemp);$i++)
	     {
	       $value = $larrtemp[$i]['idvenues'];
		   $idvens = $idvens.','.$value;
	     }
		$larrresultvenues = $this->lobjonedayschedulermodel->fnvalidateonlytakafultrainingcenters($idvens,$takafuloperator);
		//print_r($larrresult);
		if($flag==2)
		{
			$larrresult=2;
			echo $larrresult;
		}
		
		else if(empty($larrresultvenues))
		{
		  echo 1;
		}
		else
		{		
		  $larrcentreDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames($larrresultvenues);
		  echo  Zend_Json_Encoder::encode($larrcentreDetailss);
		}
    }
    public function fninsertvenuetextAction()
    {   
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintsess = Zend_Session::getId();
		$larrtemp = $this->lobjonedayschedulermodel->fnajaxgettempvenues($lintsess);
		$tabledata = '';
		for($linti = 0;$linti<count($larrtemp);$linti++){
		       $tabledata.= '<table><tr><td>'.$larrtemp[$linti]['centername'].'('.$larrtemp[$linti]['capacity'].')
		                      <input type ="hidden" id="venue" name = "venarr[]" value = "'.$larrtemp[$linti]['idcenter'].'"/>
		                      <input type ="hidden" id="capacities" name = "caparr[]" value = "'.$larrtemp[$linti]['capacity'].'"/><td></tr>';
		}
		$tabledata.= '</table>';
		echo  $tabledata;
		
    }
    public function fngetdateinwordsAction()
    {
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$ldtdate =  $this->_getParam('dat');
		$larrtemp = $this->lobjonedayschedulermodel->fngetdate($ldtdate);
		echo $larrtemp['date'];
    }
     public function editschedulerAction() { //title

		$lintidscheduler= $this->_getParam('id');
		$this->view->lobjprogramForm = $this->lobjonedayschedulermodelForm;		
		/////////////function to fetch all the details/////////////////
		$larrresultdays = $this->lobjonedayschedulermodel->fngetDaysDetails();
		$this->view->days=$larrresultdays;
		
         $larrresult = $this->lobjonedayschedulermodel->fngetCourse();
		 $this->view->courses=$larrresult;
		 
		 $larrresult22 = $this->lobjonedayschedulermodel->fnGetAllMonthList();
		$this->view->lobjprogramForm->From->addMultiOptions($larrresult22);
		$this->view->lobjprogramForm->To->addMultiOptions($larrresult22);
		
		//$this->view->activeset=$larrresult22;
		
		 $larrresults = $this->lobjonedayschedulermodel->fngetSessionDetails();
		 $this->view->mansession=$larrresults;
		
		 $larrresult2 = $this->lobjonedayschedulermodel->fnfetchvenue($this->gsessionidtakafuloperator->idtakafuloperator);
		 $this->view->centerarray=$larrresult2;
		/////////////////////////////end of fetch all details//////////////////
			//extending the scheduler///////////////////
		 $this->view->idnewscheduler = $lintidscheduler;
		/////end of the extending the scheudler///////////
		
		$larrresultscheduler = $this->lobjonedayschedulermodel->fnGetSchedulerDetails($lintidscheduler); 
		
			$this->view->lobjprogramForm->From->setValue($larrresultscheduler['From']);
			$this->view->lobjprogramForm->To->setValue($larrresultscheduler['To']);
				$this->view->lobjprogramForm->From->setAttrib('readonly','readonly');
				$this->view->lobjprogramForm->To->setAttrib('readonly','readonly'); 
				$this->view->lobjprogramForm->Year->setAttrib('readonly','readonly'); 
				$this->view->lobjprogramForm->description->setValue($larrresultscheduler['Description']); 
				$this->view->lobjprogramForm->description->setAttrib('readonly','readonly'); 
				$this->view->lobjprogramForm->Active->setValue($larrresultscheduler['Active']); 
	//print_r($larrresultscheduler);die();
		//$this->view->lobjprogramForm->From->addMultiOption($this->lobjschedulermodel->fnGetfromList($larrresultscheduler['From']));
		//$this->view->lobjprogramForm->To->addMultiOption($this->lobjschedulermodel->fnGettoList($larrresultscheduler['To']));
		$this->view->lobjprogramForm->Year->setValue($larrresultscheduler['Year']);
		
		/////////////////FUNCTION TO FETCH FOR THE SCHEDULER DAYS/////////////
		$larrresultschedulerdays = $this->lobjonedayschedulermodel->fnGetSchedulerDays($lintidscheduler); 
	
		 $arrschedulerdays = array();
		foreach ($larrresultschedulerdays as $arrschedulerdays)
		{
			$arrIDschedulerdays[] = $arrschedulerdays['Days'];
		}
		/*print_r($arrIDschedulerdays);
		die();*/
		$this->view->schedulerdays = $arrIDschedulerdays;
		//////////////////////////////////////////////////////////////////
	
		////////////function tofetch the scheduler session and view////////////////
		$larrresultschedulersession = $this->lobjonedayschedulermodel->fnGetSchedulerSession($lintidscheduler);
		
	    $arrschedulersession = array();
		foreach ($larrresultschedulersession as $arrschedulersession)
		{
			$arrIdarrschedulersession[] = $arrschedulersession['idmanagesession'];
		}
	
		$this->view->schedulersession = $arrIdarrschedulersession;
		//////////////////end of scheduler session///////////////////////////////
		

		////////////function tofetch the scheduler session and view////////////////
		$larrresultschedulercourse = $this->lobjonedayschedulermodel->fnGetSchedulerCourse($lintidscheduler);
	    $arrschedulercourse = array();
		foreach ($larrresultschedulercourse as $arrschedulercourse)
		{
			$larrresultschedulercourse[] = $arrschedulercourse['IdProgramMaster'];
		}
		$this->view->schedulercourse = $larrresultschedulercourse;
		//////////////////end of scheduler session///////////////////////////////
		
		
		////////////function tofetch the scheduler venue and view////////////////
		$larrresultschedulervenue = $this->lobjonedayschedulermodel->fnGetSchedulerVenue($lintidscheduler);
		$this->view->Examdate = $larrresultschedulervenue[0]['date'];
	    $arrschedulervenue = array();
		foreach ($larrresultschedulervenue as $arrschedulervenue)
		{
			$larrresultschedulervenue[] = $arrschedulervenue['idvenue'];
		}
		$this->view->schedulervenue = $larrresultschedulervenue;		
		//////////////////end of scheduler session///////////////////////////////
		
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
		//echo "<pre>";
			//echo $lintidscheduler;
			//print_r($formData);
			//die();
			
				$checkscheduler=$this->lobjonedayschedulermodel->fncheckidsechduler($lintidscheduler);
			     // print_r($checkscheduler);die();
				if($checkscheduler)
				{
					$larrresult = $this->lobjonedayschedulermodel->fnupdatenewScheduler($lintidscheduler,$formData);
				}
				else 
				{
				     //$larrresult = $this->lobjonedayschedulermodel->fninsertintoSchedulerdatevenue($formData,$lintidscheduler);
				}
				
				//$larrresult = $this->lobjonedayschedulermodel->fnupdateScheduler($lintidscheduler,$formData);
				 $this->_redirect( $this->baseUrl . '/inhousescheduler/index');
    	    
    }
		}	
}
