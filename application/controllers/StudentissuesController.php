<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
ini_set('memory_limit', '-1');
class StudentissuesController extends Zend_Controller_Action 
{
	
	public function init() 
	{
		$this->gstrsessiontextfile= Zend_Registry::get('sis');		
		$this->_helper->layout()->setLayout('/examcenter/usty1');
		$this->lobjstudentissueform = new App_Form_Studentissueform();
		$this->lobjstudentissuemodel = new App_Model_Studentissuemodel();
	}
	
	public function indexAction() 
	{
		$this->view->lobjform = $this->lobjstudentissueform;
		if($this->_request->isPost() && $this->_request->getPost('Search')) 
 		{
 			$larrformData = $this->_request->getPost();
			$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		    $this->view->lobjform->UpdDate->setValue( $ldtsystemDate );
		    $auth = Zend_Auth::getInstance();
		    $this->view->lobjform->UpdUser->setValue( $auth->getIdentity()->iduser);
			$result = $this->lobjstudentissuemodel->fngetdetails($larrformData);
			$this->view->paginator =$result;
 			//echo "<pre>";
			//print_r($larrformData);die();
			
			
 			
		
	  }
	}
	public function addissueAction() 
	{
	    $this->view->lobjform = $this->lobjstudentissueform;
	    $ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjform->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjform->UpdUser->setValue( $auth->getIdentity()->iduser);
		if ($this->getRequest()->isPost())
		{
			   $larrformData = $this->getRequest()->getPost();
			   $result = $this->lobjstudentissuemodel->fninsertissues($larrformData,$ldtsystemDate);
			   //echo "<pre>";
			   //print_r($larrformData);die();
				//unset ( $formData ['Save'] );
				//unset ( $formData ['Back'] );
				//$fromtimearray = $formData['starttime'];
			 	//$pieces = explode("T", $fromtimearray);
			 	//$fromtime = $pieces[1];
			 	//$fromtimearray = $formData['endtime'];
			 	//$pieces1 = explode("T", $fromtimearray);
			 	//$totime = $pieces1[1];
				//$larrresult = $this->lobjprogram->fnaddSession($formData,$fromtime,$totime);
				// $this->_redirect( $this->baseUrl . '/scheduler/managesession/index');
        }     
	}
	public function editissueAction() 
	{
	    $this->view->lobjform = $this->lobjstudentissueform;
		$ldtsystemDate = date ( 'Y-m-d H:i:s' );
		$this->view->lobjform->UpdDate->setValue( $ldtsystemDate );
		$auth = Zend_Auth::getInstance();
		$this->view->lobjform->UpdUser->setValue( $auth->getIdentity()->iduser);
		$Idissue = $this->_getParam('id');
		$result=$this->lobjstudentissuemodel->fnGetissueList($Idissue);
    	$this->view->Idissue=$Idissue;
		$this->lobjstudentissueform->populate($result);
		if ($this->getRequest()->isPost()) {
    		$formData = $this->getRequest()->getPost();
			//echo $Idissue;
			//echo "<pre>";
			//print_r($formData);die();
	    	if ($this->lobjstudentissueform->isValid($formData)) {
	    		unset ( $formData ['Save'] );
				unset ( $formData ['Back'] );
				
	   			//$lintIdProgram = $formData ['idmangesession'];
				$larrresults = $this->lobjstudentissuemodel->fnupdateissue($formData,$Idissue);//update university
				//$auth = Zend_Auth::getInstance();
				 $this->_redirect( $this->baseUrl . '/studentissues/index');
			}
    	}
	}
}
