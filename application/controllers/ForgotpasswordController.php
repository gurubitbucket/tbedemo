<?php
class ForgotpasswordController extends Zend_Controller_Action {

 	private $gstrsessionSIS;//Global Session Name
	public function init() {
  	}
	
  	
	public function indexAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->layout()->setLayout('/plain');
		$this->gstrsessionSIS = new Zend_Session_Namespace('sis');
		$lintiduser = "";
	    $lobjLoginForm = new App_Form_Login;
	    $reterivepassword = new App_Form_Forgotpassword;
	
	    $lvarpagename = $this->_getParam('page');
	    $this->view->pageName = $this->_getParam('page');
	    
	    
  	    //$this->view->lobjForgotPassword = $lobjForgotPassword;
  	    $this->view->lobjLoginForm = $lobjLoginForm;
  	    $this->view->reterivepassword = $reterivepassword;

  	    // checking wheather form is pressed or not
	    if($this->getRequest()->ispost()) {	 
			//creating db object			
			$lobjdb = Zend_Db_Table::getDefaultAdapter();
						
			$Email = $this->getRequest()->getParam('email');
			
			if($lvarpagename == 'company'){
				$lstrsql = $lobjdb->select()
						->from('tbl_companies',array('Email','IdCompany AS masterid'))
						->where("Email = '$Email' ");				
			}else if($lvarpagename == 'takaful'){
				$lstrsql = $lobjdb->select()
						->from('tbl_takafuloperator',array('email AS Email','idtakafuloperator AS masterid'))
						->where("email = '$Email' ");	
			}			
			//getting current user password from db
			$larrResult = $lobjdb->fetchRow($lstrsql);		
			if(empty($larrResult["masterid"])){
			 	echo '<script language="javascript">alert("User Not Found")</script>';
			} else {
				$lintiduser =  $larrResult['masterid'];
				if($lvarpagename == 'company'){				
					$lstrsql2 = $lobjdb	->select()
									->from(array('usr'=>'tbl_companies'),array("usr.IdCompany AS masterid","usr.CompanyName AS MasterName","usr.Email","usr.Login"))
									->where("Email = '$Email' ");	
				}else if($lvarpagename == 'takaful'){	
					$lstrsql2 = $lobjdb	->select()
									->from(array('usr'=>'tbl_takafuloperator'),array("usr.idtakafuloperator AS masterid","usr.TakafulName AS MasterName","usr.email AS Email","usr.LoginId AS Login"))
									->where("email = '$Email' ");	
				}											 			
				$larrResult2 = $lobjdb->fetchRow($lstrsql2);
				$lstrUser = $larrResult2['MasterName'];
            	$lstrUserName = $larrResult2["Login"];
            	$strEmailAddress = $larrResult2['Email'];
            	
            
            	
				/*
				 * Get the Template Details
				 */	  	
            	
            	$lobjcommonCommonModel = new App_Model_Common();
            	$StudModel = new App_Model_Studentapplication();
				$larrEmailTempResult = $StudModel->fnGetEmailTemplateDescription("Forgot Password");
				
				$stremailTemplateFrom =  $larrEmailTempResult['TemplateFrom'];
				$stremailTemplateFromDesc =  $larrEmailTempResult['TemplateFromDesc'];
	      		$stremailTemplateSubject =  $larrEmailTempResult['TemplateSubject'];
	      		$stremailTemplateBody =  $larrEmailTempResult['TemplateBody'];      

				//generate random password
				$lstrpasswd = self::fnCreateRandPassword(6);			
		
		  
				$stremailTemplateBody = str_replace("[Student]", $lstrUser , $stremailTemplateBody);
			    $stremailTemplateBody = str_replace("[LoginID]", $larrResult2["Login"] , $stremailTemplateBody);
				$stremailTemplateBody = str_replace("[Passwd]", $lstrpasswd , $stremailTemplateBody);
				 $lobjDbAdpt = Zend_Db_Table::getDefaultAdapter();
		  	    self::sendmailsAction($stremailTemplateBody,$stremailTemplateSubject,$strEmailAddress,$lstrUser);
				if($lvarpagename == 'company'){
       	 			$bind = array('Password'=>md5($lstrpasswd));
					$where = 'IdCompany = '.$lintiduser;
       	 			$lobjDbAdpt->update('tbl_companies',$bind,$where);														
				}else if($lvarpagename == 'takaful'){
					$bind = array('Password'=>md5($lstrpasswd));
					$where = 'idtakafuloperator = '.$lintiduser;
					$lobjDbAdpt->update('tbl_takafuloperator',$bind,$where);											
				}				
				echo '<script language="javascript">alert("Password Sent to Your Mail");</script>';	
				$this->_redirect( $this->baseUrl . "/introduction/index/page/".$lvarpagename."/mail/true");	
				exit;	
	      		
			}
			
		}
	}
	public function sendmailsAction($lstrEmailTemplateBody,$lstrEmailTemplateSubject,$receiver_email,$receiver){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$auth = 'ssl';
		$port = '465';
		$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#');
		$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
		$mail = new Zend_Mail();
		$mail->setBodyHtml($lstrEmailTemplateBody);
		$sender_email = 'ibfiminfo@gmail.com';
		$sender = 'ibfim';
		$mail->setFrom($sender_email, $sender)
			 ->addTo($receiver_email, $receiver)
	         ->setSubject($lstrEmailTemplateSubject);
		$mail->send($transport);			
	}
	//Action To Generate Passowrd Randomly
    public function fnCreateRandPassword($length) {
		$chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;
		$password = "";
		while ($i <= $length) {
			$password .= $chars{mt_rand(0,strlen($chars))};
			$i++;
		}
		return $password;    
	}
	
	public function getemailAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();		
		$email = $this->_getParam('email');
		$pagename = $this->_getParam('pagename');
		$lobjdb = Zend_Db_Table::getDefaultAdapter();
		if($pagename == 'company'){
			$lstrsql = $lobjdb->select()
							  ->from('tbl_companies',array('Email','IdCompany'))
							  ->where("Email = '$email' ");
		}else if($pagename == 'takaful'){	
			$lstrsql = $lobjdb->select()
					  ->from('tbl_takafuloperator',array('email AS Email','idtakafuloperator'))
					  ->where("email = '$email' ");	
		}				
		$larrResult = $lobjdb->fetchRow($lstrsql);
		echo $larrResult['Email'];
		
	}

}