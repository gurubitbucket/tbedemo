<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
class IndividualbatchreportController extends Zend_Controller_Action
{	
    public function init() 
	{		
		$this->gsessionidtakafuloperator = Zend_Registry::get('sis');
	    if (empty ( $this->gsessionidtakafuloperator->idtakafuloperator )){
			$this->_redirect ( $this->baseUrl . '/takafullogin/logout' );
		}
		$this->_helper->layout ()->setLayout ( '/takaful/usty1' );
		$this->view->translate = Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjbatchreportmodel = new App_Model_Individualbatchreportmodel();
		$this->lobjExammarksform = new  App_Form_Individualbatchform();
	}
	//function to set and display the result
	public function indexAction() 
	{    
		$this->view->lobjform = $this->lobjExammarksform;
		$larrresult = $this->lobjbatchreportmodel->fngetTakafulOperator( $this->gsessionidtakafuloperator->idtakafuloperator ); 
		$this->lobjExammarksform->Takcompnames->setValue($larrresult['TakafulName']);
		$larrpins =  $this->lobjbatchreportmodel->fnajaxgetpins($larrresult['idtakafuloperator']);
		$this->lobjExammarksform->Pin->addMultiOption('','Select');
		$this->lobjExammarksform->Pin->addMultiOptions($larrpins);
	    
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			if ($this->lobjExammarksform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				$this->view->set = 1;
				unset ($larrformData ['$larrformData']);
				$this->view->name = $larrformData['Takcompnames'];
				$this->view->Takcompnames = $lintidcomp = $this->gsessionidtakafuloperator->idtakafuloperator;
				$this->view->Pin = $lintpin = $larrformData['Pin']; 
				$larrresult = $this->lobjbatchreportmodel->fngetstuddetails($larrformData['Pin']);
				$this->view->paginator = $larrresult;
				$larrpins = $this->lobjbatchreportmodel->fnajaxgetpins($lintidcomp);
				$this->lobjExammarksform->Pin->addmultioptions($larrpins);
				$this->lobjExammarksform->Pin->setValue($larrformData['Pin']);
		   }
	   }	
    }
    public function fngetstudentmarksAction()
    {   
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$lintidapp = $this->_getParam('idapp'); 
		$lintidprog = $this->_getParam('idprog');
		$larrmarks = $this->lobjbatchreportmodel->fngetstudmarks($lintidapp);
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left"> Student Marks </legend>
						          <table class="table" border=1 align = "center" width=100%><tr><th><b>Program Name</b></th><th><b>Marks Obtained</b></th><th><b>Part A</b></th><th><b>Part B</b></th><th><b>Part C</b></th></tr><tr>';
		$tabledata.= '<td align = "left">'.$lintidprog.'</td><td align = "left">'.$larrmarks['Obtained'].'</td><td align = "left">'.$larrmarks['PartAmarks'].'</td><td align = "left">'.$larrmarks['PartBmarks'].'</td><td align = "left">'.$larrmarks['PartCmarks'].'</td></tr>';
		$tabledata.="</table></fieldset><br><br>";
		$tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closefn();'></td></tr>";
		echo  $tabledata;
    }
	public function fnexportAction()
    {   
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		$larrresult = $this->lobjbatchreportmodel->fngetstuddetails($larrformData['Pin']);
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		$filename = 'Batchregistration_Report_'.$larrformData['name'].'_'.$larrformData['Pin'];
		$ReportName = $this->view->translate( "Batchregistration" ).' '.$this->view->translate( "Report" );
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';		
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 1><b>Date </b></td><td align=left colspan = 3><b>$day</b></td><td  align=left colspan = 1><b> Time</b></td><td align=left colspan = 3><b>$time</b></td></tr>";
        $tabledata.= "<tr><td align=left colspan = 1><b>Name</b></td><td align=left colspan = 3><b>".$larrformData['name']."</b></td><td  align=left colspan = 1><b> RegistrationPin</b></td><td align=left colspan = 3><b>".$larrformData['Pin']."</b></td></tr></table>";
   	    $tabledata.= "<br><table border=1  align=center width=100%><tr><td align=center colspan = 8><b> {$ReportName}</b></td></tr></table><br>";
	    $tabledata.="<table class='table' border=1 align='center' width=100%><tr><th><b>Candidate Name</b></th><th><b>ICNO</b></th><th><b>Program Name</b></th><th><b>Exam Date</b></th><th><b>Exam Venue</b></th><th><b>Exam Session</b></th><th><b>Status</b></th><th><b>Grade</b></th></tr>";
	    foreach($larrresult as $lobjCountry)
		{
			$tabledata.="<tr><td align = 'left'>".$lobjCountry['FName']."</td><td align = 'left'>".$lobjCountry['ICNO']."</td><td align = 'left' >".$lobjCountry['ProgramName']."</td><td align = 'left'>".$lobjCountry['ExamDate']."</td><td align = 'left'>".$lobjCountry['ExamVenue']."</td><td align = 'left'>".$lobjCountry['session']."</td><td align = 'left'>".$lobjCountry['Result']."</td><td align = 'left'>".$lobjCountry['Grade']."</td></tr>";
		}
		$tabledata.="</table><br>";
    	
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		
    }
    
    
}