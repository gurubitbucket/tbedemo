<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE); 
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
class ChangepaymentController extends Zend_Controller_Action
{
    public function init()
    {
    	
    	$this->gsessionidtakafuloperator = Zend_Registry::get('sis');
    	$this->gsessionidtakafuloperator = Zend_Registry::get('sis');
	    if (empty ( $this->gsessionidtakafuloperator->idtakafuloperator )){
			$this->_redirect ( $this->baseUrl . '/takafullogin/logout' );
		}
		$this->_helper->layout ()->setLayout ( '/takaful/usty1' );
		$this->view->translate = Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
        $this->fnsetObj();
    }
    public function fnsetObj()
    {
		$this->lobjChangepaymentmodel = new App_Model_Changepayment();
		$this->lobjChangepaymentForm = new App_Form_Changepayment();  	
	}
    public function indexAction()
    {
       	$this->view->title="Pyament Setup";
		$this->view->lobjform = $this->lobjform; //send the lobjuniversityForm object to the view
				$this->view->lobjChangepaymentForm = $this->lobjChangepaymentForm; 
		$larrresult = $this->lobjChangepaymentmodel->fngettakafulname($this->gsessionidtakafuloperator->idtakafuloperator); 
		
		
		
	/*	echo "<pre>";
		print_R($larrregpins);
		die();
  */
			if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
			$larrformData = $this->_request->getPost ();
			
			
			
			$modeofpayment = $larrformData['ModeofPayment'];
			$larrregpins = $this->lobjChangepaymentmodel->fngetuppaidregpins($this->gsessionidtakafuloperator->idtakafuloperator,$modeofpayment);
		//	print_R($larrregpins);
			$this->view->paymentdetails = $larrregpins;
			$this->view->lobjChangepaymentForm->ModeofPayment->setValue($modeofpayment);
		}
    }
    
   
    public function changepaymentoptionAction()
    {
    	$_SESSION['changepayment']=1;
    	$this->view->lobjChangepaymentForm = $this->lobjChangepaymentForm; 
    	$idbatchregistration = $this->_getParam('idBatchRegistration');
        $larresultdetails = $this->lobjChangepaymentmodel->fngetpindetails($idbatchregistration);
        $modeofpayment = $larresultdetails['ModeofPayment'];
        
        $this->view->idbatchregistration = $idbatchregistration;
        $this->view->lobjChangepaymentForm->ModeofPaymentmade->setValue($modeofpayment);
        $this->view->displayedresults = $larresultdetails;
        $this->view->lobjChangepaymentForm->Amount->setValue($larresultdetails['totalAmount']);
		$larrresult = $this->lobjChangepaymentmodel->fngettakafulname($this->gsessionidtakafuloperator->idtakafuloperator); 
		$larresultpaylater = $this->lobjChangepaymentmodel->fngetpaylater($this->gsessionidtakafuloperator->idtakafuloperator);
		
		if($larresultpaylater)
		{
			 
		       $this->view->lobjChangepaymentForm->ModeofPayment->addMultiOptions(array('181' => 'Paylater'));
		}
    	
		if ($this->_request->isPost () && $this->_request->getPost( 'Pay' ))
		{
			$larrformData = $this->_request->getPost();
			$larrpreviousdetails = $this->lobjChangepaymentmodel->fnpreviouspayment($larresultdetails,$larrformData['ModeofPayment'],$this->gsessionidtakafuloperator->idtakafuloperator);
			$idstudentpaymentoption = $larresultdetails['idstudentpaymentoption'];
		
			$updatepayment = $this->lobjChangepaymentmodel->fnupdatechequepayment($idstudentpaymentoption,$larrformData['ModeofPayment']);
			if($larrformData['ModeofPayment']==4)
			{
				$idtakaful = $this->gsessionidtakafuloperator->idtakafuloperator;
				
			}
			else if($larrformData['ModeofPayment']==181)
			{
				$randomnumber1 = rand(10000,99999);
				$randomnumber2 = rand(10000,99999);
				$randomnumber  = $randomnumber1.''.$randomnumber2;
			
				$updatepaymentresult = $this->lobjChangepaymentmodel->fnupdatepaylater($idbatchregistration,$larrformData['ModeofPayment'],$randomnumber);
			}
			else if($larrformData['ModeofPayment']==2)
			{
				
				$this->_redirect ( $this->baseUrl . "/takafulapplication/confirmpayment/insertedId/$idbatchregistration/changemode/2" );
			}
			else if($larrformData['ModeofPayment']==1)
			{
				$this->_redirect ( $this->baseUrl . "/takafulapplication/fpxpageone/insertedId/" . $idbatchregistration );
			}
			
			echo '<script language="javascript">alert("Payment has been updated")</script>';
  			echo "<script>parent.location = '".$this->view->baseUrl()."/changepayment/index';</script>";
  			die();
		}
    }

}

