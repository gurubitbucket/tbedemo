<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class StudentviewdetailsController extends Zend_Controller_Action
{
	public function init() 
	{ 
		$this->_helper->layout()->setLayout('/batch3/usty1');	
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	Private function fnsetObj() 
	{
		$this->lobjcommonmodel=new GeneralSetup_Model_DbTable_Common();//object of common model
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		 
	}
		
	public function indexAction() 
	{
  	if ($this->_request->isPost())
  	{  	
		$larrformData = $this->_request->getPost ();
   		$ICNO=$larrformData['candidateicno'];
		$strQry="select ICNO from tbl_studentapplication where ICNO='$ICNO' and Examvenue!=000 and IDApplication >1148";//to check icno exists or not
		$larrcheckstudent=$this->lobjcommonmodel->fnGetValuesRow($strQry);
	if($larrcheckstudent)
	{
		$this->_redirect( $this->baseUrl . "/studentviewdetails/fnstudentview/ICNO/$ICNO");		
	}
	else 
	{
		echo '<script language="javascript">alert("We are sorry but we could not find your Registration Details.Please contact IBFIM")</script>';
		echo "<script>parent.location = '".$this->view->baseUrl()."/studentviewdetails/index';</script>";
 		die();
	}		
  	}	
	}

	public function fnstudentviewAction() 
	{ 
		$ICNO = $this->_getParam('ICNO');
		$strQry="select a.IDApplication,a.pass,b.ProgramName,a.Payment from tbl_studentapplication as a,tbl_programmaster as b where  a.Program = b.IdProgrammaster  and  a.ICNO='$ICNO' and a.Examvenue!=000 order by a.Program,a.datetime,a.pass";//to fetch all details of icno
		$larrgetalldetailsofstudent=$this->lobjcommonmodel->fnGetValues($strQry);
		$this->view->results=$larrgetalldetailsofstudent;
	
	}
		
	public function fnviewdetailsAction()
	{
		$this->_helper->layout->disableLayout();
		$appid = $this->_getParam('id');
		$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($appid);
	
		
		$this->view->idapp=$appid;
		$strQry="select a.FName,a.ICNO,a.PermAddressDetails,a.CorrAddress,a.Payment,a.batchpayment,a.Examdate,a.Exammonth,a.Amount,a.pass,b.ProgramName,d.Year as years,c.centername,c.addr1,c.addr2,e.managesessionname,e.ampmstart,e.ampmend from tbl_studentapplication as a,tbl_programmaster as b,tbl_newscheduler as d,tbl_center as c,tbl_managesession as e where a.IDApplication='$appid' and a.Program=b.IdProgrammaster and a.Year=d.idnewscheduler and a.Examvenue=c.idcenter and a.Examsession=e.idmangesession";//to fill email template
		$larrgetperticulerstd=$this->lobjcommonmodel->fnGetValuesRow($strQry);
		$strQry="select Regid from tbl_registereddetails where IDApplication='$appid'";//to get regid
		$Regid=$this->lobjcommonmodel->fnGetValuesRow($strQry);
		
		$strQry="select m.Grade,n.A,n.B,n.C from tbl_studentmarks as m,tbl_studentdetailspartwisemarks as n where m.IDApplication=n.IDApplication  and  m.IDApplication='$appid'";//to get regid
		$Graderesults=$this->lobjcommonmodel->fnGetValuesRow($strQry);
		 
		$strQry="select  group_concat(distinct(b.IdPart),'') as parts from tbl_registereddetails as a,tbl_batchdetail as b where a.IdBatch=b.IdBatch  and  a.IDApplication='$appid'";//to get regid
		$Batchid=$this->lobjcommonmodel->fnGetValuesRow($strQry);
		$resarr= explode(',',$Batchid['parts']);
		//echo count($resarr);die();
		if($resarr['0'])
		{
		$partidone=$resarr['0'];
		}
		if($resarr['1'])
		{
		$partidtwo=$resarr['1'];
		}
			
		$TemplateName="Student Attended View";	
		if($larrgetperticulerstd['pass']==3 || $larrgetperticulerstd['pass']==4 )
		{
		$TemplateName="Student Review";	
		}
		
		$strQry3="select a.idTemplate,a.TemplateName,a.TemplateFrom,a.TemplateSubject,a.TemplateFromDesc,a.TemplateHeader,a.TemplateBody from tbl_emailtemplate as a,tbl_definationms as b where a.idDefinition = b.idDefinition and b.DefinitionDesc LIKE '".$TemplateName."%'";
		$larrEmailTemplateDesc=$this->lobjcommonmodel->fnGetValuesRow($strQry3);
		//print_r($larrEmailTemplateDesc);
		 $larrEmailTemplateDesc['TemplateBody'];
		
		$regids="payment is pending. Please contact IBFIM if you have already made payment";	
	if($Regid)
	{
		$regids=$Regid['Regid'];	
	}
			if($larrgetperticulerstd['Payment']=='3')
		{
			$regids="Payment is Canceled";	
			
		}	
		$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
		$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
		$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
		$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
		$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
		$lstrEmailTemplateBody = str_replace("[Candidate]",$larrgetperticulerstd['FName'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[ICNO]",$larrgetperticulerstd['ICNO'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Program]",$larrgetperticulerstd['ProgramName'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[LoginId]",$regids,$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[venue]",$larrgetperticulerstd['centername'].' '.$larrgetperticulerstd['addr1'].' '.$larrgetperticulerstd['addr2'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Date]",$larrgetperticulerstd['Examdate'].'-'.$larrgetperticulerstd['Exammonth'].'-'.$larrgetperticulerstd['years'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Address]",$larrgetperticulerstd['PermAddressDetails'].'-'.$larrgetperticulerstd['CorrAddress'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Session]",$larrgetperticulerstd['managesessionname'].'('.$larrgetperticulerstd['ampmstart'].'--'.$larrgetperticulerstd['ampmend'].')',$lstrEmailTemplateBody);
	$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['password'],$lstrEmailTemplateBody);	
	
		$amtValue = 'NA';
		if($larrgetperticulerstd['batchpayment']==0)
		{
			$amtValue=	$larrgetperticulerstd['Amount'];
		}
		$lstrEmailTemplateBody = str_replace("[Amount]",$amtValue,$lstrEmailTemplateBody);
	    
	   if($larrgetperticulerstd['pass']!=3 || $larrgetperticulerstd['pass']!=4)
		{
		$lstrEmailTemplateBody = str_replace("[Grade]",$Graderesults['Grade'],$lstrEmailTemplateBody);
		if( count($resarr)>=2)
		$lstrEmailTemplateBody = str_replace("[Marks]",'Part '.$partidone.':'.$Graderesults[$partidone].'  '.'Part '.$partidtwo.':'.$Graderesults[$partidtwo],$lstrEmailTemplateBody);
		if(count($resarr)==1)
			$lstrEmailTemplateBody = str_replace("[Marks]",'Part '.$partidone.':'.$Graderesults[$partidone],$lstrEmailTemplateBody);
		}
		
		/*if($larrgetperticulerstd['batchpayment']!=0)
		{
		$amount='NA';
		$lstrEmailTemplateBody = str_replace("[Amount]",$amount,$lstrEmailTemplateBody);
		}
		else
		{
		$lstrEmailTemplateBody = str_replace("[Amount]",$larrgetperticulerstd['Amount'],$lstrEmailTemplateBody);
		}*/		
		
		$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;			
		$this->view->template = $lstrEmailTemplateBody;
							
	}
	
	
	public function fnexportexcelAction()
	{
	    $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$day= date("d-m-Y");
		$host = $_SERVER['SERVER_NAME'];
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$time = date('h:i:s',time());
		//$filename = 'Result';
   	    $appid = $this->_getParam('id');
	$larrresult = $this->lobjstudentmodel->fnviewstudentdetailssss($appid);
		
		$this->view->idapp=$appid;
		$strQry="select a.FName,a.ICNO,a.PermAddressDetails,a.CorrAddress,a.Payment,a.batchpayment,a.Examdate,a.Exammonth,a.Amount,a.pass,b.ProgramName,d.Year as years,c.centername,c.addr1,c.addr2,e.managesessionname,e.ampmstart,e.ampmend from tbl_studentapplication as a,tbl_programmaster as b,tbl_newscheduler as d,tbl_center as c,tbl_managesession as e where a.IDApplication='$appid' and a.Program=b.IdProgrammaster and a.Year=d.idnewscheduler and a.Examvenue=c.idcenter and a.Examsession=e.idmangesession";//to fill email template
		$larrgetperticulerstd=$this->lobjcommonmodel->fnGetValuesRow($strQry);
		$strQry="select Regid from tbl_registereddetails where IDApplication='$appid'";//to get regid
		$Regid=$this->lobjcommonmodel->fnGetValuesRow($strQry);
		
		$strQry="select m.Grade,n.A,n.B,n.C from tbl_studentmarks as m,tbl_studentdetailspartwisemarks as n where m.IDApplication=n.IDApplication  and  m.IDApplication='$appid'";//to get regid
		$Graderesults=$this->lobjcommonmodel->fnGetValuesRow($strQry);
		 
		$strQry="select  group_concat(distinct(b.IdPart),'') as parts from tbl_registereddetails as a,tbl_batchdetail as b where a.IdBatch=b.IdBatch  and  a.IDApplication='$appid'";//to get regid
		$Batchid=$this->lobjcommonmodel->fnGetValuesRow($strQry);
		$resarr= explode(',',$Batchid['parts']);
		//echo count($resarr);die();
		if($resarr['0'])
		{
		$partidone=$resarr['0'];
		}
		if($resarr['1'])
		{
		$partidtwo=$resarr['1'];
		}
			
		$TemplateName="Student Attended View";	
		if($larrgetperticulerstd['pass']==3 || $larrgetperticulerstd['pass']==4 )
		{
		$TemplateName="Student Review";	
		}
		
		$strQry3="select a.idTemplate,a.TemplateName,a.TemplateFrom,a.TemplateSubject,a.TemplateFromDesc,a.TemplateHeader,a.TemplateBody from tbl_emailtemplate as a,tbl_definationms as b where a.idDefinition = b.idDefinition and b.DefinitionDesc LIKE '".$TemplateName."%'";
		$larrEmailTemplateDesc=$this->lobjcommonmodel->fnGetValuesRow($strQry3);
		//print_r($larrEmailTemplateDesc);die();
		
		$regids="payment is pending. Please contact IBFIM if you have already made payment";	
	if($Regid)
	{
		$regids=$Regid['Regid'];	
	}

if($larrgetperticulerstd['Payment']=='3')
		{
			$regids="Payment is Canceled";	
			
		}	
					
		$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
		$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
		$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
		$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
		$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
		$lstrEmailTemplateBody = str_replace("[Candidate]",$larrgetperticulerstd['FName'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[ICNO]",$larrgetperticulerstd['ICNO'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Program]",$larrgetperticulerstd['ProgramName'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[LoginId]",$regids,$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[venue]",$larrgetperticulerstd['centername'].' '.$larrgetperticulerstd['addr1'].' '.$larrgetperticulerstd['addr2'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Date]",$larrgetperticulerstd['Examdate'].'-'.$larrgetperticulerstd['Exammonth'].'-'.$larrgetperticulerstd['years'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Address]",$larrgetperticulerstd['PermAddressDetails'].'-'.$larrgetperticulerstd['CorrAddress'],$lstrEmailTemplateBody);
		$lstrEmailTemplateBody = str_replace("[Session]",$larrgetperticulerstd['managesessionname'].'('.$larrgetperticulerstd['ampmstart'].'--'.$larrgetperticulerstd['ampmend'].')',$lstrEmailTemplateBody);
	$lstrEmailTemplateBody = str_replace("[Password]",$larrresult['password'],$lstrEmailTemplateBody);	
	
		$amtValue = 'NA';
		if($larrgetperticulerstd['batchpayment']==0)
		{
			$amtValue=	$larrgetperticulerstd['Amount'];
		}
		$lstrEmailTemplateBody = str_replace("[Amount]",$amtValue,$lstrEmailTemplateBody);
	    
	   if($larrgetperticulerstd['pass']!=3 || $larrgetperticulerstd['pass']!=4)
		{
		$lstrEmailTemplateBody = str_replace("[Grade]",$Graderesults['Grade'],$lstrEmailTemplateBody);
		if( count($resarr)>=2)
		$lstrEmailTemplateBody = str_replace("[Marks]",'Part '.$partidone.':'.$Graderesults[$partidone].'  '.'Part '.$partidtwo.':'.$Graderesults[$partidtwo],$lstrEmailTemplateBody);
		if(count($resarr)==1)
			$lstrEmailTemplateBody = str_replace("[Marks]",'Part '.$partidone.':'.$Graderesults[$partidone],$lstrEmailTemplateBody);
		}
		
		/*if($larrgetperticulerstd['batchpayment']!=0)
		{
		$amount='NA';
		$lstrEmailTemplateBody = str_replace("[Amount]",$amount,$lstrEmailTemplateBody);
		}
		else
		{
		$lstrEmailTemplateBody = str_replace("[Amount]",$larrgetperticulerstd['Amount'],$lstrEmailTemplateBody);
		}*/		
		
		$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;	
		$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" /><br>';
   	    $tabledata.= '<table border=1 align=center width=100%>
	                       <tr><td>'.$lstrEmailTemplateBody.'</td></tr></table>';
	        $filename = $larrgetperticulerstd['FName'];               
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
    }


}
