<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class CenterauthController extends Zend_Controller_Action { //Controller for the User Module
public $gsessionidCenter;//Global Session Name
	public function init() { //initialization function		
		$this->gsessionidCenter = Zend_Registry::get('sis'); 	
		if(empty($this->gsessionidCenter->idcenter)){ 
			$this->_redirect( $this->baseUrl . '/batchlogin/logout');					
		}
		$this->_helper->layout()->setLayout('/examcenter/usty1');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	public function fnsetObj() {
		$this->lobjCommon = new App_Model_Common();
		$this->lobjCenterloginmodel = new App_Model_Centerlogin(); //user model object
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates(); //user model object		
		$this->lobjBatchcandidatesForm = new App_Form_Batchcandidates (); //intialize user lobjuserForm
		$this->lobjAdhocApplicationForm=new App_Form_Adhocapplication();
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjdeftype = new App_Model_Definitiontype();
	}
	
	public function indexAction() {
		
		$larrresult = $this->lobjCenterloginmodel->fngetCenterDetails($this->gsessionidCenter->idcenter);
		$this->view->centername = $larrresult['centername'];
		$this->view->contactperson = $larrresult['contactperson'];
		$this->view->seessionidcenter =$this->gsessionidCenter->idcenter;
		$presntdate=date("Y-m-d");
		$this->view->examsession = $larrsessionresult = $this->lobjCenterloginmodel->fngetsessionDetails($this->gsessionidCenter->idcenter,$presntdate);
		$this->view->hur = date('H');
		$this->view->mun = date('i');
		$this->view->sur = date('s');
		
		$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($this->gsessionidCenter->idcenter,date('Y'));
		foreach($resultcount as $resultcounts) {
			$maxcolourcodes[] = $resultcounts['countidmanagesession'];
		}
		$this->view->maxcolourcodes = max($maxcolourcodes);
		
		$this->view->currenttime = date("H:i:s");
	}
	
	public function showsessionsAction() {
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		$this->view->seessionidcenter =$this->gsessionidCenter->idcenter;
		$iddays= $this->_getParam('iddays');
		$idmonth= $this->_getParam('idmonth');
		$presentyear = date("Y");
		if($iddays<10)
	   	{
	   		$iddays = '0'.$iddays;
	   	}
	   	else 
	   	{
	   		$iddays = $iddays;
	   	}
	   	
	    if($idmonth<10)
	   	{
	   		$idmonth = '0'.$idmonth;
	   	}
	   	else {
	   		$idmonth = $idmonth;
	   	}
		
		$this->view->normaldate= $normaldate = $presentyear."-".$idmonth."-".$iddays;
		$dauyoftheweek = $this->lobjCenterloginmodel->fngetdayofdate($normaldate);
		if($dauyoftheweek['days']==1)
		{
			$dauyoftheweek['days']= 7;
		}
		else 
		{
			$dauyoftheweek['days']=$dauyoftheweek['days']-1;	
		}
		
		$this->view->examsession = $larrsessionresultbasedoncenter = $this->lobjCenterloginmodel->fngetsessionDetailsBasedOnCenter($this->gsessionidCenter->idcenter,$dauyoftheweek['days'],$presentyear);
		
		$this->view->normalyear = $presentyear;
		$this->view->normalmonth = $this->_getParam('idmonth');
		$this->view->normalday = $this->_getParam('iddays');
	}
	
	
	
	
	public function insertintotempAction()
	{
 		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();		
		$issession= $this->_getParam('issession');
		$idcenter = $this->_getParam('idcenter');
		$endtime = $this->_getParam('endtime');
		
		$sessionID = Zend_Session::getId();
	    $date =date('d');
		if($date<10)
		{
			$date = $date[1];
		}
		
		
		$hour = date('H');
		$minutes = date('i');
		$hours = $hour*60;
		$startedmin = $minutes+$hours;
		$presntdate = date('Y-m-d');
		$larrsessionresult = $this->lobjCenterloginmodel->fngetprogramDetails($idcenter,$issession,$presntdate);
		$todaytime = date('H:i:s');
		
		if($endtime <= $todaytime) {
			echo "0";
		} else {
			foreach($larrsessionresult as $larrsessionresults) {
				$totaltime = $this->lobjCenterloginmodel->fngetactivetotaltime($larrsessionresults['IdProgrammaster']);
				$totaltimerequired = $totaltime[0]['TimeLimit'];
				$larrresult = $this->lobjCenterloginmodel->fninsertintotemp($idcenter,$sessionID,$larrsessionresults['IdProgrammaster'],$startedmin,$totaltimerequired,$date,$issession);
			}
			echo "1";
		}

	}
	
	
public function changepasswordAction() { // action for search and view
		$larrresult = $this->lobjCenterloginmodel->fngetCenterDetails($this->gsessionidCenter->idcenter); //get user details
 		$this->view->lobjAdhocApplicationForm = $this->lobjAdhocApplicationForm;
		$idcomp=$this->gsessionidCenter->idcenter;
		//print_r($larrresult);die();
		$pass=$larrresult['centerpassword'];
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save')){
			$larrformData = $this->_request->getPost ();
			if ($this->lobjAdhocApplicationForm->isValid($larrformData)) {	
			
			$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );
			$larrformData['oldpassword']=md5($larrformData['oldpassword']);
			//print_r($larrformData);die();
		if($pass==$larrformData['oldpassword'])
		{
			$larrformData['newpassword']=md5($larrformData['newpassword']);
           //echo $larrformData['newpassword'];die();
			$lastInsId = $this->lobjCenterloginmodel->updatecenterpass($larrformData['newpassword'],$idcomp);	
		  	$this->_redirect( $this->baseUrl . "/centerauth/index");
		
		}
		else 
		{
			echo '<script language="javascript">alert("The Old Password is Not Correct")</script>';
				//$this->_redirect( $this->baseUrl . "/companyapplication/changepassword");
  			//die();
			
		}	
			
		}
		}
	}
	
	public function newfnnewcaleshowAction() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		//Get Country Id
		$NewCity = $this->gsessionidCenter->idcenter;
		$year = date("Y"); 
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
		$this->lobjCenterloginmodel = new App_Model_Centerlogin(); //user model object
		
		$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($this->gsessionidCenter->idcenter,$year);
		foreach($resultcount as $resultcounts) {
			$maxcolourcodes[] = $resultcounts['countidmanagesession'];
		}
		$this->view->maxcolourcodes = max($maxcolourcodes);
		
		
		
		$resultcount = $this->lobjCenterloginmodel->fngetcountofsessions($NewCity,$year);

		$mondays="#FFFFF";
		$tuesdays="#FFFFF";
		$wednesdays="#FFFFF";
		$thursdays="#FFFFF";
		$fridays="#FFFFF";
		$saturdays="#FFFFF";
		$sundays="#FFFFF";
		for($k=0;$k<count($resultcount);$k++)
				{
			
					switch($resultcount[$k]['countidmanagesession'])
					{

						 Case 1:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="green";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="green";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="green";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="green";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="green";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="green";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="green";
							 	   	  break;
						 		}
						 		break;   

						 Case 2:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="skyblue";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="skyblue";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="skyblue";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="skyblue";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="skyblue";
							 	   	  break;
						 		}
						 		break; 
						 		
						 Case 3:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="violet";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="violet";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="violet";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="violet";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="violet";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="violet";
							 	   	  break;
						 		}
						 		break; 
						
						 Case 4:
						 	   switch($resultcount[$k]['Days'])
						 	   {
							 	     Case 1:
							 	     	  $mondays="pink";
							 	     	  break;
							 	    
									Case 2:
							 	     	  $tuesdays="pink";
							 	     	  break;
	
							 	   Case 3:
							 	   	  $wednesdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 4:
							 	   	  $thursdays="pink";
							 	   	  break;
							 	     	  
							 	    Case 5:
							 	   	  $fridays="pink";
							 	   	  break;
							 	     	  
							 	    Case 6:
							 	   	  $saturdays="pink";
							 	   	  break;
							 	     	  
							 	   Case 7:
							 	   	  $sundays="pink";
							 	   	  break;
						 		}
						 		break; 
						}
				}
     
		$curmonth = date('m');  
		$monat=date('n');
		$jahr=$year;
		$heute=date('d');
		$monate=array('January','February','March','April','May','June','July','August','September','October','November','December');
		echo '<table border=0  width=100% align=center>';
		echo '<th colspan=4 align=center style="font-family:Verdana; font-size:18pt; color:#ff9900;"></th>';
		$cnt=0;
		for($reihe=1;$reihe<=3;$reihe++)
		{
			echo '<tr>';
			for ($spalte=1;$spalte<=4;$spalte++)
			{
				$cnt++;
				$larrdays = $this->lobjstudentmodel->fngetdays($NewCity,$cnt,$year);
				//echo "<pre>";
				//print_r($larrdays);
				if(count($larrdays)<1)
				{
					$larrdays = $this->lobjstudentmodel->fngetdaysto($NewCity,$cnt,$year);
				}
				if(count($larrdays)<1)
				{
					$larrdays = $this->lobjstudentmodel->fngetdaysbetween($NewCity,$cnt,$year);
				}
				/*	print_r($larrdays);
					die();*/
				$monday=0;
				$tuesday=0;
				$wednesday=0;
				$thursday=0;
				$friday=0;
				$saturday=0;
				$sunday=0;
				for($j=0;$j<count($larrdays);$j++)
				{
					if($larrdays[$j]['Days']==1)
						$monday=1;
					if($larrdays[$j]['Days']==2)
						$tuesday=1;
					if($larrdays[$j]['Days']==3)
						$wednesday=1;
					if($larrdays[$j]['Days']==4)
						$thursday=1;
					if($larrdays[$j]['Days']==5)
						$friday=1;
					if($larrdays[$j]['Days']==6)
						$saturday=1;
					if($larrdays[$j]['Days']==7)
						$sunday=1;
				}
				
				

				
				
				$curmonth = date('m');
				if($curmonth==$cnt)
				{
					$this_month=($reihe-1)*4+$spalte;
					$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
					$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
					if($erster==0){$erster=7;}
					echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
					echo '<table width=80% height="80%" border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
					echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
					echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
					echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
					echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
					echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
					echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
					echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
					echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
					echo '<tr>';
					$i=1;
					while($i<$erster){echo '<td> </td>'; $i++;}
					$i=1;
					while($i<=$insgesamt)
					{
						$rest=($i+$erster-1)%7;
						if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
						else{echo '<td  align=center ';}
						$curdate = date('d');
						if($i<$curdate)
						{
							if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
						}
						else 
						{
							if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
						}
						if($rest==0){echo "</tr>\n<tr>\n";}
						$i++;
					}
					echo '</tr>';
					echo '</table>';
					echo '</td>';
				}
				else
				{
					if($cnt>=$curmonth)
					{
						$this_month=($reihe-1)*4+$spalte;
						$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
						$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
						if($erster==0){$erster=7;}
						echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
						echo '<table width=80% height="80%" border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
						echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
						echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
						echo '<tr>';
						$i=1;
						while($i<$erster){echo '<td> </td>'; $i++;}
						$i=1;
						while($i<=$insgesamt)
						{
							$rest=($i+$erster-1)%7;
							if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
							else{echo '<td  align=center ';}
							$curdate = 0;
							if($i<$curdate)
							{
								if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else{echo $i;}
								echo "</td>\n";
							}
							else 
							{
								if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$sundays.'"  style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$mondays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$tuesdays.'"  style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1]; echo'bgcolor="'.$wednesdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;"  id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo'bgcolor="'.$thursdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$fridays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo'bgcolor="'.$saturdays.'" style="font-size:10pt; font-family:Verdana;border:1px solid green;" id="'.$i.''.$this_month.'"><span onclick="funct('.$i.','.$this_month.')">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
								else{echo $i;}
								echo "</td>\n";
							}
							if($rest==0){echo "</tr>\n<tr>\n";}
							$i++;
						}
						echo '</tr>';
						echo '</table>';
						echo '</td>';
					}
					else 
					{
						$this_month=($reihe-1)*4+$spalte;
						$erster=date('w',mktime(0,0,0,$this_month,1,$jahr));
						$insgesamt=date('t',mktime(0,0,0,$this_month,1,$jahr));
						if($erster==0){$erster=7;}
						echo '<td width="25%" height="200" cellpadding="10px" style="border : 1px solid black; ">';
						echo '<table width=80% height="80%" border=0 align=center style="font-size:10pt;font-family:Verdana;background-color: #f6f6f6;border : 1px solid #cccccc">';
						echo '<th colspan=7 align=center style="COLOR:#000000;font-size:14px;background-color: #FFFFFF"><div align="center"> '.$monate[$this_month-1].' '.$presentyear.'</div></th>';
						echo '<tr><td align=center height="20px" style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Mon</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Tue</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Wed</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Thu</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Fri</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sat</b></td>';
						echo '<td align=center style="COLOR:#ffffff;font-size:11px;background-color: #808080;width:50px"><b>Sun</b></td></tr>';
						echo '<tr>';
						$i=1;
						while($i<$erster){echo '<td> </td>'; $i++;}
						$i=1;
						while($i<=$insgesamt)
						{
							$rest=($i+$erster-1)%7;
							if($i==$heute && $this_month==$monat){echo '<td style="font-size:10pt; font-family:Verdana;border:1px solid green" align=center ';}
							else{echo '<td  align=center ';}
							$curdate = 0;
							if($rest==0){ if($sunday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==1){if($monday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==2){if($tuesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==3){if($wednesday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==4){if($thursday>0){   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==5){if($friday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else if($rest==6){if($saturday>0) {   $va =$monate[$this_month-1];echo '><span id="'.$i.''.$this_month.'" style="color:#000000">'.$i.'</span>';} else echo '<span style="color:#000000">'.$i.'</span>';}
							else{echo $i;}
							echo "</td>\n";
							if($rest==0){echo "</tr>\n<tr>\n";}
							$i++;
						}
						echo '</tr>';
						echo '</table>';
						echo '</td>';
					}
				}
			  
			}
			echo '</tr>';
		}
		echo '</table>';
	}
		
}