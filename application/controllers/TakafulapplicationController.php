<?php
error_reporting ( E_ALL ^ E_WARNING );
error_reporting ( E_ALL ^ E_NOTICE );
class TakafulapplicationController extends Zend_Controller_Action { //Controller for the User Module
	
	public function init() { //initialization function
		$this->gsessionidtakafuloperator = Zend_Registry::get ( 'sis' );
		if (empty ( $this->gsessionidtakafuloperator->idtakafuloperator )) {
			$this->_redirect ( $this->baseUrl . '/takafullogin/logout' );
		}

		$this->_helper->layout ()->setLayout ( '/takaful/usty1' );
		$this->view->translate = Zend_Registry::get ( 'Zend_Translate' );
		Zend_Form::setDefaultTranslator ( $this->view->translate );
		$this->fnsetObj ();
	}
	

	public function fnsetObj() {
		$this->lobjTakafulmodel = new App_Model_Takafulapplication ();
		$this->lobjTakafulForm = new App_Form_Takafulapplication (); //intialize user lobjuserForm
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates ();
		$this->lobjTakafulcandidatesForm = new App_Form_Takafulcandidates ();
		$this->lobjCompanypayment = new Finance_Model_DbTable_Approvecreditstudenttakaful ();
		$this->lobjform = new App_Form_Search ();
		$this->registry = Zend_Registry::getInstance ();
		$this->locale = $this->registry->get ( 'Zend_Locale' );
	}
	
	public function indexAction() {
		$this->view->editid = $this->_getParam ( 'editid' );
		$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $this->gsessionidtakafuloperator->idtakafuloperator ); //get user details
		$this->view->lobjTakafulForm = $this->lobjTakafulForm;
		$this->view->lobjTakafulcandidatesForm = $this->lobjTakafulcandidatesForm;
		$this->view->takafulDetails = $larrresult;
		$larrresultrespin = $this->lobjTakafulmodel->fngetRegistratinpin ( $this->gsessionidtakafuloperator->idtakafuloperator );
		
		$larrresulttakaful = $this->lobjTakafulmodel->listBatchApplication ( $this->gsessionidtakafuloperator->idtakafuloperator );
		
		$this->view->application = $larrresulttakaful;
		$this->lobjTakafulForm->IdRegister->addMultiOptions ( $larrresultrespin );
		if ($this->gsessionidtakafuloperator->mess != "")
		$this->view->alertError = $this->gsessionidtakafuloperator->mess;
		$this->gsessionidtakafuloperator->mess = "";
		if ($this->_getParam ( 'IdRegister' )) {
			$IdRegister = $this->_getParam ( 'IdRegister' );
			$larrresultBatch = $this->lobjTakafulmodel->fngetBatchDetails ( $IdRegister );
			if ($larrresultBatch) {
				$batchId = $larrresultBatch ['idBatchRegistration'];
				if ($larrresultBatch ['AdhocDate'] == '0000-00-00') {
					$this->_redirect ( $this->baseUrl . "/takafulcandidates/index/batchId/$batchId" );
				} else {
					$this->_redirect ( $this->baseUrl . "/takafulcandidates/adhoc/batchId/$batchId" );
				}
				exit ();
			} else {
				$this->gsessionidtakafuloperator->mess = 'Rgistration Pin is incorrect';
				$this->_redirect ( $this->baseUrl . "/takafulapplication/index" );
				exit ();
			}
			$this->_redirect ( $this->baseUrl . "/takafulapplication/index" );
		}
	}
	
	public function newapplicationAction() { // action for search and view
		
		$this->view->lobjform = $this->lobjform;
		$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $this->gsessionidtakafuloperator->idtakafuloperator ); //get user details
		$this->view->lobjTakafulForm = $this->lobjTakafulForm;
		$this->view->takafulDetails = $larrresult;
		$month = date ( "m" ); // Month value
		$day = date ( "d" ); //today's date
		$year = date ( "Y" ); // Year value
		$yesterdaydate = date ( 'Y-m-d', mktime ( 0, 0, 0, $month, ($day - 1), $year ) );
		$datsearch = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}";
		$this->view->lobjform->FromDate->setAttrib ( 'constraints', $datsearch );
		$this->view->lobjform->FromDate->setAttrib ( 'required', "true" );
		$this->view->lobjform->ToDate->setAttrib ( 'required', "true" );
		$larrbatchresult = $this->lobjTakafulmodel->fnGetProgramNamess();
		$this->view->lobjTakafulForm->Programsearch->addMultiOptions ( $larrbatchresult );
		if ($this->_request->isPost () && $this->_request->getPost ( 'Print' )) {
			$larrformData = $this->_request->getPost ();
			$larrresultcenter = $this->lobjTakafulmodel->fnGetexamcenter ( $larrformData );
			$this->view->lobjform->FromDate->setValue ( $larrformData ['FromDate'] );
			$this->view->lobjform->ToDate->setValue ( $larrformData ['ToDate'] );
			$this->view->examcenter = $larrresultcenter;
		}
		$larrresultdiscount = $this->lobjTakafulmodel->fngetintialdiscount ();
		$this->view->discount = $larrresultdiscount ['Discount'];
		$larrcaptionresult = $this->lobjTakafulmodel->fnGetCaptionName ();
		if ($larrcaptionresult ['CourseAliasName']) {
			$this->view->caption = $larrcaptionresult ['CourseAliasName'];
		}
		$larrbatchresult = $this->lobjTakafulmodel->fnGetProgramNamess();
		$this->lobjTakafulForm->idPrograms->addMultiOptions ( $larrbatchresult );
		$this->lobjTakafulForm->idTakaful->setValue ( $this->gsessionidtakafuloperator->idtakafuloperator );
		///////////////////////////for pay later/////////////////////////
		$larrpaylater = $this->lobjTakafulmodel->fngetpaylater ( $this->gsessionidtakafuloperator->idtakafuloperator );
		$cntpaylater = count ( $larrpaylater );
		if ($cntpaylater > 1) {
			$this->lobjTakafulForm->ModeofPayment->addMultiOption ( $larrpaylater ['idDefinition'], $larrpaylater ['DefinitionDesc'] );
		}
		$this->lobjTakafulForm->ModeofPayment->setValue ( 181 );
		//////////////////////////end for pay later/////////////////////
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost ();
			//echo "<pre/>";print_r($larrformData);die();
			$larrformData['Servicetax'] = number_format($larrformData['Servicetax'], 2);
			if ($this->lobjTakafulForm->isValid ( $larrformData )) {
				$larrformData ['UpdUser'] = 1;
				$larrformData ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
				$lastInsId = $this->lobjTakafulmodel->fnInsertPaymentdetails ( $larrformData );
				$lastpaymentid = $this->lobjTakafulmodel->fnInsertStudentPaymentdetails ( $larrformData ['ModeofPayment'], $lastInsId );
				$lastpayid = $this->lobjTakafulmodel->fngetmodeofpayment ( $lastpaymentid );
				if ($larrformData ['ModeofPayment'] == 181) {
					$larrformDataapp ['IDApplication'] = $lastInsId;
					$larrformDataapp ['companyflag'] = 1;
					$larrformDataapp ['Amount'] = $larrformData ['grossAmt'];
					$larrformDataapp ['UpdUser'] = 1;
					$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
					$regid = $this->lobjCompanypayment->fngeneraterandom ();
					$flag=0;// IF ordinary registration	
					$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);
				}
				if ($lastpayid ['ModeofPayment'] == 4) {
					$this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
				}
			if ($lastpayid['ModeofPayment'] == 10) {
					$this->_redirect ( $this->baseUrl . "/takafulapplication/migspayment/insertedid/" . $lastInsId );
				}
				if ($lastpayid ['ModeofPayment'] > 100) {
					$this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
				}
				if ($lastpayid ['ModeofPayment'] == 1) {
					$this->_redirect ( $this->baseUrl . "/takafulapplication/fpxpageone/insertedId/" . $lastInsId );
				} else if ($lastpayid ['ModeofPayment'] != 2) {
					$this->_redirect ( $this->baseUrl . "/takafulapplication/index/editid/" . $lastInsId );
				} else {
					$this->_redirect ( $this->baseUrl . "/takafulapplication/confirmpayment/insertedId/$lastInsId" );
					exit ();
				}
				$this->_redirect ( $this->baseUrl . "/takafulapplication/display/insertedid/" . $lastInsId );
				exit ();
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {
			$this->_redirect ( $this->baseUrl . "/takafulapplication/newapplication/" );
		}
	}
	
	public function venueseatavailabilityAction() {
		
		$this->_helper->layout->disableLayout ();
		$idvenue = $this->_getParam ( 'idvenue' );
		$fromdate = $this->_getParam ( 'fromdate' );
		$todate = $this->_getParam ( 'todate' );		
		$larrresultcenter = $this->lobjTakafulmodel->fnGetexamcenterfull ( $idvenue, $fromdate, $todate );		
		$this->view->examcenterfull = $larrresultcenter;	
	}
	
	public function showpopupregistrationAction() {
		$this->_helper->layout->disableLayout ();
		$idregpin = $this->_getParam ( 'idbatch' );			
		$larrsultfailedStud=$this->lobjTakafulmodel->fnGetfailedregistrered ($idregpin);		
		$this->view->failedstudentlist=$larrsultfailedStud;
		$larrresultStudent = $this->lobjTakafulmodel->fnGetStudregistrered ($idregpin);
		$this->view->regpin = $idregpin;
		if (!$larrresultStudent) {
			$this->view->showshceduler = 0;
		} else {
			$this->view->showshceduler = 1;
		}	
	}
	
	public function displayAction() {
		//$this->view->lobjstudentForm = $this->lobjstudentForm;
		$lintinsertedId = $this->_getParam ( 'insertedid' );
		$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $this->gsessionidtakafuloperator->idtakafuloperator );
		$larrPaymentDetails = $this->lobjTakafulmodel->fngetPaymentDetails ( $lintinsertedId );
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $lintinsertedId;
	}
	
	public function adhocapplicationAction() { // action for search and view
		

		$lobjform = $this->view->lobsearchform = new App_Form_Search (); //send the lobjuserForm object to the view
		$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $this->gsessionidtakafuloperator->idtakafuloperator ); //get user details
		$this->view->lobjTakafulForm = $this->lobjTakafulForm;
		$this->view->takafulDetails = $larrresult;
		
		$larrresultdiscount = $this->lobjTakafulmodel->fngetintialdiscount ();
		
		$this->view->discount = $larrresultdiscount ['Discount'];
		
		$this->view->lobjAdhocApplicationForm = $this->lobjAdhocApplicationForm;
		$larrcaptionresult = $this->lobjTakafulmodel->fnGetCaptionName ();
		if ($larrcaptionresult ['CourseAliasName']) {
			$this->view->caption = $larrcaptionresult ['CourseAliasName'];
		}
		$larrbatchresult = $this->lobjTakafulmodel->fnGetProgramName ();
		//$this->lobjstudentForm->idPrograms->addMultiOption('0','Select');
		$this->lobjTakafulForm->idPrograms->addMultiOptions ( $larrbatchresult );
		$this->lobjTakafulForm->idTakaful->setValue ( $this->gsessionidtakafuloperator->idtakafuloperator );
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost ();
			if ($this->lobjTakafulForm->isValid ( $larrformData )) {
				$larrformData ['UpdUser'] = 1;
				$larrformData ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
				
				$lastInsId = $this->lobjTakafulmodel->fnInsertPaymentdetailsadhoc ( $larrformData );
				$lastpaymentid = $this->lobjTakafulmodel->fnInsertStudentPaymentdetails ( $larrformData ['ModeofPayment'], $lastInsId );
				$lastpayid = $this->lobjTakafulmodel->fngetmodeofpayment ( $lastpaymentid );
				if ($lastpayid ['ModeofPayment'] != 2) {
					//$this->_redirect( $this->baseUrl . "/takafulapplication/index");
					$this->_redirect ( $this->baseUrl . "/takafulapplication/index/editid/" . $lastInsId );
				} else {
					$this->_redirect ( $this->baseUrl . "/takafulapplication/confirmpayment/insertedId/$lastInsId" );
					exit ();
				}
			}
		}
	}
	
	public function confirmpaymentAction() {
		//$this->view->lobjstudentForm = $this->lobjstudentForm;
		
		$changepayment = $this->_getParam('changemode');
		if($changepayment=='')
		{
			$_SESSION['changepayment']=0;
		}
		
		$lintinsertedId = $this->_getParam ( 'insertedId' );
		$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $this->gsessionidtakafuloperator->idtakafuloperator );
		$larrPaymentDetails = $this->lobjTakafulmodel->fngetPaymentDetails ( $lintinsertedId );
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $lintinsertedId;
		//Get SMTP Mailing Server Setting Details
		$postArray = $this->_request->getPost ();
		$this->view->pageStatus = 1;
		if ($postArray) {
			require_once ('Zend/Mail.php');
			require_once ('Zend/Mail/Transport/Smtp.php');
			if ($postArray ['payment_status'] = 'Completed') {
				$postArray ['UpdUser'] = 1; //$auth->getIdentity()->iduser;
				$postArray ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
				$postArray ['Regid'] = substr ( $postArray ['txn_id'], 1, 6 ) . rand ( 1000, 9999 ) . substr ( $postArray ['txn_id'], 5, 9 );
				$this->lobjTakafulmodel->fnInsertPaypaldetails ( $postArray, $this->gsessionidtakafuloperator->idtakafuloperator, $lintinsertedId );
				$this->view->Regid = $postArray ['Regid'];
				$this->view->mess = "Payment Completed Sucessfully";
				$this->view->pageStatus = 2;
				$StudModel = new App_Model_Studentapplication ();
				$larrSMTPDetails = $StudModel->fnGetSMTPSettings ();
				$lstrSMTPServer = $larrSMTPDetails ['SMTPServer'];
				$lstrSMTPUsername = $larrSMTPDetails ['SMTPUsername'];
				$lstrSMTPPassword = $larrSMTPDetails ['SMTPPassword'];
				$lstrSMTPPort = $larrSMTPDetails ['SMTPPort'];
				$lstrSSL = $larrSMTPDetails ['SSL'];
				$lstrSMTPFromEmail = $larrSMTPDetails ['DefaultEmail'];
				
				$lobjTransport = new Zend_Mail_Transport_Smtp ();
				$lobjProtocol = new Zend_Mail_Protocol_Smtp ( $lstrSMTPServer );
				
				//Get Email Template Description
				$larrEmailTemplateDesc = $StudModel->fnGetEmailTemplateDescription ( "Batch Registration" );
				
				//Get Student's Mailing Details
				$larrStudentMailingDetails = $larrresult;
				
				if ($larrEmailTemplateDesc ['TemplateFrom'] != "") {
					$lstrEmailTemplateFrom = $larrEmailTemplateDesc ['TemplateFrom'];
					$lstrEmailTemplateFromDesc = $larrEmailTemplateDesc ['TemplateFromDesc'];
					$lstrEmailTemplateSubject = $larrEmailTemplateDesc ['TemplateSubject'];
					$lstrEmailTemplateBody = $larrEmailTemplateDesc ['TemplateBody'];
					$lstrEmailTemplateFooter = $larrEmailTemplateDesc ['TemplateFooter'];
					
					$larrEmailIds [0] = $larrStudentMailingDetails ["Email"];
					$larrNames [0] = $larrStudentMailingDetails ['CompanyName'];
					$lstrStudentName = $larrStudentMailingDetails ['CompanyName'];
					
					try {
						$lobjProtocol->connect ();
						$lobjProtocol->helo ( $lstrSMTPUsername );
						$lobjTransport->setConnection ( $lobjProtocol );
						
						//Intialize Zend Mailing Object
						$lobjMail = new Zend_Mail ();
						
						$lobjMail->setFrom ( $lstrSMTPFromEmail, $lstrEmailTemplateFromDesc );
						$lobjMail->setHeaderEncoding ( Zend_Mime::ENCODING_BASE64 );
						$lobjMail->addHeader ( 'MIME-Version', '1.0' );
						$lobjMail->setSubject ( $lstrEmailTemplateSubject );
						
						for($lintI = 0; $lintI < count ( $larrEmailIds ); $lintI ++) {
							if ($larrEmailIds [$lintI] != "") {
								$lobjMail->addTo ( $larrEmailIds [$lintI], $larrNames [$lintI] );
								
								//replace tags with values
								//$Link = "<a href='".$this->Url."/registration'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
								$lstrEmailTemplateBody = str_replace ( "[Company]", $lstrStudentName, $lstrEmailTemplateBody );
								$lstrEmailTemplateBody = str_replace ( "[Amount]", $postArray ['payment_gross'], $lstrEmailTemplateBody );
								$lstrEmailTemplateBody = str_replace ( "[TransactionId]", $postArray ['txn_id'], $lstrEmailTemplateBody );
								$lstrEmailTemplateBody = str_replace ( "[LoginId]", $postArray ['Regid'], $lstrEmailTemplateBody );
								//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
								$lstrEmailTemplateBody .= "<br>" . $lstrEmailTemplateFooter;
								
								$auth = 'ssl';
								$port = '465';
								$config = array ('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' => 'ibfiminfo@gmail.com', 'password' => 'abcd123#' );
								$transport = new Zend_Mail_Transport_Smtp ( 'smtp.gmail.com', $config );
								$mail = new Zend_Mail ();
								$mail->setBodyHtml ( $lstrEmailTemplateBody );
								$sender_email = 'ibfiminfo@gmail.com';
								$sender = 'ibfim';
								$receiver_email = $larrresult ["EmailAddress"];
								$receiver = $larrresult ['FName'];
								$mail->setFrom ( $sender_email, $sender )->addTo ( $receiver_email, $receiver )->setSubject ( $lstrEmailTemplateSubject );
								
								try {
									$result = $mail->send ( $transport );
								} catch ( Exception $e ) {
									$lstrMsg = "error";
								}
								$lobjMail->clearRecipients ();
								$this->view->mess .= ". Login Details have been sent to user Email";
								unset ( $larrEmailIds [$lintI] );
							}
						}
					} catch ( Exception $e ) {
						$lstrMsg = "error";
					}
				} else {
					$lstrMsg = "No Template Found";
				}
			} else {
				$this->view->mess = "Payment Failed";
			}
		}
	}
	
	public function reportAction() {		
		$ids = $this->_getParam ( 'IdRegister' );
		$this->view->idbatchss = $ids;
		$resultBatchRegistration = $this->lobjBatchcandidatesmodel->fnAdhocvenue ( $ids );
		
		$regpin = $resultBatchRegistration ['registrationPin'];
		$larrresult = $this->lobjBatchcandidatesmodel->fngetregisteredstudentsdetails ( $regpin );
		
		$this->view->candidate = $larrresult;	
	}
	
	public function newstudentapplicationAction() {
		exit ();
	}
	
public function pdfexportAction()
	{
		//Exporting data to an excel sheet 

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	
		if ($this->_request->isPost () && $this->_request->getPost ( 'Export' )) {
				
			$larrformData = $this->_request->getPost ();	
			
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
			
				
		$time = date('h:i:s',time());
		$filename = 'Company_Registration_Report_'.$frmdate;
		$ReportName = $this->view->translate( "Company" ).' '.$this->view->translate( "Registration" ).' '.$this->view->translate( "Report" );
		if($lstrreportytpe=='Pdf'){
			$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		}else{
		$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br<br><br><br<br>';
		}
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>	
								<td><b>Date </b></td>
								<td align= 'left'><b>$day</b></td>
								<td><b> Time</b></td>
								<td align = 'left' colspan= 6><b>$time</b></td>
							</tr>
						</table>";
		$tabledata.= "<br>
						<table border=1  align=center width=100%>
							<tr>
								<td align=center colspan = 9><b> {$ReportName}</b></td>	
							</tr>
						</table>
					<br>";
		$tabledata.= '<table border=1 align=center width=100%>
						<tr>
							<th><b></b></th>
							<th><b>Student Name</b></th>
							<th><b>ICNO</b></th>
							<th><b>E-Mail</b></th>
							<th><b>Registration Id</b></th>
							<th><b>Program Applied</b></th>
							<th><b>Venue Name</b></th>
							<th><b>Exam Date</b></th>
							<th><b>Exam Status</b></th>						
						</tr>';     
	 
		if (count($larrformData)){
			 $cnt = 0; 
		
			 
      	for($expdata=0;$expdata<count($larrformData['FName']);$expdata++){
      		  	
			$tabledata.= ' <tr>
				   		   <td><b>'; 
      		      $tabledata.= '</b></td>
					       <td>'.$larrformData['FName'][$expdata].'</td> 
						   <td>'.$larrformData['ICNO'][$expdata].'</td> 
						   <td>'.$larrformData['EmailAddress'][$expdata].'</td> 
						   <td>'.$larrformData['Regid'][$expdata].'</td> 
						   <td>'.$larrformData['ProgramName'][$expdata].'</td> 
						   <td>'.$larrformData['centername'][$expdata].'</td> 		  
						   <td>'.$larrformData['dateofexam'][$expdata].'</td> 
						   <td>'.$larrformData['status'][$expdata].'</td> 		   
				        </tr> ';				   	
	    	$cnt++; 
      		  }
	     
		 }		
	
		 $tabledata.= '<tr>		      
		     	<td></td>
		     	<td></td>
		     	<td></td>
		     </tr>
		     </table>';	
		
		//echo $tabledata;exit;
		if($lstrreportytpe=='Pdf'){
			include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->useLang = true;
			$mpdf->SetAutoFont();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename.pdf,'D');
		}else {
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
		}				
		}		
	}
	
	
	
	
	
	public function getprogramtotalAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$idprogram = ( int ) $this->_getParam ( 'idprogram' );
		$larrresult = $this->lobjTakafulmodel->fnGetProgramFee ( $idprogram );
		echo json_encode($larrresult);die();
		exit ();
	}
	
	public function getdiscountAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ();
		$nocandidates = ( int ) $this->_getParam ( 'nocandidates' );
		$idprogram = ( int ) $this->_getParam ( 'idprogram' );
		
		$larrresultprog = $this->lobjTakafulmodel->fngetprogramdiscount ( $idprogram, $nocandidates );
		
		echo $larrresultprog [0] ['Amount'];
		die ();
	}
	
	public function printreportAction() {
		$this->_helper->viewRenderer->setNoRender ( true );
		$this->_helper->layout->disableLayout ();
		$IdApplication = ( int ) $this->_getParam ( 'insertedId' );
		/*print_r($IdApplication);
		die();*/
		
		$larrresult = $this->lobjTakafulmodel->fnGetExamDetails ( $IdApplication );
		$totamt = ( int ) $larrresult ['totalAmount'];
		$AmountInWords = $this->lobjTakafulmodel->fnGetAmountInWords ( $totamt );
		
		//object to initialize ini file
		$lobjAppconfig = new Zend_Config_Ini ( APPLICATION_PATH . '/configs/application.ini', 'development' );
		try {
			//java class
			$lobjdbdriverclass = new Java ( "java.lang.Class" );
			
			//set db driver
			$lobjdbdriverclass->forName ( "com.mysql.jdbc.Driver" );
			
			//driver manager object
			$lobjdrivermanager = new Java ( "java.sql.DriverManager" );
			
			//get the db connection
			$lstrConnection = "jdbc:mysql://" . $lobjAppconfig->resources->db->params->host . "/" . $lobjAppconfig->resources->db->params->dbname . "?user=" . $lobjAppconfig->resources->db->params->username . "&password=" . $lobjAppconfig->resources->db->params->password;
			
			$lobjconnection = $lobjdrivermanager->getConnection ( $lstrConnection );
			
			//Jasper Compile manager object
			$lobjcompileManager = new Java ( "net.sf.jasperreports.engine.JasperCompileManager" );
			echo "CompileManager object created</br>";
			$lstrreportdir = realpath ( "." ) . "/report/";
			$lstrimagepath = realpath ( "." ) . "/images/";
			
			//compiled report path
			$lobjreport = $lobjcompileManager->compileReport ( realpath ( $lstrreportdir . "takafulreport.jrxml" ) );
			//Jasper Fill Manager object
			

			$lobjfillManager = new Java ( "net.sf.jasperreports.engine.JasperFillManager" );
			$int1 = new Java ( "java.lang.Integer" );
			//Hashmap object
			//print_r($lstrreportdir);die();
			$lobjparams = new Java ( "java.util.HashMap" );
			$lobjparams->put ( "IDAPPLICATION", $IdApplication );
			$lobjparams->put ( "IMAGEPATH", $lstrimagepath . "ibfim.jpg" );
			$lobjparams->put ( "AMOUNTINWORDS", $AmountInWords ['Amount'] );
			
			echo "Fill Manager</br>";
			
			//Jasper Print Object
			$lobjjasperPrint = $lobjfillManager->fillReport ( $lobjreport, $lobjparams, $lobjconnection );
			
			echo "Jasper Printed</br>";
			
			//Jasper Export Manager object
			$lobjexportManager = new Java ( "net.sf.jasperreports.engine.JasperExportManager" );
			
			//output file path
			$lstrhtmloutputPath = realpath ( "." ) . "/" . "output.html";
			echo "Before Export</br>";
			$session = Zend_Session::getId ();
			$lstrpdfoutputPath = realpath ( "." ) . "/" . "$session.pdf";
			$objStream = new Java ( "java.io.ByteArrayOutputStream" );
			$lobjexportManager->exportReportToPdfFile ( $lobjjasperPrint, $lstrpdfoutputPath );
			
			//Export report to HTML	            
			echo 'HTML Exported</br>';
			
			header ( "Content-type: application/pdf;charset=utf-8;encoding=utf-8" );
			header ( 'Content-Disposition: attachment; filename="Takaful_Application_details.pdf"' );
			
			readfile ( $lstrpdfoutputPath );
			unlink ( $lstrpdfoutputPath );
			echo "finished";
		
		} catch ( JavaException $lobjexception ) {
			echo 'Exception caught: ', $lobjexception->getMessage () . "\n";
		}
	
	}
	
	public function changepasswordAction() {
		$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ( $this->gsessionidtakafuloperator->idtakafuloperator ); //get user details
		$this->view->lobjAdhocApplicationForm = $this->lobjAdhocApplicationForm;
		$this->view->takafulDetails = $larrresult;
		$idcomp = $this->gsessionidtakafuloperator->idtakafuloperator;
		//print_r($larrresult);die();
		$pass = $larrresult ['Password'];
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjAdhocApplicationForm->isValid ( $larrformData )) {
				$larrformData ['UpdUser'] = 1;
				$larrformData ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
				$larrformData ['oldpassword'] = md5 ( $larrformData ['oldpassword'] );
				//print_r($larrformData);die();
				if ($pass == $larrformData ['oldpassword']) {
					$larrformData ['newpassword'] = md5 ( $larrformData ['newpassword'] );
					// echo $larrformData['newpassword'];die();
					$lastInsId = $this->lobjTakafulmodel->updatecompanypass ( $larrformData ['newpassword'], $idcomp );
					$auth = Zend_Auth::getInstance (); // Write Logs
					$priority = Zend_Log::INFO;
					$controller = Zend_Controller_Front::getInstance ()->getRequest ()->getControllerName ();
					$message = "\t\t\t\t" . $controller . "\t\t\t\t" . "Successfully Password Changed" . "\t\t\t\t" . $this->getRequest ()->getServer ( 'REMOTE_ADDR' ) . "\t\t\t\t" . "Success" . "\t\t\t\t\r";
					$this->_gobjlogger->log ( $message, 5 );
					$this->_redirect ( $this->baseUrl . "/takafulapplication/index" );
				
				} else {
					echo '<script language="javascript">alert("The Old Password is Not Correct")</script>';
				
		//$this->_redirect( $this->baseUrl . "/companyapplication/changepassword");
				//die();
				

				}
			
			}
		}
	}
	public function fpxpageoneAction() {
		
		$this->_helper->layout ()->setLayout ( 'plain' );
		$this->view->intidstudent = $lintinsertedId = $this->_getParam ( 'insertedId' );
		
		$larrresult = $this->lobjTakafulmodel->fngetPaymentDetails ( $lintinsertedId );
		$this->view->data = $larrresult;
		unset ( $_SESSION ["pageName"] );
		unset ( $_SESSION ["StudsId"] );
		$_SESSION ["pageName"] = "takafulapplication";
		$_SESSION ["idTakaful"] = $this->gsessionidtakafuloperator->idtakafuloperator;
		$_SESSION ["InsertedId"] = $lintinsertedId;
	}
	public function fpxpagetwoAction() {
		$this->_helper->layout ()->setLayout ( 'plain' );
		$this->view->intidstudent = $lintinsertedId = $this->_getParam ( 'insertedId' );
		$larrresult = $this->lobjTakafulmodel->fngetPaymentDetails ( $lintinsertedId );
		//print_r($larrresult);	
		//exit;
		$this->view->data = $larrresult;
		
	
		
		
		error_reporting ( E_ALL );
		$address = "127.0.0.1";
		$service_port = 6000;
		// Create a TCP/IP socket. 
		$socket = socket_create ( AF_INET, SOCK_STREAM, SOL_TCP );
		if ($socket < 0) {
		
		//echo "socket_create() failed: reason: " . socket_strerror($socket) . "\n"; 
		} else {
		
		//echo "Socket creation successfull."; 
		}
		// Establish socket connection. 
		$result = socket_connect ( $socket, $address, $service_port );
		if (! $result) {
			//echo "Socket connection failed.<br>";
			die ();
		} else {
		
		//echo "Socket connection successfull.<br>"; 
		}
		// Generating String to send to plugin. 
		$messageOrderNo = $_POST ['TxnOrderNo'];
		$messageTXNTime = date ( 'YmdHis' );
		$sellerOrderNo = $_POST ['TxnOrderNo'];
		$this->view->TxnAmount = $messageAmount = $_POST ['TxnAmount'];
		$sellerID = $_POST ['sellerID'];
		
		$in = "message:request|message.type:AR|message.token:01|message.orderno:$messageOrderNo|message.ordercount:1|message.txntime:$messageTXNTime|message.serialno:1|message.currency:MYR|message.amount:$messageAmount|charge.type:AA|seller.orderno:$sellerOrderNo|seller.id:$sellerID|seller.bank:01|\n";
		$out = '';
		
		socket_write ( $socket, $in );
		while ( $out = socket_read ( $socket, 6001 ) ) {
			$fpxValue = $out;
		}
		$sendFpxValue = str_replace ( "\n", "", $fpxValue );
		$this->view->sendFpxValue = $sendFpxValue;
		socket_close ( $socket );
	}
	
	
public function migspaymentAction()
{
		$insertedid = $this->_getParam('insertedid');
		$larrcompanydetails = $this->lobjTakafulmodel->fngetbatchtakafuldetails($insertedid);
		
		$this->view->name = $larrcompanydetails['TakafulName'];
		$this->view->amount = $larrcompanydetails['totalAmount'];
		$this->view->operatortype = $operator = 2;
		$this->view->insertedId =$insertedid;
	$this->view->idcompany = $idcompany = $this->gsessionidtakafuloperator->idtakafuloperator;
}
public function mipgtwoAction()
{
		$larrformData = $this->_request->getPost ();
	$this->view->formdata = $larrformData;	
	$this->view->operatortype = $larrformData['operatortype'];
}

public function mipgrequestingAction()
{
	$operator = $this->_getParam('operatortype');
	$migsarray = $_GET;
	$larresult = $this->lobjTakafulmodel->fninsertmigspayment($_GET,$operator);
	$responsecode = $_GET['vpc_TxnResponseCode'];
	$idbatchregistration =$_GET['vpc_MerchTxnRef'];
	$larresultbatchdetails = $this->lobjTakafulmodel->fngetbatchregistrationdetails($idbatchregistration);
	$this->view->idcompany = $larresultbatchdetails['idCompany'];
	if($responsecode=='0')
	{
		$regpin = $larresultbatchdetails['registrationPin'];
		if($regpin==0)
		{
			$randomnumber1 = rand(100000,999999);
		$randomnumber2 = rand(100000,999999);
		$regpin  = $randomnumber1.''.$randomnumber2;
		}
		
		$larresult = $this->lobjTakafulmodel->fnupdatebatchregdetails($idbatchregistration,$regpin);
		$this->view->status = 1;
	}
	else 
	{
		
	}
	
	
}


}
