<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class RegistrationreportController extends Zend_Controller_Action
{
	public function init() 
	{ 
		$this->_helper->layout()->setLayout('/batch3/usty1');	
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	Private function fnsetObj() 
	{
		$this->lobjCommon = new App_Model_Common();//object of common model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjregistrationreportmodel = new App_Model_Registrationreport();
		$this->lobjregistrationreportform =  new App_Form_Registrationreport();
	}
		
	public function indexAction() 
	{
	    $idappn = $this->_getParam('id');
		//echo $idappn;
		if($idappn)
		{
		    $this->view->clear = 1;
		}
	    $venuelist = $this->lobjregistrationreportmodel->getvenuelist();
		$this->lobjregistrationreportform->Venue->addMultiOption('','All Venues');
		$this->lobjregistrationreportform->Venue->addMultiOptions($venuelist);
		$this->view->lobjform = $this->lobjregistrationreportform;
		if(!$this->_getParam('search'))
		unset($this->gobjsessionsis->registrationreport);
		$lintpagecount = 1000;
		$lintpage = $this->_getParam('page',1);//Paginator instance
		$larrresult = array();
		if(isset($this->gobjsessionsis->registrationreport))
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gobjsessionsis->registrationreport,$lintpage,$lintpagecount);
		}
		else
		{
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' ))
		{
		    
			$larrformData = $this->_request->getPost ();
		//echo "<pre>";
			//print_r($larrformData);die();
			if ($this->lobjregistrationreportform->isValid($larrformData))
			{
				$larrformData = $this->_request->getPost();
				//echo "<pre>";
				//print_r($larrformData);die();
				unset ( $larrformData ['Search']);
				$this->view->search = 1;
				$this->view->FromDate = $FromDate = $larrformData['FromDate'];
				$this->view->ToDate = $ToDate = $larrformData['ToDate'];
				$this->view->day = $day = $larrformData['day'];
				
				 
                         					
											
					//$yesterdaydate= date('Y-m-d', mktime(0,0,0,$month,($day-1),$year));
					//echo $yesterdaydate;die();
					
					$larrtomonth=$this->lobjregistrationreportmodel->fngettodate($larrformData['FromDate']);
					$yesterdaydate=$larrtomonth['todate'];
					//print_r($larrtomonth);die();
					$dateofbirth = "{max:'$yesterdaydate',datePattern:'dd-MM-yyyy'}";
					
					$this->view->lobjform->ToDate->setAttrib('constraints',"$dateofbirth");
//				$this->view->lobjregistrationreportform->ToDate->setAttrib('readonly',true);
				$larrresultcomp = $this->lobjregistrationreportmodel->fngetschdeuledates($larrformData);
				if($larrformData['Venue'] == '')
				{
				   $this->view->center =0;
				   $larrvenues = $this->lobjregistrationreportmodel->fngetvenues();
				   $this->view->venuegroup = $larrvenues;
				   $this->view->bulkvenue = 1;
				}
				else
				{
				   $this->view->center =  $larrformData['Venue'];
				   $larrresultvenues = $this->lobjregistrationreportmodel->fngetvenuedetails($larrformData['Venue']);
				   $this->view->venues = $larrresultvenues;
				   $this->view->bulkvenue = 0;
				}
				//echo "<pre>";print_r($larrresultcomp);
				//echo "<pre>";print_r($larrresultvenues);die();
				$this->view->dates = $larrresultcomp;
				$this->view->lobjform->populate($larrformData);
			}
		}
		if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' ))
		{
		    
		    $this->_redirect( $this->baseUrl . '/registrationreport/index/id/1');
		}
	}
    public function fnexportexcelAction()
    {	
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$larrformData = $this->_request->getPost();
		
		$ldtfromdate = $larrformData['FromDate'];
		$ldttodate =$larrformData['ToDate'];
		$centername =$larrformData['centername'];
		$center =$larrformData['center'];
		if($larrformData['day']){
					$lstrday =$larrformData['day'];
				}else{
					$lstrday ='All Days';
				}
				$fdate = date('d-m-Y',strtotime($ldtfromdate));
				$tdate = date('d-m-Y',strtotime($ldttodate));
				
				
		if($center == 0)
		{
		    $larrresultcomp = $this->lobjregistrationreportmodel->fngetschdeuledates($larrformData);
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbenew/images/reportheader.jpg";
			$time = date('h:i:s',time());
		    $filename = 'RegistrationReport'.$fdate.'___'.$tdate.'__'.'AllVenue';
			$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
			$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 2><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 2><b> Time</b></td><td align=left colspan = 2><b>$time</b></td><td  align=left colspan = 2><b>Venue</b></td><td align=left colspan = 2><b>Allvenues</b></td></tr>";
			$tabledata.= "<tr><td align=left colspan = 2><b>From Date </b></td><td align=left colspan = 2><b>".$fdate."</b></td><td  align=left colspan = 2><b>To Date</b></td><td align=left colspan = 2><b>".$tdate."</b></td><td  align=left colspan = 2><b>Day</b></td><td align=left colspan = 2><b>".$lstrday."</b></td></tr></table><br>";
			$tabledata.= "<table border = '1' ><tr><td valign = 'top'><b>VENUES/DATE</b></td>";
			$cols = count($larrresultcomp)+1; 
			for($lvars=0;$lvars<count($larrresultcomp);$lvars++){
				$tabledata.="<td align = 'left' colspan = '2'><b>".$larrresultcomp[$lvars]['Date']."</b><br>";
				$tabledata.="<b>".$larrresultcomp[$lvars]['dayname']."</b></td>";
			}
			$tabledata.="</tr>";
			$larrresultvenues = $this->lobjregistrationreportmodel->fngetvenues();
			for($lvar=0;$lvar<count($larrresultvenues);$lvar++)
			  {
				$tabledata.= "<tr><td valign = 'top'>".$larrresultvenues[$lvar]['centername']."</td>";
				for($j=0;$j<count($larrresultcomp);$j++)
				   {
					    $this->lobjregistrationreportmodel = new App_Model_Registrationreport();
					   $idcenter = $larrresultvenues[$lvar]['idcenter'];
					   $date = $larrresultcomp[$j]['date'];
					   $larresult =$this->lobjregistrationreportmodel->fngetseats($idcenter,$date); 
					   $cnt = count($larresult);
					   if($cnt==2)
					   { 
						$tabledata.= "<td valign = 'top' colspan = '2'>
										<table width='100%'>";
										if($larresult[0]['Totalcapacity']-$larresult[0]['Allotedseats'] <= 0){
							$tabledata.= "<tr><td align='left'>".$larresult[1]['ampmstart']."</td><td align='right'>[Full]</td></tr>";
							
							             }
										 else
										 {
							$tabledata.= "<tr><td align='left'>".$larresult[0]['ampmstart']."</td><td align='right'>[".($larresult[0]['Totalcapacity']-$larresult[0]['Allotedseats'])."]</td></tr>";			 
										 }
										 if($larresult[1]['Totalcapacity']-$larresult[1]['Allotedseats'] <= 0)
										 {
							$tabledata.= "<tr><td align='left'>".$larresult[1]['ampmstart']."</td><td align='right'>[Full]</td></tr>";
										 }
										 else
										 {
							$tabledata.= "<tr><td align='left'>".$larresult[1]['ampmstart']."</td><td align='right'>[".($larresult[1]['Totalcapacity']-$larresult[1]['Allotedseats'])."]</td></tr>";			  
										 }
							$tabledata.= "</table>  
									  </td>";
					   }
					   else if($cnt==1)
					   {  
						
					    
						$tabledata.= "<td valign = 'top' colspan = '2'>
										<table width='100%'>";
										if($larresult[0]['Totalcapacity']-$larresult[0]['Allotedseats'] <= 0){
							 $tabledata.= "<tr><td align='left'>".$larresult[0]['ampmstart']."</td><td align='right'>[Full]</td></tr></table></td>";
							             }
										 else
										 {
							$tabledata.= "<tr><td align='left'>".$larresult[0]['ampmstart']."</td><td align='right'>[".($larresult[0]['Totalcapacity']-$larresult[0]['Allotedseats'])."]</td></tr></table></td>";			
										 }
					   }
					   else 
					   {
						$tabledata.= "<td valign = 'top' colspan = '2'>No Exam</td>";
					   }
				   }     
					$tabledata.= "</tr>";
			  }
				$tabledata.="</table>";
		  
		}
		else
		{
		        $larrresultcomp = $this->lobjregistrationreportmodel->fngetschdeuledetails($larrformData);
			    $day= date("d-m-Y");
			    $host = $_SERVER['SERVER_NAME'];
			    $imgp = "http://".$host."/tbenew/images/reportheader.jpg";
			    $time = date('h:i:s',time());
				$filename = 'RegistrationReport'.$fdate.'___'.$tdate.'__'.$centername;
				$ReportName = $this->view->translate( "Registration" ).' '.$this->view->translate( "Report" );
				$tabledata = '<img width=100% src= "'.$imgp.'" /><br><br><br><br><br>';	
				$tabledata.= "<br><table border=1  align=center width=100%><tr><td align=left colspan = 2><b>Date </b></td><td align=left colspan = 2><b>$day</b></td><td  align=left colspan = 2><b> Time</b></td><td align=left colspan = 2><b>$time</b></td>
				<td  align=left colspan = 2><b>Venue</b></td><td align=left colspan = 2><b>$centername</b></td></tr>";
				$tabledata.= "<tr><td align=left colspan = 2><b>From Date </b></td><td align=left colspan = 2><b>".$fdate."</b></td><td  align=left colspan = 2><b>To Date</b></td><td align=left colspan = 2><b>".$tdate."</b></td><td  align=left colspan = 2><b>Day</b></td><td align=left colspan = 2><b>".$lstrday."</b></td></tr></table><br>";
				$tabledata.= "<table border = '1' ><tr><td valign = 'top'><b>VENUE/DATE</b></td>";
				$cols = count($larrresultcomp)+1; 
				for($lvars=0;$lvars<count($larrresultcomp);$lvars++){
					$tabledata.="<td align = 'left' colspan = '2'><b>".$larrresultcomp[$lvars]['Date']."</b><br>";
					$tabledata.="<b>".$larrresultcomp[$lvars]['dayname']."</b></td>";
				}
				$tabledata.="</tr>";
				$larrresultvenues = $this->lobjregistrationreportmodel->fngetvenuedetails($center);
				
					$tabledata.= "<tr><td valign = 'top'>".$larrresultvenues[0]['centername']."</td>";
					for($j=0;$j<count($larrresultcomp);$j++)
					   {
						   $this->lobjregistrationreportmodel = new App_Model_Registrationreport();
						   $idcenter = $larrresultvenues[0]['idcenter'];
						   $date = $larrresultcomp[$j]['date'];
						   $larresult =$this->lobjregistrationreportmodel->fngetseats($idcenter,$date); 
						   $cnt = count($larresult);
						    if($cnt==2)
					   { 
						$tabledata.= "<td valign = 'top' colspan = '2'>
										<table width='100%'>";
										if($larresult[0]['Totalcapacity']-$larresult[0]['Allotedseats'] <= 0){
							$tabledata.= "<tr><td align='left'>".$larresult[1]['ampmstart']."</td><td align='right'>[Full]</td></tr>";
							
							             }
										 else
										 {
							$tabledata.= "<tr><td align='left'>".$larresult[0]['ampmstart']."</td><td align='right'>[".($larresult[0]['Totalcapacity']-$larresult[0]['Allotedseats'])."]</td></tr>";			 
										 }
										 if($larresult[1]['Totalcapacity']-$larresult[1]['Allotedseats'] <= 0)
										 {
							$tabledata.= "<tr><td align='left'>".$larresult[1]['ampmstart']."</td><td align='right'>[Full]</td></tr>";
										 }
										 else
										 {
							$tabledata.= "<tr><td align='left'>".$larresult[1]['ampmstart']."</td><td align='right'>[".($larresult[1]['Totalcapacity']-$larresult[1]['Allotedseats'])."]</td></tr>";			  
										 }
							$tabledata.= "</table>  
									  </td>";
					   }
					   else if($cnt==1)
					   {  
					
					    
						$tabledata.= "<td valign = 'top' colspan = '2'>
										<table width='100%'>";
										if($larresult[0]['Totalcapacity']-$larresult[0]['Allotedseats'] <= 0){
							 $tabledata.= "<tr><td align='left'>".$larresult[0]['ampmstart']."</td><td align='right'>[Full]</td></tr></table></td>";
							             }
										 else
										 {
							$tabledata.= "<tr><td align='left'>".$larresult[0]['ampmstart']."</td><td align='right'>[".($larresult[0]['Totalcapacity']-$larresult[0]['Allotedseats'])."]</td></tr></table></td>";			
										 }
					   }
						   else 
						   {
							$tabledata.= "<td valign = 'top' colspan = '2'>No Exam</td>";
						   }
					   }     
						$tabledata.= "</tr>";
				  
					$tabledata.="</table>";
			  }
			$ourFileName = realpath('.')."/data";
			$ourFileHandle = fopen($ourFileName, 'w')or die("can't open file");
			ini_set('max_execution_time', 3600);
			fwrite($ourFileHandle,htmlspecialchars_decode($tabledata));
			fclose($ourFileHandle);
			header("Content-Type: application/vnd.ms-excel,charset=UTF-8");
			header("Content-Disposition: attachment; filename=$filename.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			readfile($ourFileName);
			unlink($ourFileName);
    }
	public function instructAction()
    {
	   $this->_helper->layout->disableLayout();
	}
	 public function centerdetailsAction()
    {
	    $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
		$idcenter = $this->_getParam('idcenter');
		$larrresult = $this->lobjregistrationreportmodel->fnGetcenterdetails($idcenter);
		$tabledata = '';
		$tabledata.= '<br><fieldset><legend align = "left">Venue Address</legend>';
		$tabledata.="<table class='table' border=1 align='center' width=100%><tr><td><b>Venue Name</b></td><td><b>".$larrresult['centername']."</td></tr><td><b>Address</b></td><td><b>".$larrresult['addr1']."</b><td></tr><tr><td><b></b></td><td><b>".$larrresult['addr2']."</b></td></tr>";
		$tabledata.="</table></form><br>";
		$tabledata.="<tr><td colspan= '8'><input type='button' id='close' name='close'  value='Close' onClick='Closevenue();'></td></tr>";
		echo  $tabledata;
		
	}			
}