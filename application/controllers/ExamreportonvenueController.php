<?php
//error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class ExamreportonvenueController extends Zend_Controller_Action { //Controller for the User Module
public $gsessionidCenter;//Global Session Name
	public function init() { //initialization function		
		//echo "init function";
		$this->gsessionidCenter = Zend_Registry::get('sis'); 
		if(empty($this->gsessionidCenter->idcenter)){ 
			$this->_redirect( $this->baseUrl . '/batchlogin/logout');					
		}
		$this->_helper->layout()->setLayout('/examcenter/usty1');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
		$this->lobjCommon = new App_Model_Common();
	}
	
	public function fnsetObj() {
		$this->lobjanswer = new Examination_Model_Answer(); //intialize user db object
		$this->lobjform = new App_Form_Search(); //intialize user lobjbusinesstypeForm
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
		$this->lobjexamreport = new Examination_Model_Examreports(); //intialize user db object
	}
	
	public function indexAction() 
	{
/*		$month= date("m"); // Month value
		$day= date("d"); //today's date
		$year= date("Y"); // Year value
		$todaydate= date('Y-m-d', mktime(0,0,0,$month,($day),$year));
		$examdate = "{max:'$todaydate',datePattern:'dd-MM-yyyy'}"; */
		
		$this->view->lobjform = $this->lobjform; //send the lobjForm object to the view

		$larrcenternames = $this->lobjanswer->fngetcenternames();
		$this->lobjform->field5->addMultiOptions($larrcenternames);
		$this->lobjform->field5->setValue($this->gsessionidCenter->idcenter);
		$this->lobjform->field5->setRegisterInArrayValidator(false);
		$this->lobjform->field5->setAttrib('required',"false");
		$this->lobjform->field10->setAttrib('required',"true");
		//$this->lobjform->field10->setAttrib('constraints', "$examdate");
		$this->lobjform->field5->setAttrib('readOnly',true);
		
		if(!$this->_getParam('search')) 
			unset($this->gsessionidCenter->examreportpaginatorresult1);
		
		
		$lintpagecount = $this->gintPageCount = 100;		
		$lintpage = $this->_getParam('page',1); //Paginator instance
		$larrresult = array();
		if(isset($this->gsessionidCenter->examreportpaginatorresult1)) {
			$this->view->venufield= $this->gsessionidCenter->examvenue;
			$this->view->datefield = $this->gsessionidCenter->examrvenuedate;
			
			$this->lobjform->field5->setValue($this->view->venufield);
			$this->lobjform->field10->setValue($this->view->datefield);
			
			
			
			$this->view->paginator = $this->lobjCommon->fnPagination($this->gsessionidCenter->examreportpaginatorresult1,$lintpage,$lintpagecount);
		} else {
			$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
		}
		
		if ($this->_request->isPost () && $this->_request->getPost( 'Search' )) {
			$larrformData = $this->_request->getPost ();
			if ($this->lobjform->isValid($larrformData)) 
			{
				$venuefield = $this->view->venufield=$larrformData['field5'];
				$examrvenuedate= $this->view->datefield=$larrformData['field10'];
				unset ( $larrformData ['Search'] );
				$larrresult = $this->lobjexamreport->fnGetsearchdetails($larrformData); //searching the values for the businesstype
//$endtime = $larrresult[0]['ExamStartTime'];
//$todaytime = date('H:i:s');
 //$Today = date('d-m-Y');
 //$ExamDate = $larrresult[0]['ExamDate'];
//if($endtime >= $todaytime && $Today == $ExamDate) {
//$this->view->display =1;			

//}
//else
//{
//$this->view->display =0;
//}


				$this->view->paginator = $this->lobjCommon->fnPagination($larrresult,$lintpage,$lintpagecount);
				$this->gsessionidCenter->examreportpaginatorresult1 = $larrresult;
				$this->gsessionidCenter->examvenue=$venuefield;
				$this->gsessionidCenter->examrvenuedate = $examrvenuedate;
			}
		}
	}
	
	public function pdfexportAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$selectedvenu = $this->_getParam('venu');
		$takenexamdate = $this->_getParam('examdate');
		include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
		$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
		
		$mpdf->SetDirectionality ( $this->gstrHTMLDir );
		$mpdf->text_input_as_HTML = true;
		$mpdf->useLang = true;
		$mpdf->SetAutoFont();
		//$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
		$mpdf->pagenumSuffix = ' / ';
		$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
		  //$stylesheet = file_get_contents('../public/css/default.css');	
		  //$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->allow_charset_conversion = true; // Set by default to TRUE
		$mpdf->charset_in = 'utf-8';
		$ReportName = $this->view->translate( "Exam" ).' '.$this->view->translate( "Report" );
		$Venue = $this->view->translate( "Venue" );
		$Pogram = $this->view->translate( "Pogram" );
		$Session = $this->view->translate( "Session" );
		$Starttime = $this->view->translate( "Start Time" );
		$GraceTime = $this->view->translate( "Extra Time(Min)" );
		$closetime = $this->view->translate( "Close Time" );
		$examdate = $this->view->translate( "Exam Date" );
		$Autoclosetime = $this->view->translate( "Auto Close Time" );
		$registered = $this->view->translate( "Registered" );
		$Approved = $this->view->translate( "Approved" );
		$attended = $this->view->translate( "Attended" );
		$absent = $this->view->translate( "Absent" );
		$pass = $this->view->translate( "Pass" );
		$fail = $this->view->translate( "Fail" );
		//$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  //x-frm left,y-frm top,Width,,Height,
		ini_set('max_execution_time',3600);
		//$mpdf->WriteHTML();
		
		$datelabel = "Date :";
		$timelabel = "Time :";
		$currentdates = date('d-m-Y');
		$currenttimes = date('H:i:s');
		
		
		$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
		$tabledata.= "<br><table border=1 align=center width=100%>
						<tr>
							<th align=center><b> {$datelabel}</b></th>
							<th align=center><b> {$currentdates}</b></th>
							<th align=center><b> {$timelabel}</b></th>
							<th align=center><b> {$currenttimes}</b></th>
						</tr></table>";
		$tabledata.= "<br><table border=1 align=center width=100%>
							<tr>
								<th align=center><b> {$ReportName}</b></th>
							</tr></table>";
		$centerarray = array();
		$tabledata.= "<br><table border=1 width=100%>
							<tr>
								<th align=center><b> {$Venue}</b></th>
								<th align=center><b> {$Pogram}</b></th>
								<th align=center><b> {$Session}</b></th>
								<th align=center><b> {$Starttime}</b></th>
								<th align=center><b> {$GraceTime}</b></th>
								<th align=center><b> {$closetime}</b></th>
								<th align=center><b> {$Autoclosetime}</b></th>
								<th align=center><b> {$examdate}</b></th>
								<th align=center><b> {$registered}</b></th>
								<th align=center><b> {$Approved}</b></th>
								<th align=center><b> {$attended}</b></th>
								<th align=center><b> {$absent}</b></th>
								<th align=center><b> {$pass}</b></th>
								<th align=center><b> {$fail}</b></th>
							</tr>";
		
			$larrformData['field10'] = $takenexamdate;
			$larrformData['field5'] = $selectedvenu;
			$larrresult = $this->lobjexamreport->fnGetsearchdetails($larrformData); //searching the values for the businesstype
			//print_r($larrresult);die();
			foreach($larrresult as $larrresultss)	{ 
				$tabledata.="<tr>";
				
						$tabledata.="<td>";
					        if(!in_array($larrresultss['idcenter'],$centerarray)){
								$centerarray[] = $larrresultss['idcenter'];		
								$centerarrays[$larrresultss['idcenter']] = array();
					        	$tabledata.=$larrresultss['centername'];
		         			}
						$tabledata.="</td>";
					
										
	
						$tabledata.="<td>";
					        if(!in_array($larrresultss['ProgramName'],$centerarrays[$larrresultss['idcenter']])){
								$centerarrays[$larrresultss['idcenter']][] = $larrresultss['ProgramName'];
								$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']] = array();;	
					        	$tabledata.=$larrresultss['ProgramName'];
		         			}
						$tabledata.="</td>";
						
						
	
						$tabledata.="<td>";
					        if(!in_array($larrresultss['managesessionname'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
								$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['managesessionname'];	
					        	$tabledata.=$larrresultss['managesessionname'];
		         			}
						$tabledata.="</td>";			
	
						
						$emptytime= "00:00:00";
						if($larrresultss['ExamStartTime'] == "" || $larrresultss['ExamStartTime']==NULL) {
							$tabledata.="<td>{$emptytime}</td>";
						} else {
							$tabledata.="<td>";
						        if(!in_array($larrresultss['ExamStartTime'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
									$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['ExamStartTime'];	
						        	$tabledata.=$larrresultss['ExamStartTime'];
			         			}
							$tabledata.="</td>";
						}
						
						
						$emptymin = "0";
	        			$larrresultextratime = $this->lobjexamreport->fnGetExtratime($larrresultss['idcenter'],$larrresultss['idmangesession'],$larrresultss['exdate']);
						if(!empty($larrresultextratime)) {
							$tabledata.="<td>{$larrresultextratime['Gracetime']}</td>";
						} else {
							$tabledata.="<td>{$emptymin}</td>";
						}
						
						if($larrresultss['CloseTime'] == "" || $larrresultss['CloseTime']==NULL) {
							$tabledata.="<td>{$emptytime}</td>";
						} else {
							$tabledata.="<td>";
						        if(!in_array($larrresultss['CloseTime'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
									$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['CloseTime'];	
						        	$tabledata.=$larrresultss['CloseTime'];
			         			}
							$tabledata.="</td>";	
						}
						
						if($larrresultss['AutoSubmitCloseTime'] == "" || $larrresultss['AutoSubmitCloseTime']==NULL) {
							$tabledata.="<td>{$emptytime}</td>";
						} else {
							$tabledata.="<td>";
						        if(!in_array($larrresultss['AutoSubmitCloseTime'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
									$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['AutoSubmitCloseTime'];	
						        	$tabledata.=$larrresultss['AutoSubmitCloseTime'];
			         			}
							$tabledata.="</td>";	
						}
						
				
					    $tabledata.="<td>{$larrresultss['ExamDate']}</td>
										<td>{$larrresultss['Registered']}</td>";
					    $registeredsum[] = $larrresultss['Registered'];	
					    
						$larrresultApprovedStudents = $this->lobjexamreport->fnGetCountOfApprovedStudents($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],$larrresultss['exdate']);	
						$Approved = COUNT($larrresultApprovedStudents);			
						$tabledata.="<td>{$Approved}</td>";
						$Approvedsum[] = count($larrresultApprovedStudents);
									
									
						$larrresultattended = $this->lobjexamreport->fnGetCountOfStudentsAttended($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],$larrresultss['exdate'],'3');	
						$attended = COUNT($larrresultattended);			
						$tabledata.="<td>{$attended}</td>";
						$attendedsum[] = count($larrresultattended);
						
						$absent = $larrresultss['Registered'] - $attended;
						$tabledata.="<td>{$absent}</td>";
						$absentsum[] = $larrresultss['Registered'] - $attended;
						
						$larrresultpass = $this->lobjexamreport->fnGetCountOfStudents($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],$larrresultss['exdate'],'1');
						$passed = COUNT($larrresultpass);		
						$tabledata.="<td>{$passed}</td>";
						$passedsum[] = count($larrresultpass);
						
						$larrresultfail = $this->lobjexamreport->fnGetCountOfStudents($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],$larrresultss['exdate'],'2');
						$failed = COUNT($larrresultfail);	
						$tabledata.="<td>{$failed}</td>";
						$failedsum[] = count($larrresultfail);
						
				$tabledata.="</tr>";	
			}

		  $regsum= array_sum($registeredsum);
		  $Apprsum= array_sum($Approvedsum);
		  $attsum=  array_sum($attendedsum);
		  $absum= array_sum($absentsum);
		  $passsum =  array_sum($passedsum);
		  $failsum =  array_sum($failedsum);
		  $total = "Total";
		  
		  $tabledata.="<tr>
		  				<td colspan='8' align='center'><b>{$total}</b></td>
		  				<td><b>{$regsum}</b></td>
		  				<td><b>{$Apprsum}</b></td>
		  				<td><b>{$attsum}</b></td>
		  				<td><b>{$absum}</b></td>
		  				<td><b>{$passsum}</b></td>
		  				<td><b>{$failsum}</b></td>
		  			</tr>";	
			
		
			$tabledata.="</table><br>";
		
		$currentselectedvenu = $larrresultss['centername'];
		$examdateorig =  date ( "M_d_Y", strtotime ($takenexamdate) );
		
		$mpdf->WriteHTML($tabledata);  
		$mpdf->Output('Exam_report_for'.'_'.$currentselectedvenu.'_'.'For'.'_'.$examdateorig,'D');
	}
public function submitpdfAction()
    {
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
		$larconfigdetails = $this->lobjexamreport->fngetreceivermail();

          include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');

	
		require_once('Zend/Mail.php');

		require_once('Zend/Mail/Transport/Smtp.php');

		//require('fpdf/fpdf.php');
		//require('/fpd/fpdf.php');
		
    	 $selectedvenu = $this->_getParam('venu');
		
    	 $takenexamdate = $this->_getParam('examdate');
	
		 $session = $this->_getParam('session');
      
        //$larresult = $this->lobjexamreport->fnchronepullstatus($takenexamdate,$selectedvenu,$session);
//echo "<pre>";print_r($larconfigdetails);die();
		//if($larresult)
		//{
		  //  echo '<script language="javascript">alert("Exam results not updated properly ")</script>';
		//    echo "<script>parent.location = '".$this->view->baseUrl()."/examination/examreport/index';</script>";
				                	// die();
		//}
    	 
    	$mpdf=new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');




    	$mpdf->SetDirectionality ( $this->gstrHTMLDir );

    	$mpdf->text_input_as_HTML = true;

    	$mpdf->useLang = true;
    	$mpdf->SetAutoFont();
    	//$mpdf->WriteHTML('<img width=100% src="../public/images/reportheader.jpg" /><PRE></PRE><PRE></PRE><PRE></PRE><PRE></PRE>');
    	$mpdf->SetDisplayMode('fullpage');



    	$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
    	$mpdf->pagenumSuffix = ' / ';
    	$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
    	//$stylesheet = file_get_contents('../public/css/default.css');
    	//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
    	$mpdf->allow_charset_conversion = true; // Set by default to TRUE
    	$mpdf->charset_in = 'utf-8';



    	$ReportName = $this->view->translate( "Exam" ).' '.$this->view->translate( "Report" );


    	$Venue = $this->view->translate( "Venue" );
    	$Pogram = $this->view->translate( "Pogram" );
    	$Session = $this->view->translate( "Session" );
    	$Starttime = $this->view->translate( "Start Time" );
    	$closetime = $this->view->translate( "Close Time" );
    	$examdate = $this->view->translate( "Exam Date" );
    	$Autoclosetime = $this->view->translate( "Auto Close Time" );
    	$registered = $this->view->translate( "Registered" );
    	$attended = $this->view->translate( "Attended" );
    	$absent = $this->view->translate( "Absent" );
    	$pass = $this->view->translate( "Pass" );
    	$fail = $this->view->translate( "Fail" );


    	//$mpdf->WriteFixedPosHTML ( "<br><br><table border=1  align=center width=100%><tr><td align=center><b> {$ReportName}</b></td></tr></table>", 80, 25, 50, 90, 'auto' );  

//x-frm left,y-frm top,Width,,Height,
    	ini_set('max_execution_time',3600);
    	//$mpdf->WriteHTML();

    	$datelabel = "Date :";
    	$timelabel = "Time :";
    	$currentdates = date('d-m-Y');
    	$currenttimes = date('H:i:s');


    	$tabledata = '<img width=100% src="../public/images/reportheader.jpg" />';
    	$tabledata.= "<br><table border=1 align=center width=100%>
						<tr>
							<th align=center><b> {$datelabel}</b></th>
							<th align=center><b> {$currentdates}</b></th>
							<th align=center><b> {$timelabel}</b></th>
							<th align=center><b> {$currenttimes}</b></th>
						</tr></table>";
    	$tabledata.= "<br><table border=1 align=center width=100%>
							<tr>
								<th align=center><b> {$ReportName}</b></th>
							</tr></table>";

    	$centerarray = array();
    	$tabledata.= "<br><table border=1 width=100%>
							<tr>
								<th align=center><b> {$Venue}</b></th>
								<th align=center><b> {$Pogram}</b></th>
								<th align=center><b> {$Session}</b></th>
								<th align=center><b> {$Starttime}</b></th>
								<th align=center><b> {$closetime}</b></th>
								<th align=center><b> {$Autoclosetime}</b></th>
								<th align=center><b> {$examdate}</b></th>
								<th align=center><b> {$registered}</b></th>
								<th align=center><b> {$attended}</b></th>
								<th align=center><b> {$absent}</b></th>
								<th align=center><b> {$pass}</b></th>
								<th align=center><b> {$fail}</b></th>
							</tr>";


    	$larrformData['field10'] = $takenexamdate;
    	$larrformData['field5'] = $selectedvenu;

    	$larrresult = $this->lobjexamreport->fngetexamsearchdetails($larrformData); //searching the values for the businesstype
    	


    	foreach($larrresult as $larrresultss)	{
    		$tabledata.="<tr>";

    		$tabledata.="<td>";
    		if(!in_array($larrresultss['idcenter'],$centerarray)){
    			$centerarray[] = $larrresultss['idcenter'];
    			$centerarrays[$larrresultss['idcenter']] = array();
    			$tabledata.=$larrresultss['centername'];
    		}
    		$tabledata.="</td>";
    			


    		$tabledata.="<td>";
    		if(!in_array($larrresultss['ProgramName'],$centerarrays[$larrresultss['idcenter']])){
    			$centerarrays[$larrresultss['idcenter']][] = $larrresultss['ProgramName'];
    			$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']] = array();;
    			$tabledata.=$larrresultss['ProgramName'];
    		}
    		$tabledata.="</td>";



    		$tabledata.="<td>";
    		if(!in_array($larrresultss['managesessionname'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
    			$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['managesessionname'];
    			$tabledata.=$larrresultss['managesessionname'];
    		}
    		$tabledata.="</td>";


    		$emptytime= "00:00:00";
    		if($larrresultss['ExamStartTime'] == "" || $larrresultss['ExamStartTime']==NULL) {
    			$tabledata.="<td>{$emptytime}</td>";
    		} else {
    			$tabledata.="<td>";
    			if(!in_array($larrresultss['ExamStartTime'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
    				$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['ExamStartTime'];
    				$tabledata.=$larrresultss['ExamStartTime'];
    			}
    			$tabledata.="</td>";
    		}
    		if($larrresultss['CloseTime'] == "" || $larrresultss['CloseTime']==NULL) {
    			$tabledata.="<td>{$emptytime}</td>";
    		} else {
    			$tabledata.="<td>";
    			if(!in_array($larrresultss['CloseTime'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
    				$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['CloseTime'];
    				$tabledata.=$larrresultss['CloseTime'];
    			}
    			$tabledata.="</td>";
    		}
    		if($larrresultss['AutoSubmitCloseTime'] == "" || $larrresultss['AutoSubmitCloseTime']==NULL) {
    			$tabledata.="<td>{$emptytime}</td>";
    		} else {
    			$tabledata.="<td>";
    			if(!in_array($larrresultss['AutoSubmitCloseTime'],$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']])){
    				$centerarrayss[$larrresultss['idcenter']][$larrresultss['IdProgrammaster']][] = $larrresultss['AutoSubmitCloseTime'];
    				$tabledata.=$larrresultss['AutoSubmitCloseTime'];
    			}
    			$tabledata.="</td>";
    		}
    		$tabledata.="<td>{$larrresultss['ExamDate']}</td>
										<td>{$larrresultss['Registered']}</td>";
    		$registeredsum[] = $larrresultss['Registered'];
    		$larrresultattended = $this->lobjexamreport->fnGetCountOfStudentsAttended($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss

['idmangesession'],$larrresultss['exdate'],'3');
    		$attended = COUNT($larrresultattended);
    		$tabledata.="<td>{$attended}</td>";
    		$attendedsum[] = count($larrresultattended);
    		$absent = $larrresultss['Registered'] - $attended;
    		$tabledata.="<td>{$absent}</td>";
    		$absentsum[] = $larrresultss['Registered'] - $attended;
    		$larrresultpass = $this->lobjexamreport->fnGetCountOfStudents($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],

$larrresultss['exdate'],'1');
    		$passed = COUNT($larrresultpass);
    		$tabledata.="<td>{$passed}</td>";
    		$passedsum[] = count($larrresultpass);
    		$larrresultfail = $this->lobjexamreport->fnGetCountOfStudents($larrresultss['idcenter'],$larrresultss['IdProgrammaster'],$larrresultss['idmangesession'],

$larrresultss['exdate'],'2');
    		 $failed = COUNT($larrresultfail);
    		$tabledata.="<td>{$failed}</td>";
    		 $failedsum[] = count($larrresultfail);
    		$tabledata.="</tr>";


    	}
    	$regsum= array_sum($registeredsum);
		
    	 $attsum=  array_sum($attendedsum);
    	$absum= array_sum($absentsum);
    	$passsum =  array_sum($passedsum);
    	$failsum =  array_sum($failedsum);
		 $attended = $passsum+$failsum;
		$registered = $absum+$passsum+$failsum;
		
		if($attsum != $attended)
		{
		  
		     echo '<script language="javascript">alert("please close the exam")</script>';
			 echo "<script>parent.location = '".$this->view->baseUrl()."/examination/examreport/index';</script>";
				                	 die();
		}
		if($regsum != $registered)
		{
		   echo '<script language="javascript">alert("Please contact Admin")</script>';
		   echo "<script>parent.location = '".$this->view->baseUrl()."/examination/examreport/index';</script>";
				                	 die();
		}
    	$total = "Total";
    	$tabledata.="<tr>
		  				<td colspan='7' align='center'><b>{$total}</b></td>
		  				<td><b>{$regsum}</b></td>
		  				<td><b>{$attsum}</b></td>
		  				<td><b>{$absum}</b></td>
		  				<td><b>{$passsum}</b></td>
		  				<td><b>{$failsum}</b></td>
		  			</tr>";	
    	$tabledata.="</table><br>";

$auth = Zend_Auth::getInstance();
		$userid =  $auth->getIdentity()->iduser;

 $this->lobjexamreport->fninsertresultdetails($selectedvenu,$takenexamdate,$userid);

		$currentselectedvenu = $larrresultss['centername'];
					$examdateorig =  date ( "M_d_Y", strtotime ($takenexamdate) );
					$mpdf->WriteHTML($tabledata);
					$mpdf->Output('pdf/Exam_report_for'.'_'.$currentselectedvenu.'_'.'For'.'_'.$examdateorig.'.pdf','F');

		
		                                $auth = 'ssl';
										$port = '465';
										$config = array('ssl' => $auth, 'port' => $port, 'auth' => 'login', 'username' =>                 

                                                                                'itwinesgm@gmail.com', 'password' => 'itwinesgm123');
										$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com',$config);
										Zend_Mail::setDefaultTransport($transport);
										
										$mail = new Zend_Mail();
                                                                                $filename ='pdf/Exam_report_for'.'_'.$currentselectedvenu.'_'.'For'.'_'.$examdateorig.'.pdf';		

								
									        $mail->setBodyHtml("Student Exam Report");
										$at = new Zend_Mime_Part(file_get_contents($filename));
										//$at = $mail->createAttachment($filename);
                                                                                $at->type= 'application/pdf';	
                                                                                //$at->disposition = Zend_Mime::DISPOSITION_INLINE;
										$at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                                                                                $at->encoding = Zend_Mime::ENCODING_BASE64;
                                                                                $at->filename = 'Exam_report_for'.'_'.$currentselectedvenu.'_'.'For'.'_'.$examdateorig.'.pdf';   
										$sender_email = 'itwinesgm@gmail.com';
										$sender = 'iTWINE';
										$receiver_email = $larconfigdetails['Schedulerpushemail'];
										$receiver = 'Admin';
										$mail->setFrom('itwinesgm@gmail.com','itwine')
											 ->addTo($larconfigdetails['Schedulerpushemail'], Zuarudin)
									                 ->setSubject("Exam_report_for"."_".$currentselectedvenu."_"."For"."_".$examdateorig)
                                                                                         ->addAttachment($at);
     
									 		  							
									    //  $result = $mail->send($transport);

	                                     try {
							$mail->send($lobjTransport);
unlink('pdf/Exam_report_for'.'_'.$currentselectedvenu.'_'.'For'.'_'.$examdateorig.'.pdf');
							echo '<script language="javascript">alert("Exam results successfully  send to admin")</script>';	
							echo "<script>parent.location = '".$this->view->baseUrl()."/examreportonvenue/index';</script>";
				                	 die();
				                	 //echo 1;
						} catch (Exception $e) {
							echo '<script language="javascript">alert("Unable to send mail \n check Internet Connection ")</script>';	
							echo "<script>parent.location = '".$this->view->baseUrl()."/examreportonvenue/index';</script>";
				                	 die();	
				                	 //echo 0;
						}
		
		
    	
    }

//updated issues on 18/07/2013
public function issuesAction() 
	{
	
	    $this->view->lobjform = $this->lobjstudentissueform;
	    $this->view->venue = $selectedvenu = $this->_getParam('venu');
		$this->view->examdate =$takenexamdate = $this->_getParam('examdate');		
		$result = $this->lobjexamreport->fngetissuelist();
		if ($this->_request->isPost () )
		{
			$larrformData = $this->_request->getPost ();
			
				$this->lobjexamreport->fninsertissuedetails($larrformData);
			}	
		$this->view->larresult = $result;
	}
	
	public function lookupstudentAction()
	{	
		$this->_helper->layout->disableLayout();
		$this->view->venue= $Examvenue = $this->_getParam('Venue');
		$this->view->examdate =$Examdate = $this->_getParam('Exam');
		$this->view->idissue =$Examdate = $this->_getParam('idissue');		
	}	
	public function refinestudentlistAction(){
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		 $strcandidatename = $this->_getParam('candidatename');
		 $strcandidateicno = $this->_getParam('candidateicno');
		$this->view->examdate =$examdate = $this->_getParam('examdate');
		$this->view->venue =$examvenue = $this->_getParam('examvenue');
		$larrgetregpin = $this->lobjexamreport->fnGetstudentexamdetails($strcandidatename,$strcandidateicno,$examdate,$examvenue);
		echo Zend_Json_Encoder::encode($larrgetregpin);
		
	}
	public function getcandidatelookupAction(){		
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$lintidapplication = $this->_getParam('idapplication');
		$idissue = $this->_getParam('idissue');
		
		$larrgetcandidatelookup = $this->lobjexamreport->fnGetcandidatelookup($lintidapplication);        		
		 $count=count($larrgetcandidatelookup);
        $tabledata.='<table class="table" width="100%"><tr><th><b>Candidates Name</b></th><th><b>ICNO</b></th>';	
		for($lokiter=0;$lokiter<$count;$lokiter++){			
			 $iadppss=$larrgetcandidatelookup[$lokiter]['IDApplication'];
				$tabledata.='<tr><td ><b>'.$larrgetcandidatelookup[$lokiter]['FName'].' </b></td><td ><b>'.$larrgetcandidatelookup[$lokiter]['ICNO']."</b></td><td><input type='hidden' id='idapp' name='idapp[$idissue][]' value='$iadppss'></td></tr>";
				
			
		}	
		echo $tabledata.='<table/>';
	}
	
	
	
}