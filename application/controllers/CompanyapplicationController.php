<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class CompanyapplicationController extends Zend_Controller_Action 
{   //Controller for the User Module
	public $gsessionbatch;//Global Session Name
	private $_gobjlogger;
	public function init()
	{   //initialization function		
		$this->gsessionbatch = Zend_Registry::get('sis'); 		
		if(empty($this->gsessionbatch->idCompany))
		{ 
		 $this->_redirect( $this->baseUrl . '/batchlogin/logout');					
		}
		$this->_helper->layout()->setLayout('/adhoc/usty1');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	    $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object
		$this->fnsetObj();
		$this->lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //Company student details model object
	}
	
	public function fnsetObj() {				
		$this->lobjTakafulmodel = new App_Model_Takafulapplication ();
		$this->lobjTakafulForm = new App_Form_Takafulapplication (); //intialize user lobjuserForm
		$this->lobjstudentmodel = new App_Model_Companyapplication(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object		
		$this->lobjstudentForm = new App_Form_Companyapplication(); //intialize user lobjuserForm
		$this->lobjAdhocApplicationForm = new App_Form_Adhocapplication();
		$this->lobjform = new App_Form_Search ();
		$this->lobjBatchcandidatesmodel = new App_Model_Batchcandidates(); //user model object	
		$this->lobjCompanypayment = new Finance_Model_DbTable_Approvecreditstudenttakaful ();	
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
	public function indexAction() {
		
		$this->view->editid = $this->_getParam('editid');		
		$idCompany = $this->gsessionbatch->idCompany;
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany); //get user details		
		$larrresultrespin=$this->lobjstudentmodel->fngetRegistratinpin($this->gsessionbatch->idCompany);

		$this->view->lobjstudentForm = $this->lobjstudentForm;
  		$this->view->companyDetails =  $larrresult;
  		$this->lobjstudentForm->IdRegister->addMultiOptions($larrresultrespin);
  		if($this->gsessionbatch->mess != "")$this->view->alertError    = $this->gsessionbatch->mess;
  		$this->gsessionbatch->mess = "";
 		
  		$larrresultCompany = $this->lobjstudentmodel->listBatchApplication($idCompany);  		
  		
		$this->view->application =  $larrresultCompany;
		$data = array();
		
		 foreach($larrresultCompany as $rs){
		 	$idApplication = $rs['idBatchRegistration'];
		 	$data['application']=$rs;
		 	$larrresultCompanyDetails = $this->lobjstudentmodel->listBatchApplicationDetails($idApplication);
		 	$data['numCandidate'] = $larrresultCompanyDetails;
			
		 }
		 
		 
  		$this->view->applicationDetails =  $data['numCandidate'];
  		
  		if ($this->_request->isPost () && $this->_request->getPost ('Next')){  			
  			if($this->_request->getPost('newapp') == 1) {
  				//echo "<script>window.location = '".$this->view->baseUrl()."/companyapplication/newapplication";
  				$this->_redirect( $this->baseUrl . "/companyapplication/newapplication");
				
  				//$this->_redirect($this->view->url(array('module'=>'default' ,'controller'=>'companyapplication', 'action'=>'newapplication'),'default',true));
				//exit;	
  			}
  			else {  				
  				    $IdRegister  = $this->_request->getPost('IdRegister');  
  				    
  					$larrresultBatch = $this->lobjstudentmodel->fngetBatchDetails($IdRegister);  
  					//print_r($larrresultBatch);die();					
  					 if($larrresultBatch){
  					 	$batchId = $larrresultBatch['idBatchRegistration'];
  					 	if($larrresultBatch['AdhocDate']=='0000-00-00')
  					 	{
			        	    //$this->gstrsessionbatch->__set('idBatchRegistration',$larrresultBatch['idBatchRegistration']);			        	
			        		$this->_redirect( $this->baseUrl . "/batchcandidates/index/batchId/$batchId");	
  					 	}
  					 	else 
  					 	{
			        		$this->_redirect( $this->baseUrl . "/batchcandidates/adhoc/batchId/$batchId");	
  					 	}
  					 	   //echo "<script>parent.location = '".$this->view->baseUrl()."/batchcandidates/index/batchId/$batchId';</script>";	
			        	exit;
			        }
			        else {
		            		$this->gsessionbatch->mess = 'Registration Pin is incorrect';
		            		$this->_redirect( $this->baseUrl . "/companyapplication/index");exit;		           
			        } 				
  							$this->_redirect( $this->baseUrl . "/companyapplication/index");
  			}
  			
  		}
  		
  		if ($this->_getParam ( 'IdRegister' )) {
  			
  			$IdRegister = $this->_getParam ( 'IdRegister' );			
			$larrresultBatch = $this->lobjTakafulmodel->fngetBatchDetails ( $IdRegister );
						
			if ($larrresultBatch) {
				$batchId = $larrresultBatch ['idBatchRegistration'];
				if ($larrresultBatch ['AdhocDate'] == '0000-00-00') {
					$this->_redirect ( $this->baseUrl . "/companycandidates/index/batchId/$batchId" );
				} else {
					$this->_redirect ( $this->baseUrl . "/companycandidates/adhoc/batchId/$batchId" );
				}
				exit ();
				
				
			} else {				
				$this->gsessionidtakafuloperator->mess = 'Rgistration Pin is incorrect';
				$this->_redirect ( $this->baseUrl . "/takafulapplication/index" );
				exit ();
			}
			$this->_redirect ( $this->baseUrl . "/takafulapplication/index" );
  		}
	}
	

   public function displayAction()
	{
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		$lintinsertedId = $this->_getParam('insertedid');
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany);		
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);
		
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $lintinsertedId;
	}
	
	
	
	public function showpopupregistrationAction() {
		
		$this->_helper->layout->disableLayout ();
		$idregpin = $this->_getParam ( 'idbatch' );		
				
		$larrsultfailedStud=$this->lobjTakafulmodel->fnGetfailedregistrered ($idregpin);		
		$this->view->failedstudentlist=$larrsultfailedStud;
		
		$larrresultStudent = $this->lobjTakafulmodel->fnGetStudregistrered ($idregpin);
		$this->view->regpin = $idregpin;
		if (!$larrresultStudent) {
			$this->view->showshceduler = 0;
		} else {
			$this->view->showshceduler = 1;
		}	
	}
	/*
	 * sadfsdafsdkfjlsdkafjs
	 */
	//asdfsadfsd
	
	/*
	 * 
	 */
	public function venueseatavailabilityAction() {
		
		$this->_helper->layout->disableLayout ();
		$idvenue = $this->_getParam ( 'idvenue' );
		$fromdate = $this->_getParam ( 'fromdate' );
		$todate = $this->_getParam ( 'todate' );		
		$larrresultcenter = $this->lobjTakafulmodel->fnGetexamcenterfull ( $idvenue, $fromdate, $todate );		
		$this->view->examcenterfull = $larrresultcenter;
	
	}
	
	public function newapplicationAction() { // action for search and view
		
		$this->view->lobjform = $this->lobjform;
		$this->view->lobjTakafulForm = $this->lobjTakafulForm;
		$month = date ( "m" ); // Month value
		$day = date ( "d" ); //today's date
		$year = date ( "Y" ); // Year value
		$yesterdaydate = date ( 'Y-m-d', mktime ( 0, 0, 0, $month, ($day - 1), $year ) );
		
		$datsearch = "{min:'$yesterdaydate',datePattern:'dd-MM-yyyy'}";
		$this->view->lobjform->FromDate->setAttrib ( 'constraints', $datsearch );
		$this->view->lobjform->FromDate->setAttrib ( 'required', "true" );
		$this->view->lobjform->ToDate->setAttrib ( 'required', "true" );
		$operatortype = $this->view->operatortype = $this->gobjsessionsis->operatortype; 
		
		
		$lobjform=$this->view->lobsearchform = new App_Form_Search ();//send the lobjuserForm object to the view
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany); //get user details
 		$this->view->lobjstudentForm = $this->lobjstudentForm;
  		$this->view->companyDetails =  $larrresult;
  		
  		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramNameList();		
		$this->view->lobsearchform->field1->addMultiOptions($larrbatchresult);
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Print' )) {
			$larrformData = $this->_request->getPost ();			
			
		$larrresultcenter = $this->lobjTakafulmodel->fnGetexamcenter ( $larrformData );
			$this->view->lobjform->FromDate->setValue ( $larrformData ['FromDate'] );
			$this->view->lobjform->ToDate->setValue ( $larrformData ['ToDate'] );
			$this->view->examcenter = $larrresultcenter;
		}
		
		
  		
  		$larrresultdiscount = $this->lobjstudentmodel->fngetintialdiscount();
  		
  		/*$larrresultcenter = $this->lobjstudentmodel->fnGetexamcenter();
  		$this->view->examcenter=$larrresultcenter;*/
  		
  		$larrresultcourse= $this->lobjstudentmodel->fnGetcoursename();
  		$this->view->coursename=$larrresultcourse;
  		
  		$this->view->discount=$larrresultdiscount['Discount'];
  		
  		$larrcaptionresult = $this->lobjstudentmodel->fnGetCaptionName();
  		if($larrcaptionresult['CourseAliasName'])
  		{
  		$this->view->caption=$larrcaptionresult['CourseAliasName'];
  		}
  		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();		
		//$this->lobjstudentForm->idPrograms->addMultiOption('0','Select');
		$this->lobjstudentForm->idPrograms->addMultiOptions($larrbatchresult);
		$this->lobjstudentForm->idCompany->setValue($this->gsessionbatch->idCompany);
		
		/// to get Credit to IBFIM account //
		
		$larrresult = $this->lobjstudentmodel->fntogetpaylater($this->gsessionbatch->idCompany);
		if($larrresult)
		{
		   $this->lobjstudentForm->ModeofPayment->addMultiOption('7','Credit to IBFIM account');
		}
		
		// end of Credit to IBFIM account //

		///////////////////////////for pay later/////////////////////////
		$larrpaylater = $this->lobjstudentmodel->fngetpaylater ( $this->gsessionbatch->idCompany );		
		$cntpaylater = count ( $larrpaylater );
		if ($cntpaylater > 1) {
			$this->lobjstudentForm->ModeofPayment->addMultiOption ( $larrpaylater ['idDefinition'], $larrpaylater ['DefinitionDesc'] );
		}		
		$this->lobjstudentForm->ModeofPayment->setValue ( 181 );
		//////////////////////////end for pay later/////////////////////
		
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save'))
		{   
			
			$larrformData = $this->_request->getPost ();
            $larrformData['Servicetax'] = number_format($larrformData['Servicetax'], 2);			
			if ($this->lobjstudentForm->isValid($larrformData)) 
			{		
				
			$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );
			
			
			
			
			$lastInsId = $this->lobjstudentmodel->fnInsertPaymentdetails($larrformData);
			
			if ($larrformData ['ModeofPayment'] == 181) {
					$larrformDataapp ['IDApplication'] = $lastInsId;
					$larrformDataapp ['companyflag'] = 1;
					$larrformDataapp ['Amount'] = $larrformData ['grossAmt'];
					$larrformDataapp ['UpdUser'] = 1;
					$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
					$regid = $this->lobjCompanypayment->fngeneraterandom ();
					$flag=0;// IF ordinary registration		
					$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);
				}
				
				
			$db = Zend_Db_Table::getDefaultAdapter();				
			//$lastid = $db->lastInsertId("tbl_batchregistration","idBatchRegistration");	
			$lastpaymentid=$this->lobjstudentmodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$lastInsId,1);
			$auth = Zend_Auth::getInstance();// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Data is Saved"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);
    			 // print_r($this->gsessionregistration->mails);
			$lastpayid=$this->lobjstudentmodel->fngetmodeofpayment($lastpaymentid);
			if($lastpayid['ModeofPayment']==4)
			{   
				//$var=$this->_getParam(); 
				$this->_redirect( $this->baseUrl . "/companyapplication/display/insertedid/".$lastInsId); 
				//$this->_redirect( $this->baseUrl . "/companyapplication/index");
			}
			if ($lastpayid['ModeofPayment'] == 10) {
					$this->_redirect ( $this->baseUrl . "/companyapplication/migspayment/insertedid/" . $lastInsId );
				}
			if($lastpayid['ModeofPayment']==7)
			{
				$this->_redirect( $this->baseUrl . "/companyapplication/ibfim/operatortype/$operatortype/insertedId/".$lastInsId."/idcompany/".$this->gsessionbatch->idCompany);
			}	
			if($lastpayid['ModeofPayment']==1)
			{   
				//$var=$this->_getParam(); 
				$this->_redirect( $this->baseUrl . "/companyapplication/fpxpageone/insertedId/".$lastInsId); 
				//$this->_redirect( $this->baseUrl . "/companyapplication/index");
			}
			else if($lastpayid['ModeofPayment']!=2)
			{   
				//$var=$this->_getParam(); 
				$this->_redirect( $this->baseUrl . "/companyapplication/index/editid/".$lastInsId); 
				//$this->_redirect( $this->baseUrl . "/companyapplication/index");
			}
			else 
			{
			$this->_redirect( $this->baseUrl . "/companyapplication/confirmpayment/insertedId/$lastInsId");
			exit;
			}
			//$this->_redirect($this->view->url(array('module'=>'default' ,'controller'=>'companyapplication', 'action'=>'confirmpayment','insertedId'=>$lastInsId),'default',true));
			exit;	
				}		
		}
	}
	

	
	
	
	public function adhocapplicationAction() { // action for search and view
	
		$lobjform=$this->view->lobsearchform = new App_Form_Search ();//send the lobjuserForm object to the view
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany); //get user details
 		$this->view->lobjstudentForm = $this->lobjstudentForm;
 		$this->view->lobjAdhocApplicationForm = $this->lobjAdhocApplicationForm;
  		$this->view->companyDetails =  $larrresult;
		$larrcaptionresult = $this->lobjstudentmodel->fnGetCaptionName();
		
		$larrresultdiscount = $this->lobjstudentmodel->fngetintialdiscount();
  		//print_r($larrresultdiscount['Discount']);die();
  		
  		$this->view->discount=$larrresultdiscount['Discount'];
  		
  		if($larrcaptionresult['CourseAliasName'])
  		{
  		$this->view->caption=$larrcaptionresult['CourseAliasName'];
  		}
  		$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName();		
		//$this->lobjstudentForm->idPrograms->addMultiOption('0','Select');
		$this->lobjstudentForm->idPrograms->addMultiOptions($larrbatchresult);
		$this->lobjstudentForm->idCompany->setValue($this->gsessionbatch->idCompany);	
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save')){
			
			$larrformData = $this->_request->getPost ();
			//print_r($larrformData);
			//die();	
			if ($this->lobjstudentForm->isValid($larrformData)) {		
			$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );
		
			$lastInsId = $this->lobjstudentmodel->fnInsertPaymentdetailsadhoc($larrformData);	
		    $lastpaymentid=$this->lobjstudentmodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$lastInsId);
			$lastpayid=$this->lobjstudentmodel->fngetmodeofpayment($lastpaymentid);
			if($lastpayid['ModeofPayment']!=2)
			{
				$this->_redirect( $this->baseUrl . "/companyapplication/index");
			}
			else 
			{
			$this->_redirect( $this->baseUrl . "/companyapplication/confirmpayment/insertedId/$lastInsId");
			exit;
			}	
		}
		}
	}
	
public function confirmpaymentAction()
{
		$this->view->lobjstudentForm = $this->lobjstudentForm;
		$lintinsertedId = $this->_getParam('insertedId');
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany);		
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);	
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $lintinsertedId;
		//Get SMTP Mailing Server Setting Details
		$postArray = $this->_request->getPost ();
		$this->view->pageStatus = 1;
		if($postArray){	
				
				if($postArray['payment_status'] = 'Completed'){
					$postArray['UpdUser']= 1;//$auth->getIdentity()->iduser;
					$postArray['UpdDate']= date ( 'Y-m-d:H-i-s' );					
					$postArray['Regid']  = substr($postArray['txn_id'], 1, 6).rand(1000, 9999).substr($postArray['txn_id'], 5, 9);
					$this->lobjstudentmodel->fnInsertPaypaldetails($postArray,$this->gsessionbatch->idCompany,$lintinsertedId);	
					$this->view->Regid= $postArray['Regid'];	
					$this->view->mess = "Payment Completed Sucessfully";					
					$this->view->pageStatus = 2;					
						$StudModel = new App_Model_Studentapplication();
						$larrSMTPDetails  = $StudModel->fnGetSMTPSettings();
						$lstrSMTPServer   = $larrSMTPDetails['SMTPServer'];
						$lstrSMTPUsername = $larrSMTPDetails['SMTPUsername'];
						$lstrSMTPPassword = $larrSMTPDetails['SMTPPassword'];
						$lstrSMTPPort     = $larrSMTPDetails['SMTPPort'];
						$lstrSSL          = $larrSMTPDetails['SSL'];
						$lstrSMTPFromEmail= $larrSMTPDetails['DefaultEmail'];
						
						$lobjTransport = new Zend_Mail_Transport_Smtp();
						$lobjProtocol = new Zend_Mail_Protocol_Smtp($lstrSMTPServer);
						
						//Get Email Template Description
						$larrEmailTemplateDesc =  $StudModel->fnGetEmailTemplateDescription("Batch Registration");
					
						//Get Student's Mailing Details
						$larrStudentMailingDetails = $larrresult;
						
						if($larrEmailTemplateDesc['TemplateFrom']!=""){
							$lstrEmailTemplateFrom 	  =  $larrEmailTemplateDesc['TemplateFrom'];
							$lstrEmailTemplateFromDesc=  $larrEmailTemplateDesc['TemplateFromDesc'];
							$lstrEmailTemplateSubject =  $larrEmailTemplateDesc['TemplateSubject'];
							$lstrEmailTemplateBody    =  $larrEmailTemplateDesc['TemplateBody'];
							$lstrEmailTemplateFooter  =  $larrEmailTemplateDesc['TemplateFooter'];
													
							$larrEmailIds[0] = $larrStudentMailingDetails["Email"];
							$larrNames[0] 	 = $larrStudentMailingDetails['CompanyName'];
							$lstrStudentName = $larrStudentMailingDetails['CompanyName'];
							
							try{
								$lobjProtocol->connect();
						   		$lobjProtocol->helo($lstrSMTPUsername);
								$lobjTransport->setConnection($lobjProtocol);
						 	
								//Intialize Zend Mailing Object
								$lobjMail = new Zend_Mail();
						
								$lobjMail->setFrom($lstrSMTPFromEmail,$lstrEmailTemplateFromDesc);
								$lobjMail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
								$lobjMail->addHeader('MIME-Version', '1.0');
								$lobjMail->setSubject($lstrEmailTemplateSubject);
						
								for($lintI=0;$lintI<count($larrEmailIds);$lintI++){
									if($larrEmailIds[$lintI] != ""){
										$lobjMail->addTo($larrEmailIds[$lintI],$larrNames[$lintI]);	
																
										//replace tags with values
										//$Link = "<a href='".$this->Url."/registration'>Here</a>";// "<a href='".$this->baseUrl()."/reg/registration'>Here</a>";											
										$lstrEmailTemplateBody = str_replace("[Company]",$lstrStudentName,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[Amount]",$postArray['mc_gross'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[TransactionId]",$postArray['txn_id'],$lstrEmailTemplateBody);
										$lstrEmailTemplateBody = str_replace("[LoginId]",$postArray['Regid'],$lstrEmailTemplateBody);
										//$lstrEmailTemplateBody = str_replace("[Link]",$Link,$lstrEmailTemplateBody);
										$lstrEmailTemplateBody .= "<br>".$lstrEmailTemplateFooter;
										
										$lobjMail->setBodyHtml($lstrEmailTemplateBody);
								
										try {
											$lobjMail->send($lobjTransport);
										} catch (Exception $e) {
											$lstrMsg = "error";      				
										}	
										$lobjMail->clearRecipients();
										$this->view->mess .= ". Login Details have been sent to user Email";
										unset($larrEmailIds[$lintI]);
									}
								}
							}catch(Exception $e){
								$lstrMsg = "error";
							}
						}else{
							$lstrMsg = "No Template Found";
						}
				}
				else {
					$this->view->mess = "Payment Failed";
				}				
			}		
	}

public function newstudentapplicationAction() {exit;
}
	public function getprogramtotalAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$idprogram = (int)$this->_getParam('idprogram');		
		$larrresult = $this->lobjstudentmodel->fnGetProgramFee($idprogram); 
		//echo $larrresult['sum(abc.amount)'];
		echo json_encode($larrresult);die();
		exit;
	}
	
	
	public function getdiscountAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$nocandidates = (int)$this->_getParam('nocandidates');		
		$idprogram = (int)$this->_getParam('idprogram');

		$larrresultprog = $this->lobjstudentmodel->fngetprogramdiscount($idprogram,$nocandidates);
		//echo "<pre>";
		//print_r($larrresultprog);die();
		if(empty($larrresultprog))
		{
		   $larrresultprog[0]['Amount']=0;
		   echo $larrresultprog[0]['Amount'];
		   die();
		}
		else
		{
		  echo $larrresultprog[0]['Amount'];
		  die();
		}
		
	}
	
	
    public function printreportAction() 
    {			
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();		
		$IdApplication = (int) $this->_getParam('insertedId');					
	    $larrresult = $this->lobjstudentmodel->fnGetExamDetails($IdApplication);  
	    $totamt= (int)$larrresult ['totalAmount'];		    
		$AmountInWords = $this->lobjstudentmodel->fnGetAmountInWords($totamt);
/*		print_r($AmountInWords);
		die();*/
		    						
		//object to initialize ini file
		  $lobjAppconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini','development');										
		    try 
		    {	
	            //java class
	            $lobjdbdriverclass = new Java("java.lang.Class");
	            
	            //set db driver
	            $lobjdbdriverclass->forName("com.mysql.jdbc.Driver");
	
	            //driver manager object
	            $lobjdrivermanager = new Java("java.sql.DriverManager");
	            
	            //get the db connection
				$lstrConnection  =  "jdbc:mysql://".
										$lobjAppconfig->resources->db->params->host."/".
										$lobjAppconfig->resources->db->params->dbname."?user=".
										$lobjAppconfig->resources->db->params->username."&password=".
										$lobjAppconfig->resources->db->params->password;
														
				$lobjconnection = $lobjdrivermanager->getConnection($lstrConnection);
	           // print_r($lobjconnection);
	          //  die();
	            //Jasper Compile manager object
	            $lobjcompileManager = new Java(
	            					"net.sf.jasperreports.engine.JasperCompileManager");	            
	            echo "CompileManager object created</br>";
	            $lstrreportdir = realpath(".") . "/report/";
	            $lstrimagepath = realpath(".") . "/images/";
	            	
	             //compiled report path
	              $lobjreport = $lobjcompileManager->compileReport(realpath($lstrreportdir."companyapplicationreport.jrxml"));	            	            
	            //Jasper Fill Manager object
	           /* print_r($lobjreport);
	            die();*/
	            $lobjfillManager = new Java(
	            					"net.sf.jasperreports.engine.JasperFillManager");
	            $int1 = new Java("java.lang.Integer");
	            //Hashmap object
	           //print_r($lstrreportdir);die();
	            $lobjparams = new Java("java.util.HashMap");	            
	          	$lobjparams->put("IDAPPLICATION",$IdApplication);
	          	$lobjparams->put ( "IMAGEPATH", $lstrimagepath . "ibfim.jpg" );
	          	$lobjparams->put("AMOUNTINWORDS",$AmountInWords['Amount']);
	          	
	          /*	print_r($lstrimagepath);
	          	die();*/
	           
	           echo "Fill Manager</br>";
	            					
	            //Jasper Print Object
	            $lobjjasperPrint = $lobjfillManager->fillReport(
	            					$lobjreport, $lobjparams, $lobjconnection);
	            					
	            echo "Jasper Printed</br>";
	            
	            //Jasper Export Manager object
	            $lobjexportManager = new Java(
	            					"net.sf.jasperreports.engine.JasperExportManager");
	            
	            //output file path
	            $lstrhtmloutputPath = realpath(".") . "/" . "output.html";
	            echo "Before Export</br>";
	            $session = Zend_Session::getId();
	            $lstrpdfoutputPath = realpath(".") . "/" . "$session.pdf";
	            $objStream = new Java("java.io.ByteArrayOutputStream");
	            $lobjexportManager->exportReportToPdfFile($lobjjasperPrint,$lstrpdfoutputPath);
	            
	            //Export report to HTML	            
	            echo 'HTML Exported</br>';
	
				header("Content-type: application/pdf;charset=utf-8;encoding=utf-8");
				header('Content-Disposition: attachment; filename="Company_details.pdf"');
				
	            readfile($lstrpdfoutputPath);
	            unlink($lstrpdfoutputPath);
				echo "finished";	
		 		 		            
		    } 
		    catch (JavaException $lobjexception) 
		    {
		    	echo 'Exception caught: ', $lobjexception->getMessage() . "\n";
		    }		    		   
			 //$this->_redirect( $this->baseUrl . "/companyapplication/index/editid/1");
	}

	
	//////
	
public function adhocindexAction() {
		$this->view->editid = $this->_getParam('editid');
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany); //get user details
		
		$larrresultrespin=$this->lobjstudentmodel->fngetAdhocRegistratinpin($this->gsessionbatch->idCompany);
		//print_r($larrresultrespin);die();
 		$this->view->lobjstudentForm = $this->lobjstudentForm;
  		$this->view->companyDetails =  $larrresult;
  		$this->lobjstudentForm->IdRegister->addMultiOptions($larrresultrespin);
  		if($this->gsessionbatch->mess != "")$this->view->alertError    = $this->gsessionbatch->mess;
  		$this->gsessionbatch->mess = "";
  		if ($this->_request->isPost () && $this->_request->getPost ('Next')){  			
  			if($this->_request->getPost('newapp') == 1) {
  				//echo "<script>window.location = '".$this->view->baseUrl()."/companyapplication/newapplication";
  				$this->_redirect( $this->baseUrl . "/companyapplication/adhocapplication");
				
  				//$this->_redirect($this->view->url(array('module'=>'default' ,'controller'=>'companyapplication', 'action'=>'newapplication'),'default',true));
				//exit;	
  			}
  			else {
  				    $IdRegister  = $this->_request->getPost('IdRegister');  				
  					$larrresultBatch = $this->lobjstudentmodel->fngetBatchDetails($IdRegister);  
  					//print_r($larrresultBatch);die();					
  					 if($larrresultBatch){
  					 	$batchId = $larrresultBatch['idBatchRegistration'];
  					 	if($larrresultBatch['AdhocDate']=='0000-00-00')
  					 	{
			        	//$this->gstrsessionbatch->__set('idBatchRegistration',$larrresultBatch['idBatchRegistration']);			        	
			        	$this->_redirect( $this->baseUrl . "/batchcandidates/index/batchId/$batchId");	
  					 	}
  					 	else 
  					 	{
			        	$this->_redirect( $this->baseUrl . "/batchcandidates/adhoc/batchId/$batchId");	
  					 	}
  					 	//echo "<script>parent.location = '".$this->view->baseUrl()."/batchcandidates/index/batchId/$batchId';</script>";	
			        	exit;
			        }
			        else {
		            	$this->gsessionbatch->mess = 'Rgistration Pin is incorrect';
		            	$this->_redirect( $this->baseUrl . "/companyapplication/adhocindex");exit;		           
			        } 				
  						$this->_redirect( $this->baseUrl . "/companyapplication/adhocindex");
  			}
  			
  		}
	}
	
	
public function changepasswordAction() { // action for search and view
		$lobjform=$this->view->lobsearchform = new App_Form_Search ();//send the lobjuserForm object to the view
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany); //get user details
 		$this->view->lobjstudentForm = $this->lobjstudentForm;
 		$this->view->lobjAdhocApplicationForm = $this->lobjAdhocApplicationForm;
  		$this->view->companyDetails =  $larrresult;
		$idcomp=$this->gsessionbatch->idCompany;
		
		$pass=$larrresult['Password'];
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save')){
			$larrformData = $this->_request->getPost ();
			if ($this->lobjAdhocApplicationForm->isValid($larrformData)) {	
			
			$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );
			$larrformData['oldpassword']=md5($larrformData['oldpassword']);
			//print_r($larrformData);die();
		if($pass==$larrformData['oldpassword'])
		{
			$larrformData['newpassword']=md5($larrformData['newpassword']);
           //echo $larrformData['newpassword'];die();
			$lastInsId = $this->lobjstudentmodel->updatecompanypass($larrformData['newpassword'],$idcomp);
			    $auth = Zend_Auth::getInstance();// Write Logs
				$priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Password Changed"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);	
		  	$this->_redirect( $this->baseUrl . "/companyapplication/index");
		
		}
		else 
		{
			echo '<script language="javascript">alert("The Old Password is Not Correct")</script>';
				//$this->_redirect( $this->baseUrl . "/companyapplication/changepassword");
  			//die();
			
		}	
			
		}
		}
	}
	
	
	public function fpxpageoneAction(){
		
		$lintinsertedId = $this->_getParam('insertedId');	
		
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany);	
		
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);	
	
		
		$this->view->data = $larrPaymentDetails;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->CmpnyIds = $lintinsertedId;
		
		unset($_SESSION["pageName"]);
		unset($_SESSION["StudsId"]);
		unset($_SESSION["idCompany"]);
		$_SESSION["pageName"] = "companyapplication";	
		$_SESSION["CmpnyId"]  = $lintinsertedId;
		$_SESSION["idCompany"]  = $this->gsessionbatch->idCompany;	
	}	
	public function fpxpagetwoAction(){		
			
		$this->view->intidstudent=$lintinsertedId = $this->_getParam('insertedId');	
			
		$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($this->gsessionbatch->idCompany);		
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($lintinsertedId);		
		//print_r($larrresult);
		
		$this->view->data = $larrPaymentDetails;	
		
		error_reporting(E_ALL);
		$address = "127.0.0.1";
		$service_port = 6000;
		// Create a TCP/IP socket. 
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($socket < 0){ 
			//echo "socket_create() failed: reason: " . socket_strerror($socket) . "\n"; 
		}
		else{ 
				//echo "Socket creation successfull."; 
		}				
		// Establish socket connection. 
		$result = socket_connect($socket, $address, $service_port);
		if (!$result){ 
			//echo "Socket connection failed.<br>";
			die();
		}
		else{ 
			//echo "Socket connection successfull.<br>"; 
		}				
		// Generating String to send to plugin. 
		$messageOrderNo = $_POST['TxnOrderNo'];
		$messageTXNTime = date('YmdHis');
		$sellerOrderNo = $_POST['TxnOrderNo'];
		$this->view->TxnAmount = $messageAmount = $_POST['TxnAmount'];
		$sellerID = $_POST['sellerID'];			
		
		$in = "message:request|message.type:AR|message.token:01|message.orderno:$messageOrderNo|message.ordercount:1|message.txntime:$messageTXNTime|message.serialno:1|message.currency:MYR|message.amount:$messageAmount|charge.type:AA|seller.orderno:$sellerOrderNo|seller.id:$sellerID|seller.bank:01|\n";
		$out = '';			
		
		socket_write($socket, $in);		
		while ($out = socket_read($socket,6001)){
			$fpxValue = $out;	
		}
		$sendFpxValue = str_replace("\n", "", $fpxValue);
		$this->view->sendFpxValue = $sendFpxValue;			
		socket_close($socket);
	}	
	public function ibfimAction()
	{
	    $this->view->operatortype = $operator = 1;
		$this->view->insertedId =$insertedId = $this->_getParam('insertedId');
	    $this->view->idcompany = $idcompany = $this->_getParam('idcompany');	
	   $larresultbatchdetails = $this->lobjstudentmodel->fngetbatchregistrationdetails($insertedId);
	  
		$regpin = $larresultbatchdetails['registrationPin'];
		if($regpin==0)
		{
			$randomnumber1 = rand(100000,999999);
		    $randomnumber2 = rand(100000,999999);
		    $regpin  = $randomnumber1.''.$randomnumber2;
		}
		
		$larresult = $this->lobjstudentmodel->fnupdatebatchregdetailsforibfimpayment($insertedId,$regpin);		
		$this->view->lobjstudentForm = $this->lobjstudentForm;		
		$idcompany=$this->_getParam('idcompany');
		
				$larrresult = $this->lobjstudentmodel->fngetCompanyDetails($idcompany);	
		
		$larrPaymentDetails = $this->lobjstudentmodel->fngetPaymentDetails($insertedId);	
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $insertedId;	
	}
	
	
public function migspaymentAction()
{
		$insertedid = $this->_getParam('insertedid');
		$larrcompanydetails = $this->lobjstudentmodel->fngetbatchcompdetails($insertedid);
		
	    $this->view->name = $larrcompanydetails['CompanyName'];
		$this->view->amount = $larrcompanydetails['totalAmount'];
		$this->view->operatortype = $operator = 1;
		$this->view->insertedId =$insertedid;
	    $this->view->idcompany = $idcompany = $this->gsessionbatch->idCompany;
}
public function mipgtwoAction()
{
		$larrformData = $this->_request->getPost ();
	$this->view->formdata = $larrformData;	
	$this->view->operatortype = $larrformData['operatortype'];
}

/*public function viewreceiptAction(){
     $receiptlastid = $this->_getParam('receiptlastid');
     $idbatchregistration = $this->_getParam('idbatchregistration');
     $receiptdetails = $this->lobjstudentmodel->fngetreceiptdetails($receiptlastid,$idbatchregistration);
        
        $this->view->receiptlastid =$receiptlastid;
        $this->view->idbatchregistration =$idbatchregistration;
        $this->view->PaymentMode ="MIGS";
        $this->view->receiptdetails =$receiptdetails;
        $larresultbatchdetails = $this->lobjstudentmodel->fngetbatchregistrationdetails($idbatchregistration);
	$this->view->idcompany = $larresultbatchdetails['idCompany'];
     if ($this->_request->isPost () && $this->_request->getPost ( 'Print' )){	
         
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$this->lobjdistributionModel = new Registrations_Model_DbTable_Paymentdistribution();//distribution model
			$lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //amount in words
										
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbedemo/images/reportheader.jpg";				
			$time = date('h:i:s',time());
			
			
			$ReportName = $this->view->translate( "INVOICE_RECEIPT" );				
			$lstrreportytpe="Pdf";	
			$tabledata = '<img width=100% src="'.$imgp.'" />';
			$larrformData = $this->_request->getPost ();	
				

            $receiptdetails = $this->lobjstudentmodel->fngetreceiptdetails($larrformData['receiptlastid'],$larrformData['idbatchregistration']); //get receipt details
			$Regpin = $receiptdetails['0']['registrationPin'];
			$companyid= $larresultbatchdetails['idCompany'];
			$filename = 'INVOICE_RECEIPT_'.$companyid.'_'.$Regpin.'_'.$frmdate;	


			
			$date =date('d-m-Y');				
			$rcn = $larrformData['rcn'];
			$flag = $larrformData['flag'];
			
			//echo "<pre/>";print_r($receiptdetails);die();	
			$totalamt=$receiptdetails['0']['totalamount'];
			$Amountinwords = $lobjcompanystudentdetails->fnconvertnumbertowords($totalamt);
			$number = number_format($totalamt, 2, ".", "");
                        sscanf($number, '%d.%d', $whole, $fraction);
                        if($fraction){
                             $fractioninwords = $lobjcompanystudentdetails->fnconvertnumbertowords($fraction);
                             $Amountinwords=$Amountinwords.' and '.$fractioninwords.' cent Only';
                        }else{
                            $Amountinwords=$Amountinwords.' Only';
                        }
                        $mode="MIGS";
			if(isset($receiptdetails['0']['Fax'])){
				$fax=$receiptdetails['0']['Fax'];
			}else {
				$fax="-";
			}
			
			
			
			
			$tabledata.= "<table  width='100%' border=1 >
			                 <tr><td align='center'  colspan='12' style='font-size:130%;'><b><u><i>INVOICE</i></u></b><br/></td></tr>
			                <tr><th colspan='9' align='left'><b>To:
                                           </b><b>&nbsp;&nbsp;&nbsp;{$receiptdetails['0']['operatorname']}</b><br/>";                                           
									$tabledata.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$receiptdetails['0']['Address']}</b><br/>";
									$tabledata.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$receiptdetails['0']['Postcode']}</b><br/>";
									$tabledata.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$receiptdetails['0']['StateName']}</b><br/>";
									$tabledata.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$receiptdetails['0']['CountryName']}</b><br/>";
									$tabledata.= "<b>Attn:</b><b>&nbsp;{$receiptdetails['0']['ContactName']}</b>";
                                    $tabledata.= "<br/>";
									$tabledata.= "<b>Tel:</b><b>&nbsp;&nbsp;{$receiptdetails['0']['workphone']}
                                    </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Fax:</b><b>{$receiptdetails['0']['Fax']}</b><br/></th>
									<th align ='left' valign='top' colspan='3'>
									<b>GST Ref. No.:</b><b>&nbsp;&nbsp;&nbsp;<b>
									001167200256
									</b><br/><br/>
								    
									<b>Invoice:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									{$receiptdetails['0']['Invoiceuniqueid']}</b><br/><br/>
									<b>Date:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									$day</b><br/><br/>
									<b>Term:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									14 Days</b></th>
							</tr>
					  </table>
					  <br>";
					  $tax=$receiptdetails['0']['ServiceTax'];
					  $amount =($receiptdetails['0']['totalAmount']-$tax);
		              $tabledata.= "<table width='100%' border=1 align='center' >
									<tr>                           
										<th ><b>BatchId</b></th>
										<th ><b>Program Name</b></th>
										<th ><b>No Of Candidates</b></th>							
										<th ><b>Amount</b></th>
										<th ><b>GST</b></th>
										<th ><b>TOTAL Amount</b></th>
									</tr>";
					  $tabledata.= ' <tr>';
					  $tabledata.= '<td  align="center"><b>'.$receiptdetails['0']['registrationPin'].'</b></td>                                    
							        <td  align="center" ><b>'.$receiptdetails['0']['ProgramName'].'</b></td> 
                                    <td  align="center"><b>'.$receiptdetails['0']['totalNoofCandidates'].'</b></td> 									
							        <td  align="center" ><b>'.number_format($amount,2).'</b></td> 
							        <td  align="center" ><b>'.number_format($tax,2).'</b></td> 
									<td  align="center" ><b>'.number_format($receiptdetails['0']['totalAmount'],2).'</b></td>									
							</tr> ';							 
                $number = number_format($receiptdetails['0']['totalAmount'], 2, ".", "");
                sscanf($number, '%d.%d', $whole, $fraction);                
				$number = number_format($receiptdetails['0']['totalAmount'], 2, ".", "");
				$gst = $lobjcompanystudentdetails->fngettaxrate();
				$Amountinwords = $lobjcompanystudentdetails->fnconvertnumbertowords($receiptdetails['0']['totalAmount']);
                $amount = ($receiptdetails['0']['totalAmount']-$receiptdetails['0']['ServiceTax']);	
                if($fraction){
                     $fractioninwords = $lobjcompanystudentdetails->fnconvertnumbertowords($fraction);
                     $Amountinwords=$Amountinwords.' and '.$fractioninwords.' cent Only';
                }else{
                   $Amountinwords=$Amountinwords.' Only';
                }
                 $tabledata.='<tr >             						   
                                        <td  colspan = "5" align="right"><b>Sub-Total:<b></td><td align="right"><b>'.number_format($amount,2).'</b></td>
                                    </tr>
                                    <tr >             						   
                                        <td  colspan = "5" align="right"><b>GST payable @'.$gst['GST'].'%:<b></td><td align="right"><b>'.number_format($receiptdetails['0']['ServiceTax'],2).'</b></td>
                                    </tr>
                                <tr >             						   
                                <td  colspan = "5" align="right"><b>Total Amount:<b></td><td align="right"><b>'.number_format($receiptdetails['0']['totalAmount'],2).'</b>
							   </td>
						</tr>
                        <tr>
			            <td  align="center" colspan="6"><b>Ringgit Malaysia:</b><b>';                              					  
                $tabledata.=$Amountinwords.' </b></td></tr></table>';
		        //$Amountinwords = $this->lobjcompanystudentdetails->fnconvertnumbertowords($receiptdetails['Totalamount']);		 
		        // $tabledata.= '<tr>';
		        // $tabledata.= '<th align="right" colspan="5"><b>Total Amount:</b></th><th>'.number_format($receiptdetails['Totalamount']).'.00'.'</th></tr>';
                // $tabledata.= '<tr><th colspan="6" >Ringgit Malaysia:<b>'.$Amountinwords.' Only</b></th></tr></table>';
                $tabledata.="<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                             <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
		        $tabledata.='<table>
								<tr><td><b>Note:</b></td>
								</tr>
								<tr><td colspan=6><b>1.  Please make payment within 14 Days from the date of "registration" to the date of "this invoice".
                                 </b></td>
								</tr>
								<tr><td colspan=6><b>2.  All cheques must be  made payable to "IBFIM-TBE".</b></td>
								</tr>
								<tr><td colspan=6><b>3.  Please send this copy of invoice together with your payment for the attention of TBE Secretariat, Learning Management				
	                                      Talent Development Department, IBFIM.</b></td>
								</tr><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                                <tr><td align="center"><b>This is a computer generated document.  No signature is required.</b></td></tr>
								<tr>
								<td><hr/></td></tr>
								<tr>
								<td align="center"><b>IBFIM (Co. No. 763075W)
								3rd Floor, Menara Takaful Malaysia, Jalan Sultan Sulaiman,  50000 Kuala Lumpur, MALAYSIA</td></tr>
								<tr><td align="center"><b>(Tel) +603-2031 1010  (Fax) +603-2026 9988   (E-mail) tbe@ibfim.com
								</b></td></tr>
						</table>';
			
			
			$tabledata.= "<br/>";
			
			$tabledata.="<div style='border:0.5mm solid;font-family:arial;'>";
			$tabledata.="<table border='0' width='100%' align='center' >
							 	<tr>
								 	<td align='left' width='50%'><b>No. {$rcn}<br/></b></td>
								  	<td align='left' style='font-size:130%;'><b><u><i>RECEIPT</i></u></b><br/></td>
								 	<td align='left'><b>Date: {$date}<br/></b></td>
								</tr>
								<tr >
								  	<td align='left' colspan='3'><i>Received With Thanks from:<b>{$larrformData['CompanyName']}</b></i><br/></td>
								</tr>
								<tr>
									<td align='left' colspan='3'><i>Address : {$larrformData['Address']}</i></td>
								</tr>
								<tr>
									<td align='left' colspan='3' style='padding-left:7%'>
										<i>{$larrformData['StateName']}<br/>
				 							{$larrformData['CountryName']}<br/>
				 							PostCode:{$larrformData['Postcode']}<br/>
				 							Phone:{$larrformData['Phone']}<br/>
				 							Email:{$larrformData['Email']}<br/>
				 							Fax:{$fax}<br/>
				 							
				 						 </i>
									</td>
									
								</tr>
                                <tr>
									<td align='left' colspan='3'>Attn:{$larrformData['Contact']}</td>
								</tr>								
								<tr >
									<td align='left' colspan='3'><i> Payment By: {$mode}.</i><br/></td>
								</tr>
								<tr>
									<td align='left' colspan='3'><i>Towards Examination Fees as detail bellow.</i><br/></td>
								</tr>
						</table>";
			
			$tabledata.="<br/><table border='1' width='100%' align='center' style='margin:1em 1cm;'>
							<tr>								
								<th ><b>BatchId</b></th>							
								<th ><b>Invoice Number</b></th>
								<th ><b>Total Candidates</b></th>
								<th ><b>Amount</b></th>							
								<th ><b>GST</b></th>
								<th ><b>Total Amount</b></th>
                                                                <th ><b>Transaction Id</b></th>
                                                               
							</tr>";
                        
						$count=1;
						foreach($receiptdetails as $receiptdetails){
							$tabledata.="<tr>								
                                                                <td >{$receiptdetails['BatchId']}</td>	    		
								<td align='center'>".$receiptdetails['InvoiceNum']."</td>	    		       
							  	<td align='right'>".$receiptdetails['totalNoofCandidates']."</td>
							  	<td align='right'>".number_format($receiptdetails['Amount']-$receiptdetails['ServiceTax'],2)."</td>	    		
								<td align='right'>".number_format($receiptdetails['ServiceTax'],2)."</td>	    		       
							 	<td align='right'>".number_format($receiptdetails['Amount'],2)."</td>
                                                                <td align='right'>".$receiptdetails['transactionId']."</td>                                                               
							</tr>";
							 $count++;
						}
						
						
			$tabledata.="</table><table border='0' width='100%' align='center'>
							<tr >             						   
								<td align='center' colspan='3'><b>Total Amount Received(Ringgit Malaysia) : ".number_format($totalamt,2)."<b></td>
							</tr>
																			<tr><td></td></tr>
							<tr>
									<td align='center' colspan='3'><b>Ringgit Malaysia(In Words): {$Amountinwords}.</b><br/></td>
							</tr>
							<tr><td></td></tr>
												
												<tr >
							                    	<td align='center' colspan='3'><b>GST Ref.No:001167200256 </b></td>
							                    </tr>
						</table>
						<table border='0' width='100%' align='center'>
							<tr><td></td></tr>
							<tr >             						   
								<td  style='padding-left:85%' ><b>Signature<b></td>
								
							</tr>
							<tr>
							<td>$condition</td>
							</tr>
						</table>
				</div>";
			
 	//echo $tabledata;die();
     		/*if($lstrreportytpe=='Pdf')
				{
					include(APPLICATION_PATH.'/../library/MPDF53/mpdf.php');
                                        //echo "<pre/>";print_r($mpdf) ;die();
					$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
					$mpdf->SetDirectionality ( $this->gstrHTMLDir );
					$mpdf->text_input_as_HTML = true;
					$mpdf->useLang = true;
					$mpdf->SetAutoFont();
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 1; // 1 or 0 - whether to indent the first level of a list
					$mpdf->pagenumSuffix = ' / ';
                                        $mpdf->setFooter ('Copyright &copy; 2013, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
					//$mpdf->setFooter ('' );
					$mpdf->allow_charset_conversion = true; // Set by default to TRUE
					$mpdf->charset_in = 'utf-8';
					ini_set('max_execution_time',3600);
					$mpdf->WriteHTML($tabledata);
					$mpdf->Output("$filename.pdf",'D');
					//header("refresh: 2; url='".$this->view->baseUrl()."/registrations/paymentdistribution/index'");
				}
				if($lstrreportytpe=='Pdf'){
			ob_clean();
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="' . $filename . '"');
			header('Content-Transfer-Encoding: binary');
			header('Accept-Ranges: bytes');

			include(APPLICATION_PATH.'/../library/mpdf60/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->autoLangToFont  = true;
			$mpdf->autoScriptToLang = true;
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename,'D');
		}
		}
     
}*/
public function viewreceiptAction(){
     $receiptlastid = $this->_getParam('receiptlastid');
     $idbatchregistration = $this->_getParam('idbatchregistration');
     $receiptdetails = $this->lobjstudentmodel->fngetreceiptdetails($receiptlastid,$idbatchregistration);
        //echo "<pre/>";print_r($receiptdetails);die();
        $this->view->receiptlastid =$receiptlastid;
        $this->view->idbatchregistration =$idbatchregistration;
        $this->view->PaymentMode ="MIGS";
        $this->view->receiptdetails =$receiptdetails;
        $larresultbatchdetails = $this->lobjstudentmodel->fngetbatchregistrationdetails($idbatchregistration);
	$this->view->idcompany = $larresultbatchdetails['idCompany'];
     if ($this->_request->isPost () && $this->_request->getPost ( 'Print' )){	
         
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			$this->lobjdistributionModel = new Registrations_Model_DbTable_Paymentdistribution();//distribution model
			$lobjcompanystudentdetails = new Registrations_Model_DbTable_Companystudentdetails(); //amount in words
										
			$frmdate =date('d-m-Y');
			$day= date("d-m-Y");
			$host = $_SERVER['SERVER_NAME'];
			$imgp = "http://".$host."/tbedemo/images/reportheader.jpg";				
			$time = date('h:i:s',time());
			$filename = 'Receipt_Report_'.$frmdate.'.pdf';
			$ReportName = $this->view->translate( "INVOICE_RECEIPT" );				
			$lstrreportytpe="Pdf";	
			$tabledata = '<img width=100% src="'.$imgp.'" />';
			$larrformData = $this->_request->getPost ();	
				
            $receiptdetails = $this->lobjstudentmodel->fngetreceiptdetails($larrformData['receiptlastid'],$larrformData['idbatchregistration']); //get receipt details
			$Regpin = $receiptdetails['0']['registrationPin'];
			$companyid= $larresultbatchdetails['idCompany'];
			$filename = 'INVOICE_RECEIPT_'.$companyid.'_'.$Regpin.'_'.$frmdate;	
				
			$date =date('d-m-Y');				
			$rcn = $larrformData['rcn'];
			$flag = $larrformData['flag'];
			//$receiptdetails = $this->lobjstudentmodel->fngetreceiptdetails($larrformData['receiptlastid'],$larrformData['idbatchregistration']); //get receipt details
			//echo "<pre/>";print_r($receiptdetails);die();	
			 //changes 15-05-2015
					  $tax=$receiptdetails['0']['GST'];
					  $amount =$receiptdetails['0']['Invamount'];
					  $totalamount = ($amount+$tax);
					  
					  
			$totalamt=$totalamount;
			$Amountinwords = $lobjcompanystudentdetails->fnconvertnumbertowords($totalamt);
			$number = number_format($totalamt, 2, ".", "");
                        sscanf($number, '%d.%d', $whole, $fraction);
                        if($fraction){
                             $fractioninwords = $lobjcompanystudentdetails->fnconvertnumbertowords($fraction);
                             $Amountinwords=$Amountinwords.' and '.$fractioninwords.' cent Only';
                        }else{
                            $Amountinwords=$Amountinwords.' Only';
                        }
                        $mode="MIGS";
			if(isset($receiptdetails['0']['Fax'])){
				$fax=$receiptdetails['0']['Fax'];
			}else {
				$fax="-";
			}
			
			
			
			// $tabledata="";
					  $tabledata.= "<table  width='100%' border=1 >
					   <tr><td align='center'  colspan='12' style='font-size:130%;'><b><u><i>INVOICE</i></u></b><br/></td></tr>
					  <tr><th colspan='9' align='left'><b>To:
                                           </b><b>&nbsp;&nbsp;&nbsp;{$receiptdetails['0']['operatorname']}</b><br/>";                                           
									$tabledata.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$receiptdetails['0']['Address']}</b><br/>";
									$tabledata.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$receiptdetails['0']['Postcode']}</b><br/>";
									$tabledata.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$receiptdetails['0']['StateName']}</b><br/>";
									$tabledata.= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{$receiptdetails['0']['CountryName']}</b><br/>";
									$tabledata.= "<b>Attn:</b><b>&nbsp;{$receiptdetails['0']['ContactName']}</b>";
                                    $tabledata.= "<br/>";
									$tabledata.= "<b>Tel:</b><b>&nbsp;&nbsp;{$receiptdetails['0']['workphone']}
                                    </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Fax:</b><b>{$receiptdetails['0']['Fax']}</b><br/></th>
									<th align ='left' valign='top' colspan='3'>
									<b>GST Ref. No.:</b><b>&nbsp;&nbsp;&nbsp;<b>
									001167200256
									</b><br/><br/>
								    
									<b>Invoice:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									{$receiptdetails['0']['Invoiceuniqueid']}</b><br/><br/>
									<b>Date:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									$day</b><br/><br/>
									<b>Term:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									14 Days</b></th>
							</tr>
					  </table>
					  <br>";
					  
					 
					  
		              $tabledata.= "<table width='100%' border=1 align='center' >
									<tr>                           
										<th ><b>BatchId</b></th>
										<th ><b>Program Name</b></th>
										<th ><b>No Of Candidates</b></th>							
										<th ><b>Amount</b></th>
										<th ><b>GST</b></th>
										<th ><b>TOTAL Amount</b></th>
									</tr>";
					  $tabledata.= ' <tr>';
					  $tabledata.= '<td  align="center"><b>'.$receiptdetails['0']['registrationPin'].'</b></td>                                    
							        <td  align="center" ><b>'.$receiptdetails['0']['ProgramName'].'</b></td> 
                                    <td  align="center"><b>'.$receiptdetails['0']['totalNoofCandidates'].'</b></td> 									
							        <td  align="center" ><b>'.number_format($amount,2).'</b></td> 
							        <td  align="center" ><b>'.number_format($tax,2).'</b></td> 
									<td  align="center" ><b>'.number_format($totalamount,2).'</b></td>									
							</tr> ';							 
                $number = number_format($totalamount, 2, ".", "");
                sscanf($number, '%d.%d', $whole, $fraction);                
				$number = number_format($totalamount, 2, ".", "");
				$gst = $lobjcompanystudentdetails->fngettaxrate();
				$Amountinwords = $lobjcompanystudentdetails->fnconvertnumbertowords($totalamount);
                $amount = ($totalamount-$tax);	
                if($fraction){
                     $fractioninwords = $lobjcompanystudentdetails->fnconvertnumbertowords($fraction);
                     $Amountinwords=$Amountinwords.' and '.$fractioninwords.' cent Only';
                }else{
                   $Amountinwords=$Amountinwords.' Only';
                }
                 $tabledata.='<tr >             						   
                                        <td  colspan = "5" align="right"><b>Sub-Total:<b></td><td align="right"><b>'.number_format($amount,2).'</b></td>
                                    </tr>
                                    <tr >             						   
                                        <td  colspan = "5" align="right"><b>GST payable @'.$gst['GST'].'%:<b></td><td align="right"><b>'.number_format($receiptdetails['0']['GST'],2).'</b></td>
                                    </tr>
                                <tr >             						   
                                <td  colspan = "5" align="right"><b>Total Amount:<b></td><td align="right"><b>'.number_format($totalamount,2).'</b>
							   </td>
						</tr>
                        <tr>
			            <td  align="center" colspan="6"><b>Ringgit Malaysia:</b><b>';                              					  
                $tabledata.=$Amountinwords.' </b></td></tr></table>';
		        //$Amountinwords = $this->lobjcompanystudentdetails->fnconvertnumbertowords($receiptdetails['Totalamount']);		 
		        // $tabledata.= '<tr>';
		        // $tabledata.= '<th align="right" colspan="5"><b>Total Amount:</b></th><th>'.number_format($receiptdetails['Totalamount']).'.00'.'</th></tr>';
                // $tabledata.= '<tr><th colspan="6" >Ringgit Malaysia:<b>'.$Amountinwords.' Only</b></th></tr></table>';
                $tabledata.="<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                             <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
		        $tabledata.='<table>
								<tr><td><b>Note:</b></td>
								</tr>
								<tr><td colspan=6><b>1.  Please make payment within 14 Days from the date of "registration" to the date of "this invoice".
                                 </b></td>
								</tr>
								<tr><td colspan=6><b>2.  All cheques must be  made payable to "IBFIM-TBE".</b></td>
								</tr>
								<tr><td colspan=6><b>3.  Please send this copy of invoice together with your payment for the attention of TBE Secretariat, Learning Management				
	                                      Talent Development Department, IBFIM.</b></td>
								</tr><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                                <tr><td align="center"><b>This is a computer generated document.  No signature is required.</b></td></tr>
								<tr>
								<td><hr/></td></tr>
								<tr>
								<td align="center"><b>IBFIM (Co. No. 763075W)
								3rd Floor, Menara Takaful Malaysia, Jalan Sultan Sulaiman,  50000 Kuala Lumpur, MALAYSIA</td></tr>
								<tr><td align="center"><b>(Tel) +603-2031 1010  (Fax) +603-2026 9988   (E-mail) tbe@ibfim.com
								</b></td></tr>
						</table>';
			
			
			
			
			
			
			
			
			
			
			$tabledata.= "<br/>";
			
			$tabledata.="<div style='border:0.5mm solid;font-family:arial;'>";
			$tabledata.="<table border='0' width='100%' align='center' >
							 	<tr>
								 	<td align='left' width='50%'><b>No. {$rcn}<br/></b></td>
								  	<td align='left' style='font-size:130%;'><b><u><i>RECEIPT</i></u></b><br/></td>
								 	<td align='left'><b>Date: {$date}<br/></b></td>
								</tr>
								<tr >
								  	<td align='left' colspan='3'><i>Received With Thanks from:<b>{$larrformData['CompanyName']}</b></i><br/></td>
								</tr>
								<tr>
									<td align='left' colspan='3'><i>Address : {$larrformData['Address']}</i></td>
								</tr>
								<tr>
									<td align='left' colspan='3' style='padding-left:7%'>
										<i>{$larrformData['StateName']}<br/>
				 							{$larrformData['CountryName']}<br/>
				 							PostCode:{$larrformData['Postcode']}<br/>
				 							Phone:{$larrformData['Phone']}<br/>
				 							Email:{$larrformData['Email']}<br/>
				 							Fax:{$fax}<br/>				 							
				 						 </i>
									</td>
									
								</tr>
								 <tr>
									<td align='left' colspan='3'>Attn:{$larrformData['Contact']}</td>
								</tr>
								<tr >
									<td align='left' colspan='3'><i>Payment By: {$mode}.</i><br/></td>
								</tr>
								<tr>
									<td align='left' colspan='3'><i>Towards Examination Fees as detail bellow.</i><br/></td>
								</tr>
						</table>";
			
			$tabledata.="<br/><table border='1' width='100%' align='center' style='margin:1em 1cm;'>
							<tr>
								
								<th ><b>BatchId</b></th>							
								<th ><b>Invoice Number</b></th>
								<th ><b>Total Candidates</b></th>
								<th ><b>Amount</b></th>							
								<th ><b>GST</b></th>
								<th ><b>Total Amount</b></th>
                                                                <th ><b>Transaction Id</b></th>
                                                               
							</tr>";
                        
						$count=1;
						foreach($receiptdetails as $receiptdetails){
						 $tax=$receiptdetails['GST'];
					  $amount =$receiptdetails['Invamount'];
					  $totalamount = ($amount+$tax);
						
							$tabledata.="<tr>
								
                                                                <td >{$receiptdetails['BatchId']}</td>	    		
								<td align='center'>".$receiptdetails['InvoiceNum']."</td>	    		       
							  	<td align='right'>".$receiptdetails['totalNoofCandidates']."</td>
							  	<td align='right'>".number_format($amount,2)."</td>	    		
								<td align='right'>".number_format($tax,2)."</td>	    		       
							 	<td align='right'>".number_format($totalamount,2)."</td>
                                                                <td align='right'>".$receiptdetails['transactionId']."</td>
                                                               
							</tr>";
							 $count++;
						}
						
						
			$tabledata.="</table><table border='0' width='100%' align='center'>
							<tr >             						   
								<td align='center' colspan='3'><b>Total Amount Received(Ringgit Malaysia) : ".number_format($totalamt,2)."<b></td>
							</tr>
																			<tr><td></td></tr>
							<tr>
									<td align='center' colspan='3'><b>Ringgit Malaysia(In Words): {$Amountinwords}.</b><br/></td>
							</tr>
							<tr><td></td></tr>
												
												<tr >
							                    	<td align='center' colspan='3'><b>GST Ref.No:001167200256 </b></td>
							                    </tr>
						</table>
						<table border='0' width='100%' align='center'>
							<tr><td></td></tr>
							<tr >             						   
								<td  style='padding-left:85%' ><b>Signature<b></td>
								
							</tr>
							<tr>
							<td>$condition</td>
							</tr>
						</table>
				</div>";
			
 	//echo $tabledata;die();
     		if($lstrreportytpe=='Pdf'){
			ob_clean();
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="' . $filename . '"');
			header('Content-Transfer-Encoding: binary');
			header('Accept-Ranges: bytes');

			include(APPLICATION_PATH.'/../library/mpdf60/mpdf.php');
			$mpdf = new mPDF('utf-8','A3','','',20,15,10,16,9,9,'L');
			$mpdf->SetDirectionality ( $this->gstrHTMLDir );
			$mpdf->text_input_as_HTML = true;
			$mpdf->autoLangToFont  = true;
			$mpdf->autoScriptToLang = true;
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
			$mpdf->pagenumSuffix = ' / ';
			$mpdf->setFooter ('Copyright &copy; 2011, Islamic Banking and Finance Institute Malaysia Sdn, Bhd.'.'       '.'{PAGENO}{nbpg}' );
			$mpdf->allow_charset_conversion = true; // Set by default to TRUE
			$mpdf->charset_in = 'utf-8';
			ini_set('max_execution_time',3600);
			$mpdf->WriteHTML($tabledata);
			$mpdf->Output($filename,'D');
		}
		}
     
}

public function mipgrequestingAction()
{
	if (!$this->_request->getPost ( 'Print' )){
           
		$operator = $this->_getParam('operatortype');
		$migsarray = $_GET;
			//echo "<pre/>";print_r($migsarray);
                ////////avoid duplicate on refresh///
                $responsecode = $_GET['vpc_TxnResponseCode'];
		$idbatchregistration =$_GET['vpc_MerchTxnRef'];
                //$dupbatch = $this->lobjstudentmodel->fngetbatchdupmigspayment($_GET,$operator);
		$larresult = $this->lobjstudentmodel->fninsertmigspayment($_GET,$operator);
                
		
		$larresultbatchdetails = $this->lobjstudentmodel->fngetbatchregistrationdetails($idbatchregistration);
			 //echo "<pre/>";print_r($larresultbatchdetails);die();
		$this->view->idcompany = $larresultbatchdetails['idCompany'];
		if($responsecode=='2')
		{
			$regpin = $larresultbatchdetails['registrationPin'];
			if($regpin==0)
			{
				$randomnumber1 = rand(100000,999999);
			$randomnumber2 = rand(100000,999999);
			$regpin  = $randomnumber1.''.$randomnumber2;
			}
			
			if($operator == 2)
			{
									 $Typeofoperator = 'T';
			}
			if($operator ==1)
			{
								   $Typeofoperator = 'C';
			}
            $Uniqueid = "INV/IBFIM/".$Typeofoperator.'/'.$larresultbatchdetails['idCompany'].'/'.date('Y').'/';
			$auth = Zend_Auth::getInstance(); 
			$upduser =  $auth->getIdentity()->iduser;
			
				//changes 15-05-2015
			$gst = $this->lobjcompanystudentdetails->fngettaxrate();
		    $taxamount=$larresultbatchdetails['totalAmount']*($gst['GST']/100);

			
			$larresult = $this->lobjstudentmodel->fnupdatebatchregdetails($idbatchregistration,$regpin);
			$this->lobjstudentmodel->fngenerateinvoice($larresultbatchdetails,$Uniqueid,$upduser,$larresultbatchdetails['idCompany'],$operator,$regpin,$taxamount);
			$larrinvoicenum = $this->lobjstudentmodel->getinvoicenumber($regpin);	
			
					if($operator==1){//////for company only
						$companynamedata=$this->lobjstudentmodel->fngetcompanyname($larresultbatchdetails['idCompany']);
						$companyname=$companynamedata['CompanyName'];
						$month= date('M');
						$Uniqueid = "RCP/IBFIM/".$companyname.'/'.$month.'/'.date('Y').'/';
						$auth = Zend_Auth::getInstance();
						$upduser =  1;        
						$receiptlastid=$this->lobjstudentmodel->fnCreateReceipNumber($regpin,$Uniqueid,$operator,$larresultbatchdetails['idCompany'],$larresultbatchdetails['totalAmount'],$upduser,$larrinvoicenum);
						$this->_redirect( $this->baseUrl . '/companyapplication/viewreceipt/receiptlastid/'.$receiptlastid.'/idbatchregistration/'.$idbatchregistration);

					}
			$this->view->status = 1;
		}
		else
		{

		}
	}
        
        
	
	
}

}

	