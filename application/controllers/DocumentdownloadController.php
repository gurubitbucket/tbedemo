<?php
error_reporting (E_ALL ^ E_WARNING);
error_reporting (E_ALL ^ E_NOTICE);
class DocumentdownloadController extends Zend_Controller_Action
{
	public function init() 
	{ 
		
		$this->_helper->layout()->setLayout('/batch3/usty1');	
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
	}
	
	Private function fnsetObj() 
	{
		$this->lobjDocumentdownloadmodel=new App_Model_Documentdownload();//object of common model
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
		
	public function indexAction() 
	{
		$largarrayresult=$this->lobjDocumentdownloadmodel->fnGetUploadfiles();
		$this->view->lararrdata=$largarrayresult;
	}
	
	public function downloadzipAction() 
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$lstruploaddir="/uploads/documentsuploads/";
		$larrformData = $this->_request->getPost();
		
		$largarrayresultsss=$this->lobjDocumentdownloadmodel->fnGetUploadfilesss($larrformData['idapp'][0]);
				
		if(!empty($larrformData['Photo']))
		{
			$filenamePhoto=$largarrayresultsss['Photo'];
			$fileaddersPhoto = realpath('.').$lstruploaddir . $filenamePhoto;
		}
		if(!empty($larrformData['Address']))
		{
			$filenameAddress=$largarrayresultsss['Address'];
			$fileaddersAddress = realpath('.').$lstruploaddir . $filenameAddress;
		}
		if(!empty($larrformData['Passport']))
		{
			$filenamePassport=$largarrayresultsss['passport'];
			$fileaddersPassport = realpath('.').$lstruploaddir . $filenamePassport;
		}
		if($filenamePhoto && $filenameAddress && $filenamePassport)
			$files = array($filenamePhoto, $filenameAddress, $filenamePassport);
		elseif($filenameAddress && $filenamePassport)
			$files = array($filenameAddress, $filenamePassport);
		elseif($filenamePhoto && $filenameAddress)
			$files = array($filenamePhoto, $filenameAddress);
		elseif($filenamePhoto && $filenamePassport)
			$files = array($filenamePhoto, $filenamePassport);
		elseif($filenamePhoto)
			$files = array($filenamePhoto);
		elseif($filenamePassport)
			$files = array($filenamePassport);
		else
			$files = array($filenameAddress);
				
		$fileDir = './uploads/documentsuploads/';
		ob_start(); 
		require_once "Zip/Zip.php"; 
		$zip = new Zip();
		if ($files) 
		{
			foreach ($files as $file) 
			{
					$pathData = pathinfo($fileDir . $file);
					//$fileName = $pathData['filename'];	
					$zip->addFile(file_get_contents($fileDir.$file),$file);	
			}
		}
		$zip->sendZip("Documents_".time().".zip");	
	}
}
