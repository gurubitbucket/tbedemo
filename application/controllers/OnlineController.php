<?php
class OnlineController extends Zend_Controller_Action {
	private $gobjlog;//class global variable
	private $gstrtranslate;//class global variable
	private $gstrsessionName;//Global Session Name
   	private $gintPageCount;//Global Pagination Count
   	private $_gobjlogger;
   	public $gsessionregistration;
   	
	public function init()
	{ 
		$this->_helper->layout()->setLayout('/reg/usty1');
		$this->view->translate = Zend_Registry::get('Zend_Translate'); 
		$this->_gobjlogger = Zend_Registry::get( 'logger' ); //instantiate log object
   	    //Zend_Form::setDefaultTranslator($this->view->translate);
		$this->fnsetObj();
   	}
   	
	public function fnsetObj(){
		$this->lobjstudentmodel = new App_Model_Studentapplication(); //user model object
		$this->lobjusermodel = new GeneralSetup_Model_DbTable_User(); //user model object
		$this->registry = Zend_Registry::getInstance();
		$this->locale = $this->registry->get('Zend_Locale');
	}
	
    public function indexAction(){ 
    	
    		$this->gsessionregistration = new Zend_Session_Namespace('sis');    		
    		//$this->gsessionregistration =Zend_Registry::set('registration','1');
			$this->view->translate = $this->gstrtranslate; //Set Transaltion 					
 			$lobjsearchform = new App_Form_Registration();  //intialize search lobjuserForm
			$this->view->lobjform = $lobjsearchform; //send the lobjuserForm object to the view
			$lobjRegistrationModel= new App_Model_Examdetails();
			//$this->view->roles = $this->gobjroles;
			 if ($this->_request->isPost() && $this->_request->getPost('Login')){
 		 	$lobjFormData = $this->_request->getPost();
 		 	//print_r($lobjFormData);die();
 		 	$countarr = 0;
 		 	$larrresult = $lobjRegistrationModel->fnGetIdBatch($lobjFormData['id']);
 		 	//$todate = $lobjFormData['Date'];
 			//print_r($larrresult);
 			$countarr = count($larrresult);




$result = $lobjRegistrationModel->checksessionexist($larrresult['IDApplication']);
			if($result)
			{
			    echo '<script language="javascript">alert("One active session is already exist for this registeredid please contact Admin")</script>';
 		 		echo "<script>parent.location = '".$this->view->baseUrl()."/online/index';</script>";
             	die();
			}
			$lobjRegistrationModel->updatesession($larrresult['IDApplication']);
			




 		 	//print_R($countarr);die();
		 	if($countarr!=7)
 		 	{
 		 		echo '<script language="javascript">alert("Registered Id Does Not Exist")</script>';
 		 		echo "<script>parent.location = '".$this->view->baseUrl()."/online/index';</script>";
             	die();
 		 	}
 		 	$larrtodate = $lobjRegistrationModel->fnCheckDate($larrresult['IdBatch']);
 		 	
 		 	$batchtodate = $larrtodate['BatchTo'];
 		 	$todaydate = $lobjFormData['Date'];
 		 	
 		 	/*print_r($lobjFormData['id']);
 		 	die();*/
 		 	
 		 	/////////////////////
 		 	$larrregisterids=$lobjRegistrationModel->fnGetRegisterid();
 		/* 	foreach($larrregisterids as $regid)
 		 	{
 		 		if($lobjFormData['id']==$regid['Regid'])
 		 		{
 		 			
 		 			echo '<script language="javascript">alert("You have already taken the exam")</script>';
 		 		 	echo "<script>parent.location = '".$this->view->baseUrl()."/online/index';</script>";
              		die();
 		 		}
 		 	}*/
 		 	
 		 	$larcheckresults = $lobjRegistrationModel->fncheckavailability($lobjFormData['id']);
 		 	if(count($larcheckresults)>1)
 		 	{
 		 		echo '<script language="javascript">alert("You have already taken the exam")</script>';
 		 		 	echo "<script>parent.location = '".$this->view->baseUrl()."/online/index';</script>";
              		die();
 		 	}
 		 	
 		 	////function for pushed into local////
 		 	$idapplication =$larrresult['IDApplication'];
 		 	$larrresultstudentdetails = $lobjRegistrationModel->fngetstudentlocaldetails($idapplication);
 		 	if($larrresultstudentdetails['VenueTime']=='1')
 		 	{
 		 		echo '<script language="javascript">alert("You have pulled to the local please use the correct URL to take the exam")</script>';
 		 		 	echo "<script>parent.location = '".$this->view->baseUrl()."/online/index';</script>";
              		die();
 		 	}
 		 	
 		 	//////end of the function//////////
 		 	
 		 	
 		/* 	print_r($larrregisterids);
 		 	die();*/
 		 	
 		 	//////////////////////
 		 	
 		 	
 		 
 		 	$this->gsessionregistration->__set('Regid',$lobjFormData['id']);
 		 	$this->gsessionregistration->__set('IdBatch',$larrresult['IdBatch']);
 		 	$this->gsessionregistration->__set('IDApplication',$larrresult['IDApplication']);
			$RegId = $lobjFormData['id'];
		
 		  	$lobjExamdetailsmodel = new App_Model_Examdetails();
 		  $larrtempanswered = $lobjExamdetailsmodel->fnGettempRegisterid($RegId);
 		   
 		  $tempcount = count($larrtempanswered);
 		  if($tempcount>0)
 		  {
 		  	echo "<script>parent.location = '".$this->view->baseUrl()."/examdetails/systemshutdown';</script>";
 		  	die();
 		  }
 		 
 		 		/*$this->gsessionregistration->Regid = $lobjFormData['id'];
 		 		$this->gsessionregistration->IdBatch =$larrresult['IdBatch'];
 		 		$this->gsessionregistration->IDApplication=$larrresult['IDApplication'];*/
 		 		 //$this->_redirect('/examdetails');
 		 	$this->_redirect( $this->baseUrl . 'examdetails/index');
 		 	//print_r($larrresult);
 		 	//die();
		 }      
	}

    function loginAction() {
    	//$this->view->title=$this->view->translate->_("SFS - LOGIN"); //Page Title
    	
    	//Set the ZEND Form  Translation
   	    Zend_Form::setDefaultTranslator($this->view->translate);
    	
        $lobjform = new App_Form_Login(); //intialize login form
                
        $this->view->lobjform = $lobjform; //send the form object to the view
        
        if ($this->_request->isPost()) {
        	Zend_Loader::loadClass('Zend_Filter_StripTags');
            $lobjfilter = new Zend_Filter_StripTags();
            $lstrusername = $lobjfilter->filter($this->_request->getPost('username')); //getting the username by post
            $lstrusername = strtolower($lstrusername);
            $UserStatus = 1;
            $lstrpassword = $lobjfilter->filter($this->_request->getPost('password')); //getting the password by post
            //$lintcompanyid = $lobjfilter->filter($this->_request->getPost('Company'));
			
            if (empty($lstrusername)) {
            	$this->view->lstrmessage = 'Please provide a username';
            } else {
				Zend_Loader::loadClass('Zend_Auth_Adapter_DbTable');   // setup Zend_Auth adapter for a database table
				
                $lobjdb = Zend_Db_Table::getDefaultAdapter();
                $lobjauthAdapter = new Zend_Auth_Adapter_DbTable($lobjdb);//,'tbl_user','loginName','passwd',"UserStatus"); 
                
                $lobjauthAdapter->setTableName('tbl_user');
                $lobjauthAdapter->setIdentityColumn('loginName');
                $lobjauthAdapter->setCredentialColumn('passwd');
                $lobjauthAdapter->setCredentialTreatment('MD5(?) AND UserStatus = 1');

                $lobjauthAdapter->setIdentity($lstrusername);
                $lobjauthAdapter->setCredential($lstrpassword);

                
                //encrypting the password
                $lobjauth = Zend_Auth::getInstance();
                $lobjresult = $lobjauth->authenticate($lobjauthAdapter);

                
                
                if ($lobjresult->isValid()) {
            		$lobjdata = $lobjauthAdapter->getResultRowObject(null, 'password');
					$lobjauth->getStorage()->write($lobjdata);
                    $lobjhrms = Zend_Registry::get('sfs'); 
                    $lobjhrms->lstrusername = $lstrusername;
                              		
                    $lobjuser = new App_Model_User();
		            $larrresult = $lobjuser->fnuserinfo($lobjhrms->lstrusername);
		            
		            //Get Language Configured
		            $lstrLangConfig = $lobjuser->fnGetInitialConfigData();
		            
					//Set the Locale
		            //$this->gstrtranslate->setLocale('ar');
		            
		            
		            foreach($larrresult as $lobjuser){
		            	$lintuid = $lobjuser['iduser'];
		                $lintidrole = $lobjuser['IdRole'];
		                $lstrfname = $lobjuser['fName'];
		                $lstrlname = $lobjuser['lName'];
		                $lstrmname = $lobjuser['mName'];
		                $lstrusername = $lobjuser['loginName'];
		                //$lintcompanyid = $lobjuser['Company'];
		           }
		           
                   /*$sessionName = new Zend_Session_Namespace('sam');
                   $sessionName->__set('primaryuserid',$lintuid); //setting the userid to session variable
                   $sessionName->__set('roleid',$lintidrole); //setting the roleid to session variable
                   $sessionName->__set('username',$lstrusername); //setting the username to session variable
                   $sessionName->__set('name', $lstrfname." ".$lstrmname." ".$lstrlname); //setting the firstname lastname and middlename   to session variable
                   $sessionName->__set('Locale',$lstrLangConfig['Language']);*/
		           
		           $this->gstrsessionName->__set('primaryuserid',$lintuid); //setting the userid to session variable
                   $this->gstrsessionName->__set('roleid',$lintidrole); //setting the roleid to session variable
                   $this->gstrsessionName->__set('username',$lstrusername); //setting the username to session variable
                   $this->gstrsessionName->__set('name', $lstrfname." ".$lstrmname." ".$lstrlname); //setting the firstname lastname and middlename   to session variable
                   $this->gstrsessionName->__set('collegeId',"1"); //setting the Hostel Id
                   //$this->gstrsessionName->__set('hostel',"1"); //setting the Hostel Id
                   
                   
                   //$sessionName->__set('companyid', $lintcompanyid);                
					//set the log parameters
                    $log = array('funcode'=>'Login',
                                 'idUser'=>$lintuid,
                                 'UpdDate'=>date('Y-m-d H:i:s'),
                                 'opcode'=>'login',
                                 'logdesc'=>'Succesful Login',
                                 'usernd'=>$this->getRequest()->getServer('REMOTE_ADDR'));
                                   
                    $this->gobjlog->write($log);   //insert to tbl_log
                     /*$auth = Zend_Auth::getInstance();
	           $priority=Zend_Log::INFO;
				$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
				$message = "\t\t\t\t".$controller."\t\t\t\t"."Successfully Logged in"."\t\t\t\t".$this->getRequest ()->getServer ( 'REMOTE_ADDR' )."\t\t\t\t"."Success"."\t\t\t\t".."\t\t\t\t\r";
				$this->_gobjlogger->log($message,5);*/
                    //$this->_redirect('/index/index'); //redirect to index                    
                    $this->_redirect('/offlinemessages/index/p/new'); // redirect to new message page
                                   
           }else {         	
               $lobjuser = new App_Model_User(); //user model object

               $larrresult = $lobjuser->fnuserinfo($lstrusername); //get user details
		       if(count($larrresult)) {
		          //set the log parameters
                  $log = array('funcode'=>'Login',
                               'idUser'=>$larrresult['0']['iduser'],
                               'UpdDate'=>date('Y-m-d H:i:s'),
                               'opcode'=>'login',
                               'logdesc'=>'Failed Login',
                               'usernd'=>$this->getRequest()->getServer('REMOTE_ADDR'));
                                		
				 $this->gobjlog->write($log);	//insert to tbl_log
		      } else {
		        	//set the log parameters
                    $log = array('funcode'=>'Login',
                                 'idUser'=>'0',
                                 'UpdDate'=>date('Y-m-d H:i:s'),
                                 'opcode'=>'login',
                                 'logdesc'=>'Failed Login '.$lstrusername,
                                 'usernd'=>$this->getRequest()->getServer('REMOTE_ADDR'));
                                		
                    $this->gobjlog->write($log);	//insert to tbl_log
		        }
		      	$this->view->lstrmessage = 'Login failed';     //set error message
     			}
			}
		}  
		$this->render(); //render the view
    }


    public function logoutAction() {
          // Zend_Session::destroy();  //destroy the session
       // $this->_redirect('index/'); //redirect to login page
        
             $log = array('funcode'=>'Logout',
                                 'idUser'=>'0',
                                 'UpdDate'=>date('Y-m-d H:i:s'),
                                 'opcode'=>'logout',
                                 'logdesc'=>'Logout '.$lstrusername,
                                 'usernd'=>$this->getRequest()->getServer('REMOTE_ADDR'));
                                		
                    $this->gobjlog->write($log);
                    
        //Zend_Session::destroy();  //destroy the session
       // Zend_Session:: namespaceUnset('sfslogin');
        Zend_Session:: namespaceUnset('sfs');
        //Zend_Session:: namespaceUnset('sfstranslate');
        $this->_redirect('index/login'); //redirect to login page
    }
}