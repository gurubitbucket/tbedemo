<?php
 error_reporting (E_ALL ^ E_WARNING);
error_reporting ( E_ALL ^ E_NOTICE );
class RegistrationfailedcompanyController extends Zend_Controller_Action { //Controller for the User Module
	
	//private $_gobjlogger;
	
	public function init() { //initialization function		
				
		$this->gsessionbatch = Zend_Registry::get('sis'); 		
		if(empty($this->gsessionbatch->idCompany)){ 
		 	$this->_redirect( $this->baseUrl . '/batchlogin/logout');					
		}
		$this->_helper->layout()->setLayout('/adhoc/usty1');
		$this->view->translate =Zend_Registry::get('Zend_Translate'); 
   	    Zend_Form::setDefaultTranslator($this->view->translate);
   	   // $this->_gobjlogger = Zend_Registry::get ( 'logger' ); //instantiate log object			
		$this->lobjTakafulmodel = new App_Model_Takafulapplication();
		$this->lobjCommon = new App_Model_Common ();		
		$this->lobjTakafulcandidatesmodel = new App_Model_Takafulcandidates (); //user model object			
		$this->lobjTakafulForm = new App_Form_Takafulapplication (); //intialize user lobjuserForm			
		$this->lobjstudentForm = new App_Form_Studentapplication ();
		$this->lobjstudentmodel = new App_Model_Studentapplication ();
		$this->lobjcompanymodel = new App_Model_Companyapplication();
		$this->lobjCompanypayment = new Finance_Model_DbTable_Approvecreditstudenttakaful ();			
		$this->registry = Zend_Registry::getInstance ();
		$this->locale = $this->registry->get ( 'Zend_Locale' );
	}
		
	
	public function indexAction() {// Action for Search
	
		$this->view->lobjstudentForm = $this->lobjstudentForm;		
		$this->view->lobjTakafulForm = $this->lobjTakafulForm; //intialize user lobjuserForm
			
		$larrbatchresult = $this->lobjTakafulmodel->fnGetProgramName ();
		$this->view->lobjTakafulForm->idPrograms->addMultiOptions ( $larrbatchresult );
		$this->view->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );
		$this->view->lobjstudentForm->Program->setAttrib('style','width:165px;');  
		
		/// to get Credit to IBFIM account //
		
		$larrresult = $this->lobjcompanymodel->fntogetpaylater($this->gsessionbatch->idCompany);
		if($larrresult)
		{
		   //echo $this->gsessionbatch->idCompany;die();
		   $this->lobjTakafulForm->ModeofPayment->addMultiOption('7','Credit to IBFIM account');
		}
		
		// end of Credit to IBFIM account //
		
		$this->view->idcompany=$this->gsessionbatch->idCompany;
		if(!$this->_getParam('search')) 
			unset($this->gobjsessionsis->companypaginator);			
			$lintpagecount = $this->gintPageCount;		
			$lintpage = $this->_getParam('page',1); //Paginator instance
        		
		$larrresultdiscount = $this->lobjTakafulmodel->fngetintialdiscount ();
		$this->view->discount = $larrresultdiscount ['Discount'];		
		
				$larrpaylater = $this->lobjTakafulmodel->fngetpaylater ( $this->gsessionbatch->idCompany );		
				$cntpaylater = count ( $larrpaylater );
				if ($cntpaylater > 1) {
					$this->view->lobjTakafulForm->ModeofPayment->addMultiOption ( $larrpaylater ['idDefinition'], $larrpaylater ['DefinitionDesc'] );
				}
					$this->view->lobjTakafulForm->ModeofPayment->setValue ( 181 );
		
		if ($this->_request->isPost () && $this->_request->getPost ( 'Save' )) {
			
			$larrformData = $this->_request->getPost ();
			$larrformData['Servicetax'] = number_format($larrformData['Servicetax'], 2);
			if ($this->view->lobjTakafulForm->isValid ( $larrformData )) {	
			$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );
						
			$lastInsId = $this->lobjcompanymodel->fnInsertPaymentdetails($larrformData);
			$flag=1;// If function called from re-registration module
			if ($larrformData ['ModeofPayment'] == 181) {
				$flag=0;
			}
					$larrformDataapp ['IDApplication'] = $lastInsId;
					$larrformDataapp ['companyflag'] = 1;
					$larrformDataapp ['Amount'] = $larrformData ['grossAmt'];
					$larrformDataapp ['UpdUser'] = 1;
					$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
					$regid = $this->lobjCompanypayment->fngeneraterandom ();								
					$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);
				
			$db = Zend_Db_Table::getDefaultAdapter();				
			
			$lastpaymentid=$this->lobjcompanymodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$lastInsId,1);
			
				$auth = Zend_Auth::getInstance();// Write Logs
						
				$lastpayid=$this->lobjcompanymodel->fngetmodeofpayment($lastpaymentid);
				$this->lobjTakafulmodel->fninsertintostudentapplication($larrformData,$lastInsId,$regid,$this->gsessionbatch->idCompany,1);
				
				//  tommorrow begin from here  6-04-2012
			if($lastpayid['ModeofPayment']==4)
			{   
				$this->_redirect( $this->baseUrl . "/companyapplication/display/insertedid/".$lastInsId); 
			}
				if ($lastpayid['ModeofPayment'] == 10) {
					$this->_redirect ( $this->baseUrl . "/companyapplication/migspayment/insertedid/" . $lastInsId );
				}
				if($lastpayid['ModeofPayment']==7)
			{
			    $operator = $this->gobjsessionsis->operatortype;
				$this->_redirect( $this->baseUrl . "/registrationfailedcompany/ibfim/operatortype/$operator/insertedId/".$lastInsId."/idcompany/".$this->gsessionbatch->idCompany);
			}
			if($lastpayid['ModeofPayment']==1)
			{   
				$this->_redirect( $this->baseUrl . "/companyapplication/fpxpageone/insertedId/".$lastInsId); 
			}
			else if($lastpayid['ModeofPayment']!=2)
			{   
				$this->_redirect( $this->baseUrl . "/companyapplication/index/editid/".$lastInsId); 
			}
			else 
			{
				$this->_redirect( $this->baseUrl . "/companyapplication/confirmpayment/insertedId/$lastInsId");
				exit;
			}
			//$this->_redirect($this->view->url(array('module'=>'default' ,'controller'=>'companyapplication', 'action'=>'confirmpayment','insertedId'=>$lastInsId),'default',true));
			exit;	
				}	
		}
		


if ($this->_request->isPost () && $this->_request->getPost ( 'proceed' )) {
			
			$larrformData = $this->_request->getPost ();
			
			//print_r($larrformData);die();
			$lintidcompany=$this->gsessionbatch->idCompany;
			if($larrformData){
				
							$larrformData['UpdUser']= 1;
			$larrformData['UpdDate']= date ( 'Y-m-d:H-i-s' );
						
			$lastInsId = $this->lobjcompanymodel->fnReregistrationPaymentdetails($larrformData);
			$flag=1;// If function called from re-registration module
			if ($larrformData ['ModeofPayment'] == 181) {
				$flag=0;
			}
					$larrformDataapp ['IDApplication'] = $lastInsId;
					$larrformDataapp ['companyflag'] = 1;
					$larrformDataapp ['Amount'] = $larrformData ['totalamount'];
					$larrformDataapp ['UpdUser'] = 1;
					$larrformDataapp ['UpdDate'] = date ( 'Y-m-d:H-i-s' );
					$regid = $this->lobjCompanypayment->fngeneraterandompins ();								
					$larrpaymentdetails = $this->lobjCompanypayment->InsertPaymentOption ( $larrformDataapp, $lastInsId, $regid ,$flag);
				
			$db = Zend_Db_Table::getDefaultAdapter();				
			$operatortype = 1; // 1 for company
			$lastpaymentid=$this->lobjcompanymodel->fnInsertStudentPaymentdetails($larrformData['ModeofPayment'],$lastInsId,$operatortype);
			
				$auth = Zend_Auth::getInstance();// Write Logs
						
				$lastpayid=$this->lobjcompanymodel->fngetmodeofpayment($lastpaymentid);
				$this->lobjTakafulmodel->fninsertintostudentapplicationonpopup($larrformData,$lastInsId,$regid,$this->gsessionbatch->idCompany,1);
				
				//  tommorrow begin from here  6-04-2012
			if($lastpayid['ModeofPayment']==4)
			{   
				$this->_redirect( $this->baseUrl . "/companyapplication/display/insertedid/".$lastInsId); 
			}
				if ($lastpayid['ModeofPayment'] == 10) {
					$this->_redirect ( $this->baseUrl . "/companyapplication/migspayment/insertedid/" . $lastInsId );
				}
				if($lastpayid['ModeofPayment']==7)
			{
			    $operator = $this->gobjsessionsis->operatortype;
				$this->_redirect( $this->baseUrl . "/registrationfailedcompany/ibfim/operatortype/$operator/insertedId/".$lastInsId."/idcompany/".$this->gsessionbatch->idCompany);
			}
			if($lastpayid['ModeofPayment']==1)
			{   
				$this->_redirect( $this->baseUrl . "/companyapplication/fpxpageone/insertedId/".$lastInsId); 
			}
			else if($lastpayid['ModeofPayment']!=2)
			{   
				$this->_redirect( $this->baseUrl . "/companyapplication/index/editid/".$lastInsId); 
			}
			else 
			{
				$this->_redirect( $this->baseUrl . "/companyapplication/confirmpayment/insertedId/$lastInsId");
				exit;
			}
			//$this->_redirect($this->view->url(array('module'=>'default' ,'controller'=>'companyapplication', 'action'=>'confirmpayment','insertedId'=>$lastInsId),'default',true));
			exit;
				
			}
			
			}
	if ($this->_request->isPost () && $this->_request->getPost ( 'Clear' )) {		
			 $this->_redirect( $this->baseUrl . '/registrationfailedcompany/index');
		}
	
	}
	public function ibfimAction()
	{
	    $this->view->operatortype = $operator = 1;
	    //$this->view->operatortype = $operator = $this->_getParam('operatortype');
		$this->view->insertedId =$insertedId = $this->_getParam('insertedId');
	    $this->view->idcompany = $idcompany = $this->_getParam('idcompany');	
	   $larresultbatchdetails = $this->lobjcompanymodel->fngetbatchregistrationdetails($insertedId);
	  
		$regpin = $larresultbatchdetails['registrationPin'];
		if($regpin==0)
		{
			$randomnumber1 = rand(100000,999999);
		    $randomnumber2 = rand(100000,999999);
		    $regpin  = $randomnumber1.''.$randomnumber2;
		}
		
		$larresult = $this->lobjcompanymodel->fnupdatebatchregdetailsforcredittoibfimpayment($insertedId,$regpin);
		
		$this->view->lobjstudentForm = $this->lobjstudentForm;		
		//$idcompany=$this->_getParam('idcompany');
		//if($this->gobjsessionsis->operatortype==1){
				$larrresult = $this->lobjcompanymodel->fngetCompanyDetails($idcompany);	
		//}
		//else
		//{
				//$larrresult = $this->lobjTakafulmodel->fngetTakafulOperator ($idcompany);
		//}	
		$larrPaymentDetails = $this->lobjcompanymodel->fngetPaymentDetails($insertedId);	
		$this->view->data = $larrresult;
		$this->view->PaymentDetails = $larrPaymentDetails;
		$this->view->idstudent = $insertedId;	
	}
	

	public function schedulestudentsAction(){
		
			$this->view->lobjstudentForm = $this->lobjstudentForm;
			
		if ($this->_getParam ( 'regpinnum' )) {	
					
			$larrbatchresult = $this->lobjstudentmodel->fnGetProgramName ();
			$this->lobjstudentForm->Program->addMultiOptions ( $larrbatchresult );			
			$Idregpinnum = $this->_getParam ( 'regpinnum' );				
			$larrstudentresult=$this->lobjTakafulmodel->fngetfailedregstered($Idregpinnum);			
			$this->view->larrresultdata=$larrstudentresult;
			// Function to check the mode of pay and blockin if program already selected
			$larrresultBatch = $this->lobjTakafulmodel->fngetBatchDetails ( $Idregpinnum );			
			$larrgetpaydetails=$this->lobjTakafulcandidatesmodel->fnGetModeofpay($larrresultBatch['idBatchRegistration']);		
			$this->view->mode0fpay=$larrgetpaydetails['ModeofPayment'];		
						
			if($larrgetpaydetails['ModeofPayment']==2){
						$larrgetprogramapplied=$this->lobjTakafulcandidatesmodel->fnGetprogramapplied($larrresultBatch['idBatchRegistration']);			
						$this->lobjstudentForm->Program->setValue($larrgetprogramapplied['idProgram']);
						$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
						$this->view->progid=$larrgetprogramapplied['idProgram'];
			}else{
						$this->view->mode0fpay=2;
						$this->lobjstudentForm->Program->setValue($larrstudentresult[0]['idProgram']);
						$this->lobjstudentForm->Program->setAttrib('readOnly','true');	
						$this->view->progid=$larrstudentresult[0]['idProgram'];
			}
			
		}
		
		$larrreappliedcandidates=$this->lobjTakafulmodel->fngetreappliedcandidates($Idregpinnum);
		$this->view->larrappliedresult = $larrreappliedcandidates;
		
	if ($this->_request->isPost () && $this->_request->getPost ( 'Yes' )) {
			$larrformData = $this->_request->getPost ();
			
			$larrformData ['Examvenue'] = $larrformData ['NewVenue'];
			$resultstate = $this->lobjstudentmodel->fngetstatecity ( $larrformData ['NewVenue'] );
			$larrformData ['ExamState'] = $resultstate ['state'];
			$larrformData ['ExamCity'] = $resultstate ['city'];
			$larrformData ['NewState'] = $resultstate ['state'];
			$larrformData ['NewCity'] = $resultstate ['city'];
			$larrformData ['hiddenscheduler'] = 1;
			
			$availdate=$larrformData['Year']."-".$larrformData['setmonth']."-".$larrformData['setdate'];			
			$larravailseat=$this->lobjTakafulcandidatesmodel->fngetavailseatvenue($larrformData['NewVenue'],$availdate,$larrformData['idsession']);
			$larrformData['scheduler']=$larravailseat['idnewscheduler'];
			
			if(count($larrformData['studentId']) > $larravailseat['availseat']){
				echo '<script language="javascript">alert("The Noof Candidates Exceeds Seat Capacity OF That Venue At Selected Date . Please Re-assign ")</script>';	
				echo "<script>parent.location = '".$this->view->baseUrl()."/takafulcandidates/coursevenue/batchId/".$ids."';</script>";
				exit;
			}else{
			
			if (count ( $larrformData ['studentId'] ) > 0) {					
				$larrbatchregID = $this->lobjTakafulcandidatesmodel->fngetBatchRegistrationPinforexcel ( $Idregpinnum );
				$lintidbatch = $larrbatchregID ['idBatchRegistration'];
				$linttotnumofapplicant = $larrbatchregID ['totalNoofCandidates'];	
				
				$larrinsertstudent = $this->lobjTakafulcandidatesmodel->fnUpdatestudapplication ( $larrformData, $lintidbatch, $linttotnumofapplicant,$Idregpinnum);
			
			} else {
				?>				
			<script>alert("Check any Applicant")</script>
		<?php
			}
			}
			$this->_redirect ( $this->baseUrl . "/registrationfailedcompany/schedulestudents/regpinnum/$Idregpinnum" );
		}
	}
	
	public function getcandidatesAction(){
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		
		$regpin = $this->_getParam ( 'regpin' );  
		$status = $this->_getParam ( 'status' );
		$idprog = $this->_getParam ( 'idprog' );
				
		$larrgetallfailedstudentsearch=$this->lobjTakafulcandidatesmodel->fnSearchFailedStudentsCompany($regpin,$status,$this->gsessionbatch->idCompany,1);
		$this->view->numcand=count($larrgetallfailedstudentsearch);		
		echo Zend_Json_Encoder::encode($larrgetallfailedstudentsearch);
	}
	
	public function getnewprogramddAction(){		
			$this->_helper->viewRenderer->setNoRender(true);
			$this->_helper->layout->disableLayout();
			$status = $this->_getParam ( 'status' );
			$idprog = $this->_getParam ( 'idprog' );	
			if($status==1){
				$larrbatchresultnew = $this->lobjTakafulcandidatesmodel->fnGetProgramNameNewreregistration ($idprog);	
			}else{
				$larrbatchresultnew = $this->lobjTakafulmodel->fnGetProgramName ();
			}		
			$larrgetprogramnewlist = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrbatchresultnew );
			echo Zend_Json_Encoder::encode ( $larrgetprogramnewlist );	
		
	}
		
	public function fngetregpinAction(){		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$idprog = $this->_getParam ( 'idprog' );
		$flag=1;
		$larrgetregpin=$this->lobjTakafulcandidatesmodel->fnGetregpin($idprog,$this->gsessionbatch->idCompany,$flag);
		
		$arrcount=count($larrgetregpin);	    
		$larrgetregpin[$arrcount]['key']=0;
		$larrgetregpin[$arrcount]['value']="Others";
		
		$larrgetregpinDetailss = $this->lobjCommon->fnResetArrayFromValuesToNames ( $larrgetregpin );
		echo Zend_Json_Encoder::encode ( $larrgetregpinDetailss );		
	}
	
	
	
	public function lookupbatchidAction(){		
		// Added on 05-09-2012
		$this->_helper->layout->disableLayout();
	}	
	
	public function refinebatchidAction(){
		// Added on 05-09-2012
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$strcandidatename = $this->_getParam('candidatename');
		$strcandidateicno = $this->_getParam('candidateicno');
		$lintprogramid = $this->_getParam('programid');
		$lintcompanyid = $this->gsessionbatch->idCompany;
		$lintstatus = $this->_getParam('status');
		
		$larrgetregpin=$this->lobjTakafulcandidatesmodel->fnGetregpinrefined($lintprogramid,$lintcompanyid,$strcandidatename,$strcandidateicno,$lintstatus,1);
		echo Zend_Json_Encoder::encode($larrgetregpin);
	}
	
	public function getcandidatelookupAction(){		
		//Added on 07-09-2012
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		$lintidapplication = $this->_getParam('idapplication');
		
		$newlookuparrays['key'][0]=0;
		$newlookuparray[0]['key']=0;
		$newlookuparray[0]['value']=0;
		
		$larrgetcandidatelookup=$this->lobjTakafulcandidatesmodel->fnGetcandidatelookup($lintidapplication);
		$count=count($larrgetcandidatelookup);
		
		for($lokiter=0;$lokiter<$count;$lokiter++){			
			if(!in_array($larrgetcandidatelookup[$lokiter]['key'],$newlookuparrays['key'])){
				$newlookuparrays['key'][$lokiter]=$larrgetcandidatelookup[$lokiter]['key'];
				$newlookuparray[$lokiter]['key']=$larrgetcandidatelookup[$lokiter]['key'];
				$newlookuparray[$lokiter]['value']=$larrgetcandidatelookup[$lokiter]['value'];
			}
		}
	
		$larrgetcandidatelookup = $this->lobjCommon->fnResetArrayFromValuesToNames ( $newlookuparray );
		echo Zend_Json_Encoder::encode($larrgetcandidatelookup);		
	}
	
	
}