<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
    protected function _initSession() {
        ini_set('max_execution_time', 3600);         //Zend_Session::start();
    	$sis = new Zend_Session_Namespace('sis');
	    if (!isset($sis->initialized)) :    
			Zend_Session::regenerateId();
			$sis->initialized = true;
		endif;
		Zend_Registry::set('sis',$sis);
        Zend_Locale::setDefault('en_GB'); 
    }

    protected function _initAppAutoload() {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'App',
            'basePath'  => dirname(__FILE__),));
        return $autoloader;
    }

    protected function _initLayoutHelper() {
    	$this->bootstrap('frontController');
        $layout = Zend_Controller_Action_HelperBroker::addHelper(
    		new App_Controller_Action_Helper_LayoutLoader());   
     }
     
     protected function _initLocale() {
         $locale = new Zend_Locale('ar_AE');
          return $locale;
	}
     
	 protected function _initLog() 
	 {		//Get the config parameters from the config file

/*	 		$config = new Zend_Config_Ini('../application/configs/application.ini','development');	 		
			//Db object 
	
			$db = Zend_Db::factory($config->resources->db->adapter,$config->resources->db->params);			
			//Column mapping of the tbl_log 
			$colmap = array('FunctionCode'=>'funcode','idUser'=>'idUser','UpdDate'=>'UpdDate','OperationCode'=>'opcode','LogDesc'=>'logdesc','UserNode'=>'usernd');
			
			//writer object 
			$writer = new Zend_Log_Writer_Db($db, "tbl_log", $colmap);
			
			//set to registry
	      	Zend_Registry::set("log", $writer);*/
	 }
     
	 protected function _initView() 
	 {
	        $view = new Zend_View();
	        $view->addHelperPath("ZendX/Jquery/View/Helper", "ZendX_Jquery_View_Helper");
	        $view->addHelperPath ( 'Zend/Dojo/View/Helper/', 'Zend_Dojo_View_Helper' );
	        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
	        $viewRenderer->setView($view);
	        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
	 }
	 
	protected function _initAuth() {
		
		$auth = Zend_Auth::getInstance();
        $fc = Zend_Controller_Front::getInstance();
        $fc->registerPlugin(new Sis_Plugin_Auth($auth,null));
        
/*		$helper= new Sis_Helper_Acl();
		$helper->setRoles();
		$helper->setResources();
		$helper->setPrivilages();
		$helper->setAcl();
		$frontController = $this->getResource('frontController');
		$frontController->registerPlugin(new Sis_Plugin_Acl());*/
        
	}
	
	
	
	
	
	protected function _initViewHelpers() {
		
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();
		
		 		
		$view->doctype ('XHTML1_TRANSITIONAL');
		$view->headMeta()->appendHttpEquiv ('Content-Type','text/html;charset=utf-8');
		$view->headMeta()->appendHttpEquiv ('Cache-control','no-cache');
		$view->headMeta()->appendHttpEquiv ('Pragma','no-cache');
		$view->headTitle()->setSeparator (' - ');
		$view->headTitle(APPLICATION_TITLE_SHORT ." - ". APPLICATION_ENTERPRISE_SHORT);
		
		$view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');	
		$view->addHelperPath('Zend/Dojo/View/Helper/', 'Zend_Dojo_View_Helper');	

			$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
	        $viewRenderer->setView($view);
	        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);		
	}
	

	
	protected function setconstants($constants){
        foreach ($constants as $key=>$value){
            if(!defined($key)){
                define($key, $value);
            }
        }
	}
	
	protected function _initTranslate(){
		$registry = Zend_Registry::getInstance();	
		
		 // Create Session block and save the locale
        $session = new Zend_Session_Namespace('session'); 


		$locale = new Zend_Locale('en_US');		
		$file = APPLICATION_PATH . DIRECTORY_SEPARATOR .'languages'. DIRECTORY_SEPARATOR . "en_US.php";

		$translate = new Zend_Translate('array',
            $file, $locale,
            array(
            'disableNotices' => true,    // This is a very good idea!
            'logUntranslated' => false,  // Change this if you debug
            )
        );

        $registry->set('Zend_Locale', $locale);
        $registry->set('Zend_Translate', $translate);
              
        
        return $registry;
	}
	
	protected function _initPlugin(){
		$fc = Zend_Controller_Front::getInstance();
        $fc->registerPlugin(new Sis_Plugin_LangSelector()); 
       // $fc->registerPlugin(new Sis_Plugin_Acl());       
	}

	
	 protected function _initLogp() {
	 	$options = $this->getOption('resources');

	 	$partitionConfig = $this->getOption('log');
        $logOptions = $options['log'];
		$baseFilename = 'application';
	 	$logFilename = '';
        switch(strtolower($partitionConfig['partitionFrequency'])){
            case 'daily':
                $logFilename = $baseFilename.'_'.date('Y_m_d');
                break;

            case 'weekly':
                $logFilename = $baseFilename.'_'.date('Y_W');
                break;

            case 'monthly':
                $logFilename = $baseFilename.'_'.date('m_Y');
                break;

            case 'yearly':
                $logFilename = $baseFilename.'_'.date('Y');
                break;

            default:
                $logFilename = $baseFilename;
        }
		

		
	 	$log = new Zend_Log_Writer_Stream('../application/logs/'.$logFilename.'log.txt');
	 	//$log = new Zend_Log_Writer_Stream('');
	 	$logger = new Zend_Log($log);
	 	//$logger->setEventItem('random',rand(1,10));
	 	Zend_Registry::set("logger", $logger);

	 }

/*protected function _initZFDebug() 
	 {
		$dbResource = $this->getPluginResource('db');
		$dbAdapter = $dbResource->getDbAdapter();
	    $autoloader = Zend_Loader_Autoloader::getInstance();
	    $autoloader->registerNamespace('ZFDebug');
	    $options = array(
	   		'jquery_path' => 'http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js',
	   		'plugins' => array('Variables',
	   		'Html',
	   		'Database' => array('adapter' => array('standard' => $dbAdapter)),
	   		'Memory',
	   		'Time',
	   		'Registry',
	   		'Exception')
	 	);
	    $debug = new ZFDebug_Controller_Plugin_Debug($options);
	    $this->bootstrap('frontController');
	    $frontController = $this->getResource('frontController');
	    $frontController->registerPlugin($debug);
	 }*/

}